package com.gov.purchase.module.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "客户首营提交传入参数")
public class CustomerGspRequestDto {

    private String client;

    @ApiModelProperty(value = "客户编码", name = "cusCode", required = true)
    @NotBlank(message = "客户编码不能为空")
    private String cusCode;

    @ApiModelProperty(value = "客户编码（企业）", name = "cusSelfCode", required = true)
    @NotBlank(message = "客户编码（企业）不能为空")
    private String cusSelfCode;

    @ApiModelProperty(value = "地点", name = "cusSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String cusSite;

    @ApiModelProperty(value = "客户名称", name = "cusName", required = true)
    @NotBlank(message = "客户名称不能为空")
    private String cusName;

    @ApiModelProperty(value = "营业执照编号", name = "cusCreditCode", required = true)
    @NotBlank(message = "营业执照编号不能为空")
    private String cusCreditCode;

    @ApiModelProperty(value = "营业期限", name = "cusCreditDate", required = true)
    @NotBlank(message = "营业期限不能为空")
    private String cusCreditDate;

    @ApiModelProperty(value = "法人", name = "cusLegalPerson", required = true)
    @NotBlank(message = "法人不能为空")
    private String cusLegalPerson;

    @ApiModelProperty(value = "注册地址", name = "cusRegAdd", required = true)
    @NotBlank(message = "注册地址不能为空")
    private String cusRegAdd;

    @ApiModelProperty(value = "许可证编号", name = "cusLicenceNo", required = true)
    @NotBlank(message = "许可证编号不能为空")
    private String cusLicenceNo;

    @ApiModelProperty(value = "发证单位", name = "cusLicenceOrgan")
    private String cusLicenceOrgan;

    @ApiModelProperty(value = "发证日期", name = "cusLicenceDate", required = true)
    @NotBlank(message = "发证日期不能为空")
    private String cusLicenceDate;

    @ApiModelProperty(value = "有效期至", name = "cusLicenceValid", required = true)
    @NotBlank(message = "有效期至不能为空")
    private String cusLicenceValid;

    @ApiModelProperty(value = "邮政编码", name = "cusPostalCode")
    private String cusPostalCode;

    @ApiModelProperty(value = "税务登记证", name = "cusTaxCard")
    private String cusTaxCard;

    @ApiModelProperty(value = "组织机构代码证", name = "cusOrgCard")
    private String cusOrgCard;

    @ApiModelProperty(value = "开户户名", name = "cusAccountName")
    private String cusAccountName;

    @ApiModelProperty(value = "开户银行", name = "cusAccountBank")
    private String cusAccountBank;

    @ApiModelProperty(value = "银行账号", name = "cusBankAccount")
    private String cusBankAccount;

    @ApiModelProperty(value = "随货同行单（票）情况", name = "cusDocState")
    private String cusDocState;

    @ApiModelProperty(value = "企业印章情况", name = "cusSealState")
    private String cusSealState;

    @ApiModelProperty(value = "生产或经营范围", name = "cusScope")
    private String cusScope;

    @ApiModelProperty(value = "经营方式", name = "cusOperationMode")
    private String cusOperationMode;

}
