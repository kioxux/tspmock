package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/27 10:35
 **/
@Data
public class DcReplenishForm {
    @ApiModelProperty(value = "仓库紧急补货单号", example = "JCD201122312")
    private String voucherId;
    @ApiModelProperty(hidden = true)
    private String client;
}
