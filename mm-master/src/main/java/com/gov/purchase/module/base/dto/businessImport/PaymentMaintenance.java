package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.01
 */
@Data
public class PaymentMaintenance {

    @ExcelValidate(index = 0, name = "用户编码", type = ExcelValidate.DataType.STRING, maxLength = 8)
    private String client;

    @ExcelValidate(index = 1, name = "付款主体编码", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String ficoPayingstoreCode;

    @ExcelValidate(index = 2, name = "收费标准", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0)
    private String ficoCurrentChargingStandard;

    @ExcelValidate(index = 3, name = "生效开始日期", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd")
    private String ficoEffectiveDate;
}

