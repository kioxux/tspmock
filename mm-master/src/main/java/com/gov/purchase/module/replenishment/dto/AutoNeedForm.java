package com.gov.purchase.module.replenishment.dto;

import lombok.Data;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/15 16:41
 **/
@Data
public class AutoNeedForm {
    private String proSelfCodes;
}
