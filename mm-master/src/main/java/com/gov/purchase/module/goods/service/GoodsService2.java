package com.gov.purchase.module.goods.service;


import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaGlobalGspConfigure;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductZdy;
import com.gov.purchase.module.goods.dto.*;

import java.util.List;
import java.util.Map;

public interface GoodsService2 {

    /**
     * 商品首营_回车查询
     */
    List<GetProBasicListDTO> getProBasicList(GetProBasicListRequestDTO dto);

    /**
     * 商品首营_商品保存1
     */
    List<GaiaProductBusiness> saveCompProList(List<SaveGaiaProductBusiness> list);

    /**
     * 商品首营_商品保存2
     */
    Map<String, Object> saveBasicProList(SaveBasicProListRequestDTO dto);

    /**
     * 商品查询及维护 商品列表
     */
    PageInfo<GaiaProductBusinessDTO> getProList(GetProListRequestDTO dto);

    /**
     * 商品分类下拉菜单
     */
    List<GoodsClassResponseDTO> getClassList();

    /**
     * 默认选中机构
     */
    GetDefaultSiteResponseDTO getDefaultSite();

    /**
     * 供货单位
     */
    List<String> getSupplierList();

    /**
     * 商品列表编辑批量保存
     *
     * @param business 商品主数据
     * @param remarks  修改原因
     */
    Map<String, Object> proEditList(List<GaiaProductBusiness> business, String remarks, Integer editNoWws);

    /**
     * 商品详情
     */
    GetProDetailsResponseDTO getProDetails(String proSite, String proSelfCode);

    /**
     * 商品详情编辑
     */
    Map<String, Object> proEdit(ProEditRequestDTO dto, String remarks, Integer editNoWws);

    /**
     * 商品数据导出
     */
    Result exportProList(GetProListRequestDTO dto);

    /**
     * 自定义字段名
     */
    GaiaProductZdy getProductZdy(String client, String proSite);

    /**
     * 该商品在首营表中是否存在
     */
    String getProductGspinfoExit(GaiaProductGspinfoKeyDTO dto);

    /**
     * 补充首营
     */
    void replenishProductGsp(GaiaProductGspinfoKeyDTO dto);

    /**
     * 商品首营_获取商品自编码最大值
     */
    String getMaxProSelfCode(GetMaxProSelfCodeDTO dto);

    /**
     * 商品主号码段配置表查询
     *
     * @return
     */
    String getProductNumber();

    List<JyfwooData> getJyfwooDataList(String jyfwName);

    /**
     * 商品、供应商、客户修改是否发起工作流开关
     */
    Integer getCountNeedWorkFlowDC();

    /**
     * 全局GSP开关
     */
    List<GaiaGlobalGspConfigure> getGlobalGspConfigureList(String gggcSite, String gggcCode);

    /**
     * 指定加盟商_商品排除配置表默认值
     */
    void productPositionExcludeDefaultValue(String client);

    /**
     * 获取铺货计划开关
     */
    boolean getSwitchForPH(String client);

    /**
     * 首营必输验证开关
     *
     * @return
     */
    List<GaiaGlobalGspConfigure> getGspRequire(String gggcCode);

    GetDefaultSiteResponseDTO getDefaultSite2();
}
