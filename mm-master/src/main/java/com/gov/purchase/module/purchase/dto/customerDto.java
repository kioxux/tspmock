package com.gov.purchase.module.purchase.dto;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.01
 */
@Data
public class customerDto {

    /**
     * 值
     */
    private String value;

    /**
     * 表示
     */
    private String label;

    /**
     * 统一社会信用代码
     */
    private String creditCode;

    /**
     * 付款条件
     */
    private String cusPayTerm;

    /**
     * 连锁总部
     */
    private String compadmId;

    /**
     * 连锁总部名称
     */
    private String compadmName;

    /**
     * 助记码
     */
    private String cusPym;

}
