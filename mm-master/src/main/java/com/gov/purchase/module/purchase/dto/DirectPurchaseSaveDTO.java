package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "要货保存传入参数")
public class DirectPurchaseSaveDTO extends DirectPurchaseListVO {

}
