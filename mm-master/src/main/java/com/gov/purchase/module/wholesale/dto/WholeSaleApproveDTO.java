package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

/**
 * @description: 批发销售审批传参
 * @author: yzf
 * @create: 2021-11-11 09:18
 */
@Data
public class WholeSaleApproveDTO {

    private String client;

    /**
     * 批发仓库
     */
    private String siteCode;


    /**
     * 销售单号
     */
    private String soId;


    /**
     * 客户编码
     */
    private String soCustomerId;

    /**
     * 销售员编码
     */
    private String gwsCode;

    /**
     * 审核状态
     */
    private String soApproveStatus;

    private String soStartDate;

    private String soEndDate;

    private Integer pageSize;

    private Integer pageNum;

    private String soType;
}
