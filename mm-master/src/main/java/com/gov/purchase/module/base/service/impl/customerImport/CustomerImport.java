package com.gov.purchase.module.base.service.impl.customerImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaDcDataKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.DictionaryMapper;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.excel.Customer;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
public abstract class CustomerImport extends ImportData {

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    DictionaryMapper dictionaryMapper;

    /**
     * 数据集合验证
     *
     * @param dataMap
     * @return
     */
    protected Result checkList(LinkedHashMap<Integer, Customer> dataMap) {
        List<String> errorList = new ArrayList<>();
        // 数据为空
        if (dataMap == null || dataMap.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        // 银行代码
        List<Dictionary> bankList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.BANK.getTable(),
                CommonEnum.DictionaryData.BANK.getField1(),
                CommonEnum.DictionaryData.BANK.getCondition(), CommonEnum.DictionaryData.BANK.getSortFields());
        // 付款条件
        List<Dictionary> paymentTypeList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.PAYMENT_TYPE.getTable(),
                CommonEnum.DictionaryData.PAYMENT_TYPE.getField1(),
                CommonEnum.DictionaryData.PAYMENT_TYPE.getCondition(), CommonEnum.DictionaryData.PAYMENT_TYPE.getSortFields());

        // 加盟商 客户自编码重复验证
        Map<String, List<String>> clientCusCodeMap = new HashMap<>();
        // 遍历验证
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Customer customer = dataMap.get(key);

            // 银行代码
            if (StringUtils.isNotBlank(customer.getCusBankCode())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(bankList, customer.getCusBankCode());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "银行代码"));
                }
            }
            // 付款条件
            if (StringUtils.isNotBlank(customer.getCusPayTerm())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(paymentTypeList, customer.getCusPayTerm());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "付款条件"));
                }
            }

            // 发证日期 有效期至 比较
            if (super.isNotBlank(customer.getCusLicenceDate()) && super.isNotBlank(customer.getCusLicenceValid())) {
                // 发证日期>=有效期至
                if (customer.getCusLicenceDate().compareTo(customer.getCusLicenceValid()) >= 0) {
                    errorList.add(MessageFormat.format(ResultEnum.E0129.getMsg(), key + 1));
                }
            }

            // 主数据验证
            baseCheck(customer, errorList, key);

            // 业务表数据存在判断
            if (!super.isNotBlank(customer.getClient())) {
                continue;
            }
            // 加盟商客户自编码行号
            List<String> clientCusCodeList = clientCusCodeMap.get(customer.getClient() + "_" + customer.getCusSelfCode());
            if (clientCusCodeList == null) {
                clientCusCodeList = new ArrayList<>();
            }
            clientCusCodeList.add(String.valueOf((key + 1)));
            clientCusCodeMap.put(customer.getClient() + "_" + customer.getCusSelfCode(), clientCusCodeList);

            // 加盟商 地点 验证
            // 门店数据
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(customer.getClient());
            gaiaStoreDataKey.setStoCode(customer.getCusSite());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            // DC数据
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(customer.getClient());
            gaiaDcDataKey.setDcCode(customer.getCusSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (gaiaStoreData == null && gaiaDcData == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0117.getMsg(), key + 1));
            }

            // 业务数据验证
            businessCheck(customer, errorList, key);
        }
        // 加盟商客户自编码重复验证
        for (String clientCusCode : clientCusCodeMap.keySet()) {
            List<String> lineList = clientCusCodeMap.get(clientCusCode);
            if (lineList.size() > 1) {
                errorList.add(MessageFormat.format(ResultEnum.E0138.getMsg(), String.join(",", lineList)));
            }
        }
        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }

    /**
     * 主数据表验证
     *
     * @param customer
     * @param errorList
     * @param key
     */
    protected abstract void baseCheck(Customer customer, List<String> errorList, int key);

    /**
     * 业务数据验证
     *
     * @param customer
     * @param errorList
     * @param key
     */
    protected abstract void businessCheck(Customer customer, List<String> errorList, int key);
}

