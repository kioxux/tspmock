package com.gov.purchase.module.goods.dto;

import com.gov.purchase.common.entity.Pageable;
import com.gov.purchase.entity.GaiaStoreData;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "首营列表传入参数")
public class QueryGspRequestDto extends Pageable {

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "商品名", name = "proName")
    private String proName;

    @ApiModelProperty(value = "批准文号", name = "proRegisterNo")
    private String proRegisterNo;

    @ApiModelProperty(value = "生产厂家", name = "proFactoryName")
    private String proFactoryName;

    @ApiModelProperty(value = "规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "剂型", name = "proForm")
    private String proForm;

    @ApiModelProperty(value = "供货单位名称", name = "proSupplyName")
    private String proSupplyName;

    @ApiModelProperty(value = "质量标准", name = "proQs")
    private String proQs;

    @ApiModelProperty(value = "地点", name = "proSite")
    private String proSite;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "地点权限", name = "limitList")
    private List<GaiaStoreData> limitList;

    @ApiModelProperty(value = "首营日期", name = "proGspDateStart")
    private String proGspDateStart;

    @ApiModelProperty(value = "首营日期", name = "proGspDateEnd")
    private String proGspDateEnd;

    @ApiModelProperty(value = "审批状态", name = "proGspStatus")
    private String proGspStatus;
}
