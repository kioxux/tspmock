package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductUpdateItem;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.09
 */
@Data
public class ProductUpdateItem extends GaiaProductUpdateItem {

    /**
     * 商品编号
     */
    private String proSelfCode;

    /**
     * 旧值
     */
    private String oldVal;

    /**
     * 新值
     */
    private String newVal;

    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 厂家
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 批准文号
     */
    private String proRegisterNo;
}

