package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@ApiModel(value = "采购订单明细LIST")
public class PoItemListDto {

    /**
     * 订单行号
     */
    private String poLineNo;

    /**
     * 商品自编码
     */
    private String poProCode;

    /**
     * 商品名称
     */
    private String poProName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryCode;

    /**
     * 生产厂家名
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 订单数量
     */
    private BigDecimal poQty;

    /**
     * 单位
     */
    private String poUnit;

    /**
     * 单位名
     */
    private String poUnitName;

    /**
     * 单价
     */
    private BigDecimal poPrice;

    /**
     * 税率
     */
    private String poRate;

    /**
     * 税率名
     */
    private String poRateName;

    /**
     * 总行价
     */
    private BigDecimal poLineTotalaMount;

    /**
     * 地点
     */
    private String poSiteCode;

    /**
     * 地点名
     */
    private String poSiteName;

    /**
     * 库位
     */
    private String poLocationCode;

    //存储条件
    private String proStorageCondition;

    /**
     * 批次
     */
    private String poBatch;

    /**
     * 计划交货日期
     */
    private String poDeliveryDate;

    /**
     * 行备注
     */
    private String poLineRemark;

    /**
     * 删除标记
     */
    private String poLineDelete;

    /**
     * 交货已完成标记
     */
    private String poCompleteFlag;

    /**
     * 已交货数量
     */
    private BigDecimal poDeliveredQty;

    /**
     * 已开票数量
     */
    private BigDecimal poInvoiceQty;

    /**
     * 已交货金额
     */
    private BigDecimal poDeliveredAmt;

    /**
     * 已开票金额
     */
    private BigDecimal poInvoiceAmt;

    /**
     * 商品集合
     */
    List<GetProductListResponseDto> productList;

    /**
     * 生产批号
     */
    private String poBatchNo;

    /**
     * 生产日期
     */
    private String poScrq;
    private String proDepict;
    private String proCommonName;
    private String proRegisterNo;

    /**
     * 有效期
     */
    private String poYxq;

    /**
     * 凭证数量
     */
    private Integer countCertificate;

    /**
     * 发票价
     */
    private BigDecimal poFpj;

    /**
     * 仓库库存
     */
    private BigDecimal wmKcsl;

    private BigDecimal lastPoPrice;

    /**
     * 中包装
     */
    private String proMidPackage;

    /**
     * 大包装
     */
    private String proBigPackage;

    /**
     * 零售价
     */
    private BigDecimal proLsj;
}