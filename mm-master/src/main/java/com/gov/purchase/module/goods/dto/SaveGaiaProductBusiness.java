package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.11
 */
@Data
public class SaveGaiaProductBusiness extends GaiaProductBusiness {

    private String proIfGmp;

    private String proSupplyName;
    /**
     * 是否订购
     */
    private Integer proOrderFlag;
    /**
     * 首营日期
     */
    private String proGspDate;

    /**
     * 铺货计划
     */
    private DistributionDTO distributionDTO;

    /**
     * 国药第三方编码
     */
    private String goodsownid;

    /**
     * 国药第三方药品名称
     */
    private String goodsname;
    /**
     * 国药第三方药品统计码
     */
    private String goodsno;
    private BigDecimal resaleprice;

    /**
     * 主治功能
     */
    private String proFunction;

    /**
     * 生产企业营业执照
     */
    private String proScqyyyzz;

    /**
     * 生产企业详细地址
     */
    private String proScqyxxdz;
}
