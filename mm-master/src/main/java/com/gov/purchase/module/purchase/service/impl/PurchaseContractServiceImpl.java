package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.DecimalUtils;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaContractMLogMapper;
import com.gov.purchase.mapper.GaiaContractMMapper;
import com.gov.purchase.mapper.GaiaContractZLogMapper;
import com.gov.purchase.mapper.GaiaContractZMapper;
import com.gov.purchase.module.base.dto.CustomerDto;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.GaiaContractZDetails;
import com.gov.purchase.module.purchase.dto.PurchaseContractDetailVO;
import com.gov.purchase.module.purchase.service.PurchaseContractService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 采购合同
 * @author: yzf
 * @create: 2022-02-07 09:41
 */
@Service
public class PurchaseContractServiceImpl implements PurchaseContractService {

    @Resource
    private FeignService feignService;
    @Resource
    private RedisClient redisClient;
    @Resource
    private GaiaContractZMapper gaiaContractZMapper;
    @Resource
    private GaiaContractMMapper gaiaContractMMapper;
    @Resource
    private GaiaContractZLogMapper gaiaContractZLogMapper;
    @Resource
    private GaiaContractMLogMapper gaiaContractMLogMapper;
    @Resource
    private CosUtils cosUtils;

    @Override
    public Result invalidPurchaseContract(String conId) {
        TokenUser user = feignService.getLoginInfo();
        GaiaContractZ gaiaContractZ = gaiaContractZMapper.selectByConId(user.getClient(), conId);
        if (gaiaContractZ != null) {
            if ("3".equals(gaiaContractZ.getConType())) {
                gaiaContractZMapper.invalidPurchaseContract(user.getClient(), conId);
                gaiaContractMMapper.invalidPurchaseContract(user.getClient(), conId);
                gaiaContractZLogMapper.deleteByConId(user.getClient(), conId);
                gaiaContractMLogMapper.deleteByConId(user.getClient(), conId);
            } else {
                throw new CustomResultException("当前合同状态不可以作废");
            }
        }
        return ResultUtil.success();
    }

    @Override
    public Result queryPurchaseContractDetail(String conId) {
        TokenUser user = feignService.getLoginInfo();
        // 合同主表
        List<GaiaContractZDetails> gaiaContractZDetailsList = gaiaContractZMapper.selectQueryContractList(user.getClient(), null, null,
                null, conId, null, null, null, null, null);
        if (CollectionUtils.isEmpty(gaiaContractZDetailsList)) {
            throw new CustomResultException("合同编号不存在");
        }
        GaiaContractZDetails gaiaContractZDetails = gaiaContractZDetailsList.get(0);
        // 合同明细表
        List<PurchaseContractDetailVO> purchaseContractDetailVOList = gaiaContractMMapper.queryContractDetail(user.getClient(), conId);
        gaiaContractZDetails.setDetails(purchaseContractDetailVOList);
        return ResultUtil.success(gaiaContractZDetails);
    }

    @Override
    public Result queryPurchaseContractRecord(String conCompanyCode, String conSupplierId, String conSalesmanCode) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaContractZDetails> gaiaContractZDetails = gaiaContractZMapper.selectQueryContractList(user.getClient(), conCompanyCode, conSupplierId, conSalesmanCode,
                null, null, null, null, null, "1");
        return ResultUtil.success(gaiaContractZDetails);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result submitPurchaseContract(GaiaContractZDetails gaiaContractZDetails) {
        if (gaiaContractZDetails == null) {
            throw new CustomResultException("参数为空");
        }
        TokenUser user = feignService.getLoginInfo();
        gaiaContractZDetails.setClient(user.getClient());
        if (StringUtils.isBlank(gaiaContractZDetails.getConId())) {
            // 现有数据查询
            List<GaiaContractZDetails> gaiaContractZDetailList = gaiaContractZMapper.selectQueryContractList(user.getClient(), gaiaContractZDetails.getConCompanyCode(),
                    gaiaContractZDetails.getConSupplierId(), gaiaContractZDetails.getConSalesmanCode(),
                    null, null, null, null, null, "1");
            if (CollectionUtils.isNotEmpty(gaiaContractZDetailList)) {
                throw new CustomResultException("供应商合同已存在，请查询后再试");
            }

            GaiaContractZ gaiaContractZ = new GaiaContractZ();
            GaiaContractZLog gaiaContractZLog = new GaiaContractZLog();
            BeanUtils.copyProperties(gaiaContractZDetails, gaiaContractZ);
            String conId = gaiaContractZMapper.getConId(user.getClient(), "HT" + DateUtils.getCurrentDateStr("yyyy"));
            while (redisClient.exists(StringUtils.parse(CommonConstants.GAIA_CONTRACT_Z, user.getClient(), conId))) {
                String newId = StringUtils.leftPad(String.valueOf(NumberUtils.toLong(StringUtils.right(conId, 6)) + 1), 6, "0");
                conId = "HT" + DateUtils.getCurrentDateStr("yyyy") + newId;
            }
            redisClient.set(StringUtils.parse(CommonConstants.GAIA_CONTRACT_Z, user.getClient(), conId), conId, 1800);
            gaiaContractZ.setConId(conId);
            gaiaContractZ.setConType("0");
            gaiaContractZ.setConCreateBy(user.getUserId());
            gaiaContractZ.setConCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaContractZ.setConCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaContractZMapper.insertSelective(gaiaContractZ);
            BeanUtils.copyProperties(gaiaContractZ, gaiaContractZLog);
            gaiaContractZLog.setConCompileIndex(0);
            gaiaContractZLogMapper.insertSelective(gaiaContractZLog);
            if (CollectionUtils.isNotEmpty(gaiaContractZDetails.getDetails())) {
                List<GaiaContractM> gaiaContractMS = new ArrayList<>();
                List<GaiaContractMLog> gaiaContractMLogs = new ArrayList<>();
                int index = 0;
                for (PurchaseContractDetailVO vo : gaiaContractZDetails.getDetails()) {
                    GaiaContractM gaiaContractM = new GaiaContractM();
                    GaiaContractMLog gaiaContractMLog = new GaiaContractMLog();
                    BeanUtils.copyProperties(vo, gaiaContractM);
                    gaiaContractM.setClient(user.getClient());
                    gaiaContractM.setConCompanyCode(gaiaContractZDetails.getConCompanyCode());
                    gaiaContractM.setConSupplierId(gaiaContractZDetails.getConSupplierId());
                    gaiaContractM.setConId(conId);
                    gaiaContractM.setConLineNo(String.valueOf(index++));
                    gaiaContractM.setConCreateBy(user.getUserId());
                    gaiaContractM.setConCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    gaiaContractM.setConCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    gaiaContractM.setConLineAmt(DecimalUtils.multiply(gaiaContractM.getConQty(), gaiaContractM.getConPrice()));
                    gaiaContractMS.add(gaiaContractM);
                    BeanUtils.copyProperties(gaiaContractM, gaiaContractMLog);
                    gaiaContractMLog.setConCompileIndex(0);
                    gaiaContractMLogs.add(gaiaContractMLog);
                }
                gaiaContractMMapper.insertBatch(gaiaContractMS);
                gaiaContractMLogMapper.insertBatch(gaiaContractMLogs);
            }
        } else {
            GaiaContractZKey gaiaContractZKey = new GaiaContractZKey();
            gaiaContractZKey.setClient(user.getClient());
            gaiaContractZKey.setConCompanyCode(gaiaContractZDetails.getConCompanyCode());
            gaiaContractZKey.setConSupplierId(gaiaContractZDetails.getConSupplierId());
            gaiaContractZKey.setConId(gaiaContractZDetails.getConId());
            // 合同主数据
            GaiaContractZ gaiaContractZ = gaiaContractZMapper.selectByPrimaryKey(gaiaContractZKey);
            if (gaiaContractZ == null) {
                throw new CustomResultException("修改数据不存在");
            }
            // 状态判断
            if ("2".equals(gaiaContractZ.getConType()) || "3".equals(gaiaContractZ.getConType())) {
                throw new CustomResultException(MessageFormat.format("合同编号{0}不可修改", gaiaContractZDetails.getConId()));
            }
            // 已审核
            String conType = gaiaContractZ.getConType();
            if ("1".equals(gaiaContractZ.getConType())) {
                gaiaContractZ.setConType("2");
                gaiaContractZMapper.updateByPrimaryKeySelective(gaiaContractZ);
            } else { // 待审核
                // 主表更新
                BeanUtils.copyProperties(gaiaContractZDetails, gaiaContractZ);
                gaiaContractZMapper.updateByPrimaryKeySelective(gaiaContractZ);
                // 明细表删除
                gaiaContractMMapper.invalidPurchaseContract(user.getClient(), gaiaContractZDetails.getConId());
                // 明细表插入
                if (CollectionUtils.isNotEmpty(gaiaContractZDetails.getDetails())) {
                    List<GaiaContractM> gaiaContractMList = new ArrayList<>();
                    int index = 0;
                    for (PurchaseContractDetailVO vo : gaiaContractZDetails.getDetails()) {
                        GaiaContractM gaiaContractM = new GaiaContractM();
                        BeanUtils.copyProperties(vo, gaiaContractM);
                        gaiaContractM.setClient(user.getClient());
                        gaiaContractM.setConCompanyCode(gaiaContractZDetails.getConCompanyCode());
                        gaiaContractM.setConSupplierId(gaiaContractZDetails.getConSupplierId());
                        gaiaContractM.setConId(gaiaContractZDetails.getConId());
                        gaiaContractM.setConLineNo(String.valueOf(index++));
                        gaiaContractM.setConCreateBy(gaiaContractZ.getConCreateBy());
                        gaiaContractM.setConCreateDate(gaiaContractZ.getConCreateDate());
                        gaiaContractM.setConCreateTime(gaiaContractZ.getConCreateTime());
                        gaiaContractM.setConLineAmt(DecimalUtils.multiply(gaiaContractM.getConQty(), gaiaContractM.getConPrice()));
                        gaiaContractMList.add(gaiaContractM);
                    }
                    gaiaContractMMapper.insertBatch(gaiaContractMList);
                }
                // 清空日志
                gaiaContractZLogMapper.deleteByConId(user.getClient(), gaiaContractZDetails.getConId());
                gaiaContractMLogMapper.deleteByConId(user.getClient(), gaiaContractZDetails.getConId());
            }
            // 插入日志表
            GaiaContractZLogKey gaiaContractZLogKey = new GaiaContractZLogKey();
            BeanUtils.copyProperties(gaiaContractZKey, gaiaContractZLogKey);
            GaiaContractZLog gaiaContractZLog = gaiaContractZLogMapper.selectLog(gaiaContractZLogKey);
            if (gaiaContractZLog == null) {
                gaiaContractZLog = new GaiaContractZLog();
                gaiaContractZLog.setConCompileIndex(-1);
            }
            GaiaContractZLog contractZLog = new GaiaContractZLog();
            BeanUtils.copyProperties(gaiaContractZDetails, contractZLog);
            if ("1".equals(conType)) {
                contractZLog.setConType("2");
            } else {
                contractZLog.setConType("0");
            }
            contractZLog.setConCreateBy(gaiaContractZ.getConCreateBy());
            contractZLog.setConCreateDate(gaiaContractZ.getConCreateDate());
            contractZLog.setConCreateTime(gaiaContractZ.getConCreateTime());
            contractZLog.setConCompileIndex(gaiaContractZLog.getConCompileIndex() + 1);

            contractZLog.setConCreateBy(user.getUserId());
            contractZLog.setConCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            contractZLog.setConCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaContractZLogMapper.insert(contractZLog);

            // 明细表插入
            if (CollectionUtils.isNotEmpty(gaiaContractZDetails.getDetails())) {
                List<GaiaContractMLog> gaiaContractMLogs = new ArrayList<>();
                int index = 0;
                for (PurchaseContractDetailVO vo : gaiaContractZDetails.getDetails()) {
                    GaiaContractMLog gaiaContractMLog = new GaiaContractMLog();
                    BeanUtils.copyProperties(vo, gaiaContractMLog);
                    gaiaContractMLog.setClient(user.getClient());
                    gaiaContractMLog.setConCompanyCode(gaiaContractZDetails.getConCompanyCode());
                    gaiaContractMLog.setConSupplierId(gaiaContractZDetails.getConSupplierId());
                    gaiaContractMLog.setConId(gaiaContractZDetails.getConId());
                    gaiaContractMLog.setConLineNo(String.valueOf(index++));
                    gaiaContractMLog.setConCreateBy(contractZLog.getConUpdateBy());
                    gaiaContractMLog.setConCreateDate(contractZLog.getConCreateBy());
                    gaiaContractMLog.setConCreateTime(contractZLog.getConCreateTime());
                    gaiaContractMLog.setConCreateBy(user.getUserId());
                    gaiaContractMLog.setConCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    gaiaContractMLog.setConCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    gaiaContractMLog.setConCompileIndex(gaiaContractZLog.getConCompileIndex() + 1);
                    gaiaContractMLog.setConLineAmt(DecimalUtils.multiply(gaiaContractMLog.getConQty(), gaiaContractMLog.getConPrice()));
                    gaiaContractMLogs.add(gaiaContractMLog);
                }
                gaiaContractMLogMapper.insertBatch(gaiaContractMLogs);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 合同列表
     *
     * @param pageSize
     * @param pageNum
     * @param conCompanyCode
     * @param conSupplierId
     * @param proSelfCode
     * @param conType
     * @param conCreateDateStart
     * @param conCreateDateEnd
     * @return
     */
    @Override
    public Result queryPurchaseContractList(Integer pageSize, Integer pageNum, String conCompanyCode, String conSupplierId, String proSelfCode, String conType,
                                            String conCreateDateStart, String conCreateDateEnd) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaContractZDetails> gaiaContractZDetailsList = gaiaContractZMapper.selectQueryContractList(user.getClient(), conCompanyCode, conSupplierId,
                null, null, proSelfCode, conType, conCreateDateStart, conCreateDateEnd, "2");
        PageInfo<GaiaContractZDetails> pageInfo = new PageInfo<>(gaiaContractZDetailsList);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 拒绝
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void refusePurchaseContract(List<GaiaContractZDetails> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        TokenUser user = feignService.getLoginInfo();
        List<String> conIdList = list.stream().map(GaiaContractZDetails::getConId).collect(Collectors.toList());
        List<GaiaContractZ> gaiaContractZList = gaiaContractZMapper.selectGaiaContractZByConIds(user.getClient(), conIdList);
        if (CollectionUtils.isEmpty(gaiaContractZList)) {
            throw new CustomResultException("审批合同不存在");
        }
        if (list.size() != gaiaContractZList.size()) {
            throw new CustomResultException("合同发生变更，请重新查询后再试");
        }
        // 审批拒绝
        for (GaiaContractZDetails gaiaContractZDetails : list) {
            GaiaContractZ gaiaContractZ = gaiaContractZList.stream().filter(t -> t.getConId().equals(gaiaContractZDetails.getConId())).findFirst().orElse(null);
            if (gaiaContractZ == null) {
                throw new CustomResultException("合同" + gaiaContractZDetails.getConId() + "发生变更，请重新查询后再试");
            }
            // 状态已变更
            if (!gaiaContractZ.getConType().equals(gaiaContractZDetails.getConType())) {
                throw new CustomResultException("合同" + gaiaContractZDetails.getConId() + "发生变更，请重新查询后再试");
            }
            // 新增拒绝
            if ("0".equals(gaiaContractZ.getConType())) {
                gaiaContractZ.setConType("3");
            }
            // 变更拒绝
            if ("2".equals(gaiaContractZ.getConType())) {
                gaiaContractZ.setConType("1");
            }
            // 修改状态
            GaiaContractZ gaiaContractZRecord = new GaiaContractZ();
            gaiaContractZRecord.setClient(user.getClient());
            gaiaContractZRecord.setConCompanyCode(gaiaContractZ.getConCompanyCode());
            gaiaContractZRecord.setConSupplierId(gaiaContractZ.getConSupplierId());
            gaiaContractZRecord.setConId(gaiaContractZ.getConId());
            gaiaContractZRecord.setConType(gaiaContractZ.getConType());
            gaiaContractZRecord.setConApproveBy(user.getUserId());
            gaiaContractZRecord.setConApproveDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaContractZRecord.setConApproveTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaContractZMapper.updateByPrimaryKeySelective(gaiaContractZRecord);
            GaiaContractZLog gaiaContractZLog = new GaiaContractZLog();
            BeanUtils.copyProperties(gaiaContractZRecord, gaiaContractZLog);
            gaiaContractZLog.setConCompileIndex(gaiaContractZDetails.getLogConCompileIndex());
            gaiaContractZLog.setConApproveBy(user.getUserId());
            gaiaContractZLog.setConApproveDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaContractZLog.setConApproveTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaContractZLogMapper.updateByPrimaryKeySelective(gaiaContractZLog);
        }
    }

    /**
     * 同意
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void approvePurchaseContract(List<GaiaContractZDetails> list) {
        TokenUser user = feignService.getLoginInfo();
        List<String> conIdList = list.stream().map(GaiaContractZDetails::getConId).collect(Collectors.toList());
        List<GaiaContractZ> gaiaContractZList = gaiaContractZMapper.selectGaiaContractZByConIds(user.getClient(), conIdList);
        if (CollectionUtils.isEmpty(gaiaContractZList)) {
            throw new CustomResultException("审批合同不存在");
        }
        if (list.size() != gaiaContractZList.size()) {
            throw new CustomResultException("合同发生变更，请重新查询后再试");
        }
        // 审批拒绝
        for (GaiaContractZDetails gaiaContractZDetails : list) {
            GaiaContractZ gaiaContractZ = gaiaContractZList.stream().filter(t -> t.getConId().equals(gaiaContractZDetails.getConId())).findFirst().orElse(null);
            if (gaiaContractZ == null) {
                throw new CustomResultException("合同" + gaiaContractZDetails.getConId() + "发生变更，请重新查询后再试");
            }
            // 状态已变更
            if (!gaiaContractZ.getConType().equals(gaiaContractZDetails.getConType())) {
                throw new CustomResultException("合同" + gaiaContractZDetails.getConId() + "发生变更，请重新查询后再试");
            }
            // 新增同意
            if ("0".equals(gaiaContractZ.getConType())) {
                // 状态
                gaiaContractZ.setConType("1");
                // 修改状态
                GaiaContractZ gaiaContractZRecord = new GaiaContractZ();
                gaiaContractZRecord.setClient(user.getClient());
                gaiaContractZRecord.setConCompanyCode(gaiaContractZ.getConCompanyCode());
                gaiaContractZRecord.setConSupplierId(gaiaContractZ.getConSupplierId());
                gaiaContractZRecord.setConId(gaiaContractZ.getConId());
                gaiaContractZRecord.setConType(gaiaContractZ.getConType());
                gaiaContractZRecord.setConApproveBy(user.getUserId());
                gaiaContractZRecord.setConApproveDate(DateUtils.getCurrentDateStrYYMMDD());
                gaiaContractZRecord.setConApproveTime(DateUtils.getCurrentTimeStrHHMMSS());
                gaiaContractZMapper.updateByPrimaryKeySelective(gaiaContractZRecord);
                GaiaContractZLog gaiaContractZLog = new GaiaContractZLog();
                BeanUtils.copyProperties(gaiaContractZRecord, gaiaContractZLog);
                gaiaContractZLog.setConCompileIndex(gaiaContractZDetails.getLogConCompileIndex());
                gaiaContractZLog.setConApproveBy(user.getUserId());
                gaiaContractZLog.setConApproveDate(DateUtils.getCurrentDateStrYYMMDD());
                gaiaContractZLog.setConApproveTime(DateUtils.getCurrentTimeStrHHMMSS());
                gaiaContractZLogMapper.updateByPrimaryKeySelective(gaiaContractZLog);
                continue;
            }
            // 变更同意
            if ("2".equals(gaiaContractZ.getConType())) {
                gaiaContractZ.setConType("1");
            }
            // 变更日志
            GaiaContractZLogKey gaiaContractZLogKey = new GaiaContractZLogKey();
            gaiaContractZLogKey.setClient(user.getClient());
            gaiaContractZLogKey.setConCompanyCode(gaiaContractZ.getConCompanyCode());
            gaiaContractZLogKey.setConSupplierId(gaiaContractZ.getConSupplierId());
            gaiaContractZLogKey.setConCompileIndex(gaiaContractZDetails.getLogConCompileIndex());
            gaiaContractZLogKey.setConId(gaiaContractZ.getConId());
            GaiaContractZLog gaiaContractZLog = gaiaContractZLogMapper.selectByPrimaryKey(gaiaContractZLogKey);
            // 合同主表
            BeanUtils.copyProperties(gaiaContractZLog, gaiaContractZ);
            gaiaContractZ.setConType("1");
            gaiaContractZ.setConApproveBy(user.getUserId());
            gaiaContractZ.setConApproveDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaContractZ.setConApproveTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaContractZMapper.updateByPrimaryKey(gaiaContractZ);
            // 合同明细表
            gaiaContractMMapper.invalidPurchaseContract(user.getClient(), gaiaContractZ.getConId());
            List<GaiaContractM> gaiaContractMList = new ArrayList<>();
            List<GaiaContractMLog> gaiaContractMLogList = gaiaContractMLogMapper.queryContractDetail(user.getClient(), gaiaContractZ.getConCompanyCode(), gaiaContractZ.getConSupplierId(),
                    gaiaContractZ.getConId(), gaiaContractZDetails.getLogConCompileIndex());
            if (CollectionUtils.isNotEmpty(gaiaContractMLogList)) {
                for (GaiaContractMLog gaiaContractMLog : gaiaContractMLogList) {
                    GaiaContractM gaiaContractM = new GaiaContractM();
                    BeanUtils.copyProperties(gaiaContractMLog, gaiaContractM);
                    gaiaContractMList.add(gaiaContractM);
                }
                gaiaContractMMapper.insertBatch(gaiaContractMList);
            }
            // 日志状态
            gaiaContractZLog.setConType("1");
            gaiaContractZLog.setConApproveBy(user.getUserId());
            gaiaContractZLog.setConApproveDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaContractZLog.setConApproveTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaContractZLogMapper.updateByPrimaryKeySelective(gaiaContractZLog);
        }
    }

    /**
     * 合同查询导出
     *
     * @param conCompanyCode
     * @param conSupplierId
     * @param proSelfCode
     * @param conType
     * @param conCreateDateStart
     * @param conCreateDateEnd
     * @return
     */
    @Override
    public Result queryPurchaseContractListExport(String conCompanyCode, String conSupplierId, String proSelfCode, String conType, String conCreateDateStart, String conCreateDateEnd) {
        try {
            TokenUser user = feignService.getLoginInfo();
            PageHelper.clearPage();
            List<GaiaContractZDetails> gaiaContractZDetailsList = gaiaContractZMapper.selectQueryContractList(user.getClient(), conCompanyCode, conSupplierId,
                    null, null, proSelfCode, conType, conCreateDateStart, conCreateDateEnd, "2");
            List<List<String>> dataList = new ArrayList<>();
            int index = 0;
            for (GaiaContractZDetails gaiaContractZDetails : gaiaContractZDetailsList) {
                index++;
                List<String> data = new ArrayList<>();
                // 序号
                data.add(String.valueOf(index));
                // 合同编号
                data.add(gaiaContractZDetails.getConId());
                // 单据日期
                data.add(gaiaContractZDetails.getConDate());
                // 供应商
                data.add(StringUtils.parse("{}-{}", gaiaContractZDetails.getConSupplierId(), gaiaContractZDetails.getSupName()));
                // 业务员
                data.add(gaiaContractZDetails.getConSalesmanName());
                // 有效期起
                data.add(gaiaContractZDetails.getConExpiryDateFrom());
                // 有效期至
                data.add(gaiaContractZDetails.getConExpiryDateTo());
                // 合同创建人
                data.add(StringUtils.parse("{}-{}", gaiaContractZDetails.getConCreateBy(), gaiaContractZDetails.getConCreateByName()));
                // 合同备注
                data.add(gaiaContractZDetails.getConRemark());
                // 合同状态
                if (CommonEnum.ConType.ConType0.getCode().equals(gaiaContractZDetails.getConType())) {
                    data.add(CommonEnum.ConType.ConType0.getName());
                } else if (CommonEnum.ConType.ConType1.getCode().equals(gaiaContractZDetails.getConType())) {
                    data.add(CommonEnum.ConType.ConType1.getName());
                } else if (CommonEnum.ConType.ConType2.getCode().equals(gaiaContractZDetails.getConType())) {
                    data.add(CommonEnum.ConType.ConType2.getName());
                } else if (CommonEnum.ConType.ConType3.getCode().equals(gaiaContractZDetails.getConType())) {
                    data.add(CommonEnum.ConType.ConType3.getName());
                } else {
                    data.add("");
                }
                dataList.add(data);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("采购合同");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (
                IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 审批明细
     *
     * @param conId
     * @param conCompileIndex
     * @return
     */
    @Override
    public Result queryPurchaseApproveContractDetails(String conId, String conCompanyCode, String conSupplierId, Integer conCompileIndex) {
        TokenUser user = feignService.getLoginInfo();
        List<PurchaseContractDetailVO> purchaseContractDetailVOList = gaiaContractMLogMapper.queryPurchaseApproveContractDetails(user.getClient(), conCompanyCode,
                conSupplierId, conId, conCompileIndex);
        return ResultUtil.success(purchaseContractDetailVOList);
    }

    /**
     * 合同审批列表
     *
     * @param pageSize
     * @param pageNum
     * @param conCompanyCode
     * @param conSupplierId
     * @param conSalesmanCode
     * @return
     */
    @Override
    public Result queryPurchaseApproveContractList(Integer pageSize, Integer pageNum, String conCompanyCode, String conSupplierId, String conSalesmanCode) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaContractZDetails> gaiaContractZDetailsList = gaiaContractZMapper.queryPurchaseApproveContractList(user.getClient(), conCompanyCode,
                conSupplierId, conSalesmanCode);
        PageInfo<GaiaContractZDetails> pageInfo = new PageInfo<>(gaiaContractZDetailsList);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 导出表头
     */
    private String[] headList = {
            "序号",
            "合同编号",
            "单据日期",
            "供应商",
            "业务员",
            "有效期起",
            "有效期至",
            "合同创建人",
            "合同备注",
            "合同状态"
    };

}
