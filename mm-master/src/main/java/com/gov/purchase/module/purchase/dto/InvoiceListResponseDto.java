package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "发票列表查询返回参数")
public class InvoiceListResponseDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 加盟商名
     */
    private String clientName;

    /**
     * 地点代码
     */
    private String poCompanyCode;

    /**
     * 地点名
     */
    private String siteName;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 供应商名
     */
    private String supName;

    /**
     * 发票号码
     */
    private String invoiceNum;

    /**
     * 发票金额
     */
    private BigDecimal poInvoiceAmt;

    /**
     * 应付金额
     */
    private BigDecimal handleAmt;

    /**
     * 已付金额
     */
    private BigDecimal paidAmt;

    /**
     * 已预付金额
     */
    private BigDecimal prepaidAmt;

    /**
     * 未付款金额
     */
    private BigDecimal unpaidAmt;

    /**
     * 本次付款金额
     */
    private BigDecimal paymentAmt;
}