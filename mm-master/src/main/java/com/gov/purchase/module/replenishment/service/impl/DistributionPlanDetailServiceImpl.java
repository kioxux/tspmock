package com.gov.purchase.module.replenishment.service.impl;

import com.gov.purchase.entity.GaiaNewDistributionPlanDetail;
import com.gov.purchase.exception.ServiceException;
import com.gov.purchase.mapper.GaiaNewDistributionPlanDetailMapper;
import com.gov.purchase.module.replenishment.service.DistributionPlanDetailService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @desc:
 * @author: ryan
 * @createTime: 2021/5/31 14:54
 */
@Service("distributionPlanDetailService")
public class DistributionPlanDetailServiceImpl implements DistributionPlanDetailService {
    @Resource
    private GaiaNewDistributionPlanDetailMapper gaiaNewDistributionPlanDetailMapper;

    @Transactional
    @Override
    public GaiaNewDistributionPlanDetail update(GaiaNewDistributionPlanDetail distributionPlanDetail) {
        int num;
        try {
            distributionPlanDetail.setUpdateTime(new Date());
            num = gaiaNewDistributionPlanDetailMapper.update(distributionPlanDetail);
        } catch (Exception e) {
            throw new ServiceException("铺货计划详情更新异常", e);
        }
        if (num == 0) {
            throw new ServiceException("铺货计划详情更新失败");
        }
        return distributionPlanDetail;
    }

    @Override
    public List<GaiaNewDistributionPlanDetail> findList(GaiaNewDistributionPlanDetail cond) {
        return gaiaNewDistributionPlanDetailMapper.findList(cond);
    }

}
