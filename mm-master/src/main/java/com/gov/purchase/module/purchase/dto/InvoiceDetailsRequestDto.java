package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.util.Map;

@Data
@ApiModel(value = "发票明细查询传入参数")
public class InvoiceDetailsRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 供应商
     */
    @ApiModelProperty(value = "供应商", name = "poSupplierId", required = true)
    @NotBlank(message = "供应商不能为空")
    private String poSupplierId;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "poCompanyCode", required = true)
    @NotBlank(message = "地点不能为空")
    private String poCompanyCode;

    /**
     * 发票号码
     */
    @ApiModelProperty(value = "发票号码", name = "invoiceNum")
    private String[] invoiceNum;

    /**
     * 本次付款金额
     */
    @ApiModelProperty(value = "付款金额", name = "paymentAmount")
    private Map<String, BigDecimal> paymentAmount;
}