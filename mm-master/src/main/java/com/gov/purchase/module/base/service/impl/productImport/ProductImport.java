package com.gov.purchase.module.base.service.impl.productImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaDcDataKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.DictionaryMapper;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryParams;
import com.gov.purchase.module.base.dto.excel.Product;
import com.gov.purchase.module.base.service.impl.DictionaryServiceImpl;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.20
 */
public abstract class ProductImport extends ImportData {

    @Resource
    DictionaryMapper dictionaryMapper;

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    DictionaryServiceImpl dictionaryService;

    /**
     * 商品新增/编辑业务验证共通部分
     *
     * @param dataMap
     * @return errorList
     */
    protected Result errorListProduct(LinkedHashMap<Integer, Product> dataMap) {
        List<String> errorList = new ArrayList<>();
        // 数据为空
        if (dataMap == null || dataMap.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0104);
        }

        // 计量单位
        List<Dictionary> nuitList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.UNIT.getTable(),
                CommonEnum.DictionaryData.UNIT.getField1(),
                CommonEnum.DictionaryData.UNIT.getCondition(), CommonEnum.DictionaryData.UNIT.getSortFields());
        // 剂型
        List<Dictionary> formList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.PRODUCT_FORM.getTable(),
                CommonEnum.DictionaryData.PRODUCT_FORM.getField1(),
                CommonEnum.DictionaryData.PRODUCT_FORM.getCondition(), CommonEnum.DictionaryData.PRODUCT_FORM.getSortFields());
        // 商品分类
        List<Dictionary> classList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.PRODUCT_CLASS_ALL.getTable(),
                CommonEnum.DictionaryData.PRODUCT_CLASS_ALL.getField1(),
                CommonEnum.DictionaryData.PRODUCT_CLASS_ALL.getCondition(), CommonEnum.DictionaryData.PRODUCT_CLASS_ALL.getSortFields());
        // 成分分类
        List<Dictionary> compclassList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.PRODUCT_COMPONENT.getTable(),
                CommonEnum.DictionaryData.PRODUCT_COMPONENT.getField1(),
                CommonEnum.DictionaryData.PRODUCT_COMPONENT.getCondition(), CommonEnum.DictionaryData.PRODUCT_COMPONENT.getSortFields());
        // 生产企业
        List<Dictionary> factoryCodeList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.PRODUCT_FACTORY.getTable(),
                CommonEnum.DictionaryData.PRODUCT_FACTORY.getField1(),
                CommonEnum.DictionaryData.PRODUCT_FACTORY.getCondition(), CommonEnum.DictionaryData.PRODUCT_FACTORY.getSortFields());
        // 进项税率
        List<Dictionary> inputTaxList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.TAX_CODE1.getTable(),
                CommonEnum.DictionaryData.TAX_CODE1.getField1(),
                CommonEnum.DictionaryData.TAX_CODE1.getCondition(), CommonEnum.DictionaryData.TAX_CODE1.getSortFields());
        // 销项税率
        List<Dictionary> outputTaxList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.TAX_CODE2.getTable(),
                CommonEnum.DictionaryData.TAX_CODE2.getField1(),
                CommonEnum.DictionaryData.TAX_CODE2.getCondition(), CommonEnum.DictionaryData.TAX_CODE2.getSortFields());

        // 加盟商 商品自编码重复验证
        Map<String, List<String>> clientProCodeMap = new HashMap<>();

        // 遍历验证
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Product product = dataMap.get(key);

            // 计量单位
            if (StringUtils.isNotBlank(product.getProUnit())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(nuitList, product.getProUnit());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "计量单位"));
                }
            }
            // 剂型
            if (StringUtils.isNotBlank(product.getProForm())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(formList, product.getProForm());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "剂型"));
                }
            }
            // 细分剂型
            if (StringUtils.isNotBlank(product.getProForm()) && StringUtils.isNotBlank(product.getProPartform())) {
                // 参数
                DictionaryParams dictionaryParams = new DictionaryParams();
                // key
                dictionaryParams.setKey(CommonEnum.DictionaryData.PRO_PARTFORM.getCode());
                // 参数
                Map<String, String> map = new HashMap<>();
                map.put("PRO_FORM", product.getProForm());
                dictionaryParams.setExactParams(map);
                Result result = dictionaryService.getDictionary(dictionaryParams);
                if (result.getData() == null || CollectionUtils.isEmpty((Collection<?>) result.getData())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "细分剂型"));
                } else {
                    Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue((List<Dictionary>) result.getData(), product.getProPartform());
                    if (dictionary == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "细分剂型"));
                    }
                }
            }
            // 批准文号批准日期 批准文号失效日期 比较
            if (super.isNotBlank(product.getProRegisterDate()) && super.isNotBlank(product.getProRegisterExdate())) {
                // 批准文号批准日期>=批准文号失效日期
                if (product.getProRegisterDate().compareTo(product.getProRegisterExdate()) >= 0) {
                    errorList.add(MessageFormat.format(ResultEnum.E0108.getMsg(), key + 1));
                }
            }
            // 商品分类 商品分类描述
            if (StringUtils.isNotBlank(product.getProClass())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(classList, product.getProClass());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品分类"));
                } else {
                    // 商品分类描述赋值
                    product.setProClassName(dictionary.getLabel());
                }
            }
            // 成分分类 成分分类描述
            if (StringUtils.isNotBlank(product.getProCompclass())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(compclassList, product.getProCompclass());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "成分分类"));
                } else {
                    // 商品分类描述赋值
                    product.setProCompclassName(dictionary.getLabel());
                }
            }
            // 企业代码  生产企业
            if (super.isNotBlank(product.getProFactoryCode())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(factoryCodeList, product.getProFactoryCode());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "企业代码"));
                } else {
                    product.setProFactoryName(dictionary.getLabel());
                }
            }
            // 中药生产企业
            if (super.isNotBlank(product.getProTcmFactoryCode())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(factoryCodeList, product.getProTcmFactoryCode());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "中药生产企业代码"));
                }
            }
            // 进项税率
            if (StringUtils.isNotBlank(product.getProInputTax())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(inputTaxList, product.getProInputTax());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "进项税率"));
                }
            }
            // 销项税率
            if (StringUtils.isNotBlank(product.getProOutputTax())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(outputTaxList, product.getProOutputTax());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "销项税率"));
                }
            }

            // 拆零单位
            if (super.isNotBlank(product.getProPartUint())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(nuitList, product.getProPartUint());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "拆零单位"));
                }
            }
            // 采购单位
            if (super.isNotBlank(product.getProPurchaseUnit())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(nuitList, product.getProPurchaseUnit());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "采购单位"));
                }
            }
            // 销售单位
            if (super.isNotBlank(product.getProSaleUnit())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(nuitList, product.getProSaleUnit());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "销售单位"));
                }
            }

            // 主数据验证
            baseCheck(product, errorList, key);

            // 业务表数据存在判断
            if (StringUtils.isBlank(product.getClient())) {
                continue;
            }

            // 加盟商商品自编码行号
            List<String> clientProCodeList = clientProCodeMap.get(product.getClient() + "_" + product.getProSelfCode());
            if (clientProCodeList == null) {
                clientProCodeList = new ArrayList<>();
            }
            clientProCodeList.add(String.valueOf((key + 1)));
            clientProCodeMap.put(product.getClient() + "_" + product.getProSelfCode(), clientProCodeList);

            // 加盟商 地点 验证
            // 门店数据
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(product.getClient());
            gaiaStoreDataKey.setStoCode(product.getProSite());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            // DC数据
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(product.getClient());
            gaiaDcDataKey.setDcCode(product.getProSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (gaiaStoreData == null && gaiaDcData == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0117.getMsg(), key + 1));
            }

            // 业务数据验证
            businessCheck(product, errorList, key);
        }
        // 加盟商商品自编码重复验证
        for (String clientProCode : clientProCodeMap.keySet()) {
            List<String> lineList = clientProCodeMap.get(clientProCode);
            if (lineList.size() > 1) {
                errorList.add(MessageFormat.format(ResultEnum.E0120.getMsg(), String.join(",", lineList)));
            }
        }
        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }

    /**
     * 主数据表验证
     *
     * @param product
     * @param errorList
     * @param key
     */
    protected abstract void baseCheck(Product product, List<String> errorList, int key);

    /**
     * 业务数据验证
     *
     * @param product
     * @param errorList
     * @param key
     */
    protected abstract void businessCheck(Product product, List<String> errorList, int key);
}
