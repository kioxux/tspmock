package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;


@Data
@ApiModel(value = "采购订单明细返回参数")
public class PoItemResponseDto {

    /**
     * 加盟商ID
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 采购主体（公司）
     */
    private String poCompanyCode;

    /**
     * 采购主体名称（公司）
     */
    private String poCompanyName;

    /**
     * 创建人
     */
    private String poCreateBy;

    /**
     * 创建人
     */
    private String poCreateByName;

    /**
     * 采购凭证号
     */
    private String poId;

    /**
     * 采购订单类型
     */
    private String poType;

    /**
     * 订单日期
     */
    private String poDate;

    /**
     * 供应商自编码
     */
    private String poSupplierId;

    /**
     * 供应商名称
     */
    private String poSupplierName;

    /**
     * 付款条件
     */
    private String poPaymentId;

    /**
     * 付款条件名
     */
    private String poPaymentName;

    /**
     * 审批状态
     */
    private String poApproveStatus;

    /**
     * 订单总金额
     */
    private BigDecimal poTotalAmount;

    /**
     * 抬头备注
     */
    private String poHeadRemark;

    /**
     * 物流模式
     */
    private String poDeliveryType;

    /**
     * 物流模式门店
     */
    private String poDeliveryTypeStore;

    /**
     * 审批流程编号
     */
    private  String poFlowNo;

    /**
     * 采购订单明细LIST
     */
    private List<PoItemListDto> poItemListDto;

    /**
     * 送货前置期(整数>=0)
     */
    private String poLeadTime;
    /**
     * 配送方式(字符串)
     */
    private String poDeliveryMode;
    /**
     * 承运单位(字符串)
     */
    private String poCarrier;
    /**
     * 运输时间(字符串)
     */
    private String poDeliveryTimes;
    /**
     * 供应商业务员
     */
    private String poSupplierSalesman;
    /**
     * 供应商业务员名称
     */
    private String poSupplierSalesmanName;

    private String poOwner;

    private String poSalesmanName;
}