package com.gov.purchase.module.store.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.module.qa.dto.QueryProductBusinessResponseDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import com.gov.purchase.module.store.dto.store.StoreListResponseDto;

import java.util.List;

public interface StoreGroupProductService {

    /**
     * 根据参数来查询数据
     *
     * @return
     */
    List<QueryProductBusinessResponseDto> queryList(String param);


    /**
     * 根据商品codeList来查询数据
     *
     * @return
     */
    List<QueryProductBusinessResponseDto> queryGroupList(List<String> param);


    /**
     * 保存数据
     *
     * @param gaiaPriceAdjustList
     * @return
     */
    Result saveList(List<GaiaPriceAdjustDto> gaiaPriceAdjustList);


    /**
     * 获取门店列表
     *
     * @return
     */
    List<GaiaStoreData> queryStoreList();

    /**
     * 获取门店组列表
     *
     * @return
     */
    List<StoreListResponseDto> queryStoreGroupList();

    /**
     * 获取门店组导出列表
     *
     * @return
     */
    Result queryStoreGroupExportList();

    /**
     * 删除门店组信息
     *
     * @return
     */
    Result deleteStoreGroupList(List<StoreListResponseDto> list);


    /**
     * 保存门店组信息
     *
     * @return
     */
    Result saveStoreGroupList(List<StoreListResponseDto> list);


}
