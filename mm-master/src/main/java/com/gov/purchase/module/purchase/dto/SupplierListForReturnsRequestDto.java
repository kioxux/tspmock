package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "供应商列表传入参数")
public class SupplierListForReturnsRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "proSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String proSite;
}