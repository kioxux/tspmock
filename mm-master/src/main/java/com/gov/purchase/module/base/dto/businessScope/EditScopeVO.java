package com.gov.purchase.module.base.dto.businessScope;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@EqualsAndHashCode
public class EditScopeVO {
    @NotBlank(message = "机构编码不能为空")
    private String jyfwOrgid;
    @NotBlank(message = "机构名称不能为空")
    private String jyfwOrgname;
    @NotNull(message = "类型不能为空（1：供应商 2：客户 3：其他（连锁、门店、批发））")
    private Integer jyfwType;
    /**
     * 地点
     */
    private String jyfwSite;
    @NotEmpty(message = "经营范围列表不能为空")
    private List<Scope> scopeList;

    @Data
    @EqualsAndHashCode
    public static class Scope{
        @NotBlank(message = "经营范围编码不能为空")
        private String jyfwId;
        @NotBlank(message = "经营范围描述不能为空")
        private String jyfwName;
        /**
         * 是否选中（0:不选 1:选中）
         */
        private Integer checked;
    }

}
