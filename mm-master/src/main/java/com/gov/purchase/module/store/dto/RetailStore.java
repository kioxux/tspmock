package com.gov.purchase.module.store.dto;

import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.02.05
 */
@Data
public class RetailStore extends GaiaStoreData {

    /**
     * 已选
     */
    private int selectd;

}
