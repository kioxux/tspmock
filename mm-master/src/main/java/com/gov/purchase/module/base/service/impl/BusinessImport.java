package com.gov.purchase.module.base.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.businessImport.*;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.EnumUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.annotation.Resource;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Slf4j
public abstract class BusinessImport {

    @Resource
    CosUtils cosUtils;

    /**
     * 文件名
     */
    protected String fileName;

    /**
     * 列名
     */
    private List<String> nameList;

    /**
     * 操作对旬类
     */
    private Class<?> cs;

    /**
     * 导入类型
     */
    protected CommonEnum.BusinessImportTypet importType;

    /**
     * 初始数据加载
     */
    private void initClass() {
        // 导入业务
        CommonEnum.BusinessImportTypet businessImportTypet = CommonEnum.BusinessImportTypet.getBusinessImportTypet(this.importType.getCode());
        if (businessImportTypet == null) {
            log.info("没有查询到该导入业务");
            throw new CustomResultException(ResultEnum.E0105);
        }
        this.cs = businessImportTypet.getCs();
        this.nameList = getNames();
    }

    /**
     * 导入
     *
     * @param path 路径
     * @param type 类型
     * @return
     */
    public Result businessImport(String path, String type, Map<String, Object> field) {
        // 业务不存在
        if (!EnumUtils.contains(type, CommonEnum.BusinessImportTypet.class)) {
            return ResultUtil.error(ResultEnum.E0102);
        }
        // 导入类型
        this.importType = CommonEnum.BusinessImportTypet.getBusinessImportTypet(type);
        // 数据初始化
        initClass();
        // excel文件
        Workbook workbook = this.getWorkbook(path);
        // 模板格式验证
        Result result = this.templateCheck(workbook);
        if (ResultUtil.hasError(result)) {
            return result;
        }
        // 获取数据
        LinkedHashMap<Integer, Object> map = dataList(workbook);
        // 数据为空
        if (map == null || map.isEmpty()) {
            log.info("导入数据为空");
            throw new CustomResultException(ResultEnum.E0104);
        }
        // 文件基础数据验证
        result = dataFormatCheck(map);
        if (ResultUtil.hasError(result)) {
            return result;
        }
        // 业务验证
        result = businessValidate(map, field);
        return result;
    }

    /**
     * 数据业务验证
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    protected abstract <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field);

    /**
     * 数据格式验证
     *
     * @param dataMap 数据
     */
    private Result dataFormatCheck(LinkedHashMap<Integer, Object> dataMap) {
        List<String> errorList = new ArrayList<>();
        LinkedHashMap<Integer, Object> map = (LinkedHashMap<Integer, Object>) dataMap;
        // 数据格式验证
        for (Integer key : map.keySet()) {
            Object entiey = map.get(key);
            // 数据验证
            checkData(key + 1, entiey, errorList);
        }
        // 验证报错
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success();
    }

    /**
     * 数据验证
     *
     * @param rowIndex
     * @param entity
     */
    private void checkData(int rowIndex, Object entity, List<String> errorList) {
        Field[] declaredFields = this.cs.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(ExcelValidate.class)) {
                ExcelValidate annotation = field.getAnnotation(ExcelValidate.class);
                // 属性
                String attr = field.getName();
                // 名称
                String name = annotation.name();
                // 必须
                boolean addRequired = annotation.addRequired();
                // 类型
                ExcelValidate.DataType dataType = annotation.type();
                // 最大长度
                int maxLength = annotation.maxLength();
                // 依赖的非空项目
                String[] relyRequired = annotation.relyRequired();
                // 值
                Object object = getFieldValueByName(attr, entity);

                // 导入直接使用新增验证
                if (addRequired) {
                    if (object == null || StringUtils.isBlank(object.toString())) {
                        errorList.add("第" + rowIndex + "行：" + name + "不能为空");
                        continue;
                    }
                }

                // 字符串
                if (dataType.equals(ExcelValidate.DataType.STRING)) {
                    // 超长
                    if (maxLength != 0 && object != null && StringUtils.length(object.toString()) > maxLength) {
                        errorList.add("第" + rowIndex + "行：" + name + "长度不能超过" + maxLength + "位");
                        continue;
                    }
                }
                // 整数
                if (dataType.equals(ExcelValidate.DataType.INTEGER) || dataType.equals(ExcelValidate.DataType.DECIMAL)) {
                    // 最大值
                    double max = annotation.max();
                    // 最小值
                    double min = annotation.min();
                    // 格式验证 比较大小
                    if (object != null && StringUtils.isNotBlank(object.toString())) {
                        double value = 0;
                        // 数据格式验证
                        if (dataType.equals(ExcelValidate.DataType.INTEGER)) {
                            try {
                                String reg = "^[0-9]+(.[0-9]+)?$";
                                if (!object.toString().matches(reg)) {
                                    errorList.add("第" + rowIndex + "行：" + name + "为整数格式");
                                    continue;
                                }
                                if (!object.toString().matches("^\\d+$$")) {
                                    errorList.add("第" + rowIndex + "行：" + name + "为整数格式");
                                    continue;
                                }
                                value = NumberUtils.toLong(object.toString());
                            } catch (Exception e) {
                                errorList.add("第" + rowIndex + "行：" + name + "为整数格式");
                                continue;
                            }
                        }
                        if (dataType.equals(ExcelValidate.DataType.DECIMAL)) {
                            try {
                                String reg = "^[0-9]+(.[0-9]+)?$";
                                if (!object.toString().matches(reg)) {
                                    errorList.add("第" + rowIndex + "行：" + name + "为数字格式");
                                    continue;
                                }
                                value = Double.parseDouble(object.toString());
                            } catch (Exception e) {
                                errorList.add("第" + rowIndex + "行：" + name + "为数字格式");
                                continue;
                            }
                        }
                        DecimalFormat decimalFormat = new DecimalFormat("###################.###########");
                        // 比较大小
                        if (value < min) {
                            errorList.add("第" + rowIndex + "行：" + name + "必须大于等于" + decimalFormat.format(min));
                            continue;
                        }
                        if (value > max) {
                            errorList.add("第" + rowIndex + "行：" + name + "必须小于等于" + decimalFormat.format(max));
                            continue;
                        }
                        setFieldValueByFieldName(attr, entity, decimalFormat.format(value));
                    }
                }
                // 日期
                if (dataType.equals(ExcelValidate.DataType.DATE)) {
                    // 格式化
                    String dateFormat = annotation.dateFormat();
                    // 格式验证
                    if (object != null && StringUtils.isNotBlank(object.toString())) {
                        String value = object.toString().replaceAll("/", "").replaceAll("-", "");
                        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
                        try {
                            // 设置lenient为false. 否则SimpleDateFormat会比较宽松地验证日期，比如2007/02/29会被接受，并转换成2007/03/01
                            format.setLenient(false);
                            format.parse(value);
                        } catch (Exception e) {
                            errorList.add("第" + rowIndex + "行：" + name + "应为日期格式");
                            continue;
                        }
                    }
                }
                // 关联非空判断
                if (relyRequired != null && relyRequired.length > 0) {
                    // 遍历项目非空判断
                    for (String relyItem : relyRequired) {
                        if (StringUtils.isNotBlank(relyItem)) {
                            // 关联属性值
                            Object relyObject = getFieldValueByName(relyItem, entity);
                            // 关联属性有值
                            if (relyObject != null && StringUtils.isNotBlank(relyObject.toString())) {
                                // 当前项目为空
                                if (object == null || StringUtils.isBlank(object.toString())) {
                                    errorList.add("第" + rowIndex + "行：" + name + "不能为空");
                                    continue;
                                }
                            }
                        }
                    }
                }
                // 存在数据
                if (object != null && StringUtils.isNotBlank(object.toString())) {
                    // 静态字典数据验证
                    CommonEnum.DictionaryStaticData dictionaryStaticData = annotation.dictionaryStaticData();
                    // 静态字典比较字段
                    ExcelValidate.DictionaryStaticType dictionaryStaticType = annotation.dictionaryStaticType();
                    if (dictionaryStaticData != null && dictionaryStaticType != null && !dictionaryStaticData.equals(CommonEnum.DictionaryStaticData.DEFAULT)) {
                        Dictionary dictionary = null;
                        if (dictionaryStaticType.equals(ExcelValidate.DictionaryStaticType.VALUE)) {
                            dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, object.toString());
                        }
                        if (dictionaryStaticType.equals(ExcelValidate.DictionaryStaticType.LABEL)) {
                            dictionary = CommonEnum.DictionaryStaticData.getDictionaryByLabel(dictionaryStaticData, object.toString());
                            // 字典数据存在
                            if (dictionary != null) {
                                // 值字段
                                String dictionaryStaticField = annotation.dictionaryStaticField();
                                if (StringUtils.isNotBlank(dictionaryStaticField)) {
                                    setFieldValueByFieldName(dictionaryStaticField, entity, dictionary.getValue());
                                }
                            }
                        }
                        if (dictionary == null) {
                            errorList.add("第" + rowIndex + "行：请填写正确的" + name);
                            continue;
                        }
                    }
                }
            }
        }
    }

    /**
     * 文件转为数据
     *
     * @return
     */
    private <T> LinkedHashMap<Integer, T> dataList(Workbook workbook) {
        Sheet sheet = this.getSheet(workbook);
        LinkedHashMap<Integer, Object> map = new LinkedHashMap<Integer, Object>();
        // 遍历excel表格
        for (int i = this.importType.getLineNo() == null ? 2 : this.importType.getLineNo(); i < sheet.getLastRowNum() + 1; i++) {
            // 行
            Row row = sheet.getRow(i);
            // 当前行为空，继续下一行
            if (row == null) {
                continue;
            }
            Object entity = getObject(row);
            // Excel取出数据
            if (entity != null) {
                map.put(i, entity);
            }
        }
        return (LinkedHashMap<Integer, T>) map;
    }

    /**
     * 数据返回
     *
     * @param row
     * @param <T>
     * @return
     */
    private <T> T getObject(Row row) {
        Object entiey = null;
        try {
            entiey = this.cs.newInstance();
        } catch (Exception e) {
            log.info("行转数据报错");
            throw new CustomResultException(ResultEnum.E0123);
        }
        // 每列均无数据
        boolean flg = false;
        for (int i = 0; i < this.nameList.size(); i++) {
            Cell cell = row.getCell(i);
            String value = getCellValue(cell);
            if (StringUtils.isNotBlank(value)) {
                flg = true;
                break;
            }
        }
        if (!flg) {
            return null;
        }
        // 取值
        for (int i = 0; i < this.nameList.size(); i++) {
            Cell cell = row.getCell(i);
            String value = getCellValue(cell);
            if (StringUtils.isNotBlank(value)) {
                Field[] declaredFields = this.cs.getDeclaredFields();
                for (Field field : declaredFields) {
                    if (field.isAnnotationPresent(ExcelValidate.class)) {
                        ExcelValidate annotation = field.getAnnotation(ExcelValidate.class);
                        // 属性
                        String attr = field.getName();
                        // 列
                        int index = annotation.index();
                        if (index == i) {
                            this.setFieldValueByFieldName(attr, entiey, value);
                            break;
                        }
                    }
                }
            }
        }
        return (T) entiey;
    }

    /**
     * 根据属性名设置属性值
     *
     * @param fieldName
     * @param object
     * @return
     */
    private void setFieldValueByFieldName(String fieldName, Object object, String value) {
        try {
            // 获取obj类的字节文件对象
            Class c = object.getClass();
            // 获取该类的成员变量
            Field f = c.getDeclaredField(fieldName);
            // 取消语言访问检查
            f.setAccessible(true);
            // 给变量赋值
            f.set(object, value);
        } catch (Exception e) {
            log.info("赋值异常");
            throw new CustomResultException(ResultEnum.UNKNOWN_ERROR);
        }
    }

    /**
     * 对象属性值
     *
     * @param fieldName 属性名
     * @param o         对象
     * @return 返回值
     */
    private Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[]{});
            Object value = method.invoke(o, new Object[]{});
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 单元格取值
     *
     * @param cell
     * @return
     */
    private String getCellValue(Cell cell) {
        if (cell == null) {
            return null;
        }
        try {
            if ("yyyy/mm;@".equals(cell.getCellStyle().getDataFormatString()) || "m/d/yy".equals(cell.getCellStyle().getDataFormatString())
                    || "yy/m/d".equals(cell.getCellStyle().getDataFormatString()) || "mm/dd/yy".equals(cell.getCellStyle().getDataFormatString())
                    || "dd-mmm-yy".equals(cell.getCellStyle().getDataFormatString()) || "yyyy/m/d".equals(cell.getCellStyle().getDataFormatString())) {
                return new SimpleDateFormat("yyyyMMdd").format(cell.getDateCellValue());
            }
        } catch (Exception e) {
            return cell.toString().trim();
        }
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            cell.setCellType(Cell.CELL_TYPE_STRING);
            return NumberUtils.toScaledBigDecimal(cell.toString(), 4, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString();
        }
        return cell.toString().trim();
    }

    /**
     * 根据url取excel
     *
     * @param path
     * @return
     * @throws Exception
     */
    private Workbook getWorkbook(String path) {
        try {
            // 文件全路径
            String url = cosUtils.urlAuth(path);
            String type = url.substring(url.lastIndexOf(".") + 1, url.indexOf("?"));
            Workbook wb;
            //根据文件后缀（xls/xlsx）进行判断
            InputStream input = new URL(url).openStream();
            if ("xls".equals(type)) {
                //文件流对象
                wb = new HSSFWorkbook(input);
            } else if ("xlsx".equals(type)) {
                wb = new XSSFWorkbook(input);
            } else {
                log.info("非excel文件");
                throw new CustomResultException(ResultEnum.E0103);
            }
            this.fileName = url.substring(url.lastIndexOf("/") + 1);
            return wb;
        } catch (Exception e) {
            log.info("文件转excel报错");
            throw new CustomResultException(ResultEnum.E0103);
        }
    }

    /**
     * 模板验证
     *
     * @param workbook
     * @return
     */
    private Result templateCheck(Workbook workbook) {
        Sheet sheet = this.getSheet(workbook);
        // 导入数据标题
        Row row = sheet.getRow(0);
        if (row == null) {
            log.info("没有标题行");
            throw new CustomResultException(ResultEnum.E0104);
        }
        int cellCnt = row.getPhysicalNumberOfCells();
        // 模板不正确
        if (this.nameList.size() != cellCnt) {
            log.info("模板不正确");
            throw new CustomResultException(ResultEnum.E0105);
        }
        // 模板比较
        for (int i = 0; i < row.getPhysicalNumberOfCells(); i++) {
            Cell cell = row.getCell(i);
            if (cell == null) {
                log.info("标题列为空");
                throw new CustomResultException(ResultEnum.E0105);
            }
            String name = row.getCell(i).toString();
            if (!this.nameList.contains(name)) {
                log.info("标题不正确");
                throw new CustomResultException(ResultEnum.E0105);
            }
        }
        return ResultUtil.success();
    }

    /**
     * excel 工作表
     *
     * @param workbook
     * @return
     */
    private Sheet getSheet(Workbook workbook) {
        // 对象为空
        if (workbook == null) {
            log.info("文件对象空");
            throw new CustomResultException(ResultEnum.E0105);
        }
        // 获取第一个张表
        Sheet sheet = workbook.getSheetAt(0);
        // 对象为空
        if (sheet == null) {
            log.info("sheet对象空");
            throw new CustomResultException(ResultEnum.E0104);
        }
        // 无行数据
        if (sheet.getLastRowNum() <= 0) {
            log.info("行对象空");
            throw new CustomResultException(ResultEnum.E0104);
        }
        return sheet;
    }

    /**
     * 导入对象列名
     *
     * @return
     */
    private List<String> getNames() {
        List<String> list = new ArrayList<>();
        Field[] declaredFields = this.cs.getDeclaredFields();
        for (Field field : declaredFields) {
            if (field.isAnnotationPresent(ExcelValidate.class)) {
                ExcelValidate annotation = field.getAnnotation(ExcelValidate.class);
                list.add(annotation.name());
            }
        }
        return list;
    }
}

