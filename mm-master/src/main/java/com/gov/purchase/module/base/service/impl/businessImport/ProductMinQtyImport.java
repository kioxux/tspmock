package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductMinDisplay;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaProductMinDisplayMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.store.dto.store.StoreMinQtyImportDto;
import com.gov.purchase.utils.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class ProductMinQtyImport extends BusinessImport {

    /**
     * 加盟商key
     */
    private final static String CLIENT = "client";

    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    private GaiaProductMinDisplayMapper gaiaProductMinDisplayMapper;

    @Resource
    private FeignService feignService;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();

        List<GaiaProductMinDisplay> addList = new ArrayList<>();

        List<GaiaProductMinDisplay> updateList = new ArrayList<>();

        // 批量导入
        try {
            // 验证excel  数据是否正确
            for (Integer key : map.keySet()) {
                StoreMinQtyImportDto storeMinQtyImportDto = (StoreMinQtyImportDto) map.get(key);


                // 门店主键对象
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                // 加盟商
                gaiaStoreDataKey.setClient(user.getClient());
                // 门店编码
                gaiaStoreDataKey.setStoCode(storeMinQtyImportDto.getGpmdBrId());
                // 门店获取
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                if (gaiaStoreData == null) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "门店"));
                }

                // 验证商品存在性
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(user.getClient(), storeMinQtyImportDto.getGpmdBrId(), storeMinQtyImportDto.getGpmdProId());
                if (gaiaProductBusiness == null) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品"));
                } else if (!gaiaProductBusiness.getProStatus().equals("0")) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品"));
                }

            }

            // 如果验证有错误则 把错误信息返回前台
            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            for (Integer key : map.keySet()) {
                StoreMinQtyImportDto storeMinQtyImportDto = (StoreMinQtyImportDto) map.get(key);
                GaiaProductMinDisplay display = new GaiaProductMinDisplay();
                BeanUtils.copyProperties(storeMinQtyImportDto, display);
                display.setClient(user.getClient());
                display.setGpmdMinQty(new BigDecimal(storeMinQtyImportDto.getGpmdMinQty()));
                GaiaProductMinDisplay existData = gaiaProductMinDisplayMapper.selectByPrimaryKey(display);
                // 插入
                if (existData == null) {
                    display.setGpmdCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    display.setGpmdCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    display.setGpmdCreateUser(user.getUserId());
                    gaiaProductMinDisplayMapper.insertSelective(display);

                } else {  // 更新
                    display.setGpmdChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                    display.setGpmdChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                    display.setGpmdChangeUser(user.getUserId());
                    gaiaProductMinDisplayMapper.updateByPrimaryKeySelective(display);
                }
            }

            return ResultUtil.success();

        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

    }
}
