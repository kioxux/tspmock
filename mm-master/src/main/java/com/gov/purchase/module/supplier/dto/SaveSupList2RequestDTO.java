package com.gov.purchase.module.supplier.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class SaveSupList2RequestDTO {

    /**
     * 商品数据列表
     */
    private List<GaiaSupplierBusinessInfo> basicsList;

    /**
     * 比对的结果数据
     */
    private List<SaveSupList1ResponseDTO> compSupList;

    /**
     * 首营地址
     */
    private String supSite;
}
