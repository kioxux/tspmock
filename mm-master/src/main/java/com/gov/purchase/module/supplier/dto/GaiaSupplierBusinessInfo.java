package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierSalesman;
import com.gov.purchase.module.base.dto.JyfwDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/9 14:39
 */
@Data
@EqualsAndHashCode
public class GaiaSupplierBusinessInfo extends GaiaSupplierBusiness {

    private List<JyfwDTO> jyfwList;

    /**
     * 首营日期
     */
    private String supGspDate;
    /**
     * GSP有效期
     */
    private String supGspDateValidity;
    /**
     * 业务员列表
     */
    private List<GaiaSupplierSalesman> supplierSalesmanList;

}
