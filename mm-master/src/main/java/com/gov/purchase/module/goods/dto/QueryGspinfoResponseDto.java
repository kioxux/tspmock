package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductGspinfo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "首营详情返回参数")
public class QueryGspinfoResponseDto extends GaiaProductGspinfo {

    private String proSiteName;

    private String proFormName;

    /**
     * 中药规格
     */
    private String proTcmSpecs;

    /**
     * 中药批准文号
     */
    private String proTcmRegisterNo;

    /**
     * 中药生产企业代码
     */
    private String proTcmFactoryCode;

    /**
     * 中药产地
     */
    private String proTcmPlace;

    /**
     * 参考进货价
     */
    private BigDecimal proCgj;

    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

    /**
     * 参考毛利率
     */
    private BigDecimal proMll;

    private String proJylbName;

}
