package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ListPayingResponseDTO {


    /**
     * 加盟商
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;


    /**
     * 组织
     */
    private String ficoCompanyCode;



    /**
     * 付款主体编码
     */
    private String ficoPayingstoreCode;

    /**
     * 付款主体名称
     */
    private String ficoPayingstoreName;




    /**
     * 付款方式
     */
    private String ficoPaymentMethod;

    /**
     * 付款金额
     */
    private BigDecimal ficoPaymentAmount;

    /**
     * 上期截止日期
     */
    private String ficoLastDeadline;

    /**
     * 本期开始日期
     */
    private String ficoStartingTime;

    /**
     * 本期截止日期
     */
    private String ficoEndingTime;
}
