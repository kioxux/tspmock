package com.gov.purchase.module.supplier.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaSupplierBasic;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.entity.GaiaSupplierGspinfo;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaSupplierBasicMapper;
import com.gov.purchase.mapper.GaiaSupplierBusinessMapper;
import com.gov.purchase.mapper.GaiaSupplierGspinfoMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.supplier.dto.QueryUnDrugSupCheckRequestDto;
import com.gov.purchase.module.supplier.service.UnDrugSupplierService;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class UnGrugSupplierServiceImpl implements UnDrugSupplierService {

    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;

    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;

    @Resource
    FeignService feignService;

    @Resource
    private UnDrugSupplierService unDrugSupplierService;

    @Resource
    GaiaSupplierGspinfoMapper gaiaSupplierGspinfoMapper;

    /**
     * 非药供应商校验
     *
     * @param dto QueryUnDrugSupCheckRequestDto
     */
    @Override
    public void resultUnDrugSupCheckInfo(QueryUnDrugSupCheckRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        // 加盟商
        // 编辑的场合
        if (CommonEnum.ProofType.PROOFVED.getCode().equals(dto.getType())) {

        } else { // 新建的场合
            dto.setClient(user.getClient());
        }

        //加盟商空值判断
        if (StringUtils.isBlank(dto.getClient())) {
            throw new CustomResultException(ResultEnum.CHECK_CLIENT);
        }

        //编辑的场合
        if (CommonEnum.ProofType.PROOFVAL.getCode().equals(dto.getType())) {
            if (StringUtils.isEmpty(dto.getSupSite()) || StringUtils.isEmpty(dto.getSupSelfCode())) {
                throw new CustomResultException(ResultEnum.CHECK_PARAMETER);
            }
            // 供应商自编码重复验证
            // 同一加盟商门店自编码不可以重复
            {
                //同加盟商下供应商自编码重复验证（首营表 GAIA_SUPPLIER_GSPINFO）
                List<GaiaSupplierGspinfo> gaiaSupplierGspinfos = gaiaSupplierGspinfoMapper.selfOnlyCheck(dto.getClient(), dto.getSupSelfCode(), dto.getSupSite());
                if (CollectionUtils.isNotEmpty(gaiaSupplierGspinfos)) {
                    throw new CustomResultException(ResultEnum.SUPPLISERSELFCODE_ERROR);
                }

                //同加盟商下供应商自编码重复验证（业务表 GAIA_SUPPLIER_BUSINESS）
                List<GaiaSupplierBusiness> gaiaSupplierBusinesses = gaiaSupplierBusinessMapper.selfOnlyCheck(dto.getClient(), dto.getSupSelfCode(), dto.getSupSite());
                if (CollectionUtils.isNotEmpty(gaiaSupplierBusinesses)) {
                    throw new CustomResultException(ResultEnum.SUPPLISERSELFCODE_ERROR);
                }
            }
        }
    }

    /**
     * 非药供应商新建
     *
     * @param dto QueryUnDrugSupCheckRequestDto
     */
    @Transactional
    public void insertUnDrugSupAddInfo(QueryUnDrugSupCheckRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //非药供应商校验
        unDrugSupplierService.resultUnDrugSupCheckInfo(dto);

        GaiaSupplierBusiness gaiaSupplierBusiness = new GaiaSupplierBusiness();
        BeanUtils.copyProperties(dto, gaiaSupplierBusiness);
        //加盟商
        dto.setClient(user.getClient());
        // 匹配状态字段默认为“0-未匹配”。
        gaiaSupplierBusiness.setSupMatchStatus(CommonEnum.MatchStatus.DIDMATCH.getCode());
        // 状态
        gaiaSupplierBusiness.setSupStatus(CommonEnum.SupplierStauts.SUPPLIERNORMAL.getCode());
        gaiaSupplierBusinessMapper.insertSelective(gaiaSupplierBusiness);
    }
}
