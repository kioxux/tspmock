package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "供应商历史请求参数")
public class SupplierHistoryRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "地点", name = "siteCode", required = true)
    @NotBlank(message = "地点不能为空")
    private String siteCode;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;



}
