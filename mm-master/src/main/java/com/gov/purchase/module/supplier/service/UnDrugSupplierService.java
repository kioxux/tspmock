package com.gov.purchase.module.supplier.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.supplier.dto.QueryUnDrugSupCheckRequestDto;


public interface UnDrugSupplierService {

    /**
     * 非药供应商校验
     *
     * @param dto QueryUnDrugSupCheckRequestDto
     */
    void resultUnDrugSupCheckInfo(QueryUnDrugSupCheckRequestDto dto);

    /**
     * 非药供应商新建
     *
     * @param dto QueryUnDrugSupCheckRequestDto
     */
    void insertUnDrugSupAddInfo(QueryUnDrugSupCheckRequestDto dto);
}
