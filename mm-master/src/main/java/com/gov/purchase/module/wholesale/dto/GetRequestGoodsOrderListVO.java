package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.common.entity.Pageable;
import com.gov.purchase.feign.dto.TokenUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author zhoushuai
 * @date 2021/3/17 9:55
 */
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class GetRequestGoodsOrderListVO extends Pageable {
    /**
     * 门店编码
     */
    private String gsrhBrId;

    /**
     * 日期开始
     */
    private String gsrhDateStart;
    /**
     * 日期结束
     */
    private String gsrhDateEnd;

    private TokenUser user;

    /**
     * 价格方式
     */
    private Integer priceType;

    /**
     * 货主
     */
    private String soOwner;
}
