package com.gov.purchase.module.fieldConf.dto;

import lombok.Data;

import java.util.List;

/**
 * @description: 首营必填字段保存传参
 * @author: yzf
 * @create: 2021-12-08 10:34
 */
@Data
public class FieldRequiredConfDTO {
    /**
     * 类型（1-商品 2-供应商 3-客户）
     */
    private String type;

    /**
     * 字段列表
     */
    private List<String> field;

}
