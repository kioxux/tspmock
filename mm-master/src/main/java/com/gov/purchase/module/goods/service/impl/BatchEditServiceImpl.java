package com.gov.purchase.module.goods.service.impl;

import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductBusinessKey;
import com.gov.purchase.entity.GaiaProductUpdateItem;
import com.gov.purchase.entity.GaiaProductUpdateItemKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaProductUpdateItemMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.BatchEditParam;
import com.gov.purchase.module.goods.dto.ProductUpdate;
import com.gov.purchase.module.goods.dto.ProductUpdateItem;
import com.gov.purchase.module.goods.service.BatchEditService;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.09
 */
@Slf4j
@Service
public class BatchEditServiceImpl implements BatchEditService {

    @Resource
    private GaiaProductUpdateItemMapper gaiaProductUpdateItemMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;

    /**
     * 修改项目
     *
     * @return
     */
    @Override
    public List<GaiaProductUpdateItem> selectProductUpdateItemList() {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaProductUpdateItem> list = gaiaProductUpdateItemMapper.selectProductUpdateItemList(user.getClient());
        return list;
    }

    /**
     * 修改地点
     *
     * @return
     */
    @Override
    public List<Dictionary> selectProductUpdateSiteList() {
        TokenUser user = feignService.getLoginInfo();
        List<Dictionary> list = gaiaProductUpdateItemMapper.selectProductUpdateSiteList(user.getClient());
        return list;
    }

    /**
     * 商品数据
     *
     * @param proSelfCode
     * @param gpuiField
     * @return
     */
    @Override
    public List<ProductUpdateItem> selectProductItem(String proSite, String proSelfCode, String gpuiField, List<BatchEditParam> batchEditParamList) {
        TokenUser user = feignService.getLoginInfo();
        GaiaProductUpdateItemKey gaiaProductUpdateItemKey = new GaiaProductUpdateItemKey();
        gaiaProductUpdateItemKey.setClient(user.getClient());
        gaiaProductUpdateItemKey.setGpuiField(gpuiField);
        GaiaProductUpdateItem gaiaProductUpdateItem = gaiaProductUpdateItemMapper.selectByPrimaryKey(gaiaProductUpdateItemKey);
        if (gaiaProductUpdateItem == null) {
            throw new CustomResultException("修改字段已变更，请刷新后再试");
        }

        List<String> proSelfCodeList = new ArrayList<>();
        if (StringUtils.isNotBlank(proSelfCode)) {
            proSelfCodeList = Arrays.asList(proSelfCode.split(","));
        }
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectByPrimaryKeyList1(user.getClient(), proSite, proSelfCodeList, batchEditParamList);
        List<ProductUpdateItem> list = new ArrayList<>();
        for (GaiaProductBusiness gaiaProductBusiness : gaiaProductBusinessList) {
            String field = StringUtils.lineToHump(gpuiField);
            Object value = StringUtils.getFieldValueByName(field, gaiaProductBusiness);
            ProductUpdateItem productUpdateItem = new ProductUpdateItem();
            BeanUtils.copyProperties(gaiaProductUpdateItem, productUpdateItem);
            BeanUtils.copyProperties(gaiaProductBusiness, productUpdateItem);
            productUpdateItem.setOldVal(value == null ? null : value.toString());
            productUpdateItem.setProSelfCode(gaiaProductBusiness.getProSelfCode());
            list.add(productUpdateItem);
        }
        return list;
    }

    /**
     * 修改商品
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateProduct(ProductUpdateItem productUpdateItem, List<Dictionary> dictionary, Map<String, List<String>> map,
                              List<String> proSiteList, List<String> proSelfCodeList) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaProductBusinessKey> list = new ArrayList<>();
        {
            for (Dictionary item : dictionary) {
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(user.getClient());
                gaiaProductBusinessKey.setProSite(item.getValue());
                gaiaProductBusinessKey.setProSelfCode(productUpdateItem.getProSelfCode());
                list.add(gaiaProductBusinessKey);
                proSiteList.add(item.getValue());
                proSelfCodeList.add(productUpdateItem.getProSelfCode());
            }
        }
        for (Dictionary item : dictionary) {
            // 仓库
            if (item.getType().equals("2")) {
                List<String> stoList = map.get(item.getValue());
                if (stoList == null) {
                    stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(user.getClient(), item.getValue());
                    map.put(item.getValue(), stoList);
                }
                // 秕发仓库判断
                Integer wmsCnt = gaiaDcDataMapper.selectCompadmWmsCnt(user.getClient(), item.getValue());
                if (wmsCnt.intValue() > 0) {
                    List<String> noWwsSiteList = gaiaProductBusinessMapper.getNoWwsSiteList(user.getClient(), item.getValue());
                    if (!CollectionUtils.isEmpty(noWwsSiteList)) {
                        stoList.addAll(noWwsSiteList);
                        stoList = stoList.stream().distinct().collect(Collectors.toList());
                        map.put(item.getValue(), stoList);
                    }
                }

                if (CollectionUtils.isNotEmpty(stoList)) {
                    for (String stoCode : stoList) {
                        GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                        gaiaProductBusinessKey.setClient(user.getClient());
                        gaiaProductBusinessKey.setProSite(stoCode);
                        gaiaProductBusinessKey.setProSelfCode(productUpdateItem.getProSelfCode());
                        list.add(gaiaProductBusinessKey);
                        proSiteList.add(stoCode);
                        proSelfCodeList.add(productUpdateItem.getProSelfCode());
                    }
                }
            }
        }
        gaiaProductBusinessMapper.updateProductByItem(user.getClient(), productUpdateItem.getProSelfCode(),
                productUpdateItem.getGpuiField(), productUpdateItem.getNewVal(), list);
    }
}
