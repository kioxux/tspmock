package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaSdReplenishD;
import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

import java.util.List;

/**
 * @description: 连锁总部及其下的门店列表
 * @author: yzf
 * @create: 2021-11-15 10:42
 */
@Data
public class CompadmForStoreDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 连锁总部id
     */
    private String compadmId;

    /**
     * 连锁总部名称
     */
    private String compadmName;

    /**
     * 门店Id
     */
    private String stoCode;

    /**
     * 门店名
     */
    private String stoName;

    /**
     * 补货开始日期
     */
    private String startDate;

    /**
     * 补货结束日期
     */
    private String endDate;

    /**
     * 补货单号
     */
    private String voucherId;

    /**
     * 补货类型
     */
    private String pattern;

    /**
     * 审核状态
     */
    private String flag;
    private String dnFlag;

    /**
     * 开单人
     */
    private String createBy;

    /**
     * 开单日期
     */
    private String createDate;

    /**
     * 开单时间
     */
    private String createTime;

    private Integer pageSize;

    private Integer pageNum;

    /**
     * 商品编码
     */
    private String proCode;

    private List<SdReplenishDExtandDTO> details;

    /**
     * 补货地点
     */
    private String gsrhAddr;

    private String poId;
}
