package com.gov.purchase.module.base.dto.excel;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
@Data
public class Customer {

    /**
     * 客户编码
     */
    @ExcelValidate(index = 0, name = "客户编码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false)
    private String cusCode;

    /**
     * 客户名称
     */
    @ExcelValidate(index = 1, name = "客户名称", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String cusName;
    /**
     * 助记码
     */
    @ExcelValidate(index = 2, name = "助记码", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String cusPym;
    /**
     * 统一社会信用代码
     */
    @ExcelValidate(index = 3, name = "统一社会信用代码", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String cusCreditCode;
    /**
     * 营业期限
     */
    @ExcelValidate(index = 4, name = "营业期限", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", addRequired = false, editRequired = false)
    private String cusCreditDate;
    /**
     * 客户分类
     */
    @ExcelValidate(index = 5, name = "客户分类", type = ExcelValidate.DataType.STRING, maxLength = 10, editRequired = false)
    private String cusClass;
    /**
     * 法人
     */
    @ExcelValidate(index = 6, name = "法人", type = ExcelValidate.DataType.STRING, maxLength = 20, editRequired = false)
    private String cusLegalPerson;
    /**
     * 注册地址
     */
    @ExcelValidate(index = 7, name = "注册地址", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String cusRegAdd;
    /**
     * 客户状态
     */
    @ExcelValidate(index = 8, name = "客户状态", type = ExcelValidate.DataType.STRING, maxLength = 10,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.CUS_STATUS, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "cusStatusValue", editRequired = false)
    private String cusStatus;
    /**
     * 客户状态
     */
    private String cusStatusValue;
    /**
     * 许可证编号
     */
    @ExcelValidate(index = 9, name = "许可证编号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String cusLicenceNo;
    /**
     * 发证日期
     */
    @ExcelValidate(index = 10, name = "发证日期", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", addRequired = false, editRequired = false)
    private String cusLicenceDate;
    /**
     * 有效期至
     */
    @ExcelValidate(index = 11, name = "有效期至", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", addRequired = false, editRequired = false)
    private String cusLicenceValid;
    /**
     * 生产或经营范围
     */
    @ExcelValidate(index = 12, name = "生产或经营范围", type = ExcelValidate.DataType.STRING, maxLength = 200, addRequired = false, editRequired = false)
    private String cusScope;
    /**
     * 加盟商
     */
    @ExcelValidate(index = 13, name = "加盟商", type = ExcelValidate.DataType.STRING, maxLength = 8, addRequired = false, editRequired = false,
            relyRequired = {"cusSite", "cusSelfCode", "siteCusStatus", "cusNoSale", "cusNoReturn", "cusPayTerm",
                    "cusBussinessContact", "cusDeliveryAdd", "cusBankCode", "cusBankName", "cusAccountPerson", "cusBankAccount",
                    "cusPayMode", "cusCreditAmt", "cusCreditFlag", "cusCreditQuota", "cusCreditCheck", ""})
    private String client;
    /**
     * 地点
     */
    @ExcelValidate(index = 14, name = "地点", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false,
            relyRequired = {"client"})
    private String cusSite;
    /**
     * 客户自编码
     */
    @ExcelValidate(index = 15, name = "客户自编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false,
            relyRequired = {"client"})
    private String cusSelfCode;
    /**
     * 地点客户状态
     */
    @ExcelValidate(index = 16, name = "地点客户状态", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.CUS_STATUS, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            relyRequired = {"client"}, dictionaryStaticField = "siteCusStatusValue", editRequired = false)
    private String siteCusStatus;
    /**
     * 地点客户状态
     */
    private String siteCusStatusValue;
    /**
     * 禁止销售
     */
    @ExcelValidate(index = 17, name = "禁止销售", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "cusNoSaleValue", editRequired = false)
    private String cusNoSale;
    /**
     * 禁止销售
     */
    private String cusNoSaleValue;
    /**
     * 禁止退货
     */
    @ExcelValidate(index = 18, name = "禁止退货", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "cusNoReturnValue", editRequired = false)
    private String cusNoReturn;
    /**
     * 禁止退货
     */
    private String cusNoReturnValue;
    /**
     * 付款条件
     */
    @ExcelValidate(index = 19, name = "付款条件", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String cusPayTerm;
    /**
     * 业务联系人
     */
    @ExcelValidate(index = 20, name = "业务联系人", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String cusBussinessContact;
    /**
     * 收货地址
     */
    @ExcelValidate(index = 21, name = "收货地址", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false,
            relyRequired = {"client"})
    private String cusDeliveryAdd;
    /**
     * 银行代码
     */
    @ExcelValidate(index = 22, name = "银行代码", type = ExcelValidate.DataType.STRING, maxLength = 12, addRequired = false, editRequired = false)
    private String cusBankCode;
    /**
     * 银行名称
     */
    @ExcelValidate(index = 23, name = "银行名称", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false, editRequired = false)
    private String cusBankName;
    /**
     * 账户持有人
     */
    @ExcelValidate(index = 24, name = "账户持有人", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String cusAccountPerson;
    /**
     * 银行账号
     */
    @ExcelValidate(index = 25, name = "银行账号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String cusBankAccount;
    /**
     * 支付方式
     */
    @ExcelValidate(index = 26, name = "支付方式", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.SUP_PAY_MODE, editRequired = false)
    private String cusPayMode;
    /**
     * 铺底授信额度
     */
    @ExcelValidate(index = 27, name = "铺底授信额度", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false, editRequired = false)
    private String cusCreditAmt;
    /**
     * 是否启用信用管理
     */
    @ExcelValidate(index = 28, name = "是否启用信用管理", type = ExcelValidate.DataType.STRING, maxLength = 4, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "cusCreditFlagValue", editRequired = false)
    private String cusCreditFlag;
    /**
     * 是否启用信用管理
     */
    private String cusCreditFlagValue;
    /**
     * 信用额度
     */
    @ExcelValidate(index = 29, name = "信用额度", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false, editRequired = false)
    private String cusCreditQuota;
    /**
     * 信用检查点
     */
    @ExcelValidate(index = 30, name = "信用检查点", type = ExcelValidate.DataType.STRING, maxLength = 4, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.CUS_CREDIT_CHECK, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "cusCreditCheckValue", editRequired = false)
    private String cusCreditCheck;
    /**
     * 信用检查点
     */
    private String cusCreditCheckValue;
}

