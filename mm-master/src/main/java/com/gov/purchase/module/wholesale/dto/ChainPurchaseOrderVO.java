package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaPoHeader;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 调取连锁采购单
 * @author: yzf
 * @create: 2021-11-18 09:48
 */
@Data
public class ChainPurchaseOrderVO extends GaiaPoHeader {


    /**
     * 仓库名-采购主体
     */
    private String dcName;

    /**
     * 门店名-采购主体
     */
    private String stoName;

    /**
     * 送达方名
     */
    private String deliveryPerName;

    /**
     * 销售员
     */
    private String salePerName;

    /**
     * 销售员Id
     */
    private String poSupplierSalesman;

    /**
     * 送达方id
     */
    private String deliveryPerCode;

    /**
     * 客户名
     */
    private String cusName;

    /**
     * 订单行数
     */
    private Integer lineCount;

    /**
     * 订单总数量
     */
    private BigDecimal totalCount;


    /**
     * 订单总金额
     */
    private BigDecimal totalPrice;

    private String poSoFlag;

    private String soId;
}
