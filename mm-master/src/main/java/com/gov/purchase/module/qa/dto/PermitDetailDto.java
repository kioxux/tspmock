package com.gov.purchase.module.qa.dto;

import lombok.Data;

@Data
public class PermitDetailDto {

    private String perCode;

    private String perName;

    private String perId;

    private String perDateStart;

    private String perDateEnd;

    private String perRecordDate;

    private String perStopFlag;

}
