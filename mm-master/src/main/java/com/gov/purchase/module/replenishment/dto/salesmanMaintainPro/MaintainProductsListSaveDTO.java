package com.gov.purchase.module.replenishment.dto.salesmanMaintainPro;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel("业务员商品授权维护保存对象DTO")
@Data
public class MaintainProductsListSaveDTO {

    @ApiModelProperty("机构编码")
    private String supSite;

    @ApiModelProperty("供应商编码")
    private String supSelfCode;

    @ApiModelProperty("业务员编码")
    private String gssCode;

    @ApiModelProperty("是否按照品种管控 1：是，0：否")
    private String isControl;

    @ApiModelProperty("商品自编码列表")
    private List<String> proSelfCodeList;
}
