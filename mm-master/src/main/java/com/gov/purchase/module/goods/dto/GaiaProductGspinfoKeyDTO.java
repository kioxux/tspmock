package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class GaiaProductGspinfoKeyDTO {

    @NotBlank(message = "加盟商不能为空")
    private String client;
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;
    @NotBlank(message = "地点不能为空")
    private String proSite;
    /**
     * 参考零售价
     */
    private BigDecimal proLsj;

}
