package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "商品状态更新-获取商品编号列表返回结果")
public class QueryProductListResponseDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "商品编号", name = "proCode")
    private String proCode;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "地点", name = "proSite")
    private String proSite;

    @ApiModelProperty(value = "商品描述", name = "proDepict")
    private String proDepict;

    @ApiModelProperty(value = "商品名称", name = "proName")
    private String proName;

    @ApiModelProperty(value = "商品状态", name = "proStatus")
    private String proStatus;

    @ApiModelProperty(value = "商品定位", name = "proPosition")
    private String proPosition;

    @ApiModelProperty(value = "禁止销售", name = "proNoRetail")
    private String proNoRetail;

    @ApiModelProperty(value = "禁止采购", name = "proNoPurchase")
    private String proNoPurchase;

    @ApiModelProperty(value = "禁止配送", name = "proNoDistributed")
    private String proNoDistributed;

    @ApiModelProperty(value = "禁止退厂", name = "proNoSupplier")
    private String proNoSupplier;

    @ApiModelProperty(value = "禁止退仓", name = "proNoDc")
    private String proNoDc;

    @ApiModelProperty(value = "禁止调剂", name = "proNoAdjust")
    private String proNoAdjust;

    @ApiModelProperty(value = "禁止批发", name = "proNoSale")
    private String proNoSale;

    @ApiModelProperty(value = "禁止请货", name = "proNoApply")
    private String proNoApply;

    @ApiModelProperty(value = "是否拆零", name = "proIfpart")
    private String proIfpart;

    @ApiModelProperty(value = "拆零单位", name = "proPartUint")
    private String proPartUint;

    @ApiModelProperty(value = "拆零比例", name = "proPartRate")
    private String proPartRate;

    @ApiModelProperty(value = "采购单位", name = "proPurchaseUnit")
    private String proPurchaseUnit;

    @ApiModelProperty(value = "采购比例", name = "proPurchaseRate")
    private String proPurchaseRate;

    @ApiModelProperty(value = "禁止配送", name = "proSaleUnit")
    private String proSaleUnit;

    @ApiModelProperty(value = "采购比例", name = "proSaleRate")
    private String proSaleRate;

    @ApiModelProperty(value = "最小订货量", name = "proMinQty")
    private BigDecimal proMinQty;

    @ApiModelProperty(value = "是否医保", name = "proIfMed")
    private String proIfMed;

    @ApiModelProperty(value = "销售级别", name = "proSlaeClass")
    private String proSlaeClass;

    @ApiModelProperty(value = "限购数量", name = "proLimitQty")
    private BigDecimal proLimitQty;

    private String fieldOldValue;

    private String fieldNewValue;
}
