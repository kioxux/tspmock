package com.gov.purchase.module.blacklist.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BatchNoData {
    @ApiModelProperty(value = "加盟商")
    private String client;
    @ApiModelProperty(value = "门店编码")
    private String stoCode;
    @ApiModelProperty(value = "商品编码")
    private String proCode;
    @ApiModelProperty(value = "生产批号")
    private String batchNo;
}
