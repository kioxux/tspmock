package com.gov.purchase.module.blacklist.service;

import com.gov.purchase.module.blacklist.dto.BatchNoData;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface BlackListProService {
    void importBlackListPro(MultipartFile file);

    List<BatchNoData> selectBatchNo(BatchNoData inData);
}
