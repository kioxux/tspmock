package com.gov.purchase.module.goods.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021/4/26 10:42
 */
@Data
public class ProEditVO {
    @NotNull(message = "商品数据不能为空")
    private ProEditRequestDTO business;

    private String remarks;

    private Integer editNoWws;
}
