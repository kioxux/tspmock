package com.gov.purchase.module.store.dto.store;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "门店列表返回参数")
public class StoreListResponseDto {

    @ApiModelProperty(value = "门店编号", name = "stoCode")
    private String stoCode;

    @ApiModelProperty(value = "门店名称", name = "stoName")
    private String stoName;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "加盟商名称", name = "francName")
    private String francName;

    @ApiModelProperty(value = "门店状态 0-营业、1-闭店", name = "stoStatus")
    private String stoStatus;

    @ApiModelProperty(value = "门店组编号", name = "stogCode")
    private String stogCode;

    @ApiModelProperty(value = "门店组名称", name = "stogName")
    private String stogName;

    @ApiModelProperty(value = "门店组编号", name = "stogCodeBefore")
    private String stogCodeBefore;

}
