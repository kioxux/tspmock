package com.gov.purchase.module.replenishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class SaveNeedProListRequestDTO {

    @NotBlank(message = "铺货地点（dc编码）不能为空")
    private String dcCode;
    @NotBlank(message = "商品编码不能为空")
    private String proSelfCode;
    @NotBlank(message = "门店编码不能为空")
    private String stoCode;


    @NotBlank(message = "铺货数量不能为空")
    private BigDecimal gsrdNeedQty;
    @NotEmpty(message = "批号信息不能为空")
    private List<BatchNoInfo> batchNoInfoList;
    @NotBlank(message = "货位号不能为空")
    private String gsrdHwh;


    @NotBlank(message = "商品名不能为空")
    private String proName;
    @NotBlank(message = "门店名不能为空")
    private String stoName;

    // @NotBlank(message = "规格不能为空")
    private String proSpecs;

    // @NotBlank(message = "生产厂家不能为空")
    private String proFactoryName;

    // @NotBlank(message = "单位不能为空")
    private String proUnit;

    // @NotNull(message = "加点金额不能为空")
    private BigDecimal proAddAmt;

    // @NotNull(message = "加点比例不能为空")
    private BigDecimal proAddRate;

    // @NotNull(message = "配送价不能为空")
    private BigDecimal deliveryPrice;

    // @NotNull(message = "零售价不能为空")
    private BigDecimal proLsj;

    // @NotNull(message = "成本价不能为空")
    private BigDecimal costPrice;

    // @NotBlank(message = "是否医保不能为空")
    private String proIfMed;

    // @NotBlank(message = "定位不能为空")
    private String proPosition;

    // @NotBlank(message = "销售级别不能为空")
    private String proSlaeClass;

    // @NotNull(message = "门店库存不能为空")
    private BigDecimal gssQty;

    // @NotBlank(message = "税率不能为空")
    private String gsrdTaxRate;

    /**
     * 备注
     */
    private String gsrdBz;
    /**
     * 铺货来源：3-新品铺货
     */
    private String gsrhSource;

    /**
     * dc委托配置中心
     */
    private String dcWtdc;

    /**
     * 是否检查库存（0： 否， 1： 是）
     */
    private String flag;
}