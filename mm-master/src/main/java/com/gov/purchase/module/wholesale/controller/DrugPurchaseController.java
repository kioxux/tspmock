package com.gov.purchase.module.wholesale.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.wholesale.dto.DrugPurchaseDTO;
import com.gov.purchase.module.wholesale.service.DrugPurchaseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 药品采购记录查询
 * @author: yzf
 * @create: 2021-11-04 11:14
 */
@Api(tags = "药品采购记录查询")
@RestController
@RequestMapping("drugPurchase")
public class DrugPurchaseController {

    @Resource
    private DrugPurchaseService drugPurchaseService;

    /**
     * 药品采购记录查询
     *
     * @return
     */
    @Log("药品采购记录查询")
    @ApiOperation("药品采购记录查询")
    @PostMapping("queryDrugPurchaseRecord")
    public Result queryDrugPurchaseRecord(@RequestBody DrugPurchaseDTO drugPurchaseDTO) {
        return ResultUtil.success(drugPurchaseService.queryDrugPurchaseRecord(drugPurchaseDTO));
    }


    /**
     * 采购员列表
     *
     * @return
     */
    @Log("采购员列表")
    @ApiOperation("采购员列表")
    @PostMapping("queryProPurchaseRate")
    public Result queryProPurchaseRate(@RequestBody DrugPurchaseDTO drugPurchaseDTO) {
        return drugPurchaseService.queryProPurchaseRate(drugPurchaseDTO);
    }

    /**
     * 药品采购记录导出
     *
     * @return
     */
    @Log("药品采购记录导出")
    @ApiOperation("药品采购记录导出")
    @PostMapping("exportDrugPurchaseRecord")
    public Result exportDrugPurchaseRecord(@RequestBody DrugPurchaseDTO drugPurchaseDTO) {
        return drugPurchaseService.exportDrugPurchaseRecord(drugPurchaseDTO);
    }
}
