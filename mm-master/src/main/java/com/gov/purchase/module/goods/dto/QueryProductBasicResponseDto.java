package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "药品库搜索返回参数")
public class QueryProductBasicResponseDto {

    @ApiModelProperty(value = "国际条形码", name = "proBarcode")
    private String proBarcode;

    @ApiModelProperty(value = "商品名", name = "proName")
    private String proName;

    @ApiModelProperty(value = "批准文号", name = "proRegisterNo")
    private String proRegisterNo;

    @ApiModelProperty(value = "生产厂家", name = "proFactoryName")
    private String proFactoryName;

    @ApiModelProperty(value = "规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "国际条形码2", name = "proBarcode2")
    private String proBarcode2;

    @ApiModelProperty(value = "单位", name = "proUnit")
    private String proUnit;

    @ApiModelProperty(value = "单位名称", name = "proUnitName")
    private String proUnitName;

    @ApiModelProperty(value = "状态 “正常”、“停用“", name = "proStatus")
    private String proStatus;

    @ApiModelProperty(value = "是否经营 “是”、“否”", name = "sell")
    private String sell;

    @ApiModelProperty(value = "剂型", name = "proForm")
    private String proForm;

    @ApiModelProperty(value = "储存条件", name = "proStorageCondition")
    private String proStorageCondition;

    @ApiModelProperty(value = "商品编码", name = "proCode")
    private String proCode;

    @ApiModelProperty(value = "商品分类", name = "proClass")
    private String proClass;

    @ApiModelProperty(value = "通用名称", name = "proCommonname")
    private String proCommonname;

    @ApiModelProperty(value = "生产厂家代码", name = "proFactoryCode")
    private String proFactoryCode;

    @ApiModelProperty(value = "进项税率", name = "proInputTax")
    private String proInputTax;
}
