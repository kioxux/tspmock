package com.gov.purchase.module.goods.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 进货质量
 * @author: yzf
 * @create: 2021-11-08 17:14
 */
@Data
public class GoodsQualityDTO {

    /**
     * 进货日期
     */
    private String purchaseDate;
    /**
     * 产品批号
     */
    private String batchNo;
    /**
     * 进货数量
     */
    private BigDecimal purchaseCount;
}
