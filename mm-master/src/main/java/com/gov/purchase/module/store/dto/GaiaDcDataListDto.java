package com.gov.purchase.module.store.dto;

import com.gov.purchase.entity.GaiaDcData;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GaiaDcDataListDto extends GaiaDcData {

    /**
     * 加盟商名称
     */
    private String francName;

}
