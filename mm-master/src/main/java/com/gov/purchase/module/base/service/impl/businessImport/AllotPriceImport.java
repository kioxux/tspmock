package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaAllotPriceMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.businessImport.AllotPrice;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.29
 */
@Service
public class AllotPriceImport extends BusinessImport {

    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaAllotPriceMapper gaiaAllotPriceMapper;

    /**
     * 导入
     *
     * @param map   数据
     * @param field 字段
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        //错误LIST
        List<String> errorList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 行数据验证
        for (Integer key : map.keySet()) {
            // 行数据
            AllotPrice allotPrice = (AllotPrice) map.get(key);
            int priceCount = 0;
            if (StringUtils.isNotBlank(allotPrice.getAlpAddAmt())) {
                priceCount++;
            }
            if (StringUtils.isNotBlank(allotPrice.getAlpAddRate())) {
                priceCount++;
            }
            if (StringUtils.isNotBlank(allotPrice.getAlpCataloguePrice())) {
                if (StringUtils.isBlank(allotPrice.getAlpProCode())) {
                    errorList.add(MessageFormat.format("第{0}行：选填目录价时，商品编码不能为空", key + 1));
                }
                priceCount++;
            }
            if (priceCount != 1) {
                errorList.add(MessageFormat.format("第{0}行：[加点比例]、[加点金额]、[目录价]必填一个,且只能填一个", key + 1));
            }
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(user.getClient());
            gaiaStoreDataKey.setStoCode(allotPrice.getAlpReceiveSite());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            if (gaiaStoreData == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "收货地点"));
                continue;
            }
            if (StringUtils.isNotBlank(allotPrice.getAlpProCode())) {
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(user.getClient());
                gaiaProductBusinessKey.setProSite(allotPrice.getAlpReceiveSite());
                gaiaProductBusinessKey.setProSelfCode(allotPrice.getAlpProCode());
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                if (gaiaProductBusiness == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品编码"));
                    continue;
                }
            }
        }
        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }

        for (Integer key : map.keySet()) {
            // 行数据
            AllotPrice allotPrice = (AllotPrice) map.get(key);
            GaiaAllotPrice gaiaAllotPrice = null;
            if (StringUtils.isBlank(allotPrice.getAlpProCode())) {
                gaiaAllotPrice = gaiaAllotPriceMapper.selectAllotPriceBySite(user.getClient(), allotPrice.getAlpReceiveSite());
            } else {
                gaiaAllotPrice = gaiaAllotPriceMapper.selectAllotPriceByPro(user.getClient(), allotPrice.getAlpReceiveSite(), allotPrice.getAlpProCode());
            }
            if (gaiaAllotPrice == null) {
                gaiaAllotPrice = new GaiaAllotPrice();
                // 加盟商
                gaiaAllotPrice.setClient(user.getClient());
                // 收货地点
                gaiaAllotPrice.setAlpReceiveSite(allotPrice.getAlpReceiveSite());
                // 商品编码
                gaiaAllotPrice.setAlpProCode(allotPrice.getAlpProCode());
                // 加价金额
                gaiaAllotPrice.setAlpAddAmt(StringUtils.isBlank(allotPrice.getAlpAddAmt()) ? null : NumberUtils.toScaledBigDecimal(allotPrice.getAlpAddAmt()));
                // 加价比例
                gaiaAllotPrice.setAlpAddRate(StringUtils.isBlank(allotPrice.getAlpAddRate()) ? null : NumberUtils.toScaledBigDecimal(allotPrice.getAlpAddRate()));
                // 目录价
                gaiaAllotPrice.setAlpCataloguePrice(StringUtils.isBlank(allotPrice.getAlpCataloguePrice()) ? null : NumberUtils.toScaledBigDecimal(allotPrice.getAlpCataloguePrice()));
                // 创建人
                gaiaAllotPrice.setAlpCreateBy(user.getUserId());
                // 创建日期
                gaiaAllotPrice.setAlpCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 创建时间
                gaiaAllotPrice.setAlpCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceMapper.insertSelective(gaiaAllotPrice);
            } else {
                // 加价金额
                gaiaAllotPrice.setAlpAddAmt(StringUtils.isBlank(allotPrice.getAlpAddAmt()) ? null : NumberUtils.toScaledBigDecimal(allotPrice.getAlpAddAmt()));
                // 加价比例
                gaiaAllotPrice.setAlpAddRate(StringUtils.isBlank(allotPrice.getAlpAddRate()) ? null : NumberUtils.toScaledBigDecimal(allotPrice.getAlpAddRate()));
                // 目录价
                gaiaAllotPrice.setAlpCataloguePrice(StringUtils.isBlank(allotPrice.getAlpCataloguePrice()) ? null : NumberUtils.toScaledBigDecimal(allotPrice.getAlpCataloguePrice()));
                // 更新人
                gaiaAllotPrice.setAlpUpdateBy(user.getUserId());
                // 更新日期
                gaiaAllotPrice.setAlpUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 更新时间
                gaiaAllotPrice.setAlpUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceMapper.updateByPrimaryKey(gaiaAllotPrice);
            }
        }
        return ResultUtil.success();
    }
}
