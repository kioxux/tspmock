package com.gov.purchase.module.base.service.impl.extendImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaDcDataKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.excel.Extend;
import com.gov.purchase.module.base.service.impl.ImportData;
import org.apache.commons.collections4.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.20
 */
public abstract class ExtendImport extends ImportData {

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;

    /**
     * 商品扩展验证
     *
     * @param dataMap
     * @return errorList
     */
    protected Result errorListExtend(LinkedHashMap<Integer, Extend> dataMap) {
        List<String> errorList = new ArrayList<>();
        // 数据为空
        if (dataMap == null || dataMap.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0104);
        }

        // 商品编码
        Map<String, List<String>> proCodeMap = new HashMap<>();
        // 加盟商 商品编码重复验证
        Map<String, List<String>> clientProCodeMap = new HashMap<>();

        // 遍历验证
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Extend extend = dataMap.get(key);
            // 商品编码行号
            List<String> proCodeList = proCodeMap.get(extend.getProCode());
            if (proCodeList == null) {
                proCodeList = new ArrayList<>();
            }
            proCodeList.add(String.valueOf((key + 1)));
            proCodeMap.put(extend.getProCode(), proCodeList);

            // 加盟商商品自编码行号
            List<String> clientProCodeList = clientProCodeMap.get(extend.getClient() + "_" + extend.getProSelfCode());
            if (clientProCodeList == null) {
                clientProCodeList = new ArrayList<>();
            }
            clientProCodeList.add(String.valueOf((key + 1)));
            clientProCodeMap.put(extend.getClient() + "_" + extend.getProSelfCode(), clientProCodeList);

            // 加盟商 地点 验证
            // 门店数据
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(extend.getClient());
            gaiaStoreDataKey.setStoCode(extend.getProSite());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            // DC数据
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(extend.getClient());
            gaiaDcDataKey.setDcCode(extend.getProSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (gaiaStoreData == null && gaiaDcData == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0117.getMsg(), key + 1));
            }

            // 业务数据验证
            businessCheck(extend, errorList, key);
        }
        // 商品编码重复验证
        for (String proCode : proCodeMap.keySet()) {
            List<String> lineList = proCodeMap.get(proCode);
            if (lineList.size() > 1) {
                errorList.add(MessageFormat.format(ResultEnum.E0119.getMsg(), String.join(",", lineList)));
            }
        }
        // 加盟商商品自编码重复验证
        for (String clientProCode : clientProCodeMap.keySet()) {
            List<String> lineList = clientProCodeMap.get(clientProCode);
            if (lineList.size() > 1) {
                errorList.add(MessageFormat.format(ResultEnum.E0120.getMsg(), String.join(",", lineList)));
            }
        }
        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }

    /**
     * 主数据表验证
     *
     * @param extend
     * @param errorList
     * @param key
     */
    protected abstract void baseCheck(Extend extend, List<String> errorList, int key);

    /**
     * 业务数据验证
     *
     * @param extend
     * @param errorList
     * @param key
     */
    protected abstract void businessCheck(Extend extend, List<String> errorList, int key);
}
