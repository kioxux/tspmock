package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.StoreRequestsRecordService;
import com.gov.purchase.utils.CommonUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @description: 连锁总部请货数据
 * @author: yzf
 * @create: 2021-11-15 11:00
 */
@Service
public class StoreRequestsRecordServiceImpl implements StoreRequestsRecordService {

    private final static String PO_ID_KEY = "POID";

    @Resource
    private GaiaCompadmMapper gaiaCompadmMapper;

    @Resource
    private GaiaStoreDataMapper storeDataMapper;

    @Resource
    private FeignService feignService;

    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;

    @Resource
    private RedisClient redisClient;

    @Resource
    private GaiaPoHeaderMapper gaiaPoHeaderMapper;

    @Resource
    private GaiaPoItemMapper gaiaPoItemMapper;

    @Override
    public List<CompadmVO> queryCompadms() {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        // 连锁总部列表
        List<CompadmVO> compadmVOS = gaiaCompadmMapper.selectCompadms(user.getClient());
        return compadmVOS;
    }

    @Override
    public List<GaiaStoreData> queryStores(String stoChainHead) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        return storeDataMapper.queryStoreListByCompadmId(user.getClient(), stoChainHead);
    }

    @Override
    public PageInfo<SdReplenishHExtandVO> queryStoreRequestsRecord(CompadmForStoreDto compadmForStoreDto) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        compadmForStoreDto.setClient(user.getClient());
        // 判断审核状态
        if (compadmForStoreDto.getFlag() == null) {
            compadmForStoreDto.setFlag("");
        }
        // 请货清单查询分页
        PageHelper.startPage(compadmForStoreDto.getPageNum(), compadmForStoreDto.getPageSize());
        return new PageInfo<SdReplenishHExtandVO>(gaiaSdReplenishHMapper.queryStoreRequestsRecord(compadmForStoreDto));
    }

    @Override
    public SdReplenishDExtandVO queryStoreRequestsDetailRecord(CompadmForStoreDto compadmForStoreDto) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        compadmForStoreDto.setClient(user.getClient());
        // 判断审核状态
        if (compadmForStoreDto.getFlag() == null) {
            compadmForStoreDto.setFlag("");
        }
        List<SdReplenishHExtandVO> sdReplenishHExtandVOS = gaiaSdReplenishHMapper.queryStoreRequestsRecord(compadmForStoreDto);
        SdReplenishDExtandVO sdReplenishDExtandVO = new SdReplenishDExtandVO();
        if (CollectionUtils.isNotEmpty(sdReplenishHExtandVOS)) {
            sdReplenishDExtandVO.setClient(sdReplenishHExtandVOS.get(0).getClient());
            // 备注
            sdReplenishDExtandVO.setGsrdBz(sdReplenishHExtandVOS.get(0).getGsrdBz());
            // 请货日期
            sdReplenishDExtandVO.setGsrhDate(sdReplenishHExtandVOS.get(0).getGsrhDate());
            // 创建人
            sdReplenishDExtandVO.setGsrhDnByName(StringUtils.isBlank(sdReplenishHExtandVOS.get(0).getGsrhDnByName()) ? user.getClient() + "-" + user.getLoginName() : sdReplenishHExtandVOS.get(0).getGsrhDnByName());
            // 审核状态
            sdReplenishDExtandVO.setGsrhFlag(sdReplenishHExtandVOS.get(0).getGsrhFlag());
            // 审核类型
            sdReplenishDExtandVO.setGsrhPattern(sdReplenishHExtandVOS.get(0).getGsrhPattern());
            // 请货单号
            sdReplenishDExtandVO.setGsrhVoucherId(sdReplenishHExtandVOS.get(0).getGsrhVoucherId());
            // 请货门店
            sdReplenishDExtandVO.setStoName(sdReplenishHExtandVOS.get(0).getStoName());
            // 请货地点
            sdReplenishDExtandVO.setGsrhAddr(sdReplenishHExtandVOS.get(0).getGsrhAddr());
            sdReplenishDExtandVO.setPoId(sdReplenishHExtandVOS.get(0).getPoId());
            sdReplenishDExtandVO.setGsrhType(sdReplenishHExtandVOS.get(0).getGsrhType());
        }
        // 明细列表
        List<SdReplenishDExtandDTO> sdReplenishDExtandDTOS = gaiaSdReplenishHMapper.queryStoreRequestsDetailRecord(compadmForStoreDto);
        if (CollectionUtils.isNotEmpty(sdReplenishDExtandDTOS)) {
            sdReplenishDExtandDTOS.forEach(
                    item -> {
//                        if ("9".equals(sdReplenishDExtandVO.getGsrhType()) && ObjectUtils.isNotEmpty(item.getGsrdNeedQty()) && ObjectUtils.isNotEmpty(item.getGsrdDnQty())) {
//                            item.setGsrdNeedQty((new BigDecimal(item.getGsrdNeedQty()).subtract(item.getGsrdDnQty())).stripTrailingZeros().toString());
//                        }
//                        // 实际数量
//                        item.setRealNeedQty(item.getGsrdNeedQty());
                        // 成本价（MAT_ADD_AMT+ MAT_ADD_TAX）/ MAT_TOTAL_QTY; MAT_TOTAL_QTY为零0用MAT_MOV_PRICE*（1+税率比如0.13）
                        if (item.getMatTotalQty() != null && StringUtils.isNotBlank(String.valueOf(item.getMatTotalQty())) && Double.parseDouble(item.getMatTotalQty().toString()) != 0) {
                            if (item.getMatAddAmt() != null && item.getMatAddTax() != null
                                    && StringUtils.isNotBlank(String.valueOf(item.getMatAddAmt()))
                                    && StringUtils.isNotBlank(String.valueOf(item.getMatAddTax()))) {
                                item.setCostPrice(item.getMatAddAmt().add(item.getMatAddTax()).divide(item.getMatTotalQty(), 4, BigDecimal.ROUND_HALF_UP));
                            } else {
                                item.setCostPrice(BigDecimal.valueOf(0));
                            }
                        } else {
                            if (item.getMatMovPrice() != null && StringUtils.isNotBlank(String.valueOf(item.getMatMovPrice()))) {
                                if (item.getGsrdTaxRate() != null && StringUtils.isNotBlank(String.valueOf(item.getGsrdTaxRate())) && item.getGsrdTaxRate().contains("%")) {
                                    item.setCostPrice(item.getMatMovPrice().multiply(BigDecimal.valueOf((1 + Double.parseDouble(item.getGsrdTaxRate().split("%")[0]) * 0.01))));
                                }
                                if (item.getGsrdTaxRate() != null && StringUtils.isNotBlank(String.valueOf(item.getGsrdTaxRate())) && !item.getGsrdTaxRate().contains("%") && StringUtils.isNotBlank(item.getTaxCodeValue())) {
                                    item.setCostPrice(item.getMatMovPrice().multiply(BigDecimal.valueOf((1 + Double.parseDouble(item.getTaxCodeValue().split("%")[0]) * 0.01))));
                                }
                            } else {
                                item.setCostPrice(BigDecimal.valueOf(0));
                            }
                        }
                        // 成本金额
                        if (item.getCostPrice() != null && item.getRealNeedQty() != null
                                && StringUtils.isNotBlank(String.valueOf(item.getCostPrice())) && ObjectUtils.isNotEmpty(item.getRealNeedQty())) {
                            item.setCostTotal(item.getCostPrice().multiply(new BigDecimal(item.getRealNeedQty())));
                        } else {
                            item.setCostTotal(BigDecimal.valueOf(0));
                        }
                        // 零售金额
                        if (item.getPriceNormal() != null && item.getRealNeedQty() != null
                                && StringUtils.isNotBlank(String.valueOf(item.getPriceNormal())) && ObjectUtils.isNotEmpty(item.getRealNeedQty())) {
                            item.setTotalNormal(item.getPriceNormal().multiply(new BigDecimal(item.getRealNeedQty())));
                        } else {
                            item.setTotalNormal(BigDecimal.valueOf(0));
                        }
                    }
            );
        }
//        sdReplenishDExtandDTOS = sdReplenishDExtandDTOS.stream().filter(u -> StringUtils.isNotBlank(u.getRealNeedQty()) && Integer.parseInt(u.getRealNeedQty()) != 0).collect(Collectors.toList());
        sdReplenishDExtandVO.setSdReplenishDExtandDTOList(sdReplenishDExtandDTOS);
        return sdReplenishDExtandVO;
    }

    @Override
    public PageInfo<SdReplenishDExtandDTO> queryDetailRecord(CompadmForStoreDto compadmForStoreDto) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        compadmForStoreDto.setClient(user.getClient());
        if (compadmForStoreDto.getFlag() == null) {
            compadmForStoreDto.setFlag("");
        }
        PageHelper.startPage(compadmForStoreDto.getPageNum(), compadmForStoreDto.getPageSize());
        List<SdReplenishDExtandDTO> sdReplenishDExtandDTOS = gaiaSdReplenishHMapper.queryStoreRequestsDetailRecord(compadmForStoreDto);
        PageInfo<SdReplenishDExtandDTO> pageInfo = new PageInfo<SdReplenishDExtandDTO>(sdReplenishDExtandDTOS);
        if (CollectionUtils.isNotEmpty(pageInfo.getList())) {
            pageInfo.getList().forEach(
                    item -> {
//                        if ("9".equals(item.getGsrhType()) && ObjectUtils.isNotEmpty(item.getGsrdNeedQty()) && ObjectUtils.isNotEmpty(item.getGsrdDnQty())) {
//                            item.setGsrdNeedQty((new BigDecimal(item.getGsrdNeedQty()).subtract(item.getGsrdDnQty())).stripTrailingZeros().toString());
//                        }
//                        // 实际数量
//                        item.setRealNeedQty(item.getGsrdNeedQty());
                        // 成本价 （MAT_ADD_AMT+ MAT_ADD_TAX）/ MAT_TOTAL_QTY; MAT_TOTAL_QTY为零0用MAT_MOV_PRICE*（1+税率比如0.13）
                        if (item.getMatTotalQty() != null && StringUtils.isNotBlank(String.valueOf(item.getMatTotalQty())) && Double.parseDouble(item.getMatTotalQty().toString()) != 0) {
                            if (item.getMatAddAmt() != null && item.getMatAddTax() != null
                                    && StringUtils.isNotBlank(String.valueOf(item.getMatAddAmt()))
                                    && StringUtils.isNotBlank(String.valueOf(item.getMatAddTax()))) {
                                item.setCostPrice(item.getMatAddAmt().add(item.getMatAddTax()).divide(item.getMatTotalQty(), 4, BigDecimal.ROUND_HALF_UP));
                            } else {
                                item.setCostPrice(BigDecimal.valueOf(0));
                            }
                        } else {
                            if (item.getMatMovPrice() != null && StringUtils.isNotBlank(String.valueOf(item.getMatMovPrice()))) {
                                if (item.getGsrdTaxRate() != null && StringUtils.isNotBlank(String.valueOf(item.getGsrdTaxRate())) && item.getGsrdTaxRate().contains("%")) {
                                    item.setCostPrice(item.getMatMovPrice().multiply(BigDecimal.valueOf((1 + Double.parseDouble(item.getGsrdTaxRate().split("%")[0]) * 0.01))));
                                }
                                if (item.getGsrdTaxRate() != null && StringUtils.isNotBlank(String.valueOf(item.getGsrdTaxRate())) && !item.getGsrdTaxRate().contains("%") && StringUtils.isNotBlank(item.getTaxCodeValue())) {
                                    item.setCostPrice(item.getMatMovPrice().multiply(BigDecimal.valueOf((1 + Double.parseDouble(item.getTaxCodeValue().split("%")[0]) * 0.01))));
                                }
                            } else {
                                item.setCostPrice(BigDecimal.valueOf(0));
                            }
                        }
                        // 成本金额
                        if (item.getCostPrice() != null && item.getRealNeedQty() != null
                                && StringUtils.isNotBlank(String.valueOf(item.getCostPrice())) && ObjectUtils.isNotEmpty(item.getRealNeedQty())) {
                            item.setCostTotal(item.getCostPrice().multiply(new BigDecimal(item.getRealNeedQty())));
                        } else {
                            item.setCostTotal(BigDecimal.valueOf(0));
                        }
                        // 零售金额
                        if (item.getPriceNormal() != null && item.getRealNeedQty() != null
                                && StringUtils.isNotBlank(String.valueOf(item.getPriceNormal())) && ObjectUtils.isNotEmpty(item.getRealNeedQty())) {
                            item.setTotalNormal(item.getPriceNormal().multiply(new BigDecimal(item.getRealNeedQty())));
                        } else {
                            item.setTotalNormal(BigDecimal.valueOf(0));
                        }
                    }
            );
        }
        return pageInfo;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateFlag(List<CompadmForStoreDto> compadmForStoreDto) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        if (CollectionUtils.isNotEmpty(compadmForStoreDto)) {
            compadmForStoreDto.forEach(
                    item -> {
                        item.setClient(user.getClient());
                        item.setFlag("2");
                        item.setDnFlag("1");
                        item.setCreateBy(user.getClient());
                        item.setCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                        item.setCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                    }
            );
        }
        // 将GSRH_FLAG更新为2，且GSRH_DN_FLAG更新成1。
        return gaiaSdReplenishHMapper.updateFlag(compadmForStoreDto);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result examineStoreRequests(List<CompadmForStoreDto> list) {
        // 当前用户信息
        TokenUser user = feignService.getLoginInfo();
        List<GaiaSdReplenishH> gaiaSdReplenishHS = new ArrayList<>();
        List<SdReplenishDExtandDTO> gaiaSdReplenishDS = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(list)) {
            list.forEach(
                    item -> {
                        if (StringUtils.isNotBlank(item.getStoCode()) && StringUtils.isNotBlank(item.getVoucherId())) {
                            GaiaSdReplenishHKey key = new GaiaSdReplenishHKey();
                            key.setClient(user.getClient());
                            key.setGsrhBrId(item.getStoCode());
                            key.setGsrhVoucherId(item.getVoucherId());
                            GaiaSdReplenishH sdReplenishH = gaiaSdReplenishHMapper.selectByPrimaryKey(key);
                            if (sdReplenishH == null) {
                                return;
                            }
                            if (StringUtils.isNotBlank(sdReplenishH.getGsrhFlag()) && !"0".equalsIgnoreCase(sdReplenishH.getGsrhFlag())) {
                                return;
                            }
                            GaiaSdReplenishH gaiaSdReplenishH = new GaiaSdReplenishH();
                            gaiaSdReplenishH.setClient(user.getClient());
                            // 请货地点
                            gaiaSdReplenishH.setGsrhAddr(item.getGsrhAddr());
                            // 请货单号
                            gaiaSdReplenishH.setGsrhVoucherId(item.getVoucherId());
                            // 门店
                            gaiaSdReplenishH.setGsrhBrId(item.getStoCode());
                            gaiaSdReplenishHS.add(gaiaSdReplenishH);
                            if (CollectionUtils.isEmpty(item.getDetails())) {
                                CompadmForStoreDto compadmForStoreDto = new CompadmForStoreDto();
                                compadmForStoreDto.setClient(user.getClient());
                                compadmForStoreDto.setVoucherId(item.getVoucherId());
                                compadmForStoreDto.setStoCode(item.getStoCode());
                                if (compadmForStoreDto.getFlag() == null) {
                                    compadmForStoreDto.setFlag("");
                                }
                                List<SdReplenishDExtandDTO> details = gaiaSdReplenishHMapper.queryStoreRequestsDetailRecord(compadmForStoreDto);
//                                for (SdReplenishDExtandDTO dto : details) {
//                                    if ("9".equals(dto.getGsrhType()) && ObjectUtils.isNotEmpty(dto.getGsrdNeedQty()) && ObjectUtils.isNotEmpty(dto.getGsrdDnQty())) {
//                                        dto.setRealNeedQty((new BigDecimal(dto.getGsrdNeedQty()).subtract(dto.getGsrdDnQty())).stripTrailingZeros().toString());
//                                    } else {
//                                        dto.setRealNeedQty(dto.getGsrdNeedQty());
//                                    }
//                                }
//                                details = details.stream().filter(u -> StringUtils.isNotBlank(u.getRealNeedQty()) && Integer.parseInt(u.getRealNeedQty()) != 0).collect(Collectors.toList());
                                gaiaSdReplenishDS.addAll(details);
                            }
                        }
                        if (CollectionUtils.isNotEmpty(item.getDetails())) {
                            item.setDetails(item.getDetails().stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getGsrdBrId() + ";" + o.getGsrdVoucherId() + ";" + o.getGsrdSerial() + ";" + o.getGsrdProId()))), ArrayList::new)));
                            item.getDetails().forEach(
                                    detail -> {
                                        GaiaSdReplenishH gaiaSdReplenishH = new GaiaSdReplenishH();
                                        gaiaSdReplenishH.setClient(user.getClient());
                                        gaiaSdReplenishH.setGsrhVoucherId(detail.getGsrdVoucherId());
                                        gaiaSdReplenishH.setGsrhAddr(detail.getGsrhAddr());
                                        gaiaSdReplenishH.setGsrhBrId(detail.getGsrdBrId());
                                        gaiaSdReplenishHS.add(gaiaSdReplenishH);
                                        CompadmForStoreDto compadmForStoreDto = new CompadmForStoreDto();
                                        compadmForStoreDto.setClient(user.getClient());
                                        compadmForStoreDto.setVoucherId(detail.getGsrdVoucherId());
                                        compadmForStoreDto.setStoCode(detail.getGsrdBrId());
                                        if (compadmForStoreDto.getFlag() == null) {
                                            compadmForStoreDto.setFlag("");
                                        }
                                        List<SdReplenishDExtandDTO> details = gaiaSdReplenishHMapper.queryStoreRequestsDetailRecord(compadmForStoreDto);
                                        for (SdReplenishDExtandDTO dto : details) {
                                            if (dto.getGsrdProId().equals(detail.getGsrdProId()) && dto.getGsrdSerial().equals(detail.getGsrdSerial())) {
                                                dto.setRealNeedQty(detail.getRealNeedQty());
                                            }
                                        }
                                        gaiaSdReplenishDS.addAll(details);
                                    }
                            );
                        }
                    }
            );
            List<GaiaSdReplenishH> distinctClass = gaiaSdReplenishHS.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getGsrhBrId() + ";" + o.getGsrhVoucherId()))), ArrayList::new));
            List<CompadmForStoreDto> dtoList = new ArrayList<>();
            List<SdReplenishDExtandDTO> realNeedList = gaiaSdReplenishDS.stream().filter(sdReplenishDExtandDTO -> StringUtils.isNotBlank(sdReplenishDExtandDTO.getRealNeedQty())).collect(Collectors.toList());
            List<SdReplenishDExtandDTO> distinctDetailClass =
                    realNeedList.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getGsrdBrId() + ";" + o.getGsrdVoucherId() + ";" + o.getGsrdSerial() + ";" + o.getGsrdProId()))), ArrayList::new));
            if (CollectionUtils.isNotEmpty(distinctClass)) {
                distinctClass.forEach(
                        item -> {
                            CompadmForStoreDto dto = new CompadmForStoreDto();
                            dto.setClient(user.getClient());
                            dto.setFlag("1");
                            dto.setStoCode(item.getGsrhBrId());
                            dto.setVoucherId(item.getGsrhVoucherId());
                            dto.setCreateBy(user.getClient());
                            dto.setCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                            dto.setCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                            dtoList.add(dto);

                            GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
                            gaiaPoHeader.setClient(item.getClient());
                            // 供应商
                            gaiaPoHeader.setPoSupplierId(gaiaSdReplenishHMapper.selectSupplier(item.getClient(), item.getGsrhAddr()));
                            // 订单类型
                            gaiaPoHeader.setPoType("Z007");
                            String newPoId = new CommonUtils().getPoId(item.getClient(), "Z007", String.valueOf(LocalDateTime.now().getYear()), gaiaPoHeaderMapper, redisClient);
                            Long poId = Long.parseLong(newPoId.split("CD")[1]);
                            while (redisClient.exists(PO_ID_KEY + poId)) {
                                poId++;
                            }
                            redisClient.set(PO_ID_KEY + poId, poId.toString(), 120);
                            // 采购订单号
                            gaiaPoHeader.setPoId(CommonEnum.PurchaseClass.PURCHASECD.getName() + poId.toString());
                            // 门店号
                            gaiaPoHeader.setPoDeliveryTypeStore(item.getGsrhBrId());
                            // 地点
                            if (StringUtils.isBlank(item.getGsrhAddr())) {
                                CompadmForStoreDto compadmForStoreDto = new CompadmForStoreDto();
                                compadmForStoreDto.setClient(item.getClient());
                                compadmForStoreDto.setVoucherId(item.getGsrhVoucherId());
                                compadmForStoreDto.setStoCode(item.getGsrhBrId());
                                List<SdReplenishHExtandVO> result = gaiaSdReplenishHMapper.queryStoreRequestsRecord(compadmForStoreDto);
                                if (result.size() > 0) {
                                    item.setGsrhAddr(result.get(0).getGsrhAddr());
                                }
                            }
                            // 采购主体
                            gaiaPoHeader.setPoCompanyCode(item.getGsrhAddr());
                            // 主体类型
                            gaiaPoHeader.setPoSubjectType("1");
                            // 凭证日期
                            gaiaPoHeader.setPoDate(DateUtils.getCurrentDateStrYYMMDD());
                            // 创建人
                            gaiaPoHeader.setPoCreateBy(user.getUserId());
                            // 创建日期
                            gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                            // 创建时间
                            gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
                            // 请货单号
                            gaiaPoHeader.setPoReqId(item.getGsrhVoucherId());
                            // 审核状态
                            gaiaPoHeader.setPoApproveStatus("2");
                            gaiaPoHeaderMapper.insert(gaiaPoHeader);
                            if (CollectionUtils.isNotEmpty(distinctDetailClass)) {
                                distinctDetailClass.forEach(
                                        itemDetail -> {
                                            if (item.getClient().equalsIgnoreCase(itemDetail.getClient()) &&
                                                    item.getGsrhVoucherId().equalsIgnoreCase(itemDetail.getGsrdVoucherId()) &&
                                                    item.getGsrhBrId().equalsIgnoreCase(itemDetail.getGsrdBrId())) {
                                                GaiaPoItem gaiaPoItem = new GaiaPoItem();
                                                gaiaPoItem.setClient(itemDetail.getClient());
                                                // 采购单号
                                                gaiaPoItem.setPoId(gaiaPoHeader.getPoId());
                                                // 行号
                                                gaiaPoItem.setPoLineNo(itemDetail.getGsrdSerial());
                                                // 商品编码
                                                gaiaPoItem.setPoProCode(itemDetail.getGsrdProId());
                                                // 数量
                                                gaiaPoItem.setPoQty(new BigDecimal(itemDetail.getRealNeedQty()));
                                                // 单位
                                                gaiaPoItem.setPoUnit(itemDetail.getProUnit());
                                                // 价格
                                                if (itemDetail.getCostPrice() == null || StringUtils.isBlank(String.valueOf(itemDetail.getCostPrice()))) {
                                                    // 成本价 （MAT_ADD_AMT+ MAT_ADD_TAX）/ MAT_TOTAL_QTY; MAT_TOTAL_QTY为零0用MAT_MOV_PRICE*（1+税率比如0.13）
                                                    if (itemDetail.getMatTotalQty() != null && StringUtils.isNotBlank(String.valueOf(itemDetail.getMatTotalQty())) && Double.parseDouble(String.valueOf(itemDetail.getMatTotalQty())) != 0) {
                                                        if (itemDetail.getMatAddAmt() != null && itemDetail.getMatAddTax() != null
                                                                && StringUtils.isNotBlank(String.valueOf(itemDetail.getMatAddAmt()))
                                                                && StringUtils.isNotBlank(String.valueOf(itemDetail.getMatAddTax()))) {
                                                            gaiaPoItem.setPoPrice(itemDetail.getMatAddAmt().add(itemDetail.getMatAddTax()).divide(itemDetail.getMatTotalQty(), 4, BigDecimal.ROUND_HALF_UP));
                                                        } else {
                                                            gaiaPoItem.setPoPrice(BigDecimal.valueOf(0));
                                                        }
                                                    } else {
                                                        if (itemDetail.getMatMovPrice() != null && StringUtils.isNotBlank(String.valueOf(itemDetail.getMatMovPrice()))) {
                                                            if (itemDetail.getGsrdTaxRate() != null && StringUtils.isNotBlank(String.valueOf(itemDetail.getGsrdTaxRate())) && itemDetail.getGsrdTaxRate().contains("%")) {
                                                                gaiaPoItem.setPoPrice(itemDetail.getMatMovPrice().multiply(BigDecimal.valueOf((1 + Double.parseDouble(itemDetail.getGsrdTaxRate().split("%")[0]) * 0.01))));
                                                            }
                                                            if (itemDetail.getGsrdTaxRate() != null && StringUtils.isNotBlank(String.valueOf(itemDetail.getGsrdTaxRate())) && !itemDetail.getGsrdTaxRate().contains("%") && StringUtils.isNotBlank(itemDetail.getTaxCodeValue())) {
                                                                gaiaPoItem.setPoPrice(itemDetail.getMatMovPrice().multiply(BigDecimal.valueOf((1 + Double.parseDouble(itemDetail.getTaxCodeValue().split("%")[0]) * 0.01))));
                                                            }
                                                        } else {
                                                            gaiaPoItem.setPoPrice(BigDecimal.valueOf(0));
                                                        }
                                                    }
                                                } else {
                                                    gaiaPoItem.setPoPrice(itemDetail.getCostPrice());
                                                }
                                                if (ObjectUtils.isNotEmpty(gaiaPoItem.getPoPrice()) && ObjectUtils.isNotEmpty(gaiaPoItem.getPoQty())) {
                                                    // 成本金额 -> 订单行金额
                                                    gaiaPoItem.setPoLineAmt(gaiaPoItem.getPoPrice().multiply(gaiaPoItem.getPoQty()));
                                                } else {
                                                    gaiaPoItem.setPoLineAmt(BigDecimal.valueOf(0));
                                                }
                                                // 地点
                                                gaiaPoItem.setPoSiteCode(itemDetail.getGsrhAddr());
                                                // 库存地点
                                                gaiaPoItem.setPoLocationCode("1000");
                                                // 税率
                                                gaiaPoItem.setPoRate(itemDetail.getProInputTax());
                                                // 交货日期
                                                gaiaPoItem.setPoDeliveryDate(DateUtils.getCurrentDateStrYYMMDD());
                                                // 订单号
                                                gaiaPoItem.setPoPrePoid(itemDetail.getGsrdVoucherId());
                                                // 订单行号
                                                gaiaPoItem.setPoPrePoitem(itemDetail.getGsrdSerial());
                                                // 批号
                                                gaiaPoItem.setPoBatchNo(itemDetail.getGsrdBatchNo());
                                                gaiaPoItemMapper.insert(gaiaPoItem);
                                            }
                                        }
                                );
                            }
                        }
                );
                gaiaSdReplenishHMapper.updateFlag(dtoList);
            }
        }
        return ResultUtil.success();
    }
}
