package com.gov.purchase.module.base.service.impl.dcImport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaFranchiseeMapper;
import com.gov.purchase.module.base.dto.BatSearchValBasic;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.base.dto.excel.StoreExport;
import com.gov.purchase.module.store.dto.GaiaDcDataListDto;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.18
 */
@Service
public class DcSearch {

    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Resource
    CosUtils cosUtils;
    /**
     * DC查询
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectDcBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initDcWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<GaiaDcDataListDto> batchList = gaiaDcDataMapper.selectDcBatch(map);
        PageInfo<GaiaDcDataListDto> pageInfo = new PageInfo<>(batchList);

        return ResultUtil.success(pageInfo);
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initDcWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        // 加盟商
        SearchValBasic client = map.get("a");
        if (client != null) {

            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(client.getSearchVal()) || CollectionUtils.isNotEmpty(client.getMoreVal())) {
                list.addAll(gaiaFranchiseeMapper.getFrancName(client));
                if (StringUtils.isNotBlank(client.getSearchVal())) {
                    list.add(client.getSearchVal());
                } else {
                    list.addAll(client.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                client.setSearchVal(null);
            }
            client.setMoreVal(list);
        }

        // DC状态
        SearchValBasic dc_status = map.get("d");
        if (dc_status != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(dc_status.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.DC_STATUS, dc_status.getSearchVal()));
                list.add(dc_status.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(dc_status.getMoreVal())) {
                for (String item : dc_status.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.DC_STATUS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                dc_status.setSearchVal(null);
            }
            dc_status.setMoreVal(list);
        }
    }

    /**
     * dc导出
     * @param batSearchValBasic
     * @return
     */
    public Result exportDcBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initDcWhere(map);
            List<GaiaDcDataListDto> batchList = gaiaDcDataMapper.selectDcBatch(map);
            // 导出数据
            List<List<String>> basicListc = new ArrayList<>();
            initData(batchList, basicListc, null);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(basicListc);
                    }},
                    new ArrayList<String>() {{
                        add("DC主数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param basicListc
     * @param businessList
     */
    private void initData(List<GaiaDcDataListDto> batchList, List<List<String>> basicListc, List<List<String>> businessList) {
        for (GaiaDcDataListDto gaiaDcDataListDto : batchList) {
            if (basicListc != null) {
                List<String> basicData = new ArrayList<>();
                //加盟商
                basicData.add(gaiaDcDataListDto.getClient());
                //DC编码
                basicData.add(gaiaDcDataListDto.getDcCode());
                //DC名称
                basicData.add(gaiaDcDataListDto.getDcName());
                //助记码
                basicData.add(gaiaDcDataListDto.getDcPym());
                //DC简称
                basicData.add(gaiaDcDataListDto.getDcShortName());
                //DC地址
                basicData.add(gaiaDcDataListDto.getDcAdd());
                //DC电话
                basicData.add(gaiaDcDataListDto.getDcTel());
                //DC状态
                String dc_status = gaiaDcDataListDto.getDcStatus();
                initDictionaryStaticData(basicData, dc_status, CommonEnum.DictionaryStaticData.DC_STATUS);
                //虚拟仓标记
                String dc_invent = gaiaDcDataListDto.getDcInvent();
                initDictionaryStaticData(basicData, dc_invent, CommonEnum.DictionaryStaticData.NO_YES);
                //是否有批发资质
                String dc_wholesale = gaiaDcDataListDto.getDcWholesale();
                initDictionaryStaticData(basicData, dc_wholesale, CommonEnum.DictionaryStaticData.NO_YES);
                //连锁总部
                basicData.add(gaiaDcDataListDto.getDcChainHead());
                //配送平均天数
                basicData.add(gaiaDcDataListDto.getDcDeliveryDays());
                basicListc.add(basicData);
            }
        }
    }

    /**
     * 查询数据静态字典数据处理
     * @param list
     * @param value
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }

    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "加盟商",
            "DC编码",
            "DC名称",
            "助记码",
            "DC简称",
            "DC地址",
            "DC电话",
            "DC状态",
            "虚拟仓标记",
            "是否有批发资质",
            "连锁总部",
            "配送平均天数"
    };

//DC	5	加盟商	2	加盟商	a
//				DC编码	b
//				DC名称	c
//				DC状态	d
//				连锁总部	e
}
