package com.gov.purchase.module.base.dto;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.15
 */
@Data
public class SearchValBasic {
    private String searchVal;
    private List<String> moreVal;
}
