package com.gov.purchase.module.customer.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户首营列表传入参数")
public class CustomerGspInfoListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "客户编号", name = "cusCode")
    String cusCode;

    @ApiModelProperty(value = "客户编号（企业）", name = "cusSelfCode")
    String cusSelfCode;

    @ApiModelProperty(value = "客户名称", name = "cusName")
    String cusName;

    @ApiModelProperty(value = "统一社会信用代码（营业执照编号）", name = "cusCreditCode")
    String cusCreditCode;

    @ApiModelProperty(value = "首营日期（YYYY-MM-DD）", name = "cusGspDate")
    String cusGspDate;

    @ApiModelProperty(value = "地点", name = "proSite")
    String proSite;


}
