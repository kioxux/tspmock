package com.gov.purchase.module.replenishment.dto;

import lombok.Data;

import java.util.List;

/**
 * @description: 批次货位
 * @author: yzf
 * @create: 2022-01-06 17:02
 */
@Data
public class StoreNeedHwAndBatchDTO {

    /**
     * 配送中心
     */
    private String gsrhAddr;

    /**
     * 生产编码
     */
    private String proSelfCode;

    /**
     * 委托配送中心
     */
    private String dcWtdc;
}
