package com.gov.purchase.module.purchase.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaFicoInvoice;
import com.gov.purchase.entity.GaiaInvoice;
import com.gov.purchase.module.purchase.dto.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface InvoicePayingService {

    /**
     * 开票查询
     */
    List<InvoicePayingDTO> listInvoice(ListInvoiceRequestDTO listInvoiceRequestDTO);

    /**
     * 开票保存
     */
    Result saveListInvoice(List<InvoiceDTO> listDto);

    /**
     * 付款查询
     */
    List<ListPayingResponseDTO> listPaying(ListPayingRequestDTO dto);

    /**
     * 付款保存
     */
    SavePayingResponseDTO savePaying(SavePayingRequestDTO dto);

    /**
     * 支付回调
     */
    void payResult(HttpServletRequest httpServletRequest);

    /**
     * 付款状态查询
     */
    GetPayStatusResponseDTO getPayStatus(String ficoId, String client);

    /**
     * 发票付款成功查询
     */
    PageInfo<GaiaFicoInvoice> listInvoicePayingSuccess(ListInvoicePayingRequestDTO dto);

    /**
     * 发票pdf下载
     *
     * @param url
     * @return
     */
    String downloadInvoice(String url);

    /**
     * APP门店付款
     *
     * @param dto
     * @return
     */
    Result appPay(SavePayingRequestDTO dto) throws Exception;

    /**
     * APP支付回调
     *
     * @param httpServletRequest
     */
    void appPayResult(HttpServletRequest httpServletRequest);

    /**
     * @return
     */
    Result payPrice();

    /**
     * 支付回调事务
     */
    void payCallback() throws Exception;

    /**
     * 生成发票
     */
    void initInvoice() throws Exception;

    /**
     * 发票下载
     *
     * @param key
     */
    void download(HttpServletResponse response, String key) throws Exception;

    /**
     * 开票
     *
     * @param gaiaFicoInvoice
     */
    void invoice(GaiaFicoInvoice gaiaFicoInvoice);

    /**
     * 短信购买下单
     *
     * @param gsrrRechargeCount
     * @param gsrrRechargeAmt
     * @return
     */
    Result buySms(Integer gsrrRechargeCount, Integer gsrrRechargeAmt);
}
