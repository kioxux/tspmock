package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 采购计划明细
 * @author: yzf
 * @create: 2021-12-02 13:12
 */

@Data
public class PurchasePlanReviewDetailVo extends GaiaProductBusiness {

    private String client;
    /**
     * 订单号
     */
    private String poId;

    /**
     * 商品编码
     */
    private String poProCode;

    /**
     * 数量
     */
    private BigDecimal poQty;

    /**
     * 价格
     */
    private BigDecimal poPrice;

    /**
     * 采购主体名称
     */
    private BigDecimal poLineAmt;

    /**
     * 仓库库存
     */
    private BigDecimal dcStockTotal;

    /**
     * 在途量
     */
    private BigDecimal trafficCount;

    /**
     * 最近一次进货日期
     */
    private String lastPurchaseDate;

    /**
     * 最后进货供应商自编码
     */
    private String lastSupplierId;

    /**
     * 最后进货供应商
     */
    private String lastSupplier;

    /**
     * 最后进价
     */
    private String lastPurchasePrice;

    /**
     * 30天门店销售量
     */
    private BigDecimal daysSalesCount;

    /**
     * 连锁总部
     */
    private String dcChainHead;
}
