package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "门店铺货列表请求参数")
public class BatchStockRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "配送中心", name = "stoChainHead", required = true)
    @NotBlank(message = "配送中心不能为空")
    private String stoChainHead;

    @ApiModelProperty(value = "商品编码集合", name = "proSelfCode")
    private List<String> proSelfCode;

}
