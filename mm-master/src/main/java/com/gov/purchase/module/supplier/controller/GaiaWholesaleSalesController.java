package com.gov.purchase.module.supplier.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaWholesaleSalesman;
import com.gov.purchase.module.supplier.service.GaiaWholesaleSalesmanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@Api(tags = "供应商优化")
@RestController
@RequestMapping("sales")
public class GaiaWholesaleSalesController {

    @Resource
    private GaiaWholesaleSalesmanService gaiaWholesaleSalesmanService;

    @Log("销售员下拉框")
    @ApiOperation("销售员下拉框")
    @PostMapping("getSalesPersonList")
    public Result getSalesPersonList() {
        return ResultUtil.success(gaiaWholesaleSalesmanService.getSalesList());
    }

    @Log("业务员下拉框")
    @ApiOperation("业务员下拉框")
    @PostMapping("getSalesManList")
    public Result getSalesManList(@RequestJson(value = "supSite", required = false) String supSite) {
        return ResultUtil.success(gaiaWholesaleSalesmanService.getSalesManList(supSite));
    }

    @Log("销售员维护列表")
    @ApiOperation("销售员维护列表")
    @PostMapping("getWholesaleSalesList")
    public Result getWholesaleSalesList(@RequestJson("pageSize") Integer pageSize,
                                        @RequestJson("pageNum") Integer pageNum,
                                        @RequestJson(value = "userId", required = false) String userId,
                                        @RequestJson(value = "salesManId", required = false) String salesManId) {
        return ResultUtil.success(gaiaWholesaleSalesmanService.getWholesaleSalesList(pageSize, pageNum, userId, salesManId));
    }

    @Log("销售员业务员关系删除")
    @ApiOperation("销售员业务员关系删除")
    @PostMapping("deleteWholesaleSales/{id}")
    public Result deleteWholesaleSales(@PathVariable Long id) {
        return gaiaWholesaleSalesmanService.deleteWholesaleSales(id);
    }

    @Log("销售员业务员关系保存")
    @ApiOperation("销售员业务员关系保存")
    @PostMapping("saveWholesaleSales")
    public Result saveWholesaleSales(@RequestBody GaiaWholesaleSalesman vo) {
        return gaiaWholesaleSalesmanService.saveWholesaleSales(vo);
    }
}
