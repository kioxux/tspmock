package com.gov.purchase.module.base.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.base.dto.BatSearchValBasic;
import com.gov.purchase.module.base.service.BatchService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.20
 */
@Api(tags = "数据批量操作")
@RestController
@RequestMapping("batch")
public class BatchController {

    @Resource
    BatchService batchService;

    /**
     * 批量导入列表
     *
     * @param bulDateType     数据类型
     * @param bulUpdateType   操作类型
     * @param bulUpdateStatus 操作状态
     * @return
     */
    @Log("批量导入列表")
    @ApiOperation("批量导入列表")
    @PostMapping("queryBatchList")
    public Result queryBatchList(
            @RequestJson(value = "pageSize") Integer pageSize,
            @RequestJson(value = "pageNum") Integer pageNum,
            @RequestJson(value = "bulDateType", required = false) String bulDateType,
            @RequestJson(value = "bulUpdateType", required = false) String bulUpdateType,
            @RequestJson(value = "bulUpdateStatus", required = false) String bulUpdateStatus) {
        // 返回批量导入日志
        return batchService.queryBatchList(pageSize, pageNum, bulDateType, bulUpdateType, bulUpdateStatus);
    }

    /**
     * 上传Exel
     * type 1:药品库导入
     *
     * @return
     */
    @Log("上传Exel")
    @ApiOperation("上传Exel")
    @PostMapping("uploadBatch/{type}")
    public Result uploadBatch(@PathVariable String type, @RequestJson("path") String path) {
        return batchService.uploadBatch(type, path);
    }

    /**
     * 数据导入
     *
     * @param bulUpdateCode
     * @return
     */
    @Log("数据导入")
    @ApiOperation("数据导入")
    @PostMapping("importBatch")
    public Result importBatch(@RequestJson("bulUpdateCode") String bulUpdateCode) {
        return batchService.importBatch(bulUpdateCode);
    }

    /**
     * 批量查询
     * @param batSearchValBasic
     * @return
     */
    @Log("批量查询")
    @ApiOperation("批量查询")
    @PostMapping("searchBatch")
    public Result searchBatch(@RequestBody BatSearchValBasic batSearchValBasic) {
        return batchService.searchBatch(batSearchValBasic);
    }

    /**
     * 导出
     *
     * @param batSearchValBasic
     * @return
     */
    @Log("导出")
    @ApiOperation("导出")
    @PostMapping("exportBatch")
    public Result exportBatch(@RequestBody BatSearchValBasic batSearchValBasic) {
        return batchService.exportBatch(batSearchValBasic);
    }
}

