package com.gov.purchase.module.replenishment.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaSourceList;
import com.gov.purchase.module.replenishment.dto.*;

import java.util.List;

public interface SourceService {

    /**
     * 货源清单创建、修改列表
     *
     * @param dto
     * @return
     */
    List<SourceCreateListResponseDto> querySourceCreateList(SourceCreateListRequestDto dto);

    /**
     * 货源清单创建、修改
     *
     * @param list
     * @return
     */
    Result saveSourceList(List<SaveSourceRequestDto> list);

    /**
     * 货源清单查看列表
     *
     * @param dto
     * @return
     */
    PageInfo<SourceListResponseDto> querySourceList(SourceListRequestDto dto);

    /**
     * 货源清单明细
     *
     * @param dto
     * @return
     */
    SourceDetailResponseDto querySourceDetail(SourceDetailRequestDto dto);

    /**
     * 货源清单批量新增
     *
     * @param sourceList
     * @return
     */
    Result batchSaveSourceList(List<SourceRequestNewDTO> sourceList);

    /**
     * 货源清单批量更新
     *
     * @param sourceList
     * @return
     */
    Result batchUpdateSourceList(List<GaiaSourceList> sourceList);

    /**
     * 导出货源清单
     * @param dto
     * @return
     */
    Result exportSourceList(SourceListExportDto dto);
}
