package com.gov.purchase.module.base.service;

import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.module.base.dto.feign.ApprovalInfo;

public interface ApprovalService {

    /**
     * 审批结果处理
     * @param info
     * @return
     */
    FeignResult approvalResultProcess(ApprovalInfo info);
}
