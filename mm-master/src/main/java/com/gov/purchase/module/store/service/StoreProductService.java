package com.gov.purchase.module.store.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.qa.dto.QueryProductBusinessResponseDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;

import java.util.List;

public interface StoreProductService {

    /**
     * 根据参数来查询数据
     *
     * @return
     */
    List<QueryProductBusinessResponseDto> queryList(String stoCode, String param);


    Result saveList(List<GaiaPriceAdjustDto> gaiaPriceAdjustList);

    Result updateList();


}
