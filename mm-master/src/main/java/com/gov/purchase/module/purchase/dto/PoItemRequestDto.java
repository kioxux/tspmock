package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "采购订单明细传入参数")
public class PoItemRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 采购凭证号
     */
    @ApiModelProperty(value = "采购凭证号", name = "poId", required = true)
    @NotBlank(message = "采购凭证号不能为空")
    private String poId;

}