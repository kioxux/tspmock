package com.gov.purchase.module.store.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaPriceTag;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListExportRequestDto;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListReponseDto;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequest;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequestDto;

import java.util.List;

public interface SalesTagService {

    PageInfo<SalesTagListReponseDto> querySalesTagList(SalesTagListRequestDto dto);

    List<SalesTagListReponseDto> querySalesTagExportList(SalesTagListExportRequestDto dto);

    /**
     * 标签查询，非套打
     *
     * @param salesTagListRequest
     * @return
     */
    Result querySalesTagList2(SalesTagListRequest salesTagListRequest);

    /**
     * 非套打打印数据保存
     *
     * @param salesTagListReponseDtoList
     * @return
     */
    Result printTag(List<SalesTagListReponseDto> salesTagListReponseDtoList);

    /**
     * 非套打打印数据获取
     *
     * @param key
     * @return
     */
    Result getPrintTag(String key);

    /**
     * 模板保存
     *
     * @param gaiaPriceTag
     */
    void savePriceTag(GaiaPriceTag gaiaPriceTag);

    Result selectPriceTagList(String gptName, Integer gptType, Integer pageNum, Integer pageSize);

    void editTag(GaiaPriceTag gaiaPriceTag);

    void deleteTag(Integer id);

    Result selectTag(Integer id);

    Result selectPriceTag(Integer gptType);

    /**
     * 销售标签导出
     *
     * @param dto
     * @return
     */
    Result salesTagExprot(SalesTagListRequestDto dto);
}
