package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierBusiness;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GaiaSupplierBusinessDTO extends GaiaSupplierBusiness {

    /**
     * 唯一标识（前端使用）
     */
    private String id;

    /**
     * 供应商名
     */
    private String francName;

    /**
     * 地点名
     */
    private String siteName;

    /**
     * 质量负责
     */
    private String zlfzrName;

    /**
     * 付款条件
     */
    private String PayTermName;
}
