package com.gov.purchase.module.base.service.impl.productImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBasic;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryParams;
import com.gov.purchase.module.base.dto.excel.Product;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.SpringUtil;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
@Service
public class ProductImportEditImpl extends ProductImport {

    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;


    /**
     * 业务验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.errorListProduct((LinkedHashMap<Integer, Product>) dataMap);
    }

    /**
     * 主数据表验证
     *
     * @param product
     * @param errorList
     * @param key
     */
    @Override
    protected void baseCheck(Product product, List<String> errorList, int key) {

        // 商品编码 存在验证
        GaiaProductBasic gaiaProductBasic = gaiaProductBasicMapper.selectByPrimaryKey(product.getProCode());
        if (gaiaProductBasic == null) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
            return;
        }

        // 细分剂型
        if (super.isNotBlank(product.getProPartform())) {
            if (StringUtils.isBlank(product.getProForm()) && StringUtils.isBlank(gaiaProductBasic.getProForm())) {
                errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "剂型"));
            } else {
                // 参数
                DictionaryParams dictionaryParams = new DictionaryParams();
                // key
                dictionaryParams.setKey(CommonEnum.DictionaryData.PRO_PARTFORM.getCode());
                // 参数
                Map<String, String> map = new HashMap<>();
                map.put("PRO_FORM", StringUtils.isNotBlank(product.getProForm()) ? product.getProForm() : gaiaProductBasic.getProForm());
                dictionaryParams.setExactParams(map);
                Result result = dictionaryService.getDictionary(dictionaryParams);
                if (result.getData() == null || CollectionUtils.isEmpty((Collection<?>) result.getData())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "细分剂型"));
                } else {
                    Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue((List<Dictionary>) result.getData(), product.getProPartform());
                    if (dictionary == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "细分剂型"));
                    }
                }
            }
        } else {
            if (StringUtils.isNotBlank(product.getProForm()) && StringUtils.isNotBlank(gaiaProductBasic.getProForm())) {
                if (!product.getProForm().equals(gaiaProductBasic.getProForm())) {
                    product.setProPartform(ImportData.NULLVALUE);
                }
            }
        }

        // 药品、器械：商品分类开头为1、2、3、5
        List<String> proClassList = new ArrayList<String>() {{
            add("1");
            add("2");
            add("3");
            add("5");
        }};

        // 药品、器械：商品分类开头为1、2、3、5
        // 批准文号 批准文号批准日期 保质期 不为空
        if (StringUtils.isNotBlank(product.getProClass())) {
            if (proClassList.contains(StringUtils.left(product.getProClass(), 1))) {
                // 批准文号
                if (StringUtils.isBlank(gaiaProductBasic.getProRegisterNo()) && !super.isNotBlank(product.getProRegisterNo())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "批准文号"));
                } else if (StringUtils.isNotBlank(gaiaProductBasic.getProRegisterNo()) && ImportData.NULLVALUE.equals(product.getProRegisterNo())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "批准文号"));
                }

                // 批准文号批准日期
                if (StringUtils.isBlank(gaiaProductBasic.getProRegisterDate()) && !super.isNotBlank(product.getProRegisterDate())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "批准文号批准日期"));
                } else if (StringUtils.isNotBlank(gaiaProductBasic.getProRegisterDate()) && ImportData.NULLVALUE.equals(product.getProRegisterDate())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "批准文号批准日期"));
                }
                // 保质期
                if (StringUtils.isBlank(gaiaProductBasic.getProLife()) && !super.isNotBlank(product.getProLife())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "保质期"));
                } else if (StringUtils.isNotBlank(gaiaProductBasic.getProLife()) && ImportData.NULLVALUE.equals(product.getProLife())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "保质期"));
                }
            }
            // 中包装量 中药饮片必填
            // 中药饮片：商品分类开头为3
            if (product.getProClass().startsWith("3")) {
                if (StringUtils.isBlank(gaiaProductBasic.getProMidPackage()) && !super.isNotBlank(product.getProMidPackage())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中包装量"));
                } else if (StringUtils.isNotBlank(gaiaProductBasic.getProMidPackage()) && ImportData.NULLVALUE.equals(product.getProMidPackage())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中包装量"));
                }
            }
        } else {
            if (proClassList.contains(StringUtils.left(gaiaProductBasic.getProClass(), 1))) {
                // 批准文号
                if (ImportData.NULLVALUE.equals(product.getProRegisterNo())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "批准文号"));
                }
                // 批准文号批准日期
                if (ImportData.NULLVALUE.equals(product.getProRegisterDate())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "批准文号批准日期"));
                }
                // 保质期
                if (ImportData.NULLVALUE.equals(product.getProLife())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "保质期"));
                }
            }
            // 中包装量 中药饮片必填
            // 中药饮片：商品分类开头为3
            if (gaiaProductBasic.getProClass().startsWith("3")) {
                if (ImportData.NULLVALUE.equals(product.getProMidPackage())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "中包装量"));
                }
            }
        }
    }

    /**
     * 业务数据验证
     *
     * @param product
     * @param errorList
     * @param key
     */
    @Override
    protected void businessCheck(Product product, List<String> errorList, int key) {
        // 加盟商 商品自编码 存在验证
        List<GaiaProductBusiness> businessList = gaiaProductBusinessMapper.selfOnlyCheck(product.getClient(), product.getProSelfCode(), product.getProSite());
        if (CollectionUtils.isEmpty(businessList)) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
            return;
        }

        //主数据
        GaiaProductBasic gaiaProductBasic = gaiaProductBasicMapper.selectByPrimaryKey(product.getProCode());

        //业务数据
        GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
        gaiaProductBusinessKey.setClient(product.getClient());
        gaiaProductBusinessKey.setProSite(product.getProSite());
        gaiaProductBusinessKey.setProSelfCode(product.getProSelfCode());

        GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
        if (null == gaiaProductBusiness) {
            errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
            return;
        }

        // 商品分类开头为3
        // 中药规格 中药批准文号 中药生产企业代码 中药产地 不为空
        if (StringUtils.isNotBlank(product.getProClass())) {
            if (product.getProClass().startsWith("3")) {
                // 中药规格
                if (StringUtils.isBlank(gaiaProductBusiness.getProTcmSpecs()) && !super.isNotBlank(product.getProTcmSpecs())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药规格"));
                } else if (StringUtils.isNotBlank(gaiaProductBusiness.getProTcmSpecs()) && ImportData.NULLVALUE.equals(product.getProTcmSpecs())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药规格"));
                }
                // 中药批准文号
                if (StringUtils.isBlank(gaiaProductBusiness.getProTcmRegisterNo()) && !super.isNotBlank(product.getProTcmRegisterNo())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药批准文号"));
                } else if (StringUtils.isNotBlank(gaiaProductBusiness.getProTcmRegisterNo()) && ImportData.NULLVALUE.equals(product.getProTcmRegisterNo())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药批准文号"));
                }
                // 中药生产企业代码
                if (StringUtils.isBlank(gaiaProductBusiness.getProTcmFactoryCode()) && !super.isNotBlank(product.getProTcmFactoryCode())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药生产企业代码"));
                } else if (StringUtils.isNotBlank(gaiaProductBusiness.getProTcmFactoryCode()) && ImportData.NULLVALUE.equals(product.getProTcmFactoryCode())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药生产企业代码"));
                }
                // 中药产地
                if (StringUtils.isBlank(gaiaProductBusiness.getProTcmPlace()) && !super.isNotBlank(product.getProTcmPlace())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药产地"));
                } else if (StringUtils.isNotBlank(gaiaProductBusiness.getProTcmPlace()) && ImportData.NULLVALUE.equals(product.getProTcmPlace())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "中药产地"));
                }
            }
        } else {
            if (gaiaProductBasic.getProClass().startsWith("3")) {
                // 中药规格
                if (ImportData.NULLVALUE.equals(product.getProTcmSpecs())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "中药规格"));
                }
                // 中药批准文号
                if (ImportData.NULLVALUE.equals(product.getProTcmRegisterNo())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "中药批准文号"));
                }
                // 中药生产企业代码
                if (ImportData.NULLVALUE.equals(product.getProTcmFactoryCode())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "中药生产企业代码"));
                }
                // 中药产地
                if (ImportData.NULLVALUE.equals(product.getProTcmPlace())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0143.getMsg(), key + 1, "中药产地"));
                }
            }
        }
    }

    /**
     * 数据验证完成
     *
     * @return
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 数据导入完成
     *
     * @param bulUpdateCode
     * @param map
     * @param <T>
     */
    @Override
    @Transactional
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> map) {

        List<String> errorList = new ArrayList<>();

        // 批量导入
        try {
            for (Integer key : map.keySet()) {
                // 行数据
                Product product = (Product) map.get(key);

                // 数据库对象
                GaiaProductBasic gaiaProductBasic = gaiaProductBasicMapper.selectByPrimaryKey(product.getProCode());
                if (gaiaProductBasic == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
                    break;
                }

                SpringUtil.copyPropertiesIgnoreNull(product, gaiaProductBasic);

                // 商品状态
                if (super.isClear(product.getProStatus())) {
                    // 清空
                    gaiaProductBasic.setProStatus(null);
                } else if (super.isNotBlank(product.getProStatusValue())) {
                    gaiaProductBasic.setProStatus(product.getProStatusValue());
                }
                // 保质期单位
                if (super.isClear(product.getProLifeUnit())) {
                    // 清空
                    gaiaProductBasic.setProLifeUnit(null);
                } else if (super.isNotBlank(product.getProLifeUnitValue())) {
                    gaiaProductBasic.setProLifeUnit(product.getProLifeUnitValue());
                }
                // 启用电子监管码
                if (super.isClear(product.getProElectronicCode())) {
                    // 清空
                    gaiaProductBasic.setProElectronicCode(null);
                } else if (super.isNotBlank(product.getProElectronicCodeValue())) {
                    gaiaProductBasic.setProElectronicCode(product.getProElectronicCodeValue());
                }
                // 国家医保品种
                if (super.isClear(product.getProMedProdct())) {
                    // 清空
                    gaiaProductBasic.setProMedProdct(null);
                } else if (super.isNotBlank(product.getProMedProdctValue())) {
                    gaiaProductBasic.setProMedProdct(product.getProMedProdctValue());
                }

                // 清空数据处理
                super.initNull(gaiaProductBasic);

                gaiaProductBasicMapper.updateByPrimaryKey(gaiaProductBasic);

                // 业务表数据存在判断
                if (!super.isNotBlank(product.getClient())) {
                    continue;
                }

                // 数据库对象
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                gaiaProductBusinessKey.setClient(product.getClient());
                gaiaProductBusinessKey.setProSite(product.getProSite());
                gaiaProductBusinessKey.setProSelfCode(product.getProSelfCode());
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                if (gaiaProductBusiness == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0128.getMsg(), key + 1));
                    break;
                }
                SpringUtil.copyPropertiesIgnoreNull(product, gaiaProductBusiness);
                // 商品状态
                if (super.isClear(product.getProStatus())) {
                    // 清空
                    gaiaProductBusiness.setProStatus(null);
                } else if (super.isNotBlank(product.getProStatusValue())) {
                    gaiaProductBusiness.setProStatus(product.getProStatusValue());
                }
                // 禁止销售
                if (super.isClear(product.getProNoRetail())) {
                    // 清空
                    gaiaProductBusiness.setProNoRetail(null);
                } else if (super.isNotBlank(product.getProNoRetailValue())) {
                    gaiaProductBusiness.setProNoRetail(product.getProNoRetailValue());
                }
                // 禁止采购
                if (super.isClear(product.getProNoPurchase())) {
                    // 清空
                    gaiaProductBusiness.setProNoPurchase(null);
                } else if (super.isNotBlank(product.getProNoPurchaseValue())) {
                    gaiaProductBusiness.setProNoPurchase(product.getProNoPurchaseValue());
                }
                // 禁止配送
                if (super.isClear(product.getProNoDistributed())) {
                    // 清空
                    gaiaProductBusiness.setProNoDistributed(null);
                } else if (super.isNotBlank(product.getProNoDistributedValue())) {
                    gaiaProductBusiness.setProNoDistributed(product.getProNoDistributedValue());
                }
                // 禁止退厂
                if (super.isClear(product.getProNoSupplier())) {
                    // 清空
                    gaiaProductBusiness.setProNoSupplier(null);
                } else if (super.isNotBlank(product.getProNoSupplierValue())) {
                    gaiaProductBusiness.setProNoSupplier(product.getProNoSupplierValue());
                }
                // 禁止退仓
                if (super.isClear(product.getProNoDc())) {
                    // 清空
                    gaiaProductBusiness.setProNoDc(null);
                } else if (super.isNotBlank(product.getProNoDcValue())) {
                    gaiaProductBusiness.setProNoDc(product.getProNoDcValue());
                }
                // 禁止调剂
                if (super.isClear(product.getProNoAdjust())) {
                    // 清空
                    gaiaProductBusiness.setProNoAdjust(null);
                } else if (super.isNotBlank(product.getProNoAdjustValue())) {
                    gaiaProductBusiness.setProNoAdjust(product.getProNoAdjustValue());
                }
                // 禁止调剂
                if (super.isClear(product.getProNoSale())) {
                    // 清空
                    gaiaProductBusiness.setProNoSale(null);
                } else if (super.isNotBlank(product.getProNoSaleValue())) {
                    gaiaProductBusiness.setProNoSale(product.getProNoSaleValue());
                }
                // 禁止请货
                if (super.isClear(product.getProNoApply())) {
                    // 清空
                    gaiaProductBusiness.setProNoApply(null);
                } else if (super.isNotBlank(product.getProNoApplyValue())) {
                    gaiaProductBusiness.setProNoApply(product.getProNoApplyValue());
                }
                // 是否拆零
                if (super.isClear(product.getProIfpart())) {
                    // 清空
                    gaiaProductBusiness.setProIfpart(null);
                } else if (super.isNotBlank(product.getProIfpartValue())) {
                    gaiaProductBusiness.setProIfpart(product.getProIfpartValue());
                }
                // 是否医保
                if (super.isClear(product.getProIfMed())) {
                    // 清空
                    gaiaProductBusiness.setProIfMed(null);
                } else if (super.isNotBlank(product.getProIfMedValue())) {
                    gaiaProductBusiness.setProIfMed(product.getProIfMedValue());
                }
                // 按中包装量倍数配货
                if (super.isClear(product.getProPackageFlag())) {
                    // 清空
                    gaiaProductBusiness.setProPackageFlag(null);
                } else if (super.isNotBlank(product.getProPackageFlagValue())) {
                    gaiaProductBusiness.setProPackageFlag(product.getProPackageFlagValue());
                }
                // 是否重点养护
                if (super.isClear(product.getProKeyCare())) {
                    // 清空
                    gaiaProductBusiness.setProKeyCare(null);
                } else if (super.isNotBlank(product.getProKeyCareValue())) {
                    gaiaProductBusiness.setProKeyCare(product.getProKeyCareValue());
                }
                // 固定货位
                if (super.isClear(product.getProFixBin())) {
                    gaiaProductBusiness.setProFixBin(gaiaProductBusiness.getProSelfCode());
                }
                // 清空数据处理
                super.initNull(gaiaProductBusiness);
                //限购数量
                if (!ImportData.NULLVALUE.equals(product.getProLimitQty()) && !StringUtils.isBlank(product.getProLimitQty())) {
                    gaiaProductBusiness.setProLimitQty(new BigDecimal(product.getProLimitQty()));
                }
                //最小订货量
                if (!ImportData.NULLVALUE.equals(product.getProMinQty()) && !StringUtils.isBlank(product.getProMinQty())) {
                    gaiaProductBusiness.setProMinQty(new BigDecimal(product.getProMinQty()));
                }
                //含麻最大配货量
                if (!ImportData.NULLVALUE.equals(product.getProMaxQty()) && !StringUtils.isBlank(product.getProMaxQty())) {
                    gaiaProductBusiness.setProMaxQty(new BigDecimal(product.getProMaxQty()));
                }
                gaiaProductBusinessMapper.updateByPrimaryKey(gaiaProductBusiness);
            }
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            // 更新导入日志
            editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }
}

