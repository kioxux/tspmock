package com.gov.purchase.module.base.dto.excel;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
@Data
public class Store {

    /**
     * 加盟商
     */
    @ExcelValidate(index = 0, name = "加盟商", type = ExcelValidate.DataType.STRING, maxLength = 8)
    private String client;

    /**
     * 门店编码
     */
    @ExcelValidate(index = 1, name = "门店编码", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String stoCode;
    /**
     * 门店名称
     */
    @ExcelValidate(index = 2, name = "门店名称", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String stoName;
    /**
     * 助记码
     */
    @ExcelValidate(index = 3, name = "助记码", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String stoPym;
    /**
     * 门店简称
     */
    @ExcelValidate(index = 4, name = "门店简称", type = ExcelValidate.DataType.STRING, maxLength = 10, editRequired = false)
    private String stoShortName;
    /**
     * 门店属性
     */
    @ExcelValidate(index = 5, name = "门店属性", type = ExcelValidate.DataType.STRING, maxLength = 1, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.STO_ATTRIBUTE)
    private String stoAttribute;
    /**
     * 配送方式
     */
    @ExcelValidate(index = 6, name = "配送方式", type = ExcelValidate.DataType.STRING, maxLength = 1, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.STO_DELIVERY_MODE)
    private String stoDeliveryMode;
    /**
     * 关联门店
     */
    @ExcelValidate(index = 7, name = "关联门店", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String stoRelationStore;
    /**
     * 门店状态
     */
    @ExcelValidate(index = 8, name = "门店状态", type = ExcelValidate.DataType.STRING, maxLength = 1, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.STO_STATUS)
    private String stoStatus;
    /**
     * 经营面积
     */
    @ExcelValidate(index = 9, name = "经营面积", type = ExcelValidate.DataType.STRING, maxLength = 10, editRequired = false)
    private String stoArea;
    /**
     * 开业日期
     */
    @ExcelValidate(index = 10, name = "开业日期", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", editRequired = false)
    private String stoOpenDate;
    /**
     * 关店日期
     */
    @ExcelValidate(index = 11, name = "关店日期", type = ExcelValidate.DataType.DATE, dateFormat = "yyyyMMdd", addRequired = false, editRequired = false)
    private String stoCloseDate;
    /**
     * 详细地址
     */
    @ExcelValidate(index = 12, name = "详细地址", type = ExcelValidate.DataType.STRING, maxLength = 50, editRequired = false)
    private String stoAdd;
    /**
     * 省
     */
    @ExcelValidate(index = 13, name = "省", type = ExcelValidate.DataType.STRING, maxLength = 20, editRequired = false)
    private String stoProvince;
    private String stoProvinceValue;

    /**
     * 城市
     */
    @ExcelValidate(index = 14, name = "城市", type = ExcelValidate.DataType.STRING, maxLength = 20, editRequired = false,
            relyRequired = {"stoProvince"})
    private String stoCity;
    private String stoCityValue;
    /**
     * 区/县
     */
    @ExcelValidate(index = 15, name = "区/县", type = ExcelValidate.DataType.STRING, maxLength = 20, editRequired = false,
            relyRequired = {"stoCity"})
    private String stoDistrict;
    private String stoDistrictValue;
    /**
     * 是否医保店
     */
    @ExcelValidate(index = 16, name = "是否医保店", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "stoIfMedicalcareValue")
    private String stoIfMedicalcare;
    /**
     * 是否医保店
     */
    private String stoIfMedicalcareValue;
    /**
     * DTP
     */
    @ExcelValidate(index = 17, name = "DTP", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.NO_YES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "stoIfDtpValue")
    private String stoIfDtp;
    /**
     * DTP
     */
    private String stoIfDtpValue;
    /**
     * 税分类
     */
    @ExcelValidate(index = 18, name = "税分类", type = ExcelValidate.DataType.STRING, editRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.STO_TAX_CLASS, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL,
            dictionaryStaticField = "stoTaxClassValue")
    private String stoTaxClass;
    /**
     * 税分类
     */
    private String stoTaxClassValue;
    /**
     * 委托配送公司 委托配送公司主数据表（GAIA_DELIVERY_COMPANY） 可多选
     */
    @ExcelValidate(index = 19, name = "委托配送公司", type = ExcelValidate.DataType.STRING, maxLength = 100, addRequired = false, editRequired = false)
    private String stoDeliveryCompany;
    /**
     * 连锁总部 同一加盟商下面的DC
     */
    @ExcelValidate(index = 20, name = "连锁总部", type = ExcelValidate.DataType.STRING, addRequired = false, editRequired = false)
    private String stoChainHead;
    /**
     * 纳税主体  不填则等于自己
     */
    @ExcelValidate(index = 21, name = "纳税主体", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false, editRequired = false)
    private String stoTaxSubject;
    /**
     * 税率
     */
    @ExcelValidate(index = 22, name = "税率", type = ExcelValidate.DataType.STRING, maxLength = 10, editRequired = false)
    private String stoTaxRate;

    /**
     * 配送中心
     */
    @ExcelValidate(index = 23, name = "配送中心", type = ExcelValidate.DataType.STRING, maxLength = 4, addRequired = false, editRequired = false)
    private String stoDcCode;
}

