package com.gov.purchase.module.purchase.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewDto;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewVo;

/**
 * @author yzf
 * 采购计划审批
 */
public interface PurchasePlanReviewService {

    /**
     * 采购计划列表
     *
     * @param purchasePlanReviewDto
     * @return
     */
    Result queryPurchasePlanReviewList(PurchasePlanReviewDto purchasePlanReviewDto);


    /**
     * 采购计划明细列表
     *
     * @param purchasePlanReviewDto
     * @return
     */
    Result queryPurchasePlanReviewDetailList(PurchasePlanReviewDto purchasePlanReviewDto);

    /**
     * 审核采购计划
     *
     * @param dto
     * @return
     */
    int updatePurchasePlan(PurchasePlanReviewDto dto);

    /**
     * 采购计划拒绝
     *
     * @param dto
     * @return
     */
    int refusePurchasePlan(PurchasePlanReviewDto dto);

    /**
     * 采购计划明细保存
     *
     * @param vo
     * @return
     */
    Result updatePurchasePlanDetail(PurchasePlanReviewVo vo);

    /**
     * 审核采购计划明细
     *
     * @param vo
     * @return
     */
    Result updatePurchasePlanAndDetail(PurchasePlanReviewVo vo);

    /**
     * 导出
     *
     * @param dto
     * @return
     */
    Result exportList(PurchasePlanReviewDto dto);
}
