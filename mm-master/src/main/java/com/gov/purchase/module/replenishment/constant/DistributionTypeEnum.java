package com.gov.purchase.module.replenishment.constant;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/8 12:19
 **/
public enum DistributionTypeEnum {

    ONCE_CALL(1, "单次调取"),
    MANY_CALL(2, "多次调取"),;

    public final Integer type;
    public final String message;

    DistributionTypeEnum(Integer type, String message) {
        this.type = type;
        this.message = message;
    }
}
