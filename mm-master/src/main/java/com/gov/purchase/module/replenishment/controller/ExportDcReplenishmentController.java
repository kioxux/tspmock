package com.gov.purchase.module.replenishment.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.replenishment.service.DcReplenishService;
import com.gov.purchase.module.replenishment.service.DcReplenishmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@Api(tags = "DC补货")
@RestController
@RequestMapping("dcReplenishment")
public class ExportDcReplenishmentController {

    @Resource
    private DcReplenishmentService dcReplenishmentService;

    @Resource
    private DcReplenishService dcReplenishService;

    @Log("DC补货列表导出")
    @ApiOperation("DC补货列表导出")
    @GetMapping("exportReplenishmentList")
    public Result exportReplenishmentList(@RequestParam("dcCode") String dcCode,
                                          @RequestParam("pageSize") Integer pageSize,
                                          @RequestParam("pageNum") Integer pageNum) {
        return dcReplenishmentService.exportReplenishmentList(dcCode, pageNum, pageSize);
    }

    @Log("DC补货列表导出")
    @ApiOperation("DC补货列表导出")
    @GetMapping("exportReplenishmentNewList")
    public Result exportReplenishmentNewList(@RequestParam("dcCode") String dcCode) {
        return dcReplenishService.exportReplenishmentNewList(dcCode);
    }

}
