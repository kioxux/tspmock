package com.gov.purchase.module.supplier.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.entity.GaiaSupplierSalesman;
import com.gov.purchase.entity.GaiaUserData;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.supplier.dto.*;

import java.util.List;

public interface OptimizeSupplierService {

    /**
     * 供应商basic表_列表数据
     */
    List<GaiaSupplierBusiness> getSupBasicList(GetSupBasicListRequestDTO dto);

    /**
     * 供应商保存1_先与已有数据进行比对_返回比对结果集
     */
    List<SaveSupList1ResponseDTO> saveSupList1(List<GaiaSupplierBusinessInfo> list);

    /**
     * 供应商保存2
     */
    void saveSupList2(SaveSupList2RequestDTO dto);

    /**
     * 供应商查询列表
     */
    PageInfo<GaiaSupplierBusinessDTO> getSupList(GetSupListRequestDTO dto);

    /**
     * 供应商列表导出
     */
    Result exportSupList(GetSupListRequestDTO dto);

    /**
     * 批量更新
     */
    void editSupList(List<GaiaSupplierBusiness> list, String remarks);

    /**
     * 详情
     */
    GetSupDetailsDTO getSupDetails(GaiaSupplierBusinessKey key);

    /**
     * 单条更新
     */
    void editSup(GaiaSupplierBusiness business, String remarks);

    List<GaiaUserData> getUserList();

    /**
     * 供应商业务员新增
     */
    void saveSupplierSalesmanList(List<GaiaSupplierSalesman> list, String client, String supSite, String supSelfCode, String gssFlowNo);

    /**
     * 供应商业务员编辑
     */
    void editSupplierSalesmanList(EditSupplierSalesmanListVO vo);

    /**
     * 供应商业务员列表
     */
    List<GaiaSupplierSalesmanDTO> getSupplierSalesmanList(String supSite, String supSelfCode);

    /**
     * 供应商经营范围编辑保存
     */
    void editSupScope(EditScopeVO vo);
}
