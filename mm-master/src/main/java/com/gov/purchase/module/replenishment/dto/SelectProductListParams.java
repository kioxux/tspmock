package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.01.12
 */
@Data
public class SelectProductListParams {

    /**
     * 配送中心
     */
    private String gsrhAddr;

    /**
     * 委托配送中心
     */
    private String dcWtdc;

    /**
     * 选择商品集合
     */
    private List<StoreReplenishDetails> storeReplenishDetailsList;

    /**
     * 铺货门店
     */
    private List<String> gaiaStoreDataList;

    /**
    * 不可铺货商品门店提示消息
     */
    private List<String> messageList;
}

