package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

@Data
public class CourseQuestion {

    @ExcelValidate(index = 0, name = "题目类型", type = ExcelValidate.DataType.STRING, maxLength = 11,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.QUESTION_TYPE, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String questionType;

    @ExcelValidate(index = 1, name = "难易级别", type = ExcelValidate.DataType.STRING, maxLength = 11,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.QUESTION_LEVEL, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String questionLevel;

    @ExcelValidate(index = 2, name = "题目名称", type = ExcelValidate.DataType.STRING, maxLength = 256)
    private String questionName;

    @ExcelValidate(index = 3, name = "题目释义", type = ExcelValidate.DataType.STRING, maxLength = 256, addRequired = false)
    private String questionRemark;

    @ExcelValidate(index = 4, name = "答案选项1", type = ExcelValidate.DataType.STRING, maxLength = 128)
    private String questionItem1;

    @ExcelValidate(index = 5, name = "答案选项2", type = ExcelValidate.DataType.STRING, maxLength = 128, addRequired = false)
    private String questionItem2;

    @ExcelValidate(index = 6, name = "答案选项3", type = ExcelValidate.DataType.STRING, maxLength = 128, addRequired = false)
    private String questionItem3;

    @ExcelValidate(index = 7, name = "答案选项4", type = ExcelValidate.DataType.STRING, maxLength = 128, addRequired = false)
    private String questionItem4;

    @ExcelValidate(index = 8, name = "答案选项5", type = ExcelValidate.DataType.STRING, maxLength = 128, addRequired = false)
    private String questionItem5;

    @ExcelValidate(index = 9, name = "答案", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String questionAnswer;

}
