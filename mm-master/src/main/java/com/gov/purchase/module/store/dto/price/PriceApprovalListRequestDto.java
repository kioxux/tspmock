package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "调价单审批列表传入参数")
public class PriceApprovalListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "价格组ID", name = "prcGroupId")
    @NotBlank(message = "加盟商编号不能为空")
    private String prcGroupId;

    @ApiModelProperty(value = "门店编号", name = "prcStore")
    private String prcStore;

    @ApiModelProperty(value = "门店编号更多", name = "prcStores")
    private List<String> prcStores;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNo")
    private String prcModfiyNo;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNos")
    private List<String> prcModfiyNos;

    @ApiModelProperty(value = "价格类型(P001-零售价、P002-会员价、P003-医保价、P004-会员日价、P005拆零价、P006网上零售价、P007-网上会员价)", name = "prcClass")
    private String prcClass;

    @ApiModelProperty(value = "调价日期", name = "prcEffectDate")
    private String prcEffectDate;

    @ApiModelProperty(value = "调价日期开始", name = "prcEffectDateStart")
    private String prcEffectDateStart;

    @ApiModelProperty(value = "调价日期结束", name = "prcEffectDateEnd")
    private String prcEffectDateEnd;

    @ApiModelProperty(value = "调价来源 0.公司调价  2.门店申请", name = "prcSource")
    private String prcSource;
}
