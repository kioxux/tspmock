package com.gov.purchase.module.store.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaProductMinDisplay;
import com.gov.purchase.entity.GaiaProductMinDisplayKey;
import com.gov.purchase.module.store.dto.store.StoreMinQtyExportSearchDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtyResultDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtySearchDto;

import java.io.IOException;
import java.util.List;

public interface StoreMinQtyService {

    /**
     * 最小成列量列表
     * @param dto
     * @return
     */
    PageInfo<StoreMinQtyResultDto> queryList(StoreMinQtySearchDto dto);

    /**
     * 插入
     * @param gaiaProductMinDisplay
     * @return
     */
    Result insertProductMinDisplay(GaiaProductMinDisplay gaiaProductMinDisplay);


    /**
     * 更新
     * @param gaiaProductMinDisplay
     * @return
     */
    Result updateProductMinDisplay(GaiaProductMinDisplay gaiaProductMinDisplay);

    /**
     * 删除
     * @param list
     * @return
     */
    Result deleteProductMinDisplay(List<GaiaProductMinDisplayKey> list);

    /**
     * 导出
     * @param dto
     */
    void exportExcel(StoreMinQtyExportSearchDto dto) throws IOException;


    Result saveProductMinDisplay(GaiaProductMinDisplay gaiaProductMinDisplay);
}
