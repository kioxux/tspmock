package com.gov.purchase.module.goods.service;

import com.gov.purchase.entity.GaiaProductUpdateItem;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.goods.dto.BatchEditParam;
import com.gov.purchase.module.goods.dto.ProductUpdate;
import com.gov.purchase.module.goods.dto.ProductUpdateItem;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.09
 */
public interface BatchEditService {
    /**
     * 修改项目
     *
     * @return
     */
    List<GaiaProductUpdateItem> selectProductUpdateItemList();

    /**
     * 修改地点
     *
     * @return
     */
    List<Dictionary> selectProductUpdateSiteList();

    /**
     * 修改
     *
     * @return
     */
    List<ProductUpdateItem> selectProductItem(String proSite, String proSelfCode, String gpuiField, List<BatchEditParam> batchEditParamList);

    /**
     * 修改商品
     */
    void updateProduct(ProductUpdateItem productUpdateItem, List<Dictionary> dictionary, Map<String, List<String>> map,
                       List<String> proSiteList, List<String> proSelfCodeList);
}
