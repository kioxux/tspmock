package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierBusiness;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/26 10:33
 */
@Data
public class EditSupListVO {
    @NotEmpty(message = "供应商修改数据列表不能为空")
    private List<GaiaSupplierBusiness> businessList;
//    @NotEmpty(message = "备注不能为空")
    private String remarks;
}
