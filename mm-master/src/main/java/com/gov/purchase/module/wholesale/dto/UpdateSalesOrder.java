package com.gov.purchase.module.wholesale.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.08
 */
@Data
public class UpdateSalesOrder {
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 订单号
     */
    @ApiModelProperty(value = "销售订单号", name = "soId")
    @NotBlank(message = "销售订单号不能为空")
    private String soId;
    /**
     * 支付条款
     */
    private String soPaymentId;
    /**
     * 抬头备注
     */
    private String soHeadRemark;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "销售员", name = "soCreateBy")
    @NotBlank(message = "销售员不能为空")
    private String soCreateBy;

    /**
     * 明细数据
     */
    private List<UpdateSalesOrderItem> detailList;
}
