package com.gov.purchase.module.store.dto.price;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "调价单审批传入参数")
public class ApprovePriceRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNo")
    @NotBlank(message = "调价单号不能为空")
    private String  prcModfiyNo;
}
