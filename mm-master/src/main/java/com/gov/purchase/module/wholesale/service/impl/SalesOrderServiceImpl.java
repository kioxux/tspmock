package com.gov.purchase.module.wholesale.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.WmsFeign;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.ProductDto;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.wholesale.constant.Constant;
import com.gov.purchase.module.wholesale.dto.GaiaSoHeader;
import com.gov.purchase.module.wholesale.dto.*;
import com.gov.purchase.module.wholesale.enums.CustomerZqType;
import com.gov.purchase.module.wholesale.service.SalesOrderService;
import com.gov.purchase.utils.*;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.02
 */
@Slf4j
@Service
public class SalesOrderServiceImpl implements SalesOrderService {

    @Resource
    GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    GaiaSoHeaderMapper gaiaSoHeaderMapper;
    @Resource
    GaiaSoItemMapper gaiaSoItemMapper;
    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    FeignService feignService;
    @Resource
    CosUtils cosUtils;
    @Resource
    WmsFeign wmsFeign;
    @Resource
    GaiaJyfwDataMapper gaiaJyfwDataMapper;
    @Resource
    GaiaWholesaleSalesmanMapper gaiaWholesaleSalesmanMapper;
    @Resource
    GaiaWholesaleChannelMapper gaiaWholesaleChannelMapper;
    @Resource
    GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;
    @Resource
    GaiaWholesaleOwnerMapper gaiaWholesaleOwnerMapper;
    @Resource
    GaiaOwnerProductMapper gaiaOwnerProductMapper;
    @Resource
    GaiaOwnerCustomerMapper gaiaOwnerCustomerMapper;
    @Resource
    GaiaProductChangeMapper gaiaProductChangeMapper;
    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private GaiaWmsDiaoboZMapper gaiaWmsDiaoboZMapper;

    /**
     * 批发销售订单创建
     *
     * @param entity
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveSalesOrder(GaiaSoHeader entity) {
        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        TokenUser user = entity.getTokenUser() == null ? feignService.getLoginInfo() : entity.getTokenUser();
        GaiaDcData dcData = gaiaDcDataMapper.selectByDcCode(user.getClient(), entity.getSoCompanyCode());
        if (dcData != null && StringUtils.isNotBlank(dcData.getDcJyfwgk()) && "1".equalsIgnoreCase(dcData.getDcJyfwgk())) {
            List<String> jyfwList = gaiaJyfwDataMapper.getJyfwIdList(user.getClient(), dcData.getDcChainHead());
            if (!CollectionUtils.isNotEmpty(jyfwList)) {
                throw new CustomResultException(ResultEnum.DC_JYFW_NOT_EXIST);
            }
            List<String> distList = jyfwList.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
            if (!CollectionUtils.isNotEmpty(distList)) {
                throw new CustomResultException(ResultEnum.DC_JYFW_NOT_EXIST);
            }
            if (CollectionUtils.isNotEmpty(entity.getSoItemList())) {
                int index = 0;
                List<GaiaProductBusinessKey> payKeyList = new ArrayList<>();
                for (SoItem item : entity.getSoItemList()) {
                    GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                    gaiaProductBusinessKey.setClient(user.getClient());
                    gaiaProductBusinessKey.setProSite(item.getSoSiteCode());
                    gaiaProductBusinessKey.setProSelfCode(item.getSoProCode());
                    payKeyList.add(gaiaProductBusinessKey);
                }
                List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.getProductBusiness2(user.getClient(), payKeyList);
                for (SoItem item : entity.getSoItemList()) {
                    index++;
                    GaiaProductBusiness productBusiness = gaiaProductBusinessList.stream().filter(t ->
                            t.getProSelfCode().equals(item.getSoProCode()) && t.getProSite().equals(item.getSoSiteCode())
                    ).findFirst().orElse(null);
                    if (productBusiness != null && StringUtils.isNotBlank(productBusiness.getProJylb())) {
                        List<String> jylb = Arrays.asList(productBusiness.getProJylb().split(","));
                        List<String> tog = jylb.stream().filter(distList::contains).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(tog)) {
                            throw new CustomResultException("第" + index + "行商品超出销售主体对应连锁总部的经营管控范围");
                        }
                    } else {
                        throw new CustomResultException("第" + index + "行商品超出销售主体对应连锁总部的经营管控范围");
                    }
                }
            }
        }
        // 检查商品经营范围
        checkProJylb(user.getClient(), entity);
        // 货主下的客户明细
        if (StringUtils.isNotBlank(entity.getSoOwner())) {
            List<GaiaWholesaleOwner> list = gaiaWholesaleOwnerMapper.queryWholesaleConsignor(user.getClient(), entity.getSoCompanyCode(), null, entity.getSoOwner());
            if (CollectionUtils.isNotEmpty(list)) {
                List<OwnerCustomerVO> customerVOS = gaiaOwnerCustomerMapper.selectCusByDcCodeAndOwner(user.getClient(), entity.getSoCompanyCode(), list.get(0).getGwoOwner());
                if (CollectionUtils.isNotEmpty(customerVOS)) {
                    List<String> cusCodes = customerVOS.stream().map(OwnerCustomerVO::getGocCusCode).collect(Collectors.toList());
                    // 客户包含
                    if ("1".equalsIgnoreCase(list.get(0).getGwoCusRule()) && !cusCodes.contains(entity.getSoCustomerId())) {
                        return ResultUtil.error(ResultEnum.OWNER_NOT_IN_CUSTOMER);
                    }
                    // 排除客户
                    if ("2".equalsIgnoreCase(list.get(0).getGwoCusRule()) && cusCodes.contains(entity.getSoCustomerId())) {
                        return ResultUtil.error(ResultEnum.OWNER_OUT_CUSTOMER);
                    }
                }
            }
        }
        // 补货单状态改变
        if (CollectionUtils.isNotEmpty(entity.getVoucherIdList())) {
            List<String> gsrhVoucherIdList = gaiaSoItemMapper.getReplenishStatus(user.getClient(), entity.getSoCustomerId(), entity.getVoucherIdList());
            if (CollectionUtils.isNotEmpty(gsrhVoucherIdList)) {
                throw new CustomResultException(MessageFormat.format("请货单[{0}]已经被使用", String.join(",", gsrhVoucherIdList)));
            }
        }

        // 销售订单明细验证
        if (entity.getSoItemList() == null || CollectionUtils.isEmpty(entity.getSoItemList())) {
            throw new CustomResultException(ResultEnum.E0114);
        } else {
            // 销售订单明细对象验证
            for (SoItem soItem : entity.getSoItemList()) {
                ValidationResult result = ValidateUtil.validateEntity(soItem);
                if (result.isHasErrors()) {
                    throw new CustomResultException(result.getMessage());
                }
            }
        }
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(user.getClient());
        gaiaDcDataKey.setDcCode(entity.getSoCompanyCode());
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);

        // 校验
        // 客户数据
        GaiaCustomerBusiness gaiaCustomerBusiness =
                gaiaCustomerBusinessMapper.getCustomerBusiness(user.getClient(), entity.getSoCompanyCode(), entity.getSoCustomerId());
        //校验信用金额和账期控制
        checkCreditQuotaAndZqts(gaiaCustomerBusiness, entity);
        // 客户的状态，
        if (!CommonEnum.CustomerStauts.CUSTOMERNORMAL.getCode().equals(gaiaCustomerBusiness.getCusStatus())) {
            throw new CustomResultException(ResultEnum.E0110);
        }
        // 禁采、
        if (StringUtils.isNotBlank(gaiaCustomerBusiness.getCusNoSale()) &&
                !CommonEnum.DictionaryStaticData.NO_YES.getList().get(0).getValue().equals(gaiaCustomerBusiness.getCusNoSale())) {
            throw new CustomResultException(ResultEnum.E0113);
        }
        // 禁退，
        if (StringUtils.isNotBlank(gaiaCustomerBusiness.getCusNoReturn()) &&
                !CommonEnum.DictionaryStaticData.NO_YES.getList().get(0).getValue().equals(gaiaCustomerBusiness.getCusNoReturn())) {
            throw new CustomResultException(ResultEnum.E0113);
        }
        // 检查 客户主数据 所有证照的效期 ：小于60天-提示，不报错；过期-报错。
        if (gaiaDcData != null && StringUtils.isNotBlank(entity.getSoCustomer2())) {
            GaiaCustomerBusiness gaiaCustomerBusiness2 =
                    gaiaCustomerBusinessMapper.getCustomerBusiness(user.getClient(), entity.getSoCompanyCode(), entity.getSoCustomer2());
            // 是否管控证照效期：1-是
            if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1") && gaiaCustomerBusiness2 != null) {
                // CUS_CREDIT_DATE
                dateCheck(gaiaCustomerBusiness2.getCusCreditDate(), "营业执照", warning, error);
                // CUS_LICENCE_VALID             许可证有效期至
                dateCheck(gaiaCustomerBusiness2.getCusLicenceValid(), "许可证", warning, error);
                // CUS_SPXKZ_DATE					食品许可证有效期
                dateCheck(gaiaCustomerBusiness2.getCusSpxkzDate(), "食品许可证", warning, error);
                //CUS_YLQXXKZ_DATE					医疗器械经营许可证有效期
                dateCheck(gaiaCustomerBusiness2.getCusYlqxxkzDate(), "医疗器械经营许可证", warning, error);
                //CUS_GMP_DATE					GMP有效期
                dateCheck(gaiaCustomerBusiness2.getCusGmpDate(), "GMP", warning, error);
                //CUS_YLJGXKZ_DATE					医疗机构执业许可证书有效期
                dateCheck(gaiaCustomerBusiness2.getCusYljgxkzDate(), "医疗机构执业许可证书", warning, error);
                //CUS_GSP_DATES					GSP有效期
                dateCheck(gaiaCustomerBusiness2.getCusGspDates(), "GSP", warning, error);
                // CUS_YWYJSRQ
                dateCheck(gaiaCustomerBusiness2.getCusYwyjsrq(), "业务员（采购员）结束日期", warning, error);
                // CUS_SHRJSRQ
                dateCheck(gaiaCustomerBusiness2.getCusShrjsrq(), "收货人/提货人结束日期", warning, error);
                // CUS_ZBXYYXQ
                dateCheck(gaiaCustomerBusiness2.getCusZbxyyxq(), "质保协议", warning, error);
                // CUS_GXHTYXQ
                dateCheck(gaiaCustomerBusiness2.getCusGxhtyxq(), "购销合同", warning, error);
                // CUS_FRWTSXQ 法人委托书
                dateCheck(gaiaCustomerBusiness2.getCusFrwtsxq(), "法人委托书", warning, error);
            }
        }
        // 批发渠道渠道明细
        List<GaiaWholesaleChannelDetail> gaiaWholesaleChannelDetailList = gaiaWholesaleChannelMapper.selectDetailList(user.getClient(), entity.getSoCompanyCode());

        List<GaiaProductBusinessKey> payKeyList = new ArrayList<>();
        // 商品的状态
        for (SoItem soItem : entity.getSoItemList()) {
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(user.getClient());
            gaiaProductBusinessKey.setProSite(entity.getSoCompanyCode());
            gaiaProductBusinessKey.setProSelfCode(soItem.getSoProCode());
            payKeyList.add(gaiaProductBusinessKey);
        }
        // 商品数据
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.getProductBusiness2(user.getClient(), payKeyList);

        // 商品的状态
        for (SoItem soItem : entity.getSoItemList()) {
            // 批发渠道管控  商品XXXX不可以对客户XXX销售。
            if (CollectionUtils.isNotEmpty(gaiaWholesaleChannelDetailList)) {
                // 类型：0-商品，1-客户
                GaiaWholesaleChannelDetail gaiaWholesaleChannelDetailPro = gaiaWholesaleChannelDetailList.stream().filter(t ->
                        t.getGwcdSelfCode().equals(soItem.getSoProCode()) && t.getGwcdType().equals("0")).findFirst().orElse(null);
                GaiaWholesaleChannelDetail gaiaWholesaleChannelDetailCus = gaiaWholesaleChannelDetailList.stream().filter(t ->
                        t.getGwcdSelfCode().equals(entity.getSoCustomerId()) && t.getGwcdType().equals("1")).findFirst().orElse(null);
                boolean flg = false;
                if (gaiaWholesaleChannelDetailPro == null) {
                    flg = true;
                }
                if (gaiaWholesaleChannelDetailPro != null && gaiaWholesaleChannelDetailCus != null) {
                    flg = true;
                }
                if (!flg) {
                    throw new CustomResultException("商品" + soItem.getSoProCode() + "不可以对客户" + entity.getSoCustomerId() + "销售");
                }
            }
            GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessList.stream().filter(t ->
                    t.getProSelfCode().equals(soItem.getSoProCode()) && t.getProSite().equals(entity.getSoCompanyCode())
            ).findFirst().orElse(null);
            if (gaiaProductBusiness == null) {
                throw new CustomResultException("商品" + gaiaProductBusiness.getProSelfCode() + "不存在");
            }
            // 禁止批发
            String proNoSale = gaiaProductBusiness.getProNoSale();
            if (StringUtils.isNotBlank(proNoSale) && "1".equals(proNoSale)) {
                throw new CustomResultException("商品" + gaiaProductBusiness.getProSelfCode() + "禁止批发");
            }
            if (gaiaDcData != null) {
                // 是否管控证照效期：1-是
                if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
                    // 检查 商品主数据 商品库存有效期：小于180天-提示，不报错；（小于30天-报错）-> 提示，不报错。
                    if (StringUtils.isNotBlank(soItem.getBatExpiryDate()) && soItem.getBatExpiryDate().trim().length() == 8) {
                        String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                        Long datediff = DateUtils.datediff(day, soItem.getBatExpiryDate());
                        // 异常
                        if (datediff.intValue() >= 0 && datediff.intValue() < 30) {
                            error.add("商品" + gaiaProductBusiness.getProSelfCode() + "库存有效期小于30天");
                        }
                        // 警告
                        if (datediff.intValue() >= 0 && datediff.intValue() < 180) {
                            warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "库存有效期即将过期");
                        }
                        if (datediff.intValue() < 0) {
                            warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号已过期");
                        }
                    }
                }
            }
            // 非正常
            if (CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(gaiaProductBusiness.getProStatus())) {
                throw new CustomResultException(MessageFormat.format(ResultEnum.E0111.getMsg(), soItem.getSoProCode()));
            }
            // 禁止配送
            if (CommonEnum.NoYesStatus.YES.getCode().equals(gaiaProductBusiness.getProNoDistributed())) {
                throw new CustomResultException(MessageFormat.format("商品[{0}]已禁止配送，请删除该商品", soItem.getSoProCode()));
            }
        }
        if (CollectionUtils.isNotEmpty(error)) {
            Result result = ResultUtil.error(ResultEnum.E0015);
            result.setWarning(error);
            return result;
        }

        CommonUtils utils = new CommonUtils();
        String soId = null;
        // 销售订单主表
        com.gov.purchase.entity.GaiaSoHeader gaiaSoHeader = new com.gov.purchase.entity.GaiaSoHeader();
        //加盟商
        gaiaSoHeader.setClient(user.getClient());
        //销售订单号
        if (StringUtils.isNotBlank(entity.getSoId())) {
            gaiaSoHeader.setSoId(entity.getSoId());
            soId = entity.getSoId();
        } else {
            soId = utils.getPoId(user.getClient(), entity.getSoType(), gaiaSoHeaderMapper, redisClient);
            // 凭证号生成失败
            if (StringUtils.isBlank(soId)) {
                throw new CustomResultException(ResultEnum.E0112);
            }
            gaiaSoHeader.setSoId(soId);
        }
        //销售订单类型
        gaiaSoHeader.setSoType(entity.getSoType());
        //客户编码
        gaiaSoHeader.setSoCustomerId(entity.getSoCustomerId());
        //公司代码
        gaiaSoHeader.setSoCompanyCode(entity.getSoCompanyCode());
        //凭证日期
        gaiaSoHeader.setSoDate(entity.getSoDate());
        //付款条款
        gaiaSoHeader.setSoPaymentId(entity.getSoPaymentId());
        //抬头备注
        gaiaSoHeader.setSoHeadRemark(entity.getSoHeadRemark());
        //创建人
        gaiaSoHeader.setSoCreateBy(user.getUserId());
        //创建日期
        gaiaSoHeader.setSoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        //创建时间
        gaiaSoHeader.setSoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        GaiaWmsShenPiPeiZhiDTO gaiaWmsShenPiPeiZhiDTO = gaiaSoHeaderMapper.queryShenPiPeiZhi(user.getClient(), entity.getSoCompanyCode(), 2);
        if (!entity.isType()) {
            gaiaSoHeader.setSoApproveStatus("-10");
        }
        if (StringUtils.isBlank(gaiaSoHeader.getSoApproveStatus())) {
            if (gaiaWmsShenPiPeiZhiDTO != null
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprxm1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())
            ) {
                String para1 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "WHOLESALE_APPROVE_OUT");
                if (StringUtils.isNotBlank(para1) && "1".equals(para1) && CollectionUtils.isNotEmpty(entity.getVoucherIdList())) {
                    gaiaSoHeader.setSoApproveStatus("2");
                } else {
                    gaiaSoHeader.setSoApproveStatus("1");
                }
            } else {
                //审批状态
                gaiaSoHeader.setSoApproveStatus("2");
            }
        }
        // 客户2
        gaiaSoHeader.setSoCustomer2(entity.getSoCustomer2());
        // 销售
        gaiaSoHeader.setGwsCode(entity.getGwsCode());
        // 货主
        gaiaSoHeader.setSoOwner(entity.getSoOwner());
        if (StringUtils.isNotBlank(entity.getSoId())) {
            GaiaSoHeaderKey soHeaderKey = new GaiaSoHeaderKey();
            soHeaderKey.setClient(user.getClient());
            soHeaderKey.setSoId(entity.getSoId());
            com.gov.purchase.entity.GaiaSoHeader gaiaSoHeaderOne = gaiaSoHeaderMapper.selectByPrimaryKey(soHeaderKey);
            if (gaiaSoHeaderOne != null && StringUtils.isNotBlank(gaiaSoHeaderOne.getSoApproveStatus()) && "-10".equals(gaiaSoHeaderOne.getSoApproveStatus())) {
                gaiaSoHeaderMapper.deleteByPrimaryKey(soHeaderKey);
                gaiaSoHeaderMapper.deletePoItemByClientAndSoId(user.getClient(), entity.getSoId());
            } else {
                throw new CustomResultException(MessageFormat.format("订单[{0}]不为暂存状态, 不可审核", entity.getSoId()));
            }
        }
        gaiaSoHeaderMapper.insertSelective(gaiaSoHeader);
        List<GaiaSoItem> gaiaSoItemList = new ArrayList<>();
        for (int i = 0; i < entity.getSoItemList().size(); i++) {
            SoItem soitem = entity.getSoItemList().get(i);
            // 销售订单明细表
            GaiaSoItem gaiaSoItem = new GaiaSoItem();
            //加盟商
            gaiaSoItem.setClient(user.getClient());
            //销售订单号
            gaiaSoItem.setSoId(soId);
            //订单行号
            gaiaSoItem.setSoLineNo(String.valueOf(i + 1));
            //商品编码
            gaiaSoItem.setSoProCode(soitem.getSoProCode());
            //销售订单数量
            gaiaSoItem.setSoQty(soitem.getSoQty());
            //订单单位
            gaiaSoItem.setSoUnit(soitem.getSoUnit());
            //销售订单单价
            gaiaSoItem.setSoPrice(soitem.getSoPrice());
            //订单行金额
            gaiaSoItem.setSoLineAmt(soitem.getSoLineAmt());
            //地点
            gaiaSoItem.setSoSiteCode((soitem.getSoSiteCode()));
            //库存地点
            gaiaSoItem.setSoLocationCode(soitem.getSoLocationCode());
            //批次
            gaiaSoItem.setSoBatch(soitem.getSoBatch());
            //税率
            gaiaSoItem.setSoRate(soitem.getSoRate());
            //计划发货日期
            gaiaSoItem.setSoDeliveryDate(soitem.getSoDeliveryDate());
            if (StringUtils.isBlank(gaiaSoItem.getSoDeliveryDate())) {
                gaiaSoItem.setSoDeliveryDate(DateUtils.getCurrentDateStrYYMMDD());
            }
            //订单行备注
            gaiaSoItem.setSoLineRemark(soitem.getSoLineRemark());
            //删除标记
            gaiaSoItem.setSoLineDelete("0");
            gaiaSoItem.setSoCompleteFlag("0");
            // 生产批号
            gaiaSoItem.setSoBatchNo(soitem.getSoBatchNo());
            // 货位号
            gaiaSoItem.setSoHwh(soitem.getGsrdHwh());
            // 单号
            gaiaSoItem.setSoReferOrder(soitem.getGsrdVoucherId());
            // 行号
            gaiaSoItem.setSoReferOrderLineno(soitem.getGsrdSerial());
            //交货已完成标记
//            gaiaSoItemMapper.insertSelective(gaiaSoItem);
            gaiaSoItemList.add(gaiaSoItem);
        }
        gaiaSoItemMapper.insertSelectiveList(gaiaSoItemList);

        // 补货单状态改变
        if (CollectionUtils.isNotEmpty(entity.getVoucherIdList())) {
            gaiaSoItemMapper.updateReplenishStatus(user.getClient(), entity.getSoCustomerId(), entity.getVoucherIdList());
        }
//        try {
//            soAutomaticBilling(gaiaSoHeader);
//        } catch (Exception e) {
//            log.error("销售自动开单异常" + e.getMessage(), e);
//        }
        Result result = ResultUtil.success(soId);
        result.setWarning(warning);
        result.setResult1(gaiaSoHeader);
        return result;
    }

    @Override
    public Result temporarySalesOrder(GaiaSoHeader gaiaSoHeader) {
        gaiaSoHeader.setType(false);
        return saveSalesOrder(gaiaSoHeader);
    }

    @Override
    public List<GaiaSoHeaderVO> temporaryList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaSoHeaderMapper.temporaryList(user.getClient(), null);
    }

    @Override
    public TemporaryOrderDetailDTO temporaryOrderDetail(String soId) {
        TokenUser user = feignService.getLoginInfo();
        TemporaryOrderDetailDTO dto = new TemporaryOrderDetailDTO();
        List<GaiaSoHeaderVO> order = gaiaSoHeaderMapper.temporaryList(user.getClient(), soId);
        if (CollectionUtils.isNotEmpty(order)) {
            BeanUtils.copyProperties(order.get(0), dto);
            // 销售订单明细
            List<SalesOrderItem> list = gaiaSoItemMapper.querySalesDetails(user.getClient(), soId);
            if (CollectionUtils.isNotEmpty(list)) {
                for (SalesOrderItem product : list) {
                    List<Dictionary> dictionaryList = new ArrayList<>();
                    if (product != null) {
                        if (product.getProLsj() != null && product.getProLsj().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getProLsj().toString(), CommonEnum.SalesPriceType.PRO_LSJ.getName()));
                        }
                        if (product.getProHyj() != null && product.getProHyj().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getProHyj().toString(), CommonEnum.SalesPriceType.PRO_HYJ.getName()));
                        }
                        if (product.getProGpj() != null && product.getProGpj().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getProGpj().toString(), CommonEnum.SalesPriceType.PRO_GPJ.getName()));
                        }
                        if (product.getProGlj() != null && product.getProGlj().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getProGlj().toString(), CommonEnum.SalesPriceType.PRO_GLJ.getName()));
                        }
                        if (product.getProYsj1() != null && product.getProYsj1().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getProYsj1().toString(), CommonEnum.SalesPriceType.PRO_YSJ1.getName()));
                        }
                        if (product.getProYsj2() != null && product.getProYsj2().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getProYsj2().toString(), CommonEnum.SalesPriceType.PRO_YSJ2.getName()));
                        }
                        if (product.getProYsj3() != null && product.getProYsj3().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getProYsj3().toString(), CommonEnum.SalesPriceType.PRO_YSJ3.getName()));
                        }
                        if (product.getBatPoPrice() != null && product.getBatPoPrice().compareTo(BigDecimal.ZERO) != 0) {
                            dictionaryList.add(new Dictionary(product.getBatPoPrice().toString(), CommonEnum.SalesPriceType.BAT_PO_PRICE.getName()));
                        }
                        product.setDictionaryList(dictionaryList);
                    }
                }
                dto.setOrderItems(list);
            }
        }
        return dto;
    }

    @Override
    public void delTemporaryOrder(String soId) {
        TokenUser user = feignService.getLoginInfo();
        GaiaSoHeaderKey soHeaderKey = new GaiaSoHeaderKey();
        soHeaderKey.setClient(user.getClient());
        soHeaderKey.setSoId(soId);
        com.gov.purchase.entity.GaiaSoHeader gaiaSoHeader = gaiaSoHeaderMapper.selectByPrimaryKey(soHeaderKey);
        if (gaiaSoHeader != null && StringUtils.isNotBlank(gaiaSoHeader.getSoApproveStatus()) && "-10".equals(gaiaSoHeader.getSoApproveStatus())) {
            gaiaSoHeaderMapper.deleteByPrimaryKey(soHeaderKey);
            gaiaSoHeaderMapper.deletePoItemByClientAndSoId(user.getClient(), soId);
        } else {
            throw new CustomResultException(MessageFormat.format("订单[{0}]不为暂存状态, 不可删除", soId));
        }
    }

    @Override
    public String querySalesOrderConfig() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "SO_IF_DEC");
    }

    /**
     * 检查商品经营范围
     */
    private void checkProJylb(String client, GaiaSoHeader entity) {
        //1.销售主体信息
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByDcCode(client, entity.getSoCompanyCode());
        if (!ObjectUtils.isNotEmpty(gaiaDcData) || (!CommonEnum.NoYesStatus.YES.getCode().equals(gaiaDcData.getDcJyfwgk()) && !"2".equals(gaiaDcData.getDcJyfwgk()))) {
            return;
        }

        // 2.客户主体信息
        GaiaCustomerBusiness customerBusiness = gaiaCustomerBusinessMapper.getCustomerBusiness(client, entity.getSoCompanyCode(), entity.getSoCustomerId());
        //3. 客户 (送达方) 经营范围
        List<String> orgJyfwIdList = gaiaJyfwDataMapper.getOrgJyfwIdList(client, entity.getSoCustomerId());
        if ("2".equals(gaiaDcData.getDcJyfwgk())) {
            List<String> class3List = gaiaJyfwDataMapper.getOrgJyfwIdListClass3(client, entity.getSoCustomerId());
            if (CollectionUtils.isNotEmpty(class3List)) {
                return;
            }
        }
        //4. 销售主体指定商品 经营范围
        List<String> proCodeList = entity.getSoItemList().stream().filter(Objects::nonNull).map(SoItem::getSoProCode).collect(Collectors.toList());
        List<GaiaProductBusiness> proList = gaiaProductBusinessMapper.getProJyfwIdList(client, entity.getSoCompanyCode(), proCodeList);
        // 经营范围不相交的数据
        List<String> finalOrgJyfwIdList = orgJyfwIdList;
        List<String> indexList = new ArrayList<>();
        List<String> messageList = proList.stream().filter(item -> {
            return CollectionUtils.isEmpty(finalOrgJyfwIdList) ||
                    StringUtils.isBlank(item.getProJylb()) ||
                    Collections.disjoint(finalOrgJyfwIdList, Arrays.asList(item.getProJylb().split(",")));
        }).map(item -> {
            int index = IntStream.range(0, entity.getSoItemList().size()).filter(x -> entity.getSoItemList().get(x).getSoProCode().equals(item.getProSelfCode())).findFirst().orElse(-1);
            if (index != -1) {
                indexList.add(String.valueOf(index));
            }
            String proName = StringUtils.isNotBlank(item.getProName()) ? item.getProName() : item.getProCommonname();
            return proName + "(" + item.getProSelfCode() + ")";
        }).collect(Collectors.toList());

        if (CollectionUtils.isNotEmpty(messageList)) {
            throw new CustomResultException(MessageFormat.format("客户[{0}]没有商品[{1}]的经营范围[第{2}行]，不可销售！",
                    ObjectUtils.isEmpty(customerBusiness.getCusName()) ? "" : customerBusiness.getCusName(),
                    String.join(",", messageList), String.join(",", indexList)));
        }
    }

    @Override
    public void soAutomaticBilling(com.gov.purchase.entity.GaiaSoHeader gaiaSoHeader) {
        if (ObjectUtils.isEmpty(gaiaSoHeader)) {
            return;
        }
        List<GaiaSoItem> replenishDList = gaiaSoItemMapper.getItemListByHeaderKey(gaiaSoHeader.getClient(), gaiaSoHeader.getSoId());

        String autoReplenishCreateOrder = gaiaSdReplenishHMapper.wmsConfig(gaiaSoHeader.getClient(), gaiaSoHeader.getSoCompanyCode(), CommonEnum.WmsConfrig.AUTO_SO_CREATE_ORDER.getCode());
        if (CommonEnum.NoYesStatus.YES.getCode().equals(autoReplenishCreateOrder)) {
            // 自动开单
            Map<String, Object> param = soAutomaticCreateOrderParamInit(gaiaSoHeader, replenishDList);
            soAutomaticCreateOrderExcute(param);
        } else {
            String autoAppointBillCreateOrder = gaiaSdReplenishHMapper.wmsConfig(gaiaSoHeader.getClient(), gaiaSoHeader.getSoCompanyCode(), CommonEnum.WmsConfrig.AUTO_APPOINT_BILL_CREATE_ORDER.getCode());
            if (CommonEnum.NoYesStatus.YES.getCode().equals(autoAppointBillCreateOrder)) {
                boolean match = replenishDList.stream().filter(Objects::nonNull)
                        .anyMatch(item -> StringUtils.isNotBlank(item.getSoBatchNo()) || StringUtils.isNotBlank(item.getSoHwh()));

                if (match) {
                    // 自动开单
                    Map<String, Object> param = soAutomaticCreateOrderParamInit(gaiaSoHeader, replenishDList);
                    soAutomaticCreateOrderExcute(param);
                }
            }
        }
    }

    /**
     * 自动开单
     */
    private void soAutomaticCreateOrderExcute(Map<String, Object> param) {
        try {
            log.info("销售自动开单参数: {}", new JSONObject(param).toJSONString());
            FeignResult feignResult = wmsFeign.automaticCreateOrder(param);
            if (feignResult.getCode() == 0) {
                log.info("销售自动开单成功,返回值: {}", JsonUtils.beanToJson(feignResult));
            } else {
                log.info("销售自动开单失败,返回值: {}", JsonUtils.beanToJson(feignResult));
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.info("销售自动开单异常");
        }
    }

    private Map<String, Object> soAutomaticCreateOrderParamInit(com.gov.purchase.entity.GaiaSoHeader header, List<GaiaSoItem> detailsList) {
        List<Map<String, String>> createOrderList = detailsList.stream().filter(Objects::nonNull).map(details -> {
            Map<String, String> createOrder = new HashMap<>();
            createOrder.put("poReqType", "S");
            createOrder.put("gsrhType", "");
            createOrder.put("poId", header.getSoId());
            createOrder.put("poLineNo", details.getSoLineNo());
            createOrder.put("poCreateDate", header.getSoCreateDate());
            createOrder.put("poHeadRemark", "");
            createOrder.put("poLineRemark", "");
            createOrder.put("customerNo", header.getSoCustomerId());
            createOrder.put("customerNo2", header.getSoCustomer2());
            createOrder.put("poProCode", details.getSoProCode());
            createOrder.put("poBatchNo", details.getSoBatchNo());
            createOrder.put("poQty", ObjectUtils.isEmpty(details.getSoQty()) ? "" : details.getSoQty().toString());
            createOrder.put("poPrice", ObjectUtils.isEmpty(details.getSoPrice()) ? "" : details.getSoPrice().toString());
            createOrder.put("poLocationCode", "1000");
            createOrder.put("dnFlag", "");
            createOrder.put("dnBy", "");
            createOrder.put("dnDate", "");
            createOrder.put("dnId", "");
            createOrder.put("poCompleteFlag", "");
            createOrder.put("gsrhFlag", "");
            createOrder.put("dnQty", "");
            createOrder.put("hwh", details.getSoHwh());
            return createOrder;
        }).collect(Collectors.toList());

        Map<String, Object> param = new HashMap<>();
        param.put("client", header.getClient());
        param.put("proSite", header.getSoCompanyCode());
        param.put("demandType", CommonEnum.WmsConfrig.AUTO_SO_CREATE_ORDER.getCode());
        param.put("createOrder", createOrderList);

        return param;
    }

    /**
     * 效期验证
     *
     * @param data
     * @param msg
     */
    private void dateCheck(String data, String msg, List<String> warning, List<String> error) {
        String day = DateUtils.getCurrentDateStr("yyyyMMdd");
        if (StringUtils.isNotBlank(data) && data.trim().length() == 8) {
            // 异常
            if (day.compareTo(data) > 0) {
                error.add("客户" + msg + "已过期");
            }
            // 警告
            Long datediff = DateUtils.datediff(day, data);
            if (datediff.intValue() < 60) {
                warning.add("客户" + msg + "即将过期");
            }
        }
    }

    /**
     * 商品列表获取
     *
     * @param client  加盟商
     * @param proSite 销售主体（DC编码）
     * @param name    商品自编码/名称
     * @return
     */
    @Override
    public Result queryGoodsList(String client, String proSite, String soCustomerId, String name, String gwsCode, Integer priceType, String owner) {
        List<RecallInfo> recallInfos = new ArrayList<>();
        List<RecallInfo> list = gaiaSoHeaderMapper.queryGoodsList(client, proSite, soCustomerId, name, null, owner);
        // 货主下的商品明细
        if (CollectionUtils.isNotEmpty(list) && StringUtils.isNotBlank(owner)) {
            List<GaiaWholesaleOwner> owners = gaiaWholesaleOwnerMapper.queryWholesaleConsignor(client, proSite, null, owner);
            List<OwnerProductVO> products = gaiaOwnerProductMapper.selectProByDcCodeAndOwner(client, proSite, owner);
            if (CollectionUtils.isNotEmpty(owners) && CollectionUtils.isNotEmpty(products)) {
                List<String> proCodes = products.stream().map(OwnerProductVO::getGopProCode).collect(Collectors.toList());
                // 包含商品
                if ("1".equalsIgnoreCase(owners.get(0).getGwoProRule())) {
                    list.forEach(
                            item -> {
                                if (proCodes.contains(item.getProSelfCode())) {
                                    recallInfos.add(item);
                                }
                            }
                    );
                }
                // 排除商品
                if ("2".equalsIgnoreCase(owners.get(0).getGwoProRule())) {
                    list.forEach(
                            item -> {
                                if (!proCodes.contains(item.getProSelfCode())) {
                                    recallInfos.add(item);
                                }
                            }
                    );
                }
            } else {
                recallInfos.addAll(list);
            }
        } else {
            recallInfos.addAll(list);
        }
        recallInfos.forEach(item -> {
            if (priceType != null) {
                if (priceType.intValue() == 1) {
                    item.setSoPrice(item.getProLsj());
                } else if (priceType.intValue() == 2) {
                    item.setSoPrice(item.getProHyj());
                } else if (priceType.intValue() == 3) {
                    item.setSoPrice(item.getProGpj());
                } else if (priceType.intValue() == 4) {
                    item.setSoPrice(item.getProGlj());
                } else if (priceType.intValue() == 5) {
                    item.setSoPrice(item.getProYsj1());
                } else if (priceType.intValue() == 6) {
                    item.setSoPrice(item.getProYsj2());
                } else if (priceType.intValue() == 7) {
                    item.setSoPrice(item.getProYsj3());
                } else if (priceType.intValue() == 8) {
                    item.setSoPrice(item.getBatPoPrice());
                }
            }
        });
        List<RecallInfo> result = recallInfos.stream().sorted(Comparator.comparing(RecallInfo::getProSelfCode).thenComparing(RecallInfo::getBatExpiryDate, Comparator.nullsLast(String::compareTo))).collect(Collectors.toList());
        // 批发开单效期锁定（配置表查询  0-关，1-开）
        String para1 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(client, "WHOLESALE_VALIDITY_LOCK");
        if ("1".equals(para1) && CollectionUtils.isNotEmpty(result)) {
            String proSelfCode = "";
            String expiryDate = "";
            int index = 0;
            for (int i = 0; i < result.size(); i++) {
                RecallInfo recallInfo = result.get(i);
                if (!recallInfo.getProSelfCode().equals(proSelfCode)) {
                    recallInfo.setIsEdit("1");
                    recallInfo.setIndex(index);
                    index++;
                } else {
                    if ((StringUtils.isBlank(recallInfo.getBatExpiryDate()) && StringUtils.isNotBlank(expiryDate))) {
                        recallInfo.setIsEdit("1");
                        recallInfo.setIndex(index);
                        index++;
                    } else if (expiryDate.equals(recallInfo.getBatExpiryDate())) {
                        recallInfo.setIsEdit("1");
                        recallInfo.setIndex(index);
                        index++;
                    } else {
                        recallInfo.setIsEdit("0");
                    }
                }
                expiryDate = StringUtils.isBlank(recallInfo.getBatExpiryDate()) ? "" : recallInfo.getBatExpiryDate();
                proSelfCode = recallInfo.getProSelfCode();
            }
        } else {
            int index = 0;
            for (int i = 0; i < result.size(); i++) {
                RecallInfo recallInfo = result.get(i);
                recallInfo.setIsEdit("1");
                recallInfo.setIndex(index);
                index++;
            }
        }
        return ResultUtil.success(result);
    }

    /**
     * 批发销售查询列表
     *
     * @param querySalesOrderList
     * @return
     */
    @Override
    public Result querySalesOrderList(QuerySalesOrderList querySalesOrderList) {
        // 分页
        PageHelper.startPage(querySalesOrderList.getPageNum(), querySalesOrderList.getPageSize());
        List<SalesOrder> list = gaiaSoHeaderMapper.querySalesOrderList(querySalesOrderList);
        PageInfo<SalesOrder> pageInfo = new PageInfo<>(list);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 批发销售查询列表_主表
     *
     * @param vo
     * @return
     */
    @Override
    public Result querySalesOrderHeaderList(QuerySalesOrderHeaderVO vo) {
        // 分页
        PageHelper.startPage(vo.getPageNum(), vo.getPageSize());
        List<SalesOrderHeader> list = gaiaSoHeaderMapper.querySalesOrderHeaderList(vo);
        PageInfo<SalesOrderHeader> pageInfo = new PageInfo<>(list);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 关闭请货单
     *
     * @param gsrhBrId
     * @param gsrhVoucherId
     */
    @Override
    public void closeBill(String gsrhBrId, String gsrhVoucherId) {
        TokenUser user = feignService.getLoginInfo();
        GaiaSdReplenishH gaiaSdReplenishH = new GaiaSdReplenishH();
        gaiaSdReplenishH.setClient(user.getClient());
        gaiaSdReplenishH.setGsrhBrId(gsrhBrId);
        gaiaSdReplenishH.setGsrhVoucherId(gsrhVoucherId);
        // 开单标记-GSRH_DN_FLAG更新为1-已开单；GSRH_FLAG更新为2-关闭
        gaiaSdReplenishH.setGsrhDnFlag("1");
        gaiaSdReplenishH.setGsrhFlag("2");
        gaiaSdReplenishHMapper.updateByPrimaryKeySelective(gaiaSdReplenishH);
    }

    /**
     * 销售员集合
     *
     * @param supSite
     * @return
     */
    @Override
    public Result getSalesmanList(String supSite) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaWholesaleSalesman> gaiaWholesaleSalesmanList = gaiaWholesaleSalesmanMapper.getSalesmanList(user.getClient(), supSite);
        return ResultUtil.success(gaiaWholesaleSalesmanList);
    }

    @Override
    public Result queryGoodsPriceList(String proSite, String proSelfCode, List<String> proSelfCodeList) {
        if (StringUtils.isBlank(proSelfCode) && CollectionUtils.isEmpty(proSelfCodeList)) {
            throw new CustomResultException("商品编码不能为空");
        }
        TokenUser user = feignService.getLoginInfo();
        List<ProductDto> productList = gaiaProductBusinessMapper.queryGoodsPrice(user.getClient(), proSite, proSelfCode, proSelfCodeList);
        Map<String, List<Dictionary>> result = new HashMap<>();
        for (ProductDto product : productList) {
            List<Dictionary> dictionaryList = new ArrayList<>();
            if (product != null) {
                if (product.getProLsj() != null && product.getProLsj().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getProLsj().toString(), CommonEnum.SalesPriceType.PRO_LSJ.getName()));
                }
                if (product.getProHyj() != null && product.getProHyj().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getProHyj().toString(), CommonEnum.SalesPriceType.PRO_HYJ.getName()));
                }
                if (product.getProGpj() != null && product.getProGpj().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getProGpj().toString(), CommonEnum.SalesPriceType.PRO_GPJ.getName()));
                }
                if (product.getProGlj() != null && product.getProGlj().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getProGlj().toString(), CommonEnum.SalesPriceType.PRO_GLJ.getName()));
                }
                if (product.getProYsj1() != null && product.getProYsj1().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getProYsj1().toString(), CommonEnum.SalesPriceType.PRO_YSJ1.getName()));
                }
                if (product.getProYsj2() != null && product.getProYsj2().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getProYsj2().toString(), CommonEnum.SalesPriceType.PRO_YSJ2.getName()));
                }
                if (product.getProYsj3() != null && product.getProYsj3().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getProYsj3().toString(), CommonEnum.SalesPriceType.PRO_YSJ3.getName()));
                }
                if (product.getBatPoPrice() != null && product.getBatPoPrice().compareTo(BigDecimal.ZERO) != 0) {
                    dictionaryList.add(new Dictionary(product.getBatPoPrice().toString(), CommonEnum.SalesPriceType.BAT_PO_PRICE.getName()));
                }
            }
            result.put(product.getProSelfCode(), dictionaryList);
        }
        return ResultUtil.success(result);
    }

    /**
     * 批发销售审批详情
     *
     * @param wholeSaleApproveDTO
     * @return
     */
    @Override
    public Result queryWholesaleApproveList(WholeSaleApproveDTO wholeSaleApproveDTO) {
        TokenUser user = feignService.getLoginInfo();
        List<WholeSaleApproveVo> wholeSaleApproveVoList = new ArrayList<>();
        // 检查审批配置表中具体的人员或者岗位
        GaiaWmsShenPiPeiZhiDTO gaiaWmsShenPiPeiZhiDTO = gaiaSoHeaderMapper.queryUserRole(user.getClient(), wholeSaleApproveDTO.getSiteCode(), 2);
        // 配置表人员存在该人员
        if (gaiaWmsShenPiPeiZhiDTO != null && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())
                && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
            // 判断审批人类型为角色  0-角色  1-人员
            if ("0".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                // 查询权限分配表是否分配该审批人权限
                int count = gaiaAuthconfiDataMapper.selectUserAuthority(user.getClient(), gaiaWmsShenPiPeiZhiDTO.getWmSpr1(), user.getUserId());
                // 无权限则返回空
                if (count == 0) {
                    return ResultUtil.success(new PageInfo<>(wholeSaleApproveVoList));
                }
            }
            if ("1".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                // 查询权限分配表是否分配该审批人权限
                // 无权限则返回空
                if (!user.getUserId().equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
                    return ResultUtil.success(new PageInfo<>(wholeSaleApproveVoList));
                }
            }
            wholeSaleApproveDTO.setClient(user.getClient());
            wholeSaleApproveDTO.setSoType("SOR");
            // 分页
            PageHelper.startPage(wholeSaleApproveDTO.getPageNum(), wholeSaleApproveDTO.getPageSize());
            // 查询销售订单表
            wholeSaleApproveVoList = gaiaSoHeaderMapper.queryWholesaleApproveList(wholeSaleApproveDTO);
            PageInfo<WholeSaleApproveVo> pageInfo = new PageInfo<>(wholeSaleApproveVoList);
            List<String> soIds = pageInfo.getList().stream().map(WholeSaleApproveVo::getSoId).collect(Collectors.toList());
            // 查询销售订单明细
            List<SoItemVo> gaiaSoItems = gaiaSoItemMapper.querySoItemTotal(user.getClient(), soIds, wholeSaleApproveDTO.getSiteCode());
            if (CollectionUtils.isNotEmpty(pageInfo.getList()) && CollectionUtils.isNotEmpty(gaiaSoItems)) {
                List<WholeSaleApproveVo> result = pageInfo.getList().stream()
                        .map(wholeSaleApproveVo -> gaiaSoItems.stream()
                                .filter(gaiaSoItem -> wholeSaleApproveVo.getSoId().equals(gaiaSoItem.getSoId()))
                                .findFirst()
                                .map(gaiaSoItem -> {
                                    // 单据行数
                                    wholeSaleApproveVo.setSoLine(gaiaSoItem.getSoLineNo());
                                    // 单据总数量
                                    wholeSaleApproveVo.setSoCount(gaiaSoItem.getSoQty());
                                    // 单据总金额
                                    wholeSaleApproveVo.setSoTotal(gaiaSoItem.getSoLineAmt());
                                    return wholeSaleApproveVo;
                                }).orElse(wholeSaleApproveVo))
                        .collect(Collectors.toList());
                pageInfo.setList(result);
            }
            return ResultUtil.success(pageInfo);
        }
        return ResultUtil.success(new PageInfo<>(wholeSaleApproveVoList));
    }

    /**
     * 批发退货审批详情
     *
     * @param wholeSaleApproveDTO
     * @return
     */
    @Override
    public Result queryWholesaleReturnsList(WholeSaleApproveDTO wholeSaleApproveDTO) {
        TokenUser user = feignService.getLoginInfo();
        List<WholeSaleApproveVo> wholeSaleReturnsVoList = new ArrayList<>();
        // 检查审批配置表中具体的人员或者岗位
        GaiaWmsShenPiPeiZhiDTO gaiaWmsShenPiPeiZhiDTO = gaiaSoHeaderMapper.queryUserRole(user.getClient(), wholeSaleApproveDTO.getSiteCode(), 3);
        // 配置表人员存在该人员
        if (gaiaWmsShenPiPeiZhiDTO != null && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())
                && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
            // 判断审批人类型为角色  0-角色  1-人员
            if ("0".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                // 查询权限分配表是否分配该审批人权限
                int count = gaiaAuthconfiDataMapper.selectUserAuthority(user.getClient(), gaiaWmsShenPiPeiZhiDTO.getWmSpr1(), user.getUserId());
                // 无权限则返回空
                if (count == 0) {
                    return ResultUtil.success(new PageInfo<>(wholeSaleReturnsVoList));
                }
            }
            if ("1".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                // 查询权限分配表是否分配该审批人权限
                // 无权限则返回空
                if (!user.getUserId().equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
                    return ResultUtil.success(new PageInfo<>(wholeSaleReturnsVoList));
                }
            }
            wholeSaleApproveDTO.setClient(user.getClient());
            wholeSaleApproveDTO.setSoType("SRE");
            // 分页
            PageHelper.startPage(wholeSaleApproveDTO.getPageNum(), wholeSaleApproveDTO.getPageSize());
            // 查询退货订单表
            wholeSaleReturnsVoList = gaiaSoHeaderMapper.queryWholesaleApproveList(wholeSaleApproveDTO);
            PageInfo<WholeSaleApproveVo> pageInfo = new PageInfo<>(wholeSaleReturnsVoList);
            List<String> soIds = pageInfo.getList().stream().map(WholeSaleApproveVo::getSoId).collect(Collectors.toList());
            // 查询退货订单明细
            List<SoItemVo> gaiaSoItems = gaiaSoItemMapper.querySoItemTotal(user.getClient(), soIds, wholeSaleApproveDTO.getSiteCode());
            if (CollectionUtils.isNotEmpty(pageInfo.getList()) && CollectionUtils.isNotEmpty(gaiaSoItems)) {
                List<WholeSaleApproveVo> result = pageInfo.getList().stream()
                        .map(wholeSaleReturnsVo -> gaiaSoItems.stream()
                                .filter(gaiaSoItem -> wholeSaleReturnsVo.getSoId().equals(gaiaSoItem.getSoId()))
                                .findFirst()
                                .map(gaiaSoItem -> {
                                    // 退货品项
                                    wholeSaleReturnsVo.setSoLine(gaiaSoItem.getProCount());
                                    // 退货总数量
                                    wholeSaleReturnsVo.setSoCount(gaiaSoItem.getSoQty());
                                    // 退货总金额
                                    wholeSaleReturnsVo.setSoTotal(gaiaSoItem.getSoLineAmt());
                                    return wholeSaleReturnsVo;
                                }).orElse(null))
                        .collect(Collectors.toList());
                pageInfo.setList(result);
            }
            return ResultUtil.success(pageInfo);
        }
        return ResultUtil.success(new PageInfo<>(wholeSaleReturnsVoList));
    }

    /**
     * 更新批发-销售/退货-审批状态 (0-不需审批，1-未审批，2-已审批 9–已拒绝)
     *
     * @param soId
     * @param soApproveStatus
     * @param siteCode
     */
    @Override
    public void updateApproveStatus(String soId, String soApproveStatus, String siteCode) {
        TokenUser user = feignService.getLoginInfo();
        gaiaSoHeaderMapper.updateApproveStatus(user.getClient(), soId, soApproveStatus, siteCode, user.getUserId(), DateUtils.getCurrentDateStr("yyyyMMdd"));
    }

    /**
     * 批发销售订单明细
     *
     * @param client 加盟商
     * @param soId   订单号
     * @return
     */
    @Override
    public Result getSalesOrderInfo(String client, String soId) {
        // 销售订单主表
        SalesOrder salesOrder = gaiaSoHeaderMapper.querySalesOrder(client, soId);
        if (salesOrder == null) {
            throw new CustomResultException(ResultEnum.E0121);
        }
        // 是否已开单
        Integer cnt = gaiaSoHeaderMapper.selectWmsDiaoboZCnt(client, soId);
        salesOrder.setEditFlg(cnt.toString());
        // 销售订单明细
        List<SalesOrderItem> list = gaiaSoItemMapper.querySalesDetails(client, soId);
        salesOrder.setDetailList(list);
        List<String> proCodes = list.stream().map(SalesOrderItem::getSoProCode).collect(Collectors.toList());
        List<SalesOrderItem> wmCkjList = gaiaSoItemMapper.queryProWmCkj(client, proCodes, list.get(0).getSoCustomerId());
        if (CollectionUtils.isNotEmpty(list) && CollectionUtils.isNotEmpty(wmCkjList)) {
            // 商品上次开票价
            List<SalesOrderItem> result = list.stream()
                    .map(salesOrderItem -> wmCkjList.stream()
                            .filter(wmCkj -> salesOrderItem.getSoProCode().equals(wmCkj.getSoProCode()))
                            .findFirst()
                            .map(wmCkj -> {
                                salesOrderItem.setWmCkj(wmCkj.getWmCkj());
                                return salesOrderItem;
                            }).orElse(salesOrderItem))
                    .collect(Collectors.toList());
            salesOrder.setDetailList(result);
        }
        return ResultUtil.success(salesOrder);
    }


    /**
     * 批发退货订单明细
     *
     * @param soId 订单号
     * @return
     */
    @Override
    public Result getSalesOrderReturnsInfo(String soId) {
        TokenUser user = feignService.getLoginInfo();
        // 批发退货订单主表
        SalesOrder salesOrder = gaiaSoHeaderMapper.querySalesOrder(user.getClient(), soId);
        if (salesOrder == null) {
            throw new CustomResultException(ResultEnum.E0121);
        }
        // 批发退货订单明细
        List<SalesOrderItem> list = gaiaSoItemMapper.querySalesDetails(user.getClient(), soId);
        salesOrder.setDetailList(list);
        return ResultUtil.success(salesOrder);
    }

    @Override
    public void updateProYsj(String proSelfCode, String proSite, String type, BigDecimal proYsj) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaProductChange> changes = new ArrayList<>();
        List<GaiaProductBusiness> productBusinesses = new ArrayList<>();
        if (StringUtils.isNotBlank(type)) {
            List<GaiaDcData> dcListToUpdate = gaiaProductBasicMapper.getDcListToUpdate(user.getClient(), user.getUserId());
            GaiaProductBusiness productBusiness = new GaiaProductBusiness();
            productBusiness.setClient(user.getClient());
            productBusiness.setProSite(proSite);
            productBusiness.setProSelfCode(proSelfCode);
            productBusinesses.add(productBusiness);
            // 如果地点是DC
            GaiaDcData gaiaDcData = dcListToUpdate.stream().filter(Objects::nonNull)
                    .filter(item -> item.getDcCode().equals(productBusiness.getProSite()))
                    .findFirst().orElse(null);
            if (!org.springframework.util.ObjectUtils.isEmpty(gaiaDcData)) {
                // # 配送中心连锁总部下的门店 + "门店主数据"地点下的门店
                List<String> stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(productBusiness.getClient(), productBusiness.getProSite());
                stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                    GaiaProductBusiness business = new GaiaProductBusiness();
                    business.setClient(user.getClient());
                    business.setProSelfCode(proSelfCode);
                    business.setProSite(stoCode);
                    productBusinesses.add(business);
                });
            }
            if (CollectionUtils.isNotEmpty(productBusinesses)) {
                productBusinesses.forEach(item -> {
                    GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                    GaiaProductChange productChange = new GaiaProductChange();
                    gaiaProductBusinessKey.setClient(item.getClient());
                    gaiaProductBusinessKey.setProSite(item.getProSite());
                    gaiaProductBusinessKey.setProSelfCode(item.getProSelfCode());
                    GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);
                    if (gaiaProductBusiness != null) {
                        boolean bool = proYsj != null && !Objects.equals(String.valueOf(proYsj), "");
                        if ("1".equalsIgnoreCase(type)) {
                            item.setProYsj1(bool ? proYsj : null);
                            item.setProYsj2(gaiaProductBusiness.getProYsj2());
                            item.setProYsj3(gaiaProductBusiness.getProYsj3());
                            productChange.setProChangeField("proYsj1");
                            productChange.setProChangeFrom(gaiaProductBusiness.getProYsj1() == null ? null : String.valueOf(gaiaProductBusiness.getProYsj1()));
                        } else if ("2".equalsIgnoreCase(type)) {
                            item.setProYsj2(bool ? proYsj : null);
                            item.setProYsj1(gaiaProductBusiness.getProYsj1());
                            item.setProYsj3(gaiaProductBusiness.getProYsj3());
                            productChange.setProChangeField("proYsj2");
                            productChange.setProChangeFrom(gaiaProductBusiness.getProYsj2() == null ? null : String.valueOf(gaiaProductBusiness.getProYsj2()));
                        } else {
                            item.setProYsj3(bool ? proYsj : null);
                            item.setProYsj2(gaiaProductBusiness.getProYsj2());
                            item.setProYsj1(gaiaProductBusiness.getProYsj1());
                            productChange.setProChangeField("proYsj3");
                            productChange.setProChangeFrom(gaiaProductBusiness.getProYsj3() == null ? null : String.valueOf(gaiaProductBusiness.getProYsj3()));
                        }
                        productChange.setProChangeTo(bool ? String.valueOf(proYsj) : null);
                        // 记录到change表
                        productChange.setClient(gaiaProductBusiness.getClient());
                        productChange.setProSite(gaiaProductBusiness.getProSite());
                        productChange.setProSelfCode(gaiaProductBusiness.getProSelfCode());
                        productChange.setProCommonname(gaiaProductBusiness.getProCommonname());
                        productChange.setProSpecs(gaiaProductBusiness.getProSpecs());
                        productChange.setProFactoryName(gaiaProductBusiness.getProFactoryName());
                        productChange.setProRegisterNo(gaiaProductBusiness.getProRegisterNo());
                        productChange.setProChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                        productChange.setProChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                        productChange.setProChangeUser(user.getUserId());
                        productChange.setProSfsp("0");
                        productChange.setProSfty("0");
                        changes.add(productChange);

                    }
                });
                List<GaiaProductBusiness> distinctClass = productBusinesses.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getProSite() + ";" + o.getProSelfCode()))), ArrayList::new));
                List<GaiaProductChange> distinctChangeClass = changes.stream().collect(Collectors.collectingAndThen(Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(o -> o.getClient() + ";" + o.getProSite() + ";" + o.getProSelfCode()))), ArrayList::new));
                gaiaProductBusinessMapper.updateBatchPro(distinctClass);
                gaiaProductChangeMapper.saveBatch(distinctChangeClass);
            }
        }
    }

    /**
     * 送达方验证
     *
     * @return
     */
    @Override
    public Result checkCustomer(GaiaSoHeader entity) {

        TokenUser user = feignService.getLoginInfo();
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(user.getClient());
        gaiaDcDataKey.setDcCode(entity.getSoCompanyCode());
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);

        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        // 检查 客户主数据 所有证照的效期 ：小于60天-提示，不报错；过期-报错。
        if (gaiaDcData != null && StringUtils.isNotBlank(entity.getSoCustomer2())) {
            GaiaCustomerBusiness gaiaCustomerBusiness2 =
                    gaiaCustomerBusinessMapper.getCustomerBusiness(user.getClient(), entity.getSoCompanyCode(), entity.getSoCustomer2());
            // 是否管控证照效期：1-是
            if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1") && gaiaCustomerBusiness2 != null) {
                // CUS_CREDIT_DATE
                dateCheck(gaiaCustomerBusiness2.getCusCreditDate(), "营业执照", warning, error);
                // CUS_LICENCE_VALID             许可证有效期至
                dateCheck(gaiaCustomerBusiness2.getCusLicenceValid(), "许可证", warning, error);
                // CUS_SPXKZ_DATE					食品许可证有效期
                dateCheck(gaiaCustomerBusiness2.getCusSpxkzDate(), "食品许可证", warning, error);
                //CUS_YLQXXKZ_DATE					医疗器械经营许可证有效期
                dateCheck(gaiaCustomerBusiness2.getCusYlqxxkzDate(), "医疗器械经营许可证", warning, error);
                //CUS_GMP_DATE					GMP有效期
                dateCheck(gaiaCustomerBusiness2.getCusGmpDate(), "GMP", warning, error);
                //CUS_YLJGXKZ_DATE					医疗机构执业许可证书有效期
                dateCheck(gaiaCustomerBusiness2.getCusYljgxkzDate(), "医疗机构执业许可证书", warning, error);
                //CUS_GSP_DATES					GSP有效期
                dateCheck(gaiaCustomerBusiness2.getCusGspDates(), "GSP", warning, error);
                // CUS_YWYJSRQ
                dateCheck(gaiaCustomerBusiness2.getCusYwyjsrq(), "业务员（采购员）结束日期", warning, error);
                // CUS_SHRJSRQ
                dateCheck(gaiaCustomerBusiness2.getCusShrjsrq(), "收货人/提货人结束日期", warning, error);
                // CUS_ZBXYYXQ
                dateCheck(gaiaCustomerBusiness2.getCusZbxyyxq(), "质保协议", warning, error);
                // CUS_GXHTYXQ
                dateCheck(gaiaCustomerBusiness2.getCusGxhtyxq(), "购销合同", warning, error);
                // CUS_FRWTSXQ 法人委托书
                dateCheck(gaiaCustomerBusiness2.getCusFrwtsxq(), "法人委托书", warning, error);
            }
        }
        return ResultUtil.success(error);
    }

    /**
     * 销售订单修改
     *
     * @param updateSalesOrder
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updateSalesOrder(UpdateSalesOrder updateSalesOrder) {
        // 销售明细
        List<UpdateSalesOrderItem> detailList = updateSalesOrder.getDetailList();
        // 明细数据不存在
        if (CollectionUtils.isEmpty(detailList)) {
            throw new CustomResultException(ResultEnum.E0122);
        }
        // 明细数据验证
        for (UpdateSalesOrderItem updateSalesOrderItem : detailList) {
            ValidationResult result = ValidateUtil.validateEntity(updateSalesOrderItem);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
        }
        // 主表主键查询
        GaiaSoHeaderKey gaiaSoHeaderKey = new GaiaSoHeaderKey();
        gaiaSoHeaderKey.setClient(updateSalesOrder.getClient());
        gaiaSoHeaderKey.setSoId(updateSalesOrder.getSoId());
        com.gov.purchase.entity.GaiaSoHeader gaiaSoHeader = gaiaSoHeaderMapper.selectByPrimaryKey(gaiaSoHeaderKey);
        if (gaiaSoHeader == null) {
            throw new CustomResultException(ResultEnum.E0121);
        }
        // 销售订单主表更新
        // 支付条款
        gaiaSoHeader.setSoPaymentId(updateSalesOrder.getSoPaymentId());
        // 抬头备注
        gaiaSoHeader.setSoHeadRemark(updateSalesOrder.getSoHeadRemark());
        // 创建人
//        gaiaSoHeader.setSoCreateBy(updateSalesOrder.getSoCreateBy());
        gaiaSoHeaderMapper.updateByPrimaryKey(gaiaSoHeader);
        // 明细数据
        for (UpdateSalesOrderItem updateSalesOrderItem : detailList) {
            // 行号
            String soLineNo = updateSalesOrderItem.getSoLineNo();
            GaiaSoItem gaiaSoItem = null;
            // 原数据
            if (StringUtils.isNotBlank(soLineNo)) {
                GaiaSoItemKey gaiaSoItemKey = new GaiaSoItemKey();
                gaiaSoItemKey.setClient(updateSalesOrder.getClient());
                gaiaSoItemKey.setSoId(updateSalesOrder.getSoId());
                gaiaSoItemKey.setSoLineNo(soLineNo);
                gaiaSoItem = gaiaSoItemMapper.selectByPrimaryKey(gaiaSoItemKey);
                if (gaiaSoItem == null) {
                    throw new CustomResultException(ResultEnum.E0121);
                }
                // 删除数据
                if ("1".equals(updateSalesOrderItem.getSoLineDelete())) {
                    GaiaSoItem entity = new GaiaSoItem();
                    entity.setClient(updateSalesOrder.getClient());
                    entity.setSoId(updateSalesOrder.getSoId());
                    entity.setSoLineNo(soLineNo);
                    entity.setSoLineDelete(updateSalesOrderItem.getSoLineDelete());
                    gaiaSoItemMapper.updateByPrimaryKeySelective(entity);
                    continue;
                }
            } else {
                gaiaSoItem = new GaiaSoItem();
                // 加盟商
                gaiaSoItem.setClient(updateSalesOrder.getClient());
                // 销售订单号
                gaiaSoItem.setSoId(updateSalesOrder.getSoId());
                // 订单行号
                List<SalesOrderItem> salesOrderItemList = gaiaSoItemMapper.querySalesDetails(updateSalesOrder.getClient(), updateSalesOrder.getSoId());
                gaiaSoItem.setSoLineNo(CollectionUtils.isEmpty(salesOrderItemList) ? "1" : String.valueOf(salesOrderItemList.size() + 1));
            }

            // 商品编码
            gaiaSoItem.setSoProCode(updateSalesOrderItem.getSoProCode());
            // 数量
            gaiaSoItem.setSoQty(updateSalesOrderItem.getSoQty());
            // 单位
            gaiaSoItem.setSoUnit(updateSalesOrderItem.getSoUnit());
            // 单价
            gaiaSoItem.setSoPrice(updateSalesOrderItem.getSoPrice());
            // 行总价
            gaiaSoItem.setSoLineAmt(updateSalesOrderItem.getSoLineAmt());
            // 地点编码
            gaiaSoItem.setSoSiteCode(updateSalesOrderItem.getSoSiteCode());
            // 库位
            gaiaSoItem.setSoLocationCode(updateSalesOrderItem.getSoLocationCode());
            // 批次
            gaiaSoItem.setSoBatch(updateSalesOrderItem.getSoBatch());
            // 税率
            gaiaSoItem.setSoRate(updateSalesOrderItem.getSoRate());
            // 计划发货日期
            gaiaSoItem.setSoDeliveryDate(updateSalesOrderItem.getSoDeliveryDate());
            // 行备注
            gaiaSoItem.setSoLineRemark(updateSalesOrderItem.getSoLineRemark());
            // 删除标记  0-否，1-是
            gaiaSoItem.setSoLineDelete(updateSalesOrderItem.getSoLineDelete());
            // 交货已完成标记  0-否，1-是
            gaiaSoItem.setSoCompleteFlag(updateSalesOrderItem.getSoCompleteFlag());
            gaiaSoItem.setSoBatchNo(updateSalesOrderItem.getSoBatchNo());
            if (StringUtils.isNotBlank(soLineNo)) {
                gaiaSoItemMapper.updateByPrimaryKey(gaiaSoItem);
            } else {
                gaiaSoItemMapper.insertSelective(gaiaSoItem);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 批发销售查询列表导出
     *
     * @param querySalesOrderList
     * @return
     */
    @Override
    public Result exportSalesOrderList(QuerySalesOrderList querySalesOrderList) {
        try {
            // 批发销售查询列表
            List<SalesOrder> salesOrderList = gaiaSoHeaderMapper.querySalesOrderList(querySalesOrderList);
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(salesOrderList) && i < salesOrderList.size(); i++) {
                // 明细数据
                SalesOrder salesOrder = salesOrderList.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                // 序号
                item.add(String.valueOf(i + 1));
                // 加盟商
                item.add(concat(salesOrder.getClient(), salesOrder.getFrancName()));
                // 公司
                item.add(salesOrder.getDcNameLine());
                // 创建人
                item.add(concat(salesOrder.getSoCreateBy(), salesOrder.getSoCreateByName()));
                // 销售订单号
                item.add(salesOrder.getSoId());
                // 订单行号
                item.add(salesOrder.getSoLineNo());
                // 订单类型
                item.add(salesOrder.getSoType());
                // 订单类型名称
                item.add(salesOrder.getOrdTypeCesc());
                // 订单日期
                item.add(formatDate(salesOrder.getSoDate()));
                // 客户编码
                item.add(salesOrder.getSoCustomerId());
                // 客户名称
                item.add(salesOrder.getCusName());
                // 收款条款
                item.add(concat(salesOrder.getSoPaymentId(), salesOrder.getPayTypeDesc()));
                // 审批状态
                Map<Object, String> map = EnumUtils.EnumToMap(CommonEnum.PoApproveStatus.class);
                item.add(map.get(salesOrder.getSoApproveStatus()));
                // 订单总金额
                item.add(salesOrder.getSoLineAmtOrder().stripTrailingZeros().toPlainString());
                // 抬头备注
                item.add(salesOrder.getSoHeadRemark());
                // 商品编码
                item.add(salesOrder.getSoProCode());
                // 商品名称
                item.add(salesOrder.getProName());
                // 生产厂家
                item.add(salesOrder.getProFactoryName());
                // 规格
                item.add(salesOrder.getProSpecs());
                // 产地
                item.add(salesOrder.getProPlace());
                // 订单数量
                item.add(salesOrder.getSoQty().stripTrailingZeros().toPlainString());
                // 单位
                item.add(salesOrder.getUnitName());
                // 单价
                item.add(salesOrder.getSoPrice().stripTrailingZeros().toPlainString());
                // 税率
                item.add(salesOrder.getTaxCodeValue());
                // 行总价
                item.add(salesOrder.getSoPrice().stripTrailingZeros().toPlainString());
                // 地点
                item.add(concat(salesOrder.getSoSiteCode(), salesOrder.getDcNameLine()));
                // 库位
                Dictionary dicLocationCode = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE, salesOrder.getSoLocationCode());
                item.add(null != dicLocationCode ? concat(dicLocationCode.getValue(), dicLocationCode.getLabel()) : "");
                // 批次
                item.add(salesOrder.getSoBatch());
                // 交货日期
                item.add(formatDate(salesOrder.getSoDeliveryDate()));
                // 行备注
                item.add(salesOrder.getSoLineRemark());
                // 删除标记
                Dictionary dictionaryDel = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.NO_YES, salesOrder.getSoLineDelete());
                item.add(null != dictionaryDel ? dictionaryDel.getLabel() : "");
                // 交货已完成
                Dictionary dictionaryRlag = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.NO_YES, salesOrder.getSoLineDelete());
                item.add(null != dictionaryRlag ? dictionaryRlag.getLabel() : "");
                // 已交货数量
                item.add(salesOrder.getSoDeliveredQty().stripTrailingZeros().toPlainString());
                // 已交货金额
                item.add(salesOrder.getSoDeliveredAmt().stripTrailingZeros().toPlainString());
                // 已开票金额
                item.add(salesOrder.getSoInvoiceAmt().stripTrailingZeros().toPlainString());
                // 生产批号
                item.add(salesOrder.getSoBatchNo());
                // 生产日期
                item.add(salesOrder.getBatProductDate());
                // 有效期
                item.add(salesOrder.getBatExpiryDate());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("批发销售");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;

        } catch (Exception e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 字符串连接
     *
     * @param val
     * @return
     */
    private String concat(String... val) {
        StringBuilder result = new StringBuilder();
        if (ObjectUtils.isEmpty(val)) {
            return result.toString();
        }
        for (int i = 0; i < val.length; i++) {
            if (StringUtils.isNotBlank(val[i])) {
                result.append("-").append(val[i]);
            }
        }
        if (result.length() > 0) {
            result.deleteCharAt(0);
        }
        return result.toString();
    }

    /**
     * 日期格式化
     *
     * @param date
     * @return
     */
    private String formatDate(String date) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        if (date.length() != 8) {
            return date;
        }
        StringBuilder sb = new StringBuilder(date);
        sb.insert(4, "/");
        sb.insert(7, "/");
        return sb.toString();
    }

    /**
     * 业务数据表头
     */
    private String[] headList = {
            "序号",
            "加盟商",
            "公司",
            "创建人",
            "销售订单号",
            "订单行号",
            "订单类型",
            "订单类型名称",
            "订单日期",
            "客户编码",
            "客户名称",
            "收款条件",
            "审批状态",
            "订单总金额",
            "抬头备注",
            "商品编码",
            "商品名称",
            "生产厂家",
            "规格",
            "产地",
            "订单数量",
            "单位",
            "单价",
            "税率",
            "行总价",
            "地点",
            "库位",
            "批次",
            "交货日期",
            "行备注",
            "删除标记",
            "交货已完成",
            "已交货数量",
            "已交货金额",
            "已开票金额",
            "生产批号",
            "生产日期",
            "有效期",
    };


    /**
     * 调取请货单列表
     *
     * @param vo
     * @return
     */
    @Override
    public PageInfo<GoodsOrderDTO> getRequestGoodsOrderList(GetRequestGoodsOrderListVO vo) {
        TokenUser user = vo.getUser() == null ? feignService.getLoginInfo() : vo.getUser();
        List<GoodsOrderDTO> list = new ArrayList<>();
        // 货主下的商品明细
        if (StringUtils.isNotBlank(vo.getSoOwner())) {
            List<GaiaWholesaleOwner> owners = gaiaWholesaleOwnerMapper.queryWholesaleConsignor(user.getClient(), null, null, vo.getSoOwner());
            if (CollectionUtils.isNotEmpty(owners)) {
                List<OwnerProductVO> products = gaiaOwnerProductMapper.selectProByDcCodeAndOwner(user.getClient(), owners.get(0).getGwoSite(), vo.getSoOwner());
                if (CollectionUtils.isNotEmpty(products)) {
                    List<String> proCodes = products.stream().map(OwnerProductVO::getGopProCode).collect(Collectors.toList());
                    PageHelper.startPage(vo.getPageNum(), vo.getPageSize());
                    list = gaiaSoHeaderMapper.getRequestGoodsOrderList(user.getClient(), vo.getGsrhBrId(), vo.getGsrhDateStart(), vo.getGsrhDateEnd(), proCodes, owners.get(0).getGwoProRule());
                }
            }
        } else {
            PageHelper.startPage(vo.getPageNum(), vo.getPageSize());
            list = gaiaSoHeaderMapper.getRequestGoodsOrderList(user.getClient(), vo.getGsrhBrId(), vo.getGsrhDateStart(), vo.getGsrhDateEnd(), null, null);
        }
        list.stream().filter(Objects::nonNull).forEach(item -> {
            List<RecallInfo> recallInfoList = gaiaSoHeaderMapper.queryGoodsListByReplenish(item);
            // 商品编号
            List<String> proSelfCodeList = recallInfoList.stream().map(RecallInfo::getProSelfCode).collect(Collectors.toList());
            // batch info 单价
            List<RecallInfo> priceList = null;
            if (vo.getPriceType() != null && vo.getPriceType().intValue() == 8) {
                priceList = gaiaSoHeaderMapper.selectBatPriceByPro(user.getClient(), proSelfCodeList);
            }
            for (RecallInfo recallInfo : recallInfoList) {
                if (vo.getPriceType() != null) {
                    if (vo.getPriceType().intValue() == 1) {
                        if (recallInfo.getProLsj() == null) {
                            recallInfo.setPriceColor("red");
                        } else {
                            recallInfo.setSoPrice(recallInfo.getProLsj());
                        }
                    } else if (vo.getPriceType().intValue() == 2) {
                        if (recallInfo.getProHyj() == null) {
                            recallInfo.setPriceColor("red");
                        } else {
                            recallInfo.setSoPrice(recallInfo.getProHyj());
                        }
                    } else if (vo.getPriceType().intValue() == 3) {
                        if (recallInfo.getProGpj() == null) {
                            recallInfo.setPriceColor("red");
                        } else {
                            recallInfo.setSoPrice(recallInfo.getProGpj());
                        }
                    } else if (vo.getPriceType().intValue() == 4) {
                        if (recallInfo.getProGlj() == null) {
                            recallInfo.setPriceColor("red");
                        } else {
                            recallInfo.setSoPrice(recallInfo.getProGlj());
                        }
                    } else if (vo.getPriceType().intValue() == 5) {
                        if (recallInfo.getProYsj1() == null) {
                            recallInfo.setPriceColor("red");
                        } else {
                            recallInfo.setSoPrice(recallInfo.getProYsj1());
                        }
                    } else if (vo.getPriceType().intValue() == 6) {
                        if (recallInfo.getProYsj2() == null) {
                            recallInfo.setPriceColor("red");
                        } else {
                            recallInfo.setSoPrice(recallInfo.getProYsj2());
                        }
                    } else if (vo.getPriceType().intValue() == 7) {
                        if (recallInfo.getProYsj3() == null) {
                            recallInfo.setPriceColor("red");
                        } else {
                            recallInfo.setSoPrice(recallInfo.getProYsj3());
                        }
                    } else if (vo.getPriceType().intValue() == 8) {
                        RecallInfo priceItem = priceList.stream().filter(t -> t.getProSelfCode().equals(recallInfo.getProSelfCode())).findFirst().orElse(null);
                        if (priceItem != null) {
                            if (priceItem.getBatPoPrice() == null) {
                                recallInfo.setPriceColor("red");
                            } else {
                                recallInfo.setSoPrice(priceItem.getBatPoPrice());
                            }
                        } else {
                            recallInfo.setPriceColor("red");
                        }
                    }
                }
                if (recallInfo.getSoPrice() == null) {
                    recallInfo.setPriceColor(null);
                }
            }
            item.setRecallInfoList(recallInfoList);
        });
        return new PageInfo<>(list);
    }

    /**
     * 校验外部客户往来余额：余额+本次补货金额是否超出额度限制
     */
    private void checkCreditQuotaAndZqts(GaiaCustomerBusiness gaiaCustomerBusiness, GaiaSoHeader entity) {
        //汇总本次批发金额
        BigDecimal orderTotalAmt = entity.getSoItemList().stream().map(SoItem::getSoLineAmt).reduce(BigDecimal.ZERO, BigDecimal::add);
        //批量开单创建校验信用金额和账期控制
        if (gaiaCustomerBusiness == null) {
            return;
        }
        //启用信用管理
        if (Constant.IS_YES.equals(gaiaCustomerBusiness.getCusCreditFlag()) && null != gaiaCustomerBusiness.getCusCreditQuota()) {
            BigDecimal totalAmt = BigDecimal.valueOf(0);
            BigDecimal cusArAmt = gaiaCustomerBusiness.getCusArAmt();
            if (null == gaiaCustomerBusiness.getCusArAmt()) {
                cusArAmt = BigDecimal.ZERO;
            }
            //如果余额+本次汇总配送金额>信用额度
            totalAmt = totalAmt.add(orderTotalAmt).add(cusArAmt);
            if (totalAmt.compareTo(gaiaCustomerBusiness.getCusCreditQuota()) > 0) {
                //总授信额度A元，欠款金额B元，剩余额度C元，本单金额D元，已不足！
                //A：主数据里的授信额度
                //B：主数据里的应收余额
                //C：A-B
                //D：本单金额
                BigDecimal arrearsAmt = gaiaCustomerBusiness.getCusCreditQuota().subtract(cusArAmt).setScale(4, BigDecimal.ROUND_HALF_UP);
                throw new CustomResultException("总授信额度为" + gaiaCustomerBusiness.getCusCreditQuota() + "元，欠款金额为" + cusArAmt + "元，剩余额度为" + arrearsAmt + "元，本次金额" + orderTotalAmt + "元，已不足");
            }
        }
        if (CustomerZqType.ZQ_DAYS.type.equals(gaiaCustomerBusiness.getCusZqlx())) {
            //如果是账期类型，检查当前日期往前推账期天数，如存在往来余额 > 0,报错
            if (null != gaiaCustomerBusiness.getCusZqts()) {
                Date date = DateUtils.addDate(new Date(), -gaiaCustomerBusiness.getCusZqts());
                String zqDate = DateUtils.format(date, "yyyyMMdd");
                BigDecimal amount = gaiaWmsDiaoboZMapper.getZqAmount(gaiaCustomerBusiness.getClient(), gaiaCustomerBusiness.getCusSelfCode(), zqDate);
                if (BigDecimal.ZERO.compareTo(amount) < 0) {
                    throw new CustomResultException(String.format("账期天数为%s天，已有%s元超过账期尚未付款，请付款后再下单", gaiaCustomerBusiness.getCusZqts(), amount));
                }
            }
        } else if (CustomerZqType.ZQ_FIXED.type.equals(gaiaCustomerBusiness.getCusZqlx())) {
            //如果是固定账期，并且欠款标志为0，报错提示
            if ("0".equals(gaiaCustomerBusiness.getCusArrearsFlag())) {
                throw new CustomResultException("不允许客户批发订单创建");
            }
        }
    }
}
