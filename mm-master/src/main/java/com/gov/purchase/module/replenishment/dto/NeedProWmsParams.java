package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.common.entity.Pageable;
import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.09.26
 */
@Data
public class NeedProWmsParams extends Pageable {

    /**
     * 配送中心
     */
    private String dcCode;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 仓库库存
     */
    private String dcStockType;

    /**
     * 仓库库存
     */
    private BigDecimal dcStockNum;

    /**
     * 门店库存
     */
    private String storeStockType;

    /**
     * 门店库存
     */
    private BigDecimal storeStockNum;

    /**
     * 门店月销
     */
    private String storeSaleType;

    /**
     * 门店月销
     */
    private BigDecimal storeSaleNum;

    /**
     * 商品分类
     */
    private String proClass;

    /**
     * 门店
     */
    private String proSiteList;

    /**
     * 销售级别
     */
    private String proSlaeClass;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 禁止采购
     */
    private String proNoPurchase;

    /**
     * 商品自分类
     */
    private String proSclass;
}
