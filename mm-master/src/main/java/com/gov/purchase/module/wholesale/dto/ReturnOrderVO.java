package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 退货单
 * @author: yzf
 * @create: 2021-12-28 10:17
 */
@Data
public class ReturnOrderVO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点编码
     */
    private String siteCode;

    /**
     * 地点名称
     */
    private String siteName;

    /**
     * 退货单号
     */
    private String orderId;

    /**
     * 退货类型
     */
    private String orderType;

    /**
     * 退货日期
     */
    private String orderDate;

    /**
     * 退货行数
     */
    private String orderLineNo;

    /**
     * 退货总数量
     */
    private BigDecimal orderSum;

    /**
     * 仓库编码
     */
    private String dcCode;
}
