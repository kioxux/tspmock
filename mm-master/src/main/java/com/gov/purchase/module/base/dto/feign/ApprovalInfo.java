package com.gov.purchase.module.base.dto.feign;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "审批结果信息")
public class ApprovalInfo {

    @ApiModelProperty(value = "加盟商", name = "clientId")
    @NotBlank(message ="加盟商不能为空")
    private String clientId; // 流程类型

    @ApiModelProperty(value = "流程类型", name = "wfDefineCode")
    @NotBlank(message ="流程类型不能为空")
    private String wfDefineCode; // 流程类型

    @ApiModelProperty(value = "业务单号", name = "wfOrder")
    @NotBlank(message ="业务单号不能为空")
    private String wfOrder; //业务单号

    @ApiModelProperty(value = "审批状态", name = "wfStatus")
    @NotBlank(message ="审批状态不能为空")
    private String wfStatus; //审批状态

    @ApiModelProperty(value = "审批意见", name = "memo")
    private String memo; //审批意见

}
