package com.gov.purchase.module.base.service;

import com.gov.purchase.entity.*;
import com.gov.purchase.entity.GaiaCustomerGspinfo;
import com.gov.purchase.entity.GaiaPoHeader;
import com.gov.purchase.entity.GaiaPriceAdjust;
import com.gov.purchase.entity.GaiaProductGspinfo;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.module.base.dto.feign.PurchaseFeignDto;
import com.gov.purchase.module.customer.dto.SaveCusList1RequestDTO;
import com.gov.purchase.module.purchase.dto.PayOrderApprovalDto;
import com.gov.purchase.module.supplier.dto.GspinfoSubmitRequestDto;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 内部接口调用接口类
 */
@Service
public interface FeignService {

    /**
     * 商品首营流程创建
     *
     * @param gspInfo
     * @return
     */
    public FeignResult createProductWorkflow(GaiaProductGspinfo gspInfo);

    /**
     * 获取当前登录人
     *
     * @return
     */
    public TokenUser getLoginInfo();

    FeignResult createProductWorkflow2(List<GaiaProductGspinfo> list, String proFlowNo);

    /**
     *
     * @param list
     * @param proFlowNo
     * @return
     */
    FeignResult createProductWorkflowNew(List<GaiaProductGspinfo> list, String proFlowNo, String client, String userId);

    /**
     * 编辑商品工作流创建
     *
     * @param changeList 修改前后对比数据集合
     * @param flowNO     工作流编码
     * @param zdyNameMap 自定义属性名Map
     * @return 向应数据
     */
    FeignResult createEditProductWorkflow(List<GaiaProductChange> changeList, String flowNO, BeanMap zdyNameMap);

    /**
     * 供应商主数据修改工作流创建
     *
     * @param changeList 修改前后对比数据集合
     * @param flowNO     工作流编码
     * @return 向应数据
     */
    FeignResult createEditSupplierWorkflow(List<GaiaSupplierChange> changeList, String flowNO);

    /**
     * 客户主数据修改工作流创建
     *
     * @param changeList 修改前后对比数据集合
     * @param flowNO 工作流编码
     * @return 向应数据
     */
    FeignResult createEditCustomerWorkflow(List<GaiaCustomerChange> changeList, String flowNO);

    /**
     * 供应商首营流程创建
     *
     * @param dto
     * @return
     */
    FeignResult createSupplierWorkflow(GspinfoSubmitRequestDto dto);

    /**
     * 供应商首营流程创建
     *
     * @param list
     * @return
     */
    FeignResult createSupplierWorkflow2(List<GspinfoSubmitRequestDto> list, String proFlowNo);

    /**
     * 客户首营流程创建
     *
     * @param gspinfo
     * @return
     */
    FeignResult createCustomerWorkflow(GaiaCustomerGspinfo gspinfo);

    /**
     * 批量客户首营流程创建
     */
    FeignResult createCustomerWorkflow2(List<SaveCusList1RequestDTO> customerList, String flowNo);

    /**
     * 采购订单流程创建
     *
     * @param gaiaPoHeader
     * @param detailList
     * @return
     */
    FeignResult createPurchaseWorkflow(GaiaPoHeader gaiaPoHeader, List<PurchaseFeignDto> detailList);

    /**
     * 付款单流程创建
     *
     * @param payOrderApprovalDto
     * @return
     */
    FeignResult createPayOrderWorkflow(PayOrderApprovalDto payOrderApprovalDto);


    /**
     * 商品调价审批流程
     *
     * @param gaiaPriceAdjustList
     * @return
     */
    FeignResult createProductAdjustWorkflow(String FlowNo, List<GaiaPriceAdjust> gaiaPriceAdjustList);


    /**
     * 补充首营
     */
    FeignResult createReplenishProductGspWorkflow(GaiaProductGspinfo gspinfo, String flowNo);
}
