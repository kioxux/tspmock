package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "门店选择传入参数")
public class StoreSelectRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 连锁公司
     */
    @ApiModelProperty(value = "连锁公司", name = "dcChainHead", required = true)
    @NotBlank(message = "连锁公司不能为空")
    private String dcChainHead;
}