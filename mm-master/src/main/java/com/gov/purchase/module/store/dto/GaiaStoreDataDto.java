package com.gov.purchase.module.store.dto;

import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.16
 */
@Data
public class GaiaStoreDataDto extends GaiaStoreData {

    /**
     * 配送中心名称
     */
    private String stoDcName;

    /**
     * 法人
     */
    private String stoLegalPersonName;

    /**
     * 负责人
     */
    private String stoQuaName;

    /**
     * 店长
     */
    private String stoLeaderName;

    /**
     * 连锁总部名
     */
    private String stoChainHeadName;

}
