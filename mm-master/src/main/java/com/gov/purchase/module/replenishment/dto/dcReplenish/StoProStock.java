package com.gov.purchase.module.replenishment.dto.dcReplenish;

import com.gov.purchase.entity.GaiaSdStockBatch;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.23
 */
@Data
public class StoProStock extends GaiaSdStockBatch {

    /**
     * 门店短称
     */
    private String stoShortName;
}
