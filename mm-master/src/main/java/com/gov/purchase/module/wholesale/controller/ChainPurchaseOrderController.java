package com.gov.purchase.module.wholesale.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.wholesale.dto.ChainPurchaseOrderDto;
import com.gov.purchase.module.wholesale.service.ChainPurchaseOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @description: 连锁采购单
 * @author: yzf
 * @create: 2021-11-17 17:12
 */
@Api(tags = "连锁采购单")
@RestController
@RequestMapping("chainPurchaseOrder")
public class ChainPurchaseOrderController {

    @Resource
    private ChainPurchaseOrderService chainPurchaseOrderService;

    /**
     * 总部下拉框列表
     *
     * @return
     */
    @Log("总部下拉框列表")
    @ApiOperation("总部下拉框列表")
    @PostMapping("queryCompadmWms")
    public Result queryCompadmWms() {
        return ResultUtil.success(chainPurchaseOrderService.queryCompadmWms());
    }

    /**
     * 客户下拉框列表
     *
     * @return
     */
    @Log("客户下拉框列表")
    @ApiOperation("客户下拉框列表")
    @PostMapping("queryCompanyCode")
    public Result queryCompanyCode() {
        return ResultUtil.success(chainPurchaseOrderService.queryCompanyCode());
    }

    /**
     * 送达方下拉框-门店列表
     *
     * @return
     */
    @Log("门店列表")
    @ApiOperation("门店列表")
    @PostMapping("queryStoreList")
    public Result queryStoreList() {
        return ResultUtil.success(chainPurchaseOrderService.queryStoreList());
    }


    /**
     * 连锁采购单列表
     *
     * @return
     */
    @Log("连锁采购单列表")
    @ApiOperation("连锁采购单列表")
    @PostMapping("queryChainPurchaseOrderList")
    public Result queryChainPurchaseOrderList(@RequestBody ChainPurchaseOrderDto chainPurchaseOrderDto) {
        return chainPurchaseOrderService.queryChainPurchaseOrderList(chainPurchaseOrderDto);
    }

    /**
     * 关闭连锁采购单
     *
     * @return
     */
    @Log("关闭连锁采购单")
    @ApiOperation("关闭连锁采购单")
    @PostMapping("updateFlag")
    public Result updateFlag(@RequestBody List<ChainPurchaseOrderDto> chainPurchaseOrderDto){
        return chainPurchaseOrderService.updateFlag(chainPurchaseOrderDto);
    }


    /**
     * 连锁采购单明细列表
     *
     * @return
     */
    @Log("连锁采购单明细列表")
    @ApiOperation("连锁采购单明细列表")
    @PostMapping("queryChainPurchaseOrderDetailList")
    public Result queryChainPurchaseOrderDetailList(@RequestBody ChainPurchaseOrderDto chainPurchaseOrderDto) {
        return chainPurchaseOrderService.queryChainPurchaseOrderDetailList(chainPurchaseOrderDto);
    }

    /**
     * 连锁采购明细列表
     *
     * @return
     */
    @Log("连锁采购明细列表")
    @ApiOperation("连锁采购明细列表")
    @PostMapping("queryChainPurchaseDetails")
    public Result queryChainPurchaseDetails(@RequestBody ChainPurchaseOrderDto chainPurchaseOrderDto) {
        return chainPurchaseOrderService.queryChainPurchaseDetails(chainPurchaseOrderDto);
    }


    @Log("审核连锁采购单")
    @ApiOperation("审核连锁采购单")
    @PostMapping("auditChainPurchaseOrder")
    public Result auditChainPurchaseOrder(@RequestBody List<ChainPurchaseOrderDto> chainPurchaseOrderDto) {
        return chainPurchaseOrderService.auditChainPurchaseOrder(chainPurchaseOrderDto);
    }

    @Log("销售员下拉表")
    @ApiOperation("销售员下拉表")
    @PostMapping("salesManList")
    public Result querySalesManList() {
        return ResultUtil.success(chainPurchaseOrderService.querySalesManList());
    }
}
