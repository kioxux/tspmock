package com.gov.purchase.module.wholesale.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.wholesale.dto.*;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.02
 */
public interface SalesOrderService {

    /**
     * 销售订单提交
     *
     * @param gaiaSoHeader
     * @return
     */
    Result saveSalesOrder(GaiaSoHeader gaiaSoHeader);

    /**
     * 销售订单暂存
     *
     * @param gaiaSoHeader
     * @return
     */
    Result temporarySalesOrder(GaiaSoHeader gaiaSoHeader);

    /**
     * 暂存销售列表
     *
     * @return
     */
    List<GaiaSoHeaderVO> temporaryList();

    /**
     * 暂存销售单明细
     *
     * @param soId
     * @return
     */
    TemporaryOrderDetailDTO temporaryOrderDetail(String soId);

    /**
     * 删除暂存销售单
     *
     * @param soId
     * @return
     */
    void delTemporaryOrder(String soId);

    /**
     * 批发订单新增数量是否允许输入小数（配置表查询  0：允许，1：不允许）
     *
     * @return
     */
    String querySalesOrderConfig();

    void soAutomaticBilling(com.gov.purchase.entity.GaiaSoHeader gaiaSoHeader);

    /**
     * 商品列表获取
     *
     * @param client  加盟商
     * @param proSite 销售主体（DC编码）
     * @param name    商品自编码/商品名称
     * @return
     */
    Result queryGoodsList(String client, String proSite, String soCustomerId, String name, String gwsCode, Integer priceType, String owner);

    /**
     * 批发销售查询列表
     *
     * @param querySalesOrderList
     * @return
     */
    Result querySalesOrderList(QuerySalesOrderList querySalesOrderList);

    /**
     * 批发销售订单明细
     *
     * @param client 加盟商
     * @param soId   订单号
     * @return
     */
    Result getSalesOrderInfo(String client, String soId);

    /**
     * 销售订单修改
     *
     * @param updateSalesOrder
     * @return
     */
    Result updateSalesOrder(UpdateSalesOrder updateSalesOrder);

    /**
     * 批发销售查询列表导出
     *
     * @param querySalesOrderList
     * @return
     */
    Result exportSalesOrderList(QuerySalesOrderList querySalesOrderList);

    /**
     * 调取请货单列表
     *
     * @param vo
     * @return
     */
    PageInfo<GoodsOrderDTO> getRequestGoodsOrderList(GetRequestGoodsOrderListVO vo);

    /**
     * 批发销售查询列表_主表
     *
     * @param vo
     * @return
     */
    Result querySalesOrderHeaderList(QuerySalesOrderHeaderVO vo);

    /**
     * 关闭请货单
     *
     * @param gsrhBrId
     * @param gsrhVoucherId
     */
    void closeBill(String gsrhBrId, String gsrhVoucherId);

    /**
     * 销售员集合
     *
     * @param supSite
     * @return
     */
    Result getSalesmanList(String supSite);

    Result queryGoodsPriceList(String proSite, String proSelfCode, List<String> proSelfCodeList);

    /**
     * 查询批发销售审批详情
     *
     * @param wholeSaleApproveDTO
     * @return
     */
    Result queryWholesaleApproveList(WholeSaleApproveDTO wholeSaleApproveDTO);

    /**
     * 查询批发退货审批单
     *
     * @param wholeSaleApproveDTO
     * @return
     */
    Result queryWholesaleReturnsList(WholeSaleApproveDTO wholeSaleApproveDTO);

    /**
     * 更新批发-销售/退货-审批状态
     *
     * @param soId
     * @param soApproveStatus
     * @param siteCode
     */
    void updateApproveStatus(String soId, String soApproveStatus, String siteCode);

    /**
     * 批发退货订单明细
     *
     * @param soId 订单号
     * @return
     */
    Result getSalesOrderReturnsInfo(String soId);

    /**
     * 更新商品预设价
     *
     * @param proSelfCode
     * @param proSite
     * @param type
     * @param proYsj
     */
    void updateProYsj(String proSelfCode, String proSite, String type, BigDecimal proYsj);

    /**
     * 送达方验证
     *
     * @return
     */
    Result checkCustomer(GaiaSoHeader entity);
}
