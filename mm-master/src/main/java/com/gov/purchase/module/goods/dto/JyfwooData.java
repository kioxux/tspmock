package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author zhoushuai
 * @date 2021/4/7 15:41
 */
@Data
@EqualsAndHashCode
public class JyfwooData {

    /**
     * 经营范围编码
     */
    private String jyfwId;

    /**
     * 经营范围描述
     */
    private String jyfwName;

    /**
     * 经营范围类别（1-经营企业，2-生产企业，3-客户）
     */
    private String jyfwClass;
}
