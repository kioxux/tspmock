package com.gov.purchase.module.customer.dto;

import com.gov.purchase.entity.GaiaCustomerBasic;
import lombok.Data;

@Data
public class CustomerGspListResponseDto extends GaiaCustomerBasic {

    /**
     * 是否经营
     */
    private String sell;

}
