package com.gov.purchase.module.replenishment.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.WmsFeign;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.SearchReplenish;
import com.gov.purchase.module.purchase.dto.SearchReplenishReq;
import com.gov.purchase.module.replenishment.dto.*;
import com.gov.purchase.module.replenishment.service.StoreNeedService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelStyleUtil;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import com.xxl.job.core.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import net.minidev.json.JSONObject;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class StoreNeedServiceImpl implements StoreNeedService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaBatchStockMapper gaiaBatchStockMapper;
    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    @Resource
    private GaiaTaxCodeMapper gaiaTaxCodeMapper;
    @Resource
    private RedisClient redisClient;
    @Resource
    private WmsFeign wmsFeign;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaJyfwDataMapper gaiaJyfwDataMapper;
    @Resource
    private GaiaStoreDataMapper storeDataMapper;
    @Resource
    private GaiaSdStoresGroupSetMapper gaiaSdStoresGroupSetMapper;
    @Resource
    private GaiaSdStoresGroupMapper gaiaSdStoresGroupMapper;
    @Resource
    GaiaDcDataMapper dcDataMapper;
    @Resource
    private GaiaWmsDiaoboZMapper gaiaWmsDiaoboZMapper;

    //新品铺货计划自动开单
    private static final Integer DISTRIBUTION_SOURCE = 1;

    @Override
    public List<GaiaStoreData> queryStoreList(StoreRequestDto dto) {
        return gaiaStoreDataMapper.getStoreListByDc(dto);
    }

    @Override
    public List<BatchStockListResponseDto> queryBatchStockList(BatchStockRequestDto dto) {
        return gaiaBatchStockMapper.selectStoreDistributionList(dto);
    }

    /**
     * 门店铺货提交
     *
     * @param list
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveStoreNeedList(List<StoreNeedRequestDto> list) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        List<String> errorList = new ArrayList<>();
        // 门店 铺货日期分组
        Map<String, Map<String, List<StoreNeedRequestDto>>> map = new HashMap<>();
        // 铺货时检查配置表
        List<GaiaProductBusinessKey> gaiaProductBusinessKeyList = new ArrayList<>();
        // 数据验证
        for (StoreNeedRequestDto item : list) {
            ValidationResult result = ValidateUtil.validateEntity(item);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(client);
            gaiaProductBusinessKey.setProSite(item.getStoCode());
            gaiaProductBusinessKey.setProSelfCode(item.getBatProCode());
            gaiaProductBusinessKeyList.add(gaiaProductBusinessKey);

            // 门店
            String stoCode = item.getStoCode();
            // 日期分组数据
            Map<String, List<StoreNeedRequestDto>> dateMap = map.get(stoCode);
            if (dateMap == null) {
                dateMap = new HashMap<>();
            }
            // 铺货日期
            String stoNeedArrivalDate = item.getStoNeedArrivalDate();
            // 门点 日期 分组明细数据
            List<StoreNeedRequestDto> dtoList = dateMap.get(stoNeedArrivalDate);
            if (dtoList == null) {
                dtoList = new ArrayList<>();
            }
            // 行数据追加
            dtoList.add(item);
            dateMap.put(stoNeedArrivalDate, dtoList);
            map.put(stoCode, dateMap);
        }
        if (CollectionUtils.isNotEmpty(gaiaProductBusinessKeyList)) {
            List<GaiaProductBusiness> gaiaProductBusinessList =
                    gaiaProductBusinessMapper.selectByPrimaryByBuyunxupinlei(client, gaiaProductBusinessKeyList);
            if (CollectionUtils.isNotEmpty(gaiaProductBusinessList)) {
                for (GaiaProductBusiness gaiaProductBusiness : gaiaProductBusinessList) {
                    String proSite = gaiaProductBusiness.getProSite();
                    String proSelfCode = gaiaProductBusiness.getProSelfCode();
                    errorList.add(MessageFormat.format("商品{0}对于门店{1}为不允许铺货的品类，不可铺货至门店。", proSelfCode, proSite));
                }
            }
        }
        if (CollectionUtils.isEmpty(errorList)) {
            List<GaiaSdReplenishH> replenishHList = new ArrayList<>();
            // 门店分组数据
            for (String stoCode : map.keySet()) {
                // 详细
                Map<String, List<StoreNeedRequestDto>> dateMap = map.get(stoCode);
                // 日期分组数据
                for (String stoNeedArrivalDate : dateMap.keySet()) {
                    // 明细数据
                    List<StoreNeedRequestDto> dtoList = dateMap.get(stoNeedArrivalDate);
                    // 铺货数据
                    BigDecimal stoNeedQty = dtoList.stream().map(StoreNeedRequestDto::getStoNeedQty).reduce(BigDecimal.ZERO, BigDecimal::add);
                    // 主表数据
                    // 补货主表
                    GaiaSdReplenishH gaiaSdReplenishH = new GaiaSdReplenishH();
                    // 加盟商
                    gaiaSdReplenishH.setClient(client);
                    //补货门店
                    gaiaSdReplenishH.setGsrhBrId(stoCode);
                    //补货单号
                    String gsrhVoucherId = getMaxVoucherId(client);
                    gaiaSdReplenishH.setGsrhVoucherId(gsrhVoucherId);
                    //补货日期
                    gaiaSdReplenishH.setGsrhDate(stoNeedArrivalDate);
                    gaiaSdReplenishH.setGsrhCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                    //补货类型 默认为固定值“2-总部配送”
                    GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                    gaiaStoreDataKey.setClient(client);
                    gaiaStoreDataKey.setStoCode(stoCode);
                    GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                    if (gaiaStoreData != null && "3".equals(gaiaStoreData.getStoDeliveryMode())) {
                        gaiaSdReplenishH.setGsrhType("3");
                    } else if (gaiaStoreData != null && "4".equals(gaiaStoreData.getStoDeliveryMode())) {
                        gaiaSdReplenishH.setGsrhType("4");
                    } else if (gaiaStoreData != null && "5".equals(gaiaStoreData.getStoDeliveryMode())) {
                        gaiaSdReplenishH.setGsrhType("5");
                    } else if (gaiaStoreData != null && "6".equals(gaiaStoreData.getStoDeliveryMode())) {
                        gaiaSdReplenishH.setGsrhType("6");
                    } else if (gaiaStoreData != null && "7".equals(gaiaStoreData.getStoDeliveryMode())) {
                        gaiaSdReplenishH.setGsrhType("7");
                    } else if (gaiaStoreData != null && "9".equals(gaiaStoreData.getStoDeliveryMode())) {
                        gaiaSdReplenishH.setGsrhType("9");
                    } else {
                        gaiaSdReplenishH.setGsrhType("2");
                    }
                    //补货地点
                    gaiaSdReplenishH.setGsrhAddr(dtoList.get(0).getDcCode());
                    //补货金额
                    gaiaSdReplenishH.setGsrhTotalAmt(BigDecimal.ZERO);
                    //补货数量
                    gaiaSdReplenishH.setGsrhTotalQty(stoNeedQty);
                    //补货人员
                    gaiaSdReplenishH.setGsrhEmp(user.getUserId());
                    //补货状态
                    gaiaSdReplenishH.setGsrhStatus("1");
                    //是否转采购订单
                    //补货方式 默认未固定值“2-铺货”
                    gaiaSdReplenishH.setGsrhPattern("2");
                    gaiaSdReplenishHMapper.insertSelective(gaiaSdReplenishH);
                    replenishHList.add(gaiaSdReplenishH);
                    // 插入明细表数据
                    for (int i = 0; i < dtoList.size(); i++) {
                        // 行数据
                        StoreNeedRequestDto dto = dtoList.get(i);
                        // 验证商品存在性
                        GaiaProductBusiness gaiaProductBusiness =
                                gaiaProductBusinessMapper.getProductBusiness(client, dto.getStoCode(), dto.getBatProCode());
                        if (gaiaProductBusiness == null) {
                            errorList.add(MessageFormat.format("门店：[{0}]未查询到商品：[{1}]数据", dto.getStoCode(), dto.getBatProCode()));
                            continue;
                        }
                        GaiaSdReplenishD gaiaSdReplenishD = new GaiaSdReplenishD();
                        // 加盟商
                        gaiaSdReplenishD.setClient(client);
                        //补货门店
                        gaiaSdReplenishD.setGsrdBrId(dto.getStoCode());
                        //补货单号
                        gaiaSdReplenishD.setGsrdVoucherId(gsrhVoucherId);
                        //补货日期
                        gaiaSdReplenishD.setGsrdDate(stoNeedArrivalDate);
                        //行号
                        gaiaSdReplenishD.setGsrdSerial(String.valueOf(i + 1));
                        //商品编码
                        gaiaSdReplenishD.setGsrdProId(dto.getBatProCode());
                        //商品批号
                        gaiaSdReplenishD.setGsrdBatchNo(dto.getGsrdBatchNo());
                        // 补货量
                        gaiaSdReplenishD.setGsrdProposeQty(dto.getStoNeedQty());
                        // 建议补货量
                        gaiaSdReplenishD.setGsrdNeedQty(dto.getStoNeedQty().toString());
                        // 订单金额
                        // 根据铺货的配送中心+商品编码到物料评估表GAIA_MATERIAL_ASSESS中的MAT_MOV_PRICE
                        GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
                        // 加盟商
                        gaiaMaterialAssessKey.setClient(client);
                        // 商品编码
                        gaiaMaterialAssessKey.setMatProCode(dto.getBatProCode());
                        // 估价地点
                        gaiaMaterialAssessKey.setMatAssessSite(gaiaSdReplenishH.getGsrhAddr());
                        GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
                        if (gaiaMaterialAssess != null) {
                            gaiaSdReplenishD.setGsrdVoucherAmt(gaiaMaterialAssess.getMatMovPrice());
                        }
                        if (StringUtils.isNotBlank(gaiaProductBusiness.getProOutputTax())) {
                            GaiaTaxCode gaiaTaxCode = gaiaTaxCodeMapper.selectByPrimaryKey(gaiaProductBusiness.getProOutputTax());
                            if (gaiaTaxCode != null) {
                                gaiaSdReplenishD.setGsrdTaxRate(gaiaTaxCode.getTaxCodeValue());
                            }
                        }
                        gaiaSdReplenishDMapper.insertSelective(gaiaSdReplenishD);
                    }
                }
            }
            try {
                automaticBilling(replenishHList);
            } catch (Exception e) {
                log.info("铺货自动开单报错 {}", e.getMessage());
            }
        }
        if (CollectionUtils.isNotEmpty(errorList)) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            Result result = ResultUtil.error(ResultEnum.E0156);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success();
    }

    /**
     * 铺货查询
     *
     * @param searchReplenishReq
     * @return
     */
    @Override
    public Result searchReplenishList(SearchReplenishReq searchReplenishReq) {
        // 明细 数据查询
        PageHelper.startPage(searchReplenishReq.getPageNum(), searchReplenishReq.getPageSize());
        List<SearchReplenish> list = gaiaSdReplenishHMapper.searchReplenishList(searchReplenishReq);
        PageInfo<SearchReplenish> pageInfo = new PageInfo<>(list);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 铺货保存
     *
     * @param list
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveReplenish(List<SearchReplenish> list) {
        if (CollectionUtils.isEmpty(list)) {
            return ResultUtil.success();
        }
        // 数据验证
        for (SearchReplenish item : list) {
            ValidationResult result = ValidateUtil.validateEntity(item);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
        }
        String client = "";
        List<String> poReqIds = new ArrayList<>();
        List<String> errorList = new ArrayList<>();
        Map<String, GaiaSdReplenishHKey> items = new HashMap<>();
        if (CollectionUtils.isNotEmpty(list)) {
            List<String> voucherIds = list.stream().map(SearchReplenish::getGsrhVoucherId).collect(Collectors.toList());
            if (CollectionUtils.isNotEmpty(voucherIds)) {
                poReqIds = gaiaSdReplenishHMapper.getPoReqIdList(list.get(0).getClient(), voucherIds);
            }
            for (SearchReplenish item : list) {
                if (CollectionUtils.isNotEmpty(poReqIds) && poReqIds.contains(item.getGsrhVoucherId())) {
                    errorList.add("铺货订单" + item.getGsrhVoucherId() + "不可修改或者删除");
                }
            }
        }
        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        // 数据保存
        for (int i = 0; i < list.size(); i++) {
            // 明细数据
            SearchReplenish item = list.get(i);
            client = item.getClient();

            GaiaSdReplenishHKey gaiaSdReplenishHKey = new GaiaSdReplenishHKey();
            // 加盟商
            gaiaSdReplenishHKey.setClient(item.getClient());
            // 补货门店
            gaiaSdReplenishHKey.setGsrhBrId(item.getGsrhBrId());
            // 补货单号
            gaiaSdReplenishHKey.setGsrhVoucherId(item.getGsrhVoucherId());
            // 补货日期
//            gaiaSdReplenishHKey.setGsrhDate(item.getGsrhDate());
            GaiaSdReplenishH gaiaSdReplenishH = gaiaSdReplenishHMapper.selectByPrimaryKey(gaiaSdReplenishHKey);
            if (gaiaSdReplenishH == null) {
                throw new CustomResultException(MessageFormat.format(ResultEnum.E0154.getMsg(), String.valueOf(i + 1)));
            }
            if ("1".equals(gaiaSdReplenishH.getGsrhDnFlag())) {
                throw new CustomResultException("铺货订单" + item.getGsrhVoucherId() + "已开单");
            }
            // 删除
            String isDel = item.getIsDel();
            GaiaSdReplenishDKey gaiaSdReplenishDKey = new GaiaSdReplenishDKey();
            // 加盟商
            gaiaSdReplenishDKey.setClient(client);
            // 补货门店
            gaiaSdReplenishDKey.setGsrdBrId(item.getGsrhBrId());
            // 补货单号
            gaiaSdReplenishDKey.setGsrdVoucherId(item.getGsrhVoucherId());
            // 补货日期
//            gaiaSdReplenishDKey.setGsrdDate(item.getGsrhDate());
            // 商品编码
            gaiaSdReplenishDKey.setGsrdProId(item.getGsrdProId());
            // 行号
            gaiaSdReplenishDKey.setGsrdSerial(item.getGsrdSerial());
            GaiaSdReplenishD gaiaSdReplenishD = gaiaSdReplenishDMapper.selectByPrimaryKey(gaiaSdReplenishDKey);
            if (gaiaSdReplenishD == null) {
                throw new CustomResultException(MessageFormat.format(ResultEnum.E0154.getMsg(), String.valueOf(i + 1)));
            }
            // 删除行
            if ("1".equals(isDel)) {
                gaiaSdReplenishDMapper.deleteByPrimaryKey(gaiaSdReplenishDKey);
            } else {
                GaiaSdReplenishD record = new GaiaSdReplenishD();
                // 加盟商
                record.setClient(client);
                // 补货门店
                record.setGsrdBrId(item.getGsrhBrId());
                // 补货单号
                record.setGsrdVoucherId(item.getGsrhVoucherId());
                // 商品
                record.setGsrdProId(item.getGsrdProId());
                // 补货日期
                record.setGsrdDate(item.getGsrhDate());
                // 行号
                record.setGsrdSerial(item.getGsrdSerial());
                // 建议补货量
                record.setGsrdProposeQty(item.getGsrdNeedQty());
                // 补货量
                record.setGsrdNeedQty(item.getGsrdNeedQty().toString());
                gaiaSdReplenishDMapper.updateByPrimaryKeySelective(record);
            }
            String key = client.concat("_").concat(item.getGsrhBrId()).concat("_").concat(item.getGsrhVoucherId()).concat("_").concat(item.getGsrhDate());
            items.put(key, gaiaSdReplenishHKey);
        }
        // 主表数据更新 明细表行号更新
        for (String key : items.keySet()) {
            GaiaSdReplenishHKey item = items.get(key);
            GaiaSdReplenishDKey entity = new GaiaSdReplenishDKey();
            //加盟商
            entity.setClient(client);
            //补货门店
            entity.setGsrdBrId(item.getGsrhBrId());
            //补货单号
            entity.setGsrdVoucherId(item.getGsrhVoucherId());
            List<GaiaSdReplenishD> details = gaiaSdReplenishDMapper.selectByVoucherId(entity);
            if (CollectionUtils.isEmpty(details)) {
                // 删除主表
                gaiaSdReplenishHMapper.deleteByPrimaryKey(item);
            } else {
                // 补货数量
                BigDecimal gsrhTotalQty = BigDecimal.ZERO;
                // 更新明细表行号
                for (int i = 0; i < details.size(); i++) {
                    // 行数据
                    GaiaSdReplenishD gaiaSdReplenishD = details.get(i);
                    // 更新条件
                    GaiaSdReplenishDKey gaiaSdReplenishDKey = new GaiaSdReplenishDKey();
                    // 加盟商
                    gaiaSdReplenishDKey.setClient(client);
                    // 补货门店
                    gaiaSdReplenishDKey.setGsrdBrId(gaiaSdReplenishD.getGsrdBrId());
                    // 补货单号
                    gaiaSdReplenishDKey.setGsrdVoucherId(gaiaSdReplenishD.getGsrdVoucherId());
//                    // 补货日期
//                    gaiaSdReplenishDKey.setGsrdDate(gaiaSdReplenishD.getGsrdDate());
                    // 商品编码
                    gaiaSdReplenishDKey.setGsrdProId(gaiaSdReplenishD.getGsrdProId());
                    // 行号
                    gaiaSdReplenishDKey.setGsrdSerial(gaiaSdReplenishD.getGsrdSerial());
                    gaiaSdReplenishDKey.setGsrdProId(gaiaSdReplenishD.getGsrdProId());
                    gaiaSdReplenishDMapper.updateSerial(gaiaSdReplenishDKey, String.valueOf(i + 1));
                    // 补货数量不为空
                    if (StringUtils.isNotBlank(gaiaSdReplenishD.getGsrdNeedQty())) {
                        gsrhTotalQty = gsrhTotalQty.add(NumberUtils.createBigDecimal(gaiaSdReplenishD.getGsrdNeedQty()));
                    }
                }
                // 更新主表 补货数量
                GaiaSdReplenishH gaiaSdReplenishH = new GaiaSdReplenishH();
                // 加盟商
                gaiaSdReplenishH.setClient(client);
                //补货门店
                gaiaSdReplenishH.setGsrhBrId(details.get(0).getGsrdBrId());
                //补货单号
                gaiaSdReplenishH.setGsrhVoucherId(details.get(0).getGsrdVoucherId());
                //补货日期
                gaiaSdReplenishH.setGsrhDate(details.get(0).getGsrdDate());
                gaiaSdReplenishH.setGsrhCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                // 补货数量
                gaiaSdReplenishH.setGsrhTotalQty(gsrhTotalQty);
                gaiaSdReplenishHMapper.updateByPrimaryKeySelective(gaiaSdReplenishH);
            }
        }
        return ResultUtil.success();
    }

    /**
     * 有权限的DC列表
     *
     * @return
     */
    @Override
    public List<GaiaDcData> getAuthDcList(TokenUser tokenUser) {
        String client = "";
        String userId = "";
        if (tokenUser == null) {
            client = feignService.getLoginInfo().getClient();
            userId = feignService.getLoginInfo().getUserId();
        } else {
            client = tokenUser.getClient();
            userId = tokenUser.getUserId();
        }
        return gaiaSdReplenishHMapper.getAuthDcList(client, userId);
    }

    /**
     * 门店列表
     *
     * @param dcCode
     * @return
     */
    @Override
    public List<GetStoreListByAuthDcDTO> getStoreListByAuthDc(String dcCode, TokenUser user, String gssgId, String stoDistrict) {
        String client = "";
        if (user == null) {
            client = feignService.getLoginInfo().getClient();
        } else {
            client = user.getClient();
        }
        return gaiaSdReplenishHMapper.getStoreListByAuthDc(client, dcCode, gssgId, stoDistrict);
    }

    /**
     * 商品选择弹出框
     *
     * @param dto
     * @return
     */
    @Override
    public List<StoreReplenishDetails> getProList(GetProListRequestDTO dto, TokenUser tokenUser) {
        if (StringUtils.isBlank(dto.getDcCode())) {
            throw new CustomResultException("配送中心编码不能为空");
        }
        dto.setClient(tokenUser == null ? feignService.getLoginInfo().getClient() : tokenUser.getClient());
        List<StoreReplenishDetails> result = gaiaSdReplenishHMapper.getProList(dto);
        return result;
    }

    /**
     * 门店铺货商品提交
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public AutoNeedProDTO saveNeedProList(List<SaveNeedProListRequestDTO> list, TokenUser user) {
        AutoNeedProDTO autoNeedProDTO = new AutoNeedProDTO();
        Integer callSource = 1;
        List<String> warning = new ArrayList<>();
        if (user == null || StringUtils.isEmpty(user.getClient())) {
            user = feignService.getLoginInfo();
            callSource = 0;
        }
        if (CollectionUtils.isNotEmpty(list)) {
            for (SaveNeedProListRequestDTO item : list) {
                GaiaDcData dcData = dcDataMapper.selectByDcCode(user.getClient(), item.getDcCode());
                if (dcData != null && StringUtils.isNotBlank(dcData.getDcJyfwgk()) && "1".equalsIgnoreCase(dcData.getDcJyfwgk())) {
                    List<String> jyfwList = gaiaJyfwDataMapper.getJyfwIdList(user.getClient(), item.getStoCode());
                    if (!CollectionUtils.isNotEmpty(jyfwList)) {
                        throw new CustomResultException(MessageFormat.format("门店[{0}]未配置经营管控范围", item.getStoName()));
                    }
                    List<String> distList = jyfwList.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
                    if (!CollectionUtils.isNotEmpty(distList)) {
                        throw new CustomResultException(MessageFormat.format("门店[{0}]未配置经营管控范围", item.getStoName()));
                    }
                    GaiaProductBusiness productBusiness = gaiaProductBusinessMapper.getProductBusiness(user.getClient(), item.getDcCode(), item.getProSelfCode());
                    if (productBusiness != null && StringUtils.isNotBlank(productBusiness.getProJylb())) {
                        List<String> jylb = Arrays.asList(productBusiness.getProJylb().split(","));
                        List<String> tog = jylb.stream().filter(distList::contains).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(tog)) {
                            throw new CustomResultException(MessageFormat.format("商品[{0}]超出门店[{1}]的经营管控范围", item.getProSelfCode(), item.getStoName()));
                        }
                    } else {
                        throw new CustomResultException(MessageFormat.format("商品[{0}]超出门店[{1}]的经营管控范围", item.getProSelfCode(), item.getStoName()));
                    }
                }
            }
        }
        // 铺货时检查配置表
        List<GaiaProductBusinessKey> gaiaProductBusinessKeyList = new ArrayList<>();
        // 校验
        TokenUser finalUser = user;
        list.forEach(item -> {
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(finalUser.getClient());
            gaiaProductBusinessKey.setProSite(item.getStoCode());
            gaiaProductBusinessKey.setProSelfCode(item.getProSelfCode());
            gaiaProductBusinessKeyList.add(gaiaProductBusinessKey);
            if (ObjectUtils.isEmpty(item.getGsrdNeedQty()) || item.getGsrdNeedQty().compareTo(BigDecimal.ZERO) < 0) {
                throw new CustomResultException(MessageFormat.format("门店[{0}]商品[{1}]铺货数量不正确", item.getStoName(), item.getProName()));
            }
            // 明细表
            AtomicBoolean flag = new AtomicBoolean(false);
            BigDecimal sum = item.getBatchNoInfoList().stream().filter(batchNoInfo -> {
                // 1.页面数据：铺货数量为零的数据过虑
                return ObjectUtils.isNotEmpty(batchNoInfo.getGsrdNeedQty()) && batchNoInfo.getGsrdNeedQty().compareTo(BigDecimal.ZERO) > 0;
            }).filter(batchNoInfo -> {
                if (ObjectUtils.isNotEmpty(batchNoInfo.getGsrdNeedQty())) {
                    flag.set(true);
                }

                // 3.GAIA_WMS_TESHUPINLEIPEISONG 配置表校验
                int check = gaiaSdReplenishHMapper.check(finalUser.getClient(), item.getDcCode(), item.getStoCode(), item.getProSelfCode());
                if (check == 0) {
                    throw new CustomResultException(MessageFormat.format("门店[{0}]商品[{1}]不可铺货", item.getStoName(), item.getProName()));
                }

                // 不做筛选，直接返回
                return true;
            }).map(BatchNoInfo::getGsrdNeedQty).reduce(BigDecimal.ZERO, BigDecimal::add);

            if (StringUtils.isBlank(item.getGsrdHwh())) {
                // 校验库存是否超限
                if (StringUtils.isNotBlank(item.getFlag()) && "1".equals(item.getFlag())) {
                    if (CollectionUtils.isNotEmpty(item.getBatchNoInfoList())) {
                        BigDecimal lineSum = item.getBatchNoInfoList().stream().map(vo -> ObjectUtils.isEmpty(vo.getWmKysl()) ? new BigDecimal(0) : vo.getWmKysl()).reduce(BigDecimal.ZERO, BigDecimal::add);
                        if (lineSum.compareTo(item.getGsrdNeedQty()) < 0) {
                            throw new CustomResultException(MessageFormat.format("门店[{0}]商品[{1}]铺货数量超过现有库存量", item.getStoName(), item.getProName()));
                        }
                    }
                }

                if (flag.get() && ObjectUtils.isNotEmpty(item.getGsrdNeedQty()) && ObjectUtils.isNotEmpty(sum) && sum.compareTo(item.getGsrdNeedQty()) != 0) {
                    throw new CustomResultException(MessageFormat.format("门店[{0}]商品[{1}]总的铺货数量和明细铺货数量总和不匹配,请重新输入数值", item.getStoName(), item.getProName()));
                }
            }
        });
        if (CollectionUtils.isNotEmpty(gaiaProductBusinessKeyList)) {
            List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectByPrimaryByBuyunxupinlei(finalUser.getClient(), gaiaProductBusinessKeyList);
            if (CollectionUtils.isNotEmpty(gaiaProductBusinessList)) {
                String proSite = gaiaProductBusinessList.get(0).getProSite();
                String proSelfCode = gaiaProductBusinessList.get(0).getProSelfCode();
                throw new CustomResultException(
                        MessageFormat.format("商品{0}对于门店{1}为不允许铺货的品类，不可铺货至门店。", proSelfCode, proSite)
                );
            }
        }
        if (CollectionUtils.isNotEmpty(list)) {
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(user.getClient());
            gaiaDcDataKey.setDcCode(list.get(0).getDcCode());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            List<String> proCodes = list.stream().map(SaveNeedProListRequestDTO::getProSelfCode).collect(Collectors.toList());
            List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectProList(user.getClient(), list.get(0).getDcCode(), proCodes);
            if (CollectionUtils.isNotEmpty(gaiaProductBusinessList)) {
                for (GaiaProductBusiness gaiaProductBusiness : gaiaProductBusinessList) {
                    if (gaiaDcData != null) {
                        // 是否管控证照效期：1-是
                        if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
                            // 检查 商品主数据 注册证效期（批准文号有效期）。
                            if (StringUtils.isNotBlank(gaiaProductBusiness.getProRegisterExdate()) && gaiaProductBusiness.getProRegisterExdate().trim().length() == 8) {
                                String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                                Long datediff = DateUtils.datediff(day, gaiaProductBusiness.getProRegisterExdate());
                                // 警告
                                if (datediff.intValue() < 0) {
                                    warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号已过期");
                                }
                            }
                        }
                    }
                }
            }
        }
        // 入库
        try {
            List<GaiaSdReplenishH> replenishHList = new ArrayList<>();
            List<GaiaSdReplenishD> replenishDList = new ArrayList<>();
            String para = gaiaSdReplenishHMapper.getParamByClientAndGcspId(finalUser.getClient(), "WHOLESALE_DELIVERY_MODE");
            // 门店分组
            Map<String, List<SaveNeedProListRequestDTO>> collect = list.stream().filter(item -> item.getGsrdNeedQty().compareTo(BigDecimal.ZERO) > 0)
                    .collect(Collectors.groupingBy(SaveNeedProListRequestDTO::getStoCode));
            TokenUser finalUser1 = user;
            Integer finalCallSource = callSource;
            collect.forEach((stoCode, proList) -> {
                // 单号
                String voucherId = getMaxVoucherId(finalUser1.getClient());
                // 铺货总数
                BigDecimal gsrdNeedQtySum = proList.stream().map(SaveNeedProListRequestDTO::getGsrdNeedQty).reduce(BigDecimal.ZERO, BigDecimal::add); //.mapToInt(SaveNeedProListRequestDTO::getGsrdNeedQty).sum();
                SaveNeedProListRequestDTO dto = proList.get(0);
                BigDecimal totalAmt = proList.stream().filter(Objects::nonNull)
                        .filter(item -> ObjectUtils.isNotEmpty(item.getCostPrice()) && ObjectUtils.isNotEmpty(item.getGsrdNeedQty()))
                        .map(item -> item.getGsrdNeedQty().multiply(item.getCostPrice()))
                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                // 主表
                GaiaSdReplenishH replenishH = new GaiaSdReplenishH();
                // 加盟商
                replenishH.setClient(finalUser1.getClient());
                // 补货门店
                replenishH.setGsrhBrId(stoCode);
                // 补货地点
                replenishH.setGsrhAddr(dto.getDcCode());
                // 补货单号
                replenishH.setGsrhVoucherId(voucherId);
                // 补货日期
                replenishH.setGsrhDate(DateUtils.getCurrentDateStrYYMMDD());
                replenishH.setGsrhCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                // 补货类型
                if (StringUtils.isNotBlank(dto.getDcWtdc()) && "1".equalsIgnoreCase(dto.getDcWtdc())) {
                    if (StringUtils.isNotBlank(para)) {
                        replenishH.setGsrhType(para);
                    }
                }
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                gaiaStoreDataKey.setClient(finalUser1.getClient());
                gaiaStoreDataKey.setStoCode(stoCode);
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                //账期以及信用额额控制
                checkCreditQuotaAndZqts(gaiaStoreData, totalAmt);
                if (StringUtils.isBlank(replenishH.getGsrhType())) {
                    if (gaiaStoreData != null && "3".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("3");
                    } else if (gaiaStoreData != null && "4".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("4");
                    } else if (gaiaStoreData != null && "5".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("5");
                    } else if (gaiaStoreData != null && "6".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("6");
                    } else if (gaiaStoreData != null && "7".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("7");
                    } else if (gaiaStoreData != null && "9".equals(gaiaStoreData.getStoDeliveryMode())) {
                        replenishH.setGsrhType("9");
                    } else {
                        replenishH.setGsrhType("2");
                    }
                }
                // 补货金额
                replenishH.setGsrhTotalAmt(totalAmt);
                // 补货数量
                replenishH.setGsrhTotalQty(gsrdNeedQtySum);
                // 补货人员
                replenishH.setGsrhEmp(finalUser1.getUserId());
                // 补货状态是否审核
                replenishH.setGsrhStatus("1");
                // 铺货来源
                if (DISTRIBUTION_SOURCE.equals(finalCallSource)) {
                    replenishH.setGsrhSource("3");//新品铺货
                }
                // 是否转采购订单（默认空）
//                replenishH.setGsrhFlag(CommonEnum.NoYesStatus.YES.getCode());
                // 补货方式 0正常补货，1紧急补货,2铺货,3互调
                replenishH.setGsrhPattern("2");
                replenishH.setGsrhFlag("0");
//                replenishH.setGsrhDnBy("");
//                replenishH.setGsrhDnDate("");
//                replenishH.setGsrhDnTime("");
//                replenishH.setGsrhDnFlag("");
                replenishHList.add(replenishH);

                // 明细表
                // 行号 (同一单号下顺序排列)
                AtomicInteger index = new AtomicInteger(1);
                proList.forEach(pro -> {
                    List<GaiaSdReplenishD> replenishDListTemp = pro.getBatchNoInfoList().stream().filter(batchNoInfo -> {
                        // 页面数据过虑：铺货数量为零的数据过虑
                        return ObjectUtils.isNotEmpty(batchNoInfo.getGsrdNeedQty()) && batchNoInfo.getGsrdNeedQty().compareTo(BigDecimal.ZERO) > 0;
                    }).map(batchNoInfo -> {
                        return createGaiaSdReplenishD(finalUser1, pro, voucherId, index, batchNoInfo, pro.getGsrdNeedQty());
                    }).collect(Collectors.toList());
                    if (CollectionUtils.isNotEmpty(replenishDListTemp)) {
                        replenishDList.addAll(replenishDListTemp);
                    } else {
                        replenishDList.add(createGaiaSdReplenishD(finalUser1, pro, voucherId, index, null, pro.getGsrdNeedQty()));
                    }
                });
            });
            if (CollectionUtils.isNotEmpty(replenishHList)) {
                gaiaSdReplenishHMapper.saveBatch(replenishHList);
                autoNeedProDTO.setReplenishHList(replenishHList);
            }
            if (CollectionUtils.isNotEmpty(replenishDList)) {
                gaiaSdReplenishDMapper.saveBatch(replenishDList);
                autoNeedProDTO.setReplenishDList(replenishDList);
            }
            try {
                if (DISTRIBUTION_SOURCE.equals(callSource)) {
                    //事务提交后再执行自动开单
                } else {
                    automaticBilling(replenishHList);
                }
            } catch (Exception e) {
                log.info("铺货自动开单报错 {}", e.getMessage());
            }

        } finally {
        }
        Result result = ResultUtil.success();
        result.setWarning(warning);
        autoNeedProDTO.setResult(result);
        return autoNeedProDTO;
    }

    /**
     * 构造 明细实体
     *
     * @param user
     * @param item
     * @param voucherId
     * @param index
     * @param batchNoInfo
     * @param gsrdNeedQty
     * @return
     */
    private GaiaSdReplenishD createGaiaSdReplenishD(TokenUser user,
                                                    SaveNeedProListRequestDTO item,
                                                    String voucherId,
                                                    AtomicInteger index,
                                                    BatchNoInfo batchNoInfo,
                                                    BigDecimal gsrdNeedQty) {
        GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
        replenishD.setClient(user.getClient());
        replenishD.setGsrdBrId(item.getStoCode());
        replenishD.setGsrdVoucherId(voucherId);
        replenishD.setGsrdSerial(String.valueOf(index.getAndIncrement()));
        replenishD.setGsrdProId(item.getProSelfCode());
        replenishD.setGsrdDate(DateUtils.getCurrentDateStrYYMMDD());
        replenishD.setGsrdBatch("");
        replenishD.setGsrdBatchNo(batchNoInfo != null ? batchNoInfo.getBatBatchNo() : "");
        replenishD.setGsrdGrossMargin("");
        replenishD.setGsrdSalesDays1(BigDecimal.ZERO);
        replenishD.setGsrdSalesDays2(BigDecimal.ZERO);
        replenishD.setGsrdSalesDays3(BigDecimal.ZERO);
        replenishD.setGsrdProposeQty(BigDecimal.ZERO);
        replenishD.setGsrdStockStore("");
        replenishD.setGsrdStockDepot("");
        replenishD.setGsrdDisplayMin("");
        replenishD.setGsrdPackMidsize("");
        replenishD.setGsrdLastSupid("");
        replenishD.setGsrdLastPrice(BigDecimal.ZERO);
        replenishD.setGsrdEditPrice(BigDecimal.ZERO);
        if (StringUtils.isBlank(item.getGsrdHwh())) {
            replenishD.setGsrdNeedQty(batchNoInfo != null ? batchNoInfo.getGsrdNeedQty().toString() : gsrdNeedQty.toString());
        } else {
            replenishD.setGsrdNeedQty(gsrdNeedQty.toString());
        }
        replenishD.setGsrdVoucherAmt(BigDecimal.ZERO);
        replenishD.setGsrdAverageCost(BigDecimal.ZERO);
        replenishD.setGsrdTaxRate(item.getGsrdTaxRate());
        replenishD.setGsrdHwh(item.getGsrdHwh());
        replenishD.setGsrdBz(item.getGsrdBz());
        return replenishD;
    }

    /**
     * 获取最大铺货单号
     *
     * @param client
     * @return
     */
    private String getMaxVoucherId(String client) {
        String maxVoucherId = "";
        while (true) {
            if (StringUtils.isBlank(maxVoucherId)) {
                maxVoucherId = gaiaSdReplenishHMapper.getMaxVoucherId(client);
                maxVoucherId = String.format("%06d", (Integer.parseInt(maxVoucherId) + 1));
            } else {
                maxVoucherId = String.format("%06d", (Integer.parseInt(maxVoucherId) + 1));
            }
            String exists = redisClient.get(CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId);
            log.info("补货单号:redis.getkey:{},value:{}", CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId, maxVoucherId);
            if (StringUtils.isBlank(exists)) {
                redisClient.set(CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId, maxVoucherId, 1800);
                log.info("补货单号:redis.setkey:{},value:{}", CommonConstants.GAIA_SD_REPLENISH_H_GSRH_VOUCHER_ID + client + maxVoucherId, maxVoucherId);
                return "UD" + LocalDate.now().getYear() + maxVoucherId;
            }
        }
    }


    /**
     * 商品选择
     *
     * @param selectProductListParams
     * @return
     */
    @Override
    public Result selectProductList(SelectProductListParams selectProductListParams, TokenUser tokenUser) {
        if (selectProductListParams == null) {
            throw new CustomResultException(ResultEnum.CHECK_PARAMETER);
        }
        if (CollectionUtils.isEmpty(selectProductListParams.getStoreReplenishDetailsList())) {
            throw new CustomResultException("未选择商品");
        }
        if (CollectionUtils.isEmpty(selectProductListParams.getGaiaStoreDataList())) {
            throw new CustomResultException("未选择门店");
        }
        TokenUser user;
        if (tokenUser == null) {
            user = feignService.getLoginInfo();
        } else {
            user = tokenUser;
        }
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(user.getClient());
        gaiaDcDataKey.setDcCode(selectProductListParams.getGsrhAddr());
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);

        List<GaiaProductBusinessKey> gaiaProductBusinessKeyList = new ArrayList<>();
        // 返回值
        SelectProductListParams result = new SelectProductListParams();
        // 可铺货商品
        List<StoreReplenishDetails> needProductList = gaiaSdReplenishHMapper.selectNeedProductList(user.getClient(), selectProductListParams.getGsrhAddr(),
                selectProductListParams.getGaiaStoreDataList(), selectProductListParams.getStoreReplenishDetailsList());
        // 初始化 批号信息
        for (StoreReplenishDetails storeReplenishDetails : needProductList) {
            // 库存批号明细
            List<BatchNoInfo> batchNoInfoList = null;
            if (StringUtils.isBlank(selectProductListParams.getDcWtdc())) {
                batchNoInfoList = gaiaSdReplenishHMapper.selectBatchNoInfoByWarehuPro(user.getClient(), selectProductListParams.getGsrhAddr(), storeReplenishDetails.getProSelfCode(), null);
            } else {
                batchNoInfoList = gaiaSdReplenishHMapper.selectBatchNoInfoByWarehuPro(user.getClient(), selectProductListParams.getDcWtdc(), storeReplenishDetails.getProSelfCode(), null);
            }
            storeReplenishDetails.setBatchNoInfoList(batchNoInfoList);
            storeReplenishDetails.setDcCode(selectProductListParams.getGsrhAddr());
            storeReplenishDetails.setDcWtdc(selectProductListParams.getDcWtdc());

            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(user.getClient());
            gaiaProductBusinessKey.setProSite(storeReplenishDetails.getStoCode());
            gaiaProductBusinessKey.setProSelfCode(storeReplenishDetails.getProSelfCode());
            gaiaProductBusinessKeyList.add(gaiaProductBusinessKey);
        }

        // 不可铺货商品
        List<GaiaProductBusiness> gaiaProductBusinessList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(gaiaProductBusinessKeyList)) {
            gaiaProductBusinessList = gaiaProductBusinessMapper.selectByPrimaryByBuyunxupinlei(user.getClient(), gaiaProductBusinessKeyList);
        }
        List<String> messageList = new ArrayList<>();
        for (GaiaProductBusiness gaiaProductBusiness : gaiaProductBusinessList) {
            messageList.add(StringUtils.parse("商品{}对于门店{}为不允许铺货的品类，不可铺货至门店。",
                    gaiaProductBusiness.getProSelfCode(), gaiaProductBusiness.getProSite()));
            // 删除不能铺货商品
            for (int i = 0; i < needProductList.size(); i++) {
                StoreReplenishDetails storeReplenishDetails = needProductList.get(i);
                if (gaiaProductBusiness.getProSelfCode().equals(storeReplenishDetails.getProSelfCode())
                        && gaiaProductBusiness.getProSite().equals(storeReplenishDetails.getStoCode())) {
                    needProductList.remove(i);
                }
            }
        }
        // 经营范围 验证
        if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && (gaiaDcData.getDcJyfwgk().equals("1") || "2".equals(gaiaDcData.getDcJyfwgk()))) {
            Map<String, List<String>> jyfwMap = new HashMap<>();
            Map<String, List<String>> class3Map = new HashMap<>();
            // 门店经营范围
            for (int i = needProductList.size() - 1; i >= 0; i--) {
                StoreReplenishDetails storeReplenishDetails = needProductList.get(i);
                List<String> jyfwList = jyfwMap.get(storeReplenishDetails.getStoCode());
                if (jyfwList == null) {
                    jyfwList = gaiaJyfwDataMapper.selectStoreJyfwBySto(user.getClient(), storeReplenishDetails.getStoCode());
                    jyfwMap.put(storeReplenishDetails.getStoCode(), jyfwList);
                }
                if ("2".equals(gaiaDcData.getDcJyfwgk())) {

                    List<String> class3List = class3Map.get(storeReplenishDetails.getStoCode());
                    if (class3List == null) {
                        class3List = gaiaJyfwDataMapper.selectStoreJyfwByStoClass3(user.getClient(), storeReplenishDetails.getStoCode());
                        class3Map.put(storeReplenishDetails.getStoCode(), class3List);
                    }
                    if (CollectionUtils.isNotEmpty(class3List)) {
                        continue;
                    }
                }
                if (CollectionUtils.isEmpty(jyfwList)) {
                    needProductList.remove(i);
                    messageList.add(StringUtils.parse("门店{}没有商品{}的经营范围，不可铺货！", storeReplenishDetails.getStoCode(), storeReplenishDetails.getProSelfCode()));
                    continue;
                }
                if (StringUtils.isBlank(storeReplenishDetails.getProJylb())) {
                    needProductList.remove(i);
                    messageList.add(StringUtils.parse("门店{}没有商品{}的经营范围，不可铺货！", storeReplenishDetails.getStoCode(), storeReplenishDetails.getProSelfCode()));
                    continue;
                }
                if (Collections.disjoint(jyfwList, Arrays.asList(storeReplenishDetails.getProJylb().split(",")))) {
                    needProductList.remove(i);
                    messageList.add(StringUtils.parse("门店{}没有商品{}的经营范围，不可铺货！", storeReplenishDetails.getStoCode(), storeReplenishDetails.getProSelfCode()));
                    continue;
                }
            }
        }
        result.setMessageList(messageList);

        //设置默认铺货量
        //新品铺货计划数据成本价界值
        String param11 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "NEW_PRODUCT_PLAN_COST_PRICE_CRITICAL_VALUE");
        //新品铺货计划数据铺货量（≥成本价界值）
        String param12 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "NEW_PRODUCT_PLAN_MAN_DISTRIBUTION_NUM");
        //新品铺货计划数据铺货量（<成本价界值）
        String param13 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "NEW_PRODUCT_PLAN_MIX_DISTRIBUTION_NUM");
        BigDecimal price = null;
        int min;
        int max;
        try {
            price = new BigDecimal(StringUtils.isNotBlank(param11) ? param11 : "100");
            max = Integer.parseInt(StringUtils.isNotBlank(param12) ? param12 : "2");
            min = Integer.parseInt(StringUtils.isNotBlank(param13) ? param13 : "3");
        } catch (NumberFormatException e) {
            price = new BigDecimal("100");
            max = 2;
            min = 3;
            e.printStackTrace();
        }
        for (StoreReplenishDetails details : needProductList) {
            if (ObjectUtils.isEmpty(details.getGsrdNeedQty())) {
                details.setGsrdNeedQty((details.getCostPrice() == null ? BigDecimal.ZERO : details.getCostPrice()).compareTo(price) > 0 ? max : min);
            }
        }

        // 商品 铺货 门店 集合
        result.setStoreReplenishDetailsList(needProductList);

//        List<StoreReplenishDetails> unNeedProductList = gaiaSdReplenishHMapper.selectUnNeedProductList(user.getClient(), selectProductListParams.getGsrhAddr(),
//                selectProductListParams.getGaiaStoreDataList(), selectProductListParams.getStoreReplenishDetailsList());
////         存在不可以铺货商品
//        if (unNeedProductList != null) {
//            Map<String, List<StoreReplenishDetails>> map = unNeedProductList.stream().collect(Collectors.groupingBy(StoreReplenishDetails::getProSelfCode));
//            if (map != null && !map.isEmpty()) {
//                List<String> messageList = new ArrayList<>();
//                for (String key : map.keySet()) {
////         商品不可以铺货的门店
//                    String stoCodes = map.get(key).stream().map(n -> n.getStoCode()).collect(Collectors.joining(","));
//                    messageList.add(StringUtils.parse("商品{}为特殊品类配置，不可以铺货到门店{}", key, stoCodes));
//                }
//                result.setMessageList(messageList);
//            }
//        }
        return ResultUtil.success(result);
    }

    /**
     * 门店铺货，自动开单
     */
    @Override
    public void automaticBilling(List<GaiaSdReplenishH> replenishHList) {
        if (CollectionUtils.isEmpty(replenishHList)) {
            return;
        }
        replenishHList.forEach(replenishH -> {
            if (!replenishH.getGsrhType().equals("2")) {
                return;
            }
            List<GaiaSdReplenishD> replenishDList = gaiaSdReplenishDMapper.getListByHeaderKey(replenishH.getClient(), replenishH.getGsrhBrId(), replenishH.getGsrhVoucherId());
            String autoReplenishCreateOrder = gaiaSdReplenishHMapper.wmsConfig(replenishH.getClient(), replenishH.getGsrhAddr(), CommonEnum.WmsConfrig.AUTO_REPLENISH_CREATE_ORDER.getCode());
            if (CommonEnum.NoYesStatus.YES.getCode().equals(autoReplenishCreateOrder)) {
                // 自动开单
                Map<String, Object> param = automaticCreateOrderParamInit(replenishH, replenishDList);
                automaticCreateOrderExcute(param);
            } else {
                String autoAppointBillCreateOrder = gaiaSdReplenishHMapper.wmsConfig(replenishH.getClient(), replenishH.getGsrhAddr(), CommonEnum.WmsConfrig.AUTO_APPOINT_BILL_CREATE_ORDER.getCode());
                if (CommonEnum.NoYesStatus.YES.getCode().equals(autoAppointBillCreateOrder)) {
                    boolean match = replenishDList.stream().filter(Objects::nonNull)
                            .anyMatch(item -> StringUtils.isNotBlank(item.getGsrdBatchNo()) || StringUtils.isNotBlank(item.getGsrdHwh()));

                    if (match) {
                        // 自动开单
                        Map<String, Object> param = automaticCreateOrderParamInit(replenishH, replenishDList);
                        automaticCreateOrderExcute(param);
                    }
                }
            }
        });
    }

    /**
     * 自动开单
     */
    private void automaticCreateOrderExcute(Map<String, Object> param) {
        try {
            log.info("铺货自动开单参数: {}", new JSONObject(param).toJSONString());
            FeignResult feignResult = wmsFeign.automaticCreateOrder(param);
            if (feignResult.getCode() == 0) {
                log.info("铺货自动开单成功,返回值: {}", JsonUtils.beanToJson(feignResult));
            } else {
                log.info("铺货自动开单失败,返回值: {}", JsonUtils.beanToJson(feignResult));
            }
        } catch (Exception e) {
            log.info("铺货自动开单异常");
        }
    }

    @Override
    public void autoCreateDistributionOrder(List<GaiaSdReplenishH> replenishHList, List<GaiaSdReplenishD> replenishDList) {
        try {
            replenishHList.forEach(replenishH -> {
                List<GaiaSdReplenishD> matchDList = replenishDList.stream().filter(replenishD -> replenishH.getGsrhVoucherId().equals(replenishD.getGsrdVoucherId()))
                        .collect(Collectors.toList());
                // 调拨开单-自动分配执行
                Map<String, Object> param = automaticCreateOrderParamInit(replenishH, matchDList);
                log.info("铺货自动开单(调拨开单)参数: {}", new JSONObject(param).toJSONString());
                FeignResult feignResult = wmsFeign.autoCreateTransferOrder(param);
                log.info("铺货自动开单(调拨开单)返回值: {}", JsonUtils.beanToJson(feignResult));
            });
        } catch (Exception e) {
            log.error(String.format("<新品铺货><调拨开单><调用wms自动分配开单服务异常：%s>", e.getMessage()));
        }
    }

    @Override
    public void autoEntrustOrder(GaiaSdReplenishH replenishH, List<GaiaSdReplenishD> replenishDList) {
        try {
            Map<String, Object> params = fillEntrustOrderParams(replenishH, replenishDList);
            // 委托出库-自动分配执行
            log.info("铺货自动开单(委托配送)参数: {}", JSON.toJSONString(params));
            FeignResult feignResult = wmsFeign.autoCreateEntrustOrder(params);
            log.info("铺货自动开单(委托配送)返回值: {}", JSON.toJSONString(feignResult));
        } catch (Exception e) {
            log.error(String.format("<新品铺货><委托开单><调用wms自动分配开单服务异常：%s>", e.getMessage()));
        }
    }

    private Map<String, Object> fillEntrustOrderParams(GaiaSdReplenishH replenishH, List<GaiaSdReplenishD> detailList) {
        List<Map<String, Object>> orderList = detailList.stream().filter(Objects::nonNull).map(detail -> {
            Map<String, Object> createOrder = new HashMap<>();
            createOrder.put("client", detail.getClient());
            createOrder.put("proSelfCode", detail.getGsrdProId());
            createOrder.put("voucherId", detail.getGsrdVoucherId());
            return createOrder;
        }).collect(Collectors.toList());
        Map<String, Object> params = new HashMap<>();
        params.put("orderList", orderList);
        Map<String, String> userMap = new HashMap<>();
        userMap.put("client", replenishH.getClient());
        userMap.put("userId", replenishH.getGsrhEmp());
        userMap.put("loginName", replenishH.getGsrhDnBy());
        userMap.put("proSite", replenishH.getGsrhAddr());
        params.put("user", userMap);
        return params;
    }

    private Map<String, Object> automaticCreateOrderParamInit(GaiaSdReplenishH header, List<GaiaSdReplenishD> detailsList) {
        List<Map<String, String>> createOrderList = detailsList.stream().filter(Objects::nonNull).map(details -> {
            Map<String, String> createOrder = new HashMap<>();
            createOrder.put("poReqType", header.getGsrhPattern());
            createOrder.put("gsrhType", header.getGsrhType());
            createOrder.put("poId", header.getGsrhVoucherId());
            createOrder.put("poLineNo", details.getGsrdSerial());
            createOrder.put("poCreateDate", header.getGsrhDate());
            createOrder.put("poHeadRemark", "");
            createOrder.put("poLineRemark", details.getGsrdBz());
            createOrder.put("customerNo", details.getGsrdBrId());
            createOrder.put("customerNo2", "");
            createOrder.put("poProCode", details.getGsrdProId());
            createOrder.put("poBatchNo", details.getGsrdBatchNo());
            createOrder.put("poQty", details.getGsrdNeedQty());
            createOrder.put("poPrice", "");
            createOrder.put("poLocationCode", "1000");
            createOrder.put("dnFlag", "");
            createOrder.put("dnBy", "");
            createOrder.put("dnDate", "");
            createOrder.put("dnId", "");
            createOrder.put("poCompleteFlag", "");
            createOrder.put("gsrhFlag", "");
            createOrder.put("dnQty", "");
            createOrder.put("hwh", details.getGsrdHwh());
            return createOrder;
        }).collect(Collectors.toList());


        Map<String, Object> param = new HashMap<>();
        param.put("client", header.getClient());
        param.put("proSite", header.getGsrhAddr());
        param.put("demandType", CommonEnum.WmsConfrig.AUTO_REPLENISH_CREATE_ORDER.getCode());
        param.put("createOrder", createOrderList);
        param.put("userId", header.getGsrhEmp());
        return param;
    }


    @Override
    public Result autoSelectProductList(String proSelfCodes, TokenUser tokenUser) {
        String[] proSelfCodeList = proSelfCodes.split(",");
        //获取配送中心【多配送中心默认取第一个配送中心】
        List<GaiaDcData> authDcList = getAuthDcList(tokenUser);
        if (authDcList == null || authDcList.size() == 0) {
            return ResultUtil.error("-1", "无配送中心");
        }
        String dcCode = authDcList.get(0).getDcCode();
        //获取门店列表
        List<GetStoreListByAuthDcDTO> storeListByAuthDc = getStoreListByAuthDc(dcCode, tokenUser, null, null);
        if (storeListByAuthDc == null) {
            return ResultUtil.success();
        }
        List<StoreReplenishDetails> replenishDetailList = new ArrayList<>();
        for (String proSelfCode : proSelfCodeList) {
            //获取自动铺货商品信息
            GetProListRequestDTO dto = new GetProListRequestDTO();
            dto.setDcCode(dcCode);
            dto.setProSelfCode(proSelfCode);
            List<StoreReplenishDetails> proList = getProList(dto, tokenUser);
            if (proList == null) {
                continue;
            }
            StoreReplenishDetails replenishDetails = fillStoreReplenishDetail(proList.get(0));
            replenishDetailList.add(replenishDetails);
        }
        //获取自动铺货单信息
        SelectProductListParams params = new SelectProductListParams();
        params.setGsrhAddr(dcCode);
        params.setGaiaStoreDataList(fillStoreDataList(storeListByAuthDc));
        params.setStoreReplenishDetailsList(replenishDetailList);
        return selectProductList(params, tokenUser);
    }

    /**
     * 自动填充门店铺货信息
     *
     * @param replenishDetails 商品信息
     */
    private StoreReplenishDetails fillStoreReplenishDetail(StoreReplenishDetails replenishDetails) {
        StoreReplenishDetails productDetail = new StoreReplenishDetails();
        BeanUtils.copyProperties(replenishDetails, productDetail);
        return productDetail;
    }

    /**
     * 填充门店编码列表
     *
     * @param storeListByAuthDc 门店列表
     */
    private List<String> fillStoreDataList(List<GetStoreListByAuthDcDTO> storeListByAuthDc) {
        List<String> storeList = new ArrayList<>();
        for (GaiaStoreData gaiaStoreData : storeListByAuthDc) {
            storeList.add(gaiaStoreData.getStoCode());
        }
        return storeList;
    }

    /**
     * 店型下拉框&区域下拉框
     */
    @Override
    public GetStoreTypeIdItemListAndAreaListDTO getStoreTypeIdItemListAndAreaList() {
        TokenUser user = feignService.getLoginInfo();
        // 店型列表
        List<GetStoreTypeIdItemListAndAreaListDTO.Item> storeTypeIdItemList = storeDataMapper.getStoreTypeIdItemList(user.getClient());
        // 区域列表
        List<GetStoreTypeIdItemListAndAreaListDTO.Item> storeAreaList = storeDataMapper.getStoreAreaList(user.getClient());

        GetStoreTypeIdItemListAndAreaListDTO dto = new GetStoreTypeIdItemListAndAreaListDTO();
        dto.setStoreAreaList(storeAreaList);
        dto.setStoreTypeIdItemList(storeTypeIdItemList);
        return dto;
    }

    /**
     * 库存对比铺货
     *
     * @param needProWmsParams
     * @return
     */
    @Override
    public Result selectNeedProListByWms(NeedProWmsParams needProWmsParams) {
        TokenUser user = feignService.getLoginInfo();
        // 分頁查詢
        PageHelper.startPage(needProWmsParams.getPageNum(), needProWmsParams.getPageSize());
        List<StoreReplenishDetails> storeReplenishDetailsList = gaiaSdReplenishDMapper.selectNeedProListByWms(user.getClient(), needProWmsParams);
        PageInfo<StoreReplenishDetails> page = new PageInfo<>(storeReplenishDetailsList);
        page.getList().forEach(item -> {
            // 库存批号明细
            List<BatchNoInfo> batchNoInfoList = gaiaSdReplenishHMapper.selectBatchNoInfoByWarehuPro(user.getClient(), needProWmsParams.getDcCode(), item.getProSelfCode(), null);
            item.setBatchNoInfoList(batchNoInfoList);
        });
        return ResultUtil.success(page);
    }

    /**
     * 管理区域
     *
     * @return
     */
    @Override
    public Result selectStoresGroup() {
        TokenUser user = feignService.getLoginInfo();
        // 管理区域
        List<GaiaSdStoresGroupSetDto> gaiaSdStoresGroupSetList = gaiaSdStoresGroupSetMapper.selectSdStoresGroupSetList(user.getClient(), "DX0004");
        if (CollectionUtils.isEmpty(gaiaSdStoresGroupSetList)) {
            return null;
        }
        // 门店
        List<GaiaSdStoresGroup> gaiaSdStoresGroupList = gaiaSdStoresGroupMapper.selectSdStoresGroupList(user.getClient());
        if (CollectionUtils.isEmpty(gaiaSdStoresGroupList)) {
            return ResultUtil.success(gaiaSdStoresGroupSetList);
        }
        for (GaiaSdStoresGroupSetDto gaiaSdStoresGroupSet : gaiaSdStoresGroupSetList) {
            List<GaiaSdStoresGroup> itemList = gaiaSdStoresGroupList.stream().filter(t -> gaiaSdStoresGroupSet.getGssgId().equals(t.getGssgId())).collect(Collectors.toList());
            gaiaSdStoresGroupSet.setGaiaSdStoresGroupList(itemList);
        }
        return ResultUtil.success(gaiaSdStoresGroupSetList);
    }

    /**
     * 门店规模
     *
     * @return
     */
    @Override
    public Result selectStoresScale() {
        TokenUser user = feignService.getLoginInfo();
        // 管理区域
        List<GaiaSdStoresGroupSetDto> gaiaSdStoresGroupSetList = gaiaSdStoresGroupSetMapper.selectSdStoresGroupSetList(user.getClient(), "DX0002");
        if (CollectionUtils.isEmpty(gaiaSdStoresGroupSetList)) {
            return null;
        }
        // 门店
        List<GaiaSdStoresGroup> gaiaSdStoresGroupList = gaiaSdStoresGroupMapper.selectSdStoresGroupList(user.getClient());
        if (CollectionUtils.isEmpty(gaiaSdStoresGroupList)) {
            return ResultUtil.success(gaiaSdStoresGroupSetList);
        }
        for (GaiaSdStoresGroupSetDto gaiaSdStoresGroupSet : gaiaSdStoresGroupSetList) {
            List<GaiaSdStoresGroup> itemList = gaiaSdStoresGroupList.stream().filter(t -> gaiaSdStoresGroupSet.getGssgId().equals(t.getGssgId())).collect(Collectors.toList());
            gaiaSdStoresGroupSet.setGaiaSdStoresGroupList(itemList);
        }
        return ResultUtil.success(gaiaSdStoresGroupSetList);
    }

    @Override
    public List<GetStoreListByAuthDcDTO> getElectronicCouponStoreList(TokenUser user, String gssgId, String stoDistrict) {
        String client = "";
        if (user == null) {
            client = feignService.getLoginInfo().getClient();
        } else {
            client = user.getClient();
        }
        return gaiaSdReplenishHMapper.getElectronicCouponStoreList(client, gssgId, stoDistrict);
    }

    @Override
    public Result selectBatchAndHw(StoreNeedHwAndBatchDTO storeNeedHwAndBatchDTO) {
        String client = feignService.getLoginInfo().getClient();
        List<BatchNoInfo> batchNoInfoList = new ArrayList<>();
        if (StringUtils.isBlank(storeNeedHwAndBatchDTO.getDcWtdc())) {
            batchNoInfoList = gaiaSdReplenishHMapper.selectBatchNoInfoByWarehuPro(client, storeNeedHwAndBatchDTO.getGsrhAddr(), storeNeedHwAndBatchDTO.getProSelfCode(), null);
        } else {
            batchNoInfoList = gaiaSdReplenishHMapper.selectBatchNoInfoByWarehuPro(client, storeNeedHwAndBatchDTO.getDcWtdc(), storeNeedHwAndBatchDTO.getProSelfCode(), null);
        }
        return ResultUtil.success(batchNoInfoList);
    }

    /**
     * 库存对比铺货导出
     * @param needProWmsParams
     * @return
     */
    @Override
    public void needProListByWmsExport(HttpServletResponse response , NeedProWmsParams needProWmsParams) {
        TokenUser user = feignService.getLoginInfo();
        //导出全部
        List<StoreReplenishDetails> storeReplenishDetailsList = gaiaSdReplenishDMapper.selectNeedProListByWms(user.getClient(), needProWmsParams);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        // 这里URLEncoder.encode可以防止中文乱码 当然和easyexcel没有关系
        String fileName = "铺货对比库存";
        try {
            fileName = URLEncoder.encode(fileName, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");

        try {
            EasyExcel.write(response.getOutputStream(),StoreReplenishDetails.class)
                    .registerWriteHandler(ExcelStyleUtil.getStyle())
                    .sheet(0).doWrite(storeReplenishDetailsList);
        } catch (IOException e) {
            e.printStackTrace();
        }



    }

    private void checkCreditQuotaAndZqts(GaiaStoreData gaiaStoreData, BigDecimal gsrhTotalAmt) {
        //门店补货校验信用金额和账期控制
        if (gaiaStoreData != null) {
            //启用信用管理
            if ("1".equals(gaiaStoreData.getStoCreditFlag()) && null != gaiaStoreData.getStoCreditQuota()) {
                BigDecimal totalAmt = BigDecimal.valueOf(0);
                BigDecimal stoArAmt = gaiaStoreData.getStoArAmt();
                if (null == gaiaStoreData.getStoArAmt()) {
                    stoArAmt = BigDecimal.ZERO;
                }
                //如果余额+本次汇总配送金额>信用额度
                totalAmt = totalAmt.add(gsrhTotalAmt).add(stoArAmt).setScale(4, BigDecimal.ROUND_HALF_UP);
                if (1 == totalAmt.compareTo(gaiaStoreData.getStoCreditQuota())) {
                    //总授信额度A元，欠款金额B元，剩余额度C元，本单金额D元，已不足！
                    //A：主数据里的授信额度
                    //B：主数据里的应收余额
                    //C：A-B
                    //D：本单金额
                    BigDecimal arrearsAmt = gaiaStoreData.getStoCreditQuota().subtract(stoArAmt).setScale(4, BigDecimal.ROUND_HALF_UP);
                    StringBuilder stringBuilder = new StringBuilder("总授信额度为");
                    stringBuilder.append(gaiaStoreData.getStoCreditQuota()).append("元，欠款金额为").append(stoArAmt).append("元，剩余额度为").append(arrearsAmt).append("元，本次金额").append(gsrhTotalAmt).append("元，已不足!");
                    throw new CustomResultException(stringBuilder.toString());
                }
            }
            if ("0".equals(gaiaStoreData.getStoZqlx())) {
                //如果是账期类型，检查当前日期往前推账期天数，如存在往来余额 > 0,报错
                if (null != gaiaStoreData.getStoZqts()) {
                    Date date = org.apache.commons.lang3.time.DateUtils.addDays(new Date(), -gaiaStoreData.getStoZqts());
                    String queryDate = DateUtil.format(date, "yyyyMMdd");
                    BigDecimal amount = gaiaWmsDiaoboZMapper.getZqAmount(gaiaStoreData.getClient(), gaiaStoreData.getStoCode(), queryDate);
                    if (BigDecimal.ZERO.compareTo(amount) < 0) {
                        throw new CustomResultException(String.format("账期天数为%s天，已有%s元超过账期尚未付款，请付款后再下单", gaiaStoreData.getStoZqts(), amount));
                    }
                }
            } else if ("1".equals(gaiaStoreData.getStoZqlx())) {
                //如果是固定账期，并且欠款标志为0，报错提示
                if ("0".equals(gaiaStoreData.getStoArrearsFlag())) {
                    throw new CustomResultException("不允许门店补货");
                }
            }
        }
    }
}
