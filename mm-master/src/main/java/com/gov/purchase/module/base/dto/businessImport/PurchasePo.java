package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021/5/7 10:58
 */
@Data
public class PurchasePo {

    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String poProCode;

    @ExcelValidate(index = 1, name = "数量", type = ExcelValidate.DataType.DECIMAL)
    private String poQty;

    @ExcelValidate(index = 2, name = "单价", type = ExcelValidate.DataType.DECIMAL)
    private String poPrice;

    @ExcelValidate(index = 3, name = "生产批号", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false)
    private String poBatchNo;

    @ExcelValidate(index = 4, name = "生产日期", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = false)
    private String poScrq;

    @ExcelValidate(index = 5, name = "有效期", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = false)
    private String poYxq;
}
