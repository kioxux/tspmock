package com.gov.purchase.module.priceCompare.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.priceCompare.dto.PriceCompareDto;
import com.gov.purchase.module.priceCompare.dto.PriceCompareInfoSaveDto;
import com.gov.purchase.module.priceCompare.dto.PriceComparePageDto;
import com.gov.purchase.module.priceCompare.service.PriceCompareService;
import com.gov.purchase.module.priceCompare.vo.PriceCompareProInfoVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.io.IOException;
import java.util.List;

@Api(tags = "比价")
@Validated
@RestController
@RequestMapping("priceCompare")
public class PriceCompareController {

    @Resource
    private PriceCompareService priceCompareService;


    @Log("比价 - 新增")
    @ApiOperation("比价 - 新增")
    @PostMapping("save")
    public Result save(@Valid @RequestBody PriceCompareDto param) {
        priceCompareService.save(param);
        return ResultUtil.success();
    }


    @Log("比价 - 分页")
    @ApiOperation("比价 - 分页")
    @PostMapping("page")
    public Result page(@RequestBody PriceComparePageDto param) {
        return ResultUtil.success(priceCompareService.page(param));
    }


    @Log("比价 - 截至时间")
    @ApiOperation("比价 - 截至时间")
    @PostMapping("offerEndTime/{id}")
    public Result offerEndTime(@PathVariable Long id, @RequestJson("offerEndTime") String offerEndTime, @RequestJson("isExpires") Boolean isExpires) {
        priceCompareService.offerEndTime(id, offerEndTime, isExpires);
        return ResultUtil.success();
    }


    @Log("比价 - 查看")
    @ApiOperation("比价 - 查看")
    @PostMapping("info/{id}")
    public Result info(@PathVariable Long id) {
        return ResultUtil.success(priceCompareService.info(id));
    }


    @Log("比价 - 保存")
    @ApiOperation("比价 - 保存")
    @PostMapping("info/save/{priceCompareId}")
    public Result infoSave(@PathVariable Long priceCompareId, @RequestBody @Size(min = 1) List<PriceCompareProInfoVo> params) {
        priceCompareService.infoSave(priceCompareId, params, 0);
        return ResultUtil.success();
    }


    @Log("比价 - 确认报价")
    @ApiOperation("比价 - 确认报价")
    @PostMapping("info/sure/{priceCompareId}")
    public Result infoSure(@PathVariable Long priceCompareId, @RequestBody @Size(min = 1) List<PriceCompareProInfoVo> params) {
        return priceCompareService.infoSure(priceCompareId, params);
    }


    @Log("比价 - 列表 - 导出")
    @ApiOperation("比价 - 列表 - 导出")
    @GetMapping("list/export/excel")
    public Result listExportExcel(PriceComparePageDto param, HttpServletResponse response) throws IOException {
        priceCompareService.listExportExcel(param, response);
        return ResultUtil.success();
    }


    @Log("比价 - 详情 - 导出")
    @ApiOperation("比价 - 详情 - 导出")
    @GetMapping("info/export/excel/{priceCompareId}")
    public Result infoExportExcel(@PathVariable Long priceCompareId, HttpServletResponse response) throws IOException {
        priceCompareService.infoExportExcel(priceCompareId, response);
        return ResultUtil.success();
    }


}
