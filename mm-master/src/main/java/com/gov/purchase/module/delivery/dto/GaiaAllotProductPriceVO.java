package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.entity.GaiaAllotProductPrice;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class GaiaAllotProductPriceVO extends GaiaAllotProductPrice {
    //成本价
    private BigDecimal costPrice;
    //商品名
    private String proName;
    private String id;
    //最新进价
    private BigDecimal latestInputPrice;
    //规格
    private String proSpecs;
    //单位
    private String proUnit;
    //厂家
    private String proFactName;
    private String proSite;
    //零售价
    private BigDecimal priceNormal;
    //销售级别
    private String proSalesClass;
    //商品定位
    private String proPosition;
    //是否医保 0 1
    private String proIfMed;
    //客户
    private String gapgCus;
}
