package com.gov.purchase.module.store.constant;

/**
 * 常量工具类
 */
public class Constant {

    /**
     * 更新标志位
     */
    public static final String IS_UPDATE = "1";

    /**
     * 电话号码正则表达式
     */
    public static final String TEL_REGEX = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9])|(16[6]))\\\\d{8}$";

    public static final String SUCCESS = "0";


}
