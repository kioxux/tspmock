package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierBusiness;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021/4/26 10:44
 */
@Data
public class EditSupVO {
    @NotNull(message = "供应商修改数据不能为空")
    private GaiaSupplierBusiness business;
//    @NotBlank(message = "备注不能为空")
    private String remarks;
}
