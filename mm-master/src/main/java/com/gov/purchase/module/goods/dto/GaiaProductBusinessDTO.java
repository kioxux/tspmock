package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode
public class GaiaProductBusinessDTO extends GaiaProductBusiness {

    private String id;

    /**
     * 进项税率名称
     */
    private String proInputTaxName;

    /**
     * 商品自分类
     */
    private String proSclassName;

    /**
     * 末次进货供应商
     */
    private String lastSupCode;

    /**
     * 末次进货供应商
     */
    private String lastSupName;

    /**
     * 末次进货价
     */
    private BigDecimal lastPoPrice;

    /**
     * 大分类名
     */
    private String proBigClassName;
    /**
     * 中分类名
     */
    private String proMidClassName;
    /**
     * 小分类名
     */
    private String proMinClassName;

    /**
     * 批发公司ID
     */
    private String wmsId;
}
