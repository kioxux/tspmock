package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PurchaseOrderResponseDto {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 采购凭证号
     */
    private String poId;

    /**
     * 供应商自编码
     */
    private String poSupplierId;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 供应商名称
     */
    private String supRegAdd;

    /**
     * 联系人
     */
    private String supBussinessContact;

    /**
     * 联系人电话
     */
    private String supContactTel;

    /**
     * 送货地址（配送中心地址、门店地址）
     */
    private String address;

    /**
     * 凭证日期
     */
    private String poDate;

    /**
     * 采购员
     */
    private String poCreateBy;

    /**
     * 采购人名
     */
    private String userNam;

    /**
     * 总金额
     */
    private BigDecimal totalPrice;

    /**
     * 订单数量
     */
    private BigDecimal orderCount;

    /**
     * 商品明细
     */
    private List<PurchaseItemResponseDto> itemList;

    /**
     * 审批日期
     */
    private String poApproveDate;

    /**
     * 审批人
     */
    private String poApproveByName;

}
