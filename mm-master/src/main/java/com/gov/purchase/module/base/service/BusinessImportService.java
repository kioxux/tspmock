package com.gov.purchase.module.base.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.base.dto.businessImport.ImportDto;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
public interface BusinessImportService {
    /**
     * 导入
     *
     * @return
     */
    Result businessImport(ImportDto dto);
}
