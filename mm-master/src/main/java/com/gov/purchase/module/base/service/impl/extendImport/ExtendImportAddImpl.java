package com.gov.purchase.module.base.service.impl.extendImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBasic;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.module.base.dto.excel.Extend;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.30
 */
@Service
public class ExtendImportAddImpl extends ExtendImport {

    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;

    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;

    /**
     * 业务验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.errorListExtend((LinkedHashMap<Integer, Extend>) dataMap);
    }

    /**
     * 主数据表验证
     *
     * @param extend
     * @param errorList
     * @param key
     */
    @Override
    protected void baseCheck(Extend extend, List<String> errorList, int key) {
        // 商品编码 存在验证
        GaiaProductBasic gaiaProductBasic = gaiaProductBasicMapper.selectByPrimaryKey(extend.getProCode());
        if (gaiaProductBasic != null) {
            errorList.add(MessageFormat.format(ResultEnum.E0116.getMsg(), key + 1, "商品编码"));
        }
    }

    /**
     * 业务数据验证
     *
     * @param extend
     * @param errorList
     * @param key
     */
    @Override
    protected void businessCheck(Extend extend, List<String> errorList, int key) {
        // 加盟商 商品自编码 存在验证
        List<GaiaProductBusiness> businessList = gaiaProductBusinessMapper.selfOnlyCheck(extend.getClient(), extend.getProSelfCode(), extend.getProSite());
        if (CollectionUtils.isNotEmpty(businessList)) {
            errorList.add(MessageFormat.format(ResultEnum.E0118.getMsg(), key + 1));
        }
    }

    /**
     * 数据验证完成
     *
     * @return
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 数据导入完成
     *
     * @param bulUpdateCode
     * @param map
     * @param <T>
     */
    @Override
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> map) {
        // 批量导入
        try {
            for (Integer key : map.keySet()) {
                // 行数据
                Extend extend = (Extend) map.get(key);

                //ProductBusiness表
                if (!StringUtils.isBlank(extend.getClient())) {
                    GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
                    BeanUtils.copyProperties(extend, gaiaProductBusiness);

                    //主键重复查询
                    GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                    BeanUtils.copyProperties(extend, gaiaProductBusinessKey);

                    GaiaProductBusiness gaiaProductBusinessInfo = gaiaProductBusinessMapper.selectByPrimaryKey(gaiaProductBusinessKey);

                    if (null != gaiaProductBusinessInfo) {
                        throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
                    }

                    // 商品状态
                    gaiaProductBusiness.setProStatus(extend.getProStatusValue());
                    // 禁止销售
                    gaiaProductBusiness.setProNoRetail(extend.getProNoRetailValue());
                    // 禁止采购
                    gaiaProductBusiness.setProNoPurchase(extend.getProNoPurchaseValue());
                    // 禁止配送
                    gaiaProductBusiness.setProNoDistributed(extend.getProNoDistributedValue());
                    // 禁止退厂
                    gaiaProductBusiness.setProNoSupplier(extend.getProNoSupplierValue());
                    // 禁止退仓
                    gaiaProductBusiness.setProNoDc(extend.getProNoDcValue());
                    // 禁止调剂
                    gaiaProductBusiness.setProNoAdjust(extend.getProNoAdjustValue());
                    // 禁止批发
                    gaiaProductBusiness.setProNoSale(extend.getProNoSaleValue());
                    // 禁止请货
                    gaiaProductBusiness.setProNoApply(extend.getProNoApplyValue());
                    // 是否拆零
                    gaiaProductBusiness.setProIfpart(extend.getProIfpartValue());
                    // 是否医保
                    gaiaProductBusiness.setProIfMed(extend.getProIfMedValue());
                    // 按中包装量倍数配货
                    gaiaProductBusiness.setProPackageFlag(extend.getProPackageFlagValue());
                    // 是否重点养护
                    gaiaProductBusiness.setProKeyCare(extend.getProKeyCareValue());

                    // 清空数据处理
                    super.initNull(gaiaProductBusiness);

                    //限购数量
                    if (!ImportData.NULLVALUE.equals(extend.getProLimitQty()) && !StringUtils.isBlank(extend.getProLimitQty())) {
                        gaiaProductBusiness.setProLimitQty(new BigDecimal(extend.getProLimitQty()));
                    }
                    //最小订货量
                    if (!ImportData.NULLVALUE.equals(extend.getProLimitQty()) && !StringUtils.isBlank(extend.getProMinQty())) {
                        gaiaProductBusiness.setProMinQty(new BigDecimal(extend.getProMinQty()));
                    }
                    //含麻最大配货量
                    if (!ImportData.NULLVALUE.equals(extend.getProMaxQty()) && !StringUtils.isBlank(extend.getProMaxQty())) {
                        gaiaProductBusiness.setProMaxQty(new BigDecimal(extend.getProMaxQty()));
                    }
                    gaiaProductBusiness.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    gaiaProductBusiness.setProStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
                    if(StringUtils.isBlank(gaiaProductBusiness.getProStorageCondition())){
                        gaiaProductBusiness.setProStorageCondition("1");
                    }
                    if (StringUtils.isBlank(gaiaProductBusiness.getProCompclass())) {
                        gaiaProductBusiness.setProCompclass("N999999");
                        gaiaProductBusiness.setProCompclassName("N999999未匹配");
                    }
                    gaiaProductBusinessMapper.insertSelective(gaiaProductBusiness);
                }
            }
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }

        // 更新导入日志
        editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());
        return ResultUtil.success();
    }
}

