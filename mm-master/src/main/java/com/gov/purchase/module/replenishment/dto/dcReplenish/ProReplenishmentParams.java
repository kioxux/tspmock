package com.gov.purchase.module.replenishment.dto.dcReplenish;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.02
 */
@Data
public class ProReplenishmentParams {

    /**
     * 1：相等，2：模糊，3：in
     */
    private Integer type;

    private String field;

    private Object data;
}

