package com.gov.purchase.module.goods.dto;

import lombok.Data;

/**
 * @description: 药品质量档案表
 * @author: yzf
 * @create: 2021-11-08 17:06
 */
@Data
public class DrugQualityDTO {
    /**
     * 机构编号
     */
    private String code;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    private Integer count;

    private String client;
}
