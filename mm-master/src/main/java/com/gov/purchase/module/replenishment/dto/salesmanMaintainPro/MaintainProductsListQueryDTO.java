package com.gov.purchase.module.replenishment.dto.salesmanMaintainPro;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@ApiModel("业务员商品授权维护查询返回对象DTO")
@Data
public class MaintainProductsListQueryDTO {

    @ApiModelProperty("是否按照品种管控 1：是，0：否")
    private String isControl;

    @ApiModelProperty("授权商品DTO列表")
    private List<SupplierSalesmanProDTO> supplierSalesmanProDTOS;
}
