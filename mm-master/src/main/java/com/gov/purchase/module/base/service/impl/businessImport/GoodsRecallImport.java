package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.businessImport.GaiaRecallDto;
import com.gov.purchase.module.base.dto.businessImport.GoodsRecall;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.delivery.dto.StockNumRequestDto;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import com.gov.purchase.module.supplier.dto.QueryUnDrugSupCheckRequestDto;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class GoodsRecallImport extends BusinessImport {

    /**
     * 加盟商key
     */
    private final static String CLIENT = "client";

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    GaiaBatchStockMapper gaiaBatchStockMapper;
    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    GaiaCompadmMapper gaiaCompadmMapper;
    @Resource
    GaiaRecallInfoMapper gaiaRecallInfoMapper;
    @Resource
    GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    /**
     * 业务验证(商品召回)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {

        // 加盟商
        Object client = field.get(GoodsRecallImport.CLIENT);
        if (client == null || StringUtils.isBlank(client.toString())) {
            throw new CustomResultException(ResultEnum.E0140);
        }

        //错误LIST
        List<String> errorList = new ArrayList<>();

        // 批量导入
        try {
            List<GaiaRecallDto> gaiaRecallList = new ArrayList<>();
            // 证照编码
            Map<String, List<String>> recProCodeMap = new HashMap<>();

            for (Integer key : map.keySet()) {
                GaiaRecallDto gaiaRecallInfo = new GaiaRecallDto();
                // 行数据
                GoodsRecall goodsRecall = (GoodsRecall) map.get(key);

                // 连锁公司 门店 验证
                GaiaCompadm gaiaCompadm = gaiaCompadmMapper.selectByPrimaryId(client.toString(), goodsRecall.getRecChainHead());
                if (gaiaCompadm == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "连锁公司"));
                    continue;
                }

                if (StringUtils.isNotBlank(goodsRecall.getRecStore())) {
                    // 门店
                    GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                    gaiaStoreDataKey.setClient(client.toString());
                    gaiaStoreDataKey.setStoCode(goodsRecall.getRecStore());
                    GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                    // DC数据
                    GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
                    gaiaDcDataKey.setClient(client.toString());
                    gaiaDcDataKey.setDcCode(goodsRecall.getRecStore());
                    GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
                    if (gaiaStoreData == null && gaiaDcData == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店"));
                        continue;
                    }
                    if (gaiaStoreData != null && !gaiaCompadm.getCompadmId().equals(gaiaStoreData.getStoChainHead())) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店"));
                        continue;
                    }
                    if (gaiaDcData != null && !gaiaCompadm.getCompadmId().equals(gaiaDcData.getDcChainHead())) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店"));
                        continue;
                    }
                }
                // 商品编码
                List<GetProductListResponseDto> list = gaiaRecallInfoMapper.getProductListForRecall(client.toString(), goodsRecall.getRecChainHead(), goodsRecall.getRecProCode());
                if (CollectionUtils.isEmpty(list)) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品编码"));
                    continue;
                } else {
                    // 门店库存
                    gaiaRecallInfo.setBatNormalQty(list.get(0).getStoreNum());
                }
                // 供应商
                if (StringUtils.isNotBlank(goodsRecall.getRecSupplier())) {
                    QueryUnDrugSupCheckRequestDto dto = new QueryUnDrugSupCheckRequestDto();
                    dto.setClient(client.toString());
                    dto.setSupSite(goodsRecall.getRecStore());
                    dto.setSupSelfCode(goodsRecall.getRecSupplier());
                    GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByCode(dto);
                    if (gaiaSupplierBusiness == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "供应商编码"));
                        continue;
                    }
                }
                // 商品编码
                List<String> proCodeList = recProCodeMap.get(goodsRecall.getRecProCode());
                if (proCodeList == null) {
                    proCodeList = new ArrayList<>();
                }
                proCodeList.add(String.valueOf((key + 1)));
                recProCodeMap.put(goodsRecall.getRecProCode(), proCodeList);
                gaiaRecallInfo.setRecChainHead(goodsRecall.getRecChainHead());
                gaiaRecallInfo.setRecStore(goodsRecall.getRecStore());
                gaiaRecallInfo.setRecProCode(goodsRecall.getRecProCode());
                gaiaRecallInfo.setRecBatchNo(goodsRecall.getRecBatchNo());
                gaiaRecallInfo.setRecSupplier(goodsRecall.getRecSupplier());
                if (StringUtils.isNotBlank(goodsRecall.getRecQty())) {
                    gaiaRecallInfo.setRecQty(new BigDecimal(goodsRecall.getRecQty()));
                } else {
                    // 库存
                    GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
                    // 加盟商
                    gaiaMaterialAssessKey.setClient(client.toString());
                    // 商品编码
                    gaiaMaterialAssessKey.setMatProCode(goodsRecall.getRecProCode());
                    // 估价地点
                    gaiaMaterialAssessKey.setMatAssessSite(goodsRecall.getRecChainHead());
                    GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
                    gaiaRecallInfo.setRecQty(BigDecimal.ZERO);
                    if (gaiaMaterialAssess != null) {
                        gaiaRecallInfo.setRecQty(gaiaMaterialAssess.getMatTotalQty());
                    }
                }
                gaiaRecallInfo.setRecRemark(goodsRecall.getRecRemark());
                gaiaRecallList.add(gaiaRecallInfo);
            }

            //商品编码重复验证
            for (String recProCode : recProCodeMap.keySet()) {
                List<String> lineList = recProCodeMap.get(recProCode);
                if (lineList.size() > 1) {
                    errorList.add(MessageFormat.format(ResultEnum.E0026.getMsg(), String.join(",", lineList)));
                }
            }

            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            return ResultUtil.success(gaiaRecallList);

        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }
    }
}
