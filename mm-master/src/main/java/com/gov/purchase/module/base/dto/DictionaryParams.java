package com.gov.purchase.module.base.dto;

import com.gov.purchase.common.request.RequestJson;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.24
 */
@Data
@ApiModel(value = "字典表查询参数")
public class DictionaryParams {
    /**
     * 字典key
     */
    @ApiModelProperty(value = "字典key", name = "key")
    @NotBlank(message = "key不能为空")
    private String key;
    /**
     * 分页
     */
    @ApiModelProperty(value = "分页", name = "pageNum")
    private Integer pageNum;
    /**
     * 分页
     */
    @ApiModelProperty(value = "分页", name = "pageSize")
    private Integer pageSize;
    /**
     * 模糊参数
     */
    @ApiModelProperty(value = "模糊参数", name = "params")
    private Map<String, String> params;
    /**
     * value label 转置
     */
    @ApiModelProperty(value = "转置", name = "type")
    private String type;
    /**
     * 精确查询参数
     */
    @ApiModelProperty(value = "精确参数", name = "exactParams")
    private Map<String, String> exactParams;
}
