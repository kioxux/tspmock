package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.DictionaryMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.businessImport.AddBusinessScope;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class BusinessScopeInfoImport extends BusinessImport {

    @Resource
    DictionaryMapper dictionaryMapper;

    /**
     * 业务验证(经营范围编辑)
     *
     * @param map 数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {

        //错误LIST
        List<String> errorList = new ArrayList<>();

        // 批量导入
        try {
            List<AddBusinessScope> addBusinessScopeList = new ArrayList<>();

            // 证照编码
            Map<String, List<String>> bscCodeMap = new HashMap<>();

            // 经营范围编码
            List<Dictionary> licenceList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.BUSINESS_SCOPE_CLASS.getTable(),
                    CommonEnum.DictionaryData.BUSINESS_SCOPE_CLASS.getField1(),
                    CommonEnum.DictionaryData.BUSINESS_SCOPE_CLASS.getCondition(), CommonEnum.DictionaryData.BUSINESS_SCOPE_CLASS.getSortFields());

            for (Integer key : map.keySet()) {
                // 行数据
                AddBusinessScope addBusinessScope = (AddBusinessScope) map.get(key);

                // 经营范围编码
                if (StringUtils.isNotBlank(addBusinessScope.getBscCode())) {
                    Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(licenceList, addBusinessScope.getBscCode());
                    if (dictionary == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "经营范围编码"));
                    } else {
                        // 经营范围名称
                        if (!dictionary.getLabel().equals(addBusinessScope.getBscCodeName())) {
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "经营范围名称"));
                        }
                    }
                }

                List<String> bscCodeList = bscCodeMap.get(addBusinessScope.getBscCode());
                if (bscCodeList == null) {
                    bscCodeList = new ArrayList<>();
                }

                bscCodeList.add(String.valueOf((key + 1)));
                bscCodeMap.put(addBusinessScope.getBscCode(), bscCodeList);

                addBusinessScopeList.add(addBusinessScope);
            }

            //经营范围编码重复验证
            for (String bscCode : bscCodeMap.keySet()) {
                List<String> lineList = bscCodeMap.get(bscCode);
                if (lineList.size() > 1) {
                    errorList.add(MessageFormat.format(ResultEnum.E0023.getMsg(), String.join(",", lineList)));
                }
            }

            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            return ResultUtil.success(addBusinessScopeList);

        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }
    }
}
