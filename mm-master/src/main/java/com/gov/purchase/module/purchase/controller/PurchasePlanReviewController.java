package com.gov.purchase.module.purchase.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewDto;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewVo;
import com.gov.purchase.module.purchase.service.PurchasePlanReviewService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 采购计划审批
 * @author: yzf
 * @create: 2021-12-02 11:32
 */
@Api(tags = "采购计划审批")
@RestController
@RequestMapping("purchasePlanReview")
public class PurchasePlanReviewController {

    @Resource
    private PurchasePlanReviewService purchasePlanReviewService;


    @Log("采购计划查询")
    @ApiOperation("采购计划查询")
    @PostMapping("getPurchasePlanReviewList")
    public Result getPurchasePlanReviewList(@RequestBody PurchasePlanReviewDto dto) {
        return ResultUtil.success(purchasePlanReviewService.queryPurchasePlanReviewList(dto));
    }

    @Log("采购计划明细")
    @ApiOperation("采购计划明细")
    @PostMapping("getPurchasePlanReviewDetailList")
    public Result getPurchasePlanReviewDetailList(@RequestBody PurchasePlanReviewDto dto) {
        return ResultUtil.success(purchasePlanReviewService.queryPurchasePlanReviewDetailList(dto));
    }

    @Log("审核采购计划")
    @ApiOperation("审核采购计划")
    @PostMapping("updatePurchasePlan")
    public Result updatePurchasePlan(@RequestBody PurchasePlanReviewDto dto) {
        dto.setPoApproveStatus("2");
        return ResultUtil.success(purchasePlanReviewService.updatePurchasePlan(dto));
    }

    @Log("采购计划拒绝")
    @ApiOperation("采购计划拒绝")
    @PostMapping("refusePurchasePlan")
    public Result refusePurchasePlan(@RequestBody PurchasePlanReviewDto dto) {
        return ResultUtil.success(purchasePlanReviewService.refusePurchasePlan(dto));
    }

    @Log("采购计划明细保存")
    @ApiOperation("采购计划明细保存")
    @PostMapping("updatePurchasePlanDetail")
    public Result updatePurchasePlanDetail(@RequestBody PurchasePlanReviewVo vo) {
        return ResultUtil.success(purchasePlanReviewService.updatePurchasePlanDetail(vo));
    }

    @Log("审核采购计划明细")
    @ApiOperation("审核采购计划明细")
    @PostMapping("updatePurchasePlanAndDetail")
    public Result updatePurchasePlanAndDetail(@RequestBody PurchasePlanReviewVo vo) {
        return ResultUtil.success(purchasePlanReviewService.updatePurchasePlanAndDetail(vo));
    }

    /**
     * 导出采购计划列表
     *
     * @param dto
     * @return
     */
    @Log("导出采购计划列表")
    @ApiOperation("导出采购计划列表")
    @PostMapping("exportList")
    public Result exportList(@RequestBody PurchasePlanReviewDto dto) {
        return purchasePlanReviewService.exportList(dto);
    }

    @Log("反审采购计划")
    @ApiOperation("反审采购计划")
    @PostMapping("retrialPurchasePlan")
    public Result retrialPurchasePlan(@RequestBody PurchasePlanReviewDto dto) {
        dto.setPoApproveStatus("1");
        return ResultUtil.success(purchasePlanReviewService.updatePurchasePlan(dto));
    }

}
