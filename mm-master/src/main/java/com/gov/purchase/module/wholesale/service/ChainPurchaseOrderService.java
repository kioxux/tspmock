package com.gov.purchase.module.wholesale.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaWholesaleSalesman;
import com.gov.purchase.module.wholesale.dto.ChainPurchaseOrderDto;
import com.gov.purchase.module.wholesale.dto.CompadmWmsVO;
import com.gov.purchase.module.wholesale.dto.PurchaseCompanyVO;

import java.util.List;

/**
 * @author yzf
 * 连锁采购单
 */
public interface ChainPurchaseOrderService {

    /**
     * 批发仓库列表
     *
     * @return
     */
    List<CompadmWmsVO> queryCompadmWms();

    /**
     * 客户列表
     *
     * @return
     */
    List<PurchaseCompanyVO> queryCompanyCode();

    /**
     * 门店列表
     *
     * @return
     */
    List<GaiaStoreData> queryStoreList();

    /**
     * 连锁采购单列表
     *
     * @param chainPurchaseOrderDto
     * @return
     */
    Result queryChainPurchaseOrderList(ChainPurchaseOrderDto chainPurchaseOrderDto);

    /**
     * 关闭连锁采购单
     *
     * @param chainPurchaseOrderDto
     * @return
     */
    Result updateFlag(List<ChainPurchaseOrderDto> chainPurchaseOrderDto);

    /**
     * 连锁采购单下-明细列表
     *
     * @param chainPurchaseOrderDto
     * @return
     */
    Result queryChainPurchaseOrderDetailList(ChainPurchaseOrderDto chainPurchaseOrderDto);


    /**
     * 连锁采购明细列表
     *
     * @param chainPurchaseOrderDto
     * @return
     */
    Result queryChainPurchaseDetails(ChainPurchaseOrderDto chainPurchaseOrderDto);


    /**
     * 审核连锁采购单
     * @param chainPurchaseOrderDto
     * @return
     */
    Result auditChainPurchaseOrder(List<ChainPurchaseOrderDto> chainPurchaseOrderDto);

    /**
     * 销售员列表
     * @return
     */
    List<GaiaWholesaleSalesman> querySalesManList();
}
