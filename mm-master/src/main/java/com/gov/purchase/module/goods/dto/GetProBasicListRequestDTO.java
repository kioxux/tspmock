package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetProBasicListRequestDTO {
    /**
     * 国际条形码
     */
    private String proBarcode;
    /**
     * 批准文号
     */
    private String proRegisterNo;
    /**
     * 商品名称
     */
    private String proName;

    /**
     * 当前行商品自编码
     */
    private String currentRowProSeflCode;

    /**
     * 最后一行商品自编码
     */
    private String lastRowProSeflCode;

}
