package com.gov.purchase.module.store.dto.store;

import com.gov.purchase.common.excel.annotation.ExcelField;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class StoreMinQtyExportDto {

    /**
     * 门店编码
     */
    @ExcelField(title = "门店编码", type = 1, sort = 0)
    private String gpmdBrId;


    /**
     * 门店名称
     */
    @ExcelField(title = "门店名称", type = 1, sort = 1)
    private String stoName;


    /**
     * 商品编码
     */
    @ExcelField(title = "商品编码", type = 1, sort = 2)
    private String gpmdProId;

    /**
     * 商品名称
     */
    @ExcelField(title = "商品名称", type = 1, sort = 3)
    private String proName;

    /**
     * 规格
     */
    @ExcelField(title = "规格", type = 1, sort = 4)
    private String proSpecs;

    /**
     * 生产厂家
     */
    @ExcelField(title = "厂家", type = 1, sort = 5)
    private String proFactoryName;

    /**
     * 单位
     */
    @ExcelField(title = "单位", type = 1, sort = 6)
    private String proUnit;

    /**
     * 最小陈列量
     */
    @ExcelField(title = "最小陈列量", type = 1, sort = 7)
    private BigDecimal gpmdMinQty;


}
