package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 批发销售/退货审批传参
 * @author: yzf
 * @create: 2021-11-11 09:18
 */
@Data
public class WholeSaleApproveVo {


    /**
     * 加盟商
     */
    private String client;

    /**
     * 客户名称
     */
    private String cusName;

    /**
     * 单据日期
     */
    private String soDate;

    /**
     * 销售单号
     */
    private String soId;

    /**
     * 客户编码
     */
    private String soCustomerId;

    /**
     * 单据行数
     */
    private String soLine;

    /**
     * 审核状态
     */
    private String soApproveStatus;

    /**
     * 单据总数量
     */
    private BigDecimal soCount;

    /**
     * 单据总金额
     */
    private BigDecimal soTotal;

    /**
     * 创建人
     */
    private String createBy;

    /**
     * 单据备注
     */
    private String soRemark;

    /**
     * 地点
     */
    private String soCompanyCode;

    /**
     * 审批人ID
     */
    private String soApproveBy;

    /**
     * 审批日期
     */
    private String soApproveDate;

    /**
     * 审批人姓名
     */
    private String soApproveName;

    /**
     * 原销售订单号
     */
    private String soReferOrder;
}
