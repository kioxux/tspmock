package com.gov.purchase.module.store.dto.store;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors( chain =true)
@ApiModel(value = "门店列表传入参数")
public class StoreListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "门店编号", name = "stoCode")
    private String stoCode;

    @ApiModelProperty(value = "门店名称", name = "stoName")
    private String stoName;

    @ApiModelProperty(value = "门店状态 0-营业、1-闭店", name = "stoStatus")
    private String  stoStatus;

}
