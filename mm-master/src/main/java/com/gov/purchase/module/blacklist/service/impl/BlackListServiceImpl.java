package com.gov.purchase.module.blacklist.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaUserData;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProBatchHmdMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.blacklist.constant.BlackListConstant;
import com.gov.purchase.module.blacklist.dto.BlackListRequestDto;
import com.gov.purchase.module.blacklist.dto.BlackListResponseDto;
import com.gov.purchase.module.blacklist.service.BlackListService;
import com.gov.purchase.module.supplier.dto.GaiaCustomerBusinessDTO;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import com.qcloud.cos.utils.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 批号黑名单服务
 */
@Service
public class BlackListServiceImpl implements BlackListService {

    @Resource
    private GaiaProBatchHmdMapper gaiaProBatchHmdMapper;
    @Resource
    private FeignService feignService;
    @Resource
    CosUtils cosUtils;

    /**
     * 新增一个批号黑名单
     *
     * @param dto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result saveBlackList(BlackListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        dto.setProSite(user.getDepId());
        if (StringUtils.isEmpty(dto.getProBatchNo())) {
            //1.0先检验该数据是否已存在黑名单
            List<BlackListRequestDto> hasList = gaiaProBatchHmdMapper.checkBlackExist(dto.getClient(), dto.getProSite(), dto.getProSelfCode(), null, BlackListConstant.TWO);
            if (!CollectionUtils.isNullOrEmpty(hasList)) {
                throw new CustomResultException("该条记录已存在黑名单，请勿重复添加！");
            }
            //2.0 如果 没有批次号的 查一下，有没有这个商品带批次的黑名单。 如果有 删除后新增
            List<BlackListRequestDto> existList = gaiaProBatchHmdMapper.checkBlackExist(dto.getClient(), dto.getProSite(), dto.getProSelfCode(), dto.getProBatchNo(), BlackListConstant.ONE);
            if (!CollectionUtils.isNullOrEmpty(existList)) {
                gaiaProBatchHmdMapper.deleteBlackList(existList);
            }
            //没有生产批号需要更新GAIA_PRODUCT_BUSINESS.PRO_OUT 置为 1
            List<BlackListRequestDto> list = new ArrayList<>();
            list.add(dto);
            gaiaProBatchHmdMapper.updateBusinessProOut(list, BlackListConstant.ONE);
        } else {
            //1.0先检验该数据是否已存在黑名单
            List<BlackListRequestDto> hasList = gaiaProBatchHmdMapper.checkBlackExist(dto.getClient(), dto.getProSite(), dto.getProSelfCode(), dto.getProBatchNo(), null);
            if (!CollectionUtils.isNullOrEmpty(hasList)) {
                throw new CustomResultException("该条记录已存在黑名单，请勿重复添加！");
            }
            //2.0 如果 有批次号的 查一下，有没有这个商品的黑名单。 如果没有能保存
            List<BlackListRequestDto> existList = gaiaProBatchHmdMapper.checkBlackExist(dto.getClient(), dto.getProSite(), dto.getProSelfCode(), null, BlackListConstant.TWO);
            if (!CollectionUtils.isNullOrEmpty(existList)) {
                throw new CustomResultException("该条商品已存在黑名单，无需单独添加此批号！");
            }

        }
        String date = DateUtils.getCurrentDateStrYYMMDD();
        String time = DateUtils.getCurrentTimeStrHHMMSS();
        setBlackList(dto, date, time, user);
        //新增数据
        gaiaProBatchHmdMapper.save(dto);
        return ResultUtil.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result deleteBatchBlackList(List<BlackListRequestDto> dtoList) {
        List<BlackListRequestDto> noBatchNoList = dtoList.stream().filter(item -> StringUtils.isBlank(item.getProBatchNo())).collect(Collectors.toList());
        if (!CollectionUtils.isNullOrEmpty(noBatchNoList)) {
            gaiaProBatchHmdMapper.updateBusinessProOut(noBatchNoList, null);
        }
        gaiaProBatchHmdMapper.deleteBlackList(dtoList);
        return ResultUtil.success();
    }

    @Override
    public List<BlackListResponseDto> listBlack(BlackListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        if (StringUtils.isNotEmpty(dto.getProBatchNo())) {
            String[] proBatchNoList = dto.getProBatchNo().split(",");
            dto.setProBatchNoList(proBatchNoList);
        }
        return gaiaProBatchHmdMapper.listBlack(dto);
    }

    private String[] headList = {
            "门店",
            "商品编号",
            "商品名称",
            "规格",
            "厂家",
            "单位",
            "生产批号",
            "库存数量"
    };

    @Override
    public Result exportBlackList(BlackListRequestDto dto) {
        List<BlackListResponseDto> blackList = this.listBlack(dto);
        try {
            List<List<String>> dataList = new ArrayList<>();
            for (BlackListResponseDto black : blackList) {
                // 打印行
                List<String> item = new ArrayList<>();
                dataList.add(item);
                // 门店
                item.add(black.getAddr());
                // 商品编号
                item.add(black.getProSelfCode());
                // 商品名称
                item.add(black.getProName());
                // 规格
                item.add(black.getProSpecs());
                // 厂家
                item.add(black.getProFactoryName());
                // 单位
                item.add(black.getProUnit());
                // 生产批号
                item.add(black.getProBatchNo());
                // 库存数量
                item.add(black.getQty().toString());
            }
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("批号黑名单");
                    }});
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }


    private void setBlackList(BlackListRequestDto dto, String date, String time, TokenUser user) {
        dto.setProCreateUser(user.getUserId());
        dto.setProCreateDate(date);
        dto.setProCreateTime(time);
        dto.setProChangeUser(user.getUserId());
        dto.setProChangeDate(date);
        dto.setProChangeTime(time);
    }
}
