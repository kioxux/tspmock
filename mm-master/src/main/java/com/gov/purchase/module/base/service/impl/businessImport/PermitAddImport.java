package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.DictionaryMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.businessImport.PermitData;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.qa.dto.PermitDetailDto;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class PermitAddImport extends BusinessImport {

    @Resource
    DictionaryMapper dictionaryMapper;

    /**
     * 业务验证(新增证照页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {

        //错误LIST
        List<String> errorList = new ArrayList<>();

        // 批量导入
        try {
            List<PermitDetailDto> permitDetailList = new ArrayList<>();
            // 证照编码
            Map<String, List<String>> perCodeMap = new HashMap<>();

            // 证照资质
            List<Dictionary> licenceList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.LICENCE_CLASS.getTable(),
                    CommonEnum.DictionaryData.LICENCE_CLASS.getField1(),
                    CommonEnum.DictionaryData.LICENCE_CLASS.getCondition(), CommonEnum.DictionaryData.LICENCE_CLASS.getSortFields());

            for (Integer key : map.keySet()) {
                // 行数据
                PermitData permitData = (PermitData) map.get(key);

                // 证照资质
                if (StringUtils.isNotBlank(permitData.getPerCode())) {
                    Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(licenceList, permitData.getPerCode());
                    if (dictionary == null) {
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "证照编码"));
                    } else {
                        // 证照名称
                        if (!dictionary.getLabel().equals(permitData.getPerName())) {
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "证照名称"));
                        }
                    }
                }

                // 有效期从 有效期至 比较
                if (StringUtils.isNotBlank(permitData.getPerDateStart()) && StringUtils.isNotBlank(permitData.getPerDateEnd())) {
                    // 有效期从>=有效期至
                    if (permitData.getPerDateStart().compareTo(permitData.getPerDateEnd()) >= 0) {
                        errorList.add(MessageFormat.format(ResultEnum.E0024.getMsg(), key + 1));
                    }
                }

                List<String> perCodeList = perCodeMap.get(permitData.getPerCode());
                if (perCodeList == null) {
                    perCodeList = new ArrayList<>();
                }

                perCodeList.add(String.valueOf((key + 1)));
                perCodeMap.put(permitData.getPerCode(), perCodeList);

                PermitDetailDto permitDetailDto = new PermitDetailDto();
                permitDetailDto.setPerCode(permitData.getPerCode());
                permitDetailDto.setPerName(permitData.getPerName());
                permitDetailDto.setPerId(permitData.getPerId());
                permitDetailDto.setPerDateStart(permitData.getPerDateStart());
                permitDetailDto.setPerDateEnd(permitData.getPerDateEnd());
                permitDetailDto.setPerStopFlag(permitData.getPerStopFlagValue());

                permitDetailList.add(permitDetailDto);
            }

            //证照编码重复验证
            for (String perCode : perCodeMap.keySet()) {
                List<String> lineList = perCodeMap.get(perCode);
                if (lineList.size() > 1) {
                    errorList.add(MessageFormat.format(ResultEnum.E0023.getMsg(), String.join(",", lineList)));
                }
            }

            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            return ResultUtil.success(permitDetailList);

        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }
    }
}
