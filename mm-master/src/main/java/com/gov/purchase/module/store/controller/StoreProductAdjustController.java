package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaPriceAdjustKey;
import com.gov.purchase.module.store.dto.price.AdjustPriceRequestDto;
import com.gov.purchase.module.store.service.StoreProductAdjustService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.10.22
 */
@Api(tags = "门店商品管理")
@RestController
@RequestMapping("goodsAdjust")
public class StoreProductAdjustController {

    @Autowired
    private StoreProductAdjustService storeProductAdjustService;


    @Log("商品调价列表")
    @ApiOperation("商品调价列表")
    @PostMapping("queryList")
    public Result queryList(
            @RequestBody AdjustPriceRequestDto adjustPriceRequestDto) {

        return ResultUtil.success(storeProductAdjustService.queryList(adjustPriceRequestDto));
    }

    @Log("商品调价列表导出")
    @ApiOperation("商品调价列表导出")
    @PostMapping("queryExportList")
    public Result queryExportList(
            @RequestBody AdjustPriceRequestDto adjustPriceRequestDto) {

        return storeProductAdjustService.queryExportList(adjustPriceRequestDto);
    }

    @Log("作废调价单")
    @ApiOperation("作废调价单")
    @PostMapping("batchAdjustList")
    public Result batchAdjust(@RequestBody List<AdjustPriceRequestDto> list) {

        return storeProductAdjustService.batchAdjust(list);
    }

    @Log("调价明细")
    @ApiOperation("调价明细列表")
    @PostMapping("queryPriceAdjustList")
    public Result queryPriceAdjustList(
            @RequestBody GaiaPriceAdjustKey gaiaPriceAdjustKey) {

        return ResultUtil.success(storeProductAdjustService.queryPriceAdjustList(gaiaPriceAdjustKey));
    }


    @Log("商品最新价格列表")
    @ApiOperation("商品最新价格列表")
    @PostMapping("queryGoodsList")
    public Result queryGoodsList(
            @RequestBody AdjustPriceRequestDto adjustPriceRequestDto) {

        return ResultUtil.success(storeProductAdjustService.queryGoodsList(adjustPriceRequestDto));
    }


    @Log("商品最新价格列表导出")
    @ApiOperation("商品最新价格列表导出")
    @PostMapping("queryGoodsListExport")
    public Result queryGoodsListExport(
            @RequestBody AdjustPriceRequestDto adjustPriceRequestDto) {

        return storeProductAdjustService.queryGoodsListExport(adjustPriceRequestDto);
    }


}
