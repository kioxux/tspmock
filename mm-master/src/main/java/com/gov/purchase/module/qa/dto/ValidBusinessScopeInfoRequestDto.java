package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "经营范围保存传入参数")
public class ValidBusinessScopeInfoRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client", required = true)
    @NotBlank(message = "所属加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "实体类型", name = "bscEntityClass", required = true)
    @NotBlank(message = "实体类型不能为空")
    private String bscEntityClass;

    @ApiModelProperty(value = "实体编码", name = "bscEntityId", required = true)
    @NotBlank(message = "实体编码不能为空")
    private String bscEntityId;

    @ApiModelProperty(value = "经营范围编码(表GAIA_BUSINESS_SCOPE_CLASS)", name = "bscCode", required = true)
    private String bscCode;

    @ApiModelProperty(value = "参数类型（0-删除，1-新增, 2-更新）", name = "type", required = true)
    @NotBlank(message = "提交类型不能为空")
    private String type;

    @ApiModelProperty(value = "经营范围编码(表GAIA_BUSINESS_SCOPE_CLASS)", name = "bscCodeNew", required = true)
    @NotBlank(message = "经营范围编码不能为空")
    private String bscCodeNew;
}
