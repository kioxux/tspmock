package com.gov.purchase.module.priceCompare.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.gov.purchase.entity.GaiaSspPriceComparePro;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class PriceCompareDto {


    /**
     * 单号（唯一）
     */
    private String priceCompareNo;

    /**
     * DC_编码
     */
    @NotBlank(message = "请选择仓库")
    private String dcCode;

    /**
     * 商品范围（多选都好隔开）
     */
    // @NotBlank(message = "请选择比价商品范围")
    private String range;

    /**
     * 报价截至时间
     */
    @NotNull(message = "请选择报价截至时间")
    @JsonFormat
    private LocalDateTime offerEndTime;

    /**
     * 比价模式： 1、全品比价
     */
    @NotNull(message = "请选择比价模式")
    private Integer pattern;

    /**
     * 显示订单数量 (0: false 不显示) （1：true 显示）
     */
    @NotNull(message = "缺少参数显示订单数量：showOrderAmount")
    private Integer showOrderAmount;

    /**
     * 失效日期
     */
    @NotNull(message = "请选择失效日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expiringDate;


    /**
     * 加盟商
     */
    private String client;

    /**
     * 比价商品List
     */
    @Valid
    @NotEmpty(message = "缺少比价商品")
    @Size(min = 1, message = "最少一种商品")
    private List<PriceCompareProDto> priceCompareProList;



    private Boolean isExpires;




}
