package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.common.entity.Pageable;
import com.gov.purchase.common.request.RequestJson;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.26
 */
@Data
public class QueryReturnOrder extends Pageable {
    private String client;
    private String soCompanyCode;
    private String soId;
    private String soDateStart;
    private String soDateEnd;
    private String soProCode;
    private String soCustomerId;
    private String createName;

    private List<String> soIds;
    private List<String> soProCodes;
    private List<String> soCustomerIds;

    /**
     * 是否只显示有退货数量数据（0： 否， 1：是）
     */
    private String flag;
}

