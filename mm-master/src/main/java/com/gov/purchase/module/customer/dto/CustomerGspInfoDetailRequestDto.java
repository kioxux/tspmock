package com.gov.purchase.module.customer.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户首营详情传入参数")
public class CustomerGspInfoDetailRequestDto  {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "客户编号", name = "cusSelfCode")
    String cusCode;

    @ApiModelProperty(value = "客户自编号", name = "cusSelfCode")
    private String cusSelfCode;

    @ApiModelProperty(value = "地点", name = "cusSite")
    private   String cusSite;

    /**
     * 首营流程编号
     */
    private String cusFlowNo;
}
