package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.entity.GaiaDcReplenishConf;
import com.gov.purchase.entity.GaiaDcReplenishRatio;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.10
 */
@Data
public class ReplenishConf extends GaiaDcReplenishConf {

    /**
     * 最小补货量 按品类
     */
    private List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinClassList;
    /**
     * 最小补货量 按商品
     */
    private List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinProList;
    /**
     * 最大补货量 按品类
     */
    private List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxClassList;
    /**
     * 最大补货量 按商品
     */
    private List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxProList;
    /**
     * 批货系数
     */
    private List<GaiaDcReplenishRatio> gaiaDcReplenishRatioList;
}

