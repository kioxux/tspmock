package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class PurchaseItemResponseDto {

    /**
     * 订单行号
     */
    private String poLineNo;

    /**
     * 商品自编码
     */
    private String poProCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 国际条形码
     */
    private String proBarcode;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 单位名称
     */
    private String unitName;

    /**
     * 生产企业
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 订单单价
     */
    private BigDecimal poPrice;

    /**
     * 订单行总价
     */
    private BigDecimal poLineAmt;

    /**
     * 订单数量
     */
    private BigDecimal poQty;


}
