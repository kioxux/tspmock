package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaPoHeader;
import com.gov.purchase.entity.GaiaPoItem;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewDetailVo;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewDto;
import com.gov.purchase.module.purchase.dto.PurchasePlanReviewVo;
import com.gov.purchase.module.purchase.service.PurchasePlanReviewService;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentListResponseDto;
import com.gov.purchase.module.wholesale.dto.GaiaWmsShenPiPeiZhiDTO;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description: 采购计划审批
 * @author: yzf
 * @create: 2021-12-02 13:10
 */
@Service
public class PurchasePlanReviewServiceImpl implements PurchasePlanReviewService {
    @Resource
    private GaiaPoHeaderMapper gaiaPoHeaderMapper;
    @Resource
    private GaiaPoItemMapper gaiaPoItemMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    private GaiaSoHeaderMapper gaiaSoHeaderMapper;
    @Resource
    private GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;
    @Resource
    CosUtils cosUtils;

    @Override
    public Result queryPurchasePlanReviewList(PurchasePlanReviewDto purchasePlanReviewDto) {
        TokenUser user = feignService.getLoginInfo();
        List<PurchasePlanReviewVo> voList = new ArrayList<>();
        // 检查审批配置表中具体的人员或者岗位
        GaiaWmsShenPiPeiZhiDTO gaiaWmsShenPiPeiZhiDTO = gaiaSoHeaderMapper.queryUserRole(user.getClient(), purchasePlanReviewDto.getPoCompanyCode(), 1);
        // 配置表人员存在该人员
        if (gaiaWmsShenPiPeiZhiDTO != null && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())
                && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
            // 判断审批人类型为角色  0-角色  1-人员
            if ("0".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                // 查询权限分配表是否分配该审批人权限
                int count = gaiaAuthconfiDataMapper.selectUserAuthority(user.getClient(), gaiaWmsShenPiPeiZhiDTO.getWmSpr1(), user.getUserId());
                // 无权限则返回空
                if (count == 0) {
                    return ResultUtil.success(new PageInfo<>(voList));
                }
            }
            if ("1".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                // 查询权限分配表是否分配该审批人权限
                // 无权限则返回空
                if (!user.getUserId().equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
                    return ResultUtil.success(new PageInfo<>(voList));
                }
            }
            purchasePlanReviewDto.setClient(user.getClient());
            PageHelper.startPage(purchasePlanReviewDto.getPageNum(), purchasePlanReviewDto.getPageSize());
            voList = gaiaPoHeaderMapper.queryPurchasePlanReview(purchasePlanReviewDto);
            PageInfo<PurchasePlanReviewVo> pageInfo = new PageInfo<>(voList);
            if (CollectionUtils.isNotEmpty(pageInfo.getList())) {
                List<String> poIds = pageInfo.getList().stream().map(PurchasePlanReviewVo::getPoId).collect(Collectors.toList());
                List<GaiaPoItem> itemList = gaiaPoItemMapper.queryPurchasePlanReviewItemSum(user.getClient(), poIds);
                if (CollectionUtils.isNotEmpty(itemList)) {
                    List<PurchasePlanReviewVo> list = pageInfo.getList().stream()
                            .map(purchasePlanReviewVo -> itemList.stream()
                                    .filter(gaiaPoItem -> purchasePlanReviewVo.getPoId().equals(gaiaPoItem.getPoId()))
                                    .findFirst()
                                    .map(gaiaPoItem -> {
                                        purchasePlanReviewVo.setLineNum(gaiaPoItem.getPoLineNo());
                                        purchasePlanReviewVo.setQtySum(gaiaPoItem.getPoQty());
                                        purchasePlanReviewVo.setAmtSum(gaiaPoItem.getPoLineAmt());
                                        return purchasePlanReviewVo;
                                    }).orElse(null))
                            .collect(Collectors.toList());
                    pageInfo.setList(list);
                }
            }
            return ResultUtil.success(pageInfo);
        }
        return ResultUtil.success(new PageInfo<>(voList));
    }

    @Override
    public Result queryPurchasePlanReviewDetailList(PurchasePlanReviewDto purchasePlanReviewDto) {
        TokenUser user = feignService.getLoginInfo();
        purchasePlanReviewDto.setClient(user.getClient());
        List<PurchasePlanReviewVo> list = gaiaPoHeaderMapper.queryPurchasePlanReview(purchasePlanReviewDto);
        if (CollectionUtils.isNotEmpty(list)) {
            List<String> poIds = list.stream().map(PurchasePlanReviewVo::getPoId).collect(Collectors.toList());
            List<GaiaPoItem> itemList = gaiaPoItemMapper.queryPurchasePlanReviewItemSum(user.getClient(), poIds);
            if (CollectionUtils.isNotEmpty(itemList)) {
                list.get(0).setAmtSum(itemList.get(0).getPoLineAmt());
            }
            if (StringUtils.isBlank(list.get(0).getPoApproveBy())) {
                list.get(0).setPoApproveBy(user.getUserId());
                list.get(0).setPoApproveName(user.getLoginName());
                list.get(0).setPoApproveDate(DateUtils.getCurrentDateStrYYMMDD());
            }

            List<PurchasePlanReviewDetailVo> voList = gaiaPoItemMapper.queryPurchasePlanReviewDetailList(purchasePlanReviewDto.getClient(), purchasePlanReviewDto.getPoId());
            if (CollectionUtils.isNotEmpty(voList)) {
                List<String> proSelfCodeList = voList.stream().map(PurchasePlanReviewDetailVo::getPoProCode).collect(Collectors.toList());
                String salesVolumeStartDate = LocalDateTime.now().plusDays(-30).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                String salesVolumeEndDate = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
                List<DcReplenishmentListResponseDto> saleList = gaiaSdSaleDMapper.selectSaleProByDays(user.getClient(), salesVolumeStartDate, salesVolumeEndDate, proSelfCodeList, null, null);
                // 销量
                voList.forEach(s -> {
                    s.setDaysSalesCount(BigDecimal.ZERO);
                    // 30
                    DcReplenishmentListResponseDto thirty = saleList.stream().filter(t ->
                            t.getType().equals("30") && s.getPoProCode().equals(t.getGssdProId()) && StringUtils.isNotBlank(s.getDcChainHead())
                                    && StringUtils.isNotBlank(t.getStoChainHead()) && s.getDcChainHead().equals(t.getStoChainHead())
                    ).findFirst().orElse(null);
                    if (thirty != null) {
                        s.setDaysSalesCount(thirty.getStoreSaleQty());
                    }
                });
            }
            list.get(0).setDetailList(voList);
        }
        return ResultUtil.success(list);
    }

    @Override
    public int updatePurchasePlan(PurchasePlanReviewDto dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        dto.setPoApproveBy(user.getUserId());
        dto.setPoApproveDate(DateUtils.getCurrentDateStrYYMMDD());
        return gaiaPoHeaderMapper.updatePurchasePlan(dto);
    }

    @Override
    public int refusePurchasePlan(PurchasePlanReviewDto dto) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaPoHeaderMapper.refusePurchasePlan(user.getClient(), dto.getPoId());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updatePurchasePlanDetail(PurchasePlanReviewVo list) {
        TokenUser user = feignService.getLoginInfo();
        if (list != null && CollectionUtils.isNotEmpty(list.getDetailList())) {
            // 删除现有的明细
            gaiaPoItemMapper.deleteByClientAndPoId(user.getClient(), list.getPoId());
            List<GaiaPoItem> poItems = new ArrayList<>();
            List<PurchasePlanReviewDetailVo> details = list.getDetailList();
            for (int i = 0; i < details.size(); i++) {
                GaiaPoItem poItem = new GaiaPoItem();
                poItem.setClient(user.getClient());
                poItem.setPoId(list.getPoId());
                poItem.setPoLineNo(String.valueOf(i + 1));
                poItem.setPoQty(details.get(i).getPoQty());
                poItem.setPoUnit(details.get(i).getProUnit());
                poItem.setPoRate(details.get(i).getProInputTax());
                poItem.setPoProCode(details.get(i).getPoProCode());
                poItem.setPoLocationCode("1000");
                poItem.setPoPrice(details.get(i).getPoPrice());
                poItem.setPoLineAmt(details.get(i).getPoLineAmt());
                poItem.setPoSiteCode(details.get(i).getProSite());
                poItem.setPoCompleteInvoice("0");
                poItem.setPoCompletePay("0");
                poItem.setPoLineDelete("0");
                poItem.setPoCompleteFlag("0");
                poItems.add(poItem);
            }
            // 重新插入采购计划明细
            gaiaPoItemMapper.batchInsert(poItems);
        }
        return ResultUtil.success();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result updatePurchasePlanAndDetail(PurchasePlanReviewVo reviewVo) {
        TokenUser user = feignService.getLoginInfo();
        if (reviewVo != null && CollectionUtils.isNotEmpty(reviewVo.getDetailList())) {
            List<GaiaPoItem> poItems = new ArrayList<>();
            // 更新采购计划主表
            GaiaPoHeader poHeader = new GaiaPoHeader();
            poHeader.setClient(user.getClient());
            poHeader.setPoId(reviewVo.getPoId());
            poHeader.setPoHeadRemark(reviewVo.getPoHeadRemark());
            poHeader.setPoApproveBy(reviewVo.getPoApproveBy());
            poHeader.setPoApproveDate(reviewVo.getPoApproveDate().replace("-", ""));
            poHeader.setPoApproveStatus("2");
            gaiaPoHeaderMapper.updateByPrimaryKeySelective(poHeader);

            // 删除现有的明细
            gaiaPoItemMapper.deleteByClientAndPoId(user.getClient(), reviewVo.getPoId());
            List<PurchasePlanReviewDetailVo> details = reviewVo.getDetailList();
            for (int i = 0; i < details.size(); i++) {
                GaiaPoItem poItem = new GaiaPoItem();
                poItem.setClient(user.getClient());
                poItem.setPoId(reviewVo.getPoId());
                poItem.setPoLineNo(String.valueOf(i + 1));
                poItem.setPoQty(details.get(i).getPoQty());
                poItem.setPoUnit(details.get(i).getProUnit());
                poItem.setPoRate(details.get(i).getProInputTax());
                poItem.setPoProCode(details.get(i).getPoProCode());
                poItem.setPoPrice(details.get(i).getPoPrice());
                poItem.setPoLocationCode("1000");
                poItem.setPoLineAmt(details.get(i).getPoLineAmt());
                poItem.setPoSiteCode(details.get(i).getProSite());
                poItem.setPoCompleteInvoice("0");
                poItem.setPoCompletePay("0");
                poItem.setPoLineDelete("0");
                poItem.setPoCompleteFlag("0");
                poItems.add(poItem);
            }
            // 重新插入采购计划明细
            gaiaPoItemMapper.batchInsert(poItems);
        }
        return ResultUtil.success();
    }

    @Override
    public Result exportList(PurchasePlanReviewDto dto) {
        try {
            TokenUser user = feignService.getLoginInfo();
            List<PurchasePlanReviewVo> voList = new ArrayList<>();
            List<PurchasePlanReviewVo> res = new ArrayList<>();
            // 检查审批配置表中具体的人员或者岗位
            GaiaWmsShenPiPeiZhiDTO gaiaWmsShenPiPeiZhiDTO = gaiaSoHeaderMapper.queryUserRole(user.getClient(), dto.getPoCompanyCode(), 1);
            // 配置表人员存在该人员
            if (gaiaWmsShenPiPeiZhiDTO != null && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
                // 判断审批人类型为角色  0-角色  1-人员
                if ("0".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                    // 查询权限分配表是否分配该审批人权限
                    int count = gaiaAuthconfiDataMapper.selectUserAuthority(user.getClient(), gaiaWmsShenPiPeiZhiDTO.getWmSpr1(), user.getUserId());
                    // 无权限则返回空
                    if (count == 0) {
                        return ResultUtil.success(new PageInfo<>(voList));
                    }
                }
                if ("1".equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSprlx1())) {
                    // 查询权限分配表是否分配该审批人权限
                    // 无权限则返回空
                    if (!user.getUserId().equalsIgnoreCase(gaiaWmsShenPiPeiZhiDTO.getWmSpr1())) {
                        return ResultUtil.success(new PageInfo<>(voList));
                    }
                }
                dto.setClient(user.getClient());
                voList = gaiaPoHeaderMapper.queryPurchasePlanReview(dto);
                if (CollectionUtils.isNotEmpty(voList)) {
                    List<String> poIds = voList.stream().map(PurchasePlanReviewVo::getPoId).collect(Collectors.toList());
                    List<GaiaPoItem> itemList = gaiaPoItemMapper.queryPurchasePlanReviewItemSum(user.getClient(), poIds);
                    if (CollectionUtils.isNotEmpty(itemList)) {
                        res = voList.stream()
                                .map(purchasePlanReviewVo -> itemList.stream()
                                        .filter(gaiaPoItem -> purchasePlanReviewVo.getPoId().equals(gaiaPoItem.getPoId()))
                                        .findFirst()
                                        .map(gaiaPoItem -> {
                                            purchasePlanReviewVo.setLineNum(gaiaPoItem.getPoLineNo());
                                            purchasePlanReviewVo.setQtySum(gaiaPoItem.getPoQty());
                                            purchasePlanReviewVo.setAmtSum(gaiaPoItem.getPoLineAmt());
                                            return purchasePlanReviewVo;
                                        }).orElse(null))
                                .collect(Collectors.toList());
                    }
                }
            }
            // 导出数据
            List<List<String>> sourceList = new ArrayList<>();
            initData(res, sourceList);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(sourceList);
                    }},
                    new ArrayList<String>() {{
                        add("采购计划列表");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param sourceList
     */
    private void initData(List<PurchasePlanReviewVo> batchList, List<List<String>> sourceList) {
        if (CollectionUtils.isNotEmpty(batchList)) {
            for (PurchasePlanReviewVo vo : batchList) {
                if (sourceList != null) {
                    List<String> basicData = new ArrayList<>();
                    // 供应商
                    basicData.add(vo.getPoSupplierId() + "-" + vo.getSupName());
                    // 采购单号
                    basicData.add(vo.getPoId());
                    // 单据日期
                    basicData.add(vo.getPoDate());
                    // 单据行数
                    basicData.add(vo.getLineNum());
                    // 单据总数量
                    basicData.add(String.valueOf(vo.getQtySum()));
                    // 单据总金额
                    basicData.add(String.valueOf(vo.getAmtSum()));
                    // 创建人
                    basicData.add(vo.getPoCreateBy() + "-" + vo.getCreateByName());
                    // 备注
                    basicData.add(vo.getPoHeadRemark());
                    // 审核状态
                    basicData.add(StringUtils.isNotBlank(vo.getPoApproveStatus()) && !"1".equalsIgnoreCase(vo.getPoApproveStatus()) ? "未审核" : "已审核");
                    sourceList.add(basicData);
                }
            }
        }
    }

    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "供应商",
            "采购单号",
            "单据日期",
            "单据行数",
            "单据总数量",
            "单据总金额",
            "创建人",
            "单据备注",
            "审批状态",
    };
}
