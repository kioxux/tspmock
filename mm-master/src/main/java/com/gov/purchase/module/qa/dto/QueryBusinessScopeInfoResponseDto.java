package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value = "经营范围明细输出参数")
public class QueryBusinessScopeInfoResponseDto {

    private String client;

    private String francName;

    private String bscEntityClass;

    private String bscEntityId;

    private String bscEntityName;

    private List<String> scopeList;


}
