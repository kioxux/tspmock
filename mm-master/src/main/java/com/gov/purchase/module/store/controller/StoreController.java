package com.gov.purchase.module.store.controller;


import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.store.dto.store.StoreInfoDetailRequestDto;
import com.gov.purchase.module.store.dto.store.StoreInfoInsertOrUpdateRequestDto;
import com.gov.purchase.module.store.dto.store.StoreInfoInsertOrUpdateValidDto;
import com.gov.purchase.module.store.dto.store.StoreListRequestDto;
import com.gov.purchase.module.store.service.StoreService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "门店")
@RestController
@RequestMapping("store")
public class StoreController {


    @Autowired
    StoreService storeService;

    /**
     * 门店列表页面
     *
     * @param dto 查询条件
     * @return
     */
    @Log("门店列表页面")
    @ApiOperation("门店列表页面")
    @PostMapping("queryStoreList")
    public Result queryStoreList(@Valid @RequestBody StoreListRequestDto dto) {
        return ResultUtil.success(storeService.queryStoreList(dto));
    }

    /**
     * 门店列表页面
     *
     * @param dto 查询条件
     * @return
     */
    @Log("门店维护详情初始化")
    @ApiOperation("门店维护详情初始化")
    @PostMapping(   "getStoreInfo")
    public Result getStoreInfo(@Valid @RequestBody StoreInfoDetailRequestDto dto) {
        return ResultUtil.success(storeService.getStoreInfo(dto));
    }

    /**
     * 门店列表页面
     *
     * @param dto 查询条件
     * @return
     */
    @Log("门店维护验证")
    @ApiOperation("门店维护验证")
    @PostMapping("validStoreInfo")
    public Result validStoreInfo(@Valid @RequestBody StoreInfoInsertOrUpdateValidDto dto) {
        return ResultUtil.success(storeService.validStoreInfo(dto));
    }

    /**
     * 门店列表页面
     *
     * @param dto 查询条件
     * @return
     */
    @Log("门店维护插入")
    @ApiOperation("门店维护插入")
    @PostMapping("insertStoreInfo")
    public Result insertStoreInfo(@Valid @RequestBody StoreInfoInsertOrUpdateRequestDto dto) {
        return ResultUtil.success(storeService.insertStoreInfo(dto));
    }

    @Log("门店维护更新")
    @ApiOperation("门店维护更新")
    @PostMapping("updateStoreInfo")
    public Result updateStoreInfo(@Valid @RequestBody StoreInfoInsertOrUpdateRequestDto dto) {
        return ResultUtil.success(storeService.updateStoreInfo(dto));
    }

    @Log("生成微信二维码")
    @ApiOperation("生成微信二维码")
    @PostMapping("getQrCode")
    public Result getQrCode(@RequestJson("stoCode") String stoCode) {
        return storeService.getQrCode(stoCode);
    }

}
