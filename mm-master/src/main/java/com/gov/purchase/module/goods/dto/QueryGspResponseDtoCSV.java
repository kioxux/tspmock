package com.gov.purchase.module.goods.dto;

import com.gov.purchase.utils.csv.annotation.CsvCell;
import com.gov.purchase.utils.csv.annotation.CsvRow;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@CsvRow("首营列表返回参数")
public class QueryGspResponseDtoCSV {

    /**
     * client
     */
//    @CsvCell(title = "client", index = 1, fieldNo = 1)
    private String client;

    /**
     * 商品自编码
     */
    @CsvCell(title = "商品自编码", index = 2, fieldNo = 1)
    private String proSelfCode;

    /**
     * 地点
     */
    @CsvCell(title = "地点", index = 3, fieldNo = 1)
    private String proSite;

    /**
     * pro_code
     */
//    @CsvCell(title = "pro_code", index = 4, fieldNo = 1)
    private String proCode;

    /**
     * 商品名
     */
    @CsvCell(title = "商品名", index = 5, fieldNo = 1)
    private String proName;

    /**
     * 通用名称
     */
    @CsvCell(title = "通用名称", index = 6, fieldNo = 1)
    private String proCommonname;

    /**
     * 剂型
     */
    @CsvCell(title = "剂型", index = 7, fieldNo = 1)
    private String proForm;

    /**
     * 规格
     */
    @CsvCell(title = "规格", index = 8, fieldNo = 1)
    private String proSpecs;

    /**
     * 包装规格
     */
    @CsvCell(title = "包装规格", index = 9, fieldNo = 1)
    private String proPackSpecs;

    /**
     * 批准文号
     */
    @CsvCell(title = "批准文号", index = 10, fieldNo = 1)
    private String proRegisterNo;

    /**
     * 供货单位名称
     */
    @CsvCell(title = "供货单位名称", index = 11, fieldNo = 1)
    private String proFactoryName;

    /**
     * 是否通过gmp认证
     */
    @CsvCell(title = "是否通过gmp认证", index = 12, fieldNo = 1)
    private String proIfGmp;

    /**
     * 质量标准
     */
    @CsvCell(title = "质量标准", index = 13, fieldNo = 1)
    private String proQs;

    /**
     * 功能主治
     */
    @CsvCell(title = "功能主治", index = 14, fieldNo = 1)
    private String proFunction;

    /**
     * 有效期
     */
    @CsvCell(title = "有效期", index = 15, fieldNo = 1)
    private String proLifeDate;

    /**
     * 贮存条件
     */
    @CsvCell(title = "贮存条件", index = 16, fieldNo = 1)
    private String proStorageCondition;

    /**
     * 供货单位名称
     */
    @CsvCell(title = "供货单位名称", index = 17, fieldNo = 1)
    private String proSupplyName;

    /**
     * 供货单位地址
     */
    @CsvCell(title = "供货单位地址", index = 18, fieldNo = 1)
    private String proSupplyAdd;

    /**
     * 供货单位联系人
     */
    @CsvCell(title = "供货单位联系人", index = 19, fieldNo = 1)
    private String proSupplyContact;

    /**
     * 供货单位联系电话
     */
    @CsvCell(title = "供货单位联系电话", index = 20, fieldNo = 1)
    private String proSupplyTel;

    /**
     * 首营日期
     */
    @CsvCell(title = "首营日期", index = 21, fieldNo = 1)
    private String proGspDate;

    /**
     * 首营流程编号
     */
    @CsvCell(title = "首营流程编号", index = 22, fieldNo = 1)
    private String proFlowNo;

    /**
     * pro_gsp_status
     */
    @CsvCell(title = "状态", index = 23, fieldNo = 1)
    private String proGspStatus;

    /**
     * 中药规格
     */
    @CsvCell(title = "中药规格", index = 24, fieldNo = 1)
    private String proTcmSpecs;

    /**
     * 中药批准文号
     */
    @CsvCell(title = "中药批准文号", index = 25, fieldNo = 1)
    private String proTcmRegisterNo;

    /**
     * 中药生产企业代码
     */
    @CsvCell(title = "中药生产企业代码", index = 26, fieldNo = 1)
    private String proTcmFactoryCode;

    /**
     * 中药产地
     */
    @CsvCell(title = "中药产地", index = 27, fieldNo = 1)
    private String proTcmPlace;

    /**
     * last_update_time
     */
//    @CsvCell(title = "last_update_time", index = 28, fieldNo = 1)
    private Date lastUpdateTime;

    /**
     * 计量单位
     */
    @CsvCell(title = "计量单位", index = 29, fieldNo = 1)
    private String proUnit;

    /**
     * 进项税率
     */
    @CsvCell(title = "进项税率", index = 30, fieldNo = 1)
    private String proInputTax;

    /**
     * 保质期
     */
    @CsvCell(title = "保质期", index = 31, fieldNo = 1)
    private String proLife;

    /**
     * 保质期单位
     */
    @CsvCell(title = "保质期单位", index = 32, fieldNo = 1)
    private String proLifeUnit;

    /**
     * 商品分类
     */
//    @CsvCell(title = "商品分类", index = 33, fieldNo = 1)
    private String proClass;

    /**
     * 批准文号分类
     */
    @CsvCell(title = "批准文号分类", index = 34, fieldNo = 1)
    private String proRegisterClass;

    /**
     * 商品仓储分区
     */
    @CsvCell(title = "商品仓储分区", index = 35, fieldNo = 1)
    private String proStorageArea;

    /**
     * 中包装量
     */
    @CsvCell(title = "中包装量", index = 36, fieldNo = 1)
    private String proMidPackage;

    /**
     * 生产企业代码
     */
    @CsvCell(title = "生产企业代码", index = 37, fieldNo = 1)
    private String proFactoryCode;

    /**
     * 参考进货价
     */
    @CsvCell(title = "参考进货价", index = 38, fieldNo = 1)
    private BigDecimal proCgj;

    /**
     * 参考零售价
     */
    @CsvCell(title = "参考零售价", index = 39, fieldNo = 1)
    private BigDecimal proLsj;

    /**
     * 参考毛利率
     */
    @CsvCell(title = "参考毛利率", index = 40, fieldNo = 1)
    private BigDecimal proMll;

    /**
     * 国际条形码1
     */
    @CsvCell(title = "国际条形码1", index = 41, fieldNo = 1)
    private String proBarcode;

    /**
     * 是否医保
     */
    @CsvCell(title = "是否医保", index = 42, fieldNo = 1)
    private String proIfMed;

    /**
     * pro_med_prodct
     */
    @CsvCell(title = "国家医保品种", index = 43, fieldNo = 1)
    private String proMedProdct;

    /**
     * pro_med_prodctcode
     */
    @CsvCell(title = "国家医保品种编码", index = 44, fieldNo = 1)
    private String proMedProdctcode;

    /**
     * pro_order_flag
     */
//    @CsvCell(title = "pro_order_flag", index = 45, fieldNo = 1)
    private String proOrderFlag;

    /**
     * 匹配状态 0-未匹配，1-部分匹配，2-完全匹配
     */
//    @CsvCell(title = "匹配状态 0-未匹配，1-部分匹配，2-完全匹配", index = 46, fieldNo = 1)
    private String proMatchStatus;

    /**
     * 商品描述
     */
    @CsvCell(title = "商品描述", index = 47, fieldNo = 1)
    private String proDepict;

    /**
     * 助记码
     */
    @CsvCell(title = "助记码", index = 48, fieldNo = 1)
    private String proPym;

    /**
     * 细分剂型
     */
    @CsvCell(title = "细分剂型", index = 49, fieldNo = 1)
    private String proPartform;

    /**
     * 最小剂量（以mg/ml计算）
     */
    @CsvCell(title = "最小剂量（以mg/ml计算）", index = 50, fieldNo = 1)
    private String proMindose;

    /**
     * 总剂量（以mg/ml计算）
     */
    @CsvCell(title = "总剂量（以mg/ml计算）", index = 51, fieldNo = 1)
    private String proTotaldose;

    /**
     * 国际条形码2
     */
    @CsvCell(title = "国际条形码2", index = 52, fieldNo = 1)
    private String proBarcode2;

    /**
     * 批准文号批准日期
     */
    @CsvCell(title = "批准文号批准日期", index = 53, fieldNo = 1)
    private String proRegisterDate;

    /**
     * 批准文号失效日期
     */
    @CsvCell(title = "批准文号失效日期", index = 54, fieldNo = 1)
    private String proRegisterExdate;

    /**
     * 商品分类描述
     */
    @CsvCell(title = "商品分类描述", index = 55, fieldNo = 1)
    private String proClassName;

    /**
     * 成分分类
     */
//    @CsvCell(title = "成分分类", index = 56, fieldNo = 1)
    private String proCompclass;

    /**
     * 成分分类描述
     */
    @CsvCell(title = "成分分类描述", index = 57, fieldNo = 1)
    private String proCompclassName;

    /**
     * 处方类别 1-处方药，2-甲类otc，3-乙类otc，4-双跨处方
     */
    @CsvCell(title = "处方类别", index = 58, fieldNo = 1)
    private String proPresclass;

    /**
     * 商标
     */
    @CsvCell(title = "商标", index = 59, fieldNo = 1)
    private String proMark;

    /**
     * 品牌标识名
     */
    @CsvCell(title = "品牌标识名", index = 60, fieldNo = 1)
    private String proBrand;

    /**
     * 品牌区分 1-全国品牌，2-省份品牌，3-地区品牌，4-本地品牌，5-其它
     */
    @CsvCell(title = "品牌区分", index = 61, fieldNo = 1)
    private String proBrandClass;

    /**
     * 上市许可持有人
     */
    @CsvCell(title = "上市许可持有人", index = 62, fieldNo = 1)
    private String proHolder;

    /**
     * 销项税率
     */
    @CsvCell(title = "销项税率", index = 63, fieldNo = 1)
    private String proOutputTax;

    /**
     * 药品本位码
     */
    @CsvCell(title = "药品本位码", index = 64, fieldNo = 1)
    private String proBasicCode;

    /**
     * 税务分类编码
     */
    @CsvCell(title = "税务分类编码", index = 65, fieldNo = 1)
    private String proTaxClass;

    /**
     * 管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制
     */
    @CsvCell(title = "管制特殊分类 1-毒性药品，2-麻醉药品，3-一类精神药品，4-二类精神药品，5-易制毒药品（麻黄碱），6-放射性药品，7-生物制品（含胰岛素），8-兴奋剂（除胰岛素），9-第一类器械，10-第二类器械，11-第三类器械，12-其它管制", index = 66, fieldNo = 1)
    private String proControlClass;

    /**
     * 生产类别 1-辅料，2-化学药品，3-生物制品，4-中药，5-器械
     */
    @CsvCell(title = "生产类别 1-辅料，2-化学药品，3-生物制品，4-中药，5-器械", index = 67, fieldNo = 1)
    private String proProduceClass;

    /**
     * 长（以mm计算）
     */
    @CsvCell(title = "长（以mm计算）", index = 68, fieldNo = 1)
    private String proLong;

    /**
     * 宽（以mm计算）
     */
    @CsvCell(title = "宽（以mm计算）", index = 69, fieldNo = 1)
    private String proWide;

    /**
     * 高（以mm计算）
     */
    @CsvCell(title = "高（以mm计算）", index = 70, fieldNo = 1)
    private String proHigh;

    /**
     * 大包装量
     */
    @CsvCell(title = "大包装量", index = 71, fieldNo = 1)
    private String proBigPackage;

    /**
     * 启用电子监管码 0-否，1-是
     */
    @CsvCell(title = "启用电子监管码 0-否，1-是", index = 72, fieldNo = 1)
    private String proElectronicCode;

    /**
     * 生产经营许可证号
     */
    @CsvCell(title = "生产经营许可证号", index = 73, fieldNo = 1)
    private String proQsCode;

    /**
     * 最大销售量
     */
    @CsvCell(title = "最大销售量", index = 74, fieldNo = 1)
    private String proMaxSales;

    /**
     * 说明书代码
     */
    @CsvCell(title = "说明书代码", index = 75, fieldNo = 1)
    private String proInstructionCode;

    /**
     * 说明书内容
     */
    @CsvCell(title = "说明书内容", index = 76, fieldNo = 1)
    private String proInstruction;

    /**
     * 商品状态 0 可用 1不可用
     */
    @CsvCell(title = "商品状态", index = 77, fieldNo = 1)
    private String proStatus;

    /**
     * 生产国家
     */
    @CsvCell(title = "生产国家", index = 78, fieldNo = 1)
    private String proCountry;

    /**
     * 产地
     */
    @CsvCell(title = "产地", index = 79, fieldNo = 1)
    private String proPlace;

    /**
     * 可服用天数
     */
    @CsvCell(title = "可服用天数", index = 80, fieldNo = 1)
    private String proTakeDays;

    /**
     * 用法用量
     */
    @CsvCell(title = "用法用量", index = 81, fieldNo = 1)
    private String proUsage;

    /**
     * 禁忌说明
     */
    @CsvCell(title = "禁忌说明", index = 82, fieldNo = 1)
    private String proContraindication;

    /**
     * 商品定位（t-淘汰；q-清场；x-新品）
     */
    @CsvCell(title = "商品定位（t-淘汰；q-清场；x-新品）", index = 83, fieldNo = 1)
    private String proPosition;

    /**
     * 禁止销售
     */
    @CsvCell(title = "禁止销售", index = 84, fieldNo = 1)
    private String proNoRetail;

    /**
     * 禁止采购
     */
    @CsvCell(title = "禁止采购", index = 85, fieldNo = 1)
    private String proNoPurchase;

    /**
     * 禁止配送
     */
    @CsvCell(title = "禁止配送", index = 86, fieldNo = 1)
    private String proNoDistributed;

    /**
     * 禁止退厂
     */
    @CsvCell(title = "禁止退厂", index = 87, fieldNo = 1)
    private String proNoSupplier;

    /**
     * 禁止退仓
     */
    @CsvCell(title = "禁止退仓", index = 88, fieldNo = 1)
    private String proNoDc;

    /**
     * 禁止调剂
     */
    @CsvCell(title = "禁止调剂", index = 89, fieldNo = 1)
    private String proNoAdjust;

    /**
     * 禁止批发
     */
    @CsvCell(title = "禁止批发", index = 90, fieldNo = 1)
    private String proNoSale;

    /**
     * 禁止请货
     */
    @CsvCell(title = "禁止请货", index = 91, fieldNo = 1)
    private String proNoApply;

    /**
     * 是否拆零 1是0否
     */
    @CsvCell(title = "是否拆零", index = 92, fieldNo = 1)
    private String proIfpart;

    /**
     * 拆零单位
     */
    @CsvCell(title = "拆零单位", index = 93, fieldNo = 1)
    private String proPartUint;

    /**
     * 拆零比例
     */
    @CsvCell(title = "拆零比例", index = 94, fieldNo = 1)
    private String proPartRate;

    /**
     * 采购单位
     */
    @CsvCell(title = "采购单位", index = 95, fieldNo = 1)
    private String proPurchaseUnit;

    /**
     * 采购员
     */
    @CsvCell(title = "采购员", index = 96, fieldNo = 1)
    private String proPurchaseRate;

    /**
     * 采购单位
     */
    @CsvCell(title = "采购单位", index = 97, fieldNo = 1)
    private String proSaleUnit;

    /**
     * 采购比例
     */
    @CsvCell(title = "采购比例", index = 98, fieldNo = 1)
    private String proSaleRate;

    /**
     * 最小订货量
     */
    @CsvCell(title = "最小订货量", index = 99, fieldNo = 1)
    private BigDecimal proMinQty;

    /**
     * 销售级别
     */
    @CsvCell(title = "销售级别", index = 100, fieldNo = 1)
    private String proSlaeClass;

    /**
     * 限购数量
     */
    @CsvCell(title = "限购数量", index = 101, fieldNo = 1)
    private BigDecimal proLimitQty;

    /**
     * 特殊药品每单最大配货量
     */
    @CsvCell(title = "特殊药品每单最大配货量", index = 102, fieldNo = 1)
    private BigDecimal proMaxQty;

    /**
     * 按中包装量倍数配货
     */
    @CsvCell(title = "按中包装量倍数配货", index = 103, fieldNo = 1)
    private String proPackageFlag;

    /**
     * 是否重点养护
     */
    @CsvCell(title = "是否重点养护", index = 104, fieldNo = 1)
    private String proKeyCare;

    /**
     * 固定货位
     */
    @CsvCell(title = "固定货位", index = 105, fieldNo = 1)
    private String proFixBin;

    /**
     * 创建日期
     */
    @CsvCell(title = "创建日期", index = 106, fieldNo = 1)
    private String proCreateDate;

    /**
     * 国家医保目录编号
     */
    @CsvCell(title = "国家医保目录编号", index = 107, fieldNo = 1)
    private String proMedListnum;

    /**
     * 国家医保目录名称
     */
    @CsvCell(title = "国家医保目录名称", index = 108, fieldNo = 1)
    private String proMedListname;

    /**
     * 国家医保医保目录剂型
     */
    @CsvCell(title = "国家医保医保目录剂型", index = 109, fieldNo = 1)
    private String proMedListform;

    /**
     * 外包装图片
     */
//    @CsvCell(title = "外包装图片", index = 110, fieldNo = 1)
    private String proPictureWbz;

    /**
     * 说明书图片
     */
//    @CsvCell(title = "说明书图片", index = 111, fieldNo = 1)
    private String proPictureSms;

    /**
     * 批件图片
     */
//    @CsvCell(title = "批件图片", index = 112, fieldNo = 1)
    private String proPicturePj;

    /**
     * 内包装图片
     */
//    @CsvCell(title = "内包装图片", index = 113, fieldNo = 1)
    private String proPictureNbz;

    /**
     * 生产企业证照图片
     */
//    @CsvCell(title = "生产企业证照图片", index = 114, fieldNo = 1)
    private String proPictureZz;

    /**
     * 其他图片
     */
//    @CsvCell(title = "其他图片", index = 115, fieldNo = 1)
    private String proPictureQt;

    /**
     * 商品自分类
     */
    @CsvCell(title = "商品自分类", index = 116, fieldNo = 1)
    private String proSclass;

    /**
     * 自定义字段1
     */
    @CsvCell(title = "自定义字段1", index = 117, fieldNo = 1)
    private String proZdy1;

    /**
     * 自定义字段2
     */
    @CsvCell(title = "自定义字段2", index = 118, fieldNo = 1)
    private String proZdy2;

    /**
     * 自定义字段3
     */
    @CsvCell(title = "自定义字段3", index = 119, fieldNo = 1)
    private String proZdy3;

    /**
     * 自定义字段4
     */
    @CsvCell(title = "自定义字段4", index = 120, fieldNo = 1)
    private String proZdy4;

    /**
     * 自定义字段5
     */
    @CsvCell(title = "自定义字段5", index = 121, fieldNo = 1)
    private String proZdy5;

    /**
     * 医保刷卡数量
     */
    @CsvCell(title = "医保刷卡数量", index = 122, fieldNo = 1)
    private String proMedQty;

    /**
     * 医保编码
     */
    @CsvCell(title = "医保编码", index = 123, fieldNo = 1)
    private String proMedId;

    /**
     * 不打折商品
     */
    @CsvCell(title = "不打折商品", index = 124, fieldNo = 1)
    private String proBdz;

    /**
     * 特殊属性：1-防疫
     */
    @CsvCell(title = "特殊属性：1-防疫", index = 125, fieldNo = 1)
    private String proTssx;

    /**
     * 医保类型：1-甲类 2-乙类
     */
    @CsvCell(title = "医保类型：1-甲类 2-乙类", index = 126, fieldNo = 1)
    private String proYblx;

    /**
     * 批文注册证号
     */
    @CsvCell(title = "批文注册证号", index = 127, fieldNo = 1)
    private String proPwzcz;

    /**
     * 质量标准
     */
    @CsvCell(title = "质量标准", index = 128, fieldNo = 1)
    private String proZlbz;

    /**
     * 排除：1-是
     */
    @CsvCell(title = "排除：1-是", index = 129, fieldNo = 1)
    private String proOut;

    /**
     * 是否批发：0-否 1-是
     */
    @CsvCell(title = "是否批发：0-否 1-是", index = 130, fieldNo = 1)
    private String proIfWholesale;

    /**
     * 经营类别
     */
    @CsvCell(title = "经营类别", index = 131, fieldNo = 1)
    private String proJylb;

    /**
     * 特殊药品每月最大配货量
     */
    @CsvCell(title = "特殊药品每月最大配货量", index = 132, fieldNo = 1)
    private BigDecimal proMaxQtyMouth;

    /**
     * 医疗器械注册人
     */
    @CsvCell(title = "医疗器械注册人", index = 133, fieldNo = 1)
    private String proYlqxZcr;

    /**
     * 医疗器械备案人
     */
    @CsvCell(title = "医疗器械备案人", index = 134, fieldNo = 1)
    private String proYlqxBar;

    /**
     * 医疗器械受托生产企业
     */
    @CsvCell(title = "医疗器械受托生产企业", index = 135, fieldNo = 1)
    private String proYlqxScqy;

    /**
     * 医疗器械受托生产企业许可证号
     */
    @CsvCell(title = "医疗器械受托生产企业许可证号", index = 136, fieldNo = 1)
    private String proYlqxScqyxkz;

    /**
     * 医疗器械备案凭证号
     */
    @CsvCell(title = "医疗器械备案凭证号", index = 137, fieldNo = 1)
    private String proYlqxBapzh;

    /**
     * 服药剂量单位
     */
    @CsvCell(title = "服药剂量单位", index = 138, fieldNo = 1)
    private String proDoseUnit;

    /**
     * 经营管理类别
     */
    @CsvCell(title = "经营管理类别", index = 139, fieldNo = 1)
    private String proJygllb;

    /**
     * 档案号
     */
    @CsvCell(title = "档案号", index = 140, fieldNo = 1)
    private String proDah;

    /**
     * 是否发送购药提醒
     */
    @CsvCell(title = "是否发送购药提醒", index = 141, fieldNo = 1)
    private String proIfSms;

    /**
     * 备注
     */
    @CsvCell(title = "备注", index = 142, fieldNo = 1)
    private String proBeizhu;

    /**
     * 会员价
     */
    @CsvCell(title = "会员价", index = 143, fieldNo = 1)
    private BigDecimal proHyj;

    /**
     * 国批价
     */
    @CsvCell(title = "国批价", index = 144, fieldNo = 1)
    private BigDecimal proGpj;

    /**
     * 国零价
     */
    @CsvCell(title = "国零价", index = 145, fieldNo = 1)
    private BigDecimal proGlj;

    /**
     * 预设售价1
     */
    @CsvCell(title = "预设售价1", index = 146, fieldNo = 1)
    private BigDecimal proYsj1;

    /**
     * 预设售价2
     */
    @CsvCell(title = "预设售价2", index = 147, fieldNo = 1)
    private BigDecimal proYsj2;

    /**
     * 预设售价3
     */
    @CsvCell(title = "预设售价3", index = 148, fieldNo = 1)
    private BigDecimal proYsj3;

}
