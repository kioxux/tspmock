package com.gov.purchase.module.replenishment.constant;

/**
 * @desc:
 * @author: Ryan
 * @createTime: 2021/8/18 14:30
 */
public enum PlanCallTypeEnum {

    NOT_CALL(0, "未调用"),
    AUTO_CALL(1, "自动调用"),
    MANUAL_CALL(2, "手动调用"),;

    public final Integer type;
    public final String message;

    PlanCallTypeEnum(Integer type, String message) {
        this.type = type;
        this.message = message;
    }
}
