package com.gov.purchase.module.store.dto.price;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel("价格范围选择返回参数")
public class PriceStoreListReponseDto {


    @ApiModelProperty(value = "门店编号", name = "stoCode")
    private String stoCode;

    @ApiModelProperty(value = "门店名称", name = "stoName")
    private String stoName;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "是否医保店", name = "stoIfMedicalcare")
    private String stoIfMedicalcare;

    /**
     * 门店简称
     */
    private String stoShortName;

}
