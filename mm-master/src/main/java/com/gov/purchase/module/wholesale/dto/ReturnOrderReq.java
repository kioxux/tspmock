package com.gov.purchase.module.wholesale.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.13
 */
@Data
public class ReturnOrderReq {
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 销售订单号
     */
    @ApiModelProperty(value = "销售订单号", name = "soId")
    @NotBlank(message = "销售订单号不能为空")
    private String soId;
    /**
     * 销售订单行号
     */
    @ApiModelProperty(value = "销售订单行号", name = "soLineNo")
    @NotBlank(message = "销售订单行号不能为空")
    private String soLineNo;
    /**
     * 本次退货数量
     */
    @ApiModelProperty(value = "本次退货数量", name = "returnQty")
    @NotNull(message = "本次退货数量不能为空")
    private BigDecimal returnQty;

    /**
     * 退库原因
     */
    @ApiModelProperty(value = "退库原因", name = "soReReason")
    @NotBlank(message = "退库原因不能为空")
    private String soReReason;

    private String batchNo;
}

