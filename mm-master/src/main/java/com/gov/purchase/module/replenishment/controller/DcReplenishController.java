package com.gov.purchase.module.replenishment.controller;

import com.alibaba.fastjson.JSON;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaDcReplenishConfKey;
import com.gov.purchase.entity.GaiaExportTask;
import com.gov.purchase.module.replenishment.dto.DcReplenishForm;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentListResponseDto;
import com.gov.purchase.module.replenishment.dto.ReplenishConf;
import com.gov.purchase.module.replenishment.dto.dcReplenish.ProReplenishmentParams;
import com.gov.purchase.module.replenishment.service.DcReplenishService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.10
 */
@Slf4j
@Api(tags = "DC补货新")
@RestController
@RequestMapping("dcReplenish")
public class DcReplenishController {

    @Resource
    private DcReplenishService dcReplenishService;

    @Log("DC紧急补货需求")
    @ApiOperation(value = "仓库紧急补货需求列表", response = DcReplenishmentListResponseDto.class)
    @PostMapping("getUrgentReplenishList")
    public Result getUrgentReplenishList(@RequestBody DcReplenishForm dcReplenishForm) {
        log.info("<公司级缺断><仓库补货新><查询仓库紧急补货需求：{}>", JSON.toJSONString(dcReplenishForm));
        return dcReplenishService.getUrgentReplenishList(dcReplenishForm);
    }

    @Log("DC紧急补货需求")
    @ApiOperation(value = "更新仓库铺货需求状态")
    @PostMapping("updateUrgentReplenish")
    public Result updateUrgentReplenish(@RequestBody DcReplenishForm dcReplenishForm) {
        log.info("<公司级缺断><仓库补货新><更新仓库紧急补货需求：{}>", JSON.toJSONString(dcReplenishForm));
        try {
            dcReplenishService.updateUrgentReplenish(dcReplenishForm);
        } catch (Exception e) {
            log.error("<公司级缺断><仓库补货新><更新紧急补货单><{}>", e.getMessage(), e);
        }
        return ResultUtil.success();
    }

    @Log("DC补货列表新")
    @ApiOperation("DC补货列表新")
    @PostMapping("getReplenishmentList")
    public Result getReplenishmentList(@RequestJson(value = "dcCode", name = "配送中心") String dcCode) throws IllegalAccessException, IOException, InstantiationException {
        return dcReplenishService.getReplenishmentList(dcCode);
    }

    @Log("新增商品")
    @ApiOperation("新增商品")
    @PostMapping("addReplenishmentPro")
    public Result addReplenishmentPro(@RequestJson(value = "proSelfCode", name = "商品编号") String proSelfCode,
                                      @RequestJson(value = "dcCode", name = "配送中心") String dcCode) {
        return dcReplenishService.addReplenishmentPro(proSelfCode, dcCode);
    }

    @Log("补货参数保存")
    @ApiOperation("补货参数保存")
    @PostMapping("saveReplenishConf")
    public Result saveReplenishConf(@RequestBody ReplenishConf replenishConf) {
        dcReplenishService.saveReplenishConf(replenishConf);
        return ResultUtil.success();
    }

    @Log("补货参数获取")
    @ApiOperation("补货参数获取")
    @PostMapping("getReplenishConf")
    public Result getReplenishConf(@RequestJson("gdrcSite") String gdrcSite) {
        return dcReplenishService.getReplenishConf(gdrcSite);
    }

    @Log("获取门店数")
    @ApiOperation("获取门店数")
    @PostMapping("getStoreCont")
    public Result getStoreCont(@RequestJson(value = "dcCode", name = "配送中心") String dcCode) {
        return dcReplenishService.getStoreCont(dcCode);
    }

    @Log("DC补货系数初始化")
    @ApiOperation("DC补货系数初始化")
    @PostMapping("initDcReplenishParams")
    public Result initDcReplenishParams(@RequestBody GaiaDcReplenishConfKey gaiaDcReplenishConfKey) {
        return dcReplenishService.initDcReplenishParams(gaiaDcReplenishConfKey);
    }

    @Log("DC补货端口数据报表")
    @ApiOperation("DC补货端口数据报表")
    @PostMapping("getProReplenishmentList")
    public Result getProReplenishmentList(@RequestJson(value = "pageSize") Integer pageSize,
                                          @RequestJson(value = "pageNum") Integer pageNum,
                                          @RequestJson(value = "dcCode", name = "补货仓库") String dcCode,
                                          @RequestJson(value = "stoCode", name = "零售价参考门店") String stoCode,
                                          @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                          @RequestJson(value = "proClass", required = false) String proClass,
                                          @RequestJson(value = "lastSupp", required = false) String lastSupp,
                                          @RequestJson(value = "recSupp", required = false) String recSupp,
                                          @RequestJson(value = "proZdy1", required = false) String proZdy1,
                                          @RequestJson(value = "proZdy2", required = false) String proZdy2,
                                          @RequestJson(value = "proZdy3", required = false) String proZdy3,
                                          @RequestJson(value = "proZdy4", required = false) String proZdy4,
                                          @RequestJson(value = "proZdy5", required = false) String proZdy5,
                                          @RequestJson(value = "proSlaeClass", required = false) String proSlaeClass,
                                          @RequestJson(value = "proSclass", required = false) String proSclass,
                                          @RequestJson(value = "proNoPurchase", required = false) Integer proNoPurchase,
                                          @RequestJson(value = "proPosition", required = false) Integer proPosition,
                                          @RequestJson(value = "proReplenishmentParamsList", required = false) List<ProReplenishmentParams> proReplenishmentParamsList,
                                          @RequestJson(value = "salesVolumeStartDate", required = false) String salesVolumeStartDate,
                                          @RequestJson(value = "salesVolumeEndDate", required = false) String salesVolumeEndDate,
                                          @RequestJson(value = "proPurchaseRate", required = false) String proPurchaseRate,
                                          @RequestJson(value = "excDea", required = false) Integer excDea,
                                          @RequestJson(value = "storeCodes", name = "排除门店", required = false) List<String> storeCodes
    ) {
        return dcReplenishService.getProReplenishmentList(pageSize, pageNum, dcCode, stoCode, proSelfCode, proClass, lastSupp, recSupp,
                proZdy1, proZdy2, proZdy3, proZdy4, proZdy5, proSlaeClass, proSclass, proNoPurchase, proPosition, proReplenishmentParamsList,
                salesVolumeStartDate, salesVolumeEndDate, proPurchaseRate, excDea, storeCodes);
    }

    @Log("DC补货端口数据报表导出")
    @ApiOperation("DC补货端口数据报表导出")
    @PostMapping("getProReplenishmentListExport")
    public Result getProReplenishmentListExport(@RequestJson(value = "dcCode", name = "补货仓库") String dcCode,
                                                @RequestJson(value = "stoCode", name = "零售价参考门店") String stoCode,
                                                @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                                @RequestJson(value = "proClass", required = false) String proClass,
                                                @RequestJson(value = "lastSupp", required = false) String lastSupp,
                                                @RequestJson(value = "recSupp", required = false) String recSupp,
                                                @RequestJson(value = "proZdy1", required = false) String proZdy1,
                                                @RequestJson(value = "proZdy2", required = false) String proZdy2,
                                                @RequestJson(value = "proZdy3", required = false) String proZdy3,
                                                @RequestJson(value = "proZdy4", required = false) String proZdy4,
                                                @RequestJson(value = "proZdy5", required = false) String proZdy5,
                                                @RequestJson(value = "proSlaeClass", required = false) String proSlaeClass,
                                                @RequestJson(value = "proSclass", required = false) String proSclass,
                                                @RequestJson(value = "proNoPurchase", required = false) Integer proNoPurchase,
                                                @RequestJson(value = "proPosition", required = false) Integer proPosition,
                                                @RequestJson(value = "proReplenishmentParamsList", required = false) List<ProReplenishmentParams> proReplenishmentParamsList,
                                                @RequestJson(value = "salesVolumeStartDate", required = false) String salesVolumeStartDate,
                                                @RequestJson(value = "salesVolumeEndDate", required = false) String salesVolumeEndDate,
                                                @RequestJson(value = "proPurchaseRate", required = false) String proPurchaseRate,
                                                @RequestJson(value = "excDea", required = false) Integer excDea,
                                                @RequestJson(value = "storeCodes", name = "排除门店", required = false) List<String> storeCodes
    ) {
        // 生成导出任务
        GaiaExportTask gaiaExportTask = dcReplenishService.initExportTask();
        // 导出
        dcReplenishService.getProReplenishmentListExport(dcCode, stoCode, proSelfCode, proClass, lastSupp, recSupp,
                proZdy1, proZdy2, proZdy3, proZdy4, proZdy5, proSlaeClass, proSclass, proNoPurchase, proPosition, proReplenishmentParamsList,
                salesVolumeStartDate, salesVolumeEndDate, proPurchaseRate, excDea, storeCodes, gaiaExportTask);
        return ResultUtil.success();
    }

    @Log("商品自定义标签")
    @ApiOperation("商品自定义标签")
    @PostMapping("getProductZdy")
    public Result getProductZdy() {
        return dcReplenishService.getProductZdy();
    }

    @Log("查询条件")
    @ApiOperation("查询条件")
    @PostMapping("getProReplenishmentCondit")
    public Result getProReplenishmentCondit() {
        return dcReplenishService.getProReplenishmentCondit();
    }

    @Log("采购员下拉框")
    @ApiOperation("采购员下拉框")
    @PostMapping("getProPurchaseRateList")
    public Result getProPurchaseRateList() {
        return ResultUtil.success(dcReplenishService.getProPurchaseRateList());
    }

    @Log("业务员下拉框")
    @ApiOperation("业务员下拉框")
    @PostMapping("supplierSalesman")
    public Result supplierSalesman(@RequestJson(value = "dcCode", name = "补货仓库") String dcCode) {
        return ResultUtil.success(dcReplenishService.supplierSalesman(dcCode));
    }

    @Log("门店库存")
    @ApiOperation("门店库存")
    @PostMapping("getStoreStock")
    public Result getStoreStock(@RequestJson(value = "dcCode", name = "仓库") String dcCode, @RequestJson(value = "proSelfCode", name = "商品编号") String proSelfCode) {
        return ResultUtil.success(dcReplenishService.getStoreStock(dcCode, proSelfCode));
    }

    @Log("商品自定义字段列表")
    @ApiOperation("商品自定义字段列表")
    @PostMapping("getProZdyList")
    public Result getProZdyList() {
        return ResultUtil.success(dcReplenishService.getProZdyList());
    }
}

