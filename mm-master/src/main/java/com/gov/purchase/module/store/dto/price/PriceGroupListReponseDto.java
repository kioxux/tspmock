package com.gov.purchase.module.store.dto.price;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(value = "零售价列表返回参数")
public class PriceGroupListReponseDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "价格组ID", name = "prcGroupId")
    private String prcGroupId;

    @ApiModelProperty(value = "价格组描述", name = "prcGroupName")
    private String prcGroupName;
}
