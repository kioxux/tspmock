package com.gov.purchase.module.replenishment.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaSourceList;
import com.gov.purchase.module.replenishment.dto.*;
import com.gov.purchase.module.replenishment.service.SourceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "货源清单")
@RestController
@RequestMapping("source")
public class SourceController {

    @Resource
    private SourceService sourceService;


    @Log("货源清单创建、修改列表")
    @ApiOperation("货源清单创建、修改列表")
    @PostMapping("querySourceCreateList")
    public Result querySourceCreateList(@Valid @RequestBody SourceCreateListRequestDto dto) {
        return ResultUtil.success(sourceService.querySourceCreateList(dto));
    }


    @Log("货源清单创建、修改")
    @ApiOperation("货源清单创建、修改")
    @PostMapping("saveSourceList")
    public Result saveSourceList(@Valid @RequestBody List<SaveSourceRequestDto> list) {
        return sourceService.saveSourceList(list);
    }


    @Log("货源清单列表")
    @ApiOperation("货源清单列表")
    @PostMapping("querySourceList")
    public Result querySourceList(@Valid @RequestBody SourceListRequestDto dto) {
        return ResultUtil.success(sourceService.querySourceList(dto));
    }


    @Log("货源清单详情")
    @ApiOperation("货源清单详情")
    @PostMapping("querySourceDetail")
    public Result querySourceDetail(@Valid @RequestBody SourceDetailRequestDto dto) {
        return ResultUtil.success(sourceService.querySourceDetail(dto));
    }


    @Log("货源清单批量新增")
    @ApiOperation("货源清单批量新增")
    @PostMapping("batchSaveSourceList")
    public Result batchSaveSourceList(@Valid @RequestBody List<SourceRequestNewDTO> sourceList) {
        return sourceService.batchSaveSourceList(sourceList);
    }


    @Log("货源清单批量更新")
    @ApiOperation("货源清单批量更新")
    @PostMapping("batchUpdateSourceList")
    public Result batchUpdateSourceList(@RequestBody List<GaiaSourceList> sourceList) {
        return sourceService.batchUpdateSourceList(sourceList);
    }

    @Log("货源清单导出")
    @ApiOperation("货源清单导出")
    @PostMapping("exportSourceList")
    public Result exportSourceList(@Valid @RequestBody SourceListExportDto dto) {
        return sourceService.exportSourceList(dto);
    }


}
