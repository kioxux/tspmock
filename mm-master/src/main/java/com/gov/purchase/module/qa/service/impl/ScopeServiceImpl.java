package com.gov.purchase.module.qa.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaBusinessScope;
import com.gov.purchase.entity.GaiaBusinessScopeKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaBusinessScopeMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.qa.dto.*;
import com.gov.purchase.module.qa.service.ScopeService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ScopeServiceImpl implements ScopeService {

    @Resource
    private GaiaBusinessScopeMapper gaiaBusinessScopeMapper;

    @Resource
    private FeignService feignService;

    @Override
    public PageInfo<QueryScopeListResponseDto> queryBusinessScopeList(QueryScopeListRequestDto dto) {
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());

        return new PageInfo<>(gaiaBusinessScopeMapper.queryBusinessScopeList(dto));
    }

    @Override
    public Result validBusinessScopeInfo(List<ValidBusinessScopeInfoRequestDto> list) {
        List<String> bscCodeList = new ArrayList<>();
        for (ValidBusinessScopeInfoRequestDto validBusinessScopeInfoRequestDto : list) {
            ValidationResult result = ValidateUtil.validateEntity(validBusinessScopeInfoRequestDto);
            if (result.isHasErrors()) {
                throw new CustomResultException(result.getMessage());
            }
            if (bscCodeList.contains(validBusinessScopeInfoRequestDto.getBscCodeNew())) {
                throw new CustomResultException(ResultEnum.SCOPE_EXIST);
            }
            if (!"0".equals(validBusinessScopeInfoRequestDto.getType())) {
                bscCodeList.add(validBusinessScopeInfoRequestDto.getBscCodeNew());
            }
            switch (validBusinessScopeInfoRequestDto.getType()) {
                case "1": //  新增 验证重复性
                    boolean flg = false;
                    for (ValidBusinessScopeInfoRequestDto entity : list) {
                        if (StringUtils.isNotBlank(entity.getBscCode()) &&
                                validBusinessScopeInfoRequestDto.getBscCodeNew().equals(entity.getBscCode()) &&
                                "0".equals(entity.getType())) {
                            flg = true;
                            continue;
                        }
                    }
                    if (!flg) {
                        GaiaBusinessScopeKey gaiaBusinessScopeKey = new GaiaBusinessScopeKey();
                        // 加盟商
                        gaiaBusinessScopeKey.setClient(validBusinessScopeInfoRequestDto.getClient());
                        // 实体类型
                        gaiaBusinessScopeKey.setBscEntityClass(validBusinessScopeInfoRequestDto.getBscEntityClass());
                        // 实体编码
                        gaiaBusinessScopeKey.setBscEntityId(validBusinessScopeInfoRequestDto.getBscEntityId());
                        // 经营范围编码
                        gaiaBusinessScopeKey.setBscCode(validBusinessScopeInfoRequestDto.getBscCodeNew());
                        GaiaBusinessScope gaiaBusinessScope = gaiaBusinessScopeMapper.selectByPrimaryKey(gaiaBusinessScopeKey);
                        if (gaiaBusinessScope != null && (StringUtils.isBlank(gaiaBusinessScope.getBscDeleteFlag()) || "0".equals(gaiaBusinessScope.getBscDeleteFlag()))) {
                            throw new CustomResultException(ResultEnum.SCOPE_ISEXIST);
                        }
                    }
                    break;
                default:   // 更新 / 删除 时 验证数据是否存在，不存在 则提示 证照不存在
//                    if (gaiaBusinessScope == null) {
//                        throw new CustomResultException(ResultEnum.SCOPE_NOT_EXIST);
//                    }
                    break;
            }
        }
        return ResultUtil.success();
    }

    @Override
    @Transactional
    public Result saveBusinessScopeInfo(List<ValidBusinessScopeInfoRequestDto> list) {
        /**
         * 1.经营范围验证
         * 2.加盟商 + 实体类型 + 实体编码 中是否有2个一样的证照编码 如果有则提示，证照编码已存在
         * 3.新增验证、更新/删除验证
         */
        validBusinessScopeInfo(list);
        /**
         * 3。经营范围数据信息表（GAIA_BUSINESS_SCOPE）
         */
        for (ValidBusinessScopeInfoRequestDto validBusinessScopeInfoRequestDto : list) {
            saveBusinessScopeInfo("0", validBusinessScopeInfoRequestDto);
        }
        for (ValidBusinessScopeInfoRequestDto validBusinessScopeInfoRequestDto : list) {
            saveBusinessScopeInfo("2", validBusinessScopeInfoRequestDto);
        }
        for (ValidBusinessScopeInfoRequestDto validBusinessScopeInfoRequestDto : list) {
            saveBusinessScopeInfo("1", validBusinessScopeInfoRequestDto);
        }
        return ResultUtil.success();
    }

    private void saveBusinessScopeInfo(String type, ValidBusinessScopeInfoRequestDto validBusinessScopeInfoRequestDto) {
        GaiaBusinessScope scope = new GaiaBusinessScope();
        BeanUtils.copyProperties(validBusinessScopeInfoRequestDto, scope);
        if (!type.equals(validBusinessScopeInfoRequestDto.getType())) {
            return;
        }

        // 获取当前登录人
        TokenUser user = feignService.getLoginInfo();


        switch (validBusinessScopeInfoRequestDto.getType()) {
            case "1": //  新增
            {
                GaiaBusinessScopeKey gaiaBusinessScopeKey = new GaiaBusinessScopeKey();
                // 加盟商
                gaiaBusinessScopeKey.setClient(scope.getClient());
                // 实体类型
                gaiaBusinessScopeKey.setBscEntityClass(scope.getBscEntityClass());
                // 实体编码
                gaiaBusinessScopeKey.setBscEntityId(scope.getBscEntityId());
                // 经营范围编码
                gaiaBusinessScopeKey.setBscCode(validBusinessScopeInfoRequestDto.getBscCodeNew());
                gaiaBusinessScopeMapper.deleteByPrimaryKey(gaiaBusinessScopeKey);
                scope.setBscCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                scope.setBscCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                scope.setBscCreateUser(user.getUserId());
                scope.setBscDeleteFlag("0");
                scope.setBscCode(validBusinessScopeInfoRequestDto.getBscCodeNew());
                gaiaBusinessScopeMapper.insert(scope);
            }
            break;
            case "2":  // 更新
                GaiaBusinessScopeKey gaiaBusinessScopeKey = new GaiaBusinessScopeKey();
                // 加盟商
                gaiaBusinessScopeKey.setClient(scope.getClient());
                // 实体类型
                gaiaBusinessScopeKey.setBscEntityClass(scope.getBscEntityClass());
                // 实体编码
                gaiaBusinessScopeKey.setBscEntityId(scope.getBscEntityId());
                if (StringUtils.isNotBlank(scope.getBscCode())) {
                    // 经营范围编码
                    gaiaBusinessScopeKey.setBscCode(scope.getBscCode());
                    gaiaBusinessScopeMapper.deleteByPrimaryKey(gaiaBusinessScopeKey);
                }
                // 经营范围编码
                gaiaBusinessScopeKey.setBscCode(validBusinessScopeInfoRequestDto.getBscCodeNew());
                gaiaBusinessScopeMapper.deleteByPrimaryKey(gaiaBusinessScopeKey);
                scope.setBscCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                scope.setBscCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                scope.setBscChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                scope.setBscChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                scope.setBscChangeUser(user.getUserId());
                scope.setBscCode(validBusinessScopeInfoRequestDto.getBscCodeNew());
                scope.setBscCreateUser(user.getUserId());
                scope.setBscDeleteFlag("0");
                gaiaBusinessScopeMapper.insert(scope);
                break;
            default:  // 删除
                if (StringUtils.isNotBlank(scope.getBscCode())) {
                    scope.setBscChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    scope.setBscChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    scope.setBscChangeUser(user.getUserId());
                    scope.setBscDeleteFlag("1");
                    gaiaBusinessScopeMapper.updateByPrimaryKeySelective(scope);
                }
                break;
        }
    }

    @Override
    public QueryBusinessScopeInfoResponseDto getBusinessScopeInfo(QueryBusinessScopeInfoRequestDto dto) {
        /**
         * 加盟商 + 实体类型 + 实体编号 查询经营范围数据信息表（GAIA_BUSINESS_SCOPE）
         */
        List<GaiaBusinessScopeExtendDto> list = gaiaBusinessScopeMapper.getBusinessScopeInfo(dto);
        QueryBusinessScopeInfoResponseDto responseDto = new QueryBusinessScopeInfoResponseDto();
        List<String> bscCodeList = new ArrayList<>();
        for (GaiaBusinessScopeExtendDto extendDto : list) {
            BeanUtils.copyProperties(extendDto, responseDto); //设置主属性

            bscCodeList.add(extendDto.getBscCode());
        }
        responseDto.setScopeList(bscCodeList);
        return responseDto;
    }

}
