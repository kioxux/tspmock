package com.gov.purchase.module.base.service.exportTask;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaExportTask;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.23
 */
public interface GaiaExportTaskService {
    /**
     * 导出任务列表
     *
     * @param pageSize
     * @param pageNum
     * @param getStatus
     * @param getType
     * @param getDateStart
     * @param getDateEnd
     * @return
     */
    Result getExportTaskList(Integer pageSize, Integer pageNum, Integer getStatus, Integer getType, String getDateStart, String getDateEnd);

    GaiaExportTask initExportTask(Integer getStatus, Integer getType);

    /**
     * 删除导出记录
     *
     * @param ids
     */
    void delExportTaskList(List<Integer> ids);
}
