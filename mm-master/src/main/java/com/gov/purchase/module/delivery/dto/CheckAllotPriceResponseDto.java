package com.gov.purchase.module.delivery.dto;

import com.gov.purchase.entity.GaiaAllotPrice;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "调拨价格查看返回参数")
public class CheckAllotPriceResponseDto extends GaiaAllotPrice {

    /**
     * 供货地点名
     */
    private String supplySiteName;

    /**
     * 收货地点名
     */
    private String receiveSiteName;

    /**
     * 订单类型名
     */
    private String orderTypeName;

    /**
     * 连锁总部名
     */
    private String chainName;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;
}
