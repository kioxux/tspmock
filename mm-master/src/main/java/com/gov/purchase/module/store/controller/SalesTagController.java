package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaPriceTag;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListExportRequestDto;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListReponseDto;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequest;
import com.gov.purchase.module.store.dto.salesTag.SalesTagListRequestDto;
import com.gov.purchase.module.store.service.SalesTagService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "销售标签")
@RestController
@RequestMapping("salesTag")
public class SalesTagController {


    @Autowired
    SalesTagService salesTagService;

    /**
     * 销售标签列表
     *
     * @param dto 查询条件
     * @return
     */
    @Log("销售标签列表")
    @ApiOperation("销售标签列表")
    @PostMapping("querysalesTagList")
    public Result querySalesTagList(@Valid @RequestBody SalesTagListRequestDto dto) {
        return ResultUtil.success(salesTagService.querySalesTagList(dto));
    }

    /**
     * 销售标签列表导出
     *
     * @param dto 查询条件
     * @return
     */
    @Log("销售标签列表")
    @ApiOperation("销售标签列表")
    @PostMapping("querysalesTagExportList")
    public Result querySalesTagExportList(@Valid @RequestBody SalesTagListExportRequestDto dto) {
        return ResultUtil.success(salesTagService.querySalesTagExportList(dto));
    }

    @Log("销售标签列表非套打")
    @ApiOperation("销售标签列表非套打")
    @PostMapping("querySalesTagList2")
    public Result querySalesTagList2(@RequestBody SalesTagListRequest salesTagListRequest) {
        return salesTagService.querySalesTagList2(salesTagListRequest);
    }

    @Log("销售标签套打导出")
    @ApiOperation("销售标签套打导出")
    @PostMapping("salesTagExprot")
    public Result salesTagExprot(@RequestBody SalesTagListRequestDto dto) {
        return salesTagService.salesTagExprot(dto);
    }

    @Log("销售标签列表非套打-打印保存")
    @ApiOperation("销售标签列表非套打-打印保存")
    @PostMapping("printTag")
    public Result printTag(@RequestBody List<SalesTagListReponseDto> salesTagListRequestList) {
        return salesTagService.printTag(salesTagListRequestList);
    }

    @Log("销售标签列表非套打-打印获取")
    @ApiOperation("销售标签列表非套打-打印获取")
    @GetMapping("getPrintTag")
    public Result getPrintTag(@RequestParam("key") String key) {
        return salesTagService.getPrintTag(key);
    }

    @Log("打印模板新增")
    @ApiOperation("打印模板新增")
    @PostMapping("savePriceTag")
    public Result savePriceTag(@RequestBody GaiaPriceTag gaiaPriceTag) {
        salesTagService.savePriceTag(gaiaPriceTag);
        return ResultUtil.success();
    }

    @Log("打印模板列表")
    @ApiOperation("打印模板列表")
    @PostMapping("selectPriceTagList")
    public Result selectPriceTagList(@RequestJson(value = "gptName", required = false) String gptName,
                                     @RequestJson(value = "gptType", required = false) Integer gptType,
                                     @RequestJson(value = "pageSize", required = true, name = "分页") Integer pageSize,
                                     @RequestJson(value = "pageNum", required = true, name = "分页") Integer pageNum) {
        return salesTagService.selectPriceTagList(gptName, gptType, pageNum, pageSize);
    }

    @Log("打印模板")
    @ApiOperation("打印模板")
    @PostMapping("selectPriceTag")
    public Result selectPriceTag(@RequestJson(value = "gptType", required = false) Integer gptType) {
        return salesTagService.selectPriceTag(gptType);
    }

    @Log("打印模板编辑")
    @ApiOperation("打印模板编辑")
    @PostMapping("editTag")
    public Result editTag(@RequestBody GaiaPriceTag gaiaPriceTag) {
        salesTagService.editTag(gaiaPriceTag);
        return ResultUtil.success();
    }

    @Log("打印模板删除")
    @ApiOperation("打印模板删除")
    @PostMapping("deleteTag")
    public Result deleteTag(@RequestJson(value = "id", required = false) Integer id) {
        salesTagService.deleteTag(id);
        return ResultUtil.success();
    }

    @Log("打印模板获取")
    @ApiOperation("打印模板获取")
    @PostMapping("selectTag")
    public Result selectTag(@RequestJson(value = "id", required = false) Integer id) {
        return salesTagService.selectTag(id);
    }
}
