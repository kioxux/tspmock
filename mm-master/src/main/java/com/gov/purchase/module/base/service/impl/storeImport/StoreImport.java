package com.gov.purchase.module.base.service.impl.storeImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.excel.Store;
import com.gov.purchase.module.base.service.impl.ImportData;
import org.apache.commons.collections4.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
public abstract class StoreImport extends ImportData {

    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;

    @Resource
    GaiaDeliveryCompanyMapper gaiaDeliveryCompanyMapper;

    @Resource
    DictionaryMapper dictionaryMapper;

    @Resource
    GaiaCompadmMapper gaiaCompadmMapper;

    /**
     * 数据集合验证
     *
     * @param dataMap
     * @return
     */
    protected Result checkList(LinkedHashMap<Integer, Store> dataMap) {
        List<String> errorList = new ArrayList<>();
        // 数据为空
        if (dataMap == null || dataMap.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        // 销项税率
        List<Dictionary> outputTaxList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.TAX_CODE2.getTable(),
                CommonEnum.DictionaryData.TAX_CODE2.getField1(),
                CommonEnum.DictionaryData.TAX_CODE2.getCondition(), CommonEnum.DictionaryData.TAX_CODE2.getSortFields());

        // 加盟商 门店编码 重复验证
        Map<String, List<String>> clientStoCodeMap = new HashMap<>();

        // 遍历验证
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Store store = dataMap.get(key);

            // 加盟商 门店编码
            List<String> clientStoCodeList = clientStoCodeMap.get(store.getClient() + "_" + store.getStoCode());
            if (clientStoCodeList == null) {
                clientStoCodeList = new ArrayList<>();
            }
            clientStoCodeList.add(String.valueOf((key + 1)));
            clientStoCodeMap.put(store.getClient() + "_" + store.getStoCode(), clientStoCodeList);

            // 其他相关验证
            otherCheck(store, errorList, key);

            // 关联门店
            {
                String stoRelationStore = store.getStoRelationStore();
                if (super.isNotBlank(stoRelationStore)) {
                    for (int i = 0; i < stoRelationStore.split(",").length; i++) {
                        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                        gaiaStoreDataKey.setClient(store.getClient());
                        gaiaStoreDataKey.setStoCode(stoRelationStore.split(",")[i]);
                        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                        // 关联门店不存在
                        if (gaiaStoreData == null) {
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "关联门店"));
                            break;
                        }
                    }
                }
            }
            // 开业日期	关店日期 比较
            if (super.isNotBlank(store.getStoOpenDate()) && super.isNotBlank(store.getStoCloseDate())) {
                if (store.getStoOpenDate().compareTo(store.getStoCloseDate()) > 0) {
                    errorList.add(MessageFormat.format(ResultEnum.E0135.getMsg(), key + 1));
                }
            }
            // 委托配送公司
            {
                String stoDeliveryCompany = store.getStoDeliveryCompany();
                if (super.isNotBlank(stoDeliveryCompany)) {
                    for (int i = 0; i < stoDeliveryCompany.split(",").length; i++) {
                        // 委托配送公司
                        GaiaDeliveryCompany gaiaDeliveryCompany = gaiaDeliveryCompanyMapper.selectByPrimaryKey(stoDeliveryCompany.split(",")[i]);
                        // 委托配送公司不存在
                        if (gaiaDeliveryCompany == null) {
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "委托配送公司"));
                            break;
                        }
                    }
                }
            }

            // 纳税主体
            String stoTaxSubject = store.getStoTaxSubject();
            if (super.isNotBlank(stoTaxSubject)) {
                GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
                gaiaDcDataKey.setClient(store.getClient());
                gaiaDcDataKey.setDcCode(stoTaxSubject);
                GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
                // 纳税主体 不存在
                if (gaiaDcData == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "纳税主体"));
                }
            }
            // 配送中心
            if (super.isNotBlank(store.getStoDcCode())) {
                GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
                gaiaDcDataKey.setClient(store.getClient());
                gaiaDcDataKey.setDcCode(store.getStoDcCode());
                GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
                // 配送中心不存在
                if (gaiaDcData == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "配送中心"));
                }
            }
        }

        // 加盟商门店编码重复验证
        for (String clientProCode : clientStoCodeMap.keySet()) {
            List<String> lineList = clientStoCodeMap.get(clientProCode);
            if (lineList.size() > 1) {
                errorList.add(MessageFormat.format(ResultEnum.E0136.getMsg(), String.join(",", lineList)));
            }
        }

        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }

    /**
     * 相关验证
     *
     * @param store
     * @param errorList
     * @param key
     */
    protected abstract void otherCheck(Store store, List<String> errorList, int key);
}

