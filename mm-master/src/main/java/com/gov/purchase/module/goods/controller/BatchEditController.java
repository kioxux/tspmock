package com.gov.purchase.module.goods.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaProductUpdateItem;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.BatchEditParam;
import com.gov.purchase.module.goods.dto.ProductUpdate;
import com.gov.purchase.module.goods.dto.ProductUpdateItem;
import com.gov.purchase.module.goods.service.BatchEditService;
import com.gov.purchase.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.09
 */
@Api(tags = "批量修改")
@RestController
@EnableAsync
@RequestMapping("batchEdit")
public class BatchEditController {
    @Resource
    private BatchEditService batchEditService;
    @Resource
    private OperationFeignService operationFeignService;
    @Resource
    private FeignService feignService;

    @Log("修改项目")
    @ApiOperation("修改项目")
    @PostMapping("selectProductUpdateItemList")
    public Result selectProductUpdateItemList() {
        List<GaiaProductUpdateItem> list = batchEditService.selectProductUpdateItemList();
        return ResultUtil.success(list);
    }

    @Log("地点")
    @ApiOperation("地点")
    @PostMapping("selectProductUpdateSiteList")
    public Result selectProductUpdateSiteList() {
        List<Dictionary> list = batchEditService.selectProductUpdateSiteList();
        return ResultUtil.success(list);
    }

    @Log("修改商品")
    @ApiOperation("修改商品")
    @PostMapping("selectProductItem")
    public Result selectProductItem(@RequestJson(value = "proSite", name = "地点") String proSite,
                                    @RequestJson(value = "proSelfCode", name = "商品编码", required = false) String proSelfCode,
                                    @RequestJson(value = "gpuiField", name = "修改字段") String gpuiField,
                                    @RequestJson(value = "batchEditParamList", required = false) List<BatchEditParam> batchEditParamList) {
        List<ProductUpdateItem> productUpdateItemList = batchEditService.selectProductItem(proSite, proSelfCode, gpuiField, batchEditParamList);
        return ResultUtil.success(productUpdateItemList);
    }

    @Log("修改商品")
    @ApiOperation("修改商品")
    @PostMapping("updateProduct")
    public Result updateProduct(@RequestBody ProductUpdate productUpdate) {
        TokenUser user = feignService.getLoginInfo();
        // 地点
        List<Dictionary> dictionary = productUpdate.getDictionary();
        if (CollectionUtils.isNotEmpty(dictionary)) {
            for (Dictionary item : dictionary) {
                if (item == null || StringUtils.isBlank(item.getValue()) || StringUtils.isBlank(item.getType())) {
                    throw new CustomResultException("请选择修改地点");
                }
            }
        } else {
            throw new CustomResultException("请选择修改地点");
        }
        // 商品
        List<ProductUpdateItem> productUpdateItemList = productUpdate.getProductUpdateItemList();
        if (CollectionUtils.isEmpty(productUpdateItemList)) {
            throw new CustomResultException("请选择修改商品");
        }
        for (ProductUpdateItem productUpdateItem : productUpdateItemList) {
            // 商品
            String proSelfCode = productUpdateItem.getProSelfCode();
            // 值
            String value = productUpdateItem.getNewVal();
            // 字段名
            String gpuiName = productUpdateItem.getGpuiName();
            // 字段类型 1:STRING,2:INT,3:decimal
            Integer gpuiType = productUpdateItem.getGpuiType();
            // 长度
            Integer gpuiLength = productUpdateItem.getGpuiLength();
            // 可空  0:可空,1:非空
            Integer gpuiEmpty = productUpdateItem.getGpuiEmpty();
            // 字段值域
            String gpuiValue = productUpdateItem.getGpuiValue();
            // 非空验证
            if (gpuiEmpty.intValue() == 1) {
                if (StringUtils.isBlank(value)) {
                    throw new CustomResultException(StringUtils.parse("商品{}字段{}值不能为空", proSelfCode, gpuiName));
                }
            }
            // 长度验证
            if (gpuiLength != null && StringUtils.isNotBlank(value)) {
                if (StringUtils.length(value) > gpuiLength.intValue()) {
                    throw new CustomResultException(StringUtils.parse("商品{}字段{}值长度不能超过{}", proSelfCode, gpuiName, gpuiLength.intValue()));
                }
            }
            // 类型验证
            if (gpuiType != null && StringUtils.isNotBlank(value)) {
                if (gpuiType.intValue() == 1) { // 1:STRING

                } else if (gpuiType.intValue() == 2) {  // 2:INT
                    if (NumberUtils.toInt(value.toString(), -999) == -999) {
                        throw new CustomResultException(StringUtils.parse("商品{}字段{}值只能为整数", proSelfCode, gpuiName));
                    }
                } else if (gpuiType.intValue() == 3) {  // 3:decimal
                    if (NumberUtils.toDouble(value.toString(), -999) == -999) {
                        throw new CustomResultException(StringUtils.parse("商品{}字段{}值只能为数字", proSelfCode, gpuiName));
                    }
                } else if (gpuiType.intValue() == 4) {  // 3:decimal
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                        sdf.setLenient(false);
                        sdf.parse(value);
                    } catch (Exception e) {
                        throw new CustomResultException(StringUtils.parse("商品{}字段{}值应为yyyyMMdd", proSelfCode, gpuiName));
                    }
                } else {
                    throw new CustomResultException(StringUtils.parse("商品{}字段{}值未定义", proSelfCode, gpuiName));
                }
            }
            // 字段值域
            if (StringUtils.isNotBlank(gpuiValue) && StringUtils.isNotBlank(value)) {
                String[] gpuiValues = gpuiValue.split(",");
                boolean flg = false;
                for (String item : gpuiValues) {
                    if (item.equals(value)) {
                        flg = true;
                    }
                }
                if (!flg) {
                    throw new CustomResultException(StringUtils.parse("商品{}字段{}值只能在指定范围{}", proSelfCode, gpuiName, gpuiValue));
                }
            }

        }
        Map<String, List<String>> map = new HashMap<>();
        List<String> proSiteList = new ArrayList<>();
        List<String> proSelfCodeList = new ArrayList<>();
        try {
            // 更新
            for (ProductUpdateItem productUpdateItem : productUpdateItemList) {
                batchEditService.updateProduct(productUpdateItem, dictionary, map, proSiteList, proSelfCodeList);
            }
        } catch (Exception e) {
            throw new CustomResultException("商品未全部更新成功，请再试一次");
        } finally {
            try {
                // 第三方 gys-operation
                if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                    operationFeignService.multipleStoreProductSync(user.getClient(), proSiteList, proSelfCodeList);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return ResultUtil.success();
    }

}
