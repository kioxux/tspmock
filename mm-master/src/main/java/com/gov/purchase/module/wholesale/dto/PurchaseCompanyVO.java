package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

/**
 * @description: 批发公司
 * @author: yzf
 * @create: 2021-11-17 17:06
 */
@Data
    public class PurchaseCompanyVO {

    private String client;

    /**
     * 采购主体id
     */
    private String code;

    /**
     * 采购客户名
     */
    private String name;

}
