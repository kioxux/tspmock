package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;


@Data
@ApiModel(value = "采购订单查询返回参数")
public class PoHeaderListResponseDto {

    /**
     * 加盟商ID
     */
    private String client;

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 采购主体（公司）
     */
    private String poCompanyCode;

    /**
     * 采购主体名称（公司）
     */
    private String poCompanyName;

    /**
     * 创建人
     */
    private String poCreateBy;

    /**
     * 创建人
     */
    private String poCreateByName;

    /**
     * 采购凭证号
     */
    private String poId;

    /**
     * 订单行号
     */
    private String poLineNo;

    /**
     * 采购订单类型
     */
    private String poType;

    /**
     * 采购订单类型名称
     */
    private String ordTypeDesc;

    /**
     * 订单日期
     */
    private String poDate;

    /**
     * 供应商自编码
     */
    private String poSupplierId;

    /**
     * 供应商名称
     */
    private String poSupplierName;

    /**
     * 付款条件
     */
    private String poPaymentId;

    /*
     * 付款条件名
     */
    private String poPaymentName;

    /**
     * 审批状态
     */
    private String poApproveStatus;

    /**
     * 订单总金额
     */
    private BigDecimal poTotalAmount;

    /**
     * 抬头备注
     */
    private String poHeadRemark;

    /**
     * 物流模式
     */
    private String poDeliveryType;

    /**
     * 物流模式门店
     */
    private String poDeliveryTypeStore;

    /**
     * 物流模式门店
     */
    private String poDeliveryTypeStoreName;

    /**
     * 商品自编码
     */
    private String poProCode;

    /**
     * 商品名称
     */
    private String poProName;

    /**
     * 通用名
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryCode;

    /**
     * 生产厂家名
     */
    private String proFactoryName;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 订单数量
     */
    private BigDecimal poQty;

    /**
     * 单位
     */
    private String poUnit;

    /**
     * 单位名
     */
    private String poUnitName;

    /**
     * 单价
     */
    private BigDecimal poPrice;

    /**
     * 税率
     */
    private String poRate;

    /**
     * 税率名
     */
    private String poRateName;

    /**
     * 总行价
     */
    private BigDecimal poLineTotalaMount;

    /**
     * 地点
     */
    private String poSiteCode;

    /**
     * 地点名
     */
    private String poSiteName;

    /**
     * 库位
     */
    private String poLocationCode;

    /**
     * 批次
     */
    private String poBatch;

    /**
     * 计划交货日期
     */
    private String poDeliveryDate;

    /**
     * 行备注
     */
    private String poLineRemark;

    /**
     * 删除标记
     */
    private String poLineDelete;

    /**
     * 交货已完成标记
     */
    private String poCompleteFlag;

    /**
     * 已交货数量
     */
    private BigDecimal poDeliveredQty;

    /**
     * 已开票数量
     */
    private BigDecimal poInvoiceQty;

    /**
     * 已交货金额
     */
    private BigDecimal poDeliveredAmt;

    /**
     * 已开票金额
     */
    private BigDecimal poInvoiceAmt;

    /**
     * 工作流编号
     */
    private String poFlowNo;

    /**
     * 生产批号
     */
    private String poBatchNo;

    /**
     * 生产日期
     */
    private String poScrq;

    /**
     * 有效期
     */
    private String poYxq;

    /**
     * 大分类名
     */
    private String proBigClassName;

    /**
     * 中分类名
     */
    private String proMidClassName;
    /**
     * 分类名
     */
    private String proClassName;
    /**
     * 采购员
     */
    private String proPurchaseRate;
    /**
     * 自定义字段1
     */
    private String proZdy1;
    /**
     * 自定义字段2
     */
    private String proZdy2;
    /**
     * 自定义字段3
     */
    private String proZdy3;
    /**
     * 自定义字段4
     */
    private String proZdy4;
    /**
     * 自定义字段5
     */
    private String proZdy5;

    /**
     * 单据金额
     */
    private BigDecimal poLineAmt;

    /**
     * 业务员姓名
     */
    private String gssName;

    /**
     * 到货日期
     */
    private String wmDhrq;

    /**
     * 商品零售价
     */
    private BigDecimal proLsj;
}