package com.gov.purchase.module.replenishment.controller;

import com.gov.purchase.module.replenishment.service.DistributionPlanJobService;
import com.gov.purchase.module.replenishment.service.DistributionPlanService;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/16 9:40
 **/
@Slf4j
@Component
public class NewDistributionPlanJob {

    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private DistributionPlanService distributionPlanService;
    @Resource
    private DistributionPlanJobService distributionPlanJobService;

    /**
     * 每天自动执行昨日入库新品的铺货计划，无计划消息提醒
     * @param params 执行参数
     * @return 成功
     */
    @XxlJob("distributionPlanJob")
    public void distributionPlanJob() {
        String jobParam = XxlJobHelper.getJobParam();
        log.info(String.format("<新品铺货><自动铺货><自动铺货任务调用成功，param=%s>", jobParam));
        threadPoolTaskExecutor.execute(() -> distributionPlanJobService.autoExecPlan(jobParam));
        XxlJobHelper.handleSuccess();
    }

    /**
     * 校验上周入库的新品是否存在未铺货的并消息提醒
     */
    @XxlJob("checkNewProductByWeek")
    public ReturnT<String> checkNewProductByWeek(String params) {
        log.info(String.format("<新品铺货><每周核检><每周核检新品入库未铺货任务调用成功，param=%s>", params));
        threadPoolTaskExecutor.execute(() -> distributionPlanService.checkNewProductByWeek());
        return ReturnT.SUCCESS;
    }

    /**
     * 比对已完成、已过期的铺货计划是否存在差异
     */
    @XxlJob("checkDistributionDiff")
    public ReturnT<String> checkDistributionDiff(String params) {
        log.info(String.format("<新品铺货><铺货差异><铺货差异比对任务调用成功，param=%s>", params));
        threadPoolTaskExecutor.execute(() -> distributionPlanService.checkDistributionDiff());
        return ReturnT.SUCCESS;
    }
}
