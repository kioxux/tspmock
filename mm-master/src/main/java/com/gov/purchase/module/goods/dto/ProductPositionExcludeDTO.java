package com.gov.purchase.module.goods.dto;

import lombok.Data;

/**
 * @author zhoushuai
 * @date 2021-06-01 13:18
 */
@Data
public class ProductPositionExcludeDTO {

    /**
     * 加盟商
     */
    private String client;
    /**
     * 成分分类开始字符
     */
    private String gppeProCompclassStart;
    /**
     * 商品分类开始字符
     */
    private String gppeProClassStart;
    /**
     * 默认定位
     */
    private String gppeProPosition;


    public ProductPositionExcludeDTO(String client, String gppeProCompclassStart, String gppeProClassStart, String gppeProPosition) {
        this.client = client;
        this.gppeProCompclassStart = gppeProCompclassStart;
        this.gppeProClassStart = gppeProClassStart;
        this.gppeProPosition = gppeProPosition;
    }
}
