package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.feign.PurchaseFeignDto;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.GaiaPoItemList;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.OrderPurchaseService;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentListResponseDto;
import com.gov.purchase.module.replenishment.service.DcReplenishService;
import com.gov.purchase.module.wholesale.dto.GaiaWmsShenPiPeiZhiDTO;
import com.gov.purchase.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderPurchaseServiceImpl implements OrderPurchaseService {

    @Resource
    GaiaPoHeaderMapper gaiaPoHeaderMapper;
    @Resource
    GaiaMaterialDocMapper gaiaMaterialDocMapper;
    @Resource
    GaiaPoItemMapper gaiaPoItemMapper;
    @Resource
    GaiaSourceListMapper gaiaSourceListMapper;
    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    GaiaApproveRuleMapper gaiaApproveRuleMapper;
    @Resource
    GaiaSoItemMapper gaiaSoItemMapper;
    @Resource
    GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;
    @Resource
    FeignService feignService;
    @Resource
    CosUtils cosUtils;
    @Resource
    GaiaMaterialAssessMapper gaiaMaterialAssessMapper;
    @Resource
    BusinessScopeServiceMapper businessScopeServiceMapper;
    @Resource
    GaiaGlobalGspConfigureMapper gaiaGlobalGspConfigureMapper;
    @Resource
    DcReplenishService dcReplenishService;
    @Resource
    GaiaDcDataMapper dcDataMapper;
    @Resource
    GaiaJyfwDataMapper gaiaJyfwDataMapper;
    @Resource
    DictionaryMapper dictionaryMapper;
    @Resource
    RedisClient redisClient;
    @Resource
    GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Autowired
    SalesmanMaintainProMapper salesmanMaintainProMapper;

    /**
     * 供应商验证
     *
     * @param client
     * @param site
     * @param supId
     */
    private void supplierCheck(GaiaDcData gaiaDcData, String client, String site, String supId, List<String> warning, List<String> error) {
        if (gaiaDcData == null) {
            return;
        }
        // 是否管控证照效期：1-是
        if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
            GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
            gaiaSupplierBusinessKey.setClient(client);
            gaiaSupplierBusinessKey.setSupSite(site);
            gaiaSupplierBusinessKey.setSupSelfCode(supId);
            GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(gaiaSupplierBusinessKey);
            if (gaiaSupplierBusiness != null) {
                // 营业执照期限、
                dateCheck(gaiaSupplierBusiness.getSupCreditDate(), "营业执照", warning, error);
                // 发证有效期、
                dateCheck(gaiaSupplierBusiness.getSupLicenceValid(), "许可证", warning, error);
                // 法人委托书有效期、
                dateCheck(gaiaSupplierBusiness.getSupFrwtsxq(), "法人委托书", warning, error);
                // GSP有效期、
                dateCheck(gaiaSupplierBusiness.getSupGspDate(), "GSP证书", warning, error);
                // GMP有效期、
                dateCheck(gaiaSupplierBusiness.getSupGmpDate(), "GMP证书", warning, error);
                // 质保协议有效期、
                dateCheck(gaiaSupplierBusiness.getSupQualityDate(), "质保协议", warning, error);
                // 生产许可证有效期、
                dateCheck(gaiaSupplierBusiness.getSupScxkzDate(), "生产许可证", warning, error);
                // 食品许可证有效期、
                dateCheck(gaiaSupplierBusiness.getSupSpxkzDate(), "食品许可证", warning, error);
                // 医疗器械经营许可证有效期
                dateCheck(gaiaSupplierBusiness.getSupYlqxxkzDate(), "医疗器械经营许可证", warning, error);
                // 年度报告有效期
                dateCheck(gaiaSupplierBusiness.getSupNdbgyxq(), "年度报告", warning, error);
                // 购销合同有效期
                dateCheck(gaiaSupplierBusiness.getSupGxhtyxq(), "购销合同", warning, error);
            }
        }
    }

    /**
     * 效期验证
     *
     * @param data
     * @param msg
     */
    private void dateCheck(String data, String msg, List<String> warning, List<String> error) {
        String day = DateUtils.getCurrentDateStr("yyyyMMdd");
        if (StringUtils.isNotBlank(data) && data.trim().length() == 8) {
            // 异常
            if (day.compareTo(data) > 0) {
                error.add("供应商" + msg + "已过期");
            }
            // 警告
            Long datediff = DateUtils.datediff(day, data);
            if (datediff.intValue() < 60) {
                warning.add("供应商" + msg + "即将过期");
            }
        }
    }

    public Result checkPurchaseOrder(CreatePurchaseOrderRequestDto dto) {
        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        //TokenUser user = feignService.getLoginInfo();
        //采购凭证号
        // CommonUtils utils = new CommonUtils();
        // String poId = utils.getPoId(dto.getClient(), dto.getPoType(), DateUtils.getCurrentDateStr("yyyy"), gaiaPoHeaderMapper);
        // 供应商效期验证
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(dto.getClient());
        gaiaDcDataKey.setDcCode(dto.getPoCompanyCode());
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
        log.info("供应商数据：{}", JsonUtils.beanToJson(gaiaDcData));
        supplierCheck(gaiaDcData, dto.getClient(), dto.getPoCompanyCode(), dto.getPoSupplierId(), warning, error);
        // 供应商经营范围
        List<String> supScopeList = businessScopeServiceMapper.getsupScopeList(dto.getClient(), dto.getPoSupplierId(), 1, dto.getPoCompanyCode());
        log.info("供应商经营范围：{}", JsonUtils.beanToJson(supScopeList));
        boolean ignoreScopeCheck = false;
        if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && "2".equals(gaiaDcData.getDcJyfwgk())) {
            List<String> class3List = businessScopeServiceMapper.getsupScopeListClass3(dto.getClient(), dto.getPoSupplierId(), 1, dto.getPoCompanyCode());
            if (CollectionUtils.isNotEmpty(class3List)) {
                ignoreScopeCheck = true;
            }
        }
        int index = 0;
        for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {
            index++;
            if (StringUtils.isBlank(gaiaPoItemList.getPoProCode())) {
                throw new CustomResultException("第" + index + "行商品异常，请删除后重新添加");
            }
            String nowDate = DateUtils.getCurrentDateStrYYMMDD();
            // 1、有效期不能小于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq())) {
                if (gaiaPoItemList.getPoYxq().compareTo(nowDate) < 0) {
                    throw new CustomResultException("第" + index + "行有效期不能小于当前日期");
                }
            }
            // 2、生产日期不能大于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(nowDate) > 0) {
                    throw new CustomResultException("第" + index + "行生产日期不能大于当前日期");
                }
            }
            // 3、有效期不能比生产日期小
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq()) && StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(gaiaPoItemList.getPoYxq()) > 0) {
                    throw new CustomResultException("第" + index + "行有效期不能比生产日期小");
                }
            }
            //商品状态检查
            GaiaProductBusiness status = gaiaProductBusinessMapper.selectGoodsStatus(dto.getClient(), gaiaPoItemList.getPoProCode(), gaiaPoItemList.getPoSiteCode());
            // 检查 商品主数据 注册证效期（批准文号有效期）：小于60天-提示，不报错；过期-报错。
            log.info("商品状态检查：{}", JsonUtils.beanToJson(status));
            if (gaiaDcData != null) {
                // 是否管控证照效期：1-是
                if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
                    if (StringUtils.isNotBlank(status.getProRegisterExdate()) && status.getProRegisterExdate().trim().length() == 8) {
                        String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                        // 警告
                        Long datediff = DateUtils.datediff(day, status.getProRegisterExdate());
                        if (0 <= datediff.intValue() && datediff.intValue() < 60) {
                            warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号即将过期");
                        }
                        if (datediff.intValue() < 0) {
                            warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号已过期");
                        }
                    }
                }
            }

            // 经营范围验证
            if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && !ignoreScopeCheck && gaiaDcData.getDcJyfwgk().equals("1")) {
                if (CollectionUtils.isEmpty(supScopeList)) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
                if (status == null || StringUtils.isBlank(status.getProJylb())) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
                if (Collections.disjoint(supScopeList, Arrays.asList(status.getProJylb().split(",")))) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
            }
            if (null != status && CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(status.getProStatus())) {
                throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "为停用商品不可进行采购");
            }
            //商品禁采检查
            if (null != status && CommonEnum.NoYesStatus.YES.getCode().equals(status.getProNoPurchase())) {
                throw new CustomResultException(MessageFormat.format(ResultEnum.PURCHASE_NOT_VALID.getMsg(), gaiaPoItemList.getPoProCode()));
            }
            //商品禁退检查
            if (null != status && CommonEnum.NoYesStatus.YES.getCode().equals(status.getProNoSupplier())) {
                throw new CustomResultException(ResultEnum.CUSPLIER_NOT_VALID);
            }
            //采购主体与详细地点必须相同
            if (!dto.getPoCompanyCode().equals(gaiaPoItemList.getPoSiteCode())) {
                throw new CustomResultException(ResultEnum.SITEDIFF_ERROR);
            }
        }
        log.info("错误信息：{}", JsonUtils.beanToJson(error));
        if (CollectionUtils.isNotEmpty(error)) {
            Result result = ResultUtil.error(ResultEnum.E0015);
            result.setWarning(error);
            return result;
        }
        return ResultUtil.success();
    }

    @Override
    public Result checkExistOrder(String proSelfCode, String poCompanyCode) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaPoItem> poItems = gaiaPoItemMapper.checkExistOrder(user.getClient(), poCompanyCode, proSelfCode);
        return ResultUtil.success(poItems);
    }

    @Override
    public String queryPoConfig() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "PO_IF_SAME");
    }

    @Override
    public String selectPoDateChange() {
        TokenUser user = feignService.getLoginInfo();
        String gcspPara1 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(user.getClient(), "PO_DATE_CHANGE");
        // 0：关 1：开
        return StringUtils.isBlank(gcspPara1) ? "1" : gcspPara1;
    }

    /**
     * 采购订单创建
     *
     * @param dto CreatePurchaseOrderRequestDto
     */
    @Transactional
    public Result insertPurchaseOrder(CreatePurchaseOrderRequestDto dto) {
        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        //采购凭证号
        CommonUtils utils = new CommonUtils();
        String poId = utils.getPoId(dto.getClient(), dto.getPoType(), DateUtils.getCurrentDateStr("yyyy"), gaiaPoHeaderMapper, redisClient);
        // 供应商效期验证
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(dto.getClient());
        gaiaDcDataKey.setDcCode(dto.getPoCompanyCode());
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
        log.info("供应商数据：{}", JsonUtils.beanToJson(gaiaDcData));
        supplierCheck(gaiaDcData, dto.getClient(), dto.getPoCompanyCode(), dto.getPoSupplierId(), warning, error);
        // 供应商经营范围
        List<String> supScopeList = businessScopeServiceMapper.getsupScopeList(dto.getClient(), dto.getPoSupplierId(), 1, dto.getPoCompanyCode());
        log.info("供应商经营范围：{}", JsonUtils.beanToJson(supScopeList));
        boolean ignoreScopeCheck = false;
        if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && "2".equals(gaiaDcData.getDcJyfwgk())) {
            List<String> class3List = businessScopeServiceMapper.getsupScopeListClass3(dto.getClient(), dto.getPoSupplierId(), 1, dto.getPoCompanyCode());
            if (CollectionUtils.isNotEmpty(class3List)) {
                ignoreScopeCheck = true;
            }
        }
        int index = 0;
        for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {
            index++;
            if (StringUtils.isBlank(gaiaPoItemList.getPoProCode())) {
                throw new CustomResultException("第" + index + "行商品异常，请删除后重新添加");
            }
            String nowDate = DateUtils.getCurrentDateStrYYMMDD();
            // 1、有效期不能小于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq())) {
                if (gaiaPoItemList.getPoYxq().compareTo(nowDate) < 0) {
                    throw new CustomResultException("第" + index + "行有效期不能小于当前日期");
                }
            }
            // 2、生产日期不能大于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(nowDate) > 0) {
                    throw new CustomResultException("第" + index + "行生产日期不能大于当前日期");
                }
            }
            // 3、有效期不能比生产日期小
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq()) && StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(gaiaPoItemList.getPoYxq()) > 0) {
                    throw new CustomResultException("第" + index + "行有效期不能比生产日期小");
                }
            }
            //商品状态检查
            GaiaProductBusiness status = gaiaProductBusinessMapper.selectGoodsStatus(dto.getClient(), gaiaPoItemList.getPoProCode(), gaiaPoItemList.getPoSiteCode());
            // 检查 商品主数据 注册证效期（批准文号有效期）：小于60天-提示，不报错；过期-报错。
            log.info("商品状态检查：{}", JsonUtils.beanToJson(status));
            String day = DateUtils.getCurrentDateStr("yyyyMMdd");
            if (gaiaDcData != null) {
                // 是否管控证照效期：1-是
                if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
                    if (StringUtils.isNotBlank(status.getProRegisterExdate()) && status.getProRegisterExdate().trim().length() == 8) {
                        // 警告
                        Long datediff = DateUtils.datediff(day, status.getProRegisterExdate());
                        if (datediff.intValue() >= 0 && datediff.intValue() < 60) {
                            warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号即将过期");
                        }
                        if (datediff.intValue() < 0) {
                            warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号已过期");
                        }
                    }
                }
            }
            // 参数表：GAIA_CL_SYSTEM_PARA增加参数 PO_PZWH_CHECK采购订单是否强控批准文号有效期；如果GSSP_PARA维护1：开
            String para1 = gaiaSdReplenishHMapper.getParamByClientAndGcspId(dto.getClient(), "PO_PZWH_CHECK");
            // 3.1 默认检查时给出过期提醒，但是可以正常保存。
            // 3.2 参数表：GAIA_CL_SYSTEM_PARA增加参数 PO_PZWH_CHECK采购订单是否强控批准文号有效期；如果GSSP_PARA维护1：开，则采购订单处直接报错控制不让保存。
            // 3.3 检查商品过期时，再增加检查一个字段 GAIA_PRODUCT_BUSINESS - PRO_SCJYXKZYXQ，过期强管控。
            // 批准文号有效期
            if (StringUtils.isNotBlank(status.getProRegisterExdate()) && status.getProRegisterExdate().trim().length() == 8) {
                if (status.getProRegisterExdate().compareTo(day) < 0) {
                    if ("1".equals(para1)) {
                        throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号已过期");
                    } else {
                        warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号已过期");
                    }
                }
            }
            // 生产经营许可证有效期
            if (StringUtils.isNotBlank(status.getProScjyxkzyxq()) && status.getProScjyxkzyxq().trim().length() == 8) {
                if (status.getProScjyxkzyxq().compareTo(day) < 0) {
                    if ("1".equals(para1)) {
                        throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "生产经营许可证已过期");
                    } else {
                        warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "生产经营许可证已过期");
                    }
                }
            }

            // 经营范围验证
            if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && !ignoreScopeCheck && gaiaDcData.getDcJyfwgk().equals("1")) {
                if (CollectionUtils.isEmpty(supScopeList)) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
                if (status == null || StringUtils.isBlank(status.getProJylb())) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
                if (Collections.disjoint(supScopeList, Arrays.asList(status.getProJylb().split(",")))) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
            }
            if (null != status && CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(status.getProStatus())) {
                throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "为停用商品不可进行采购");
            }
            //商品禁采检查
            if (null != status && CommonEnum.NoYesStatus.YES.getCode().equals(status.getProNoPurchase())) {
                throw new CustomResultException(MessageFormat.format(ResultEnum.PURCHASE_NOT_VALID.getMsg(), gaiaPoItemList.getPoProCode()));
            }
            //商品禁退检查
            if (null != status && CommonEnum.NoYesStatus.YES.getCode().equals(status.getProNoSupplier())) {
                throw new CustomResultException(ResultEnum.CUSPLIER_NOT_VALID);
            }
            //采购主体与详细地点必须相同
            if (!dto.getPoCompanyCode().equals(gaiaPoItemList.getPoSiteCode())) {
                throw new CustomResultException(ResultEnum.SITEDIFF_ERROR);
            }
        }
        log.info("错误信息：{}", JsonUtils.beanToJson(error));
        if (CollectionUtils.isNotEmpty(error)) {
            Result result = ResultUtil.error(ResultEnum.E0015);
            result.setWarning(error);
            return result;
        }

        //GAIA_PO_HEADER主键重复检查
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        BeanUtils.copyProperties(dto, gaiaPoHeaderKey);
        gaiaPoHeaderKey.setPoId(poId);

        GaiaPoHeader gaiaPoHeaderInfo = gaiaPoHeaderMapper.selectByPrimaryKey(gaiaPoHeaderKey);
        if (null != gaiaPoHeaderInfo) {
            throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
        }

        //GAIA_PO_HEADER信息插入
        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
        BeanUtils.copyProperties(dto, gaiaPoHeader);
        gaiaPoHeader.setPoId(poId);
        gaiaPoHeader.setPoCreateBy(user.getUserId());
        gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        // 检查是否在审批配置表中配置
        List<GaiaWmsShenPiPeiZhiDTO> gaiaWmsShenPiPeiZhiDTO = gaiaPoHeaderMapper.queryUserRole(user.getClient(), dto.getPoCompanyCode());
        if (CollectionUtils.isNotEmpty(gaiaWmsShenPiPeiZhiDTO)) {
            // 是否配置审批人
            if (gaiaWmsShenPiPeiZhiDTO.get(0) != null && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.get(0).getWmSprlx1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.get(0).getWmSpr1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.get(0).getWmSprxm1())) {
                gaiaPoHeader.setPoApproveStatus("1");
            } else {
                gaiaPoHeader.setPoApproveStatus("2");
            }
        } else {
            gaiaPoHeader.setPoApproveStatus("2");
        }
        GaiaGlobalGspConfigure gaiaGlobalGspConfigure = gaiaGlobalGspConfigureMapper.getCurrentClientConfigureList(user.getClient(), null, CommonEnum.GaiaGlobalGspConfigure.GSP_PO_APPROVE_STATUS.getCode())
                .stream().filter(Objects::nonNull).findFirst().orElse(null);
        if (!ObjectUtils.isEmpty(gaiaGlobalGspConfigure)
                && CommonEnum.NoYesStatus.YES.getCode().equals(gaiaGlobalGspConfigure.getGggcValue())) {
            gaiaPoHeader.setPoAuditDate(dto.getPoAuditDate());
            gaiaPoHeader.setPoDate(dto.getPoAuditDate());
        }
        GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
        supplierBusinessKey.setClient(dto.getClient());
        supplierBusinessKey.setSupSite(dto.getPoCompanyCode());
        supplierBusinessKey.setSupSelfCode(dto.getPoSupplierId());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(supplierBusinessKey);
        if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
            gaiaPoHeader.setPoLeadTime(gaiaSupplierBusiness.getSupLeadTime());
            gaiaPoHeader.setPoDeliveryMode(gaiaSupplierBusiness.getSupDeliveryMode());
            gaiaPoHeader.setPoCarrier(gaiaSupplierBusiness.getSupCarrier());
            gaiaPoHeader.setPoDeliveryTimes(gaiaSupplierBusiness.getSupDeliveryTimes());
        }
        gaiaPoHeader.setPoOwner(dto.getPoOwner());
        gaiaPoHeaderMapper.insert(gaiaPoHeader);

        //GAIA_PO_ITEM信息插入
        for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {

            //GAIA_PO_ITEM主键重复检查
            GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
            gaiaPoItemKey.setClient(dto.getClient());
            gaiaPoItemKey.setPoId(poId);
            gaiaPoItemKey.setPoLineNo(gaiaPoItemList.getPoLineNo());

            GaiaPoItem gaiaPoItemInfo = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);

            if (null != gaiaPoItemInfo) {
                throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
            }

            GaiaPoItem gaiaPoItem = new GaiaPoItem();
            BeanUtils.copyProperties(gaiaPoItemList, gaiaPoItem);

            gaiaPoItem.setClient(dto.getClient());
            gaiaPoItem.setPoId(poId);
            gaiaPoItem.setPoCompleteInvoice("0");
            gaiaPoItem.setPoCompletePay("0");
            gaiaPoItem.setPoLineDelete("0");
            gaiaPoItem.setPoCompleteFlag("0");

            if (StringUtils.isBlank(gaiaPoItemList.getPoLineNo())) {
                //订单行号查询
                String maxLineNo = gaiaPoItemMapper.selectLineNo(poId, dto.getClient());
                if (StringUtils.isBlank(maxLineNo)) {
                    gaiaPoItem.setPoLineNo("1");
                } else {
                    gaiaPoItem.setPoLineNo(String.valueOf(Integer.valueOf(maxLineNo) + 1));
                }
            } else {
                gaiaPoItem.setPoLineNo(gaiaPoItemList.getPoLineNo());
            }

            gaiaPoItemMapper.insert(gaiaPoItem);

            //当采购类型为Z001时
            if (CommonEnum.PurchaseClass.PURCHASECD.getFieldName().equals(dto.getPoType())) {

                //检查货源清单表
                GaiaSourceList gaiaSource = new GaiaSourceList();
                gaiaSource.setClient(dto.getClient());
                gaiaSource.setSouProCode(gaiaPoItemList.getPoProCode());
                gaiaSource.setSouSiteCode(gaiaPoItemList.getPoSiteCode());
                gaiaSource.setSouSupplierId(dto.getPoSupplierId());

                List<GaiaSourceList> gaiaSourceList = gaiaSourceListMapper.selectSourceList(gaiaSource);

                //若无值则插入数据
                if (CollectionUtils.isEmpty(gaiaSourceList)) {

                    //GAIA_SOURCE_LIST插入数据
                    GaiaSourceList insertSourceList = new GaiaSourceList();
                    insertSourceList.setClient(dto.getClient());
                    //货源清单编码
                    GaiaSourceList souListCodeInfo = gaiaSourceListMapper.selectNo(dto.getClient(), gaiaPoItemList.getPoProCode(), gaiaPoItemList.getPoSiteCode());

                    insertSourceList.setSouListCode(souListCodeInfo.getSouListCode());
                    insertSourceList.setSouProCode(gaiaPoItemList.getPoProCode());
                    insertSourceList.setSouSiteCode(gaiaPoItemList.getPoSiteCode());
                    insertSourceList.setSouSupplierId(dto.getPoSupplierId());
                    insertSourceList.setSouEffectFrom(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouEffectEnd("20991231");
                    insertSourceList.setSouMainSupplier("0");
                    insertSourceList.setSouLockSupplier("0");
                    insertSourceList.setSouDeleteFlag("0");
                    insertSourceList.setSouCreateType("0");
                    insertSourceList.setSouCreateBy(user.getUserId());
                    insertSourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));

                    gaiaSourceListMapper.insert(insertSourceList);
                }
            }
        }

        //当采购类型为Z001时
        if (CommonEnum.PurchaseClass.PURCHASECD.getFieldName().equals(dto.getPoType())) {
            //审批策略逻辑
            //查询是否需要审批
            GaiaApproveRule gaiaApproveRule = gaiaApproveRuleMapper.selectByPrimaryKey(dto.getClient(), dto.getPoCompanyCode());
            // 采购审批集合
            List<PurchaseFeignDto> purchaseFeignDtoList = new ArrayList<>();
            if (null != gaiaApproveRule) {
                for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {
                    Integer approvalType = 0;  // 0-不需要审批， 1-单挑调增金额触发审批， 2-单价调增比例触发审批， 3-存销比触发审批
                    // 商品编码查询
                    GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(dto.getClient(), gaiaPoItemList.getPoSiteCode(),
                            gaiaPoItemList.getPoProCode());
                    if (null == gaiaProductBusiness || StringUtils.isBlank(gaiaProductBusiness.getProCode())) {
                        throw new CustomResultException(ResultEnum.E0017);
                    }

                    GaiaProductBasic basic = gaiaProductBasicMapper.selectByPrimaryKey(gaiaProductBusiness.getProCode());

                    // 采购价格平均值获取
                    BigDecimal unitPriceList = gaiaPoItemMapper.selectUnitPrice(dto.getClient(), gaiaPoItemList.getPoSiteCode(), gaiaProductBusiness.getProCode());

                    // 采购次数获取
                    List<GaiaPoItem> saleTime = gaiaPoItemMapper.selectSaleTime(dto.getClient(), gaiaPoItemList.getPoSiteCode(), gaiaProductBusiness.getProCode());

                    BigDecimal timeNum = new BigDecimal(saleTime.size());
                    if (null == unitPriceList || unitPriceList.equals(BigDecimal.ZERO)) {
                        approvalType = 1;
                    }

                    //本次采购价格
                    BigDecimal thisPrice = gaiaPoItemList.getPoPrice();

                    //单价增调值
                    if (unitPriceList != null && (thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 0
                            || thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 1)) {
                        approvalType = 1;
                    }

                    //单价调增比例
                    if (unitPriceList != null && (thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 0
                            || thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 1)) {
                        if (approvalType == 0) {
                            approvalType = 2;
                        }
                    }

                    //总库存量
                    BigDecimal stock = BigDecimal.ZERO;

                    //30天销售总量
                    BigDecimal saleVolume = BigDecimal.ZERO;

                    //采购主体是门店
                    if (CommonEnum.SaleSubject.SUBJECTVED.getCode().equals(dto.getPoSubjectType())) {
                        saleVolume = gaiaSdSaleDMapper.selectSaleNum(dto.getClient(), gaiaPoItemList.getPoSiteCode(), gaiaPoItemList.getPoProCode());
                    } else if (CommonEnum.SaleSubject.SUBJECTVAL.getCode().equals(dto.getPoSubjectType())) {
                        saleVolume = gaiaSoItemMapper.selectTotalSale(dto.getClient(), gaiaPoItemList.getPoSiteCode());
                    }

                    // 库存
                    GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
                    // 加盟商
                    gaiaMaterialAssessKey.setClient(dto.getClient());
                    // 商品编码
                    gaiaMaterialAssessKey.setMatProCode(gaiaPoItemList.getPoProCode());
                    // 估价地点
                    gaiaMaterialAssessKey.setMatAssessSite(gaiaPoItemList.getPoSiteCode());
                    GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
                    if (gaiaMaterialAssess != null) {
                        stock = gaiaMaterialAssess.getMatTotalQty() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatTotalQty();
                    }

                    //最新存销比
                    BigDecimal rate = new BigDecimal(0);

                    //30天销售总量为0
                    if (BigDecimal.ZERO.compareTo(saleVolume) == 0) {
                        if (approvalType == 0) {
                            approvalType = 3;
                        }
//                        gaiaPoHeaderMapper.updateApproveStatus(dto.getClient(), poId);
//                        break;
                    } else {

                        rate = gaiaPoItemList.getPoQty().add(stock).divide(saleVolume, 4, RoundingMode.HALF_UP);

                        if (rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 0
                                || rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 1) {
                            if (approvalType == 0) {
                                approvalType = 3;
                            }
//                        gaiaPoHeaderMapper.updateApproveStatus(dto.getClient(), poId);
//                        break;
                        }
                    }


                    if (approvalType > 0) {
                        PurchaseFeignDto purchaseFeignDto = new PurchaseFeignDto();
                        purchaseFeignDto.setProCode(gaiaPoItemList.getPoProCode()); // 商品编码
                        purchaseFeignDto.setProName(basic.getProName());  // 商品名
                        purchaseFeignDto.setProSpecs(basic.getProSpecs()); // 规格
                        purchaseFeignDto.setWfSite(gaiaPoItemList.getPoSiteCode()); // 门店/部门/配送中心
                        if (unitPriceList != null) {
                            purchaseFeignDto.setPriceAverage(unitPriceList.setScale(2, BigDecimal.ROUND_HALF_UP).toString());  // 5次均价
                        } else {
                            purchaseFeignDto.setPriceAverage("0.00");  // 5次均价
                        }
                        purchaseFeignDto.setPriceReal(gaiaPoItemList.getPoPrice().setScale(4, BigDecimal.ROUND_DOWN).toString()); // 实际采购价
                        // 存销比
                        if (rate != null) {
                            purchaseFeignDto.setStockUseRate(rate.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                        } else {
                            purchaseFeignDto.setStockUseRate("0.00");
                        }

                        if (approvalType == 1) {  // 1-单挑调增金额触发审批，
                            if (unitPriceList != null) {
                                purchaseFeignDto.setWarnValue(gaiaPoItemList.getPoPrice().subtract(unitPriceList).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        } else if (approvalType == 2) {  // 2-单价调增比例触发审批，
                            if (unitPriceList != null) {
                                purchaseFeignDto.setWarnValue(gaiaPoItemList.getPoPrice().subtract(unitPriceList).divide(unitPriceList, 4, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        } else {  // 3-存销比触发审批
                            if (rate != null) {
                                purchaseFeignDto.setWarnValue(rate.subtract(gaiaApproveRule.getAprStockUseRate()).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        }
                        purchaseFeignDtoList.add(purchaseFeignDto);
                    }
                }

                // 需要审批 并提交审批流
                if (purchaseFeignDtoList.size() > 0) {
                    // 流程编号
                    int i = (int) ((Math.random() * 4 + 1) * 100000);
                    gaiaPoHeader.setPoFlowNo(System.currentTimeMillis() + org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(i), 6, "0"));
                    gaiaPoHeaderMapper.updateApproveStatus(dto.getClient(), poId, gaiaPoHeader.getPoFlowNo());
                    FeignResult result = feignService.createPurchaseWorkflow(gaiaPoHeader, purchaseFeignDtoList);
                    if (result.getCode() != 0) {
                        throw new CustomResultException(result.getMessage());
                    }

                }
            }
        }
        Result result = ResultUtil.success(MessageFormat.format(ResultEnum.I0101.getMsg(), poId));
        result.setWarning(warning);
        return result;
    }

    /**
     * 暂存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result temporaryStoragePurchaseOrder(CreatePurchaseOrderRequestDto dto) {
        if (StringUtils.isBlank(dto.getPoId())) {
            // 新增
            return temporaryStoragePurchaseOrderAdd(dto);
        } else {
            // 编辑
            return temporaryStoragePurchaseOrderEdit(dto);
        }
    }


    /**
     * 新增暂存
     */
    public Result temporaryStoragePurchaseOrderAdd(CreatePurchaseOrderRequestDto dto) {
        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 经营范围管控
        checkJyFw(user, dto);
        //采购凭证号
        String poId = new CommonUtils().getPoId(dto.getClient(), dto.getPoType(), DateUtils.getCurrentDateStr("yyyy"), gaiaPoHeaderMapper, redisClient);
        Result checkResult = checkParams(dto, warning, error, poId);
        if (checkResult != null) {
            return checkResult;
        }

        //GAIA_PO_HEADER信息插入
        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
        BeanUtils.copyProperties(dto, gaiaPoHeader);
        gaiaPoHeader.setPoId(poId);
        gaiaPoHeader.setPoCreateBy(user.getUserId());
        gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        gaiaPoHeader.setPoApproveStatus("-10");
        gaiaPoHeader.setPoOwner(dto.getPoOwner());

        GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
        supplierBusinessKey.setClient(dto.getClient());
        supplierBusinessKey.setSupSite(dto.getPoCompanyCode());
        supplierBusinessKey.setSupSelfCode(dto.getPoSupplierId());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(supplierBusinessKey);
        if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
            gaiaPoHeader.setPoLeadTime(gaiaSupplierBusiness.getSupLeadTime());
            gaiaPoHeader.setPoDeliveryMode(gaiaSupplierBusiness.getSupDeliveryMode());
            gaiaPoHeader.setPoCarrier(gaiaSupplierBusiness.getSupCarrier());
            gaiaPoHeader.setPoDeliveryTimes(gaiaSupplierBusiness.getSupDeliveryTimes());
        }
        gaiaPoHeaderMapper.insert(gaiaPoHeader);
        int index = 0;
        //GAIA_PO_ITEM信息插入
        for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {
            index++;
            if (StringUtils.isBlank(gaiaPoItemList.getPoProCode())) {
                throw new CustomResultException("第" + index + "行商品异常，请删除后重新添加");
            }
            String nowDate = DateUtils.getCurrentDateStrYYMMDD();
            // 1、有效期不能小于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq())) {
                if (gaiaPoItemList.getPoYxq().compareTo(nowDate) < 0) {
                    throw new CustomResultException("第" + index + "行有效期不能小于当前日期");
                }
            }
            // 2、生产日期不能大于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(nowDate) > 0) {
                    throw new CustomResultException("第" + index + "行生产日期不能大于当前日期");
                }
            }
            // 3、有效期不能比生产日期小
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq()) && StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(gaiaPoItemList.getPoYxq()) > 0) {
                    throw new CustomResultException("第" + index + "行有效期不能比生产日期小");
                }
            }
            //GAIA_PO_ITEM主键重复检查
            GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
            gaiaPoItemKey.setClient(dto.getClient());
            gaiaPoItemKey.setPoId(poId);
            gaiaPoItemKey.setPoLineNo(gaiaPoItemList.getPoLineNo());
            GaiaPoItem gaiaPoItemInfo = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);
            if (null != gaiaPoItemInfo) {
                throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
            }

            GaiaPoItem gaiaPoItem = new GaiaPoItem();
            BeanUtils.copyProperties(gaiaPoItemList, gaiaPoItem);
            gaiaPoItem.setClient(dto.getClient());
            gaiaPoItem.setPoId(poId);
            gaiaPoItem.setPoCompleteInvoice("0");
            gaiaPoItem.setPoCompletePay("0");
            gaiaPoItem.setPoLineDelete("0");
            gaiaPoItem.setPoCompleteFlag("0");
            gaiaPoItem.setPoDeliveryDate(gaiaPoItemList.getPoDeliveryDate());
            if (StringUtils.isBlank(gaiaPoItemList.getPoLineNo())) {
                //订单行号查询
                String maxLineNo = gaiaPoItemMapper.selectLineNo(poId, dto.getClient());
                if (StringUtils.isBlank(maxLineNo)) {
                    gaiaPoItem.setPoLineNo("1");
                } else {
                    gaiaPoItem.setPoLineNo(String.valueOf(Integer.valueOf(maxLineNo) + 1));
                }
            } else {
                gaiaPoItem.setPoLineNo(gaiaPoItemList.getPoLineNo());
            }
            gaiaPoItemMapper.insert(gaiaPoItem);

            //当采购类型为Z001时
            if (CommonEnum.PurchaseClass.PURCHASECD.getFieldName().equals(dto.getPoType())) {
                //检查货源清单表
                GaiaSourceList gaiaSource = new GaiaSourceList();
                gaiaSource.setClient(dto.getClient());
                gaiaSource.setSouProCode(gaiaPoItemList.getPoProCode());
                gaiaSource.setSouSiteCode(gaiaPoItemList.getPoSiteCode());
                gaiaSource.setSouSupplierId(dto.getPoSupplierId());
                List<GaiaSourceList> gaiaSourceList = gaiaSourceListMapper.selectSourceList(gaiaSource);

                //若无值则插入数据
                if (CollectionUtils.isEmpty(gaiaSourceList)) {
                    //GAIA_SOURCE_LIST插入数据
                    GaiaSourceList insertSourceList = new GaiaSourceList();
                    insertSourceList.setClient(dto.getClient());
                    //货源清单编码
                    GaiaSourceList souListCodeInfo = gaiaSourceListMapper.selectNo(dto.getClient(), gaiaPoItemList.getPoProCode(), gaiaPoItemList.getPoSiteCode());
                    insertSourceList.setSouListCode(souListCodeInfo.getSouListCode());
                    insertSourceList.setSouProCode(gaiaPoItemList.getPoProCode());
                    insertSourceList.setSouSiteCode(gaiaPoItemList.getPoSiteCode());
                    insertSourceList.setSouSupplierId(dto.getPoSupplierId());
                    insertSourceList.setSouEffectFrom(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouEffectEnd("20991231");
                    insertSourceList.setSouMainSupplier("0");
                    insertSourceList.setSouLockSupplier("0");
                    insertSourceList.setSouDeleteFlag("0");
                    insertSourceList.setSouCreateType("0");
                    insertSourceList.setSouCreateBy(user.getUserId());
                    insertSourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    gaiaSourceListMapper.insert(insertSourceList);
                }
            }
        }
        Result result = ResultUtil.success(poId);
        result.setWarning(warning);
        return result;
    }

    /**
     * 编辑暂存
     */
    public Result temporaryStoragePurchaseOrderEdit(CreatePurchaseOrderRequestDto dto) {
        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 经营范围管控
        checkJyFw(user, dto);
        // 供应商效期验证
        Result checkResult = checkParams(dto, warning, error, null);
        if (checkResult != null) {
            return checkResult;
        }
        // 删除之前暂存的数据
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        gaiaPoHeaderKey.setClient(dto.getClient());
        gaiaPoHeaderKey.setPoId(dto.getPoId());
        gaiaPoHeaderMapper.deleteByPrimaryKey(gaiaPoHeaderKey);
        //GAIA_PO_HEADER信息插入
        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
        BeanUtils.copyProperties(dto, gaiaPoHeader);
        gaiaPoHeader.setPoCreateBy(user.getUserId());
        gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        gaiaPoHeader.setPoApproveStatus("-10");

        GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
        supplierBusinessKey.setClient(dto.getClient());
        supplierBusinessKey.setSupSite(dto.getPoCompanyCode());
        supplierBusinessKey.setSupSelfCode(dto.getPoSupplierId());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(supplierBusinessKey);
        if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
            gaiaPoHeader.setPoLeadTime(gaiaSupplierBusiness.getSupLeadTime());
            gaiaPoHeader.setPoDeliveryMode(gaiaSupplierBusiness.getSupDeliveryMode());
            gaiaPoHeader.setPoCarrier(gaiaSupplierBusiness.getSupCarrier());
            gaiaPoHeader.setPoDeliveryTimes(gaiaSupplierBusiness.getSupDeliveryTimes());
        }
        gaiaPoHeaderMapper.insert(gaiaPoHeader);

        // 删除之前暂存的明细数据
        gaiaPoItemMapper.deleteByClientAndPoId(dto.getClient(), dto.getPoId());
        //GAIA_PO_ITEM信息插入
        int index = 0;
        for (GaiaPoItemList poItem : dto.getGaiaPoItemList()) {
            index++;
            if (StringUtils.isBlank(poItem.getPoProCode())) {
                throw new CustomResultException("第" + index + "行商品异常，请删除后重新添加");
            }
            GaiaPoItem gaiaPoItem = new GaiaPoItem();
            BeanUtils.copyProperties(poItem, gaiaPoItem);
            gaiaPoItem.setClient(dto.getClient());
            gaiaPoItem.setPoId(dto.getPoId());
            gaiaPoItem.setPoCompleteInvoice("0");
            gaiaPoItem.setPoCompletePay("0");
            gaiaPoItem.setPoLineDelete("0");
            gaiaPoItem.setPoCompleteFlag("0");
            gaiaPoItem.setPoDeliveryDate(poItem.getPoDeliveryDate());
            if (StringUtils.isBlank(poItem.getPoLineNo())) {
                //订单行号查询
                String maxLineNo = gaiaPoItemMapper.selectLineNo(dto.getPoId(), dto.getClient());
                if (StringUtils.isBlank(maxLineNo)) {
                    gaiaPoItem.setPoLineNo("1");
                } else {
                    gaiaPoItem.setPoLineNo(String.valueOf(Integer.valueOf(maxLineNo) + 1));
                }
            } else {
                gaiaPoItem.setPoLineNo(poItem.getPoLineNo());
            }
            gaiaPoItemMapper.insert(gaiaPoItem);
            //当采购类型为Z001时
            if (CommonEnum.PurchaseClass.PURCHASECD.getFieldName().equals(dto.getPoType())) {
                //检查货源清单表
                GaiaSourceList gaiaSource = new GaiaSourceList();
                gaiaSource.setClient(dto.getClient());
                gaiaSource.setSouProCode(poItem.getPoProCode());
                gaiaSource.setSouSiteCode(poItem.getPoSiteCode());
                gaiaSource.setSouSupplierId(dto.getPoSupplierId());
                List<GaiaSourceList> gaiaSourceList = gaiaSourceListMapper.selectSourceList(gaiaSource);
                //若无值则插入数据
                if (CollectionUtils.isEmpty(gaiaSourceList)) {

                    //GAIA_SOURCE_LIST插入数据
                    GaiaSourceList insertSourceList = new GaiaSourceList();
                    insertSourceList.setClient(dto.getClient());
                    //货源清单编码
                    GaiaSourceList souListCodeInfo = gaiaSourceListMapper.selectNo(dto.getClient(), poItem.getPoProCode(), poItem.getPoSiteCode());

                    insertSourceList.setSouListCode(souListCodeInfo.getSouListCode());
                    insertSourceList.setSouProCode(poItem.getPoProCode());
                    insertSourceList.setSouSiteCode(poItem.getPoSiteCode());
                    insertSourceList.setSouSupplierId(dto.getPoSupplierId());
                    insertSourceList.setSouEffectFrom(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouEffectEnd("20991231");
                    insertSourceList.setSouMainSupplier("0");
                    insertSourceList.setSouLockSupplier("0");
                    insertSourceList.setSouDeleteFlag("0");
                    insertSourceList.setSouCreateType("0");
                    insertSourceList.setSouCreateBy(user.getUserId());
                    insertSourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));

                    gaiaSourceListMapper.insert(insertSourceList);
                }
            }
        }
        Result result = ResultUtil.success(dto.getPoId());
        result.setWarning(warning);
        return result;
    }

    /**
     * 暂存数据保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result temporaryToSave(CreatePurchaseOrderRequestDto dto) {
        List<String> warning = new ArrayList<>();
        List<String> error = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        // 供应商效期验证
        Result checkResult = checkParams(dto, warning, error, null);
        if (checkResult != null) {
            return checkResult;
        }
        // 删除之前暂存的数据
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        gaiaPoHeaderKey.setClient(dto.getClient());
        gaiaPoHeaderKey.setPoId(dto.getPoId());
        gaiaPoHeaderMapper.deleteByPrimaryKey(gaiaPoHeaderKey);
        //GAIA_PO_HEADER信息插入
        GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
        BeanUtils.copyProperties(dto, gaiaPoHeader);
        gaiaPoHeader.setPoCreateBy(user.getUserId());
        gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
        // 检查是否在审批配置表中配置
        List<GaiaWmsShenPiPeiZhiDTO> gaiaWmsShenPiPeiZhiDTO = gaiaPoHeaderMapper.queryUserRole(user.getClient(), dto.getPoCompanyCode());
        if (CollectionUtils.isNotEmpty(gaiaWmsShenPiPeiZhiDTO)) {
            // 是否配置审批人
            if (gaiaWmsShenPiPeiZhiDTO.get(0) != null && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.get(0).getWmSprlx1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.get(0).getWmSpr1())
                    && StringUtils.isNotBlank(gaiaWmsShenPiPeiZhiDTO.get(0).getWmSprxm1())) {
                gaiaPoHeader.setPoApproveStatus("1");
            } else {
                gaiaPoHeader.setPoApproveStatus("2");
            }
        } else {
            gaiaPoHeader.setPoApproveStatus("2");
        }
        gaiaPoHeader.setPoOwner(dto.getPoOwner());
        GaiaGlobalGspConfigure gaiaGlobalGspConfigure = gaiaGlobalGspConfigureMapper.getCurrentClientConfigureList(user.getClient(), null, CommonEnum.GaiaGlobalGspConfigure.GSP_PO_APPROVE_STATUS.getCode())
                .stream().filter(Objects::nonNull).findFirst().orElse(null);
        if (!ObjectUtils.isEmpty(gaiaGlobalGspConfigure)
                && CommonEnum.NoYesStatus.YES.getCode().equals(gaiaGlobalGspConfigure.getGggcValue())) {
            gaiaPoHeader.setPoAuditDate(dto.getPoAuditDate());
            gaiaPoHeader.setPoDate(dto.getPoAuditDate());
        }

        GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
        supplierBusinessKey.setClient(dto.getClient());
        supplierBusinessKey.setSupSite(dto.getPoCompanyCode());
        supplierBusinessKey.setSupSelfCode(dto.getPoSupplierId());
        GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(supplierBusinessKey);
        if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
            gaiaPoHeader.setPoLeadTime(gaiaSupplierBusiness.getSupLeadTime());
            gaiaPoHeader.setPoDeliveryMode(gaiaSupplierBusiness.getSupDeliveryMode());
            gaiaPoHeader.setPoCarrier(gaiaSupplierBusiness.getSupCarrier());
            gaiaPoHeader.setPoDeliveryTimes(gaiaSupplierBusiness.getSupDeliveryTimes());
        }
        gaiaPoHeaderMapper.insert(gaiaPoHeader);

        // 删除之前暂存的明细数据
        gaiaPoItemMapper.deleteByClientAndPoId(dto.getClient(), dto.getPoId());
        //GAIA_PO_ITEM信息插入
        int index = 0;
        for (GaiaPoItemList poItem : dto.getGaiaPoItemList()) {
            index++;
            if (StringUtils.isBlank(poItem.getPoProCode())) {
                throw new CustomResultException("第" + index + "行商品异常，请删除后重新添加");
            }
            GaiaPoItem gaiaPoItem = new GaiaPoItem();
            BeanUtils.copyProperties(poItem, gaiaPoItem);
            gaiaPoItem.setClient(dto.getClient());
            gaiaPoItem.setPoId(dto.getPoId());
            gaiaPoItem.setPoCompleteInvoice("0");
            gaiaPoItem.setPoCompletePay("0");
            gaiaPoItem.setPoLineDelete("0");
            gaiaPoItem.setPoCompleteFlag("0");
            gaiaPoItem.setPoDeliveryDate(poItem.getPoDeliveryDate());
            if (StringUtils.isBlank(poItem.getPoLineNo())) {
                //订单行号查询
                String maxLineNo = gaiaPoItemMapper.selectLineNo(dto.getPoId(), dto.getClient());
                if (StringUtils.isBlank(maxLineNo)) {
                    gaiaPoItem.setPoLineNo("1");
                } else {
                    gaiaPoItem.setPoLineNo(String.valueOf(Integer.valueOf(maxLineNo) + 1));
                }
            } else {
                gaiaPoItem.setPoLineNo(poItem.getPoLineNo());
            }
            gaiaPoItemMapper.insert(gaiaPoItem);
            //当采购类型为Z001时
            if (CommonEnum.PurchaseClass.PURCHASECD.getFieldName().equals(dto.getPoType())) {
                //检查货源清单表
                GaiaSourceList gaiaSource = new GaiaSourceList();
                gaiaSource.setClient(dto.getClient());
                gaiaSource.setSouProCode(poItem.getPoProCode());
                gaiaSource.setSouSiteCode(poItem.getPoSiteCode());
                gaiaSource.setSouSupplierId(dto.getPoSupplierId());
                List<GaiaSourceList> gaiaSourceList = gaiaSourceListMapper.selectSourceList(gaiaSource);
                //若无值则插入数据
                if (CollectionUtils.isEmpty(gaiaSourceList)) {

                    //GAIA_SOURCE_LIST插入数据
                    GaiaSourceList insertSourceList = new GaiaSourceList();
                    insertSourceList.setClient(dto.getClient());
                    //货源清单编码
                    GaiaSourceList souListCodeInfo = gaiaSourceListMapper.selectNo(dto.getClient(), poItem.getPoProCode(), poItem.getPoSiteCode());

                    insertSourceList.setSouListCode(souListCodeInfo.getSouListCode());
                    insertSourceList.setSouProCode(poItem.getPoProCode());
                    insertSourceList.setSouSiteCode(poItem.getPoSiteCode());
                    insertSourceList.setSouSupplierId(dto.getPoSupplierId());
                    insertSourceList.setSouEffectFrom(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouEffectEnd("20991231");
                    insertSourceList.setSouMainSupplier("0");
                    insertSourceList.setSouLockSupplier("0");
                    insertSourceList.setSouDeleteFlag("0");
                    insertSourceList.setSouCreateType("0");
                    insertSourceList.setSouCreateBy(user.getUserId());
                    insertSourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));

                    gaiaSourceListMapper.insert(insertSourceList);
                }
            }
        }

        //当采购类型为Z001时
        if (CommonEnum.PurchaseClass.PURCHASECD.getFieldName().equals(dto.getPoType())) {
            //审批策略逻辑
            //查询是否需要审批
            GaiaApproveRule gaiaApproveRule = gaiaApproveRuleMapper.selectByPrimaryKey(dto.getClient(), dto.getPoCompanyCode());
            // 采购审批集合
            List<PurchaseFeignDto> purchaseFeignDtoList = new ArrayList<>();
            if (null != gaiaApproveRule) {
                for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {
                    Integer approvalType = 0;  // 0-不需要审批， 1-单挑调增金额触发审批， 2-单价调增比例触发审批， 3-存销比触发审批
                    // 商品编码查询
                    GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessMapper.getProductBusiness(dto.getClient(), gaiaPoItemList.getPoSiteCode(),
                            gaiaPoItemList.getPoProCode());
                    if (null == gaiaProductBusiness || StringUtils.isBlank(gaiaProductBusiness.getProCode())) {
                        throw new CustomResultException(ResultEnum.E0017);
                    }

                    GaiaProductBasic basic = gaiaProductBasicMapper.selectByPrimaryKey(gaiaProductBusiness.getProCode());

                    // 采购价格平均值获取
                    BigDecimal unitPriceList = gaiaPoItemMapper.selectUnitPrice(dto.getClient(), gaiaPoItemList.getPoSiteCode(), gaiaProductBusiness.getProCode());

                    // 采购次数获取
                    List<GaiaPoItem> saleTime = gaiaPoItemMapper.selectSaleTime(dto.getClient(), gaiaPoItemList.getPoSiteCode(), gaiaProductBusiness.getProCode());

                    BigDecimal timeNum = new BigDecimal(saleTime.size());
                    if (null == unitPriceList || unitPriceList.equals(BigDecimal.ZERO)) {
                        approvalType = 1;
                    }

                    //本次采购价格
                    BigDecimal thisPrice = gaiaPoItemList.getPoPrice();

                    //单价增调值
                    if (unitPriceList != null && (thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 0
                            || thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 1)) {
                        approvalType = 1;
                    }

                    //单价调增比例
                    if (unitPriceList != null && (thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 0
                            || thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 1)) {
                        if (approvalType == 0) {
                            approvalType = 2;
                        }
                    }

                    //总库存量
                    BigDecimal stock = BigDecimal.ZERO;

                    //30天销售总量
                    BigDecimal saleVolume = BigDecimal.ZERO;

                    //采购主体是门店
                    if (CommonEnum.SaleSubject.SUBJECTVED.getCode().equals(dto.getPoSubjectType())) {
                        saleVolume = gaiaSdSaleDMapper.selectSaleNum(dto.getClient(), gaiaPoItemList.getPoSiteCode(), gaiaPoItemList.getPoProCode());
                    } else if (CommonEnum.SaleSubject.SUBJECTVAL.getCode().equals(dto.getPoSubjectType())) {
                        saleVolume = gaiaSoItemMapper.selectTotalSale(dto.getClient(), gaiaPoItemList.getPoSiteCode());
                    }

                    // 库存
                    GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
                    // 加盟商
                    gaiaMaterialAssessKey.setClient(dto.getClient());
                    // 商品编码
                    gaiaMaterialAssessKey.setMatProCode(gaiaPoItemList.getPoProCode());
                    // 估价地点
                    gaiaMaterialAssessKey.setMatAssessSite(gaiaPoItemList.getPoSiteCode());
                    GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
                    if (gaiaMaterialAssess != null) {
                        stock = gaiaMaterialAssess.getMatTotalQty() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatTotalQty();
                    }

                    //最新存销比
                    BigDecimal rate = new BigDecimal(0);

                    //30天销售总量为0
                    if (BigDecimal.ZERO.compareTo(saleVolume) == 0) {
                        if (approvalType == 0) {
                            approvalType = 3;
                        }
//                        gaiaPoHeaderMapper.updateApproveStatus(dto.getClient(), poId);
//                        break;
                    } else {

                        rate = gaiaPoItemList.getPoQty().add(stock).divide(saleVolume, 4, RoundingMode.HALF_UP);

                        if (rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 0
                                || rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 1) {
                            if (approvalType == 0) {
                                approvalType = 3;
                            }
//                        gaiaPoHeaderMapper.updateApproveStatus(dto.getClient(), poId);
//                        break;
                        }
                    }


                    if (approvalType > 0) {
                        PurchaseFeignDto purchaseFeignDto = new PurchaseFeignDto();
                        purchaseFeignDto.setProCode(gaiaPoItemList.getPoProCode()); // 商品编码
                        purchaseFeignDto.setProName(basic.getProName());  // 商品名
                        purchaseFeignDto.setProSpecs(basic.getProSpecs()); // 规格
                        purchaseFeignDto.setWfSite(gaiaPoItemList.getPoSiteCode()); // 门店/部门/配送中心
                        if (unitPriceList != null) {
                            purchaseFeignDto.setPriceAverage(unitPriceList.setScale(2, BigDecimal.ROUND_HALF_UP).toString());  // 5次均价
                        } else {
                            purchaseFeignDto.setPriceAverage("0.00");  // 5次均价
                        }
                        purchaseFeignDto.setPriceReal(gaiaPoItemList.getPoPrice().setScale(4, BigDecimal.ROUND_DOWN).toString()); // 实际采购价
                        // 存销比
                        if (rate != null) {
                            purchaseFeignDto.setStockUseRate(rate.setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                        } else {
                            purchaseFeignDto.setStockUseRate("0.00");
                        }

                        if (approvalType == 1) {  // 1-单挑调增金额触发审批，
                            if (unitPriceList != null) {
                                purchaseFeignDto.setWarnValue(gaiaPoItemList.getPoPrice().subtract(unitPriceList).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        } else if (approvalType == 2) {  // 2-单价调增比例触发审批，
                            if (unitPriceList != null) {
                                purchaseFeignDto.setWarnValue(gaiaPoItemList.getPoPrice().subtract(unitPriceList).divide(unitPriceList, 4, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        } else {  // 3-存销比触发审批
                            if (rate != null) {
                                purchaseFeignDto.setWarnValue(rate.subtract(gaiaApproveRule.getAprStockUseRate()).setScale(2, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        }
                        purchaseFeignDtoList.add(purchaseFeignDto);
                    }
                }

                // 需要审批 并提交审批流
                if (purchaseFeignDtoList.size() > 0) {
                    // 流程编号
                    int i = (int) ((Math.random() * 4 + 1) * 100000);
                    gaiaPoHeader.setPoFlowNo(System.currentTimeMillis() + org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(i), 6, "0"));
                    gaiaPoHeaderMapper.updateApproveStatus(dto.getClient(), dto.getPoId(), gaiaPoHeader.getPoFlowNo());
                    FeignResult result = feignService.createPurchaseWorkflow(gaiaPoHeader, purchaseFeignDtoList);
                    if (result.getCode() != 0) {
                        throw new CustomResultException(result.getMessage());
                    }

                }
            }
        }

        Result result = ResultUtil.success(dto.getPoId());
        result.setWarning(warning);
        return result;
    }

    @Override
    public void checkForJyFw(CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto) {
        TokenUser user = feignService.getLoginInfo();
        checkJyFw(user, createPurchaseOrderRequestDto);
    }

    @Override
    public PageInfo<GetProductListResponseDto> getProductListPage(GetProductListRequestDto dto) {
        //地点检查
        if (dto.getProSite().size() == 0) {
            throw new CustomResultException(ResultEnum.SITE_ERROR);
        }
        //商品列表查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<GetProductListResponseDto> list = gaiaProductBasicMapper.selectProductList(dto);
        PageInfo<GetProductListResponseDto> pageInfo = new PageInfo<>(list);
        String client = feignService.getLoginInfo().getClient();
        List<String> proSelfCodeList = pageInfo.getList().stream().map(GetProductListResponseDto::getProSelfCode).collect(Collectors.toList());
        List<DcReplenishmentListResponseDto> saleList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(proSelfCodeList) && (dto.getIsPurchase() == null || dto.getIsPurchase().intValue() != 1)) {
            saleList = gaiaProductBasicMapper.selectSaleProByPro(client, proSelfCodeList);
        }
        for (GetProductListResponseDto item : pageInfo.getList()) {
            item.setSales7(0);
            item.setSales30(0);
            // 7
            DcReplenishmentListResponseDto seven = saleList.stream().filter(t ->
                    t.getType().equals("7") && item.getProSelfCode().equals(t.getGssdProId()) && item.getProSite().equals(t.getGssdBrId())
            ).findFirst().orElse(null);
            if (seven != null && seven.getGssdQty() != null) {
                item.setSales7(seven.getGssdQty().intValue());
            }
            // 30
            DcReplenishmentListResponseDto thirty = saleList.stream().filter(t ->
                    t.getType().equals("30") && item.getProSelfCode().equals(t.getGssdProId()) && item.getProSite().equals(t.getGssdBrId())
            ).findFirst().orElse(null);
            if (thirty != null && thirty.getGssdQty() != null) {
                item.setSales30(thirty.getGssdQty().intValue());
            }
        }
        return pageInfo;
    }

    /**
     * 暂存明细
     *
     * @param poId
     */
    @Override
    public PoItemResponseDto temporaryDetails(String poId) {
        String client = feignService.getLoginInfo().getClient();
        String userId = feignService.getLoginInfo().getUserId();
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        gaiaPoHeaderKey.setPoId(poId);
        gaiaPoHeaderKey.setClient(client);
        GaiaPoHeader gaiaPoHeader = gaiaPoHeaderMapper.selectByPrimaryKey(gaiaPoHeaderKey);
        if (ObjectUtils.isEmpty(gaiaPoHeader)) {
            throw new CustomResultException("该暂存已经被删除");
        }
        if ("0".equals(gaiaPoHeader.getPoApproveStatus())) {
            throw new CustomResultException("该暂存已经被保存");
        }

        //暂存订单查询
        List<PoItemResponseDto> poHeaderList = gaiaPoHeaderMapper.temporaryDetails(client, userId, poId);

        //头部信息设置
        PoItemResponseDto poHeader = new PoItemResponseDto();
        if (null != poHeaderList && poHeaderList.size() > 0) {
            BeanUtils.copyProperties(poHeaderList.get(0), poHeader);
        } else {
            return null;
        }
        BigDecimal total = BigDecimal.ZERO;
        //明细部设置
        List<PoItemListDto> poItemList = gaiaPoHeaderMapper.temporaryPoItemDetails(poHeader.getClient(), poHeader.getPoId());
        poHeader.setPoItemListDto(poItemList);
        if (CollectionUtils.isNotEmpty(poItemList)) {
            List<Map<String, String>> params = new ArrayList<>();
            for (PoItemListDto poItem : poItemList) {
                if (!"1".equals(poItem.getPoLineDelete())) {
                    if (poItem != null && poItem.getPoLineTotalaMount() != null) {
                        total = total.add(poItem.getPoLineTotalaMount());
                    }
                }
                Map<String, String> map = new HashMap<>();
                // 地点
                map.put("proSite", poHeader.getPoCompanyCode());
                // 商品
                map.put("proSelfCode", poItem.getPoProCode());
                params.add(map);
            }
            // 商品集合
            GetProductListRequestDto getProductListRequestDto = new GetProductListRequestDto();
            // 加盟商
            getProductListRequestDto.setClient(client);
            getProductListRequestDto.setParams(params);
            List<GetProductListResponseDto> productListResponseDtoList = gaiaProductBasicMapper.selectProductList(getProductListRequestDto);
            for (PoItemListDto poItem : poItemList) {
                List<GetProductListResponseDto> rowList = productListResponseDtoList.stream().filter(
                        item -> item.getProSite().equals(poHeader.getPoCompanyCode()) && item.getProSelfCode().equals(poItem.getPoProCode())
                ).collect(Collectors.toList());
                poItem.setProductList(rowList);
            }
        }
        poHeader.setPoTotalAmount(total);
        return poHeader;
    }

    /**
     * 暂存列表
     */
    @Override
    public List<PoHeaderListResponseDto> temporaryList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaPoHeaderMapper.temporaryList(user.getClient(), user.getUserId());
    }

    /**
     * 暂存删除
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void temporaryRemove(String poId) {
        String client = feignService.getLoginInfo().getClient();
        String userId = feignService.getLoginInfo().getUserId();
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        gaiaPoHeaderKey.setPoId(poId);
        gaiaPoHeaderKey.setClient(client);
        GaiaPoHeader gaiaPoHeader = gaiaPoHeaderMapper.selectByPrimaryKey(gaiaPoHeaderKey);
        if (ObjectUtils.isEmpty(gaiaPoHeader)) {
            throw new CustomResultException("该暂存已经被删除");
        }
        if ("0".equals(gaiaPoHeader.getPoApproveStatus())) {
            throw new CustomResultException("该暂存已经被保存");
        }
        gaiaPoHeaderMapper.deleteByPrimaryKey(gaiaPoHeaderKey);
        gaiaPoItemMapper.deleteByClientAndPoId(client, poId);
    }

    /**
     * 商品列表_弹出框
     */
    @Override
    public PageInfo<GetProductListResponseDto> getProductListForAdjustmentPopup(GetProductListRequestDto dto) {
        //地点检查
        if (dto.getProSite().size() == 0) {
            throw new CustomResultException(ResultEnum.SITE_ERROR);
        }
        // 当前用户
        TokenUser user = feignService.getLoginInfo();
        //商品列表查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<GetProductListResponseDto> list = gaiaProductBasicMapper.getProductListForAdjustmentPopup(dto);
        PageInfo<GetProductListResponseDto> pageInfo = new PageInfo<>(list);
        List<GetProductListResponseDto> resultList = pageInfo.getList();
        List<String> proCodeList = resultList.stream().map(GetProductListResponseDto::getLabel).collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(proCodeList)) {
            List<GetProductListResponseDto> batchList = gaiaProductBasicMapper.getBatchListByProCOde(dto.getClient(), dto.getSupCode(), proCodeList);
            if (CollectionUtils.isNotEmpty(batchList)) {
                pageInfo.getList().forEach(item -> {
                    String proCode = item.getLabel();
                    GetProductListResponseDto getProductListResponseDto = batchList.stream().filter(t -> proCode.equals(t.getLabel())).findFirst().orElse(null);
                    if (getProductListResponseDto != null) {
                        item.setPoPrice(getProductListResponseDto.getPoPrice());
                    }
                });
            }
        }
        return pageInfo;
    }

    /**
     * 采购订单删除
     *
     * @param poId
     */
    @Override
    public void delPurchase(String poId) {
        // 当前用户
        TokenUser user = feignService.getLoginInfo();
        // 后续单据判断：
        // GAIA_SD_ACCEPT_H，CLIENT + GSAH_POID没有值。
        // GAIA_WMS_RKSH_Z，CLIENT + WM_CGDDH没有值。
        // GAIA_MATERIAL_DOC，CLIENT + PO_ID没有值。
        Integer poCnt = gaiaPoHeaderMapper.selectFollow(user.getClient(), poId);
        if (poCnt.intValue() > 0) {
            throw new CustomResultException("当前采购订单不可以删除");
        }
        // 采购订单删除
        GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
        gaiaPoHeaderKey.setClient(user.getClient());
        gaiaPoHeaderKey.setPoId(poId);
        gaiaPoHeaderMapper.deleteByPrimaryKey(gaiaPoHeaderKey);
        gaiaPoItemMapper.deleteByClientAndPoId(user.getClient(), poId);
    }

    @Override
    public Result getPoFollow(String poId) {
        // 当前用户
        TokenUser user = feignService.getLoginInfo();
        // 后续单据判断：
        // GAIA_SD_ACCEPT_H，CLIENT + GSAH_POID没有值。
        // GAIA_WMS_RKSH_Z，CLIENT + WM_CGDDH没有值。
        // GAIA_MATERIAL_DOC，CLIENT + PO_ID没有值。
        Integer poCnt = gaiaPoHeaderMapper.selectFollow(user.getClient(), poId);
        if (poCnt.intValue() > 0) {
            return ResultUtil.success(1);
        }
        return ResultUtil.success(0);
    }

    /**
     * 历史采购记录
     *
     * @param proSelfCode
     * @param pageSize
     * @param pageNum
     * @return
     */
    @Override
    public Result selectHisPo(String proSelfCode, Integer pageSize, Integer pageNum) {
        PageHelper.startPage(pageNum, pageSize);
        TokenUser user = feignService.getLoginInfo();
        List<BatchInfoDto> batchInfoDtoList = gaiaPoHeaderMapper.selectHisPo(user.getClient(), proSelfCode);
        PageInfo pageInfo = new PageInfo<BatchInfoDto>(batchInfoDtoList);
        return ResultUtil.success(pageInfo);
    }

    /**
     * 供应预付附件生成
     *
     * @param supplierPaymentAtt
     */
    @Override
    public void supplierPaymentAtt(SupplierPaymentAtt supplierPaymentAtt) throws IOException {
        // 打印模板
        String template = "supplierPay/TEMPLATE001.xlsx";
        Workbook workbook = getWorkbook(template);
        // 样式
        CellStyle cellStyle = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontHeightInPoints((short) 10);
        cellStyle.setFont(font);
        int templateRowIndex = 8;
        // 获取第一个张表
        Sheet sheet = workbook.getSheetAt(0);
        // 申请日期
        ExcelUtils.setVal(sheet, 3, 9, DateUtils.getCurrentDateStr(), cellStyle);
        // 付款单号
        ExcelUtils.setVal(sheet, 4, 9, supplierPaymentAtt.getPayOrderId(), cellStyle);
        // 供应商编码
        ExcelUtils.setVal(sheet, 4, 35, supplierPaymentAtt.getSupSelfCode(), cellStyle);
        // 供应商名称
        ExcelUtils.setVal(sheet, 4, 57, supplierPaymentAtt.getSupName(), cellStyle);
        // 开户行名称
        ExcelUtils.setVal(sheet, 5, 10, supplierPaymentAtt.getSupBankName(), cellStyle);
        // 开户行账号
        ExcelUtils.setVal(sheet, 5, 35, supplierPaymentAtt.getSupBankAccount(), cellStyle);
        // 开户行行号
        ExcelUtils.setVal(sheet, 5, 57, supplierPaymentAtt.getSupBankCode(), cellStyle);

        PoHeaderListRequestDto poHeaderListRequestDto = new PoHeaderListRequestDto();
        poHeaderListRequestDto.setClient(supplierPaymentAtt.getClient());
        poHeaderListRequestDto.setPoIds(supplierPaymentAtt.getPoIds());
        poHeaderListRequestDto.setPoLineDelete("0");
        List<PoHeaderListResponseDto> poHeaderListResponseDtoList = gaiaPoHeaderMapper.selectPoHeaderList(poHeaderListRequestDto);
        BigDecimal poQty = BigDecimal.ZERO;
        BigDecimal poLineTotalaMount = BigDecimal.ZERO;
        for (int i = 0; i < poHeaderListResponseDtoList.size(); i++) {
            PoHeaderListResponseDto poHeaderListResponseDto = poHeaderListResponseDtoList.get(i);
            int rowIndex = templateRowIndex + i;
            {
                // 序号
                ExcelUtils.setVal(sheet, rowIndex, 0, String.valueOf(i + 1), cellStyle);
                CellRangeAddress cellRangeAddress1 = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
                ExcelUtils.setBorder(cellRangeAddress1, sheet);
                sheet.addMergedRegion(cellRangeAddress1);
                // 单据编号
                ExcelUtils.setVal(sheet, rowIndex, 3, poHeaderListResponseDto.getPoId(), cellStyle);
                CellRangeAddress cellRangeAddress2 = new CellRangeAddress(rowIndex, rowIndex, 3, 9);
                ExcelUtils.setBorder(cellRangeAddress2, sheet);
                sheet.addMergedRegion(cellRangeAddress2);
                // 商品编码
                ExcelUtils.setVal(sheet, rowIndex, 10, poHeaderListResponseDto.getPoProCode(), cellStyle);
                CellRangeAddress cellRangeAddress3 = new CellRangeAddress(rowIndex, rowIndex, 10, 16);
                ExcelUtils.setBorder(cellRangeAddress3, sheet);
                sheet.addMergedRegion(cellRangeAddress3);
                // 商品名称
                ExcelUtils.setVal(sheet, rowIndex, 17, poHeaderListResponseDto.getPoProName(), cellStyle);
                CellRangeAddress cellRangeAddress4 = new CellRangeAddress(rowIndex, rowIndex, 17, 39);
                ExcelUtils.setBorder(cellRangeAddress4, sheet);
                sheet.addMergedRegion(cellRangeAddress4);
                // 生产厂家
                ExcelUtils.setVal(sheet, rowIndex, 40, poHeaderListResponseDto.getProFactoryName(), cellStyle);
                CellRangeAddress cellRangeAddress5 = new CellRangeAddress(rowIndex, rowIndex, 40, 53);
                ExcelUtils.setBorder(cellRangeAddress5, sheet);
                sheet.addMergedRegion(cellRangeAddress5);
                // 规格
                ExcelUtils.setVal(sheet, rowIndex, 54, poHeaderListResponseDto.getProSpecs(), cellStyle);
                CellRangeAddress cellRangeAddress6 = new CellRangeAddress(rowIndex, rowIndex, 54, 61);
                ExcelUtils.setBorder(cellRangeAddress6, sheet);
                sheet.addMergedRegion(cellRangeAddress6);
                // 数量
                if (poHeaderListResponseDto.getPoQty() != null) {
                    poQty = poQty.add(poHeaderListResponseDto.getPoQty());
                }
                ExcelUtils.setVal(sheet, rowIndex, 62, poHeaderListResponseDto.getPoQty() == null ? "" : String.valueOf(poHeaderListResponseDto.getPoQty()), cellStyle);
                CellRangeAddress cellRangeAddress7 = new CellRangeAddress(rowIndex, rowIndex, 62, 66);
                ExcelUtils.setBorder(cellRangeAddress7, sheet);
                sheet.addMergedRegion(cellRangeAddress7);
                // 单价
                ExcelUtils.setVal(sheet, rowIndex, 67, poHeaderListResponseDto.getPoPrice() == null ? "" : String.valueOf(poHeaderListResponseDto.getPoPrice()), cellStyle);
                CellRangeAddress cellRangeAddress8 = new CellRangeAddress(rowIndex, rowIndex, 67, 71);
                ExcelUtils.setBorder(cellRangeAddress8, sheet);
                sheet.addMergedRegion(cellRangeAddress8);
                // 金额
                if (poHeaderListResponseDto.getPoLineTotalaMount() != null) {
                    poLineTotalaMount = poLineTotalaMount.add(poHeaderListResponseDto.getPoLineTotalaMount());
                }
                ExcelUtils.setVal(sheet, rowIndex, 72, poHeaderListResponseDto.getPoLineTotalaMount() == null ? "" : String.valueOf(poHeaderListResponseDto.getPoLineTotalaMount()), cellStyle);
                CellRangeAddress cellRangeAddress9 = new CellRangeAddress(rowIndex, rowIndex, 72, 77);
                ExcelUtils.setBorder(cellRangeAddress9, sheet);
                sheet.addMergedRegion(cellRangeAddress9);
            }
            // 合计
            if (i == poHeaderListResponseDtoList.size() - 1) {
                rowIndex++;
                // 序号
                ExcelUtils.setVal(sheet, rowIndex, 0, "合计", cellStyle);
                CellRangeAddress cellRangeAddress1 = new CellRangeAddress(rowIndex, rowIndex, 0, 2);
                ExcelUtils.setBorder(cellRangeAddress1, sheet);
                sheet.addMergedRegion(cellRangeAddress1);
                // 单据编号
                ExcelUtils.setVal(sheet, rowIndex, 3, "", cellStyle);
                CellRangeAddress cellRangeAddress2 = new CellRangeAddress(rowIndex, rowIndex, 3, 9);
                ExcelUtils.setBorder(cellRangeAddress2, sheet);
                sheet.addMergedRegion(cellRangeAddress2);
                // 商品编码
                ExcelUtils.setVal(sheet, rowIndex, 10, "", cellStyle);
                CellRangeAddress cellRangeAddress3 = new CellRangeAddress(rowIndex, rowIndex, 10, 16);
                ExcelUtils.setBorder(cellRangeAddress3, sheet);
                sheet.addMergedRegion(cellRangeAddress3);
                // 商品名称
                ExcelUtils.setVal(sheet, rowIndex, 17, "", cellStyle);
                CellRangeAddress cellRangeAddress4 = new CellRangeAddress(rowIndex, rowIndex, 17, 39);
                ExcelUtils.setBorder(cellRangeAddress4, sheet);
                sheet.addMergedRegion(cellRangeAddress4);
                // 生产厂家
                ExcelUtils.setVal(sheet, rowIndex, 40, "", cellStyle);
                CellRangeAddress cellRangeAddress5 = new CellRangeAddress(rowIndex, rowIndex, 40, 53);
                ExcelUtils.setBorder(cellRangeAddress5, sheet);
                sheet.addMergedRegion(cellRangeAddress5);
                // 规格
                ExcelUtils.setVal(sheet, rowIndex, 54, "", cellStyle);
                CellRangeAddress cellRangeAddress6 = new CellRangeAddress(rowIndex, rowIndex, 54, 61);
                ExcelUtils.setBorder(cellRangeAddress6, sheet);
                sheet.addMergedRegion(cellRangeAddress6);
                // 数量
                ExcelUtils.setVal(sheet, rowIndex, 62, String.valueOf(poQty), cellStyle);
                CellRangeAddress cellRangeAddress7 = new CellRangeAddress(rowIndex, rowIndex, 62, 66);
                ExcelUtils.setBorder(cellRangeAddress7, sheet);
                sheet.addMergedRegion(cellRangeAddress7);
                // 单价
                ExcelUtils.setVal(sheet, rowIndex, 67, "", cellStyle);
                CellRangeAddress cellRangeAddress8 = new CellRangeAddress(rowIndex, rowIndex, 67, 71);
                ExcelUtils.setBorder(cellRangeAddress8, sheet);
                sheet.addMergedRegion(cellRangeAddress8);
                // 金额
                ExcelUtils.setVal(sheet, rowIndex, 72, String.valueOf(poLineTotalaMount), cellStyle);
                CellRangeAddress cellRangeAddress9 = new CellRangeAddress(rowIndex, rowIndex, 72, 77);
                ExcelUtils.setBorder(cellRangeAddress9, sheet);
                sheet.addMergedRegion(cellRangeAddress9);
            }
        }
        String path = String.valueOf(System.currentTimeMillis()).concat(RandomStringUtils.randomNumeric(3)).concat(".xlsx");
        Result result = cosUtils.uploadWorkbook(workbook, "supplierPay/", path);
        if (!ResultUtil.hasError(result)) {
            dictionaryMapper.setSupplierPayAttr("supplierPay/" + path, path, supplierPaymentAtt.getClient(), supplierPaymentAtt.getPayOrderId());
        }
    }

    private Workbook getWorkbook(String path) throws IOException {
        InputStream input = null;
        try {
            // 文件全路径
            String url = cosUtils.urlAuth(path);
            String type = url.substring(url.lastIndexOf(".") + 1, url.indexOf("?"));
            Workbook wb;
            //根据文件后缀（xls/xlsx）进行判断
            input = new URL(url).openStream();
            if ("xls".equals(type)) {
                //文件流对象
                wb = new HSSFWorkbook(input);
            } else if ("xlsx".equals(type)) {
                wb = new XSSFWorkbook(input);
            } else {
                throw new CustomResultException("生成附件异常");
            }
            return wb;
        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException("生成附件异常");
        } finally {
            if (input != null) {
                input.close();
            }
        }
    }

    private Result checkParams(CreatePurchaseOrderRequestDto dto, List<String> warning, List<String> error, String poId) {
        // 供应商效期验证
        GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
        gaiaDcDataKey.setClient(dto.getClient());
        gaiaDcDataKey.setDcCode(dto.getPoCompanyCode());
        GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
        // 是否管控证照效期：1-是
        if (StringUtils.isNotBlank(dto.getPoSupplierSalesman()) && gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
            String wtsxq = salesmanMaintainProMapper.queryGssWtsxq(dto.getClient(), dto.getPoCompanyCode(), dto.getPoSupplierId(), dto.getPoSupplierSalesman());
            if (StringUtils.isNotBlank(wtsxq) && wtsxq.trim().length() == 8) {
                // 业务员的委托书有效期
                String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                // 异常
                if (day.compareTo(wtsxq) > 0) {
                    error.add("业务员授权委托书效期已过期");
                }
            }
        }
        supplierCheck(gaiaDcData, dto.getClient(), dto.getPoCompanyCode(), dto.getPoSupplierId(), warning, error);
        // 供应商经营范围
        List<String> supScopeList = businessScopeServiceMapper.getsupScopeList(dto.getClient(), dto.getPoSupplierId(), 1, dto.getPoCompanyCode());
        boolean ignoreScopeCheck = false;
        if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && "2".equals(gaiaDcData.getDcJyfwgk())) {
            List<String> class3List = businessScopeServiceMapper.getsupScopeListClass3(dto.getClient(), dto.getPoSupplierId(), 1, dto.getPoCompanyCode());
            if (CollectionUtils.isNotEmpty(class3List)) {
                ignoreScopeCheck = true;
            }
        }

        int index = 0;
        for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {
            index++;
            if (StringUtils.isBlank(gaiaPoItemList.getPoProCode())) {
                throw new CustomResultException("第" + index + "行商品异常，请删除后重新添加");
            }
            String nowDate = DateUtils.getCurrentDateStrYYMMDD();
            // 1、有效期不能小于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq())) {
                if (gaiaPoItemList.getPoYxq().compareTo(nowDate) < 0) {
                    throw new CustomResultException("第" + index + "行有效期不能小于当前日期");
                }
            }
            // 2、生产日期不能大于当前日期
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(nowDate) > 0) {
                    throw new CustomResultException("第" + index + "行生产日期不能大于当前日期");
                }
            }
            // 3、有效期不能比生产日期小
            if (StringUtils.isNotBlank(gaiaPoItemList.getPoYxq()) && StringUtils.isNotBlank(gaiaPoItemList.getPoScrq())) {
                if (gaiaPoItemList.getPoScrq().compareTo(gaiaPoItemList.getPoYxq()) > 0) {
                    throw new CustomResultException("第" + index + "行有效期不能比生产日期小");
                }
            }

            //商品状态检查
            GaiaProductBusiness status = gaiaProductBusinessMapper.selectGoodsStatus(dto.getClient(), gaiaPoItemList.getPoProCode(), gaiaPoItemList.getPoSiteCode());
            if (status == null) {
                continue;
            }
            // 检查 商品主数据 注册证效期（批准文号有效期）：小于60天-提示，不报错；过期-提示。
            if (gaiaDcData != null) {
                // 是否管控证照效期：1-是
                if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1")) {
                    if (StringUtils.isNotBlank(status.getProRegisterExdate()) && status.getProRegisterExdate().trim().length() == 8) {
                        String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                        // 警告
                        Long datediff = DateUtils.datediff(day, status.getProRegisterExdate());
                        if (datediff.intValue() >= 0 && datediff.intValue() < 60) {
                            warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号即将过期");
                        }
                        if (datediff.intValue() < 0) {
                            warning.add("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "批准文号已过期");
                        }
                    }
                }
            }

            // 经营范围验证
            if (gaiaDcData != null && StringUtils.isNotBlank(gaiaDcData.getDcJyfwgk()) && !ignoreScopeCheck && gaiaDcData.getDcJyfwgk().equals("1")) {
                if (CollectionUtils.isEmpty(supScopeList)) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
                if (status == null || StringUtils.isBlank(status.getProJylb())) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
                if (Collections.disjoint(supScopeList, Arrays.asList(status.getProJylb().split(",")))) {
                    throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "超出供应商经营范围");
                }
            }

            if (null != status && CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(status.getProStatus())) {
                throw new CustomResultException("第" + index + "行商品" + gaiaPoItemList.getPoProCode() + "为停用商品不可进行采购");
            }
            //商品禁采检查
            if (null != status && CommonEnum.NoYesStatus.YES.getCode().equals(status.getProNoPurchase())) {
                throw new CustomResultException(MessageFormat.format(ResultEnum.PURCHASE_NOT_VALID.getMsg(), gaiaPoItemList.getPoProCode()));
            }
            //商品禁退检查
            if (null != status && CommonEnum.NoYesStatus.YES.getCode().equals(status.getProNoSupplier())) {
                throw new CustomResultException(ResultEnum.CUSPLIER_NOT_VALID);
            }
            //采购主体与详细地点必须相同
            if (!dto.getPoCompanyCode().equals(gaiaPoItemList.getPoSiteCode())) {
                throw new CustomResultException(ResultEnum.SITEDIFF_ERROR);
            }
        }
        if (CollectionUtils.isNotEmpty(error)) {
            Result result = ResultUtil.error(ResultEnum.E0015);
            result.setWarning(error);
            return result;
        }

        //GAIA_PO_HEADER主键重复检查
        if (StringUtils.isNotBlank(poId)) {
            GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
            BeanUtils.copyProperties(dto, gaiaPoHeaderKey);
            gaiaPoHeaderKey.setPoId(poId);
            GaiaPoHeader gaiaPoHeaderInfo = gaiaPoHeaderMapper.selectByPrimaryKey(gaiaPoHeaderKey);
            if (null != gaiaPoHeaderInfo) {
                throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
            }
        }
        return null;
    }

    /**
     * 供应商列表
     *
     * @param dto SupplierListForPurchaseRequestDto
     * @return List<SupplierListForPurchaseResponseDto>
     */
    public List<SupplierListForPurchaseResponseDto> selectSupplierListForPurchaseo(SupplierListForPurchaseRequestDto dto) {
        String client = feignService.getLoginInfo().getClient();
        dto.setClient(client);
        List<SupplierListForPurchaseResponseDto> resultList = new ArrayList<SupplierListForPurchaseResponseDto>();
        //当订单类型为调拨订单（Z003-Z006）以外时
        List<SupplierListReturnDto> supplierList = gaiaSupplierBasicMapper.selectSupplierList(dto);
        //设定返回值
        for (SupplierListReturnDto returnDate : supplierList) {
            SupplierListForPurchaseResponseDto result = new SupplierListForPurchaseResponseDto();
            result.setValue(returnDate.getSupSelfCode() + " " + returnDate.getSupName() + " " + returnDate.getSupCreditCode() + " " + returnDate.getSupPym());
            result.setLabel(returnDate.getSupSelfCode());
            result.setSupPayTerm(returnDate.getSupPayTerm());
            result.setSupLeadTime(returnDate.getSupLeadTime());
            result.setSupNoPurchase(returnDate.getSupNoPurchase());
            resultList.add(result);
        }
        return resultList;
    }


    /**
     * 供应商列表
     *
     * @param dto SupplierListForPurchaseRequestDto
     * @return List<SupplierListForPurchaseResponseDto>
     */
    @Override
    public List<SupplierListForPurchaseResponseDto> resultGetSupplierListAndDCForPurchase(SupplierListForPurchaseRequestDto dto) {
        String client = feignService.getLoginInfo().getClient();
        dto.setClient(client);
        List<SupplierListForPurchaseResponseDto> resultList = new ArrayList<SupplierListForPurchaseResponseDto>();
        List<GaiaDcData> dcList = gaiaDcDataMapper.getDcList(client);
        for (GaiaDcData dcData : dcList) {
            SupplierListForPurchaseResponseDto result = new SupplierListForPurchaseResponseDto();
            result.setValue(dcData.getDcCode() + " " + dcData.getDcName() + " " + dcData.getDcShortName() + " " + dcData.getDcPym());
            result.setLabel(dcData.getDcCode());
            resultList.add(result);
        }
        //当订单类型为调拨订单（Z003-Z006）以外时
        List<SupplierListReturnDto> supplierList = gaiaSupplierBasicMapper.selectSupplierList(dto);
        //设定返回值
        for (SupplierListReturnDto returnDate : supplierList) {
            SupplierListForPurchaseResponseDto result = new SupplierListForPurchaseResponseDto();
            result.setValue(returnDate.getSupSelfCode() + " " + returnDate.getSupName() + " " + returnDate.getSupCreditCode() + " " + returnDate.getSupPym());
            result.setLabel(returnDate.getSupSelfCode());
            result.setSupPayTerm(returnDate.getSupPayTerm());
            result.setSupLeadTime(returnDate.getSupLeadTime());
            result.setSupNoPurchase(returnDate.getSupNoPurchase());
            resultList.add(result);
        }
        return resultList;
    }

    /**
     * 商品列表
     *
     * @param dto GetProductListRequestDto
     * @return List<GetProductListResponseDto>
     */
    public List<GetProductListResponseDto> selectGetProductList(GetProductListRequestDto dto) {
        //地点检查
        if (dto.getProSite().size() == 0) {
            throw new CustomResultException(ResultEnum.SITE_ERROR);
        }
        // 地点为:ALL全部
        if (dto.getProSite().get(0).equals("ALL")) {
            return gaiaProductBasicMapper.selectProductListSiteAll(dto.getClient(), dto.getProSelfCode());
        }
        //商品列表查询
        List<GetProductListResponseDto> supplierList = gaiaProductBasicMapper.selectProductList(dto);
        List<String> proSelfCodeList = supplierList.stream().map(GetProductListResponseDto::getProSelfCode).collect(Collectors.toList());
        List<DcReplenishmentListResponseDto> saleList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(proSelfCodeList)) {
            String client = feignService.getLoginInfo().getClient();
            saleList = gaiaProductBasicMapper.selectSaleProByPro(client, proSelfCodeList);
        }
        List<GaiaDcData> dcList = gaiaProductBasicMapper.getDcListToUpdate(dto.getClient(), null);
        GaiaDcData gaiaDcData = dcList.stream().filter(Objects::nonNull).filter(item -> item.getDcCode().equals(dto.getProSite().get(0))).findFirst().orElse(null);
        List<String> stoList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(gaiaDcData)) {
            // # 仓库对应门店
            stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(dto.getClient(), dto.getProSite().get(0));
            stoList = stoList.stream().distinct().collect(Collectors.toList());
            // 商品月销(仓库对应门店月销之和)
            for (GetProductListResponseDto item : supplierList) {
                item.setSalesThirty(0);
                int thirty = 0;
                for (DcReplenishmentListResponseDto sale : saleList) {
                    if ("30".equals(sale.getType()) && item.getProSelfCode().equals(sale.getGssdProId()) && stoList.contains(sale.getGssdBrId())) {
                        if (!ObjectUtils.isEmpty(sale.getGssdQty())) {
                            thirty = thirty + sale.getGssdQty().intValue();
                        }
                    }
                }
                item.setSalesThirty(thirty);
            }
        }
        for (GetProductListResponseDto item : supplierList) {
            item.setSales7(0);
            item.setSales30(0);
            // 7
            DcReplenishmentListResponseDto seven = saleList.stream().filter(t ->
                    t.getType().equals("7") && item.getProSelfCode().equals(t.getGssdProId()) && item.getProSite().equals(t.getGssdBrId())
            ).findFirst().orElse(null);
            if (seven != null) {
                item.setSales7(seven.getGssdQty().intValue());
            }
            // 30
            DcReplenishmentListResponseDto thirty = saleList.stream().filter(t ->
                    t.getType().equals("30") && item.getProSelfCode().equals(t.getGssdProId()) && item.getProSite().equals(t.getGssdBrId())
            ).findFirst().orElse(null);
            if (thirty != null) {
                item.setSales30(thirty.getGssdQty().intValue());
            }
        }
        return supplierList;
    }

    /**
     * 地点集合
     *
     * @param dto GetSiteListRequestDto
     * @return List<GetSiteListResponseDto>
     */
    public List<GetSiteListResponseDto> selectGetSiteList(GetSiteListRequestDto dto) {
        String client = feignService.getLoginInfo().getClient();
        if (dto == null) {
            dto = new GetSiteListRequestDto();
        }
        dto.setClient(client);
        //地点集合查询
        List<GetSiteListResponseDto> siteList = gaiaDcDataMapper.selectSiteList(dto);
        return siteList;
    }

    /**
     * 地点集合
     *
     * @param dto GetSiteListRequestDto
     * @return List<GetSiteListResponseDto>
     */
    @Override
    public List<GetSiteListResponseDto> selectGetSiteList1(GetSiteListRequestDto dto) {
        String client = feignService.getLoginInfo().getClient();
        if (dto == null) {
            dto = new GetSiteListRequestDto();
        }
        dto.setClient(client);
        dto.setUserId(feignService.getLoginInfo().getUserId());
        //地点集合查询
        List<GetSiteListResponseDto> siteList = gaiaDcDataMapper.selectSiteList1(dto);
        return siteList;
    }

    /**
     * 门店选择
     *
     * @param dto StoreSelectRequestDto
     * @return List<GetSiteListResponseDto>
     */
    public List<GetSiteListResponseDto> StoreSelect(StoreSelectRequestDto dto) {
        //门店选择查询
        List<GetSiteListResponseDto> siteList = gaiaDcDataMapper.storeSelect(dto);
        return siteList;
    }

    /**
     * 客户信息
     *
     * @param client 加盟商
     * @param site   地点
     * @return
     */
    public Result customerInfo(String client, String site) {
        // 当前用户
        TokenUser user = feignService.getLoginInfo();
        return ResultUtil.success(gaiaCustomerBusinessMapper.customerInfo(user.getClient(), site));
    }

    /**
     * 采购订单查询
     *
     * @param dto PoHeaderListRequestDto
     * @return PageInfo<PoHeaderListResponseDto>
     */
    @Override
    public PageInfo<PoHeaderListResponseDto> selectPoHeaderList(PoHeaderListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        //分页
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        if (StringUtils.isNotBlank(dto.getPoProCode())) {
            dto.setPoProCodes(Arrays.asList(dto.getPoProCode().split(",")));
        }
        //采购订单查询
        List<PoHeaderListResponseDto> poHeaderList = gaiaPoHeaderMapper.selectPoHeaderList(dto);
        PageInfo<PoHeaderListResponseDto> pageInfo = new PageInfo<>(poHeaderList);
        return pageInfo;
    }

    /**
     * 采购订单明细
     *
     * @param dto PoItemRequestDto
     * @return PoItemResponseDto
     */
    @Override
    public PoItemResponseDto selectPoItemList(PoItemRequestDto dto) {
        //采购订单查询
        List<PoItemResponseDto> poHeaderList = gaiaPoHeaderMapper.selectPoHeader(dto);

        //头部信息设置
        PoItemResponseDto poHeader = new PoItemResponseDto();
        if (null != poHeaderList && poHeaderList.size() > 0) {
            BeanUtils.copyProperties(poHeaderList.get(0), poHeader);
        }
        BigDecimal total = BigDecimal.ZERO;
        //明细部设置
        List<PoItemListDto> poItemList = gaiaPoHeaderMapper.selectPoItemList(dto);
        poHeader.setPoItemListDto(poItemList);
        if (CollectionUtils.isNotEmpty(poItemList)) {
            List<Map<String, String>> params = new ArrayList<>();
            for (PoItemListDto poItem : poItemList) {
                if (!"1".equals(poItem.getPoLineDelete())) {
                    if (poItem != null && poItem.getPoLineTotalaMount() != null) {
                        total = total.add(poItem.getPoLineTotalaMount());
                    }
                }
                Map<String, String> param = new HashMap<>();
                param.put("proSite", poHeader.getPoCompanyCode());
                param.put("proSelfCode", poItem.getPoProCode());
                params.add(param);
            }
            // 商品集合
            GetProductListRequestDto getProductListRequestDto = new GetProductListRequestDto();
            // 加盟商
            getProductListRequestDto.setClient(dto.getClient());
            getProductListRequestDto.setParams(params);
            //商品列表查询
            List<GetProductListResponseDto> supplierList = gaiaProductBasicMapper.selectProductList(getProductListRequestDto);
            for (PoItemListDto poItem : poItemList) {
                List<GetProductListResponseDto> suppliers = supplierList.stream().filter(
                        item -> poHeader.getPoCompanyCode().equals(item.getProSite()) && poItem.getPoProCode().equals(item.getProSelfCode())
                ).collect(Collectors.toList());
                poItem.setProductList(suppliers);
            }
        }
        poHeader.setPoTotalAmount(total);
        return poHeader;
    }

    /**
     * 后续凭证
     *
     * @param dto FuCertificateRequestDto
     * @return List<FuCertificateResponseDto>
     */
    public List<FuCertificateResponseDto> selectFuCertificate(FuCertificateRequestDto dto) {
        //后续凭证查询
        List<FuCertificateResponseDto> poHeaderList = gaiaMaterialDocMapper.selectFuCertificate(dto);
        return poHeaderList;
    }

    /**
     * 采购订单保存
     *
     * @param dto SavePurchaseOrderRequestDto
     */
    @Override
    @Transactional
    public void updateSavePurchaseOrder(SavePurchaseOrderRequestDto dto) {
        //审批状态 = 1已审批 均不可以修改
        GaiaPoHeaderKey headerKey = new GaiaPoHeaderKey();
        BeanUtils.copyProperties(dto, headerKey);
        GaiaPoHeader headerResult = gaiaPoHeaderMapper.selectByPrimaryKey(headerKey);
//        if (CommonEnum.PoApproveStatus.APPROVED.getCode().equals(headerResult.getPoApproveStatus())) {
//            throw new CustomResultException(ResultEnum.APPROVE_ERROR);
//        }
        //更新采购订单主表（GAIA_PO_HEADER）
        dto.setPoDate(dto.getPoDateStr().replaceAll("-", "").replaceAll("/", ""));
        gaiaPoHeaderMapper.updatePoHeader(dto);
        int num = 1;
        //订单行号不为空则编辑当前行
        for (GaiaPoItemList gaiaPoItemList : dto.getGaiaPoItemList()) {
            GaiaPoItem item = new GaiaPoItem();
            item.setClient(dto.getClient());
            item.setPoId(dto.getPoId());
            BeanUtils.copyProperties(gaiaPoItemList, item);
            if (!StringUtils.isBlank(gaiaPoItemList.getPoLineNo())) {
                if (StringUtils.isBlank(item.getPoLineDelete()) || "false".equals(item.getPoLineDelete().toLowerCase()) || "0".equals(item.getPoLineDelete().toLowerCase())) {
                    item.setPoLineDelete("0");
                } else {
                    item.setPoLineDelete("1");
                }
                GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
                gaiaPoItemKey.setClient(dto.getClient());
                gaiaPoItemKey.setPoId(dto.getPoId());
                gaiaPoItemKey.setPoLineNo(gaiaPoItemList.getPoLineNo());
                GaiaPoItem gaiaPoItem = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);
                if (item.getPoQty() != null && gaiaPoItem.getPoDeliveredQty() != null && item.getPoQty().compareTo(gaiaPoItem.getPoDeliveredQty()) < 0) {
                    throw new CustomResultException("行" + gaiaPoItemList.getPoLineNo() + "不能小于已交货数量");
                }
                item.setPoLineAmt(item.getPoQty().multiply(item.getPoPrice()));
                if (StringUtils.isBlank(item.getPoCompleteFlag()) || "false".equals(item.getPoCompleteFlag().toLowerCase()) || "0".equals(item.getPoCompleteFlag().toLowerCase())) {
                    item.setPoCompleteFlag("0");
                } else {
                    item.setPoCompleteFlag("1");
                }
                //更新采购订单明细表（GAIA_PO_ITEM）
                item.setPoScrq(StringUtils.isNotBlank(item.getPoScrq()) ? item.getPoScrq().replaceAll("/", "") : "");
                item.setPoYxq(StringUtils.isNotBlank(item.getPoYxq()) ? item.getPoYxq().replaceAll("/", "") : "");
                gaiaPoItemMapper.updateByPrimaryKeySelective2(item);
            } else {
//                GaiaPoItemKey gaiaPoItemKey = new GaiaPoItemKey();
//                gaiaPoItemKey.setClient(dto.getClient());
//                gaiaPoItemKey.setPoId(dto.getPoId());
//                GaiaPoItem gaiaPoItem = gaiaPoItemMapper.selectByPrimaryKey(gaiaPoItemKey);
//                if (null != gaiaPoItem) {
//                    throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
//                }
                //订单行号查询
                String maxLineNo = gaiaPoItemMapper.selectLineNo(dto.getPoId(), dto.getClient());
                if (StringUtils.isBlank(maxLineNo)) {
                    item.setPoLineNo("1");
                } else {
                    item.setPoLineNo(String.valueOf(Integer.valueOf(maxLineNo) + num));
                }
                num++;
                if (StringUtils.isBlank(item.getPoLineDelete()) || "false".equals(item.getPoLineDelete().toLowerCase()) || "0".equals(item.getPoLineDelete().toLowerCase())) {
                    item.setPoLineDelete("0");
                } else {
                    item.setPoLineDelete("1");
                }
                if (StringUtils.isBlank(item.getPoCompleteFlag()) || "false".equals(item.getPoCompleteFlag().toLowerCase()) || "0".equals(item.getPoCompleteFlag().toLowerCase())) {
                    item.setPoCompleteFlag("0");
                } else {
                    item.setPoCompleteFlag("1");
                }
                item.setPoScrq(StringUtils.isNotBlank(item.getPoScrq()) ? item.getPoScrq().replaceAll("/", "") : "");
                item.setPoYxq(StringUtils.isNotBlank(item.getPoYxq()) ? item.getPoYxq().replaceAll("/", "") : "");
                item.setPoLineAmt(item.getPoQty().multiply(item.getPoPrice()));
                //插入采购订单明细表（GAIA_PO_ITEM）
                gaiaPoItemMapper.insert(item);
            }
        }
    }

    /**
     * 采购订单打印信息
     *
     * @param dto
     * @return
     */
    @Override
    public PurchaseOrderResponseDto getPurchaseOrderInfo(PoItemRequestDto dto) {
        // 获取订单主表数据
        PurchaseOrderResponseDto resp = gaiaPoHeaderMapper.getPurchaseOrderInfo(dto);
        if (resp != null) {
            //明细表数据
            List<PurchaseItemResponseDto> itemList = gaiaPoItemMapper.getPoItemList(dto);
            BigDecimal totalPrice = new BigDecimal(0); // 总价
            BigDecimal orderCount = new BigDecimal(0); // 总数量

            for (PurchaseItemResponseDto item : itemList) {
                if (item.getPoLineAmt() != null) {
                    totalPrice = totalPrice.add(item.getPoLineAmt());
                }
                if (item.getPoQty() != null) {
                    orderCount = orderCount.add(item.getPoQty());
                }
            }
            resp.setTotalPrice(totalPrice);
            resp.setOrderCount(orderCount);
            resp.setItemList(itemList);
        }
        return resp;
    }

    /**
     * 商品列表
     *
     * @param dto
     * @return
     */
    @Override
    public List<GetProductListResponseDto> getProductListForAdjustment(GetProductListRequestDto dto) {
        if (CollectionUtils.isEmpty(dto.getProSite())) {
            return new ArrayList<GetProductListResponseDto>();
        }
        //商品列表查询
        List<GetProductListResponseDto> list = gaiaProductBasicMapper.getProductListForAdjustment(dto);
        return list;
    }

    /**
     * 导出
     *
     * @param dto
     * @return
     */
    @Override
    public Result exportList(PoHeaderListRequestDto dto) {
        try {
            PageHelper.clearPage();
            //采购订单查询
            List<PoHeaderListResponseDto> poHeaderList = gaiaPoHeaderMapper.selectPoHeaderList(dto);
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(poHeaderList) && i < poHeaderList.size(); i++) {
                // 明细数据
                PoHeaderListResponseDto poHeaderListResponseDto = poHeaderList.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                //序号
                item.add(String.valueOf(i + 1));
                //公司
                item.add(this.concat(poHeaderListResponseDto.getPoCompanyCode(), poHeaderListResponseDto.getPoCompanyName()));
                // 门店
                if (StringUtils.isNotBlank(poHeaderListResponseDto.getPoDeliveryTypeStore()) && StringUtils.isNotBlank(poHeaderListResponseDto.getPoDeliveryTypeStoreName())) {
                    item.add(this.concat(poHeaderListResponseDto.getPoDeliveryTypeStore(), poHeaderListResponseDto.getPoDeliveryTypeStoreName()));
                } else {
                    item.add("");
                }
                //创建人
                item.add(this.concat(poHeaderListResponseDto.getPoCreateBy(), poHeaderListResponseDto.getPoCreateByName()));
                //采购订单号
                item.add(poHeaderListResponseDto.getPoId());
                //订单行号
                item.add(poHeaderListResponseDto.getPoLineNo());
                //订单类型
                item.add(this.concat(poHeaderListResponseDto.getPoType(), poHeaderListResponseDto.getOrdTypeDesc()));
                //订单日期
                item.add(this.formatDate(poHeaderListResponseDto.getPoDate()));
                //供应商编码
                item.add(poHeaderListResponseDto.getPoSupplierId());
                //供应商名称
                item.add(poHeaderListResponseDto.getPoSupplierName());
                //付款条件
                item.add(poHeaderListResponseDto.getPoPaymentName());
                //审批状态
                Map<Object, String> statusMap = EnumUtils.EnumToMap(CommonEnum.PoApproveStatus.class);
                item.add(statusMap.get(poHeaderListResponseDto.getPoApproveStatus()));
                //订单总金额
                item.add(poHeaderListResponseDto.getPoTotalAmount() == null ? "" : poHeaderListResponseDto.getPoTotalAmount().toString());
                //抬头备注
                item.add(poHeaderListResponseDto.getPoHeadRemark());
                //商品编码
                item.add(poHeaderListResponseDto.getPoProCode());
                //商品名称
                item.add(poHeaderListResponseDto.getPoProName());
                // 通用名
                item.add(poHeaderListResponseDto.getProCommonname());
                //规格
                item.add(poHeaderListResponseDto.getProSpecs());
                //生产厂家
                item.add(this.concat(poHeaderListResponseDto.getProFactoryCode(), poHeaderListResponseDto.getProFactoryName()));
                //产地
                item.add(poHeaderListResponseDto.getProPlace());
                //订单数量
                item.add(poHeaderListResponseDto.getPoQty() == null ? "" : poHeaderListResponseDto.getPoQty().toString());
                //单位
                item.add(poHeaderListResponseDto.getPoUnitName());
                //单价
                item.add(poHeaderListResponseDto.getPoPrice() == null ? "" : poHeaderListResponseDto.getPoPrice().toString());
                //建议零售价
                item.add(poHeaderListResponseDto.getProLsj() == null ? "" : poHeaderListResponseDto.getProLsj().toString());
                //税率
                item.add(poHeaderListResponseDto.getPoRateName());
                //行总价
                item.add(poHeaderListResponseDto.getPoLineTotalaMount() == null ? "" : poHeaderListResponseDto.getPoLineTotalaMount().toString());
//                //地点
//                item.add(this.concat(poHeaderListResponseDto.getPoCompanyCode(), poHeaderListResponseDto.getPoCompanyName()));
//                //库位
//                Dictionary locationDictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE,
//                        poHeaderListResponseDto.getPoLocationCode());
//                if (locationDictionary != null) {
//                    item.add(this.concat(poHeaderListResponseDto.getPoLocationCode(), locationDictionary.getLabel()));
//                } else {
//                    item.add(poHeaderListResponseDto.getPoLocationCode());
//                }
                // 批次
                item.add(poHeaderListResponseDto.getPoBatch());
                // 生产批号
                item.add(poHeaderListResponseDto.getPoBatchNo());
                // 生产日期
                item.add(this.formatDate(poHeaderListResponseDto.getPoScrq()));
                // 有效期
                item.add(this.formatDate(poHeaderListResponseDto.getPoYxq()));
                //计划交货日期
                item.add(this.formatDate(poHeaderListResponseDto.getPoDeliveryDate()));
                //行备注
                item.add(poHeaderListResponseDto.getPoLineRemark());
                //删除标记
                Dictionary deleteDictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.NO_YES,
                        poHeaderListResponseDto.getPoLineDelete());
                if (deleteDictionary != null) {
                    item.add(deleteDictionary.getLabel());
                } else {
                    item.add(poHeaderListResponseDto.getPoLineDelete());
                }
                //交货已完成
                Dictionary completeDictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(CommonEnum.DictionaryStaticData.NO_YES,
                        poHeaderListResponseDto.getPoCompleteFlag());
                if (completeDictionary != null) {
                    item.add(completeDictionary.getLabel());
                } else {
                    item.add(poHeaderListResponseDto.getPoCompleteFlag());
                }
                //已交货数量
                item.add(poHeaderListResponseDto.getPoDeliveredQty() == null ? "" : poHeaderListResponseDto.getPoDeliveredQty().toString());
                //已开票数量
//                item.add(poHeaderListResponseDto.getPoInvoiceQty() == null ? "" : poHeaderListResponseDto.getPoInvoiceQty().toString());
                //已交货金额
                item.add(poHeaderListResponseDto.getPoDeliveredAmt() == null ? "" : poHeaderListResponseDto.getPoDeliveredAmt().toString());
                //已开票金额
//                item.add(poHeaderListResponseDto.getPoDeliveredAmt() == null ? "" : poHeaderListResponseDto.getPoDeliveredAmt().toString());
                item.add(!ObjectUtils.isEmpty(poHeaderListResponseDto.getPoQty()) && !ObjectUtils.isEmpty(poHeaderListResponseDto.getPoDeliveredQty()) ? String.valueOf(poHeaderListResponseDto.getPoQty().subtract(poHeaderListResponseDto.getPoDeliveredQty())) : "");
                item.add(poHeaderListResponseDto.getWmDhrq());
                //  大类",
                item.add(poHeaderListResponseDto.getProBigClassName());
                //  中类",
                item.add(poHeaderListResponseDto.getProMidClassName());
                //  小类",
                item.add(poHeaderListResponseDto.getProClassName());
                //  自定义字段1",
                item.add(poHeaderListResponseDto.getProZdy1());
                //  自定义字段2",
                item.add(poHeaderListResponseDto.getProZdy2());
                //  自定义字段3",
                item.add(poHeaderListResponseDto.getProZdy3());
                //  自定义字段4",
                item.add(poHeaderListResponseDto.getProZdy4());
                //  自定义字段5",
                item.add(poHeaderListResponseDto.getProZdy5());
                //  采购员",
                item.add(poHeaderListResponseDto.getProPurchaseRate());
                //  业务员
                item.add(poHeaderListResponseDto.getGssName());
                dataList.add(item);
            }
            try {
                Result zdyResult = dcReplenishService.getProductZdy();
                GaiaProductZdy gaiaProductZdy = (GaiaProductZdy) zdyResult.getData();
                if (gaiaProductZdy != null) {
                    headList[38] = gaiaProductZdy.getProZdy1Name();
                    headList[39] = gaiaProductZdy.getProZdy2Name();
                    headList[40] = gaiaProductZdy.getProZdy3Name();
                    headList[41] = gaiaProductZdy.getProZdy4Name();
                    headList[42] = gaiaProductZdy.getProZdy5Name();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("采购订单");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 采购订单汇总查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageInfo<PoHeaderListResponseDto> getPoHeaderList(PoHeaderListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        //分页
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        //采购订单查询
        List<PoHeaderListResponseDto> poHeaderList = gaiaPoHeaderMapper.getPoHeaderList(dto);
        PageInfo<PoHeaderListResponseDto> pageInfo = new PageInfo<>(poHeaderList);
        return pageInfo;
    }

    /**
     * 日期格式化
     *
     * @param date
     * @return
     */
    private String formatDate(String date) {
        if (StringUtils.isBlank(date)) {
            return null;
        }
        if (date.length() != 8) {
            return date;
        }
        StringBuilder sb = new StringBuilder(date);
        sb.insert(4, "/");
        sb.insert(7, "/");
        return sb.toString();
    }

    /**
     * 字符串连接
     *
     * @param val
     * @return
     */
    private String concat(String... val) {
        StringBuilder result = new StringBuilder();
        if (val == null || val.length == 0) {
            return result.toString();
        }
        for (int i = 0; i < val.length; i++) {
            if (StringUtils.isNotBlank(val[i])) {
                result.append("-").append(val[i]);
            }
        }
        if (result.length() > 0) {
            result.deleteCharAt(0);
        }
        return result.toString();
    }

    public void checkJyFw(TokenUser user, CreatePurchaseOrderRequestDto dto) {
        // 经营范围管控
        List<GaiaDcData> dataList = dcDataMapper.selectDcListByClient(user.getClient());
        boolean authority = dataList.stream().anyMatch(m -> "1".equals(m.getDcJyfwgk()));
        if (authority) {
            GaiaDcData dcData = dcDataMapper.selectByDcCode(user.getClient(), dto.getPoCompanyCode());
            List<String> jyfwList = new ArrayList<>();
            List<String> distList = new ArrayList<>();
            if (dcData != null) {
                jyfwList = gaiaJyfwDataMapper.getJyfwIdList(user.getClient(), dcData.getDcChainHead());
                if (!CollectionUtils.isNotEmpty(jyfwList)) {
                    throw new CustomResultException(ResultEnum.DC_JYFW_NOT_EXIST);
                }
                distList = jyfwList.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
                if (!CollectionUtils.isNotEmpty(distList)) {
                    throw new CustomResultException(ResultEnum.DC_JYFW_NOT_EXIST);
                }
            } else {
                jyfwList = gaiaJyfwDataMapper.getJyfwIdList(user.getClient(), dto.getPoCompanyCode());
                if (!CollectionUtils.isNotEmpty(jyfwList)) {
                    throw new CustomResultException(ResultEnum.STORE_JYFW_NOT_EXIST);
                }
                distList = jyfwList.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
                if (!CollectionUtils.isNotEmpty(distList)) {
                    throw new CustomResultException(ResultEnum.STORE_JYFW_NOT_EXIST);
                }
            }
            if (CollectionUtils.isNotEmpty(dto.getGaiaPoItemList())) {
                int index = 0;
                for (GaiaPoItemList poItem : dto.getGaiaPoItemList()) {
                    index++;
                    GaiaProductBusiness productBusiness = gaiaProductBusinessMapper.getProductBusiness(user.getClient(), poItem.getPoSiteCode(), poItem.getPoProCode());
                    if (productBusiness != null && StringUtils.isNotBlank(productBusiness.getProJylb())) {
                        List<String> jylb = Arrays.asList(productBusiness.getProJylb().split(","));
                        List<String> tog = jylb.stream().filter(distList::contains).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(tog)) {
                            throw new CustomResultException("第" + index + "行商品超出采购主体对应连锁总部的经营管控范围");
                        }
                    } else {
                        throw new CustomResultException("第" + index + "行商品超出采购主体对应连锁总部的经营管控范围");
                    }
                }
            }
        }
    }

    /**
     * 业务数据表头
     */
    private String[] headList = {
            "序号",
            "公司",
            "门店",
            "创建人",
            "采购订单号",
            "订单行号",
            "订单类型",
            "订单日期",
            "供应商编码",
            "供应商名称",
            "付款条件",
            "审批状态",
            "订单总金额",
            "抬头备注",
            "商品编码",
            "商品名称",
            "商品通用名",
            "规格",
            "生产厂家",
            "产地",
            "订单数量",
            "单位",
            "单价",
            "建议零售价",
            "税率",
            "行总价",
            "批次",
            "生产批号",
            "生产日期",
            "有效期",
            "计划交货日期",
            "行备注",
            "删除标记",
            "交货已完成",
            "已到货数量",
            "已到货金额",
            "到货差异",
            "到货日期",
            "大类",
            "中类",
            "小类",
            "自定义字段1",
            "自定义字段2",
            "自定义字段3",
            "自定义字段4",
            "自定义字段5",
            "采购员",
            "业务员",
    };
}
