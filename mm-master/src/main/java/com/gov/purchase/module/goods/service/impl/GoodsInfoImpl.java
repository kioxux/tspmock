package com.gov.purchase.module.goods.service.impl;

import com.gov.purchase.constants.ProductClassEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaProductBasicMapper;
import com.gov.purchase.module.goods.dto.SaveBasicProListRequestDTO;
import com.gov.purchase.module.goods.dto.SaveCompProListRequestDTO;
import com.gov.purchase.module.goods.dto.SaveGaiaProductBusiness;
import com.gov.purchase.module.goods.service.GoodsInfoService;
import com.gov.purchase.module.goods.service.GoodsService2;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author Zhangchi
 * @since 2021/11/02/11:14
 */
@Slf4j
@Service
public class GoodsInfoImpl implements GoodsInfoService {

    @Resource
    GoodsService2 goodsService2;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Value("${penghui.userId}")
    private String penghuiUserId;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void firstSaleGoods(SaveBasicProListRequestDTO dto){
        if (ObjectUtils.isEmpty(dto.getBasicsList())){
            throw new CustomResultException("商品数据为空！");
        }
        String client = dto.getClient();
        String userId = penghuiUserId;
        for (SaveGaiaProductBusiness productBusiness : dto.getBasicsList()){
            productBusiness.setProInputTax("J0");
            if (ProductClassEnum.DRUGS.code.equals(productBusiness.getProClass())){
                productBusiness.setProClass(ProductClassEnum.DRUGS.proClass);
            }
            else if(ProductClassEnum.APPARATUS.code.equals(productBusiness.getProClass())){
                productBusiness.setProClass(ProductClassEnum.APPARATUS.proClass);
            }
            else if (ProductClassEnum.HEALTH_FOOD.code.equals(productBusiness.getProClass())){
                productBusiness.setProClass(ProductClassEnum.HEALTH_FOOD.proClass);
            }
            if (StringUtils.isBlank(productBusiness.getProStorageArea())){
                //商品仓储分区 1-内服药品，2-外用药品，3-中药饮片区，4-精装中药区，5-针剂，6-二类精神药品，7-含麻药品，8-冷藏商品，9-外用非药，10-医疗器械，11-食品，12-保健食品，13-易燃商品区, 14-其他
                productBusiness.setProStorageArea("14");
                //是否通过GMP认证
                productBusiness.setProIfGmp("1");
                productBusiness.setGoodsownid(productBusiness.getProSelfCode());
                productBusiness.setProSupplyName("山东蓬晖医药有限公司");
                if (StringUtils.isBlank(productBusiness.getProCommonname())){
                    productBusiness.setProCommonname(productBusiness.getProName());
                }
            }
        }
        checkParams(dto.getBasicsList(), client);
        // 1.获取有国际条形码的列表
        List<String> barcodeList = dto.getBasicsList().stream().filter(Objects::nonNull)
                .filter(item -> !StringUtils.isBlank(item.getProBarcode()))
                .map(GaiaProductBusiness::getProBarcode)
                .distinct()
                .collect(Collectors.toList());

        // 2.当前权限下的 地点列表
        List<String> siteList = gaiaProductBasicMapper.getSiteList(client, userId);
        if (CollectionUtils.isEmpty(siteList)) {
            throw new CustomResultException("所有门店/配送中心都不具有权限");
        }

        // 3.根据地点+对应行的国际条形码 精确匹配 business表
        List<GaiaProductBusiness> compBusinessList = gaiaProductBasicMapper.getCompBusinessList(client, barcodeList, siteList);
        List<SaveCompProListRequestDTO> list = new ArrayList<>();
        for (GaiaProductBusiness productBusiness : compBusinessList) {
            SaveCompProListRequestDTO compProList = new SaveCompProListRequestDTO();
            compProList.setProSite(productBusiness.getProSite());
            compProList.setProBarcode(productBusiness.getProBarcode());
            compProList.setProCode(productBusiness.getProCode());
            compProList.setProName(productBusiness.getProName());
            compProList.setProFactoryCode(productBusiness.getProFactoryCode());
            compProList.setProRegisterNo(productBusiness.getProRegisterNo());
            compProList.setProSpecs(productBusiness.getProSpecs());
            compProList.setOperation(2);
            list.add(compProList);
        }
        dto.setCompProList(list);

        dto.setClient(client);
        dto.setUserId(userId);
        dto.setStatus("0");
        goodsService2.saveBasicProList(dto);
    }


    /**
     * 参数校验
     */
    private void checkParams(List<SaveGaiaProductBusiness> list, String client) {
        if (CollectionUtils.isEmpty(list)) {
            throw new CustomResultException("商品列表不能为空");
        }
        list.forEach(item -> {
            if (StringUtils.isBlank(item.getProClass())) {
                throw new CustomResultException("商品分类编码不能为空");
            }
            if (StringUtils.isBlank(item.getProSelfCode())) {
                throw new CustomResultException("商品自编码不能为空");
            }
            if (StringUtils.isBlank(item.getProCommonname())) {
                throw new CustomResultException("商品通用名不能为空");
            }
            if (StringUtils.isBlank(item.getProSpecs())) {
                throw new CustomResultException("规格不能为空");
            }
            if (StringUtils.isBlank(item.getProFactoryName())) {
                throw new CustomResultException("生产企业不能为空");
            }
            if (StringUtils.isBlank(item.getProInputTax())) {
                throw new CustomResultException("进项税率不能为空");
            }
            if (StringUtils.isBlank(item.getProStorageCondition())) {
                throw new CustomResultException("贮存条件不能为空 1-常温，2-阴凉，3-冷藏");
            }
        });
    }
}
