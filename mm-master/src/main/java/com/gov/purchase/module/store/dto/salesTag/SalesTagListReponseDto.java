package com.gov.purchase.module.store.dto.salesTag;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
@ApiModel("销售标签列表返回参数")
public class SalesTagListReponseDto {
    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "门店编码", name = "prcStore")
    private String prcStore;

    @ApiModelProperty(value = "门店名称", name = "prcStoreName")
    private String prcStoreName;

    @ApiModelProperty(value = "商品编码", name = "prcProduct")
    private String prcProduct;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    private String proSelfCode;

    @ApiModelProperty(value = "商品名称", name = "proName")
    private String proName;

    @ApiModelProperty(value = "商品通用名", name = "proName")
    private String proCommonname;

    @ApiModelProperty(value = "商品描述", name = "proDepict")
    private String proDepict;

    @ApiModelProperty(value = "商品规格", name = "proSpecs")
    private String proSpecs;

    @ApiModelProperty(value = "价格分类", name = "prcClass")
    private String prcClass;

    /**
     * 价格分类名称
     */
    private String prcClassName;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNo")
    private String prcModfiyNo;

    @ApiModelProperty(value = "价格", name = "prcAmount")
    private BigDecimal prcAmount;

    @ApiModelProperty(value = "有效期起", name = "prcEffectDate")
    private String prcEffectDate;

    @ApiModelProperty(value = "单位", name = "prcUnit")
    private String prcUnit;

    @ApiModelProperty(value = "单位名称", name = "unitName")
    private String unitName;

    @ApiModelProperty(value = "产地", name = "proPlace")
    private String proPlace;

    @ApiModelProperty(value = "国际条形码", name = "proBarcode")
    private String proBarcode;

    @ApiModelProperty(value = "核价人（当前人姓名）", name = "priceOfficer")
    private String priceOfficer;

    /**
     * 厂家
     */
    private String factoryName;

    /**
     * 是否参保 0-否，1-是
     */
    private String ifMed;

    /**
     * 零售价
     */
    private BigDecimal priceNormal;

    /**
     * 会员价
     */
    private BigDecimal priceHy;

    /**
     * 商品定位
     */
    private String proPosition;

    /**
     * 会员价
     */
    private BigDecimal prcAmountHy;

    /**
     * 门店编码
     */
    private String labStore;

    /**
     * 价签申请单号
     */
    private String labApplyNo;

    /**
     * 门店简称
     */
    private String stoShortName;

    /**
     * 是否医保
     */
    private String proIfMed;

    /**
     * 是否医保
     */
    private String proIfMedName;

    /**
     * 处方类别
     */
    private String proPresclass;

    /**
     * 处方类别
     */
    private String proPresclassName;

    /**
     * 销售级别
     */
    private String proSlaeClass;

    // 不打折商品
    private String proBdz;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 自定义字段1
     */
    private String proZdy1;

    /**
     * 自定义字段2
     */
    private String proZdy2;

    /**
     * 自定义字段3
     */
    private String proZdy3;

    /**
     * 自定义字段4
     */
    private String proZdy4;

    /**
     * 自定义字段5
     */
    private String proZdy5;

    private String stoName;

    /**
     * 是否医保店
     */
    private String stoIfMedicalcare;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 剂型
     */
    private String shopowner;

    /**
     * 国家医保支付价
     */
    private BigDecimal proGjybzfj;
}
