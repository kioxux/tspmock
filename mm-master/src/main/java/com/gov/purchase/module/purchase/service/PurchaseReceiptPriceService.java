package com.gov.purchase.module.purchase.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.purchase.dto.PurchaseReceiptPriceDTO;

import java.util.List;

/**
 * @author : Yzf
 * description: 采购入库差价单功能
 * create time: 2021/12/13 9:28
 */
public interface PurchaseReceiptPriceService {

    /**
     * 采购差价单列表（未审核）
     *
     * @return
     */
    List<String> selectCjIdList();

    /**
     * 保存/审核采购差价单
     *
     * @param dto
     * @return
     */
    Result savePurchaseReceiptPriceList(PurchaseReceiptPriceDTO dto);

    /**
     * 采购入库差价单删除
     *
     * @param dto
     * @return
     */
    Result deletePurchaseReceiptPriceList(PurchaseReceiptPriceDTO dto);

    /**
     * 根据采购差价单号查询
     *
     * @param dto
     * @return
     */
    Result getRecordByCjId(PurchaseReceiptPriceDTO dto);

    /**
     *采购入库差价单查询
     *
     * @param dto
     * @return
     */
    Result getPurchaseReceiptPriceRecord(PurchaseReceiptPriceDTO dto);

}
