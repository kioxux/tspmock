package com.gov.purchase.module.purchase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetPayStatusResponseDTO {
    /**
     * 支付状态
     */
    private String status;
}
