package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.09
 */
@Data
public class ProductBatchUpdate {
    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proSelfCode;
    /**
     * 字段更新值
     */
    @ExcelValidate(index = 1, name = "字段更新值", type = ExcelValidate.DataType.STRING, maxLength = 255)
    private String newVal;
}
