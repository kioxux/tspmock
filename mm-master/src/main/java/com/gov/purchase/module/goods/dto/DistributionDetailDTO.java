package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class DistributionDetailDTO {

    private String code;

    private String stoCode;

    private String stoName;
    //门店类型id
    private String gssgId;
    //门店属性
    private String attribute;

    private BigDecimal qty;
}
