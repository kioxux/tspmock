package com.gov.purchase.module.delivery.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "调拨价格提交传入参数")
public class SaveAllotPriceRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    /**
     * 条件类型
     */
    @ApiModelProperty(value = "条件类型", name = "alpConditionType")
    private String alpConditionType;

    /**
     * 供货地点
     */
    @ApiModelProperty(value = "供货地点", name = "alpSupplySite")
    private String alpSupplySite;

    /**
     * 收货地点
     */
    @ApiModelProperty(value = "收货地点", name = "alpReceiveSite")
    private String alpReceiveSite;

    /**
     * 订单类型
     */
    @ApiModelProperty(value = "订单类型", name = "alpOrderType")
    private String alpOrderType;

    /**
     * 连锁总部
     */
    @ApiModelProperty(value = "连锁总部", name = "alpChainHead")
    private String alpChainHead;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码", name = "alpProCode")
    private String alpProCode;

    /**
     * 条件金额
     */
    @ApiModelProperty(value = "条件金额", name = "alpConditionQty")
    private String alpConditionQty;

    /**
     * 有效期从
     */
    @ApiModelProperty(value = "有效期从", name = "alpEffectFrom")
    private String alpEffectFrom;

    /**
     * 有效期至
     */
    @ApiModelProperty(value = "有效期至", name = "alpEffectEnd")
    private String alpEffectEnd;

    /**
     * 删除标记
     */
    @ApiModelProperty(value = "删除标记", name = "alpDeleteFlag")
    private String alpDeleteFlag;
}