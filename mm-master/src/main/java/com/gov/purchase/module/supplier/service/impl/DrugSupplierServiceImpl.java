package com.gov.purchase.module.supplier.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.ListUtils;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.supplier.dto.*;
import com.gov.purchase.module.supplier.service.DrugSupplierService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.SpringUtil;
import com.gov.purchase.utils.StringUtils;
import com.gov.purchase.utils.csv.CsvClient;
import com.gov.purchase.utils.csv.dto.CsvFileInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Service
public class DrugSupplierServiceImpl implements DrugSupplierService {

    @Resource
    GaiaSupplierBasicMapper gaiaSupplierBasicMapper;
    @Resource
    GaiaSupplierGspinfoMapper gaiaSupplierGspinfoMapper;
    @Resource
    GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    FeignService feignService;
    @Resource
    GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;
    @Resource
    GaiaSupplierGspinfoLogMapper gaiaSupplierGspinfoLogMapper;
    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    OperationFeignService operationFeignService;
    @Autowired
    private CosUtils cosUtils;
    /**
     * 厂商库搜索
     *
     * @param dto QueryProductBasicRequestDto
     * @return PageInfo<QuerySupListResponseDto>
     */
    public PageInfo<QuerySupListResponseDto> selectDrugSupplierInfo(QuerySupListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //分页查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        //加盟商
        dto.setClient(user.getClient());
        List<QuerySupListResponseDto> querySupList;
        if (StringUtils.isBlank(dto.getSupSite())) {
            //地点是空的情况下，只查询 GAIA_SUPPLIER_BASIC
            querySupList = gaiaSupplierBasicMapper.selectBasicDrugSupplierInfo(dto);
        } else {
            //查询 GAIA_SUPPLIER_BASIC 和 GAIA_SUPPLIER_BUSINESS
            querySupList = gaiaSupplierBasicMapper.selectDrugSupplierInfo(dto);
        }
        PageInfo<QuerySupListResponseDto> pageInfo = new PageInfo<>(querySupList);
        return pageInfo;
    }

    /**
     * 供应商首营提交
     *
     * @param dto QuerySupListRequestDto
     * @return int
     */
    @Transactional
    public void insertGspinfoSubmitInfo(GspinfoSubmitRequestDto dto) {
        //加盟商
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        dto.setClient(client);

        // 供应商自编码重复验证
        // 同一加盟商门店自编码不可以重复
        {
            //同加盟商下供应商自编码重复验证（首营表 GAIA_SUPPLIER_GSPINFO）
            List<GaiaSupplierGspinfo> gaiaSupplierGspinfos = gaiaSupplierGspinfoMapper.selfOnlyCheck(dto.getClient(), dto.getSupSelfCode(), dto.getSupSite());
            if (CollectionUtils.isNotEmpty(gaiaSupplierGspinfos)) {
                throw new CustomResultException(ResultEnum.SUPPLISERSELFCODE_ERROR);
            }

            //同加盟商下供应商自编码重复验证（业务表 GAIA_SUPPLIER_BUSINESS）
            List<GaiaSupplierBusiness> gaiaSupplierBusinesses = gaiaSupplierBusinessMapper.selfOnlyCheck(dto.getClient(), dto.getSupSelfCode(), dto.getSupSite());
            if (CollectionUtils.isNotEmpty(gaiaSupplierBusinesses)) {
                throw new CustomResultException(ResultEnum.SUPPLISERSELFCODE_ERROR);
            }
        }
        // 供应商停用判断
        GaiaSupplierBasic gaiaSupplierBasic = gaiaSupplierBasicMapper.selectByPrimaryKey(dto.getSupCode());
        if (CommonEnum.SupplierStauts.SUPPLIERDISNORMAL.getCode().equals(gaiaSupplierBasic.getSupStatus())) {
            throw new CustomResultException(ResultEnum.E0162);
        }
        // 是否连锁
        boolean isChain = false;
        // 非连锁时，取消发起工作流
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(dto.getClient());
        gaiaStoreDataKey.setStoCode(dto.getSupSite());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        // 首营地点为门店
        if (gaiaStoreData != null) {
            // 连锁
            if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(gaiaStoreData.getStoAttribute())) {
                isChain = true;
            }
        } else {
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(dto.getClient());
            gaiaDcDataKey.setDcCode(dto.getSupSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            // 连锁
            if (StringUtils.isNotBlank(gaiaDcData.getDcChainHead())) {
                isChain = true;
            }
        }

        // 连锁
        if (isChain) {
            //首营日期
            dto.setSupGspDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            //首营状态
            dto.setSupGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode().toString());
            // 首营流程编号
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            dto.setSupFlowNo(System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
            gaiaSupplierGspinfoMapper.insertGspinfoSubmit(dto);
            // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
            FeignResult result = feignService.createSupplierWorkflow(dto);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
        } else { // 非连锁
            GaiaSupplierBusiness business = new GaiaSupplierBusiness();
            // 商品主数据复制到业务表
            // SpringUtil.copyPropertiesIgnoreNull(gaiaSupplierBasic, business);
            SpringUtil.copyPropertiesIgnoreNull(dto, business);
            //供应商状态正常
            business.setSupStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
            // 禁止采购
            business.setSupNoPurchase(CommonEnum.NoYesStatus.NO.getCode());
            // 禁止退厂
            business.setSupNoSupplier(CommonEnum.NoYesStatus.NO.getCode());
            // 匹配状态
            business.setSupMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
            // 助计码
            if (StringUtils.isBlank(business.getSupPym())) {
                business.setSupPym(StringUtils.ToFirstChar(business.getSupName()));
            }
            gaiaSupplierBusinessMapper.insertSelective(business);
            GaiaSupplierGspinfo info = new GaiaSupplierGspinfo();
            BeanUtils.copyProperties(business, info);
            info.setSupGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            info.setSupFlowNo("N" + System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
            gaiaSupplierGspinfoMapper.insert(info);
            // 第三方 gys-operation
            try {
                operationFeignService.supplierSync(business.getClient(), business.getSupSite(), "1", Collections.singletonList(business.getSupSelfCode()));
            } catch (Exception e) {
                log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
            }
        }
        // 首营日志
        GaiaSupplierGspinfoLog gaiaSupplierGspinfoLogOld = new GaiaSupplierGspinfoLog();
        BeanUtils.copyProperties(gaiaSupplierBasic, gaiaSupplierGspinfoLogOld);
        // 加盟商
        gaiaSupplierGspinfoLogOld.setClient(user.getClient());
        // 自编码
        gaiaSupplierGspinfoLogOld.setSupSelfCode(dto.getSupSelfCode());
        // 地点
        gaiaSupplierGspinfoLogOld.setSupSite(dto.getSupSite());
        gaiaSupplierGspinfoLogOld.setSupLine(1);
        gaiaSupplierGspinfoLogMapper.insertSelective(gaiaSupplierGspinfoLogOld);
        GaiaSupplierGspinfoLog gaiaSupplierGspinfoLogNew = new GaiaSupplierGspinfoLog();
        BeanUtils.copyProperties(dto, gaiaSupplierGspinfoLogNew);
        gaiaSupplierGspinfoLogOld.setSupLine(2);
        gaiaSupplierGspinfoLogMapper.insertSelective(gaiaSupplierGspinfoLogOld);
    }

    /**
     * 供应商首营列表
     *
     * @param dto QueryGspinfoListRequestDto
     * @return PageInfo<QueryGspInfoResponseDto>
     */
    public PageInfo<QueryGspListResponseDto> selectQueryGspinfo(QueryGspinfoListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        List<QueryGspListResponseDto> QueryGspList = new ArrayList<>();
        //用户权限判断
        List<GaiaStoreData> limit = gaiaAuthconfiDataMapper.getSiteList(user.getClient(), user.getUserId());
        //参数地点为空的场合
        if (StringUtils.isBlank(dto.getSupSite())) {
            if (null != limit && limit.size() > 0) {
                dto.setLimitList(limit);
            } else {
                PageInfo<QueryGspListResponseDto> pageInfo = new PageInfo<>(QueryGspList);
                return pageInfo;
            }
            //参数地点不为空的场合
        } else {
            List<GaiaStoreData> gaiaStoreData = new ArrayList<>();
            GaiaStoreData gaiaStore = new GaiaStoreData();
            gaiaStore.setStoCode(dto.getSupSite());
            gaiaStoreData.add(gaiaStore);
            dto.setLimitList(gaiaStoreData);
        }
        //分页查询
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        QueryGspList = gaiaSupplierGspinfoMapper.selectQueryGspinfo(dto);
        PageInfo<QueryGspListResponseDto> pageInfo = new PageInfo<>(QueryGspList);
        return pageInfo;
    }

    /**
     * 供应商首营详情
     *
     * @param dto GaiaSupplierRequestDto
     * @return QueryGspListResponseDto
     */
    @Override
    public QueryGspListResponseDto selectQueryGspValue(GaiaSupplierRequestDto dto) {
        QueryGspListResponseDto gaiaProductGspinfo = gaiaSupplierGspinfoMapper.selectByPrimary(dto);
        return gaiaProductGspinfo;
    }

    /**
     * 最大自编码查询
     *
     * @param client
     * @param supSite
     * @param code
     * @return
     */
    @Override
    public Result getMaxSelfCodeNum(String client, String supSite, String code) {
        if (StringUtils.isNotBlank(code)) {
            return ResultUtil.success(code);
        }
        // 自编码集合
        List<String> list = gaiaSupplierBusinessMapper.getSelfCodeList(client, supSite, code);
        if (CollectionUtils.isEmpty(list)) {
            return ResultUtil.success();
        }
        String maxCode = ListUtils.getMaxCode(list);
        String selfCode = ListUtils.getMaxCode(maxCode);
        return ResultUtil.success(selfCode);
    }

    /**
     * 手工新增
     *
     * @param gspinfoSubmitRequestDto
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result addBusioness(GspinfoSubmitRequestDto gspinfoSubmitRequestDto) {
        //加盟商
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        gspinfoSubmitRequestDto.setClient(client);
        // 供应商自编码重复验证
        // 同一加盟商门店自编码不可以重复
        {
            //同加盟商下供应商自编码重复验证（首营表 GAIA_SUPPLIER_GSPINFO）
            List<GaiaSupplierGspinfo> gaiaSupplierGspinfos = gaiaSupplierGspinfoMapper.selfOnlyCheck(gspinfoSubmitRequestDto.getClient(), gspinfoSubmitRequestDto.getSupSelfCode(), gspinfoSubmitRequestDto.getSupSite());
            if (CollectionUtils.isNotEmpty(gaiaSupplierGspinfos)) {
                throw new CustomResultException(ResultEnum.SUPPLISERSELFCODE_ERROR);
            }

            //同加盟商下供应商自编码重复验证（业务表 GAIA_SUPPLIER_BUSINESS）
            List<GaiaSupplierBusiness> gaiaSupplierBusinesses = gaiaSupplierBusinessMapper.selfOnlyCheck(gspinfoSubmitRequestDto.getClient(), gspinfoSubmitRequestDto.getSupSelfCode(), gspinfoSubmitRequestDto.getSupSite());
            if (CollectionUtils.isNotEmpty(gaiaSupplierBusinesses)) {
                throw new CustomResultException(ResultEnum.SUPPLISERSELFCODE_ERROR);
            }
        }
        // 参数验证
        //供应商自编码 地点 供应商名称 营业执照编号 营业期限 法人 注册地址
        if (StringUtils.isBlank(gspinfoSubmitRequestDto.getSupSelfCode()) || StringUtils.isBlank(gspinfoSubmitRequestDto.getSupSite())
                || StringUtils.isBlank(gspinfoSubmitRequestDto.getSupName())) {
            throw new CustomResultException(ResultEnum.CHECK_PARAMETER);
        }

        // 是否连锁
        boolean isChain = false;
        // 非连锁时，取消发起工作流
        GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
        gaiaStoreDataKey.setClient(gspinfoSubmitRequestDto.getClient());
        gaiaStoreDataKey.setStoCode(gspinfoSubmitRequestDto.getSupSite());
        GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
        // 首营地点为门店
        if (gaiaStoreData != null) {
            // 连锁
            if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(gaiaStoreData.getStoAttribute())) {
                isChain = true;
            }
        } else {
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(gspinfoSubmitRequestDto.getClient());
            gaiaDcDataKey.setDcCode(gspinfoSubmitRequestDto.getSupSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            // 连锁
            if (StringUtils.isNotBlank(gaiaDcData.getDcChainHead())) {
                isChain = true;
            }
        }
        //首营日期
        gspinfoSubmitRequestDto.setSupGspDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        // 连锁
        if (isChain) {
            //首营状态
            gspinfoSubmitRequestDto.setSupGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode().toString());
            // 首营流程编号
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            gspinfoSubmitRequestDto.setSupFlowNo(System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
            gaiaSupplierGspinfoMapper.insertGspinfoSubmit(gspinfoSubmitRequestDto);
            // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
            FeignResult result = feignService.createSupplierWorkflow(gspinfoSubmitRequestDto);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
            try {
                operationFeignService.supplierSync(user.getClient(), gspinfoSubmitRequestDto.getSupSite(), "2", Collections.singletonList(gspinfoSubmitRequestDto.getSupSelfCode()));
            } catch (Exception e) {
                log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
            }
        } else { // 非连锁
            GaiaSupplierBusiness business = new GaiaSupplierBusiness();
            // 商品主数据复制到业务表
            SpringUtil.copyPropertiesIgnoreNull(gspinfoSubmitRequestDto, business);
            //供应商状态正常
            business.setSupStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
            // 禁止采购
            business.setSupNoPurchase(CommonEnum.NoYesStatus.NO.getCode());
            // 禁止退厂
            business.setSupNoSupplier(CommonEnum.NoYesStatus.NO.getCode());
            // 匹配状态
            business.setSupMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
            // 助计码
            if (StringUtils.isBlank(business.getSupPym())) {
                business.setSupPym(StringUtils.ToFirstChar(business.getSupName()));
            }
            gaiaSupplierBusinessMapper.insertSelective(business);
            GaiaSupplierGspinfo info = new GaiaSupplierGspinfo();
            BeanUtils.copyProperties(business, info);
            info.setSupGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode());
            int i = (int) ((Math.random() * 4 + 1) * 100000);
            info.setSupFlowNo("N" + System.currentTimeMillis() + StringUtils.leftPad(String.valueOf(i), 6, "0"));
            gaiaSupplierGspinfoMapper.insert(info);
            try {
                operationFeignService.supplierSync(user.getClient(), business.getSupSite(), "1", Collections.singletonList(business.getSupSelfCode()));
            } catch (Exception e) {
                log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
            }
        }
        return ResultUtil.success();
    }

    @Override
    public Result exportQueryGspinfoList(QueryGspinfoListRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        dto.setClient(user.getClient());
        List<QueryGspListResponseDtoCSV> queryGspList = new ArrayList<>() ;
        //用户权限判断
        List<GaiaStoreData> limit = gaiaAuthconfiDataMapper.getSiteList(user.getClient(), user.getUserId());
        CsvFileInfo csvInfo ;
        Result result = new Result();
        //参数地点为空的场合
        if (StringUtils.isBlank(dto.getSupSite())) {
            if (null != limit && limit.size() > 0) {
                dto.setLimitList(limit);
            } else {
                result.setMessage("提示：查询结果为空");
                return result;
            }
            //参数地点不为空的场合
        } else {
            List<GaiaStoreData> gaiaStoreData = new ArrayList<>();
            GaiaStoreData gaiaStore = new GaiaStoreData();
            gaiaStore.setStoCode(dto.getSupSite());
            gaiaStoreData.add(gaiaStore);
            dto.setLimitList(gaiaStoreData);
        }
        // 不分页查询
        dto.setPageNum(0);
        dto.setPageSize(0);
        queryGspList = gaiaSupplierGspinfoMapper.selectExportQueryGspinfo(dto);
        if (ObjectUtils.isEmpty(queryGspList)){
            result.setMessage("提示：查询结果为空");
            return result;
        }
        for (QueryGspListResponseDtoCSV queryGspListResponseDtoCSV : queryGspList) {
            // 转换数据内容
            convertData(queryGspListResponseDtoCSV);
        }
        try {
            csvInfo = CsvClient.getCsvByte(queryGspList, "供应商首营列表", Collections.singletonList((short) 1));
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            try {
                bos.write(csvInfo.getFileContent());
                result = cosUtils.uploadFile(bos, csvInfo.getFileName());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                bos.flush();
                bos.close();
            }
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;

    }

    private void convertData(QueryGspListResponseDtoCSV queryGspResponseDtoCSV) {
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupBankAccount())){
            queryGspResponseDtoCSV.setSupBankAccount(queryGspResponseDtoCSV.getSupBankAccount()+"\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupContactTel())){
            queryGspResponseDtoCSV.setSupContactTel(queryGspResponseDtoCSV.getSupContactTel()+"\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupFlowNo())){
            queryGspResponseDtoCSV.setSupFlowNo(queryGspResponseDtoCSV.getSupFlowNo()+"\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupSelfCode())){
            queryGspResponseDtoCSV.setSupSelfCode(queryGspResponseDtoCSV.getSupSelfCode()+"\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupSite())){
            queryGspResponseDtoCSV.setSupSite(queryGspResponseDtoCSV.getSupSite()+"\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupCreditCode())){
            queryGspResponseDtoCSV.setSupCreditCode(queryGspResponseDtoCSV.getSupCreditCode()+"\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupFrwtsSfz())){
            queryGspResponseDtoCSV.setSupFrwtsSfz(queryGspResponseDtoCSV.getSupFrwtsSfz()+"\t");
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupClass())){
            switch (queryGspResponseDtoCSV.getSupClass()) {
                case "1":
                    queryGspResponseDtoCSV.setSupClass("生产企业");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setSupClass("商业公司");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setSupClass("非商品供应商");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setSupClass("财务专用");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setSupClass("食品经营企业");
                    break;
                case "6":
                    queryGspResponseDtoCSV.setSupClass("药品经营企业");
                    break;
                case "7":
                    queryGspResponseDtoCSV.setSupClass("器械经营企业");
                    break;
                case "8":
                    queryGspResponseDtoCSV.setSupClass("药品生产企业");
                    break;
                case "9":
                    queryGspResponseDtoCSV.setSupClass("器械生产企业");
                    break;
                case "10":
                    queryGspResponseDtoCSV.setSupClass("食品生产企业");
                    break;
                case "11":
                    queryGspResponseDtoCSV.setSupClass("其他经营企业");
                    break;
                case "12":
                    queryGspResponseDtoCSV.setSupClass("委托配送供应商");
                    break;
                case "13":
                    queryGspResponseDtoCSV.setSupClass("物流承运商");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupStatus())){
            switch (queryGspResponseDtoCSV.getSupStatus()) {
                case "0":
                    queryGspResponseDtoCSV.setSupStatus("正常");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setSupStatus("停用");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupNoPurchase())){
            switch (queryGspResponseDtoCSV.getSupNoPurchase()) {
                case "0":
                    queryGspResponseDtoCSV.setSupNoPurchase("是");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setSupNoPurchase("否");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupNoSupplier())){
            switch (queryGspResponseDtoCSV.getSupNoSupplier()) {
                case "0":
                    queryGspResponseDtoCSV.setSupNoSupplier("是");
                    break;
                case "1":
                    queryGspResponseDtoCSV.setSupNoSupplier("否");
                    break;
            }
        }
        if (StringUtils.isNotEmpty(queryGspResponseDtoCSV.getSupPayMode())){
            switch (queryGspResponseDtoCSV.getSupPayMode()) {
                case "1":
                    queryGspResponseDtoCSV.setSupPayMode("银行转账");
                    break;
                case "2":
                    queryGspResponseDtoCSV.setSupPayMode("承兑汇票");
                    break;
                case "3":
                    queryGspResponseDtoCSV.setSupPayMode("线下票据");
                    break;
                case "4":
                    queryGspResponseDtoCSV.setSupPayMode("赊账");
                    break;
                case "5":
                    queryGspResponseDtoCSV.setSupPayMode("电汇");
                    break;
            }
        }
    }
}
