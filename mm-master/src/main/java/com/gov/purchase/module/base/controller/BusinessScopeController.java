package com.gov.purchase.module.base.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.base.service.BusinessScopeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "经营范围")
@RestController
@RequestMapping("businessScope")
public class BusinessScopeController {

    @Resource
    private BusinessScopeService businessScopeService;

    @Log("经营范围列表")
    @ApiOperation("经营范围列表")
    @PostMapping("getScopeList")
    public Result getScopeList(@RequestJson(value = "jyfwOrgid", name = "机构编码") String jyfwOrgid,
                               @RequestJson(value = "jyfwOrgname", name = "机构名称") String jyfwOrgname,
                               @RequestJson(value = "jyfwSite", name = "地点", required = false) String jyfwSite,
                               @RequestJson(value = "jyfwType", name = "类型") Integer jyfwType,
                               @RequestJson(value = "jyfwClass", required = false) String jyfwClass) {
        return ResultUtil.success(businessScopeService.getScopeList(jyfwOrgid, jyfwOrgname, jyfwSite, jyfwType, jyfwClass));
    }

    @Log("经营范围编辑保存")
    @ApiOperation("经营范围编辑保存")
    @PostMapping("editScope")
    public Result editScope(@RequestBody @Valid EditScopeVO vo) {
        businessScopeService.editScope(vo);
        return ResultUtil.success();
    }

    @Log("经营范围列表全部")
    @ApiOperation("经营范围列表全部")
    @PostMapping("getAllScopeList")
    public Result getAllScopeList(@RequestJson(value = "jyfwClass", required = false) String jyfwClass) {
        return ResultUtil.success(businessScopeService.getAllScopeList(jyfwClass));
    }


}
