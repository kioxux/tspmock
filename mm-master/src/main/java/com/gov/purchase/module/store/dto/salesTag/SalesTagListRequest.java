package com.gov.purchase.module.store.dto.salesTag;

import com.gov.purchase.common.entity.Pageable;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.07.27
 */
@Data
public class SalesTagListRequest extends Pageable {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 连锁总部
     */
    private String chainHead;

    /**
     * 门店编码
     */
    private String stoCode;

    /**
     * 门店更多
     */
    private List<String> stoCodes;

    /**
     * 商品分类
     */
    private String proClass;

    /**
     * 有库存 1:有
     */
    private String hasStore;

    /**
     * 商品自编码
     */
    private String proSelfCode;

    /**
     * 商品自编码更多
     */
    private List<String> proSelfCodes;

    /**
     * 商品新建日期开始
     */
    private String proCreateDateStrat;

    /**
     * 商品新建日期结束
     */
    private String proCreateDateEnd;

    /**
     * 商品调价日期开始
     */
    private String priceDateStart;

    /**
     * 商品调价日期结束
     */
    private String priceDateEnd;

    //    @NotNull(message = "会员价选择不能为空 (0:会员价(默认), 1:会员日价, 2:会员卡折扣(级联 折扣：60%到100%，空 = 100%))")
    private Integer memberPriceSelection;

    /**
     * 折扣
     */
    private BigDecimal discount;

    /**
     * 是否医保
     */
    private String proIfMed;

    /**
     * 医保类型
     */
    private String proYblx;


    /**
     * 排除中药
     */
    private Integer excludeTcm;

    private String proZdy1;
    private String proZdy2;
    private String proZdy3;
    private String proZdy4;
    private String proZdy5;

}
