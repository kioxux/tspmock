package com.gov.purchase.module.priceCompare.dto;

import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Valid
@Data
public class PriceCompareProDto {

    /**
     * 比价表主表.ID
     */
    private Long priceCompareId;

    /**
     * 商品编号
     */
    private String proNo;

    /**
     * 商品名称
     */
    private String proName;

    /**
     * 规格
     */
    private String specs;

    /**
     * 生成厂家
     */
    private String manufacturer;

    /**
     * 单位
     */
    private String unit;

    /**
     * 中包装
     */
    private String mediumPack;

    /**
     * 大包装
     */
    private String bigPack;

    /**
     * 批准文号
     */
    private String approvalNumber;

    /**
     * 国际条形码
     */
    private String internationalBarCode;

    /**
     * 建议采购量
     */
    private Integer adviseAmount;

    /**
     * 最终采购量
     */
    private Integer finalAmount;



}
