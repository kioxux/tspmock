package com.gov.purchase.module.replenishment.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaSdReplenishD;
import com.gov.purchase.entity.GaiaSdReplenishH;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.module.purchase.dto.SearchReplenish;
import com.gov.purchase.module.purchase.dto.SearchReplenishReq;
import com.gov.purchase.module.replenishment.dto.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

public interface StoreNeedService {


    /**
     * 根据配送中心获取门店集合
     *
     * @param dto
     * @return
     */
    List<GaiaStoreData> queryStoreList(StoreRequestDto dto);

    /**
     * 门店铺货列表
     *
     * @param dto
     * @return
     */
    List<BatchStockListResponseDto> queryBatchStockList(BatchStockRequestDto dto);


    /**
     * 门店铺货提交
     *
     * @param list
     * @return
     */
    Result saveStoreNeedList(List<StoreNeedRequestDto> list);

    /**
     * 铺货查询
     *
     * @param searchReplenishReq
     * @return
     */
    Result searchReplenishList(SearchReplenishReq searchReplenishReq);

    /**
     * 铺货保存
     *
     * @param list
     * @return
     */
    Result saveReplenish(List<SearchReplenish> list);

    /**
     * 有权限的DC列表
     *
     * @return
     */
    List<GaiaDcData> getAuthDcList(TokenUser tokenUser);

    /**
     * 门店列表
     *
     * @param dcCode
     * @return
     */
    List<GetStoreListByAuthDcDTO> getStoreListByAuthDc(String dcCode, TokenUser user, String gssgId, String stoDistrict);

    /**
     * 商品选择弹出框
     *
     * @param dto
     * @return
     */
    List<StoreReplenishDetails> getProList(GetProListRequestDTO dto, TokenUser tokenUser);

    /**
     * 门店铺货商品提交
     */
    AutoNeedProDTO saveNeedProList(List<SaveNeedProListRequestDTO> list, TokenUser user);

    /**
     * 商品选择
     *
     * @param selectProductListParams
     * @return
     */
    Result selectProductList(SelectProductListParams selectProductListParams, TokenUser tokenUser);

    /**
     * 门店铺货，自动开单
     */
    void automaticBilling(List<GaiaSdReplenishH> replenishHList);

    Result autoSelectProductList(String proSelfCodes, TokenUser tokenUser);

    void autoCreateDistributionOrder(List<GaiaSdReplenishH> replenishHList, List<GaiaSdReplenishD> replenishDList);

    void autoEntrustOrder(GaiaSdReplenishH replenishH, List<GaiaSdReplenishD> replenishDList);

    /**
     * 店型下拉框&区域下拉框
     */
    GetStoreTypeIdItemListAndAreaListDTO getStoreTypeIdItemListAndAreaList();

    /**
     * 库存对比铺货
     * @param needProWmsParams
     * @return
     */
    Result selectNeedProListByWms(NeedProWmsParams needProWmsParams);

    /**
     * 管理区域
     * @return
     */
    Result selectStoresGroup();

    /**
     * 门店规模
     *
     * @return
     */
    Result selectStoresScale();

    List<GetStoreListByAuthDcDTO> getElectronicCouponStoreList( TokenUser user ,String gssgId, String stoDistrict);

    /**
     * 批次货位
     *
     * @param storeNeedHwAndBatchDTO
     * @return
     */
    Result selectBatchAndHw(StoreNeedHwAndBatchDTO storeNeedHwAndBatchDTO);

    /**
     * 库存对比铺货导出
     * @param needProWmsParams
     * @return
     */
    void needProListByWmsExport(HttpServletResponse response , NeedProWmsParams needProWmsParams);
}
