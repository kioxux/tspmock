package com.gov.purchase.module.purchase.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 销售订单
 * @author: yzf
 * @create: 2021-11-04 15:04
 */

@Data
public class SalesOrderRecordVO {

    /**
     * 日期
     */
    private String soCreateDate;
    /**
     * 订单号
     */
    private String soId;
    /**
     * 客户编码
     */
    private String soCustomerId;
    /**
     * 订单行号
     */
    private String soLineNo;
    /**
     * 地点
     */
    private String soSiteCode;
    /**
     * 客户名称
     */
    private String cusName;
    /**
     * 商品编码
     */
    private String soProCode;
    /**
     * 通用名称
     */
    private String proCommonName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 剂型
     */
    private String proForm;
    /**
     * 厂家
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 批号
     */
    private String soBatchNo;
    /**
     * 有效期
     */
    private String batExpiryDate;
    /**
     * 购进单位
     */
    private String batSupplierName;
    /**
     * 返回数量/销售数量
     */
    private BigDecimal soQty;
    /**
     * 返回单价/销售单价
     */
    private BigDecimal soPrice;
    /**
     * 批准文号
     */
    private String proRegisterNo;
    /**
     * 金额
     */
    private BigDecimal soTotal;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 原销售单号
     */
    private String soReferOrder;
    /**
     * 原销售日期
     */
    private String soOldCreteDate;
    /**
     * 原销售单价
     */
    private BigDecimal soOldPrice;

    /**
     * 上市许可持有人
     */
    private String proHolder;

    /**
     * 生产许可证号/备案号
     */
    private String proQsCode;
}
