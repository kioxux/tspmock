package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductBusinessKey;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.entity.GaiaSupplierBusinessKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaSdReplenishDMapper;
import com.gov.purchase.mapper.GaiaSupplierBusinessMapper;
import com.gov.purchase.module.base.dto.businessImport.NoDcReplenishment;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.purchase.dto.DirectPurchaseListDTO;
import com.gov.purchase.module.purchase.dto.DirectPurchaseListVO;
import com.gov.purchase.module.purchase.dto.DirectPurchaseSaveDTO;
import com.gov.purchase.module.purchase.service.IDirectPurchaseService;
import com.gov.purchase.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class NoDcReplenishmentImport extends BusinessImport {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private IDirectPurchaseService directPurchaseService;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;

    /**
     * 业务验证(证照详情页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    @Transactional
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        List<String> errorList = new ArrayList<>();
        // 返回结果集
        List<DirectPurchaseSaveDTO> resultList = new ArrayList<>();
        // 批量导入
        try {
            // 配送平均天数  默认为 3天
            Integer dcDeliveryDays = 3;
            List<String> proCodes = new ArrayList<>();
            List<GaiaProductBusinessKey> gaiaProductBusinessKeyList = new ArrayList<>();
            List<GaiaSupplierBusinessKey> gaiaSupplierBusinessKeyList = new ArrayList<>();
            List<String> supCodes = new ArrayList<>();
            for (Integer key : map.keySet()) {
                NoDcReplenishment dcReplenishment = (NoDcReplenishment) map.get(key);
                GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
                GaiaSupplierBusinessKey gaiaSupplierBusinessKey = new GaiaSupplierBusinessKey();
                gaiaSupplierBusinessKey.setSupSite(dcReplenishment.getStoCode());
                gaiaSupplierBusinessKey.setSupSelfCode(dcReplenishment.getPoSupplierId());
                gaiaSupplierBusinessKeyList.add(gaiaSupplierBusinessKey);
                gaiaProductBusinessKey.setProSite(dcReplenishment.getStoCode());
                gaiaProductBusinessKey.setProSelfCode(dcReplenishment.getProSelfCode());
                gaiaProductBusinessKeyList.add(gaiaProductBusinessKey);
                proCodes.add(dcReplenishment.getProSelfCode());
                if (StringUtils.isNotBlank(dcReplenishment.getPoSupplierId())) {
                    supCodes.add(dcReplenishment.getPoSupplierId());
                }
            }
            List<GaiaSupplierBusiness> gaiaSupplierBusinessList = null;
            if (!CollectionUtils.isEmpty(supCodes)) {
                //验证供应商存在性
                gaiaSupplierBusinessList = gaiaSupplierBusinessMapper.selectSupplierListStatusNoDC(client, gaiaSupplierBusinessKeyList);
            }
            // 根据当前人的配送中心 查询DC补货列表
            DirectPurchaseListDTO directPurchaseListDTO = new DirectPurchaseListDTO();
            directPurchaseListDTO.setClient(user.getClient());
            if (StringUtils.isEmpty(directPurchaseListDTO.getPattern())) {
                directPurchaseListDTO.setPattern("0,1");
            }
//            List<DirectPurchaseListVO> list = gaiaSdReplenishDMapper.selectDirectPurchaseList(directPurchaseListDTO);
            // 商品列表
            List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectProListNoDC(user.getClient(), gaiaProductBusinessKeyList);
            // 验证excel  数据是否正确
            for (Integer key : map.keySet()) {
                NoDcReplenishment dcReplenishment = (NoDcReplenishment) map.get(key);
                GaiaProductBusiness gaiaProductBusinesses = gaiaProductBusinessList.stream().filter(item -> (item.getProSelfCode().equals(dcReplenishment.getProSelfCode()))).findFirst().orElse(null);
                if (gaiaProductBusinesses == null) {
                    // 第几行商品不存在
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品"));
                    continue;
                }
                if ("T".equals(gaiaProductBusinesses.getProPosition())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "商品定位不能为淘汰"));
                    continue;
                }
                if ("1".equals(gaiaProductBusinesses.getProNoPurchase())) {
                    errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "商品禁采"));
                    continue;
                }
//                List<DirectPurchaseListVO> row = list.stream().filter(item -> (item.getProSelfCode().equals(dcReplenishment.getProSelfCode()))).collect(Collectors.toList());
//                if (CollectionUtils.isEmpty(row)) {
//                    // 第几行商品不存在
//                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "商品"));
//                    continue;
//                }
                DirectPurchaseSaveDTO dcReplenishmentListResponseDto = new DirectPurchaseSaveDTO();
//                BeanUtils.copyProperties(row.get(0), dcReplenishmentListResponseDto);
                dcReplenishmentListResponseDto.setStoCode(dcReplenishment.getStoCode());
                dcReplenishmentListResponseDto.setProSelfCode(dcReplenishment.getProSelfCode());
                dcReplenishmentListResponseDto.setSupplierCode(dcReplenishment.getPoSupplierId());
                dcReplenishmentListResponseDto.setSupplierId(dcReplenishment.getPoSupplierId());
                dcReplenishmentListResponseDto.setProPrice(new BigDecimal(dcReplenishment.getPurchasePrice()));
                dcReplenishmentListResponseDto.setGsrdNeedQty(new BigDecimal(dcReplenishment.getPoQty()));
                dcReplenishmentListResponseDto.setProInputTax(gaiaProductBusinesses.getProInputTax());
                dcReplenishmentListResponseDto.setProUnit(gaiaProductBusinesses.getProUnit());
                /**
                 * （中包装从商品主数据中取值，若为空则默认是1）
                 */
                if (dcReplenishmentListResponseDto.getProMidPackage() == null) {
                    dcReplenishmentListResponseDto.setProMidPackage(1);
                }
                // 供应商
                if (StringUtils.isNotBlank(dcReplenishment.getPoSupplierId())) {
                    if (CollectionUtils.isEmpty(gaiaSupplierBusinessList)) {
                        // 第几行供应商不存在
                        errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "供应商"));
                        continue;
                    } else {
                        List<GaiaSupplierBusiness> gaiaSupplierBusinessesRow = gaiaSupplierBusinessList.stream().filter(item -> (item.getSupSelfCode().equals(dcReplenishment.getPoSupplierId()))).collect(Collectors.toList());
                        if (CollectionUtils.isEmpty(gaiaSupplierBusinessesRow)) {
                            // 第几行供应商不存在
                            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "供应商"));
                            continue;
                        }
                        dcReplenishmentListResponseDto.setSupplierCode(dcReplenishment.getPoSupplierId());
                    }
                }
                dcReplenishmentListResponseDto.setGsrdNeedQty(new BigDecimal(dcReplenishment.getPoQty()));
                dcReplenishmentListResponseDto.setProPrice(StringUtils.isNotBlank(dcReplenishment.getPurchasePrice()) ? new BigDecimal(dcReplenishment.getPurchasePrice()) : null);
                dcReplenishmentListResponseDto.setProUnit(dcReplenishmentListResponseDto.getProUnit());
                dcReplenishmentListResponseDto.setProName(gaiaProductBusinesses.getProName());
                resultList.add(dcReplenishmentListResponseDto);
            }

            if (errorList.size() > 0) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }
            directPurchaseService.save(resultList);
            return ResultUtil.success();

        } catch (Exception e) {
            e.printStackTrace();
            throw new CustomResultException(ResultEnum.E0126);
        }

    }
}
