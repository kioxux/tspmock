package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;

@Data
@ApiModel("发票付款成功查询接口传入参数")
public class ListInvoicePayingRequestDTO extends Pageable {

    /**
     * 发票号码
     */
    private String ficoFphm;

    /**
     * 发票开票日期开始
     */
    private String ficoFprqStart;

    /**
     * 发票开票日期结束
     */
    private String ficoFprqEnd;

    /**
     * 付款金额开始
     */
    private BigDecimal ficoPaymentAmountStart;
    /**
     * 付款金额结束
     */
    private BigDecimal ficoPaymentAmountEnd;

    /**
     * 发票状态 0未开票 1开票中 2已开票 3开票失败
     */
    private String ficoInvoiceStatus;

    private String client;

    /**
     * 操作者
     */
    private String ficoOperator;

    /**
     * 1:电子发票，2:纸质发票
     * 1-增值税普通发票；2-增值税专用发票
     */
    private String ficoType;

}
