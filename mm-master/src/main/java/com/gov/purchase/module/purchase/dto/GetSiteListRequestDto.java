package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "地点集合传入参数")
public class GetSiteListRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
//    @NotBlank(message = "加盟商不能为空")
    private String client;

    private String userId;

}