package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author yzf
 */
@Data
@ApiModel(value = "货源清单列表导出请求参数")
public class SourceListExportDto{

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "地点", name = "souSiteCode")
    private String souSiteCode;

    @ApiModelProperty(value = "商品自编码", name = "souProCode")
    private String souProCode;

    @ApiModelProperty(value = "商品自编码更多", name = "souProCodes")
    private List<String> souProCodes;

    @ApiModelProperty(value = "供应商自编码", name = "souSupplierId")
    private String souSupplierId;

    @ApiModelProperty(value = "供应商自编码", name = "souSupplierIds")
    private List<String> souSupplierIds;

    @ApiModelProperty(value = "是否主供应商(0-否，1-是)", name = "souMainSupplier")
    private String souMainSupplier;

    @ApiModelProperty(value = "是否锁定供应商(0-否，1-是)", name = "souLockSupplier")
    private String souLockSupplier;

    @ApiModelProperty(value = "冻结标志(0-否，1-是)", name = "souDeleteFlag")
    private String souDeleteFlag;

    /**
     * 商品编码（目前更新传这个字段， 2021-11-09）
     */
    private String proCodes;

}
