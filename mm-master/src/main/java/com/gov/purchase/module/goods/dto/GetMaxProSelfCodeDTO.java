package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetMaxProSelfCodeDTO {

    /**
     * 当前行商品自编码
     */
    private String currentRowProSeflCode;

    /**
     * 最后一行商品自编码
     */
    private String lastRowProSeflCode;

    private String beginCode;
}
