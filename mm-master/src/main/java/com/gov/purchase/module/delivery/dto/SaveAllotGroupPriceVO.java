package com.gov.purchase.module.delivery.dto;

import lombok.Data;

import java.util.List;

@Data
public class SaveAllotGroupPriceVO {
    private Integer gapgType;
    private String alpReceiveSite;
    private String gapgCode;
    private List<GaiaAllotGroupPriceVO> voList;
}
