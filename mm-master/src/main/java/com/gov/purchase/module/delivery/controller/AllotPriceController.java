package com.gov.purchase.module.delivery.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaAllotPrice;
import com.gov.purchase.entity.GaiaAllotPriceCus;
import com.gov.purchase.entity.GaiaAllotPriceGroup;
import com.gov.purchase.module.delivery.dto.*;
import com.gov.purchase.module.delivery.service.AllotPriceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "调拨价格管理")
@RestController
@RequestMapping("allotPrice")
public class AllotPriceController {

    @Resource
    private AllotPriceService allotPriceService;

    @Log("调拨门店集合")
    @ApiOperation("调拨门店集合")
    @GetMapping("getAlpReceiveSite")
    public Result getAlpReceiveSite() {
        return allotPriceService.getAlpReceiveSite();
    }

    /**
     * 调拨价格列表
     *
     * @return
     */
    @Log("调拨价格列表")
    @ApiOperation("调拨价格列表")
    @PostMapping("queryAllotPriceList")
    public Result resultAllotPriceList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                       @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                       @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
                                       @RequestJson(value = "alpProCode", required = false) String alpProCode) {
        // 调拨价格列表
        PageInfo<GaiaAllotPriceDto> list = allotPriceService.selectAllotPriceList(alpReceiveSite, alpProCode, pageNum, pageSize);
        return ResultUtil.success(list);
    }

    /**
     * 调拨价格列表
     *
     * @return
     */
    @Log("导出调拨价格列表")
    @ApiOperation("导出调拨价格列表")
    @PostMapping("exportData")
    public Result exportData(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                       @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                       @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
                                       @RequestJson(value = "alpProCode", required = false) String alpProCode) {
        return allotPriceService.exportData(alpReceiveSite, alpProCode);
    }

    /**
     * 调拨价格列表
     *
     * @return
     */
    @Log("当前加盟商下的DC")
    @ApiOperation("当前加盟商下的DC")
    @PostMapping("getDCListByCurrentClient")
    public Result getDCListByCurrentClient() {
        return ResultUtil.success(allotPriceService.getDCListByCurrentClient());
    }

    /**
     * 调拨价格提交
     *
     * @param list
     * @return
     */
    @Log("调拨价格提交")
    @ApiOperation("调拨价格提交")
    @PostMapping("saveAllotPrice")
    public Result saveAllotPrice(@RequestBody List<GaiaAllotPrice> list) {
        allotPriceService.saveAllotPrice(list);
        return ResultUtil.success();
    }

    @Log("调拨价格提交通过弹出框")
    @ApiOperation("调拨价格提交通过弹出框")
    @PostMapping("saveAllotPriceByPopup")
    public Result saveAllotPriceByPopup(@RequestBody GaiaAllotPrice dto) {
        allotPriceService.saveAllotPriceByPopup(dto);
        return ResultUtil.success();
    }

//    @Log("商品列表")
//    @ApiOperation("商品列表")
//    @PostMapping("getProductList")
//    public Result getProductList(@RequestJson(value = "alpReceiveSite") String alpReceiveSite,
//                                 @RequestJson(value = "alpProCode", required = false) String alpProCode) {
//        //商品列表搜索
//        List<GaiaProductBusiness> supplierList = allotPriceService.getProductList(alpReceiveSite, alpProCode);
//        return ResultUtil.success(supplierList);
//    }

    @Log("调拨价格删除")
    @ApiOperation("商品列表")
    @PostMapping("deleteAllotPrice")
    public Result deleteAllotPrice(@RequestBody List<GaiaAllotPrice> list){
        allotPriceService.deleteAllotPrice(list);
        return ResultUtil.success();
    }


    @Log("客户调拨价格列表")
    @ApiOperation("客户调拨价格列表")
    @PostMapping("queryAllotPriceCusList")
    public Result queryAllotPriceCusList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                         @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                         @RequestJson(value = "alpReceiveSite", required = true) String alpReceiveSite,
                                         @RequestJson(value = "alpCusCode", required = false) String alpCusCode,
                                         @RequestJson(value = "alpProCode", required = false) String alpProCode) {
        // 调拨价格列表
        PageInfo<GaiaAllotPriceCusDto> list = allotPriceService.queryAllotPriceCusList(alpReceiveSite, alpCusCode, alpProCode, pageNum, pageSize);
        return ResultUtil.success(list);
    }

    @Log("价格组列表")
    @ApiOperation("价格组列表")
    @PostMapping("queryAllotPriceGroupList")
    public Result queryAllotPriceGroupList(
            @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
            @RequestJson(value = "gapgType") String gapgType) {
        List<GaiaAllotPriceGroup> list = allotPriceService.queryAllotPriceGroupList(alpReceiveSite, gapgType);
        return ResultUtil.success(list);
    }

    @Log("门店下拉框")
    @ApiOperation("门店下拉框")
    @PostMapping("queryStoreList")
    public Result queryStoreList(
            @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
            @RequestJson(value = "gapgType") String gapgType) {
        List<StoreVO> list = allotPriceService.queryStoreList(alpReceiveSite, gapgType);
        return ResultUtil.success(list);
    }

    @Log("价格组保存")
    @ApiOperation("价格组保存")
    @PostMapping("saveAllotPriceGroup")
    public Result saveAllotPriceGroup(@RequestBody SaveAllotPriceGroupDTO dto) {
        return allotPriceService.saveAllotPriceGroup(dto);
    }

    @Log("价格组删除")
    @ApiOperation("价格组删除")
    @PostMapping("deleteAllotPriceGroup")
    public Result deleteAllotPriceGroup(@RequestBody SaveAllotPriceGroupDTO dto) {
        return allotPriceService.deleteAllotPriceGroup(dto);
    }

    @Log("目录商品价格列表")
    @ApiOperation("目录商品价格列表")
    @PostMapping("queryAllotProductPriceList")
    public Result queryAllotProductPriceList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                             @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                             @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
                                             @RequestJson(value = "gapgType") String gapgType,
                                             @RequestJson(value = "gapgCode") String gapgCode) {
        PageInfo<GaiaAllotProductPriceVO> list = allotPriceService.queryAllotProductPriceList(pageSize, pageNum, alpReceiveSite, gapgType, gapgCode);
        return ResultUtil.success(list);
    }

    @Log("目录商品价格导出")
    @ApiOperation("目录商品价格导出")
    @PostMapping("exportAllotProductPriceList")
    public Result exportAllotProductPriceList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                              @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                              @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
                                              @RequestJson(value = "gapgType") String gapgType,
                                              @RequestJson(value = "gapgCode") String gapgCode) {
        return allotPriceService.exportAllotProductPriceList(pageSize, pageNum, alpReceiveSite, gapgType, gapgCode);
    }

    @Log("目录商品价格保存")
    @ApiOperation("目录商品价格保存")
    @PostMapping("saveAllotProductPrice")
    public Result saveAllotProductPrice(@RequestBody SaveGaiaAllotProductPriceDTO dto) {
        return allotPriceService.saveAllotProductPrice(dto);
    }

    @Log("目录商品价格删除")
    @ApiOperation("目录商品价格删除")
    @PostMapping("deleteAllotProductPrice")
    public Result deleteAllotProductPrice(@RequestBody SaveGaiaAllotProductPriceDTO dto) {
        return allotPriceService.deleteAllotProductPrice(dto);
    }

    @Log("价格组员列表")
    @ApiOperation("价格组员列表")
    @PostMapping("queryAllotGroupPriceList")
    public Result queryAllotGroupPriceList(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                           @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                           @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
                                           @RequestJson(value = "gapgCode", required = false) String gapgCode,
                                           @RequestJson(value = "gapgType") String gapgType) {
        PageInfo<GaiaAllotGroupPriceVO> list = allotPriceService.queryAllotGroupPriceList(pageSize, pageNum, alpReceiveSite,
                gapgCode, gapgType);
        return ResultUtil.success(list);
    }

    @Log("目录价格组员保存")
    @ApiOperation("目录价格组员保存")
    @PostMapping("saveAllotGroupPrice")
    public Result saveAllotGroupPrice(@RequestBody SaveAllotGroupPriceVO dto) {
        return allotPriceService.saveAllotGroupPrice(dto);
    }

    @Log("目录价格组员删除")
    @ApiOperation("目录价格组员删除")
    @PostMapping("deleteAllotGroupPrice")
    public Result deleteAllotGroupPrice(@RequestBody SaveAllotGroupPriceVO dto) {
        return allotPriceService.deleteAllotGroupPrice(dto);
    }

    @Log("导出客户调拨价格列表")
    @ApiOperation("导出客户调拨价格列表")
    @PostMapping("exportAllotPriceCusData")
    public Result exportAllotPriceCusData(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                                          @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                                          @RequestJson(value = "alpReceiveSite", required = false) String alpReceiveSite,
                                          @RequestJson(value = "alpCusCode", required = false) String alpCusCode,
                                          @RequestJson(value = "alpProCode", required = false) String alpProCode) {
        return allotPriceService.exportAllotPriceCusData(alpReceiveSite, alpCusCode, alpProCode);
    }

    @Log("客户调拨价格提交")
    @ApiOperation("客户调拨价格提交")
    @PostMapping("saveAllotPriceCus")
    public Result saveAllotPriceCus(@RequestBody List<GaiaAllotPriceCus> list) {
        allotPriceService.saveAllotPriceCus(list);
        return ResultUtil.success();
    }

    @Log("客户调拨价格删除")
    @ApiOperation("客户调拨价格删除")
    @PostMapping("deleteAllotPriceCus")
    public Result deleteAllotPriceCus(@RequestBody List<GaiaAllotPriceCus> list) {
        allotPriceService.deleteAllotPriceCus(list);
        return ResultUtil.success();
    }

    @Log("客户调拨价格提交通过弹出框")
    @ApiOperation("客户调拨价格提交通过弹出框")
    @PostMapping("saveAllotPriceByPopupCus")
    public Result saveAllotPriceByPopupCus(@RequestBody GaiaAllotPriceCus dto) {
        allotPriceService.saveAllotPriceByPopupCus(dto);
        return ResultUtil.success();
    }

}
