package com.gov.purchase.module.base.service.impl.permitImport;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.mapper.GaiaFranchiseeMapper;
import com.gov.purchase.mapper.GaiaPermitDataMapper;
import com.gov.purchase.module.base.dto.BatSearchValBasic;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.SearchValBasic;
import com.gov.purchase.module.qa.dto.GaiaPermitDataExtendDto;
import com.gov.purchase.module.store.dto.GaiaDcDataListDto;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.18
 */
@Service
public class PermitSearch {

    @Resource
    GaiaPermitDataMapper gaiaPermitDataMapper;

    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Resource
    CosUtils cosUtils;
    /**
     * 证照资质查询
     *
     * @param batSearchValBasic
     * @return
     */
    public Result selectPermitBatch(BatSearchValBasic batSearchValBasic) {
        // 查询条件
        Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
        initStoreWhere(map);
        PageHelper.startPage(batSearchValBasic.getPageNum(), batSearchValBasic.getPageSize());
        List<GaiaPermitDataExtendDto> batchList = gaiaPermitDataMapper.selectPermitBatch(map);
        PageInfo<GaiaPermitDataExtendDto> pageInfo = new PageInfo<>(batchList);
        List<GaiaPermitDataExtendDto> list = pageInfo.getList();
        if (CollectionUtils.isNotEmpty(list)) {
            for (GaiaPermitDataExtendDto gaiaPermitDataExtendDto : list) {
                List<GaiaPermitDataExtendDto> details = gaiaPermitDataMapper.selectPermitDetailBatch(null,
                        gaiaPermitDataExtendDto.getClient(), gaiaPermitDataExtendDto.getPerEntityClass(), gaiaPermitDataExtendDto.getPerEntityId());
                gaiaPermitDataExtendDto.setDetails(details);
            }
        }
        return ResultUtil.success(pageInfo);
    }

    /**
     * 查询明细
     * @param batSearchValBasic
     * @return
     */
    public Result exportPermitDetailBatch(BatSearchValBasic batSearchValBasic) {
        try {
            // 查询条件
            Map<String, SearchValBasic> map = batSearchValBasic.getSearchValBasic();
            initStoreWhere(map);
            List<GaiaPermitDataExtendDto> batchList = gaiaPermitDataMapper.selectPermitDetailBatch(map, null, null, null);
            // 导出数据
            List<List<String>> basicListc = new ArrayList<>();
            initData(batchList, basicListc, null);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(basicHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(basicListc);
                    }},
                    new ArrayList<String>() {{
                        add("证照资质数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param basicListc
     * @param businessList
     */
    private void initData(List<GaiaPermitDataExtendDto> batchList, List<List<String>> basicListc, List<List<String>> businessList) {
        for (GaiaPermitDataExtendDto gaiaPermitDataExtendDto : batchList) {
            if (basicListc != null) {
                List<String> basicData = new ArrayList<>();
                //加盟商
                basicData.add(gaiaPermitDataExtendDto.getClient());
                //实体类型
                basicData.add(gaiaPermitDataExtendDto.getPerEntityName());
                //实体编码
                basicData.add(gaiaPermitDataExtendDto.getPerEntityId());
                //证照编码
                basicData.add(gaiaPermitDataExtendDto.getPerCode());
                //证照号
                basicData.add(gaiaPermitDataExtendDto.getPerId());
                //有效期从
                basicData.add(gaiaPermitDataExtendDto.getPerDateStart());
                //有效期至
                basicData.add(gaiaPermitDataExtendDto.getPerDateEnd());
                //登记日期
                basicData.add(gaiaPermitDataExtendDto.getPerRecordDate());
                //停用标志
                String per_stop_flag = gaiaPermitDataExtendDto.getPerStopFlag();
                initDictionaryStaticData(basicData, per_stop_flag, CommonEnum.DictionaryStaticData.NO_YES);
                //创建日期
                basicData.add(gaiaPermitDataExtendDto.getPerCreateDate());
                //创建时间
                basicData.add(gaiaPermitDataExtendDto.getPerCreateTime());
                //创建人
                basicData.add(gaiaPermitDataExtendDto.getPerCreateUser());
                //更新日期
                basicData.add(gaiaPermitDataExtendDto.getPerChangeDate());
                //更新时间
                basicData.add(gaiaPermitDataExtendDto.getPerChangeTime());
                //更新人
                basicData.add(gaiaPermitDataExtendDto.getPerChangeUser());
                basicListc.add(basicData);
            }
        }
    }

    /**
     * 查询数据静态字典数据处理
     * @param list
     * @param value
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }

    /**
     * 加载查询条件
     *
     * @param map
     */
    private void initStoreWhere(Map<String, SearchValBasic> map) {
        // 查询条件为空
        if (map == null || map.isEmpty()) {
            return;
        }

        // 加盟商
        SearchValBasic client = map.get("a");
        if (client != null) {

            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(client.getSearchVal()) || CollectionUtils.isNotEmpty(client.getMoreVal())) {
                list.addAll(gaiaFranchiseeMapper.getFrancName(client));
                if (StringUtils.isNotBlank(client.getSearchVal())) {
                    list.add(client.getSearchVal());
                } else {
                    list.addAll(client.getMoreVal());
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                client.setSearchVal(null);
            }
            client.setMoreVal(list);
        }

        // 实体类型
        SearchValBasic per_entity_class = map.get("b");
        if (per_entity_class != null) {
            List<String> list = new ArrayList<>();
            // 查询条件
            if (StringUtils.isNotBlank(per_entity_class.getSearchVal())) {
                list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.ENTITY_CLASS, per_entity_class.getSearchVal()));
                list.add(per_entity_class.getSearchVal());
            }
            // 更多
            if (CollectionUtils.isNotEmpty(per_entity_class.getMoreVal())) {
                for (String item : per_entity_class.getMoreVal()) {
                    if (StringUtils.isNotBlank(item)) {
                        list.addAll(CommonEnum.DictionaryStaticData.getDictionaryValByLikeLabel(CommonEnum.DictionaryStaticData.ENTITY_CLASS, item));
                        list.add(item);
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(list)) {
                per_entity_class.setSearchVal(null);
            }
            per_entity_class.setMoreVal(list);
        }
    }

    /**
     * 基础数据头
     */
    private String[] basicHead = {
            "加盟商",
            "实体类型",
            "实体编码",
            "证照编码",
            "证照号",
            "有效期从",
            "有效期至",
            "登记日期",
            "停用标志",
            "创建日期",
            "创建时间",
            "创建人",
            "更新日期",
            "更新时间",
            "更新人",
    };
//证照资质	6	加盟商	2	加盟商	a
//				实体类型	b
//				实体编码	c
}
