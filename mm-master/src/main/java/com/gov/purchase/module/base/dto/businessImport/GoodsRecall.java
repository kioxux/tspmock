package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Data
public class GoodsRecall {

    /**
     * 连锁公司
     */
    @ExcelValidate(index = 0, name = "连锁公司", type = ExcelValidate.DataType.STRING, maxLength = 4)
    private String recChainHead;

    /**
     * 门店
     */
    @ExcelValidate(index = 1, name = "门店", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false)
    private String recStore;

    /**
     * 商品编码
     */
    @ExcelValidate(index = 2, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String recProCode;

    /**
     * 批号
     */
    @ExcelValidate(index = 3, name = "批号", type = ExcelValidate.DataType.STRING, maxLength = 30, addRequired = false)
    private String recBatchNo;

    /**
     * 供应商
     */
    @ExcelValidate(index = 4, name = "供应商", type = ExcelValidate.DataType.STRING, maxLength = 30, addRequired = false)
    private String recSupplier;

    /**
     * 召回数量
     */
    @ExcelValidate(index = 5, name = "召回数量", type = ExcelValidate.DataType.INTEGER, min = 1, max = 999999999999.0, addRequired = false)
    private String recQty;

    /**
     * 召回备注
     */
    @ExcelValidate(index = 6, name = "召回备注", type = ExcelValidate.DataType.STRING, maxLength = 30, addRequired = false)
    private String recRemark;

}

