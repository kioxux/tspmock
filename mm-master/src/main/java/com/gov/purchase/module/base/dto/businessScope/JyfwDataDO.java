package com.gov.purchase.module.base.dto.businessScope;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class JyfwDataDO {

    private String id;
    private String client;
    private String jyfwOrgid;
    private String jyfwOrgname;
    private String jyfwId;
    private String jyfwName;
    private String jyfwCreDate;
    private String jyfwCreTime;
    private String jyfwCreId;
    private String jyfwModiDate;
    private String jyfwModiTime;
    private String jyfwModiId;

    private String jyfwSite;
    private Integer jyfwType;

}
