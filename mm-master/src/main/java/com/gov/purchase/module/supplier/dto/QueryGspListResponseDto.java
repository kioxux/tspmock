package com.gov.purchase.module.supplier.dto;

import com.gov.purchase.entity.GaiaSupplierGspinfo;
import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "供应商首营列表返回参数")
public class QueryGspListResponseDto extends GaiaSupplierGspinfo {

    private String supSiteName;

    private String supCode;

    /**
     * 业务员编号
     */
    private String gssCode;

    /**
     * 业务员姓名
     */
    private String gssName;

    /**
     * 身份证号
     */
    private String gssIdCard;

    /**
     * 电话
     */
    private String gssPhone;

    /**
     * 业务员人数
     */
    private Integer gssCount;

    /**
     * 许可证编号
     */
    private String supLicenceNo;
}
