package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaRetailPrice;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaRetailPriceMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.businessImport.PriceAdjustment;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.store.dto.price.ProductBasicListReponseDto;
import com.gov.purchase.module.store.dto.price.ProductBasicListRequestDto;
import com.gov.purchase.module.store.service.PriceService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class PriceAdjustmentImport extends BusinessImport {
    @Resource
    FeignService feignService;
    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Autowired
    PriceService priceService;
    @Resource
    GaiaRetailPriceMapper gaiaRetailPriceMapper;
    @Resource
    RedisClient redisClient;
    private final static String PRICEADJUSTMENTIMPORT_KEY = "PRICEADJUSTMENTIMPORT_KEY";
    private static String ALL_STRING = "全部";

    /**
     * @param map   数据
     * @param field 字段
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 价格类型
        if (field == null || field.get("type") == null) {
            throw new CustomResultException("请选择价格类型");
        }
        String type = field.get("type").toString();
        if (StringUtils.isBlank(type)) {
            throw new CustomResultException("请选择价格类型");
        }
        CommonEnum.GaiaRetailPrice_PrcClass price_prcClass = CommonEnum.GaiaRetailPrice_PrcClass.getGaiaRetailPrice_PrcClass(type);
        if (price_prcClass == null) {
            throw new CustomResultException("请选择正确的价格类型");
        }
        String client = feignService.getLoginInfo().getClient();
        List<String> errorList = new ArrayList<>();
        // 明细数据
        List<GaiaRetailPrice> list = new ArrayList<>();
        // 门店集合
        List<String> storeList = new ArrayList<>();
        String yymmdd = LocalDate.now().toString().replace("-", "");
        //  主键: 生成调价单号  YYYYMMDD+4位流水
        // 获取当前最大流水
        String currentMaxNo = gaiaRetailPriceMapper.selectMaxModifNo(client, yymmdd);
        String prcModfiyNo = yymmdd + "0001";
        if (currentMaxNo != null) {
            prcModfiyNo = yymmdd + (String.format("%04d", Integer.parseInt(currentMaxNo.substring(currentMaxNo.length() - 4)) + 1));
        }
        while (redisClient.exists(PRICEADJUSTMENTIMPORT_KEY + prcModfiyNo)) {
            prcModfiyNo = String.valueOf(NumberUtils.toLong(prcModfiyNo) + 1);
        }
        redisClient.set(PRICEADJUSTMENTIMPORT_KEY + prcModfiyNo, prcModfiyNo, 120);
        List<GaiaStoreData> gaiaStoreDataList = gaiaStoreDataMapper.getStoreDataListByClient(client);
        List<String> keyList = new ArrayList<>();
        List<Map<String, String>> params = new ArrayList<>();
        // 行数据处理
        boolean existsAll = false;
        for (Integer key : map.keySet()) {
            // 行数据
            PriceAdjustment priceAdjustment = (PriceAdjustment) map.get(key);
            Map<String, String> proMap = new HashMap<>();
            if (ALL_STRING.equals(priceAdjustment.getPrcStore())) {
                existsAll = true;
            }
            proMap.put("client", client);
            proMap.put("stoCode", priceAdjustment.getPrcStore());
            proMap.put("proSelfCode", priceAdjustment.getPrcProduct());
            params.add(proMap);
        }
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectByPrimaryKeyList(client, params);
        if (existsAll) {
            gaiaProductBusinessList = gaiaProductBusinessMapper.selectByClientList(params);
        }
        List<ProductBasicListRequestDto> productBasicListRequestDtoList = new ArrayList<>();
        // 行数据处理
        for (Integer key : map.keySet()) {
            // 行数据
            PriceAdjustment priceAdjustment = (PriceAdjustment) map.get(key);
            String rowKey = priceAdjustment.getPrcStore() + "_" + priceAdjustment.getPrcProduct();
            boolean exists = false;

            if (existsAll) {
                for (String s : keyList) {
                    if (s.endsWith(priceAdjustment.getPrcProduct())) {
                        exists = true;
                    }
                }
                if (exists) {
                    errorList.add(MessageFormat.format("第{0}行：数据重复", key + 1));
                    continue;
                } else {
                    keyList.add(rowKey);
                }
            } else {
                if (keyList.contains(rowKey)) {
                    errorList.add(MessageFormat.format("第{0}行：数据重复", key + 1));
                    continue;
                } else {
                    keyList.add(rowKey);
                }
            }
            if (ALL_STRING.equals(priceAdjustment.getPrcStore())) {
                // 商品编码
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessList.stream().filter(item ->
                        item.getProSelfCode().equals(priceAdjustment.getPrcProduct())
                ).findFirst().orElse(null);
                // 加盟商，门店下不存在此商品
                if (gaiaProductBusiness == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品编码"));
                    continue;
                }
                for (GaiaStoreData gaiaStoreData : gaiaStoreDataList) {
                    ProductBasicListRequestDto productBasicListRequestDto = new ProductBasicListRequestDto();
                    productBasicListRequestDto.setClient(client);
                    productBasicListRequestDto.setPrcClass(price_prcClass.getCode());
                    productBasicListRequestDto.setProSelfCode(priceAdjustment.getPrcProduct());
                    productBasicListRequestDto.setStoreCode(gaiaStoreData.getStoCode());
                    productBasicListRequestDtoList.add(productBasicListRequestDto);
                }
            } else {
                // 门店获取
                GaiaStoreData gaiaStoreData = gaiaStoreDataList.stream().filter(item -> item.getStoCode().equals(priceAdjustment.getPrcStore())).findFirst().orElse(null);
                // 门店号验证
                if (gaiaStoreData == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "门店号"));
                    continue;
                }

                // 商品编码
                GaiaProductBusiness gaiaProductBusiness = gaiaProductBusinessList.stream().filter(item ->
                        item.getProSite().equals(priceAdjustment.getPrcStore()) && item.getProSelfCode().equals(priceAdjustment.getPrcProduct())
                ).findFirst().orElse(null);
                // 加盟商，门店下不存在此商品
                if (gaiaProductBusiness == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0141.getMsg(), key + 1, "商品编码"));
                    continue;
                }
                ProductBasicListRequestDto productBasicListRequestDto = new ProductBasicListRequestDto();
                productBasicListRequestDto.setClient(client);
                productBasicListRequestDto.setPrcClass(price_prcClass.getCode());
                productBasicListRequestDto.setProSelfCode(priceAdjustment.getPrcProduct());
                productBasicListRequestDto.setStoreCode(priceAdjustment.getPrcStore());
                productBasicListRequestDtoList.add(productBasicListRequestDto);
            }
        }
        List<ProductBasicListReponseDto> productBasicListReponseDtoList = gaiaRetailPriceMapper.queryProductBasicListImport(client, price_prcClass.getCode(), productBasicListRequestDtoList);
        // 行数据处理
        for (Integer key : map.keySet()) {
            // 行数据
            PriceAdjustment priceAdjustment = (PriceAdjustment) map.get(key);
            // 旧数据
            List<ProductBasicListReponseDto> productBasicListReponseDtoListAll = productBasicListReponseDtoList.stream().filter(item ->
                    item.getProSelfCode().equals(priceAdjustment.getPrcProduct())
            ).collect(Collectors.toList());
            if (ALL_STRING.equals(priceAdjustment.getPrcStore())) {
                for (ProductBasicListReponseDto productBasicListReponseDto : productBasicListReponseDtoListAll) {
                    GaiaRetailPrice retailPrice = new GaiaRetailPrice();
                    BeanUtils.copyProperties(productBasicListReponseDto, retailPrice);
                    // 加盟商
                    retailPrice.setClient(client);
                    // 门店编码
                    retailPrice.setPrcStore(productBasicListReponseDto.getProSite());
                    // 商品编码
                    retailPrice.setPrcProduct(productBasicListReponseDto.getProSelfCode());
                    // 价格类型
                    retailPrice.setPrcClass(price_prcClass.getCode());
                    // 调价单号
                    retailPrice.setPrcModfiyNo(prcModfiyNo);
                    // 单价
                    retailPrice.setPrcAmount(new BigDecimal(priceAdjustment.getProAmount()));
                    // 修改前价格
                    retailPrice.setPrcAmountBefore(productBasicListReponseDto.getPrePrice());
                    // 单位
                    retailPrice.setPrcUnit(productBasicListReponseDto.getProUnit());
                    // 审批状态
                    retailPrice.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
                    // 有效期起
                    retailPrice.setPrcEffectDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    // 不允许积分
                    retailPrice.setPrcNoIntegral("0");
                    // 不允许会员卡打折
                    retailPrice.setPrcNoDiscount("0");
                    // 不允许积分兑换
                    retailPrice.setPrcNoExchange("0");
//                // 限购数量
//                retailPrice.setPrcLimitAmount(priceAdjustment.getPrcLimitAmount());
                    // 创建日期
                    retailPrice.setPrcCreateDate(DateUtils.getCurrentDateStr().replaceAll("-", ""));
                    // 创建时间
                    retailPrice.setPrcCreateTime(DateUtils.getCurrentTimeStr().replaceAll(":", ""));
                    // 调价来源
                    retailPrice.setPrcSource("0");
                    // 总部调价
                    retailPrice.setPrcHeadPrice(null);
                    list.add(retailPrice);
                }
            } else {
                ProductBasicListReponseDto productBasicListReponseDto = productBasicListReponseDtoListAll.stream().filter(item ->
                        item.getProSite().equals(priceAdjustment.getPrcStore())
                ).findFirst().orElse(null);
                if (productBasicListReponseDto != null) {
                    GaiaRetailPrice retailPrice = new GaiaRetailPrice();
                    BeanUtils.copyProperties(productBasicListReponseDto, retailPrice);
                    // 加盟商
                    retailPrice.setClient(client);
                    // 门店编码
                    retailPrice.setPrcStore(priceAdjustment.getPrcStore());
                    // 商品编码
                    retailPrice.setPrcProduct(productBasicListReponseDto.getProSelfCode());
                    // 价格类型
                    retailPrice.setPrcClass(price_prcClass.getCode());
                    // 调价单号
                    retailPrice.setPrcModfiyNo(prcModfiyNo);
                    // 单价
                    retailPrice.setPrcAmount(new BigDecimal(priceAdjustment.getProAmount()));
                    // 修改前价格
                    retailPrice.setPrcAmountBefore(productBasicListReponseDto.getPrePrice());
                    // 单位
                    retailPrice.setPrcUnit(productBasicListReponseDto.getProUnit());
                    // 审批状态
                    retailPrice.setPrcApprovalSuatus(CommonEnum.GspinfoStauts.APPROVAL.getCode());
                    // 有效期起
                    retailPrice.setPrcEffectDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    // 不允许积分
                    retailPrice.setPrcNoIntegral("0");
                    // 不允许会员卡打折
                    retailPrice.setPrcNoDiscount("0");
                    // 不允许积分兑换
                    retailPrice.setPrcNoExchange("0");
//                // 限购数量
//                retailPrice.setPrcLimitAmount(priceAdjustment.getPrcLimitAmount());
                    // 创建日期
                    retailPrice.setPrcCreateDate(DateUtils.getCurrentDateStr().replaceAll("-", ""));
                    // 创建时间
                    retailPrice.setPrcCreateTime(DateUtils.getCurrentTimeStr().replaceAll(":", ""));
                    // 调价来源
                    retailPrice.setPrcSource("0");
                    // 总部调价
                    retailPrice.setPrcHeadPrice(null);
                    list.add(retailPrice);
                }
            }
            // 门店
            if (!storeList.contains(priceAdjustment.getPrcStore())) {
                storeList.add(priceAdjustment.getPrcStore());
            }
        }
        // 验证不通过
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        // 明细数据
        if (CollectionUtils.isNotEmpty(list)) {
            int cnt = 0;
            // 500条数据
            for (int i = 0; i < list.size(); i += 500) {
                if (cnt * 500 + 500 > list.size()) {
                    List<GaiaRetailPrice> newList = list.subList(cnt * 500, list.size());
                    gaiaRetailPriceMapper.insertList(newList);
                } else {
                    List<GaiaRetailPrice> newList = list.subList(cnt * 500, cnt * 500 + 500);
                    gaiaRetailPriceMapper.insertList(newList);
                }
                cnt++;
            }
        }
        return ResultUtil.success();
    }
}
