package com.gov.purchase.module.replenishment.controller;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.MaintainProductsListQueryDTO;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.MaintainProductsListSaveDTO;
import com.gov.purchase.module.replenishment.dto.salesmanMaintainPro.SupplierSalesmanProDTO;
import com.gov.purchase.module.replenishment.service.SalesmanMaintainProService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;


@Api(tags = "供应商业务员维护商品")
@RestController
@RequestMapping("salesman-maintain-pro")
public class SalesmanMaintainProController {

    @Resource
    private SalesmanMaintainProService salesmanMaintainProService;


    @ApiOperation(value = "查询授权商品维护列表",response = MaintainProductsListQueryDTO.class)
    @GetMapping("getProductsList")
    public Result getProductsList(@ApiParam(value = "机构编码", required = true) @RequestParam String supSite
            , @ApiParam(value = "供应商编码", required = true) @RequestParam String supSelfCode
            , @ApiParam(value = " 业务员编码", required = true) @RequestParam String gssCode) {
        return ResultUtil.success(salesmanMaintainProService.getProductsList(supSite, supSelfCode, gssCode));
    }

    @ApiOperation("保存商品维护列表")
    @PostMapping("saveProductsList")
    public Result saveProductsList(@ApiParam(value = "保存授权商品维护列表DTO", required = true) @RequestBody MaintainProductsListSaveDTO maintainProductsListSaveDTO) {
        salesmanMaintainProService.saveProductsList(maintainProductsListSaveDTO);
        return ResultUtil.success("保存成功");
    }

    @ApiOperation(value = "模糊查询商品明细",response = SupplierSalesmanProDTO.class)
    @GetMapping("getProDetails")
    public Result getProDetails(@ApiParam(value = "机构编码", required = true) @RequestParam String supSite
            , @ApiParam(value = "商品模糊查询信息") @RequestParam(required = false) String proInfo
            , @ApiParam(value = "页大小") @RequestParam(required = false) Integer pageSize) {
        pageSize = ObjectUtils.isEmpty(pageSize) ? 100 : pageSize;
        return ResultUtil.success(salesmanMaintainProService.getProDetails(supSite, proInfo, pageSize));
    }

    @ApiOperation(value = "导入查询",response = SupplierSalesmanProDTO.class)
    @PostMapping("importProductsList")
    public Result importProductsList(@ApiParam(value = "机构编码", required = true) @RequestParam String supSite
            , @ApiParam(value = "导入对象", required = true) @RequestParam MultipartFile file) {
        return ResultUtil.success(salesmanMaintainProService.importProductsList(supSite, file));
    }

    @ApiOperation("导出")
    @GetMapping("exportProductsList")
    public void exportProductsList(@ApiParam(value = "机构编码", required = true) @RequestParam String supSite
            , @ApiParam(value = "供应商编码", required = true) @RequestParam String supSelfCode
            , @ApiParam(value = " 业务员编码", required = true) @RequestParam String gssCode) throws IOException {
        salesmanMaintainProService.exportProductsList(supSite, supSelfCode, gssCode);
    }

    @ApiOperation("导出模板")
    @GetMapping("exportTemplete")
    public void exportTemplete() throws IOException {
        salesmanMaintainProService.exportTemplete();
    }

    @ApiOperation("供应商业务员下拉框")
    @GetMapping("getSupplierSalesManList")
    public Result getSupplierSalesManList(@ApiParam(value = " 补货仓库", required = true) @RequestParam String dcCode) {
        return ResultUtil.success(salesmanMaintainProService.getSupplierSalesManList(dcCode));
    }


}

