package com.gov.purchase.module.customer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author zhoushuai
 * @date 2021/4/20 13:51
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class GaiaCustomerChangeKey {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String cusSite;

    /**
     * 客户自编码
     */
    private String cusSelfCode;
}

