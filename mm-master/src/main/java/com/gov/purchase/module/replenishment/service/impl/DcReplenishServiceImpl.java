package com.gov.purchase.module.replenishment.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.entity.Tencent;
import com.gov.purchase.common.kylin.KylinHttpBasic;
import com.gov.purchase.common.kylin.entity.KylinQuery;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.exception.ServiceException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.DictionaryParams;
import com.gov.purchase.module.base.service.DictionaryService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.base.service.exportTask.GaiaExportTaskService;
import com.gov.purchase.module.replenishment.dto.*;
import com.gov.purchase.module.replenishment.dto.dcReplenish.ProReplenishmentParams;
import com.gov.purchase.module.replenishment.dto.dcReplenish.StoProStock;
import com.gov.purchase.module.replenishment.dto.dcReplenish.StoreSale;
import com.gov.purchase.module.replenishment.service.DcReplenishService;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.10
 */
@Slf4j
@Service
public class DcReplenishServiceImpl implements DcReplenishService {

    @Resource
    private FeignService feignService;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaDcReplenishConfMapper gaiaDcReplenishConfMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaDcReplenishExcludMapper gaiaDcReplenishExcludMapper;
    @Resource
    private GaiaDcReplenishRatioMapper gaiaDcReplenishRatioMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private DictionaryService dictionaryService;
    @Resource
    private GaiaDcReplenishHMapper gaiaDcReplenishHMapper;
    @Resource
    private GaiaProductZdyMapper gaiaProductZdyMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaDcReplenishDMapper gaiaDcReplenishDMapper;
    @Resource
    private GaiaSupplierSalesmanMapper gaiaSupplierSalesmanMapper;
    @Resource
    private GaiaSdSaleDMapper gaiaSdSaleDMapper;
    @Resource
    private DcReplenishService dcReplenishService;
    @Resource
    private GaiaSdStockBatchMapper gaiaSdStockBatchMapper;
    @Resource
    private GaiaExportTaskService gaiaExportTaskService;
    @Resource
    private Tencent tencent;
    @Resource
    private GaiaExportTaskMapper gaiaExportTaskMapper;
    @Resource
    private KylinHttpBasic kylinHttpBasic;

    /**
     * DC补货 新
     *
     * @param dcCode
     * @return
     */
    @Override
    public Result getReplenishmentList(String dcCode) throws IllegalAccessException, IOException, InstantiationException {
        TokenUser user = feignService.getLoginInfo();
        // 补货参数
        GaiaDcReplenishConfKey gaiaDcReplenishConfKey = new GaiaDcReplenishConfKey();
        gaiaDcReplenishConfKey.setClient(user.getClient());
        gaiaDcReplenishConfKey.setGdrcSite(dcCode);
        GaiaDcReplenishConf gaiaDcReplenishConf = gaiaDcReplenishConfMapper.selectByPrimaryKey(gaiaDcReplenishConfKey);
        // 系数
        List<GaiaDcReplenishRatio> gaiaDcReplenishRatioList = gaiaDcReplenishRatioMapper.selectByDc(user.getClient(), dcCode);
        // 最小补货量品类
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinClassList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 1, 1);
        // 最小补货量商品
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinProList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 1, 2);
        // 最大补货量品类
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxClassList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 2, 1);
        // 最大补货量商品
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxProList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 2, 2);
        // 补货列表
        List<DcReplenishmentListResponseDto> list = gaiaDcDataMapper.getReplenishmentList2(user.getClient(), dcCode, gaiaDcReplenishConf);
        // 商品列表空
        if (CollectionUtils.isEmpty(list)) {
            return ResultUtil.success();
        }
        // 业务员
        List<GaiaSupplierSalesman> gaiaSupplierSalesmanList = gaiaSupplierSalesmanMapper.supplierSalesmanList(user.getClient(), dcCode, null);
        List<StoreSale> storeSaleList = this.getSaleList(user.getClient(), dcCode, gaiaDcReplenishConf);
        Map<String, List<GaiaSupplierSalesman>> gaiaSupplierSalesmanMap = gaiaSupplierSalesmanList.stream().collect(Collectors.groupingBy(t -> t.getSupSelfCode()));
        for (int i = list.size() - 1; i >= 0; i--) {
            DcReplenishmentListResponseDto entity = list.get(i);
            // 7天销量
            entity.setDaysSalesCountSeven(BigDecimal.ZERO);
            // 30天销量
            entity.setDaysSalesCount(BigDecimal.ZERO);
            // 90天销量
            entity.setDaysSalesCountNinety(BigDecimal.ZERO);
            // 单店月均销量
            entity.setMonthStoreSalesAvg(BigDecimal.ZERO);
            // 商品销量
            StoreSale storeSale = storeSaleList.stream().filter(t -> entity.getProSelfCode().equals(t.getGssdProId().trim())).findFirst().orElse(null);
            if (storeSale != null) {
                // 7
                if (storeSale.getStoreSaleQty7() != null) {
                    entity.setDaysSalesCountSeven(storeSale.getStoreSaleQty7());
                }
                // 30
                if (storeSale.getStoreSaleQty30() != null) {
                    entity.setDaysSalesCount(storeSale.getStoreSaleQty30());
                }
                // 90
                if (storeSale.getStoreSaleQty90() != null) {
                    entity.setDaysSalesCountNinety(storeSale.getStoreSaleQty90());
                }
            }
            // 单店月均销量
            if (entity.getStoreCount() == null || entity.getStoreCount().intValue() == 0) {
                entity.setMonthStoreSalesAvg(BigDecimal.ZERO);
            } else {
                // ROUND( IFNULL( F.STORE_SALE_QTY, 0 ) / E.STO_CNT, 2 )
                entity.setMonthStoreSalesAvg(entity.getDaysSalesCount().divide(new BigDecimal(entity.getStoreCount()), 2, RoundingMode.HALF_UP));
            }
            // 补货配置
            // test="gaiaDcReplenishConf != null
            if (gaiaDcReplenishConf != null) {
                // <if test="gaiaDcReplenishConf != null and gaiaDcReplenishConf.gdrcMsUncm != null and gaiaDcReplenishConf.gdrcMsUncm == 1">
                //    <if test="gaiaDcReplenishConf.gdrcMsUncmFixed != null and gaiaDcReplenishConf.gdrcMsUncmFixed == 1 and gaiaDcReplenishConf.gdrcMsUncmFixedNum != null">
                //        AND (CASE WHEN (INSTR(PRO_CLASS, '301') != 1 AND INSTR(PRO_CLASS, '302') != 1) THEN daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsUncmFixedNum} ELSE TRUE END)
                //    </if>
                //    <if test="gaiaDcReplenishConf.gdrcMsUncmAvg != null and gaiaDcReplenishConf.gdrcMsUncmAvg == 1 and gaiaDcReplenishConf.gdrcMsUncmAvgNum != null">
                //        AND (CASE WHEN (INSTR(PRO_CLASS, '301') != 1 AND INSTR(PRO_CLASS, '302') != 1) THEN daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsUncmAvgNum} * storeCount ELSE TRUE END)
                //    </if>
                //</if>
                // gaiaDcReplenishConf.gdrcMsUncm != null and gaiaDcReplenishConf.gdrcMsUncm == 1"
                if (gaiaDcReplenishConf.getGdrcMsUncm() != null && gaiaDcReplenishConf.getGdrcMsUncm().intValue() == 1) {
                    // test="gaiaDcReplenishConf.gdrcMsUncmFixed != null and gaiaDcReplenishConf.gdrcMsUncmFixed == 1 and gaiaDcReplenishConf.gdrcMsUncmFixedNum != null
                    if (gaiaDcReplenishConf.getGdrcMsUncmFixed() != null && gaiaDcReplenishConf.getGdrcMsUncmFixed().intValue() == 1 && gaiaDcReplenishConf.getGdrcMsUncmFixedNum() != null) {
                        // AND (CASE WHEN (INSTR(PRO_CLASS, '301') != 1 AND INSTR(PRO_CLASS, '302') != 1)
                        if (StringUtils.isNotBlank(entity.getProClass()) && !entity.getProClass().startsWith("301") && !entity.getProClass().startsWith("302")) {
                            // daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsUncmFixedNum
                            if (entity.getDaysSalesCount().compareTo(gaiaDcReplenishConf.getGdrcMsUncmFixedNum()) < 0) {
                                list.remove(i);
                                continue;
                            }
                        }
                    }
                    // gaiaDcReplenishConf.gdrcMsUncmAvg != null and gaiaDcReplenishConf.gdrcMsUncmAvg == 1 and gaiaDcReplenishConf.gdrcMsUncmAvgNum != null
                    if (gaiaDcReplenishConf.getGdrcMsUncmAvg() != null && gaiaDcReplenishConf.getGdrcMsUncmAvg().intValue() == 1 && gaiaDcReplenishConf.getGdrcMsUncmAvgNum() != null) {
                        // AND (CASE WHEN (INSTR(PRO_CLASS, '301') != 1 AND INSTR(PRO_CLASS, '302') != 1)
                        if (StringUtils.isNotBlank(entity.getProClass()) && !entity.getProClass().startsWith("301") && !entity.getProClass().startsWith("302")) {
                            // daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsUncmAvgNum} * storeCount
                            if (entity.getDaysSalesCount().compareTo(gaiaDcReplenishConf.getGdrcMsUncmAvgNum().multiply(new BigDecimal(entity.getStoreCount()))) < 0) {
                                list.remove(i);
                                continue;
                            }
                        }
                    }
                }
                //<if test="gaiaDcReplenishConf != null and gaiaDcReplenishConf.gdrcMsCm != null and gaiaDcReplenishConf.gdrcMsCm == 1">
                //    <if test="gaiaDcReplenishConf.gdrcMsCmFixed != null and gaiaDcReplenishConf.gdrcMsCmFixed == 1 and gaiaDcReplenishConf.gdrcMsCmFixedNum != null">
                //        AND (CASE WHEN (INSTR(PRO_CLASS, '301') = 1 OR INSTR(PRO_CLASS, '302') = 1) THEN daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsCmFixedNum} ELSE TRUE END)
                //    </if>
                //    <if test="gaiaDcReplenishConf.gdrcMsCmAvg != null and gaiaDcReplenishConf.gdrcMsCmAvg == 1 and gaiaDcReplenishConf.gdrcMsCmAvgNum != null">
                //        AND (CASE WHEN (INSTR(PRO_CLASS, '301') = 1 OR INSTR(PRO_CLASS, '302') = 1) THEN daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsCmAvgNum} * storeCount ELSE TRUE END)
                //    </if>
                //</if>
                // gaiaDcReplenishConf.gdrcMsCm != null and gaiaDcReplenishConf.gdrcMsCm == 1"
                if (gaiaDcReplenishConf.getGdrcMsCm() != null && gaiaDcReplenishConf.getGdrcMsCm().intValue() == 1) {
                    // gaiaDcReplenishConf.gdrcMsCmFixed != null and gaiaDcReplenishConf.gdrcMsCmFixed == 1 and gaiaDcReplenishConf.gdrcMsCmFixedNum != null
                    if (gaiaDcReplenishConf.getGdrcMsCmFixed() != null && gaiaDcReplenishConf.getGdrcMsCmFixed().intValue() == 1 && gaiaDcReplenishConf.getGdrcMsCmFixedNum() != null) {
                        // AND (CASE WHEN (INSTR(PRO_CLASS, '301') = 1 OR INSTR(PRO_CLASS, '302') = 1)
                        if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {
                            // daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsCmFixedNum}
                            if (entity.getDaysSalesCount().compareTo(gaiaDcReplenishConf.getGdrcMsCmFixedNum()) < 0) {
                                list.remove(i);
                                continue;
                            }
                        }
                    }
                    // gaiaDcReplenishConf.gdrcMsCmAvg != null and gaiaDcReplenishConf.gdrcMsCmAvg == 1 and gaiaDcReplenishConf.gdrcMsCmAvgNum != null
                    if (gaiaDcReplenishConf.getGdrcMsCmAvg() != null && gaiaDcReplenishConf.getGdrcMsCmAvg().intValue() == 1 && gaiaDcReplenishConf.getGdrcMsCmAvgNum() != null) {
                        // AND (CASE WHEN (INSTR(PRO_CLASS, '301') = 1 OR INSTR(PRO_CLASS, '302') = 1)
                        if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {
                            // daysSalesCount >= #{gaiaDcReplenishConf.gdrcMsCmAvgNum} * storeCount
                            if (entity.getDaysSalesCount().compareTo(gaiaDcReplenishConf.getGdrcMsCmAvgNum().multiply(new BigDecimal(entity.getStoreCount()))) < 0) {
                                list.remove(i);
                                continue;
                            }
                        }
                    }
                }
            }

            // 商品名称表示为描述
            entity.setProName(entity.getProDepict());
            // 配送平均天数  默认为 7天
            Integer q = 7;
            Integer m = 7;
            // 补货系数 默认 = 1
            BigDecimal ratio = null;
            // 门店数
            Integer storeCount = entity.getStoreCount();
            // 仓库库存
            BigDecimal dcStockTotal = entity.getDcStockTotal();
            // 30天销量
            BigDecimal daysSalesCount = entity.getDaysSalesCount();
            // 在途量
            BigDecimal trafficCount = entity.getTrafficCount();
            // 30对外批发量
            BigDecimal daysWholesaleCount = entity.getDaysWholesaleCount();
            // 30对外批发量 - 仓库库存 = X
            BigDecimal x = daysWholesaleCount.subtract(dcStockTotal);
            if (x.compareTo(BigDecimal.ONE) < 0) {
                x = BigDecimal.ZERO;
            }
            boolean delFlg = false;
            // 对外批发是否参与补货
            boolean gdrcWholesaleFlg = gaiaDcReplenishConf == null ? false : gaiaDcReplenishConf.getGdrcWholesaleFlg() == null ? false :
                    gaiaDcReplenishConf.getGdrcWholesaleFlg().intValue() == 1 ? true : false;
            if (!gdrcWholesaleFlg) {
                x = BigDecimal.ZERO;
            }

            // 按供应商实际送货天数计算
            if (gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcActualDeliveryDays() != null && gaiaDcReplenishConf.getGdrcActualDeliveryDays().intValue() == 1) {
                if (entity.getSupLeadTime() != null) {
                    q = entity.getSupLeadTime();
                }
            }
            if (CollectionUtils.isNotEmpty(gaiaDcReplenishRatioList)) {
                List<GaiaDcReplenishRatio> gaiaDcReplenishRatioTypeList = null;
                // Q<=7
                if (q.intValue() <= 7) {
                    gaiaDcReplenishRatioTypeList = gaiaDcReplenishRatioList.stream().filter(item -> item.getGdrrType().intValue() == 1).collect(Collectors.toList());
                } else if (q.intValue() > 7 && q.intValue() <= 15) {// 7<Q<=15
                    gaiaDcReplenishRatioTypeList = gaiaDcReplenishRatioList.stream().filter(item -> item.getGdrrType().intValue() == 2).collect(Collectors.toList());
                } else { // 15<Q
                    gaiaDcReplenishRatioTypeList = gaiaDcReplenishRatioList.stream().filter(item -> item.getGdrrType().intValue() == 3).collect(Collectors.toList());
                }
                // 设置系数
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishRatioTypeList)) {
                    GaiaDcReplenishRatio gaiaDcReplenishRatio = gaiaDcReplenishRatioTypeList.stream().filter(item -> (
                                    (item.getGdrrLnum() == null && item.getGdrrHnum() != null &&
                                            daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrHnum().intValue() * storeCount)) < 0) ||
                                            (item.getGdrrHnum() == null && item.getGdrrLnum() != null &&
                                                    daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrLnum() * storeCount)) >= 0) ||
                                            (daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrHnum() * storeCount)) < 0 &&
                                                    daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrLnum() * storeCount)) >= 0)
                            )
                    ).findFirst().orElse(null);
                    if (gaiaDcReplenishRatio != null) {
                        // 起补点
                        if (gaiaDcReplenishRatio.getGdrrLmake() != null) {
                            // 补货点系数（起补） (仓库库存+在途)/30销量*30
                            BigDecimal lReplenis = BigDecimal.ZERO;
                            if (daysSalesCount.compareTo(BigDecimal.ZERO) == 0) {
                                lReplenis = (dcStockTotal.add(trafficCount)).divide(new BigDecimal("0.0001"), 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(30));
                            } else {
                                lReplenis = (dcStockTotal.add(trafficCount)).divide(daysSalesCount, 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(30));
                            }
                            // > 起补点 不补
                            if (lReplenis.compareTo(BigDecimal.valueOf(gaiaDcReplenishRatio.getGdrrLmake())) > 0) {
                                if (gdrcWholesaleFlg && x.compareTo(BigDecimal.ZERO) > 0) {
                                    delFlg = true;
                                } else {
                                    list.remove(i);
                                    continue;
                                }
                            }
                        }
                        // 补货系数设置
                        if (gaiaDcReplenishRatio.getGdrrRatio() != null) {
                            ratio = gaiaDcReplenishRatio.getGdrrRatio();
                        }
                    }
                }
            }
            if (ratio == null) {
                ratio = getReplenishmentRatio(entity, new BigDecimal(1), new BigDecimal("0.9"), new BigDecimal("0.8"), new BigDecimal("0.7"), new BigDecimal("0.7"));
            }
            if (ratio == null) {
                ratio = BigDecimal.ONE;
            }

            // 系数
            if (!delFlg) {
                entity.setReplenishmentRatio(ratio);
            }
            // 按商品实际中包装量参与计算
            if (gaiaDcReplenishConf == null || gaiaDcReplenishConf.getGdrcActualMidPackage() == null || gaiaDcReplenishConf.getGdrcActualMidPackage().intValue() != 1) {
                entity.setProMidPackage(null);
            }
            if (StringUtils.isBlank(entity.getProMidPackage())) {
                // 中药饮片 默认100
                if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {
                    entity.setProMidPackage("100");
                } else { // 其他商品1
                    entity.setProMidPackage("1");
                }
            }

            // 仓库配送到所有门店的平均天数
            if (gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcDcDelivery() != null && gaiaDcReplenishConf.getGdrcDcDelivery().intValue() == 1 &&
                    gaiaDcReplenishConf.getGdrcDcDeliveryDays() != null) {
                m = gaiaDcReplenishConf.getGdrcDcDeliveryDays();
            } else {
                if (entity.getDcDeliveryDays() != null) {
                    m = entity.getDcDeliveryDays();
                }
            }
            // 建议补货数量
            BigDecimal replenishmentValue = new BigDecimal(0);
            if (!delFlg) {
                // 最终补货数量 = X ( S + M / 30 ) - T
                replenishmentValue = new BigDecimal(m).divide(new BigDecimal(30), 4, RoundingMode.HALF_UP);
                if (entity.getReplenishmentRatio() != null) {
                    replenishmentValue = replenishmentValue.add(entity.getReplenishmentRatio());
                }
                replenishmentValue = replenishmentValue.multiply(entity.getDaysSalesCount());
                // T = 在途量 + 大仓库存
                BigDecimal t = entity.getTrafficCount().add(entity.getDcStockTotal());
                replenishmentValue = replenishmentValue.subtract(t);
            } else {
                replenishmentValue = x;
            }

            /**
             * 1. 非中药（商品分类不为3开头）
             * 二舍八入原则：
             * 即补货量/中包量 余数<2 向下取整； >=8向上取整；介于中间的就不变。
             * 例：A商品中包装量10，补货量算出为21，21/10=2.1，余数是1，小于2，则向下取整，最终建议补货量为20。又若补货量算出为28，28/10=2.8，余数是8，向上取整，即补30个。
             * 2. 中药（商品分类为3开头）
             * 四舍五入原则：
             * 即补货量/中包量 余数<5向下取整； >=5向上取整
             */

            BigDecimal remain = replenishmentValue.divide(new BigDecimal(entity.getProMidPackage()), 4, RoundingMode.HALF_UP);
            BigDecimal[] remainArr = {remain.setScale(0, BigDecimal.ROUND_DOWN), remain.remainder(BigDecimal.ONE)};
            BigDecimal newValue = remainArr[0].multiply(new BigDecimal(entity.getProMidPackage()));
            if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {  // 中药
                if (remainArr[1].compareTo(new BigDecimal("0.5")) < 0) {
                    entity.setProposalReplenishment(newValue);
                } else {
                    newValue = newValue.add(new BigDecimal(entity.getProMidPackage()));
                    entity.setProposalReplenishment(newValue);
                }
            } else {  // 非中药
                if (remainArr[1].compareTo(new BigDecimal("0.2")) < 0) {
                    entity.setProposalReplenishment(newValue);
                } else if (remainArr[1].compareTo(new BigDecimal("0.8")) >= 0) {
                    newValue = newValue.add(new BigDecimal(entity.getProMidPackage()));
                    entity.setProposalReplenishment(newValue);
                } else {
                    entity.setProposalReplenishment(replenishmentValue.setScale(0, BigDecimal.ROUND_DOWN));
                }
            }

            // 获取建议补货量
            // 中药
            if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {
                entity.setReplenishmentValue(
                        entity.getProposalReplenishment().divide(new BigDecimal(entity.getProMidPackage()), 0, RoundingMode.CEILING)
                                .multiply(new BigDecimal(entity.getProMidPackage()))
                );
            } else {
                // 建议补货 / 中包装量 四舍五入 * 中包装量
                entity.setReplenishmentValue(
                        entity.getProposalReplenishment().divide(new BigDecimal(entity.getProMidPackage()), 4, RoundingMode.HALF_UP)
                                .multiply(new BigDecimal(entity.getProMidPackage())).setScale(0, RoundingMode.HALF_UP)
                );
            }
            if (!delFlg) {
                entity.setReplenishmentValue(entity.getReplenishmentValue().add(x).setScale(0, RoundingMode.HALF_UP));
            }

            if (entity.getReplenishmentValue() == null || entity.getReplenishmentValue().compareTo(BigDecimal.ZERO) <= 0) {
                list.remove(i);
                continue;
            }

            // 最小补货量排除 非中药
            if ((StringUtils.isBlank(entity.getProClass()) || (!entity.getProClass().startsWith("301") && !entity.getProClass().startsWith("302")))
                    && gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcMinReplenish() != null && gaiaDcReplenishConf.getGdrcMinReplenish().intValue() == 1) {
                Integer gdrcMinReplenishNum = gaiaDcReplenishConf.getGdrcMinReplenishNum();
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMinPro = null;
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMinClass = null;
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMinProList)) {
                    gaiaDcReplenishExcludDtoMinPro = gaiaDcReplenishExcludMinProList.stream().filter(item -> item.getGdreCode().equals(entity.getProSelfCode())).findFirst().orElse(null);
                    boolean flg = removeItem(gaiaDcReplenishExcludDtoMinPro, list, entity, i);
                    if (flg) {
                        continue;
                    }
                }
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMinClassList)) {
                    gaiaDcReplenishExcludDtoMinClass = gaiaDcReplenishExcludMinClassList.stream().filter(item -> item.getGdreCode().equals(entity.getProBigClass())).findFirst().orElse(null);
                    boolean flg = removeItem(gaiaDcReplenishExcludDtoMinClass, list, entity, i);
                    if (flg) {
                        continue;
                    }
                }
                if (gaiaDcReplenishExcludDtoMinPro == null && gaiaDcReplenishExcludDtoMinClass == null && gdrcMinReplenishNum != null) {
                    if (entity.getReplenishmentValue().compareTo(BigDecimal.valueOf(gdrcMinReplenishNum)) < 0) {
                        list.remove(i);
                        continue;
                    }
                }
            }
            // 最大补货数量设置 非中药
            if ((StringUtils.isBlank(entity.getProClass()) || (!entity.getProClass().startsWith("301") && !entity.getProClass().startsWith("302")))
                    && gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcMaxReplenish() != null && gaiaDcReplenishConf.getGdrcMaxReplenish().intValue() == 1) {
                Integer gdrcMaxReplenishNum = gaiaDcReplenishConf.getGdrcMaxReplenishNum();
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMaxPro = null;
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMaxClass = null;
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMaxProList)) {
                    gaiaDcReplenishExcludDtoMaxPro = gaiaDcReplenishExcludMaxProList.stream().filter(item -> item.getGdreCode().equals(entity.getProSelfCode())).findFirst().orElse(null);
                    boolean flg = setMax(gaiaDcReplenishExcludDtoMaxPro, entity);
                    if (flg) {
                        continue;
                    }
                }
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMaxClassList)) {
                    gaiaDcReplenishExcludDtoMaxClass = gaiaDcReplenishExcludMaxClassList.stream().filter(item -> item.getGdreCode().equals(entity.getProBigClass())).findFirst().orElse(null);
                    boolean flg = setMax(gaiaDcReplenishExcludDtoMaxClass, entity);
                    if (flg) {
                        continue;
                    }
                }
                if (gaiaDcReplenishExcludDtoMaxPro == null && gaiaDcReplenishExcludDtoMaxClass == null && gdrcMaxReplenishNum != null) {
                    if (entity.getReplenishmentValue().compareTo(BigDecimal.valueOf(gdrcMaxReplenishNum)) > 0) {
                        entity.setReplenishmentValue(BigDecimal.valueOf(gdrcMaxReplenishNum));
                        continue;
                    }
                }
            }
            // 业务员
            if (StringUtils.isNotBlank(entity.getLastSupplierId())) {
                List<GaiaSupplierSalesman> gaiaSupplierSalesmanList1 = gaiaSupplierSalesmanMap.get(entity.getLastSupplierId());
                if (CollectionUtils.isNotEmpty(gaiaSupplierSalesmanList1)) {
                    entity.setSalesList(gaiaSupplierSalesmanList1);
                    GaiaSupplierSalesman gaiaSupplierSalesman = gaiaSupplierSalesmanList1.stream().filter(
                            s -> s.getGssDefault() != null && s.getGssDefault().intValue() == 1).findFirst().orElse(null);
                    if (gaiaSupplierSalesman != null) {
                        entity.setPoSupplierSalesman(gaiaSupplierSalesman.getGssCode());
                        entity.setPoSupplierSalesmanName(gaiaSupplierSalesman.getGssName());
                    }
                }
            }
        }
        Result result = ResultUtil.success();
        result.setData(list);
        if (CollectionUtils.isNotEmpty(list)) {
            Map<String, List<String>> map = new HashMap<>();
            // 自定义字段1
            List<String> proZdy1List = list.stream().filter(t -> StringUtils.isNotBlank(t.getProZdy1())).map(DcReplenishmentListResponseDto::getProZdy1).distinct().collect(Collectors.toList());
            map.put("proZdy1List", proZdy1List);
            // 自定义字段2
            List<String> proZdy2List = list.stream().filter(t -> StringUtils.isNotBlank(t.getProZdy2())).map(DcReplenishmentListResponseDto::getProZdy2).distinct().collect(Collectors.toList());
            map.put("proZdy2List", proZdy2List);
            // 自定义字段3
            List<String> proZdy3List = list.stream().filter(t -> StringUtils.isNotBlank(t.getProZdy3())).map(DcReplenishmentListResponseDto::getProZdy3).distinct().collect(Collectors.toList());
            map.put("proZdy3List", proZdy3List);
            // 自定义字段4
            List<String> proZdy4List = list.stream().filter(t -> StringUtils.isNotBlank(t.getProZdy4())).map(DcReplenishmentListResponseDto::getProZdy4).distinct().collect(Collectors.toList());
            map.put("proZdy4List", proZdy4List);
            // 自定义字段5
            List<String> proZdy5List = list.stream().filter(t -> StringUtils.isNotBlank(t.getProZdy5())).map(DcReplenishmentListResponseDto::getProZdy5).distinct().collect(Collectors.toList());
            map.put("proZdy5List", proZdy5List);
            // 采购员
            List<String> proPurchaseRateList = list.stream().filter(t -> StringUtils.isNotBlank(t.getProPurchaseRate())).map(DcReplenishmentListResponseDto::getProPurchaseRate).distinct().collect(Collectors.toList());
            map.put("proPurchaseRateList", proPurchaseRateList);
            result.setWarning(map);
        }
        return result;
    }

    /**
     * 门店销量
     *
     * @param client
     * @param dcCode
     * @return
     * @throws IllegalAccessException
     * @throws IOException
     * @throws InstantiationException
     */
    private List<StoreSale> getSaleList(String client, String dcCode, GaiaDcReplenishConf gaiaDcReplenishConf) throws IllegalAccessException, IOException, InstantiationException {
        StringBuilder builder = new StringBuilder();
        builder.append(" select x.GSSD_PRO_ID,y.type7,y.STORE_SALE_QTY7,z.type30,z.STORE_SALE_QTY30,x.type90,x.STORE_SALE_QTY90 ");
        builder.append(" from  ");
        builder.append(" ( ");
        builder.append(" SELECT ");
        builder.append("   DT90.GCD_AREA_90 AS TYPE90, ");
        builder.append("   A90.GSSD_PRO_ID, ");
        builder.append("   ROUND(SUM(A90.GSSD_QTY), 2) AS STORE_SALE_QTY90 ");
        builder.append(" FROM ");
        builder.append("   GAIA_SD_SALE_D A90 ");
        builder.append("   INNER JOIN GAIA_STORE_DATA B90 ");
        builder.append("     ON A90.CLIENT = B90.CLIENT  AND A90.GSSD_BR_ID = B90.STO_CODE ");
        builder.append("   INNER JOIN V_GAIA_CAL_DT_AREA DT90 ON A90.GSSD_DATE = DT90.GCD_DATE ");
        builder.append(" WHERE ");
        builder.append("   A90.CLIENT = '" + client + "' ");
        builder.append("   AND DT90.GCD_AREA_90 = '90' ");
        if (gaiaDcReplenishConf == null || gaiaDcReplenishConf.getGdrcFranchiseeFlg() == null || gaiaDcReplenishConf.getGdrcFranchiseeFlg().intValue() != 1) {
            builder.append("   AND B90.STO_ATTRIBUTE != '3'  ");
        }
        builder.append("   AND B90.GSD_BHCK = '" + dcCode + "' ");
        builder.append(" GROUP BY ");
        builder.append("   A90.GSSD_PRO_ID, ");
        builder.append("   DT90.GCD_AREA_90 ");
        builder.append(" ) x ");
        builder.append(" left join ");
        builder.append(" ( ");
        builder.append(" SELECT ");
        builder.append("   DT7.GCD_AREA_7 AS TYPE7, ");
        builder.append("   A7.GSSD_PRO_ID, ");
        builder.append("   ROUND(SUM(A7.GSSD_QTY), 2) AS STORE_SALE_QTY7 ");
        builder.append(" FROM ");
        builder.append("   GAIA_SD_SALE_D A7 ");
        builder.append("   INNER JOIN GAIA_STORE_DATA B7 ON A7.CLIENT = B7.CLIENT ");
        builder.append("   AND A7.GSSD_BR_ID = B7.STO_CODE ");
        builder.append("   INNER JOIN V_GAIA_CAL_DT_AREA DT7 ON A7.GSSD_DATE = DT7.GCD_DATE ");
        builder.append(" WHERE ");
        builder.append("   A7.CLIENT = '" + client + "' ");
        builder.append("   AND DT7.GCD_AREA_7 = '7' ");
        if (gaiaDcReplenishConf == null || gaiaDcReplenishConf.getGdrcFranchiseeFlg() == null || gaiaDcReplenishConf.getGdrcFranchiseeFlg().intValue() != 1) {
            builder.append("   AND B7.STO_ATTRIBUTE != '3'  ");
        }
        builder.append("   AND B7.GSD_BHCK = '" + dcCode + "' ");
        builder.append(" GROUP BY ");
        builder.append("   A7.GSSD_PRO_ID, ");
        builder.append("   DT7.GCD_AREA_7 ");
        builder.append(" ) y ");
        builder.append(" on x.GSSD_PRO_ID=y.GSSD_PRO_ID ");
        builder.append(" left join ( ");
        builder.append(" SELECT ");
        builder.append("   DT30.GCD_AREA_30 AS TYPE30, ");
        builder.append("   A30.GSSD_PRO_ID, ");
        builder.append("   ROUND(SUM(A30.GSSD_QTY), 2) AS STORE_SALE_QTY30 ");
        builder.append(" FROM ");
        builder.append("   GAIA_SD_SALE_D A30 ");
        builder.append("   INNER JOIN GAIA_STORE_DATA B30 ON A30.CLIENT = B30.CLIENT ");
        builder.append("   AND A30.GSSD_BR_ID = B30.STO_CODE ");
        builder.append("   INNER JOIN V_GAIA_CAL_DT_AREA DT30 ON A30.GSSD_DATE = DT30.GCD_DATE ");
        builder.append(" WHERE ");
        builder.append("   A30.CLIENT = '" + client + "' ");
        builder.append("   AND DT30.GCD_AREA_30 = '30' ");
        if (gaiaDcReplenishConf == null || gaiaDcReplenishConf.getGdrcFranchiseeFlg() == null || gaiaDcReplenishConf.getGdrcFranchiseeFlg().intValue() != 1) {
            builder.append("   AND B30.STO_ATTRIBUTE != '3'  ");
        }
        builder.append("   AND B30.GSD_BHCK = '" + dcCode + "' ");
        builder.append(" GROUP BY ");
        builder.append("   A30.GSSD_PRO_ID, ");
        builder.append("   DT30.GCD_AREA_30 ");
        builder.append(" ) z ");
        builder.append(" on x.GSSD_PRO_ID=z.GSSD_PRO_ID ");
        builder.append(" order by x.GSSD_PRO_ID ");

        KylinQuery kylinQuery = new KylinQuery(builder.toString(), KylinQuery.C_D_STORE);
        List<StoreSale> list = kylinHttpBasic.queryList(kylinQuery, StoreSale.class);
        return list;
    }

    /**
     * 最大补货量设置
     *
     * @param gaiaDcReplenishExcludDto
     * @param entity
     * @return
     */
    private boolean setMax(GaiaDcReplenishExcludDto gaiaDcReplenishExcludDto, DcReplenishmentListResponseDto entity) {
        if (gaiaDcReplenishExcludDto != null) {
            if (gaiaDcReplenishExcludDto.getGdreReplenishNum() != null) {
                if (entity.getReplenishmentValue().compareTo(BigDecimal.valueOf(gaiaDcReplenishExcludDto.getGdreReplenishNum())) > 0) {
                    entity.setReplenishmentValue(BigDecimal.valueOf(gaiaDcReplenishExcludDto.getGdreReplenishNum()));
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 最小补货量删除
     *
     * @param gaiaDcReplenishExcludDto
     * @param list
     * @param entity
     * @param i
     * @return
     */
    private boolean removeItem(GaiaDcReplenishExcludDto gaiaDcReplenishExcludDto, List<DcReplenishmentListResponseDto> list,
                               DcReplenishmentListResponseDto entity, int i) {
        if (gaiaDcReplenishExcludDto != null) {
            if (gaiaDcReplenishExcludDto.getGdreReplenishNum() != null) {
                if (entity.getReplenishmentValue().compareTo(BigDecimal.valueOf(gaiaDcReplenishExcludDto.getGdreReplenishNum())) < 0) {
                    list.remove(i);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 补货参数保存
     *
     * @param replenishConf
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveReplenishConf(ReplenishConf replenishConf) {
        if (StringUtils.isBlank(replenishConf.getGdrcSite())) {
            throw new CustomResultException("请选择配送中心");
        }
        TokenUser user = feignService.getLoginInfo();
        // 删除原数据
        GaiaDcReplenishConfKey gaiaDcReplenishConfKey = new GaiaDcReplenishConfKey();
        gaiaDcReplenishConfKey.setClient(user.getClient());
        gaiaDcReplenishConfKey.setGdrcSite(replenishConf.getGdrcSite());
        gaiaDcReplenishConfMapper.deleteByPrimaryKey(gaiaDcReplenishConfKey);
        gaiaDcReplenishExcludMapper.deleteByDc(user.getClient(), replenishConf.getGdrcSite());
        gaiaDcReplenishRatioMapper.deleteByDc(user.getClient(), replenishConf.getGdrcSite());
        // 新增
        // 配置参数
        GaiaDcReplenishConf gaiaDcReplenishConf = new GaiaDcReplenishConf();
        BeanUtils.copyProperties(replenishConf, gaiaDcReplenishConf);
        gaiaDcReplenishConf.setClient(user.getClient());
        gaiaDcReplenishConfMapper.insertSelective(gaiaDcReplenishConf);
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludList = new ArrayList<>();
        // 最小补货品类
        initExclud(gaiaDcReplenishExcludList, replenishConf.getGaiaDcReplenishExcludMinClassList(), replenishConf.getGdrcSite(), 1, 1);
        // 最小补货商品
        initExclud(gaiaDcReplenishExcludList, replenishConf.getGaiaDcReplenishExcludMinProList(), replenishConf.getGdrcSite(), 1, 2);
        // 最大补货品类
        initExclud(gaiaDcReplenishExcludList, replenishConf.getGaiaDcReplenishExcludMaxClassList(), replenishConf.getGdrcSite(), 2, 1);
        // 最大补货商品
        initExclud(gaiaDcReplenishExcludList, replenishConf.getGaiaDcReplenishExcludMaxProList(), replenishConf.getGdrcSite(), 2, 2);
        if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludList)) {
            gaiaDcReplenishExcludMapper.insertBatch(gaiaDcReplenishExcludList);
        }
        List<GaiaDcReplenishRatio> gaiaDcReplenishRatioList = new ArrayList<>();
        initRatio(gaiaDcReplenishRatioList, replenishConf.getGaiaDcReplenishRatioList(), replenishConf.getGdrcSite());
        if (CollectionUtils.isNotEmpty(gaiaDcReplenishRatioList)) {
            gaiaDcReplenishRatioMapper.insertBatch(gaiaDcReplenishRatioList);
        }
    }

    /**
     * 补货参数获取
     *
     * @param gdrcSite
     * @return
     */
    @Override
    public Result getReplenishConf(String gdrcSite) {
        TokenUser user = feignService.getLoginInfo();
        ReplenishConf replenishConf = new ReplenishConf();
        GaiaDcReplenishConfKey gaiaDcReplenishConfKey = new GaiaDcReplenishConfKey();
        gaiaDcReplenishConfKey.setClient(user.getClient());
        gaiaDcReplenishConfKey.setGdrcSite(gdrcSite);
        GaiaDcReplenishConf gaiaDcReplenishConf = gaiaDcReplenishConfMapper.selectByPrimaryKey(gaiaDcReplenishConfKey);
        if (gaiaDcReplenishConf == null) {
            return ResultUtil.success(replenishConf);
        }
        BeanUtils.copyProperties(gaiaDcReplenishConf, replenishConf);

        // 排除数据
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinClassList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), replenishConf.getGdrcSite(), 1, 1);
        replenishConf.setGaiaDcReplenishExcludMinClassList(gaiaDcReplenishExcludMinClassList);
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinProList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), replenishConf.getGdrcSite(), 1, 2);
        replenishConf.setGaiaDcReplenishExcludMinProList(gaiaDcReplenishExcludMinProList);
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxClassList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), replenishConf.getGdrcSite(), 2, 1);
        replenishConf.setGaiaDcReplenishExcludMaxClassList(gaiaDcReplenishExcludMaxClassList);
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxProList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), replenishConf.getGdrcSite(), 2, 2);
        replenishConf.setGaiaDcReplenishExcludMaxProList(gaiaDcReplenishExcludMaxProList);

        // 系数
        List<GaiaDcReplenishRatio> gaiaDcReplenishRatioList = gaiaDcReplenishRatioMapper.selectByDc(user.getClient(), replenishConf.getGdrcSite());
        replenishConf.setGaiaDcReplenishRatioList(gaiaDcReplenishRatioList);
        return ResultUtil.success(replenishConf);
    }

    @Override
    public Result getStoreCont(String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaStoreData> gaiaStoreDataList = gaiaStoreDataMapper.getStoreCont(user.getClient(), dcCode);
        return ResultUtil.success(gaiaStoreDataList.size());
    }

    @Override
    public Result exportReplenishmentNewList(String dcCode) {
        try {
            List<DcReplenishmentListResponseDto> batchList = (List<DcReplenishmentListResponseDto>) getReplenishmentList(dcCode).getData();
            // 导出数据
            List<List<String>> dcList = new ArrayList<>();
            initData(batchList, dcList);
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(dcHead);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dcList);
                    }},
                    new ArrayList<String>() {{
                        add("DC补货列表数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Objects.requireNonNull(workbook).write(bos);
            return cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        } catch (Exception e) {
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * DC补货系数初始化
     *
     * @param gaiaDcReplenishConfKey
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result initDcReplenishParams(GaiaDcReplenishConfKey gaiaDcReplenishConfKey) {
        if (StringUtils.isBlank(gaiaDcReplenishConfKey.getClient())) {
            throw new CustomResultException("加盟商不能为空");
        }
        if (StringUtils.isBlank(gaiaDcReplenishConfKey.getGdrcSite())) {
            throw new CustomResultException("仓库编号不能为空");
        }
        GaiaDcReplenishConf gaiaDcReplenishConf = gaiaDcReplenishConfMapper.selectByPrimaryKey(gaiaDcReplenishConfKey);
        if (gaiaDcReplenishConf == null) {
            gaiaDcReplenishConf = new GaiaDcReplenishConf();
            gaiaDcReplenishConf.setClient(gaiaDcReplenishConfKey.getClient());
            gaiaDcReplenishConf.setGdrcSite(gaiaDcReplenishConfKey.getGdrcSite());
            gaiaDcReplenishConf.setGdrcDcDelivery(1);
            gaiaDcReplenishConf.setGdrcDcDeliveryDays(7);
            gaiaDcReplenishConf.setGdrcCloseDate(1);
            gaiaDcReplenishConf.setGdrcCloseDateDays(10);
            gaiaDcReplenishConfMapper.insertSelective(gaiaDcReplenishConf);
        }
        List<GaiaDcReplenishRatio> ratioList = gaiaDcReplenishRatioMapper.selectByDc(gaiaDcReplenishConfKey.getClient(), gaiaDcReplenishConfKey.getGdrcSite());
        if (CollectionUtils.isEmpty(ratioList)) {
            List<GaiaDcReplenishRatio> gaiaDcReplenishRatioList = new ArrayList<>();
            for (int i = 1; i < 4; i++) {
                gaiaDcReplenishRatioList.addAll(initRatio(gaiaDcReplenishConfKey.getClient(), gaiaDcReplenishConfKey.getGdrcSite(), i));
            }
            gaiaDcReplenishRatioMapper.insertBatch(gaiaDcReplenishRatioList);
        }
        return ResultUtil.success();
    }

    /**
     * 商品补货数据表
     *
     * @param pageSize
     * @param pageNum
     * @param proPurchaseRate
     * @return
     */
    @Override
    public Result getProReplenishmentList(Integer pageSize, Integer pageNum, String dcCode, String stoCode,
                                          String proSelfCode, String proClass, String lastSupp, String recSupp,
                                          String proZdy1, String proZdy2, String proZdy3, String proZdy4,
                                          String proZdy5, String proSlaeClass, String proSclass,
                                          Integer proNoPurchase, Integer proPosition,
                                          List<ProReplenishmentParams> proReplenishmentParamsList,
                                          String salesVolumeStartDate, String salesVolumeEndDate, String proPurchaseRate, Integer excDea, List<String> storeCodes) {
        TokenUser user = feignService.getLoginInfo();
        if (StringUtils.isBlank(salesVolumeEndDate)) {
            salesVolumeEndDate = LocalDate.now().format(DateUtils.DATE_FORMATTER_YYMMDD);
        }
        if (StringUtils.isBlank(salesVolumeStartDate)) {
            salesVolumeStartDate = LocalDate.now().minusDays(60).format(DateUtils.DATE_FORMATTER_YYMMDD);
        }
        // 分頁查詢
        PageHelper.startPage(pageNum, pageSize);
        List<DcReplenishmentListResponseDto> list = gaiaDcDataMapper.getProReplenishmentList(user.getClient(), dcCode, stoCode, proSelfCode, proClass, lastSupp, recSupp,
                StringUtils.isBlank(proZdy1) ? null : Arrays.asList(proZdy1.split(",")),
                StringUtils.isBlank(proZdy2) ? null : Arrays.asList(proZdy2.split(",")),
                StringUtils.isBlank(proZdy3) ? null : Arrays.asList(proZdy3.split(",")),
                StringUtils.isBlank(proZdy4) ? null : Arrays.asList(proZdy4.split(",")),
                StringUtils.isBlank(proZdy5) ? null : Arrays.asList(proZdy5.split(",")),
                proSlaeClass, proSclass, proNoPurchase, proPosition, proReplenishmentParamsList,
                salesVolumeStartDate, salesVolumeEndDate, proPurchaseRate, excDea, storeCodes);
        PageInfo<DcReplenishmentListResponseDto> pageInfo = new PageInfo<>(list);
        if (CollectionUtils.isNotEmpty(pageInfo.getList())) {
            List<String> proSelfCodeList = pageInfo.getList().stream().map(DcReplenishmentListResponseDto::getProSelfCode).collect(Collectors.toList());
            List<DcReplenishmentListResponseDto> saleList = gaiaSdSaleDMapper.selectSaleProByDays(user.getClient(), salesVolumeStartDate, salesVolumeEndDate, proSelfCodeList, dcCode, storeCodes);
            // 销量
            pageInfo.getList().forEach(s -> {
                s.setDaysSalesCountSeven(BigDecimal.ZERO);
                s.setDaysSalesCount(BigDecimal.ZERO);
                s.setDaysSalesCountNinety(BigDecimal.ZERO);
                s.setXxStoreSaleQty(BigDecimal.ZERO);
                s.setXxStoCount(BigDecimal.ZERO);
                s.setXxStoCountPercentage(BigDecimal.ZERO);
                // 7
                DcReplenishmentListResponseDto seven = saleList.stream().filter(t ->
                        t.getType().equals("7") && s.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (seven != null) {
                    s.setDaysSalesCountSeven(seven.getStoreSaleQty());
                }
                // 30
                DcReplenishmentListResponseDto thirty = saleList.stream().filter(t ->
                        t.getType().equals("30") && s.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (thirty != null) {
                    s.setDaysSalesCount(thirty.getStoreSaleQty());
                }
                // 90
                DcReplenishmentListResponseDto ninety = saleList.stream().filter(t ->
                        t.getType().equals("90") && s.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (ninety != null) {
                    s.setDaysSalesCountNinety(ninety.getStoreSaleQty());
                }
                // XX
                DcReplenishmentListResponseDto xx = saleList.stream().filter(t ->
                        t.getType().equals("X") && s.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (xx != null) {
                    s.setXxStoreSaleQty(xx.getStoreSaleQty());
                    s.setXxStoCount(xx.getStoCount());
                    if (s.getStoreCount() != null && s.getStoreCount().intValue() != 0) {
                        s.setXxStoCountPercentage(xx.getStoCount().divide(NumberUtils.createBigDecimal(s.getStoreCount().toString()), 4, RoundingMode.HALF_UP));
                    }
                }
                if (ObjectUtils.isNotEmpty(s.getXxStoreSaleQty()) && ObjectUtils.isNotEmpty(s.getDcStockTotal())) {
                    s.setRangeSalesDcQty(s.getXxStoreSaleQty().subtract(s.getDcStockTotal()));
                }
                if (s.getStockTotal() == null || s.getStockTotal().compareTo(BigDecimal.ZERO) == 0) {
                    s.setSaleDays(BigDecimal.ZERO);
                } else {
                    s.setSaleDays(new BigDecimal(9999));
                    // 可销天数 = 总库存/（30天门店销售+30天批发销售）*30
                    BigDecimal sale30 = add(s.getDaysSalesCount(), s.getDaysWholesaleCount());
                    if (sale30.compareTo(BigDecimal.ZERO) != 0) {
                        s.setSaleDays(s.getStockTotal().divide(sale30, 1, RoundingMode.CEILING).multiply(new BigDecimal(30)));
                    }
                }
            });
        }
        return ResultUtil.success(pageInfo);
    }

    /**
     * 数值相加
     *
     * @param decimals
     * @return
     */
    private BigDecimal add(BigDecimal... decimals) {
        if (decimals == null || decimals.length == 0) {
            return BigDecimal.ZERO;
        }
        BigDecimal result = BigDecimal.ZERO;
        for (BigDecimal decimal : decimals) {
            if (decimal != null) {
                result = result.add(decimal);
            }
        }
        return result;
    }

    /**
     * 商品补货数据表 导出
     *
     * @param dcCode
     * @param proSelfCode
     * @param proClass
     * @param lastSupp
     * @param recSupp
     * @param proPurchaseRate
     * @return
     */
    @Override
    public void getProReplenishmentListExport(String dcCode, String stoCode, String proSelfCode, String proClass, String lastSupp, String recSupp,
                                              String proZdy1, String proZdy2, String proZdy3, String proZdy4, String proZdy5,
                                              String proSlaeClass, String proSclass, Integer proNoPurchase, Integer proPosition,
                                              List<ProReplenishmentParams> proReplenishmentParamsList, String salesVolumeStartDate,
                                              String salesVolumeEndDate, String proPurchaseRate, Integer excDea, List<String> storeCodes,
                                              GaiaExportTask gaiaExportTask) {
        try {
            TokenUser user = feignService.getLoginInfo();
            if (StringUtils.isBlank(salesVolumeEndDate)) {
                salesVolumeEndDate = LocalDate.now().format(DateUtils.DATE_FORMATTER_YYMMDD);
            }
            if (StringUtils.isBlank(salesVolumeStartDate)) {
                salesVolumeStartDate = LocalDate.now().minusDays(60).format(DateUtils.DATE_FORMATTER_YYMMDD);
            }
            long days = LocalDate.parse(salesVolumeEndDate, DateUtils.DATE_FORMATTER_YYMMDD).toEpochDay()
                    - LocalDate.parse(salesVolumeStartDate, DateUtils.DATE_FORMATTER_YYMMDD).toEpochDay();

            // 分頁查詢
            List<DcReplenishmentListResponseDto> list = gaiaDcDataMapper.getProReplenishmentList(user.getClient(), dcCode, stoCode, proSelfCode, proClass, lastSupp, recSupp,
                    StringUtils.isBlank(proZdy1) ? null : Arrays.asList(proZdy1.split(",")),
                    StringUtils.isBlank(proZdy2) ? null : Arrays.asList(proZdy2.split(",")),
                    StringUtils.isBlank(proZdy3) ? null : Arrays.asList(proZdy3.split(",")),
                    StringUtils.isBlank(proZdy4) ? null : Arrays.asList(proZdy4.split(",")),
                    StringUtils.isBlank(proZdy5) ? null : Arrays.asList(proZdy5.split(",")),
                    proSlaeClass, proSclass, proNoPurchase, proPosition, proReplenishmentParamsList,
                    salesVolumeStartDate, salesVolumeEndDate, proPurchaseRate, excDea, storeCodes);
            List<List<Object>> dataList = new ArrayList<>();
            List<DcReplenishmentListResponseDto> saleList = null;
            if (CollectionUtils.isNotEmpty(list)) {
                List<String> proSelfCodeList = list.stream().map(DcReplenishmentListResponseDto::getProSelfCode).collect(Collectors.toList());
                saleList = gaiaSdSaleDMapper.selectSaleProByDays(user.getClient(), salesVolumeStartDate, salesVolumeEndDate, proSelfCodeList, dcCode, storeCodes);
            }
            // 配送平均天数  默认为 3天
            for (int i = 0; i < list.size(); i++) {
                DcReplenishmentListResponseDto dto = list.get(i);
                // 销售
                dto.setDaysSalesCountSeven(BigDecimal.ZERO);
                dto.setDaysSalesCount(BigDecimal.ZERO);
                dto.setDaysSalesCountNinety(BigDecimal.ZERO);
                dto.setXxStoreSaleQty(BigDecimal.ZERO);
                dto.setXxStoCount(BigDecimal.ZERO);
                dto.setXxStoCountPercentage(BigDecimal.ZERO);
                // 7
                DcReplenishmentListResponseDto seven = saleList.stream().filter(t ->
                        t.getType().equals("7") && dto.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (seven != null) {
                    dto.setDaysSalesCountSeven(seven.getStoreSaleQty());
                }
                // 30
                DcReplenishmentListResponseDto thirty = saleList.stream().filter(t ->
                        t.getType().equals("30") && dto.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (thirty != null) {
                    dto.setDaysSalesCount(thirty.getStoreSaleQty());
                }
                // 90
                DcReplenishmentListResponseDto ninety = saleList.stream().filter(t ->
                        t.getType().equals("90") && dto.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (ninety != null) {
                    dto.setDaysSalesCountNinety(ninety.getStoreSaleQty());
                }
                // XX
                DcReplenishmentListResponseDto xx = saleList.stream().filter(t ->
                        t.getType().equals("X") && dto.getProSelfCode().equals(t.getGssdProId())).findFirst().orElse(null);
                if (xx != null) {
                    dto.setXxStoreSaleQty(xx.getStoreSaleQty());
                    dto.setXxStoCount(xx.getStoCount());
                    if (dto.getStoreCount() != null && dto.getStoreCount().intValue() != 0) {
                        dto.setXxStoCountPercentage(xx.getStoCount().divide(NumberUtils.createBigDecimal(dto.getStoreCount().toString()), 4, RoundingMode.HALF_UP));
                    }
                }
                if (dto.getStockTotal() == null || dto.getStockTotal().compareTo(BigDecimal.ZERO) == 0) {
                    dto.setSaleDays(BigDecimal.ZERO);
                } else {
                    dto.setSaleDays(new BigDecimal(9999));
                    // 可销天数 = 总库存/（30天门店销售+30天批发销售）*30
                    BigDecimal sale30 = add(dto.getDaysSalesCount(), dto.getDaysWholesaleCount());
                    if (sale30.compareTo(BigDecimal.ZERO) != 0) {
                        dto.setSaleDays(dto.getStockTotal().divide(sale30, 1, RoundingMode.CEILING).multiply(new BigDecimal(30)));
                    }
                }

                // 打印行
                List<Object> item = new ArrayList<>();
                // 商品编码
                item.add(dto.getProSelfCode());
                // 商品名称
                item.add(dto.getProName());
                // 商品大类
                item.add(dto.getProBigClassName());
                // 规格
                item.add(dto.getProSpecs());
                // 生产厂家
                item.add(dto.getProFactoryName());
                // 大仓库存
                item.add(dto.getDcStockTotal());
                // 可用数量
                item.add(dto.getWmKysl());
                // 门店库存
                item.add(dto.getStoreStockTotal());
                // 总库存
                item.add(dto.getStockTotal());
                // 7天门店销量
                item.add(dto.getDaysSalesCountSeven());
                // 30天门店销量
                item.add(dto.getDaysSalesCount());
                // 90天门店销量
                item.add(dto.getDaysSalesCountNinety());
                // 30天对外批发量
                item.add(dto.getDaysWholesaleCount());
                // 在途量
                item.add(dto.getTrafficCount());
                // XX天门店销售量
                item.add(ObjectUtils.isEmpty(dto.getXxStoreSaleQty()) ? null : dto.getXxStoreSaleQty());
                // XX天动销门店数
                item.add(ObjectUtils.isEmpty(dto.getXxStoCount()) ? null : dto.getXxStoCount());
                // XX天动销率
                item.add(dto.getXxStoCountPercentage() == null ? "" : dto.getXxStoCountPercentage().multiply(BigDecimal.valueOf(100)).toString() + "%");
                // 区间销售-仓库库存
                item.add(ObjectUtils.isNotEmpty(dto.getXxStoreSaleQty()) && ObjectUtils.isNotEmpty(dto.getDcStockTotal()) ? dto.getXxStoreSaleQty().subtract(dto.getDcStockTotal()) : null);
                // 可销天数
                item.add(dto.getSaleDays());
                // 单位
                item.add(dto.getProUnit());
                // 中包装量
                item.add(dto.getProMidPackage());
                // 大包装
                item.add(dto.getProBigPackage());
                // 最后进货日期
                item.add(dto.getLastPurchaseDate());
                // 最后一次进货供应商
                item.add(dto.getLastSupplier());
                // 结款方式
                item.add(dto.getSupPayTerm());
                // 末次进货价
                item.add(dto.getLastPurchasePrice());
                // 末次进货数量
                item.add(dto.getLastReceiveQty());
                // 补货门店数
                item.add(dto.getStoreCount());
                // 库存门店数
                item.add(ObjectUtils.isEmpty(dto.getStoCountHasStock()) ? null : dto.getStoCountHasStock());
                // 铺货率
                item.add(dto.getStoCountHasStockPercentage() == null ? "" : dto.getStoCountHasStockPercentage().multiply(BigDecimal.valueOf(100)).toString() + "%");
                // 商品分类
                item.add(dto.getProClassName());
                // 零售价
                item.add(dto.getGsppPriceNormal());
                // 会员价
                item.add(dto.getGsphsPrice());
                // 会员日价
                item.add(dto.getGsphpPrice());
                // 最新进价毛利
                item.add(dto.getGrossProfit() == null ? "" : dto.getGrossProfit().toString() + "%");
                // 自定义字段1
                item.add(dto.getProZdy1());
                // 自定义字段2
                item.add(dto.getProZdy2());
                // 自定义字段3
                item.add(dto.getProZdy3());
                // 自定义字段4
                item.add(dto.getProZdy4());
                // 自定义字段5
                item.add(dto.getProZdy5());
                // 备注
                item.add(dto.getProBeizhu());
                // 经营管理类别
                item.add(dto.getProJygllb());
                // 档案号
                item.add(dto.getProDah());
                // 采购员
                item.add(dto.getProPurchaseRate());
                // 批准文号
                item.add(dto.getProRegisterNo());
                // 国际条形码
                item.add(dto.getProBarcode());
                // 销售级别
                item.add(dto.getProSlaeClass());
                // 商品定位
                item.add(dto.getProPosition());
                // 禁止采购
                if (CommonEnum.NoYesStatus.NO.getCode().equals(dto.getProNoPurchase())) {
                    item.add(CommonEnum.NoYesStatus.NO.getName());
                } else if (CommonEnum.NoYesStatus.YES.getCode().equals(dto.getProNoPurchase())) {
                    item.add(CommonEnum.NoYesStatus.YES.getName());
                } else {
                    item.add("");
                }
                // 税率
                item.add(dto.getTaxCodename());
                // 商品状态
                if (CommonEnum.GoodsStauts.GOODSNORMAL.getCode().equals(dto.getProStatus())) {
                    item.add(CommonEnum.GoodsStauts.GOODSNORMAL.getName());
                } else if (CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(dto.getProStatus())) {
                    item.add(CommonEnum.GoodsStauts.GOODSDEACTIVATE.getName());
                } else {
                    item.add("");
                }
                // 国家医保品种编码
                item.add(dto.getProMedProdctcode());
                // 产地"
                item.add(dto.getProPlace());
                // 待出量
                item.add(dto.getWaitCount());
                // 商品自分类
                item.add(dto.getProSclassName());
                // 7天配送量
                item.add(dto.getSevenPsl());
                dataList.add(item);
            }
            // 导出数据表头
            String[] headList = {
                    "商品编码",
                    "商品名称",
                    "商品大类",
                    "规格",
                    "生产厂家",
                    "大仓库存",
                    "仓库可用库存",
                    "门店库存",
                    "总库存",
                    "7天门店销量",
                    "30天门店销量",
                    "90天门店销量",
                    "30天对外批发量",
                    "在途量",
                    "{0}天门店销售量", // index 41
                    "{0}天动销门店数", // index 42
                    "{0}天动销率",    // index 43
                    "区间销售-仓库库存",
                    "可销天数",
                    "单位",
                    "中包装量",
                    "大包装量",
                    "最后进货日期",
                    "最后一次进货供应商",
                    "结款方式",
                    "末次进货价",
                    "末次进货数量",
                    "补货门店数",
                    "库存门店数",
                    "铺货率",
                    "商品分类",
                    "零售价",
                    "会员价",
                    "会员日价",
                    "最新进价毛利",
                    "自定义字段1",
                    "自定义字段2",
                    "自定义字段3",
                    "自定义字段4",
                    "自定义字段5",
                    "备注",
                    "经营管理类别",
                    "档案号",
                    "采购员",
                    "批准文号",
                    "国际条形码",
                    "销售级别",
                    "商品定位",
                    "禁止采购",
                    "税率",
                    "商品状态",
                    "国家医保品种编码",
                    "产地",
                    "待出量",
                    "商品自分类",
                    "7天配送量",
            };
            headList[13] = MessageFormat.format(headList[13], String.valueOf(days + 1));
            headList[14] = MessageFormat.format(headList[14], String.valueOf(days + 1));
            headList[15] = MessageFormat.format(headList[15], String.valueOf(days + 1));
            try {
                Result zdyResult = dcReplenishService.getProductZdy();
                GaiaProductZdy gaiaProductZdy = (GaiaProductZdy) zdyResult.getData();
                if (gaiaProductZdy != null) {
                    headList[34] = gaiaProductZdy.getProZdy1Name();
                    headList[35] = gaiaProductZdy.getProZdy2Name();
                    headList[36] = gaiaProductZdy.getProZdy3Name();
                    headList[37] = gaiaProductZdy.getProZdy4Name();
                    headList[38] = gaiaProductZdy.getProZdy5Name();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel2(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<Object>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("门店补货");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            String fileName = String.valueOf(System.currentTimeMillis()).concat(RandomStringUtils.randomNumeric(3)).concat(".xls");
            Result result = cosUtils.uploadFile(bos, fileName);
            if (ResultUtil.hasError(result)) {
                gaiaExportTask.setGetStatus(3);
                gaiaExportTask.setGetReason("文件导出失败");
            } else {
                gaiaExportTask.setGetStatus(2);
                gaiaExportTask.setGetPath(tencent.getExportPath() + fileName);
            }
            gaiaExportTaskMapper.updateByPrimaryKeySelective(gaiaExportTask);
        } catch (IOException e) {
            e.printStackTrace();
            gaiaExportTask.setGetStatus(3);
            gaiaExportTask.setGetReason(e.getMessage());
            gaiaExportTaskMapper.updateByPrimaryKeySelective(gaiaExportTask);
        }
    }

    /**
     * 仓库补货系数
     *
     * @param client
     * @param site
     * @param type
     * @return
     */
    private List<GaiaDcReplenishRatio> initRatio(String client, String site, int type) {
        List<GaiaDcReplenishRatio> list = new ArrayList<>();
        Integer[] lsales = {null, 5, 10, 15, 30};
        Integer[] hsales = {5, 10, 15, 30, null};
        for (int i = 0; i < lsales.length; i++) {
            GaiaDcReplenishRatio gaiaDcReplenishRatio = new GaiaDcReplenishRatio();
            gaiaDcReplenishRatio.setClient(client);
            gaiaDcReplenishRatio.setGdrrSite(site);
            gaiaDcReplenishRatio.setGdrrType(type);
            gaiaDcReplenishRatio.setGdrrLnum(lsales[i]);
            gaiaDcReplenishRatio.setGdrrHnum(hsales[i]);
            if (type == 1) {
                gaiaDcReplenishRatio.setGdrrLmake(10);
                if (i == 0) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.ONE.multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.ONE);
                }
                if (i == 1) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(0.9).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(0.9));
                }
                if (i == 2) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(0.9).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(0.9));
                }
                if (i == 3) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(0.7).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(0.7));
                }
                if (i == 4) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(0.7).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(0.7));
                }
            }
            if (type == 2) {
                gaiaDcReplenishRatio.setGdrrLmake(12);
                if (i == 0) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(1.5).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(1.5));
                }
                if (i == 1) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.ONE.multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.ONE);
                }
                if (i == 2) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.ONE.multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.ONE);
                }
                if (i == 3) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.ONE.multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.ONE);
                }
                if (i == 4) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(0.8).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(0.8));
                }
            }
            if (type == 3) {
                gaiaDcReplenishRatio.setGdrrLmake(20);
                if (i == 0) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(2).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(2));
                }
                if (i == 1) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(2).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(2));
                }
                if (i == 2) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(1.5).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(1.5));
                }
                if (i == 3) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.valueOf(1.2).multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.valueOf(1.2));
                }
                if (i == 4) {
                    gaiaDcReplenishRatio.setGdrrHmake(BigDecimal.ONE.multiply(BigDecimal.valueOf(30)).intValue());
                    gaiaDcReplenishRatio.setGdrrRatio(BigDecimal.ONE);
                }
            }
            list.add(gaiaDcReplenishRatio);
        }
        return list;
    }

    /**
     * 导出数据准备
     *
     * @param batchList
     * @param dcList
     */
    private void initData(List<DcReplenishmentListResponseDto> batchList, List<List<String>> dcList) {
        //进项税 字典
        List<Dictionary> dictionaryList = null;
        DictionaryParams dictionaryParams = new DictionaryParams();
        dictionaryParams.setKey("TAX_CODE1");
        Result result = dictionaryService.getDictionary(dictionaryParams);
        if (result.getCode().equals(ResultEnum.SUCCESS.getCode())) {
            dictionaryList = (List<Dictionary>) result.getData();
        }

        for (DcReplenishmentListResponseDto dcDTO : batchList) {
            List<String> basicData = new ArrayList<>();
            //	商品编码
            basicData.add(dcDTO.getProSelfCode());
            //	商品名称
            basicData.add(dcDTO.getProName());
            // 商品大分类
            basicData.add(dcDTO.getProBigClassName());
            //	规格
            basicData.add(dcDTO.getProSpecs());
            //	生产厂家
            basicData.add(dcDTO.getProFactoryName());
            //	建议补货量
            basicData.add(ObjectUtils.isEmpty(dcDTO.getReplenishmentValue()) ? "" : dcDTO.getReplenishmentValue().stripTrailingZeros().toPlainString());
            //	供应商下拉列表
            if (StringUtils.isBlank(dcDTO.getLastSupplierId()) && StringUtils.isBlank(dcDTO.getLastSupplier())) {
                basicData.add("");
            } else if (StringUtils.isNotBlank(dcDTO.getLastSupplierId()) && StringUtils.isNotBlank(dcDTO.getLastSupplier())) {
                basicData.add(dcDTO.getLastSupplierId() + "-" + dcDTO.getLastSupplier());
            } else if (StringUtils.isNotBlank(dcDTO.getLastSupplierId())) {
                basicData.add(dcDTO.getLastSupplierId());
            } else if (StringUtils.isNotBlank(dcDTO.getLastSupplier())) {
                basicData.add(dcDTO.getLastSupplier());
            } else {
                basicData.add("");
            }
            //	采购价格
            basicData.add(ObjectUtils.isEmpty(dcDTO.getLastPurchasePrice()) ? "" : dcDTO.getLastPurchasePrice().stripTrailingZeros().toPlainString());
            //	大仓库存
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDcStockTotal()) ? dcDTO.getDcStockTotal().stripTrailingZeros().toPlainString() : "");
            //	门店库存
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getStoreStockTotal()) ? dcDTO.getStoreStockTotal().stripTrailingZeros().toPlainString() : "");
            //	总库存量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getStockTotal()) ? dcDTO.getStockTotal().stripTrailingZeros().toPlainString() : "");
            //	7天门店销售量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysSalesCountSeven()) ? dcDTO.getDaysSalesCountSeven().stripTrailingZeros().toPlainString() : "");
            //	30天门店销售量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysSalesCount()) ? dcDTO.getDaysSalesCount().stripTrailingZeros().toPlainString() : "");
            //	90天门店销售量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysSalesCountNinety()) ? dcDTO.getDaysSalesCountNinety().stripTrailingZeros().toPlainString() : "");
            //	30天对外批发量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getDaysWholesaleCount()) ? dcDTO.getDaysWholesaleCount().stripTrailingZeros().toPlainString() : "");
            //	在途量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getTrafficCount()) ? dcDTO.getTrafficCount().stripTrailingZeros().toPlainString() : "");
            //	进项税率
            if (CollectionUtils.isNotEmpty(dictionaryList)) {
                Dictionary dictionary = dictionaryList.stream().filter(item -> item.getValue().equals(dcDTO.getProInputTax())).findFirst().orElse(null);
                if (ObjectUtils.isNotEmpty(dictionary)) {
                    basicData.add(dictionary.getLabel());
                } else {
                    basicData.add("");
                }
            }
            //	单位
            basicData.add(dcDTO.getProUnit());
            //	中包装量
            basicData.add(dcDTO.getProMidPackage());
            //	最近一次进货日期
            basicData.add(formatDate(dcDTO.getLastPurchaseDate()));
            //	最后进货供应商自编码
            basicData.add(dcDTO.getLastSupplierId());
            //	末次进货价
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getLastPurchasePrice()) ? dcDTO.getLastPurchasePrice().stripTrailingZeros().toPlainString() : "");
            //	待出量
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getWaitCount()) ? dcDTO.getWaitCount().stripTrailingZeros().toPlainString() : "");
            // 补货点
            basicData.add(ObjectUtils.isNotEmpty(dcDTO.getReplenishmentRatio()) ? dcDTO.getReplenishmentRatio().stripTrailingZeros().toString() : "");
            // 商品分类
            basicData.add(dcDTO.getProClassName());
            dcList.add(basicData);
        }
    }

    /**
     * 日期格式化
     */
    private String formatDate(String date) {
        if (StringUtils.isEmpty(date)) {
            return null;
        }
        if (date.length() != 8) {
            return date;
        }
        StringBuilder sb = new StringBuilder(date);
        sb.insert(4, "/");
        sb.insert(7, "/");
        return sb.toString();
    }

    /**
     * 系数初始化
     *
     * @param gaiaDcReplenishRatioList
     * @param list
     */
    private void initRatio(List<GaiaDcReplenishRatio> gaiaDcReplenishRatioList, List<GaiaDcReplenishRatio> list, String site) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        if (list.size() != 15) {
            throw new CustomResultException("补货系数设置不正确");
        }
        TokenUser user = feignService.getLoginInfo();
        int index1 = 0;
        int index2 = 0;
        int index3 = 0;
        Integer[] lsales = {null, 5, 10, 15, 30};
        Integer[] hsales = {5, 10, 15, 30, null};
        // 补货系数初始化
        for (GaiaDcReplenishRatio gaiaDcReplenishRatio : list) {
            // 补货模式
            Integer gdrrType = gaiaDcReplenishRatio.getGdrrType();
            // Q<=7
            if (gdrrType.intValue() == 1) {
                // 月销下限
                gaiaDcReplenishRatio.setGdrrLnum(lsales[index1]);
                // 月销上限
                gaiaDcReplenishRatio.setGdrrHnum(hsales[index1]);
                index1++;
            }
            // 7<Q<=15
            if (gdrrType.intValue() == 2) {
                // 月销下限
                gaiaDcReplenishRatio.setGdrrLnum(lsales[index2]);
                // 月销上限
                gaiaDcReplenishRatio.setGdrrHnum(hsales[index2]);
                index2++;
            }
            // 15<Q
            if (gdrrType.intValue() == 3) {
                // 月销下限
                gaiaDcReplenishRatio.setGdrrLnum(lsales[index3]);
                // 月销上限
                gaiaDcReplenishRatio.setGdrrHnum(hsales[index3]);
                index3++;
            }
            // 加盟商
            gaiaDcReplenishRatio.setClient(user.getClient());
            // 地点
            gaiaDcReplenishRatio.setGdrrSite(site);
            // 系数
            if (gaiaDcReplenishRatio.getGdrrHmake() != null) {
                gaiaDcReplenishRatio.setGdrrRatio(
                        BigDecimal.valueOf(gaiaDcReplenishRatio.getGdrrHmake()).divide(BigDecimal.valueOf(30), 2, RoundingMode.HALF_UP)
                );
            }
            gaiaDcReplenishRatioList.add(gaiaDcReplenishRatio);
        }
        if (index1 != 5 || index2 != 5 || index3 != 5) {
            throw new CustomResultException("补货系数设置不正确");
        }
    }

    /**
     * 排除数据
     *
     * @param gaiaDcReplenishExcludList
     * @param list
     * @param site
     * @param gdreBtype
     * @param gdrePtype
     */
    private void initExclud(List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludList, List<GaiaDcReplenishExcludDto> list,
                            String site, int gdreBtype, int gdrePtype) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        TokenUser user = feignService.getLoginInfo();
        for (GaiaDcReplenishExcludDto gaiaDcReplenishExclud : list) {
            // 加盟商
            gaiaDcReplenishExclud.setClient(user.getClient());
            // 地点
            gaiaDcReplenishExclud.setGdreSite(site);
            // 业务类型
            gaiaDcReplenishExclud.setGdreBtype(gdreBtype);
            // 商品类型
            gaiaDcReplenishExclud.setGdrePtype(gdrePtype);
            // 编号
            gaiaDcReplenishExclud.setGdreCode(gaiaDcReplenishExclud.getGdreCode());
            gaiaDcReplenishExcludList.add(gaiaDcReplenishExclud);
        }
    }

    /**
     * 得到补货系数
     *
     * @param dto
     * @param args
     * @return
     */
    private BigDecimal getReplenishmentRatio(DcReplenishmentListResponseDto dto, BigDecimal... args) {
        // X<N5
        if (dto.getDaysSalesCount().compareTo(new BigDecimal(5 * dto.getStoreCount())) < 0) {
            return args[0];
            // 5N <= X < 10N
        } else if (dto.getDaysSalesCount().compareTo(new BigDecimal(5 * dto.getStoreCount())) > 0
                && dto.getDaysSalesCount().compareTo(new BigDecimal(10 * dto.getStoreCount())) <= 0
        ) {
            return args[1];
            // 10N<= X < 15N
        } else if (dto.getDaysSalesCount().compareTo(new BigDecimal(10 * dto.getStoreCount())) > 0
                && dto.getDaysSalesCount().compareTo(new BigDecimal(15 * dto.getStoreCount())) <= 0) {
            return args[2];
            // 15N <= X < 30N
        } else if (dto.getDaysSalesCount().compareTo(new BigDecimal(15 * dto.getStoreCount())) > 0
                && dto.getDaysSalesCount().compareTo(new BigDecimal(30 * dto.getStoreCount())) <= 0) {
            return args[3];
            // X>=30N
        } else {
            return args[4];
        }
    }

    /**
     * 业务数据表头
     */
    private final String[] dcHead = {
            "商品编码",
            "商品名称",
            "商品大分类",
            "规格",
            "生产厂家",
            "建议补货量",
            "供应商",
            "采购价格",
            "大仓库存",
            "门店库存",
            "总库存量",
            "7天门店销售量",
            "30天门店销售量",
            "90天门店销售量",
            "30天对外批发量",
            "在途量",
            "进项税率",
            "单位",
            "中包装",
            "最近一次进货日期",
            "最后进货供应商",
            "末次进货价",
            "待出量",
            "补货点",
            "商品分类",
    };

    @Override
    public Result getUrgentReplenishList(DcReplenishForm dcReplenishForm) {
        TokenUser user = feignService.getLoginInfo();
        //获取仓库紧急补货单号
        GaiaDcReplenishH cond = new GaiaDcReplenishH();
        cond.setClient(user.getClient());
        cond.setVoucherId(dcReplenishForm.getVoucherId());
        GaiaDcReplenishH dcReplenishH = gaiaDcReplenishHMapper.getUnique(cond);
        if (dcReplenishH == null) {
            return ResultUtil.success(new ArrayList<>());
        }
        //仓库补货需求明细
        List<GaiaProductBusiness> replenishProductList = gaiaDcReplenishDMapper.getReplenishProductList(cond);
        if (replenishProductList == null || replenishProductList.size() == 0) {
            return ResultUtil.success();
        }

        String dcCode = dcReplenishH.getProSite();
        // 补货参数
        GaiaDcReplenishConfKey gaiaDcReplenishConfKey = new GaiaDcReplenishConfKey();
        gaiaDcReplenishConfKey.setClient(user.getClient());
        gaiaDcReplenishConfKey.setGdrcSite(dcCode);
        GaiaDcReplenishConf gaiaDcReplenishConf = gaiaDcReplenishConfMapper.selectByPrimaryKey(gaiaDcReplenishConfKey);
        // 系数
        List<GaiaDcReplenishRatio> gaiaDcReplenishRatioList = gaiaDcReplenishRatioMapper.selectByDc(user.getClient(), dcCode);
        // 最小补货量品类
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinClassList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 1, 1);
        // 最小补货量商品
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMinProList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 1, 2);
        // 最大补货量品类
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxClassList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 2, 1);
        // 最大补货量商品
        List<GaiaDcReplenishExcludDto> gaiaDcReplenishExcludMaxProList = gaiaDcReplenishExcludMapper.selectByDc(user.getClient(), dcCode, 2, 2);
        // 补货列表
        List<DcReplenishmentListResponseDto> list = gaiaDcDataMapper.getReplenishmentList(user.getClient(), dcCode, gaiaDcReplenishConf);
        // 商品列表空
        if (CollectionUtils.isEmpty(list)) {
            return ResultUtil.success();
        }
        for (int i = list.size() - 1; i >= 0; i--) {
            DcReplenishmentListResponseDto entity = list.get(i);
            // 商品名称表示为描述
            entity.setProName(entity.getProDepict());
            // 配送平均天数  默认为 7天
            Integer q = 7;
            Integer m = 7;
            // 补货系数 默认 = 1
            BigDecimal ratio = null;
            // 门店数
            Integer storeCount = entity.getStoreCount();
            // 仓库库存
            BigDecimal dcStockTotal = entity.getDcStockTotal();
            // 30天销量
            BigDecimal daysSalesCount = entity.getDaysSalesCount();
            // 在途量
            BigDecimal trafficCount = entity.getTrafficCount();
            // 30对外批发量
            BigDecimal daysWholesaleCount = entity.getDaysWholesaleCount();
            // 30对外批发量 - 仓库库存 = X
            BigDecimal x = daysWholesaleCount.subtract(dcStockTotal);
            if (x.compareTo(BigDecimal.ONE) < 0) {
                x = BigDecimal.ZERO;
            }
            boolean delFlg = false;
            // 对外批发是否参与补货
            boolean gdrcWholesaleFlg = gaiaDcReplenishConf != null && (gaiaDcReplenishConf.getGdrcWholesaleFlg() != null && (gaiaDcReplenishConf.getGdrcWholesaleFlg() == 1));
            if (!gdrcWholesaleFlg) {
                x = BigDecimal.ZERO;
            }

            // 按供应商实际送货天数计算
            if (gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcActualDeliveryDays() != null && gaiaDcReplenishConf.getGdrcActualDeliveryDays() == 1) {
                if (entity.getSupLeadTime() != null) {
                    q = entity.getSupLeadTime();
                }
            }
            if (CollectionUtils.isNotEmpty(gaiaDcReplenishRatioList)) {
                List<GaiaDcReplenishRatio> gaiaDcReplenishRatioTypeList = null;
                // Q<=7
                if (q <= 7) {
                    gaiaDcReplenishRatioTypeList = gaiaDcReplenishRatioList.stream().filter(item -> item.getGdrrType() == 1).collect(Collectors.toList());
                } else if (q <= 15) {// 7<Q<=15
                    gaiaDcReplenishRatioTypeList = gaiaDcReplenishRatioList.stream().filter(item -> item.getGdrrType() == 2).collect(Collectors.toList());
                } else { // 15<Q
                    gaiaDcReplenishRatioTypeList = gaiaDcReplenishRatioList.stream().filter(item -> item.getGdrrType() == 3).collect(Collectors.toList());
                }
                // 设置系数
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishRatioTypeList)) {
                    GaiaDcReplenishRatio gaiaDcReplenishRatio = gaiaDcReplenishRatioTypeList.stream().filter(item -> (
                                    (item.getGdrrLnum() == null && item.getGdrrHnum() != null &&
                                            daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrHnum().intValue() * storeCount)) < 0) ||
                                            (item.getGdrrHnum() == null && item.getGdrrLnum() != null &&
                                                    daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrLnum() * storeCount)) >= 0) ||
                                            (daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrHnum() * storeCount)) < 0 &&
                                                    daysSalesCount.compareTo(BigDecimal.valueOf(item.getGdrrLnum() * storeCount)) >= 0)
                            )
                    ).findFirst().orElse(null);
                    if (gaiaDcReplenishRatio != null) {
                        // 起补点
                        if (gaiaDcReplenishRatio.getGdrrLmake() != null) {
                            // 补货点系数（起补） (仓库库存+在途)/30销量*30
                            BigDecimal lReplenis = (dcStockTotal.add(trafficCount)).divide(daysSalesCount, 4, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(30));
                            // > 起补点 不补
                            if (lReplenis.compareTo(BigDecimal.valueOf(gaiaDcReplenishRatio.getGdrrLmake())) > 0) {
                                if (gdrcWholesaleFlg && x.compareTo(BigDecimal.ZERO) > 0) {
                                    delFlg = true;
                                } else {
                                    list.remove(i);
                                    continue;
                                }
                            }
                        }
                        // 补货系数设置
                        if (gaiaDcReplenishRatio.getGdrrRatio() != null) {
                            ratio = gaiaDcReplenishRatio.getGdrrRatio();
                        }
                    }
                }
            }
            if (ratio == null) {
                ratio = getReplenishmentRatio(entity, new BigDecimal(1), new BigDecimal("0.9"), new BigDecimal("0.8"), new BigDecimal("0.7"), new BigDecimal("0.7"));
            }
            if (ratio == null) {
                ratio = BigDecimal.ONE;
            }

            // 系数
            if (!delFlg) {
                entity.setReplenishmentRatio(ratio);
            }
            // 按商品实际中包装量参与计算
            if (gaiaDcReplenishConf == null || gaiaDcReplenishConf.getGdrcActualMidPackage() == null || gaiaDcReplenishConf.getGdrcActualMidPackage() != 1) {
                entity.setProMidPackage(null);
            }
            if (StringUtils.isBlank(entity.getProMidPackage())) {
                // 中药饮片 默认100
                if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {
                    entity.setProMidPackage("100");
                } else { // 其他商品1
                    entity.setProMidPackage("1");
                }
            }

            // 仓库配送到所有门店的平均天数
            if (gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcDcDelivery() != null && gaiaDcReplenishConf.getGdrcDcDelivery() == 1 &&
                    gaiaDcReplenishConf.getGdrcDcDeliveryDays() != null) {
                m = gaiaDcReplenishConf.getGdrcDcDeliveryDays();
            } else {
                if (entity.getDcDeliveryDays() != null) {
                    m = entity.getDcDeliveryDays();
                }
            }
            // 建议补货数量
            BigDecimal replenishmentValue = new BigDecimal(0);
            if (!delFlg) {
                // 最终补货数量 = X ( S + M / 30 ) - T
                replenishmentValue = new BigDecimal(m).divide(new BigDecimal(30), 4, RoundingMode.HALF_UP);
                if (entity.getReplenishmentRatio() != null) {
                    replenishmentValue = replenishmentValue.add(entity.getReplenishmentRatio());
                }
                replenishmentValue = replenishmentValue.multiply(entity.getDaysSalesCount());
                // T = 在途量 + 大仓库存
                BigDecimal t = entity.getTrafficCount().add(entity.getDcStockTotal());
                replenishmentValue = replenishmentValue.subtract(t);
            } else {
                replenishmentValue = x;
            }

            /**
             * 1. 非中药（商品分类不为3开头）
             * 二舍八入原则：
             * 即补货量/中包量 余数<2 向下取整； >=8向上取整；介于中间的就不变。
             * 例：A商品中包装量10，补货量算出为21，21/10=2.1，余数是1，小于2，则向下取整，最终建议补货量为20。又若补货量算出为28，28/10=2.8，余数是8，向上取整，即补30个。
             * 2. 中药（商品分类为3开头）
             * 四舍五入原则：
             * 即补货量/中包量 余数<5向下取整； >=5向上取整
             */

            BigDecimal remain = replenishmentValue.divide(new BigDecimal(entity.getProMidPackage()), 4, RoundingMode.HALF_UP);
            BigDecimal[] remainArr = {remain.setScale(0, BigDecimal.ROUND_DOWN), remain.remainder(BigDecimal.ONE)};
            BigDecimal newValue = remainArr[0].multiply(new BigDecimal(entity.getProMidPackage()));
            if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {  // 中药
                if (remainArr[1].compareTo(new BigDecimal("0.5")) < 0) {
                    entity.setProposalReplenishment(newValue);
                } else {
                    newValue = newValue.add(new BigDecimal(entity.getProMidPackage()));
                    entity.setProposalReplenishment(newValue);
                }
            } else {  // 非中药
                if (remainArr[1].compareTo(new BigDecimal("0.2")) < 0) {
                    entity.setProposalReplenishment(newValue);
                } else if (remainArr[1].compareTo(new BigDecimal("0.8")) >= 0) {
                    newValue = newValue.add(new BigDecimal(entity.getProMidPackage()));
                    entity.setProposalReplenishment(newValue);
                } else {
                    entity.setProposalReplenishment(replenishmentValue.setScale(0, BigDecimal.ROUND_DOWN));
                }
            }

            // 获取建议补货量
            // 中药
            if (StringUtils.isNotBlank(entity.getProClass()) && (entity.getProClass().startsWith("301") || entity.getProClass().startsWith("302"))) {
                entity.setReplenishmentValue(
                        entity.getProposalReplenishment().divide(new BigDecimal(entity.getProMidPackage()), 0, RoundingMode.CEILING)
                                .multiply(new BigDecimal(entity.getProMidPackage()))
                );
            } else {
                // 建议补货 / 中包装量 四舍五入 * 中包装量
                entity.setReplenishmentValue(
                        entity.getProposalReplenishment().divide(new BigDecimal(entity.getProMidPackage()), 4, RoundingMode.HALF_UP)
                                .multiply(new BigDecimal(entity.getProMidPackage())).setScale(0, RoundingMode.HALF_UP)
                );
            }
            if (!delFlg) {
                entity.setReplenishmentValue(entity.getReplenishmentValue().add(x));
            }

            if (entity.getReplenishmentValue() == null || entity.getReplenishmentValue().compareTo(BigDecimal.ZERO) <= 0) {
                list.remove(i);
                continue;
            }

            // 最小补货量排除 非中药
            if ((StringUtils.isBlank(entity.getProClass()) || (!entity.getProClass().startsWith("301") && !entity.getProClass().startsWith("302")))
                    && gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcMinReplenish() != null && gaiaDcReplenishConf.getGdrcMinReplenish() == 1) {
                Integer gdrcMinReplenishNum = gaiaDcReplenishConf.getGdrcMinReplenishNum();
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMinPro = null;
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMinClass = null;
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMinProList)) {
                    gaiaDcReplenishExcludDtoMinPro = gaiaDcReplenishExcludMinProList.stream().filter(item -> item.getGdreCode().equals(entity.getProSelfCode())).findFirst().orElse(null);
                    boolean flg = removeItem(gaiaDcReplenishExcludDtoMinPro, list, entity, i);
                    if (flg) {
                        continue;
                    }
                }
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMinClassList)) {
                    gaiaDcReplenishExcludDtoMinClass = gaiaDcReplenishExcludMinClassList.stream().filter(item -> item.getGdreCode().equals(entity.getProBigClass())).findFirst().orElse(null);
                    boolean flg = removeItem(gaiaDcReplenishExcludDtoMinClass, list, entity, i);
                    if (flg) {
                        continue;
                    }
                }
                if (gaiaDcReplenishExcludDtoMinPro == null && gaiaDcReplenishExcludDtoMinClass == null && gdrcMinReplenishNum != null) {
                    if (entity.getReplenishmentValue().compareTo(BigDecimal.valueOf(gdrcMinReplenishNum)) < 0) {
                        list.remove(i);
                        continue;
                    }
                }
            }
            // 最大补货数量设置 非中药
            if ((StringUtils.isBlank(entity.getProClass()) || (!entity.getProClass().startsWith("301") && !entity.getProClass().startsWith("302")))
                    && gaiaDcReplenishConf != null && gaiaDcReplenishConf.getGdrcMaxReplenish() != null && gaiaDcReplenishConf.getGdrcMaxReplenish() == 1) {
                Integer gdrcMaxReplenishNum = gaiaDcReplenishConf.getGdrcMaxReplenishNum();
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMaxPro = null;
                GaiaDcReplenishExcludDto gaiaDcReplenishExcludDtoMaxClass = null;
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMaxProList)) {
                    gaiaDcReplenishExcludDtoMaxPro = gaiaDcReplenishExcludMaxProList.stream().filter(item -> item.getGdreCode().equals(entity.getProSelfCode())).findFirst().orElse(null);
                    boolean flg = setMax(gaiaDcReplenishExcludDtoMaxPro, entity);
                    if (flg) {
                        continue;
                    }
                }
                if (CollectionUtils.isNotEmpty(gaiaDcReplenishExcludMaxClassList)) {
                    gaiaDcReplenishExcludDtoMaxClass = gaiaDcReplenishExcludMaxClassList.stream().filter(item -> item.getGdreCode().equals(entity.getProBigClass())).findFirst().orElse(null);
                    boolean flg = setMax(gaiaDcReplenishExcludDtoMaxClass, entity);
                    if (flg) {
                        continue;
                    }
                }
                if (gaiaDcReplenishExcludDtoMaxPro == null && gaiaDcReplenishExcludDtoMaxClass == null && gdrcMaxReplenishNum != null) {
                    if (entity.getReplenishmentValue().compareTo(BigDecimal.valueOf(gdrcMaxReplenishNum)) > 0) {
                        entity.setReplenishmentValue(BigDecimal.valueOf(gdrcMaxReplenishNum));
                    }
                }
            }
        }

        //list转map
        Map<String, String> collect = replenishProductList.stream().collect(Collectors.toMap(GaiaProductBusiness::getProSelfCode, GaiaProductBusiness::getProMidPackage));
        //删除其他商品补货
        list.removeIf(responseDto -> collect.get(responseDto.getProSelfCode()) == null);
        //补充未补货商品
        Map<String, BigDecimal> map = list.stream().collect(Collectors.toMap(DcReplenishmentListResponseDto::getProSelfCode, DcReplenishmentListResponseDto::getReplenishmentValue));
        Integer storeCount = gaiaStoreDataMapper.getStoreCount(dcReplenishH.getClient());
        for (GaiaProductBusiness productBusiness : replenishProductList) {
            if (map.get(productBusiness.getProSelfCode()) == null) {
                DcReplenishmentListResponseDto newDTO = getDcReplenishmentListResponseDto(storeCount, productBusiness);
                list.add(newDTO);
            }
        }
        return ResultUtil.success(list);
    }

    private DcReplenishmentListResponseDto getDcReplenishmentListResponseDto(Integer storeCount, GaiaProductBusiness productBusiness) {
        DcReplenishmentListResponseDto newDTO = new DcReplenishmentListResponseDto();
        newDTO.setClient(productBusiness.getClient());
        newDTO.setProSelfCode(productBusiness.getProSelfCode());
        newDTO.setProCode(productBusiness.getProCode());
        newDTO.setProCommonname(productBusiness.getProCommonname());
        newDTO.setProFactoryName(productBusiness.getProFactoryName());
        newDTO.setProUnit(productBusiness.getProUnit());
        newDTO.setProSpecs(productBusiness.getProSpecs());
        newDTO.setProClass(productBusiness.getProClass());
        newDTO.setProClassName(productBusiness.getProClassName());
        newDTO.setProBigClass(productBusiness.getProClass());
        newDTO.setProBigClassName(productBusiness.getProClassName());
        newDTO.setProposalReplenishment(getDcReplenishQuantity(storeCount, new BigDecimal(productBusiness.getProMidPackage())));
        return newDTO;
    }

    /**
     * 根据门店数量和中包装计算仓库补货量
     *
     * @param storeCount 门店数量
     * @param midPackage 中包装
     * @return 补货数量
     */
    private BigDecimal getDcReplenishQuantity(Integer storeCount, BigDecimal midPackage) {
        BigDecimal baseX = new BigDecimal((storeCount * 2)).divide(midPackage, 2, RoundingMode.HALF_UP);
        BigDecimal floorX = BigDecimal.valueOf(Math.floor(baseX.doubleValue()));
        BigDecimal value = baseX.subtract(floorX);
        BigDecimal t = BigDecimal.ZERO;
        if (value.compareTo(new BigDecimal("0.8")) >= 0) {
            t = BigDecimal.ONE;
        } else if (value.compareTo(new BigDecimal("0.2")) < 0) {
            t = BigDecimal.ZERO;
        } else {
            t = value;
        }
        BigDecimal decimal = (floorX.add(t)).multiply(midPackage);
        return BigDecimal.valueOf(Math.floor(decimal.doubleValue()));
    }

    /**
     * 商品自定义标签
     *
     * @return
     */
    @Override
    public Result getProductZdy() {
        TokenUser user = feignService.getLoginInfo();
        GaiaProductZdy gaiaProductZdy = gaiaProductZdyMapper.selectZdyName(user.getClient());
        if (gaiaProductZdy == null) {
            gaiaProductZdy = new GaiaProductZdy();
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy1Name())) {
            gaiaProductZdy.setProZdy1Name("自定义字段1");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy2Name())) {
            gaiaProductZdy.setProZdy2Name("自定义字段2");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy3Name())) {
            gaiaProductZdy.setProZdy3Name("自定义字段3");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy4Name())) {
            gaiaProductZdy.setProZdy4Name("自定义字段4");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy5Name())) {
            gaiaProductZdy.setProZdy5Name("自定义字段5");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy6Name())) {
            gaiaProductZdy.setProZdy6Name("自定义字段6");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy7Name())) {
            gaiaProductZdy.setProZdy7Name("自定义字段7");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy8Name())) {
            gaiaProductZdy.setProZdy8Name("自定义字段8");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy9Name())) {
            gaiaProductZdy.setProZdy9Name("自定义字段9");
        }
        if (StringUtils.isBlank(gaiaProductZdy.getProZdy10Name())) {
            gaiaProductZdy.setProZdy10Name("自定义字段10");
        }
        return ResultUtil.success(gaiaProductZdy);
    }

    /**
     * 查询条件
     *
     * @return
     */
    @Override
    public Result getProReplenishmentCondit() {
        Map<String, Object> map = new HashMap<>();
        TokenUser user = feignService.getLoginInfo();
        List<Dictionary> dictionaryList = gaiaProductBusinessMapper.selectProSlaeClass(user.getClient());
        map.put("proSlaeClass", dictionaryList);

        return ResultUtil.success(map);
    }

    @Override
    public void updateUrgentReplenish(DcReplenishForm dcReplenishForm) {
        TokenUser user = feignService.getLoginInfo();
        //获取仓库紧急补货单号
        GaiaDcReplenishH cond = new GaiaDcReplenishH();
        cond.setClient(user.getClient());
        cond.setVoucherId(dcReplenishForm.getVoucherId());
        GaiaDcReplenishH dcReplenishH = gaiaDcReplenishHMapper.getUnique(cond);
        if (dcReplenishH == null) {
            throw new ServiceException("仓库紧急补货单不存在");
        }
        try {
            dcReplenishH.setStatus(1);
            dcReplenishH.setProcessTime(new Date());
            dcReplenishH.setUpdateTime(new Date());
            dcReplenishH.setUpdateUser(user.getUserId());
            gaiaDcReplenishHMapper.update(dcReplenishH);
        } catch (Exception e) {
            throw new ServiceException("更新仓库紧急补货单异常", e);
        }
    }

    /**
     * 采购员下拉框
     */
    @Override
    public List<String> getProPurchaseRateList() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaDcDataMapper.getProPurchaseRateList(user.getClient());
    }

    /**
     * 采购员下拉框
     *
     * @param dcCode
     * @return
     */
    @Override
    public Map<String, List<GaiaSupplierSalesman>> supplierSalesman(String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaSupplierSalesman> gaiaSupplierSalesmanList = gaiaSupplierSalesmanMapper.supplierSalesmanList(user.getClient(), dcCode, null);
        Map<String, List<GaiaSupplierSalesman>> gaiaSupplierSalesmanMap = gaiaSupplierSalesmanList.stream().collect(Collectors.groupingBy(t -> t.getSupSelfCode()));
        return gaiaSupplierSalesmanMap;
    }

    /**
     * 新增商品
     *
     * @param proSelfCode
     * @return
     */
    @Override
    public Result addReplenishmentPro(String proSelfCode, String dcCode) {
        TokenUser user = feignService.getLoginInfo();
        DcReplenishmentListResponseDto dcReplenishmentListResponseDto = gaiaDcDataMapper.addReplenishmentPro(user.getClient(), dcCode, proSelfCode);
        if (dcReplenishmentListResponseDto != null) {
            // 业务员
            if (StringUtils.isNotBlank(dcReplenishmentListResponseDto.getLastSupplierId())) {
                // 业务员
                List<GaiaSupplierSalesman> gaiaSupplierSalesmanList =
                        gaiaSupplierSalesmanMapper.supplierSalesmanList(user.getClient(), dcCode, dcReplenishmentListResponseDto.getLastSupplierId());
                if (CollectionUtils.isNotEmpty(gaiaSupplierSalesmanList)) {
                    dcReplenishmentListResponseDto.setSalesList(gaiaSupplierSalesmanList);
                    GaiaSupplierSalesman gaiaSupplierSalesman = gaiaSupplierSalesmanList.stream().filter(
                            s -> s.getGssDefault() != null && s.getGssDefault().intValue() == 1).findFirst().orElse(null);
                    if (gaiaSupplierSalesman != null) {
                        dcReplenishmentListResponseDto.setPoSupplierSalesman(gaiaSupplierSalesman.getGssCode());
                        dcReplenishmentListResponseDto.setPoSupplierSalesmanName(gaiaSupplierSalesman.getGssName());
                    }
                }
            }
        }
        return ResultUtil.success(dcReplenishmentListResponseDto);
    }

    /**
     * 门店库存
     *
     * @param dcCode
     * @param proSelfCode
     * @return
     */
    @Override
    public List<StoProStock> getStoreStock(String dcCode, String proSelfCode) {
        TokenUser user = feignService.getLoginInfo();
        List<StoProStock> stoProStockList = gaiaSdStockBatchMapper.getStoreStock(user.getClient(), dcCode, proSelfCode);
        return stoProStockList;
    }

    @Override
    public GaiaExportTask initExportTask() {
        GaiaExportTask gaiaExportTask = gaiaExportTaskService.initExportTask(1, 1);
        return gaiaExportTask;
    }

    @Override
    public Result getProZdyList() {
        ProZdyDTO dto = new ProZdyDTO();
        TokenUser user = feignService.getLoginInfo();
        List<ProZdyListDto> zdyListDtoList = gaiaProductBusinessMapper.selectAllPro(user.getClient());
        Map<String, List<ProZdyListDto>> map = zdyListDtoList.stream().collect(Collectors.groupingBy(ProZdyListDto::getType));
        if (map.containsKey("PRO_ZDY1") && CollectionUtils.isNotEmpty(map.get("PRO_ZDY1"))) {
            dto.setZdy1(map.get("PRO_ZDY1").stream().map(ProZdyListDto::getValue).collect(Collectors.toList()));
        }
        if (map.containsKey("PRO_ZDY2") && CollectionUtils.isNotEmpty(map.get("PRO_ZDY2"))) {
            dto.setZdy2(map.get("PRO_ZDY2").stream().map(ProZdyListDto::getValue).collect(Collectors.toList()));
        }
        if (map.containsKey("PRO_ZDY3") && CollectionUtils.isNotEmpty(map.get("PRO_ZDY3"))) {
            dto.setZdy3(map.get("PRO_ZDY3").stream().map(ProZdyListDto::getValue).collect(Collectors.toList()));
        }
        if (map.containsKey("PRO_ZDY4") && CollectionUtils.isNotEmpty(map.get("PRO_ZDY4"))) {
            dto.setZdy4(map.get("PRO_ZDY4").stream().map(ProZdyListDto::getValue).collect(Collectors.toList()));
        }
        if (map.containsKey("PRO_ZDY5") && CollectionUtils.isNotEmpty(map.get("PRO_ZDY5"))) {
            dto.setZdy5(map.get("PRO_ZDY5").stream().map(ProZdyListDto::getValue).collect(Collectors.toList()));
        }
        return ResultUtil.success(dto);
    }
}
