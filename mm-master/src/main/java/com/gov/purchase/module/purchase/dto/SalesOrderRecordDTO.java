package com.gov.purchase.module.purchase.dto;

import lombok.Data;

/**
 * @description: 销售订单
 * @author: yzf
 * @create: 2021-11-04 15:15
 */
@Data
public class SalesOrderRecordDTO {

    private String client;

    /**
     * 仓库编码
     */
    private String dcCode;

    /**
     * 客户编码
     */
    private String cusCode;

    /**
     * 商品编码
     */
    private String proSelfCodes;

    /**
     * 销售日期
     */
    private String saleDate;
    private String startDate;
    private String endDate;

    /**
     * 销售员
     */
    private String salePer;

    private Integer pageSize;

    private Integer pageNum;

    private String type;


}
