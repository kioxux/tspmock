package com.gov.purchase.module.base.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zhoushuai
 * @date 2021/4/13 10:42
 */
@Data
public class JyfwDTO {
    @NotBlank(message = "经营范围编码不能为空")
    private String jyfwId;
    @NotBlank(message = "经营范围名称不能为空")
    private String jyfwName;
    @NotNull(message = "经营范围是否选中不能为空(0:不选 1:选中)")
    private Integer checked;
}
