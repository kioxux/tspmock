package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

@Data
public class PurchaseContract {

    /**
     * 商品
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING)
    private String proSelfCode;

    /**
     * 目标数量
     */
    @ExcelValidate(index = 1, name = "目标数量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0, addRequired = false)
    private String conQty;

    /**
     * 采购单价
     */
    @ExcelValidate(index = 2, name = "采购单价", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.0)
    private String conPrice;

    /**
     * 税率
     */
    @ExcelValidate(index = 3, name = "税率", type = ExcelValidate.DataType.STRING, addRequired = false)
    private String conProInputTax;
}
