package com.gov.purchase.module.base.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaMaterialDocLogHWithBLOBs;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.module.base.dto.feign.ApprovalInfo;
import com.gov.purchase.module.base.dto.feign.MaterialDocRequestDto;
import com.gov.purchase.module.base.service.ApprovalService;
import com.gov.purchase.module.base.service.MaterialDocService;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Slf4j
@Api(tags = "对外接口")
@RestController
@RequestMapping("api")
public class ApiController {

    @Resource
    private ApprovalService approvalService;
    @Resource
    private MaterialDocService materialDocService;
    @Resource
    private OperationFeignService operationFeignService;

    @Log("工作流审批回调")
    @ApiOperation("审批回调")
    @PostMapping("getApprovalResult")
    public FeignResult getApprovalResult(@RequestBody ApprovalInfo info) {
        try {
            FeignResult feignResult = approvalService.approvalResultProcess(info);
            try {
                if (feignResult.getData() != null) {
                    Map<String, Object> map = (Map<String, Object>) feignResult.getData();
                    // 第三方 gys-operation
                    if (map != null && !map.isEmpty()) {
                        List<String> proSiteList = (List<String>) map.get("proSiteList");
                        List<String> proSelfCodeList = (List<String>) map.get("proSelfCodeList");
                        if (CollectionUtils.isNotEmpty(proSiteList) && CollectionUtils.isNotEmpty(proSelfCodeList)) {
                            operationFeignService.multipleStoreProductSync(map.get("client").toString(), proSiteList, proSelfCodeList);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return feignResult;
        } catch (Exception e) {
            log.info(e.getMessage());
            e.printStackTrace();
            FeignResult result = new FeignResult();
            result.setCode(9999);
            result.setMessage("系统异常");
            return result;
        }
    }

    @Log("物料凭证生成任务")
    @ApiOperation("物料凭证生成任务")
    @PostMapping("materialDocBatchTask")
    public void materialDocBatchTask() {
        log.info("物料凭证 定时任务开始");
        List<GaiaMaterialDocLogHWithBLOBs> list = materialDocService.getMaterialDocBatchTask();
        if (CollectionUtils.isEmpty(list)) {
            log.info("物料凭证 定时任务结束 _ 无数据");
            return;
        }
        log.info("物料凭证 定时任务 _ {}条", list.size());
        for (int i = 0; i < list.size(); i++) {
            GaiaMaterialDocLogHWithBLOBs gaiaMaterialDocLogHWithBLOBs = list.get(i);
            try {
                FeignResult feignResult = materialDocService.insertMaterialDocBatchTask(gaiaMaterialDocLogHWithBLOBs);
                if (feignResult.getCode() == 0) {
                    log.info("物料凭证 定时任务成功 _ 第{}条", i);
                    materialDocService.taskSucess(gaiaMaterialDocLogHWithBLOBs.getGuid());
                    continue;
                } else {
                    log.info("物料凭证 定时任务失败 _ 第{}条", i);
                    materialDocService.taskError(gaiaMaterialDocLogHWithBLOBs.getGuid(), JsonUtils.beanToJson(feignResult));
                    continue;
                }
            } catch (Exception e) {
                log.info("物料凭证 定时任务失败 _ 第{}条", i);
                e.printStackTrace();
                try {
                    materialDocService.taskError(gaiaMaterialDocLogHWithBLOBs.getGuid(), JsonUtils.beanToJson(e));
                } catch (Exception ex) {
                    materialDocService.taskError(gaiaMaterialDocLogHWithBLOBs.getGuid(), ex.getMessage());
                }
            }
        }
        log.info("物料凭证 定时任务结束");
    }

    @Log("物料凭证生成")
    @ApiOperation("物料凭证生成")
    @PostMapping("insertMaterialDoc")
    public FeignResult insertMaterialDoc(@RequestBody List<MaterialDocRequestDto> list) {
        FeignResult result = new FeignResult();
        String logId = "";
        try {
            logId = materialDocService.addLog(list);
            return materialDocService.insertMaterialDocBatch(list, logId, true);
        } catch (Exception ex) {
            // 执行异常
            if (StringUtils.isNotBlank(logId)) {
                materialDocService.exeError(logId, "");
            }
            log.info(ex.getMessage());
            ex.printStackTrace();
            result.setCode(9999);
            result.setMessage("系统异常");
        }
        return result;
    }

    @Log("计算某一天的门店往来")
    @ApiOperation("计算某一天的门店往来")
    @PostMapping("initComeAndGo")
    public Result initComeAndGo(@ApiParam("加盟商编码") @RequestParam(required = false) String client,
                                @ApiParam("门店code") @RequestParam(required = false) String stoCode,
                                @ApiParam("开始日期") @RequestParam String startDate,
                                @ApiParam("结束日期") @RequestParam String endDate) {
        materialDocService.initComeAndGo(client, stoCode, startDate, endDate);
        return ResultUtil.success("初始化完成");
    }

}
