package com.gov.purchase.module.goods.dto;

import com.gov.purchase.module.base.dto.Dictionary;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.08.09
 */
@Data
public class ProductUpdate {

    /**
     * 修改商品
     */
    private List<ProductUpdateItem> productUpdateItemList;

    /**
     * 修改地点
     */
    private List<Dictionary> dictionary;
}
