package com.gov.purchase.module.supplier.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.supplier.dto.*;

public interface DrugSupplierService {

    /**
     * 药品库搜索
     *
     * @param dto QuerySupListRequestDto
     * @return PageInfo<QuerySupListResponseDto>
     */
    PageInfo<QuerySupListResponseDto> selectDrugSupplierInfo(QuerySupListRequestDto dto);

    /**
     * 供应商首营提交
     *
     * @param dto QuerySupListRequestDto
     * @return int
     */
    void insertGspinfoSubmitInfo(GspinfoSubmitRequestDto dto);

    /**
     * 供应商首营列表
     *
     * @param dto QueryGspinfoListRequestDto
     * @return PageInfo<QueryGspListResponseDto>
     */
    PageInfo<QueryGspListResponseDto> selectQueryGspinfo(QueryGspinfoListRequestDto dto);

    /**
     * 供应商首营详情
     *
     * @param dto GaiaSupplierRequestDto
     * @return QueryGspListResponseDto
     */
    QueryGspListResponseDto selectQueryGspValue(GaiaSupplierRequestDto dto);

    /**
     * 最大自编码查询
     * @param client
     * @param supSite
     * @param code
     * @return
     */
    Result getMaxSelfCodeNum(String client, String supSite, String code);

    /**
     * 手工新增
     * @param gspinfoSubmitRequestDto
     * @return
     */
    Result addBusioness(GspinfoSubmitRequestDto gspinfoSubmitRequestDto);

    /**
     * 导出供应商首营列表
     * @param dto
     * @return
     */
    Result exportQueryGspinfoList(QueryGspinfoListRequestDto dto);
}
