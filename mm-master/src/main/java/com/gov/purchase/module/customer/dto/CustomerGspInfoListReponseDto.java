package com.gov.purchase.module.customer.dto;

import com.gov.purchase.entity.GaiaCustomerBasic;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户首营列表返回结果")
public class CustomerGspInfoListReponseDto extends GaiaCustomerBasic {

    @ApiModelProperty(value = "客户编号", name = "cusSelfCode")
    String cusCode;

    @ApiModelProperty(value = "客户编号（企业）", name = "cusSelfCode")
    String cusSelfCode;

    @ApiModelProperty(value = "客户名称", name = "cusName")
    String cusName;

    @ApiModelProperty(value = "统一社会信用代码（营业执照编号）", name = "cusCreditCode")
    String cusCreditCode;

    @ApiModelProperty(value = "首营日期（YYYY-MM-DD）", name = "cusGspDate")
    String cusGspDate;

    @ApiModelProperty(value = "地点编号", name = "proSite")
    String proSite;

    @ApiModelProperty(value = "地点名称", name = "stoName")
    String stoName;

    @ApiModelProperty(value = "流程编号", name = "cusFlowNo")
    String cusFlowNo;

}
