package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "商品状态更新提交传入参数")
public class UpdateProductResquestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "地点", name = "proSite")
    @NotBlank(message = "地点不能为空")
    private String proSite;

    @ApiModelProperty(value = "商品编号", name = "proCode")
//    @NotBlank(message = "商品编号不能为空")
    private String proCode;

    @ApiModelProperty(value = "更新字段名", name = "fieldName")
    @NotBlank(message = "更新字段名不能为空")
    private String fieldName;

    @ApiModelProperty(value = "更新字段值", name = "fieldValue")
    @NotBlank(message = "更新字段值不能为空")
    private String fieldValue;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;
}
