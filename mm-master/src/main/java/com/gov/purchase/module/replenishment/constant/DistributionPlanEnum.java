package com.gov.purchase.module.replenishment.constant;

/***
 * @desc: 新品铺货计划状态枚举类
 * @author: ryan
 * @createTime: 2021/6/6 23:56
 **/
public enum DistributionPlanEnum {
    WAIT_CHECK(0, "待审批"),
    WAIT_CALL(1, "待调用"),
    CALLING(2, "调用中"),
    COMPLETED(3, "已完成"),
    EXPIRED(4, "已过期"),
    CANCELED(5, "已取消"),
    WAIT_DISTRIBUTION(6, "待补铺"),
    ;

    public final Integer status;
    public final String message;

    DistributionPlanEnum(Integer status, String message) {
        this.status = status;
        this.message = message;
    }
}
