package com.gov.purchase.module.store.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/3/26 13:16
 */
@Data
@EqualsAndHashCode
public class EditLabelItemVO {

    @NotNull(message = "门店编码不能为空")
    private String labStore;

    @NotNull(message = "价签申请单号不能为空")
    private String labApplyNo;

    private List<LabelItemStore> labelItemStoreList;

    @Data
    @EqualsAndHashCode
    public static class LabelItemStore {
        /**
         * 商品编码
         */
        private String labProduct;
        /**
         * 申请份数
         */
        private String labApplyQty;
    }

}
