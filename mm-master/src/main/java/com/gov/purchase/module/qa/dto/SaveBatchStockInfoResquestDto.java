package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "质量管控-批次状态提交传入参数")
public class SaveBatchStockInfoResquestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client" ,required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "地点", name = "batSiteCode" ,required = true)
    @NotBlank(message = "地点不能为空")
    private String batSiteCode;

    @ApiModelProperty(value = "商品编号", name = "batProCode" ,required = true)
    @NotBlank(message = "商品编号不能为空")
    private String batProCode;

    @ApiModelProperty(value = "批号", name = "batBatch",required = true)
    @NotBlank(message = "批号不能为空")
    private String batBatch;

    @ApiModelProperty(value = "原状态(0-正常,1-异常)", name = "status" ,required = true)
    @NotBlank(message = "原状态不能为空")
    private String status;

    @ApiModelProperty(value = "新库存状态(0-正常,1-异常)", name = "newStatus" ,required = true)
    @NotBlank(message = "新状态不能为空")
    private String newStatus;
}
