package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "供应商列表传入参数")
public class SupplierListForPurchaseRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "采购订单类型", name = "poType", required = true)
    private String poType;

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
//    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "采购主体", name = "site")
    @NotBlank(message = "采购主体不能为空")
    private String site;
}