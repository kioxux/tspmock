package com.gov.purchase.module.base.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.mapper.GaiaAreaMapper;
import com.gov.purchase.module.base.dto.AreaDto;
import com.gov.purchase.module.base.service.AreaService;
import com.gov.purchase.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AreaServiceImpl implements AreaService {

    @Resource
    private GaiaAreaMapper gaiaAreaMapper;

    @Override
    public Result getAreaList() {
        List<AreaDto> list = gaiaAreaMapper.getAreaList();
        List<AreaDto> resultList = new ArrayList<>();
        if (list != null && list.size() > 0) {
            resultList = list.stream().filter(t -> t.getLevel().equals("1")).collect(Collectors.toList());
        }


        for (AreaDto entity : resultList) {
            entity.setChildren(getChildNodes(entity.getAreaId(), list));
        }

        return ResultUtil.success(resultList);
    }

    /**
     * 获取子节点下
     *
     * @param districtCode
     * @param list
     * @return
     */
    private List<AreaDto> getChildNodes(String districtCode, List<AreaDto> list) {
        List<AreaDto> childList = new ArrayList<>();
        for (AreaDto areaDto : list) {
            if (StringUtils.isNotBlank(areaDto.getParentId()) && districtCode.equals(areaDto.getParentId())) {
                childList.add(areaDto);
            }
        }

        if (childList.size() == 0) {
            return null;
        }

        for (AreaDto entity : childList) {
            entity.setChildren(getChildNodes(entity.getAreaId(), list));
        }
        return childList;

    }
}
