package com.gov.purchase.module.wholesale.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.wholesale.dto.ControlWholesaleOrdersVO;
import com.gov.purchase.module.wholesale.dto.OwnerCustomerVO;
import com.gov.purchase.module.wholesale.dto.WholesaleConsignorDTO;

import java.util.List;

/**
 * @description: 批发订单按货主管控功能
 * @author: yzf
 * @create: 2021-11-23 13:17
 */
public interface WholesaleConsignorService {

    /**
     * 根据仓库查询批发货主客户
     *
     * @param dcCode
     * @return
     */
    List<OwnerCustomerVO> queryOwnerCustomer(String dcCode);


    /**
     * 批发货主订单查询
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    Result queryControlWholesaleOrders(WholesaleConsignorDTO wholesaleConsignorDTO);


    /**
     * 新增批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    Result insertControlWholesaleOrders(WholesaleConsignorDTO wholesaleConsignorDTO);

    /**
     * 删除批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    Result delOwner(WholesaleConsignorDTO wholesaleConsignorDTO);

    /**
     * 更新批发货主
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    Result updateOwner(WholesaleConsignorDTO wholesaleConsignorDTO);

    /**
     * 查询货主已存在客户
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    Result queryOwnerCustomer(WholesaleConsignorDTO wholesaleConsignorDTO);

    /**
     * 查询货主已存在商品
     *
     * @param wholesaleConsignorDTO
     * @return
     */
    Result queryOwnerProduct(WholesaleConsignorDTO wholesaleConsignorDTO);

    /**
     * 货主明细保存
     *
     * @param dto
     * @return
     */
    Result saveOwnerDetail(WholesaleConsignorDTO dto);

    /**
     * 货主列表
     *
     * @param dcCode
     * @param saleManCode
     * @return
     */
    Result queryWholesaleConsignor(String dcCode, String saleManCode);
}
