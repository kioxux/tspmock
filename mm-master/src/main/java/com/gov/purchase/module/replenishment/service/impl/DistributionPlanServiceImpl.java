package com.gov.purchase.module.replenishment.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.util.StringUtil;
import com.google.common.collect.Lists;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.constants.DeliveryModeEnum;
import com.gov.purchase.constants.GenerateMessageEnum;
import com.gov.purchase.constants.SdMessageTypeEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.ServiceException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.replenishment.constant.DistributionPlanEnum;
import com.gov.purchase.module.replenishment.constant.DistributionTypeEnum;
import com.gov.purchase.module.replenishment.constant.PlanCallTypeEnum;
import com.gov.purchase.module.replenishment.constant.PlanDetailStatusEnum;
import com.gov.purchase.module.replenishment.dto.AutoNeedProDTO;
import com.gov.purchase.module.replenishment.dto.SaveNeedProListRequestDTO;
import com.gov.purchase.module.replenishment.dto.StoreReplenishDetails;
import com.gov.purchase.module.replenishment.service.DistributionPlanService;
import com.gov.purchase.module.replenishment.service.StoreNeedService;
import com.gov.purchase.module.store.constant.Constant;
import com.gov.purchase.module.wholesale.dto.GaiaSoHeader;
import com.gov.purchase.module.wholesale.dto.*;
import com.gov.purchase.module.wholesale.service.SalesOrderService;
import com.gov.purchase.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/16 9:43
 **/
@Slf4j
@Service("distributionPlanService")
public class DistributionPlanServiceImpl implements DistributionPlanService {
    @Resource
    private GaiaNewDistributionPlanMapper gaiaNewDistributionPlanMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaWmsRkysMapper gaiaWmsRkysMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaSdMessageMapper gaiaSdMessageMapper;
    @Resource
    private GaiaNewDistributionPlanDetailMapper gaiaNewDistributionPlanDetailMapper;
    @Resource
    private StoreNeedService storeNeedService;
    @Resource
    private DistributionPlanRelationMapper distributionPlanRelationMapper;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Resource
    private GaiaWmsKuCunMapper gaiaWmsKuCunMapper;
    @Resource
    private SalesOrderService salesOrderService;
    @Resource
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Resource
    private GaiaNoplanMessageMapper gaiaNoplanMessageMapper;

    @Override
    public List<GaiaNewDistributionPlan> findList(GaiaNewDistributionPlan cond) {
        return gaiaNewDistributionPlanMapper.findList(cond);
    }

    @Override
    public List<GaiaNewDistributionPlan> getWaitAutoList(GaiaNewDistributionPlan cond) {
        return gaiaNewDistributionPlanMapper.getWaitAutoList(cond);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(GaiaNewDistributionPlan distributionPlan) {
        int num;
        try {
            distributionPlan.setUpdateTime(new Date());
            num = gaiaNewDistributionPlanMapper.update(distributionPlan);
        } catch (Exception e) {
            throw new RuntimeException("更新新品铺货计划异常," + e.getMessage());
        }
        if (num <= 0) {
            throw new RuntimeException("更新新品铺货计划异常失败");
        }
    }

    @Override
    public void confirmCall(GaiaNewDistributionPlan distributionPlan) {
        //手动调取实际可完成的铺货的明细列表
        List<GaiaNewDistributionPlanDetail> needList;
        List<WmsRkys> wmsRkysList = getDistributionRkysList(distributionPlan);
        if (wmsRkysList.size() > 0) {
            BigDecimal rkysQuantity = wmsRkysList.stream().map(WmsRkys::getWmShsl).reduce(BigDecimal.ZERO, BigDecimal::add);
            needList = fillCanCompleteList(rkysQuantity, distributionPlan);
            //执行铺货（调用）
            TokenUser user = new TokenUser();
            user.setUserId(distributionPlan.getCreateUser());
            user.setClient(distributionPlan.getClient());
            execAndUpdateDistributionPlan(distributionPlan, needList, user, wmsRkysList, false);
        }
    }

    @Override
    public GaiaNewDistributionPlan getUnique(GaiaNewDistributionPlan cond) {
        return gaiaNewDistributionPlanMapper.getUnique(cond);
    }

    @Override
    public void checkNewProductByWeek() {
        //配送中心
        GaiaDcData dcCond = new GaiaDcData();
        List<GaiaDcData> dcDataList = gaiaDcDataMapper.findList(dcCond);
        for (GaiaDcData gaiaDcData : dcDataList) {
            //配送中心是否有新品入库
            List<WmsRkys> productList = getNewProductListByWeek(gaiaDcData);
            if (productList == null || productList.size() == 0) {
                continue;
            }
            Map<String, Set<String>> messageMap = new HashMap<>();
            for (WmsRkys wmsRkys : productList) {
                //根据入库记录获取对应商品信息
                GaiaProductBusiness productBusiness = getProductInfoByRkys(wmsRkys);
                //商品定位是否“X”时执行新品铺货计划
                if (productBusiness != null && "X".equals(productBusiness.getProPosition()) && !hasDistributionPlanAndReplenishRecord(wmsRkys)) {
                    addMessageMap(wmsRkys.getClient(), wmsRkys.getWmSpBm(), messageMap);
                }
            }
            //发送上周入库新品，无铺货计划仍未手动铺货的消息提醒
            for (Map.Entry<String, Set<String>> message : messageMap.entrySet()) {
                generateDistributionMessage(message.getKey(), message.getValue(), GenerateMessageEnum.NOT_HAS_PLAN_WEEKLY);
                //新品周消息提醒 报表处理
                try {
                    threadPoolTaskExecutor.execute(()->saveWeekNoPlanMessage(message.getKey(),message.getValue()));
                } catch (Exception e) {
                    e.printStackTrace();
                    log.info("<无计划周新品><保存失败>参数:{}",message);
                }
            }
        }
    }


    private void saveWeekNoPlanMessage(String client, Set<String> proCodes){
        Date date = new Date();
        for (String proCode : proCodes) {
            GaiaNoplanMessage message = new GaiaNoplanMessage();
            message.setClient(client);
            message.setProCode(proCode);
            message.setIsWeekMsg("Y");
            message.setWeekMsgDate(date);
            gaiaNoplanMessageMapper.updateWeekMsg(message);
        }
        log.info("<无计划周新品><保存成功>");
    }


    @Override
    public void checkDistributionDiff() {
        //已完成、已过期的铺货计划
        List<GaiaNewDistributionPlan> list = gaiaNewDistributionPlanMapper.getCompleteAndExpiredList();
        //是否存在差异
        Map<String, Set<String>> messageMap = new HashMap<>();
        for (GaiaNewDistributionPlan distributionPlan : list) {
            if (distributionPlan.getPlanQuantity().compareTo(distributionPlan.getActualQuantity()) != 0) {
                addMessageMap(distributionPlan.getClient(), distributionPlan.getProSelfCode(), messageMap);
                //更新计划状态：待补铺
                distributionPlan.setStatus(DistributionPlanEnum.WAIT_DISTRIBUTION.status);
                distributionPlan.setNeedReplenishFlag(1);
                update(distributionPlan);
            }
        }
        //消息提醒
        for (Map.Entry<String, Set<String>> message : messageMap.entrySet()) {
            generateDistributionMessage(message.getKey(), message.getValue(), GenerateMessageEnum.EXIST_DISTRIBUTION_DIFF);
        }
    }

    /**
     * 是否存在新品铺货计划、是否存在铺货记录
     * @param wmsRkys 商品信息
     * @return true、false
     */
    private boolean hasDistributionPlanAndReplenishRecord(WmsRkys wmsRkys) {
        GaiaNewDistributionPlan cond = new GaiaNewDistributionPlan();
        cond.setClient(wmsRkys.getClient());
        cond.setProSelfCode(wmsRkys.getWmSpBm());
        List<GaiaNewDistributionPlan> planList = findList(cond);
        if (planList != null && planList.size() > 0) {
            return true;
        }
        GaiaSdReplenishD replenishD = new GaiaSdReplenishD();
        replenishD.setClient(wmsRkys.getClient());
        replenishD.setGsrdProId(wmsRkys.getWmSpBm());
        List<GaiaSdReplenishD> replenishDList = gaiaSdReplenishDMapper.findList(replenishD);
        return replenishDList != null && replenishDList.size() > 0;
    }

    @Override
    public void confirmReplenish(GaiaNewDistributionPlan distributionPlan) {
        //库存信息
        WmsKuCun cond = new WmsKuCun();
        cond.setClient(distributionPlan.getClient());
        cond.setWmSpBm(distributionPlan.getProSelfCode());
        WmsKuCun wmsKuCun = gaiaWmsKuCunMapper.getKuCunInfo(cond);
        if (wmsKuCun != null && BigDecimal.ZERO.compareTo(wmsKuCun.getWmKysl()) <= 0) {
            List<GaiaNewDistributionPlanDetail> actualList = fillCanCompleteList(wmsKuCun.getWmKysl(), distributionPlan);
            //执行铺货（调用）
            TokenUser user = new TokenUser();
            user.setUserId(distributionPlan.getCreateUser());
            user.setClient(distributionPlan.getClient());
            execAndUpdateDistributionPlan(distributionPlan, actualList, user, new ArrayList<>(), false);
            //更新状态-完成
            GaiaNewDistributionPlan planCond = new GaiaNewDistributionPlan();
            planCond.setPlanCode(distributionPlan.getPlanCode());
            planCond.setClient(distributionPlan.getClient());
            distributionPlan = getUnique(planCond);
            distributionPlan.setStatus(DistributionPlanEnum.COMPLETED.status);
            distributionPlan.setExecReplenishFlag(1);
            update(distributionPlan);
        } else {
            log.warn(String.format("<新品铺货><确认补铺><当前仓库无库存，无法补铺，计划单号：%s>", distributionPlan.getPlanCode()));
        }
    }

    public GaiaProductBusiness getProductInfoByRkys(WmsRkys wmsRkys) {
        try {
            GaiaProductBusiness productCond = new GaiaProductBusiness();
            productCond.setProSite(wmsRkys.getProSite());
            productCond.setProSelfCode(wmsRkys.getWmSpBm());
            productCond.setClient(wmsRkys.getClient());
            return gaiaProductBusinessMapper.getUnique(productCond);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("查询入库新品信息异常", e);
        }
    }

    private List<WmsRkys> getNewProductListByWeek(GaiaDcData gaiaDcData) {
        WmsRkys cond = new WmsRkys();
        cond.setClient(gaiaDcData.getClient());
        cond.setProSite(gaiaDcData.getDcCode());
        cond.setState(2);//上架结束
        Date date = new Date();
        Date startDate = org.apache.commons.lang3.time.DateUtils.addDays(date, -7);
        Date endDate = org.apache.commons.lang3.time.DateUtils.addDays(date, -1);
        cond.setStartDate(DateUtils.format(startDate, "yyyyMMdd"));
        cond.setEndDate(DateUtils.format(endDate, "yyyyMMdd"));
        return gaiaWmsRkysMapper.findDistinctList(cond);
    }

    /**
     * 填充可完成铺货计划的明细列表
     * @param stockQuantity    新品入库数量
     * @param distributionPlan 新品铺货计划
     * @return 可完成铺货计划的明细列表
     */
    private List<GaiaNewDistributionPlanDetail> fillCanCompleteList(BigDecimal stockQuantity, GaiaNewDistributionPlan distributionPlan) {
        List<GaiaNewDistributionPlanDetail> needList = new ArrayList<>();
        GaiaNewDistributionPlanDetail cond = new GaiaNewDistributionPlanDetail();
        cond.setClient(distributionPlan.getClient());
        cond.setPlanCode(distributionPlan.getPlanCode());
        cond.setStatus(PlanDetailStatusEnum.NOT_EXEC.status);
        List<GaiaNewDistributionPlanDetail> detailList = gaiaNewDistributionPlanDetailMapper.findList(cond);

        BigDecimal useQuantity = BigDecimal.ZERO;
        for (GaiaNewDistributionPlanDetail distributionPlanDetail : detailList) {
            if (BigDecimal.ZERO.compareTo(distributionPlanDetail.getPlanQuantity()) == 0) {
                continue;
            }
            BigDecimal leftQuantity = stockQuantity.subtract(useQuantity);
            if (BigDecimal.ZERO.compareTo(leftQuantity) >= 0) {
                break;
            }
            GaiaNewDistributionPlanDetail planDetail = new GaiaNewDistributionPlanDetail();
            BeanUtils.copyProperties(distributionPlanDetail, planDetail);
            BigDecimal needQuantity = distributionPlanDetail.getPlanQuantity().subtract(distributionPlanDetail.getActualQuantity());
            if (leftQuantity.compareTo(needQuantity) >= 0) {
                planDetail.setNeedQuantity(needQuantity);
                useQuantity = useQuantity.add(needQuantity);
            } else {
                planDetail.setNeedQuantity(leftQuantity);
                useQuantity = useQuantity.add(leftQuantity);
            }
            needList.add(planDetail);
        }
        return needList;
    }

    /**
     * 发送铺货消息
     * @param client      加盟商
     * @param productList 商品编码列表
     * @param messageEnum 消息生成枚举
     */
    @Override
    public void generateDistributionMessage(String client, Set<String> productList, GenerateMessageEnum messageEnum) {
        GaiaSdMessage sdMessage = new GaiaSdMessage();
        sdMessage.setClient(client);
        sdMessage.setGsmId("product");//门店、配送中心
        String voucherId = gaiaSdMessageMapper.selectNextVoucherId(client, "company");
        sdMessage.setGsmVoucherId(voucherId);//消息流水号
        switch (messageEnum) {
            case NOT_ENOUGH_STOCK:
                sdMessage.setGsmType(SdMessageTypeEnum.NEW_DISTRIBUTION.code);
                sdMessage.setGsmPage(SdMessageTypeEnum.NEW_DISTRIBUTION.page);
                sdMessage.setGsmRemark("您有<font color=\"#FF0000\">" + productList.size() + "</font>个新品铺货计划因库存不足未处理，请处理。");
                break;
            case NOT_HAS_PLAN:
                sdMessage.setGsmType(SdMessageTypeEnum.STORE_DISTRIBUTION.code);
                sdMessage.setGsmPage(SdMessageTypeEnum.STORE_DISTRIBUTION.page);
                sdMessage.setGsmBusinessVoucherId("N");//日新品提示
                sdMessage.setGsmRemark("您有<font color=\"#FF0000\">" + productList.size() + "</font>个新品已入库，可安排铺货。");
                break;
            case NOT_HAS_PLAN_WEEKLY:
                sdMessage.setGsmType(SdMessageTypeEnum.STORE_DISTRIBUTION.code);
                sdMessage.setGsmPage(SdMessageTypeEnum.STORE_DISTRIBUTION.page);
                sdMessage.setGsmBusinessVoucherId("Y");//周新品提示
                String startDate = DateUtils.format(org.apache.commons.lang3.time.DateUtils.addDays(new Date(), -7), "yyyy-MM-dd");
                String endDate = DateUtils.format(org.apache.commons.lang3.time.DateUtils.addDays(new Date(), -1), "yyyy-MM-dd");
                sdMessage.setGsmRemark(String.format("您有<font color=\"#FF0000\">%s</font>个新品已入库，可安排铺货。(%s至%s)", productList.size(), startDate, endDate));
                break;
            case EXIST_DISTRIBUTION_DIFF:
                sdMessage.setGsmType(SdMessageTypeEnum.NEW_DISTRIBUTION.code);
                sdMessage.setGsmPage(SdMessageTypeEnum.NEW_DISTRIBUTION.page);
                sdMessage.setGsmRemark(String.format("您有<font color=\"#FF0000\">%s</font>个新品铺货计划有差异，请查看。", productList.size()));
                break;
        }
        sdMessage.setGsmFlag("N");//是否查看
        sdMessage.setGsmWarningDay(getMessageProSelfCode(productList));//有效期天数=存储商品编码
        sdMessage.setGsmPlatform("WEB");//消息渠道
        sdMessage.setGsmDeleteFlag("0");
        sdMessage.setGsmArriveDate(DateUtils.formatLocalDate(LocalDate.now(), "yyyyMMdd"));
        sdMessage.setGsmArriveTime(DateUtils.formatLocalDateTime(LocalDateTime.now(), "HHmmss"));
        gaiaSdMessageMapper.insertSelective(sdMessage);
    }

    /**
     * 拼接消息中包含的商品编码字符串
     * @param productCodeList 商品列表
     * @return 商品编码字符串
     */
    private String getMessageProSelfCode(Set<String> productCodeList) {
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String proSelfCode : productCodeList) {
            sb.append(proSelfCode).append(",");
            if (++i >= 60) {
                break;
            }
        }
        return sb.substring(0, sb.length() - 1);
    }

    /**
     * 执行单个新品的铺货计划（可能存在多个计划）
     * @param wmsRkys 新品入库信息
     * @param noPlanMessageMap 消息集合（无计划消息）
     * @param notStockMessageMap 消息集合（有计划、库存不足消息）
     */
    public void execSingleDistributionPlan(WmsRkys wmsRkys, Map<String, Set<String>> noPlanMessageMap, Map<String, Set<String>> notStockMessageMap,String proPosition) {
        try {
            //入库新品是否有铺货计划
            GaiaNewDistributionPlan cond = new GaiaNewDistributionPlan();
            cond.setClient(wmsRkys.getClient());
            cond.setProSelfCode(wmsRkys.getWmSpBm());
            List<GaiaNewDistributionPlan> planList = getWaitAutoList(cond);
            if ((planList == null || planList.size() == 0) && "X".equals(proPosition)) {
                //加入待发消息集合
                addMessageMap(wmsRkys.getClient(), wmsRkys.getWmSpBm(), noPlanMessageMap);
            } else {
                //执行铺货
                generateAutoDistribution(wmsRkys, planList, notStockMessageMap);
            }
        } catch (Exception e) {
            log.error(String.format("<新品铺货><自动铺货><执行异常：%s>", e.getMessage()), e);
        }
    }

    /**
     * 自动执行铺货计划
     * @param wmsRkys    入库新品
     * @param planList   铺货计划
     * @param notStockMessageMap 消息集合【新品入库，库存不足消息】
     */
    private void generateAutoDistribution(WmsRkys wmsRkys, List<GaiaNewDistributionPlan> planList, Map<String, Set<String>> notStockMessageMap) {
        BigDecimal useQuantity = BigDecimal.ZERO;
        for (GaiaNewDistributionPlan distributionPlan : planList) {
            GaiaNewDistributionPlanDetail detailCond = new GaiaNewDistributionPlanDetail();
            detailCond.setClient(distributionPlan.getClient());
            detailCond.setPlanCode(distributionPlan.getPlanCode());
            detailCond.setStatus(PlanDetailStatusEnum.NOT_EXEC.status);
            List<GaiaNewDistributionPlanDetail> planDetailList = gaiaNewDistributionPlanDetailMapper.findList(detailCond);

            //模拟用户信息（client、dcCode）
            TokenUser user = new TokenUser();
            user.setUserId(distributionPlan.getCreateUser());
            user.setClient(distributionPlan.getClient());
            //库存知否充足
            BigDecimal leftQuantity = wmsRkys.getWmShsl().subtract(useQuantity);
            boolean enoughFlag = leftQuantity.compareTo(distributionPlan.getPlanQuantity()) >= 0;
            if (DistributionTypeEnum.MANY_CALL.type.equals(distributionPlan.getDistributionType())) {
                List<GaiaNewDistributionPlanDetail> actualList = fillCanCompleteList(leftQuantity, distributionPlan);
                execAndUpdateDistributionPlan(distributionPlan, actualList, user, Lists.newArrayList(wmsRkys), true);
                useQuantity = useQuantity.add(actualList.stream().map(GaiaNewDistributionPlanDetail::getNeedQuantity).reduce(BigDecimal.ZERO, BigDecimal::add));
            } else if (DistributionTypeEnum.ONCE_CALL.type.equals(distributionPlan.getDistributionType()) && enoughFlag) {
                //填充需要铺货量
                fillNeedQuantity(planDetailList);
                //执行铺货计划明细
                execAndUpdateDistributionPlan(distributionPlan, planDetailList, user, Lists.newArrayList(wmsRkys), true);
                useQuantity = useQuantity.add(distributionPlan.getPlanQuantity());
            } else {
                //加入待发消息集合
                addMessageMap(wmsRkys.getClient(), wmsRkys.getWmSpBm(), notStockMessageMap);
            }
        }
    }

    /**
     * 填充新品铺货计划明细-需要铺货量
     * @param planDetailList 新品铺货计划明细
     */
    private void fillNeedQuantity(List<GaiaNewDistributionPlanDetail> planDetailList) {
        for (GaiaNewDistributionPlanDetail distributionPlanDetail : planDetailList) {
            if (BigDecimal.ZERO.compareTo(distributionPlanDetail.getPlanQuantity()) == 0) {
                continue;
            }
            distributionPlanDetail.setNeedQuantity(distributionPlanDetail.getPlanQuantity());
        }
    }

    /**
     * 封装消息集合
     * @param client 加盟商
     * @param proSelfCode 商品信息
     * @param messageMap 消息集合
     */
    private void addMessageMap(String client,String proSelfCode, Map<String, Set<String>> messageMap) {
        Set<String> strings;
        if (messageMap.containsKey(client)) {
            strings = messageMap.get(client);
        } else {
            strings = new HashSet<>();
        }
        strings.add(proSelfCode);
        messageMap.put(client, strings);
    }

    /**
     * 执行铺货核心流程：生成铺货信息、自动开单、调拨入库开单执行、更新铺货计划（执行数量、铺货单号）
     *
     * @param distributionPlan 新品铺货计划
     * @param planDetailList   需要执行的铺货计划明细
     * @param user             用户信息
     * @param wmsRkysList      入库信息
     */
    private void execAndUpdateDistributionPlan(GaiaNewDistributionPlan distributionPlan, List<GaiaNewDistributionPlanDetail> planDetailList, TokenUser user, List<WmsRkys> wmsRkysList, boolean autoFlag) {
        try {
            //生成铺货信息
            Result result = storeNeedService.autoSelectProductList(distributionPlan.getProSelfCode(), user);
            if (Constant.SUCCESS.equals(result.getCode())) {
                JSONObject data = JSON.parseObject(JSON.toJSONString(result.getData()));
                List<StoreReplenishDetails> replenishDetails = JSON.parseArray(data.getJSONArray("storeReplenishDetailsList").toJSONString(), StoreReplenishDetails.class);
                if (replenishDetails == null || replenishDetails.size() == 0) {
                    return;
                }
                for (StoreReplenishDetails replenishDetail : replenishDetails) {
                    //找出对应门店铺货计划明细
                    List<GaiaNewDistributionPlanDetail> matchDetailList = planDetailList.stream().filter(detail -> detail.getStoreId().equals(replenishDetail.getStoCode())).collect(Collectors.toList());
                    if (matchDetailList.size() == 0) {
                        continue;
                    }
                    GaiaNewDistributionPlanDetail distributionPlanDetail = matchDetailList.get(0);

                    //门店铺货（新）-自动开单
                    List<SaveNeedProListRequestDTO> saveNeedProList = transformSaveNeedProList(distributionPlanDetail, replenishDetail);
                    AutoNeedProDTO autoNeedProDTO = storeNeedService.saveNeedProList(saveNeedProList, user);
                    List<GaiaSdReplenishH> replenishHList = autoNeedProDTO.getReplenishHList();
                    //保存铺货计划明细与铺货主表关系
                    if (replenishHList != null && replenishHList.size() > 0) {
                        GaiaSdReplenishH replenishH = replenishHList.get(0);
                        if (DeliveryModeEnum.COMPANY_SEND.type.equals(replenishH.getGsrhType())) {
                            //调拨开单-自动开单
                            storeNeedService.autoCreateDistributionOrder(replenishHList, autoNeedProDTO.getReplenishDList());
                        } else if (DeliveryModeEnum.ENTRUST_SEND.type.equals(replenishH.getGsrhType())) {
                            //委托配送-自动开单
                            storeNeedService.autoEntrustOrder(replenishH, autoNeedProDTO.getReplenishDList());
                        } else if (DeliveryModeEnum.INTERNAL_PIFA.type.equals(replenishH.getGsrhType())) {
                            // 内部批发-调取批发单
                            Result pfResult = callAndSaveRequestOrder(replenishH, user);
                            if (Constant.SUCCESS.equals(pfResult.getCode())) {
                                //调拨开单-自动开单
                                log.info(String.format("<新品铺货><内部批发><调拨开单：%s>", replenishH.getGsrhVoucherId()));
                                storeNeedService.autoCreateDistributionOrder(replenishHList, autoNeedProDTO.getReplenishDList());
                            }
                        }

                        DistributionPlanRelation planRelation = new DistributionPlanRelation();
                        planRelation.setPlanDetailId(distributionPlanDetail.getId());
                        planRelation.setReplenishCode(replenishH.getGsrhVoucherId());
                        planRelation.setCreateUser(distributionPlan.getCreateUser());
                        distributionPlanRelationMapper.add(planRelation);
                        //更新铺货计划明细
                        distributionPlanDetail.setActualQuantity(distributionPlanDetail.getActualQuantity().add(distributionPlanDetail.getNeedQuantity()));
                        distributionPlanDetail.setStatus(distributionPlanDetail.getPlanQuantity().equals(distributionPlanDetail.getActualQuantity())
                                ? PlanDetailStatusEnum.EXECUTED.status : PlanDetailStatusEnum.NOT_EXEC.status);
                        distributionPlanDetail.setUpdateTime(new Date());
                        gaiaNewDistributionPlanDetailMapper.update(distributionPlanDetail);
                    }
                }
                distributionPlan.setWmRkdh(distributionPlan.getWmRkdh() + "," + getDistributionRkdh(wmsRkysList));
            } else {
                log.error("<新品铺货><执行铺货><铺货信息生成失败：>" + result.getMessage());
            }
            //记录调取时间
            Date currentTime = new Date();
            if (DateUtils.format(distributionPlan.getFirstCallTime()).startsWith("1970-01-01")) {
                distributionPlan.setFirstCallTime(currentTime);
            }
            //记录手动调取标记
            if (autoFlag) {
                distributionPlan.setAutoFlag(PlanCallTypeEnum.AUTO_CALL.type);
            }else if (PlanCallTypeEnum.NOT_CALL.type.equals(distributionPlan.getAutoFlag())) {
                distributionPlan.setAutoFlag(PlanCallTypeEnum.MANUAL_CALL.type);
            }
            distributionPlan.setLastCallTime(currentTime);
            updateDistributionPlan(distributionPlan);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServiceException("执行铺货计划异常", e);
        }
    }

    private Result callAndSaveRequestOrder(GaiaSdReplenishH replenishH, TokenUser user) {
        Result result = new Result();
        try {
            GetRequestGoodsOrderListVO orderListVO = new GetRequestGoodsOrderListVO();
            orderListVO.setGsrhBrId(replenishH.getGsrhBrId());
            orderListVO.setPageNum(1);
            orderListVO.setPageSize(1000);
            orderListVO.setGsrhDateStart(DateUtils.format(new Date(), "yyyyMMdd"));
            orderListVO.setGsrhDateEnd(DateUtils.format(new Date(), "yyyyMMdd"));
            orderListVO.setUser(user);
            //调取请货单
            log.info(String.format("<新品铺货><内部批发><调取请货单，请求参数：%s>", JSON.toJSONString(orderListVO)));
            PageInfo<GoodsOrderDTO> goodsOrderList = salesOrderService.getRequestGoodsOrderList(orderListVO);
            log.info(String.format("<新品铺货><内部批发><调取请货单，响应参数：%s>", JSON.toJSONString(goodsOrderList)));
            if (goodsOrderList != null) {
                List<GoodsOrderDTO> list = goodsOrderList.getList();
                List<GoodsOrderDTO> matchList = list.stream()
                        .filter(order -> replenishH.getGsrhVoucherId().equals(order.getGsrhVoucherId())).collect(Collectors.toList());
                if (matchList.size() > 0) {
                    GoodsOrderDTO goodsOrderDTO = matchList.get(0);
                    //保存请货单
                    GaiaSoHeader soHeaderDTO = transformRequestOrder(goodsOrderDTO);
                    soHeaderDTO.setTokenUser(user);
                    log.info(String.format("<新品铺货><内部批发><保存请货单，请求参数：%s>", JSON.toJSONString(soHeaderDTO)));
                    result = salesOrderService.saveSalesOrder(soHeaderDTO);
                    log.info(String.format("<新品铺货><内部批发><保存请货单，响应参数：%s>", JSON.toJSONString(result)));
                }
            }
        } catch (Exception e) {
            log.error(String.format("<新品铺货><内部批发><调取请货单异常：%s>", e.getMessage()), e);
        }
        return result;
    }

    private GaiaSoHeader transformRequestOrder(GoodsOrderDTO goodsOrderDTO) {
        GaiaSoHeader soHeader = new GaiaSoHeader();
        soHeader.setSoCompanyCode(goodsOrderDTO.getDcCode());
        soHeader.setSoType("SOR");
        soHeader.setSoCustomer2(goodsOrderDTO.getCompadmId());
        soHeader.setSoCustomerId(goodsOrderDTO.getGsrhBrId());
        soHeader.setSoHeadRemark(goodsOrderDTO.getRecallInfoList().get(0).getProSelfCode());
        soHeader.setSoDate(DateUtils.format(new Date(), "yyyyMMdd"));
        List<String> voucherIdList = new ArrayList<>();
        List<SoItem> itemList = new ArrayList<>();
        for (RecallInfo recallInfo : goodsOrderDTO.getRecallInfoList()) {
            SoItem soItem = new SoItem();
            soItem.setGsrdVoucherId(recallInfo.getGsrdVoucherId());
            soItem.setGsrdSerial(recallInfo.getGsrdSerial());
            soItem.setSoBatch(recallInfo.getBatBatch());
            soItem.setSoBatchNo(recallInfo.getBatBatchNo());
            soItem.setSoPrice(recallInfo.getSoPrice());
            soItem.setSoQty(recallInfo.getGsrdProposeQty());
            soItem.setSoLineAmt(recallInfo.getSoPrice().multiply(recallInfo.getGsrdProposeQty()));
            soItem.setSoProCode(recallInfo.getProSelfCode());
            soItem.setSoRate(recallInfo.getPoRate());
            soItem.setSoSiteCode(StringUtil.isNotEmpty(recallInfo.getDcCode()) ? recallInfo.getDcCode() : goodsOrderDTO.getDcCode());
            soItem.setSoUnit(recallInfo.getProUnit());
            itemList.add(soItem);
            voucherIdList.add(soItem.getGsrdVoucherId());
        }
        soHeader.setVoucherIdList(voucherIdList);
        soHeader.setSoItemList(itemList);
        return soHeader;
    }

    private String getDistributionRkdh(List<WmsRkys> wmsRkysList) {
        StringBuilder sb = new StringBuilder();
        for (WmsRkys wmsRkys : wmsRkysList) {
            sb.append(wmsRkys.getWmRkdh()).append(",");
        }
        return sb.toString();
    }

    /**
     * 更新新品铺货计划-实际铺货量、计划状态、完成时间等
     * @param distributionPlan 新品铺货计划
     */
    private void updateDistributionPlan(GaiaNewDistributionPlan distributionPlan) {
        DistributionCountInfo countInfo = gaiaNewDistributionPlanDetailMapper.getPlanCountInfo(distributionPlan);
        distributionPlan.setActualStoreNum(countInfo.getActualStoreNum());
        distributionPlan.setActualQuantity(countInfo.getActualQuantity());
        distributionPlan.setDistributionNum(distributionPlan.getDistributionNum() + 1);
        if (DistributionTypeEnum.ONCE_CALL.type.equals(distributionPlan.getDistributionType())) {
            distributionPlan.setStatus(DistributionPlanEnum.COMPLETED.status);
            distributionPlan.setFinishDate(new Date());
        } else if (DistributionTypeEnum.MANY_CALL.type.equals(distributionPlan.getDistributionType())) {
            if (distributionPlan.getPlanQuantity().equals(distributionPlan.getActualQuantity())) {
                distributionPlan.setStatus(DistributionPlanEnum.COMPLETED.status);
                distributionPlan.setFinishDate(new Date());
            }else {
                distributionPlan.setStatus(DistributionPlanEnum.CALLING.status);
            }
        }
        update(distributionPlan);
    }

    /**
     * 转换铺货信息保存实体
     * @param distributionPlanDetail 计划明细
     * @param replenishDetails 铺货信息
     */
    private List<SaveNeedProListRequestDTO> transformSaveNeedProList(GaiaNewDistributionPlanDetail distributionPlanDetail, StoreReplenishDetails replenishDetails) {
        List<SaveNeedProListRequestDTO> list = new ArrayList<>();
        SaveNeedProListRequestDTO requestDTO = new SaveNeedProListRequestDTO();
        BeanUtils.copyProperties(replenishDetails, requestDTO);
        //铺货数量
        requestDTO.setGsrdNeedQty(distributionPlanDetail.getNeedQuantity());
        //批次信息
        requestDTO.setBatchNoInfoList(replenishDetails.getBatchNoInfoList());
        list.add(requestDTO);
        return list;
    }

    private List<WmsRkys> getDistributionRkysList(GaiaNewDistributionPlan distributionPlan) {
        WmsRkys cond = new WmsRkys();
        cond.setClient(distributionPlan.getClient());
        cond.setWmSpBm(distributionPlan.getProSelfCode());
        cond.setState(2);
        List<WmsRkys> list = gaiaWmsRkysMapper.findList(cond);
        list.removeIf(wmsRkys -> StringUtil.isNotEmpty(distributionPlan.getWmRkdh()) && distributionPlan.getWmRkdh().contains(wmsRkys.getWmRkdh()));
        return list;
    }

}
