package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.module.store.dto.price.ProductBasicListReponseDto;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.28
 */
@Data
public class PriceAdjustmentResp {

    /**
     * 价格类型
     */
    private String prcClass;

    /**
     * 门店集合
     */
    private List<String> storeList;

    /**
     * 明细数据
     */
    private List<ProductBasicListReponseDto> list;
}

