package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.29
 */
@Data
public class AllotPrice {
    /**
     * 供货地点
     */
    @ExcelValidate(index = 0, name = "收货地点", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String alpReceiveSite;
    /**
     * 商品编码
     */
    @ExcelValidate(index = 1, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false)
    private String alpProCode;
    /**
     * 加点比例
     */
    @ExcelValidate(index = 2, name = "加点比例", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String alpAddRate;
    /**
     * 加点金额
     */
    @ExcelValidate(index = 3, name = "加点金额", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String alpAddAmt;
    /**
     * 目录价
     */
    @ExcelValidate(index = 4, name = "目录价", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String alpCataloguePrice;

}
