package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * @author tl
 */
@Data
public class SalesOrderProducts {
    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proCode;

    /**
     * 数量
     */
    @ExcelValidate(index = 1, name = "数量", type = ExcelValidate.DataType.DECIMAL, min = 0.0001)
    private String proQty;

    /**
     * 单价
     */
    @ExcelValidate(index = 2, name = "单价", type = ExcelValidate.DataType.DECIMAL, addRequired = false)
    private String proPrice;

    /**
     * 生产批号
     */
    @ExcelValidate(index = 3, name = "生产批号", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired = false)
    private String proBatch;

    /**
     * 货位号
     */
    @ExcelValidate(index = 4, name = "货位号", type = ExcelValidate.DataType.STRING, maxLength = 15, addRequired = false)
    private String soHwh;
}
