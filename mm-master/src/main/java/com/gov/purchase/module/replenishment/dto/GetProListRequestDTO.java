package com.gov.purchase.module.replenishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GetProListRequestDTO {

    /**
     * 门店编码
     */
    private String dcCode;

    /**
     * 商品查询条件： 商品自编码、商品名、通用名、助记码
     */
    private String proSelfCode;

    /**
     * 加盟商
     */
    private String client;

    /**
     * 委托配送中心
     */
    private String dcWtdc;

    private String flag;

}
