package com.gov.purchase.module.goods.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaProductGspinfo;
import com.gov.purchase.entity.GaiaWorkflowLog;
import com.gov.purchase.module.goods.dto.*;

import java.util.List;
import java.util.Map;


public interface GoodsService {

    /**
     * 药品库搜索
     *
     * @param dto QueryProductBasicRequestDto
     * @return PageInfo<QueryProductBasicResponseDto>
     */
    PageInfo<QueryProductBasicResponseDto> selectGoodsInfo(QueryProductBasicRequestDto dto);

    /**
     * 首营提交
     *
     * @param dto GspinfoSubmitRequestDto
     * @return int
     */
    Map<String, Object> insertGspinfoSubmitInfo(GaiaProductGspinfo dto);

    /**
     * 首营列表
     *
     * @param dto QueryGspRequestDto
     * @return PageInfo<QueryGspResponseDto>
     */
    PageInfo<QueryGspResponseDto> selectQueryGspinfo(QueryGspRequestDto dto);

    /**
     * 首营列表参考地点
     *
     * @return
     */
    String queryReferenceSite();

    /**
     * 首营审批日志
     *
     * @param dto QueryGspinfoWorkflowLogRequestDto
     * @return List<GaiaWorkflowLog>
     */
    List<GaiaWorkflowLog> selectQueryGspinfoWorkflowLogInfo(QueryGspinfoWorkflowLogRequestDto dto);

    /**
     * 首营详情
     *
     * @param dto GspinfoRequestDto
     * @return GaiaProductGspinfo
     */
    QueryGspinfoResponseDto selectQueryGspValue(GspinfoRequestDto dto);

    /**
     * 商品首营自编码返回
     *
     * @param client
     * @param proSite
     * @param code
     * @return
     */
    Result getMaxSelfCodeNum(String client, String proSite, String code);

    /**
     * 商品详情获取
     *
     * @param proCode
     * @return
     */
    Result getProBasicByCode(String proCode);

    /**
     * 手工新增
     *
     * @param gaiaProductGspinfo
     * @return
     */
    Map<String, Object> addBusioness(GaiaProductGspinfo gaiaProductGspinfo);

    Result getSupplierList(String site);

    /**
     * 首营详情
     */
    GaiaProductGspinfo getGspinfoDetails(GetGspinfoDetailsDTO dto);

    /**
     * 导出首营列表
     * @param dto
     * @return
     */
    Result exportSelectQueryGspinfo(QueryGspRequestDto dto) throws Exception;
}
