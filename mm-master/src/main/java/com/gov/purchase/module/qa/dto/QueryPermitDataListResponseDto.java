package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "证照资质列表输出结果")
public class QueryPermitDataListResponseDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "加盟商名称", name = "francName")
    private String francName;

    @ApiModelProperty(value = "实体分类", name = "perEntityClass")
    private String perEntityClass;

    @ApiModelProperty(value = "实体编号", name = "perEntityId")
    private String perEntityId;

    @ApiModelProperty(value = "实体名称", name = "perEntityName")
    private String perEntityName;
}
