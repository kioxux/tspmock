package com.gov.purchase.module.purchase.dto;

import lombok.Data;

@Data
public class QueryPayStatus {

    /**
     * 消息id
     */
    private String msgId;

    /**
     * 订单号
     */
    private String billNo;

    /**
     * 账单日期
     */
    private String billDate;
    /**
     * 支付类型
     */
    private Integer ficoType;
}
