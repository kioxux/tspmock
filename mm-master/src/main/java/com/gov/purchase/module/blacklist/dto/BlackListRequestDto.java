package com.gov.purchase.module.blacklist.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户首营列表传入参数")
public class BlackListRequestDto extends Pageable {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "地点", name = "proSite")
    String proSite;

    @ApiModelProperty(value = "商品自编码", name = "proSelfCode")
    String proSelfCode;

    @ApiModelProperty(value = "生产批号", name = "proBatchNo")
    String proBatchNo;

    @ApiModelProperty(value = "创建人", name = "proCreateUser")
    String proCreateUser;

    @ApiModelProperty(value = "创建日期", name = "proCreateDate")
    String proCreateDate;

    @ApiModelProperty(value = "创建时间", name = "proCreateTime")
    String proCreateTime;

    @ApiModelProperty(value = "变更人", name = "proChangeUser")
    String proChangeUser;

    @ApiModelProperty(value = "变更日期", name = "proChangeDate")
    String proChangeDate;

    @ApiModelProperty(value = "变更时间", name = "proChangeTime")
    String proChangeTime;

    String[] proBatchNoList;

}
