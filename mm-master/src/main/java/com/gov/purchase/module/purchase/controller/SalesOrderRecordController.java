package com.gov.purchase.module.purchase.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.purchase.dto.SalesOrderRecordDTO;
import com.gov.purchase.module.purchase.service.SalesOrderRecordService;
import com.gov.purchase.module.wholesale.dto.QuerySalesOrderList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 药品采购记录查询
 * @author: yzf
 * @create: 2021-11-04 11:14
 */
@Api(tags = "药品采购记录查询")
@RestController
@RequestMapping("salesOrderRecord")
public class SalesOrderRecordController {

    @Resource
    private SalesOrderRecordService salesOrderRecordService;

    /**
     * 销售退回记录明细
     *
     * @return
     */
    @Log("销售退回记录明细")
    @ApiOperation("销售退回记录明细")
    @PostMapping("querySalesOrderBackRecord")
    public Result querySalesOrderBackRecord(@RequestBody SalesOrderRecordDTO salesOrderRecordDTO) {
        salesOrderRecordDTO.setType("SRE");
        return ResultUtil.success(salesOrderRecordService.querySalesRecord(salesOrderRecordDTO));
    }

    /**
     * 销售退回记录导出
     *
     * @param salesOrderRecordDTO
     * @return
     */
    @Log("销售退回记录导出")
    @ApiOperation("销售退回记录导出")
    @PostMapping("exportSalesOrderBackRecord")
    public Result exportSalesOrderBackRecord(@RequestBody SalesOrderRecordDTO salesOrderRecordDTO) {
        salesOrderRecordDTO.setType("SRE");
        return salesOrderRecordService.exportSalesOrderBackRecord(salesOrderRecordDTO);
    }

    /**
     * 销售记录明细
     *
     * @return
     */
    @Log("销售记录明细")
    @ApiOperation("销售记录明细")
    @PostMapping("querySalesOrderRecord")
    public Result querySalesOrderRecord(@RequestBody SalesOrderRecordDTO salesOrderRecordDTO) {
        salesOrderRecordDTO.setType("SOR");
        return ResultUtil.success(salesOrderRecordService.querySalesRecord(salesOrderRecordDTO));
    }

    /**
     * 销售记录导出
     *
     * @param salesOrderRecordDTO
     * @return
     */
    @Log("销售记录导出")
    @ApiOperation("销售记录导出")
    @PostMapping("exportSalesOrderRecord")
    public Result exportSalesOrderRecord(@RequestBody SalesOrderRecordDTO salesOrderRecordDTO) {
        salesOrderRecordDTO.setType("SOR");
        return salesOrderRecordService.exportSalesOrderRecord(salesOrderRecordDTO);
    }
}
