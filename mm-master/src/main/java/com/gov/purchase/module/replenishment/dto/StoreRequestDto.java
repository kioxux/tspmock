package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "获取门店集合请求参数")
public class StoreRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "配送中心", name = "stoChainHead", required = true)
    @NotBlank(message = "配送中心不能为空")
    private String stoChainHead;

}
