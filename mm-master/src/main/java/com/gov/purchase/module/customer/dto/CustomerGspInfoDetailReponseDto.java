package com.gov.purchase.module.customer.dto;

import com.gov.purchase.entity.GaiaCustomerGspinfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户首营信息详情返回参数")
public class CustomerGspInfoDetailReponseDto extends GaiaCustomerGspinfo {

    private String client;

    @ApiModelProperty(value = "客户编号", name = "cusSelfCode")
    String cusCode;

    @ApiModelProperty(value = "客户编号（企业）", name = "cusSelfCode")
    private  String cusSelfCode;

    @ApiModelProperty(value = "地点", name = "cusSite")
    private  String cusSite;

    @ApiModelProperty(value = "客户名称", name = "cusName")
    private  String cusName;

    @ApiModelProperty(value = "营业执照编号", name = "cusCreditCode")
    private  String cusCreditCode;

    @ApiModelProperty(value = "营业期限", name = "cusCreditDate")
    private   String cusCreditDate;

    @ApiModelProperty(value = "法人", name = "cusLegalPerson")
    private   String cusLegalPerson;

    @ApiModelProperty(value = "注册地址", name = "cusRegAdd")
    private   String cusRegAdd;

    @ApiModelProperty(value = "许可证编号", name = "cusLicenceNo")
    private  String cusLicenceNo;

    @ApiModelProperty(value = "发证单位", name = "cusLicenceOrgan")
    private  String cusLicenceOrgan;

    @ApiModelProperty(value = "发证日期", name = "cusLicenceDate")
    private  String cusLicenceDate;

    @ApiModelProperty(value = "有效期至", name = "cusLicenceValid")
    private  String cusLicenceValid;

    @ApiModelProperty(value = "邮政编码", name = "cusPostalCode")
    private String cusPostalCode;

    @ApiModelProperty(value = "税务登记证", name = "cusTaxCard")
    private  String cusTaxCard;

    @ApiModelProperty(value = "组织机构代码证", name = "cusOrgCard")
    private String cusOrgCard;

    @ApiModelProperty(value = "开户户名", name = "cusAccountName")
    private  String cusAccountName;

    @ApiModelProperty(value = "开户银行", name = "cusAccountBank")
    private  String cusAccountBank;

    @ApiModelProperty(value = "银行账号", name = "cusBankAccount")
    private String cusBankAccount;

    @ApiModelProperty(value = "随货同行单（票）情况", name = "cusDocState")
    private  String cusDocState;

    @ApiModelProperty(value = "企业印章情况", name = "cusSealState")
    private String cusSealState;

    @ApiModelProperty(value = "生产或经营范围", name = "cusScope")
    private  String cusScope;

    @ApiModelProperty(value = "经营方式", name = "cusOperationMode")
    private   String cusOperationMode;

    @ApiModelProperty(value = "地点名称", name = "stoName")
    private   String stoName;

    private String cusFlowNo;

    private String cusGspDate;


}
