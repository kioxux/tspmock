package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class GspinfoRequestDto {
    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码", name = "proSelfCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "proSite", required = true)
    @NotBlank(message = "地点不能为空")
    private String proSite;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", name = "proCode", required = true)
    private String proCode;

    @ApiModelProperty(value = "工作流编号", name = "proFlowNo", required = true)
    private String proFlowNo;
}