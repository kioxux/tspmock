package com.gov.purchase.module.store.dto.price;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(value = "调价单查看导出传入参数")
public class PriceListExportRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    private String client;

    @ApiModelProperty(value = "价格组ID", name = "prcGroupId")
    private String prcGroupId;

    @ApiModelProperty(value = "门店编号", name = "prcStore")
    private List<String> prcStore;

    @ApiModelProperty(value = "调价单号", name = "prcModfiyNo")
    private String prcModfiyNo;

    @ApiModelProperty(value = "商品编号", name = "prcProduct")
    private String prcProduct;

    @ApiModelProperty(value = "价格类型(P001-零售价、P002-会员价、P003-医保价、P004-会员日价、P005拆零价、P006网上零售价、P007-网上会员价)", name = "prcClass")
    private String prcClass;

    @ApiModelProperty(value = "调价日期", name = "prcEffectDate")
    private String prcEffectDate;

    @ApiModelProperty(value = "查询类型 1.按价格查询  2.按商品查询", name = "type")
    @NotBlank(message = "查询类型不可为空")
    private String type;
}
