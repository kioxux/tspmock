package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class StoreNeedRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 商品编码
     */
    @ApiModelProperty(value = "商品编码", name = "proCode", required = true)
    private String proCode;

    @ApiModelProperty(value = "配送中心", name = "dcCode", required = true)
    @NotBlank(message = "配送中心不能为空")
    private String dcCode;

    @ApiModelProperty(value = "门店编号", name = "stoCode", required = true)
    @NotBlank(message = "门店编号不能为空")
    private String stoCode;

    @ApiModelProperty(value = "商品自编码", name = "batProCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String batProCode;

    @ApiModelProperty(value = "铺货日期", name = "stoNeedArrivalDate", required = true)
    @NotBlank(message = "铺货日期不能为空")
    private String stoNeedArrivalDate;

    @ApiModelProperty(value = "铺货数量", name = "stoNeedQty", required = true)
    @NotNull(message = "铺货数量不能为空")
    @DecimalMin(value = "0.0", message = "铺货数量不能为负数")
    @DecimalMax(value = "999999999.9999", message = "铺货数量格式不正确")
    private BigDecimal stoNeedQty;

    // 批号
    @ApiModelProperty(value = "生产批号", name = "gsrdBatchNo", required = true)
    private String gsrdBatchNo;

}
