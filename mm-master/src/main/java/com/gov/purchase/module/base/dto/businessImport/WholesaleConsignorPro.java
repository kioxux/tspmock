package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * @author tl
 */
@Data
public class WholesaleConsignorPro {
    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proCode;

}
