package com.gov.purchase.module.goods.dto;

import java.io.Serializable;

/**
 * 委托配送第三方商品编码对照表(GaiaSdWtpro)实体类
 *
 * @author makejava
 * @since 2021-08-13 15:53:54
 */
public class GaiaSdWtproDTO implements Serializable {
    private static final long serialVersionUID = 179084735103877095L;
    /**
     * 主键ID
     */
    private Long id;
    /**
     * 加盟商
     */
    private String client;
    /**
     * 药德商品自编码
     */
    private String proSelfCode;
    /**
     * 第三方商品编码
     */
    private String proDsfCode;
    /**
     * 第三方商品名
     */
    private String proDsfName;
    /**
     * 药德/原始内码
     */
    private String proNm;
    /**
     * 状态 0：审核中 1：已审核
     */
    private Integer status;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getProSelfCode() {
        return proSelfCode;
    }

    public void setProSelfCode(String proSelfCode) {
        this.proSelfCode = proSelfCode;
    }

    public String getProDsfCode() {
        return proDsfCode;
    }

    public void setProDsfCode(String proDsfCode) {
        this.proDsfCode = proDsfCode;
    }

    public String getProDsfName() {
        return proDsfName;
    }

    public void setProDsfName(String proDsfName) {
        this.proDsfName = proDsfName;
    }

    public String getProNm() {
        return proNm;
    }

    public void setProNm(String proNm) {
        this.proNm = proNm;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
