package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductBusiness;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/26 10:14
 */
@Data
public class ProEditListVO {
    @NotNull(message = "商品列表不能为空")
    private List<GaiaProductBusiness> businessList;

    private String remarks;

    private Integer editNoWws;
}
