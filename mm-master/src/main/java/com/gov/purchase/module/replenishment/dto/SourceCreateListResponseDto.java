package com.gov.purchase.module.replenishment.dto;

import com.gov.purchase.entity.GaiaSourceList;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceCreateListResponseDto extends GaiaSourceList {

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用代码
     */
    private String supCreditCode;
}
