package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

@Data
public class StoreDistribution {

    /**
     * 门店编号
     */
    @ExcelValidate(index = 0, name = "门店编码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired=true)
    private String stoNeedStore;

    /**
     * 商品编号
     */
    @ExcelValidate(index = 1, name = "商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50, addRequired=true)
    private String stoNeedPro;

    /**
     * 铺货日期
     */
    @ExcelValidate(index = 2, name = "铺货日期", type = ExcelValidate.DataType.DATE, maxLength = 8, dateFormat = "yyyyMMdd", addRequired = true)
    private String stoNeedArrivalDate;


    /**
     * 铺货数量
     */
    @ExcelValidate(index = 3, name = "计划铺货数量", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 9999999999.9999, addRequired = true)
    private String stoNeedQty;

    /**
     * 批次
     */
    @ExcelValidate(index = 4, name = "批次", type = ExcelValidate.DataType.STRING, addRequired = false, maxLength = 20)
    private String stoNeedBatch;

    /**
     * 备注
     */
    @ExcelValidate(index = 5, name = "备注", type = ExcelValidate.DataType.STRING, addRequired = false, maxLength = 200)
    private String stoNeedRemark;




}
