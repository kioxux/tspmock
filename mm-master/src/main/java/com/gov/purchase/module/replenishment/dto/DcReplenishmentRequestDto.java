package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@ApiModel(value = "保存DC补货请求参数")
public class DcReplenishmentRequestDto {

    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "DC编号", name = "dcCode", required = true)
    @NotBlank(message = "DC编号不能为空")
    private String dcCode;

    @ApiModelProperty(value = "商品自编码", name = "poProCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String poProCode;

    @ApiModelProperty(value = "商品单位", name = "poUnit", required = true)
    @NotBlank(message = "商品单位不能为空")
    private String poUnit;

    @ApiModelProperty(value = "采购价格", name = "poPrice", required = true)
    @NotNull(message = "采购价格不能为空")
    @DecimalMin(value = "0.0", message = "采购价格不能为负数")
    @DecimalMax(value = "999999999.9999", message = "采购价格格式不正确")
    private BigDecimal poPrice;

    @ApiModelProperty(value = "供应商自编码", name = "poSupplierId", required = true)
    @NotBlank(message = "供应商自编码不能为空")
    private String poSupplierId;

    @ApiModelProperty(value = "采购数量", name = "poQty", required = true)
    @NotNull(message = "采购数量不能为空")
    private BigDecimal poQty;

    @ApiModelProperty(value = "税率", name = "poRate", required = true)
    @NotBlank(message = "税率不能为空")
    private String poRate;

    @ApiModelProperty(value = "计划到货日期", name = "poDeliveryDate", required = true)
    //@NotBlank(message = "计划到货日期不能为空")
    private String poDeliveryDate;

    @ApiModelProperty(value = "行备注", name = "poLineRemark")
    private String poLineRemark;

    /**
     * 付款条件
     */
    @ApiModelProperty(value = "付款条件", name = "supPayTerm")
    private String supPayTerm;

    //前序订单单号
    @ApiModelProperty(value = "前序订单单号", name = "poPrePoid")
    private String poPrePoid;
    //前序订单行号
    @ApiModelProperty(value = "前序订单行号", name = "poPrePoitem")
    private String poPrePoitem;
    @ApiModelProperty(value = "补货日期", name = "gsrhDate")
    private String gsrhDate;

    //@NotBlank(message = "存储条件不为空")
    private String proStorageCondition;
    /**
     * 生产批号
     */
    private String poBatchNo;
    /**
     * 生产日期
     */
    private String poScrq;
    /**
     * 有效期
     */
    private String poYxq;

    /**
     * 自定义字段
     */
    private String proZdy1;
    /**
     * 自定义字段
     */
    private String proZdy2;
    /**
     * 自定义字段
     */
    private String proZdy3;
    /**
     * 自定义字段
     */
    private String proZdy4;
    /**
     * 自定义字段
     */
    private String proZdy5;

    /**
     * 供应商业务员
     */
    private String poSupplierSalesman;

    /**
     * 供应商业务员
     */
    private String poSalesmanName;

    /**
     * 货主
     */
    private String poOwner;

    /**
     * 货主
     */
    private String poOwnerName;
}
