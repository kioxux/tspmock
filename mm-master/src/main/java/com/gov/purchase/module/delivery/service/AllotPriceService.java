package com.gov.purchase.module.delivery.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.*;
import com.gov.purchase.module.delivery.dto.*;

import java.util.List;

public interface AllotPriceService {

    /**
     * 调拨价格门店集合
     */
    Result getAlpReceiveSite();

    /**
     * 调拨价格查询
     * @param alpReceiveSite
     * @param alpProCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<GaiaAllotPriceDto> selectAllotPriceList(String alpReceiveSite, String alpProCode, Integer pageNum, Integer pageSize);

    /**
     * 商品列表
     * @param alpReceiveSite
     * @param alpProCode
     * @return
     */
    List<GaiaProductBusiness> getProductList(String alpReceiveSite, String alpProCode);

    /**
     * 调拨价格保存
     * @param list
     */
    void saveAllotPrice(List<GaiaAllotPrice> list);

    /**
     * 调价删除
     * @param list
     */
    void deleteAllotPrice(List<GaiaAllotPrice> list);

    /**
     * 调拨价格提交通过弹出框
     * @param dto
     */
    void saveAllotPriceByPopup(GaiaAllotPrice dto);

    /**
     * 导出调拨价格列表
     * @param alpReceiveSite
     * @param alpProCode
     * @return
     */
    Result exportData(String alpReceiveSite, String alpProCode);

    /**
     * 客户调拨价格列表
     *
     * @param alpReceiveSite
     * @param alpCusCode
     * @param alpProCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<GaiaAllotPriceCusDto> queryAllotPriceCusList(String alpReceiveSite, String alpCusCode, String alpProCode, Integer pageNum, Integer pageSize);

    /**
     * 客户调拨价格提交
     *
     * @param list
     */
    void saveAllotPriceCus(List<GaiaAllotPriceCus> list);

    /**
     * 客户调拨价格删除
     *
     * @param list
     */
    void deleteAllotPriceCus(List<GaiaAllotPriceCus> list);

    /**
     * 导出客户调拨价格列表
     *
     * @param alpReceiveSite
     * @param alpCusCode
     * @param alpProCode
     * @return
     */
    Result exportAllotPriceCusData(String alpReceiveSite, String alpCusCode, String alpProCode);


    /**
     * 客户调拨价格提交通过弹出框
     *
     * @param dto
     */
    void saveAllotPriceByPopupCus(GaiaAllotPriceCus dto);

    /**
     * 当前加盟商下的DC列表
     *
     * @return
     */
    List<GaiaDcData> getDCListByCurrentClient();

    List<GaiaAllotPriceGroup> queryAllotPriceGroupList(String alpReceiveSite, String gapgType);

    Result saveAllotPriceGroup(SaveAllotPriceGroupDTO dto);

    PageInfo<GaiaAllotProductPriceVO> queryAllotProductPriceList(Integer pageSize, Integer pageNum, String alpReceiveSite, String gapgType, String gapgCode);

    Result saveAllotProductPrice(SaveGaiaAllotProductPriceDTO dto);

    Result deleteAllotPriceGroup(SaveAllotPriceGroupDTO dto);

    PageInfo<GaiaAllotGroupPriceVO> queryAllotGroupPriceList(Integer pageSize, Integer pageNum, String alpReceiveSite, String gapgCode, String gapgType);

    Result deleteAllotProductPrice(SaveGaiaAllotProductPriceDTO dto);

    List<StoreVO> queryStoreList(String alpReceiveSite, String gapgType);

    Result saveAllotGroupPrice(SaveAllotGroupPriceVO dto);

    Result deleteAllotGroupPrice(SaveAllotGroupPriceVO dto);

    Result exportAllotProductPriceList(Integer pageSize, Integer pageNum, String alpReceiveSite, String gapgType, String gapgCode);
}


