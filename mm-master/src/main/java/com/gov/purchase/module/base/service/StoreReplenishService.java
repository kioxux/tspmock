package com.gov.purchase.module.base.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.replenishment.dto.DcReplenishmentRequestDto;
import com.gov.purchase.module.replenishment.dto.QueryReplenishReq;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.05.27
 */
public interface StoreReplenishService {

    /**
     * 门店补货查询
     * @return
     */
    Result queryReplenishList(QueryReplenishReq queryReplenishReq);

    /**
     * 门店补货
     */
    void saveReplenish(List<DcReplenishmentRequestDto> list);

    /**
     * 导出
     * @param queryReplenishReq
     * @return
     */
    Result exportList(QueryReplenishReq queryReplenishReq);

    /**
     * 门店补货金额确认
     * @param list
     * @return
     */
    Result checkAmt(List<DcReplenishmentRequestDto> list);
}
