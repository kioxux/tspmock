package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaSoHeader;
import lombok.Data;

/**
 * @author tl
 */
@Data
public class GaiaSoHeaderVO extends GaiaSoHeader {

    /**
     * 前序单号
     */
    private String cusName;
}