package com.gov.purchase.module.store.dto.store;

import com.gov.purchase.entity.GaiaStoreData;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class EditStoDetailsRequestDTO extends GaiaStoreData {

    @NotBlank(message = "门店编码不能为空")
    private String stoCode;
    @NotBlank(message = "门店名称不能为空")
    private String stoName;
    @NotBlank(message = "门店简称不能为空")
    private String stoShortName;
    @NotBlank(message = "门店状态不能为空")
    private String stoStatus;
    @NotBlank(message = "门店属性不能为空")
    private String stoAttribute;
    @NotBlank(message = "配送方式不能为空")
    private String stoDeliveryMode;
    @NotBlank(message = "经营面积不能为空")
    private String stoArea;
    @NotBlank(message = "省不能为空")
    private String stoProvince;
    @NotBlank(message = "城市不能为空")
    private String stoCity;
    @NotBlank(message = "区不能为空")
    private String stoDistrict;
    @NotBlank(message = "详细地址不能为空")
    private String stoAdd;
    @NotBlank(message = "开业日期不能为空")
    private String stoOpenDate;
    @NotBlank(message = "税分类不能为空")
    private String stoTaxClass;
    @NotBlank(message = "税率不能为空")
    private String stoTaxRate;

}
