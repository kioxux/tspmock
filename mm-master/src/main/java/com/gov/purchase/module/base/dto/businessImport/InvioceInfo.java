package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import com.gov.purchase.constants.CommonEnum;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.05
 */
@Data
public class InvioceInfo {

    /**
     * 门店编码
     */
    @ExcelValidate(index = 0, name = "付款主体编码", type = ExcelValidate.DataType.STRING, maxLength = 10)
    private String companyCode;

    /**
     * 纳税主体编码
     */
    @ExcelValidate(index = 1, name = "纳税主体编码", type = ExcelValidate.DataType.STRING, maxLength = 10, addRequired = false)
    private String taxCompanyCode;

    /**
     * 纳税主体名称
     */
    private String taxCompanyName;

    /**
     * 发票类型
     * 1-增值税普通发票；2-增值税专用发票
     */
    @ExcelValidate(index = 2, name = "发票类型", type = ExcelValidate.DataType.STRING, maxLength = 7,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.INVIOCE_TYPE, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String invioceType;

    /**
     * 统一社会信用码
     */
    @ExcelValidate(index = 3, name = "统一社会信用码", type = ExcelValidate.DataType.STRING, maxLength = 20)
    private String socialCreditCode;

    /**
     * 开户行名称
     */
    @ExcelValidate(index = 4, name = "开户行名称", type = ExcelValidate.DataType.STRING, maxLength = 40, addRequired = false)
    private String bankName;

    /**
     * 开户行账号
     */
    @ExcelValidate(index = 5, name = "开户行账号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false)
    private String bankNumber;

    /**
     * 地址
     */
    @ExcelValidate(index = 6, name = "地址", type = ExcelValidate.DataType.STRING, maxLength = 40, addRequired = false)
    private String companyAddress;

    /**
     * 电话号码
     */
    @ExcelValidate(index = 7, name = "电话号码", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false)
    private String companyPhone;

    /**
     * 付款银行
     */
    @ExcelValidate(index = 8, name = "付款银行", type = ExcelValidate.DataType.STRING, maxLength = 40, addRequired = false)
    private String payingbankName;

    /**
     * 付款银行账号
     */
    @ExcelValidate(index = 9, name = "付款银行账号", type = ExcelValidate.DataType.STRING, maxLength = 20, addRequired = false)
    private String payingbankNumber;

    /**
     * 纳税属性
     */
    @ExcelValidate(index = 10, name = "纳税属性", type = ExcelValidate.DataType.STRING, maxLength = 1, addRequired = false,
            dictionaryStaticData = CommonEnum.DictionaryStaticData.FICO_TAX_ATTRIBUTES, dictionaryStaticType = ExcelValidate.DictionaryStaticType.LABEL)
    private String ficoTaxAttributes;
}
