package com.gov.purchase.module.base.service.impl;

import com.gov.purchase.common.utils.UUIDUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.entity.GaiaJyfwData;
import com.gov.purchase.entity.GaiaJyfwDataGspinfoKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.BusinessScopeServiceMapper;
import com.gov.purchase.module.base.dto.JyfwDTO;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.base.dto.businessScope.GetScopeListDTO;
import com.gov.purchase.module.base.dto.businessScope.JyfwDataDO;
import com.gov.purchase.module.base.service.BusinessScopeService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.JyfwooData;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BusinessScopeServiceImpl implements BusinessScopeService {

    @Resource
    private FeignService feignService;
    @Resource
    private BusinessScopeServiceMapper businessScopeServiceMapper;

    /**
     * 经营范围列表
     *
     * @param jyfwOrgid
     * @param jyfwOrgname
     * @param jyfwSite
     * @param jyfwType
     * @return
     */
    @Override
    public List<GetScopeListDTO> getScopeList(String jyfwOrgid, String jyfwOrgname, String jyfwSite, Integer jyfwType, String jyfwClass) {
        if (CommonEnum.JyfwType.SUPPLIER.getCode().equals(jyfwType) && StringUtils.isBlank(jyfwSite)) {
            throw new CustomResultException("供应商经营范围查询时,地点不能为空");
        }
        if (CommonEnum.JyfwType.CUSTOMER.getCode().equals(jyfwType) && StringUtils.isBlank(jyfwSite)) {
            throw new CustomResultException("客户经营范围查询时,地点不能为空");
        }
        TokenUser user = feignService.getLoginInfo();
        return businessScopeServiceMapper.getScopeList(user.getClient(), jyfwOrgid, jyfwOrgname, jyfwSite, jyfwType, jyfwClass);
    }

    /**
     * 经营范围编辑保存
     *
     * @param vo
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editScope(EditScopeVO vo) {
        TokenUser user = feignService.getLoginInfo();
        if (CommonEnum.JyfwType.SUPPLIER.getCode().equals(vo.getJyfwType()) && StringUtils.isBlank(vo.getJyfwSite())) {
            throw new CustomResultException("供应商经营范围编辑时,地点不能为空");
        }
        if (CommonEnum.JyfwType.CUSTOMER.getCode().equals(vo.getJyfwType()) && StringUtils.isBlank(vo.getJyfwSite())) {
            throw new CustomResultException("客户经营范围编辑时,地点不能为空");
        }
        // 全删
        businessScopeServiceMapper.deleteScope(user.getClient(), vo.getJyfwOrgid(), vo.getJyfwOrgname(), vo.getJyfwSite(), vo.getJyfwType());
        // 保存
        List<JyfwDataDO> jyfwDataDOList = vo.getScopeList().stream().filter(Objects::nonNull).filter(scope -> scope.getChecked().equals(1)).map(scope -> {
            JyfwDataDO jyfwDataDO = new JyfwDataDO();
            jyfwDataDO.setId(UUIDUtil.getUUID());
            jyfwDataDO.setClient(user.getClient());
            jyfwDataDO.setJyfwOrgid(vo.getJyfwOrgid());
            jyfwDataDO.setJyfwOrgname(vo.getJyfwOrgname());
            jyfwDataDO.setJyfwType(vo.getJyfwType());
            jyfwDataDO.setJyfwSite(vo.getJyfwSite());
            jyfwDataDO.setJyfwId(scope.getJyfwId());
            jyfwDataDO.setJyfwName(scope.getJyfwName());
            jyfwDataDO.setJyfwModiDate(DateUtils.getCurrentDateStrYYMMDD());
            jyfwDataDO.setJyfwModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            jyfwDataDO.setJyfwModiId(user.getUserId());
            return jyfwDataDO;
        }).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(jyfwDataDOList)) {
            businessScopeServiceMapper.saveBatchJyfwData(jyfwDataDOList);
        }

    }

    /**
     * 经营范围编辑保存 审批回调
     */
    @Override
    public void editScopeApproval(EditScopeVO vo, String client, String userId) {
        if (CommonEnum.JyfwType.SUPPLIER.getCode().equals(vo.getJyfwType()) && StringUtils.isBlank(vo.getJyfwSite())) {
            throw new CustomResultException("供应商经营范围编辑时,地点不能为空");
        }
        if (CommonEnum.JyfwType.CUSTOMER.getCode().equals(vo.getJyfwType()) && StringUtils.isBlank(vo.getJyfwSite())) {
            throw new CustomResultException("客户经营范围编辑时,地点不能为空");
        }
        // 全删
        businessScopeServiceMapper.deleteScope(client, vo.getJyfwOrgid(), vo.getJyfwOrgname(), vo.getJyfwSite(), vo.getJyfwType());
        // 保存
        List<JyfwDataDO> jyfwDataDOList = vo.getScopeList().stream().filter(Objects::nonNull).filter(scope -> scope.getChecked().equals(1)).map(scope -> {
            JyfwDataDO jyfwDataDO = new JyfwDataDO();
            jyfwDataDO.setId(UUIDUtil.getUUID());
            jyfwDataDO.setClient(client);
            jyfwDataDO.setJyfwOrgid(vo.getJyfwOrgid());
            jyfwDataDO.setJyfwOrgname(vo.getJyfwOrgname());
            jyfwDataDO.setJyfwType(vo.getJyfwType());
            jyfwDataDO.setJyfwSite(vo.getJyfwSite());
            jyfwDataDO.setJyfwId(scope.getJyfwId());
            jyfwDataDO.setJyfwName(scope.getJyfwName());
            jyfwDataDO.setJyfwModiDate(DateUtils.getCurrentDateStrYYMMDD());
            jyfwDataDO.setJyfwModiTime(DateUtils.getCurrentTimeStrHHMMSS());
            jyfwDataDO.setJyfwModiId(userId);
            return jyfwDataDO;
        }).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(jyfwDataDOList)) {
            businessScopeServiceMapper.saveBatchJyfwData(jyfwDataDOList);
        }
    }

    /**
     * 特殊经营范围配置表
     *
     * @param client      加盟商
     * @param userId      用户id
     * @param jyfwOrgid   机构
     * @param jyfwOrgname 机构名
     * @param jyfwSite    地点
     * @param jyfwType    类型
     * @param jyfwList    范围列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveJyfwData(String client, String userId,
                             String jyfwOrgid, String jyfwOrgname, String jyfwSite, Integer jyfwType,
                             List<JyfwDTO> jyfwList) {
        List<GaiaJyfwData> jyfwDOList = jyfwList.stream().filter(Objects::nonNull)
                // 1.选中
                .filter(item -> CommonEnum.NoYesStatusInteger.YES.getCode().equals(item.getChecked()))
                .map(item -> {
                    GaiaJyfwData jyfwData = new GaiaJyfwData();
                    jyfwData.setId(UUIDUtil.getUUID());
                    jyfwData.setClient(client);
                    jyfwData.setJyfwOrgid(jyfwOrgid);
                    jyfwData.setJyfwOrgname(jyfwOrgname);
                    jyfwData.setJyfwId(item.getJyfwId());
                    jyfwData.setJyfwName(item.getJyfwName());
                    jyfwData.setJyfwCreDate(DateUtils.getCurrentDateStrYYMMDD());
                    jyfwData.setJyfwCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                    jyfwData.setJyfwCreId(userId);
                    jyfwData.setJyfwModiDate(DateUtils.getCurrentDateStrYYMMDD());
                    jyfwData.setJyfwModiTime(DateUtils.getCurrentTimeStrHHMMSS());
                    jyfwData.setJyfwModiId(userId);
                    jyfwData.setJyfwSite(jyfwSite);
                    jyfwData.setJyfwType(jyfwType);
                    return jyfwData;
                }).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(jyfwDOList)) {
            businessScopeServiceMapper.saveJyfwDataList(jyfwDOList);
        }
    }

    /**
     * 首营回调保存
     *
     * @param client         加盟商
     * @param userId         用户id
     * @param jyfwOrgid      机构
     * @param jyfwOrgname    机构名
     * @param jyfwSite       地点
     * @param jyfwType       类型
     * @param jyfwList       范围列表
     * @param jyfwooDataList 所有特殊经营范围 k-v,
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveJyfwDataFromApproval(String client, String userId,
                                         String jyfwOrgid, String jyfwOrgname, String jyfwSite, Integer jyfwType,
                                         List<GaiaJyfwDataGspinfoKey> jyfwList, List<JyfwooData> jyfwooDataList) {
        if (jyfwList == null) {
            return;
        }
        List<GaiaJyfwData> jyfwDOList = jyfwList.stream().filter(Objects::nonNull)
                .map(item -> {
                    GaiaJyfwData jyfwData = new GaiaJyfwData();
                    jyfwData.setId(UUIDUtil.getUUID());
                    jyfwData.setClient(client);
                    jyfwData.setJyfwOrgid(jyfwOrgid);
                    jyfwData.setJyfwOrgname(jyfwOrgname);
                    jyfwData.setJyfwId(item.getJyfwId());
                    String jyfwName = jyfwooDataList.stream().filter(Objects::nonNull).filter(jyfwooData -> jyfwData.getJyfwId().equals(item.getJyfwId())).findFirst().orElse(new JyfwooData()).getJyfwName();
                    jyfwData.setJyfwName(jyfwName);
                    jyfwData.setJyfwCreDate(DateUtils.getCurrentDateStrYYMMDD());
                    jyfwData.setJyfwCreTime(DateUtils.getCurrentTimeStrHHMMSS());
                    jyfwData.setJyfwCreId(userId);
                    jyfwData.setJyfwModiDate(DateUtils.getCurrentDateStrYYMMDD());
                    jyfwData.setJyfwModiTime(DateUtils.getCurrentTimeStrHHMMSS());
                    jyfwData.setJyfwModiId(userId);
                    jyfwData.setJyfwSite(jyfwSite);
                    jyfwData.setJyfwType(jyfwType);
                    return jyfwData;
                }).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(jyfwDOList)) {
            businessScopeServiceMapper.saveJyfwDataList(jyfwDOList);
        }
    }

    /**
     * 特殊经营范围首营表保存
     *
     * @param client     加盟商
     * @param jyfwOrgid  机构
     * @param jyfwFlowNo 首营流程编号
     * @param jyfwList   范围列表
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveJyfwGspinfoData(String client, String jyfwOrgid, String jyfwFlowNo, List<JyfwDTO> jyfwList) {
        List<GaiaJyfwDataGspinfoKey> jyfwGspInfoDOList = jyfwList.stream().filter(Objects::nonNull)
                // 1.选中
                .filter(item -> CommonEnum.NoYesStatusInteger.YES.getCode().equals(item.getChecked()))
                .map(item -> {
                    GaiaJyfwDataGspinfoKey jyfwDataGspinfo = new GaiaJyfwDataGspinfoKey();
                    jyfwDataGspinfo.setClient(client);
                    jyfwDataGspinfo.setJyfwOrgid(jyfwOrgid);
                    jyfwDataGspinfo.setJyfwId(item.getJyfwId());
                    jyfwDataGspinfo.setJyfwFlowNo(jyfwFlowNo);
                    return jyfwDataGspinfo;
                }).collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(jyfwGspInfoDOList)) {
            businessScopeServiceMapper.saveJyfwDataGspInfoList(jyfwGspInfoDOList);
        }
    }

    @Override
    public List<JyfwooData> getAllScopeList(String jyfwClass) {
        return businessScopeServiceMapper.getJyfwooDataList(jyfwClass);
    }

}
