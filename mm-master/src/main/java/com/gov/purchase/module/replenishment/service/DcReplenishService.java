package com.gov.purchase.module.replenishment.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaDcReplenishConfKey;
import com.gov.purchase.entity.GaiaExportTask;
import com.gov.purchase.entity.GaiaSupplierSalesman;
import com.gov.purchase.module.replenishment.dto.DcReplenishForm;
import com.gov.purchase.module.replenishment.dto.ReplenishConf;
import com.gov.purchase.module.replenishment.dto.dcReplenish.ProReplenishmentParams;
import com.gov.purchase.module.replenishment.dto.dcReplenish.StoProStock;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.05.10
 */
public interface DcReplenishService {

    /**
     * DC补货 新
     *
     * @param dcCode
     * @return
     */
    Result getReplenishmentList(String dcCode) throws IllegalAccessException, IOException, InstantiationException;

    /**
     * 补货参数保存
     *
     * @param replenishConf
     * @return
     */
    void saveReplenishConf(ReplenishConf replenishConf);

    /**
     * 补货参数获取
     *
     * @param gdrcSite
     * @return
     */
    Result getReplenishConf(String gdrcSite);

    /**
     * 获取门店数
     *
     * @param dcCode
     * @return
     */
    Result getStoreCont(String dcCode);

    /**
     * 导出
     *
     * @param dcCode
     * @return
     */
    Result exportReplenishmentNewList(String dcCode);

    /**
     * DC补货系数初始化
     *
     * @param gaiaDcReplenishConfKey
     * @return
     */
    Result initDcReplenishParams(GaiaDcReplenishConfKey gaiaDcReplenishConfKey);

    /**
     * 商品补货数据表
     *
     * @param pageSize
     * @param pageNum
     * @param proPurchaseRate
     * @return
     */
    Result getProReplenishmentList(Integer pageSize, Integer pageNum, String dcCode, String stoCode, String proSelfCode, String proClass, String lastSupp, String recSupp,
                                   String proZdy1, String proZdy2, String proZdy3, String proZdy4, String proZdy5, String proSlaeClass, String proSclass,
                                   Integer proNoPurchase, Integer proPosition, List<ProReplenishmentParams> proReplenishmentParamsList, String salesVolumeStartDate,
                                   String salesVolumeEndDate, String proPurchaseRate, Integer excDea, List<String> storeCodes);

    /**
     * 商品补货数据表导出
     *
     * @return
     */
    void getProReplenishmentListExport(String dcCode, String stoCode, String proSelfCode, String proClass, String lastSupp, String recSupp,
                                         String proZdy1, String proZdy2, String proZdy3, String proZdy4, String proZdy5, String proSlaeClass, String proSclass,
                                         Integer proNoPurchase, Integer proPosition, List<ProReplenishmentParams> proReplenishmentParamsList,
                                         String salesVolumeStartDate, String salesVolumeEndDate, String proPurchaseRate, Integer excDea, List<String> storeCodes,
                                         GaiaExportTask gaiaExportTask);

    /**
     * 仓库紧急补货需求列表
     *
     * @param dcReplenishForm 仓库紧急补货需求
     */
    Result getUrgentReplenishList(DcReplenishForm dcReplenishForm);

    /**
     * 商品自定义标签
     *
     * @return
     */
    Result getProductZdy();

    /**
     * 查询条件
     *
     * @return
     */
    Result getProReplenishmentCondit();

    void updateUrgentReplenish(DcReplenishForm dcReplenishForm);

    /**
     * 采购员下拉框
     */
    List<String> getProPurchaseRateList();

    Map<String, List<GaiaSupplierSalesman>> supplierSalesman(String dcCode);

    /**
     * 新增商品
     *
     * @param proSelfCode
     * @return
     */
    Result addReplenishmentPro(String proSelfCode, String dcCode);

    /**
     * DC补货，门店库存
     *
     * @param dcCode
     * @param proSelfCode
     * @return
     */
    List<StoProStock> getStoreStock(String dcCode, String proSelfCode);

    /**
     * 生成导出任务
     *
     * @return
     */
    GaiaExportTask initExportTask();

    /**
     * 商品自定义字段列表
     *
     * @return
     */
    Result getProZdyList();
}
