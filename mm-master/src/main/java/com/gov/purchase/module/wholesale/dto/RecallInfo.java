package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.07
 */
@Data
public class RecallInfo {

    /**
     * 商品编号
     */
    private String batProCode;

    /**
     * 地点名称
     */
    private String dcName;

    /**
     * 地点编号
     */
    private String dcCode;
    /**
     * 商品自编码
     */
    private String proSelfCode;
    /**
     * 商品名称
     */
    private String proName;
    /**
     * 规格
     */
    private String proSpecs;
    /**
     * 生产企业
     */
    private String proFactoryName;
    /**
     * 产地
     */
    private String proPlace;
    /**
     * 中包装量
     */
    private String proMidPackage;
    /**
     * 单位
     */
    private String proUnit;
    /**
     * 单位名
     */
    private String unitName;
    /**
     * 库存地点
     */
    private String batLocationCode;
    /**
     * 批次
     */
    private String batBatch;
    /**
     * 库存数量
     */
    private BigDecimal batNormalQty;
    /**
     * 库存金额
     */
    private BigDecimal batBatchCost;
    /**
     * 生产批号
     */
    private String batBatchNo;
    /**
     * 生产日期
     */
    private String batProductDate;
    /**
     * 有效期
     */
    private String batExpiryDate;
    /**
     * 采购供应商编码
     */
    private String batSupplierCode;
    /**
     * 采购供应商名
     */
    private String batSupplierName;
    /**
     * 采购价格
     */
    private BigDecimal batPoPrice;
    /**
     * 采购订单
     */
    private String batPoId;
    /**
     * 采购订单行
     */
    private String batPoLineno;
    /**
     * 采购税率编码
     */
    private String poRate;
    /**
     * 采购税率值
     */
    private String taxCodeValue;

    private String proCommonname;
    /**
     * 商品单价
     */
    private BigDecimal soPrice;

    /**
     * 建议补货量
     */
    private BigDecimal gsrdProposeQty;

    /**
     * 货位号
     */
    private String gsrdHwh;

    /**
     * 行号
     */
    private String gsrdSerial;

    /**
     * 补货单号
     */
    private String gsrdVoucherId;

    /**
     * 是否提醒
     */
    private Integer wholesaleDate;

    /**
     * 近效期天数
     */
    private String gcspPara1;

    /**
     * 上次开票价
     */
    private BigDecimal wmCkj;

    /**
     * 上次开票数量
     */
    private BigDecimal wmGzsl;

    /**
     * 上次开票时间
     */
    private String wmCjrq;

    /**
     * 成本价
     */
    private BigDecimal costPrice;

    /**
     * 零售价
     */
    private BigDecimal proLsj;

    /**
     * 会员价
     */
    private BigDecimal proHyj;

    /**
     * 国批价
     */
    private BigDecimal proGpj;

    /**
     * 国零价
     */
    private BigDecimal proGlj;

    /**
     * 预设售价1
     */
    private BigDecimal proYsj1;

    /**
     * 预设售价2
     */
    private BigDecimal proYsj2;

    /**
     * 预设售价3
     */
    private BigDecimal proYsj3;

    /**
     * 单价字体色
     */
    private String priceColor;

    /**
     * 自定义字段1
     */
    private String proZdy1;
    /**
     * 自定义字段2
     */
    private String proZdy2;
    /**
     * 自定义字段3
     */
    private String proZdy3;
    /**
     * 自定义字段4
     */
    private String proZdy4;
    /**
     * 自定义字段5
     */
    private String proZdy5;

    /**
     * 批准文号
     */
    private String proRegisterNo;

    /**
     * 商品地点
     */
    private String proSite;

    /**
     * 合计数量
     */
    private BigDecimal totalQty;

    /**
     * 可编辑商品下标
     */
    private Integer index;

    /**
     * 是否可编辑
     */
    private String isEdit;

}
