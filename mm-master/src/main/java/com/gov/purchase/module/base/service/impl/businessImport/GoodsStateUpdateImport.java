package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.module.base.dto.businessImport.GoodsStateUpdate;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import com.gov.purchase.module.qa.dto.QueryProductListRequestDto;
import com.gov.purchase.module.qa.dto.QueryProductListResponseDto;
import com.gov.purchase.module.qa.service.QualityService;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.26
 */
@Service
public class GoodsStateUpdateImport extends BusinessImport {

    @Resource
    private QualityService qualityService;

    /**
     * 业务验证(商品状态更新页面)
     *
     * @param map   数据
     * @param field 字段
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        List<String> errorList = new ArrayList<>();
        // 批量导入
        try {
            QueryProductListRequestDto queryProductListRequestDto = new QueryProductListRequestDto();

            for (String key : field.keySet()) {

                if (key.equals("client")) {
                    //参数空值判断
                    if (StringUtils.isBlank((String) field.get(key))) {
                        errorList.add(MessageFormat.format(ResultEnum.CHECK_PARAMETER.getMsg(), ""));
                    }
                    queryProductListRequestDto.setClient((String) field.get(key));
                }
                if (key.equals("fieldName")) {
                    //参数空值判断
                    if (StringUtils.isBlank((String) field.get(key))) {
                        errorList.add(MessageFormat.format(ResultEnum.CHECK_PARAMETER.getMsg(), ""));
                    }
                    queryProductListRequestDto.setFieldName((String) field.get(key));
                }
                if (key.equals("siteList")) {
                    //参数空值判断
                    if ("[]".equals(field.get(key).toString())) {
                        errorList.add(MessageFormat.format(ResultEnum.CHECK_PARAMETER.getMsg(), ""));
                    }
                    queryProductListRequestDto.setSiteList((List<String>) field.get(key));
                }
            }

            List<QueryProductListResponseDto> queryProductList = qualityService.queryProductList(queryProductListRequestDto);
            List<QueryProductListResponseDto> returnList = new ArrayList<>();
            for (Integer key : map.keySet()) {
                // 行数据
                GoodsStateUpdate goodsStateUpdate = (GoodsStateUpdate) map.get(key);
                int index = 0;
                for (QueryProductListResponseDto queryProduct : queryProductList) {

                    QueryProductListResponseDto returnValue = new QueryProductListResponseDto();

                    if (goodsStateUpdate.getProCode().equals(queryProduct.getProSelfCode()) && goodsStateUpdate.getFieldNameValue().equals(queryProductListRequestDto.getFieldName())) {
                        index++;
                        //更新字段
                        if ("商品状态".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.GoodsStauts.GOODSNORMAL.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
                            } else if (CommonEnum.GoodsStauts.GOODSDEACTIVATE.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode());
                            }

                            if (CommonEnum.GoodsStauts.GOODSNORMAL.getCode().equals(queryProduct.getProStatus())) {
                                returnValue.setFieldOldValue(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
                            } else if (CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode().equals(queryProduct.getProStatus())) {
                                returnValue.setFieldOldValue(CommonEnum.GoodsStauts.GOODSDEACTIVATE.getCode());
                            }

                        } else if ("商品定位".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());

                            returnValue.setFieldOldValue(queryProduct.getProPosition());

                        } else if ("禁止销售".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoRetail())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoRetail())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("禁止采购".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoPurchase())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoPurchase())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("禁止配送".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoDistributed())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoDistributed())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("禁止退厂".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoSupplier())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoSupplier())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("禁止退仓".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoDc())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoDc())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("禁止调剂".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoAdjust())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoAdjust())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("禁止批发".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoSale())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoSale())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("禁止请货".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProNoApply())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProNoApply())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("是否拆零".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProIfpart())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProIfpart())) {
                                returnValue.setFieldOldValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("拆零单位".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProPartUint());
                        } else if ("拆零比例".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProPartRate());
                        } else if ("采购单位".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProPurchaseUnit());
                        } else if ("采购单位比例".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProPurchaseRate());
                        } else if ("销售单位".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProSaleUnit());
                        } else if ("销售单位比例".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProSaleRate());
                        } else if ("最小订货量".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProMinQty().toString());
                        } else if ("是否医保".equals(goodsStateUpdate.getFieldName())) {
                            if (CommonEnum.NoYesStatus.NO.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getName().equals(goodsStateUpdate.getFieldNewValue())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                            if (CommonEnum.NoYesStatus.NO.getCode().equals(queryProduct.getProIfMed())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.NO.getCode());
                            } else if (CommonEnum.NoYesStatus.YES.getCode().equals(queryProduct.getProIfMed())) {
                                returnValue.setFieldNewValue(CommonEnum.NoYesStatus.YES.getCode());
                            }

                        } else if ("销售级别".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProSlaeClass());
                        } else if ("限购数量".equals(goodsStateUpdate.getFieldName())) {
                            returnValue.setFieldNewValue(goodsStateUpdate.getFieldNewValue());
                            returnValue.setFieldOldValue(queryProduct.getProLimitQty().toString());
                        }

                        returnValue.setProSelfCode(queryProduct.getProSelfCode());
                        returnValue.setProDepict(queryProduct.getProDepict());
                        returnList.add(returnValue);
                    }
                }
                if (index == 0) {
                    errorList.add(MessageFormat.format(ResultEnum.E0028.getMsg(), key + 1));
                }
            }

            // 验证报错
            if (CollectionUtils.isNotEmpty(errorList)) {
                Result result = ResultUtil.error(ResultEnum.E0115);
                result.setData(errorList);
                return result;
            }

            return ResultUtil.success(returnList);
        } catch (Exception e) {
            throw new CustomResultException(ResultEnum.E0126);
        }
    }
}
