package com.gov.purchase.module.base.service.impl.businessImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.businessImport.PaymentMaintenance;
import com.gov.purchase.module.base.service.impl.BusinessImport;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.12.01
 */
@Service
public class PaymentMaintenanceImport extends BusinessImport {

    @Resource
    private GaiaFiUserPaymentMaintenanceMapper gaiaFiUserPaymentMaintenanceMapper;
    @Resource
    private GaiaFranchiseeMapper gaiaFranchiseeMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaPayingItemMapper gaiaPayingItemMapper;

    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> map, Map<String, Object> field) {
        // 错误 消息
        List<String> errorList = new ArrayList<>();
        // 行数据处理 验证
        for (Integer key : map.keySet()) {
            PaymentMaintenance paymentMaintenance = (PaymentMaintenance) map.get(key);
            String ficoPayingstoreName;
            // 加盟商验证
            GaiaFranchisee gaiaFranchisee = gaiaFranchiseeMapper.selectByPrimaryKey(paymentMaintenance.getClient());
            if (gaiaFranchisee == null) {
                errorList.add(MessageFormat.format("第{0}行：用户编码不存在。", key + 1));
                continue;
            } else {
                // 门店
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                gaiaStoreDataKey.setClient(paymentMaintenance.getClient());
                gaiaStoreDataKey.setStoCode(paymentMaintenance.getFicoPayingstoreCode());
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                // 配送中心
                GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
                gaiaDcDataKey.setClient(paymentMaintenance.getClient());
                gaiaDcDataKey.setDcCode(paymentMaintenance.getFicoPayingstoreCode());
                GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
                if (gaiaStoreData == null && gaiaDcData == null) {
                    errorList.add(MessageFormat.format("第{0}行：付款主体编码不存在。", key + 1));
                    continue;
                } else {
                    ficoPayingstoreName = gaiaStoreData != null ? gaiaStoreData.getStoName() : gaiaDcData.getDcName();
                }
            }
            // 开始生效日期判断
            GaiaPayingItem gaiaPayingItem = gaiaPayingItemMapper.selectPayingItem(paymentMaintenance.getClient(), paymentMaintenance.getFicoPayingstoreCode());
            if (gaiaPayingItem != null) {
                // 生效日期小于等于上期截止日期
                if (paymentMaintenance.getFicoEffectiveDate().compareTo(gaiaPayingItem.getFicoLastDeadline()) <= 0) {
                    errorList.add(MessageFormat.format("第{0}行：生效日期必须大于{}。", key + 1, gaiaPayingItem.getFicoLastDeadline()));
                    continue;
                }
            }
            GaiaFiUserPaymentMaintenanceKey gaiaFiUserPaymentMaintenanceKey = new GaiaFiUserPaymentMaintenanceKey();
            // 加盟商
            gaiaFiUserPaymentMaintenanceKey.setClient(paymentMaintenance.getClient());
            // 付款主体编码
            gaiaFiUserPaymentMaintenanceKey.setFicoPayingstoreCode(paymentMaintenance.getFicoPayingstoreCode());
            GaiaFiUserPaymentMaintenance gaiaFiUserPaymentMaintenance = gaiaFiUserPaymentMaintenanceMapper.selectByPrimaryKey(gaiaFiUserPaymentMaintenanceKey);
            if (gaiaFiUserPaymentMaintenance == null) {
                gaiaFiUserPaymentMaintenance = new GaiaFiUserPaymentMaintenance();
                // 加盟商
                gaiaFiUserPaymentMaintenance.setClient(paymentMaintenance.getClient());
                // 付款主体编码
                gaiaFiUserPaymentMaintenance.setFicoPayingstoreCode(paymentMaintenance.getFicoPayingstoreCode());
                // 付款主体名称
                gaiaFiUserPaymentMaintenance.setFicoPayingstoreName(ficoPayingstoreName);
                // 生效日期
                gaiaFiUserPaymentMaintenance.setFicoEffectiveDate(paymentMaintenance.getFicoEffectiveDate());
                // 收费标准
                gaiaFiUserPaymentMaintenance.setFicoCurrentChargingStandard(NumberUtils.toScaledBigDecimal(paymentMaintenance.getFicoCurrentChargingStandard()));
            } else {
                // 生效日期
                gaiaFiUserPaymentMaintenance.setFicoEffectiveDate(paymentMaintenance.getFicoEffectiveDate());
                // 收费标准
                gaiaFiUserPaymentMaintenance.setFicoCurrentChargingStandard(NumberUtils.toScaledBigDecimal(paymentMaintenance.getFicoCurrentChargingStandard()));
                gaiaFiUserPaymentMaintenanceMapper.updateByPrimaryKey(gaiaFiUserPaymentMaintenance);
            }
        }
        // 验证报错
        if (CollectionUtils.isNotEmpty(errorList)) {
            Result result = ResultUtil.error(ResultEnum.E0115);
            result.setData(errorList);
            return result;
        }
        return ResultUtil.success();
    }
}
