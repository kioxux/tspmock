package com.gov.purchase.module.store.dto.price;

import com.gov.purchase.entity.GaiaSdProductPrice;
import lombok.Data;


@Data
public class GaiaSdProductPriceVo extends GaiaSdProductPrice {
    /**
     * 销售级别
     */
    private String proSlaeClass;

    /**
     * 自定义字段1
     */
    private String proZdy1;

    /**
     * 自定义字段2
     */
    private String proZdy2;

    /**
     * 自定义字段3
     */
    private String proZdy3;

    /**
     * 自定义字段4
     */
    private String proZdy4;

    /**
     * 自定义字段5
     */
    private String proZdy5;
}