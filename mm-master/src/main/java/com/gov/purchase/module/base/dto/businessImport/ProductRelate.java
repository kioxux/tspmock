package com.gov.purchase.module.base.dto.businessImport;

import com.gov.purchase.common.validate.ExcelValidate;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.11.24
 */
@Data
public class ProductRelate {

    /**
     * 商品编码
     */
    @ExcelValidate(index = 0, name = "商品编号", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proSelfCode;

    /**
     * 关联商品编码
     */
    @ExcelValidate(index = 1, name = "对应第三方商品编码", type = ExcelValidate.DataType.STRING, maxLength = 50)
    private String proRelateCode;

    /**
     * 采购价格
     */
    @ExcelValidate(index = 2, name = "采购单价", type = ExcelValidate.DataType.DECIMAL, min = 0, max = 999999999999.99)
    private String proPrice;
}

