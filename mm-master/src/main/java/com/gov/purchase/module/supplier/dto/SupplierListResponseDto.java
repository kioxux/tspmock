package com.gov.purchase.module.supplier.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "供应商列表返回参数")
public class SupplierListResponseDto {

    @ApiModelProperty(value = "供应商编码", name = "supCode")
    private String supCode;

    @ApiModelProperty(value = "供应商自编码", name = "supSelfCode")
    private String supSelfCode;

    @ApiModelProperty(value = "供应商名", name = "supName")
    private String supName;

    @ApiModelProperty(value = "统一社会信用代码", name = "supCreditCode")
    private String supCreditCode;

    @ApiModelProperty(value = "状态", name = "status")
    private String status;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "地点", name = "supSite")
    private String supSite;

    @ApiModelProperty(value = "地点名称", name = "supSiteName")
    private String supSiteName;
}
