package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 退货单明细
 * @author: yzf
 * @create: 2021-12-28 10:17
 */
@Data
public class ReturnOrderDetailVO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点编码
     */
    private String proSite;

    /**
     * 退货单号
     */
    private String orderId;

    private String lineNo;

    private String proOutputTax;

    /**
     * 退货类型
     */
    private String orderType;

    /**
     * 退货日期
     */
    private String orderDate;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 商品名称
     */
    private String proDepict;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 产家
     */
    private String proFactoryName;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 生产批号
     */
    private String batBatchNo;
    private String batBatch;

    /**
     * 最近参考价
     */
    private BigDecimal poPrice;

    /**
     * 数量
     */
    private BigDecimal soQty;

    /**
     * 退货单价
     */
    private BigDecimal soPrice;

    /**
     * 有效期
     */
    private String batExpiryDate;

    /**
     * 生产日期
     */
    private String batProductDate;
}
