package com.gov.purchase.module.store.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.store.dto.GetStoreProductPriceListDTO;
import com.gov.purchase.module.store.dto.price.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface PriceService {

    /**
     * 价格组列表
     *
     * @return
     */
    List<PriceGroupListReponseDto> queryPriceGroupList(PriceGroupListRequestDto dto);

    /**
     * 价格范围选择
     *
     * @return
     */

    List<PriceStoreListReponseDto> queryPriceStoreList(PriceStoreListRequestDto dto);


    /**
     * 根据商品名称筛选
     *
     * @return
     */
    ProductBasicListReponseDto queryProductBasicList(ProductBasicListRequestDto dto);

    /**
     * 根据商品名称筛选
     *
     * @return
     */
    Result saveRetailPriceInfo(RetailPriceInfoSaveRequestDto dto);

    /**
     * 调价单审批列表
     *
     * @return
     */
    PageInfo<PriceApprovalListReponseDto> queryPriceApprovalList(PriceApprovalListRequestDto dto);


    /**
     * 调价单查看列表
     *
     * @return
     */
    PageInfo<PriceListReponseDto> queryPriceList(PriceListRequestDto dto);

    /**
     * 调价单验证
     *
     * @param dto
     * @return
     */
    Result validRetailPriceInfo(RetailPriceInfoSaveRequestDto dto);

    /**
     * 调价单查看导出
     *
     * @param dto
     * @return
     */
    void excelPriceList(PriceListRequestDto dto) throws IOException;

    /**
     * 调价单审批
     *
     * @param list
     */
    void approvePriceList(List<PriceApprovalListReponseDto> list, Map<String, Object> map);

    /**
     * 调价商品选择
     *
     * @param params
     * @param labStore
     * @return
     */
    Result getProductListBySto(String params, Integer inStock, String labStore, String prcClass);

    /**
     * 门店调价提交
     *
     * @param dto
     * @return
     */
    Result saveStoreRetailPriceInfo(ProductPriceDto dto);

    /**
     * 门店调价查询
     *
     * @param pageNum
     * @param pageSize
     * @param prcSource
     * @param prcEffectDate
     * @param proKey
     * @param prcApprovalSuatus
     * @return
     */
    Result selectStoreRetailPriceList(Integer pageNum, Integer pageSize, String prcSource, String prcEffectDate, String proKey, String prcApprovalSuatus, String praAdjustNo);

    /**
     * 门店调价导出
     *
     * @param prcSource
     * @param prcEffectDate
     * @param proKey
     * @param prcApprovalSuatus
     * @return
     */
    Result exportStoreRetailPriceList(String prcSource, String prcEffectDate, String proKey, String prcApprovalSuatus, String praAdjustNo);

    /**
     * 调价单审批拒绝
     *
     * @param prcModfiyNoList
     * @return
     */
    void refusePriceList(List<PriceApprovalListReponseDto> list);

    /**
     * 商品门店价格
     *
     * @param proSelfCode
     * @return
     */
    Result getProPriceList(String proSelfCode);

    /**
     * 调价门店
     *
     * @param prcModfiyNo
     * @param proSelfCode
     * @return
     */
    Result queryStoreList(String prcModfiyNo, String proSelfCode);

    /**
     * 调价单明细
     */
    List<GetStoreProductPriceListDTO> getStoreProductPriceList(String client, String proSelfCode, String prcClass, String prcModfiyNo);

    /**
     * 当前用户商品组以及权限下的门店列表
     */
    List<PriceStoreListReponseDto> getCurrentUserAuthStoreList(String prcGroupId);

    /**
     * 价格组门店保存
     *
     * @param dto
     * @return
     */
    Result savePriceGroupAndStore(PriceGroupProductDTO dto);

    /**
     * 价格组商品明细保存
     *
     * @param dto
     * @return
     */
    Result savePriceGroupPro(PriceGroupProductDTO dto);

    /**
     * 价格组及其门店列表
     *
     * @return
     */
    Result queryPriceGroupAndStoreList();

    /**
     * 价格组商品明细
     *
     * @param groupId
     * @return
     */
    List<PriceGroupProductVO> queryPriceGroupProList(String groupId);

    /**
     * 删除价格组
     *
     * @param groupId
     */
    void delPriceGroup(String groupId);
}
