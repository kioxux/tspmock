package com.gov.purchase.module.purchase.dto;

import com.gov.purchase.entity.GaiaContractM;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 合同功能
 * @author: yzf
 * @create: 2022-02-07 13:19
 */
@Data
public class PurchaseContractDetailVO extends GaiaContractM {

    /**
     * 商品描述
     */
    private String proDepict;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 进项税率
     */
    private String proInputTax;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 计量单位
     */
    private String proUnit;

    /**
     * 生产企业
     */
    private String proFactoryName;

}
