package com.gov.purchase.module.qa.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.qa.dto.*;
import com.gov.purchase.module.qa.service.ScopeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@Api(tags = "经营范围")
@RestController
@RequestMapping("scope")
public class ScopeController {

    @Resource
    private ScopeService scopeService;

    @Log("经营范围列表")
    @ApiOperation("经营范围列表")
    @PostMapping("queryBusinessScopeList")
    public Result queryBusinessScopeList(@RequestBody QueryScopeListRequestDto dto) {
        return ResultUtil.success(scopeService.queryBusinessScopeList(dto));
    }

    @Log("经营范围验证")
    @ApiOperation("经营范围验证")
    @PostMapping("validBusinessScopeInfo")
    public Result validBusinessScopeInfo(@Valid @RequestBody List<ValidBusinessScopeInfoRequestDto> list) {
        return scopeService.validBusinessScopeInfo(list);
    }


    @Log("经营范围保存")
    @ApiOperation("经营范围保存")
    @PostMapping("saveBusinessScopeInfo")
    public Result saveBusinessScopeInfo(@Valid @RequestBody List<ValidBusinessScopeInfoRequestDto> list) {
        return scopeService.saveBusinessScopeInfo(list);
    }

    @Log("经营范围详情")
    @ApiOperation("经营范围详情")
    @PostMapping("getBusinessScopeInfo")
    public Result getBusinessScopeInfo(@RequestBody QueryBusinessScopeInfoRequestDto dto) {
        return ResultUtil.success(scopeService.getBusinessScopeInfo(dto));
    }


}
