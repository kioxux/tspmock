package com.gov.purchase.module.goods.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.goods.dto.DrugQualityDTO;
import com.gov.purchase.module.goods.service.DrugQualityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 药品质量档案表
 * @author: yzf
 * @create: 2021-11-08 17:02
 */

@Api(tags = "药品质量档案表")
@RestController
@RequestMapping("drugQuality")
public class DrugQualityController {

    @Resource
    private DrugQualityService drugQualityService;

    @Log("药品质量档案")
    @ApiOperation("药品质量档案")
    @PostMapping("queryDrugQuality")
    public Result queryDrugQuality(@RequestBody DrugQualityDTO drugQualityDTO){
        return ResultUtil.success(drugQualityService.queryDrugQuality(drugQualityDTO));
    }
}
