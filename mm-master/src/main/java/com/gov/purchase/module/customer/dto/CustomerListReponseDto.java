package com.gov.purchase.module.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户维护列表页面返回结果")
public class CustomerListReponseDto {

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;

    @ApiModelProperty(value = "客户编号", name = "cusCode")
    private  String cusCode;

    @ApiModelProperty(value = "客户自编号", name = "cusSelfCode")
    private  String cusSelfCode;

    @ApiModelProperty(value = "客户姓名", name = "cusName")
    private String cusName;

    @ApiModelProperty(value = "地点", name = "cusSite")
    private  String cusSite;

    @ApiModelProperty(value = "统一社会信用代码", name = "cusCreditCode")
    private  String cusCreditCode;

    @ApiModelProperty(value = "客户状态 “正常”、“停用“", name = "cusStatus")
    private  String cusStatus;


}
