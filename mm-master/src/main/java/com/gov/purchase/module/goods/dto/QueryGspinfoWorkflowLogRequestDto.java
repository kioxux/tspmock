package com.gov.purchase.module.goods.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "首营审批日志传入参数")
public class QueryGspinfoWorkflowLogRequestDto {

    @ApiModelProperty(value = "工作流编号", name = "flowNo", required = true)
    @NotBlank(message = "工作流编号不能为空")
    private String flowNo;

    @ApiModelProperty(value = "加盟商", name = "client")
    private String client;
}
