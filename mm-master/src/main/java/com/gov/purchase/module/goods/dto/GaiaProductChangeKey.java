package com.gov.purchase.module.goods.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class GaiaProductChangeKey {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 地点
     */
    private String proSite;

    /**
     * 商品自编码
     */
    private String proSelfCode;

}
