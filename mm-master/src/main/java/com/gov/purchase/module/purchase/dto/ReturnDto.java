package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "采购退货明细参数")
public class ReturnDto {

    /**
     * 订单行号
     */
    @ApiModelProperty(value = "订单行号", name = "poLineNo", required = true)
    @NotBlank(message = "订单行号不能为空")
    private String poLineNo;

    /**
     * 商品自编码
     */
    @ApiModelProperty(value = "商品自编码", name = "poProCode", required = true)
    @NotBlank(message = "商品自编码不能为空")
    private String poProCode;

    /**
     * 本次退货数据
     */
    @ApiModelProperty(value = "本次退货数据", name = "poQty", required = true)
    @NotBlank(message = "本次退货数据不能为空")
    private String poQty;

    /**
     * 订单单位
     */
    @ApiModelProperty(value = "订单单位", name = "poUnit", required = true)
    @NotBlank(message = "订单单位不能为空")
    private String poUnit;

    /**
     * 地点
     */
    @ApiModelProperty(value = "地点", name = "poSiteCode", required = true)
    @NotBlank(message = "地点不能为空")
    private String poSiteCode;

    /**
     * 库存地点
     */
    @ApiModelProperty(value = "库存地点", name = "poLocationCode", required = true)
    @NotBlank(message = "库存地点不能为空")
    private String poLocationCode;

    /**
     * 批次
     */
    @ApiModelProperty(value = "批次", name = "poBatch", required = true)
    @NotBlank(message = "批次不能为空")
    private String poBatch;

    /**
     * 税率
     */
    @ApiModelProperty(value = "税率", name = "poRate", required = true)
    @NotBlank(message = "税率不能为空")
    private String poRate;

}
