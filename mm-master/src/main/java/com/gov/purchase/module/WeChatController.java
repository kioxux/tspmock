package com.gov.purchase.module;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.gov.purchase.common.entity.WeChat;
import com.gov.purchase.common.utils.WechatUtils;
import com.gov.purchase.entity.GaiaOfficialAccounts;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.GaiaOfficialAccountsMapper;
import com.gov.purchase.module.wechat.WechatArticles;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Validated
@RestController
@RequestMapping("weChat")
public class WeChatController {

    @Resource
    private WeChat weChat;

    @Resource
    private GaiaOfficialAccountsMapper gaiaOfficialAccountsMapper;

    @Resource
    private CosUtils cosUtils;

    @Resource
    private WechatUtils wechatUtils;

    @GetMapping(produces = "text/plain;charset=utf-8")
    public void authGet(@RequestParam(name = "signature", required = false) String signature,
                        @RequestParam(name = "timestamp", required = false) String timestamp,
                        @RequestParam(name = "nonce", required = false) String nonce,
                        @RequestParam(name = "echostr", required = false) String echostr, HttpServletResponse response) {
        PrintWriter print;
        try {
            print = response.getWriter();
            print.write(echostr);
            print.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @PostMapping
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //接受微信服务器发送过来的XML形式的消息
        InputStream in = request.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
        String sReqData = "";
        String itemStr = "";//作为输出字符串的临时串，用于判断是否读取完毕
        while ((itemStr = reader.readLine()) != null) {
            sReqData += itemStr;
        }
        in.close();
        reader.close();
        log.info("公众号接收到消息:{}", sReqData);
        //防止中文乱码
        response.setCharacterEncoding("UTF-8");
        JSONArray j = printNote(sReqData);
        JSONObject message = j.getJSONObject(0).getJSONArray("xml").getJSONObject(0);
        // 事件
        String event = message.getString("Event");
        // 关注
        if ("subscribe".equals(event)) {
            // 发送消息
            String toUserName = message.getString("ToUserName");
            String eventKey = message.getString("EventKey");
            String fromUserName = message.getString("FromUserName");
            Long createTime = System.currentTimeMillis();
            String returnXml = "";
            List<GaiaOfficialAccounts> accounts = gaiaOfficialAccountsMapper.selectListByOriginalId(toUserName);

            if (CollectionUtils.isNotEmpty(accounts) && StringUtils.isEqual(accounts.get(0).getWechatMembershipCard(), "1")) {
                log.info("是否开启微信卡包：开启");
                GaiaOfficialAccounts accounts1 = accounts.get(0);
                // 获取开卡地址
                String accessToken = wechatUtils.getAccessToken(accounts1.getAppId(), accounts1.getAppSecret());
                if (StringUtils.isBlank(accessToken)) {
                    log.error("该客户未配置正确的APPID以及密钥，appId: {},appSecret:{}", accounts1.getAppId(), accounts1.getAppSecret());
                    throw new CustomResultException("该客户未配置正确的APPID以及密钥");
                }

                String outer_str = "";
                if (StringUtils.isNotBlank(eventKey) && eventKey.startsWith("qrscene_")) {
                    String[] returnInfo = eventKey.substring(8).split(",");
                    outer_str = returnInfo[2];
                    log.info("开卡门店, 门店:{}", outer_str);
                }

                String activeUrl = wechatUtils.getActiveUrl(accessToken, accounts1.getCardId(), outer_str);
                if (StringUtils.isBlank(activeUrl)) {
                    log.error("该客户未配置正确的会员卡ID，cardId: {},outer_str:{}", accounts1.getCardId(), outer_str);
                    throw new CustomResultException("该客户未配置正确的会员卡ID");
                }

                // 感谢关注
                String returnMessage = "感谢关注" + accounts1.getStoName() + "！";
                String textXml = "<xml>" +
                        "  <ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>" +
                        "  <FromUserName><![CDATA[" + toUserName + "]]></FromUserName>" +
                        "  <CreateTime>" + createTime + "</CreateTime>" +
                        "  <MsgType><![CDATA[text]]></MsgType>" +
                        "  <Content><![CDATA[" + returnMessage + "]]></Content>" +
                        "</xml>";
               sendMsgToWx(response, textXml);

                String logoUrl = cosUtils.urlAuth(accounts1.getLogo());
//                returnXml = "<xml>" +
//                        "  <ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>" +
//                        "  <FromUserName><![CDATA[" + toUserName + "]]></FromUserName>" +
//                        "  <CreateTime>" + createTime + "</CreateTime>" +
//                        "  <MsgType><![CDATA[news]]></MsgType>" +
//                        "  <ArticleCount>1</ArticleCount>" +
//                        "  <Articles>" +
//                        "    <item>" +
//                        "      <Title><![CDATA[" + "领取会员卡" + "]]></Title>" +
//                        "      <Description><![CDATA[" + "完善资料成为，立享会员特权！" + "]]></Description>" +
//                        "      <PicUrl><![CDATA[" + logoUrl + "]]></PicUrl>" +
//                        "      <Url><![CDATA[" + activeUrl + "]]></Url>" +
//                        "    </item>" +
//                        "  </Articles>" +
//                        "</xml>";

                Map<String, Object> map = new HashMap<>();
                map.put("touser", fromUserName);
                map.put("msgtype", "news");
                Map<String, Object> map2 = new HashMap<>();
                List<WechatArticles> articles = new ArrayList<>();
                articles.add(new WechatArticles().setTitle("领取会员卡")
                        .setDescription("完善资料成为，立享会员特权！")
                        .setPicurl(logoUrl).setUrl(activeUrl));
                map2.put("articles",articles );
                map.put("news", map2);
                wechatUtils.customTextImageSend(accessToken, map);

            } else {
                log.info("是否开启微信卡包：未开启");
                if (StringUtils.isNotBlank(eventKey) && eventKey.startsWith("qrscene_")) {
                    String[] returnInfo = eventKey.substring(8).split(",");
                    String returnMessage = MessageFormat.format(weChat.getPushUrl(), returnInfo[0], returnInfo[1], returnInfo[2]);
                    returnXml = "<xml>" +
                            "<ToUserName><![CDATA[" + fromUserName + "]]></ToUserName>" +
                            "<FromUserName><![CDATA[" + toUserName + "]]></FromUserName>" +
                            "<CreateTime>" + createTime + "</CreateTime>" +
                            "<MsgType><![CDATA[text]]></MsgType>" +
                            "<Content><![CDATA[" + weChat.getPushContent() + returnMessage + "]]></Content>" +
                            "</xml>";
                }
                sendMsgToWx(response, returnXml);
            }

        }
    }

    private static void sendMsgToWx(HttpServletResponse response, String xml) {
        log.info("回复消息:{}", xml);
        PrintWriter print;
        try {
            print = response.getWriter();
            print.write(xml);
            print.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static JSONArray printNote(String xml) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document document = db.parse(new InputSource(new StringReader(xml)));
            NodeList sessions = document.getChildNodes();
            return printNote_1(sessions);
        } catch (Exception e) {

        }
        return null;
    }

    private static JSONArray printNote_1(NodeList nodeList) {
        JSONArray dataArr = new JSONArray();
        JSONObject dataObject = new JSONObject();
        for (int count = 0; count < nodeList.getLength(); count++) {
            Node tempNode = nodeList.item(count);
            if (tempNode.getNodeType() == Node.ELEMENT_NODE) {
                if (tempNode.hasChildNodes() && tempNode.getChildNodes().getLength() > 1) {
                    JSONArray temArr = printNote_1(tempNode.getChildNodes());
                    if (dataObject.containsKey(tempNode.getNodeName())) {
                        dataObject.getJSONArray(tempNode.getNodeName()).add(temArr.getJSONObject(0));
                    } else {
                        dataObject.put(tempNode.getNodeName(), temArr);
                    }
                } else {
                    dataObject.put(tempNode.getNodeName(), tempNode.getTextContent());
                }
            }
        }
        dataArr.add(dataObject);
        return dataArr;
    }

 /*   public static void main(String arg[]){
        String xml = "<xml>" +
                "<ToUserName><![CDATA[toUser]]></ToUserName>" +
                "<FromUserName><![CDATA[FromUser]]></FromUserName>" +
                "<CreateTime>123456789</CreateTime>" +
                "<MsgType><![CDATA[event]]></MsgType>" +
                "<Event><![CDATA[subscribe]]></Event>" +
                "<EventKey><![CDATA[qrscene_1,2,3]]></EventKey>" +
                "<Ticket><![CDATA[TICKET]]></Ticket>" +
                "</xml>";
        System.out.println(xml);
        JSONArray j = printNote(xml);
        JSONObject message = j.getJSONObject(0).getJSONArray("xml").getJSONObject(0);
        String ToUserName = message.getString("ToUserName");
        String FromUserName = message.getString("FromUserName");
        String CreateTime = message.getString("CreateTime");
        String EventKey = message.getString("EventKey");
        String[] returnInfo = EventKey.substring(8).split(",");
        String returnMessage = "参数1:"+returnInfo[0]+"参数2:"+returnInfo[1]+"参数3:"+returnInfo[2]
                + "<a href=\"https://www.baidu.com\">测试链接</a>";
        String returnXml = "<xml>"+
                "<ToUserName><![CDATA["+ToUserName+"]]></ToUserName>"+
                "<FromUserName><![CDATA["+FromUserName+"]]></FromUserName>"+
                "<CreateTime>"+CreateTime+"</CreateTime>"+
                "<MsgType><![CDATA[text]]></MsgType>"+
                "<Content><![CDATA["+returnMessage+"]]></Content>"+
                "</xml>";
        System.out.println(returnXml);
    }*/
}
