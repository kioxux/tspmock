package com.gov.purchase.module.qa.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@ApiModel(value = "证照资质验证传入参数")
public class ValidPermitDataInfoRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client" ,required = true)
    @NotBlank(message = "所属加盟商不能为空")
    private String client;

    @ApiModelProperty(value = "实体类型", name = "perEntityClass" ,required = true)
    @NotBlank(message = "实体类型不能为空")
    private String perEntityClass;

    @ApiModelProperty(value = "实体编码", name = "perEntityId" ,required = true)
    @NotBlank(message = "实体编码不能为空")
    private String perEntityId;

    @ApiModelProperty(value = "证照编码(表GAIA_LICENCE_CLASS)", name = "perCode" ,required = true)
    private String perCode;

    @ApiModelProperty(value = "证照编码", name = "perCodeNew" ,required = true)
    @NotBlank(message = "证照编码不能为空")
    private String perCodeNew;

    @ApiModelProperty(value = "证照号", name = "perId" ,required = true)
    @NotBlank(message = "证照号不能为空")
    private String perId;

    @ApiModelProperty(value = "有效期从", name = "perDateStart" ,required = true)
    @NotBlank(message = "有效期从不能为空")
    private String perDateStart;

    @ApiModelProperty(value = "有效期至", name = "perDateEnd" ,required = true)
    @NotBlank(message = "有效期至不能为空")
    private String perDateEnd;

    @ApiModelProperty(value = "登记日期", name = "perRecordDate" )
    private String perRecordDate;

    @ApiModelProperty(value = "停用标志（0-否，1-是）", name = "perStopFlag")
    private String perStopFlag;

    @ApiModelProperty(value = "参数类型（0-删除，1-新增, 2-更新）", name = "type" ,required = true)
    @NotBlank(message = "提交类型不能为空")
    private String type;

}
