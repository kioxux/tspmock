package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

/**
 * @description: 仓库审批配置
 * @author: yzf
 * @create: 2021-11-10 17:45
 */
@Data
public class GaiaWmsShenPiPeiZhiDTO {

    /**
     * 审批人1
     */
    private String wmSpr1;

    /**
     * 审批人姓名1
     */
    private String wmSprxm1;

    /**
     * 审批人类型1, 0-角色, 1-人员
     */
    private String wmSprlx1;
}
