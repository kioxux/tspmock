package com.gov.purchase.module.delivery.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.UUIDUtil;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.delivery.dto.*;
import com.gov.purchase.module.delivery.service.AllotPriceService;
import com.gov.purchase.module.purchase.dto.GetProductListRequestDto;
import com.gov.purchase.module.purchase.dto.GetProductListResponseDto;
import com.gov.purchase.module.purchase.dto.customerDto;
import com.gov.purchase.utils.CosUtils;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.ExcelUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class AllotPriceServiceImpl implements AllotPriceService {

    @Resource
    private GaiaAllotPriceMapper gaiaAllotPriceMapper;
    @Resource
    private GaiaAllotPriceCusMapper gaiaAllotPriceCusMapper;
    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaAllotPriceGroupMapper gaiaAllotPriceGroupMapper;
    @Resource
    GaiaAllotProductPriceMapper gaiaAllotProductPriceMapper;
    @Resource
    GaiaAllotGroupPriceMapper gaiaAllotGroupPriceMapper;
    @Resource
    CosUtils cosUtils;


    /**
     * 门店集合
     *
     * @return
     */
    @Override
    public Result getAlpReceiveSite() {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaStoreData> list = gaiaStoreDataMapper.getStoreDataListByClient(user.getClient());
        return ResultUtil.success(list);
    }

    /**
     * 调拨价格查询
     *
     * @param alpReceiveSite
     * @param alpProCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<GaiaAllotPriceDto> selectAllotPriceList(String alpReceiveSite, String alpProCode, Integer pageNum, Integer pageSize) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaAllotPriceDto> list = gaiaAllotPriceMapper.selectAllotPriceList(user.getClient(), alpReceiveSite, alpProCode);
        PageInfo<GaiaAllotPriceDto> pageInfo = new PageInfo<>(list);
        if (CollectionUtils.isNotEmpty(pageInfo.getList())) {
            pageInfo.getList().forEach(item -> {
                if (StringUtils.isNotBlank(item.getAlpProCode())) {
//                    GetProductListRequestDto getProductListRequestDto = new GetProductListRequestDto();
//                    getProductListRequestDto.setClient(user.getClient());
//                    getProductListRequestDto.setProSite(new ArrayList<String>() {{
//                        add(item.getAlpReceiveSite());
//                    }});
//                    getProductListRequestDto.setProSelfCode2(item.getAlpProCode());
                    //商品列表查询
//                    List<GetProductListResponseDto> supplierList = gaiaProductBasicMapper.selectProductList(getProductListRequestDto);
//                    item.setGoodsList(supplierList);
                    if (item.getCostPrice() != null) {
                        item.setCostPrice(item.getCostPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
                    }
                }
            });
        }
        return pageInfo;
    }

    /**
     * 商品列表
     *
     * @param alpReceiveSite
     * @param alpProCode
     * @return
     */
    @Override
    public List<GaiaProductBusiness> getProductList(String alpReceiveSite, String alpProCode) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaProductBusiness> list = gaiaAllotPriceMapper.getProductList(user.getClient(), alpReceiveSite, alpProCode, null);
        return list;
    }

    /**
     * 调拨价格提交
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveAllotPrice(List<GaiaAllotPrice> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        list.forEach(item->{
            AtomicInteger count = new AtomicInteger();
            if (!ObjectUtils.isEmpty(item.getAlpAddAmt())) {
                count.getAndIncrement();
            }
            if (!ObjectUtils.isEmpty(item.getAlpAddRate())) {
                count.getAndIncrement();
            }
            if (!ObjectUtils.isEmpty(item.getAlpCataloguePrice())) {
                if (StringUtils.isBlank(item.getAlpProCode())) {
                    throw new CustomResultException("选填目录价时，商品编码不能为空");
                }
                count.getAndIncrement();
            }
            if (count.get() != 1) {
                throw new CustomResultException("[加点比例]、[加点金额]、[目录价]必填一个,且只能填一个");
            }
        });
        TokenUser user = feignService.getLoginInfo();
        list.forEach(item -> {
            // 修改
            if (item.getId() != null) {
                gaiaAllotPriceMapper.deleteByPrimaryKey(item.getId());
            }
            GaiaAllotPrice gaiaAllotPrice = null;
            if (StringUtils.isBlank(item.getAlpProCode())) {
                gaiaAllotPrice = gaiaAllotPriceMapper.selectAllotPriceBySite(user.getClient(), item.getAlpReceiveSite());
            } else {
                gaiaAllotPrice = gaiaAllotPriceMapper.selectAllotPriceByPro(user.getClient(), item.getAlpReceiveSite(), item.getAlpProCode());
            }
            if (gaiaAllotPrice == null) {
                gaiaAllotPrice = new GaiaAllotPrice();
                // 加盟商
                gaiaAllotPrice.setClient(user.getClient());
                // 收货地点
                gaiaAllotPrice.setAlpReceiveSite(item.getAlpReceiveSite());
                // 商品编码
                gaiaAllotPrice.setAlpProCode(item.getAlpProCode());
                // 加价金额
                gaiaAllotPrice.setAlpAddAmt(item.getAlpAddAmt());
                // 加价比例
                gaiaAllotPrice.setAlpAddRate(item.getAlpAddRate());
                // 目录价
                gaiaAllotPrice.setAlpCataloguePrice(item.getAlpCataloguePrice());
                // 创建人
                gaiaAllotPrice.setAlpCreateBy(user.getUserId());
                // 创建日期
                gaiaAllotPrice.setAlpCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 创建时间
                gaiaAllotPrice.setAlpCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceMapper.insertSelective(gaiaAllotPrice);
            } else {
                // 加价金额
                gaiaAllotPrice.setAlpAddAmt(item.getAlpAddAmt());
                // 加价比例
                gaiaAllotPrice.setAlpAddRate(item.getAlpAddRate());
                // 目录价
                gaiaAllotPrice.setAlpCataloguePrice(item.getAlpCataloguePrice());
                // 更新人
                gaiaAllotPrice.setAlpUpdateBy(user.getUserId());
                // 更新日期
                gaiaAllotPrice.setAlpUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 更新时间
                gaiaAllotPrice.setAlpUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceMapper.updateByPrimaryKey(gaiaAllotPrice);
            }
        });
    }

    /**
     * 调拨价格删除
     *
     * @param list
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteAllotPrice(List<GaiaAllotPrice> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        list.forEach(item -> {
            // 修改
            if (item.getId() != null) {
                gaiaAllotPriceMapper.deleteByPrimaryKey(item.getId());
            }
        });
    }

    /**
     * 调拨价格提交通过弹出框
     */
    @Override
    public void saveAllotPriceByPopup(GaiaAllotPrice dto) {
        TokenUser user = feignService.getLoginInfo();

        AtomicInteger count = new AtomicInteger();
        if (!ObjectUtils.isEmpty(dto.getAlpAddAmt())) {
            count.getAndIncrement();
        }
        if (!ObjectUtils.isEmpty(dto.getAlpAddRate())) {
            count.getAndIncrement();
        }
        if (!ObjectUtils.isEmpty(dto.getAlpCataloguePrice())) {
            if (StringUtils.isBlank(dto.getAlpProCode())) {
                throw new CustomResultException("选填目录价时，商品编码不能为空");
            }
            count.getAndIncrement();
        }
        if (count.get() != 1) {
            throw new CustomResultException("[加点比例]、[加点金额]、[目录价]必填一个,且只能填一个");
        }
        // 收货地点为ALL 全部
        if (dto.getAlpReceiveSite().equals("ALL")) {
            // 所有门店
            List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getStoreDataListByClient(user.getClient());
            List<GaiaAllotPrice> collect = storeDataList.stream().map(store -> {
                GaiaAllotPrice price = new GaiaAllotPrice();
                BeanUtils.copyProperties(dto, price);
                price.setAlpReceiveSite(store.getStoCode());
                return price;
            }).collect(Collectors.toList());
            this.saveAllotPrice(collect);
        } else {
            this.saveAllotPrice(Arrays.asList(dto));
        }
    }

    /**
     * 导出调拨价格列表
     *
     * @param alpReceiveSite
     * @param alpProCode
     * @return
     */
    @Override
    public Result exportData(String alpReceiveSite, String alpProCode) {
        TokenUser user = feignService.getLoginInfo();
        try {
            PageHelper.clearPage();
            //调拨价格列表
            List<GaiaAllotPriceDto> list = this.selectAllotPriceList(alpReceiveSite, alpProCode, 0, 0).getList();
            List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getStoreDataListByClient(user.getClient());
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(list) && i < list.size(); i++) {
                // 明细数据
                GaiaAllotPriceDto allotPriceDto = list.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                // 收货地点
                item.add(allotPriceDto.getAlpReceiveSite() + (allotPriceDto.getSiteName() == null ? "" : "-" + allotPriceDto.getSiteName()));
                // 商品编码
                item.add(allotPriceDto.getAlpProCode() == null ? "" : allotPriceDto.getAlpProCode());
                //商品描述
                item.add(allotPriceDto.getProDepict() == null ? "" : allotPriceDto.getProDepict());
                // 规格
                item.add(allotPriceDto.getProSpecs() == null ? "" : allotPriceDto.getProSpecs());
                // 厂家
                item.add(allotPriceDto.getProFactName() == null ? "" : allotPriceDto.getProFactName());
                // 单位
                item.add(allotPriceDto.getProUnit() == null ? "" : allotPriceDto.getProUnit());
                // 零售价
                item.add(allotPriceDto.getPriceNormal() == null ? "" : allotPriceDto.getPriceNormal().toString());
                // 成本价
                item.add(allotPriceDto.getCostPrice() == null ? "" : allotPriceDto.getCostPrice().toString());
                // 加点比例
                item.add(allotPriceDto.getAlpAddRate() == null ? "" : allotPriceDto.getAlpAddRate().stripTrailingZeros().toString());
                // 加点金额
                item.add(allotPriceDto.getAlpAddAmt() == null ? "" : allotPriceDto.getAlpAddAmt().stripTrailingZeros().toString());
                // 目录价
                item.add(allotPriceDto.getAlpCataloguePrice() == null ? "" : allotPriceDto.getAlpCataloguePrice().stripTrailingZeros().toString());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("调拨价格列表");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Objects.requireNonNull(workbook).write(bos);
            return cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 业务数据表头
     */
    private final String[] headList = {
            "收货地点",
            "商品编码",
            "商品描述",
            "规格",
            "厂家",
            "单位",
            "零售价",
            "仓库成本价",
            "加点比例",
            "加点金额",
            "目录价",
    };

    /**
     * 客户调拨价格列表
     *
     * @param alpReceiveSite
     * @param alpCusCode
     * @param alpProCode
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<GaiaAllotPriceCusDto> queryAllotPriceCusList(String alpReceiveSite, String alpCusCode, String alpProCode, Integer pageNum, Integer pageSize) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaAllotPriceCusDto> list = gaiaAllotPriceCusMapper.selectAllotPriceCusList(user.getClient(), alpReceiveSite, alpCusCode, alpProCode);
        PageInfo<GaiaAllotPriceCusDto> pageInfo = new PageInfo<>(list);
        Map<String, List<customerDto>> map = new HashMap<>();
        if (CollectionUtils.isNotEmpty(pageInfo.getList())) {
            pageInfo.getList().forEach(item -> {
                if (StringUtils.isNotBlank(item.getAlpProCode())) {
//                    GetProductListRequestDto getProductListRequestDto = new GetProductListRequestDto();
//                    getProductListRequestDto.setClient(user.getClient());
//                    getProductListRequestDto.setProSite(new ArrayList<String>() {{
//                        add(item.getAlpReceiveSite());
//                    }});
//                    getProductListRequestDto.setProSelfCode2(item.getAlpProCode());
//                    //商品列表查询
//                    List<GetProductListResponseDto> supplierList = gaiaProductBasicMapper.selectProductList(getProductListRequestDto);
//                    item.setGoodsList(supplierList);
                    if (item.getCostPrice() != null) {
                        item.setCostPrice(item.getCostPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
                    }
                }
//                List<customerDto> customerDtos = map.get(item.getAlpReceiveSite());
//                if (customerDtos == null) {
//                    customerDtos = gaiaCustomerBusinessMapper.customerInfo(user.getClient(), item.getAlpReceiveSite());
//                    map.put(item.getAlpReceiveSite(), customerDtos);
//                }
//                item.setCustomerList(customerDtos);
            });
        }
        return pageInfo;
    }

    /**
     * 客户调拨价格提交
     *
     * @param list
     */
    @Override
    public void saveAllotPriceCus(List<GaiaAllotPriceCus> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        list.forEach(allotPriceCus -> {
            if (StringUtils.isBlank(allotPriceCus.getAlpReceiveSite())) {
                throw new CustomResultException("收货地点不能为空");
            }
            if (StringUtils.isBlank(allotPriceCus.getAlpCusCode())) {
                throw new CustomResultException("客户不能为空");
            }
            int count = 0;
            if (!ObjectUtils.isEmpty(allotPriceCus.getAlpAddAmt())) {
                count++;
            }
            if (!ObjectUtils.isEmpty(allotPriceCus.getAlpAddRate())) {
                count++;
            }
            if (!ObjectUtils.isEmpty(allotPriceCus.getAlpCataloguePrice())) {
                if (StringUtils.isBlank(allotPriceCus.getAlpProCode())) {
                    throw new CustomResultException("选填目录价时，商品编码不能为空");
                }
                count++;
            }
            if (count != 1) {
                throw new CustomResultException("[加点比例]、[加点金额]、[目录价]必填一个,且只能填一个");
            }
        });
        list.stream().filter(Objects::nonNull).collect(Collectors.groupingBy(item -> item.getAlpReceiveSite() + item.getAlpCusCode()))
                .forEach((key, proList) -> {
                    long count = proList.stream().filter(Objects::nonNull).filter(item -> StringUtils.isBlank(item.getAlpProCode())).count();
                    if (count > 1) {
                        throw new CustomResultException("商品编码为空的数据只能有一条");
                    }
                });

        TokenUser user = feignService.getLoginInfo();
        list.forEach(item -> {
            // 修改
            if (item.getId() != null) {
                gaiaAllotPriceCusMapper.deleteByPrimaryKey(item.getId());
            }
            GaiaAllotPriceCus gaiaAllotPriceCus = null;
            if (StringUtils.isBlank(item.getAlpProCode())) {
                gaiaAllotPriceCus = gaiaAllotPriceCusMapper.selectAllotPriceCusBySite(user.getClient(), item.getAlpReceiveSite(), item.getAlpCusCode());
            } else {
                gaiaAllotPriceCus = gaiaAllotPriceCusMapper.selectAllotPriceCusByPro(user.getClient(), item.getAlpReceiveSite(), item.getAlpCusCode(), item.getAlpProCode());
            }
            if (gaiaAllotPriceCus == null) {
                gaiaAllotPriceCus = new GaiaAllotPriceCus();
                // 加盟商
                gaiaAllotPriceCus.setClient(user.getClient());
                // 收货地点
                gaiaAllotPriceCus.setAlpReceiveSite(item.getAlpReceiveSite());
                // 客户
                gaiaAllotPriceCus.setAlpCusCode(item.getAlpCusCode());
                // 商品编码
                gaiaAllotPriceCus.setAlpProCode(item.getAlpProCode());
                // 加价金额
                gaiaAllotPriceCus.setAlpAddAmt(item.getAlpAddAmt());
                // 加价比例
                gaiaAllotPriceCus.setAlpAddRate(item.getAlpAddRate());
                // 目录价
                gaiaAllotPriceCus.setAlpCataloguePrice(item.getAlpCataloguePrice());
                // 创建人
                gaiaAllotPriceCus.setAlpCreateBy(user.getUserId());
                // 创建日期
                gaiaAllotPriceCus.setAlpCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 创建时间
                gaiaAllotPriceCus.setAlpCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceCusMapper.insertSelective(gaiaAllotPriceCus);
            } else {
                // 加价金额
                gaiaAllotPriceCus.setAlpAddAmt(item.getAlpAddAmt());
                // 加价比例
                gaiaAllotPriceCus.setAlpAddRate(item.getAlpAddRate());
                // 目录价
                gaiaAllotPriceCus.setAlpCataloguePrice(item.getAlpCataloguePrice());
                // 更新人
                gaiaAllotPriceCus.setAlpUpdateBy(user.getUserId());
                // 更新日期
                gaiaAllotPriceCus.setAlpUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                // 更新时间
                gaiaAllotPriceCus.setAlpUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                gaiaAllotPriceCusMapper.updateByPrimaryKey(gaiaAllotPriceCus);
            }
        });
    }

    /**
     * 客户调拨价格删除
     *
     * @param list
     */
    @Override
    public void deleteAllotPriceCus(List<GaiaAllotPriceCus> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        list.forEach(item -> {
            // 修改
            if (item.getId() != null) {
                gaiaAllotPriceCusMapper.deleteByPrimaryKey(item.getId());
            }
        });
    }

    @Override
    public Result exportAllotPriceCusData(String alpReceiveSite, String alpCusCode, String alpProCode) {
        TokenUser user = feignService.getLoginInfo();
        try {
            PageHelper.clearPage();
            //调拨价格列表
            List<GaiaAllotPriceCusDto> list = this.queryAllotPriceCusList(alpReceiveSite, alpCusCode, alpProCode, 0, 0).getList();
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(list) && i < list.size(); i++) {
                // 明细数据
                GaiaAllotPriceCusDto allotPriceCusDto = list.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                // 仓库
                item.add(allotPriceCusDto.getAlpReceiveSite() + (allotPriceCusDto.getSiteName() == null ? "" : "-" + allotPriceCusDto.getSiteName()));
                // 客户
                item.add(allotPriceCusDto.getAlpCusCode() + (allotPriceCusDto.getAlpCusName() == null ? "" : "-" + allotPriceCusDto.getAlpCusName()));
                // 商品编码
                item.add(allotPriceCusDto.getAlpProCode() == null ? "" : allotPriceCusDto.getAlpProCode());
                //商品描述
                item.add(allotPriceCusDto.getProDepict() == null ? "" : allotPriceCusDto.getProDepict());
                // 规格
                item.add(allotPriceCusDto.getProSpecs() == null ? "" : allotPriceCusDto.getProSpecs());
                // 厂家
                item.add(allotPriceCusDto.getProFactName() == null ? "" : allotPriceCusDto.getProFactName());
                // 单位
                item.add(allotPriceCusDto.getProUnit() == null ? "" : allotPriceCusDto.getProUnit());
                // 零售价
                item.add(allotPriceCusDto.getPriceNormal() == null ? "" : allotPriceCusDto.getPriceNormal().toString());
                // 成本价
                item.add(allotPriceCusDto.getCostPrice() == null ? "" : allotPriceCusDto.getCostPrice().toString());
                // 加点比例
                item.add(allotPriceCusDto.getAlpAddRate() == null ? "" : allotPriceCusDto.getAlpAddRate().stripTrailingZeros().toString());
                // 加点金额
                item.add(allotPriceCusDto.getAlpAddAmt() == null ? "" : allotPriceCusDto.getAlpAddAmt().stripTrailingZeros().toString());
                // 目录价
                item.add(allotPriceCusDto.getAlpCataloguePrice() == null ? "" : allotPriceCusDto.getAlpCataloguePrice().stripTrailingZeros().toString());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headAllotPriceCusList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("客户调拨价格列表");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Objects.requireNonNull(workbook).write(bos);
            return cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 业务数据表头
     */
    private final String[] headAllotPriceCusList = {
            "仓库",
            "客户",
            "商品编码",
            "商品描述",
            "规格",
            "厂家",
            "单位",
            "零售价",
            "仓库成本价",
            "加点比例",
            "加点金额",
            "目录价",
    };

    private final String[] headAllotProductPriceList = {
            "商品编码",
            "商品名",
            "规格",
            "厂家",
            "单位",
            "成本价",
            "零售价",
            "最新进价",
            "目录价",
            "销售级别",
            "商品定位",
            "是否医保",
    };

    /**
     * 客户调拨价格提交通过弹出框
     *
     * @param dto
     */
    @Override
    public void saveAllotPriceByPopupCus(GaiaAllotPriceCus dto) {
        if (StringUtils.isBlank(dto.getAlpReceiveSite())) {
            throw new CustomResultException("收货地点不能为空");
        }
        if (StringUtils.isBlank(dto.getAlpCusCode())) {
            throw new CustomResultException("客户不能为空");
        }
        int count = 0;
        if (!ObjectUtils.isEmpty(dto.getAlpAddAmt())) {
            count++;
        }
        if (!ObjectUtils.isEmpty(dto.getAlpAddRate())) {
            count++;
        }
        if (!ObjectUtils.isEmpty(dto.getAlpCataloguePrice())) {
            if (StringUtils.isBlank(dto.getAlpProCode())) {
                throw new CustomResultException("选填目录价时，商品编码不能为空");
            }
            count++;
        }
        if (count != 1) {
            throw new CustomResultException("[加点比例]、[加点金额]、[目录价]必填一个,且只能填一个");
        }

        TokenUser user = feignService.getLoginInfo();
        // 收货地点为ALL 全部
        if (dto.getAlpReceiveSite().equals("ALL") && dto.getAlpCusCode().equals("ALL")) {
            // 所有门店
            List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getStoreDataListByClient(user.getClient());
            // 所有客户
            List<GaiaCustomerBusiness> customerDataList = gaiaCustomerBusinessMapper.getCustomerDataListByClient(user.getClient());

            List<GaiaAllotPriceCus> gaiaAllotPriceCusList = storeDataList.stream().filter(Objects::nonNull).flatMap(store -> customerDataList.stream()
                    .filter(Objects::nonNull)
                    .filter(customer -> customer.getCusSite().equals(store.getStoCode()))
                    .map(customer -> {
                        GaiaAllotPriceCus price = new GaiaAllotPriceCus();
                        BeanUtils.copyProperties(dto, price);
                        price.setAlpReceiveSite(store.getStoCode());
                        price.setAlpCusCode(customer.getCusSelfCode());
                        return price;
                    })).collect(Collectors.toList());
            this.saveAllotPriceCus(gaiaAllotPriceCusList);

        } else if (!dto.getAlpReceiveSite().equals("ALL") && dto.getAlpCusCode().equals("ALL")) {
            // 所有客户
            List<GaiaCustomerBusiness> customerDataList = gaiaCustomerBusinessMapper.getCustomerDataListByClient(user.getClient());
            List<GaiaAllotPriceCus> gaiaAllotPriceCusList = customerDataList.stream()
                    .filter(Objects::nonNull)
                    .filter(customer -> customer.getCusSite().equals(dto.getAlpReceiveSite()))
                    .map(customer -> {
                        GaiaAllotPriceCus price = new GaiaAllotPriceCus();
                        BeanUtils.copyProperties(dto, price);
                        price.setAlpReceiveSite(dto.getAlpReceiveSite());
                        price.setAlpCusCode(customer.getCusSelfCode());
                        return price;
                    }).collect(Collectors.toList());
            this.saveAllotPriceCus(gaiaAllotPriceCusList);

        } else {
            this.saveAllotPriceCus(Collections.singletonList(dto));
        }
    }

    /**
     * 当前加盟商下的DC列表
     *
     * @return
     */
    @Override
    public List<GaiaDcData> getDCListByCurrentClient() {
        TokenUser user = feignService.getLoginInfo();
        return gaiaAllotPriceMapper.getDCListByCurrentClient(user.getClient());
    }

    @Override
    public List<GaiaAllotPriceGroup> queryAllotPriceGroupList(String alpReceiveSite, String gapgType) {
        TokenUser user = feignService.getLoginInfo();
        return gaiaAllotPriceGroupMapper.queryAllotPriceGroupList(user.getClient(), alpReceiveSite, gapgType);
    }

    @Override
    @Transactional
    public Result saveAllotPriceGroup(SaveAllotPriceGroupDTO dto) {
        if (CollectionUtils.isEmpty(dto.getVoList())) {
            throw new CustomResultException("无记录");
        } else {
            TokenUser user = feignService.getLoginInfo();
            List<GaiaAllotPriceGroup> insertList = new ArrayList<>();
            List<GaiaAllotPriceGroup> updateList = new ArrayList<>();
            List<GaiaAllotPriceGroupKey> kList = new ArrayList<>();
            for (GaiaAllotPriceGroup s : dto.getVoList()) {
                GaiaAllotPriceGroupKey k = new GaiaAllotPriceGroupKey();
                s.setClient(user.getClient());
                s.setGapgType(dto.getGapgType());
                s.setGapgSite(dto.getAlpReceiveSite());
                k.setClient(s.getClient());
                if (StringUtils.isEmpty(s.getGapgCode())) {
                    s.setGapgCode(UUIDUtil.getNumUUID());
                }
                k.setGapgCode(s.getGapgCode());
                k.setGapgSite(s.getGapgSite());
                k.setGapgType(s.getGapgType());
                kList.add(k);
            }
            List<GaiaAllotPriceGroup> oldList = gaiaAllotPriceGroupMapper.selectByPrimaryKeyList(kList);
            for (GaiaAllotPriceGroup s : dto.getVoList()) {
                if (CollectionUtils.isEmpty(oldList)) {
                    insertList.add(s);
                    continue;
                }
                boolean isInsert = true;
                for (GaiaAllotPriceGroup o : oldList) {
                    if (s.getClient().equals(o.getClient())
                            && ((s.getGapgSite() == null && o.getGapgSite() == null) || s.getGapgSite().equals(o.getGapgSite()))
                            && s.getGapgCode().equals(o.getGapgCode())
                            && s.getGapgType().equals(o.getGapgType())) {
                        updateList.add(s);
                        isInsert = false;
                        break;
                    }
                }
                if (isInsert) {
                    insertList.add(s);
                }
            }
            if (!CollectionUtils.isEmpty(insertList)) {
                gaiaAllotPriceGroupMapper.insertBatchList(insertList);
            }
            if (!CollectionUtils.isEmpty(updateList)) {
                gaiaAllotPriceGroupMapper.updateBatch(updateList);
            }
        }
        return ResultUtil.success("保存成功");
    }

    @Override
    public PageInfo<GaiaAllotProductPriceVO> queryAllotProductPriceList(Integer pageSize, Integer pageNum, String alpReceiveSite, String gapgType, String gapgCode) {
        TokenUser user = feignService.getLoginInfo();
        if (pageSize != null && pageNum != null) {
            PageHelper.startPage(pageNum, pageSize);
        }
        List<GaiaAllotProductPriceVO> list = gaiaAllotProductPriceMapper.selectByGapgCode(user.getClient(), gapgCode, gapgType);
        for (GaiaAllotProductPriceVO gaiaAllotProductPriceVO : list) {
            if (gaiaAllotProductPriceVO.getCostPrice() != null) {
                gaiaAllotProductPriceVO.setCostPrice(gaiaAllotProductPriceVO.getCostPrice().setScale(2, BigDecimal.ROUND_HALF_UP));
            }
        }
        PageInfo<GaiaAllotProductPriceVO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    @Transactional
    public Result saveAllotProductPrice(SaveGaiaAllotProductPriceDTO dto) {
        if (CollectionUtils.isEmpty(dto.getVoList())) {
            throw new CustomResultException("无记录");
        }
        List<GaiaAllotProductPriceVO> insertList = new ArrayList<>();
        List<GaiaAllotProductPriceVO> updateList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        Integer index = gaiaAllotProductPriceMapper.selectMaxIndex();
        List<GaiaAllotProductPriceKey> kList = new ArrayList<>();
        for (GaiaAllotProductPriceVO s : dto.getVoList()) {
            s.setClient(user.getClient());
            s.setGapgType(dto.getGapgType());
            s.setGapgCode(dto.getGapgCode());
            s.setGapgSite(dto.getAlpReceiveSite());
            GaiaAllotProductPriceKey k = new GaiaAllotProductPriceKey();
            k.setClient(s.getClient());
            k.setGapgCode(s.getGapgCode());
            k.setGapgSite(s.getGapgSite());
            k.setGapgType(s.getGapgType());
            k.setGapgProSelfCode(s.getGapgProSelfCode());
            kList.add(k);
        }
        List<GaiaAllotProductPrice> oldList = gaiaAllotProductPriceMapper.selectByPrimaryKeyList(kList);
        for (GaiaAllotProductPriceVO s : dto.getVoList()) {
            if (CollectionUtils.isEmpty(oldList)) {
                s.setGapgIndex(++index);
                insertList.add(s);
                continue;
            }
            boolean isInsert = true;
            for (GaiaAllotProductPrice o : oldList) {
                if (s.getClient().equals(o.getClient())
                        && s.getGapgSite().equals(o.getGapgSite())
                        && s.getGapgCode().equals(o.getGapgCode())
                        && s.getGapgType().equals(o.getGapgType())
                        && s.getGapgProSelfCode().equals(o.getGapgProSelfCode())
                ) {
                    updateList.add(s);
                    isInsert = false;
                    break;
                }
            }
            if (isInsert) {
                s.setGapgIndex(++index);
                insertList.add(s);
            }
        }
       /* for (GaiaAllotProductPriceVO gaiaAllotProductPriceVO : dto.getVoList()) {
            GaiaAllotProductPrice gapp = new GaiaAllotProductPrice();
            BeanUtils.copyProperties(gaiaAllotProductPriceVO, gapp);
            if (gaiaAllotProductPriceVO.getGapgIndex() == null) {
                gaiaAllotProductPriceVO.setClient(user.getClient());
                gaiaAllotProductPriceVO.setGapgType(dto.getGapgType());
                gaiaAllotProductPriceVO.setGapgCode(dto.getGapgCode());
                gaiaAllotProductPriceVO.setGapgIndex(++index);
                insertList.add(gaiaAllotProductPriceVO);
            } else {
                updateList.add(gaiaAllotProductPriceVO);
            }
        }*/
        if (!CollectionUtils.isEmpty(insertList)) {
            gaiaAllotProductPriceMapper.batchInsert(insertList);
        }
        if (!CollectionUtils.isEmpty(updateList)) {
            gaiaAllotProductPriceMapper.batchUpdateByPrimaryKey(updateList);
        }
        // 同步GAIA_ALLOT_PRICE或GAIA_ALLOT_PRICE_CUS
        //新增的产品
        List<String> proList = new ArrayList<>();
        for (GaiaAllotProductPriceVO gaiaAllotGroupPrice : dto.getVoList()) {
            proList.add(gaiaAllotGroupPrice.getGapgProSelfCode());
        }
        List<GaiaAllotProductPriceVO> groupList = gaiaAllotGroupPriceMapper.selectAllotPriceListByPro(user.getClient(),
                proList, dto.getGapgType(), dto.getAlpReceiveSite());
        if (dto.getGapgType() == 1) {
            List<GaiaAllotPrice> mainList = gaiaAllotPriceMapper.selectAllotPriceListByPro(user.getClient(),
                    proList, dto.getGapgType());
            List<GaiaAllotPrice> gapInsert = new ArrayList<>();
            List<GaiaAllotPrice> gapUpdate = new ArrayList<>();
            for (GaiaAllotProductPriceVO s : groupList) {
                if (CollectionUtils.isEmpty(mainList)) {
                    GaiaAllotPrice gap = new GaiaAllotPrice();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                    continue;
                }
                boolean isInsert = true;
                for (GaiaAllotPrice o : mainList) {
                    if (s.getClient().equals(o.getClient())
                            && s.getGapgProSelfCode().equals(o.getAlpProCode())
                            && s.getGapgCus().equals(o.getAlpReceiveSite())
                    ) {
                        o.setAlpAddAmt(s.getGapgAddAmt());
                        o.setAlpAddRate(s.getGapgAddRate());
                        o.setAlpCataloguePrice(s.getGapgCataloguePrice());
                        gapUpdate.add(o);
                        isInsert = false;
                        break;
                    }
                }
                if (isInsert) {
                    GaiaAllotPrice gap = new GaiaAllotPrice();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                }
            }
            if (CollectionUtils.isNotEmpty(gapInsert)) {
                gaiaAllotPriceMapper.batchInsert(gapInsert);
            }
            if (CollectionUtils.isNotEmpty(gapUpdate)) {
                gaiaAllotPriceMapper.batchUpdate(gapUpdate);
            }
        } else {
            List<GaiaAllotPriceCusDto> mainList = gaiaAllotPriceCusMapper.selectAllotPriceCusListByPro(user.getClient(),
                    dto.getAlpReceiveSite(), proList, dto.getGapgType());
            List<GaiaAllotPriceCus> gapInsert = new ArrayList<>();
            List<GaiaAllotPriceCus> gapUpdate = new ArrayList<>();
            for (GaiaAllotProductPriceVO s : groupList) {
                if (CollectionUtils.isEmpty(mainList)) {
                    GaiaAllotPriceCus gap = new GaiaAllotPriceCus();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgSite());
                    gap.setAlpCusCode(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                    continue;
                }
                boolean isInsert = true;
                for (GaiaAllotPriceCus o : mainList) {
                    if (s.getClient().equals(o.getClient())
                            && s.getGapgProSelfCode().equals(o.getAlpProCode())
                            && s.getGapgCus().equals(o.getAlpCusCode())
                            && s.getGapgSite().equals(o.getAlpReceiveSite())
                    ) {
                        o.setAlpAddAmt(s.getGapgAddAmt());
                        o.setAlpAddRate(s.getGapgAddRate());
                        o.setAlpCataloguePrice(s.getGapgCataloguePrice());
                        gapUpdate.add(o);
                        isInsert = false;
                        break;
                    }
                }
                if (isInsert) {
                    GaiaAllotPriceCusDto gap = new GaiaAllotPriceCusDto();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgSite());
                    gap.setAlpCusCode(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                }
            }
            if (CollectionUtils.isNotEmpty(gapInsert)) {
                gaiaAllotPriceCusMapper.batchInsert(gapInsert);
            }
            if (CollectionUtils.isNotEmpty(gapUpdate)) {
                gaiaAllotPriceCusMapper.batchUpdate(gapUpdate);
            }
        }
        /*if (dto.getGapgType() == 1) {
            List<GaiaAllotPrice> gapInsertList = new ArrayList<>();
            for (GaiaAllotProductPrice gaiaAllotProductPrice : insertList) {
                GaiaAllotPrice gap = new GaiaAllotPrice();
                BeanUtils.copyProperties(gaiaAllotProductPrice, gap);
                gap.setAlpAddRate(gaiaAllotProductPrice.getGapgAddRate());
                gap.setAlpCataloguePrice(gaiaAllotProductPrice.getGapgCataloguePrice());
                gap.setAlpAddAmt(gaiaAllotProductPrice.getGapgAddAmt());
                gap.setAlpReceiveSite(gaiaAllotProductPrice.getGapgSite());
                gap.setAlpProCode(gaiaAllotProductPrice.getGapgProSelfCode());
                gapInsertList.add(gap);
            }
            if (!CollectionUtils.isEmpty(gapInsertList)) {
                gaiaAllotPriceMapper.batchInsert(gapInsertList);
            }
            List<GaiaAllotPrice> gapUpdateList = new ArrayList<>();
            for (GaiaAllotProductPrice gaiaAllotProductPrice : updateList) {
                GaiaAllotPrice gap = new GaiaAllotPrice();
                BeanUtils.copyProperties(gaiaAllotProductPrice, gap);
                gap.setAlpAddRate(gaiaAllotProductPrice.getGapgAddRate());
                gap.setAlpCataloguePrice(gaiaAllotProductPrice.getGapgCataloguePrice());
                gap.setAlpAddAmt(gaiaAllotProductPrice.getGapgAddAmt());
                gap.setAlpReceiveSite(gaiaAllotProductPrice.getGapgSite());
                gapUpdateList.add(gap);
            }
            if (!CollectionUtils.isEmpty(gapUpdateList)) {
                gaiaAllotPriceMapper.batchUpdate(gapUpdateList);
            }
        } else {
            List<GaiaAllotPriceCus> gapInsertList = new ArrayList<>();
            for (GaiaAllotProductPriceVO gaiaAllotProductPrice : insertList) {
                GaiaAllotPriceCus gap = new GaiaAllotPriceCus();
                BeanUtils.copyProperties(gaiaAllotProductPrice, gap);
                gap.setAlpAddRate(gaiaAllotProductPrice.getGapgAddRate());
                gap.setAlpCataloguePrice(gaiaAllotProductPrice.getGapgCataloguePrice());
                gap.setAlpAddAmt(gaiaAllotProductPrice.getGapgAddAmt());
                gap.setAlpCusCode(gaiaAllotProductPrice.getProSite());
                gap.setAlpReceiveSite(gaiaAllotProductPrice.getGapgSite());
                gap.setAlpProCode(gaiaAllotProductPrice.getGapgProSelfCode());
                gapInsertList.add(gap);
            }
            if (!CollectionUtils.isEmpty(gapInsertList)) {
                gaiaAllotPriceCusMapper.batchInsert(gapInsertList);
            }
            List<GaiaAllotPriceCus> gapUpdateList = new ArrayList<>();
            for (GaiaAllotProductPrice gaiaAllotProductPrice : updateList) {
                GaiaAllotPriceCus gap = new GaiaAllotPriceCus();
                BeanUtils.copyProperties(gaiaAllotProductPrice, gap);
                gap.setAlpAddRate(gaiaAllotProductPrice.getGapgAddRate());
                gap.setAlpCataloguePrice(gaiaAllotProductPrice.getGapgCataloguePrice());
                gap.setAlpAddAmt(gaiaAllotProductPrice.getGapgAddAmt());
                gap.setAlpReceiveSite(gaiaAllotProductPrice.getGapgSite());
                gapUpdateList.add(gap);
            }
            if (!CollectionUtils.isEmpty(gapUpdateList)) {
                gaiaAllotPriceCusMapper.batchUpdate(gapUpdateList);
            }
        }*/
        return ResultUtil.success("保存成功");
    }

    @Override
    @Transactional
    public Result deleteAllotPriceGroup(SaveAllotPriceGroupDTO dto) {
        if (CollectionUtils.isEmpty(dto.getVoList())) {
            throw new CustomResultException("无记录选中");
        }

        List<GaiaAllotProductPrice> priceList = new ArrayList<>();
        for (GaiaAllotPriceGroup gaiaAllotPriceGroup : dto.getVoList()) {
            GaiaAllotProductPrice gap = new GaiaAllotProductPrice();
            BeanUtils.copyProperties(gaiaAllotPriceGroup, gap);
            priceList.add(gap);
        }
        gaiaAllotProductPriceMapper.batchDelete(priceList);

        List<GaiaAllotGroupPrice> gpList = new ArrayList<>();
        for (GaiaAllotPriceGroup gaiaAllotPriceGroup : dto.getVoList()) {
            GaiaAllotGroupPrice gap = new GaiaAllotGroupPrice();
            gap.setGagpCode(gaiaAllotPriceGroup.getGapgCode());
            gap.setGagpSite(gaiaAllotPriceGroup.getGapgSite());
            gap.setGagpType(gaiaAllotPriceGroup.getGapgType());
            BeanUtils.copyProperties(gaiaAllotPriceGroup, gap);
            gpList.add(gap);
        }
        gaiaAllotGroupPriceMapper.batchDelete(gpList);
        gaiaAllotPriceGroupMapper.batchDelete(dto.getVoList());
        return ResultUtil.success("删除成功");
    }

    @Override
    public PageInfo<GaiaAllotGroupPriceVO> queryAllotGroupPriceList(Integer pageSize, Integer pageNum, String alpReceiveSite, String gapgCode, String gapgType) {
        TokenUser user = feignService.getLoginInfo();
        PageHelper.startPage(pageNum, pageSize);
        List<GaiaAllotGroupPriceVO> list = gaiaAllotGroupPriceMapper.queryAllotGroupPriceList(user.getClient(), alpReceiveSite, gapgCode, gapgType);
        PageInfo<GaiaAllotGroupPriceVO> pageInfo = new PageInfo<>(list);
        return pageInfo;
    }

    @Override
    @Transactional
    public Result deleteAllotProductPrice(SaveGaiaAllotProductPriceDTO dto) {
        if (CollectionUtils.isEmpty(dto.getVoList())) {
            throw new CustomResultException("无记录选中");
        }
        List<GaiaAllotProductPrice> list = new ArrayList<>();
        for (GaiaAllotProductPriceVO gaiaAllotProductPriceVO : dto.getVoList()) {
            GaiaAllotProductPrice p = new GaiaAllotProductPrice();
            BeanUtils.copyProperties(gaiaAllotProductPriceVO, p);
            list.add(p);
        }
        gaiaAllotProductPriceMapper.batchDelete(list);
        return ResultUtil.success();
    }

    @Override
    public List<StoreVO> queryStoreList(String alpReceiveSite, String gapgType) {
        TokenUser user = feignService.getLoginInfo();
        if ("1".equals(gapgType)) {
            return gaiaAllotProductPriceMapper.selectStoreVO(user.getClient());
        } else {
            return gaiaAllotProductPriceMapper.selectStoreVOBySite(user.getClient(), alpReceiveSite);
        }
    }

    @Override
    public Result saveAllotGroupPrice(SaveAllotGroupPriceVO dto) {
        List<GaiaAllotGroupPrice> insertList = new ArrayList<>();
        List<GaiaAllotGroupPrice> updateList = new ArrayList<>();
        TokenUser user = feignService.getLoginInfo();
        Integer index = gaiaAllotGroupPriceMapper.selectMaxIndex();
        List<GaiaAllotGroupPriceKey> kList = new ArrayList<>();
        for (GaiaAllotGroupPriceVO s : dto.getVoList()) {
            s.setClient(user.getClient());
            s.setGagpType(dto.getGapgType());
            s.setGagpSite(dto.getAlpReceiveSite());
            s.setGagpCode(dto.getGapgCode());
            if (s.getGagpIndex() == null) {
                s.setGagpIndex(++index);
            }
            GaiaAllotGroupPriceKey k = new GaiaAllotGroupPriceKey();
            k.setClient(s.getClient());
            k.setGagpCode(s.getGagpCode());
            k.setGagpSite(s.getGagpSite());
            k.setGagpType(s.getGagpType());
            k.setGagpCus(s.getGagpCus());
            kList.add(k);
        }
        List<GaiaAllotGroupPrice> oldList = gaiaAllotGroupPriceMapper.selectByPrimaryKeyList(kList);
        for (GaiaAllotGroupPriceVO s : dto.getVoList()) {
            if (CollectionUtils.isEmpty(oldList)) {
                insertList.add(s);
                continue;
            }
            boolean isInsert = true;
            for (GaiaAllotGroupPrice o : oldList) {
                if (s.getClient().equals(o.getClient())
                        && ((s.getGagpSite() == null && o.getGagpSite() == null) || s.getGagpSite().equals(o.getGagpSite()))
                        && s.getGagpCode().equals(o.getGagpCode())
                        && s.getGagpType().equals(o.getGagpType())
                        && s.getGagpCus().equals(o.getGagpCus())
                ) {
                    updateList.add(s);
                    isInsert = false;
                    break;
                }
            }
            if (isInsert) {
                insertList.add(s);
            }
        }
        if (!CollectionUtils.isEmpty(insertList)) {
            gaiaAllotGroupPriceMapper.batchInsert(insertList);
        }
        if (!CollectionUtils.isEmpty(updateList)) {
            gaiaAllotGroupPriceMapper.batchUpdate(updateList);
        }
        //新增或修改GAIA_ALLOT_PRICE或GAIA_ALLOT_PRICE_CUS
        List<String> cusList = new ArrayList<>();
        for (GaiaAllotGroupPriceVO gaiaAllotGroupPrice : dto.getVoList()) {
            cusList.add(gaiaAllotGroupPrice.getGagpCus());
        }
        List<GaiaAllotProductPriceVO> groupList = gaiaAllotGroupPriceMapper.selectAllotPriceListForGroup(user.getClient(),
                cusList, dto.getGapgType(), dto.getAlpReceiveSite());
        if (dto.getGapgType() == 1) {
            List<GaiaAllotPrice> mainList = gaiaAllotPriceMapper.selectAllotPriceListForGroup(user.getClient(),
                    cusList, dto.getGapgType());
            List<GaiaAllotPrice> gapInsert = new ArrayList<>();
            List<GaiaAllotPrice> gapUpdate = new ArrayList<>();
            for (GaiaAllotProductPriceVO s : groupList) {
                if (CollectionUtils.isEmpty(mainList)) {
                    GaiaAllotPrice gap = new GaiaAllotPrice();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                    continue;
                }
                boolean isInsert = true;
                for (GaiaAllotPrice o : mainList) {
                    if (s.getClient().equals(o.getClient())
                            && s.getGapgProSelfCode().equals(o.getAlpProCode())
                            && s.getGapgCus().equals(o.getAlpReceiveSite())
                    ) {
                        o.setAlpAddAmt(s.getGapgAddAmt());
                        o.setAlpAddRate(s.getGapgAddRate());
                        o.setAlpCataloguePrice(s.getGapgCataloguePrice());
                        gapUpdate.add(o);
                        isInsert = false;
                        break;
                    }
                }
                if (isInsert) {
                    GaiaAllotPrice gap = new GaiaAllotPrice();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                }
            }
            if (CollectionUtils.isNotEmpty(gapInsert)) {
                gaiaAllotPriceMapper.batchInsert(gapInsert);
            }
            if (CollectionUtils.isNotEmpty(gapUpdate)) {
                gaiaAllotPriceMapper.batchUpdate(gapUpdate);
            }
        } else {
            List<GaiaAllotPriceCusDto> mainList = gaiaAllotPriceCusMapper.selectAllotPriceCusListForGroup(user.getClient(),
                    dto.getAlpReceiveSite(), cusList, dto.getGapgType());
            List<GaiaAllotPriceCus> gapInsert = new ArrayList<>();
            List<GaiaAllotPriceCus> gapUpdate = new ArrayList<>();
            for (GaiaAllotProductPriceVO s : groupList) {
                if (CollectionUtils.isEmpty(mainList)) {
                    GaiaAllotPriceCus gap = new GaiaAllotPriceCus();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgSite());
                    gap.setAlpCusCode(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                    continue;
                }
                boolean isInsert = true;
                for (GaiaAllotPriceCus o : mainList) {
                    if (s.getClient().equals(o.getClient())
                            && s.getGapgProSelfCode().equals(o.getAlpProCode())
                            && s.getGapgCus().equals(o.getAlpCusCode())
                            && s.getGapgSite().equals(o.getAlpReceiveSite())
                    ) {
                        o.setAlpAddAmt(s.getGapgAddAmt());
                        o.setAlpAddRate(s.getGapgAddRate());
                        o.setAlpCataloguePrice(s.getGapgCataloguePrice());
                        gapUpdate.add(o);
                        isInsert = false;
                        break;
                    }
                }
                if (isInsert) {
                    GaiaAllotPriceCusDto gap = new GaiaAllotPriceCusDto();
                    gap.setClient(s.getClient());
                    gap.setAlpReceiveSite(s.getGapgSite());
                    gap.setAlpCusCode(s.getGapgCus());
                    gap.setAlpProCode(s.getGapgProSelfCode());
                    gap.setAlpAddAmt(s.getGapgAddAmt());
                    gap.setAlpAddRate(s.getGapgAddRate());
                    gap.setAlpCataloguePrice(s.getGapgCataloguePrice());
                    gapInsert.add(gap);
                }
            }
            if (CollectionUtils.isNotEmpty(gapInsert)) {
                gaiaAllotPriceCusMapper.batchInsert(gapInsert);
            }
            if (CollectionUtils.isNotEmpty(gapUpdate)) {
                gaiaAllotPriceCusMapper.batchUpdate(gapUpdate);
            }
        }
        return ResultUtil.success();
    }

    @Override
    public Result deleteAllotGroupPrice(SaveAllotGroupPriceVO dto) {
        if (CollectionUtils.isEmpty(dto.getVoList())) {
            throw new CustomResultException("无记录选中");
        }
        List<GaiaAllotGroupPrice> list = new ArrayList<>();
        for (GaiaAllotGroupPriceVO gaiaAllotProductPriceVO : dto.getVoList()) {
            GaiaAllotGroupPrice p = new GaiaAllotGroupPrice();
            BeanUtils.copyProperties(gaiaAllotProductPriceVO, p);
            list.add(p);
        }
        gaiaAllotGroupPriceMapper.batchDelete(list);
        return ResultUtil.success();
    }

    @Override
    public Result exportAllotProductPriceList(Integer pageSize, Integer pageNum, String alpReceiveSite, String gapgType, String gapgCode) {
        try {
            PageHelper.clearPage();
            //目录商品价格列表
            List<GaiaAllotProductPriceVO> list = this.queryAllotProductPriceList(null, null, alpReceiveSite, gapgType, gapgCode).getList();
            List<List<String>> dataList = new ArrayList<>();
            for (int i = 0; CollectionUtils.isNotEmpty(list) && i < list.size(); i++) {
                // 明细数据
                GaiaAllotProductPriceVO allotPriceCusDto = list.get(i);
                // 打印行
                List<String> item = new ArrayList<>();
                // 商品编码
                item.add(allotPriceCusDto.getGapgProSelfCode() == null ? "" : allotPriceCusDto.getGapgProSelfCode());
                // 商品名
                item.add(allotPriceCusDto.getProName() == null ? "" : allotPriceCusDto.getProName());
                // 规格
                item.add(allotPriceCusDto.getProSpecs() == null ? "" : allotPriceCusDto.getProSpecs());
                // 厂家
                item.add(allotPriceCusDto.getProFactName() == null ? "" : allotPriceCusDto.getProFactName());
                // 单位
                item.add(allotPriceCusDto.getProUnit() == null ? "" : allotPriceCusDto.getProUnit());
                // 成本价
                item.add(allotPriceCusDto.getCostPrice() == null ? "" : allotPriceCusDto.getCostPrice().toString());
                // 零售价
                item.add(allotPriceCusDto.getPriceNormal() == null ? "" : allotPriceCusDto.getPriceNormal().toString());
                // 最新进价
                item.add(allotPriceCusDto.getLatestInputPrice() == null ? "" : allotPriceCusDto.getLatestInputPrice().toString());
                // 目录价
                item.add(allotPriceCusDto.getGapgCataloguePrice() == null ? "" : allotPriceCusDto.getGapgCataloguePrice().toString());
                // 销售级别
                item.add(allotPriceCusDto.getProSalesClass() == null ? "" : allotPriceCusDto.getProSalesClass());
                // 商品定位
                item.add(allotPriceCusDto.getProPosition() == null ? "" : allotPriceCusDto.getProPosition());
                // 是否医保
                item.add(allotPriceCusDto.getProIfMed() == null ? "" : allotPriceCusDto.getProIfMed());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headAllotProductPriceList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("目录商品价格列表");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            Objects.requireNonNull(workbook).write(bos);
            return cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }
}
