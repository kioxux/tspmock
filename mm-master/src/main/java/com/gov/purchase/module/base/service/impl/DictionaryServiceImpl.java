package com.gov.purchase.module.base.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaAuthconfiData;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.DictionaryMapper;
import com.gov.purchase.mapper.GaiaAuthconfiDataMapper;
import com.gov.purchase.mapper.GaiaFranchiseeMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.*;
import com.gov.purchase.module.base.service.DictionaryService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.utils.EnumUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.03.18
 */
@Service
public class DictionaryServiceImpl implements DictionaryService {

    @Resource
    DictionaryMapper dictionaryMapper;

    @Resource
    private FeignService feignService;

    @Resource
    GaiaFranchiseeMapper gaiaFranchiseeMapper;

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaAuthconfiDataMapper gaiaAuthconfiDataMapper;

    /**
     * 数据字典集合
     *
     * @return
     */
    @Override
    public Result getDictionary(DictionaryParams dictionaryParams) {
        // 字典key错误
        if (!EnumUtils.contains(dictionaryParams.getKey(), CommonEnum.DictionaryData.class)) {
            return ResultUtil.error(ResultEnum.E0101);
        }
        CommonEnum.DictionaryData dictionaryData = CommonEnum.DictionaryData.getDictionary(dictionaryParams.getKey());
        // 查询属性
        String field = StringUtils.isBlank(dictionaryParams.getType()) ? dictionaryData.getField1() : dictionaryData.getField2();
        // 查询条件
        String where = StringUtils.isBlank(dictionaryData.getCondition()) ? " 1 = 1 " : dictionaryData.getCondition();
        // 模糊查询
        Map<String, String> params = dictionaryParams.getParams();
        if (params != null && !params.isEmpty()) {
            for (String key : params.keySet()) {
                String param = params.get(key);
                if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(param)) {
                    where += " AND INSTR(" + key + ", '" + param + "') > 0 ";
                }
            }
        }
        // 精确查询
        Map<String, String> exactParams = dictionaryParams.getExactParams();
        if (exactParams != null && !exactParams.isEmpty()) {
            for (String key : exactParams.keySet()) {
                String param = exactParams.get(key);
                if (StringUtils.isNotBlank(key) && StringUtils.isNotBlank(param)) {
                    where += " AND " + key + " = '" + param.replaceAll("'", "''") + "'";
                }
            }
        }
        if (dictionaryParams.getPageNum() != null && dictionaryParams.getPageSize() != null &&
                dictionaryParams.getPageSize() > 0 && dictionaryParams.getPageNum() > 0) {
            PageHelper.startPage(dictionaryParams.getPageNum(), dictionaryParams.getPageSize());
            // 查询
            List<Dictionary> list = dictionaryMapper.getDictionaryList(dictionaryData.getTable(), field, where, dictionaryData.getSortFields());
            PageInfo<Dictionary> pageInfo = new PageInfo<>(list);
            return ResultUtil.success(pageInfo);
        } else {
            List<Dictionary> list = dictionaryMapper.getDictionaryList(dictionaryData.getTable(), field, where, dictionaryData.getSortFields());
            return ResultUtil.success(list);
        }
    }

    /**
     * 权限地点集合
     *
     * @return
     */
    @Override
    public Result siteList() {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaStoreData> list = gaiaAuthconfiDataMapper.getSiteList(user.getClient(), user.getUserId());
        return ResultUtil.success(list);
    }

    /**
     * 加盟商集合
     *
     * @param client 加盟商ID
     * @return
     */
    @Override
    public Result franchiseeList(String client) {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaFranchiseeResponseDto> list = gaiaFranchiseeMapper.franchiseeList(user.getClient());
        return ResultUtil.success(list);
    }

    /**
     * 门店信息集合
     *
     * @param client 加盟商ID
     * @return
     */
    @Override
    public Result storeDataList(String client) {
        List<GaiaFranchiseeResponseDto> gaiaFranchiseeResponseDtoList = gaiaFranchiseeMapper.franchiseeList(client);
        if (CollectionUtils.isEmpty(gaiaFranchiseeResponseDtoList)) {
            return ResultUtil.success(gaiaFranchiseeResponseDtoList);
        }
        // 门店集合
        List<GaiaStoreData> gaiaStoreDataList = gaiaStoreDataMapper.storeDataList();
        // 加盟商追加门店子级
        for (GaiaFranchiseeResponseDto gaiaFranchiseeResponseDto : gaiaFranchiseeResponseDtoList) {
            List<GaiaStoreData> list = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(gaiaStoreDataList)) {
                for (GaiaStoreData gaiaStoreData : gaiaStoreDataList) {
                    // 门店属于当前加盟商
                    if (gaiaStoreData.getClient().equals(gaiaFranchiseeResponseDto.getClient())) {
                        list.add(gaiaStoreData);
                    }
                }
            }
            // 门店
            gaiaFranchiseeResponseDto.setGaiaStoreDataList(list);
        }
        return ResultUtil.success(gaiaFranchiseeResponseDtoList);
    }

    /**
     * 静态数据字典集合
     *
     * @param key
     * @return
     */
    @Override
    public Result getDictionaryStatic(String key) {
        List<DictionaryStatic> list = new ArrayList<>();
        if (StringUtils.isNotBlank(key)) {
            // 字典key错误
            if (!EnumUtils.contains(key, CommonEnum.DictionaryStaticData.class)) {
                return ResultUtil.success(list);
            }
        }
        list = CommonEnum.DictionaryStaticData.getDictionary(key);
        return ResultUtil.success(list);
    }

    /**
     * 权限地点集合 首营用
     *
     * @return
     */
    @Override
    public Result siteListForGspinfo() {
        TokenUser user = feignService.getLoginInfo();
        List<GaiaStoreDataForGspinfo> list = gaiaAuthconfiDataMapper.siteListForGspinfo(user.getClient(), user.getUserId());
        return ResultUtil.success(list);
    }

    /**
     * 纳税主体选择
     *
     * @param chainHead
     * @return
     */
    @Override
    public Result getTaxSubject(String chainHead) {
        TokenUser user = feignService.getLoginInfo();
        return ResultUtil.success(gaiaStoreDataMapper.getTaxSubject(user.getClient(), chainHead));
    }
}

