package com.gov.purchase.module.store.dto.price;

import lombok.Data;

import java.util.List;

/**
 * @description: 价格组下的门店
 * @author: yzf
 * @create: 2021-12-16 16:25
 */
@Data
public class PriceStoreListDTO {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 价格组id
     */
    private String priceGroupId;

    /**
     * 门店编码列表
     */
    private List<String> stores;
}
