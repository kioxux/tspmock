package com.gov.purchase.module.purchase.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SavePayingResponseDTO {

    /**
     * 付款二维码信息
     */
    private String billQRCode;

    /**
     * 订单号
     */
    private String ficoId;

    /**
     * 当前加盟商
     */
    private String client;

    /**
     * 银联下单url
     */
    private String orderUrl;

}
