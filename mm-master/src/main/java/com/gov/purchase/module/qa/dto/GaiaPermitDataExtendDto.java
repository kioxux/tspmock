package com.gov.purchase.module.qa.dto;

import com.gov.purchase.entity.GaiaPermitData;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GaiaPermitDataExtendDto extends GaiaPermitData {

    /**
     * 加盟商名称
     */
    private String francName;

    /**
     * 实体名称
     */
    private String perEntityName;

    /**
     * 证照名
     */
    private String perCodeName;

    /**
     * 实体名
     */
    private String perEntityIdName;

    private List<GaiaPermitDataExtendDto> details;

}
