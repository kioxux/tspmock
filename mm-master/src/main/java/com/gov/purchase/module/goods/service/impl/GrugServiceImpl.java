package com.gov.purchase.module.goods.service.impl;

import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaProductBusiness;
import com.gov.purchase.entity.GaiaProductGspinfo;
import com.gov.purchase.entity.GaiaSdProductPrice;
import com.gov.purchase.entity.GaiaSdProductPriceKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductBusinessMapper;
import com.gov.purchase.mapper.GaiaProductGspinfoMapper;
import com.gov.purchase.mapper.GaiaSdProductPriceMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.goods.dto.QueryUnDrugCheckRequestDto;
import com.gov.purchase.module.goods.service.GrugService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class GrugServiceImpl implements GrugService {

    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    GaiaProductGspinfoMapper gaiaProductGspinfoMapper;
    @Resource
    FeignService feignService;
    @Resource
    GrugService grugService;
    @Resource
    GaiaSdProductPriceMapper gaiaSdProductPriceMapper;

    /**
     * 非药新建校验
     *
     * @param dto GspinfoSubmitRequestDto
     */
    public void checkUnDrugCheckInfo(QueryUnDrugCheckRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //加盟商
        //编辑的场合
        if (CommonEnum.ProofType.PROOFVED.getCode().equals(dto.getType())) {
        } else { //新建的场合
            dto.setClient(user.getClient());
        }
        if (StringUtils.isBlank(dto.getClient())) {
            throw new CustomResultException(ResultEnum.CHECK_CLIENT);
        }
        //批准文号批准日期小于失效日期
        if (!StringUtils.isBlank(dto.getProRegisterDate()) && !StringUtils.isBlank(dto.getProRegisterExdate())) {
            if (DateUtils.compareTime(LocalDate.parse(dto.getProRegisterDate(), DateTimeFormatter.BASIC_ISO_DATE), LocalDate.parse(dto.getProRegisterExdate(), DateTimeFormatter.BASIC_ISO_DATE))) {
                throw new CustomResultException(ResultEnum.CHECK_DATE);
            }
        }
        // 新建
        if (!CommonEnum.ProofType.PROOFVED.getCode().equals(dto.getType())) {
            // 同一加盟商 地点 下 自编码不可以重复
            List<GaiaProductGspinfo> gspinfos = gaiaProductGspinfoMapper.selfOnlyCheck(dto.getClient(), dto.getProSelfCode(), dto.getProSite());
            if (CollectionUtils.isNotEmpty(gspinfos)) {
                throw new CustomResultException(ResultEnum.SELFCODE_ERROR);
            }
            List<GaiaProductBusiness> businesses = gaiaProductBusinessMapper.selfOnlyCheck(dto.getClient(), dto.getProSelfCode(), dto.getProSite());
            if (CollectionUtils.isNotEmpty(businesses)) {
                throw new CustomResultException(ResultEnum.SELFCODE_ERROR);
            }
        }
    }

    /**
     * 非药新建
     *
     * @param dto QueryUnDrugCheckRequestDto
     */
    @Transactional
    public void insertUnDrugAddInfo(QueryUnDrugCheckRequestDto dto) {
        TokenUser user = feignService.getLoginInfo();
        //非药新建校验
        grugService.checkUnDrugCheckInfo(dto);
        // 商品主数据业务信息表
        GaiaProductBusiness gaiaProductBusiness = new GaiaProductBusiness();
        BeanUtils.copyProperties(dto, gaiaProductBusiness);
        // 匹配状态字段默认为“0-未匹配”。
        gaiaProductBusiness.setProMatchStatus(CommonEnum.MatchStatus.DIDMATCH.getCode());
        // 加盟商
        gaiaProductBusiness.setClient(user.getClient());
        // 商品描述
        if (org.apache.commons.lang3.StringUtils.isBlank(gaiaProductBusiness.getProDepict())) {
            String proDepict = gaiaProductBusiness.getProCommonname();
            if (StringUtils.isNotBlank(gaiaProductBusiness.getProCommonname()) && !gaiaProductBusiness.getProCommonname().equals(gaiaProductBusiness.getProName())) {
                String proNameTemp = StringUtils.isBlank(gaiaProductBusiness.getProName()) ? "" : "(" + gaiaProductBusiness.getProName() + ")";
                proDepict = gaiaProductBusiness.getProCommonname() + proNameTemp;
            }
            gaiaProductBusiness.setProDepict(proDepict);
        }
        // 助记码
        if (org.apache.commons.lang3.StringUtils.isBlank(gaiaProductBusiness.getProPym())) {
            gaiaProductBusiness.setProPym(com.gov.purchase.utils.StringUtils.ToFirstChar(gaiaProductBusiness.getProDepict()));
        }
        // 中包装量
        gaiaProductBusiness.setProMidPackage(StringUtils.isBlank(dto.getProMidPackage()) ? "1" : dto.getProMidPackage());
        // 固定货位
        gaiaProductBusiness.setProFixBin(StringUtils.isBlank(dto.getProFixBin()) ? dto.getProSelfCode() : dto.getProFixBin());
        //商品状态
        gaiaProductBusiness.setProStatus(StringUtils.isBlank(gaiaProductBusiness.getProStatus()) ?
                CommonEnum.GoodsStauts.GOODSNORMAL.getCode() : gaiaProductBusiness.getProStatus());
        // 固定货位
        gaiaProductBusiness.setProFixBin(StringUtils.isBlank(dto.getProFixBin()) ? dto.getProSelfCode() : dto.getProFixBin());
        gaiaProductBusiness.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));

        // 参考毛利率按照零售、采购价自动计算存入，公式：（参考售价-参考进价）/参考售价*100%（若参考售价为空或零，直接赋值0）
        // 参考进货价
        BigDecimal pro_cgj = dto.getProCgj() == null ? BigDecimal.ZERO : dto.getProCgj();
        // 参考零售价
        BigDecimal pro_lsj = dto.getProLsj() == null ? BigDecimal.ZERO : dto.getProLsj();
        // 参考毛利率
        BigDecimal pro_mll = BigDecimal.ZERO;
        // 若参考售价为空或零，直接赋值0
        if (pro_lsj.compareTo(BigDecimal.ZERO) != 0) {
            // （参考售价-参考进价）/参考售价*100%
            pro_mll = (pro_lsj.subtract(pro_cgj)).divide(pro_lsj, 4, BigDecimal.ROUND_HALF_UP);
        }
        gaiaProductBusiness.setProMll(pro_mll);
        gaiaProductBusiness.setProStatus(CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
        if (StringUtils.isBlank(gaiaProductBusiness.getProStorageCondition())) {
            gaiaProductBusiness.setProStorageCondition("1");
        }
        if (StringUtils.isBlank(gaiaProductBusiness.getProCompclass())) {
            gaiaProductBusiness.setProCompclass("N999999");
            gaiaProductBusiness.setProCompclassName("N999999未匹配");
        }
        gaiaProductBusinessMapper.insertSelective(gaiaProductBusiness);
        // 零售价联动
        GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
        // 加盟商
        gaiaSdProductPriceKey.setClient(user.getClient());
        // 门店编码
        gaiaSdProductPriceKey.setGsppBrId(dto.getProSite());
        // 商品编码
        gaiaSdProductPriceKey.setGsppProId(dto.getProSelfCode());
        GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
        if (gaiaSdProductPrice == null) {
            gaiaSdProductPrice = new GaiaSdProductPrice();
            // 加盟商
            gaiaSdProductPrice.setClient(user.getClient());
            // 门店编码
            gaiaSdProductPrice.setGsppBrId(dto.getProSite());
            // 商品编码
            gaiaSdProductPrice.setGsppProId(dto.getProSelfCode());
            // 零售价
            gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
            gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
        } else {
            // 零售价
            gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
            gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
        }
    }
}
