package com.gov.purchase.module.wholesale.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.wholesale.dto.*;
import com.gov.purchase.module.wholesale.service.SalesOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.01
 */
@Slf4j
@Api(tags = "批发订单处理")
@Validated
@RestController
@RequestMapping("salesOrder")
public class SalesOrderController {

    @Resource
    SalesOrderService salesOrderService;

    /**
     * 批发销售订单创建
     *
     * @return
     */
    @Log("批发销售订单创建")
    @ApiOperation("批发销售订单创建")
    @PostMapping("saveSalesOrder")
    public Result saveSalesOrder(@Valid @RequestBody GaiaSoHeader gaiaSoHeader) {
        gaiaSoHeader.setType(true);
        Result result = salesOrderService.saveSalesOrder(gaiaSoHeader);
        if (ResultUtil.hasError(result)) {
            return result;
        }
        try {
            if (result.getResult1() != null) {
                com.gov.purchase.entity.GaiaSoHeader result1 = (com.gov.purchase.entity.GaiaSoHeader) result.getResult1();
                salesOrderService.soAutomaticBilling(result1);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("销售自动开单异常" + e.getMessage(), e);
        }
        return result;
    }

    @Log("批发销售订单创建客户验证")
    @ApiOperation("批发销售订单创建客户验证")
    @PostMapping("checkCustomer")
    public Result checkCustomer(@RequestBody GaiaSoHeader entity) {
        return salesOrderService.checkCustomer(entity);
    }

    /**
     * 批发销售订单暂存
     *
     * @return
     */
    @Log("批发销售订单暂存")
    @ApiOperation("批发销售订单暂存")
    @PostMapping("temporarySalesOrder")
    public Result temporarySalesOrder(@Valid @RequestBody GaiaSoHeader gaiaSoHeader) {
        return salesOrderService.temporarySalesOrder(gaiaSoHeader);
    }

    /**
     * 暂存销售列表
     *
     * @return
     */
    @Log("暂存销售列表")
    @ApiOperation("暂存销售列表")
    @PostMapping("temporaryList")
    public Result temporaryList() {
        return ResultUtil.success(salesOrderService.temporaryList());
    }

    /**
     * 暂存销售单明细
     *
     * @return
     */
    @Log("暂存销售单明细")
    @ApiOperation("暂存销售单明细")
    @PostMapping("temporaryOrderDetail")
    public Result temporaryOrderDetail(@RequestJson("soId") String soId) {
        return ResultUtil.success(salesOrderService.temporaryOrderDetail(soId));
    }

    /**
     * 删除暂存销售单
     *
     * @return
     */
    @Log("删除暂存销售单")
    @ApiOperation("删除暂存销售单")
    @PostMapping("delTemporaryOrder")
    public Result delTemporaryOrder(@RequestJson("soId") String soId) {
        salesOrderService.delTemporaryOrder(soId);
        return ResultUtil.success();
    }

    /**
     * 批发订单新增数量是否允许输入小数（配置表查询  0：允许，1：不允许）
     *
     * @return
     */
    @Log("批发订单新增数量是否允许输入小数")
    @ApiOperation("批发订单新增数量是否允许输入小数")
    @PostMapping("querySalesOrderConfig")
    public Result querySalesOrderConfig() {
        return ResultUtil.success(salesOrderService.querySalesOrderConfig());
    }

    /**
     * 商品列表获取
     *
     * @param client  加盟商
     * @param proSite 销售主体（DC编码）
     * @param name    商品自编码/名称
     * @return
     */
    @Log("商品列表获取")
    @ApiOperation("商品列表获取")
    @PostMapping("queryGoodsList")
    public Result queryGoodsList(@RequestJson("client") String client, @RequestJson("proSite") String proSite,
                                 @RequestJson(value = "soCustomerId", name = "客户编码") String soCustomerId,
                                 @RequestJson("name") String name,
                                 @RequestJson(value = "gwsCode", required = false) String gwsCode,
                                 @RequestJson(value = "priceType", required = false) Integer priceType,
                                 @RequestJson(value = "soOwner", required = false) String soOwner
    ) {
        return salesOrderService.queryGoodsList(client, proSite, soCustomerId, name, gwsCode, priceType, soOwner);
    }

    @Log("商品价格列表获取")
    @ApiOperation("商品价格列表获取")
    @PostMapping("queryGoodsPriceList")
    public Result queryGoodsPriceList(@RequestJson("proSite") String proSite,
                                      @RequestJson(value = "proSelfCode", required = false) String proSelfCode,
                                      @RequestJson(value = "proSelfCodeList", required = false) List<String> proSelfCodeList) {
        return salesOrderService.queryGoodsPriceList(proSite, proSelfCode, proSelfCodeList);
    }

    /**
     * 批发销售查询列表
     *
     * @param querySalesOrderList
     * @return
     */
    @Log("批发销售查询列表")
    @ApiOperation("批发销售查询列表")
    @PostMapping("querySalesOrderList")
    public Result querySalesOrderList(@Valid @RequestBody QuerySalesOrderList querySalesOrderList) {
        return salesOrderService.querySalesOrderList(querySalesOrderList);
    }

    @Log("批发销售查询列表_主表")
    @ApiOperation("批发销售查询列表_主表")
    @PostMapping("querySalesOrderHeaderList")
    public Result querySalesOrderHeaderList(@Valid @RequestBody QuerySalesOrderHeaderVO vo) {
        return salesOrderService.querySalesOrderHeaderList(vo);
    }

    /**
     * 批发销售查询列表导出
     *
     * @param querySalesOrderList
     * @return
     */
    @Log("批发销售查询列表导出")
    @ApiOperation("批发销售查询列表导出")
    @PostMapping("exportSalesOrderList")
    public Result exportSalesOrderList(@RequestBody QuerySalesOrderList querySalesOrderList) {
        return salesOrderService.exportSalesOrderList(querySalesOrderList);
    }

    /**
     * 批发销售订单明细
     *
     * @param client 加盟商
     * @param soId   订单号
     * @return
     */
    @Log("批发销售订单明细")
    @ApiOperation("批发销售订单明细")
    @PostMapping("getSalesOrderInfo")
    public Result getSalesOrderInfo(@RequestJson("client") String client, @RequestJson("soId") String soId) {
        return salesOrderService.getSalesOrderInfo(client, soId);
    }

    /**
     * 销售订单修改
     *
     * @param updateSalesOrder
     * @return
     */
    @Log("销售订单修改")
    @ApiOperation("销售订单修改")
    @PostMapping("updateSalesOrder")
    public Result updateSalesOrder(@Valid @RequestBody UpdateSalesOrder updateSalesOrder) {
        return salesOrderService.updateSalesOrder(updateSalesOrder);
    }

    @Log("调取请货单列表")
    @ApiOperation("调取请货单列表")
    @PostMapping("getRequestGoodsOrderList")
    public Result getRequestGoodsOrderList(@Valid @RequestBody GetRequestGoodsOrderListVO vo) {
        return ResultUtil.success(salesOrderService.getRequestGoodsOrderList(vo));
    }

    @Log("关闭请货单")
    @ApiOperation("关闭请货单")
    @PostMapping("closeBill")
    public Result closeBill(@RequestJson(value = "gsrhBrId", name = "门店") String gsrhBrId, @RequestJson(value = "gsrhVoucherId", name = "单号") String gsrhVoucherId) {
        salesOrderService.closeBill(gsrhBrId, gsrhVoucherId);
        return ResultUtil.success();
    }

    @Log("销售员下拉框")
    @ApiOperation("销售员下拉框")
    @PostMapping("getSalesmanList")
    public Result getSalesmanList(@RequestJson(value = "supSite", name = "销售主体") String supSite) {
        return salesOrderService.getSalesmanList(supSite);
    }

    @Log("查询批发销售审批详情")
    @ApiOperation("查询批发销售审批详情")
    @PostMapping("queryWholesaleApproveList")
    public Result queryWholesaleApproveList(@RequestBody WholeSaleApproveDTO wholeSaleApproveDTO) {
        return salesOrderService.queryWholesaleApproveList(wholeSaleApproveDTO);
    }

    @Log("查询批发退货审批单")
    @ApiOperation("查询批发退货审批单")
    @PostMapping("queryWholesaleReturnsList")
    public Result queryWholesaleReturnsList(@RequestBody WholeSaleApproveDTO wholeSaleApproveDTO) {
        return salesOrderService.queryWholesaleReturnsList(wholeSaleApproveDTO);
    }

    /**
     * 批发退货订单明细
     *
     * @param soId 订单号
     * @return
     */
    @Log("批发退货订单明细")
    @ApiOperation("批发退货订单明细")
    @PostMapping("getSalesOrderReturnsInfo")
    public Result getSalesOrderReturnsInfo(@RequestJson("soId") String soId) {
        return salesOrderService.getSalesOrderReturnsInfo(soId);
    }

    @Log("更新批发-销售/退货-审批状态")
    @ApiOperation("更新批发-销售/退货-审批状态")
    @PostMapping("updateApproveStatus")
    public Result updateApproveStatus(@RequestJson(value = "soId", name = "销售订单号") String soId,
                                      @RequestJson(value = "soApproveStatus", name = "审批状态") String soApproveStatus,
                                      @RequestJson(value = "siteCode", name = "仓库地点", required = false) String siteCode
    ) {
        salesOrderService.updateApproveStatus(soId, soApproveStatus, siteCode);
        return ResultUtil.success();
    }


    @Log("更新商品预设价")
    @ApiOperation("更新商品预设价")
    @PostMapping("updateProYsj")
    public Result updateProYsj(@RequestJson(value = "proSelfCode", name = "商品自编码") String proSelfCode,
                               @RequestJson(value = "proSite", name = "地点") String proSite,
                               @RequestJson(value = "type", name = "预设价类别") String type,
                               @RequestJson(value = "proYsj", name = "预设价") BigDecimal proYsj
    ) {
        salesOrderService.updateProYsj(proSelfCode, proSite, type, proYsj);
        return ResultUtil.success();
    }
}

