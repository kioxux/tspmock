package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaLabelApply;
import com.gov.purchase.module.store.dto.EditLabelItemVO;
import com.gov.purchase.module.store.service.LabelApplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.03.08
 */
@Api(tags = "价签申请")
@RestController
@RequestMapping("labelApply")
public class LabelApplyController {

    @Resource
    private LabelApplyService labelApplyService;

    @Log("价签申请")
    @ApiOperation("价签申请")
    @PostMapping("addLabel")
    public Result addLabel(@RequestBody List<GaiaLabelApply> gaiaLabelApply) {
        labelApplyService.addLabel(gaiaLabelApply);
        return ResultUtil.success();
    }

    @Log("申请列表")
    @ApiOperation("申请列表")
    @PostMapping("getLabelList")
    public Result getLabelList(
            @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
            @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
            @RequestJson(value = "labApplyDate", required = false) String labApplyDate,
            @RequestJson(value = "labProduct", required = false) String labProduct,
            @RequestJson(value = "stores", required = false) String stores,
            @RequestJson(value = "labSuatus", required = false) String labSuatus
    ) {
        return ResultUtil.success(labelApplyService.getLabelList(pageSize, pageNum, labApplyDate, labProduct, labSuatus, stores));
    }

    @Log("有权限的门店列表")
    @ApiOperation("有权限的门店列表")
    @PostMapping("getLabelStoList")
    public Result getLabelStoList() {
        return ResultUtil.success(labelApplyService.getLabelStoList());
    }

    @Log("申请列表明细")
    @ApiOperation("申请列表明细")
    @PostMapping("getLabelItemList")
    public Result getLabelItemList(@RequestJson(value = "labStore", name = "门店") String labStore,
                                   @RequestJson(value = "labApplyNo", name = "单号") String labApplyNo) {
        return labelApplyService.getLabelItemList(labStore, labApplyNo);
    }

    @Log("申请列表编辑")
    @ApiOperation("申请列表编辑")
    @PostMapping("editLabelItem")
    public Result editLabelItem(@Valid @RequestBody EditLabelItemVO vo) {
        labelApplyService.editLabelItem(vo);
        return ResultUtil.success();
    }

    @Log("申请列表明细导出")
    @ApiOperation("申请列表明细导出")
    @PostMapping("labelItemExport")
    public Result labelItemExport(@RequestJson(value = "labStore", name = "门店") String labStore,
                                  @RequestJson(value = "labApplyNo", name = "单号") String labApplyNo) {
        return labelApplyService.labelItemExport(labStore, labApplyNo);
    }

    @Log("删除申请")
    @ApiOperation("删除申请")
    @PostMapping("delLabel")
    public Result delLabel(@RequestJson(value = "labStore", name = "门店") String labStore,
                           @RequestJson(value = "labApplyNo", name = "单号") String labApplyNo) {
        labelApplyService.delLabel(labStore, labApplyNo);
        return ResultUtil.success();
    }

    @Log("申请列表选择")
    @ApiOperation("申请列表选择")
    @PostMapping("selectLabelList")
    public Result selectLabelList(
            @RequestJson(value = "pageSize", name = "分页") Integer pageSize,
            @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
            @RequestJson(value = "labStore", required = false) String labStore,
            @RequestJson(value = "labApplyNo", required = false) String labApplyNo,
            @RequestJson(value = "labApplyDateStart", required = false) String labApplyDateStart,
            @RequestJson(value = "labApplyDateEnd", required = false) String labApplyDateEnd,
            @RequestJson(value = "labPrintTimes", required = false) Integer labPrintTimes) {
        return labelApplyService.selectLabelList(pageSize, pageNum, labStore, labApplyNo, labApplyDateStart, labApplyDateEnd, labPrintTimes);
    }

    /**
     * 申请单关闭
     *
     * @param labStore
     * @param labApplyNo
     * @return
     */
    @Log("申请单关闭")
    @ApiOperation("申请单关闭")
    @PostMapping("closeLable")
    public Result closeLable(@RequestJson(value = "labStore", name = "门店") String labStore,
                             @RequestJson(value = "labApplyNo", name = "单号") String labApplyNo) {
        labelApplyService.closeLable(labStore, labApplyNo);
        return ResultUtil.success();
    }

}
