package com.gov.purchase.module.customer.dto;

import com.gov.purchase.utils.csv.annotation.CsvCell;
import com.gov.purchase.utils.csv.annotation.CsvRow;
import lombok.Data;

import java.math.BigDecimal;

@Data
@CsvRow("客户表返回参数")
public class CustomerGspInfoListReponseDtoCSV {

    /**
     * client
     */
//    @CsvCell(title = "client", index = 1, fieldNo = 1)
    private String client;

    /**
     * 客户自编码
     */
    @CsvCell(title = "客户自编码", index = 1, fieldNo = 1)
    private String cusSelfCode;

    /**
     * 地点
     */
//    @CsvCell(title = "地点", index = 1, fieldNo = 1)
    private String cusSite;

    /**
     * cus_code
     */
//    @CsvCell(title = "cus_code", index = 1, fieldNo = 1)
    private String cusCode;

    /**
     * 客户名称
     */
    @CsvCell(title = "客户名称", index = 1, fieldNo = 1)
    private String cusName;

    /**
     * 营业执照编号
     */
    @CsvCell(title = "营业执照编号", index = 1, fieldNo = 1)
    private String cusCreditCode;

    /**
     * 营业期限
     */
    @CsvCell(title = "营业期限", index = 1, fieldNo = 1)
    private String cusCreditDate;

    /**
     * 法人
     */
    @CsvCell(title = "法人", index = 1, fieldNo = 1)
    private String cusLegalPerson;

    /**
     * 注册地址
     */
    @CsvCell(title = "注册地址", index = 1, fieldNo = 1)
    private String cusRegAdd;

    /**
     * 许可证编号
     */
    @CsvCell(title = "许可证编号", index = 1, fieldNo = 1)
    private String cusLicenceNo;

    /**
     * 发证单位
     */
    @CsvCell(title = "发证单位", index = 1, fieldNo = 1)
    private String cusLicenceOrgan;

    /**
     * 发证日期
     */
    @CsvCell(title = "发证日期", index = 1, fieldNo = 1)
    private String cusLicenceDate;

    /**
     * 有效期至
     */
    @CsvCell(title = "有效期至", index = 1, fieldNo = 1)
    private String cusLicenceValid;

    /**
     * 邮政编码
     */
    @CsvCell(title = "邮政编码", index = 1, fieldNo = 1)
    private String cusPostalCode;

    /**
     * 税务登记证
     */
    @CsvCell(title = "税务登记证", index = 1, fieldNo = 1)
    private String cusTaxCard;

    /**
     * 组织机构代码证
     */
    @CsvCell(title = "组织机构代码证", index = 1, fieldNo = 1)
    private String cusOrgCard;

    /**
     * 开户户名
     */
    @CsvCell(title = "开户户名", index = 1, fieldNo = 1)
    private String cusAccountName;

    /**
     * 开户银行
     */
    @CsvCell(title = "开户银行", index = 1, fieldNo = 1)
    private String cusAccountBank;

    /**
     * 银行账号
     */
    @CsvCell(title = "银行账号", index = 1, fieldNo = 1)
    private String cusBankAccount;

    /**
     * 随货同行单（票）情况
     */
    @CsvCell(title = "随货同行单（票）情况", index = 1, fieldNo = 1)
    private String cusDocState;

    /**
     * 企业印章情况
     */
    @CsvCell(title = "企业印章情况", index = 1, fieldNo = 1)
    private String cusSealState;

    /**
     * 生产或经营范围
     */
    @CsvCell(title = "生产或经营范围", index = 1, fieldNo = 1)
    private String cusScope;

    /**
     * 经营方式
     */
    @CsvCell(title = "经营方式", index = 1, fieldNo = 1)
    private String cusOperationMode;

    /**
     * 首营日期
     */
    @CsvCell(title = "首营日期", index = 1, fieldNo = 1)
    private String cusGspDate;

    /**
     * 首营流程编号
     */
    @CsvCell(title = "首营流程编号", index = 1, fieldNo = 1)
    private String cusFlowNo;

    /**
     * cus_gsp_status
     */
//    @CsvCell(title = "cus_gsp_status", index = 1, fieldNo = 1)
    private String cusGspStatus;

    /**
     * cus_delivery_add
     */
    @CsvCell(title = "客户收货地址", index = 1, fieldNo = 1)
    private String cusDeliveryAdd;

    /**
     * 质量负责人
     */
    @CsvCell(title = "质量负责人", index = 1, fieldNo = 1)
    private String cusQua;

    /**
     * 业务联系人
     */
    @CsvCell(title = "业务联系人", index = 1, fieldNo = 1)
    private String cusBussinessContact;

    /**
     * last_update_time
     */
//    @CsvCell(title = "last_update_time", index = 1, fieldNo = 1)
    private String lastUpdateTime;

    /**
     * 医疗器械经营许可证有效期
     */
    @CsvCell(title = "医疗器械经营许可证有效期", index = 1, fieldNo = 1)
    private String cusYlqxxkzDate;

    /**
     * 医疗器械经营许可证书
     */
    @CsvCell(title = "医疗器械经营许可证书", index = 1, fieldNo = 1)
    private String cusYlqxxkz;

    /**
     * 医疗机构执业许可证书有效期
     */
    @CsvCell(title = "医疗机构执业许可证书有效期", index = 1, fieldNo = 1)
    private String cusYljgxkzDate;

    /**
     * 医疗机构执业许可证书
     */
    @CsvCell(title = "医疗机构执业许可证书", index = 1, fieldNo = 1)
    private String cusYljgxkz;

    /**
     * 客户状态
     */
    @CsvCell(title = "客户状态", index = 1, fieldNo = 1)
    private String cusStatus;

    /**
     * 食品许可证有效期
     */
    @CsvCell(title = "食品许可证有效期", index = 1, fieldNo = 1)
    private String cusSpxkzDate;

    /**
     * 食品许可证书
     */
    @CsvCell(title = "食品许可证书", index = 1, fieldNo = 1)
    private String cusSpxkz;

    /**
     * 助记码
     */
    @CsvCell(title = "助记码", index = 1, fieldNo = 1)
    private String cusPym;

    /**
     * 医疗器械经营许可证图片
     */
//    @CsvCell(title = "医疗器械经营许可证图片", index = 1, fieldNo = 1)
    private String cusPictureYlqxxkz;

    /**
     * 医疗机构执业许可证图片
     */
//    @CsvCell(title = "医疗机构执业许可证图片", index = 1, fieldNo = 1)
    private String cusPictureYljgxkz;

    /**
     * 食品许可证书图片
     */
//    @CsvCell(title = "食品许可证书图片", index = 1, fieldNo = 1)
    private String cusPictureSpxkz;

    /**
     * 随货同行单图片
     */
//    @CsvCell(title = "随货同行单图片", index = 1, fieldNo = 1)
    private String cusPictureShtxd;

    /**
     * 其他图片
     */
//    @CsvCell(title = "其他图片", index = 1, fieldNo = 1)
    private String cusPictureQt;

    /**
     * gsp证书图片
     */
//    @CsvCell(title = "gsp证书图片", index = 1, fieldNo = 1)
    private String cusPictureGsp;

    /**
     * gmp证书图片
     */
//    @CsvCell(title = "gmp证书图片", index = 1, fieldNo = 1)
    private String cusPictureGmp;

    /**
     * 法人委托书图片
     */
//    @CsvCell(title = "法人委托书图片", index = 1, fieldNo = 1)
    private String cusPictureFrwts;

    /**
     * 付款条件
     */
    @CsvCell(title = "付款条件", index = 1, fieldNo = 1)
    private String cusPayTerm;

    /**
     * 支付方式
     */
    @CsvCell(title = "支付方式", index = 1, fieldNo = 1)
    private String cusPayMode;

    /**
     * 禁止销售
     */
    @CsvCell(title = "禁止销售", index = 1, fieldNo = 1)
    private String cusNoSale;

    /**
     * 禁止退货
     */
    @CsvCell(title = "禁止退货", index = 1, fieldNo = 1)
    private String cusNoReturn;

    /**
     * 匹配状态
     */
    @CsvCell(title = "匹配状态", index = 1, fieldNo = 1)
    private String cusMatchStatus;

    /**
     * gmp有效期
     */
    @CsvCell(title = "gmp有效期", index = 1, fieldNo = 1)
    private String cusGmpDate;

    /**
     * gmp证书
     */
    @CsvCell(title = "gmp证书", index = 1, fieldNo = 1)
    private String cusGmp;

    /**
     * 信用额度
     */
    @CsvCell(title = "信用额度", index = 1, fieldNo = 1)
    private BigDecimal cusCreditQuota;

    /**
     * 是否启用信用管理
     */
    @CsvCell(title = "是否启用信用管理", index = 1, fieldNo = 1)
    private String cusCreditFlag;

    /**
     * 信用检查点
     */
    @CsvCell(title = "信用检查点", index = 1, fieldNo = 1)
    private String cusCreditCheck;

    /**
     * 铺底授信额度
     */
    @CsvCell(title = "铺底授信额度", index = 1, fieldNo = 1)
    private String cusCreditAmt;

    /**
     * 联系人电话
     */
    @CsvCell(title = "联系人电话", index = 1, fieldNo = 1)
    private String cusContactTel;

    /**
     * 客户分类
     */
    @CsvCell(title = "客户分类", index = 1, fieldNo = 1)
    private String cusClass;

    /**
     * 银行名称
     */
    @CsvCell(title = "银行名称", index = 1, fieldNo = 1)
    private String cusBankName;

    /**
     * 银行代码
     */
    @CsvCell(title = "银行代码", index = 1, fieldNo = 1)
    private String cusBankCode;

    /**
     * 账户持有人
     */
    @CsvCell(title = "账户持有人", index = 1, fieldNo = 1)
    private String cusAccountPerson;

    /**
     * gsp证书
     */
    @CsvCell(title = "gsp证书", index = 1, fieldNo = 1)
    private String cusGsp;

    /**
     * gsp有效期
     */
    @CsvCell(title = "gsp有效期", index = 1, fieldNo = 1)
    private String cusGspDates;

    /**
     * 注册资本
     */
    @CsvCell(title = "注册资本", index = 1, fieldNo = 1)
    private String cusZczb;

    /**
     * 仓库地址
     */
    @CsvCell(title = "仓库地址", index = 1, fieldNo = 1)
    private String cusCkdz;

    /**
     * 业务员（销售）
     */
    @CsvCell(title = "业务员（销售）", index = 1, fieldNo = 1)
    private String cusYwy;

    /**
     * 业务员（采购员）的身份证号码
     */
    @CsvCell(title = "业务员（采购员）的身份证号码", index = 1, fieldNo = 1)
    private String cusYwysfz;

    /**
     * 业务员（采购员）授权日期
     */
    @CsvCell(title = "业务员（采购员）授权日期", index = 1, fieldNo = 1)
    private String cusYwysqrq;

    /**
     * 业务员（采购员）结束日期
     */
    @CsvCell(title = "业务员（采购员）结束日期", index = 1, fieldNo = 1)
    private String cusYwyjsrq;

    /**
     * 收货人/提货人姓名
     */
    @CsvCell(title = "收货人/提货人姓名", index = 1, fieldNo = 1)
    private String cusShr;

    /**
     * 收货人/提货人身份证号
     */
    @CsvCell(title = "收货人/提货人身份证号", index = 1, fieldNo = 1)
    private String cusShrsfz;

    /**
     * 收货人/提货人授权日期
     */
    @CsvCell(title = "收货人/提货人授权日期", index = 1, fieldNo = 1)
    private String cusShrsqrq;

    /**
     * 收货人/提货人结束日期
     */
    @CsvCell(title = "收货人/提货人结束日期", index = 1, fieldNo = 1)
    private String cusShrjsrq;

    /**
     * 备注
     */
    @CsvCell(title = "备注", index = 1, fieldNo = 1)
    private String cusRemarks;

    /**
     * 企业负责人
     */
    @CsvCell(title = "企业负责人", index = 1, fieldNo = 1)
    private String cusQyfzr;

    /**
     * 经营方式
     */
    @CsvCell(title = "经营方式", index = 1, fieldNo = 1)
    private String cusJyfs;

    /**
     * 开户许可证号
     */
    @CsvCell(title = "开户许可证号", index = 1, fieldNo = 1)
    private String cusKhxkzh;

    /**
     * 税号
     */
    @CsvCell(title = "税号", index = 1, fieldNo = 1)
    private String cusSh;

    /**
     * 电话号码
     */
    @CsvCell(title = "电话号码", index = 1, fieldNo = 1)
    private String cusDhhm;

    /**
     * 发票类型”及其选项（增值税专用发票、普通发票或普票）
     */
    @CsvCell(title = "发票类型”及其选项（增值税专用发票、普通发票或普票）", index = 1, fieldNo = 1)
    private String cusFplx;

    /**
     * 医疗器械备案证
     */
    @CsvCell(title = "医疗器械备案证", index = 1, fieldNo = 1)
    private String cusYlqxbaz;

    /**
     * 医疗器械类别”（选项：一类、二类、三类）
     */
    @CsvCell(title = "医疗器械类别”（选项：一类、二类、三类）", index = 1, fieldNo = 1)
    private String cusYlqxlb;

    /**
     * 质保协议期限
     */
    @CsvCell(title = "质保协议期限", index = 1, fieldNo = 1)
    private String cusZbxyyxq;

    /**
     * 购销合同期限
     */
    @CsvCell(title = "购销合同期限", index = 1, fieldNo = 1)
    private String cusGxhtyxq;

    /**
     * 法人委托书
     */
    @CsvCell(title = "法人委托书", index = 1, fieldNo = 1)
    private String cusFrwts;

    /**
     * 法人委托书效期
     */
    @CsvCell(title = "法人委托书效期", index = 1, fieldNo = 1)
    private String cusFrwtsxq;

    /**
     * 客户签收图片
     */
    @CsvCell(title = "客户签收图片", index = 1, fieldNo = 1)
    private String cusKhqstp;

    /**
     * 运输方式
     */
    @CsvCell(title = "运输方式", index = 1, fieldNo = 1)
    private String cusYsfs;

    /**
     * 运输时长
     */
    @CsvCell(title = "运输时长", index = 1, fieldNo = 1)
    private String cusYssc;

    /**
     * gsp控制类型”及其选项（批发企业/医疗机构/连锁药店/其他）
     */
    @CsvCell(title = "gsp控制类型”及其选项（批发企业/医疗机构/连锁药店/其他）", index = 1, fieldNo = 1)
    private String cusGspgklx;

}
