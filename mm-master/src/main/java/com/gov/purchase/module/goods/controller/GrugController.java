package com.gov.purchase.module.goods.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.module.goods.dto.QueryUnDrugCheckRequestDto;
import com.gov.purchase.module.goods.service.GrugService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "非药新建")
@RestController
@RequestMapping("grug")
public class GrugController {

    @Resource
    private GrugService grugService;

    /*
     * 非药新建校验
     *
     * @param dto QueryUnDrugCheckRequestDto
     * @return Result
     */
    @Log("非药新建校验")
    @ApiOperation("非药新建校验")
    @PostMapping("unDrugCheck")
    public Result resultUnDrugCheckInfo(@Valid @RequestBody QueryUnDrugCheckRequestDto queryUnDrugCheckRequestDto) {
        // 非药新建校验
        grugService.checkUnDrugCheckInfo(queryUnDrugCheckRequestDto);
        return ResultUtil.success();
    }

    /*
     * 非药新建
     *
     * @param dto QueryUnDrugCheckRequestDto
     * @return
     */
    @Log("非药新建")
    @ApiOperation("非药新建")
    @PostMapping("unDrugAdd")
    public Result resultUnDrugAddInfo(@Valid @RequestBody QueryUnDrugCheckRequestDto queryUnDrugAddRequestDto) {
        // 非药新建校验
        grugService.insertUnDrugAddInfo(queryUnDrugAddRequestDto);
        return ResultUtil.success();
    }
}
