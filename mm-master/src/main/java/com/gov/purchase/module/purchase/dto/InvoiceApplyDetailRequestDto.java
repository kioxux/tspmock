package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * @Author staxc
 * @Date 2020/10/23 14:08
 * @desc
 */
@Data
@ApiModel(value = "发票申请明细传入参数")
public class InvoiceApplyDetailRequestDto {
    /**
     * 商品名称  如FPHXZ=1，则此商品行为折扣行，此版本折扣行不允许多行折扣，折扣行必须紧邻被折扣行，项目名称必须与被折扣行一致。
     */
    private String goodsname;



    /**
     * 冲红时项目数量为负数
     * 量，数量、单价必须都不填，或都必填，不可只填一个；当数量、单价都不填时，不含税金额、税额、含税金额都必填；当数量
     * 单价都填时，不含税金额、税额、含税金额可为空。
     * 开具成品油发票时必填。
     * 建议保留小数点后8位。
     */
    private String num;

    /**
     * 冲红时项目单价为正数
     * 单价，数量、单价必须都不填，或者都必填，不可只填一个；当数量、单价都不填时，不含税金额、税额、含税金额都必填；当数量、
     * 单价都填时，不含税金额、税额、含税金额可为空。
     * 开具成品油发票时必填。
     * 建议保留小数点后8位。
     */
    private String price;

    /**
     * 单价含税标志，0:不含税,1:含税
     */
    private String hsbz;

    /**
     * 税率
     */
    private String taxrate;

    /**
     * 规格型号
     */
    private String spec;

    /**
     * 单位，开具成品油发票时必填，必须为”升”或“吨”。
     */
    private String unit;

    /**
     * 税收分类编码 签订免责协议客户可不传入，由接口进行匹配，如对接口速度敏感、赋码准确要求高的企业，建议传入该字段
     */
    private String spbm;

    /**
     * 发票行性质，0:正常行;1:折扣行;2:被折扣行
     */
    private String fphxz;

    /**
     * 优惠政策标识,0:不使用;1:使用
     */
    private String yhzcbs;


}
