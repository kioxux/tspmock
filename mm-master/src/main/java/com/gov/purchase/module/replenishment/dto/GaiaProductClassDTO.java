package com.gov.purchase.module.replenishment.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class GaiaProductClassDTO {

    private String proBigClassCode;
    private String proBigClassName;

}
