package com.gov.purchase.module.supplier.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaSupplierBusiness;
import com.gov.purchase.module.supplier.dto.*;
import com.gov.purchase.module.supplier.service.EditSupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@Api(tags = "供应商维护")
@RestController
@RequestMapping("supplier")
public class EditSupplierController {

    @Resource
    private EditSupplierService editSupplierService;

    /**
     * 供应商列表
     *
     * @param dto QueryProRequestDto
     * @return pageInfo PageInfo<QueryProResponseDto>
     */
    @Log("供应商列表")
    @ApiOperation("供应商列表")
    @PostMapping("querySupplierList")
    public Result resultQuerySupInfo(@Valid @RequestBody SupplierListRequestDto dto) {
        //供应商列表信息查询
        PageInfo<SupplierListResponseDto> queryProList = editSupplierService.selectQuerySupInfo(dto);
        return ResultUtil.success(queryProList);
    }

    /*
     * 供应商详情
     *
     * @param dto QueryPorRequestDto
     * @return QuerySupResponseDto
     */
    @Log("供应商详情")
    @ApiOperation("供应商详情")
    @PostMapping("querySupplier")
    public Result resultQuerySupInfo(@Valid @RequestBody QuerySupRequestDto dto) {
        //供应商详情
        GaiaSupplierBusiness gaiaSupplierBusiness = editSupplierService.selectQuerySup(dto);
        return ResultUtil.success(gaiaSupplierBusiness);
    }

    /*
     * 供应商维护
     *
     * @param dto QueryUnDrugUpdateRequestDto
     * @return
     */
    @Log("供应商维护")
    @ApiOperation("供应商维护")
    @PostMapping("supplierEdit")
    public Result resultSupEditInfo(@Valid @RequestBody QueryUnDrugUpdateRequestDto dto) {
        // 编辑保存
        editSupplierService.updateSupEditInfo(dto);
        return ResultUtil.success();
    }
}
