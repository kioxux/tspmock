package com.gov.purchase.module.customer.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.diff.DiffEntityConfig;
import com.gov.purchase.common.diff.DiffEntityReuslt;
import com.gov.purchase.common.diff.DiffEntityUtils;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.utils.ListUtils;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.base.dto.businessScope.GetScopeListDTO;
import com.gov.purchase.module.base.service.BusinessScopeService;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.customer.dto.GetCusDetailsDTO;
import com.gov.purchase.module.customer.dto.GetCusListRequestDTO;
import com.gov.purchase.module.customer.dto.SaveCusList1RequestDTO;
import com.gov.purchase.module.customer.dto.SaveCusList2RequestDTO;
import com.gov.purchase.module.customer.service.OptimizeCustomerService;
import com.gov.purchase.module.supplier.dto.GaiaCustomerBusinessDTO;
import com.gov.purchase.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 商品首营优化
 */
@Slf4j
@Service
public class OptimizeCustomerServiceImpl implements OptimizeCustomerService {

    @Resource
    CosUtils cosUtils;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaCustomerBasicMapper gaiaCustomerBasicMapper;
    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;
    @Resource
    private GaiaCustomerGspinfoMapper gaiaCustomerGspinfoMapper;
    @Resource
    private GaiaCustomerGspinfoLogMapper gaiaCustomerGspinfoLogMapper;
    @Resource
    GaiaUserDataMapper gaiaUserDataMapper;
    @Resource
    BusinessScopeService businessScopeService;
    @Resource
    GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    GaiaCustomerChangeMapper gaiaCustomerChangeMapper;
    @Resource
    DiffEntityUtils diffEntityUtils;
    @Resource
    DiffEntityConfig diffEntityConfig;
    @Resource
    BusinessScopeServiceMapper businessScopeServiceMapper;


    /**
     * 选择机构列表
     */
    @Override
    public List<GaiaDcData> getSiteList() {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        List<GaiaDcData> siteList = gaiaCustomerBasicMapper.getSiteList(client, userId);
        return siteList;
    }

    /**
     * 回车查询
     */
    @Override
    public List<GaiaCustomerBusiness> enterSelect(String cusName, String cusCreditCode) {
        if (StringUtils.isEmpty(cusName) && StringUtils.isEmpty(cusCreditCode)) {
            throw new CustomResultException("请输入[客户名称]或[社会统一信用代码]进行查询");
        }
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        // 查询结果
        List<GaiaCustomerBasic> basicList = gaiaCustomerBasicMapper.enterSelect(cusName, cusCreditCode);
        // 地点列表
        List<String> siteList = gaiaCustomerBasicMapper.getSiteList(client, userId)
                .stream().filter(Objects::nonNull).map(GaiaDcDataKey::getDcCode).collect(Collectors.toList());
        // 自编码列表
        List<String> selfCodelist = gaiaCustomerBusinessMapper.getSelfCodeList2(client, siteList);
        // 查询结果设置新的自编码
        List<GaiaCustomerBusiness> collect = basicList.stream().map(basic -> {
            // 转类型 放出 cusSelfCode 字段
            GaiaCustomerBusiness business = new GaiaCustomerBusiness();
            BeanUtils.copyProperties(basic, business);
            // cusSelfCode 字段设置值
            if (CollectionUtils.isEmpty(selfCodelist)) {
                business.setCusSelfCode("1000000");
                selfCodelist.add("1000000");
            } else {
                String maxCode = ListUtils.getMaxCode(selfCodelist);
                String selfCode = ListUtils.getMaxCode(maxCode);
                business.setCusSelfCode(selfCode);
                selfCodelist.add(selfCode);
            }
            return business;
        }).collect(Collectors.toList());

        return collect;
    }

    /**
     * 保存1
     */
    @Override
    public List<GaiaCustomerBusiness> saveCusList1(List<SaveCusList1RequestDTO> list) {
        list.forEach(item -> {
            if (StringUtils.isEmpty(item.getCusName())) {
                throw new CustomResultException("客户名称不能为空");
            }
            if (StringUtils.isEmpty(item.getCusSelfCode())) {
                throw new CustomResultException("客户编码不能为空");
            }
        });
        TokenUser user = feignService.getLoginInfo();
        List<SaveCusList1RequestDTO> objList = list.stream().filter(item -> StringUtils.isNotBlank(item.getCusCreditCode())).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(objList)) {
            return new ArrayList<GaiaCustomerBusiness>();
        }
        // 比对
        List<GaiaCustomerBusiness> compList = gaiaCustomerBusinessMapper.getCompList(user.getClient(), objList);
        return compList;
    }

    /**
     * 保存2
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveCusList2(SaveCusList2RequestDTO dto) {
        List<SaveCusList1RequestDTO> cusBaseList = dto.getCusBaseList();
        if (CollectionUtils.isEmpty(cusBaseList)) {
            throw new CustomResultException("客户列表不通为空");
        }
        String proSelfCodeRepeat = cusBaseList.stream().filter(Objects::nonNull)
                .map(GaiaCustomerGspinfo::getCusSelfCode)
                .collect(Collectors.toMap(item -> item, item -> 1, Integer::sum))
                .entrySet().stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.joining(","));
        if (StringUtils.isNotEmpty(proSelfCodeRepeat)) {
            throw new CustomResultException(MessageFormat.format("商品编码[{}]重复", proSelfCodeRepeat));
        }

        // 参数校验
        checkParam(cusBaseList);
        TokenUser user = feignService.getLoginInfo();
        // 流程编号
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNo = System.currentTimeMillis() + org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(i), 6, "0");
        // 直接发起首营
        for (SaveCusList1RequestDTO customer : cusBaseList) {
            GaiaCustomerBusiness customerBusiness = gaiaCustomerBusinessMapper.getCustomerBusiness(user.getClient(), dto.getDcCode(), customer.getCusSelfCode());
            if (!ObjectUtils.isEmpty(customerBusiness)) {
                throw new CustomResultException(MessageFormat.format("客户编码[{0}]已经被使用", customer.getCusSelfCode()));
            }
            GaiaCustomerGspinfo gaiaCustomerGspinfo = gaiaCustomerGspinfoMapper.getCustomerGspinfo(user.getClient(), dto.getDcCode(), customer.getCusSelfCode());
            if (!ObjectUtils.isEmpty(gaiaCustomerGspinfo)) {
                throw new CustomResultException(MessageFormat.format("客户编码[{0}]已经被使用", customer.getCusSelfCode()));
            }
            customer.setCusSite(dto.getDcCode());
            saveCus(customer, user.getClient(), flowNo, dto.getDcCode());
            // 特殊经营范围
            businessScopeService.saveJyfwGspinfoData(user.getClient(), customer.getCusSelfCode(), flowNo, customer.getJyfwList());
        }

        // 提交后自动新建工作流，流程节点根据人员所属门店号，从权限表取出对应质量负责人、企业负责人。审批流程：提交人（已提交）->质量->企业负责人->END
        FeignResult result = feignService.createCustomerWorkflow2(cusBaseList, flowNo);
        if (result.getCode() != 0) {
            throw new CustomResultException(result.getMessage());
        }
    }

    private void saveCus(GaiaCustomerGspinfo gspinfo, String client, String flowNo, String dcCode) {
        gspinfo.setClient(client);
        gspinfo.setCusGspStatus(CommonEnum.GspinfoStauts.APPROVAL.getCode()); //审批状态 0-审批中
        gspinfo.setCusFlowNo(flowNo);
        // 客户状态
        if (StringUtils.isNotBlank(gspinfo.getCusStatus())) {
            gspinfo.setCusStatus(CommonEnum.CustomerStauts.CUSTOMERNORMAL.getCode());
        }
        // 客户助记码
        if (StringUtils.isNotBlank(gspinfo.getCusName())) {
            gspinfo.setCusPym(StringUtils.ToFirstChar(gspinfo.getCusName()));
        }
        if (StringUtils.isBlank(gspinfo.getCusCode())) {
            gspinfo.setCusSite(dcCode);
        }
        if (StringUtils.isBlank(gspinfo.getCusGspDate())) {
            gspinfo.setCusGspDate(DateUtils.getCurrentDateStrYYMMDD());
        }
        gaiaCustomerGspinfoMapper.insertSelective(gspinfo);
        // 首营日志
        GaiaCustomerGspinfoLog gaiaCustomerGspinfoLogOld = new GaiaCustomerGspinfoLog();
        BeanUtils.copyProperties(gspinfo, gaiaCustomerGspinfoLogOld);
        // 加盟商
        gaiaCustomerGspinfoLogOld.setClient(client);
        // 自编码
        gaiaCustomerGspinfoLogOld.setCusSelfCode(gspinfo.getCusSelfCode());
        // 地点
        gaiaCustomerGspinfoLogOld.setCusSite(gspinfo.getCusSite());
        gaiaCustomerGspinfoLogOld.setCusLine(1);
        gaiaCustomerGspinfoLogMapper.insertSelective(gaiaCustomerGspinfoLogOld);

        GaiaCustomerGspinfoLog gaiaCustomerGspinfoLogNew = new GaiaCustomerGspinfoLog();
        BeanUtils.copyProperties(gspinfo, gaiaCustomerGspinfoLogNew);
        gaiaCustomerGspinfoLogOld.setCusLine(2);
        gaiaCustomerGspinfoLogMapper.insertSelective(gaiaCustomerGspinfoLogNew);

    }

    private void checkParam(List<SaveCusList1RequestDTO> list) {
        list.forEach(item -> {
            if (StringUtils.isEmpty(item.getCusName())) {
                throw new CustomResultException("客户名称不能为空");
            }
            if (StringUtils.isEmpty(item.getCusSelfCode())) {
                throw new CustomResultException("客户编码不能为空");
            }
        });
    }


    /**
     * 客户列表
     */
    @Override
    public PageInfo<GaiaCustomerBusinessDTO> getCusList(GetCusListRequestDTO dto) {
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        dto.setClient(client);

        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        List<GaiaCustomerBusinessDTO> cusList = gaiaCustomerBusinessMapper.getCusList(dto);
        // 拼接 唯一主键（前端使用）
        cusList.forEach(item -> item.setId(item.getClient() + item.getCusSite() + item.getCusSelfCode()));
        // 分页信息
        PageInfo<GaiaCustomerBusinessDTO> pageInfo = new PageInfo<>(cusList);
        return pageInfo;
    }

    /**
     * 导出
     */
    @Override
    public Result exportCusList(GetCusListRequestDTO dto) {
        dto.setPageNum(0);
        dto.setPageSize(0);
        PageInfo<GaiaCustomerBusinessDTO> cusPage = getCusList(dto);
        List<GaiaUserData> userDataList = gaiaUserDataMapper.selectByClient(feignService.getLoginInfo().getClient());

        List<GaiaCustomerBusinessDTO> list = cusPage.getList();
        try {
            List<List<String>> dataList = new ArrayList<>();
            for (GaiaCustomerBusinessDTO redto : list) {
                // 打印行
                List<String> item = new ArrayList<>();
                dataList.add(item);
                // 加盟商
                item.add(redto.getClient());
                // 机构
                item.add(redto.getCusSite());
                // 客户编码
                item.add(redto.getCusSelfCode());
                // 客户名称
                item.add(redto.getCusName());
                // 助记码
                item.add(redto.getCusPym());
                // 统一社会信用代码
                item.add(redto.getCusCreditCode());
                // 营业期限
                item.add(redto.getCusCreditDate());
                // 客户分类
                item.add(EnumUtils.EnumToMap(CommonEnum.CusClass.class).get(redto.getCusClass()));
                // 禁销
                initDictionaryStaticData(item, redto.getCusNoSale(), CommonEnum.DictionaryStaticData.NO_YES);
                // 禁退
                initDictionaryStaticData(item, redto.getCusNoReturn(), CommonEnum.DictionaryStaticData.NO_YES);
                // 收款条款
                item.add(redto.getCusPayTerm());
                // 客户分类
                String cusClassName = CommonEnum.CusClass.getEnumLabel(redto.getCusClass());
                item.add(cusClassName);
                // 法人
                item.add(redto.getCusLegalPerson());
                // 注册地址
                item.add(redto.getCusRegAdd());
                // 客户状态
                initDictionaryStaticData(item, redto.getCusStatus(), CommonEnum.DictionaryStaticData.CUS_STATUS);
                // 生产或经营范围
                item.add(redto.getCusScope());
                // 业务联系人
                item.add(redto.getCusBussinessContact());
                // 联系人电话
                item.add(redto.getCusContactTel());
                // 质量负责人
                item.add(redto.getCusQua());
                // 收货地址
                item.add(redto.getCusDeliveryAdd());
                // 许可证编号
                item.add(redto.getCusLicenceNo());
                // 发证日期
                item.add(redto.getCusLicenceDate());
                // 有效期至
                item.add(redto.getCusLicenceValid());
                // 是否启用信用管理
                initDictionaryStaticData(item, redto.getCusCreditFlag(), CommonEnum.DictionaryStaticData.NO_YES);
                // 信用额度
                item.add(redto.getCusCreditQuota() == null ? "" : redto.getCusCreditQuota().stripTrailingZeros().toPlainString());
                // 信用检查点
                item.add(redto.getCusCreditCheck());
                // 银行代码
                item.add(redto.getCusBankCode());
                // 银行名称
                item.add(redto.getCusBankName());
                // 账户持有人
                item.add(redto.getCusAccountPerson());
                // 银行账号
                item.add(redto.getCusBankAccount());
                // 支付方式
                item.add(EnumUtils.EnumToMap(CommonEnum.CusPayMode.class).get(redto.getCusPayMode()));
                // 铺底授信额度
                item.add(ObjectUtils.isEmpty(redto.getCusCreditQuota()) ? null : redto.getCusCreditQuota().toString());
                // 质量负责人
                item.add(redto.getCusQua());
                // 法人委托书图片
                item.add(redto.getCusPictureFrwts());
                // GSP证书图片
                item.add(redto.getCusPictureGsp());
                // GMP证书图片
                item.add(redto.getCusPictureGmp());
                // 随货同行单图片
                item.add(redto.getCusPictureShtxd());
                // 其他图片
                item.add(redto.getCusPictureQt());
                // 食品许可证书图片
                item.add(redto.getCusPictureSpxkz());
                // 食品许可证书
                item.add(redto.getCusSpxkz());
                // 食品许可证有效期
                item.add(redto.getCusSpxkzDate());
                // 医疗器械经营许可证图片
                item.add(redto.getCusPictureYlqxxkz());
                // 医疗器械经营许可证书
                item.add(redto.getCusYlqxxkz());
                // 医疗器械经营许可证有效期
                item.add(redto.getCusYlqxxkzDate());
                // GMP证书
                item.add(redto.getCusGmp());
                // GMP有效期
                item.add(redto.getCusGmpDate());
                // 医疗机构执业许可证图片
                item.add(redto.getCusPictureYljgxkz());
                // 医疗机构执业许可证书
                item.add(redto.getCusYljgxkz());
                // 医疗机构执业许可证书有效期
                item.add(redto.getCusYljgxkzDate());
                // GSP证书
                item.add(redto.getCusGsp());
                // GSP有效期
                item.add(redto.getCusGspDates());
                // 创建日期
                item.add(redto.getCusCreateDate());
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("客户数据");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }
    }

    /**
     * 查询数据静态字典数据处理
     */
    private void initDictionaryStaticData(List<String> list, String value, CommonEnum.DictionaryStaticData dictionaryStaticData) {
        if (com.gov.purchase.utils.StringUtils.isBlank(value)) {
            list.add("");
        } else {
            Dictionary dictionary = CommonEnum.DictionaryStaticData.getDictionaryByValue(dictionaryStaticData, value);
            if (dictionary != null) {
                list.add(dictionary.getLabel());
            } else {
                list.add("");
            }
        }
    }

    private String[] headList = {
            "加盟商",
            "机构",
            "客户编码",
            "客户名称",
            "助记码",
            "统一社会信用代码",
            "营业期限",
            "客户分类",
            "禁销",
            "禁退",
            "销售收款条件",
            "客户分类",
            "法人",
            "注册地址",
            "客户状态",
            "生产或经营范围",
            "业务联系人",
            "联系人电话",
            "质量负责人",
            "收货地址",
            "许可证编号",
            "发证日期",
            "有效期至",
            "是否启用信用管理",
            "信用额度",
            "信用检查点",
            "银行代码",
            "银行名称",
            "账户持有人",
            "银行账号",
            "支付方式",
            "铺底授信额度",
            "质量负责人",
            "法人委托书图片",
            "GSP证书图片",
            "GMP证书图片",
            "随货同行单图片",
            "其他图片",
            "食品许可证书图片",
            "食品许可证书",
            "食品许可证有效期",
            "医疗器械经营许可证图片",
            "医疗器械经营许可证书",
            "医疗器械经营许可证有效期",
            "GMP证书",
            "GMP有效期",
            "医疗机构执业许可证图片",
            "医疗机构执业许可证书",
            "医疗机构执业许可证书有效期",
            "GSP证书",
            "GSP有效期",
            "创建日期",
    };


    /**
     * 批量编辑保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editCusList(List<GaiaCustomerBusiness> list, String remarks) {
        // 主键校验
        list.forEach(this::checkKey);

        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        String userId = user.getUserId();
        List<GaiaDcData> dcListToUpdate = gaiaProductBasicMapper.getDcListToUpdate(client, userId);
        // 工作流
        List<GaiaCustomerChange> changeList = new ArrayList<>();
        int i = (int) ((Math.random() * 4 + 1) * 100000);
        String flowNO = System.currentTimeMillis() + com.gov.purchase.utils.StringUtils.leftPad(String.valueOf(i), 6, "0");

        for (GaiaCustomerBusiness customerBusiness : list) {
            List<GaiaCustomerBusinessKey> customerKeyList = new ArrayList<>();
            // 页面修改的那条数据
            GaiaCustomerBusinessKey customerKey = new GaiaCustomerBusinessKey();
            customerKey.setClient(customerBusiness.getClient());
            customerKey.setCusSite(customerBusiness.getCusSite());
            customerKey.setCusSelfCode(customerBusiness.getCusSelfCode());
            customerKeyList.add(customerKey);
            //如果账期类型为0-账期天数，忽略欠款标志更新
            if("0".equals(customerBusiness.getCusZqlx())){
                customerBusiness.setCusArrearsFlag(null);
            }
            // 配送中心连锁总部下的门店 + 商品主数据下的门店
            List<String> stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(customerBusiness.getClient(), customerBusiness.getCusSite());
            stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                GaiaCustomerBusinessKey supKey = new GaiaCustomerBusinessKey();
                supKey.setClient(customerBusiness.getClient());
                supKey.setCusSite(stoCode);
                supKey.setCusSelfCode(customerBusiness.getCusSelfCode());
                customerKeyList.add(supKey);
            });

            // 如果要发起工作流
            String dcXgsfwf = dcListToUpdate.stream().filter(Objects::nonNull)
                    .filter(item -> item.getDcCode().equals(customerBusiness.getCusSite()))
                    .findFirst().orElse(new GaiaDcData()).getDcXgsfwf();
            if (CommonEnum.NoYesStatus.YES.getCode().equals(dcXgsfwf)) {
                // 查询更新前的数据
                List<GaiaCustomerBusiness> cusOriginalProList = gaiaCustomerBusinessMapper.selectCusListByKeyList(customerKeyList);
                // 比对以及发起工作流
                changeList.addAll(customerDiff(cusOriginalProList, customerBusiness, userId, flowNO, remarks));
            } else {
                // 查询更新前的数据
                List<GaiaCustomerBusiness> cusOriginalProList = gaiaCustomerBusinessMapper.selectCusListByKeyList(customerKeyList);
                // 比对以及发起工作流
                changeList.addAll(customerDiff(cusOriginalProList, customerBusiness, userId, null, remarks));
                // 直接更新
                gaiaCustomerBusinessMapper.updateBatch(customerBusiness, customerKeyList);
            }
        }
        // 发起工作流
        if (!CollectionUtils.isEmpty(changeList)) {
            gaiaCustomerChangeMapper.saveBatch(changeList);
            // 地点为DC的发起工作流
            List<String> dcCodeList = dcListToUpdate.stream().filter(Objects::nonNull).map(GaiaDcData::getDcCode).collect(Collectors.toList());
            List<GaiaCustomerChange> changeListToCreateFlow = changeList.stream().filter(Objects::nonNull)
                    .filter(item -> dcCodeList.contains(item.getCusSite()) && StringUtils.isNotBlank(item.getCusFlowNo())).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(changeListToCreateFlow)) {
                FeignResult result = feignService.createEditCustomerWorkflow(changeListToCreateFlow, flowNO);
                if (result.getCode() != 0) {
                    throw new CustomResultException(result.getMessage());
                }
            }
        }
    }


    private List<GaiaCustomerChange> customerDiff(List<GaiaCustomerBusiness> customerFromList, GaiaCustomerBusiness customerTo, String userId, String flowNO, String remarks) {
        return customerFromList.stream().flatMap(customerFrom -> {
            List<DiffEntityReuslt> diffResultList = diffEntityUtils.diff(customerFrom, customerTo, diffEntityConfig.getCustomerDiffPropertyList());
            return diffResultList.stream().map(item -> {
                GaiaCustomerChange customerChange = new GaiaCustomerChange();
                customerChange.setClient(customerFrom.getClient());
                customerChange.setCusSite(customerFrom.getCusSite());
                customerChange.setCusSelfCode(customerFrom.getCusSelfCode());
                customerChange.setCusName(customerFrom.getCusName());
                customerChange.setCusCreditCode(customerFrom.getCusCreditCode());
                customerChange.setCusChangeField(item.getPropertyFrom());
                customerChange.setCusChangeColumn(item.getColumn());
                customerChange.setCusChangeColumnDes(item.getColumnDes());
                customerChange.setCusChangeFrom(item.getValueFrom());
                customerChange.setCusChangeTo(item.getValueTo());
                customerChange.setCusChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                customerChange.setCusChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                customerChange.setCusChangeUser(userId);
                customerChange.setCusFlowNo(flowNO);
                customerChange.setCusSfsp("0");
                customerChange.setCusSfty("0");
                customerChange.setCusRemarks(remarks);
                return customerChange;
            });
        }).collect(Collectors.toList());
    }

    /**
     * 详情
     */
    @Override
    public GetCusDetailsDTO getCusDetails(GaiaCustomerBusinessKey key) {
        checkKey(key);
        GaiaCustomerBusiness business = gaiaCustomerBusinessMapper.selectByPrimaryKey(key);
        if (!ObjectUtils.isEmpty(business)) {
            GetCusDetailsDTO dto = new GetCusDetailsDTO();
            BeanUtils.copyProperties(business, dto);
//            dto.setCusPictureFrwtsUrl(cosUtils.urlAuth(business.getCusPictureFrwts()));
//            dto.setCusPictureGmpUrl(cosUtils.urlAuth(business.getCusPictureGmp()));
//            dto.setCusPictureGspUrl(cosUtils.urlAuth(business.getCusPictureGsp()));
//            dto.setCusPictureQtUrl(cosUtils.urlAuth(business.getCusPictureQt()));
//            dto.setCusPictureShtxdUrl(cosUtils.urlAuth(business.getCusPictureShtxd()));
//            dto.setCusPictureSpxkzUrl(cosUtils.urlAuth(business.getCusPictureSpxkz()));
//            dto.setCusPictureYlqxxkzUrl(cosUtils.urlAuth(business.getCusPictureYlqxxkz()));
//            dto.setCusYlqxxkzDateUrl(cosUtils.urlAuth(business.getCusYlqxxkzDate()));
//            dto.setCusPictureYljgxkzUrl(cosUtils.urlAuth(business.getCusPictureYljgxkz()));
            return dto;
        }
        return null;
    }

    /**
     * 单条数据编辑保存
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editCus(GaiaCustomerBusiness business, String remarks) {
        String client = feignService.getLoginInfo().getClient();
        business.setClient(client);
        checkKey(business);
        editCusList(Collections.singletonList(business), remarks);
    }

    private void checkKey(GaiaCustomerBusinessKey key) {
        if (StringUtils.isEmpty(key.getClient())) {
            throw new CustomResultException("加盟商不能为空");
        }
        if (StringUtils.isEmpty(key.getCusSite())) {
            throw new CustomResultException("地点不能为空");
        }
        if (StringUtils.isEmpty(key.getCusSelfCode())) {
            throw new CustomResultException("自编码不能为空");
        }

    }

    /**
     * 客户经营范围编辑
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void editCusScope(EditScopeVO vo) {

        String client = feignService.getLoginInfo().getClient();
        String userId = feignService.getLoginInfo().getUserId();

        // 修改后的值
        List<String> jyfwIdAfterList = vo.getScopeList().stream().filter(Objects::nonNull).filter(scope -> scope.getChecked() == 1).map(EditScopeVO.Scope::getJyfwId).sorted().collect(Collectors.toList());
        String jyfwIdAfterStr = String.join(",", jyfwIdAfterList);

        // 发起工作流编码
        String flowNO = System.currentTimeMillis() + StringUtils.leftPad(String.valueOf((int) ((Math.random() * 4 + 1) * 100000)), 6, "0");

        // 地点集合
        List<GaiaCustomerBusinessKey> customerKeyList = new ArrayList<>();
        GaiaCustomerBusinessKey cusKey = new GaiaCustomerBusinessKey();
        cusKey.setClient(client);
        cusKey.setCusSite(vo.getJyfwSite());
        cusKey.setCusSelfCode(vo.getJyfwOrgid());
        customerKeyList.add(cusKey);

        // # 配送中心连锁总部下的门店 + 商品主数据下的门店
        List<String> stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(client, vo.getJyfwSite());
        stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
            GaiaCustomerBusinessKey customerKey = new GaiaCustomerBusinessKey();
            customerKey.setClient(client);
            customerKey.setCusSite(stoCode);
            customerKey.setCusSelfCode(vo.getJyfwOrgid());
            customerKeyList.add(customerKey);
        });

        // 修改记录
        List<GaiaCustomerChange> changeList = new ArrayList<>();
        // 客户基础数据
        List<GaiaCustomerBusiness> cusList = gaiaCustomerBusinessMapper.selectCusListByKeyList(customerKeyList);
        cusList.forEach(cus -> {
            // 修改前的值
            List<GetScopeListDTO> scopeList = businessScopeService.getScopeList(vo.getJyfwOrgid(), vo.getJyfwOrgname(), vo.getJyfwSite(),
                    CommonEnum.JyfwType.CUSTOMER.getCode(), "1,3")
                    .stream().filter(item -> item.getChecked() == 1).collect(Collectors.toList());

            String jyfwIdBeforeStr = scopeList.stream().filter(Objects::nonNull)
                    .map(GetScopeListDTO::getJyfwId)
                    .sorted()
                    .collect(Collectors.joining(","));

            changeList.add(this.saveEditCusScopeRecord(client, userId, cus.getCusSite(), cus.getCusSelfCode(), cus.getCusName(), cus.getCusCreditCode(),
                    jyfwIdBeforeStr, jyfwIdAfterStr, flowNO));
        });

        // 发起工作流的条件
        String dcXgsfwf = gaiaProductBasicMapper.getDcListToUpdate(client, userId).stream().filter(Objects::nonNull)
                .filter(item -> item.getDcCode().equals(vo.getJyfwSite()))
                .findFirst().orElse(new GaiaDcData()).getDcXgsfwf();

        if (CommonEnum.NoYesStatus.YES.getCode().equals(dcXgsfwf)) {
            gaiaCustomerChangeMapper.saveBatch(changeList);
            // 发起工作流
            List<GaiaCustomerChange> collect = changeList.stream().filter(item -> item.getCusSite().equals(vo.getJyfwSite())).collect(Collectors.toList());
            // 翻译
            collect.forEach(change -> {
                String cusChangeFromTranslation = String.join(",", businessScopeServiceMapper.getJyfwNameByjyfwIdList(change.getCusChangeFrom()));
                String cusChangeToTranslation = String.join(",", businessScopeServiceMapper.getJyfwNameByjyfwIdList(change.getCusChangeTo()));
                change.setCusChangeFrom(cusChangeFromTranslation);
                change.setCusChangeTo(cusChangeToTranslation);
            });
            FeignResult result = feignService.createEditCustomerWorkflow(collect, flowNO);
            if (result.getCode() != 0) {
                throw new CustomResultException(result.getMessage());
            }
        } else {
            // 不发起工作流
            changeList.forEach(change -> {
                EditScopeVO editScopeVO = new EditScopeVO();
                BeanUtils.copyProperties(vo, editScopeVO);
                editScopeVO.setJyfwSite(change.getCusSite());
                editScopeVO.setJyfwType(CommonEnum.JyfwType.CUSTOMER.getCode());
                businessScopeService.editScope(editScopeVO);
            });
        }
    }

    /**
     * 客户经营范围编辑_保存记录
     */
    private GaiaCustomerChange saveEditCusScopeRecord(String client, String userId, String site, String cusSelfCode, String cusName, String cusCreditCode,
                                                      String cusChangeFrom, String cusChangeTo, String flowNO) {
        GaiaCustomerChange customerChange = new GaiaCustomerChange();
        customerChange.setClient(client);
        customerChange.setCusSite(site);
        customerChange.setCusSelfCode(cusSelfCode);
        customerChange.setCusName(cusName);
        customerChange.setCusCreditCode(cusCreditCode);
        customerChange.setCusChangeField(CommonEnum.JyfwEditFlowField.CUSTOMER.getCode());
        customerChange.setCusChangeColumn(CommonEnum.JyfwEditFlowField.CUSTOMER.getCode());
        customerChange.setCusChangeColumnDes("客户经营范围");
        customerChange.setCusChangeFrom(cusChangeFrom);
        customerChange.setCusChangeTo(cusChangeTo);
        customerChange.setCusChangeDate(DateUtils.getCurrentDateStrYYMMDD());
        customerChange.setCusChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
        customerChange.setCusChangeUser(userId);
        customerChange.setCusFlowNo(flowNO);
        customerChange.setCusSfsp("0");
        customerChange.setCusSfty("0");
        customerChange.setCusRemarks("客户经营范围");
        return customerChange;
    }


}
