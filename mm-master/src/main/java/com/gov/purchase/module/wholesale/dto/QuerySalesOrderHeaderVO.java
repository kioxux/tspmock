package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.common.entity.Pageable;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author zhoushuai
 * @date 2021/4/1 13:57
 */
@Data
@EqualsAndHashCode
public class QuerySalesOrderHeaderVO extends Pageable {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client")
    @NotBlank(message = "加盟商不能为空")
    private String client;
    /**
     * 销售订单号
     */
    private String soId;
    /**
     * 销售订单号 更多
     */
    private List<String> soIds;
    /**
     * 销售主体（DC编号）
     */
    private String soCompanyCode;
    /**
     * 订单日期开始
     */
    private String soDateStart;
    /**
     * 订单日期结束
     */
    private String soDateEnd;
    /**
     * 订单类型
     */
    private String soType;
    /**
     * 订单状态(1-全部、2-未发货、3-已发货、4-已发货未开票、5-已发货已开票；默认选择1-全部；)
     */
    private String soApproveStatus;
    /**
     * 客户自编码
     */
    private String soCustomerId;
    /**
     * 客户自编码
     */
    private List<String> soCustomerIds;
    /**
     * 创建人
     */
    private String createName;

    /**
     * 备注
     */
    private String soHeadRemark;

}
