package com.gov.purchase.module.customer.dto;

import com.gov.purchase.entity.GaiaCustomerGspinfo;
import com.gov.purchase.module.base.dto.JyfwDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode
public class SaveCusList1RequestDTO extends GaiaCustomerGspinfo{

        @NotBlank(message = "地点不能为空")
        private String dcCode;

        private List<JyfwDTO> jyfwList;
}
