package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@ApiModel(value = "采购退货传入参数")
public class PurchaseReturnsRequestDto {

    /**
     * 加盟商
     */
    @ApiModelProperty(value = "加盟商", name = "client", required = true)
    @NotBlank(message = "加盟商不能为空")
    private String client;

    /**
     * 地点
     */
    @ApiModelProperty(value = "供应商自编码", name = "poSupplierId", required = true)
    @NotBlank(message = "供应商自编码不能为空")
    private String poSupplierId;

    /**
     * 退货库位
     */
    @ApiModelProperty(value = "采购主体", name = "poCompanyCode", required = true)
    @NotBlank(message = "采购主体不能为空")
    private String poCompanyCode;

    /**
     * 商品编码
     */
    @ApiModelProperty(value = "主体类型", name = "poSubjectType", required = true)
    @NotBlank(message = "主体类型不能为空")
    private String poSubjectType;

    /**
     * 退货参数
     */
    private List<ReturnDto> returnDto;
}