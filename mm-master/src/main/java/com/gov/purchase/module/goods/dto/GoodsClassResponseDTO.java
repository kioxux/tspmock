package com.gov.purchase.module.goods.dto;

import com.gov.purchase.entity.GaiaProductClass;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode
public class GoodsClassResponseDTO extends GaiaProductClass {

    private String proBigClassCode;
    private String proBigClassName;

    private String proMidClassCode;
    private String proMidClassName;

    /**
     * 下一级列表
     */
    private List<GoodsClassResponseDTO> list ;


}
