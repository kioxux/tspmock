package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "付款单明细返回参数")
public class PayOrderDetailsResponseDto {

    /**
     * PO类型编码
     */
    private String poType;

    /**
     * PO类型名
     */
    private String poTypeName;

    /**
     * 供应商编码
     */
    private String poSupplierId;

    /**
     * 供应商名
     */
    private String poSupplierName;

    /**
     * 发票号码
     */
    private String invoiceNum;

    /**
     * 发票项目
     */
    private String ivLineNo;

    /**
     * 采购订单号
     */
    private String matPoId;

    /**
     * 采购订单行号
     */
    private String poLineNo;

    /**
     * 商品自编码
     */
    private String proCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 含税单价
     */
    private BigDecimal poPrice;

    /**
     * 含税总价
     */
    private BigDecimal totalAmt;

    /**
     * 入库单号
     */
    private String matId;

    /**
     * 入库单项目
     */
    private String matLineNo;

    /**
     * 入库数量
     */
    private String matQty;

    /**
     * 入库日期
     */
    private String matPostDate;

    /**
     * 应付金额
     */
    private BigDecimal payableAmt;

    /**
     * 已预付金额
     */
    private BigDecimal prepaymentAmt;

    /**
     * 已付金额
     */
    private BigDecimal paidAmt;

    /**
     * 未付金额
     */
    private BigDecimal unpaidAmt;

    /**
     * 本次付款金额
     */
    private BigDecimal paymentAmt;

    /**
     * 剩余应付金额
     */
    private BigDecimal lastPayableAmt;

    /**
     * 尾差调整
     */
    private String spread;
}