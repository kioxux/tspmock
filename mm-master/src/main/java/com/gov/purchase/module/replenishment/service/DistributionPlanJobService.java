package com.gov.purchase.module.replenishment.service;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/7/29 14:23
 **/
public interface DistributionPlanJobService {

    void autoExecPlan(String params);

}
