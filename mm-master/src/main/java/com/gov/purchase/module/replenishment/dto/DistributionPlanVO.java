package com.gov.purchase.module.replenishment.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;

/***
 * @desc:
 * @author: ryan
 * @createTime: 2021/6/4 10:24
 **/
@Data
@ApiModel
public class DistributionPlanVO {
    @ApiModelProperty(value = "序号", example = "1")
    private Integer index;
    @ApiModelProperty(value = "铺货计划单号", example = "ndp202100011")
    private String planCode;
    @ApiModelProperty(value = "计划时间", example = "2021/03/04 12:23:00")
    private String createTime;
    @ApiModelProperty(value = "计划完成时间", example = "2021/03/08 12:23:00")
    private String finishTime;
    @ApiModelProperty(value = "计划铺货数量", example = "100")
    private BigDecimal planQuantity;
    @ApiModelProperty(value = "实际铺货数量", example = "100")
    private BigDecimal actualQuantity;
    @ApiModelProperty(value = "计划铺货门店", example = "10")
    private Integer planStoreNum;
    @ApiModelProperty(value = "实际铺货门店", example = "10")
    private Integer actualStoreNum;
    @ApiModelProperty(value = "计划状态:1-待调用 2-调用中 3-已完成 4-已过期 5-已取消 6-待补铺", example = "1")
    private Integer status;
}
