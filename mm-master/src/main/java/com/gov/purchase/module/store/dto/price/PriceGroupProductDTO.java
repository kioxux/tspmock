package com.gov.purchase.module.store.dto.price;

import lombok.Data;

import java.util.List;

/**
 * @description: 价格组商品明细表
 * @author: yzf
 * @create: 2021-12-16 17:24
 */
@Data
public class PriceGroupProductDTO {
    /**
     * 加盟商
     */
    private String client;

    /**
     * 价格组
     */
    private String prcGroupId;

    /**
     * 价格组名称
     */
    private String prcGroupName;

    /**
     * 商品
     */
    private List<PriceGroupProductVO> pros;

    /**
     * 门店列表
     */
    private List<String> storeIds;
}
