package com.gov.purchase.module.base.dto;

import com.gov.purchase.entity.GaiaSdMemberBasic;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class MemberDTO extends GaiaSdMemberBasic {

    /**
     * 所属店号
     */
    private String gmbBrId;
    /**
     * 当前积分
     */
    private String gmbIntegral;
    /**
     * 生日
     */
    private String gmbBirth;
    /**
     * 年龄
     */
    private String gmbAge;
    /**
     * 性别
     */
    private String gmbSex;
    /**
     * 手机
     */
    private String gmbMobile;

    /**
     * 会员姓名
     */
    private String gmbName;
    /**
     * 会员卡号
     */
    private String gmbCardId;

    /**
     * 所属门店简称
     */
    private String gmbBrShortName;

    /**
     * 所属门店名称
     */
    private String gmbBrName;

    /**
     * 会员ID
     */
    private String gsmbMemberId;

    /**
     * 最后销售门店
     */
    private String lastSaleStore;

    /**
     * 最后消费日期
     */
    private String gsshDate;

    /**
     * 最后消费时间
     */
    private String gsshTime;

    /**
     * 最后积分日期
     */
    private String gsmbcIntegralLastdate;

    /**
     * 卡类型
     */
    private String gsmbcClassId;

    /**
     * 是否发送短信 0:没有发送短信， 1:已经发送短信
     */
    private Integer hasSendSms;
}
