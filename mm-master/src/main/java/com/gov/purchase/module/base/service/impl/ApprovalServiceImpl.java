package com.gov.purchase.module.base.service.impl;

import com.gov.purchase.common.diff.DiffEntityConfig;
import com.gov.purchase.common.diff.DiffEntityUtils;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.common.validate.ValidateUtil;
import com.gov.purchase.common.validate.ValidationResult;
import com.gov.purchase.constants.ApprovalEnum;
import com.gov.purchase.constants.CommonConstants;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ProcessEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.OperateFeign;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.GaiaSdProductPriceDto;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.base.dto.feign.ApprovalInfo;
import com.gov.purchase.module.base.service.ApprovalService;
import com.gov.purchase.module.base.service.BaseService;
import com.gov.purchase.module.base.service.BusinessScopeService;
import com.gov.purchase.module.base.service.MaterialDocService;
import com.gov.purchase.module.customer.dto.GaiaCustomerChangeKey;
import com.gov.purchase.module.goods.dto.GaiaProductChangeKey;
import com.gov.purchase.module.goods.dto.JyfwooData;
import com.gov.purchase.module.goods.dto.ProductPositionExcludeDTO;
import com.gov.purchase.module.replenishment.dto.StoreRequestDto;
import com.gov.purchase.module.store.dto.price.GaiaPriceAdjustDto;
import com.gov.purchase.module.supplier.dto.GaiaSupplierChangeKey;
import com.gov.purchase.module.supplier.service.OptimizeSupplierService;
import com.gov.purchase.module.wholesale.dto.GaiaBillOfApVO;
import com.gov.purchase.module.wholesale.dto.SoItem;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.beans.FeatureDescriptor;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class ApprovalServiceImpl implements ApprovalService {
    @Resource
    private GaiaProductBasicMapper gaiaProductBasicMapper;
    @Resource
    private GaiaProductGspinfoMapper gaiaProductGspinfoMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    private GaiaSupplierGspinfoMapper gaiaSupplierGspinfoMapper;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    BaseService baseService;
    @Resource
    private GaiaCustomerGspinfoMapper gaiaCustomerGspinfoMapper;
    @Resource
    private GaiaCustomerBusinessMapper gaiaCustomerBusinessMapper;
    @Resource
    private GaiaPoHeaderMapper gaiaPoHeaderMapper;
    @Resource
    private GaiaPayOrderMapper gaiaPayOrderMapper;
    @Resource
    private GaiaDcDataMapper gaiaDcDataMapper;
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private GaiaTaxCodeMapper gaiaTaxCodeMapper;
    @Resource
    private GaiaSmsTemplateMapper gaiaSmsTemplateMapper;
    @Resource
    private GaiaProductChangeMapper gaiaProductChangeMapper;
    @Resource
    private GaiaSdProductPriceMapper gaiaSdProductPriceMapper;
    @Resource
    private DictionaryMapper dictionaryMapper;
    @Resource
    private OperateFeign operateFeign;
    @Resource
    GaiaBatchInfoMapper gaiaBatchInfoMapper;
    @Resource
    private GaiaPriceAdjustMapper gaiaPriceAdjustMapper;
    @Resource
    private OperationFeignService operationFeignService;
    @Resource
    private BusinessScopeService businessScopeService;
    @Resource
    private BusinessScopeServiceMapper businessScopeServiceMapper;
    @Resource
    private DiffEntityConfig diffEntityConfig;
    @Resource
    private DiffEntityUtils diffEntityUtils;
    @Resource
    private GaiaCustomerChangeMapper gaiaCustomerChangeMapper;
    @Resource
    private GaiaSupplierChangeMapper gaiaSupplierChangeMapper;
    @Resource
    private GaiaSupplierSalesmanMapper gaiaSupplierSalesmanMapper;
    @Resource
    private OptimizeSupplierService optimizeSupplierService;
    @Resource
    private GaiaNewDistributionPlanMapper gaiaNewDistributionPlanMapper;
    @Resource
    private GaiaSdWtproMapper gaiaSdWtproMapper;
    @Resource
    private GaiaSdReplenishHMapper gaiaSdReplenishHMapper;
    @Autowired
    private GaiaMaterialDocMapper materialDocMapper;
    @Autowired
    private MaterialDocService materialDocService;
    @Resource
    private GaiaBillOfArMapper gaiaBillOfArMapper;

    @Override
    @Transactional
    public FeignResult approvalResultProcess(ApprovalInfo info) {
        FeignResult feignResult = new FeignResult();
        log.info("接收到参数：{}", JsonUtils.beanToJson(info));
        /**
         * 数据验证
         */
        ValidationResult result = ValidateUtil.validateEntity(info);
        if (result.isHasErrors()) {
            feignResult.setCode(1001);
            feignResult.setMessage(result.getMessage());
            return feignResult;
        }

        ProcessEnum processEnum = ProcessEnum.getEnumByCode(info.getWfDefineCode());
        if (processEnum == null) {
            feignResult.setCode(1002);
            feignResult.setMessage("流程类型不存在");
            return feignResult;
        }

        switch (processEnum) {
            case GAIA_WF_014:  // 商品首营流程
                feignResult = productHandle(info);
                break;
            case GAIA_WF_015:  // 供应商首营流程
                feignResult = supplierHandle(info);
                break;
            case GAIA_WF_016: // 批发客户首营流程
                feignResult = customerHandle(info);
                break;
            case GAIA_WF_017:  // 采购订单审批流程
                feignResult = purchaseHandle(info);
                break;
            case GAIA_WF_018:  // 采购付款申请审批流程
                feignResult = paymentHandle(info);
                break;
            case GAIA_WF_027:  // 短信模板审批流程
                feignResult = smsTemplateHandle(info);
                break;
            case GAIA_WF_029:  // 营销任务审批流程
                feignResult = marketingHandle(info);
                break;
            case GAIA_WF_031: // 薪酬核算审批流程
                feignResult = salary(info);
                break;
            case GAIA_WF_032: // 供应商付款审批流程
                feignResult = supplierPayment(info);
                break;
            case GAIA_WF_041: // 商品主数据修改审批流程
                feignResult = productEditHandle(info);
                break;
            case GAIA_WF_033: // 商品調價审批回调
                feignResult = productAdjustHandle(info);
                break;
            case GAIA_WF_044: // 补充首营审批回调
                feignResult = replenishProductGspHandle(info);
                break;
            case GAIA_WF_048: // 供应商修改审批回调
                feignResult = supplierEditHandle(info);
                break;
            case GAIA_WF_049: // 客户修改审批回调
                feignResult = customerEditHandle(info);
                break;
            default:
                break;
        }
        return feignResult;
    }

    /**
     * 供应商付款
     *
     * @param info
     * @return
     */
    private FeignResult supplierPayment(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        result.setCode(0);
        result.setMessage("执行成功");
        // 付款记录
        List<PaymentApplications> paymentApplicationsList = dictionaryMapper.selectPaymentApplications(info.getClientId(), info.getWfOrder());
        if (CollectionUtils.isEmpty(paymentApplicationsList)) {
            result.setCode(1001);
            result.setMessage("审批数据不存在");
            return result;
        }
        for (PaymentApplications paymentApplications : paymentApplicationsList) {
            if (paymentApplications.getApprovalStatus() == null || paymentApplications.getApprovalStatus().intValue() != 0) {  // 审批中的数据
                result.setCode(1001);
                result.setMessage("当前付款已审批");
                return result;
            }
        }
        Integer approvalStatus = 0;
        // 审批状态
        if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 已审批
            approvalStatus = 1;

        } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 审批拒绝
            approvalStatus = 2;
        }
        dictionaryMapper.paymentApplications(info.getClientId(), info.getWfOrder(), approvalStatus);
        //暂时注释
        /*if(Objects.equals(1,approvalStatus)){
            //已审批 供应商应付表插入数据 GAIA_BILL_OF_AP
            insertBillOfAp(paymentApplicationsList);
        }*/

        return result;
    }


    /**
     * 已审批 供应商应付表插入数据 GAIA_BILL_OF_AP
     *
     * @param paymentApplicationsList 付款记录
     * @param
     */
    private void insertBillOfAp(List<PaymentApplications> paymentApplicationsList) {
        try {
            Map<String, List<PaymentApplications>> map = paymentApplicationsList.stream().collect(Collectors.groupingBy(s -> s.getClient() + "-" + s.getProSite() + "-" + s.getSupCode()));
            for (Map.Entry<String, List<PaymentApplications>> application : map.entrySet()) {
                List<PaymentApplications> applications = application.getValue();
                if (!CollectionUtils.isEmpty(applications)) {
                    PaymentApplications pay = applications.get(0);
                    //判断是否有期初记录
                    Integer count = gaiaBillOfArMapper.getBeginningOfPeriod(pay.getClient(), pay.getSupCode(), pay.getProSite());
                    if (Objects.nonNull(count) && count > 0) {
                        pay.setInvoiceAmountOfThisPayment(applications.stream().map(PaymentApplications::getInvoiceAmountOfThisPayment).reduce(BigDecimal.ZERO, BigDecimal::add));
                        GaiaBillOfApVO billOfApVO = new GaiaBillOfApVO();
                        billOfApVO.setClient(pay.getClient());
                        billOfApVO.setDcCode(pay.getProSite());
                        billOfApVO.setPaymentId("6666");
                        billOfApVO.setAmountOfPayment(BigDecimal.ZERO.subtract(pay.getInvoiceAmountOfThisPayment()));
                        billOfApVO.setAmountOfTax(BigDecimal.ZERO);
                        billOfApVO.setAmountExcludingTax(BigDecimal.ZERO);
                        //String nowDate = DateUtil.format(DateUtil.parse(DateUtil.today(), "yyyy-MM-dd"), "yyyyMMdd");
                        String nowDate = DateUtils.getCurrentDateStrYYMMDD();
                        billOfApVO.setPaymentDate(nowDate);
                        billOfApVO.setSupSelfCode(pay.getSupCode());
                        billOfApVO.setSupName(pay.getSupName());
                        materialDocMapper.insertBillOfAp(billOfApVO);
                        //修改供应商主业务
                        materialDocService.updateSupplierBusiness(pay.getClient(), pay.getSupCode(), pay.getProSite(), nowDate);
                    }
                }
            }
        } catch (Exception e) {
            //e.printStackTrace();
            log.info(String.format("<付款申请><新增供应商应付清单><报错：%s>", e.getMessage()));
        }
    }

    /**
     * 薪酬核算
     *
     * @param info
     * @return
     */
    private FeignResult salary(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        result.setCode(0);
        result.setMessage("执行成功");
        // 薪酬审批数据
        List<SalaryProgram> salaryProgramList = dictionaryMapper.selectSalary(info.getClientId(), info.getWfOrder());
        if (CollectionUtils.isEmpty(salaryProgramList)) {
            result.setCode(1001);
            result.setMessage("审批数据不存在");
            return result;
        }
        for (SalaryProgram salaryProgram : salaryProgramList) {
            if (salaryProgram.getGspApprovalStatus() == null || salaryProgram.getGspApprovalStatus().intValue() != 0) {  // 审批中的数据
                result.setCode(1001);
                result.setMessage("当前薪酬方案已审批");
                return result;
            }
        }
        SalaryProgram salaryProgram = new SalaryProgram();
        // 工作流编号
        salaryProgram.setGspFlowNo(info.getWfOrder());
        // 审批状态
        if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 已审批
            salaryProgram.setGspApprovalStatus(1);

        } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 审批拒绝
            salaryProgram.setGspApprovalStatus(2);
        }
        // 审批日期
        salaryProgram.setGspApprovalDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        // 审批时间
        salaryProgram.setGspApprovalTime(DateUtils.getCurrentTimeStr("HHmmss"));
        // 修改日期
        salaryProgram.setGspChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        // 修改时间
        salaryProgram.setGspChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
        dictionaryMapper.salaryApproval(salaryProgram);
        return result;
    }

    /**
     * 付款订单处理
     *
     * @param info
     * @return
     */
    private FeignResult paymentHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        GaiaPayOrder order = gaiaPayOrderMapper.selectByOrderId(info.getClientId(), info.getWfOrder());
        if (order == null) {
            result.setCode(1002);
            result.setMessage("付款单不存在");
            return result;
        }

        if (!"1".equals(order.getPayOrderStatus())) {  // 审批中的数据
            result.setCode(1003);
            result.setMessage("付款单已审批");
            return result;
        }

        if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 已审批
            order.setPayOrderStatus("3");

        } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 审批拒绝
            order.setPayOrderStatus("2");
        }
        order.setPayChangeDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
        order.setPayChangeTime(DateUtils.getCurrentTimeStr("HHmmss"));
        gaiaPayOrderMapper.updateByPrimaryKeySelective(order);

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 采购订单处理
     *
     * @param info
     * @return
     */
    private FeignResult purchaseHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        GaiaPoHeader header = gaiaPoHeaderMapper.selectByPoFlowNo(info.getClientId(), info.getWfOrder());
        if (header == null) {
            result.setCode(1002);
            result.setMessage("采购订单不存在");
            return result;
        }

        if (!CommonEnum.PoApproveStatus.APPROVAL.getCode().equals(header.getPoApproveStatus())) {  // 审批中的数据
            result.setCode(1003);
            result.setMessage("采购订单已审批");
            return result;
        }

        if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 已审批
            header.setPoApproveStatus(CommonEnum.PoApproveStatus.APPROVED.getCode());
            header.setPoUpdateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            header.setPoUpdateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            gaiaPoHeaderMapper.updateByPrimaryKeySelective(header);
        }
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 客户首营处理
     *
     * @param info
     * @return
     */
    private FeignResult customerHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        List<GaiaCustomerGspinfo> gspinfoList = gaiaCustomerGspinfoMapper.selectCustomerGspByFlowNo(info.getClientId(), info.getWfOrder());
        // 这里可能是有些问题的
        Map<String, List<GaiaJyfwDataGspinfoKey>> jyfwGspinfoDataMap = businessScopeServiceMapper.getJyfwGspinfoDataByFlowNo(info.getClientId(), info.getWfOrder())
                .stream().filter(Objects::nonNull)
                .collect(Collectors.groupingBy(GaiaJyfwDataGspinfoKey::getJyfwOrgid));
        List<JyfwooData> jyfwooDataList = businessScopeServiceMapper.getJyfwooDataList(null);
        if (CollectionUtils.isEmpty(gspinfoList)) {
            result.setCode(1002);
            result.setMessage("首营信息不存在");
            return result;
        }
        String status = gspinfoList.stream().filter(Objects::nonNull).map(GaiaCustomerGspinfo::getCusGspStatus).filter(Objects::nonNull).findFirst().orElse(null);
        if (!CommonEnum.GspinfoStauts.APPROVAL.getCode().equals(status)) {  // 审批中的数据
            result.setCode(1003);
            result.setMessage("首营信息已审批");
            return result;
        }
        gspinfoList.forEach(gspinfo -> {
            // 审批状态 = 审批通过
            if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
                GaiaCustomerBusiness business = new GaiaCustomerBusiness();
                BeanUtils.copyProperties(gspinfo, business);
                //供应商状态正常
                business.setCusStatus(StringUtils.isBlank(business.getCusStatus()) ? CommonEnum.GoodsStauts.GOODSNORMAL.getCode() : business.getCusStatus());
                // 禁止销售
                business.setCusNoSale(StringUtils.isBlank(business.getCusNoSale()) ? CommonEnum.NoYesStatus.NO.getCode() : business.getCusNoSale());
                // 禁止退货
                business.setCusNoReturn(StringUtils.isBlank(business.getCusNoReturn()) ? CommonEnum.NoYesStatus.NO.getCode() : business.getCusNoReturn());
                // 启用信用管理 - 否
                business.setCusCreditFlag(StringUtils.isBlank(business.getCusCreditFlag()) ? CommonEnum.NoYesStatus.NO.getCode() : business.getCusCreditFlag());
                // 匹配状态
                business.setCusMatchStatus(StringUtils.isBlank(business.getCusMatchStatus()) ? CommonEnum.MatchStatus.EXACTMATCH.getCode() : business.getCusMatchStatus());
                // 助计码
                business.setCusPym(StringUtils.isBlank(business.getCusPym()) ? StringUtils.ToFirstChar(business.getCusName()) : business.getCusPym());
                // 创建日期
                business.setCusCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                gaiaCustomerBusinessMapper.insertSelective(business);
                // 特殊经营范围
                businessScopeService.saveJyfwDataFromApproval(business.getClient(), null, business.getCusSelfCode(), business.getCusName(), business.getCusSite(),
                        CommonEnum.JyfwType.CUSTOMER.getCode(), jyfwGspinfoDataMap.get(business.getCusSelfCode()), jyfwooDataList);
                // 更新首营信息表
                gspinfo.setCusGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode()); // 已审批
                gaiaCustomerGspinfoMapper.updateByPrimaryKeySelective(gspinfo);
            } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
                // 更新首营信息表
                gspinfo.setCusGspStatus(CommonEnum.GspinfoStauts.ABANDONED.getCode()); // 已审批
                gaiaCustomerGspinfoMapper.updateByPrimaryKeySelective(gspinfo);
            }
        });
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 供应商首营处理
     *
     * @param info
     * @return
     */
    private FeignResult supplierHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        List<GaiaSupplierGspinfo> gspinfoList = gaiaSupplierGspinfoMapper.selectSupplierGspByFlowNo(info.getClientId(), info.getWfOrder());
        Map<String, List<GaiaJyfwDataGspinfoKey>> jyfwGspinfoDataMap = businessScopeServiceMapper.getJyfwGspinfoDataByFlowNo(info.getClientId(), info.getWfOrder())
                .stream().filter(Objects::nonNull)
                .collect(Collectors.groupingBy(GaiaJyfwDataGspinfoKey::getJyfwOrgid));
        List<JyfwooData> jyfwooDataList = businessScopeServiceMapper.getJyfwooDataList(null);
        if (CollectionUtils.isEmpty(gspinfoList)) {
            result.setCode(1002);
            result.setMessage("首营信息不存在");
            return result;
        }
        if (!CommonEnum.GspinfoStauts.APPROVAL.getCode().equals(gspinfoList.get(0).getSupGspStatus())) {  // 审批中的数据
            result.setCode(1003);
            result.setMessage("首营信息已审批");
            return result;
        }
        Map<String, List<String>> map = new HashMap<>();
        for (GaiaSupplierGspinfo gspinfo : gspinfoList) {
            // 审批状态 = 审批通过
            if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
                List<GaiaSupplierBusiness> supplierSyncList = new ArrayList<>();
                GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByDcCode(gspinfo.getClient(), gspinfo.getSupSite());
                if (gaiaDcData != null) {
                    // 业务员
                    List<GaiaSupplierSalesman> salesmanList = gaiaSupplierSalesmanMapper.getSupplierSalesmanListByFlowNo(gspinfo.getClient(), gspinfo.getSupSite(), gspinfo.getSupSelfCode(), gspinfo.getSupFlowNo());
                    salesmanList.forEach(item -> item.setId(null));
                    StoreRequestDto dto = new StoreRequestDto();
                    dto.setClient(gspinfo.getClient());
                    dto.setStoChainHead(gspinfo.getSupSite());
                    List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getStoreListByDc(dto);
                    for (GaiaStoreData storeData : storeDataList) {
                        GaiaSupplierBusiness business = new GaiaSupplierBusiness();
//                        if (StringUtils.isNotBlank(gspinfo.getSupCode())) {
//                            // 商品主数据
//                            GaiaSupplierBasic gaiaSupplierBasic = gaiaSupplierBasicMapper.selectByPrimaryKey(gspinfo.getSupCode());
//                            if (gaiaSupplierBasic != null) {
//                                // 商品主数据复制到业务表
//                                BeanUtils.copyProperties(gaiaSupplierBasic, business);
//                            }
//                        }
                        BeanUtils.copyProperties(gspinfo, business);
                        //供应商状态正常
                        business.setSupStatus(StringUtils.isNotBlank(business.getSupStatus()) ? business.getSupStatus() : CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
                        // 禁止采购
                        business.setSupNoPurchase(StringUtils.isNotBlank(business.getSupNoPurchase()) ? business.getSupNoPurchase() : CommonEnum.NoYesStatus.NO.getCode());
                        // 禁止退厂
                        business.setSupNoSupplier(StringUtils.isNotBlank(business.getSupNoSupplier()) ? business.getSupNoSupplier() : CommonEnum.NoYesStatus.NO.getCode());
                        // 匹配状态
                        business.setSupMatchStatus(StringUtils.isNotBlank(business.getSupMatchStatus()) ? business.getSupMatchStatus() : CommonEnum.MatchStatus.EXACTMATCH.getCode());
                        // 助计码
                        business.setSupPym(StringUtils.ToFirstChar(business.getSupName()));
                        business.setSupSite(storeData.getStoCode());
                        String key = storeData.getStoCode();
                        List<String> selfCodeList = map.get(key);
                        if (CollectionUtils.isEmpty(selfCodeList)) {
                            selfCodeList = new ArrayList<>();
                            selfCodeList.add(business.getSupSelfCode());
                            map.put(key, selfCodeList);
                        } else {
                            if (selfCodeList.contains(business.getSupSelfCode())) {
                                continue;
                            }
                            selfCodeList.add(business.getSupSelfCode());
                            map.put(key, selfCodeList);
                        }
                        GaiaSupplierBusiness businessExit = gaiaSupplierBusinessMapper.selectByPrimaryKey(business);
                        if (!ObjectUtils.isEmpty(businessExit)) {
                            continue;
                        }
                        // 创建日期
                        if (StringUtils.isBlank(business.getSupCreateDate())) {
                            business.setSupCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                        }
                        // GSP有效期
                        business.setSupGspDate(gspinfo.getSupGspDateValidity());
                        gaiaSupplierBusinessMapper.insertSelective(business);
                        // 特殊经营范围
                        businessScopeService.saveJyfwDataFromApproval(business.getClient(), business.getSupLoginUser(), business.getSupSelfCode(), business.getSupName(), business.getSupSite(),
                                CommonEnum.JyfwType.SUPPLIER.getCode(), jyfwGspinfoDataMap.get(business.getSupSelfCode()), jyfwooDataList);
                        // 业务员
                        optimizeSupplierService.saveSupplierSalesmanList(salesmanList, business.getClient(), business.getSupSite(), business.getSupSelfCode(), null);
                        // 第三方 gys-operation
                        supplierSyncList.add(business);
                    }
                    GaiaSupplierBusiness business = new GaiaSupplierBusiness();
                    // 配送中心首营
                    BeanUtils.copyProperties(gspinfo, business);
                    //供应商状态正常
                    business.setSupStatus(StringUtils.isNotBlank(business.getSupStatus()) ? business.getSupStatus() : CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
                    // 禁止采购
                    business.setSupNoPurchase(StringUtils.isNotBlank(business.getSupNoPurchase()) ? business.getSupNoPurchase() : CommonEnum.NoYesStatus.NO.getCode());
                    // 禁止退厂
                    business.setSupNoSupplier(StringUtils.isNotBlank(business.getSupNoSupplier()) ? business.getSupNoSupplier() : CommonEnum.NoYesStatus.NO.getCode());
                    // 匹配状态
                    business.setSupMatchStatus(StringUtils.isNotBlank(business.getSupMatchStatus()) ? business.getSupMatchStatus() : CommonEnum.MatchStatus.EXACTMATCH.getCode());
                    // 助计码
                    business.setSupPym(StringUtils.ToFirstChar(business.getSupName()));
                    business.setSupSite(gaiaDcData.getDcCode());
                    // 创建日期
                    if (StringUtils.isBlank(business.getSupCreateDate())) {
                        business.setSupCreateDate(DateUtils.getCurrentDateStrYYMMDD());
                    }
                    // GSP有效期
                    business.setSupGspDate(gspinfo.getSupGspDateValidity());
                    gaiaSupplierBusinessMapper.insertSelective(business);
                    // 特殊经营范围
                    businessScopeService.saveJyfwDataFromApproval(business.getClient(), business.getSupLoginUser(), business.getSupSelfCode(), business.getSupName(), business.getSupSite(),
                            CommonEnum.JyfwType.SUPPLIER.getCode(), jyfwGspinfoDataMap.get(business.getSupSelfCode()), jyfwooDataList);
                    // 第三方 gys-operation
                    supplierSyncList.add(business);
                }
                // 更新首营信息表
                gspinfo.setSupGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode()); // 已审批
                gaiaSupplierGspinfoMapper.updateByPrimaryKeySelective(gspinfo);
                try {
                    if (!CollectionUtils.isEmpty(supplierSyncList)) {
                        // 门店列表
                        List<String> stoCodeList = gaiaStoreDataMapper.getNotChainStoCodeList(info.getClientId());
                        Map<String, List<GaiaSupplierBusiness>> supplierMap = supplierSyncList.stream().filter(Objects::nonNull)
                                .collect(Collectors.groupingBy(GaiaSupplierBusiness::getSupSite));
                        supplierMap.forEach((supSite, supList) -> {
                            // 属性
                            String attribute = stoCodeList.contains(supSite) ? "1" : "2";
                            // 自编码列表
                            List<String> supSelfCodeList = supList.stream().filter(Objects::nonNull).map(GaiaSupplierBusiness::getSupSelfCode).collect(Collectors.toList());

                            operationFeignService.supplierSync(info.getClientId(), supSite, attribute, supSelfCodeList);
                        });
                    }
                } catch (Exception e) {
                    log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
                }

            } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
                // 更新首营信息表
                gspinfo.setSupGspStatus(CommonEnum.GspinfoStauts.ABANDONED.getCode()); // 已废弃
                gaiaSupplierGspinfoMapper.updateByPrimaryKeySelective(gspinfo);
            }
        }
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 商品首营处理
     *
     * @param info
     */
    private FeignResult productHandle(ApprovalInfo info) {
        Map<String, Object> resultMap = new HashMap<String, Object>();
        FeignResult result = new FeignResult();
        List<GaiaProductGspinfo> list = gaiaProductGspinfoMapper.selectProductGspByFlowNo(info.getClientId(), info.getWfOrder());
        if (CollectionUtils.isEmpty(list)) {
            result.setCode(1002);
            result.setMessage("首营信息不存在");
            return result;
        }
        // 审批中的数据
        if (!CommonEnum.GspinfoStauts.APPROVAL.getCode().equals(list.get(0).getProGspStatus())) {
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        }
        String createUser = gaiaProductGspinfoMapper.getCreateUser(info.getClientId(), info.getWfOrder());
        Map<String, List<String>> map = new HashMap<>();

        // 商品定位接口
        List<ProductPositionExcludeDTO> proPosiitonList = gaiaProductBusinessMapper.getProductPositionDefaultValue(info.getClientId());
        List<GaiaProductClass> gaiaProductClassList = baseService.selectGaiaProductClass();
        List<GaiaSdWtpro> sdWtpros = new ArrayList<>();
        List<String> multipleStoreProductSyncStoreList = new ArrayList<>();
        List<String> multipleStoreProductSyncProIdList = new ArrayList<>();
        // 商品首营
        for (GaiaProductGspinfo gspinfo : list) {
            // 销项税率
            String pro_output_tax = "";
            // 进项税率
            String pro_input_tax = gspinfo.getProInputTax();
            try {
                // 税率
                GaiaTaxCode gaiaTaxCode = gaiaTaxCodeMapper.selectByPrimaryKey(pro_input_tax);
                List<GaiaTaxCode> gaiaTaxCodeList = gaiaTaxCodeMapper.selectTaxList();
                // 销项税率集合
                List<GaiaTaxCode> boys = gaiaTaxCodeList.stream().filter(s -> s.getTaxCodeValue().equals(gaiaTaxCode.getTaxCodeValue()) && s.getTaxCodeClass().equals("2")).collect(Collectors.toList());
                pro_output_tax = boys.get(0).getTaxCode();
            } catch (Exception e) {
                pro_output_tax = pro_input_tax.replace("J", "X");
            }
            // 审批状态 = 审批通过
            if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
                // 参考毛利率按照零售、采购价自动计算存入，公式：（参考售价-参考进价）/参考售价*100%（若参考售价为空或零，直接赋值0）
                // 参考进货价
                BigDecimal pro_cgj = gspinfo.getProCgj() == null ? BigDecimal.ZERO : gspinfo.getProCgj();
                // 参考零售价
                BigDecimal pro_lsj = gspinfo.getProLsj() == null ? BigDecimal.ZERO : gspinfo.getProLsj();
                // 参考毛利率
                BigDecimal pro_mll = BigDecimal.ZERO;
                // 若参考售价为空或零，直接赋值0
                if (pro_lsj.compareTo(BigDecimal.ZERO) != 0) {
                    // （参考售价-参考进价）/参考售价*100%
                    pro_mll = (pro_lsj.subtract(pro_cgj)).divide(pro_lsj, 4, BigDecimal.ROUND_HALF_UP);
                }
                GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByDcCode(gspinfo.getClient(), gspinfo.getProSite());
                if (gaiaDcData != null) {
                    StoreRequestDto dto = new StoreRequestDto();
                    dto.setClient(gspinfo.getClient());
                    dto.setStoChainHead(gspinfo.getProSite());
                    List<GaiaStoreData> storeDataList = gaiaStoreDataMapper.getStoreListByDc(dto);
                    for (GaiaStoreData storeData : storeDataList) {
//                    if ("2".equals(storeData.getStoAttribute())) {  // 门店属性 = 2-连锁
                        // 将商品首营信息插入到 业务表
                        GaiaProductBusiness business = initBusiness(gspinfo);
                        business.setProSite(storeData.getStoCode());
                        // 助记码
                        if (StringUtils.isBlank(business.getProPym())) {
                            business.setProPym(StringUtils.ToFirstChar(StringUtils.isNotBlank(business.getProDepict()) ? business.getProDepict() : business.getProName()));
                        }
                        business.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                        business.setProOutputTax(pro_output_tax);
                        business.setProFixBin(StringUtils.isBlank(business.getProFixBin()) ? business.getProSelfCode() : business.getProFixBin());
                        // 启用电子监管码
//                        business.setProElectronicCode(CommonEnum.NoYesStatus.NO.getCode());
                        business.setProMll(pro_mll);
                        String key = storeData.getStoCode();
                        List<String> selfCodeList = map.get(key);
                        if (CollectionUtils.isEmpty(selfCodeList)) {
                            selfCodeList = new ArrayList<>();
                            selfCodeList.add(business.getProSelfCode());
                            map.put(key, selfCodeList);
                        } else {
                            if (selfCodeList.contains(business.getProSelfCode())) {
                                continue;
                            }
                            selfCodeList.add(business.getProSelfCode());
                            map.put(key, selfCodeList);
                        }
                        GaiaProductBusiness businessExit = gaiaProductBusinessMapper.selectByPrimaryKey(business);
                        if (!ObjectUtils.isEmpty(businessExit)) {
                            continue;
                        }
                        // 是否订购
                        ProductPositionExcludeDTO productPositionExcludeDTO = null;
                        if (StringUtils.isNotBlank(business.getProCompclass())) {
                            productPositionExcludeDTO = proPosiitonList.stream().filter(Objects::nonNull).filter(item -> {
                                if (StringUtils.isNotBlank(item.getGppeProCompclassStart()) && business.getProCompclass().startsWith(item.getGppeProCompclassStart())) {
                                    return true;
                                }
                                if (StringUtils.isNotBlank(item.getGppeProClassStart()) && business.getProClass().startsWith(item.getGppeProClassStart())) {
                                    return true;
                                }
                                return false;
                            }).findFirst().orElse(null);
                        }
                        if (ObjectUtils.isNotEmpty(productPositionExcludeDTO) && StringUtils.isNotBlank(productPositionExcludeDTO.getGppeProPosition())) {
                            business.setProPosition(productPositionExcludeDTO.getGppeProPosition());
                        } else {
                            if ("1".equals(gspinfo.getProOrderFlag())) {
                                business.setProPosition("D");
                            } else {
                                business.setProPosition("X");
                            }
                        }

                        // 更新人
                        business.setProUpdateUser(createUser);
                        if (!CollectionUtils.isEmpty(gaiaProductClassList) && StringUtils.isBlank(business.getProClassName())) {
                            GaiaProductClass gaiaProductClass = gaiaProductClassList.stream()
                                    .filter(item -> business.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                            if (gaiaProductClass != null) {
                                business.setProClassName(gaiaProductClass.getProClassName());
                            }
                        }
                        gaiaProductBusinessMapper.insert(business);
                        // 第三方 gys-operation
                        multipleStoreProductSyncStoreList.add(business.getProSite());
                        multipleStoreProductSyncProIdList.add(business.getProSelfCode());

                        // 零售价联动
                        GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
                        // 加盟商
                        gaiaSdProductPriceKey.setClient(gspinfo.getClient());
                        // 门店编码
                        gaiaSdProductPriceKey.setGsppBrId(storeData.getStoCode());
                        // 商品编码
                        gaiaSdProductPriceKey.setGsppProId(business.getProSelfCode());
                        GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
                        if (gaiaSdProductPrice == null) {
                            gaiaSdProductPrice = new GaiaSdProductPrice();
                            // 加盟商
                            gaiaSdProductPrice.setClient(gspinfo.getClient());
                            // 门店编码
                            gaiaSdProductPrice.setGsppBrId(storeData.getStoCode());
                            // 商品编码
                            gaiaSdProductPrice.setGsppProId(business.getProSelfCode());
                            // 零售价
                            gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
                            // 会员价
                            gaiaSdProductPrice.setGsppPriceHy(business.getProHyj());
                            gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
                        } else {
                            // 零售价
                            gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
                            // 会员价
                            gaiaSdProductPrice.setGsppPriceHy(business.getProHyj());
                            gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
                        }
//                    }
                    }
                }
                // 将商品首营信息插入到 业务表
                GaiaProductBusiness business = initBusiness(gspinfo);
                // 助记码
                if (StringUtils.isBlank(business.getProPym())) {
                    business.setProPym(StringUtils.ToFirstChar(StringUtils.isNotBlank(business.getProDepict()) ? business.getProDepict() : business.getProName()));
                }
                business.setProCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                business.setProOutputTax(pro_output_tax);
                business.setProFixBin(StringUtils.isBlank(business.getProFixBin()) ? business.getProSelfCode() : business.getProFixBin());
                // 启用电子监管码
//                business.setProElectronicCode(CommonEnum.NoYesStatus.NO.getCode());
                business.setProMll(pro_mll);
                // 是否订购
                ProductPositionExcludeDTO productPositionExcludeDTO = null;
                if (StringUtils.isNotBlank(business.getProCompclass())) {
                    productPositionExcludeDTO = proPosiitonList.stream().filter(Objects::nonNull).filter(item -> {
                        if (StringUtils.isNotBlank(item.getGppeProCompclassStart()) && business.getProCompclass().startsWith(item.getGppeProCompclassStart())) {
                            return true;
                        }
                        if (StringUtils.isNotBlank(item.getGppeProClassStart()) && business.getProClass().startsWith(item.getGppeProClassStart())) {
                            return true;
                        }
                        return false;
                    }).findFirst().orElse(null);
                }
                if (ObjectUtils.isNotEmpty(productPositionExcludeDTO) && StringUtils.isNotBlank(productPositionExcludeDTO.getGppeProPosition())) {
                    business.setProPosition(productPositionExcludeDTO.getGppeProPosition());
                } else {
                    if ("1".equals(gspinfo.getProOrderFlag())) {
                        business.setProPosition("D");
                    } else {
                        business.setProPosition("X");
                    }
                }

                // 更新人
                business.setProUpdateUser(createUser);
                if (!CollectionUtils.isEmpty(gaiaProductClassList) && StringUtils.isBlank(business.getProClassName())) {
                    GaiaProductClass gaiaProductClass = gaiaProductClassList.stream()
                            .filter(item -> business.getProClass().equals(item.getProClassCode())).findFirst().orElse(null);
                    if (gaiaProductClass != null) {
                        business.setProClassName(gaiaProductClass.getProClassName());
                    }
                }
                gaiaProductBusinessMapper.insert(business);
                GaiaSdWtpro sdWtpro = new GaiaSdWtpro();
                sdWtpro.setClient(business.getClient());
                sdWtpro.setProSelfCode(business.getProSelfCode());
                sdWtpro.setProDsfCode(business.getProSelfCode());
                sdWtpro.setProNm(business.getProSelfCode());
                sdWtpro.setProDsfName(business.getProDepict());
                sdWtpro.setStatus(1);
                sdWtpros.add(sdWtpro);
                // 第三方 gys-operation
                multipleStoreProductSyncStoreList.add(business.getProSite());
                multipleStoreProductSyncProIdList.add(business.getProSelfCode());

                // 更新首营信息表
                gspinfo.setProGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode()); // 已审批
                gaiaProductGspinfoMapper.updateByPrimaryKeySelective(gspinfo);

                //  审核铺货计划
                checkPlan(business, "check", createUser);
                log.info(business.toString());
                // 审核第三方编码
                checkThirdCode(business, "check");


//                // 零售价联动
//                GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
//                // 加盟商
//                gaiaSdProductPriceKey.setClient(gspinfo.getClient());
//                // 门店编码
//                gaiaSdProductPriceKey.setGsppBrId(gspinfo.getProSite());
//                // 商品编码
//                gaiaSdProductPriceKey.setGsppProId(business.getProSelfCode());
//                GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
//                if (gaiaSdProductPrice == null) {
//                    gaiaSdProductPrice = new GaiaSdProductPrice();
//                    // 加盟商
//                    gaiaSdProductPrice.setClient(gspinfo.getClient());
//                    // 门店编码
//                    gaiaSdProductPrice.setGsppBrId(gspinfo.getProSite());
//                    // 商品编码
//                    gaiaSdProductPrice.setGsppProId(business.getProSelfCode());
//                    // 零售价
//                    gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
//                    gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
//                } else {
//                    // 零售价
//                    gaiaSdProductPrice.setGsppPriceNormal(pro_lsj);
//                    gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
//                }
            } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
                // 更新首营信息表
                gspinfo.setProGspStatus(CommonEnum.GspinfoStauts.ABANDONED.getCode()); // 已废弃
                gaiaProductGspinfoMapper.updateByPrimaryKeySelective(gspinfo);
                // 删除铺货计划
                GaiaProductBusiness business = new GaiaProductBusiness();
                business.setClient(gspinfo.getClient());
                business.setProSelfCode(gspinfo.getProSelfCode());
                checkPlan(business, "cancel", createUser);
                // 删除第三方编码
                checkThirdCode(business, "cancel");
            }
        }
        if (!CollectionUtils.isEmpty(multipleStoreProductSyncStoreList) && !CollectionUtils.isEmpty(multipleStoreProductSyncProIdList)) {
            resultMap.put("client", info.getClientId());
            resultMap.put("proSiteList", multipleStoreProductSyncStoreList.stream().distinct().collect(Collectors.toList()));
            resultMap.put("proSelfCodeList", multipleStoreProductSyncProIdList.stream().distinct().collect(Collectors.toList()));
        }
        //检查参数表，若参数存在且为开启状态（值为1）时
        String para = gaiaSdReplenishHMapper.getParamByClientAndGcspId(info.getClientId(), "WTPS_SP_AUTO");
        if (StringUtils.isNotBlank(para) && "1".equalsIgnoreCase(para)) {
            if (!CollectionUtils.isEmpty(sdWtpros)) {
                List<String> proCodes = sdWtpros.stream().map(GaiaSdWtpro::getProSelfCode).collect(Collectors.toList());
                List<String> existCodes = gaiaSdWtproMapper.querySdWtPro(info.getClientId(), proCodes);
                if (existCodes != null) {
                    if (!CollectionUtils.isEmpty(existCodes)) {
                        List<GaiaSdWtpro> distintPros = sdWtpros.stream().collect(
                                Collectors.collectingAndThen(
                                        Collectors.toCollection(() -> new TreeSet<>(Comparator.comparing(GaiaSdWtpro::getProSelfCode))), ArrayList::new)
                        );
                        List<GaiaSdWtpro> remain = distintPros.stream().filter(dist -> !existCodes.contains(dist.getProSelfCode())).collect(Collectors.toList());
                        if (!CollectionUtils.isEmpty(remain)) {
                            gaiaSdWtproMapper.insertBatch(remain);
                        }
                    } else {
                        gaiaSdWtproMapper.insertBatch(sdWtpros);
                    }
                } else {
                    gaiaSdWtproMapper.insertBatch(sdWtpros);
                }
            }
        }
        result.setCode(0);
        result.setMessage("执行成功");
        result.setData(resultMap);
        return result;
    }

    private void checkThirdCode(GaiaProductBusiness business, String type) {
        GaiaSdWtpro wtpro = new GaiaSdWtpro();
        wtpro.setClient(business.getClient());
        wtpro.setProSelfCode(business.getProSelfCode());
        String para = gaiaSdReplenishHMapper.getParamByClientAndGcspId(business.getClient(), "COLD_CHAIN");
        if ("1".equals(para)) {
            GaiaSdWtpro gaiaSdWtpro = gaiaSdWtproMapper.queryById(wtpro);
            if ("check".equals(type) && ObjectUtils.isNotEmpty(gaiaSdWtpro)) {
                gaiaSdWtpro.setStatus(1);
                gaiaSdWtproMapper.update(gaiaSdWtpro);
            } else if ("cancel".equals(type) && ObjectUtils.isNotEmpty(gaiaSdWtpro)) {
                gaiaSdWtproMapper.deleteById(gaiaSdWtpro.getId());
            }
        }
    }

    private void checkPlan(GaiaProductBusiness business, String type, String createUser) {
        Date now = new Date();
        GaiaNewDistributionPlan plan = gaiaNewDistributionPlanMapper.getPlan(business);
        // 状态不为0 的时候不修改
        if (!ObjectUtils.isEmpty(plan) && 0 == plan.getStatus()) {
            if ("check".equals(type)) {
                plan.setStatus(1);
                plan.setUpdateUser(createUser);
                plan.setUpdateTime(now);
                gaiaNewDistributionPlanMapper.updatePlan(plan);
            } else if ("cancel".equals(type)) {
                plan.setDeleteFlag(1);
                plan.setUpdateUser(createUser);
                plan.setUpdateTime(now);
                gaiaNewDistributionPlanMapper.updatePlan(plan);
            }
        }

    }

    /**
     * 商品业务表数据生成
     *
     * @param gspinfo
     * @return
     */
    private GaiaProductBusiness initBusiness(GaiaProductGspinfo gspinfo) {
        GaiaProductBusiness business = new GaiaProductBusiness();
        // 「匹配状态」默认为“2-完全匹配”
        business.setProMatchStatus(CommonEnum.MatchStatus.DIDMATCH.getCode());
        if (StringUtils.isNotBlank(gspinfo.getProCode())) {
            // 商品主数据
            GaiaProductBasic gaiaProductBasic = gaiaProductBasicMapper.selectByPrimaryKey(gspinfo.getProCode());
            if (gaiaProductBasic != null) {
                // 商品主数据复制到业务表
                BeanUtils.copyProperties(gaiaProductBasic, business);
                // 「匹配状态」默认为“2-完全匹配”
                business.setProMatchStatus(CommonEnum.MatchStatus.EXACTMATCH.getCode());
            }
        }
        // 商品描述
        if (StringUtils.isBlank(business.getProDepict())) {
            business.setProDepict("(".concat(gspinfo.getProName()).concat(")").concat(gspinfo.getProCommonname()));
        }
        // 商品首营数据复制到业务表
        BeanUtils.copyProperties(gspinfo, business);
        // 国际条形码
        business.setProBarcode(gspinfo.getProBarcode());
        //商品状态正常
        business.setProStatus(StringUtils.isNotBlank(business.getProStatus()) ? business.getProStatus() : CommonEnum.GoodsStauts.GOODSNORMAL.getCode());
//        // 禁止销售
//        business.setProNoRetail(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止采购
//        business.setProNoPurchase(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止配送
//        business.setProNoDistributed(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止退厂
//        business.setProNoSupplier(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止退仓
//        business.setProNoDc(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止调剂
//        business.setProNoAdjust(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止批发
//        business.setProNoSale(CommonEnum.NoYesStatus.NO.getCode());
//        // 禁止请货
//        business.setProNoApply(CommonEnum.NoYesStatus.NO.getCode());
//        // 是否医保
//        business.setProIfMed(CommonEnum.NoYesStatus.NO.getCode());
//        // 按中包装量倍数配货
//        business.setProPackageFlag(CommonEnum.NoYesStatus.NO.getCode());
//        // 是否重点养护
//        business.setProKeyCare(CommonEnum.NoYesStatus.NO.getCode());
        // 固定货位
        business.setProFixBin(business.getProSelfCode());
        if (StringUtils.isBlank(business.getProStorageCondition())) {
            business.setProStorageCondition("1");
        }
        if (StringUtils.isBlank(business.getProCompclass())) {
            business.setProCompclass("N999999");
            business.setProCompclassName("N999999未匹配");
        }
        return business;
    }

    /**
     * 短信模板处理
     */
    private FeignResult smsTemplateHandle(ApprovalInfo info) {
        String templateId = null;
        FeignResult result = new FeignResult();
        GaiaSmsTemplate template = gaiaSmsTemplateMapper.selectTemplateByFlowNo(info.getWfOrder());
        if (ObjectUtils.isEmpty(template)) {
            result.setCode(1002);
            result.setMessage("该短信模板信息不存在");
            return result;
        }
        // 审批中的数据
        if (!CommonEnum.GspStatus.PROCESS.getCode().equals(template.getSmsGspStatus())) {
            result.setCode(1003);
            result.setMessage("该模板信息已经被审批");
            return result;
        }
        // 审批通过时 审批意见不能为空
        if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
            if (StringUtils.isEmpty(info.getMemo())) {
                result.setCode(1004);
                result.setMessage("审批意见不能为空");
                return result;
            }
            Matcher matcher = CommonConstants.pattern.matcher(info.getMemo());
            List<String> idlist = new ArrayList<>();
            while (matcher.find()) {
                idlist.add(matcher.group(1));
            }
            if (idlist.size() != 1) {
                result.setCode(1005);
                result.setMessage("审批意见格式格式不正确");
                return result;
            }
            templateId = idlist.get(0);
        }
        GaiaSmsTemplate smsTemplate = new GaiaSmsTemplate();
        smsTemplate.setSmsFlowNo(info.getWfOrder());
        if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 审批通过
            smsTemplate.setSmsGspStatus(CommonEnum.GspStatus.FINISH.getCode());
            smsTemplate.setSmsTemplateId(templateId);

        } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
            // 审批状态 = 审批驳回
            smsTemplate.setSmsGspStatus(CommonEnum.GspStatus.REFUSE.getCode());
        }
        gaiaSmsTemplateMapper.updateByFlowNo(smsTemplate);
        result.setCode(0);
        result.setMessage("执行成功");
        return result;

    }

    /**
     * 营销任务处理
     */
    private FeignResult marketingHandle(ApprovalInfo info) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("clientId", info.getClientId());
            map.put("flowNo", info.getWfOrder());
            map.put("status", info.getWfStatus());
            // 营销审批结果
            Result operateResult = operateFeign.execute(map);
            log.info("营销审批结果operateResult{}", JsonUtils.beanToJson(operateResult));
            FeignResult result = new FeignResult();
            if (ResultUtil.hasError(operateResult)) {
                result.setCode(1001);
                result.setMessage(operateResult.getMessage());
                log.info("营销审批结果result{}", JsonUtils.beanToJson(result));
                return result;
            }
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            FeignResult result = new FeignResult();
            result.setCode(1001);
            result.setMessage("审批异常");
            return result;
        }
//
//        String templateId = null;
//        FeignResult result = new FeignResult();
//        GaiaSdMarketing marketingExit = gaiaSdMarketingMapper.selectTemplateByFlowNo(info.getWfOrder());
//        if (ObjectUtils.isEmpty(marketingExit)) {
//            result.setCode(1002);
//            result.setMessage("记录不存在");
//            return result;
//        }
//        // 审批中的数据
//        if (!CommonEnum.GsmImpl.PENDING_APPROVAL.getCode().equals(marketingExit.getGsmImpl())) {
//            result.setCode(1003);
//            result.setMessage("该营销活动已经被审批");
//            return result;
//        }
//
//        if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
//            // 审批状态 = 审批通过
//            // (1)主数据更新为4:审批通过
//            marketingExit.setGsmFlowNo(info.getWfOrder());
//            marketingExit.setGsmImpl(CommonEnum.GsmImpl.APPROVED.getCode());
//            marketingExit.setGsmImpltime(DateUtils.getCurrentDateTimeStrYMDHMS());
//            gaiaSdMarketingMapper.updateByFlowNo(marketingExit);
//
//            // (2)推送到门店
//            List<GaiaSdMarketing> marketingList = new ArrayList<>();
//            List<GaiaSdMarketingSearch> searchList = new ArrayList<>();
//            // 短信查询条件
//            GaiaSdMarketingSearch smsSearchExit = gaiaSdMarketingSearchMapper.selectByPrimaryKey(marketingExit.getGsmSmscondi());
//            // 电话查询条件
//            GaiaSdMarketingSearch telSearchExit = gaiaSdMarketingSearchMapper.selectByPrimaryKey(marketingExit.getGsmTelcondi());
//            // 门店列表
//            List<GaiaStoreData> storeList = gaiaStoreDataMapper.getStoreListByClient(marketingExit.getClient());
//            storeList.forEach(store -> {
//                GaiaSdMarketing marketing = new GaiaSdMarketing();
//                marketingList.add(marketing);
//                BeanUtils.copyProperties(marketingExit, marketing);
//                marketing.setGsmStore(store.getStoCode());
//
//                // 另存常用查询/短信
//                if (!ObjectUtils.isEmpty(smsSearchExit)) {
//                    GaiaSdMarketingSearch smsSearch = new GaiaSdMarketingSearch();
//                    searchList.add(smsSearch);
//                    BeanUtils.copyProperties(smsSearchExit, smsSearch);
//                    smsSearch.setGsmsId(UUIDUtil.getUUID());
//                    smsSearch.setGsmsStore(store.getStoCode());
//                    marketing.setGsmSmscondi(smsSearch.getGsmsId());
//                }
//                // 另存常用查询/电话
//                if (!ObjectUtils.isEmpty(telSearchExit)) {
//                    GaiaSdMarketingSearch telSearch = new GaiaSdMarketingSearch();
//                    searchList.add(telSearch);
//                    BeanUtils.copyProperties(telSearchExit, telSearch);
//                    telSearch.setGsmsId(UUIDUtil.getUUID());
//                    telSearch.setGsmsStore(store.getStoCode());
//                    marketing.setGsmTelcondi(telSearch.getGsmsId());
//                }
//
//            });
//            // 保存营销任务
//            gaiaSdMarketingMapper.saveBatch(marketingList);
//            // 保存短信/电话查询条件
//            gaiaSdMarketingSearchMapper.saveBatch(searchList);
//            // (3)发送短信
//            customMultiThreadingService.sendMarketTaskSms(storeList, marketingExit);
//
//            result.setCode(0);
//            result.setMessage("执行成功");
//            return result;
//        } else if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
//            // 审批状态 = 审批驳回
//            marketingExit.setGsmFlowNo(info.getWfOrder());
//            marketingExit.setGsmImpl(CommonEnum.GsmImpl.REFUSE.getCode());
//            marketingExit.setGsmImpltime(DateUtils.getCurrentDateTimeStrYMDHMS());
//            gaiaSdMarketingMapper.updateByFlowNo(marketingExit);
//            result.setCode(0);
//            result.setMessage("执行成功");
//            return result;
//        }
//        result.setCode(107);
//        result.setMessage("不存在该审批状态");
//        return result;


    }

    /**
     * 商品主数据修改处理
     */
    private FeignResult productEditHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        List<GaiaProductChange> proChangeList = gaiaProductChangeMapper.checkFlowNoExit(info.getWfOrder());
        List<Integer> changeIdList = proChangeList.stream().map(GaiaProductChange::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(proChangeList)) {
            result.setCode(1010);
            result.setMessage("该商品信息不存在/或已经审批完成");
            return result;
        }
        // 若审请驳回 则直接返回
        if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
            gaiaProductChangeMapper.updateStatusFaile(changeIdList);
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        }

        // 反射属性名
        Map<String, PropertyDescriptor> propertyInfoMap = Stream.of(BeanUtils.getPropertyDescriptors(GaiaProductBusiness.class))
                .filter(Objects::nonNull).collect(Collectors.toMap(FeatureDescriptor::getName, item -> item));
        // 修改记录转DB (商品的每一条修改记录作为一个key)
        Map<GaiaProductChangeKey, List<GaiaProductChange>> changeKeyListMap = proChangeList.stream()
                .collect(Collectors.groupingBy(item -> new GaiaProductChangeKey(item.getClient(), item.getProSite(), item.getProSelfCode())));
        List<Map<String, String>> fixBinList = new ArrayList<>();
        List<String> proSiteList = new ArrayList<>();
        List<String> proSelfList = new ArrayList<>();
        String client = proChangeList.get(0).getClient();
        List<GaiaBatchInfo> gaiaBatchInfoList = new ArrayList<>();

        List<GaiaProductBusinessKey> payKeyList = new ArrayList<>();
        changeKeyListMap.forEach((primaryKey, changeList) -> {
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setClient(client);
            gaiaProductBusinessKey.setProSite(primaryKey.getProSite());
            gaiaProductBusinessKey.setProSelfCode(primaryKey.getProSelfCode());
            payKeyList.add(gaiaProductBusinessKey);
        });
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.getProductBusiness2(client, payKeyList);
        if (CollectionUtils.isEmpty(gaiaProductBusinessList)) {
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        }
        changeKeyListMap.forEach((primaryKey, changeList) -> {
            GaiaProductBusiness business = gaiaProductBusinessList.stream().filter(t ->
                    t.getClient().equals(client)
                            && t.getProSite().equals(primaryKey.getProSite())
                            && t.getProSelfCode().equals(primaryKey.getProSelfCode())).findFirst().orElse(null);
            if (business == null) {
                throw new CustomResultException("该商品已经被删除");
            }
            String proFixBin = business.getProFixBin();
            changeList.forEach(change -> {
                // 属性名
                String proChangeField = change.getProChangeField();
                // 值
                String proChangeTo = change.getProChangeTo();
                // set方法
                Method writeMethod = propertyInfoMap.get(proChangeField).getWriteMethod();
                // 属性类型
                Class<?> propertyType = propertyInfoMap.get(proChangeField).getPropertyType();
                try {
                    // set方法执行
                    if (propertyType == String.class) {
                        writeMethod.invoke(business, proChangeTo);
                    } else if (propertyType == BigDecimal.class) {
                        writeMethod.invoke(business, (proChangeTo == null || proChangeTo.length() == 0) ? null : new BigDecimal(proChangeTo));
                    } else if (propertyType == Integer.class || propertyType == Long.class) {
                        writeMethod.invoke(business, (proChangeTo == null || proChangeTo.length() == 0) ? null : Integer.valueOf(proChangeTo));
                    } else {
                        throw new CustomResultException("反射调用set方法异常，类型不匹配");
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                    throw new CustomResultException(MessageFormat.format("商品主数据修改审批回调,属性{0}设置失败", proChangeField));
                }

                if ("proFixBin".equals(change.getProChangeField())) {
                    if (!StringUtils.equals(proFixBin, business.getProFixBin())) {
                        Map<String, String> map = new HashMap<>();
                        map.put("client", primaryKey.getClient());
                        map.put("proSite", primaryKey.getProSite());
                        map.put("userId", proChangeList.get(0).getProChangeUser());
                        map.put("commodityCode", primaryKey.getProSelfCode());
                        map.put("originalCargoLocationNo", proFixBin);
                        map.put("cargoLocationNo", business.getProFixBin());
                        fixBinList.add(map);
                    }
                }
                // 生产厂家PRO_FACTORY_NAME，产地 PRO_PLACE时，检查PRO_STORAGE_AREA如果不是3，
                // 则根据加盟商+商品 到GAIA_BATCH_INFO中把对应的BAT_FACTORY_NAME 或者 BAT_PRO_PLACE改掉。
                if (!"3".equals(business.getProStorageArea()) && (change.getProChangeField().equals("proFactoryName") || change.getProChangeField().equals("proPlace"))) {
                    GaiaBatchInfo gaiaBatchInfo = gaiaBatchInfoList.stream().filter(t ->
                            t.getBatProCode().equals(business.getProSelfCode())
                                    && t.getClient().equals(primaryKey.getClient())
                    ).findFirst().orElse(null);
                    if (gaiaBatchInfo == null) {
                        gaiaBatchInfo = new GaiaBatchInfo();
                        gaiaBatchInfo.setClient(primaryKey.getClient());
                        gaiaBatchInfo.setBatProCode(business.getProSelfCode());
                        gaiaBatchInfo.setBatChangeDate(DateUtils.getCurrentDateStrYYMMDD());
                        gaiaBatchInfo.setBatChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
                        gaiaBatchInfo.setBatChangeUser(proChangeList.get(0).getProChangeUser());
                        if (StringUtils.isNotBlank(business.getProFactoryName())) {
                            gaiaBatchInfo.setBatFactoryCode(business.getProFactoryCode());
                            gaiaBatchInfo.setBatFactoryName(business.getProFactoryName());
                        }
                        if (StringUtils.isNotBlank(business.getProPlace())) {
                            gaiaBatchInfo.setBatProPlace(business.getProPlace());
                        }
                        gaiaBatchInfoList.add(gaiaBatchInfo);
                    }
                }
            });
            // 变更人
            business.setProUpdateUser(proChangeList.get(0).getProChangeUser());
            //更新 business 表
            gaiaProductBusinessMapper.updateByPrimaryKey(business);

            proSiteList.add(business.getProSite());
            proSelfList.add(business.getProSelfCode());
        });
        // 生产厂家PRO_FACTORY_NAME，产地 PRO_PLACE时，检查PRO_STORAGE_AREA如果不是3，
        // 则根据加盟商+商品 到GAIA_BATCH_INFO中把对应的BAT_FACTORY_NAME 或者 BAT_PRO_PLACE改掉。
        if (!CollectionUtils.isEmpty(gaiaBatchInfoList)) {
            for (GaiaBatchInfo gaiaBatchInfo : gaiaBatchInfoList) {
                gaiaBatchInfoMapper.updateFPByPrimaryKeySelective(gaiaBatchInfo);
            }
        }

        Map<String, Object> resultMap = new HashMap<>();
        if (StringUtils.isNotBlank(client) && !CollectionUtils.isEmpty(proSiteList) && !CollectionUtils.isEmpty(proSelfList)) {
            resultMap.put("client", client);
            resultMap.put("proSiteList", proSiteList.stream().distinct().collect(Collectors.toList()));
            resultMap.put("proSelfCodeList", proSelfList.stream().distinct().collect(Collectors.toList()));
        }

        // 记录表状态修改
        gaiaProductChangeMapper.updateStatusSuccess(changeIdList);

        // 在商品主数据修改字段：「固定货位-PRO_FIX_BIN」时，需要调用WMS的『修改商品的自动货位接口』。
        if (!CollectionUtils.isEmpty(fixBinList)) {
            for (Map<String, String> map : fixBinList) {
                baseService.updateProFixBin(map.get("client"), map.get("proSite"), map.get("userId"),
                        map.get("commodityCode"), map.get("originalCargoLocationNo"), map.get("cargoLocationNo"));
            }
        }

        result.setCode(0);
        result.setMessage("执行成功");
        result.setData(resultMap);
        return result;
    }

    /**
     * 供应商主数据修改处理
     */
    private FeignResult supplierEditHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        List<GaiaSupplierChange> supChangeList = gaiaSupplierChangeMapper.getChangeList(info.getClientId(), info.getWfOrder());
        List<Integer> changeIdList = supChangeList.stream().map(GaiaSupplierChange::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(supChangeList)) {
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        }
        // 若审请驳回 则直接返回
        if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
            gaiaSupplierChangeMapper.updateStatusFaile(changeIdList);
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        }

        // 判断是否为修改经营范围
        boolean matchEditSupScope = supChangeList.stream().anyMatch(change -> CommonEnum.JyfwEditFlowField.SUPPLIER.getCode().equals(change.getSupChangeField()));
        if (matchEditSupScope) {
            return editSupScope(supChangeList, changeIdList, info.getClientId());
        }

        // 反射属性名
        Map<String, PropertyDescriptor> propertyInfoMap = Stream.of(BeanUtils.getPropertyDescriptors(GaiaSupplierBusiness.class))
                .filter(Objects::nonNull).collect(Collectors.toMap(FeatureDescriptor::getName, item -> item));
        // 修改记录转 business (供应商的每一条修改记录作为一个key)
        Map<GaiaSupplierChangeKey, List<GaiaSupplierChange>> changeKeyListMap = supChangeList.stream()
                .collect(Collectors.groupingBy(item -> new GaiaSupplierChangeKey(item.getClient(), item.getSupSite(), item.getSupSelfCode())));
        // 第三方 gys-operation
        List<GaiaSupplierBusiness> gysOperationList = new ArrayList<>();

        changeKeyListMap.forEach((primaryKey, changeList) -> {
            GaiaSupplierBusinessKey businessKey = new GaiaSupplierBusinessKey();
            businessKey.setClient(primaryKey.getClient());
            businessKey.setSupSite(primaryKey.getSupSite());
            businessKey.setSupSelfCode(primaryKey.getSupSelfCode());
            GaiaSupplierBusiness business = gaiaSupplierBusinessMapper.selectByPrimaryKey(businessKey);
            if (ObjectUtils.isEmpty(business)) {
                throw new CustomResultException("该供应商已经被删除");
            }
            changeList.forEach(change -> {
                // 属性名
                String changeField = change.getSupChangeField();
                // 值
                String changeTo = change.getSupChangeTo();
                // set方法
                Method writeMethod = propertyInfoMap.get(changeField).getWriteMethod();
                // 属性类型
                Class<?> propertyType = propertyInfoMap.get(changeField).getPropertyType();

                try {
                    // set方法执行
                    if (propertyType == String.class) {
                        writeMethod.invoke(business, changeTo);
                    } else if (propertyType == BigDecimal.class) {
                        writeMethod.invoke(business, (changeTo == null || changeTo.length() == 0) ? null : new BigDecimal(changeTo));
                    } else if (propertyType == Integer.class || propertyType == Long.class) {
                        writeMethod.invoke(business, (changeTo == null || changeTo.length() == 0) ? null : Integer.valueOf(changeTo));
                    } else {
                        throw new CustomResultException("反射调用set方法，类型不匹配");
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                    throw new CustomResultException(MessageFormat.format("供应商主数据修改审批回调,属性{0}设置失败", changeField));
                }
            });
            //更新 business 表 todo 如果对应门店不存在(新增的门店是维护不到里面的)
            gaiaSupplierBusinessMapper.updateByPrimaryKey(business);
            // 第三方 gys-operation
            gysOperationList.add(business);
        });
        // 记录表状态修改
        gaiaSupplierChangeMapper.updateStatusSuccess(changeIdList);
        // 第三方 gys-operation
        try {
            // 非连锁门店集合
            List<String> stoList = gaiaProductBasicMapper.getStoList(info.getClientId());
            Map<String, List<GaiaSupplierBusiness>> collect = gysOperationList.stream().collect(Collectors.groupingBy(GaiaSupplierBusiness::getSupSite));
            collect.forEach((supSite, businessKeyList) -> {
                if (!CollectionUtils.isEmpty(stoList) && stoList.contains(supSite)) {
                    operationFeignService.supplierSync(info.getClientId(), supSite, "1", businessKeyList.stream().map(GaiaSupplierBusiness::getSupSelfCode).collect(Collectors.toList()));
                } else {
                    operationFeignService.supplierSync(info.getClientId(), supSite, "2", businessKeyList.stream().map(GaiaSupplierBusiness::getSupSelfCode).collect(Collectors.toList()));
                }
            });
        } catch (Exception e) {
            log.info("[供应商信息修改 gys-operation调用 异常]{}", e.getMessage());
        }
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 修改供应商经营范围
     */
    private FeignResult editSupScope(List<GaiaSupplierChange> supChangeList, List<Integer> changeIdList, String client) {
        supChangeList.forEach(change -> {
            EditScopeVO editScopeVO = new EditScopeVO();
            editScopeVO.setJyfwSite(change.getSupSite());
            editScopeVO.setJyfwOrgid(change.getSupSelfCode());
            editScopeVO.setJyfwOrgname(change.getSupName());
            editScopeVO.setJyfwType(CommonEnum.JyfwType.SUPPLIER.getCode());
            List<JyfwooData> jyfwooDataList = businessScopeServiceMapper.getJyfwIdAndNameByIdList(change.getSupChangeTo());
            List<EditScopeVO.Scope> scopeList = jyfwooDataList.stream().filter(Objects::nonNull).map(item -> {
                EditScopeVO.Scope scope = new EditScopeVO.Scope();
                scope.setJyfwId(item.getJyfwId());
                scope.setJyfwName(item.getJyfwName());
                scope.setChecked(1);
                return scope;
            }).collect(Collectors.toList());
            editScopeVO.setScopeList(scopeList);
            businessScopeService.editScopeApproval(editScopeVO, client, change.getSupChangeUser());
        });
        // 记录表状态修改
        gaiaSupplierChangeMapper.updateStatusSuccess(changeIdList);
        FeignResult result = new FeignResult();
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 客户主数据修改处理
     */
    private FeignResult customerEditHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        List<GaiaCustomerChange> cusChangeList = gaiaCustomerChangeMapper.getChangeList(info.getClientId(), info.getWfOrder());
        List<Integer> changeIdList = cusChangeList.stream().map(GaiaCustomerChange::getId).collect(Collectors.toList());
        if (CollectionUtils.isEmpty(cusChangeList)) {
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        }
        // 若审请驳回 则直接返回
        if (ApprovalEnum.CANCEL.getCode().equals(info.getWfStatus())) {
            gaiaCustomerChangeMapper.updateStatusFaile(changeIdList);
            result.setCode(0);
            result.setMessage("执行成功");
            return result;
        }

        // 判断是否为修改经营范围
        boolean matchEditCusScope = cusChangeList.stream().anyMatch(change -> CommonEnum.JyfwEditFlowField.CUSTOMER.getCode().equals(change.getCusChangeField()));
        if (matchEditCusScope) {
            return editCusScope(cusChangeList, changeIdList, info.getClientId());
        }

        // 反射属性名
        Map<String, PropertyDescriptor> propertyInfoMap = Stream.of(BeanUtils.getPropertyDescriptors(GaiaCustomerBusiness.class))
                .filter(Objects::nonNull).collect(Collectors.toMap(FeatureDescriptor::getName, item -> item));
        // 修改记录转DB (客户的每一条修改记录作为一个key)
        Map<GaiaCustomerChangeKey, List<GaiaCustomerChange>> changeKeyListMap = cusChangeList.stream()
                .collect(Collectors.groupingBy(item -> new GaiaCustomerChangeKey(item.getClient(), item.getCusSite(), item.getCusSelfCode())));

        changeKeyListMap.forEach((primaryKey, changeList) -> {
            GaiaCustomerBusinessKey businessKey = new GaiaCustomerBusinessKey();
            businessKey.setClient(primaryKey.getClient());
            businessKey.setCusSite(primaryKey.getCusSite());
            businessKey.setCusSelfCode(primaryKey.getCusSelfCode());
            GaiaCustomerBusiness business = gaiaCustomerBusinessMapper.selectByPrimaryKey(businessKey);
            if (ObjectUtils.isEmpty(business)) {
                throw new CustomResultException("该客户已经被删除");
            }
            changeList.forEach(change -> {
                // 属性名
                String changeField = change.getCusChangeField();
                // 值
                String changeTo = change.getCusChangeTo();
                // set方法
                Method writeMethod = propertyInfoMap.get(changeField).getWriteMethod();
                // 属性类型
                Class<?> propertyType = propertyInfoMap.get(changeField).getPropertyType();

                try {
                    // set方法执行
                    if (propertyType == String.class) {
                        writeMethod.invoke(business, changeTo);
                    } else if (propertyType == BigDecimal.class) {
                        writeMethod.invoke(business, (changeTo == null || changeTo.length() == 0) ? null : new BigDecimal(changeTo));
                    } else if (propertyType == Integer.class || propertyType == Long.class) {
                        writeMethod.invoke(business, (changeTo == null || changeTo.length() == 0) ? null : Integer.valueOf(changeTo));
                    } else {
                        throw new CustomResultException("反射调用set方法，类型不匹配");
                    }
                } catch (Exception e) {
                    log.error(e.getMessage());
                    throw new CustomResultException(MessageFormat.format("客户主数据修改审批回调,属性{0}设置失败", changeField));
                }
            });
            //更新 business 表 todo 如果对应门店不存在(新增的门店是维护不到里面的)
            gaiaCustomerBusinessMapper.updateByPrimaryKey(business);
        });
        // 记录表状态修改
        gaiaCustomerChangeMapper.updateStatusSuccess(changeIdList);

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 修改客户经营范围
     */
    private FeignResult editCusScope(List<GaiaCustomerChange> cusChangeList, List<Integer> changeIdList, String client) {
        cusChangeList.forEach(change -> {
            EditScopeVO editScopeVO = new EditScopeVO();
            editScopeVO.setJyfwSite(change.getCusSite());
            editScopeVO.setJyfwOrgid(change.getCusSelfCode());
            editScopeVO.setJyfwOrgname(change.getCusName());
            editScopeVO.setJyfwType(CommonEnum.JyfwType.CUSTOMER.getCode());
            List<JyfwooData> jyfwooDataList = businessScopeServiceMapper.getJyfwIdAndNameByIdList(change.getCusChangeTo());
            List<EditScopeVO.Scope> scopeList = jyfwooDataList.stream().filter(Objects::nonNull).map(item -> {
                EditScopeVO.Scope scope = new EditScopeVO.Scope();
                scope.setJyfwId(item.getJyfwId());
                scope.setJyfwName(item.getJyfwName());
                scope.setChecked(1);
                return scope;
            }).collect(Collectors.toList());
            editScopeVO.setScopeList(scopeList);
            businessScopeService.editScopeApproval(editScopeVO, client, change.getCusChangeUser());
        });
        // 记录表状态修改
        gaiaCustomerChangeMapper.updateStatusSuccess(changeIdList);
        FeignResult result = new FeignResult();
        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 商品調價审批回调
     *
     * @param info
     * @return
     */
    private FeignResult productAdjustHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        Map<Object, Object> map = new HashMap<>();
        map.put("client", info.getClientId());
        map.put("praFlowNo", info.getWfOrder());

        List<GaiaPriceAdjustDto> gaiaPriceAdjusts = gaiaPriceAdjustMapper.selectByFlowNo(map);
        if (null == gaiaPriceAdjusts || gaiaPriceAdjusts.isEmpty()) {
            result.setCode(1002);
            result.setMessage("商品调价数据不存在");
            return result;
        }

        boolean flag = false;
        Integer today = Integer.valueOf(DateUtils.getCurrentDateStrYYMMDD());
        List<GaiaPriceAdjust> updateList = new ArrayList<>();
        List<GaiaSdProductPriceDto> productPrices = new ArrayList<>();
        for (GaiaPriceAdjustDto item : gaiaPriceAdjusts) {
            if ("2".equals(item.getPraApprovalStatus())) {
                flag = true;
                break;
            } else {

                GaiaPriceAdjust adjust = new GaiaPriceAdjust();
                adjust.setClient(item.getClient());
                adjust.setPraAdjustNo(item.getPraAdjustNo());
                adjust.setPraProduct(item.getPraProduct());
                adjust.setPraStore(item.getPraStore());

                if (ApprovalEnum.SUCCESS.getCode().equals(info.getWfStatus())) {
                    adjust.setPraApprovalStatus("2");
                    adjust.setPraApprovalDate(DateUtils.getCurrentDateStrYYMMDD());
                    adjust.setPraApprovalTime(DateUtils.getCurrentDateTimeStr("HHmmss"));
                    Integer mondy = Integer.valueOf(item.getPraStartDate());
                    if (mondy <= today) {
                        GaiaSdProductPriceDto productPrice = new GaiaSdProductPriceDto();
                        productPrice.setClient(info.getClientId());
                        productPrice.setGsppBrId(item.getPraStore());
                        productPrice.setGsppProId(item.getPraProduct());

                        //零售价
                        if (null != item.getPraPriceNormal()) {
                            //说明调过零售价了
                            productPrice.setGsppPriceNormal(item.getPraPriceNormal());
                        } else {

                            //调后没有价格 则 等于调前价格
                            if (null != item.getPraPriceNormalBefore()) {
                                //说明没调价格
                                productPrice.setGsppPriceNormal(item.getPraPriceNormalBefore());
                            } else {
                                //说明没有零售价 但是调最贵的价格

                                //选取其中最贵的一个价格
                                BigDecimal praPriceHy = null != item.getPraPriceHy() ? item.getPraPriceHy() : BigDecimal.ZERO;
                                BigDecimal gsppPriceYb = null != item.getPraPriceYb() ? item.getPraPriceYb() : BigDecimal.ZERO;
                                BigDecimal gsppPriceCl = null != item.getPraPriceCl() ? item.getPraPriceCl() : BigDecimal.ZERO;
                                BigDecimal praPriceOln = null != item.getPraPriceOln() ? item.getPraPriceOln() : BigDecimal.ZERO;
                                BigDecimal praPriceOlhy = null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : BigDecimal.ZERO;
                                BigDecimal max = BigDecimal.ZERO;
                                if (praPriceHy.compareTo(gsppPriceYb) > -1) {
                                    max = praPriceHy;
                                } else {
                                    max = gsppPriceYb;
                                }
                                if (gsppPriceCl.compareTo(max) > -1) {
                                    max = gsppPriceCl;
                                }

                                if (praPriceOln.compareTo(max) > -1) {
                                    max = praPriceOln;
                                }

                                if (praPriceOlhy.compareTo(max) > -1) {
                                    max = praPriceOlhy;
                                }
                                productPrice.setGsppPriceNormal(max);
                            }

                        }

                        //调后没有价格 则 等于调前价格
                        productPrice.setGsppPriceHy(null != item.getPraPriceHy() ? item.getPraPriceHy() : item.getPraPriceYhBefore());
                        productPrice.setGsppPriceYb(null != item.getPraPriceYb() ? item.getPraPriceYb() : item.getPraPriceYbBefore());
                        productPrice.setGsppPriceCl(null != item.getPraPriceCl() ? item.getPraPriceCl() : item.getPraPriceClBefore());
                        productPrice.setGsppPriceOnlineNormal(null != item.getPraPriceOln() ? item.getPraPriceOln() : item.getPraPriceOlnBefore());
                        productPrice.setGsppPriceOnlineHy(null != item.getPraPriceOlhy() ? item.getPraPriceOlhy() : item.getPraPriceOlhyBefore());
                        productPrice.setGsppPriceHyr(item.getGsppPriceHyr());

                        productPrices.add(productPrice);
                        adjust.setPraSendStatus("2");
                    }
                } else {
                    adjust.setPraSendStatus("3");
                }
                updateList.add(adjust);

            }
        }


        if (flag) {
            result.setCode(1003);
            result.setMessage("商品调价数据已审批");
            return result;
        }

        if (!productPrices.isEmpty()) {
            //全删全插
            gaiaSdProductPriceMapper.deleteList(productPrices);
            //更新商品价格
            gaiaSdProductPriceMapper.insertList(productPrices);

        }

        //更新商品調價專題状态
        gaiaPriceAdjustMapper.updateBatch(updateList);


        result.setCode(0);
        result.setMessage("执行成功");
        return result;

    }

    /**
     * 补充首营审批回调
     */
    private FeignResult replenishProductGspHandle(ApprovalInfo info) {
        FeignResult result = new FeignResult();
        String client = info.getClientId();
        String wfOrder = info.getWfOrder();
        List<GaiaProductGspinfo> gspinfoList = gaiaProductGspinfoMapper.selectProductGspByFlowNo(client, wfOrder);
        if (CollectionUtils.isEmpty(gspinfoList)) {
            result.setCode(1011);
            result.setMessage("该商品信息不存在");
            return result;
        }
        if (!CommonEnum.GspinfoStauts.APPROVAL.getCode().equals(gspinfoList.get(0).getProGspStatus())) {
            result.setCode(1003);
            result.setMessage("首营信息已审批");
            return result;
        }

        GaiaProductGspinfo gspinfo = gspinfoList.get(0);
        gspinfo.setProGspStatus(CommonEnum.GspinfoStauts.APPROVED.getCode()); // 已审批
        gaiaProductGspinfoMapper.updateByPrimaryKeySelective(gspinfo);
        updateProductPrice(gspinfo.getClient(), gspinfo.getProSite(), gspinfo.getProSelfCode(), gspinfo.getProLsj());

        // dc编码 列表
        List<GaiaDcData> dcList = gaiaProductBasicMapper.getDcListToUpdate(client, null);
        boolean isDC = dcList.stream().filter(Objects::nonNull).map(GaiaDcData::getDcCode).filter(Objects::nonNull).anyMatch(a -> a.equals(gspinfo.getProSite()));
        if (isDC) {
            // # 配送中心连锁总部下的门店 + 门店主数据地点相同的其他门店
            List<String> stoList = gaiaProductBusinessMapper.getStoreByDcAndAtoMdSite(client, gspinfo.getProSite());
            stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                updateProductPrice(client, stoCode, gspinfo.getProSelfCode(), gspinfo.getProLsj());
            });
        }

        // 门店编码 列表
        List<String> storeList = gaiaStoreDataMapper.getStoreCodeList(client);
        boolean isStore = storeList.stream().filter(Objects::nonNull).anyMatch(a -> a.equals(gspinfo.getProSite()));
        if (isStore) {
            // 门店主数据地点相同的其他门店
            List<String> stoList = gaiaProductBusinessMapper.getStoreByMdSite(client, gspinfo.getProSite());
            stoList.stream().filter(Objects::nonNull).distinct().forEach(stoCode -> {
                updateProductPrice(client, stoCode, gspinfo.getProSelfCode(), gspinfo.getProLsj());
            });
        }

        result.setCode(0);
        result.setMessage("执行成功");
        return result;
    }

    /**
     * 更新商品价格表
     *
     * @param client      加盟商
     * @param proSite     地点
     * @param proSelfCode 自编码
     * @param proLsj      参考零售价
     */
    private void updateProductPrice(String client, String proSite, String proSelfCode, BigDecimal proLsj) {
        if (ObjectUtils.isNotEmpty(proLsj) && proLsj.compareTo(BigDecimal.ZERO) > 0) {
            // 零售价联动
            GaiaSdProductPriceKey gaiaSdProductPriceKey = new GaiaSdProductPriceKey();
            gaiaSdProductPriceKey.setClient(client);
            gaiaSdProductPriceKey.setGsppBrId(proSite);
            gaiaSdProductPriceKey.setGsppProId(proSelfCode);
            GaiaSdProductPrice gaiaSdProductPrice = gaiaSdProductPriceMapper.selectByPrimaryKey(gaiaSdProductPriceKey);
            if (gaiaSdProductPrice == null) {
                gaiaSdProductPrice = new GaiaSdProductPrice();
                // 加盟商
                gaiaSdProductPrice.setClient(client);
                // 门店编码
                gaiaSdProductPrice.setGsppBrId(proSite);
                // 商品编码
                gaiaSdProductPrice.setGsppProId(proSelfCode);
                // 零售价
                gaiaSdProductPrice.setGsppPriceNormal(proLsj);
                gaiaSdProductPriceMapper.insertSelective(gaiaSdProductPrice);
            } else {
                gaiaSdProductPrice.setGsppPriceNormal(proLsj);
                gaiaSdProductPriceMapper.updateByPrimaryKeySelective(gaiaSdProductPrice);
            }
        }
    }


}
