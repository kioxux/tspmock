package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.List;

@Data
@EqualsAndHashCode
public class DistributionDTO {

    private String id;

    /**
     * 加盟商
     */
    private String clientId;

    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 计划类型 1-门店属性 2-门店类型 3-门店 4-店效级别
     */
    private int planType;

    /**
     * 计划天数
     */
    private int planDays;

    /**
     * 铺货方式：1-单次提取 2-多次提取
     */
    private int distributionType;
    /**
     * 计划铺货数量
     */
    private BigDecimal planQuantity;

    /**
     * 门店属性 选择列表
     */
    private List<DistributionDetailDTO> stoAttributesList;

    /**
     * 门店店型 选择列表
     */
    private List<DistributionDetailDTO> stoTypeList;

    /**
     * 门店id 选择列表
     */
    private List<DistributionDetailDTO> stoCodeList;

    /**
     * 店效级别 选择列表
     */
    private List<DistributionDetailDTO> stoEffectList;

    /**
     * 是否包含加盟店 0或空：不包含、 1：包含
     */
    private String isIncludeClient;
}
