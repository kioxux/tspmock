package com.gov.purchase.module.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "客户详情请求体")
public class CustomerInfoRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client", hidden = true)
    private String client;

    @ApiModelProperty(value = "客户编号")
    private String cusCode;

    @ApiModelProperty(value = "地点")
    private String cusSite;

    @ApiModelProperty(value = "客户编号（企业）")
    private String cusSelfCode;

}
