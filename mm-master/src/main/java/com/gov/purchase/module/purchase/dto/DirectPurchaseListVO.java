package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

@Data
@ApiModel(value = "要货查询返回参数")
public class DirectPurchaseListVO {
    /**
     * 补货门店
     */
    private String gsrhBrId;
    //门店号-门店名
    private String stoCodeName;
    //门店code
    private String stoCode;
    // 请货单号
    private String voucherCode;
    // 前序请货单号
    private String preVoucherCode;
    //要货日期
    private String gsrhDate;
    //商品编码
    private String gsrdProId;
    //税率
    private String taxRate;
    //补货数量
    private BigDecimal gsrdNeedQty;
    // 请货类型
    private String pattern;
    //请货行
    private String gsrdSerial;
    //请货行
    private String preGsrdSerial;
    //商品名称
    private String proName;
    //商品自编码
    private String proSelfCode;
    //规格
    private String proSpecs;
    //厂家
    private String proFactoryName;
    //单位
    private String proUnit;
    //中包装量
    private Integer proMidPackage;
    //零售价
    private BigDecimal gsppPriceNormal;
    //门店库存
    private Integer gssQty;
    //末次供应商
    private String lastSupplierCode;
    //末次进价
    private BigDecimal lastProPrice;
    //补货供应商
    private String supplierCode;
    private String supplierId;
    //采购单价
    private BigDecimal proPrice;
    //末次日期
    private String lastDate;
    //采购金额
    private BigDecimal purchaseAmount;
    //7天销售量
    private Integer sales7;
    //30天销售量
    private Integer sales30;
    //进项税
    private String proInputTax;
}
