package com.gov.purchase.module.base.service.impl.storeImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.mapper.GaiaAreaMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.excel.Store;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.21
 */
@Service
public class StoreImportAddImpl extends StoreImport {

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaAreaMapper gaiaAreaMapper;

    /**
     * 数据验证结果
     *
     * @param path 文件路径
     */
    @Override
    protected void checkEnd(String path) {
        // 生成导入日志
        createBatchLog(this.importType, path, CommonEnum.BulUpdateStatus.WAIT.getCode());
    }

    /**
     * 数据导入
     *
     * @param bulUpdateCode 批量操作编码
     * @param dataMap
     * @param <T>
     */
    @Override
    protected <T> Result importEnd(String bulUpdateCode, LinkedHashMap<Integer, T> dataMap) {
        // 批量导入
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Store store = (Store) dataMap.get(key);
            GaiaStoreData gaiaStoreData = new GaiaStoreData();
            BeanUtils.copyProperties(store, gaiaStoreData);
            // 是否医保店
            gaiaStoreData.setStoIfMedicalcare(store.getStoIfMedicalcareValue());
            // DTP
            gaiaStoreData.setStoIfDtp(store.getStoIfDtpValue());
            // 税分类
            gaiaStoreData.setStoTaxClass(store.getStoTaxClassValue());
            // 清空数据处理
            super.initNull(gaiaStoreData);
            //省
            gaiaStoreData.setStoProvince(store.getStoProvinceValue());
            //城市
            gaiaStoreData.setStoCity(store.getStoCityValue());
            //区/县
            gaiaStoreData.setStoDistrict(store.getStoDistrictValue());
            gaiaStoreDataMapper.insertSelective(gaiaStoreData);
        }
        // 更新导入日志
        editBatchLog(bulUpdateCode, CommonEnum.BulUpdateStatus.COMPLETE.getCode());
        return ResultUtil.success();
    }

    /**
     * 业务数据验证
     *
     * @param dataMap 数据
     * @param <T>
     * @return
     */
    @Override
    protected <T> Result businessValidate(LinkedHashMap<Integer, T> dataMap) {
        return super.checkList((LinkedHashMap<Integer, Store>) dataMap);
    }

    /**
     * 相关数据验证
     *
     * @param store
     * @param errorList
     * @param key
     */
    @Override
    protected void otherCheck(Store store, List<String> errorList, int key) {
        // 加盟商
        GaiaFranchisee gaiaFranchisee = gaiaFranchiseeMapper.selectByPrimaryKey(store.getClient());
        // 加盟商不存在
        if (gaiaFranchisee == null) {
            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "加盟商"));
        } else {
            // 门店编码验证
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(store.getClient());
            gaiaStoreDataKey.setStoCode(store.getStoCode());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            // 门店编码已存在
            if (gaiaStoreData != null) {
                errorList.add(MessageFormat.format(ResultEnum.E0134.getMsg(), key + 1, "门店编码"));
            }
            // 门店编码与DC重复
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(store.getClient());
            gaiaDcDataKey.setDcCode(store.getStoCode());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            // 门店编码已存在
            if (gaiaDcData != null) {
                errorList.add(MessageFormat.format(ResultEnum.E0134.getMsg(), key + 1, "门店编码"));
            }
        }

        // 省
        String province = store.getStoProvince();
        List<GaiaArea> gaiaAreaProvince = gaiaAreaMapper.selectByName(province);
        if (CollectionUtils.isEmpty(gaiaAreaProvince)) {
            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "省"));
            return;
        } else {
            store.setStoProvinceValue(gaiaAreaProvince.get(0).getAreaId());
        }
        // 城市
        String city = store.getStoCity();
        List<GaiaArea> gaiaAreaCity = gaiaAreaMapper.selectByName(city);
        if (CollectionUtils.isEmpty(gaiaAreaCity)) {
            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "城市"));
            return;
        } else {
            boolean flg = false;
            for (GaiaArea gaiaArea : gaiaAreaCity) {
                if (StringUtils.isNotBlank(gaiaArea.getParentId()) && gaiaArea.getParentId().equals(store.getStoProvinceValue())) {
                    store.setStoCityValue(gaiaArea.getAreaId());
                    flg = true;
                    break;
                }
            }
            if (!flg) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "城市"));
                return;
            }
        }
        // 区/县
        String district = store.getStoDistrict();
        List<GaiaArea> gaiaAreaDistrict = gaiaAreaMapper.selectByName(district);
        if (CollectionUtils.isEmpty(gaiaAreaDistrict)) {
            errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "区/县"));
            return;
        } else {
            boolean flg = false;
            for (GaiaArea gaiaArea : gaiaAreaDistrict) {
                if (StringUtils.isNotBlank(gaiaArea.getParentId()) && gaiaArea.getParentId().equals(store.getStoCityValue())) {
                    store.setStoDistrictValue(gaiaArea.getAreaId());
                    flg = true;
                    break;
                }
            }
            if (!flg) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "城市"));
                return;
            }
        }
        // 连锁总部判断
        // 门店属性
        String stoAttribute = store.getStoAttribute();
        // 连锁总部
        String stoChainHead = store.getStoChainHead();
        // 纳税主体
        String sto_tax_subject = store.getStoTaxSubject();
        // 非连锁门店
        if (!CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(stoAttribute)) {
            if (super.isNotBlank(stoChainHead)) {
                errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "非连锁门店不可以有连锁总部"));
                return;
            }
            if (super.isNotBlank(sto_tax_subject)) {
                errorList.add(MessageFormat.format(ResultEnum.E0159.getMsg(), key + 1, "非连锁门店不可以有纳税主体"));
                return;
            }
        }
        // 2-连锁
        if (CommonEnum.StoAttribute.ATTRIBUTE2.getCode().equals(stoAttribute)) {
            if (!super.isNotBlank(stoChainHead)) {
                errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "连锁总部"));
                return;
            }
            if (!super.isNotBlank(sto_tax_subject)) {
                errorList.add(MessageFormat.format(ResultEnum.E0107.getMsg(), key + 1, "纳税主体"));
                return;
            }
        }
        if (super.isNotBlank(stoChainHead)) {
            GaiaCompadm gaiaCompadm = gaiaCompadmMapper.selectByPrimaryId(store.getClient(), stoChainHead);
            if (gaiaCompadm == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "连锁总部"));
                return;
            }
        }
        if (super.isNotBlank(sto_tax_subject)) {
            if (!sto_tax_subject.equals(stoChainHead)) {
                GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
                gaiaStoreDataKey.setClient(store.getClient());
                gaiaStoreDataKey.setStoCode(sto_tax_subject);
                GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
                if (gaiaStoreData == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "纳税主体"));
                    return;
                }
            }
        }
    }
}
