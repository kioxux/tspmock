package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "供应商列表返回参数")
public class SupplierListForReturnsResponseDto {

    /**
     * 值
     */
    private String value;

    /**
     * 表示名
     */
    private String label;

    /**
     * 供应商编码
     */
    private String supCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 统一社会信用码
     */
    private String supCreditCode;

    /**
     * 供应商自编码
     */
    private String supSelfCode;
    /**
     * 助记码
     */
    private String supPym;
    /**
     * 地点
     */
    private String supSite;
}