package com.gov.purchase.module.delivery.dto;

import lombok.Data;

import java.util.List;

@Data
public class SaveGaiaAllotProductPriceDTO {
    /**
     * 类型 1 配送 2 批发
     */
    private Integer gapgType;
    /**
     * 价格组编号
     */
    private String gapgCode;
    private String alpReceiveSite;
    private List<GaiaAllotProductPriceVO> voList;
}
