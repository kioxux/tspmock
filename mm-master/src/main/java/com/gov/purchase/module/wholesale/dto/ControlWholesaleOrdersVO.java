package com.gov.purchase.module.wholesale.dto;

import com.gov.purchase.entity.GaiaWholesaleOwner;
import lombok.Data;

/**
 * @description: 批发订单按货主管控功能
 * @author: yzf
 * @create: 2021-11-23 14:11
 */
@Data
public class ControlWholesaleOrdersVO extends GaiaWholesaleOwner {

    /**
     * 商品数量
     */
    private String proCount;

    /**
     * 客户数量
     */
    private String cusCount;

    /**
     * 销售员名称
     */
    private String salesName;

}
