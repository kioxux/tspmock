package com.gov.purchase.module.goods.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
public class GetGspinfoDetailsDTO {

    @NotBlank(message = "加盟商不能为空")
    private String client;

    @NotBlank(message = "商品自编码不能为空")
    private String proSelfCode;

    @NotBlank(message = "地点不能为空")
    private String proSite;

    @NotBlank(message = "工作流编号")
    private String proFlowNo;
}
