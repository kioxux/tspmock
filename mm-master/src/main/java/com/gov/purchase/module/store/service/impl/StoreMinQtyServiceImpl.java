package com.gov.purchase.module.store.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.excel.ExportExcel;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaProductMinDisplay;
import com.gov.purchase.entity.GaiaProductMinDisplayKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.GaiaProductMinDisplayMapper;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.store.dto.store.StoreMinQtyExportDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtyExportSearchDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtyResultDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtySearchDto;
import com.gov.purchase.module.store.service.StoreMinQtyService;
import com.gov.purchase.utils.DateUtils;
import com.gov.purchase.utils.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StoreMinQtyServiceImpl implements StoreMinQtyService {

    @Resource
    private GaiaProductMinDisplayMapper gaiaProductMinDisplayMapper;

    @Resource
    FeignService feignService;

    /**
     * 查询
     *
     * @param dto
     * @return
     */
    @Override
    public PageInfo<StoreMinQtyResultDto> queryList(StoreMinQtySearchDto dto) {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        // 分頁查詢
        PageHelper.startPage(dto.getPageNum(), dto.getPageSize());
        return new PageInfo<>(gaiaProductMinDisplayMapper.queryPageList(dto));
    }

    /**
     * 新增
     */
    @Override
    public Result insertProductMinDisplay(GaiaProductMinDisplay gaiaProductMinDisplay) {
        TokenUser user = feignService.getLoginInfo();
        gaiaProductMinDisplay.setClient(user.getClient());
        if (StringUtils.isBlank(gaiaProductMinDisplay.getGpmdBrId())) {
            return ResultUtil.error("0001", "门店编码不能为空");
        }

        if (StringUtils.isBlank(gaiaProductMinDisplay.getGpmdProId())) {
            return ResultUtil.error("0001", "商品编码不能为空");
        }

        if (gaiaProductMinDisplay.getGpmdMinQty() == null) {
            return ResultUtil.error("0001", "最小陈列量不能为空");
        }

        GaiaProductMinDisplay existData = gaiaProductMinDisplayMapper.selectByPrimaryKey(gaiaProductMinDisplay);
        if (existData != null) {
            return ResultUtil.error("0002", "该门店的商品成列量已存在");
        }
        gaiaProductMinDisplay.setGpmdCreateDate(DateUtils.getCurrentDateStrYYMMDD());
        gaiaProductMinDisplay.setGpmdCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
        gaiaProductMinDisplay.setGpmdCreateUser(user.getUserId());
        gaiaProductMinDisplayMapper.insertSelective(gaiaProductMinDisplay);

        return ResultUtil.success();
    }

    /**
     * 更新
     */
    @Override
    public Result updateProductMinDisplay(GaiaProductMinDisplay gaiaProductMinDisplay) {
        TokenUser user = feignService.getLoginInfo();
        gaiaProductMinDisplay.setClient(user.getClient());
        if (StringUtils.isBlank(gaiaProductMinDisplay.getGpmdBrId())) {
            return ResultUtil.error("0001", "门店编码不能为空");
        }

        if (StringUtils.isBlank(gaiaProductMinDisplay.getGpmdProId())) {
            return ResultUtil.error("0001", "商品编码不能为空");
        }

        if (gaiaProductMinDisplay.getGpmdMinQty() == null) {
            return ResultUtil.error("0001", "最小陈列量不能为空");
        }
        GaiaProductMinDisplay existData = gaiaProductMinDisplayMapper.selectByPrimaryKey(gaiaProductMinDisplay);
        if (existData == null) {
            return ResultUtil.error("0003", "该门店的商品成列量不存在");
        }
        gaiaProductMinDisplay.setGpmdChangeDate(DateUtils.getCurrentDateStrYYMMDD());
        gaiaProductMinDisplay.setGpmdChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
        gaiaProductMinDisplay.setGpmdChangeUser(user.getUserId());
        gaiaProductMinDisplayMapper.updateByPrimaryKeySelective(gaiaProductMinDisplay);
        return ResultUtil.success();
    }

    /**
     * 删除
     *
     * @param list
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result deleteProductMinDisplay(List<GaiaProductMinDisplayKey> list) {
        TokenUser user = feignService.getLoginInfo();
        for (GaiaProductMinDisplayKey gaiaProductMinDisplayKey : list) {
            gaiaProductMinDisplayKey.setClient(user.getClient());
            if (StringUtils.isBlank(gaiaProductMinDisplayKey.getGpmdBrId())) {
                throw new CustomResultException("门店编码不能为空");
            }

            if (StringUtils.isBlank(gaiaProductMinDisplayKey.getGpmdProId())) {
                throw new CustomResultException("商品编码不能为空");
            }

            GaiaProductMinDisplay existData = gaiaProductMinDisplayMapper.selectByPrimaryKey(gaiaProductMinDisplayKey);
            if (existData == null) {
                throw new CustomResultException("该门店的商品成列量不存在");
            }

            gaiaProductMinDisplayMapper.deleteByPrimaryKey(gaiaProductMinDisplayKey);
        }
        return ResultUtil.success();
    }

    @Override
    public void exportExcel(StoreMinQtyExportSearchDto dto) throws IOException {
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        List<StoreMinQtyResultDto> list = gaiaProductMinDisplayMapper.queryExportList(dto);
        List<StoreMinQtyExportDto> exportList = new ArrayList<>();
        if (list.size() > 0) {
            exportList = list.stream().map(temp -> {
                StoreMinQtyExportDto exportDto = new StoreMinQtyExportDto();
                BeanUtils.copyProperties(temp, exportDto);
                return exportDto;
            }).collect(Collectors.toList());
        }
        ExportExcel excel = new ExportExcel("最小陈列量", StoreMinQtyExportDto.class, 1);
        excel.setDataList(exportList);
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletResponse response = servletRequestAttributes.getResponse();
        excel.write(response, "最小陈列量" + DateUtils.getCurrentDateTimeStr("yyyyMMddHHmmss") + ".xlsx");
    }

    @Override
    public Result saveProductMinDisplay(GaiaProductMinDisplay gaiaProductMinDisplay) {
        TokenUser user = feignService.getLoginInfo();
        gaiaProductMinDisplay.setClient(user.getClient());
        if (StringUtils.isBlank(gaiaProductMinDisplay.getGpmdBrId())) {
            return ResultUtil.error("0001", "门店编码不能为空");
        }

        if (StringUtils.isBlank(gaiaProductMinDisplay.getGpmdProId())) {
            return ResultUtil.error("0001", "商品编码不能为空");
        }

        if (gaiaProductMinDisplay.getGpmdMinQty() == null) {
            return ResultUtil.error("0001", "最小陈列量不能为空");
        }

        GaiaProductMinDisplay existData = gaiaProductMinDisplayMapper.selectByPrimaryKey(gaiaProductMinDisplay);
        if (existData != null) {
            gaiaProductMinDisplay.setGpmdChangeDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaProductMinDisplay.setGpmdChangeTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaProductMinDisplay.setGpmdChangeUser(user.getUserId());
            gaiaProductMinDisplayMapper.updateByPrimaryKeySelective(gaiaProductMinDisplay);
        } else {
            gaiaProductMinDisplay.setGpmdCreateDate(DateUtils.getCurrentDateStrYYMMDD());
            gaiaProductMinDisplay.setGpmdCreateTime(DateUtils.getCurrentTimeStrHHMMSS());
            gaiaProductMinDisplay.setGpmdCreateUser(user.getUserId());
            gaiaProductMinDisplayMapper.insertSelective(gaiaProductMinDisplay);
        }
        return ResultUtil.success();
    }
}
