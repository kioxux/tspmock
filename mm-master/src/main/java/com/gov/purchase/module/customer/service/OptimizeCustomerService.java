package com.gov.purchase.module.customer.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaCustomerBusiness;
import com.gov.purchase.entity.GaiaCustomerBusinessKey;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.module.base.dto.businessScope.EditScopeVO;
import com.gov.purchase.module.customer.dto.GetCusDetailsDTO;
import com.gov.purchase.module.customer.dto.GetCusListRequestDTO;
import com.gov.purchase.module.customer.dto.SaveCusList1RequestDTO;
import com.gov.purchase.module.customer.dto.SaveCusList2RequestDTO;
import com.gov.purchase.module.supplier.dto.GaiaCustomerBusinessDTO;

import java.util.List;

/**
 * 商品首营优化
 */
public interface OptimizeCustomerService {

     /**
      * 选择机构列表
      */
     List<GaiaDcData> getSiteList();

     /**
      * 回车查询
      */
     List<GaiaCustomerBusiness> enterSelect(String cusName, String cusCreditCode);

     /**
      * 保存1
      */
     List<GaiaCustomerBusiness> saveCusList1(List<SaveCusList1RequestDTO> dto);

     /**
      * 保存2
      */
     void saveCusList2(SaveCusList2RequestDTO dto);

     /**
      * 客户列表
      */
     PageInfo<GaiaCustomerBusinessDTO> getCusList(GetCusListRequestDTO dto);

     /**
      * 导出
      */
     Result exportCusList(GetCusListRequestDTO dto);

     /**
      * 批量编辑保存
      */
     void editCusList(List<GaiaCustomerBusiness> list, String remarks);

     /**
      * 详情
      */
     GetCusDetailsDTO getCusDetails(GaiaCustomerBusinessKey key);

     /**
      * 单条数据编辑保存
      */
     void editCus(GaiaCustomerBusiness business, String remarks);

     /**
      * 客户经营范围编辑
      */
     void editCusScope(EditScopeVO vo);
}
