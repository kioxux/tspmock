package com.gov.purchase.module.wholesale.service;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.wholesale.dto.DrugPurchaseDTO;
import com.gov.purchase.module.wholesale.dto.DrugPurchaseVo;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author: Yzf
 * description: TODO
 * create time: 2021/11/4 11:23
 */
public interface DrugPurchaseService {
    /**
     * 药品采购记录
     *
     * @param drugPurchaseDTO
     * @return
     */
    PageInfo<DrugPurchaseVo> queryDrugPurchaseRecord(DrugPurchaseDTO drugPurchaseDTO);


    /**
     * 采购员列表
     *
     * @param drugPurchaseDTO
     * @return
     */
    Result queryProPurchaseRate(DrugPurchaseDTO drugPurchaseDTO);

    /**
     * 药品采购记录导出
     *
     * @param drugPurchaseDTO
     * @return
     */
    Result exportDrugPurchaseRecord(DrugPurchaseDTO drugPurchaseDTO);
}
