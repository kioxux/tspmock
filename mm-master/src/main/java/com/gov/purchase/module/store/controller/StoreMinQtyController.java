package com.gov.purchase.module.store.controller;

import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.entity.GaiaProductMinDisplay;
import com.gov.purchase.entity.GaiaProductMinDisplayKey;
import com.gov.purchase.module.store.dto.store.StoreMinQtyExportSearchDto;
import com.gov.purchase.module.store.dto.store.StoreMinQtySearchDto;
import com.gov.purchase.module.store.service.StoreMinQtyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@Api(tags = "最小成列量维护")
@RestController
@RequestMapping("storeMinQty")
public class StoreMinQtyController {

    @Resource
    private StoreMinQtyService storeMinQtyService;

    @Log("最小成列量列表")
    @ApiOperation("最小成列量列表")
    @PostMapping("queryList")
    public Result queryList(@RequestBody StoreMinQtySearchDto dto) {
        return ResultUtil.success(storeMinQtyService.queryList(dto));
    }

    @Log("最小成列量添加")
    @ApiOperation("最小成列量添加")
    @PostMapping("insert")
    public Result insert(@RequestBody GaiaProductMinDisplay gaiaProductMinDisplay) {
        return storeMinQtyService.insertProductMinDisplay(gaiaProductMinDisplay);
    }


    @Log("最小成列量更新")
    @ApiOperation("最小成列量更新")
    @PostMapping("update")
    public Result update(@RequestBody GaiaProductMinDisplay gaiaProductMinDisplay) {
        return storeMinQtyService.updateProductMinDisplay(gaiaProductMinDisplay);
    }

    @Log("最小成列量保存")
    @ApiOperation("最小成列量保存")
    @PostMapping("save")
    public Result save(@RequestBody GaiaProductMinDisplay gaiaProductMinDisplay) {
        return storeMinQtyService.saveProductMinDisplay(gaiaProductMinDisplay);
    }

    @Log("最小成列量删除")
    @ApiOperation("最小成列量删除")
    @PostMapping("delete")
    public Result delete(@RequestBody List<GaiaProductMinDisplayKey> list) {
        return storeMinQtyService.deleteProductMinDisplay(list);
    }

    @Log("最小成列量导出")
    @ApiOperation("最小成列量导出")
    @PostMapping("export")
    public void export(@RequestBody StoreMinQtyExportSearchDto dto) {
        try {
            storeMinQtyService.exportExcel(dto);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }




}
