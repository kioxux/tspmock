package com.gov.purchase.module.purchase.service.impl;

import com.github.pagehelper.PageHelper;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.*;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.feign.dto.TokenUser;
import com.gov.purchase.mapper.*;
import com.gov.purchase.module.base.dto.feign.PurchaseFeignDto;
import com.gov.purchase.module.base.service.FeignService;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.IDirectPurchaseService;
import com.gov.purchase.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DirectPurchaseServiceImpl implements IDirectPurchaseService {
    @Resource
    private GaiaStoreDataMapper gaiaStoreDataMapper;
    @Resource
    private FeignService feignService;
    @Resource
    private GaiaSdReplenishDMapper gaiaSdReplenishDMapper;
    @Resource
    private GaiaSupplierBusinessMapper gaiaSupplierBusinessMapper;
    @Resource
    private GaiaProductBusinessMapper gaiaProductBusinessMapper;
    @Resource
    GaiaPoHeaderMapper gaiaPoHeaderMapper;
    @Resource
    GaiaPoItemMapper gaiaPoItemMapper;
    @Resource
    private GaiaSourceListMapper gaiaSourceListMapper;
    @Resource
    private CosUtils cosUtils;
    @Resource
    private RedisClient redisClient;

    @Override
    public List<DeliveryStoreResponseDto> selectStore() {
        DeliveryStoreRequestDto dto = new DeliveryStoreRequestDto();
        TokenUser user = feignService.getLoginInfo();
        dto.setClient(user.getClient());
        List<DeliveryStoreResponseDto> deliveryStore = gaiaStoreDataMapper.selectDeliveryStoreShort(dto);
        List<SupplierListForReturnsResponseDto> spList = gaiaSupplierBusinessMapper.selectSupplierListByClient(user.getClient());
        Map<String, List<SupplierListForReturnsResponseDto>> groupByPriceMap = spList.stream()
                .collect(Collectors.groupingBy(SupplierListForReturnsResponseDto::getSupSite));
        for (DeliveryStoreResponseDto deliveryStoreResponseDto : deliveryStore) {
            deliveryStoreResponseDto.setSupList(groupByPriceMap.get(deliveryStoreResponseDto.getValue()));
        }
        return deliveryStore;
    }

    @Override
    public List<DirectPurchaseListVO> selectList(DirectPurchaseListDTO directPurchaseListDTO) {
        TokenUser user = feignService.getLoginInfo();
        directPurchaseListDTO.setClient(user.getClient());
        if (directPurchaseListDTO.getProClassCode() != null && directPurchaseListDTO.getProClassCode().length > 0) {
            List<String> resultList = new ArrayList<>();
            for (String[] proClass : directPurchaseListDTO.getProClassCode()) {
                if (proClass != null && proClass.length > 0) {
                    resultList.addAll(Arrays.asList(proClass));
                }
            }
            if (CollectionUtils.isNotEmpty(resultList)) {
                directPurchaseListDTO.setProClassList(resultList.stream().distinct().collect(Collectors.toList()));
            }
        }
        if (StringUtils.isNotEmpty(directPurchaseListDTO.getProCodeStr())) {
            directPurchaseListDTO.setProCode(Arrays.asList(directPurchaseListDTO.getProCodeStr().split(",")));
        }
        if (StringUtils.isEmpty(directPurchaseListDTO.getPattern())) {
            directPurchaseListDTO.setPattern("0,1");
        }
        return gaiaSdReplenishDMapper.selectDirectPurchaseList(directPurchaseListDTO);
    }

    @Override
    @Transactional
    public void save(List<DirectPurchaseSaveDTO> voList) {
        if (voList.size() == 0) {
            throw new CustomResultException(ResultEnum.DC_REPLENISHMENT_EMPTY);
        }
        TokenUser user = feignService.getLoginInfo();
        String client = user.getClient();
        List<GaiaProductBusinessKey> gaiaProductBusinessKeyList = new ArrayList<>();
        for (DirectPurchaseSaveDTO directPurchaseListVO : voList) {
            GaiaProductBusinessKey gaiaProductBusinessKey = new GaiaProductBusinessKey();
            gaiaProductBusinessKey.setProSite(directPurchaseListVO.getStoCode());
            gaiaProductBusinessKey.setProSelfCode(directPurchaseListVO.getProSelfCode());
            gaiaProductBusinessKeyList.add(gaiaProductBusinessKey);
        }
        List<GaiaProductBusiness> gaiaProductBusinessList = gaiaProductBusinessMapper.selectProListNoDC(client, gaiaProductBusinessKeyList);
        if (CollectionUtils.isNotEmpty(gaiaProductBusinessList)) {
            for (GaiaProductBusiness gaiaProductBusiness : gaiaProductBusinessList) {
                // 检查 商品主数据 注册证效期（批准文号有效期）：小于60天-提示，不报错；过期-报错。
                /*if (gaiaDcData != null) {
                    // 是否管控证照效期：1-是
                    if (StringUtils.isNotBlank(gaiaDcData.getDcZzxqgk()) && gaiaDcData.getDcZzxqgk().equals("1") &&
                            StringUtils.isNotBlank(gaiaProductBusiness.getProRegisterExdate()) && gaiaProductBusiness.getProRegisterExdate().trim().length() == 8) {
                        String day = DateUtils.getCurrentDateStr("yyyyMMdd");
                        // 异常
                        if (day.compareTo(gaiaProductBusiness.getProRegisterExdate()) > 0) {
                            error.add("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号已过期");
                        }
                        // 警告
                        Long datediff = DateUtils.datediff(day, gaiaProductBusiness.getProRegisterExdate());
                        if (datediff.intValue() <= 60) {
                            warning.add("商品" + gaiaProductBusiness.getProSelfCode() + "批准文号即将过期");
                        }
                    }
                }*/
                //商品禁采检查
                if (CommonEnum.NoYesStatus.YES.getCode().equals(gaiaProductBusiness.getProNoPurchase())) {
                    throw new CustomResultException(MessageFormat.format(ResultEnum.PURCHASE_NOT_VALID.getMsg(), gaiaProductBusiness.getProSelfCode()));
                }
            }
        }

        /**
         * 将数据按照按照供应商分组
         */
        Map<String, List<DirectPurchaseSaveDTO>> mapList = voList.stream().collect(Collectors.groupingBy(o -> o.getSupplierCode() + "_*_" + (StringUtils.isBlank(o.getStoCode()) ? "" : o.getStoCode())));
        CommonUtils utils = new CommonUtils();
        for (Map.Entry<String, List<DirectPurchaseSaveDTO>> entry : mapList.entrySet()) {
            List<DirectPurchaseSaveDTO> itemList = entry.getValue();
            String supplierId = itemList.get(0).getSupplierId();
            if (StringUtils.isEmpty(supplierId)) {
                supplierId = entry.getKey().split("_")[0];
            }
            //GAIA_PO_HEADER主键重复检查
            GaiaPoHeaderKey gaiaPoHeaderKey = new GaiaPoHeaderKey();
            String poId = utils.getPoId(client, CommonEnum.PurchaseClass.PURCHASECD.getFieldName(), DateUtils.getCurrentDateStr("yyyy"), gaiaPoHeaderMapper, redisClient);
            gaiaPoHeaderKey.setClient(client);
            gaiaPoHeaderKey.setPoId(poId);

            GaiaPoHeader gaiaPoHeaderInfo = gaiaPoHeaderMapper.selectByPrimaryKey(gaiaPoHeaderKey);
            if (null != gaiaPoHeaderInfo) {
                throw new CustomResultException(ResultEnum.KEYREPEAT_ERROR);
            }
            //GAIA_PO_HEADER信息插入
            GaiaPoHeader gaiaPoHeader = new GaiaPoHeader();
            BeanUtils.copyProperties(gaiaPoHeaderKey, gaiaPoHeader);
            gaiaPoHeader.setPoType(CommonEnum.PurchaseClass.PURCHASECD.getFieldName());  // 订单类型 - Z001
            gaiaPoHeader.setPoSupplierId(supplierId); // 供应商自编码
            gaiaPoHeader.setPoSubjectType("1");  // 主体类型  1。连锁
            gaiaPoHeader.setPoDate(DateUtils.getCurrentDateStr("yyyyMMdd"));  // 凭证日期  当前日期
            gaiaPoHeader.setPoCompanyCode(itemList.get(0).getStoCode()); // 采购主体 - DC编码
            //gaiaPoHeader.setPoPaymentId(itemList.get(0).getSupPayTerm()); //付款条款
            gaiaPoHeader.setPoCreateBy(user.getUserId());
            gaiaPoHeader.setPoDeliveryTypeStore(itemList.get(0).getStoCode());
            gaiaPoHeader.setPoCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
            gaiaPoHeader.setPoCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
            gaiaPoHeader.setPoApproveStatus("0"); // 审批状态 - 不需要审批
            GaiaSupplierBusinessKey supplierBusinessKey = new GaiaSupplierBusinessKey();
            supplierBusinessKey.setClient(client);
            supplierBusinessKey.setSupSite(itemList.get(0).getStoCode());
            supplierBusinessKey.setSupSelfCode(supplierId);
            GaiaSupplierBusiness gaiaSupplierBusiness = gaiaSupplierBusinessMapper.selectByPrimaryKey(supplierBusinessKey);
            if (!ObjectUtils.isEmpty(gaiaSupplierBusiness)) {
                gaiaPoHeader.setPoLeadTime(gaiaSupplierBusiness.getSupLeadTime());
                gaiaPoHeader.setPoDeliveryMode(gaiaSupplierBusiness.getSupDeliveryMode());
                gaiaPoHeader.setPoCarrier(gaiaSupplierBusiness.getSupCarrier());
                gaiaPoHeader.setPoDeliveryTimes(gaiaSupplierBusiness.getSupDeliveryTimes());
            }
            gaiaPoHeaderMapper.insert(gaiaPoHeader);
            //查询是否需要审批
            //GaiaApproveRule gaiaApproveRule = gaiaApproveRuleMapper.selectByPrimaryKey(client, dcCode);
            int num = 1;
            // 采购审批集合
            List<PurchaseFeignDto> purchaseFeignDtoList = new ArrayList<>();
            for (DirectPurchaseSaveDTO item : itemList) {
                Integer approvalType = 0;  // 0-不需要审批， 1-单挑调增金额触发审批， 2-单价调增比例触发审批， 3-存销比触发审批
                //GAIA_PO_ITEM信息插入
                GaiaPoItem gaiaPoItem = new GaiaPoItem();
                BeanUtils.copyProperties(item, gaiaPoItem);
                gaiaPoItem.setClient(client);
                gaiaPoItem.setPoId(poId);
                gaiaPoItem.setPoSiteCode(item.getStoCode());
                // 行总价
                gaiaPoItem.setPoLineAmt(item.getProPrice().multiply(item.getGsrdNeedQty()));
                // 库位 1000 合格库
                gaiaPoItem.setPoLocationCode(CommonEnum.DictionaryStaticData.PO_LOCATION_CODE.getList().get(0).getValue());
                gaiaPoItem.setPoCompleteInvoice("0");
                gaiaPoItem.setPoCompletePay("0");
                gaiaPoItem.setPoLineDelete("0");
                gaiaPoItem.setPoPrice(item.getProPrice());
                gaiaPoItem.setPoProCode(item.getProSelfCode());
                gaiaPoItem.setPoQty(item.getGsrdNeedQty());
                gaiaPoItem.setPoUnit(item.getProUnit());
                GaiaProductBusiness gpb = gaiaProductBusinessMapper.getProductBusiness(client, item.getStoCode(), item.getProSelfCode());
                if (StringUtils.isNotEmpty(item.getProInputTax())) {
                    gaiaPoItem.setPoRate(item.getProInputTax());
                } else {
                    gaiaPoItem.setPoRate(gpb.getProInputTax());
                }
                gaiaPoItem.setPoLineNo(String.valueOf(num));
                // 前序订单单号
                gaiaPoItem.setPoPrePoid(item.getPreVoucherCode());
                // 前序订单行号
                gaiaPoItem.setPoPrePoitem((ObjectUtils.isEmpty(item.getPreGsrdSerial())) ? "" : item.getPreGsrdSerial());
                gaiaPoItem.setPoCompleteFlag("0");
                // 生产批号 PO_BATCH_NO
                gaiaPoItem.setPoBatchNo(gaiaPoItem.getPoBatchNo());
                // 生产日期 PO_SCRQ
                gaiaPoItem.setPoScrq(gaiaPoItem.getPoScrq());
                // 有效期 PO_YXQ
                gaiaPoItem.setPoYxq(gaiaPoItem.getPoYxq());
                gaiaPoItem.setPoDeliveryDate(DateUtils.getCurrentDateStrYYMMDD());
                gaiaPoItemMapper.insert(gaiaPoItem);
                // 将补货主表中的是否转采购订单字段值更新成：1-是
//                if (StringUtils.isNotBlank(item.getVoucherCode())) {
//                    GaiaSdReplenishH gaiaSdReplenishH = new GaiaSdReplenishH();
//                    //加盟商
//                    gaiaSdReplenishH.setClient(client);
//                    //补货门店
//                    gaiaSdReplenishH.setGsrhBrId(item.getStoCode());
//                    //补货单号
//                    gaiaSdReplenishH.setGsrhVoucherId(item.getVoucherCode());
//                    //补货日期
//                    gaiaSdReplenishH.setGsrhDate(item.getGsrhDate());
//                    gaiaSdReplenishH.setGsrhCreTime(DateUtils.getCurrentTimeStrHHMMSS());
//                    gaiaSdReplenishH.setGsrhFlag(CommonEnum.NoYesStatus.YES.getCode());
//                    gaiaSdReplenishHMapper.updateByPrimaryKeySelective(gaiaSdReplenishH);
//                }
                if (StringUtils.isNotBlank(item.getVoucherCode())) {
                    GaiaSdReplenishD gaiaSdReplenishD = new GaiaSdReplenishD();
                    // 加盟商
                    gaiaSdReplenishD.setClient(client);
                    // 补货门店
                    gaiaSdReplenishD.setGsrdBrId(item.getGsrhBrId());
                    // 补货单号
                    gaiaSdReplenishD.setGsrdVoucherId(item.getVoucherCode());
                    // 行号
                    gaiaSdReplenishD.setGsrdSerial(item.getGsrdSerial());
                    // 商品编码
                    gaiaSdReplenishD.setGsrdProId(item.getGsrdProId());
                    gaiaSdReplenishD.setGsrdFlag(CommonEnum.NoYesStatus.YES.getCode());
                    gaiaSdReplenishDMapper.updateByPrimaryKeySelective(gaiaSdReplenishD);
                }
                //行数自增
                num++;

                //检查货源清单表
                GaiaSourceList gaiaSource = new GaiaSourceList();
                gaiaSource.setClient(client);
                gaiaSource.setSouProCode(item.getProSelfCode());
                gaiaSource.setSouSiteCode(item.getStoCode());
                gaiaSource.setSouSupplierId(supplierId);
                List<GaiaSourceList> gaiaSourceList = gaiaSourceListMapper.selectSourceList(gaiaSource);
                //若无值则插入数据
                if (CollectionUtils.isEmpty(gaiaSourceList)) {
                    //GAIA_SOURCE_LIST插入数据
                    GaiaSourceList insertSourceList = new GaiaSourceList();
                    insertSourceList.setClient(client);
                    //货源清单编码
                    GaiaSourceList souListCodeInfo = gaiaSourceListMapper.selectNo(client, item.getProSelfCode(), item.getStoCode());
                    insertSourceList.setSouListCode(souListCodeInfo.getSouListCode());
                    insertSourceList.setSouProCode(item.getProSelfCode());
                    insertSourceList.setSouSiteCode(item.getStoCode());
                    insertSourceList.setSouSupplierId(supplierId);
                    insertSourceList.setSouEffectFrom(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouEffectEnd("20991231");
                    insertSourceList.setSouMainSupplier("0");
                    insertSourceList.setSouLockSupplier("0");
                    insertSourceList.setSouDeleteFlag("0");
                    insertSourceList.setSouCreateType("0");
                    insertSourceList.setSouCreateBy(user.getUserId());
                    insertSourceList.setSouCreateDate(DateUtils.getCurrentDateStr("yyyyMMdd"));
                    insertSourceList.setSouCreateTime(DateUtils.getCurrentTimeStr("HHmmss"));
                    gaiaSourceListMapper.insert(insertSourceList);
                }

                /*if (null != gaiaApproveRule) {

                    //采购价格平均值获取
                    BigDecimal unitPriceList = gaiaPoItemMapper.selectUnitPrice(item.getClient(), item.getDcCode(), item.getPoProCode());
                    //采购次数获取
                    List<GaiaPoItem> saleTime = gaiaPoItemMapper.selectSaleTime(item.getClient(), item.getDcCode(), item.getPoProCode());
                    BigDecimal timeNum = new BigDecimal(saleTime.size());
                    if (null == unitPriceList || unitPriceList.equals(BigDecimal.ZERO)) {
                        approvalType = 1;
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                    }
                    //本次采购价格
                    BigDecimal thisPrice = item.getPoPrice();
                    //单价增调值
                    if (unitPriceList != null && (thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 0
                            || thisPrice.subtract(unitPriceList).compareTo(gaiaApproveRule.getAprPriceAmt()) == 1)) {
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                        if (approvalType == 0) {
                            approvalType = 1;
                        }
                    }
                    //单价调增比例
                    if (unitPriceList != null && (thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 0
                            || thisPrice.subtract(unitPriceList).divide(timeNum, 4, RoundingMode.HALF_UP).compareTo(gaiaApproveRule.getAprPriceRate()) == 1)) {
                        if (approvalType == 0) {
                            approvalType = 3;
                        }
                    }
                    //总库存量
                    BigDecimal stock = BigDecimal.ZERO;
                    //30天销售总量
                    BigDecimal saleVolume = BigDecimal.ZERO;

                    if (params == null || params.length == 0) {
                        saleVolume = gaiaSoItemMapper.selectTotalSale(item.getClient(), item.getDcCode());
                    } else {
                        saleVolume = gaiaSdSaleDMapper.selectSaleNum(item.getClient(), item.getDcCode(), item.getPoProCode());
                    }
                    // 库存
                    GaiaMaterialAssessKey gaiaMaterialAssessKey = new GaiaMaterialAssessKey();
                    // 加盟商
                    gaiaMaterialAssessKey.setClient(item.getClient());
                    // 商品编码
                    gaiaMaterialAssessKey.setMatProCode(item.getPoProCode());
                    // 估价地点
                    gaiaMaterialAssessKey.setMatAssessSite(item.getDcCode());
                    GaiaMaterialAssess gaiaMaterialAssess = gaiaMaterialAssessMapper.selectByPrimaryKey(gaiaMaterialAssessKey);
                    if (gaiaMaterialAssess != null) {
                        stock = gaiaMaterialAssess.getMatTotalQty() == null ? BigDecimal.ZERO : gaiaMaterialAssess.getMatTotalQty();
                    }
                    //最新存销比
                    BigDecimal rate = new BigDecimal(0);
                    //30天销售总量为0
                    if (saleVolume.equals(BigDecimal.ZERO)) {
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                        if (approvalType == 0) {
                            approvalType = 3;
                        }
                    } else {
                        //最新存销比
                        rate = item.getPoQty().add(stock).divide(saleVolume, 4, RoundingMode.HALF_UP);
                        if (rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 0
                                || rate.compareTo(gaiaApproveRule.getAprStockUseRate()) == 1) {
//                        gaiaPoHeaderMapper.updateApproveStatus(item.getClient(), poId);
//                        break;
                            if (approvalType == 0) {
                                approvalType = 3;
                            }
                        }
                    }

                    if (approvalType > 0) {
                        //商品状态检查
                        GaiaProductBusiness status = gaiaProductBusinessMapper.selectGoodsStatus(client, item.getPoProCode(), dcCode);
                        GaiaProductBasic basic = gaiaProductBasicMapper.selectByPrimaryKey(status.getProCode());
                        PurchaseFeignDto purchaseFeignDto = new PurchaseFeignDto();
                        purchaseFeignDto.setProCode(item.getPoProCode()); // 商品编码
                        purchaseFeignDto.setProName(basic.getProName());  // 商品名
                        purchaseFeignDto.setProSpecs(basic.getProSpecs()); // 规格
                        purchaseFeignDto.setWfSite(item.getDcCode()); // 门店/部门/配送中心
                        if (unitPriceList != null) {
                            purchaseFeignDto.setPriceAverage(unitPriceList.setScale(2).toString());  // 5次均价
                        } else {
                            purchaseFeignDto.setPriceAverage("0.00");  // 5次均价
                        }
                        purchaseFeignDto.setPriceReal(item.getPoPrice().setScale(2).toString()); // 实际采购价
                        // 存销比
                        if (rate != null) {
                            purchaseFeignDto.setStockUseRate(rate.setScale(2).toString());
                        } else {
                            purchaseFeignDto.setStockUseRate("0.00");
                        }
                        if (approvalType == 1) {  // 1-单挑调增金额触发审批，
                            if (unitPriceList != null) {
                                purchaseFeignDto.setWarnValue(item.getPoPrice().subtract(unitPriceList).setScale(2).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        } else if (approvalType == 2) {  // 2-单价调增比例触发审批，
                            if (unitPriceList != null) {
                                purchaseFeignDto.setWarnValue(item.getPoPrice().subtract(unitPriceList).divide(unitPriceList, 4, BigDecimal.ROUND_HALF_UP).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        } else {  // 3-存销比触发审批
                            if (rate != null) {
                                purchaseFeignDto.setWarnValue(rate.subtract(gaiaApproveRule.getAprStockUseRate()).setScale(2).toString());
                            } else {
                                purchaseFeignDto.setWarnValue("0.00");
                            }
                        }
                        purchaseFeignDtoList.add(purchaseFeignDto);
                    }
                }*/
                //明细循环结束
            }

            // 需要审批 并提交审批流
            /*if (purchaseFeignDtoList.size() > 0) {
                int i = (int) ((Math.random() * 4 + 1) * 100000);
                gaiaPoHeader.setPoFlowNo(System.currentTimeMillis() + org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(i), 6, "0"));
                gaiaPoHeaderMapper.updateApproveStatus(client, poId, gaiaPoHeader.getPoFlowNo());
                FeignResult result = feignService.createPurchaseWorkflow(gaiaPoHeader, purchaseFeignDtoList);
                if (result.getCode() != 0) {
                    throw new CustomResultException(result.getMessage());
                }
            }*/
        }
    }

    @Override
    public List<DirectPurchaseSummaryListVO> selectSummaryList(DirectPurchaseListDTO directPurchaseListDTO) {
        List<DirectPurchaseListVO> voList = selectList(directPurchaseListDTO);
        List<DirectPurchaseSummaryListVO> resultList = new ArrayList<>();
        Map<String, List<DirectPurchaseListVO>> groupByPriceMap = voList.stream()
                .collect(Collectors.groupingBy(DirectPurchaseListVO::getVoucherCode));
        for (Map.Entry<String, List<DirectPurchaseListVO>> entry : groupByPriceMap.entrySet()) {
            Integer gsrdSerialTotal = 0;
            BigDecimal gsrdNeedQtyTotal = BigDecimal.ZERO;
            DirectPurchaseSummaryListVO rVO = new DirectPurchaseSummaryListVO();
            BeanUtils.copyProperties(entry.getValue().get(0), rVO);
            for (DirectPurchaseListVO resultVO : entry.getValue()) {
                gsrdNeedQtyTotal = gsrdNeedQtyTotal.add(resultVO.getGsrdNeedQty());
            }
            rVO.setGsrdSerialTotal(entry.getValue().size());
            rVO.setGsrdNeedQtyTotal(gsrdNeedQtyTotal);
            rVO.setVoList(entry.getValue());
            resultList.add(rVO);
        }
        return resultList;
    }

    @Override
    public Result export(List<DirectPurchaseSaveDTO> voList) {
        try {
            TokenUser user = feignService.getLoginInfo();
            PageHelper.clearPage();
            List<List<String>> dataList = new ArrayList<>();
            // 配送平均天数  默认为 3天
            for (DirectPurchaseSaveDTO dto : voList) {
                // 打印行
                List<String> item = new ArrayList<>();
                // 门店号
                item.add(dto.getStoCodeName());
                // 商品编码
                item.add(dto.getGsrdProId());
                // 商品名称
                item.add(dto.getProName());
                // 规格
                item.add(dto.getProSpecs());
                // 厂家
                item.add(dto.getProFactoryName());
                // 单位
                item.add(dto.getProUnit());
                //补货数量
                item.add((ObjectUtils.isEmpty(dto.getGsrdNeedQty())) ? "" : dto.getGsrdNeedQty().toString());
                //门店库存
                item.add((ObjectUtils.isEmpty(dto.getGssQty())) ? "" : dto.getGssQty().toString());
                // 补货供应商
                item.add(dto.getSupplierCode());
                // 采购单价
                item.add((ObjectUtils.isEmpty(dto.getProPrice())) ? "" : dto.getProPrice().toString());
                // 采购金额
                item.add((ObjectUtils.isEmpty(dto.getPurchaseAmount())) ? "" : dto.getPurchaseAmount().toString());
                // 零售价
                item.add((ObjectUtils.isEmpty(dto.getGsppPriceNormal())) ? "" : dto.getGsppPriceNormal().toString());
                // 要货日期
                item.add(dto.getGsrhDate());
                // 7天销售量
                item.add((ObjectUtils.isEmpty(dto.getSales7())) ? "" : dto.getSales7().toString());
                // 30天门店销售量
                item.add((ObjectUtils.isEmpty(dto.getSales30())) ? "" : dto.getSales30().toString());
                // 中包装量
                item.add((ObjectUtils.isEmpty(dto.getProMidPackage())) ? "" : dto.getProMidPackage().toString());
                // 末次供应商
                item.add(dto.getLastSupplierCode());
                // 末次进价
                item.add((ObjectUtils.isEmpty(dto.getLastProPrice())) ? "" : dto.getLastProPrice().toString());
                // 末次日期
                item.add(dto.getLastDate());
                // 请货单号
                item.add(dto.getVoucherCode());
                dataList.add(item);
            }
            HSSFWorkbook workbook = ExcelUtils.exportExcel(
                    new ArrayList<String[]>() {{
                        add(headList);
                    }},
                    new ArrayList<List<List<String>>>() {{
                        add(dataList);
                    }},
                    new ArrayList<String>() {{
                        add("补货");
                    }});
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            workbook.write(bos);
            Result result = cosUtils.uploadFile(bos, String.valueOf(System.currentTimeMillis()).concat(".xls"));
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return ResultUtil.error(ResultEnum.E0155);
        }

    }

    /**
     * 导出数据表头
     */
    private String[] headList = {
            "门店号",
            "商品编码",
            "商品名称",
            "规格",
            "厂家",
            "单位",
            "补货数量",
            "门店库存",
            "补货供应商",
            "采购单价",
            "采购金额",
            "零售价",
            "要货日期",
            "7天销售量",
            "30天销售量",
            "中包装量",
            "末次供应商",
            "末次进价",
            "末次日期",
            "请货单号"
    };
}
