package com.gov.purchase.module.purchase.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.12.08
 */
@Data
public class SupplierPaymentAtt {

    /**
     * 加盟商
     */
    private String client;

    /**
     * 单号
     */
    private String payOrderId;

    /**
     * 供应商编号
     */
    private String supSelfCode;

    /**
     * 供应商名称
     */
    private String supName;

    /**
     * 开户行行号
     */
    private String supBankCode;

    /**
     * 开户行名称
     */
    private String supBankName;

    /**
     * 开户行账号
     */
    private String supBankAccount;

    /**
     * 采购订单号
     */
    private List<String> poIds;
}
