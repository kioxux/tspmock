package com.gov.purchase.module.purchase.controller;

import com.github.pagehelper.PageInfo;
import com.gov.purchase.common.log.Log;
import com.gov.purchase.common.redis.jedis.RedisClient;
import com.gov.purchase.common.request.RequestJson;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.mapper.GaiaPoHeaderMapper;
import com.gov.purchase.module.goods.dto.GaiaPoItemList;
import com.gov.purchase.module.purchase.dto.*;
import com.gov.purchase.module.purchase.service.OrderPurchaseService;
import com.gov.purchase.utils.CommonUtils;
import com.gov.purchase.utils.DateUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Api(tags = "采购订单处理")
@RestController
@RequestMapping("purchase")
public class OredrPurchaseController {
    private Logger logger = LoggerFactory.getLogger(OredrPurchaseController.class);
    @Resource
    private OrderPurchaseService orderPurchaseService;

    @Resource
    private GaiaPoHeaderMapper gaiaPoHeaderMapper;

    @Resource
    private RedisClient redisClient;

    /*
     * 采购订单创建
     *
     * @param dto QueryUnDrugSupCheckRequestDto
     * @return
     */
    @Log("采购订单创建")
    @ApiOperation("采购订单创建")
    @PostMapping("createPurchaseOrder")
    public Result resultCreatePurchaseOrder(@Valid @RequestBody CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto) {
        logger.info("采购订单创建数据：{}", createPurchaseOrderRequestDto);
        // 过滤数据
        List<GaiaPoItemList> gaiaPoItemList = createPurchaseOrderRequestDto.getGaiaPoItemList();
        //集中检查采购单
        Result checkResult = orderPurchaseService.checkPurchaseOrder(createPurchaseOrderRequestDto);
        if (!String.valueOf(0).equals(checkResult.getCode())) {
            // 不成功直接返回
            return checkResult;
        }
        //拿出单独的冷藏商品
        List<GaiaPoItemList> coldStorageProductList = gaiaPoItemList.stream().filter(x -> String.valueOf(3).equals(x.getProStorageCondition())).collect(Collectors.toList());
        List<GaiaPoItemList> normalStorageProductList = gaiaPoItemList.stream().filter(x -> !String.valueOf(3).equals(x.getProStorageCondition())).collect(Collectors.toList());
        logger.info("冷藏商品：{}", coldStorageProductList);
        // 插入普通采购单
        String normalOrderCode = null;
        if (!normalStorageProductList.isEmpty()) {
            createPurchaseOrderRequestDto.setGaiaPoItemList(normalStorageProductList);
            Result purchaseOrder = orderPurchaseService.insertPurchaseOrder(createPurchaseOrderRequestDto);
            if (purchaseOrder != null && purchaseOrder.getData() != null) {
                normalOrderCode = purchaseOrder.getData().toString();
            }
        }
        // 插入冷藏采购单
        String coldOrderCode = null;
        if (!coldStorageProductList.isEmpty()) {
            createPurchaseOrderRequestDto.setGaiaPoItemList(coldStorageProductList);
            Result coldInsertPurchaseOrder = orderPurchaseService.insertPurchaseOrder(createPurchaseOrderRequestDto);
            //采购订单创建
            if (coldInsertPurchaseOrder != null && coldInsertPurchaseOrder.getData() != null) {
                coldOrderCode = coldInsertPurchaseOrder.getData().toString();
            }
        }
        Result result = new Result();
        String resultCold = null;
        if (StringUtils.isNotBlank(normalOrderCode)) {
            resultCold = normalOrderCode;
        }
        if (StringUtils.isNotBlank(coldOrderCode) && StringUtils.isNotBlank(resultCold)) {
            resultCold = resultCold + "," + coldOrderCode;
        } else {
            if (StringUtils.isNotBlank(coldOrderCode)) {
                resultCold = coldOrderCode;
            }
        }
        result.setData(resultCold);
        result.setCode("0");
        return result;
    }

    @Log("暂存数据保存")
    @ApiOperation("暂存数据保存")
    @PostMapping("temporaryToSave")
    public Result temporaryToSave(@Valid @RequestBody CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto) {
        //logger.info("商品分单入参：{}", createPurchaseOrderRequestDto.getGaiaPoItemList());
        //分离数据
        List<GaiaPoItemList> gaiaPoItemList = createPurchaseOrderRequestDto.getGaiaPoItemList();
        List<GaiaPoItemList> coldStorageProductList = gaiaPoItemList.stream().filter(x -> String.valueOf(3).equals(x.getProStorageCondition())).collect(Collectors.toList());
        List<GaiaPoItemList> normalStorageProductList = gaiaPoItemList.stream().filter(x -> !String.valueOf(3).equals(x.getProStorageCondition())).collect(Collectors.toList());
        logger.info("冷藏商品：{}", coldStorageProductList);
        //插入非冷藏商品
        String normalOrderCode = null;
        if (!normalStorageProductList.isEmpty()) {
            createPurchaseOrderRequestDto.setGaiaPoItemList(normalStorageProductList);
            Result nomalresult = orderPurchaseService.temporaryToSave(createPurchaseOrderRequestDto);
            normalOrderCode = nomalresult.getData().toString();
        }
        //插入冷藏商品
        String coldOrderCode = null;
        if (!coldStorageProductList.isEmpty()) {
            String poId = new CommonUtils().getPoId(createPurchaseOrderRequestDto.getClient(), createPurchaseOrderRequestDto.getPoType(),
                    DateUtils.getCurrentDateStr("yyyy"), gaiaPoHeaderMapper, redisClient);
            createPurchaseOrderRequestDto.setPoId(poId);
            createPurchaseOrderRequestDto.setGaiaPoItemList(coldStorageProductList);
            Result coldresult = orderPurchaseService.temporaryToSave(createPurchaseOrderRequestDto);
            coldOrderCode = coldresult.getData().toString();
        }

        Result result = new Result();
        String resultCold = null;
        if (StringUtils.isNotBlank(normalOrderCode)) {
            resultCold = normalOrderCode;
        }
        if (StringUtils.isNotBlank(coldOrderCode) && StringUtils.isNotBlank(resultCold)) {
            resultCold = resultCold + "," + coldOrderCode;
        } else {
            if (StringUtils.isNotBlank(coldOrderCode)) {
                resultCold = coldOrderCode;
            }
        }
        result.setData(resultCold);
        result.setCode("0");
        return result;
    }


    @Log("暂存")
    @ApiOperation("暂存")
    @PostMapping("temporaryStoragePurchaseOrder")
    public Result temporaryStoragePurchaseOrder(@Valid @RequestBody CreatePurchaseOrderRequestDto createPurchaseOrderRequestDto) {
        return orderPurchaseService.temporaryStoragePurchaseOrder(createPurchaseOrderRequestDto);
    }

    @Log("暂存明细详情")
    @ApiOperation("暂存明细详情")
    @PostMapping("temporaryDetails")
    public Result temporaryDetails(@RequestJson(value = "poId", name = "采购凭证号") String poId) {
        return ResultUtil.success(orderPurchaseService.temporaryDetails(poId));
    }

    @Log("暂存列表")
    @ApiOperation("暂存列表")
    @PostMapping("temporaryList")
    public Result temporaryList() {
        return ResultUtil.success(orderPurchaseService.temporaryList());
    }

    @Log("暂存删除")
    @ApiOperation("暂存删除")
    @PostMapping("temporaryRemove")
    public Result temporaryRemove(@RequestJson(value = "poId", name = "采购凭证号") String poId) {
        orderPurchaseService.temporaryRemove(poId);
        return ResultUtil.success();
    }

    @Log("采购订单删除")
    @ApiOperation("采购订单删除")
    @PostMapping("delPurchase")
    public Result delPurchase(@RequestJson(value = "poId", name = "采购单号") String poId) {
        orderPurchaseService.delPurchase(poId);
        return ResultUtil.success();
    }

    @Log("采购订单后续")
    @ApiOperation("采购订单后续")
    @PostMapping("getPoFollow")
    public Result getPoFollow(@RequestJson(value = "poId", name = "采购单号") String poId) {
        return orderPurchaseService.getPoFollow(poId);
    }

    /**
     * 历史采购记录
     *
     * @return
     */
    @Log("历史采购记录")
    @ApiOperation("历史采购记录")
    @PostMapping("selectHisPo")
    public Result selectHisPo(@RequestJson(value = "pageSize", name = "分页") Integer pageSize,
                              @RequestJson(value = "pageNum", name = "分页") Integer pageNum,
                              @RequestJson(value = "proSelfCode", name = "商品编码") String proSelfCode) {
        return orderPurchaseService.selectHisPo(proSelfCode, pageSize, pageNum);
    }

    /**
     * 供应商列表
     *
     * @param dto SupplierListForPurchaseRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("供应商列表")
    @ApiOperation("供应商列表")
    @PostMapping("getSupplierListForPurchase")
    public Result resultGetSupplierListForPurchase(@Valid @RequestBody SupplierListForPurchaseRequestDto dto) {
        //供应商列表搜索
        List<SupplierListForPurchaseResponseDto> supplierList = orderPurchaseService.selectSupplierListForPurchaseo(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 供应商列表
     *
     * @param dto SupplierListForPurchaseRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("供应商列表")
    @ApiOperation("供应商列表")
    @PostMapping("getSupplierListAndDcForPurchase")
    public Result resultGetSupplierListAndDCForPurchase(@Valid @RequestBody SupplierListForPurchaseRequestDto dto) {
        //供应商列表搜索
        List<SupplierListForPurchaseResponseDto> supplierList = orderPurchaseService.resultGetSupplierListAndDCForPurchase(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 商品列表
     *
     * @param dto GgetProductListRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("商品列表")
    @ApiOperation("商品列表")
    @PostMapping("getProductList")
    public Result resultGetProductList(@Valid @RequestBody GetProductListRequestDto dto) {
        //商品列表搜索
        List<GetProductListResponseDto> supplierList = orderPurchaseService.selectGetProductList(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 商品列表
     *
     * @param dto GgetProductListRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("商品列表")
    @ApiOperation("商品列表")
    @PostMapping("getProductListPage")
    public Result getProductListPage(@Valid @RequestBody GetProductListRequestDto dto) {
        //商品列表搜索
        return ResultUtil.success(orderPurchaseService.getProductListPage(dto));
    }

    /**
     * 商品列表
     *
     * @param dto GgetProductListRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("商品列表")
    @ApiOperation("商品列表")
    @PostMapping("getProductListForAdjustment")
    public Result getProductListForAdjustment(@Valid @RequestBody GetProductListRequestDto dto) {
        //商品列表搜索
        List<GetProductListResponseDto> supplierList = orderPurchaseService.getProductListForAdjustment(dto);
        return ResultUtil.success(supplierList);
    }

    @Log("商品列表_弹出框")
    @ApiOperation("商品列表_弹出框")
    @PostMapping("getProductListForAdjustmentPopup")
    public Result getProductListForAdjustmentPopup(@Valid @RequestBody GetProductListRequestDto dto) {
        //商品列表搜索
        return ResultUtil.success(orderPurchaseService.getProductListForAdjustmentPopup(dto));
    }

    /**
     * 地点集合
     *
     * @param dto GetSiteListRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("地点集合")
    @ApiOperation("地点集合")
    @PostMapping("getSiteList")
    public Result resultGetSiteList(@Valid @RequestBody GetSiteListRequestDto dto) {
        //地点集合搜索
        List<GetSiteListResponseDto> supplierList = orderPurchaseService.selectGetSiteList(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 地点集合
     *
     * @param dto GetSiteListRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("地点集合")
    @ApiOperation("地点集合")
    @PostMapping("getSiteList1")
    public Result resultGetSiteList1(@Valid @RequestBody GetSiteListRequestDto dto) {
        //地点集合搜索
        List<GetSiteListResponseDto> supplierList = orderPurchaseService.selectGetSiteList1(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 门店选择
     *
     * @param dto StoreSelectRequestDto
     * @return List<QuerySupListResponseDto>
     */
    @Log("门店选择")
    @ApiOperation("门店选择")
    @PostMapping("getStoreSelect")
    public Result resultStoreSelect(@Valid @RequestBody StoreSelectRequestDto dto) {

        //地点集合搜索
        List<GetSiteListResponseDto> supplierList = orderPurchaseService.StoreSelect(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 客户信息
     *
     * @param client 加盟商
     * @param site   地点
     * @return
     */
    @Log("客户信息")
    @ApiOperation("客户信息")
    @PostMapping("customerInfo")
    public Result customerInfo(@RequestJson("client") String client, @RequestJson("site") String site) {
        return orderPurchaseService.customerInfo(client, site);
    }

    /**
     * 采购订单查询
     *
     * @param dto GetSiteListRequestDto
     * @return PageInfo<QuerySupListResponseDto>
     */
    @Log("采购订单查询")
    @ApiOperation("采购订单查询")
    @PostMapping("queryPoHeaderList")
    public Result resultPoHeaderList(@Valid @RequestBody PoHeaderListRequestDto dto) {
        //地点集合搜索
        PageInfo<PoHeaderListResponseDto> supplierList = orderPurchaseService.selectPoHeaderList(dto);
        return ResultUtil.success(supplierList);
    }

    @Log("采购订单主表查询")
    @ApiOperation("采购订单主表查询")
    @PostMapping("getPoHeaderList")
    public Result getPoHeaderList(@RequestBody PoHeaderListRequestDto dto) {
        return ResultUtil.success(orderPurchaseService.getPoHeaderList(dto));
    }

    /**
     * 导出 采购订单
     *
     * @param dto
     * @return
     */
    @Log("导出 采购订单")
    @ApiOperation("导出 采购订单")
    @PostMapping("exportList")
    public Result exportList(@RequestBody PoHeaderListRequestDto dto) {
        return orderPurchaseService.exportList(dto);
    }

    /**
     * 采购订单明细
     *
     * @param dto PoItemRequestDto
     * @return PoItemResponseDto
     */
    @Log("采购订单明细")
    @ApiOperation("采购订单明细")
    @PostMapping("queryPoItem")
    public Result resultPoItem(@Valid @RequestBody PoItemRequestDto dto) {
        //采购订单明细
        PoItemResponseDto supplierList = orderPurchaseService.selectPoItemList(dto);
        return ResultUtil.success(supplierList);
    }

    /**
     * 后续凭证
     *
     * @param dto FuCertificateRequestDto
     * @return PoItemResponseDto
     */
    @Log("后续凭证")
    @ApiOperation("后续凭证")
    @PostMapping("fuCertificate")
    public Result resultFuCertificate(@Valid @RequestBody FuCertificateRequestDto dto) {

        //地点集合搜索
        List<FuCertificateResponseDto> supplierList = orderPurchaseService.selectFuCertificate(dto);
        return ResultUtil.success(supplierList);
    }

    /*
     * 采购订单保存
     *
     * @param dto QueryUnDrugUpdateRequestDto
     * @return
     */
    @Log("采购订单保存")
    @ApiOperation("采购订单保存")
    @PostMapping("savePurchaseOrder")
    public Result resultSavePurchaseOrder(@Valid @RequestBody SavePurchaseOrderRequestDto dto) {

        // 采购订单保存
        orderPurchaseService.updateSavePurchaseOrder(dto);

        return ResultUtil.success();
    }

    @Log("采购订单打印信息")
    @ApiOperation("采购订单打印信息")
    @PostMapping("getPurchaseOrderInfo")
    public Result getPurchaseOrderInfo(@Valid @RequestBody PoItemRequestDto dto) {
        return ResultUtil.success(orderPurchaseService.getPurchaseOrderInfo(dto));
    }

    @Log("供应商付款附件生成")
    @ApiOperation("供应商付款附件生成")
    @PostMapping("supplierPaymentAtt")
    public Result supplierPaymentAtt(@RequestBody SupplierPaymentAtt supplierPaymentAtt) throws IOException {
        orderPurchaseService.supplierPaymentAtt(supplierPaymentAtt);
        return ResultUtil.success();
    }

    /**
     * 检查同品在途订单
     *
     * @param proSelfCode   商品编码
     * @param poCompanyCode 采购主体
     * @return
     */
    @Log("检查同品在途订单")
    @ApiOperation("检查同品在途订单")
    @PostMapping("checkExistOrder")
    public Result checkExistOrder(@RequestJson("proSelfCode") String proSelfCode, @RequestJson("poCompanyCode") String poCompanyCode) {
        return orderPurchaseService.checkExistOrder(proSelfCode, poCompanyCode);
    }

    /**
     * 采购订单创建，或者仓库补货生成采购订单是否允许同一张订单中有相同的多行商品（配置表查询  0：允许，1：不允许）
     *
     * @return
     */
    @Log("生成采购订单是否允许同一张订单中有相同的多行商品")
    @ApiOperation("生成采购订单是否允许同一张订单中有相同的多行商品")
    @PostMapping("queryPoConfig")
    public Result queryPoConfig() {
        return ResultUtil.success(orderPurchaseService.queryPoConfig());
    }

    @Log("凭证日期是否可以更改")
    @ApiOperation("凭证日期是否可以更改")
    @PostMapping("selectPoDateChange")
    public Result selectPoDateChange() {
        return ResultUtil.success(orderPurchaseService.selectPoDateChange());
    }
}
