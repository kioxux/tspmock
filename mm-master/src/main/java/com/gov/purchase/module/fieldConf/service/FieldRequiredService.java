package com.gov.purchase.module.fieldConf.service;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.module.fieldConf.dto.FieldRequiredConfDTO;

/**
 * @author yzf
 * 首营字段配置以及必填字段
 */
public interface FieldRequiredService {

    /**
     * 首营字段配置列表
     *
     * @return
     */
    Result queryFieldRequiredDataList();

    /**
     * 首营必填字段列表
     *
     * @return
     */
    Result queryFieldRequiredConfList();

    /**
     * 更新首营必填字段
     *
     * @param fieldConfDTO
     * @return
     */
    void updateFieldRequiredConf(FieldRequiredConfDTO fieldConfDTO);
}
