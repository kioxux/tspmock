package com.gov.purchase.module.store.dto.price;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2021.01.13
 */
@Data
public class StoreProductDto {
    /**
     * 商品编码
     */
    private String proSelfCode;

    /**
     * 商品名
     */
    private String proName;

    /**
     * 通用名称
     */
    private String proCommonname;

    /**
     * 规格
     */
    private String proSpecs;

    /**
     * 生产厂家
     */
    private String proFactoryName;

    /**
     * 单位
     */
    private String proUnit;

    /**
     * 门店库存
     */
    private BigDecimal gssQty;

    /**
     * 定位
     */
    private String proPosition;

    /**
     * 是否医保
     */
    private String proIfMed;

    /**
     * 零售价
     */
    private BigDecimal gsppPriceNormal;

    /**
     * 新零售价
     */
    private BigDecimal newGsppPriceNormal;

    /**
     * 有效期起
     */
    private String prcEffectDate;

    /**
     * 审批状态
     */
    private String prcApprovalSuatus;

    /**
     * 调前价
     */
    private BigDecimal prcAmountBefore;

    /**
     * 调后价
     */
    private BigDecimal prcAmount;

    /**
     * 调价来源
     */
    private String prcSource;

    /**
     * 调价原因
     */
    private String prcReason;

    /**
     * 发起门店
     */
    private String prcInitStore;

    /**
     * 发起门店名
     */
    private String prcInitStoreName;

    /**
     * 国际条形码
     */
    private String proBarcode;

    /**
     * 会员价
     */
    private BigDecimal prcAmountHy;

    /**
     * 助记码
     */
    private String proPym;

    /**
     * 剂型
     */
    private String proForm;

    /**
     * 产地
     */
    private String proPlace;

    /**
     * 销售级别
     */
    private String proSlaeClass;
}

