package com.gov.purchase.module.wholesale.dto;

import lombok.Data;

/**
 * @description: 批发公司
 * @author: yzf
 * @create: 2021-11-17 17:06
 */
@Data
public class PurchaseCompanyDTO {

    private String client;

    /**
     * 采购主体id
     */
    private String poCompanyCode;

    /**
     * 仓库名
     */
    private String dcName;

    /**
     * 门店名
     */
    private String stoName;
}
