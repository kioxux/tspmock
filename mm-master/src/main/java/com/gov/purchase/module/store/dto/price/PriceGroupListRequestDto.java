package com.gov.purchase.module.store.dto.price;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
@ApiModel(value = "零售价列表传入参数")
public class PriceGroupListRequestDto {

    @ApiModelProperty(value = "加盟商编号", name = "client")
    @NotBlank(message = "加盟商编号不能为空")
    private String client;
}
