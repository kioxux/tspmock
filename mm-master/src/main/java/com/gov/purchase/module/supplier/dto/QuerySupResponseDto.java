package com.gov.purchase.module.supplier.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "供应商详情返回参数")
public class QuerySupResponseDto {

    private String supCode;

    private String supPym;

    private String supName;

    private String supCreditCode;

    private String supCreditDate;

    private String supClass;

    private String supLegalPerson;

    private String supRegAdd;

    private String supLicenceNo;

    private String supLicenceDate;

    private String supLicenceValid;

    private String supScope;

    private String client;

    private String supSite;

    private String supSiteName;

    private String supSelfCode;

    private String supStatus;

    private String supNoPurchase;

    private String supNoSupplier;

    private String supPayTerm;

    private String supBussinessContact;

    private String supContactTel;

    private String supLeadTime;

    private String supBankCode;

    private String supBankName;

    private String supAccountPerson;

    private String supBankAccount;

    private String supPayMode;

    private String supCreditAmt;

}
