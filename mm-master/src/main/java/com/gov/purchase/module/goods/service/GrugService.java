package com.gov.purchase.module.goods.service;

import com.gov.purchase.module.goods.dto.QueryUnDrugCheckRequestDto;


public interface GrugService {

    /**
     * 非药新建校验
     *
     * @param dto QueryUnDrugCheckRequestDto
     */
    void checkUnDrugCheckInfo(QueryUnDrugCheckRequestDto dto);

    /**
     * 非药新建
     *
     * @param dto QueryUnDrugCheckRequestDto
     */
    void insertUnDrugAddInfo(QueryUnDrugCheckRequestDto dto);

}
