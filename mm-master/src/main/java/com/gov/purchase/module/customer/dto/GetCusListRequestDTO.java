package com.gov.purchase.module.customer.dto;

import com.gov.purchase.common.entity.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@EqualsAndHashCode
public class GetCusListRequestDTO extends Pageable {

    @NotBlank(message = "机构不能为空")
    private String cusSite;
    /**
     * 客户名称
     */
    private String cusName;
    /**
     * 统一社会信用代码
     */
    private String cusCreditCode;
    /**
     * 供应商自编码
     */
    private String cusSelfCode;


    private String client;

    /**
     * 排序字段
     */
    private String orderItem;

    private List<String> cusSelfCodes;
}
