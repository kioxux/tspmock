package com.gov.purchase.module.goods.service;

import com.gov.purchase.entity.GaiaSdWtpro;

import java.util.List;

/**
 * 委托配送第三方商品编码对照表(GaiaSdWtpro)表服务接口
 *
 * @author makejava
 * @since 2021-08-13 15:53:54
 */
public interface GaiaSdWtproService {


    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<GaiaSdWtpro> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param gaiaSdWtpro 实例对象
     * @return 实例对象
     */
    GaiaSdWtpro insert(GaiaSdWtpro gaiaSdWtpro);


    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Long id);

}
