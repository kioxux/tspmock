package com.gov.purchase.module.base.service.impl.supplierImport;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.common.response.ResultUtil;
import com.gov.purchase.constants.CommonEnum;
import com.gov.purchase.constants.ResultEnum;
import com.gov.purchase.entity.GaiaDcData;
import com.gov.purchase.entity.GaiaDcDataKey;
import com.gov.purchase.entity.GaiaStoreData;
import com.gov.purchase.entity.GaiaStoreDataKey;
import com.gov.purchase.exception.CustomResultException;
import com.gov.purchase.mapper.DictionaryMapper;
import com.gov.purchase.mapper.GaiaDcDataMapper;
import com.gov.purchase.mapper.GaiaStoreDataMapper;
import com.gov.purchase.module.base.dto.Dictionary;
import com.gov.purchase.module.base.dto.excel.Supplier;
import com.gov.purchase.module.base.service.impl.ImportData;
import com.gov.purchase.utils.StringUtils;
import org.apache.commons.collections4.CollectionUtils;

import javax.annotation.Resource;
import java.text.MessageFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 *
 * @author: libb
 * @date: 2020.04.20
 */
public abstract class SupplierImport extends ImportData {

    @Resource
    DictionaryMapper dictionaryMapper;

    @Resource
    GaiaStoreDataMapper gaiaStoreDataMapper;

    @Resource
    GaiaDcDataMapper gaiaDcDataMapper;

    /**
     * 供应商新增/编辑业务验证共通部分
     *
     * @param dataMap
     * @return errorList
     */
    protected Result errorListSupplier(LinkedHashMap<Integer, Supplier> dataMap) {
        List<String> errorList = new ArrayList<>();
        // 数据为空
        if (dataMap == null || dataMap.isEmpty()) {
            throw new CustomResultException(ResultEnum.E0104);
        }
        // 银行代码
        List<Dictionary> bankList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.BANK.getTable(),
                CommonEnum.DictionaryData.BANK.getField1(),
                CommonEnum.DictionaryData.BANK.getCondition(), CommonEnum.DictionaryData.BANK.getSortFields());
        // 付款条件
        List<Dictionary> paymentTypeList = dictionaryMapper.getDictionaryList(CommonEnum.DictionaryData.PAYMENT_TYPE.getTable(),
                CommonEnum.DictionaryData.PAYMENT_TYPE.getField1(),
                CommonEnum.DictionaryData.PAYMENT_TYPE.getCondition(), CommonEnum.DictionaryData.PAYMENT_TYPE.getSortFields());
        // 加盟商 供应商自编码重复验证
        Map<String, List<String>> clientSupCodeMap = new HashMap<>();
        // 遍历验证
        for (Integer key : dataMap.keySet()) {
            // 行数据
            Supplier supplier = dataMap.get(key);

            // 银行代码
            if (StringUtils.isNotBlank(supplier.getSupBankCode())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(bankList, supplier.getSupBankCode());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "银行代码"));
                }
            }
            // 付款条件
            if (StringUtils.isNotBlank(supplier.getSupPayTerm())) {
                Dictionary dictionary = CommonEnum.DictionaryData.getDictionaryByValue(paymentTypeList, supplier.getSupPayTerm());
                if (dictionary == null) {
                    errorList.add(MessageFormat.format(ResultEnum.E0106.getMsg(), key + 1, "付款条件"));
                }
            }

            // 发证日期 有效期至 比较
            if (super.isNotBlank(supplier.getSupLicenceDate()) && super.isNotBlank(supplier.getSupLicenceValid())) {
                // 发证日期>=有效期至
                if (supplier.getSupLicenceDate().compareTo(supplier.getSupLicenceValid()) >= 0) {
                    errorList.add(MessageFormat.format(ResultEnum.E0129.getMsg(), key + 1));
                }
            }

            // 主数据验证
            baseCheck(supplier, errorList, key);

            // 业务表数据存在判断
            if (StringUtils.isBlank(supplier.getClient())) {
                continue;
            }
            // 加盟商供应商自编码行号
            List<String> clientSupCodeList = clientSupCodeMap.get(supplier.getClient() + "_" + supplier.getSupSelfCode());
            if (clientSupCodeList == null) {
                clientSupCodeList = new ArrayList<>();
            }
            clientSupCodeList.add(String.valueOf((key + 1)));
            clientSupCodeMap.put(supplier.getClient() + "_" + supplier.getSupSelfCode(), clientSupCodeList);

            // 加盟商 地点 验证
            // 门店数据
            GaiaStoreDataKey gaiaStoreDataKey = new GaiaStoreDataKey();
            gaiaStoreDataKey.setClient(supplier.getClient());
            gaiaStoreDataKey.setStoCode(supplier.getSupSite());
            GaiaStoreData gaiaStoreData = gaiaStoreDataMapper.selectByPrimaryKey(gaiaStoreDataKey);
            // DC数据
            GaiaDcDataKey gaiaDcDataKey = new GaiaDcDataKey();
            gaiaDcDataKey.setClient(supplier.getClient());
            gaiaDcDataKey.setDcCode(supplier.getSupSite());
            GaiaDcData gaiaDcData = gaiaDcDataMapper.selectByPrimaryKey(gaiaDcDataKey);
            if (gaiaStoreData == null && gaiaDcData == null) {
                errorList.add(MessageFormat.format(ResultEnum.E0117.getMsg(), key + 1));
            }

            // 业务数据验证
            businessCheck(supplier, errorList, key);
        }
        // 加盟商供应商自编码重复验证
        for (String clientProCode : clientSupCodeMap.keySet()) {
            List<String> lineList = clientSupCodeMap.get(clientProCode);
            if (lineList.size() > 1) {
                errorList.add(MessageFormat.format(ResultEnum.E0131.getMsg(), String.join(",", lineList)));
            }
        }
        // 没有错误
        if (CollectionUtils.isEmpty(errorList)) {
            return ResultUtil.success();
        }
        Result result = ResultUtil.error(ResultEnum.E0115);
        result.setData(errorList);
        return result;
    }

    /**
     * 主数据表验证
     *
     * @param supplier
     * @param errorList
     * @param key
     */
    protected abstract void baseCheck(Supplier supplier, List<String> errorList, int key);

    /**
     * 业务数据验证
     *
     * @param supplier
     * @param errorList
     * @param key
     */
    protected abstract void businessCheck(Supplier supplier, List<String> errorList, int key);
}
