package com.gov.purchase.feign.service;

import java.util.List;

public interface OperationFeignService {

    /**
     * 供应商信息新增/修改
     *
     * @param client        加盟商
     * @param site          地点
     * @param attribute     属性（1：单体；2：连锁）
     * @param supplierCodes 供应商编码列表
     */
    void supplierSync(String client, String site, String attribute, List<String> supplierCodes);

    /**
     * 商品价格新增/修改
     *
     * @param client 加盟商
     * @param site   地点
     * @param proIds 商品编码列表
     */
    void proPriceSync(String client, String site, List<String> proIds);

    void multipleStoreProPriceSync(String client, List<String> site, List<String> proIds);

    /**
     * 多门店批量商品新增/修改
     *
     * @param client    加盟商
     * @param storeList 地点
     * @param proIds    商品编码列表
     */
    void multipleStoreProductSync(String client, List<String> storeList, List<String> proIds);

}
