package com.gov.purchase.feign.service.impl;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.feign.OperationFeign;
import com.gov.purchase.feign.service.OperationFeignService;
import com.gov.purchase.utils.JsonUtils;
import com.gov.purchase.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OperationFeignServiceImpl implements OperationFeignService {

    @Resource
    private OperationFeign operationFeign;

    /**
     * 供应商信息新增/修改
     *
     * @param client        加盟商
     * @param site          地点
     * @param attribute     属性（1：单体；2：连锁）
     * @param supplierCodes 供应商编码列表
     */
    @Override
    public void supplierSync(String client, String site, String attribute, List<String> supplierCodes) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("client", client);
            map.put("site", site);
            map.put("attribute", attribute);
            map.put("supplierCodes", supplierCodes);
            log.info("[供应商信息新增/修改][/sync/supplierSync]参数:{}", map);
            Result result = operationFeign.supplierSync(map);
            log.info("[供应商信息新增/修改][/sync/supplierSync]返回结果{}", JsonUtils.beanToJson(result));
        } catch (Exception e) {
            log.info("[供应商信息新增/修改 gys-operation调用 异常]{}", e.getMessage());
        }
    }

    /**
     * 商品价格新增/修改
     *
     * @param client 加盟商
     * @param site   地点
     * @param proIds 商品编码列表
     */
    @Override
    public void proPriceSync(String client, String site, List<String> proIds) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("client", client);
            map.put("site", site);
            map.put("proIds", proIds);
            log.info("[商品价格新增/修改][/sync/proPriceSync]参数:{}", map);
            Result result = operationFeign.proPriceSync(map);
            log.info("[商品价格新增/修改][/sync/proPriceSync]返回结果{}", JsonUtils.beanToJson(result));
        } catch (Exception e) {
            log.info("[商品价格新增/修改 gys-operation调用 异常]{}",e.getMessage());
        }
    }

    /**
     * 商品价格新增/修改
     *
     * @param client 加盟商
     * @param site   地点
     * @param proIds 商品编码列表
     */
    @Async
    @Override
    public void multipleStoreProPriceSync(String client, List<String> site, List<String> proIds) {
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("client", client);
            map.put("storeList", site);
            map.put("proIds", proIds);
            log.info("[商品价格新增/修改][/sync/multipleStoreProPriceSync]参数:{}", map);
            Result result = operationFeign.multipleStoreProPriceSync(map);
            log.info("[商品价格新增/修改][/sync/multipleStoreProPriceSync]返回结果{}", JsonUtils.beanToJson(result));
        } catch (Exception e) {
            e.printStackTrace();
            log.info("[商品价格新增/修改 gys-operation调用 异常]{}",e.getMessage());
        }
    }

    /**
     * 多门店批量商品新增/修改
     *
     * @param client    加盟商
     * @param storeList 地点
     * @param proIds    商品编码列表
     */
    @Async
    @Override
    public void multipleStoreProductSync(String client, List<String> storeList, List<String> proIds) {
        try {
            if (StringUtils.isBlank(client) || CollectionUtils.isEmpty(storeList) || CollectionUtils.isEmpty(proIds)) {
                return;
            }
            Map<String, Object> map = new HashMap<>();
            map.put("client", client);
            map.put("storeList", storeList.stream().distinct().collect(Collectors.toList()));
            map.put("proIds", proIds.stream().distinct().collect(Collectors.toList()));
            log.info("[多门店批量商品新增/修改][/sync/multipleStoreProductSync]参数:{}", map);
            Result result = operationFeign.multipleStoreProductSync(map);
            log.info("[多门店批量商品新增/修改][/sync/multipleStoreProductSync]返回结果{}", JsonUtils.beanToJson(result));
        } catch (Exception e) {
            log.info("[多门店批量商品新增/修改 gys-operation调用 异常]{}", e.getMessage());
        }
    }
}
