package com.gov.purchase.feign;

import com.gov.purchase.feign.dto.FeignResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@Component
@FeignClient(value = "wms")
public interface WmsFeign {

    /**
     * 流程发起
     *
     * @param param
     */
    @PostMapping(value = "/web/api/v1/thirdPartyManager/external/init/baseData")
    public FeignResult baseData(Map<String, String> param);

    /**
     * 门店铺货/采购订单创建 自动开单
     */
    @PostMapping(value = "/web/api/v1/thirdPartyManager/external/automaticCreateOrder")
    public FeignResult automaticCreateOrder(Map<String, Object> param);

    @PostMapping(value = "/web/api/v1/thirdPartyManager/external/autoCreateTransferOrder")
    FeignResult autoCreateTransferOrder(Map<String, Object> param);

    @PostMapping(value = "/web/api/v1/entrustStock/autoCreateEntrustOrder")
    FeignResult autoCreateEntrustOrder(Map<String, Object> params);

    @PostMapping(value = "/web/api/v1/thirdPartyManager/external/updateProFixBin")
    FeignResult updateProFixBin(Map<String, String> params);
}
