package com.gov.purchase.feign.fallback;

import com.alibaba.fastjson.JSONObject;
import com.gov.purchase.feign.AuthFeign;
import com.gov.purchase.feign.dto.FeignResult;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class AuthFeignFallback implements AuthFeign {


    @Override
    public FeignResult createWorkflow(Map<String, Object> param) {
        FeignResult result = new FeignResult();
        result.setCode(9999);
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }

    /**
     * 蓬晖
     * @param param
     * @return
     */
    @Override
    public FeignResult createWorkflowNew(Map<String, Object> param) {
        FeignResult result = new FeignResult();
        result.setCode(9999);
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }


    @Override
    public JSONObject getLoginInfo(Map<String, Object> param) {
        FeignResult result = new FeignResult();
        result.setCode(9999);
        result.setMessage("调用超时或系统异常，请稍后再试");
        return  (JSONObject) JSONObject.toJSON(result);
    }
}
