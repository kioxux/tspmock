package com.gov.purchase.feign;

import com.alibaba.fastjson.JSONObject;
import com.gov.purchase.common.response.Result;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.fallback.AuthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Component
@FeignClient(value = "gys-operate", fallback = AuthFeignFallback.class)
public interface OperateFeign {

    /**
     * 流程发起
     */
    @PostMapping(value = "/marketing/marketing/execute")
    public Result execute(@RequestBody Map<String, Object> map);

}
