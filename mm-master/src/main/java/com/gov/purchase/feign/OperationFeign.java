package com.gov.purchase.feign;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.entity.GaiaMaterialDoc;
import com.gov.purchase.feign.fallback.OperationFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@Component
@FeignClient(value = "gys-operation", fallback = OperationFeignFallback.class)
public interface OperationFeign {

    /**
     * 多门店批量商品新增/修改
     */
    @PostMapping(value = "/sync/multipleStoreProductSync")
    Result multipleStoreProductSync(@RequestBody Map<String, Object> map);

    /**
     * 供应商信息新增/修改
     */
    @PostMapping(value = "/sync/supplierSync")
    Result supplierSync(@RequestBody Map<String, Object> map);

    /**
     * 商品价格新增/修改
     */
    @PostMapping(value = "/sync/proPriceSync")
    Result proPriceSync(@RequestBody Map<String, Object> map);

    /**
     * 商品价格新增/修改
     */
    @PostMapping(value = "/sync/multipleStoreProPriceSync")
    Result multipleStoreProPriceSync(Map<String, Object> map);
}
