package com.gov.purchase.feign.fallback;

import com.gov.purchase.common.response.Result;
import com.gov.purchase.feign.OperationFeign;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class OperationFeignFallback implements OperationFeign {

    /**
     * 多门店批量商品新增/修改
     *
     * @param map
     * @return
     */
    @Override
    public Result multipleStoreProductSync(Map<String, Object> map) {
        Result result = new Result();
        result.setCode("9999");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }

    /**
     * 供应商信息新增/修改
     *
     * @param map
     * @return
     */
    @Override
    public Result supplierSync(Map<String, Object> map) {
        Result result = new Result();
        result.setCode("9999");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }

    /**
     * 商品价格新增/修改
     *
     * @param map
     * @return
     */
    @Override
    public Result proPriceSync(Map<String, Object> map) {
        Result result = new Result();
        result.setCode("9999");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }

    @Override
    public Result multipleStoreProPriceSync(Map<String, Object> map) {
        Result result = new Result();
        result.setCode("9999");
        result.setMessage("调用超时或系统异常，请稍后再试");
        return result;
    }

}
