package com.gov.purchase.feign;

import com.alibaba.fastjson.JSONObject;
import com.gov.purchase.feign.dto.FeignResult;
import com.gov.purchase.feign.fallback.AuthFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@Component
@FeignClient(value = "gys-auth", fallback = AuthFeignFallback.class)
public interface AuthFeign {

    /**
     * 流程发起
     *
     * @param param
     */
    @PostMapping(value = "/createWorkflow")
    public FeignResult createWorkflow(Map<String, Object> param);

    /**
     * 流程发起(蓬晖)
     *
     * @param param
     */
    @PostMapping(value = "/createWorkflowNew")
    public FeignResult createWorkflowNew(Map<String, Object> param);

    /**
     * 当前登录人信息
     *
     * @param param
     * @return
     */
    @PostMapping(value = "/getLoginInfo")
    public JSONObject getLoginInfo(Map<String, Object> param);
}
