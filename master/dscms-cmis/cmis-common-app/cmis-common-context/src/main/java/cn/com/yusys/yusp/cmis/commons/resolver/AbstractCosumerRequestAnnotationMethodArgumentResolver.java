package cn.com.yusys.yusp.cmis.commons.resolver;

import cn.com.yusys.yusp.cmis.commons.exception.CmisContextException;
import cn.com.yusys.yusp.cmis.commons.holder.RequestBodyHolder;
import cn.com.yusys.yusp.cmis.commons.process.ConsumerProcessor;
import cn.com.yusys.yusp.cmis.commons.util.RequestHolderUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Objects;

/**
 * 抽象自定义请求报文的处理参数.
 *
 * @author yangzai
 * @since 2021/4/15
 **/
public abstract class AbstractCosumerRequestAnnotationMethodArgumentResolver implements HandlerMethodArgumentResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCosumerRequestAnnotationMethodArgumentResolver.class);

    /**
     * consumerRequestBody处理之后的消息的实现.
     */
    private final ConsumerProcessor consumerProcessor;

    /**
     * 报文体解析处理类,包含{@code HttpMessageConverter}和{@code requestResponseBodyAdvice}.
     */
    private HandlerMethodArgumentResolver requestResponseBodyMethodProcessor;

    public AbstractCosumerRequestAnnotationMethodArgumentResolver(ConsumerProcessor consumerProcessor) {
        this.consumerProcessor = consumerProcessor;
    }

    public ConsumerProcessor getConsumerProcessor() {
        return consumerProcessor;
    }

    public void setRequestResponseBodyMethodProcessor(HandlerMethodArgumentResolver handlerMethodArgumentResolver) {
        this.requestResponseBodyMethodProcessor = handlerMethodArgumentResolver;
    }

    @Override
    public Object resolveArgument(@NonNull MethodParameter parameter, ModelAndViewContainer mavContainer,
                                  @Nullable NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        RequestHolderUtils.putMethodAttrIfNotAbsent(parameter.getMethod());
        // 提取请求requestBody.
        Object params = parseParams(mavContainer, webRequest, binderFactory);
        // 从参数中提取需要信息.
        Object result = resolveBodyArgument(parameter, params);
        // 进行参数是否必填的校验.
        validateIfRequired(result, parameter);
        // 执行基于注解的校验规则.
        validateInternal(parameter, result, webRequest, binderFactory);
        return result;
    }

    /**
     * 解析{@code RequestBody}处理参数.
     *
     * @param mavContainer  容器.
     * @param webRequest    请求对象.
     * @param binderFactory 绑定处理工厂.
     * @return 解析body的参数.
     * @throws Exception body解析异常.
     */
    protected Object parseParams(ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
                                 WebDataBinderFactory binderFactory) {
        // 需要依赖request body进行参数消息的处理.
        Object params = RequestBodyHolder.getRequest();
        // 上下文副本不存在解析的参数代表requestBody尚未解析.
        if (Objects.isNull(params)) {
            // 当参数没有解析,并且上下文不存在.
            // 不存在,代表需要进行requestBody的解析.
            // 修改方法参数为RequestBody-Object.
            MethodParameter requestParameter = RequestBodyHolder.requestMethodParameter();
            try {
                params = requestResponseBodyMethodProcessor.resolveArgument(requestParameter, mavContainer, webRequest, binderFactory);
            } catch (Exception e) {
                LOGGER.warn("Parse RequestBody exception.", e);
                return null;
            }
            // 放入请求上下文中.
            RequestBodyHolder.setRequest(params);
        }
        return params;
    }

    /**
     * 从解析的报文中解析需要的请求参数.
     *
     * @param parameter 方法参数描述.
     * @param params    解析的报文实体.
     * @return 提取的对应的方法参数.
     */
    protected abstract Object resolveBodyArgument(MethodParameter parameter, Object params);

    /**
     * 校验是否参数必须.
     *
     * @param result    解析结果
     * @param parameter 当前解析对的方法参数
     */
    private void validateIfRequired(Object result, MethodParameter parameter) {
        boolean required = isRequired(parameter);
        if (required && Objects.isNull(result)) {
            throw new CmisContextException("parameter[" + parameter.getParameterName() + "] is required,but is null");
        }
    }

    /**
     * 根据方法参数判断是否当前入参是否必须，不能为null.
     *
     * @param parameter 方法参数
     * @return 是否必须
     */
    protected abstract boolean isRequired(MethodParameter parameter);

    /**
     * 进行参数注解的校验.
     *
     * @param parameter     方法参数
     * @param result        串行化结果
     * @param webRequest    web请求
     * @param binderFactory 绑定工厂
     * @throws Exception 校验异常
     */
    protected abstract void validateInternal(MethodParameter parameter, Object result,
                                             NativeWebRequest webRequest, WebDataBinderFactory binderFactory);


    /**
     * Whether to raise a fatal bind exception on validation errors.
     *
     * @param binder    the data binder used to perform data binding
     * @param parameter the method parameter descriptor
     * @return {@code true} if the next method argument is not of type {@link Errors}
     * @since 4.1.5
     */
    protected boolean isBindExceptionRequired(WebDataBinder binder, MethodParameter parameter) {
        int i = parameter.getParameterIndex();
        Class<?>[] paramTypes = parameter.getExecutable().getParameterTypes();
        boolean hasBindingResult = (paramTypes.length > (i + 1) && Errors.class.isAssignableFrom(paramTypes[i + 1]));
        return !hasBindingResult;
    }

}