package cn.com.yusys.yusp.cmis.commons.exception;

import cn.com.yusys.yusp.commons.exception.PlatformException;

/**
 * @author yangzai
 * @since 2021/4/15
 **/
public class CmisContextException extends PlatformException {

    public CmisContextException(String message) {
        super(message);
    }

    public CmisContextException(String message, Throwable cause) {
        super(message, cause);
    }

}