package cn.com.yusys.yusp.cmis.commons.holder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 请求体参数的上下文维护.
 */
public class RequestBodyHolder {

    private static final Logger log = LoggerFactory.getLogger(RequestBodyHolder.class);

    /**
     * 使用{@code Object}进行{@code RequestBody}的维护.
     */
    private static final ThreadLocal<Object> request_holder = new ThreadLocal<>();

    private static MethodParameter methodParameter = null;

    static {
        try {
            methodParameter = MethodParameter.forExecutable(RequestBodyHolder.class.getMethod("setRequest", Object.class), 0);
        } catch (NoSuchMethodException e) {
            log.error("Not exists method:setRequest.");
        }
    }

    private RequestBodyHolder() {
    }

    /**
     * 获取请求线程副本.
     *
     * @return 请求解析的body.
     */
    public static Object getRequest() {
        return request_holder.get();
    }

    /**
     * 进行请求参数的初始化.
     * <pre>
     *     该方法同时用于{@code MessageHeader}和{@code MessageBody}处理过程中进行body参数的处理.
     * </pre>
     *
     * @param request 请求参数.
     */
    public static void setRequest(@RequestBody Object request) {
        request_holder.set(request);
    }

    /**
     * 进行请求参数的清理.
     */
    public static void clear() {
        request_holder.remove();
    }

    /**
     * 返回用于模拟map请求的方法参数.
     *
     * @return MethodParameter
     */
    public static MethodParameter requestMethodParameter() {
        return methodParameter;
    }

}