package cn.com.yusys.yusp.cmis.commons.handler;

import cn.com.yusys.yusp.cmis.commons.adapter.CmisDefaultError;
import cn.com.yusys.yusp.commons.biz.adapter.DefaultError;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.exception.PlatformException;
import cn.com.yusys.yusp.commons.exception.web.handler.ExceptionHandler;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.core.Ordered;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Optional;

/**
 * cmis系统统一处理RuntimeException未知异常
 *
 * @author yangzai
 * @since 2021/5/14
 **/
public class CmisCommonExceptionHandler implements ExceptionHandler<RuntimeException>, Ordered {

    @Override
    public Object handleException(RuntimeException exception) {
        addResponseHeaderException(null, exception.getMessage());
        DefaultError defaultError = CmisDefaultError.builder()
                .error("500")
                .message("系统错误，请联系管理员！")
                .timestamp(DateUtils.formatDateTimeByDef())
                .build();
        CmisDefaultError cmisDefaultError =
                new CmisDefaultError(defaultError.getPath(), defaultError.getError(), defaultError.getMessage(), defaultError.getTimestamp(), defaultError.getRequestId(), defaultError.getCode());
        cmisDefaultError.setErorcd("9999");// 行里统一定义系统错误为9999
        cmisDefaultError.setErortx("系统错误，请联系管理员！");
        return cmisDefaultError;
    }

    @Override
    public boolean isSupported(Exception e) {
        return e instanceof RuntimeException && !(e instanceof BizException) && !(PlatformException.class.isAssignableFrom(e.getClass()));
    }

    @Override
    public void addResponseHeaderException(String code, String message) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        // 将异常信息放入响应头中
        Optional.ofNullable(attributes)
                .map(ServletRequestAttributes::getResponse)
                .ifPresent(response -> {
                    try {
                        response.addHeader(EXCEPTION_MESSAGE, URLEncoder.encode(message, "UTF-8"));
                    } catch (UnsupportedEncodingException e) {
                        throw new RuntimeException(e);
                    }
                });
    }

    @Override
    public int getOrder() {
        return 0;
    }

}