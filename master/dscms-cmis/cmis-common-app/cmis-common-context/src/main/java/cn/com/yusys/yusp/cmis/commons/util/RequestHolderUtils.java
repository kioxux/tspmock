package cn.com.yusys.yusp.cmis.commons.util;

import cn.com.yusys.yusp.cmis.commons.constant.ContextConstants;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import java.lang.reflect.Method;

public class RequestHolderUtils {

    private RequestHolderUtils() {
    }

    public static void putMethodAttrIfNotAbsent(Method method) {
        // 如果存在不放入.
        RequestAttributes attr = RequestContextHolder.getRequestAttributes();
        if (attr != null && attr.getAttribute(ContextConstants.REQUEST_HANDLE_METHOD, RequestAttributes.SCOPE_REQUEST) == null) {
            // 请求方法名.直接放入RequestAttributes.
            attr.setAttribute(ContextConstants.REQUEST_HANDLE_METHOD, method, RequestAttributes.SCOPE_REQUEST);
        }
    }

}