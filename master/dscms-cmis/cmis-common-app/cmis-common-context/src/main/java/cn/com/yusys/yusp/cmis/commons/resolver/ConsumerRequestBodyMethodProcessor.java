package cn.com.yusys.yusp.cmis.commons.resolver;

import cn.com.yusys.yusp.cmis.commons.annonation.ConsumerRequestBody;
import cn.com.yusys.yusp.cmis.commons.exception.CmisContextException;
import cn.com.yusys.yusp.cmis.commons.process.ConsumerProcessor;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.lang.Nullable;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.util.Map;
import java.util.Objects;

/**
 * 方法参数注解{@code ConsumerRequestBody}的处理类.
 *
 * @author yangzai
 * @since 2021/4/15
 **/
public class ConsumerRequestBodyMethodProcessor extends AbstractCosumerRequestAnnotationMethodArgumentResolver {

    private final Logger log = LoggerFactory.getLogger(ConsumerRequestBodyMethodProcessor.class);

    public ConsumerRequestBodyMethodProcessor(ConsumerProcessor consumerProcessor) {
        super(consumerProcessor);
    }

    @Override
    protected boolean isRequired(MethodParameter parameter) {
        return Objects.requireNonNull(parameter.getParameterAnnotation(ConsumerRequestBody.class)).required();
    }

    @Override
    protected void validateInternal(MethodParameter parameter, Object result, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        if (Objects.nonNull(binderFactory)) {
            try {
                WebDataBinder webDataBinder = binderFactory.createBinder(webRequest, result, parameter.getGenericParameterType().getTypeName());
                this.validate(parameter, webDataBinder);
            } catch (Exception e) {
                throw new CmisContextException("Validate parameter failed.", e);
            }
        }
    }

    /**
     * 进行方法参数的校验.
     *
     * @param parameter 参数名称
     * @param binder    WebDataBinder
     * @throws MethodArgumentNotValidException 请求参数不满足校验结果
     */
    protected void validate(MethodParameter parameter, @Nullable WebDataBinder binder) throws MethodArgumentNotValidException {
        Annotation[] annotations = parameter.getParameterAnnotations();
        for (Annotation annotation : annotations) {
            Validated validatedAnna = parameter.getParameterAnnotation(Validated.class);
            if (validatedAnna != null || annotation.annotationType().getSimpleName().startsWith("Valid")) {
                Object hints = (validatedAnna != null ? validatedAnna.value() : AnnotationUtils.getValue(annotation));
                Object[] validationHints = (hints instanceof Object[]) ? (Object[]) hints : new Object[]{hints};
                if (Objects.isNull(binder)) {
                    throw new CmisContextException("Validated Annotation is present, but not find WebDataBinder.");
                }
                binder.validate(validationHints);
                break;
            }
        }

        if (Objects.nonNull(binder) && binder.getBindingResult().hasErrors() && isBindExceptionRequired(binder, parameter)) {
            throw new MethodArgumentNotValidException(parameter, binder.getBindingResult());
        }
    }

    @Override
    protected Object resolveBodyArgument(MethodParameter parameter, Object params) {
        if (Objects.isNull(params)) {
            log.warn("Missing RequestBody,Can't resolve param:{} with annotation @ConsumerRequestBody.", parameter.getParameterName());
            return null;
        }

        // 获取报文体部分.
        Object toParseBody = getConsumerProcessor().extractBody(params);
        if (Objects.isNull(toParseBody)) {
            log.warn("RequestBody missing necessary body.");
            return null;
        }

        ConsumerRequestBody consumerRequestBody = parameter.getParameterAnnotation(ConsumerRequestBody.class);
        String name = consumerRequestBody.value();
        if (StringUtils.nonEmpty(name)) {
            // 当不为默认值的时候，提取对应的值进行提取.
            if (!(toParseBody instanceof Map)) {
                // 不为map类型无法解析.
                log.warn("RequestBody not is Map,Can't Parse ConsumerRequestBody:{}.", name);
                return null;
            }

            toParseBody = ((Map<?, ?>) toParseBody).get(name);
            if (Objects.isNull(toParseBody)) {
                log.warn("RequestBody not exist key:{}", name);
                return null;
            }
        }

        // 进行序列化.
        Type parameterType = parameter.getGenericParameterType();
        try {
            return ObjectMapperUtils.instance().convertValue(toParseBody, TypeFactory.defaultInstance().constructType(parameterType));
        } catch (Exception e) {
            log.warn(String.format("Convert body to [%s] failed.", parameterType.getTypeName()), e);
            return null;
        }
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        // 处理带有注解ConsumerRequestBody的参数.
        return parameter.hasParameterAnnotation(ConsumerRequestBody.class);
    }

}