package cn.com.yusys.yusp.cmis.commons.config;

import cn.com.yusys.yusp.cmis.commons.process.ConsumerProcessor;
import cn.com.yusys.yusp.cmis.commons.resolver.ConsumerRequestBodyMethodProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.AnnotationAwareOrderComparator;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ObjectToStringHttpMessageConverter;
import org.springframework.lang.NonNull;
import org.springframework.web.method.ControllerAdviceBean;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdvice;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestResponseBodyMethodProcessor;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 自定义报文规范自动配置.
 *
 * @author yangzai
 * @since 2021/4/15
 **/
@ConditionalOnClass({ConsumerProcessor.class})
public class HeaderSpecAutoConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(HeaderSpecAutoConfiguration.class);

    /**
     * 装配适配的服务规范处理器.
     *
     * @return 适配的报文处理器.
     */
    @Bean
    @ConditionalOnMissingBean
    public ConsumerProcessor contextServiceProcessor() {
        logger.debug("Init Default ServiceProcessor.");
        return new ConsumerProcessor.Default();
    }

    @Bean
    @ConditionalOnMissingBean
    public ConsumerRequestBodyMethodProcessor messageBodyMethodProcessor(ConsumerProcessor processor) {
        logger.debug("Init @ConsumerRequestBody method arguments processor.");
        return new ConsumerRequestBodyMethodProcessor(processor);
    }

    @Bean
    public RequestMappingHandlerAdapterPostProcessor requestMappingHandlerAdapterPostProcessor() {
        return new RequestMappingHandlerAdapterPostProcessor();
    }

    static class RequestMappingHandlerAdapterPostProcessor implements SmartInitializingSingleton, ApplicationContextAware {
        private final List<Object> requestResponseBodyAdvice = new ArrayList<>();
        private final List<HttpMessageConverter<?>> converters = new ArrayList<>();
        private ApplicationContext context;

        @Override
        public void afterSingletonsInstantiated() {
            // 单实例初始化之后执行.
            // 从容器中获取RequestMappingHandlerAdapter.
            RequestMappingHandlerAdapter adapter = context.getBean(RequestMappingHandlerAdapter.class);
            // 从容器中获取自定义的RequestBody注解的处理类.
            ConsumerRequestBodyMethodProcessor consumerRequestBodyMethodProcessor = context.getBean(ConsumerRequestBodyMethodProcessor.class);
            // 添加自定义的参数解析器.
            List<HandlerMethodArgumentResolver> argumentResolvers = new ArrayList<>(Objects.requireNonNull(adapter.getArgumentResolvers()));
            argumentResolvers.add(0, consumerRequestBodyMethodProcessor);
            adapter.setArgumentResolvers(argumentResolvers);

            initAdvice();
            initMessageConverters();

            RequestResponseBodyMethodProcessor processor = new RequestResponseBodyMethodProcessor(converters, requestResponseBodyAdvice);
            consumerRequestBodyMethodProcessor.setRequestResponseBodyMethodProcessor(processor);
        }

        private void initMessageConverters() {
            context.getBeansOfType(HttpMessageConverter.class).values().forEach(converters::add);
            // 添加对string接受的支持，不能放入容器中.
            ConversionService conversionService = context.getBean("mvcConversionService", ConversionService.class);
            ObjectToStringHttpMessageConverter converter = new ObjectToStringHttpMessageConverter(conversionService);
            converters.add(converter);
        }

        void initAdvice() {
            List<ControllerAdviceBean> adviceBeans = ControllerAdviceBean.findAnnotatedBeans(context);
            AnnotationAwareOrderComparator.sort(adviceBeans);

            List<Object> requestResponseBodyAdviceBeans = new ArrayList<>();

            for (ControllerAdviceBean adviceBean : adviceBeans) {
                Class<?> beanType = adviceBean.getBeanType();
                if (beanType == null) {
                    throw new IllegalStateException("Unresolvable type for ControllerAdviceBean: " + adviceBean);
                }
                if (RequestBodyAdvice.class.isAssignableFrom(beanType) || ResponseBodyAdvice.class.isAssignableFrom(beanType)) {
                    requestResponseBodyAdviceBeans.add(adviceBean);
                }
            }

            if (!requestResponseBodyAdviceBeans.isEmpty()) {
                this.requestResponseBodyAdvice.addAll(0, requestResponseBodyAdviceBeans);
            }
        }

        @Override
        public void setApplicationContext(@NonNull ApplicationContext applicationContext) {
            context = applicationContext;
        }
    }

}