package cn.com.yusys.yusp.cmis.commons.annonation;

import java.lang.annotation.*;

/**
 * 移动端请求报文
 *
 * @author yangzai
 * @since 2021/4/15
 **/
@Target(ElementType.PARAMETER)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ConsumerRequestBody {

    /**
     * 从报文体中提取body部分,然后根据当前值,参数值进行解析.
     * 如果对应的是全部报文不设置value.
     * 如果只是报文下面的一个key必须使用对应参数key值,目前只支持解析一级的节点.
     *
     * @return body中的key.
     */
    String value() default "";

    /**
     * 该参数是否必须.
     * 默认必须，当不存在该参数的情况下,会直接抛出参数异常.
     *
     * @return 参数是否必须.
     */
    boolean required() default true;

}