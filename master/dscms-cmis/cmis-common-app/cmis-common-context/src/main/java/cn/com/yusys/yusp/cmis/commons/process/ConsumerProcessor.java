package cn.com.yusys.yusp.cmis.commons.process;

/**
 * 服务上下文信息处理.
 * 提供解析参数到上下文中.
 * 或者写入上下文到报文中.
 *
 * @author yangzai
 * @since 2021/4/15
 **/
public interface ConsumerProcessor {

    /**
     * 处理接入参数.
     *
     * @param in 接入对象
     */
    default void processIn(Object in) {

    }

    /**
     * 处理接出参数.
     *
     * @param out 接出对象
     * @return Object.
     */
    default <T> T processOut(T out) {
        return out;
    }

    /**
     * 从请求参数中提取报文体部分.
     * <pre>
     *    上下文存在的时候.存在两种可能传递部分报文，传递全量报文.
     *    存在全量报文的情况,需要提取指定bodyKey的部分,
     *    报文不包括系统请求头的部分时候，直接使用当前报文.
     * </pre>
     *
     * @param params 解析的body报文.
     * @return 请求报文的body部分.
     */
    default Object extractBody(Object params) {
        return params;
    }

    /**
     * 添加默认实现.
     */
    class Default implements ConsumerProcessor {
    }
}
