package cn.com.yusys.yusp.cmis.commons.config;

import cn.com.yusys.yusp.cmis.commons.handler.CmisBizExceptionHandler;
import cn.com.yusys.yusp.cmis.commons.handler.CmisCommonExceptionHandler;
import cn.com.yusys.yusp.cmis.commons.handler.CmisMethodArgumentNotValidExceptionHandler;
import cn.com.yusys.yusp.cmis.commons.handler.CmisPlatformExceptionHandler;
import cn.com.yusys.yusp.commons.exception.web.handler.ExceptionHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * cmis web请求产生的异常统一拦截
 *
 * @author yangzai
 * @since 2021/5/14
 **/
@Configuration
@ConditionalOnClass({ExceptionHandler.class})
@ConditionalOnProperty(prefix = "cmis." + "exception", name = "enabled", havingValue = "true", matchIfMissing = true)
public class CmisWebExceptionAutoConfiguration {

    @Bean
    public ExceptionHandler<?> cmisBizExceptionHandler(@Value("${" + "cmis." + ".exception.biz" +
            ".response-status:200}") int status, @Value("${" + "cmis." + ".exception.biz" +
            ".feign-handled:true}") boolean handleFeign) {
        return new CmisBizExceptionHandler(status, handleFeign);
    }

    @Bean
    public ExceptionHandler<?> cmisPlatformExceptionHandler() {
        return new CmisPlatformExceptionHandler();
    }

    @Bean
    public ExceptionHandler<?> cmisMethodArgumentNotValidExceptionHandler() {
        return new CmisMethodArgumentNotValidExceptionHandler();
    }

    @Bean
    public ExceptionHandler<?> cmisCommonExceptionHandler() {
        return new CmisCommonExceptionHandler();
    }

}