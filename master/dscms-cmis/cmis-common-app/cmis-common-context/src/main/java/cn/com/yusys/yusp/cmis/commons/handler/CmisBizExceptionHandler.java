package cn.com.yusys.yusp.cmis.commons.handler;

import cn.com.yusys.yusp.cmis.commons.adapter.CmisDefaultError;
import cn.com.yusys.yusp.commons.biz.adapter.DefaultError;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.exception.web.handler.ExceptionHandler;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import org.springframework.core.Ordered;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

/**
 * cmis系统统一处理BizException
 *
 * @author yangzai
 * @since 2021/5/14
 **/
public class CmisBizExceptionHandler implements ExceptionHandler<BizException>, Ordered {

    private final int responseStatus;

    private final boolean handleFeign;

    public static final String EXCEPTION_ERROR_CODE = "exception-error";

    public CmisBizExceptionHandler(int responseStatus, boolean handleFeign) {
        this.responseStatus = responseStatus;
        this.handleFeign = handleFeign;
    }

    @Override
    public Object handleException(BizException exception) {
        // 如果配置的返回状态不为 200, 那就先检查 request header 判断是不是 feign 请求, 如果是则优先直接扔出异常, 否则仍正常处理
        if (handleFeign) {
            AtomicReference<String> isFeignRequest = new AtomicReference<>();
            ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
            Optional.ofNullable(attributes).map(ServletRequestAttributes::getRequest).ifPresent(
                    request -> isFeignRequest.set(request.getHeader("feign-request")));
            if (isFeignRequest.get() != null) {
                throw exception;
            }
        }
        DefaultError defaultError = CmisDefaultError.builder().error(exception.getErrorCode()).message(exception.getMessage())
                .timestamp(DateUtils.formatDateTimeByDef()).build();
        CmisDefaultError cmisDefaultError =
                new CmisDefaultError(defaultError.getPath(), defaultError.getError(), defaultError.getMessage(), defaultError.getTimestamp(), defaultError.getRequestId(), defaultError.getCode());
        String oldOrder = defaultError.getCode();
        cmisDefaultError.setErorcd(oldOrder);
        cmisDefaultError.setErortx(defaultError.getMessage());
        return cmisDefaultError;
    }

    @Override
    public boolean isSupported(Exception exception) {
        return exception instanceof BizException;
    }

    @Override
    public void tag(BizException bizException) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        // 将异常信息放入响应头中
        Optional.ofNullable(attributes)
                .map(ServletRequestAttributes::getResponse)
                .ifPresent(response -> {
                    response.addHeader(EXCEPTION_CLASS, BizException.class.getName());
                    response.setStatus(responseStatus);
                    response.addHeader(EXCEPTION_CODE, bizException.getI18nCode());
                    response.addHeader(EXCEPTION_MESSAGE, bizException.getMessage());
                    response.addHeader(EXCEPTION_ERROR_CODE, bizException.getErrorCode());
                });
    }

    @Override
    public int getOrder() {
        return 0;
    }

}