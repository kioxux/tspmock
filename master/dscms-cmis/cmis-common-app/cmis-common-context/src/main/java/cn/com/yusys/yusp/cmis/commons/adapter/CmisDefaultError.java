package cn.com.yusys.yusp.cmis.commons.adapter;

import cn.com.yusys.yusp.commons.biz.adapter.DefaultError;

/**
 * CMIS默认错误信息对象
 *
 * @author yangzai
 * @since 2021/5/14
 **/
public class CmisDefaultError extends DefaultError {

    private String erorcd;

    private String erortx;

    private String code;

    public CmisDefaultError(String path, String error, String message , String timestamp, String requestId, String code){
        this.setPath(path);
        this.setError(error);
        this.code = code;
        this.setMessage(message);
        this.setTimestamp(timestamp);
        this.setRequestId(requestId);
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}