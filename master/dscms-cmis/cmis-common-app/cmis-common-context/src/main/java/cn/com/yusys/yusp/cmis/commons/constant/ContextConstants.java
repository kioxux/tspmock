package cn.com.yusys.yusp.cmis.commons.constant;

public class ContextConstants {

    public static final String REQUEST_HANDLE_METHOD = "request_handle_method";

    private ContextConstants() {
    }

}