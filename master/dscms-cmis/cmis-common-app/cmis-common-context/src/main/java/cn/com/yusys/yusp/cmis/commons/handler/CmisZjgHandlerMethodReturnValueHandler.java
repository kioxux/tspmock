package cn.com.yusys.yusp.cmis.commons.handler;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.openfeign.constant.FeignConstants;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.dto.server.HyyRespDto;
import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Map;

/**
 * 张家港统一返回二次处理
 * 只能正常的返回，不能处理异常，异常需自己处理
 *
 * @author yangzai
 * @since 2021/5/13
 **/
public class CmisZjgHandlerMethodReturnValueHandler implements HandlerMethodReturnValueHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmisZjgHandlerMethodReturnValueHandler.class);

    private HandlerMethodReturnValueHandler handler;

    /**
     * 返回信息中原始code码存放字段
     */
    private static final String RESULTDTO_CODE_FIELD = "code";

    /**
     * 返回信息中原始message存放字段
     */
    private static final String RESULTDTO_MESSAGE_FIELD = "message";

    /**
     * 张家港通用返回code码字段
     */
    private static final String ZJG_COMMON_CODEKEY = "erorcd";

    /**
     * 张家港通用返回CODE字段的默认值
     */
    private static final String ZJG_COMMON_DEFAULTCODE = "0000";

    /**
     * 张家港通用返回message字段
     */
    private static final String ZJG_COMMON_MSGKEY = "erortx";

    public CmisZjgHandlerMethodReturnValueHandler(HandlerMethodReturnValueHandler handler) {
        this.handler = handler;
    }

    @Override
    public boolean supportsReturnType(MethodParameter returnType) {
        return handler.supportsReturnType(returnType);
    }

    @Override
    public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest) throws Exception {
        String feignServieName = webRequest.getHeader(FeignConstants.FEIGN_REQUEST_SPONSOR_HEADER);
        if (StringUtils.isNotBlank(feignServieName)) {
            // feign请求过来的接口，不去添加字段，直接返回原始结果
            handler.handleReturnValue(returnValue, returnType, mavContainer, webRequest);
        } else {
            if (returnValue instanceof ResultDto || returnValue instanceof Map) {
                // 如果是返回ResultDto，则取ResultDto的code和message字段
                Map returnMap = ObjectMapperUtils.instance().convertValue(returnValue, Map.class);
                operateReturnMap(returnMap);
                handler.handleReturnValue(returnMap, returnType, mavContainer, webRequest);
            } else if (returnValue instanceof TradeServerRespDto || returnValue instanceof HyyRespDto) {
                // ESB系统间交互不做处理
                Map returnMap = ObjectMapperUtils.instance().convertValue(returnValue, Map.class);
                handler.handleReturnValue(returnMap, returnType, mavContainer, webRequest);
            } else if (returnValue.getClass().isPrimitive()) {
                handler.handleReturnValue(returnValue, returnType, mavContainer, webRequest);
                LOGGER.info(">>>>>>>>返回为纯基础数据类型[{}]<<<<<<<<<<<<<", returnValue.getClass().getSimpleName());
            } else {
                handler.handleReturnValue(returnValue, returnType, mavContainer, webRequest);
                LOGGER.info(">>>>>>>>未知的返回对象类型[{}]<<<<<<<<<<<<<", returnValue.getClass().getSimpleName());
            }
        }
    }

    /**
     * 移动端特殊处理
     *
     * @param returnMap
     */
    private static void operateReturnMap(Map returnMap) {
        if (returnMap.containsKey(RESULTDTO_CODE_FIELD)) {
            String code = returnMap.get(RESULTDTO_CODE_FIELD).toString();
            if ("0".equals(code)) {
                returnMap.put(ZJG_COMMON_CODEKEY, ZJG_COMMON_DEFAULTCODE);
            } else {
                returnMap.put(ZJG_COMMON_CODEKEY, code);
            }
            returnMap.put(ZJG_COMMON_MSGKEY, returnMap.getOrDefault(RESULTDTO_MESSAGE_FIELD, ""));
        }
    }

}