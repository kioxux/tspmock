package cn.com.yusys.yusp.enums.out.common;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;
/**
 * @class DicTranEnum
 * @author 王玉坤
 * @date 2021/5/17 14:30
 * @version 1.0.0
 * @desc 字典项公共转换 key:信贷字典项 value:其他系统字典项
 * @修改历史: 修改时间    修改人员    修改原因
 */
public enum DicTranEnum {

    /** 与零售内评字典项转换 start **/
    // 担保方式转换
    GUAR_MODE_XDTOLSNP_00("00", "4", "信用"),
    GUAR_MODE_XDTOLSNP_10("10", "2", "抵押"),
    GUAR_MODE_XDTOLSNP_20("20", "3", "质押"),
    GUAR_MODE_XDTOLSNP_21("21", "3", "低风险质押"),
    GUAR_MODE_XDTOLSNP_30("30", "1", "保证"),
    // 婚姻状态转换
    MAR_STATUS_XDTOLSNP_20("20", "M", "已婚"),
    MAR_STATUS_XDTOLSNP_10("10", "S", "未婚"),
    MAR_STATUS_XDTOLSNP_40("40", "O", "离异"),
    MAR_STATUS_XDTOLSNP_30("30", "O", "丧偶"),
    MAR_STATUS_XDTOLSNP_60("90", "O", "未说明的婚姻状况"),

    // 职业转换
    /** 零售内评字典项
     MV 缺失
     0  国家机关、党群组织、企业、事业单位负责人
     1  专业技术人员
     3  办事人员和有关人员
     4  商业、服务业人员
     5  农、林、牧、渔、水利业生产人员
     6  生产、运输设备操作人员及有关人员
     7  军人
     8  不便分类的其他从业人员
     9  未知
    */
    PROFESSION_XDTOLSNP_010000("010000", "8", "工薪类职业"),
    PROFESSION_XDTOLSNP_010101("010101", "0", "国家机关事业单位"),
    PROFESSION_XDTOLSNP_010107("010107", "0", "税务"),
    PROFESSION_XDTOLSNP_010209("010209", "4", "社会服务"),
    PROFESSION_XDTOLSNP_010211("010211", "4", "健康/医疗服务"),
    PROFESSION_XDTOLSNP_010214("010214", "4", "法律/司法"),
    PROFESSION_XDTOLSNP_010303("010303", "6", "邮电通信"),
    PROFESSION_XDTOLSNP_010306("010306", "4", "银行/金融/证券/保险"),
    PROFESSION_XDTOLSNP_010400("010400", "7", "军人、警察、武警"),
    PROFESSION_XDTOLSNP_010504("010504", "1", "IT/网络/计算机"),
    PROFESSION_XDTOLSNP_010600("010600", "4", "商业/贸易"),
    PROFESSION_XDTOLSNP_010601("010601", "4", "一般工商业"),
    PROFESSION_XDTOLSNP_010719("010719", "4", "制造业"),
    PROFESSION_XDTOLSNP_010818("010818", "5", "农林畜牧"),
    PROFESSION_XDTOLSNP_010916("010916", "8", "广告"),
    PROFESSION_XDTOLSNP_011010("011010", "4", "旅游/餐饮/娱乐"),
    PROFESSION_XDTOLSNP_011113("011113", "6", "交通运输"),
    PROFESSION_XDTOLSNP_011212("011212", "6", "房地产/建筑/装修"),
    PROFESSION_XDTOLSNP_020000("020000", "8", "私营类职业"),
    PROFESSION_XDTOLSNP_020100("020100", "8", "个体"),
    PROFESSION_XDTOLSNP_020200("020200", "8", "合伙"),
    PROFESSION_XDTOLSNP_020300("020300", "8", "独资"),
    PROFESSION_XDTOLSNP_020400("020400", "9", "其他"),
    PROFESSION_XDTOLSNP_020402("020402", "8", "学生"),
    PROFESSION_XDTOLSNP_020403("020403", "8", "自由职业"),
    PROFESSION_XDTOLSNP_020408("020408", "8", "咨询"),
    PROFESSION_XDTOLSNP_020417("020417", "1", "科研/教育"),
    PROFESSION_XDTOLSNP_020418("020418", "4", "文化/娱乐/体育"),
    PROFESSION_XDTOLSNP_999999("999999", "8", "其他类职业"),

    // 公司性质转换
    /**
     MV 缺失
     1  机关事业国有企业
     2  独资企业
     3  合资合作企业
     4  股份制企业
     5  私营企业
     6  其他
     7  未知
     */
    COM_TYPE_XDTOLSNP_001("001", "1", "机关事业国有企业"),
    COM_TYPE_XDTOLSNP_008("008", "6", "合伙企业"),
    COM_TYPE_XDTOLSNP_007("007", "2", "个人独资"),
    COM_TYPE_XDTOLSNP_100("100", "1", "党政机关"),
    COM_TYPE_XDTOLSNP_200("200", "1", "事业单位"),
    COM_TYPE_XDTOLSNP_300("300", "6", "军队"),
    COM_TYPE_XDTOLSNP_400("400", "6", "社会团体"),
    COM_TYPE_XDTOLSNP_500("500", "6", "内资企业"),
    COM_TYPE_XDTOLSNP_510("510", "1", "国有企业"),
    COM_TYPE_XDTOLSNP_520("520", "6", "集体企业"),
    COM_TYPE_XDTOLSNP_530("530", "6", "股份合作企业"),
    COM_TYPE_XDTOLSNP_540("540", "6", "联营企业"),
    COM_TYPE_XDTOLSNP_550("550", "6", "有限责任公司"),
    COM_TYPE_XDTOLSNP_560("560", "4", "股份有限公司"),
    COM_TYPE_XDTOLSNP_570("570", "5", "私营企业"),
    COM_TYPE_XDTOLSNP_600("600", "6", "外商投资企业业(含港、澳、台)"),
    COM_TYPE_XDTOLSNP_610("610", "3", "中外合资经营企业(含港、澳、台)"),
    COM_TYPE_XDTOLSNP_620("620", "3", "中外合作经营企业(含港、澳、台)"),
    COM_TYPE_XDTOLSNP_630("630", "6", "外资企业(含港、澳、台)"),
    COM_TYPE_XDTOLSNP_640("640", "6", "外商投资股份有限公司(含港、澳、台)"),
    COM_TYPE_XDTOLSNP_700("700", "5", "个体经营"),
    COM_TYPE_XDTOLSNP_800("800", "6", "其他"),
    COM_TYPE_XDTOLSNP_900("900", "7", "未知"),

    // 还款方式转换
    /**
     1 等额本息
     2 等额本金
     3 利随本清
     4 其他
     */
    REPAY_MODE_XDTOLSNP_A001("A001", "4", "按期付息到期还本"),
    REPAY_MODE_XDTOLSNP_A002("A002", "1", "等额本息"),
    REPAY_MODE_XDTOLSNP_A003("A003", "2", "等额本金"),
    REPAY_MODE_XDTOLSNP_A009("A009", "3", "利随本清"),
    REPAY_MODE_XDTOLSNP_A012("A012", "4", "按226比例还款"),
    REPAY_MODE_XDTOLSNP_A013("A013", "4", "按月还息按季还本"),
    REPAY_MODE_XDTOLSNP_A014("A014", "4", "按月还息按半年还本"),
    REPAY_MODE_XDTOLSNP_A015("A015", "4", "按月还息,按年还本"),
    REPAY_MODE_XDTOLSNP_A016("A016", "4", "新226"),
    REPAY_MODE_XDTOLSNP_A017("A017", "4", "前6月按月还息，后6个月等额本息"),
    REPAY_MODE_XDTOLSNP_A018("A018", "4", "前4个月按月还息，后8个月等额本息"),
    REPAY_MODE_XDTOLSNP_A019("A019", "4", "第一年按月还息，接下来等额本息"),
    REPAY_MODE_XDTOLSNP_A020("A020", "4", "334比例还款"),
    REPAY_MODE_XDTOLSNP_A021("A021", "4", "433比例还款"),
    REPAY_MODE_XDTOLSNP_A022("A022", "4", "10年期等额本息"),
    REPAY_MODE_XDTOLSNP_A023("A023", "4", "按月付息，定期还本"),
    REPAY_MODE_XDTOLSNP_A030("A030", "4", "定制还款"),
    REPAY_MODE_XDTOLSNP_A031("A031", "4", "5年期等额本息"),
    REPAY_MODE_XDTOLSNP_A040("A040", "4", "按期付息,按计划还本"),
    REPAY_MODE_XDTOLSNP_A041("A041", "4", "其他方式"),

    //
    /** 与零售内评字典项转换 end **/

    /** 与零售只能风控转换开始 */
    CERT_TYPE_XDTORIRCP_A("A","10","身份证"),
    /** 与零售只能风控转换结束 */

    /** 与核心系统字典项转换开始 */
    // 币种转换
    CUR_TYPE_XDTOHX_CNY("CNY", "01", "人民币"),
    CUR_TYPE_XDTOHX_MOP("MOP", "81", "澳门币"),
    CUR_TYPE_XDTOHX_CAD("CAD", "28", "加元"),
    CUR_TYPE_XDTOHX_JPY("JPY", "27", "日元"),
    CUR_TYPE_XDTOHX_EUR("EUR", "38", "欧元"),
    CUR_TYPE_XDTOHX_GBP("GBP", "12", "英镑"),
    CUR_TYPE_XDTOHX_HKD("HKD", "13", "港元"),
    CUR_TYPE_XDTOHX_AUD("AUD", "29", "澳大利亚元"),
    CUR_TYPE_XDTOHX_USD("USD", "14", "美元"),
    CUR_TYPE_XDTOHX_SGD("SGD", "18", "新加坡元"),
    CUR_TYPE_XDTOHX_SEK("SEK", "21", "瑞典克郎"),
    CUR_TYPE_XDTOHX_DKK("DKK", "22", "丹麦克朗"),
    CUR_TYPE_XDTOHX_NOK("NOK", "23", "挪威克朗"),

    // 担保方式转换
    GUAR_MODE_XDTOHX_00("00", "4", "信用"),
    GUAR_MODE_XDTOHX_10("10", "1", "抵押"),
    GUAR_MODE_XDTOHX_20("20", "2", "质押"),
    GUAR_MODE_XDTOHX_21("21", "5", "低风险质押"),
    GUAR_MODE_XDTOHX_30("30", "3", "保证"),
    GUAR_MODE_XDTOHX_40("40", "3", "全额保证金"),

    // 贷款形式转换
    LOAN_MODAL_XDTOHX_3("3", "0", "借新还旧"),
    LOAN_MODAL_XDTOHX_6("6", "1", "无还本续贷"),
    LOAN_MODAL_XDTOHX_8("8", "1", "小企业无还本续贷"),

    // 利率调整方式
    RATE_ADJ_MODE_XDTOHX_01("01", "6", "固定利率"),
    RATE_ADJ_MODE_XDTOHX_02("02", "1", "浮动利率"),

    // 利率调整选项
    RATE_ADJ_TYPE_XDTOHX_IMM("IMM", "4", "立即调整"),
    RATE_ADJ_TYPE_XDTOHX_FIX("FIX", "2", "固定日调整"),
    RATE_ADJ_TYPE_XDTOHX_NNR("NNR", "1", "还款周期"),
    RATE_ADJ_TYPE_XDTOHX_NYF("NYF", "2", "每年1月1日调整"),
    RATE_ADJ_TYPE_XDTOHX_DDA("DDA", "1", "满一年调整"),

    // 还款方式
    //新核心字典项：：1--利随本清、2--多次还息一次还本、3--等额本息、4--等额本金、5--等比累进、6--等额累进、7--定制还款
    REPAY_MODE_XDTOHX_A001("A001", "2", "按期付息到期还本"),
    REPAY_MODE_XDTOHX_A002("A002", "3", "等额本息"),
    REPAY_MODE_XDTOHX_A003("A003", "4", "等额本金"),
    REPAY_MODE_XDTOHX_A004("A004", "2", "气球还款"),
    REPAY_MODE_XDTOHX_A009("A009", "1", "利随本清"),
    REPAY_MODE_XDTOHX_A012("A012", "", "按226比例还款"),
    REPAY_MODE_XDTOHX_A013("A013", "", "按月还息按季还本"),
    REPAY_MODE_XDTOHX_A014("A014", "", "按月还息按半年还本"),
    REPAY_MODE_XDTOHX_A015("A015", "", "按月还息,按年还本"),
    REPAY_MODE_XDTOHX_A016("A016", "", "新226"),
    REPAY_MODE_XDTOHX_A017("A017", "", "前6个月按月还息，后6个月等额本息"),
    REPAY_MODE_XDTOHX_A018("A018", "", "前4个月按月还息，后8个月等额本息"),
    REPAY_MODE_XDTOHX_A019("A019", "", "第一年按月还息，接下来等额本息"),
    REPAY_MODE_XDTOHX_A020("A020", "", "334比例还款"),
    REPAY_MODE_XDTOHX_A021("A021", "", "433比例还款"),
    REPAY_MODE_XDTOHX_A022("A022", "3", "10年期等额本息"),
    REPAY_MODE_XDTOHX_A023("A023", "2", "按月付息，定期还本"),
    REPAY_MODE_XDTOHX_A030("A030", "7", "定制还款"),
    REPAY_MODE_XDTOHX_A1("A1", "", "定制还款"),
    REPAY_MODE_XDTOHX_A2("A2", "", "定制还款"),
    REPAY_MODE_XDTOHX_A3("A3", "", "定制还款"),
    REPAY_MODE_XDTOHX_A031("A031", "3", "5年期等额本息"),
    REPAY_MODE_XDTOHX_A040("A040", "7", "按期付息,按计划还本"),
    /** 与核心系统字典项转换结束 */

    /** 与押品系统字典项转换开始 */
    // 担保方式转换
    GUAR_MODE_XDTOYP_00("00", "XY", "信用"),
    GUAR_MODE_XDTOYP_10("10", "DY", "抵押"),
    GUAR_MODE_XDTOYP_20("20", "ZY", "质押"),
    GUAR_MODE_XDTOYP_21("21", "ZY", "低风险质押"),
    GUAR_MODE_XDTOYP_30("30", "BZ", "保证"),
    GUAR_MODE_XDTOYP_40("40", "QE", "全额保证金");
    /** 与押品系统字典项转换开始 */

    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DicTranEnum enumData : EnumSet.allOf(DicTranEnum.class)) {
            keyValue.put(enumData.name(), enumData.value);
        }
    }

    public String key;
    public String value;
    public String desc;

    private DicTranEnum(String key, String value,  String desc) {
        this.key = key;
        this.value = value;
        this.desc = desc;
    }

    public static String lookup(String key) {
        if (keyValue.containsKey(key)) {
            return (String) keyValue.get(key);
        } else {
            return "";
        }
    }

    public static String key(String dataValue) {
        String key = null;
        for (DicTranEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
