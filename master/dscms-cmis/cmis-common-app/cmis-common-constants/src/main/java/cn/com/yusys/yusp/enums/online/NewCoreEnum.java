package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 新核心的枚举</br>
 *
 * @author mochi
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum NewCoreEnum {
    /* LN3111接口枚举start */
    /**
     * 贷款归还试算--还款种类
     */
    LN3111_HUANKZLE_1("1", "结清贷款"),//结清贷款
    LN3111_HUANKZLE_2("2", "归还欠款"),//归还欠款
    LN3111_HUANKZLE_3("3", "提前还款"),//提前还款
    /* LN3111接口枚举end */
    /* LN3108接口枚举start */
    /**
     * 贷款形态
     */
    LN3108_DAIKXTAI_0("0", "正常"),//正常
    Ln3108_DAIKXTAI_1("1", "逾期"),//逾期
    Ln3108_DAIKXTAI_2("2", "呆滞"),//呆滞
    Ln3108_DAIKXTAI_3("3", "呆账"),//呆账
    /**
     * 贷款账户状态
     */
    Ln3108_DKZHHZHT_0("0", "正常"),//正常
    Ln3108_DKZHHZHT_1("1", "销户"),//销户
    Ln3108_DKZHHZHT_2("2", "已核销"),//已核销
    Ln3108_DKZHHZHT_3("3", "准销户"),//准销户
    Ln3108_DKZHHZHT_4("4", "录入"),//录入
    Ln3108_DKZHHZHT_5("5", "已减免"),//已减免
    /**
     * 上一日借据状态标志  0-正常,1-逾期
     */
    Ln3108_JYCHGBZH_0("0", "正常"),//正常
    Ln3108_JYCHGBZH_1("1", "逾期"),//逾期
    /* LN3108接口枚举end */;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (NewCoreEnum enumData : EnumSet.allOf(NewCoreEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private NewCoreEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (NewCoreEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
