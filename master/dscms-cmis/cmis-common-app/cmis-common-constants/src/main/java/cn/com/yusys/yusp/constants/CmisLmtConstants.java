package cn.com.yusys.yusp.constants;


/**
 * 常量类:额度管理服务
 */
public class CmisLmtConstants {

    /**
     * 授信额度状态 未生效
     **/
    public static final String STD_ZB_LMT_STATE_00 = "00";
    /**
     * 授信额度状态 正常
     **/
    public static final String STD_ZB_LMT_STATE_01 = "01";
    /**
     * 授信额度状态 已冻结
     **/
    public static final String STD_ZB_LMT_STATE_02 = "02";
    /**
     * 授信额度状态 终止
     **/
    public static final String STD_ZB_LMT_STATE_03 = "03";
    /**
     * 授信额度状态 预生效
     **/
    public static final String STD_ZB_LMT_STATE_04 = "04";

    /**
     * 交易台账状态 100-未生效
     **/
    public static final String STD_ZB_BIZ_STATUS_100 = "100";
    /**
     * 交易台账状态 200-有效
     **/
    public static final String STD_ZB_BIZ_STATUS_200 = "200";
    /**
     * 交易台账状态 300-结清已使用
     **/
    public static final String STD_ZB_BIZ_STATUS_300 = "300";
    /**
     * 交易台账状态 400-结清未使用
     **/
    public static final String STD_ZB_BIZ_STATUS_400 = "400";
    /**
     * 交易台账状态 500-到期未结清
     **/
    public static final String STD_ZB_BIZ_STATUS_500 = "500";

    /**
     * 交易台账状态 查询余额所用到的状态
     **/
    public static final String STD_ZB_BIZ_STATUS_STR = STD_ZB_BIZ_STATUS_200 + "," + STD_ZB_BIZ_STATUS_500 + "," + STD_ZB_BIZ_STATUS_100;

    /*** 授信分项类型 01-额度分项***/
    public static final String STD_ZB_LMT_SUB_TYPE_01 = "01";
    /*** 授信分项类型 02-产品分项***/
    public static final String STD_ZB_LMT_SUB_TYPE_02 = "02";

    /*** 授信模式 01-综合授信 ***/
    public static final String STD_ZB_LMT_MODE_01 = "01";

    /*** 授信模式 02-单笔单批 ***/
    public static final String STD_ZB_LMT_MODE_02 = "02";

    /*** 额度类型 01-综合 ***/
    public static final String STD_ZB_LIMIT_TYPE_01 = "01";
    /*** 额度类型 02-专项 ***/
    public static final String STD_ZB_LIMIT_TYPE_02 = "02";

    /*** 交易业务类型 1-一般合同***/
    public static final String DEAL_BIZ_TYPE_1 = "1";
    /*** 交易业务类型 2-最高额合同***/
    public static final String DEAL_BIZ_TYPE_2 = "2";
    /*** 交易业务类型 3-最高额协议***/
    public static final String DEAL_BIZ_TYPE_3 = "3";

    /*** 交易业务类型 1-一般合同  && 2-最高额合同****/
    public static final String DEAL_BIZ_TYPE_STR = DEAL_BIZ_TYPE_1 + "," + DEAL_BIZ_TYPE_2 ;

    /*** 业务属性 1-合同***/
    public static final String STD_ZB_BIZ_ATTR_1 = "1";
    /*** 业务属性 2-台账***/
    public static final String STD_ZB_BIZ_ATTR_2 = "2";
    /*** 业务属性 3-授信***/
    public static final String STD_ZB_BIZ_ATTR_3 = "3";

    /*** 业务属性1-合同 3-协议 ***/
    public static final String STD_ZB_BIZ_ATTR_STR = STD_ZB_BIZ_ATTR_1+","+STD_ZB_BIZ_ATTR_3;

    /**
     * 额度类型 02-续授信
     **/
    public static final String LMT_TYPE_02 = "02";
    /**
     * 额度类型 03-新增复议
     **/
    public static final String LMT_TYPE_03 = "03";


    /*** 分项占用校验 额度类型 01-单一客户授信***/
    public static final String STD_ZB_LMT_TYPE_01 = "01";

    /*** 授信类型 03-合作方额度 ***/
    public static final String STD_ZB_LMT_TYPE_03 = "03";

    /*** 分项占用校验 额度类型 04-产品方授信***/
    public static final String STD_ZB_LMT_TYPE_04 = "04";

    /*** 授信类型 05-主体额度***/
    public static final String STD_ZB_LMT_TYPE_05 = "05";

    /*** 分项占用校验 额度类型 06-白名单额度***/
    public static final String STD_ZB_LMT_TYPE_06 = "06";

    /*** 分项占用校验 额度类型 07-同业额度***/
    public static final String STD_ZB_LMT_TYPE_07 = "07";
    /*** 授信类型 08-承销额度***/
    public static final String STD_ZB_LMT_TYPE_08 = "08";

    /**
     * 额度状态/批复状态 status  生效-01
     **/
    public static final String STD_ZB_APPR_ST_01 = "01";

    /**
     * 额度状态/批复状态 status  冻结-11
     **/
    public static final String STD_ZB_APPR_ST_11 = "11";

    /**
     * 额度状态/批复状态 status  失效已结清-99
     **/
    public static final String STD_ZB_APPR_ST_99 = "99";

    /**
     * 额度状态/批复状态 status  失效未结清-90
     **/
    public static final String STD_ZB_APPR_ST_90 = "90";

    /**
     * 额度状态修改范围  部分-01
     **/
    public static final String STD_ZB_APPR_INFO_01 = "01";

    /**
     * 额度状态修改范围  全部-02
     **/
    public static final String STD_ZB_APPR_INFO_02 = "02";

    /**
     * 是否  是-Y
     **/
    public static final String YES_NO_Y = "1";
    /**
     * 是否  否-N
     **/
    public static final String YES_NO_N = "0";

    public static final String IQP_LMT_REL_TYPE_SELF = "00";

    /**
     * 合同状态  未生效
     **/
    public static final String CONT_STATUS_INEFFECTIVE = "100";

    /**
     * 合同状态  生效
     **/
    public static final String CONT_STATUS_EFFECT = "200";

    /**
     * 合同状态  终止
     **/
    public static final String CONT_STATUS_STOP = "500";

    /**
     * 合同状态  注销
     **/
    public static final String CONT_STATUS_CANCEL = "600";

    /**
     * 合同状态  撤回
     **/
    public static final String CONT_STATUS_RECALL = "700";

    /**
     * 合同状态
     **/
    public static final String CONT_STATUS_DELETE = "800";

    /**
     * 操作标识 01-新增
     **/
    public static final String OPR_TYPE_ADD = "01";

    /**
     * 操作标识 02-删除
     **/
    public static final String OPR_TYPE_DELETE = "02";

    /**
     * 授信申请流水号序列号
     **/
    public static final String LMT_SERNO = "LMT_SERNO";

    /**
     * 授信申请分项流水号序列号
     **/
    public static final String LMT_SUB_SERNO = "LMT_SUB_SERNO";

    /**
     * 授信申请流水号序列号
     **/
    public static final String PK_VALUE = "PK_VALUE";

    /*** 例外类型 01-忽略冻结状态***/
    public static final String EXPT_TYPE_01 = "01";
    /*** 例外类型 02-允许突破额度***/
    public static final String EXPT_TYPE_02 = "02";
    /*** 例外类型 03-允许突破限额***/
    public static final String EXPT_TYPE_03 = "03";
    /*** 例外类型 04-不占用额度***/
    public static final String EXPT_TYPE_04 = "04";

    /*** 交易业务类型 1-一般合同***/
    public static final String STD_ZB_CONT_TYPE_1 = "1";
    /*** 交易业务类型 2-最高额合同***/
    public static final String STD_ZB_CONT_TYPE_2 = "2";
    /*** 交易业务类型 3-最高协议***/
    public static final String STD_ZB_CONT_TYPE_3 = "3";

    /*** 交易业务状态 200-有效***/
    public static final String STD_ZB_CONT_STATUS_200 = "200";
    /*** 交易业务状态 400-失效***/
    public static final String STD_ZB_CONT_STATUS_400 = "400";

    /**
     * 额度台账状态  001-预生效
     **/
    public static final String LMT_CTR_STATUS_001 = "001";
    /**
     * 额度台账状态  002-生效
     **/
    public static final String LMT_CTR_STATUS_002 = "002";
    /**
     * 额度台账状态  003-注销
     **/
    public static final String LMT_CTR_STATUS_003 = "003";

    /**
     * 申请类型 app_type  冻结-01
     **/
    public static final String LMT_APP_TYPE_01 = "01";
    /**
     * 申请类型 app_type  解冻-02
     **/
    public static final String LMT_APP_TYPE_02 = "02";

    /**
     * 审批状态 app_status  通过-997
     **/
    public static final String APP_STATUS_997 = "997";

    /**
     * 审批状态 app_status  审批中-111
     **/
    public static final String APP_STATUS_111 = "111";

    /**
     * 授信额度台账类型  01-循环额度类型
     **/
    public static final String LMT_ACC_TYPE_01 = "01";
    /**
     * 授信额度台账类型  02-一次性额度类型
     **/
    public static final String LMT_ACC_TYPE_02 = "02";

    /**
     * 定义第三方数据查询接口的方法 1-第三方额度查询
     **/
    public static final String TH_QUERY_OP_TYPE_1 = "thQuery";
    /**
     * 第三方额度类型  01-商圈
     **/
    public static final String TH_LIMIT_TYPE_01 = "01";
    /**
     * 第三方额度类型  02-融资担保公司
     **/
    public static final String TH_LIMIT_TYPE_02 = "02";
    /**
     * 第三方额度类型  03-合作方额度
     **/
    public static final String TH_LIMIT_TYPE_03 = "03";

    /**
     * 额度台账状态  001-预生效
     **/
    public static final String LMT_ACC_STATUS_001 = "001";
    /**
     * 额度台账状态  002-生效
     **/
    public static final String LMT_ACC_STATUS_002 = "002";
    /**
     * 额度台账状态  003-注销
     **/
    public static final String LMT_ACC_STATUS_003 = "003";

    /**
     * 贷款形式  1 新增贷款
     **/
    public static final String LOAN_MODAL_1 = "1";
    /**
     * 贷款形式  2 收回再贷
     **/
    public static final String LOAN_MODAL_2 = "2";
    /**
     * 贷款形式  3 借新还旧
     **/
    public static final String LOAN_MODAL_3 = "3";
    /**
     * 贷款形式  4 资产重组
     **/
    public static final String LOAN_MODAL_4 = "4";
    /**
     * 贷款形式  5 转入
     **/
    public static final String LOAN_MODAL_5 = "5";
    /**
     * 贷款形式  6 其他
     **/
    public static final String LOAN_MODAL_6 = "6";

    /**
     * 额度冻结/解冻标志位 冻结-frozen
     ****/
    public static final String LMT_OPETYPE_FROZE = "frozen";
    /**
     * 额度冻结/解冻标志位 解冻-unFreeze
     **/
    public static final String LMT_OPETYPE_UNFREEZE = "unFreeze";
    /**
     * 操作标志位 新增-nextAdd
     **/
    public static final String LMT_OP_TYPE_NEXT_ADD = "nextAdd";
    /**
     * 操作标志位 修改-updateAmt
     **/
    public static final String LMT_OP_TYPE_UPDATE_AMT = "updateAmt";

    /**
     * 恢复类型  01-结清恢复
     **/
    public static final String STD_RECOVER_TYPE_01 = "01";
    /**
     * 恢复类型  02-未用注销
     **/
    public static final String STD_RECOVER_TYPE_02 = "02";
    /**
     * 恢复类型  03-还款
     **/
    public static final String STD_RECOVER_TYPE_03 = "03";
    /**
     * 恢复类型  04-追加保证金
     **/
    public static final String STD_RECOVER_TYPE_04 = "04";
    /**
     * 恢复类型  05-押品出库
     **/
    public static final String STD_RECOVER_TYPE_05 = "05";
    /**
     * 恢复类型  06-撤销占用
     **/
    public static final String STD_RECOVER_TYPE_06 = "06";
    /**
     * 恢复类型  07-信用证修改
     **/
    public static final String STD_RECOVER_TYPE_07 = "07";
    /**
     * 恢复类型  08-提前终止
     * 恢复类型  08-未用退回
     **/
    public static final String STD_RECOVER_TYPE_08 = "08";

    /**
     * 客户主体类型 1-对私
     **/
    public static final String STD_ZB_CUS_CATALOG1 = "1";
    /**
     * 客户主体类型 2-对公
     **/
    public static final String STD_ZB_CUS_CATALOG2 = "2";
    /**
     * 客户主体类型 3-同业
     **/
    public static final String STD_ZB_CUS_CATALOG3 = "3";
    /**
     * 客户主体类型 4-集团
     **/
    public static final String STD_ZB_CUS_CATALOG4 = "4";

    /**
     * 白名单额度类型 01-承兑行白名单额度
     **/
    public static final String STD_ZB_LMT_WHITE_TYPE_01 = "01";
    /**
     * 白名单额度类型 02-产品买入返售(信用债)额度
     **/
    public static final String STD_ZB_LMT_WHITE_TYPE_02 = "02";

    /**
     * 额度统计类型 11-资金业务存量授信
     **/
    public static final String STD_ZB_LMT_STATS_TYPE_11 = "11";
    /**
     * 额度统计类型 13-债券投资存量业务
     **/
    public static final String STD_ZB_LMT_STATS_TYPE_13 = "13";
    /**
     * 额度统计类型 21-敞口存量授信
     **/
    public static final String STD_ZB_LMT_STATS_TYPE_21 = "21";
    /**
     * 额度统计类型 22-低风险存量授信
     **/
    public static final String STD_ZB_LMT_STATS_TYPE_22 = "22";

    /**
     * 额度品种编号 3001-同业融资类额度
     **/
    public static final String STD_ZB_LMT_BIZ_TYPE_3001 = "3001";
    /**
     * 额度品种编号 3002-同业投资类额度
     **/
    public static final String STD_ZB_LMT_BIZ_TYPE_3002 = "3002";
    /**
     * 额度品种编号 3004-同业交易类额度
     **/
    public static final String STD_ZB_LMT_BIZ_TYPE_3005 = "3005";
    /**
     * 额度品种编号 3006-同业管理类额度
     **/
    public static final String STD_ZB_LMT_BIZ_TYPE_3006 = "3006";
    /**
     * 额度品种编号 300601-货币基金额度
     **/
    public static final String STD_ZB_LMT_BIZ_TYPE_300601 = "300601";
    /**
     * 额度品种名称 300601-货币基金额度
     **/
    public static final String STD_ZB_LMT_BIZ_TYPE_NAME_300601 = "货币基金额度";

    /**
     * 是否   Y-是
     **/
    public static final String STD_ZB_YES_NO_Y = "1";
    /**
     * 是否   N-否
     **/
    public static final String STD_ZB_YES_NO_N = "0";

    /**
     * 单笔投资业务类型  4001--债券池
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4001 = "4001";

    /**
     * 单笔投资业务类型  4002--债券投资
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4002 = "4002";

    /**
     * 单笔投资业务类型  4003--资产-其他
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4003 = "4003";

    /**
     * 单笔投资业务类型  4004--资产-债权融资计划、理财直融工具
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4004 = "4004";

    /**
     * 单笔投资业务类型  4005--净值型产品
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4005 = "4005";

    /**
     * 单笔投资业务类型  4006--资产证券化产品（标准）
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4006 = "4006";

    /**
     * 单笔投资业务类型  4007--资产证券化产品（非标）
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4007 = "4007";

    /**
     * 单笔投资业务类型  4008--其他标准化债权投资
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4008 = "4008";

    /**
     * 单笔投资业务类型  4009--债务融资工具（投资）
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4009 = "4009";

    /**
     * 单笔投资业务类型  4010--理财直融工具（投资）
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4010 = "4010";

    /**
     * 单笔投资业务类型  4011--结构化融资
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4011 = "4011";

    /**
     * 单笔投资业务类型  4012--其他非标债权投资
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_4012 = "4012";

    /**
     * 单笔投资业务类型  4012--其他非标债权投资
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_16010101 = "债务融资工具（承销）";

    /**
     * 单笔投资业务类型  4012--其他非标债权投资
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_16020101 = "理财直融工具（承销）";

    /**
     * 单笔投资业务类型  3002--同业投资类
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_3002 = STD_ZB_PRD_BIZ_TYPE_4001 + "," + STD_ZB_PRD_BIZ_TYPE_4002 + "," + STD_ZB_PRD_BIZ_TYPE_4008;

    /**
     * 单笔投资业务类型  3006--同业管理类
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_3006 = STD_ZB_PRD_BIZ_TYPE_4005 + "," + STD_ZB_PRD_BIZ_TYPE_4006 + "," + STD_ZB_PRD_BIZ_TYPE_4007;

    /**
     * 单笔投资业务类型  400101--债券池自营额度
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_400101 = "400101";
    /**
     * 单笔投资业务类型  400101--债券池自营额度
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_400101_NAME = "债券池自营额度";

    /**
     * 单笔投资业务类型  400102--债券池资管额度
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_400102 = "400102";
    /**
     * 单笔投资业务类型  400102--债券池资管额度
     **/
    public static final String STD_ZB_PRD_BIZ_TYPE_400102_NAME = "债券池资管额度";


    /**
     * 资金业务授信申报审批流程（总行发起-除债券池） -- 单笔投资授信
     **/
    public static final String STD_TYSX02_LMT_SIG_INVEST_APP = "lmtSigInvestApp";

    /**
     * 单笔投资业务类型  4012--其他非标债权投资  -- 同业授信
     **/
    public static final String STD_TYSX02_LMT_INTBANK_APP = "lmtIntbankApp";

    /**
     * 01 授信新增
     **/
    public static final String STD_SX_LMT_TYPE_01 = "01";
    /**
     * 02 授信变更
     **/
    public static final String STD_SX_LMT_TYPE_02 = "02";
    /**
     * 03 授信续作
     **/
    public static final String STD_SX_LMT_TYPE_03 = "03";
    /**
     * 05 授信复议
     **/
    public static final String STD_SX_LMT_TYPE_05 = "05";

    //常用的分隔符 等号  ,
    public final static String COMMON_SPLIT_COMMA = ",";

    /**
     * 01 待提交
     **/
    public static final String STD_LMT_DISC_ORG_STATUS_01 = "01";
    /**
     * 02 待审核
     **/
    public static final String STD_LMT_DISC_ORG_STATUS_02 = "02";
    /**
     * 03 已生效
     **/
    public static final String STD_LMT_DISC_ORG_STATUS_03 = "03";
    /**
     * 04 已失效
     **/
    public static final String STD_LMT_DISC_ORG_STATUS_04 = "04";

    /**
     * 发送ComStar 01-同业综合授信额度
     **/
    public static final String COMSTAR_LMT_TYPE_01 = "01";
    /**
     * 发送ComStar 02-单笔投资业务额度
     **/
    public static final String COMSTAR_LMT_TYPE_02 = "02";
    /**
     * 发送ComStar 01-调整
     **/
    public static final String COMSTAR_OPT_TYPE_01 = "01";

    /**
     * 底层融资人额度品种编号
     **/
    public static final String LIMIT_SUB_NO_15030101 = "15030101";

    /**
     * 额度品种 一般低风险-12010103
     */
    public static final String LIMIT_SUB_NO_12010103 ="12010103" ;

    /**
     * 底层融资人额度品种编号
     **/
    public static final String LIMIT_SUB_NAME_15030101 = "穿透化额度";

    /**
     * 标准化投资类型 4 企业债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_4 = "4";

    /**
     * 标准化投资类型 E 公司债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_E = "E";

    /**
     * 标准化投资类型 N 中期票据
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_N = "N";

    /**
     * 标准化投资类型 N1 PPN
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_N1 = "N1";

    /**
     * 标准化投资类型 6 短期融资券
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_6 = "6";

    /**
     * 标准化投资类型 O 超短期融资券
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_O = "O";

    /**
     * 标准化投资类型 G 项目收益债券
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_G = "G";

    /**
     * 标准化投资类型 J 绿色债务融资工具
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_J = "J";

    /**
     * 标准化投资类型 M 地方政府债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_M = "M";

    /**
     * 标准化投资类型 Q 政府支持机构债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_Q = "Q";

    /**
     * 标准化投资类型 H 项目收益票据
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_H = "H";

    /**
     * 标准化投资类型 9 商业银行
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_9 = "9";

    /**
     * 标准化投资类型 C 非银行金融债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_C = "C";

    /**
     * 标准化投资类型 C1 汽车金融公司金融债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_C1 = "C1";

    /**
     * 标准化投资类型 C3 资产管理公司金融债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_C3 = "C3";

    /**
     * 标准化投资类型 C4 金融租赁公司金融债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_C4 = "C4";

    /**
     * 标准化投资类型 C5 证券公司债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_C5 = "C5";

    /**
     * 标准化投资类型 C6 财务公司债
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_C6 = "C6";

    /**
     * 标准化投资类型 61 证券公司短期融资券
     **/
    public static final String STD_ZB_NORM_INVEST_TYPE_61 = "61";

    public static final String STD_ZB_NORM_INVEST_TYPE_DMD = STD_ZB_NORM_INVEST_TYPE_4 + "," + STD_ZB_NORM_INVEST_TYPE_E +
            "," + STD_ZB_NORM_INVEST_TYPE_N + "," + STD_ZB_NORM_INVEST_TYPE_N1 + "," + STD_ZB_NORM_INVEST_TYPE_6
            + "," + STD_ZB_NORM_INVEST_TYPE_O + "," + STD_ZB_NORM_INVEST_TYPE_G + "," + STD_ZB_NORM_INVEST_TYPE_J
            + "," + STD_ZB_NORM_INVEST_TYPE_M+ "," + STD_ZB_NORM_INVEST_TYPE_Q+ "," + STD_ZB_NORM_INVEST_TYPE_H
            + "," + STD_ZB_NORM_INVEST_TYPE_9+ "," + STD_ZB_NORM_INVEST_TYPE_C+ "," + STD_ZB_NORM_INVEST_TYPE_C1
            + "," + STD_ZB_NORM_INVEST_TYPE_C3+ "," + STD_ZB_NORM_INVEST_TYPE_C4+ "," + STD_ZB_NORM_INVEST_TYPE_C5
            + "," + STD_ZB_NORM_INVEST_TYPE_C6+ "," + STD_ZB_NORM_INVEST_TYPE_61 ;

    /**
     * STD_REPLY_STATUS  01 生效
     **/
    public static final String STD_REPLY_STATUS_01 = "01";

    /**
     * STD_REPLY_STATUS  02 失效
     **/
    public static final String STD_REPLY_STATUS_02 = "02";

    /**
     * 额度品种编号 默认 300701-产品户买入返售（信用债）
     **/
    public static final String LIMIT_SUB_NO_300701 = "300701";

    /**
     * 额度品种编号 默认 300701-产品户买入返售（信用债）
     **/
    public static final String LIMIT_SUB_NO_300701_NAME = "产品户买入返售（信用债）";

    /**
     * 票据产品编号和名称  052198 银行承兑汇票直现
     */
    public static final String STD_PJ_PRD_ID_052198 = "052198";
    public static final String STD_PJ_PRD_NAME_052198 = "银行承兑汇票直现";

    /**
     * 票据产品编号和名称  052199 商业承兑汇票直现
     */
    public static final String STD_PJ_PRD_ID_052199 = "052199";
    public static final String STD_PJ_PRD_NAME_052199 = "商业承兑汇票直现";

    /**
     * 新微贷额度品种编号  200101---般个人经营性贷款 200201--特色个人经营性贷款  210101--小微消费类贷款 210202--个人消费非按揭类
     */
    public static final String XWD_LMT_SUB_NO = "200101,200201,210101,210202";

    /**
     * cmisLmt0037  批复查询类型 1：根据批复编号查询
     */
    public static final String CMISLMT0037_QUERY_TYPE_1 = "1";
    /**
     * cmisLmt0037  批复查询类型 2：根据分项编号查询
     */
    public static final String CMISLMT0037_QUERY_TYPE_2 = "2";

    /**
     * 补充保证金释放金额倍数
     */
    public static final String FY_BAIL_REL_RATE = "FY_BAIL_REL_RATE";

    /**
     * 是否校验授信总额倍数
     */
    public static final String CHECK_TOTAL_SALES = "CHECK_TOTAL_SALES";

    /**
     * 集团客户认定
     */
    public static final String STD_GRP_APP_TYPE_01 = "01";
    /**
     * 集团客户变更
     */
    public static final String STD_GRP_APP_TYPE_02 = "02";
    /**
     * 集团客户解散
     */
    public static final String STD_GRP_APP_TYPE_03 = "03";

    public static final String APPR_COOP_SUB_INFO = "APPR_COOP_SUB_INFO";
    public static final String APPR_COOP_SUB_INFO_OUTSTNDAMT = "APPR_COOP_SUB_INFO_OUTSTNDAMT";
    public static final String LMT_WHITE_INFO = "LMT_WHITE_INFO";
    public static final String LMT_WHITE_INFO_SIG_USE_AMT = "LMT_WHITE_INFO_SIG_USE_AMT";

    /**
     * 单一法人额度建立服务码
     */
    public static final String CMIS_LMT0001_SERVICE_CODE = "cmisLmt0001";
    /**
     * 合作方额度建立服务码
     */
    public static final String CMIS_LMT0004_SERVICE_CODE = "cmisLmt0004";
    /**
     * 资金同业额度建立服务码
     */
    public static final String CMIS_LMT0005_SERVICE_CODE = "cmisLmt0005";
    /**
     * 额度占用接口服务码
     */
    public static final String CMIS_LMT0011_SERVICE_CODE = "cmisLmt0011";
    /**
     * 额度恢复接口服务码
     */
    public static final String CMIS_LMT0012_SERVICE_CODE = "cmisLmt0012";
    /**
     * 台账占用服务码
     */
    public static final String CMIS_LMT0013_SERVICE_CODE = "cmisLmt0013";
    /**
     * 台账恢复服务码
     */
    public static final String CMIS_LMT0014_SERVICE_CODE = "cmisLmt0014";
    /**
     * 更新台账编号及出账成功通知服务码
     */
    public static final String CMIS_LMT0029_SERVICE_CODE = "cmisLmt0029";

    /**
     * 查询类型 01 贷款 02 非标
     */
    public static final String CMIS_QUERY_TYPE_01 = "01";
    /**
     * 查询类型 01 贷款 02 非标和贷款
     */
    public static final String CMIS_QUERY_TYPE_02 = "02";

    /**
     * 合作法额度不做超额校验产品配置【admin_sm_prop】
     */
    public static final String LMT_COOP_UNCHECK_PRD = "LMT_COOP_UNCHECK_PRD";

    /**
     * 业务阶段类型 01-业务申请阶段
     */
    public static final String STD_BUSS_STAGE_TYPE_01 ="01" ;
    /**
     * 业务阶段类型 02-合同签订阶段
     */
    public static final String STD_BUSS_STAGE_TYPE_02 ="02" ;

    /**
     * 合作方类别 1-房地产开发商
     */
    public static final String STD_ZB_PARTNER_TYPE_1 ="1" ;
    /**
     * 合作方类别 2-专业担保公司
     */
    public static final String STD_ZB_PARTNER_TYPE_2 ="2" ;
    /**
     * 合作方类别 3-保险公司
     */
    public static final String STD_ZB_PARTNER_TYPE_3 ="3" ;
    /**
     * 合作方类别 4-集群贷市场方
     */
    public static final String STD_ZB_PARTNER_TYPE_4 ="4" ;
    /**
     * 合作方类别 5-光伏设备生产企业
     */
    public static final String STD_ZB_PARTNER_TYPE_5 ="5" ;
    /**
     * 合作方类别 8-核心企业
     */
    public static final String STD_ZB_PARTNER_TYPE_8 ="8" ;

    /**
     * 法人客户综合额度体系 01-法人客户综合额度体系
     */
    public static final String STD_ZB_LMT_STR_MTABLE_CONF_01 ="01" ;
    /**
     * 个人客户额度体系 02-个人客户额度体系
     */
    public static final String STD_ZB_LMT_STR_MTABLE_CONF_02 ="02" ;
    /**
     * 同业额度体系 03-同业额度体系
     */
    public static final String STD_ZB_LMT_STR_MTABLE_CONF_03 ="03" ;
    /**
     * 合作方额度体系 04-合作方额度体系
     */
    public static final String STD_ZB_LMT_STR_MTABLE_CONF_04 ="04" ;

    /**
     * 恢复标准值 01-余额
     */
    public static final String STD_ZB_RECOVER_STD_01 ="01" ;
    /**
     * 恢复标准值 02-差额
     */
    public static final String STD_ZB_RECOVER_STD_02 ="02" ;

    /**
     * 恢复标准值 02-差额
     */
    public static final String LARGE_LIMIT_TYPE_TDMDZTTZ ="4009,4004,4005" ;

    /**
     * 额度占用交易流水号
     */
    public static final String DEAL_BIZ_NO = "DEAL_BIZ_NO";

    /**
     * 台账占用交易流水号
     */
    public static final String TRAN_ACC_NO = "TRAN_ACC_NO";

    /**
     * 出账校验日期，不校验的产品
     */
    public static final String PRD_ID_CONT_UNCHECK = "PRD_ID_CONT_UNCHECK";

    /**
     * 合同校验日期，不校验的产品
     */
    public static final String PRD_ID_LMT_UNCHECK = "PRD_ID_LMT_UNCHECK";

    /**
     * 大额风险暴露客户类型 01-非同业单一客户
     */
    public static final String STD_DE_CUS_TYPE_01 = "01";

    /**
     * 大额风险暴露客户类型 02-非同业关联客户
     */
    public static final String STD_DE_CUS_TYPE_02 = "02";

    /**
     * 大额风险暴露客户类型 03-同业单一客户
     */
    public static final String STD_DE_CUS_TYPE_03 = "03";

    /**
     * 大额风险暴露客户类型 04-同业关联客户
     */
    public static final String STD_DE_CUS_TYPE_04 = "04";

    /**
     * 大额风险暴露客户类型 05-匿名客户
     */
    public static final String STD_DE_CUS_TYPE_05 = "05";

    /**
     * 大额风险暴露客户类型 07-中央交易对手
     */
    public static final String STD_DE_CUS_TYPE_07 = "07";

    /**
     * 大额风险暴露客户类型 默认为0，不做计算
     */
    public static final String STD_DE_CUS_TYPE_0 = "0";

    /**
     *  客户大类 1：个人客户
     */
    public static final String STD_ZB_CUS_TYPE_1 = "1";

    /**
     * 客户大类 2：法人客户
     */
    public static final String STD_ZB_CUS_TYPE_2 = "2";

    /**
     * 客户大类 3：同业客户
     */
    public static final String STD_ZB_CUS_TYPE_3 = "3";

    /**
     * 合作方客户特殊处理   --start
     */
    /** 张家港市农业融资担保有限公司（客户号：8001092178，专业担保公司），
     * 信保贷产品使用该合作方担保额度时不做超额校验控制 ，出账时不做超额校验控制 **/
    public static final String STD_COOP_CUS_ID_8001092178 = "8001092178";
    /**
     * 江苏扬帆广睿国际贸易有限公司（客户号：8000749823，集群贷市场方），合同占用合作方额度是，
     * 不做超额校验控制 ，出账时做超额校验控制
     */
    public static final String STD_COOP_CUS_ID_8000749823 = "8000749823";
    /**
     * 合作方客户特殊处理   --end
     */

    /**
     * 产品类型属性 P010 信保贷
     */
    public static final String STD_PRD_TYPE_PROP_P010 = "P010";

    /**
     * 商票保贴 10020201
     */
    public static final String STD_PRD_SPBT_10020201 = "10020201";

    /**
     * 大额风险暴露指标类型 01-非同业单一客户贷款余额占比
     */
    public enum STD_DE_RISK_TYPE {
        /**
         * 01-非同业单一客户贷款余额占比      cont_lon_bal 非同业单一客户贷款余额 ----------->    01-非同业单一客户
         */
        TYPE_01("01","contLonBal","01"),
        /**
         * 02-非同业单一客户风险暴露占比     ftydy_khmx_expo 非同业单一客户风险暴露  ----------->    01-非同业单一客户
         */
        TYPE_02("02","ftydyKnmxExpo","01"),
        /**
         * 03-非同业关联客户风险暴露占比     ftygl_khmx_expo 非同业关联客户风险暴露  ----------->    02-非同业关联客户
         */
        TYPE_03("03","ftyglKhmxExpo","02"),
        /**
         * 04-同业单一客户风险暴露占比       tydy_khmx_expo 同业单一客户风险暴露  ----------->    03-同业单一客户
         */
        TYPE_04("04","tydyKhmxExpo","03"),
        /**
         * 05-同业关联客户风险暴露占比      tygl_khmx_expo 同业关联客户风险暴露   ----------->    04-同业关联客户
         */
        TYPE_05("05","tyglKhmxExpo","04"),
        /**
         * 06-匿名客户风险暴露占比         mmkh_khmx_expo 匿名客户风险暴露    ----------->    05-匿名客户
         */
        TYPE_06("06","mmkhKhmxExpo","05"),
        /**
         * 07-合格中央交易对手非清算风险暴露占比  0 ----------->  07-中央交易对手
         */
        TYPE_07("07","","07"),
        /**
         * 08-场外衍生交易名义本金对总资产占比   0 ----------->    默认为0，不做计算
         */
        TYPE_08("08","","0");

        public String riskType;
        public String zbVal;
        public String cusTypeId;

        STD_DE_RISK_TYPE(String riskType, String zbVal, String cusTypeId) {
            this.riskType = riskType;
            this.zbVal = zbVal;
            this.cusTypeId = cusTypeId;
        }



        public String getRiskType() {
            return riskType;
        }

        public void setRiskType(String riskType) {
            this.riskType = riskType;
        }

        public String getZbVal() {
            return zbVal;
        }

        public void setZbVal(String zbVal) {
            this.zbVal = zbVal;
        }

        public String getCusTypeId() {
            return cusTypeId;
        }

        public void setCusTypeId(String cusTypeId) {
            this.cusTypeId = cusTypeId;
        }

        public static String matchZbValue(String riskType){
            STD_DE_RISK_TYPE[] enums = STD_DE_RISK_TYPE.values();
            for (STD_DE_RISK_TYPE anEnum : enums) {
                if (riskType.equals(anEnum.getRiskType())){
                    return anEnum.zbVal;
                }
            }
            return "";
        }

        public static String matchCusTypeId(String riskType){
            STD_DE_RISK_TYPE[] enums = STD_DE_RISK_TYPE.values();
            for (STD_DE_RISK_TYPE anEnum : enums) {
                if (riskType.equals(anEnum.getRiskType())){
                    return anEnum.cusTypeId;
                }
            }
            return "";
        }

        public static String matchRiskType(String cusTypeId){
            STD_DE_RISK_TYPE[] enums = STD_DE_RISK_TYPE.values();
            for (STD_DE_RISK_TYPE anEnum : enums) {
                if (cusTypeId.equals(anEnum.getCusTypeId())){
                    return anEnum.riskType;
                }
            }
            return "";
        }

        public static String matchZbValueByCustTypeId(String custTypeId) {
            STD_DE_RISK_TYPE[] enums = STD_DE_RISK_TYPE.values();
            for (STD_DE_RISK_TYPE anEnum : enums) {
                if (custTypeId.equals(anEnum.getCusTypeId())){
                    return anEnum.zbVal;
                }
            }
            return "";
        }

        public static String matchRiskTypeByCustTypeId(String custTypeId) {
            STD_DE_RISK_TYPE[] enums = STD_DE_RISK_TYPE.values();
            for (STD_DE_RISK_TYPE anEnum : enums) {
                if (custTypeId.equals(anEnum.getCusTypeId())){
                    return anEnum.riskType;
                }
            }
            return "";
        }
    };
}
