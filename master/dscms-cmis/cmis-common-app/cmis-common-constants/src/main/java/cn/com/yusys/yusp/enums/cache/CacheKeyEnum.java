package cn.com.yusys.yusp.enums.cache;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中缓存的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum CacheKeyEnum {

    CACHE_KEY_CUSBASE("CusBase::", "客户基本信息缓存"),
    CACHE_KEY_CUSINDIV("CusIndiv::", "个人客户基本信息缓存"),
    CACHE_KEY_DATADICT("datadict", "字典项缓存"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (CacheKeyEnum enumData : EnumSet.allOf(CacheKeyEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private CacheKeyEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return keyValue.get(key);
    }

    public static String getKey(String dataValue) {
        String key = null;
        for (CacheKeyEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String getValue() {
        return keyValue.get(key);
    }
}
