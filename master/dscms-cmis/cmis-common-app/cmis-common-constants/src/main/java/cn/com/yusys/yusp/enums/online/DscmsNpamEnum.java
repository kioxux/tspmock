package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中资产保全管理服务的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsNpamEnum {
    TEST_ENUM("TEST", "中文描述"),//中文描述
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsNpamEnum enumData : EnumSet.allOf(DscmsNpamEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsNpamEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsNpamEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
