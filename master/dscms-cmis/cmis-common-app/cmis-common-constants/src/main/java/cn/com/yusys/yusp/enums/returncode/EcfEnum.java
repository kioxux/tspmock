package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 参数服务的错误异常枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EcfEnum {
    ECF020000("20000", "查询成功"),//查询成功
    ECF020001("20001", "参数信息获取失败"),//参数信息获取失败
    ECF020002("20002", "查询的汇率信息不存在"),//查询的汇率信息不存在
    ECF020003("20003", "该机构已存在，请重新选择"),//该机构已存在，请重新选择
    ECF020004("20004", "该数据已被产品引用，不能删除"),//该数据已被产品引用，不能删除
    ECF020005("20005", "配置参数所在表不在同一数据源中,无法查询，请重新配置"),//配置参数所在表不在同一数据源中,无法查询，请重新配置
    ECF020006("20006", "参数缺失"),//参数缺失
    ECF020007("20007", "数据流配置不存在"),//数据流配置不存在
    ECF020008("20008", "数据流映射配置不存在"),//数据流映射配置不存在
    ECF020009("20009", "数据已存在"),//数据已存在
    ECF020010("20010", "cfg服务查询参数配置异常！异常原因："),//cfg服务查询参数配置异常！异常原因：
    ECF020011("20011", "查询参数配置-入参为空"),//查询参数配置-入参为空
    ECF020012("20012", "查询参数配置-未获取到指定的参数配置信息"),//查询参数配置-未获取到指定的参数配置信息
    ECF020013("20013", "查询参数配置-存在多条生效的参数配置"),//查询参数配置-存在多条生效的参数配置
    ECF020014("20014", "cfg服务变更项目池当前分配规则关联状态时异常！异常原因："),//cfg服务变更项目池当前分配规则关联状态时异常！异常原因：
    ECF020015("20015", "cfg服务更新当前分配规则登记信息时异常！异常原因："),//cfg服务更新当前分配规则登记信息时异常！异常原因：
    ECF020016("20016", "cfg服务新增项目池与分配规则关联信息时异常！异常原因："),//cfg服务新增项目池与分配规则关联信息时异常！异常原因：
    ECF020017("20017", "项目池未关联分配规则"),//项目池未关联分配规则
    ECF020018("20018", "查询项目池关联岗位信息失败"),//查询项目池关联岗位信息失败
    ECF020019("20019", "核心科目计算获取异常"),//核心科目计算获取异常
    ECF040000("40000", "CFG服务调用失败"),//CFG服务调用失败
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EcfEnum enumData : EnumSet.allOf(EcfEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EcfEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EcfEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
