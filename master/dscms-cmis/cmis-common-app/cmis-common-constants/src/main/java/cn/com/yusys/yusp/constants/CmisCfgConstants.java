package cn.com.yusys.yusp.constants;

/**
 * 常量类:参数管理服务
 */
public final class CmisCfgConstants {

    /**
     * 01-页面
     **/
    public static final String PAGE = "01";

    /**
     * 02-子模板组
     **/
    public static final String MODEL_GROUP = "02";
    /**
     * no-不排序
     **/
    public static final String ORDER_BY_TYPE_NO = "no";

    /**
     * 01-条件字段
     **/
    public static final String PARAM_TYPE_COND_COLUMN = "01";
    /**
     * 02-显示字段
     **/
    public static final String PARAM_TYPE_SHOW_COLUMN = "02";
    /**
     * 03-分组字段
     **/
    public static final String PARAM_TYPE_GROUP_BY_COLUMN = "03";
    /**
     * 04-汇总字段
     **/
    public static final String PARAM_TYPE_GROUP_SHOW_COLUMN = "04";

    /**
     * 关于 CFG_FLEX_QRY_PARAM 表设计的问题
     * __ 为什么这样设计，因为张岩设计的表一开始把条件、显示字段组成了一张表去存储sql属性
     * 因此本来是多张表的属性字段，现在只能用__后缀去区分本属于各个表的属性列值得数据
     * 前台页面为了节省没有必要的大量if else判断，根据_转驼峰的方式，可以直接eval实现方法的调用，
     * 及在配置中就可以知道这个属性数据会加载什么样的模板，从而在页面上怎么展示，方便以后扩展，
     * 但如果这个功能可以拓展为插件的话:
     *  1.更好的表设计
     *  2.更为复杂SQL装配解析问题
     *  3.需要支持多数据版本
     *  4.解决后台多数据源问题
     *  5.面向非数据库使用人员语言表达问题
     *  6.抽象化
     */

    /**
     * 显示字段
     **/
    public static final String PARAM_KEY_SHOW_COLUMN = "__show_index_code";

    /**
     * 条件字段
     **/
    public static final String PARAM_KEY_COND_COLUMN = "__cond_index_code";

    /**
     * 条件字段 字段显示标题
     **/
    public static final String PARAM_KEY_COND_TITLE = "__cond_title";

    /**
     * 条件字段 字段比较值
     **/
    public static final String PARAM_KEY_COND_VALUE = "__cond_value";

    /**
     * 显示字段 字段显示排序方式
     **/
    public static final String PARAM_KEY_SHOW_ORDER_BY = "__show_order_by";

    /**
     * 显示字段 列表显示表头
     **/
    public static final String PARAM_KEY_SHOW_SHOW_TITLE = "__show_title";

    /**
     * 汇总显示字段 列表显示表头
     **/
    public static final String PARAM_KEY_GROUP_SHOW_TITLE = "__group_show_type";

    /**
     * 汇总显示字段 汇总显示数据
     **/
    public static final String PARAM_KEY_GROUP_SHOW_TYPE = "__group_show_type";

    /**
     * 汇总显示字段 汇总显示名称
     **/
    public static final String PARAM_KEY_GROUP_SHOW_NAME = "__group_show_name";

    /**
     * 分组字段
     **/
    public static final String PARAM_KEY_GROUP_BY_INDEX_CODE = "__group_by_index_code";

    // 为什么要使用中文 太离谱了
    /**
     * 字段 类型 数字
     **/
    public static final String ITEM_TYPE_DIGIT = "数字框";

    /**
     * 区分规则类型 010：授权
     */
    public static final String RULE_TYPE_010 = "010";
    /**
     * 区分规则类型 020：资本占用率
     */
    public static final String RULE_TYPE_020 = "020";

    /**
     * 规则状态  1：有效  0：无效
     */
    public static final String RULE_STATUS_1 = "1";

    public static final String RULE_STATUS_0 = "0";

    /**
     * 操作成功标志位 S 成功
     */
    public static final String OP_FLAG_S = "S";
    /**
     * 操作成功标志位 F 失败
     */
    public static final String OP_FLAG_F = "F";
    /**
     * 描述信息 操作成功
     */
    public static final String OP_MSG_S = "操作成功！";
    /**
     * 描述信息 操作失败
     */
    public static final String OP_MSG_F = "操作失败！";

    private CmisCfgConstants() {
        // do nothing
    }
}
