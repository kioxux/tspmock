package cn.com.yusys.yusp.constants;

/**
 * 常量类:资产保全服务
 */
public class CmisNpamConstants {

    /**
     * 成功标志
     **/
    public static final String FLAG_SUCCESS = "S";

    /**
     * 失败标志
     **/
    public static final String FLAG_FAIL = "F";

    /**
     * 记账状态标志位 01-待记账
     **/
    public static final String HX_STATUS_01 = "01";

    /**
     * 记账状态标志位 02-记账中
     **/
    public static final String HX_STATUS_02 = "02";

    /**
     * 记账状态标志位 03-记账成功
     **/
    public static final String HX_STATUS_03 = "03";

    /**
     * 记账状态标志位 04-记账失败
     **/
    public static final String HX_STATUS_04 = "04";

    /**
     * 记账状态标志位 05-记账部分失败
     **/
    public static final String HX_STATUS_05 = "05";
    /**
     * 登记状态 01-未登记
     **/
    public static final String HX_REGI_STATUS_01 = "01";
    /**
     * 登记状态 02-已登记
     **/
    public static final String HX_REGI_STATUS_02 = "02";
    /**
     * 登记状态 04-登记失败
     **/
    public static final String HX_REGI_STATUS_04 = "04";

    /**
     * 操作标识
     * 1--录入
     *
     **/
    public static final String HX_OPT_STATUS_1 = "1";
    /**
     * 操作标识
     * 2--复核
     **/
    public static final String HX_OPT_STATUS_2 = "2";
    /**
     * 操作标识
     * 3--直通
     */
    public static final String HX_OPT_STATUS_3 = "3";
    /**
     * 归还方式 16 其他
     */
    public static final String HX_DAIKGHFS_16 = "16";

    /**
     * 起始笔数 0
     */
    public static final int HX_QISHIBIS_0 = 0;
    /**
     * 转让操作方式
     */
    public static final String HX_CAOZFSHI_A = "A";

    /**
     * 核销状态01-待核销
     */
    public static final String WRITEOFF_STATUS_01= "01";

    /**
     * 核销状态02-核销中
     */
    public static final String WRITEOFF_STATUS_02= "02";
    /**
     * 核销状态03-核销成功
     */
    public static final String WRITEOFF_STATUS_03= "03";
    /**
     * 核销状态04-核销失败
     */
    public static final String WRITEOFF_STATUS_04= "04";

    /**
     * 任务编号
     */
    public static final String ZCBQ_BCM_SERNO = "TRADE_ID_SEQ";

    /**
     * 登记编号
     */
    public static final String ZCBQ_REG_SERNO = "TRADE_ID_SEQ";

    /**
     * 分配状态01-已分配
     **/
    public static final String TASK_STATUS_01 = "01";

    /**
     * 分配状态02-未分配
     **/
    public static final String TASK_STATUS_02 = "02";

    /**
     * 分配状态01-已完成
     **/
    public static final String BCM_STATUS_01 = "01";

    /**
     * 分配状态02-未完成
     **/
    public static final String BCM_STATUS_02 = "02";
    /**
     * 资产处置方式 01-出租
     **/
    public static final String DISP_MODE_01 = "01";
    /**
     * 资产处置方式 02-出售
     **/
    public static final String DISP_MODE_02 = "02";
    /**
     * 资产处置方式 03-转固
     **/
    public static final String DISP_MODE_03 = "03";
    /**
     * 资产处置方式04-毁损灭失
     **/
    public static final String DISP_MODE_04 = "04";

}
