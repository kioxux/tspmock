package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-担保管理的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizDbEnum {
    /**
     * 是否主担保
     */
    GRT_FLG_1("1", "主担保"),
    GRT_FLG_2("2", "副担保"),
    // 符号
    SYMBOL_QUESTION("?", "问号"),
    SYMBOL_AND("&", "与"),
    SYMBOL_LEFT_PARENTHESIS("[", "左括号"),
    SYMBOL_LEFT_BRACE("{", "左大括号"),
    SYMBOL_RIGHT_PARENTHESIS("]", "右括号"),
    SYMBOL_RIGHT_BRACE("}", "右大括号"),
    SYMBOL_QUOTES("\"", "引号"),
    SYMBOL_COMMA(",", "逗号"),
    SYMBOL_COLON(":", "冒号"),
    SYMBOL_EQUAL("=", "等于号"),
    SYMBOL_OR("|", "或"),

    /**
     * 押品系统相关枚举 开始
     **/
    // 押品系统跳转调用页面
    CALLMETHOD_TGUAREXISTQRY("tGuarExistQry", "押品新增界面"),
    CALLMETHOD_TGUARDETAILINFO("tGuarDetailInfo", "查看押品详细信息界面"),
    CALLMETHOD_TGUARUPDATE("tGuarUpdate", "押品信息修改界面"),
    CALLMETHOD_TGUARFIRSTEVAL("tGuarFirstEval", "客户经理进行押品价值的初估"),
    CALLMETHOD_TGUARREEVALAPPLY("tGuarReevalApply", "客户经理进行押品价值的重估"),
    CALLMETHOD_TCOLLWARNING("tCollWarning", "待处理任务界面"),
    CALLMETHOD_TWELCOME("tWelCome", "押品系统首页"),

    /**
     * 请求的系统标识（SystemNo）:
     * 01-信贷系统</br>
     * 02-小贷系统</br>
     */
    SYSTEMNO_01("01", "信贷系统"),
    SYSTEMNO_02("02", "小贷系统"),
    /**
     * 担保方式标识（GuarWay）:
     * BZ-保证</br>
     * DY-抵押</br>
     * ZY-质押</br>
     */
    GUARWAY_BZ("BZ", "保证"),
    GUARWAY_DY("DY", "抵押"),
    GUARWAY_ZY("ZY", "质押"),

    /**
     * 押品信息同步方式标识（iffadd）:
     * 1-增加</br>
     * 2-修改</br>
     * 3-删除</br>
     * 4-4重估</br>
     * 5-移交</br>
     */
    IFFADD_ADD("1", "增加"),
    IFFADD_UPDATE("2", "修改"),
    IFFADD_DELETE("3", "删除"),
    IFFADD_EVAL("4", "4重估"),
    IFFADD_TRAN("5", "移交"),

    /**
     * 客户类型（iffadd）:
     * 1-个人客户</br>
     * 2-对公客户</br>
     * 3-删除</br>
     * 4-4重估</br>
     * 5-移交</br>
     */
    CUS_TYPE_01("1", "个人客户"),
    CUS_TYPE_02("2", "对公客户"),
    CUS_TYP_290("290", "担保公司"),
    CUS_TYP_02("02", "专业担保公司"),
    CUS_TYPE_10001("10001", "担保公司"),
    CUS_TYPE_10002("10002", "个人"),
    CUS_TYPE_10003("10003", "企业"),
    /**
     * 主担保或副担保（GrtFlg）:
     * 1-主担保</br>
     * 2-副担保</br>
     */
    GRTFLG_1("1", "主担保"),
    GRTFLG_2("2", "副担保"),
    /**
     * 物业情况（Property）:
     * 010-有物业</br>
     * 020-无物业</br>
     */
    Property_010("010", "有物业"),
    Property_020("020", "无物业"),
    /**
     * 评估方式（EvalType）:
     * 01-外部评估</br>
     * 02-内部评估</br>
     * 03-云估价
     * 若为小贷系统云估价，默认为“03-云估价”
     */
    EVALTYPE_01("01", "外部评估"),
    EVALTYPE_02("02", "内部评估"),
    EVALTYPE_03("03", "云估价"),
    /**
     * 数据修改标识（DataFlag）:
     * 01-正常修改</br>
     * 02-期房换证入库修改</br>
     * 03-需走数据修改流程的修改</br>
     * 04-特殊业务及期房换证修改</br>
     */
    DATAFLAG_01("01", "正常修改"),
    DATAFLAG_02("02", "期房换证入库修改"),
    DATAFLAG_03("03", "需走数据修改流程的修改"),
    DATAFLAG_04("04", "特殊业务及期房换证修改"),
    /**
     * 页面跳转标识（PageMark）:
     * 03-押品处置信息</br>
     */
    PAGEMARK_01("01", "押品基本信息"),
    PAGEMARK_03("03", "押品处置信息"),
    /**
     * 请求的系统标识（SystemFlag）:
     * 01-对公系统</br>
     * 02-个贷系统</br>
     */
    SYSTEMFLAG_01("01", "对公系统"),
    SYSTEMFLAG_02("02", "个贷系统"),

    /**
     * 押品注销类型（RETYPE）:
     * 91015-注销类型</br>
     * 91016-登记类型</br>
     */
    RETYPE_01("91015", "注销类型"),
    RETYPE_02("91016", "登记类型"),

    /**
     * 权证类型（CERTCODE）:
     * 10008-正常出库</br>
     * 10013-部分出库</br>
     * 10014-提前出库</br>
     */
    CERTCODE_01("10008", "正常出库"),
    CERTCODE_02("10013", "部分出库"),
    CERTCODE_03("10014", "提前出库"),

    /**
     * 是否共有（COMMON_OWNER）:
     * 01-共有</br>
     * 02-不是共有</br>
     */
    COMMON_OWNER_01("01", "共有"),
    COMMON_OWNER_02("02", "不是共有"),

    /**
     * 共有类型（COMMON_OWNER）:
     * 01-共同共有</br>
     * 02-按份共有</br>
     */
    COMMON_TYPE_01("01", "共同共有"),
    COMMON_TYPE_02("02", "按份共有"),

    /**
     * 共有比例（COMMON_OWNER）:
     * 01-50%</br>
     * 02-100%</br>
     */
    COMMON_BIL_01("50", "50%"),
    COMMON_BIL_02("100", "100%"),

    /**
     * 押品系统相关枚举 结束
     **/

    YESNO_YES("Y", "是否"),//是否-是
    YESNO_NO("N", "是否"),//是否-否
    EXIST_OPMSG("EXIST", "押品存在按揭"),//押品存在按揭
    NOEXIST_OPMSG("NOEXIST", "押品不存在按揭"),//押品不存在按揭
    EXIST_BIZ_REL("EXIST", "押品存在业务关联关系"),//押品存在按揭
    FALG_SUCCESS("S", "成功"),//是否-是
    FLAG_FAILD("F", "失败"),//是否-否
    NO_RECORD("N", "未查询到记录"),//是否-否
    NOEXIT_GUAR_CONT_NO("NOEXIST", "担保合同编号不正确，请确认！"),
    EXIT_GUAR_CONT_NO("EXIST", "担保合同编号正确"),
    GUAR_STATE_IN("IN","该押品已入库，不能重复入库"),
    GUAR_IN_FAIL("INFAIL","核心入库失败"),
    NO_GUAR_BASE_INFO("INFAIL","核心入库失败"),
    CUS_BASE_NO("N","信贷无该客户,请根据ecif客户数据至信贷新增"),
    PARAM_NULL ("N","数据不全"),

    /**
     * 返回结果:</br>
     * S-成功</br>
     * F-失败</br>
     */
    RETURN_SUCCESS("S", "成功"),
    RETURN_FAIL("F", "失败"),

    ;

    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizDbEnum enumData : EnumSet.allOf(DscmsBizDbEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizDbEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizDbEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
