package cn.com.yusys.yusp.constants;

/**
 * @author xianglei
 * @className SeqConstant
 * @description 序列号常量
 * @since 2020/12/21 : 10:35
 */
public class SeqConstant {
    /**
     * 全局序列号
     */
    public static final String YPSEQ = "YPSEQ";
    /**
     * iqp_serno序列号
     */
    public static final String IQP_SERNO = "IQP_SERNO";
    /**
     * cont_no序列号
     */
    public static final String CONT_NO = "CONT_NO_NEW";

    /*合同账号变更申请流水号*/
    public static final String IQP_ACCT_SERNO = "IQP_ACCT_SERNO";

    /**
     * 担保合同序列号
     */
    public static final String GRT_CONT_NO = "GRT_GUAR_SERNO";
    /**
     * 担保合同流水序列号
     */
    public static final String DB_SERNO = "DB_SERNO";
    /**
     * pvp_serno 放款序列号
     */
    public static final String PVP_SERNO = "PVP_SERNO";
    /**
     * bill_no 借据表   借据编号 序列名称
     **/
    public static final String BILL_NO = "BILL_NO_SEQ";
    /**
     * tran_serno 授权信息表 交易流水号序列名称
     **/
    public static final String PA_TRAN_SERNO = "PA_TRAN_SERNO_SEQ";
    /**
     * tran_serno 授权信息表 交易流水号序列名称
     **/
    public static final String PTPI_TRAN_SERNO = "PAPI_TRAN_SERNO_SEQ";
    /**
     * lmt_ctr_no 额度协议编号
     **/
    public static final String LMT_CTR_NO = "LMT_CTR_NO";
    /**
     * LMT_COOP_CTR_NO 合作方协议编号
     **/
    public static final String LMT_COOP_CTR_NO = "LMT_COOP_CTR_NO";
    /**
     * LMT_FIN_CTR_NO 担保公司协议编号
     **/
    public static final String LMT_FIN_CTR_NO = "LMT_FIN_CTR_NO";
    /**
     * 借款台账序列号
     **/
    public static final String BILL_NO_SEQ = "BILL_NO_SEQ";
    /**
     * lmt_indiv_app 个人额度申请序列号
     **/
    public static final String LMT_INDIV_APP_SEQ = "LMT_SERNO";
    /**
     * lmt_ctr 个人额度协议  协议编号序列
     **/
    public static final String LMT_CTR_SEQ = "LMT_CTR_NO_SEQ";
    /**
     * lmt_sub 个人额度申请-额度分项  申请流水号模板
     **/
    public static final String LMT_SUB_SEQ = "LMT_SUB_SEQ";
    /**
     * YP_SERNO  核心担保编号
     **/
    public static final String YP_SERNO = "YP_SERNO";
    /**
     * GUAR_BASE_SEQ  押品流水号
     **/
    public static final String GUAR_BASE_SEQ = "GUAR_BASE_SEQ";
    /**
     * SERNO  权证出入库申请流水号
     **/
    public static final String SERNO = "SERNO";
    /**
     * IQP  展期申请申请流水号
     **/
    public static final String IQP = "IQP";
    /**
     * FILE_TASK_NO  档案编号
     **/
    public static final String FILE_TASK_NO = "FILE_TASK_NO";

    /**
     * 全局流水号业务类型 10-PC端
     **/
    public static final String YPSEQ_BIZ_10 = "10";
    /**
     * 全局流水号业务类型 11-PAD端
     **/
    public static final String YPSEQ_BIZ_11 = "11";
    /**
     * 全局流水号业务类型 12-直销银行
     **/
    public static final String YPSEQ_BIZ_12 = "12";
    /**
     * 全局流水号业务类型 13-微信
     **/
    public static final String YPSEQ_BIZ_13 = "13";
    /**
     * 全局流水号业务类型 14-手机银行
     **/
    public static final String YPSEQ_BIZ_14 = "14";
    /**
     * 全局流水号业务类型 15-网银
     **/
    public static final String YPSEQ_BIZ_15 = "15";
    /**
     * 全局流水号业务类型 20-第三方平台
     **/
    public static final String YPSEQ_BIZ_20 = "20";
    /**
     * 全局流水号业务类型 21-开发商
     **/
    public static final String YPSEQ_BIZ_21 = "21";
    /**
     * 全局流水号业务类型 22-房屋中介机构
     **/
    public static final String YPSEQ_BIZ_22 = "22";
    /**
     * 全局流水号业务类型 23-法拍网
     **/
    public static final String YPSEQ_BIZ_23 = "23";

    /**
     * PK_TP_ALLOT_RECORD 项目池分配记录表主键对应的序列编号
     **/
    public static final String PK_TP_ALLOT_RECORD = "PK_TP_ALLOT_RECORD";

    /**
     * 序列模板编号，用于生成序列号作为项目池与分配规则关联关系表的主键
     */
    public static final String TP_RULE_PK = "TP_RULE_PK";
    /* 征信查询流水号*/
    public static final String CRQL_SERNO = "CRQL_SERNO";
    /* 小微合同编号*/
    public static final String CONT_NO_XW = "CONT_NO_XW";
    /* 征信授权书编号 */
    public static final String AUTHBOOK_NO_SEQ = "AUTHBOOK_NO_SEQ";
    /* 征信融资业务流水号 */
    public static final String CRLF_SERNO = "CRLF_SERNO";

    /** 档案管理流水号 **/
    public static final String DOC_SERNO = "DOC_SERNO";
    /** 档案归档流水号 DA_GD_SEQ **/
    public static final String DA_GD_SEQ = "DA_GD_SEQ";
    /** 档案征信归档流水号 DA_ZX_SEQ **/
    public static final String DA_ZX_SEQ = "DA_ZX_SEQ";
    /** 档案调阅流水号 DA_DY_SEQ **/
    public static final String DA_DY_SEQ = "DA_DY_SEQ";
    /** 档案延期流水号 DA_YQ_SEQ **/
    public static final String DA_YQ_SEQ = "DA_YQ_SEQ";
    /** 档案销毁流水号 DA_XH_SEQ **/
    public static final String DA_XH_SEQ = "DA_XH_SEQ";
    /** 影像补扫流水号 DA_BS_SEQ **/
    public static final String DA_BS_SEQ = "DA_BS_SEQ";
    /** 档案清单流水号 DA_QD_SEQ **/
    public static final String DA_QD_SEQ = "DA_QD_SEQ";

    /** 影像流水号 **/
    public static final String YX_SERNO_SEQ = "YX_SERNO_SEQ";
    /** 购销背景流水号 **/
    public static final String GXBJ_SERNO_SEQ = "GXBJ_SERNO_SEQ";

    /*****************************张家港农商行标准序列规则start add by 王玉坤*********************************/
    /* 张家港最高额授信协议序列 */
    public static final String ZRCXY_SEQ = "ZRCXY";
    /* 寿光最高额授信协议序列 */
    public static final String SGCXY_SEQ = "SGCXY";
    /* 东海最高额授信协议序列 */
    public static final String DHCXY_SEQ = "DHCXY";
    /* 张家港普通贷款序列 */
    public static final String ZRCDK_SEQ = "ZRCDK";
    /* 寿光普通贷款序列 */
    public static final String SGCDK_SEQ = "SGCDK";
    /* 东海普通贷款序列 */
    public static final String DHCDK_SEQ = "DHCDK";
    /* 张家港委托贷款序列 */
    public static final String ZRCWT_SEQ = "ZRCWT";
    /* 寿光委托贷款序列 */
    public static final String SGCWT_SEQ = "SGCWT";
    /* 东海委托贷款序列 */
    public static final String DHCWT_SEQ = "DHCWT";
    /* 张家港银票合同序列 */
    public static final String ZRCYC_SEQ = "ZRCYC";
    /* 寿光银票合同序列 */
    public static final String SGCYC_SEQ = "SGCYC";
    /* 东海银票合同序列 */
    public static final String DHCYC_SEQ = "DHCYC";
    /* 张家港保函协议序列 */
    public static final String ZRCBH_SEQ = "ZRCBH";
    /* 寿光保函协议序列 */
    public static final String SGCBH_SEQ = "SGCBH";
    /* 东海保函协议序列 */
    public static final String DHCBH_SEQ = "DHCBH";
    /* 张家港贸易融资序列 */
    public static final String ZRCMR_SEQ = "ZRCMR";
    /* 寿光贸易融资序列 */
    public static final String SGCMR_SEQ = "SGCMR";
    /* 东海贸易融资序列 */
    public static final String DHCMR_SEQ = "DHCMR";
    /* 张家港贴现协议序列 */
    public static final String ZRCTX_SEQ = "ZRCTX";
    /* 寿光贴现协议序列 */
    public static final String SGCTX_SEQ = "SGCTX";
    /* 东海贴现协议序列 */
    public static final String DHCTX_SEQ = "DHCTX";
    /* 张家港资产池协议序列 */
    public static final String ZRCZC_SEQ = "ZRCZC";
    /* 寿光资产池协议序列 */
    public static final String SGCZC_SEQ = "SGCZC";
    /* 东海资产池协议序列 */
    public static final String DHCZC_SEQ = "DHCZC";
    /* 张家港开证序列 */
    public static final String ZRCXYZ_SEQ = "ZRCXYZ";
    /* 寿光开证序列 */
    public static final String SGCXYZ_SEQ = "SGCXYZ";
    /* 东海开证序列 */
    public static final String DHCXYZ_SEQ = "DHCXYZ";
    /* 张家港福费廷序列 */
    public static final String ZRCFFT_SEQ = "ZRCFFT";
    /* 寿光福费廷序列 */
    public static final String SGCFFT_SEQ = "SGCFFT";
    /* 东海福费廷序列 */
    public static final String DHCFFT_SEQ = "DHCFFT";
    /*****************************张家港农商行标准序列规则end add by 王玉坤*********************************/


    /*****************************张家港农商行标准序列规则start add by 张静雯*********************************/
    /* 主键 统一生成流水号，不使用UUID */
    public static final String PK_ID = "PK_VALUE"; //PK{yyyymmddhhss}SEQ

    /* 同业机构准入申请编号序列 */
    public static final String INTBANK_ADMIT_SEQ = "IASQ"; //IASQ{yyyymmddhhss}SEQ
    /* 同业机构准入批复编号序列 */
    public static final String INTBANK_ADMIT_REPLY_SEQ = "IAPF"; //IAPF{yyyymmddhhss}SEQ
    /* 同业机构准入台账编号序列 */
    public static final String INTBANK_ADMIT_ACC_SEQ = "IATZ";//IATZ{yyyymmddhhss}SEQ

    /* 同业授信申请编号序列 */
    public static final String INTBANK_LMT_SEQ = "IBSQ"; //IBSQ{yyyymmddhhss}SEQ
    /* 同业授信分项申请编号序列 */
    public static final String INTBANK_LMT_SUB_SEQ = "IBSQS"; //IBSQS{yyyymmddhhss}SEQ
    /* 同业授信批复编号序列 */
    public static final String INTBANK_LMT_REPLY_SEQ = "IBPF"; //IBPF{yyyymmddhhss}SEQ
    /* 同业授信批复分项编号序列 */
    public static final String INTBANK_LMT_REPLY_SUB_SEQ = "IBPFS"; //IBPFS{yyyymmddhhss}SEQ
    /* 同业授信台账编号序列 */
    public static final String INTBANK_LMT_ACC_SEQ = "IBSX"; //IBSX{yyyymmddhhss}SEQ
    /* 同业授信台账分项编号序列 */
    public static final String INTBANK_LMT_ACC_SUB_SEQ = "IBTZ"; //IBTZ{yyyymmddhhss}SEQ

    /* 单笔投资授信申请编号序列 */
    public static final String INVEST_LMT_SEQ = "ZJSQ"; //ZJSQ{yyyymmddhhss}SEQ
    /* 单笔投资授信批复编号序列 */
    public static final String INVEST_LMT_REPLY_SEQ = "ZJPF"; //ZJPF{yyyymmddhhss}SEQ
    /* 单笔投资授信台账编号序列 */
    public static final String INVEST_LMT_ACC_SEQ = "ZJSX"; //ZJSX{yyyymmddhhss}SEQ

    /* 单笔投资授信项目编号序列 */
    public static final String INVEST_PRO_SEQ = "CCD_SERNO"; //PRO{yyyymmddhhss}SEQ
    /* 信用卡序列 */
    public static final String CREDIT_CARD_SEQ = "PRO"; //PRO{yyyymmddhhss}SEQ
    /* 调额序列 */
    public static final String CREDIT_ADJUST_SEQ = "CCA_SERNO"; //PRO{yyyymmddhhss}SEQ
    /* 大额分期序列 */
    public static final String CREDIT_LARGE_SEQ = " CCL_SERNO"; //PRO{yyyymmddhhss}SEQ
    /* 大额分期合同序列 */
    public static final String CREDIT_CONT_SEQ = " CLC_SERNO"; //PRO{yyyymmddhhss}SEQ

    /***************************张家港农商行标准序列规则end add by 张静雯*********************************/

    /**********************合同批次号流水号信息 start*************************/
    public static final String CONT_PC_SEQ = "CONT_PC_SEQ";
    /**********************合同批次号流水号信息 end*************************/
}
