package cn.com.yusys.yusp.constants;

/**
 * 常量类:流程服务
 **/
public class CmisFlowConstants {

    /**
     * 默认的分隔符
     **/
    public static final String DEF_SPILIT_COMMMA = ",";

    /*** 提交部门类型 1 合规部 **/
    public static final String SUBMIT_DEPT_TYPE_1 = "1";
    /*** 提交部门类型 2 特资部 **/
    public static final String SUBMIT_DEPT_TYPE_2 = "2";
    /*** 提交部门类型 3 合规部、特资部 **/
    public static final String SUBMIT_DEPT_TYPE_3 = "3";
    /*** 提交部门类型 4 特资部、合规部 **/
    public static final String SUBMIT_DEPT_TYPE_4 = "4";

    /*** 审批模式 01 单签模式 **/
    public static final String STD_APPR_MODE_01 = "01";
    /*** 审批模式 02 双签模式 **/
    public static final String STD_APPR_MODE_02 = "02";
    /*** 审批模式 03 三签模式 **/
    public static final String STD_APPR_MODE_03 = "03";
    /*** 审批模式 04 三人会商模式 **/
    public static final String STD_APPR_MODE_04 = "04";
    /*** 审批模式 05 贷审会模式 **/
    public static final String STD_APPR_MODE_05 = "05";
    /*** 审批模式 06 投委会模式 **/
    public static final String STD_APPR_MODE_06 = "06";
    /*** 审批模式 07 双签模式一 **/
    public static final String STD_APPR_MODE_07 = "07";
    /*** 审批模式 08 双签模式二 **/
    public static final String STD_APPR_MODE_08 = "08";
    /*** 审批模式 51 信贷管理部负责人权限 **/
    public static final String STD_APPR_MODE_51 = "51";
    /*** 审批模式 52 信贷管理部分管行长权限 **/
    public static final String STD_APPR_MODE_52 = "52";
    /*** 审批模式 53 投委会权限 **/
    public static final String STD_APPR_MODE_53 = "53";

    /*** 关联交易类型 01 一般关联交易 **/
    public static final String STD_GL_TYPE_01 = "01";
    /*** 关联交易类型 02 重大关联交易 **/
    public static final String STD_GL_TYPE_02 = "02";
    /*** 关联交易类型 03 特大关联交易 **/
    public static final String STD_GL_TYPE_03 = "03";

    /**
     * 展期放款流程
     **/
    public static final String BIZ_TYPE_PVP_EXT_LOAN_APP = "PVP_EXT_LOAN_APP";
    /**
     * 合作方额度申请
     **/
    public static final String BIZ_TYPE_LMT_COOP_APP = "WF_LMT_COOP_APP";
    /**
     * 合作方额度-共享范围调整申请
     **/
    public static final String BIZ_TYPE_LMT_COOP_SHARED_APP = "WF_LMT_COOP_SHARED_APP";
    /**
     * 担保公司授信申请
     **/
    public static final String BIZ_TYPE_LMT_FIN_GUAR_APP = "LMT_FIN_GUAR_APP";

    /**
     * 担保公司-共享范围调整申请
     **/
    public static final String BIZ_TYPE_LMT_FIN_SHARED_APP = "WF_LMT_FIN_SHARED_APP";


    /**
     * 放款申请退汇申请业务流程
     **/
    public static final String PVP_RRU_UPD_APP = "PVP_RRU_UPD_APP";

    /**
     * 个人额度申请
     **/
    public static final String BIZ_TYPE_LMT_INDIV_APP = "LMT_INDIV_APP";

    /**
     * 额度冻结/解冻申请
     **/
    public static final String BIZ_TYPE_LMT_FR_UFR_APP = "LMT_FR_UFR_APP";


    /**
     * 担保公司授信代偿申请
     **/
    public static final String BIZ_TYPE_LMT_FIN_SP_APP = "LMT_FIN_SP_APP_FLOW";
    /**
     * 担保公司授信代偿申请
     **/
    public static final String BIZ_TYPE_WF_LMT_SURVEY_REPORT_MAIN = "WF_LMT_SURVEY_REPORT_MAIN";


    /**
     * 合作方额度调整申请
     **/
    public static final String ED003 = "ED003";

    /**
     * 单一额度调整申请
     **/
    public static final String ED001 = "ED001";
    /**
     * 合作方额度冻结解冻
     **/
    public static final String HZXM05 = "HZXM05";
    public static final String HZXM06 = "HZXM06";

    /**
     * 额度管控白名单申请
     **/
    public static final String EDGL01 = "EDGL01";

    /**
     * 零售合同申请（空白合同）
     **/
    public static final String WF_RETAIL_BLANK_CONT_APP = "WF_RETAIL_BLANK_CONT_APP";

    /**
     * 零售业务授信审批流程
     **/
    public static final String LSYW01 = "LSYW01";

    /**
     * 零售空白合同签订审核流程
     **/
    public static final String LSYW02 = "LSYW02";

    /**
     * 零售放款审核流程-空白合同模式
     **/
    public static final String LSYW03 = "LSYW03";

    /**
     * 零售放款审核流程-生成打印模式
     **/
    public static final String LSYW04 = "LSYW04";

    /**
     * 零售线上提款启用审核流程
     **/
    public static final String LSYW05 = "LSYW05";

    /**
     * 零售生成打印合同签订审核流程
     **/
    public static final String LSYW06 = "LSYW06";
    /**
     * 零售批复变更
     **/
    public static final String LSYW07 = "LSYW07";
    /**
     * 同业机构准入申请
     **/
    public static final String TYSX01 = "TYSX01";
    /**
     * 同业机构准入申请
     **/
    public static final String TYSX02 = "TYSX02";
    /**
     * 资金业务授信批复变更审批流程
     **/
    public static final String TYSX03 = "TYSX03";
    /**
     * 投行业务授信审批流程（分支机构发起）
     **/
    public static final String TYSX04 = "TYSX04";
    /**
     * 资金业务授信申报审批流程（总行发起-债券池)
     **/
    public static final String TYSX06 = "TYSX06";



    /**
     * 对公授信申报申请流程
     **/
    public static final String FLOW_ID_DGSX01 = "DGSX01";
    /**
     * 集团额度调剂审批流程
     **/
    public static final String FLOW_ID_DGSX02 = "DGSX02";
    /**
     * 对公额度冻结解冻终止审批流程
     **/
    public static final String FLOW_ID_DGSX03 = "DGSX03";
    /**
     * 对公授信批复变更审批流程
     **/
    public static final String FLOW_ID_DGSX04 = "DGSX04";
    /**
     * 集团成员授信申报审核子流程
     **/
    public static final String FLOW_ID_DGSX05 = "DGSX05";
    /**
     * 对公内评低准入例外审批流程
     **/
    public static final String FLOW_ID_DGSX06 = "DGSX06";

    /**
     * 单一客户授信新增
     **/
    public static final String FLOW_TYPE_TYPE_SX001 = "SX001";
    /**
     * 单一客户授信续作
     **/
    public static final String FLOW_TYPE_TYPE_SX002 = "SX002";
    /**
     * 单一客户授信复审
     **/
    public static final String FLOW_TYPE_TYPE_SX003 = "SX003";
    /**
     * 单一客户授信复议
     **/
    public static final String FLOW_TYPE_TYPE_SX004 = "SX004";
    /**
     * 单一客户授信再议
     **/
    public static final String FLOW_TYPE_TYPE_SX005 = "SX005";
    /**
     * 单一客户授信变更
     **/
    public static final String FLOW_TYPE_TYPE_SX006 = "SX006";
    /**
     * 单一客户授信预授信细化
     **/
    public static final String FLOW_TYPE_TYPE_SX007 = "SX007";
    /**
     * 集团客户授信新增
     **/
    public static final String FLOW_TYPE_TYPE_SX008 = "SX008";
    /**
     * 集团客户授信续作
     **/
    public static final String FLOW_TYPE_TYPE_SX009 = "SX009";
    /**
     * 集团客户授信复审
     **/
    public static final String FLOW_TYPE_TYPE_SX010 = "SX010";
    /**
     * 集团客户授信复议
     **/
    public static final String FLOW_TYPE_TYPE_SX011 = "SX011";
    /**
     * 集团客户授信再议
     **/
    public static final String FLOW_TYPE_TYPE_SX012 = "SX012";
    /**
     * 集团客户授信变更
     **/
    public static final String FLOW_TYPE_TYPE_SX013 = "SX013";
    /**
     * 集团客户授信预授信细化
     **/
    public static final String FLOW_TYPE_TYPE_SX014 = "SX014";
    /**
     * 集团成员额度调剂
     **/
    public static final String FLOW_TYPE_TYPE_SX015 = "SX015";
    /**
     * 单一客户额度冻结
     **/
    public static final String FLOW_TYPE_TYPE_SX016 = "SX016";
    /**
     * 单一客户额度解冻
     **/
    public static final String FLOW_TYPE_TYPE_SX017 = "SX017";
    /**
     * 单一客户额度终止
     **/
    public static final String FLOW_TYPE_TYPE_SX018 = "SX018";
    /**
     * 集团客户额度冻结
     **/
    public static final String FLOW_TYPE_TYPE_SX019 = "SX019";
    /**
     * 集团客户额度解冻
     **/
    public static final String FLOW_TYPE_TYPE_SX020 = "SX020";
    /**
     * 集团客户额度终止
     **/
    public static final String FLOW_TYPE_TYPE_SX021 = "SX021";
    /**
     * 单一客户授信批复变更
     **/
    public static final String FLOW_TYPE_TYPE_SX022 = "SX022";
    /**
     * 集团客户授信批复变更
     **/
    public static final String FLOW_TYPE_TYPE_SX023 = "SX023";
    /**
     * 集团子成员授信新增
     **/
    public static final String FLOW_TYPE_TYPE_SX024 = "SX024";
    /**
     * 集团子成员授信续作
     **/
    public static final String FLOW_TYPE_TYPE_SX025 = "SX025";
    /**
     * 集团子成员授信复审
     **/
    public static final String FLOW_TYPE_TYPE_SX026 = "SX026";
    /**
     * 集团子成员授信复议
     **/
    public static final String FLOW_TYPE_TYPE_SX027 = "SX027";
    /**
     * 集团子成员授信再议
     **/
    public static final String FLOW_TYPE_TYPE_SX028 = "SX028";
    /**
     * 集团子成员授信变更
     **/
    public static final String FLOW_TYPE_TYPE_SX029 = "SX029";
    /**
     * 集团子成员预授信细化
     **/
    public static final String FLOW_TYPE_TYPE_SX030 = "SX030";
    /**
     * 集团子成员额度调剂
     **/
    public static final String FLOW_TYPE_TYPE_SX031 = "SX031";

    /**
     * 内评低准入例外审批
     **/
    public static final String FLOW_TYPE_TYPE_SX032 = "SX032";

    /**
     * SX033集团成员额度调剂（全流程）
     **/
    public static final String FLOW_TYPE_TYPE_SX033 = "SX033";

    /**
     * BG004利率变更（小微）BGYW01
     **/
    public static final String FLOW_ID_BGYW01 = "BGYW01";
    /**
     * BG007展期申请（小微）BGYW02
     **/
    public static final String FLOW_ID_BGYW02 = "BGYW02";
    /**
     * BG011担保变更（小微）BGYW03
     **/
    public static final String FLOW_ID_BGYW03 = "BGYW03";
    /**
     * BG012还款账户变更（对公）BGYW04
     **/
    public static final String FLOW_ID_BGYW04 = "BGYW04";
    /**
     * BG013还款账户变更（零售）BGYW05
     **/
    public static final String FLOW_ID_BGYW05 = "BGYW05";
    /**
     * BG014还款账户变更（小微）BGYW06
     **/
    public static final String FLOW_ID_BGYW06 = "BGYW06";
    /**
     * SGH09还款账户变更（寿光村镇）SGCZ13
     **/
    public static final String FLOW_ID_SGCZ13 = "SGCZ13";
    /**
     * DHCZ13还款账户变更（东海村镇）
     **/
    public static final String FLOW_ID_DHCZ13 = "DHCZ13";
    /**
     * BG017还款计划变更（小微）BGYW07
     **/
    public static final String FLOW_ID_BGYW07 = "BGYW07";
    /**
     * SGH10 还款计划变更（寿光村镇）SGCZ14
     **/
    public static final String FLOW_ID_SGCZ14 = "SGCZ14";
    /**
     * DHCZ14 还款计划变更（东海村镇）
     **/
    public static final String FLOW_ID_DHCZ14 = "DHCZ14";
    /**
     * BG018延期还款（对公）BGYW08
     **/
    public static final String FLOW_ID_BGYW08 = "BGYW08";
    /**
     * BG020延期还款（小微）BGYW09
     **/
    public static final String FLOW_ID_BGYW09 = "BGYW09";
    /**
     * BG021合作方保证金代偿BGYW10
     **/
    public static final String FLOW_ID_BGYW10 = "BGYW10";
    /**
     * BG022主动还款（对公）BGYW11
     **/
    public static final String FLOW_ID_BGYW11 = "BGYW11";
    /**
     * BG023主动还款（零售）BGYW12
     **/
    public static final String FLOW_ID_BGYW12 = "BGYW12";
    /**
     * BG024主动还款（小微）BGYW13
     **/
    public static final String FLOW_ID_BGYW13 = "BGYW13";
    /**
     * SGCZ15 主动还款（寿光村镇）
     **/
    public static final String FLOW_ID_SGCZ15 = "SGCZ15";
    /**
     * DHCZ15 主动还款（东海村镇）
     **/
    public static final String FLOW_ID_DHCZ15 = "DHCZ15";
    /**
     * BG025受托支付账号变更BGYW14
     **/
    public static final String FLOW_ID_BGYW14 = "BGYW14";
    /**
     * BG041/BG042停息申请审批流程BGYW15
     **/
    public static final String FLOW_ID_BGYW15 = "BGYW15";

    /**
     * SGH17 停息申请审批流程-寿光 SGCZ40
     **/
    public static final String FLOW_ID_SGCZ40 = "SGCZ40";

    /**
     * DHH17 停息申请审批流程-东海 DHCZ40
     **/
    public static final String FLOW_ID_DHCZ40 = "DHCZ40";
    /**
     * BG043/BG044取消停息申请审批流程BGYW16
     **/
    public static final String FLOW_ID_BGYW16 = "BGYW16";

    /**
     * SGH18 取消停息申请审批流程 -寿光
     **/
    public static final String FLOW_ID_SGCZ41 = "SGCZ41";

    /**
     * DHH18 取消停息申请审批流程 -东海
     **/
    public static final String FLOW_ID_DHCZ41 = "DHCZ41";

    /**
     * BG001利率变更（对公人民币）
     **/
    public static final String FLOW_TYPE_TYPE_BG001 = "BG001";
    /**
     * BG002利率变更（对公外币）
     **/
    public static final String FLOW_TYPE_TYPE_BG002 = "BG002";
    /**
     * BG003利率变更（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG003 = "BG003";
    /**
     * BG004利率变更（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG004 = "BG004";
    /**
     * SGF01 利率定价流程-经营性及消费非按揭 村镇(寿光)
     **/
    public static final String FLOW_TYPE_TYPE_SGF01 = "SGF01";

    /**
     * SGF04 人民币利率定价-展期
     **/
    public static final String FLOW_TYPE_TYPE_SGF04 = "SGF04";
    /**
     * SGF01 利率定价流程-经营性及消费非按揭 村镇(东海)
     **/
    public static final String FLOW_TYPE_TYPE_DHF01 = "DHF01";

    /**
     * DHF04 人民币利率定价-展期 村镇(东海)
     **/
    public static final String FLOW_TYPE_TYPE_DHF04 = "DHF04";

    /**
     * SGH01 利率变更-经营性及消费非按揭（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH01 = "SGH01";
    /**
     * DHH01 利率变更-经营性及消费非按揭（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH01 = "DHH01";
    /**
     * SGF02 利率定价-一手房按揭贷款 寿光村镇
     **/
    public static final String FLOW_TYPE_TYPE_SGF02 = "SGF02";
    /**
     * SGF02 利率定价-一手房按揭贷款 东海村镇
     **/
    public static final String FLOW_TYPE_TYPE_DHF02 = "DHF02";

    /**
     * SGH02 利率变更-一手房按揭贷款 寿光村镇
     **/
    public static final String FLOW_TYPE_TYPE_SGH02 = "SGH02";
    /**
     * DHH02 利率变更-一手房按揭贷款 东海村镇
     **/
    public static final String FLOW_TYPE_TYPE_DHH02 = "DHH02";
    /**
     * SGF03 利率定价-二手房按揭贷款 寿光村镇
     **/
    public static final String FLOW_TYPE_TYPE_SGF03 = "SGF03";
    /**
     * SGF03 利率定价-二手房按揭贷款 东海村镇
     **/
    public static final String FLOW_TYPE_TYPE_DHF03 = "DHF03";

    /**
     * SGH03 利率变更-二手房按揭贷款 寿光村镇
     **/
    public static final String FLOW_TYPE_TYPE_SGH03 = "SGH03";
    /**
     * DHH03 利率变更-二手房按揭贷款 东海村镇
     **/
    public static final String FLOW_TYPE_TYPE_DHH03 = "DHH03";
    /**
     * BG005展期申请（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG005 = "BG005";
    /**
     * BG006展期申请（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG006 = "BG006";
    /**
     * BG007展期申请（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG007 = "BG007";
    /**
     * SGH05展期申请（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH05 = "SGH05";
    /**
     * DHH05展期申请（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH05 = "DHH05";
    /**
     * BG008担保变更（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG008 = "BG008";
    /**
     * BG009担保变更（对公授信变更后）
     **/
    public static final String FLOW_TYPE_TYPE_BG009 = "BG009";
    /**
     * BG010担保变更（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG010 = "BG010";
    /**
     * BG011担保变更（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG011 = "BG011";
    /**
     * SGH07担保变更（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH07 = "SGH07";
    /**
     * DHH07担保变更（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH07 = "DHH07";
    /**
     * BG012还款账户变更（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG012 = "BG012";
    /**
     * BG013还款账户变更（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG013 = "BG013";
    /**
     * BG014还款账户变更（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG014 = "BG014";
    /**
     * SGH09还款账户变更（寿光村镇）
     **/
    public static final String FLOW_TYPE_TYPE_SGH09 = "SGH09";
    /**
     * DHH09还款账户变更（东海村镇）
     **/
    public static final String FLOW_TYPE_TYPE_DHH09 = "DHH09";
    /**
     * BG015还款计划变更（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG015 = "BG015";
    /**
     * BG016还款计划变更（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG016 = "BG016";
    /**
     * BG017还款计划变更（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG017 = "BG017";
    /**
     * SGH10 还款计划变更（寿光村镇）
     **/
    public static final String FLOW_TYPE_TYPE_SGH10 = "SGH10";
    /**
     * DHH10 还款计划变更（东海村镇）
     **/
    public static final String FLOW_TYPE_TYPE_DHH10 = "DHH10";

    /**
     * DHH14 主动还款保证金代偿 东海
     **/
    public static final String FLOW_TYPE_TYPE_DHH14 = "DHH14";

    /**
     * SGH12 延期还款（寿光村镇）
     **/
    public static final String FLOW_TYPE_TYPE_SGH12 = "SGH12";
    /**
     * DHH12 延期还款（东海村镇）
     **/
    public static final String FLOW_TYPE_TYPE_DHH12 = "DHH12";

    /**
     * SGH14 合作方保证金代偿（村镇）
     **/
    public static final String FLOW_TYPE_TYPE_SGH14 = "SGH14";
    /**
     * BG018延期还款（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG018 = "BG018";
    /**
     * BG019延期还款（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG019 = "BG019";
    /**
     * BG020延期还款（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG020 = "BG020";
    /**
     * BG021合作方保证金代偿
     **/
    public static final String FLOW_TYPE_TYPE_BG021 = "BG021";
    /**
     * BG022主动还款（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG022 = "BG022";
    /**
     * BG023主动还款（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG023 = "BG023";
    /**
     * BG024主动还款（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG024 = "BG024";
    /**
     * SGH15主动还款（寿光村镇）
     **/
    public static final String FLOW_TYPE_TYPE_SGH15 = "SGH15";
    /**
     * DHH15主动还款（东海村镇）
     **/
    public static final String FLOW_TYPE_TYPE_DHH15 = "DHH15";
    /**
     * BG025受托支付账号变更
     **/
    public static final String FLOW_TYPE_TYPE_BG025 = "BG025";
    /**
     * BG026展期协议签订审核（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG026 = "BG026";
    /**
     * BG027展期协议签订审核（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG027 = "BG027";
    /**
     * BG028展期协议签订审核（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG028 = "BG028";
    /**
     * SGH06展期协议签订审核（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH06 = "SGH06";
    /**
     * DHH06展期协议签订审核（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_DHH06 = "DHH06";
    /**
     * BG029担保变更协议签订审核（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG029 = "BG029";
    /**
     * BG030担保变更协议签订审核（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG030 = "BG030";
    /**
     * BG031担保变更协议签订审核（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG031 = "BG031";
    /**
     * SGH08担保变更协议签订审核（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH08 = "SGH08";
    /**
     * DHH08担保变更协议签订审核（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH08 = "DHH08";
    /**
     * BG032利率变更协议签订审核（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG032 = "BG032";
    /**
     * BG033利率变更协议签订审核（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG033 = "BG033";
    /**
     * BG034还款计划变更协议签订审核（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG034 = "BG034";
    /**
     * BG035还款计划变更协议签订审核（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG035 = "BG035";
    /**
     * SGH11还款计划变更协议签订审核（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH11 = "SGH11";
    /**
     * DHH11还款计划变更协议签订审核（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH11 = "DHH11";
    /**
     * BG036还款计划变更协议签订审核（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG036 = "BG036";
    /**
     * BG037延期还款协议签订审核（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG037 = "BG037";
    /**
     * BG038延期还款协议签订审核（对公）
     **/
    public static final String FLOW_TYPE_TYPE_BG038 = "BG038";
    /**
     * SGH13延期还款协议签订审核（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH13 = "SGH13";
    /**
     * DHH13延期还款协议签订审核（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH13 = "DHH13";
    /**
     * BG039延期还款协议签订审核（零售）
     **/
    public static final String FLOW_TYPE_TYPE_BG039 = "BG039";
    /**
     * BG040利率变更协议签订审核（小微）
     **/
    public static final String FLOW_TYPE_TYPE_BG040 = "BG040";
    /**
     * SGH04 利率变更协议签订审核（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH04 = "SGH04";
    /**
     * DHH04 利率变更协议签订审核（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH04 = "DHH04";
    /**
     * DA010 放款影像补扫（小微）
     **/
    public static final String FLOW_TYPE_TYPE_DA010 = "DA010";
    /**
     * DA028 展期协议影像补扫（小微）
     **/
    public static final String FLOW_TYPE_TYPE_DA028 = "DA028";
    /**
     * DA031 担保变更协议影像补扫（小微）
     **/
    public static final String FLOW_TYPE_TYPE_DA031 = "DA031";

    /*** 一般合同申请审批流程 **/
    public static final String FLOW_ID_DGYX01 = "DGYX01";
    /*** 贴现协议申请审批流程 **/
    public static final String FLOW_ID_DGYX02 = "DGYX02";
    /*** 合同影像审核(通用) **/
    public static final String FLOW_ID_DGYX03 = "DGYX03";
    /*** 一般贷款出账审批流程 **/
    public static final String FLOW_ID_DGYX04 = "DGYX04";
    /*** 银承出账审批流程 **/
    public static final String FLOW_ID_DGYX05 = "DGYX05";
    /*** 合同影像审核-一般贴现协议 **/
    public static final String FLOW_ID_DGYX06 = "DGYX06";

    /*** 最高额授信申请**/
    public static final String FLOW_TYPE_TYPE_YX001 = "YX001";
    /*** 普通贷款合同申请流程**/
    public static final String FLOW_TYPE_TYPE_YX002 = "YX002";
    /*** 贸易融资贷款合同申请流程**/
    public static final String FLOW_TYPE_TYPE_YX003 = "YX003";
    /***福费廷贷款合同申请流程**/
    public static final String FLOW_TYPE_TYPE_YX004 = "YX004";
    /***开证贷款合同申请流程**/
    public static final String FLOW_TYPE_TYPE_YX005 = "YX005";
    /***银承贷款合同申请流程**/
    public static final String FLOW_TYPE_TYPE_YX006 = "YX006";
    /***保函贷款合同申请流程**/
    public static final String FLOW_TYPE_TYPE_YX007 = "YX007";
    /***委托贷款合同申请流程**/
    public static final String FLOW_TYPE_TYPE_YX008 = "YX008";
    /***贴现合同贷款申请流程**/
    public static final String FLOW_TYPE_TYPE_YX009 = "YX009";
    /***合同影像审核**/
    public static final String FLOW_TYPE_TYPE_YX010 = "YX010";
    /***对公贷款出账流程**/
    public static final String FLOW_TYPE_TYPE_YX011 = "YX011";
    /**
     * 银承出账申请
     **/
    public static final String FLOW_TYPE_TYPE_YX012 = "YX012";
    /***委托贷款出账申请 **/
    public static final String FLOW_TYPE_TYPE_YX013 = "YX013";
    /*** 合同影像审核-一般贴现协议 **/
    public static final String FLOW_TYPE_TYPE_YX014 = "YX014";


    /*** 小微授信申请（惠享贷） **/
    public static final String FLOW_TYPE_TYPE_XW001 = "XW001";
    /*** 小微放款申请 **/
    public static final String FLOW_TYPE_TYPE_XW002 = "XW002";
    /*** 小微合同签章 **/
    public static final String FLOW_TYPE_TYPE_XW003 = "XW003";
    /*** 小微授信申请（优抵贷尽调） **/
    public static final String FLOW_TYPE_TYPE_XW004 = "XW004";
    /*** 小微授信申请（优抵贷税务） **/
    public static final String FLOW_TYPE_TYPE_XW005 = "XW005";
    /*** 小微授信申请（无还本普转） **/
    public static final String FLOW_TYPE_TYPE_XW006 = "XW006";
    /*** 小微授信申请（无还本优转） **/
    public static final String FLOW_TYPE_TYPE_XW007 = "XW007";
    /*** 小微授信申请（优企贷） **/
    public static final String FLOW_TYPE_TYPE_XW008 = "XW008";
    /*** 小微授信申请（增享贷） **/
    public static final String FLOW_TYPE_TYPE_XW009 = "XW009";
    /*** 小微追缴扣款冲正 **/
    public static final String FLOW_TYPE_TYPE_XW010 = "XW010";
    /*** 小微对公客户出账申请 **/
    public static final String FLOW_TYPE_TYPE_XW016 = "XW016";
    /*** 零售业务授信（新增） **/
    public static final String FLOW_TYPE_TYPE_LS001 = "LS001";
    /*** 零售业务授信（复议） **/
    public static final String FLOW_TYPE_TYPE_LS002 = "LS002";
    /*** 零售业务授信（变更） **/
    public static final String FLOW_TYPE_TYPE_LS003 = "LS003";
    /*** 零售业务授信（新增）-东海 **/
    public static final String FLOW_TYPE_TYPE_DHE01 = "DHE01";
    /*** 零售业务授信（复议）-东海 **/
    public static final String FLOW_TYPE_TYPE_DHE02 = "DHE02";
    /*** 零售业务授信（变更）-东海 **/
    public static final String FLOW_TYPE_TYPE_DHE03 = "DHE03";
    /*** 零售业务授信（新增）-寿光 **/
    public static final String FLOW_TYPE_TYPE_SGE01 = "SGE01";
    /*** 零售业务授信（复议）-寿光 **/
    public static final String FLOW_TYPE_TYPE_SGE02 = "SGE02";
    /*** 零售业务授信（变更）-寿光 **/
    public static final String FLOW_TYPE_TYPE_SGE03 = "SGE03";

    /**
     * 小微合同申请
     **/
    public static final String FLOW_TYPE_TYPE_XWHT001 = "XW003";

    /**
     * 信用卡申请（信贷PC端进件）
     **/
    public static final String FLOW_ID_XKYW01 = "XKYW01";
    /**
     * 信用卡申请（移动APP端进件）
     **/
    public static final String FLOW_ID_XKYW02 = "XKYW02";

    /**
     * 小微对公客户授信审批流程
     **/
    public static final String FLOW_ID_XWYW05 = "XWYW05";

    /**
     * 资产出池
     **/
    public static final String FLOW_ID_ZC001 = "ZC001";
    /**
     * 发票补录
     **/
    public static final String FLOW_ID_ZC002 = "ZC002";
    /**
     * 贸易合同
     **/
    public static final String FLOW_ID_ZC003 = "ZC003";
    /**
     * 资产池协议申请
     **/
    public static final String FLOW_ID_ZC004 = "ZC004";
    /**
     * 资产池协议变更
     **/
    public static final String FLOW_ID_ZC005 = "ZC005";
    /**
     * 流程状态标识位 000-待发起
     **/
    public static final String WF_STATUS_000 = "000";
    /**
     * 流程状态标识位 111-审批中
     **/
    public static final String WF_STATUS_111 = "111";
    /**
     * 流程状态标识位 990-取消
     **/
    public static final String WF_STATUS_990 = "990";
    /**
     * 流程状态标识位 991-拿回
     **/
    public static final String WF_STATUS_991 = "991";
    /**
     * 流程状态标识位 992-打回
     **/
    public static final String WF_STATUS_992 = "992";
    /**
     * 流程状态标识位 997-通过
     **/
    public static final String WF_STATUS_997 = "997";
    /**
     * 流程状态标识位 998-拒绝
     **/
    public static final String WF_STATUS_998 = "998";

    /**
     * 不允许重复申请相同业务的流程状态List 包括 待发起、审批中、追回、打回
     **/
    public static final String WF_STATUS_CAN_NOT_APPLY_SAME =
            WF_STATUS_000 + DEF_SPILIT_COMMMA +
                    WF_STATUS_111 + DEF_SPILIT_COMMMA +
                    WF_STATUS_991 + DEF_SPILIT_COMMMA +
                    WF_STATUS_992;
    /**
     * 不允许重复提交相同的还款方式变更业务的流程状态List 包括 审批中、追回、打回
     **/
    public static final String REPAY_WAY_CHG_WF_STATUS_CANNOT_COMMIT_SAME = "111,991,992";

    /**
     * 额度流程审批标志位
     **/
    public static final String BIZ_TYPE_LMT = "lmt";
    /**
     * 业务流程审批标志位
     **/
    public static final String BIZ_TYPE_BIZ = "biz";


    public static final String SX008 = "SX008-gbt";

    /**
     * 抵押物信息创建流程-对公
     **/
    public static final String DBGL01 = "DBGL01";

    /**
     * 抵押登记审批流程_本地机构集中办理模式
     **/
    public static final String DBGL02 = "DBGL02";

    /**
     * 抵押登记审批流程_本地机构放款后抵押模式
     **/
    public static final String DBGL03 = "DBGL03";

    /**
     * 权证入库审批流程_纸质权证集中入库模式
     **/
    public static final String DBGL04 = "DBGL04";

    /**
     * 权证入库审批流程_电子权证手工入库模式
     **/
    public static final String DBGL05 = "DBGL05";

    /**
     * 权证出库审批流程_非诉讼原因借阅
     **/
    public static final String DBGL06 = "DBGL06";

    /**
     * 权证出库审批流程_非诉讼原因借阅--东海
     **/
    public static final String DHCZ16 = "DHCZ16";

    /**
     * 权证出库审批流程_非诉讼原因借阅--寿光
     **/
    public static final String SGCZ16 = "SGCZ16";

    /**
     * 权证出库审批流程_诉讼原因借阅
     **/
    public static final String DBGL07 = "DBGL07";

    /**
     * 权证出库审批流程_诉讼原因借阅--东海
     **/
    public static final String DHCZ17 = "DHCZ17";

    /**
     * 权证出库审批流程_诉讼原因借阅--寿光
     **/
    public static final String SGCZ17 = "SGCZ17";

    /**
     * 权证出库审批流程_其他
     **/
    public static final String DBGL08 = "DBGL08";

    /**
     * 抵押注销审批流程_本地机构
     **/
    public static final String DBGL09 = "DBGL09";

    /**
     * 信用卡调额申请审批流程-分支机构
     **/
    public static final String XKYW03 = "XKYW03";

    /**
     * 信用卡调额申请审批流程（总行部门）
     **/
    public static final String XKYW04 = "XKYW04";

    /**
     * 信用卡大额分期申请审批流程
     **/
    public static final String XKYW05 = "XKYW05";

    /**
     * 信用卡大额分期放款审批流程
     **/
    public static final String XKYW06 = "XKYW06";

    /**
     * 人行征信查询审批流程（分支机构）
     **/
    public static final String ZXGL01 = "ZXGL01";

    /**
     * 人行征信查询审批流程（小微条线）
     **/
    public static final String ZXGL02 = "ZXGL02";

    /**
     * 苏州征信查询审批流程
     **/
    public static final String ZXGL03 = "ZXGL03";

    /**
     * 司法诉讼申请审批流程（非小微）
     **/
    public static final String ZCBQ01 = "ZCBQ01";

    /**
     * 司法诉讼申请审批流程（小微）
     **/
    public static final String ZCBQ02 = "ZCBQ02";
    /**
     * 核销申请审批流程（对公及个人经营性贷款）
     **/
    public static final String ZCBQ03 = "ZCBQ03";
    /**
     * 核销申请审批流程（小微）
     **/
    public static final String ZCBQ04 = "ZCBQ04";
    /**
     * 核销申请审批流程（消费类贷款）
     **/
    public static final String ZCBQ05 = "ZCBQ05";
    /**
     * 核销申请审批流程（网金）
     **/
    public static final String ZCBQ06 = "ZCBQ06";
    /**
     * 核销申请审批流程（信用卡透支贷款）
     **/
    public static final String ZCBQ07 = "ZCBQ07";
    /**
     * 核销记账审批流程
     **/
    public static final String ZCBQ08 = "ZCBQ08";
    /**
     * 债权减免审批流程（非小微）
     **/
    public static final String ZCBQ09 = "ZCBQ09";
    /**
     * 债权减免审批流程（小微）
     **/
    public static final String ZCBQ10 = "ZCBQ10";
    /**
     * 核销记账审批流程
     **/
    public static final String ZCBQ11 = "ZCBQ11";
    /**
     * 以物抵债登记审批流程（非小微）
     **/
    public static final String ZCBQ12 = "ZCBQ12";
    /**
     * 抵债资产处置审批流程（非小微）
     **/
    public static final String ZCBQ14 = "ZCBQ14";
    /**
     * 抵债资产处置审批流程（小微）
     **/
    public static final String ZCBQ15 = "ZCBQ15";
    /**
     * 以物抵债登记审批流程（小微）
     **/
    public static final String ZCBQ13 = "ZCBQ13";
    /**
     * 档案调阅审批流程（分支机构）
     **/
    public static final String DAGL01 = "DAGL01";
    /**
     * 档案调阅审批流程（集中作业中心）
     **/
    public static final String DAGL02 = "DAGL02";
    /**
     * 档案调阅审批流程（小微）
     **/
    public static final String DAGL03 = "DAGL03";
    /**
     * 档案延期审批流程（分支机构）
     **/
    public static final String DAGL04 = "DAGL04";
    /**
     * 档案延期审批流程（集中作业中心）
     **/
    public static final String DAGL05 = "DAGL05";
    /**
     * 档案延期审批流程（小微）
     **/
    public static final String DAGL06 = "DAGL06";

    /**
     * 档案销毁审批流程
     **/
    public static final String DAGL14 = "DAGL14";

    /**
     * 额度调整审批流程的流程编码
     **/
    public static final String EDGL02 = "EDGL02";

    /**
     * 司法诉讼申请审批流程（寿光）
     **/
    public static final String SGCZ26 = "SGCZ26";

    /**
     * 核销申请审批流程（寿光）
     **/
    public static final String SGCZ27 = "SGCZ27";

    /**
     * 核销记账审批流程（寿光）
     **/
    public static final String SGCZ28 = "SGCZ28";


    /**
     * 单一客户授信业务场景编号
     */
    public static final String FLOW_TYPE_TYPE_SINGLE_LMT = "SX001,SX002,SX003,SX004,SX005,SX006,SX007,DHC01,DHC02,DHC03,DHC04,DHC05,DHC06,DHC07,SGC01,SGC02,SGC03,SGC04,SGC05,SGC06,SGC07";


    /**
     * 集团客户授信业务场景编号
     */
    public static final String FLOW_TYPE_TYPE_SX008_TO_SX014 = "SX008,SX009,SX010,SX011,SX012,SX013,SX014,";

    /**
     * 零售业务申请
     */
    public static final String FLOW_TYPE_TYPE_LS001_TO_LS003 = "LS001,LS002,LS003,";


    /**
     * 小微对公客户授信新增 XW011
     */
    public static final String FLOW_TYPE_TYPE_XW011 = "XW011";
    /**
     * 小微对公客户授信续作 XW012
     */
    public static final String FLOW_TYPE_TYPE_XW012 = "XW012";
    /**
     * 小微对公客户授信复审 XW013
     */
    public static final String FLOW_TYPE_TYPE_XW013 = "XW013";
    /**
     * 小微对公客户授信复议 XW014
     */
    public static final String FLOW_TYPE_TYPE_XW014 = "XW014";
    /**
     * 小微对公客户授信再议 XW015
     */
    public static final String FLOW_TYPE_TYPE_XW015 = "XW015";

    /**
     * LS004 零售合同申请
     */
    public static final String FLOW_TYPE_TYPE_LS004 = "LS004";

    /**
     * LS008 零售合同申请
     */
    public static final String FLOW_TYPE_TYPE_LS008 = "LS008";
    /**
     * 放款影像补扫（分支机构）
     **/
    public static final String DAGL08 = "DAGL08";

    /**
     * 放款影像补扫（集中作业中心）
     **/
    public static final String DAGL09 = "DAGL09";

    /**
     * 合同审核影像补扫审批流程-分支机构
     **/
    public static final String DAGL10 = "DAGL10";

    /**
     * 合作方准入影像补扫（分支机构）
     **/
    public static final String DAGL11 = "DAGL11";

    /**
     * 合作方协议签订影像补扫（分支机构）
     **/
    public static final String DAGL12 = "DAGL12";

    /**
     * 授信影像补扫（分支机构）
     **/
    public static final String DAGL13 = "DAGL13";

    /**
     * LS005 零售放款申请（空白合同模式）
     */
    public static final String FLOW_TYPE_TYPE_LS005 = "LS005";

    /**
     * LS006 零售放款申请（生成打印模式）
     */
    public static final String FLOW_TYPE_TYPE_LS006 = "LS006";

    /**
     * 对公及个人经营性首次贷后检查审批流程
     **/
    public static final String DHGL01 = "DHGL01";

    /**
     * 小微首次贷后检查审批流程
     **/
    public static final String DHGL02 = "DHGL02";

    /**
     * 个人消费性首次贷后检查审批流程
     **/
    public static final String DHGL03 = "DHGL03";

    /**
     * 不良类对公及个人经营性定期贷后检查审批流程
     **/
    public static final String DHGL04 = "DHGL04";

    /**
     * 不良类小微定期贷后检查审批流程
     **/
    public static final String DHGL05 = "DHGL05";

    /**
     * 不良类个人消费性定期贷后检查审批流程
     **/
    public static final String DHGL06 = "DHGL06";

    /**
     * 投后定期贷后检查审批流程
     **/
    public static final String DHGL07 = "DHGL07";

    /**
     * 总行下发不定期贷后检查审批流程
     **/
    public static final String DHGL08 = "DHGL08";

    /**
     * 贷后检查延期/终止申请流程（对公及个人经营性）
     **/
    public static final String DHGL09 = "DHGL09";

    /**
     * 贷后检查延期/终止申请流程（个人消费性）
     **/
    public static final String DHGL10 = "DHGL10";

    /**
     * 贷后检查延期/终止申请流程（小微）
     **/
    public static final String DHGL11 = "DHGL11";

    /**
     * 贷后检查延期/终止申请流程（投后业务）
     **/
    public static final String DHGL12 = "DHGL12";

    /**
     * 对公及个人经营性风险分类流程（新发放及未调整）
     **/
    public static final String DHGL13 = "DHGL13";

    /**
     * 小微风险分类流程（新发放及未调整）
     **/
    public static final String DHGL14 = "DHGL14";

    /**
     * 个人消费性风险分类流程（新发放及未调整）
     **/
    public static final String DHGL15 = "DHGL15";

    /**
     * 对公及个人经营性风险分类流程（小类调整）
     **/
    public static final String DHGL16 = "DHGL16";

    /**
     * 小微经营性风险分类流程（小类调整）
     **/
    public static final String DHGL17 = "DHGL17";

    /**
     * 对公及个人经营性风险分类流程（大类调整）
     **/
    public static final String DHGL18 = "DHGL18";

    /**
     * 小微风险分类流程（大类调整）
     **/
    public static final String DHGL19 = "DHGL19";

    /**
     * 个人消费性风险分类流程（大类调整）
     **/
    public static final String DHGL20 = "DHGL20";

    /**
     * 微业贷贷后任务退回流程
     **/
    public static final String DHGL21 = "DHGL21";


    /**
     * 流程id:500
     * 业务名称：首次贷后检查
     */
    public static final String DH001 = "DH001";
    /**
     * 流程id:501
     * 业务名称：首次贷后检查-小微经营性
     */
    public static final String DH002 = "DH002";
    /**
     * 流程id:501
     * 业务名称：首次贷后检查
     */
    public static final String DH003 = "DH003";
    /**
     * 流程id:502
     * 业务名称：首次贷后检查-个人消费性
     */
    public static final String DH004 = "DH004";
    /**
     * 流程id:500
     * 业务名称：定期贷后检查-对公正常类
     */
    public static final String DH005 = "DH005";
    /**
     * 流程id:500
     * 业务名称：定期贷后检查-对公正常类
     */
    public static final String DH006 = "DH006";
    /**
     * 流程id:503
     * 业务名称：定期贷后检查-对公不良类
     */
    public static final String DH007 = "DH007";
    /**
     * 流程id:500
     * 业务名称：定期贷后检查-个人经营性正常类
     */
    public static final String DH008 = "DH008";
    /**
     * 流程id:500
     * 业务名称：定期贷后检查-个人经营性瑕疵类
     */
    public static final String DH009 = "DH009";
    /**
     * 流程id:503
     * 业务名称：定期贷后检查-个人经营性不良类
     */
    public static final String DH010 = "DH010";
    /**
     * 流程id:501
     * 业务名称：定期贷后检查-小微经营性正常类
     */
    public static final String DH011 = "DH011";
    /**
     * 流程id:501
     * 业务名称：定期贷后检查-小微经营性瑕疵类
     */
    public static final String DH012 = "DH012";
    /**
     * 流程id:504
     * 业务名称：定期贷后检查-小微经营性不良类
     */
    public static final String DH013 = "DH013";
    /**
     * 流程id:501
     * 业务名称：定期贷后检查-小微消费性正常类
     */
    public static final String DH014 = "DH014";
    /**
     * 流程id:501
     * 业务名称：定期贷后检查-小微消费性瑕疵类
     */
    public static final String DH015 = "DH015";
    /**
     * 流程id:504
     * 业务名称：定期贷后检查
     */
    public static final String DH016 = "DH016";
    /**
     * 流程id:502
     * 业务名称：定期贷后检查-个人消费性
     */
    public static final String DH017 = "DH017";
    /**
     * 流程id:505
     * 业务名称：定期贷后检查-个人消费性不良类
     */
    public static final String DH018 = "DH018";
    /**
     * 流程id:506
     * 业务名称：定期贷后检查-同业授信
     */
    public static final String DH019 = "DH019";
    /**
     * 流程id:506
     * 业务名称：定期贷后检查-主题授信
     */
    public static final String DH020 = "DH020";
    /**
     * 流程id:506
     * 业务名称：定期贷后检查-产品授信
     */
    public static final String DH021 = "DH021";
    /**
     * 流程id:500
     * 业务名称：不定期贷后检查-对公正常类
     */
    public static final String DH022 = "DH022";
    /**
     * 流程id:500
     * 业务名称：不定期贷后检查-对公瑕疵类
     */
    public static final String DH023 = "DH023";
    /**
     * 流程id:503
     * 业务名称：不定期贷后检查-对公不良类
     */
    public static final String DH024 = "DH024";
    /**
     * 流程id:500
     * 业务名称：不定期贷后检查
     */
    public static final String DH025 = "DH025";
    /**
     * 流程id:500
     * 业务名称：不定期贷后检查-个人经营性瑕疵类
     */
    public static final String DH026 = "DH026";
    /**
     * 流程id:503
     * 业务名称：不定期贷后检查-个人经营性不良类
     */
    public static final String DH027 = "DH027";
    /**
     * 流程id:501
     * 业务名称：不定期贷后检查-小微经营性正常类
     */
    public static final String DH028 = "DH028";
    /**
     * 流程id:501
     * 业务名称：不定期贷后检查-小微经营性瑕疵类
     */
    public static final String DH029 = "DH029";
    /**
     * 流程id:504
     * 业务名称：不定期贷后检查-小微经营性不良类
     */
    public static final String DH030 = "DH030";
    /**
     * 流程id:501
     * 业务名称：不定期贷后检查-小微消费性正常类
     */
    public static final String DH031 = "DH031";
    /**
     * 流程id:501
     * 业务名称：不定期贷后检查-小微消费性瑕疵类
     */
    public static final String DH032 = "DH032";
    /**
     * 流程id:504
     * 业务名称：不定期贷后检查-小微消费性不良类
     */
    public static final String DH033 = "DH033";
    /**
     * 流程id:502
     * 业务名称：不定期贷后检查-个人消费性
     */
    public static final String DH034 = "DH034";
    /**
     * 流程id:505
     * 业务名称：不定期贷后检查-个人消费性不良类
     */
    public static final String DH035 = "DH035";
    /**
     * 流程id:506
     * 业务名称：不定期贷后检查-同业授信
     */
    public static final String DH036 = "DH036";
    /**
     * 流程id:506
     * 业务名称：不定期贷后检查-主体授信
     */
    public static final String DH037 = "DH037";
    /**
     * 流程id:506
     * 业务名称：不定期贷后检查-产品授信
     */
    public static final String DH038 = "DH038";
    /**
     * 流程id:507
     * 业务名称：不定期贷后检查-总行下发
     */
    public static final String DH039 = "DH039";
    /**
     * 流程id:508
     * 业务名称：贷后检查延期-对公及个人经营性
     */
    public static final String DH040 = "DH040";
    /**
     * 流程id:509
     * 业务名称：贷后检查延期-个人消费性
     */
    public static final String DH041 = "DH041";
    /**
     * 流程id:510
     * 业务名称：贷后检查延期-小微
     */
    public static final String DH042 = "DH042";
    /**
     * 流程id:511
     * 业务名称：贷后检查延期-投后业务
     */
    public static final String DH043 = "DH043";
    /**
     * 流程id:508
     * 业务名称：贷后检查终止-对公及个人经营性
     */
    public static final String DH044 = "DH044";
    /**
     * 流程id:509
     * 业务名称：贷后检查终止-个人消费性
     */
    public static final String DH045 = "DH045";
    /**
     * 流程id:510
     * 业务名称：贷后检查终止-小微
     */
    public static final String DH046 = "DH046";
    /**
     * 流程id:511
     * 业务名称：贷后检查终止-投后业务
     */
    public static final String DH047 = "DH047";
    /**
     * 流程id:512
     * 业务名称：对公及个人经营性风险分类-新发放及未调整
     */
    public static final String DH048 = "DH048";
    /**
     * 流程id:513
     * 业务名称：小微风险分类-新发放及未调整
     */
    public static final String DH049 = "DH049";
    /**
     * 流程id:514
     * 业务名称：个人消费性风险分类-新发放及未调整
     */
    public static final String DH050 = "DH050";
    /**
     * 流程id:519
     * 业务名称：对公及个人经营性风险分类-小类调整
     */
    public static final String DH051 = "DH051";
    /**
     * 流程id:515
     * 业务名称：小微经营性风险分类-小类调整
     */
    public static final String DH052 = "DH052";
    /**
     * 流程id:516
     * 业务名称：对公及个人经营性风险分类-大类调整
     */
    public static final String DH053 = "DH053";
    /**
     * 流程id:517
     * 业务名称：小微风险分类-大类调整
     */
    public static final String DH054 = "DH054";
    /**
     * 流程id:518
     * 业务名称：个人消费性风险分类-大类调整
     */
    public static final String DH055 = "DH055";

    /**
     * 流程id:604
     * 业务名称：空白凭证修改作废审批流程
     */
    public static final String XTGL02 = "XTGL02";

    /* 空白凭证修改 **/
    public static final String FLOW_TYPE_TYPE_XT003 = "XT003";
    /* 空白凭证作废 **/
    public static final String FLOW_TYPE_TYPE_XT004 = "XT004";

    /**
     * 信用卡大额分期申请
     */
    public static final String FLOW_ID_XK005 = "XK005";
    /**
     * 流程id:611
     * 房抵e点贷尽调结果录入流程
     **/
    public static final String DGYX07 = "DGYX07";
    /**
     * 流程id:613
     * 房抵e点贷押品查询查封流程
     **/
    public static final String DGYX09 = "DGYX09";

    /**
     * 流程id:612
     * 房抵e点贷授信押品关联流程
     **/
    public static final String DGYX08 = "DGYX08";

    /**
     * 流程id:614
     * 房抵e点贷无还本续贷审核流程
     **/
    public static final String DGYX10 = "DGYX10";

    /**
     * 流程id:615
     * 省心快贷线上提款查封查验流程
     **/
    public static final String DGYX11 = "DGYX11";

    /**
     * 流程id:616
     * 省心快贷提款利率修改流程
     **/
    public static final String DGYX12 = "DGYX12";

    /**
     * 对公授信申报审批流程（寿光）
     **/
    public static final String SGCZ04 = "SGCZ04";

    /**
     * 对公授信批复变更审批流程（寿光）
     **/
    public static final String SGCZ05 = "SGCZ05";

    /**
     * 对公出账申请审批流程（寿光）
     **/
    public static final String SGCZ06 = "SGCZ06";

    /**
     * 档案调阅/延期审批流程（寿光）
     **/
    public static final String SGCZ18 = "SGCZ18";

    /**
     * 档案销毁审批流程（寿光）
     **/
    public static final String SGCZ20 = "SGCZ20";

    /**
     * 影像补扫审批流程（寿光）
     **/
    public static final String SGCZ21 = "SGCZ21";

    /**
     * 贷后检查审批流程（寿光）
     **/
    public static final String SGCZ22 = "SGCZ22";

    /**
     * 总行下发不定期检查审批流程（寿光）
     **/
    public static final String SGCZ23 = "SGCZ23";

    /**
     * 贷后检查延期终止审批流程（寿光）
     **/
    public static final String SGCZ24 = "SGCZ24";

    /**
     * 风险分类审批流程（寿光）
     **/
    public static final String SGCZ25 = "SGCZ25";

    /**
     * 档案调阅/延期审批流程（东海）
     **/
    public static final String DHCZ18 = "DHCZ18";

    /**
     * 档案销毁审批流程（东海）
     **/
    public static final String DHCZ20 = "DHCZ20";

    /**
     * 影像补扫审批流程（东海）
     **/
    public static final String DHCZ21 = "DHCZ21";

    /**
     * 风险分类审批流程（东海）
     **/
    public static final String DHCZ25 = "DHCZ25";

    /* 单一客户授信新增（寿光） **/
    public static final String FLOW_TYPE_SGC01 = "SGC01";

    /* 单一客户授信续作（寿光） **/
    public static final String FLOW_TYPE_SGC02 = "SGC02";

    /* 单一客户授信复审（寿光） **/
    public static final String FLOW_TYPE_SGC03 = "SGC03";

    /* 单一客户授信复议（寿光） **/
    public static final String FLOW_TYPE_SGC04 = "SGC04";

    /* 单一客户授信再议（寿光） **/
    public static final String FLOW_TYPE_SGC05 = "SGC05";

    /* 单一客户授信变更（寿光） **/
    public static final String FLOW_TYPE_SGC06 = "SGC06";

    /* 单一客户预授信细化（寿光） **/
    public static final String FLOW_TYPE_SGC07 = "SGC07";

    /* 集团客户授信新增（寿光） **/
    public static final String FLOW_TYPE_SGC08 = "SGC08";

    /* 集团客户授信续作（寿光） **/
    public static final String FLOW_TYPE_SGC09 = "SGC09";

    /* 集团客户授信复审（寿光） **/
    public static final String FLOW_TYPE_SGC10 = "SGC10";

    /* 集团客户授信复议（寿光） **/
    public static final String FLOW_TYPE_SGC11 = "SGC11";

    /* 集团客户授信再议（寿光） **/
    public static final String FLOW_TYPE_SGC12 = "SGC12";

    /* 集团客户授信变更（寿光） **/
    public static final String FLOW_TYPE_SGC13 = "SGC13";

    /* 集团客户预授信细化（寿光） **/
    public static final String FLOW_TYPE_SGC14 = "SGC14";

    /* 单一客户授信批复变更（寿光） **/
    public static final String FLOW_TYPE_SGC15 = "SGC15";

    /* 集团客户授信批复变更（寿光） **/
    public static final String FLOW_TYPE_SGC16 = "SGC16";

    /* 合同影像审核（寿光） **/
    public static final String FLOW_TYPE_SGD01 = "SGD01";

    /* 对公贷款出账申请（寿光） **/
    public static final String FLOW_TYPE_SGD02 = "SGD02";

    /* 银承出账申请（寿光） **/
    public static final String FLOW_TYPE_SGD03 = "SGD03";

    /* 委托贷款出账申请（寿光） **/
    public static final String FLOW_TYPE_SGD04 = "SGD04";

    /**
     * 对公授信申报审批流程（东海）
     **/
    public static final String DHCZ04 = "DHCZ04";

    /**
     * 对公授信批复变更审批流程（东海）
     **/
    public static final String DHCZ05 = "DHCZ05";

    /**
     * 对公出账申请审批流程（东海）
     **/
    public static final String DHCZ06 = "DHCZ06";

    /* 单一客户授信新增（东海） **/
    public static final String FLOW_TYPE_DHC01 = "DHC01";

    /* 单一客户授信续作（东海） **/
    public static final String FLOW_TYPE_DHC02 = "DHC02";

    /* 单一客户授信复审（东海） **/
    public static final String FLOW_TYPE_DHC03 = "DHC03";

    /* 单一客户授信复议（东海） **/
    public static final String FLOW_TYPE_DHC04 = "DHC04";

    /* 单一客户授信再议（东海） **/
    public static final String FLOW_TYPE_DHC05 = "DHC05";

    /* 单一客户授信变更（东海） **/
    public static final String FLOW_TYPE_DHC06 = "DHC06";

    /* 单一客户预授信细化（东海） **/
    public static final String FLOW_TYPE_DHC07 = "DHC07";

    /* 集团客户授信新增（东海） **/
    public static final String FLOW_TYPE_DHC08 = "DHC08";

    /* 集团客户授信续作（东海） **/
    public static final String FLOW_TYPE_DHC09 = "DHC09";

    /* 集团客户授信复审（东海） **/
    public static final String FLOW_TYPE_DHC10 = "DHC10";

    /* 集团客户授信复议（东海） **/
    public static final String FLOW_TYPE_DHC11 = "DHC11";

    /* 集团客户授信再议（东海） **/
    public static final String FLOW_TYPE_DHC12 = "DHC12";

    /* 集团客户授信变更（东海） **/
    public static final String FLOW_TYPE_DHC13 = "DHC13";

    /* 集团客户预授信细化（东海） **/
    public static final String FLOW_TYPE_DHC14 = "DHC14";

    /* 单一客户授信批复变更（东海） **/
    public static final String FLOW_TYPE_DHC15 = "DHC15";

    /* 集团客户授信批复变更（东海） **/
    public static final String FLOW_TYPE_DHC16 = "DHC16";

    /* 合同影像审核（东海） **/
    public static final String FLOW_TYPE_DHD01 = "DHD01";

    /* 对公贷款出账申请（东海） **/
    public static final String FLOW_TYPE_DHD02 = "DHD02";

    /* 银承出账申请（东海） **/
    public static final String FLOW_TYPE_DHD03 = "DHD03";

    /* 委托贷款出账申请（东海） **/
    public static final String FLOW_TYPE_DHD04 = "DHD04";
    /**
     * 贷后检查审批流程（东海）
     **/
    public static final String FLOW_TYPE_DHCZ22 = "DHCZ22";

    /**
     * 总行下发不定期检查审批流程（东海）
     **/
    public static final String DHCZ23 = "DHCZ23";

    /**
     * 贷后检查延期终止审批流程（东海）
     **/
    public static final String FLOW_TYPE_DHCZ24 = "DHCZ24";
    /**
     * 司法诉讼申请审批流程（东海）
     **/
    public static final String FLOW_TYPE_DHCZ26 = "DHCZ26";

    /**
     * 核销申请审批流程（东海）
     **/
    public static final String FLOW_TYPE_DHCZ27 = "DHCZ27";

    /**
     * 核销记账审批流程（东海）
     **/
    public static final String FLOW_TYPE_DHCZ28 = "DHCZ28";

    /**
     * 人行征信查询审批流程（东海）
     **/
    public static final String FLOW_TYPE_DHCZ37 = "DHCZ37";

    /**
     * 零售授信申请审批流程（寿光）
     **/
    public static final String SGCZ07 = "SGCZ07";

    /**
     * 零售授信申请审批流程（东海）
     **/
    public static final String DHCZ07 = "DHCZ07";

    /**
     * 零DHH16受托支付账号变更（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHH16 = "DHH16";
    /**
     * SGH16受托支付账号变更（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGH16 = "SGH16";


    /**
     * 售放款申请（东海）
     **/
    public static final String FLOW_TYPE_TYPE_DHE04 = "DHE04";

    /**
     * 零售放款申请（寿光）
     **/
    public static final String FLOW_TYPE_TYPE_SGE04 = "SGE04";

    /** XT005数据修改-统计分类（对公及零售） **/
    public final static String FLOW_TYPE_TYPE_XT005 = "XT005";

    /** XT008 数据修改-科目投向（对公及零售） **/
    public final static String FLOW_TYPE_TYPE_XT008 = "XT008";
    /** XT011 数据修改-其他信息（对公及零售） **/
    public final static String FLOW_TYPE_TYPE_XT011 = "XT011";
    /** XT006 数据修改-统计分类（小微） **/
    public final static String FLOW_TYPE_TYPE_XT006 = "XT006";
    /**XT009 数据修改-科目投向（小微） **/
    public final static String FLOW_TYPE_TYPE_XT009 = "XT009";
    /** XT012 数据修改-其他信息（小微） **/
    public final static String FLOW_TYPE_TYPE_XT012 = "XT012";
    /** XT007 数据修改-统计分类（网金） **/
    public final static String FLOW_TYPE_TYPE_XT007 = "XT007";
    /** XT010 数据修改-科目投向（网金） **/
    public final static String FLOW_TYPE_TYPE_XT010 = "XT010";
    /** XT013 数据修改-其他信息（网金） **/
    public final static String FLOW_TYPE_TYPE_XT013 = "XT013";
    /** SGH19 数据修改-统计分类（寿光） **/
    public final static String FLOW_TYPE_TYPE_SGH19 = "SGH19";
    /** SGH20 数据修改-科目投向（寿光） **/
    public final static String FLOW_TYPE_TYPE_SGH20 = "SGH20";
    /** SGH21 数据修改-其他信息（寿光）**/
    public final static String FLOW_TYPE_TYPE_SGH21 = "SGH21";
    /** DHH19 数据修改-统计分类（东海）**/
    public final static String FLOW_TYPE_TYPE_DHH19 = "DHH19";
    /** DHH20数据修改-科目投向（东海）**/
    public final static String FLOW_TYPE_TYPE_DHH20 = "DHH20";
    /** DHH21数据修改-其他信息（东海）**/
    public final static String FLOW_TYPE_TYPE_DHH21 = "DHH21";

}
