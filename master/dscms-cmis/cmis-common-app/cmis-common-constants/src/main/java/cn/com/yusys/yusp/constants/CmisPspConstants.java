package cn.com.yusys.yusp.constants;

/**
 * 常量类:贷后管理服务
 */
public class CmisPspConstants {

    /**
     * 成功标志
     **/
    public static final String FLAG_SUCCESS = "S";

    /**
     * 失败标志
     **/
    public static final String FLAG_FAIL = "F";

    /**
     * 寿光村镇延期业务类型
     **/
    public static final String BIZ_TYPE_SGK20 = "SGK20";

    /**
     * 寿光村镇延期业务类型
     **/
    public static final String BIZ_TYPE_SGK21 = "SGK21";


    /**
     * 东海村镇延期业务类型
     **/
    public static final String BIZ_TYPE_DHK20 = "DHK20";

    /**
     * 东海村镇延期业务类型
     **/
    public static final String BIZ_TYPE_DHK21 = "DHK21";
}
