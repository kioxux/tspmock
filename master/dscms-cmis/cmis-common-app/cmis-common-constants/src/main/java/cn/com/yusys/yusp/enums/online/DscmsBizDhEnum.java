package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-贷后管理的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizDhEnum {
    /* 还款模式 */
    REPAY_TYPE_A009("A009", "利随本清 "),//利随本清
    REPAY_TYPE_A012("A012", "按226比例还款"),//按226比例还款
    REPAY_TYPE_A030("A030", "定制还款"),//定制还款
    REPAY_TYPE_A015("A015", "按月还息,按年还本"),//按月还息,按年还本
    REPAY_TYPE_A003("A003", "等额本金"),//等额本金
    REPAY_TYPE_A002("A002", "等额本息"),//等额本息
    REPAY_TYPE_A001("A001", "按期付息到期还本"),//按期付息到期还本
    /* 币种 */
    CUR_TYPE_01("01", "人民币"),
    CUR_TYPE_12("12", "英镑"),
    CUR_TYPE_13("13", "港币"),
    CUR_TYPE_14("14", "美元"),
    CUR_TYPE_38("38", "欧元"),
    CUR_TYPE_18("18", "新加坡元"),
    CUR_TYPE_27("27", "日元"),
    CUR_TYPE_28("28", "加元"),
    CUR_TYPE_29("29", "澳元"),
    CUR_TYPE_15("15", "瑞士法郎"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizDhEnum enumData : EnumSet.allOf(DscmsBizDhEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizDhEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizDhEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
