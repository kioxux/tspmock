package cn.com.yusys.yusp.enums.batch;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 贷记卡账户基本信息文件字段枚举类
 */
public enum DjkDataAccountFieldEnums {
    ACCT_NO("acctNo", 9),//账户编号
    CUST_ID("custId", 9),//客户编号
    PRODUCT_CD("productCd", 6),//产品代码
    CURR_CD("currCd", 3),//币种
    CREDIT_LIMIT("creditLimit", 13),//信用额度
    TEMP_LIMIT("tempLimit", 13),//临时额度
    TEMP_LIMIT_BEGIN_DATE("tempLimitBeginDate", 8),//临时额度开始日期
    TEMP_LIMIT_END_DATE("tempLimitEndDate", 8),//临时额度结束日期
    CASH_LIMIT_RT("cashLimitRt", 5),//取现额度比例
    OVRLMT_RATE("ovrlmtRate", 5),//授权超限比例
    LOAN_LIMIT_RT("loanLimitRt", 5),//额度内分期额度比例
    CURR_BAL("currBal", 15),//当前余额
    CASH_BAL("cashBal", 15),//取现余额
    PRINCIPAL_BAL("principalBal", 15),//本金余额
    LOAN_BAL("loanBal", 15),//额度内分期余额
    QUAL_GRACE_BAL("qualGraceBal", 15),//全部应还款额
    POINT_BAL("pointBal", 13),//积分余额
    SETUP_DATE("setupDate", 8),//创建日期
    BILLING_CYCLE("billingCycle", 2),//账单周期
    STMT_FLAG("stmtFlag", 1),//寄送账单标志 STD_RZ_STMT_FLAG
    STMT_MAIL_ADDR_IND("stmtMailAddrInd", 1),//账单寄送地址标志 STD_RZ_STMT_MAIL_ADDR_IND
    STMT_MEDIA_TYPE("stmtMediaType", 1),//账单介质类型 STD_RZ_STMT_MEDIA_TYPE
    BLOCK_CODE("blockCode", 27),//锁定码
    AGE_CD("ageCd", 1),//账龄
    DD_IND("ddInd", 1),//约定还款类型 STD_RZ_DD_IND
    DD_BANK_NAME("ddBankName", 80),//约定还款银行名称
    DD_BANK_BRANCH("ddBankBranch", 9),//约定还款开户行号
    DD_BANK_ACCT_NO("ddBankAcctNo", 40),//约定还款扣款账号
    DD_BANK_ACCT_NAME("ddBankAcctName", 80),//约定还款扣款账户姓名
    CLOSED_DATE("closedDate", 8),//最终销户日期
    CANCEL_DATE("cancelDate", 8),//预销户日期
    TOT_DUE_AMT("totDueAmt", 15),//最小还款额
    PAYMENT_HIST("paymentHist", 24),//还款历史信息 STD_RZ_PAYMENT_HIST
    CTD_PAYMENT_AMT("ctdPaymentAmt", 15),//上期还款金额
    LAST_TRANS_DATE("lastTransDate", 8),//客户最后交易日期
    ACTUAL_PAYMENT_AMT("actualPaymentAmt", 15),//本月实际还款金额
    CHARGE_OFF_AMT("chargeOffAmt", 15),//核销金额
    CHARGE_OFF_DATE("chargeOffDate", 8),//核销日期
    UNSTMT_BAL("unstmtBal", 15),//未出账单余额
    REMARK("remark", 300),//备注
    YTD_RETAIL_AMT("ytdRetailAmt", 15),//当年消费金额
    COLLECT_TIMES("collectTimes", 2),//入催次数
    COLLECT_REASON("collectReason", 2),//入催原因 STD_RZ_COLLECT_REASON
    ACCT_TYPE("acctType", 1),//账户类型 STD_RZ_ACCT_TYPE
    BANK_CUSTOMER_ID("bankCustomerId", 20),//行内客户号
    CTD_REAIL_PRIN_BAL("ctdReailPrinBal", 15),//当期消费本金余额
    STMT_REAIL_PRIN_BAL("stmtReailPrinBal", 15),//已出账单消费本金余额
    CTD_CASH_PRIN_BAL("ctdCashPrinBal", 15),//当期取现本金余额
    STMT_CASH_PRIN_BAL("stmtCashPrinBal", 15),//已出账单取现本金余额
    CTD_LOAN_PRIN_BAL("ctdLoanPrinBal", 15),//当期分期应还本金余额
    STMT_LOAN_PRIN_BAL("stmtLoanPrinBal", 15),//已出账单分期应还本金余额
    LARGE_LOAN_BAL_XFROUT("largeLoanBalXfrout", 15),//额度外分期余额
    DEPOSIT_BAL("depositBal", 15),//溢缴款
    CTD_LOAN_TXN_FEE("ctdLoanTxnFee", 15),//当期分期交易费
    STMT_LOAN_TXN_FEE("stmtLoanTxnFee", 15),//已出账单分期交易费
    CTD_CASH_TXN_FEE("ctdCashTxnFee", 15),//当期取现交易费
    STMT_CASH_TXN_FEE("stmtCashTxnFee", 15),//已出账单取现交易费
    CTD_OTHER_TXN_FEE("ctdOtherTxnFee", 15),//当期其他交易费
    STMT_OTHER_TXN_FEE("stmtOtherTxnFee", 15),//已出账单其他交易费
    CTD_ANUAL_FEE("ctdAnualFee", 15),//当期年费
    STMT_ANUAL_FEE("stmtAnualFee", 15),//已出账单年费
    CTD_LATE_CHARGE_FEE("ctdLateChargeFee", 15),//当期违约费
    STMT_LATE_CHARGE_FEE("stmtLateChargeFee", 15),//已出账单违约费
    CTD_SERVICE_FEE("ctdServiceFee", 15),//当期服务费
    STMT_SERVICE_FEE("stmtServiceFee", 15),//已出账单服务费
    CTD_INT("ctdInt", 15),//当期利息
    STMT_INT("stmtInt", 15),//已出账单利息
    OWNING_BRANCH("owningBranch", 9),//发卡网点
    UNMATCH_DB_AMT("unmatchDbAmt", 15),//未匹配借记金额
    UNMATCH_CR_AMT("unmatchCrAmt", 15),//未匹配贷记金额
    COLL_REC_TYPE("collRecType", 1),//催收记录类型 STD_RZ_COLL_REC_TYPE
    COLLECT_DATE("collectDate", 8),//进入催收日期
    SANTISFIED_DATE("santisfiedDate", 8),//满意日期
    OVERLIMIT_AMT("overlimitAmt", 15),//超限金额
    LOAN_IND("loanInd", 1),//有无分期账户 STD_RZ_LOAN_IND
    LAST_STMT_DATE("lastStmtDate", 8),//上一账单日
    LAST_DUE_DATE("lastDueDate", 8),//上期到期还款日
    REMAIN_GRACE_BAL("remainGraceBal", 15),//当期欠款余额
    PAST_DUE_AMT_1("pastDueAmt1", 15),//30 天以下欠款
    PAST_DUE_AMT_2("pastDueAmt2", 15),//30-59 天欠款
    PAST_DUE_AMT_3("pastDueAmt3", 15),//60-89 天欠款
    PAST_DUE_AMT_4("pastDueAmt4", 15),//90-119 天欠款
    PAST_DUE_AMT_5("pastDueAmt5", 15),//120-149 天欠款
    PAST_DUE_AMT_6("pastDueAmt6", 15),//150-179 天欠款
    PAST_DUE_AMT_7("pastDueAmt7", 15),//180 天及以上欠款
    AGE_HIST("ageHist", 24),//24 个月账龄历史信息
    CTD_CASH_CNT("ctdCashCnt", 6),//本周期取现笔数
    DD_OTHER_BANK_IND("ddOtherBankInd", 1),//约定还款他行代扣标识
    DEFAULT_LOGICAL_CARD_NO("defaultLogicalCardNo", 19),//默认逻辑卡号
    RETAIL_INT("retailInt", 15),//消费利息
    CASH_INT("cashInt", 15),//取现利息
    LOAN_INT("loanInt", 15),//分期利息
    PENDING_INT_BAL("pendingIntBal", 15),//生息的应收账款余额
    DD_BANK_BRANCH_2("ddBankBranch2", 20),//约定还款开户行行号 2
    GRACE_DAYS_FULL_IND("graceDaysFullInd", 1),//是否全额还款
    GLP_BRANCH("glpBranch", 9),//核算分行
    CREATE_TIME("createTime", 14),//账户开立时间
    LAST_MODIFIED_TIME("lastModifiedTime", 14),//最近一次维护日期
    FILLER("filler", 62);//预留字段
    public static Map<String, Integer> fieldLength;

    static {
        fieldLength = new TreeMap<String, Integer>();
        for (DjkDataAccountFieldEnums enumData : EnumSet.allOf(DjkDataAccountFieldEnums.class)) {
            fieldLength.put(enumData.field, enumData.length);
        }
    }

    public String field;//字段名
    public Integer length;//长度

    DjkDataAccountFieldEnums(String field, Integer length) {
        this.field = field;
        this.length = length;
    }

    public static Integer lookup(String field) {
        return fieldLength.get(field);
    }

    public static String getField(Integer dataLength) {
        String field = null;
        for (DjkDataAccountFieldEnums enumData : values()) {
            if (enumData.length.equals(dataLength)) {
                field = enumData.field;
                break;
            }
        }
        return field;
    }

    public final Integer getLength() {
        return fieldLength.get(this.field);
    }
}
