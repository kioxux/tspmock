package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 贷后管理服务的错误异常枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EpsEnum {

    EPS030001("30001", ""),
    EPS030002("30002", ""),
    EPS039999("9999", "系统异常!"),
    EPS030000("0000", "操作成功!"),
    EPS030003("30003", "传递参数为空"),
    EPS030004("30004","该客户存在在途贷后检查任务不可移交，请先完成任务"),
    EPS030005("30005","该客户存在在途风险分类任务不可移交，请先完成任务"),
    EPS030006("30006","该客户存在在途贷后检查和风险分类任务不可移交，请先完成任务"),

    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EcsEnum enumData : EnumSet.allOf(EcsEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EpsEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EpsEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
