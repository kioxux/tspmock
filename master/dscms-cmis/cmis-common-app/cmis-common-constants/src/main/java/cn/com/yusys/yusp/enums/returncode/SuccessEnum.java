package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 成功枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum SuccessEnum {
    CMIS_SUCCSESS("0", "调用Feign成功"),
    SUCCESS("0000", "交易成功"),
    SUCCESS_WXD("00", "交易成功"),//
    OCR_SUCCESS_CODE("00000000", "调用ocr成功"),
    OCR_SUCCESS_FLAG("1", "调用ocr成功"),
    YKY_SUCCESS("GSS001", "调用印控成功"),
    /**
     * 惠押押交易成功
     */
    HYY_SUCCESS("200","OK"),
    Add_DATA_SUCCESS("","添加数据成功"),
    Add_DATA_("","添加数据成功"),
    CODE_SUCCESS("200", "交易成功"),
    MESSAGE_SUCCESS("OK", "交易成功"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (SuccessEnum enumData : EnumSet.allOf(SuccessEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private SuccessEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (SuccessEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
