package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 额度服务的错误异常枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EclEnum {

    ECL070001("70001", ""),
    ECL070002("70002", ""),
    ECL070003("70003", "额度状态必须是生效"),
    ECL070004("70004", "合同到期日不能超过额度分项到期日"),
    ECL070005("70005", "额度为预授信额度时不允许占用"),
    ECL070006("70006", "本次占用敞口金额+额度分项已占用敞口大于授信敞口金额"),
    ECL070007("70007", "未找到额度分项信息"),
    ECL070008("70008", "非最高额授信协议，不允许占用到法人客户额度额度分项层级，只允许占用至产品分项层级"),
    ECL070009("70009", "未获取到白名单额度信息"),
    ECL070010("70010", "白名单额度信息不足额，检验不通过"),
    ECL070011("70011", "未获取到合作方批复分项信息"),
    ECL070012("70012", "未获取到合作方批复信息"),
    ECL070013("70013", "本次占用金额超出合作方单笔业务限额"),
    ECL070014("70014", "合作方总额不足，校验不通过"),
    ECL070015("70015", "合作方单户限额校验不通过"),
    ECL070016("70016", "未找到额度批复信息"),
    ECL070017("70017", "额度同步，非全行适用，需要有适用机构列表"),
    ECL070018("70018", "合同不存在"),
    ECL070019("70019", "合同状态不可使用"),
    ECL070020("70020", "台账到期日不能超过额度分项到期日"),
    ECL070021("70021", "额度同步，该批复已经存在，不允许重新生成"),
    ECL070022("70022", "额度同步，该批复分项已经存在，不允许重新生成"),
    ECL070023("70023", "额度同步，该批复分项存在未结清业务，不允许删除"),
    ECL070024("70024", "授信台账不存在"),//授信台账不存在
    ECL070025("70025", "计算可用额度失败"),//计算可用额度失败
    ECL070026("70026", "额度台账项下冻结金额为空"),//额度台账项下冻结金额为空
    ECL070027("70027", "请求入参数据不存在"),//请求入参数据不存在
    ECL070028("70028", "查询第三方额度信息不存在"),//查询第三方额度信息不存在
    ECL070029("70029", "未获取到需要查询的额度台账/第三方额度台账入参信息"),//未获取到需要查询的额度台账/第三方额度台账入参信息
    ECL070030("70030", "授信协议不存在"),//授信协议不存在
    ECL070031("70031", "新增数据失败该数据已存在"),//新增数据失败该数据已存在
    ECL070032("70032", "获取当前登录用户信息异常"),//获取当前登录用户信息异常
    ECL070033("70033", "新增数据异常"),//新增数据异常
    ECL070034("70034", "获取额度协议及额度台账集合-参数异常"),//获取额度协议及额度台账集合-参数异常
    ECL070035("70035", "获取额度协议及额度台账集合-额度协议不存"),//获取额度协议及额度台账集合-额度协议不存
    ECL070036("70036", "获取额度协议及额度台账集合-额度"),//获取额度协议及额度台账集合-额度
    ECL070037("70037", "未获取到额度协议编号"),//未获取到额度协议编号
    ECL070038("70038", "计算授信余额异常：该额度编号项下额度类型为空，请确认数据完整性"),//计算授信余额异常：该额度编号项下额度类型为空，请确认数据完整性
    ECL070039("70039", "计算授信余额异常：该额度编号项下启用额度为空，请确认数据完整性"),//计算授信余额异常：该额度编号项下启用额度为空，请确认数据完整性
    ECL070040("70040", "额度批复不存在"),//额度批复不存在
    ECL070041("70041", "请求入参数据不存在"),//请求入参数据不存在
    ECL070042("70042", "单一客户额度调整失败，批复主信息不存在"),//单一客户额度调整失败，批复主信息不存在
    ECL070043("70043", "单一客户额度调整失败，额度调整申请主信息不存在"),//单一客户额度调整失败，额度调整申请主信息不存在
    ECL070044("70044", "单一客户额度调整失败，批复分项信息不存在"),//单一客户额度调整失败，批复分项信息不存在
    ECL070045("70045", "单一客户额度调整失败，占用合同信息不存在"),//单一客户额度调整失败，占用合同信息不存在
    ECL070046("70046", "合作方台账状态修改失败"),//合作方台账状态修改失败
    ECL070047("70047", "合作方授信分项状态修改失败"),//合作方授信分项状态修改失败
    ECL070048("70048", "单一客户台账状态修改失败"),//单一客户台账状态修改失败
    ECL070049("70049", "单一客户授信分项状态修改失败"),//单一客户授信分项状态修改失败
    ECL070050("70050", "更新异常"),//更新异常
    ECL070051("70051", "未获取到授信协议编号"),//未获取到授信协议编号
    ECL070052("70052", "个人授信协议注销操作异常"),//个人授信协议注销操作异常
    ECL070053("70053", "该授信协议项下存在在途的业务申请"),//该授信协议项下存在在途的业务申请
    ECL070054("70054", "该授信协议项下未找到额度分项信息"),//该授信协议项下未找到额度分项信息
    ECL070055("70055", "该个人授信协议项下关联有未结清的贷款合同不能注销"),//该个人授信协议项下关联有未结清的贷款合同不能注销
    ECL070056("70056", "获取当前登录用户为空"),//获取当前登录用户为空
    ECL070057("70057", "未获取到本次冻结解冻金额"),//未获取到本次冻结解冻金额
    ECL070058("70058", "授信冻结金额的计算异常"),//授信冻结金额的计算异常
    ECL070059("70059", "获取额度协议接口参数异常"),//获取额度协议接口参数异常
    ECL070060("70060", "该额度台账数据不存在"),//该额度台账数据不存在
    ECL070061("70061", "获取该额度台账数据异常"),//获取该额度台账数据异常
    ECL070062("70062", "保存额度协议和额度台账数据失败未获取到保存的参数信息"),//保存额度协议和额度台账数据失败未获取到保存的参数信息
    ECL070063("70063", "转化额度协议数据异常"),//转化额度协议数据异常
    ECL070064("70064", "保存额度协议失败"),//保存额度协议失败
    ECL070065("70065", "转化额度台账数据异常"),//转化额度台账数据异常
    ECL070066("70066", "转化额度台账数据信息为空"),//转化额度台账数据信息为空
    ECL070067("70067", "保存额度台账数据失败"),//保存额度台账数据失败
    ECL070068("70068", "更新额度协议数据失败"),//更新额度协议数据失败
    ECL070069("70069", "更新额度台账数据失败"),//更新额度台账数据失败
    ECL070070("70070", "生成额度协议以及额度台账异常异常原因：存在多条授信协议数据"),//生成额度协议以及额度台账异常异常原因：存在多条授信协议数据
    ECL070071("70071", "本次台账占用总额之和超过分项可出账金额"),
    ECL070072("70072", "生成新批复台账，未找到原批复台账信息"),
    ECL070073("70073", "最高额授信协议，只允许占用到法人客户额度额度分项层级，不允许占用至产品分项层级"),
    ECL070075("70075", "com001接口comstar同步失败"),
    ECL070076("70076", "该授信申请下暂无授信产品"),
    ECL070077("70077", "未找到同业授信批复信息"),
    ECL070078("70078", "客户批复累加授信总额+授信总额部分，不允许超过客户上年度销售总额和当年销售总额较大值（纳税申报表）的"),

    ECL070079("70079", "占用金额总和超过可用限额"),
    ECL070080("70080", "调用【cmiscus0005】接口失败"),
    ECL070081("70081", "最高协议校验，未找到占用分项信息"),
    ECL070082("70082", "未查找到额度分项产品信息！"),
    ECL070083("70083", "未找到用信产品对应额度信息！"),
    ECL070084("70084", "查询类型为批复编号，批复编号字段不可以为空"),
    ECL070085("70085", "查询类型为分项编号，分项编号字段不可以为空"),
    ECL070086("70086", "授信品种类型属性不相等"),
    ECL070087("70087", "不支持该交易业务恢复类型"),
    ECL070088("70088", "客户号不允许为空"),
    ECL070089("70089", "台账占用明细中的客户号与合同中不匹配"),

    ECL070090("70090", "未查到该客户额度信息"),
    ECL070091("70091", "该客户属于集团客户，请到集团额度视图中查询"),
    ECL070092("70092", "该额度结构编号已存在"),
    ECL070093("70093", "初始化额度调账变更信息"),
    ECL070094("70094", "承兑行白名单管理批量导入失败"),
    ECL070095("70095", "承兑行白名单管理批量导出失败"),
    ECL070096("70096", "合同下不存在担保合同"),
    ECL070097("70097", "票据明细批量导入失败"),
    ECL070098("70098", "票据明细批量导出失败"),
    ECL070099("70099", "贴现汇票明细批量导入失败"),
    ECL070100("70100", "贴现汇票明细批量导出失败"),
    ECL070101("70101", "未查到分项占用关系记录"),
    ECL070102("70102", "占用金额超过可用额度"),
    ECL070103("70103", "未找到同业额度分项信息"),
    ECL070104("70104", "额度分项到期日为空"),
    ECL070105("70105", "调cmislmt0001接口失败"),
    ECL070106("70106", "提前终止的额度数据不允许为空"),
    ECL070107("70107", "根据合作方额度编号未找到合作方额度信息"),
    ECL070108("70108", "单笔业务限额为空，不进行单笔业务限额校验"),
    ECL070109("70109", "单户限额为空，不进行单户限额校验"),
    ECL070110("70110", "提前终止的批复台账为空，请检查"),
    ECL070111("70111", "客户不存在有效的综合授信额度，不允许同步单笔单批的低风险额度"),
    ECL070112("70112", "该客户存在授信批复信息"),
    ECL070113("70113", "未查到批复分项信息"),
    ECL070114("70114", "未查到产品分项信息"),
    ECN060021("60021", "底层资产明细批量导入失败"),
    ECL070116("70116", "授信续作时，根据原批复流水号未查询到原批复信息，原批复流水号"),
    ECL070115("70115", "批复台账编号不允许为空"),
    ECL070117("70117", "授信续作时，原批复流水号不允许为空，申请流水号"),
    ECL070118("70118", "覆盖原批复台账时，原批复台账不允许为空"),
    ECL070119("70119", "非低风险额度，不允许缴纳全额保证"),
    ECL070120("70120", "低风险占用，占用敞口占用金额不允许大于0"),
    ECL070121("70121", "未找到原台账占用信息"),
    ECL070122("70122", "请求报文中，占用明细列表不允许为空"),
    ECL070123("70123", "请求报文中，原台账编号不允许为空"),
    ECL070124("70124", "未查询到需恢复的台账信息"),
    ECL070125("70125", "该交易已经成功，请不要重复占用"),
    ECL070126("70126", "额度分项编号不允许为空"),
    ECL070127("70127", "放款日期超出合作方的有效期，不允许通过"),
    ECL070128("70128", "授信分项起始日为空"),
    ECL070129("70129", "提前终止的授信类型不允许为空"),
    ECL070130("70130", "金融机构代码不允许为空"),
    ECL070131("70131", "分项类型不允许为空"),
    ECL070132("70132", "占用额度列表不允许为空"),
    ECL070133("70133", "不允许为空"),
    ECL070134("70134", "合同项下存在未结清的业务，不允许注销"),
    ECL070135("70135", "合同编号不允许为空"),
    ECL070136("70136", "客户不存在有效的单一客户授信"),
    ECL070137("70137", "恢复担保公司额度，查询主合同信息，查出多条记录"),
    ECL070138("70138", "非全行适用，适用机构不可以为空"),
    ECL070139("70139", "合作方不适用于该机构，不允许占用"),
    ECL070140("70140", "未找到额度品种信息"),
    ECL070141("70141", "未找到贴现恢复占用信息"),
    ECL070142("70142", "客户已存在有效的综合授信额度"),
    ECL070143("70143", "分项下未找到明细信息"),
    ECL070144("70144", "明细未找到分项信息"),
    ECL070145("70145", "承兑行白名单额度已到期"),
    ECL070146("70146", "承兑行白名单额度已到期"),

    // 授信台账异常枚举类
    E_LMT_ACC_NOT_EXISTS_FAILED("LAE000001", "授信台账不存在！"),
    E_LMT_ACC_CAL_AMT_FAILED("LAE000002", "计算可用额度失败！"),
    E_LMT_ACC_NOT_FROZEAMT_FAILED("LAE000001", "额度台账项下冻结金额为空！！"),

    E_TH_LMT_ACC_PARAMS_FAILED("TLAE000001", "请求入参数据不存在！"),
    E_TH_LMT_ACC_QUERY_FAILED("TLAE000002", "查询第三方额度信息不存在！"),

    E_LMTQUERY_PARAMS_EXCEPTION("LMTQUERY00001", "未获取到需要查询的额度台账/第三方额度台账入参信息！"),
    E_LMTQUERY_LMT_CTR_NOT_EXISTS_FAILED("LMTQUERY00002", "授信协议不存在！"),

    E_LMT_ACC_EXISTS("LMTQUERY00003", "新增数据失败！该数据已存在"),
    E_LMT_ACC_GERUSER_FAILED("LMTQUERY00004", "获取当前登录用户信息异常"),
    E_LMT_ACC_INSERT_FAILED("IQPIE0004", "新增数据异常！"),


    E_LMTACCLISTQUERY_PARAMS_EXCEPTION("LALQ000001", "获取额度协议及额度台账集合-参数异常！"),
    E_LMTACCLISTQUERY_LMTCTRNULL_EXCEPTION("LALQ000002", "获取额度协议及额度台账集合-额度协议不存在！"),
    E_LMTACCLISTQUERY_LMTACCLISTNULL_EXCEPTION("LALQ000003", "获取额度协议及额度台账集合-额度台账列表为空！"),


    //getLmtAccNosByLmtCtrNo 异常定义
    E_GETLMTNOS_PARAMS_EXCEPTION("GPE000001", "未获取到额度协议编号！"),

    // 计算授信余额 异常定义
    E_GETLMTBAL_EXCEPTION_1("GLB000001", "计算授信余额异常：该额度编号项下额度类型为空，请确认数据完整性！"),
    E_GETLMTBAL_EXCEPTION_2("GLB000001", "计算授信余额异常：该额度编号项下启用额度为空，请确认数据完整性！"),

    //
    LMT_APPR_DEF_SUCCESS("ECL070000", "成功"),
    LMT_APPR_DEF_EXCEPTION("ECL079999", "异常"),
    E_LMT_APPR_NOT_EXISTS_FAILED("ECL0700001", "额度批复不存在！"),
    E_TH_LMT_APPR_PARAMS_FAILED("ECL070002", "请求入参数据不存在！"),
    AFTEREND_APPRLMTCHGAPP_EXCEPTION("ECL010001", "单一客户额度调整失败，批复主信息不存在！"),
    AFTEREND_APPRLMTINFO_EXCEPTION("ECL010002", "单一客户额度调整失败，额度调整申请主信息不存在！"),
    AFTEREND_APPRLMTSUBBASICINFOCHG_EXCEPTION("ECL010003", "单一客户额度调整失败，批复分项信息不存在！"),
    AFTEREND_APPRLMTLMTCONTRELCHG_EXCEPTION("ECL010003", "单一客户额度调整失败，占用合同信息不存在！"),
    E_LMT_COOP_UPDATE_STATUS_FAILED("ECL010004", "合作方台账状态修改失败"),
    E_LMT_COOP_SUB_UPDATE_STATUS_FAILED("ECL010005", "合作方授信分项状态修改失败"),
    E_LMT_STR_MTABLE_UPDATE_STATUS_FAILED("ECL010006", "单一客户台账状态修改失败"),
    E_LMT_SUB_BASIC__UPDATE_STATUS_FAILED("ECL010007", "单一客户授信分项状态修改失败"),

    //定义默认的成功以及异常返回信息
    LMT_CTR_DEF_SUCCESS("000000", "成功"),
    LMT_CTR_DEF_EXCEPTION("999999", "异常"),


    LMT_UPDATE_EXCEPTION("UPDATE000001", "更新异常"),

    // 异常定义
    E_LMT_CTR_CANCEL_EXCEPTION1("CANCEL000001", "个人授信协议注销操作异常！"),
    E_LMT_CTR_CANCEL_EXCEPTION2("CANCEL000002", "该授信协议项下存在在途的业务申请！"),
    E_LMT_CTR_CANCEL_EXCEPTION3("CANCEL000003", "该授信协议项下未找到额度分项信息！"),
    E_LMT_CTR_CANCEL_EXCEPTION4("CANCEL000004", "该个人授信协议项下关联有未结清的贷款合同不能注销！"),
    E_LMT_CTR_CANCEL_EXCEPTION5("CANCEL000004", "获取当前登录用户为空！"),

    E_LMT_FR_UFR_APP_FROZENAMT_EXCEPTION("FROZE000001", "未获取到本次冻结解冻金额！"),
    E_LMT_FR_UFR_APP_LMTFROZENAMT_EXCEPTION("FROZE000002", "授信冻结金额的计算异常！"),

    //获取额度协议数据异常定义
    LMTCTRLIST_CLIENT_PARAMS_EXCEPTION("LCLC000001", "获取额度协议接口参数异常！"),
    LMTCTR_CLIENT_PARAMS_EXCEPTION("LCLC000002", "该额度台账数据不存在"),
    LMTCTR_LMTBEGINAMT_EXCEPTION("FROZE000002", "获取该额度台账数据异常！"),

    //保存额度协议和额度台账数据异常定义
    LMTCTR_HANDLE_PARAMS_EXCEPTION("LCH000001", "保存额度协议和额度台账数据失败！未获取到保存的参数信息！"),
    LMTCTR_HANDLE_LMTCTRTRANS_EXCEPTION("LCH000002", "转化额度协议数据异常！"),
    LMTCTR_HANDLE_LMTCTRSAVE_EXCEPTION("LCH000003", "保存额度协议失败！"),
    LMTCTR_HANDLE_LMTACCTRANS_EXCEPTION("LCH000004", "转化额度台账数据异常！"),
    LMTCTR_HANDLE_LMTACCTRANSNULL_EXCEPTION("LCH000005", "转化额度台账数据信息为空！"),
    LMTCTR_HANDLE_LMTACCSAVE_EXCEPTION("LCH000006", "保存额度台账数据失败！"),
    LMTCTR_HANDLE_LMTCTRUPDATE_EXCEPTION("LCH000007", "更新额度协议数据失败！"),
    LMTCTR_HANDLE_LMTACCUPDATE_EXCEPTION("LCH000008", "更新额度台账数据失败！"),
    LMTCTR_HANDLE_LMTCTRMORE_EXCEPTION("LCH000009", "生成额度协议以及额度台账异常！异常原因：存在多条授信协议数据"),

    //主体授信相关异常
    LMT_SIG_INVESTAPP_NO_EXIST("INVEST000001","授信主表数据查询失败！"),
    LMT_SIG_INVESTAPP_INSERT_FAILD("INVEST000002","授信主表数据新增失败！"),
    LMT_SIG_INVESTAPP_UPDATE_FAILD("INVEST000003","授信主表数据更新失败！"),
    LMT_SIG_INVESTAPP_FUYI_FAILD("INVEST000004","授信复议操作失败！"),
    LMT_SIG_INVESTAPP_FUYI("INVEST000005","授信复议不能超过"),
    LMT_SIG_INVESTAPP_REAPPLY_TODO("INVEST000006","客户当前业务类型存在正在流转中的(复议、续作、变更）申请，无法发起新的申请！"),
    LMT_SIG_INVESTAPP_ASSURE_FAILED("INVEST000007","担保及增信情况添加失败！"),
    LMT_SIG_INVESTAPP_CUSINTBAK_FAILED("INVEST000008","客户信息添加失败！"),
    LMT_SIG_INVESTAPP_CORRE_SHD_DEL_FAILED("INVEST000009","股东删除失败！"),
    LMT_SIG_INVESTAPP_XUZUO_FAILED("INVEST000010","授信续作添加失败！"),
    LMT_CALCULATE_PARAMS_ERROR("INVEST000010","参数传递错误，计算失败！"),
    LMT_CALCULATE_DANBAOREN_ERROR_1("INVEST000011","担保人/增信人不能选择授信关联客户，请重新选择！"),
    LMT_CALCULATE_DANBAOREN_ERROR_2("INVEST000012","该客户已存在，请重新选择！"),
    LMT_INVEST_CHG_LMTAMT_FAILED("INVEST000013","授信金额不能大于原授信金额！请修改"),
    LMT_INVEST_CHG_LMTTERM_FAILED("INVEST000014","授信期限不能大于原授信期限！请修改"),
    LMT_INVEST_CHG_FIALED("INVEST000015","授信变更保存失败！"),
    LMT_INVEST_TONGYEINFO_GET_FAILED("INVEST000016","同业客户信息获取失败！"),
    LMT_INVEST_TONGYEINFO_SAVE_FAILED("INVEST000021","同业客户信息保存失败！"),
    LMT_INVEST_RELCUSINFO_GET_FAILED("INVEST000017","客户额度相关信息添加失败！"),
    LMT_SIG_INVESTAPP_FUYI_ORIGIINFO_GET_FAILD("INVEST000018","原批复（审批）数据获取失败！"),
    LMT_SIG_INVEST_MAINBUSS_UP_3("INVEST000019","前三大主营业务情况不能超过三条"),
    LMT_SIG_INVEST_MAINBUSS_SUM_MOVE_100("INVEST000020","前三大主营业务情况总占比不能超过100%"),
    LMT_SIG_INVESTAPP_ASSETNO_SAVE_FAILED("INVEST000021","资产编号保存失败！"),
    LMT_SIG_INVESTAPP_EORROR000007("INVEST000007","已存在新增记录，只能续作！"),
    LMT_SIG_INVESTAPP_EORROR000022("INVEST000022","不能发起复议！"),
    LMT_SIG_INVESTAPP_EORROR000023("INVEST000023","发起复议不能超过"),
    LMT_SIG_INVESTAPP_EORROR000024("INVEST000024","客户信息获取失败！"),
    LMT_SIG_INVESTAPP_EORROR000025("INVEST000025","客户信息同业机构类型未设置！"),
    LMT_SIG_INVESTAPP_EORROR000026("INVEST000025","客户信息注册地行政区划未设置！"),
    LMT_SIG_INVESTAPP_EORROR000027("INVEST000025","已存在在途的批复变更申请！请勿重复申请"),
    LMT_SIG_INVESTAPP_EORROR000028("INVEST000028","批复台账信息获取失败！"),
    LMT_SIG_INVESTAPP_EORROR000029("INVEST000029","pkId、serno获取失败！"),
    LMT_SIG_INVESTAPP_EORROR000030("INVEST000030","该资产编号【XXX1】已存在对应项目编号【XXX2】、项目名称为【XXX3】的授信，请核实！"),
    LMT_SIG_INVESTAPP_EORROR000031("INVEST000031","该资产编号【XXX1】已存在对应项目编号【XXX2】、项目名称为【XXX3】的授信正在申请中，请核实！"),

    //同业机构准入
    INTBANK_ORG_APP_ADJUST_EXIST_RUN("INTBANK000001","该记录存在正在流转的调整申请，无法发起新的申请！"),
    INTBANK_ORG_APP_ADJUST_FAILED("INTBANK000002","准入名单调整保存失败！"),
    INTBANK_ORG_APP_WORKFLOW_FAILED("INTBANK000003","同业机构准入流程监听操作处理失败！"),
    INTBANK_ORG_APP_REPLY_FAILED("INTBANK000004","同业机构准入批复信息保存失败！"),
    INTBANK_ORG_APP_ACC_FAILED("INTBANK000005","同业机构准入台账信息保存失败！"),
    INTBANK_ORG_APP_APPR_FAILED("INTBANK000006","同业机构准入审批信息保存失败！"),
    INTBANK_ERROR_000005("INTBANK0000045","未找到该记录，删除失败！"),
    INTBANK_ERROR_000006("INTBANK0000046","审批记录获取失败！"),
    INTBANK_ERROR_000007("INTBANK0000047","已存在该客户的准入申请记录，请勿重复申请！"),
    INTBANK_ERROR_000008("INTBANK0000048","客户已存在在途的同业机构准入申请，请勿重复申请！"),
    INTBANK_ERROR_000009("INTBANK0000049","客户已存在在途的同业机构年审申请，请勿重复申请！"),
    INTBANK_ERROR_000010("INTBANK0000050","客户已存在在途的同业机构调整申请，请勿重复申请！"),
    INTBANK_ERROR_000011("INTBANK0000051","客户已存在在途的申请，请勿重复申请！"),
    INTBANK_ERROR_000012("INTBANK0000052","客户已存在生效的机构准入名单，无需重复添加！"),
    INTBANK_ERROR_000013("INTBANK0000053","客户不存在生效的机构准入名单，不允许调整！"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EclEnum enumData : EnumSet.allOf(EclEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EclEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EclEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
