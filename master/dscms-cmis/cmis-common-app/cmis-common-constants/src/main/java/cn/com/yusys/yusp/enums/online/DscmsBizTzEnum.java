package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-信贷台账的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizTzEnum {
    /*** xdtz0058 中QUERY_TYPE 开始 **/
    QUERY_TYPE_LOANAMOUNTFORMACCLOAN("queryLoanAmountFormAccLoan", "根据借据号查询信贷房贷借据金额"),
    QUERY_TYPE_LOANMONTHFORMACCLOAN("queryLoanMonthFormAccLoan", "根据借据号查询信贷房贷贷款期限"),
    QUERY_TYPE_FINABRIDFROMACCLOAN("queryFinaBrIdFromAccLoan", "根据借据号取信贷账务机构"),
    QUERY_TYPE_OVERTIMESTOTALFROMACCLOAN("queryOverTimesTotalFromAccLoan", "根据借据号查询本行其他贷款拖欠情况"),
    /*** xdtz0058中QUERY_TYPE 结束 **/
    /**
     * 是否相关枚举 开始
     **/
    YESNO_YES("Y", "是否"),//是否-是
    YESNO_NO("N", "是否"),//是否-否

    /**
     * 删除成功失败枚举 开始
     **/
    DELLEET_SUCCESS("0000", "删除成功"),//是否-否
    DELLEET_FAIL("9999", "删除失败"),//是否-是


    /**
     * 成功失败标志枚举 开始
     **/
    SUCCSEE("S", "成功"),//成功
    FAIL("F", "失败"),//失败
    FAIL_MESSAGE("FAIL", "未查询到记录"),//未查询到记录
    FAIL_MESSAGE_1("FAIL", "台账状态不为正常状态,不能结清"),
    FAIL_MESSAGE_2("FAIL", "台账状态不为当日结清,不能撤销结清"),
    FAIL_MESSAGE_3("FAIL", "台账状态为结清,不能出账撤销"),

    /**
     * XDTZ0013
     * 经营消费类贷款相关枚举 开始
     **/
    PRDTYPE_08("08", "经营类贷款"),//经营类贷款
    PRDTYPE_09("09", "消费类贷款"),//消费类贷款
    PRDTYPE_01("01", "经营类贷款"),//经营类贷款
    PRDTYPE_02("02", "消费类贷款"),//消费类贷款


    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizTzEnum enumData : EnumSet.allOf(DscmsBizTzEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizTzEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizTzEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
