package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-合同管理的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizHtEnum {
    /* XDHT0019 合同生成	 枚举 start*/
    XDHT0019_LOANTYPE_1("1", "小微贷？"),//小微
    XDHT0019_LOANTYPE_2("2", "PW030003"),//PW030003工薪贷
    XDHT0019_LOANTYPE_3("3", "022033"),//房易贷？022033公积金贷???  公易贷
    XDHT0019_LOANTYPE_4("4", "SC010008"),//SC010008优企贷（线上）
    XDHT0019_LOANTYPE_5("5", "SC020010"),//SC020010优农贷（线上）
    XDHT0019_LOANTYPE_6("6", "PW010004"),//PW010004优享贷
    XDHT0019_LOANTYPE_7("7", "SC010014"),//SC010014增享贷
    /* XDHT0019 合同生成 枚举 end  */
    /*** XDHT0032中QUERY_TYPE 开始 **/
    QUERY_TYPE_APPLYAMOUNT("queryApplyAmount", "根据核心客户号查询我行信用类合同金额"),
    QUERY_TYPE_LASTYQDCONTAMT("queryLastYqdContAmt", "查询最近生效的一笔优企贷合同金额"),
    /*** XDHT0032中QUERY_TYPE 结束 **/
    /*** XDHT0033中QUERY_TYPE 开始 **/
    QUERY_TYPE_SURVEYCONTLOANAMT("querySurveyContLoanAmt", "根据核心客户号查询小贷是否具有客户调查合同并返回合同金额"),
    QUERY_TYPE_OPERATINGCONTAMT("queryOperatingContAmt", "根据核心客户号查询经营性贷款关联生效合同的批复额度"),
    QUERY_TYPE_OPERATINGCONTPFAMT("queryOperatingContPfAmt", "根据核心客户号查询经营性贷款关联生效合同的批复额度"),
    /*** XDHT0033中QUERY_TYPE 结束 **/
    /*** XDHT0034中QUERY_TYPE 开始 **/
    QUERY_TYPE_LASTCONTAMT("queryLastContAmt", "根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额"),
    QUERY_TYPE_LASTYNDCONTAMT("queryLastYndContAmt", "根据核心客户号查询最近生效的一笔优农贷合同金额"),
    /*** XDHT0034中QUERY_TYPE 结束 **/
    /*** XDHT0044中枚举 开始 **/
    /*** 查询类型 ***/
    QUERY_TYPE_LOANHOUSEFROMCMIS("queryLoanHouseFromCmis", "根据证件号码查询客户是否是房群客户"),
    QUERY_TYPE_NORMALLOANHOUSEFROMCMIS("queryNormalLoanHouseFromCmis", "根据证件号码查询客户是否是房群客户(排除已结清)"),
    /*** 是否是房群客户 ***/
    ISHOUSECUS_Y("Y", "是"),
    ISHOUSECUS_N("N", "否"),
    /*** XDHT0044中枚举 结束 **/
    FALG_SUCCESS("S", "成功"),//是否-是
    FLAG_FAILD("F", "失败"),//是否-否

    /**
     * 查询类型相关枚举 开始
     **/
    QUERY_TYPE_01("01", "个人"),//个人
    QUERY_TYPE_02("02", "公司"),//公司

    /**
     * 优企贷优农贷查询类型枚举 开始
     **/
    YQD_TYPE_01("01", "优企贷"),//个人
    YQD_TYPE_02("02", "优农贷"),//公司

    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizHtEnum enumData : EnumSet.allOf(DscmsBizHtEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizHtEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizHtEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
