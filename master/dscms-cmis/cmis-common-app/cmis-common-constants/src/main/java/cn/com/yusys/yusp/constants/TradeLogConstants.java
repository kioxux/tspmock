package cn.com.yusys.yusp.constants;

/**
 * 交易日志常量类
 *
 * @author leehuang
 * @date 2021/04/26 09:50
 */
public class TradeLogConstants {
    public static final String BSP_BEGIN_PREFIX_LOGGER = "BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]";
    public static final String BSP_EXCEPTION_PREFIX_LOGGER = "BSP中处理[{}|{}]逻辑结束,异常信息为:[{}]";
    public static final String BSP_END_PREFIX_LOGGER = "BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]";

    public static final String RESOURCE_BEGIN_PREFIX_LOGGER = "处理[{}|{}]的Resource逻辑开始,请求参数为:[{}]";
    public static final String RESOURCE_EXCEPTION_PREFIX_LOGGER = "处理[{}|{}]的Resource逻辑结束,异常信息为:[{}]";
    public static final String RESOURCE_END_PREFIX_LOGGER = "处理[{}|{}]的Resource逻辑结束,响应参数为:[{}]";

    public static final String SERVICE_BEGIN_PREFIX_LOGGER = "处理[{}|{}]的Service逻辑开始";
    public static final String SERVICE_EXCEPTION_PREFIX_LOGGER = "处理[{}|{}]的Service逻辑结束,异常信息为:[{}]";
    public static final String SERVICE_BIZ_EXCEPTION_PREFIX_LOGGER = "处理[{}|{}]的Service逻辑,业务异常信息为:[{}]";
    public static final String SERVICE_END_PREFIX_LOGGER = "处理[{}|{}]的Service逻辑结束";

    public static final String SERVICE_VALIDATE_SET_BEGIN_PREFIX_LOGGER = "校验并赋值[{}|{}]的对象开始,校验前参数为:[{}]";
    public static final String SERVICE_VALIDATE_SET_END_PREFIX_LOGGER = "校验并赋值[{}|{}]的对象结束,校验后参数为:[{}]";

    public static final String CALL_FEIGN_BEGIN_PREFIX_LOGGER = "调用[{}|{}]的Feign接口逻辑开始,请求参数为:[{}]";
    public static final String CALL_FEIGN_END_PREFIX_LOGGER = "调用[{}|{}]的Feign接口逻辑结束,响应参数为:[{}]";

    public static final String CALL_SERVICE_BEGIN_PREFIX_LOGGER = "调用[{}|{}]的Service逻辑开始,请求参数为:[{}]";
    public static final String CALL_SERVICE_END_PREFIX_LOGGER = "调用[{}|{}]的Service逻辑结束,响应参数为:[{}]";

    public static final String CALL_ESB_BEGIN_PREFIX_LOGGER = "调用[{}|{}]的ESB逻辑开始";
    public static final String CALL_ESB_END_PREFIX_LOGGER = "调用[{}|{}]的ESB逻辑结束";

    public static final String CALL_HTTP_BEGIN_PREFIX_LOGGER = "调用[{}|{}]的HTTP逻辑开始,请求参数为:[{}]";
    public static final String CALL_HTTP_END_PREFIX_LOGGER = "调用[{}|{}]的HTTP逻辑结束,响应参数为:[{}]";

    public static final String CALL_SEQUENCE_BEGIN_PREFIX_LOGGER = "根据模板名称[{}|{}]获取下一个序列号开始";
    public static final String CALL_SEQUENCE_END_PREFIX_LOGGER = "根据模板名称[{}|{}]获取下一个序列号值为:[{}]";

    public static final String IDEMPOTENT_EXCEPTION_BEGIN_PREFIX_LOGGER = "幂等性组件异常处理类逻辑开始,异常信息为:[{}]";
    public static final String IDEMPOTENT_EXCEPTION_END_PREFIX_LOGGER = "幂等性组件异常处理类逻辑结束,响应参数为:[{}]";

    public static final String GXP_BEGIN_PREFIX_LOGGER = "BSP中处理GXP对象[{}|{}]逻辑开始";
    public static final String GXP_END_PREFIX_LOGGER = "BSP中处理GXP对象[{}|{}]逻辑结束,响应参数为:[{}]";

    public static final String CALL_GXP_BEGIN_PREFIX_LOGGER = "调用[{}|{}]的GXP逻辑开始";
    public static final String CALL_GXP_END_PREFIX_LOGGER = "调用[{}|{}]的GXP逻辑结束";

    public static final String BATCH_JOB_BEGIN_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Job逻辑开始,开始时间为:[{}]";
    public static final String BATCH_JOB_EXCEPTION_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Job逻辑结束,异常信息为:[{}]";
    public static final String BATCH_JOB_END_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Job逻辑结束,结束时间为:[{}]";


    public static final String BATCH_STEP_BEGIN_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Step逻辑开始,开始时间为:[{}]";
    public static final String BATCH_STEP_INFO_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Step逻辑,[{}]";
    public static final String BATCH_STEP_RESULT_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Step逻辑,影响条数为:[{}]";
    public static final String BATCH_STEP_EXCEPTION_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Step逻辑,异常信息为:[{}]";
    public static final String BATCH_STEP_END_PREFIX_LOGGER = "处理批量任务中[{}|{}]的Step逻辑结束,结束时间为:[{}]";

    public static final String BATCH_TIMED_TASK_BEGIN_PREFIX_LOGGER = "处理批量定时任务中[{}|{}]的逻辑开始,开始时间为:[{}]";
    public static final String BATCH_TIMED_TASK_INFO_PREFIX_LOGGER = "处理批量定时任务中[{}|{}]的逻辑,[{}]";
    public static final String BATCH_TIMED_TASK_RESULT_PREFIX_LOGGER = "处理批量定时任务中[{}|{}]的逻辑,影响条数为:[{}]";
    public static final String BATCH_TIMED_TASK_EXCEPTION_PREFIX_LOGGER = "处理批量定时任务中[{}|{}]的逻辑,异常信息为:[{}]";
    public static final String BATCH_TIMED_TASK_END_PREFIX_LOGGER = "处理批量定时任务中[{}|{}]的逻辑结束,结束时间为:[{}]";

}
