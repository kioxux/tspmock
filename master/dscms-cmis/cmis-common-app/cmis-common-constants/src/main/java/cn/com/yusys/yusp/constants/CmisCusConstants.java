package cn.com.yusys.yusp.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 常量类:流程作业服务
 */
public class CmisCusConstants {
    //押品信息同步接口XDDB0003 担保人证件类型转换为信贷证件类型的map
    public static Map<String, String> certTypeMap = new HashMap<>();

    static {
        certTypeMap.put("10", "A");//身份证
        certTypeMap.put("20", "Q");//组织机构代码
        certTypeMap.put("22", "V");//境外企业代码
        certTypeMap.put("21", "W");//港、澳、台身份证
        certTypeMap.put("11", "C");//户口簿
        certTypeMap.put("12", "B");//护照
        certTypeMap.put("13", "G");//军官证
        certTypeMap.put("14", "H");//士兵证
        certTypeMap.put("15", "D");//港澳居民来往内地通行证
        certTypeMap.put("16", "E");//台湾同胞来往内地通行证
        certTypeMap.put("17", "X");//临时身份证
        certTypeMap.put("18", "S");//外国人居留证
        certTypeMap.put("19", "Y");//警官证
        certTypeMap.put("1X", "T");//其他证件
        certTypeMap.put("23", "T");//回乡证
        certTypeMap.put("24", "M");//营业执照
    }
    //工商返回信息与客户信息 国家字典项映射
    public static Map<String, String> countryTypeMap = new HashMap<>();
    static {
        countryTypeMap.put("124","CAN");//加拿大
        countryTypeMap.put("756","CHE");//瑞士联邦
        countryTypeMap.put("840","USA");//美利坚合众国
        countryTypeMap.put("158","TWN");//中国台湾
        countryTypeMap.put("036","AUS");//澳大利亚联邦
        countryTypeMap.put("344","HKG");//中国香港特别行政区
        countryTypeMap.put("392","JPN");//日本国
        countryTypeMap.put("446","MAC");//中国澳门特别行政区
        countryTypeMap.put("156","CHN");//中华人民共和国
        countryTypeMap.put("752","SWE");//瑞典王国
    }
    //工商返回信息与客户信息 币种字典项映射
    public static Map<String, String> moneyTypeMap = new HashMap<>();
    static {
        moneyTypeMap.put("156","CNY");//人民币元
        moneyTypeMap.put("036","AUD");//澳大利亚元
        moneyTypeMap.put("344","HKD");//香港元
        moneyTypeMap.put("840","USD");//美元
        moneyTypeMap.put("826","GBP");//英镑
        moneyTypeMap.put("752","SEK");//瑞典克朗
        moneyTypeMap.put("756","CHF");//瑞士法郎
        moneyTypeMap.put("392","JPY");//日元
        moneyTypeMap.put("124","CAD");//加元
        moneyTypeMap.put("954","EUR");//欧元
        moneyTypeMap.put("978","EUR");//欧元
    }
    //工商返回信息与客户信息 经营状况字典项映射STD_PRD_TYPE_PROP_COOP
    public static Map<String, String> operTypeMap = new HashMap<>();
    static {
        operTypeMap.put("1","100");//正常经营
    }
    //产品名称映射STD_PRD_TYPE_PROP_COOP
    public static Map<String, String> prdTypePropName = new HashMap<>();
    static {
        prdTypePropName.put("P010","信保贷");
        prdTypePropName.put("P003","科技贷");
        prdTypePropName.put("P021","南通信保通");
        prdTypePropName.put("P020","徐信保");
        prdTypePropName.put("P022","无锡园区保");
        prdTypePropName.put("P023","宿迁园区保");
        prdTypePropName.put("P017","即墨政银");
    }

    //外部信用评级从低到高的字符串
    public static String creditLevelOuterStr = "[000],[D],[C],[CC],[CCC],[B-],[B],[B+],[BB-],[BB],[BB+],[BBB-],[BBB],[BBB+],[A-],[A],[A+],[AA-],[AA],[AA+],[AAA]";

    /**
     * 是
     **/
    public static final String COMMON_FLAG_YES = "1";
    /**
     * 否
     **/
    public static final String COMMON_FLAG_NO = "0";

    /**
     * 待发起
     **/
    public static final String APPLY_STATE_TODO = "000";
    /**
     * 审批中
     **/
    public static final String APPLY_STATE_APP = "111";
    /**
     * 取消
     **/
    public static final String APPLY_STATE_CANCEL = "990";
    /**
     * 追回
     **/
    public static final String APPLY_STATE_TACK_BACK = "991";
    /**
     * 打回
     **/
    public static final String APPLY_STATE_CALL_BACK = "992";
    /**
     * 通过
     **/
    public static final String APPLY_STATE_PASS = "997";
    /**
     * 否决
     **/
    public static final String APPLY_STATE_REFUSE = "998";
    /**
     * 申请类型 01-列入
     **/
    public static final String APPLY_TYPE_BLACK = "01";
    /**
     * 申请类型 02-注销
     **/
    public static final String APPLY_TYPE_LOGOUT = "02";
    // 对公
    public static final String CUS_CATALOG_PUB = "2";
    // 对私
    public static final String CUS_CATALOG_PRI = "1";
    /**
     * 状态 01-登记
     **/
    public static final String STATUS_REGISTER = "01";
    /**
     * 状态 02-生效
     **/
    public static final String STATUS_TAKE_EFFECT = "02";
    /**
     * 状态 03-失效
     **/
    public static final String STATUS_INVALID = "03";
    //客户类型
    /****************一般自然人*********************/
    public static final String CUS_TYPE_110 = "110";
    /****************个体工商户*********************/
    public static final String CUS_TYPE_120 = "120";

    //证件类型
    /******************证件类型 居民身份证**********************/
    public static final String CERT_TYPE_A = "A";

    /******************操作类型 新增**********************/
    public static final String OPR_TYPE_INSERT = "01";

    /******************管理类型 **********************/
    /**
     * 02-管户人员
     **/
    public static final String MGR_TYPE_02 = "02";

    /******************状态 **********************/
    /**
     * A-生效
     **/
    public static final String STATUS_A = "A";


    /**
     * 集团客户状态 1-生效
     **/
    public static final String GROUP_CUS_STATUS_01 = "01";
    /**
     * 集团客户状态 2-失效
     **/
    public static final String GROUP_CUS_STATUS_02 = "02";

    /**
     * 集团关系类型 1-母公司
     **/
    public static final String STD_ZB_GRP_CORRE_TYPE_1 = "1";
    /**
     * 集团关系类型 2-子公司
     **/
    public static final String STD_ZB_GRP_CORRE_TYPE_2 = "2";
    /**
     * 集团关系类型 3-核心成员
     **/
    public static final String STD_ZB_GRP_CORRE_TYPE_3 = "3";
    /**
     * 集团关系类型 4-普通成员
     **/
    public static final String STD_ZB_GRP_CORRE_TYPE_4 = "4";

    /**
     * 变更类型 01-原有不变
     **/
    public static final String STD_CUS_MODIFY_TYPE_01 = "01";
    /**
     * 变更类型 02-本次新增
     **/
    public static final String STD_CUS_MODIFY_TYPE_02 = "02";
    /**
     * 变更类型 03-本次退出
     **/
    public static final String STD_CUS_MODIFY_TYPE_03 = "03";

    /**
     * 集团紧密程度 01-实体集团
     **/
    public static final String STD_ZB_CLOSELY_DEGRE_01 = "01";
    /**
     * 集团紧密程度 02-一般关联
     **/
    public static final String STD_ZB_CLOSELY_DEGRE_02 = "02";

    /**
     * 集团客户状态 01-认定
     **/
    public static final String STD_GROUP_CUS_STATUS_01 = "01";
    /**
     * 集团客户状态 02-变更
     **/
    public static final String STD_GROUP_CUS_STATUS_02 = "02";
    /**
     * 集团客户状态 03-解散
     **/
    public static final String STD_GROUP_CUS_STATUS_03 = "03";

    /******************状态 **********************/
    /**
     * 集团是否生效  1-是
     **/
    public static final String STD_AVAILABLE_IND_1 = "1";
    /**
     * 集团是否生效  0-否
     **/
    public static final String STD_AVAILABLE_IND_0 = "0";

    /******************集团申请类型 **********************/
    /**
     * 集团申请类型  01-集团认定
     **/
    public static final String STD_GRP_APP_TYPE_01 = "01";
    /**
     * 集团申请类型  02-集团变更
     **/
    public static final String STD_GRP_APP_TYPE_02 = "02";
    /**
     * 集团申请类型  03-集团解散
     **/
    public static final String STD_GRP_APP_TYPE_03 = "03";


    /******************客户移交方式 **********************/
    /**
     * 客户移交方式  1-客户移交
     **/
    public static final String STD_ZB_HAND_TYP_1 = "1";
    /**
     * 客户移交方式  2-业务移交
     **/
    public static final String STD_ZB_HAND_TYP_2 = "2";

    /******************个人客户查询接口-查询类型**********************/
    /**
     * 01-按客户号查询；
     * 02-按证件类型和证件号码查询；
     * 03-按客户姓名和证件号查询；
     * 04-证件号
     */
    public static final String INDIV_QUERYTYPE_01 = "01";
    public static final String INDIV_QUERYTYPE_02 = "02";
    public static final String INDIV_QUERYTYPE_03 = "03";
    public static final String INDIV_QUERYTYPE_04 = "04";

    /**
     * 不宜贷款户状态 01-登记
     **/
    public static final String STD_ZB_BLKLS_ST_01 = "01";
    /**
     * 不宜贷款户状态 02-生效
     **/
    public static final String STD_ZB_BLKLS_ST_02 = "02";
    /**
     * 不宜贷款户状态 03-失效
     **/
    public static final String STD_ZB_BLKLS_ST_03 = "03";

    /**
     * 不宜贷款客户来源 11-系统内客户（系统生成）
     **/
    public static final String STD_ZB_BLKLS_SOUR_11 = "11";
    /**
     * 不宜贷款客户来源 12-系统内客户（人工审批）
     **/
    public static final String STD_ZB_BLKLS_SOUR_12 = "12";
    /**
     * 不宜贷款客户来源 20-系统外客户（外部接入）
     **/
    public static final String STD_ZB_BLKLS_SOUR_20 = "20";
    /**
     * 不宜贷款客户来源 21-系统外客户（人工导入）
     **/
    public static final String STD_ZB_BLKLS_SOUR_21 = "21";

    /**
     * 01-按客户号查询；
     * 02-按客户姓名和证件号查询；
     * 03-按贷款卡号（中征码）查询；
     * 04-按组织机构代码查询
     */
    public static final String QUERY_TYPE_01 = "01";

    /**
     * 02-按客户姓名和证件号查询；
     */
    public static final String QUERY_TYPE_02 = "02";

    /**
     * 03-按贷款卡号（中征码）查询；
     */
    public static final String QUERY_TYPE_03 = "03";

    /**
     * cusId
     */
    public static final String QUERY_MAP_CUS_ID = "cusId";

    /**
     * cusName
     */
    public static final String QUERY_MAP_CUS_NAME = "cusName";

    /**
     * certCode
     */
    public static final String QUERY_MAP_CERT_CODE = "certCode";

    /**
     * loanCardId
     */
    public static final String QUERY_MAP_LOAN_CARD_ID = "loanCardId";

    /**
     * cmonNo
     */
    public static final String QUERY_MAP_CMON_NO = "cmonNo";

    /**
     * 任务状态
     * 1:待处理
     * 2：处理中
     * 3：已处理
     */
    public static final String STD_TASK_STATUS_1 = "1";
    public static final String STD_TASK_STATUS_2 = "2";
    public static final String STD_TASK_STATUS_3 = "3";

    /**
     * 客户状态
     * 2：生效
     * 1：暂存
     */
    public static final String STD_CUS_STATE_1 = "1";
    public static final String STD_CUS_STATE_2 = "2";

    /**
     * 操作成功标志位 S 成功
     */
    public static final String OP_FLAG_S = "S";
    /**
     * 操作成功标志位 F 失败
     */
    public static final String OP_FLAG_F = "F";
    /**
     * 描述信息 操作成功
     */
    public static final String OP_MSG_S = "操作成功！";
    /**
     * 描述信息 操作失败
     */
    public static final String OP_MSG_F = "操作失败！";

    /**
     * 证件类型字典 10 身份证
     */
    public static final String STD_ZB_CERT_TYP_10 = "10";
    /**
     * 证件类型字典 11	户口簿
     */
    public static final String STD_ZB_CERT_TYP_11 = "11";
    /**
     * 证件类型字典 12	护照
     */
    public static final String STD_ZB_CERT_TYP_12 = "12";
    /**
     * 证件类型字典 13	军官证
     */
    public static final String STD_ZB_CERT_TYP_13 = "13";
    /**
     * 证件类型字典 14	士兵证
     */
    public static final String STD_ZB_CERT_TYP_14 = "14";
    /**
     * 证件类型字典 15	港澳居民来往内地通行证
     */
    public static final String STD_ZB_CERT_TYP_15 = "15";
    /**
     * 证件类型字典 16	台湾同胞来往内地通行证
     */
    public static final String STD_ZB_CERT_TYP_16 = "16";
    /**
     * 证件类型字典 18	外国人居留证
     */
    public static final String STD_ZB_CERT_TYP_18 = "18";
    /**
     * 证件类型字典 20	组织机构代码
     */
    public static final String STD_ZB_CERT_TYP_20 = "20";
    /**
     * 证件类型字典 22	境外企业代码
     */
    public static final String STD_ZB_CERT_TYP_22 = "22";
    /**
     * 证件类型字典 23	登记证书
     */
    public static final String STD_ZB_CERT_TYP_23 = "23";
    /**
     * 证件类型字典 24	营业执照
     */
    public static final String STD_ZB_CERT_TYP_24 = "24";
    /**
     * 证件类型字典 25	统一社会信用代码
     */
    public static final String STD_ZB_CERT_TYP_25 = "25";
    /**
     * 证件类型字典 26	批文
     */
    public static final String STD_ZB_CERT_TYP_26 = "25";
    /**
     * 证件类型字典 27	对公其他
     */
    public static final String STD_ZB_CERT_TYP_27 = "27";
    /**
     * 证件类型字典 28	对私其他
     */
    public static final String STD_ZB_CERT_TYP_28 = "28";
    /**
     * 证件类型字典 29	港澳台居民居住证
     */
    public static final String STD_ZB_CERT_TYP_29 = "29";

    /**
     * ECIF 证件类型字典映射 START
     */
    public static final String STD_ECIF_CERT_TYP_A = "A";
    public static final String STD_ECIF_CERT_TYP_B = "B";
    public static final String STD_ECIF_CERT_TYP_C = "C";
    public static final String STD_ECIF_CERT_TYP_D = "D";
    public static final String STD_ECIF_CERT_TYP_E = "E";
    public static final String STD_ECIF_CERT_TYP_F = "F";
    public static final String STD_ECIF_CERT_TYP_G = "G";
    public static final String STD_ECIF_CERT_TYP_H = "H";
    public static final String STD_ECIF_CERT_TYP_I = "I";
    public static final String STD_ECIF_CERT_TYP_J = "J";
    public static final String STD_ECIF_CERT_TYP_K = "K";
    public static final String STD_ECIF_CERT_TYP_L = "L";
    public static final String STD_ECIF_CERT_TYP_M = "M";
    public static final String STD_ECIF_CERT_TYP_N = "N";
    public static final String STD_ECIF_CERT_TYP_O = "O";
    public static final String STD_ECIF_CERT_TYP_P = "P";
    public static final String STD_ECIF_CERT_TYP_Q = "Q";
    public static final String STD_ECIF_CERT_TYP_R = "R";
    public static final String STD_ECIF_CERT_TYP_S = "S";
    public static final String STD_ECIF_CERT_TYP_T = "T";
    public static final String STD_ECIF_CERT_TYP_U = "U";
    public static final String STD_ECIF_CERT_TYP_V = "V";
    public static final String STD_ECIF_CERT_TYP_W = "W";
    /** ECIF 证件类型字典映射 END */

    /**
     * ECIF 是否字典 START
     */
    public static final String STD_ECIF_YES = "1";
    public static final String STD_ECIF_NO = "0";
    /**ECIF 是否字典 END */

    /**
     * 学历字典项 00	博士研究生
     */
    public static final String STD_ZB_EDU_00 = "00";
    /**
     * 学历字典项 10	研究生
     */
    public static final String STD_ZB_EDU_10 = "10";
    /**
     * 学历字典项 20	大学本科（简称"大学"）
     */
    public static final String STD_ZB_EDU_20 = "20";
    /**
     * 学历字典项 30	大学专科和专科学校（简称"大专"）
     */
    public static final String STD_ZB_EDU_30 = "30";
    /**
     * 学历字典项 40	中等专业学校或中等技术学校
     */
    public static final String STD_ZB_EDU_40 = "40";
    /**
     * 学历字典项 50	技术学校
     */
    public static final String STD_ZB_EDU_50 = "50";
    /**
     * 学历字典项 60	高中
     */
    public static final String STD_ZB_EDU_60 = "60";
    /**
     * 学历字典项 70	初中
     */
    public static final String STD_ZB_EDU_70 = "70";
    /**
     * 学历字典项 80	小学
     */
    public static final String STD_ZB_EDU_80 = "80";
    /**
     * 学历字典项 90	文盲或半文盲
     */
    public static final String STD_ZB_EDU_90 = "90";
    /**
     * 学历字典项 99	未知
     */
    public static final String STD_ZB_EDU_99 = "99";

    /**
     * ECIF学历字典项 START
     */
    public static final String STD_ECIF_EDU_10 = "10";
    public static final String STD_ECIF_EDU_20 = "20";
    public static final String STD_ECIF_EDU_30 = "30";
    public static final String STD_ECIF_EDU_40 = "40";
    public static final String STD_ECIF_EDU_50 = "50";
    public static final String STD_ECIF_EDU_60 = "60";
    public static final String STD_ECIF_EDU_70 = "70";
    public static final String STD_ECIF_EDU_80 = "80";
    public static final String STD_ECIF_EDU_90 = "90";
    public static final String STD_ECIF_EDU_A0 = "A0";
    public static final String STD_ECIF_EDU_99 = "99";
    /** ECIF学历字典项 END */

    /**
     * 性别字典项 9 不详
     */
    public static final String STD_ZB_SEX_9 = "9";

    /**
     * ecif 性别字典 0
     */
    public static final String STD_ECIF_SEX_0 = "0";

    /**
     * ecif 客户状态 01
     */
    public static final String STD_ECIF_CUS_STATE_01 = "01";
    /**
     * ecif 客户状态 04
     */
    public static final String STD_ECIF_CUS_STATE_04 = "04";

    /**
     * ecif 币种 CHF
     */
    public static final String STD_ECIF_CUR_TYPE_CHF = "CHF";

    /**
     * 新信贷币种 STD_ZB_CUR_TYP SFR 瑞典克郎
     */
    public static final String STD_ZB_CUR_TYP_SFR = "SFR";

    /**
     * ecif 企业规模 CHF
     */
    public static final String STD_ECIF_M_ENTERPRISE_9 = "9";

    /**
     * 新信贷 企业规模 SFR
     */
    public static final String STD_M_ENTERPRISE_90 = "90";

    /**
     * 新信贷 经营场地所有权 3 其他
     */
    public static final String STD_ZX_FIELD_OWNER_3 = "3";

    /**
     * ECIF 经营场地所有权 9
     */
    public static final String STD_ECIF_FIELD_OWNER_9 = "9";

    /**
     * 新信贷 经营状况字典项 100	正常经营
     */
    public static final String STD_ZB_OPT_ST_100 = "100";
    /**
     * 新信贷 经营状况字典项 200	经营困难
     */
    public static final String STD_ZB_OPT_ST_200 = "200";
    /**
     * 新信贷 经营状况字典项 300	关停倒闭----营业执照已吊销
     */
    public static final String STD_ZB_OPT_ST_300 = "300";
    /**
     * 新信贷 经营状况字典项 400	关停倒闭----营业执照未吊销
     */
    public static final String STD_ZB_OPT_ST_400 = "400";
    /**
     * 新信贷 经营状况字典项 500	破产----清算完毕
     */
    public static final String STD_ZB_OPT_ST_500 = "500";
    /**
     * 新信贷 经营状况字典项 600	已停产
     */
    public static final String STD_ZB_OPT_ST_600 = "600";
    /**
     * 新信贷 经营状况字典项 700	破产重整
     */
    public static final String STD_ZB_OPT_ST_700 = "700";
    /**
     * 新信贷 经营状况字典项 800	破产处于清算
     */
    public static final String STD_ZB_OPT_ST_800 = "800";
    /**
     * 新信贷 经营状况字典项 900	其他
     */
    public static final String STD_ZB_OPT_ST_900 = "900";

    /**
     * ECIF 经营状况字典项 110
     */
    public static final String STD_ECIF_OPT_ST_110 = "110";
    /**
     * ECIF 经营状况字典项 200
     */
    public static final String STD_ECIF_OPT_ST_200 = "200";
    /**
     * ECIF 经营状况字典项 210
     */
    public static final String STD_ECIF_OPT_ST_210 = "210";
    /**
     * ECIF 经营状况字典项 300
     */
    public static final String STD_ECIF_OPT_ST_300 = "300";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 200300 高管人员
     */
    public static final String STD_CROP_MRG_TYPE_200300 = "200300";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 200400 法人代表
     */
    public static final String STD_CROP_MRG_TYPE_200400 = "200400";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 200500 公司董事长
     */
    public static final String STD_CROP_MRG_TYPE_200500 = "200500";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 200600 公司总经理/厂长/CEO
     */
    public static final String STD_CROP_MRG_TYPE_200600 = "200600";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 200700 财务主管/CFO
     */
    public static final String STD_CROP_MRG_TYPE_200700 = "200700";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 200800 授权经办人
     */
    public static final String STD_CROP_MRG_TYPE_200800 = "200800";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 200900 部门经理
     */
    public static final String STD_CROP_MRG_TYPE_200900 = "200900";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201000 董事
     */
    public static final String STD_CROP_MRG_TYPE_201000 = "201000";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201100 监事
     */
    public static final String STD_CROP_MRG_TYPE_201100 = "201100";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201200 实际控制人
     */
    public static final String STD_CROP_MRG_TYPE_201200 = "201200";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201300 受益人
     */
    public static final String STD_CROP_MRG_TYPE_201300 = "201300";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201400 公司副总经理级的管理人员
     */
    public static final String STD_CROP_MRG_TYPE_201400 = "201400";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201500 合伙制企业的合伙人
     */
    public static final String STD_CROP_MRG_TYPE_201500 = "201500";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201600 中层管理人员
     */
    public static final String STD_CROP_MRG_TYPE_201600 = "201600";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201700 股东
     */
    public static final String STD_CROP_MRG_TYPE_201700 = "201700";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201800 顾问
     */
    public static final String STD_CROP_MRG_TYPE_201800 = "201800";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 201900 联系人
     */
    public static final String STD_CROP_MRG_TYPE_201900 = "201900";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 202000 执行董事
     */
    public static final String STD_CROP_MRG_TYPE_202000 = "202000";

    /**
     * 高管类别字典项 【STD_CROP_MRG_TYPE】 209900 其他
     */
    public static final String STD_CROP_MRG_TYPE_209900 = "209900";


    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 102000 配偶
     */
    public static final String STD_ZB_INDIV_CUS_1 = "102000";

    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 101000 父母
     */
    public static final String STD_ZB_INDIV_CUS_2 = "101000";

    /**
     * 与客户关系字典项  STD_ZB_SELFPER_REL_TYP】 103000 子女
     */
    public static final String STD_ZB_INDIV_CUS_3 = "103000";

    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 105000 其他血亲关系
     */
    public static final String STD_ZB_INDIV_CUS_4 = "105000";

    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 106000 其他姻亲关系
     */
    public static final String STD_ZB_INDIV_CUS_5 = "106000";

    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 107000 同事
     */
    public static final String STD_ZB_INDIV_CUS_6 = "107000";

    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 111000 合伙人
     */
    public static final String STD_ZB_INDIV_CUS_7 = "111000";

    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 112000 其他关系
     */
    public static final String STD_ZB_INDIV_CUS_8 = "112000";

    /**
     * 与客户关系字典项 【STD_ZB_SELFPER_REL_TYP】 104000 兄弟姐妹
     */
    public static final String STD_ZB_INDIV_CUS_9 = "104000";

    /**
     * 客户大类字典项 1  对私
     */
    public static final String STD_ZB_CUS_CATALOG_1 = "1";

    /**
     * 客户大类字典项 2 对公
     */
    public static final String STD_ZB_CUS_CATALOG_2 = "2";

    /**
     * 客户大类字典项 3 同业
     */
    public static final String STD_ZB_CUS_CATALOG_3 = "3";

    /**
     * 关联方类型字典项 01 法人
     */
    public static final String STD_ZB_RELATED_PARTY_TYPE_O1 = "01";
    /**
     * 关联方类型字典项 02 自然人
     */
    public static final String STD_ZB_RELATED_PARTY_TYPE_O2 = "02";
    /**
     * 对公客户：注册登记类型   110 国有全资
     */
    public static final String STD_ZB_REG_TYPE_110 = "110";
    /**
     * 对公客户：注册登记类型   141 国有联营
     */
    public static final String STD_ZB_REG_TYPE_141 = "141";
    /**
     * 对公客户：注册登记类型   151 国有独资（公司）
     */
    public static final String STD_ZB_REG_TYPE_151 = "151";
    /**
     * 007	个人独资
     */
    /**
     * 企业性质- 党政机关
     */
    public static final String STD_ZB_CORP_QLTY_100 = "100";
    /**
     * 企业性质- 事业单位
     */
    public static final String STD_ZB_CORP_QLTY_200 = "200";
    /**
     * 企业性质- 军队
     */
    public static final String STD_ZB_CORP_QLTY_300 = "300";
    /**
     * 企业性质- 社会团体
     */
    public static final String STD_ZB_CORP_QLTY_400 = "400";
    /**
     * 企业性质- 内资企业
     */
    public static final String STD_ZB_CORP_QLTY_500 = "500";
    /**
     * 企业性质- 国有企业
     */
    public static final String STD_ZB_CORP_QLTY_510 = "510";
    /**
     * 企业性质- 集体企业
     */
    public static final String STD_ZB_CORP_QLTY_520 = "520";
    /**
     * 企业性质- 股份合作企业
     */
    public static final String STD_ZB_CORP_QLTY_530 = "530";
    /**
     * 企业性质- 联营企业
     */
    public static final String STD_ZB_CORP_QLTY_540 = "540";
    /**
     * 企业性质- 有限责任公司
     */
    public static final String STD_ZB_CORP_QLTY_550 = "550";
    /**
     * 企业性质- 股份有限公司
     */
    public static final String STD_ZB_CORP_QLTY_560 = "560";
    /**
     * 企业性质- 私营企业
     */
    public static final String STD_ZB_CORP_QLTY_570 = "570";
    /**
     * 企业性质- 外商投资企业业(含港、澳、台)
     */
    public static final String STD_ZB_CORP_QLTY_600 = "600";
    /**
     * 企业性质- 中外合资经营企业(含港、澳、台)
     */
    public static final String STD_ZB_CORP_QLTY_610 = "610";
    /**
     * 企业性质- 中外合作经营企业(含港、澳、台)
     */
    public static final String STD_ZB_CORP_QLTY_620 = "620";
    /**
     * 企业性质- 外资企业(含港、澳、台)
     */
    public static final String STD_ZB_CORP_QLTY_630 = "630";
    /**
     * 企业性质- 外商投资股份有限公司(含港、澳、台)
     */
    public static final String STD_ZB_CORP_QLTY_640 = "640";
    /**
     * 企业性质- 个体经营
     */
    public static final String STD_ZB_CORP_QLTY_700 = "700";
    /**
     * 企业性质- 其他
     */
    public static final String STD_ZB_CORP_QLTY_800 = "800";
    /**
     * 企业性质- 未知
     */
    public static final String STD_ZB_CORP_QLTY_900 = "900";
    /**
     * 企业性质- 全民所有制
     */
    public static final String STD_ZB_CORP_QLTY_001 = "001";
    /**
     * 企业性质- 合伙企业
     */
    public static final String STD_ZB_CORP_QLTY_008 = "008";
    /**
     * 企业性质- 个人独资
     */
    public static final String STD_ZB_CORP_QLTY_007 = "007";

    /**
     * 企业控股类型		国有绝对控股
     */
    public static final String STD_ZB_HOLD_TYPE_111 = "111";
    /**
     * 企业控股类型		国有相对控股
     */
    public static final String STD_ZB_HOLD_TYPE_112 = "112";
    /**
     * 企业控股类型		集体绝对控股
     */
    public static final String STD_ZB_HOLD_TYPE_121 = "121";
    /**
     * 企业控股类型		集体相对控股
     */
    public static final String STD_ZB_HOLD_TYPE_122 = "122";
    /**
     * 企业控股类型		私人绝对控股
     */
    public static final String STD_ZB_HOLD_TYPE_171 = "171";
    /**
     * 企业控股类型		私人相对控股
     */
    public static final String STD_ZB_HOLD_TYPE_172 = "172";
    /**
     * 企业控股类型		港澳台商绝对控股
     */
    public static final String STD_ZB_HOLD_TYPE_201 = "201";
    /**
     * 企业控股类型		港澳台商相对控股
     */
    public static final String STD_ZB_HOLD_TYPE_202 = "202";
    /**
     * 企业控股类型		外商绝对控股
     */
    public static final String STD_ZB_HOLD_TYPE_301 = "301";
    /**
     * 企业控股类型		外商相对控股
     */
    public static final String STD_ZB_HOLD_TYPE_302 = "302";
    /**
     * 企业控股类型		其他
     */
    public static final String STD_ZB_HOLD_TYPE_900 = "900";

    /**
     * 内部信用等级		AAA
     */
    public static final String STD_ZB_GRADE_RANK_001 = "001";
    /**
     * 内部信用等级		AA+
     */
    public static final String STD_ZB_GRADE_RANK_002 = "002";
    /**
     * 内部信用等级		AA
     */
    public static final String STD_ZB_GRADE_RANK_003 = "003";
    /**
     * 内部信用等级		AA-
     */
    public static final String STD_ZB_GRADE_RANK_004 = "004";
    /**
     * 内部信用等级		A+
     */
    public static final String STD_ZB_GRADE_RANK_005 = "005";
    /**
     * 内部信用等级		A
     */
    public static final String STD_ZB_GRADE_RANK_006 = "006";
    /**
     * 内部信用等级		A-
     */
    public static final String STD_ZB_GRADE_RANK_007 = "007";
    /**
     * 内部信用等级		BBB+
     */
    public static final String STD_ZB_GRADE_RANK_008 = "008";
    /**
     * 内部信用等级		BBB
     */
    public static final String STD_ZB_GRADE_RANK_009 = "009";
    /**
     * 内部信用等级		BBB-
     */
    public static final String STD_ZB_GRADE_RANK_010 = "010";
    /**
     * 内部信用等级		BB
     */
    public static final String STD_ZB_GRADE_RANK_011 = "011";
    /**
     * 内部信用等级		B
     */
    public static final String STD_ZB_GRADE_RANK_012 = "012";
    /**
     * 内部信用等级		CCC
     */
    public static final String STD_ZB_GRADE_RANK_013 = "013";
    /**
     * 内部信用等级		CC
     */
    public static final String STD_ZB_GRADE_RANK_014 = "014";
    /**
     * 内部信用等级		C
     */
    public static final String STD_ZB_GRADE_RANK_015 = "015";
    /**
     * 内部信用等级		D
     */
    public static final String STD_ZB_GRADE_RANK_016 = "016";
    /**
     * 内部信用等级		其他
     */
    public static final String STD_ZB_GRADE_RANK_017 = "017";

    /**
     * 企业规模		大型
     */
    public static final String STD_ZB_CUS_SCALE_1 = "1";
    /**
     * 企业规模		中型
     */
    public static final String STD_ZB_CUS_SCALE_2 = "2";
    /**
     * 企业规模		小型
     */
    public static final String STD_ZB_CUS_SCALE_3 = "3";
    /**
     * 企业规模		微型
     */
    public static final String STD_ZB_CUS_SCALE_4 = "4";
    /**
     * 企业规模		其他
     */
    public static final String STD_ZB_CUS_SCALE_5 = "5";
    /**
     * 报表录入类型  自制报表
     */
    public static final String STD_ZB_REPORT_TYPE_01 = "01";
    /**
     * 报表录入类型  税务财报
     */
    public static final String STD_ZB_REPORT_TYPE_02 = "02";
}
