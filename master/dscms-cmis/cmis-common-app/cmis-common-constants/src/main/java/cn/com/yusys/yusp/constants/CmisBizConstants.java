package cn.com.yusys.yusp.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * 常量类:流程作业服务
 */
public class CmisBizConstants {

    //押品状态同步接口XDDB0018 押品状态转换为信贷权证状态的map
    public static Map<String, String> guarStatusMap = new HashMap<>();

    static {
        guarStatusMap.put("10006", "04");//已入库
        guarStatusMap.put("10008", "10");//正常出库
        guarStatusMap.put("10015", "10");//不良资产押品出库
        guarStatusMap.put("10014", "10");//提前出库
        guarStatusMap.put("10013", "10");//部分出库
        guarStatusMap.put("10011", "04");//预抵押入库
        guarStatusMap.put("10012", "04");//预入库已发送核心
        guarStatusMap.put("10010", "04");//未出库已发送核心
        guarStatusMap.put("10028", "10");//预抵押出库已发送核心
        guarStatusMap.put("10018", "04");//预入库转入库发送核算
        guarStatusMap.put("10007", "09");//出库未记账
        guarStatusMap.put("10005", "04");//虚入库
        guarStatusMap.put("10001", "01");//未入库
        guarStatusMap.put("10001", "01");//未入库
        guarStatusMap.put("10040", "07");//出借记账
    }

    //key:押品系统抵质押物类别  value：权证系统抵质押物类别
    public static Map<String, String> gageTypeMap = new HashMap<>();

    static {
        gageTypeMap.put("ZY1001001", "10015");
        gageTypeMap.put("ZY1101001", "10004");
        gageTypeMap.put("ZY1101002", "10004");
        gageTypeMap.put("ZY1101003", "10004");
        gageTypeMap.put("ZY1101004", "10004");
        gageTypeMap.put("ZY1101999", "10004");
        gageTypeMap.put("ZY1102001", "10045");
        gageTypeMap.put("ZY1103001", "10043");
        gageTypeMap.put("ZY1103002", "10043");
        gageTypeMap.put("ZY1103003", "10043");
        gageTypeMap.put("ZY1103999", "10043");
        gageTypeMap.put("ZY1104001", "10006");
        gageTypeMap.put("ZY1104002", "10006");
        gageTypeMap.put("ZY1104003", "10006");
        gageTypeMap.put("ZY1104004", "10006");
        gageTypeMap.put("ZY1104999", "10006");
        gageTypeMap.put("ZY1199999", "10046");
        gageTypeMap.put("ZY1201001", "10041");
        gageTypeMap.put("ZY9901001", "10028");
        gageTypeMap.put("ZY9901002", "10028");
        gageTypeMap.put("ZY9901003", "10028");
        gageTypeMap.put("ZY0503001", "10016");
        gageTypeMap.put("ZY0503002", "10016");
        gageTypeMap.put("ZY0504001", "10016");
        gageTypeMap.put("ZY0504002", "10016");
        gageTypeMap.put("ZY0504003", "10016");
        gageTypeMap.put("ZY0505001", "10016");
        gageTypeMap.put("ZY0505002", "10016");
        gageTypeMap.put("ZY0505003", "10016");
        gageTypeMap.put("ZY0505004", "10016");
        gageTypeMap.put("ZY0505005", "10016");
        gageTypeMap.put("ZY0505999", "10016");
        gageTypeMap.put("ZY0506999", "10016");
        gageTypeMap.put("ZY0601002", "10018");
        gageTypeMap.put("ZY0601003", "10018");
        gageTypeMap.put("ZY0601004", "10018");
        gageTypeMap.put("ZY0601999", "10018");
        gageTypeMap.put("ZY0602001", "10018");
        gageTypeMap.put("ZY0602002", "10018");
        gageTypeMap.put("ZY0602003", "10018");
        gageTypeMap.put("ZY0602004", "10018");
        gageTypeMap.put("ZY0602005", "10018");
        gageTypeMap.put("ZY0602006", "10018");
        gageTypeMap.put("ZY0602007", "10018");
        gageTypeMap.put("ZY0602008", "10018");
        gageTypeMap.put("ZY0602009", "10018");
        gageTypeMap.put("ZY0602010", "10018");
        gageTypeMap.put("ZY0602011", "10018");
        gageTypeMap.put("ZY0602012", "10018");
        gageTypeMap.put("ZY0602013", "10018");
        gageTypeMap.put("ZY0602014", "10018");
        gageTypeMap.put("ZY0602015", "10018");
        gageTypeMap.put("ZY0602016", "10018");
        gageTypeMap.put("ZY0602017", "10018");
        gageTypeMap.put("ZY0602018", "10018");
        gageTypeMap.put("ZY0602019", "10018");
        gageTypeMap.put("ZY0602020", "10018");
        gageTypeMap.put("ZY0602021", "10018");
        gageTypeMap.put("ZY0602022", "10018");
        gageTypeMap.put("ZY0602023", "10018");
        gageTypeMap.put("ZY0601001", "10018");
        gageTypeMap.put("ZY0801001", "10015");
        gageTypeMap.put("ZY0801002", "10015");
        gageTypeMap.put("ZY0801003", "10015");
        gageTypeMap.put("ZY0801004", "10015");
        gageTypeMap.put("ZY0801005", "10015");
        gageTypeMap.put("ZY0801006", "10015");
        gageTypeMap.put("ZY0801007", "10015");
        gageTypeMap.put("ZY0801008", "10015");
        gageTypeMap.put("ZY0801009", "10015");
        gageTypeMap.put("ZY0801010", "10015");
        gageTypeMap.put("ZY0801011", "10015");
        gageTypeMap.put("ZY0801012", "10015");
        gageTypeMap.put("ZY0801999", "10015");
        gageTypeMap.put("ZY0602024", "10018");
        gageTypeMap.put("ZY0802001", "10015");
        gageTypeMap.put("ZY0602999", "10018");
        gageTypeMap.put("ZY0699001", "10018");
        gageTypeMap.put("ZY0699999", "10018");
        gageTypeMap.put("ZY0701001", "10021");
        gageTypeMap.put("ZY0701002", "10021");
        gageTypeMap.put("ZY0701003", "10021");
        gageTypeMap.put("ZY0701004", "10021");
        gageTypeMap.put("ZY0701005", "10021");
        gageTypeMap.put("ZY0701999", "10021");
        gageTypeMap.put("ZY0802002", "10015");
        gageTypeMap.put("ZY0802003", "10015");
        gageTypeMap.put("ZY0802004", "10015");
        gageTypeMap.put("ZY0802005", "10015");
        gageTypeMap.put("ZY0802006", "10015");
        gageTypeMap.put("ZY0802007", "10015");
        gageTypeMap.put("ZY0802008", "10015");
        gageTypeMap.put("ZY0802009", "10015");
        gageTypeMap.put("ZY0802010", "10015");
        gageTypeMap.put("ZY0802011", "10015");
        gageTypeMap.put("ZY0802012", "10015");
        gageTypeMap.put("ZY0802013", "10015");
        gageTypeMap.put("ZY0802014", "10015");
        gageTypeMap.put("ZY0802015", "10015");
        gageTypeMap.put("ZY0802999", "10015");
        gageTypeMap.put("ZY0803001", "10015");
        gageTypeMap.put("ZY0803002", "10015");
        gageTypeMap.put("ZY0803003", "10015");
        gageTypeMap.put("ZY0803004", "10015");
        gageTypeMap.put("ZY0803005", "10015");
        gageTypeMap.put("ZY0803006", "10015");
        gageTypeMap.put("ZY0803007", "10015");
        gageTypeMap.put("ZY0803008", "10015");
        gageTypeMap.put("ZY0803009", "10015");
        gageTypeMap.put("ZY0803010", "10015");
        gageTypeMap.put("ZY0803011", "10015");
        gageTypeMap.put("ZY0803012", "10015");
        gageTypeMap.put("ZY0803013", "10015");
        gageTypeMap.put("ZY0803999", "10015");
        gageTypeMap.put("ZY0899999", "10015");
        gageTypeMap.put("ZY0901001", "10015");
        gageTypeMap.put("ZY9901004", "10028");
        gageTypeMap.put("ZY9901005", "10028");
        gageTypeMap.put("ZY9901006", "10028");
        gageTypeMap.put("ZY9902001", "10013");
        gageTypeMap.put("ZY9902002", "10013");
        gageTypeMap.put("ZY9902003", "10013");
        gageTypeMap.put("ZY9902004", "10013");
        gageTypeMap.put("ZY9999001", "10026");
        gageTypeMap.put("ZY9999002", "10021");
        gageTypeMap.put("ZY9999003", "10021");
        gageTypeMap.put("ZY9999999", "10021");
        gageTypeMap.put("ZY0402001", "10012");
        gageTypeMap.put("ZY0402002", "10012");
        gageTypeMap.put("ZY0402003", "10012");
        gageTypeMap.put("ZY0402999", "10012");
        gageTypeMap.put("ZY0403001", "10012");
        gageTypeMap.put("ZY0403002", "10012");
        gageTypeMap.put("ZY0403003", "10012");
        gageTypeMap.put("ZY0403004", "10012");
        gageTypeMap.put("ZY0403005", "10012");
        gageTypeMap.put("ZY0403006", "10012");
        gageTypeMap.put("ZY0403999", "10012");
        gageTypeMap.put("ZY0404001", "10012");
        gageTypeMap.put("ZY0405001", "10012");
        gageTypeMap.put("ZY0406001", "10012");
        gageTypeMap.put("ZY0406002", "10012");
        gageTypeMap.put("ZY0406003", "10012");
        gageTypeMap.put("ZY0406004", "10012");
        gageTypeMap.put("ZY0407001", "10012");
        gageTypeMap.put("ZY0408001", "10012");
        gageTypeMap.put("ZY0499999", "10012");
        gageTypeMap.put("ZY0501001", "10016");
        gageTypeMap.put("ZY0501002", "10016");
        gageTypeMap.put("ZY0501003", "10016");
        gageTypeMap.put("DY0101001", "10003");
        gageTypeMap.put("DY0101002", "10003");
        gageTypeMap.put("DY0101003", "10003");
        gageTypeMap.put("DY0101004", "10003");
        gageTypeMap.put("DY0101005", "10003");
        gageTypeMap.put("DY0101999", "10003");
        gageTypeMap.put("DY0102001", "10003");
        gageTypeMap.put("DY0102002", "10003");
        gageTypeMap.put("DY0102003", "10003");
        gageTypeMap.put("DY0102004", "10003");
        gageTypeMap.put("DY0102005", "10003");
        gageTypeMap.put("DY0102006", "10003");
        gageTypeMap.put("DY0102999", "10003");
        gageTypeMap.put("DY0103001", "10003");
        gageTypeMap.put("DY0103002", "10003");
        gageTypeMap.put("DY0103003", "10003");
        gageTypeMap.put("DY0103004", "10003");
        gageTypeMap.put("DY0199999", "10003");
        gageTypeMap.put("DY0201001", "10001");
        gageTypeMap.put("DY0201002", "10001");
        gageTypeMap.put("DY0201003", "10001");
        gageTypeMap.put("DY0201004", "10001");
        gageTypeMap.put("DY0201999", "10001");
        gageTypeMap.put("DY0299001", "10047");
        gageTypeMap.put("DY0299002", "10047");
        gageTypeMap.put("DY0299003", "10047");
        gageTypeMap.put("DY0299004", "10007");
        gageTypeMap.put("DY0299999", "10047");
        gageTypeMap.put("DY0301001", "10001");
        gageTypeMap.put("DY0301002", "10001");
        gageTypeMap.put("DY0301003", "10001");
        gageTypeMap.put("DY0301004", "10001");
        gageTypeMap.put("DY0301999", "10001");
        gageTypeMap.put("DY0401001", "10040");
        gageTypeMap.put("DY0501001", "10005");
        gageTypeMap.put("DY0501002", "10005");
        gageTypeMap.put("DY0501003", "10005");
        gageTypeMap.put("DY0501004", "10005");
        gageTypeMap.put("DY0501005", "10005");
        gageTypeMap.put("DY0501006", "10005");
        gageTypeMap.put("DY0501007", "10005");
        gageTypeMap.put("DY0501008", "10005");
        gageTypeMap.put("DY0501009", "10005");
        gageTypeMap.put("DY0601001", "10001");
        gageTypeMap.put("DY0602001", "10002");
        gageTypeMap.put("DY0603001", "10002");
        gageTypeMap.put("DY0604001", "10002");
        gageTypeMap.put("DY0699999", "10002");
        gageTypeMap.put("DY0701001", "10004");
        gageTypeMap.put("DY0701002", "10004");
        gageTypeMap.put("DY0701003", "10004");
        gageTypeMap.put("DY0701004", "10004");
        gageTypeMap.put("DY0701999", "10004");
        gageTypeMap.put("DY0702001", "10045");
        gageTypeMap.put("DY0703001", "10043");
        gageTypeMap.put("DY0703002", "10043");
        gageTypeMap.put("DY0703003", "10043");
        gageTypeMap.put("DY0703999", "10043");
        gageTypeMap.put("DY0704001", "10006");
        gageTypeMap.put("DY0704002", "10006");
        gageTypeMap.put("DY0704003", "10006");
        gageTypeMap.put("DY0704004", "10006");
        gageTypeMap.put("DY0704999", "10006");
        gageTypeMap.put("DY0705001", "10043");
        gageTypeMap.put("DY0705002", "10006");
        gageTypeMap.put("DY0705999", "10046");
        gageTypeMap.put("DY0799999", "10046");
        gageTypeMap.put("DY0801001", "10010");
        gageTypeMap.put("ZY0102007", "10017");
        gageTypeMap.put("ZY0102008", "10017");
        gageTypeMap.put("ZY0102999", "10017");
        gageTypeMap.put("ZY0201001", "10011");
        gageTypeMap.put("ZY0201002", "10011");
        gageTypeMap.put("ZY0201003", "10011");
        gageTypeMap.put("DY9999999", "10009");
        gageTypeMap.put("ZY0101001", "10021");
        gageTypeMap.put("ZY0101002", "10021");
        gageTypeMap.put("ZY0102001", "10017");
        gageTypeMap.put("ZY0102002", "10017");
        gageTypeMap.put("ZY0102003", "10017");
        gageTypeMap.put("ZY0102004", "10017");
        gageTypeMap.put("ZY0102005", "10017");
        gageTypeMap.put("ZY0102006", "10017");
        gageTypeMap.put("ZY0202001", "10011");
        gageTypeMap.put("ZY0202002", "10011");
        gageTypeMap.put("ZY0202003", "10011");
        gageTypeMap.put("ZY0203001", "10011");
        gageTypeMap.put("ZY0203002", "10011");
        gageTypeMap.put("ZY0203003", "10011");
        gageTypeMap.put("ZY0299999", "10011");
        gageTypeMap.put("ZY0301001", "10014");
        gageTypeMap.put("ZY0301002", "10014");
        gageTypeMap.put("ZY0301003", "10014");
        gageTypeMap.put("ZY0301004", "10014");
        gageTypeMap.put("ZY0301005", "10014");
        gageTypeMap.put("ZY0301006", "10014");
        gageTypeMap.put("ZY0301999", "10014");
        gageTypeMap.put("ZY0302001", "10014");
        gageTypeMap.put("ZY0303001", "10014");
        gageTypeMap.put("ZY0303002", "10014");
        gageTypeMap.put("ZY0401001", "10042");
        gageTypeMap.put("ZY0401002", "10042");
        gageTypeMap.put("ZY0401003", "10042");
        gageTypeMap.put("ZY0401004", "10042");
        gageTypeMap.put("ZY0401005", "10042");
        gageTypeMap.put("ZY0401998", "10042");
        gageTypeMap.put("ZY0401999", "10042");
        gageTypeMap.put("ZY0501004", "10016");
        gageTypeMap.put("ZY0501005", "10016");
        gageTypeMap.put("ZY0501006", "10016");

    }

    //key:信贷系统权证类型  value：押品系统权证类型
    public static Map<String, String> certiTypeCdMap = new HashMap<>();

    static {
        //01 房屋抵押合同及他项权证 02
        certiTypeCdMap.put("01","02");
        //02 土地抵押合同及他项权证 03
        certiTypeCdMap.put("02","03");
        //03 抵押登记证明 04
        certiTypeCdMap.put("03","04");
        //04 不动产登记证明 05
        certiTypeCdMap.put("04","05");
        //05 不动产权证 07
        certiTypeCdMap.put("05","07");
        //06 国内信用证 08
        certiTypeCdMap.put("06","08");
        //07 已经背书(质押) 字样的提单 09
        certiTypeCdMap.put("07","09");
        //08 仓储公司出具的质物清单 10
        certiTypeCdMap.put("08","10");
        //09 质押登记证明 12
        certiTypeCdMap.put("09","12");
        //10 贵金属入库单据 14
        certiTypeCdMap.put("10","14");
        //11 债券凭证 15
        certiTypeCdMap.put("11","15");
        //12 银行汇票 17
        certiTypeCdMap.put("12","17");
        //13 其他票据证明文件 21
        certiTypeCdMap.put("13","21");
        //14 收费许可证 41
        certiTypeCdMap.put("14","41");
        //15 提单 42
        certiTypeCdMap.put("15","42");
        //16 汽车合格证 43
        certiTypeCdMap.put("16","43");
        //17 共有权证 44
        certiTypeCdMap.put("17","44");
        //18 进口证明书/随车检验单 45
        certiTypeCdMap.put("18","45");
        //19 保函 24
        certiTypeCdMap.put("19","24");
        //20 汽车履约保证保函 46
        certiTypeCdMap.put("20","46");
        //21 备用信用证 11
        certiTypeCdMap.put("21","11");
        //22 理财产品协议 16
        certiTypeCdMap.put("22","16");
        //23 信托产品协议 47
        certiTypeCdMap.put("23","47");
        //24 股权证 48
        certiTypeCdMap.put("24","48");
        //25 债券 49
        certiTypeCdMap.put("25","49");
        //27 营运车牌 50
        certiTypeCdMap.put("27","50");
        //28 增值税发票 51
        certiTypeCdMap.put("28","51");
        //29 购房合同 52
        certiTypeCdMap.put("29","52");
        //30 股票 53
        certiTypeCdMap.put("30","53");
        //31 批准证件 54
        certiTypeCdMap.put("31","54");
        //32 权利证明单据 55
        certiTypeCdMap.put("32","55");
        //33 商业承兑汇票 20
        certiTypeCdMap.put("33","20");
        //34 水域滩涂养殖使用证 56
        certiTypeCdMap.put("34","56");
        //35 机动车销售统一发票 57
        certiTypeCdMap.put("35","57");
        //36 车辆购置税缴交凭证 58
        certiTypeCdMap.put("36","58");
        //37 购房发票 59
        certiTypeCdMap.put("37","59");
        //40 存单 13
        certiTypeCdMap.put("40","13");
        //41 房屋所有权证 01
        certiTypeCdMap.put("41","01");
        //44 国有土地使用证 06
        certiTypeCdMap.put("44","06");
        //45 机动车登记证 32
        certiTypeCdMap.put("45","32");
        //46 设备购置发票 60
        certiTypeCdMap.put("46","60");
        //48 轮船所有权证 61
        certiTypeCdMap.put("48","61");
        //49 标准仓单 62
        certiTypeCdMap.put("49","62");
        //51 保单 22
        certiTypeCdMap.put("51","22");
        //52 银行本票 18
        certiTypeCdMap.put("52","18");
        //54 保险公司出具的相关证明 23
        certiTypeCdMap.put("54","23");
        //55 收费（益）权批准文件或收费许可证 25
        certiTypeCdMap.put("55","25");
        //56 仓单或监管方回执 26
        certiTypeCdMap.put("56","26");
        //57 林权证 27
        certiTypeCdMap.put("57","27");
        //58 海域使用权证 28
        certiTypeCdMap.put("58","28");
        //59 机械设备 29
        certiTypeCdMap.put("59","29");
        //60 车辆合格证 30
        certiTypeCdMap.put("60","30");
        //61 出租车运营证 31
        certiTypeCdMap.put("61","31");
        //62 质提登记证 33
        certiTypeCdMap.put("62","33");
        //63 船舶产权登记证 34
        certiTypeCdMap.put("63","34");
        //64 国外信用证 36
        certiTypeCdMap.put("64","36");
        //99 其他可证明我行拥有合法抵（质）押权利的权属证书和书面凭证（文本填写说明）99
        certiTypeCdMap.put("99","99");
    }


    /**
     * 办理人员类型  营销人员-01
     **/
    public static final String CORRE_REL_TYPE_MAR = "01";
    /**
     * 办理人员类型  管户人员-02
     **/
    public static final String CORRE_REL_TYPE_MANA = "02";
    /**
     * 办理人员类型  销售操作人员-03
     **/
    public static final String CORRE_REL_TYPE_SALE = "03";
    /**
     * 办理人员类型  主办人员-04
     **/
    public static final String CORRE_REL_TYPE_MAIN = "04";
    /**
     * 办理人员类型  协办人员-05
     **/
    public static final String CORRE_REL_TYPE_ASSI = "05";

    /**
     * 操作类型  insert-新增操作
     **/
    public static final String CORRE_PAGE_OP_FLAG_INSERT = "insert";
    /**
     * 操作类型  update-修改操作
     **/
    public static final String CORRE_PAGE_OP_FLAG_UPDATE = "update";


    public static final String TATLE_BUS_HAND = "业务办理";
    public static final String TATLE_LMT_APP = "额度审批";
    public static final String TATLE_TODO_CONT = "待签合同";
    public static final String TATLE_TODO_LOAN = "待放款";
    public static final String TATLE_CALL_BACK = "审批打回";
    public static final String TATLE_REFUSE = "审批否决";
    public static final String TATLE_WAIT_REPAY_FOR_FIFTEEN_DAYS = "15天内待还款";
    public static final String TATLE_WARRANT_LOAN = "权证出借";
    public static final String TATLE_OVERDUE = "逾期";
    public static final String TATLE_MATURE_TOPTEN_FOR_BUS = "到期前十天业务";
    public static final String TATLE_BUS_ROWS = "业务统计-笔数";
    public static final String TATLE_BUS_AMT = "业务统计-金额（万元）";
    public static final String TATLE_BUS_BALANCE = "业务统计-余额（万元）";


    public static final String MEM_LIST = "集团授信填报";


    /**
     * 流程查询类型 0-流程中
     **/
    public static final String FLOW_QUERY_TYPE_0 = "0";
    /**
     * 流程查询类型 1-流程结束
     **/
    public static final String FLOW_QUERY_TYPE_1 = "1";
    /**
     * 流程查询类型 2-所有
     **/
    public static final String FLOW_QUERY_TYPE_2 = "2";
    /**
     * 页面展示标记 0-使用平台公共界面
     **/
    public static final String PAGE_VIEW_FLAG_0 = "0";
    /**
     * 页面展示标记 1-使用业务页面
     **/
    public static final String PAGE_VIEW_FLAG_1 = "1";
    /**
     * 待签合同业务类型
     **/
    public static final String BIZ_TYPE_TODO_CONT = "TODOCONT";
    /**
     * 审批打回
     **/
    public static final String BIZ_TYPE_CALL_BACK = "CALLBACK";
    /**
     * 审批否决
     **/
    public static final String BIZ_TYPE_REFUSE = "REFUSE";
    /**
     * 15天内待还款
     **/
    public static final String BIZ_TYPE_WAIT_REPAY_FOR_FIFTEEN_DAYS = "WAIT_REPAY_FOR_FIFTEEN_DAYS";
    /**
     * 权证借出
     */
    public static final String BIZ_TYPE_WARRANT_LOAN = "WARRANT_LOAN";
    /**
     * 逾期
     */
    public static final String BIZ_TYPE_OVERDUE = "OVERDUE";
    /**
     * 到期前十天业务
     */
    public static final String BIZ_TYPE_MATURE_TOPTEN_FOR_BUS = "MATURE_TOPTEN_FOR_BUS";
    /**
     * 业务统计-笔数
     */
    public static final String BIZ_TYPE_BUS_ROWS = "BUS_ROWS";
    /**
     * 业务统计-金额（万元）
     */
    public static final String BIZ_TYPE_BUS_AMT = "BUS_AMT";
    /**
     * 业务统计-余额（万元）
     */
    public static final String BIZ_TYPE_BUS_BALANCE = "BUS_BALANCE";
    /**
     * 流程审批上一节点 审批操作 打回
     */
    public static final String FLOW_NODE_STATE_CALLBACK = "O-1";
    /**
     * 流程状态 否决
     */
    public static final String FLOW_STATE_REFUSE = "F";


    /**
     * 待发起
     **/
    public static final String APPLY_STATE_TODO = "000";
    /**
     * 审批中
     **/
    public static final String APPLY_STATE_APP = "111";
    /**
     * 取消
     **/
    public static final String APPLY_STATE_CANCEL = "990";
    /**
     * 追回
     **/
    public static final String APPLY_STATE_TACK_BACK = "991";
    /**
     * 退回
     **/
    public static final String APPLY_STATE_CALL_BACK = "992";
    /**
     * 通过
     **/
    public static final String APPLY_STATE_PASS = "997";
    /**
     * 否决
     **/
    public static final String APPLY_STATE_REFUSE = "998";
    /**
     * 自行退出
     **/
    public static final String APPLY_STATE_QUIT = "996";

    /**
     * 初次评估/贷时评估
     **/
    public static final String EVAL_TYPE_FIRST = "01";
    /**
     * 贷后重估
     **/
    public static final String EVAL_TYPE_AFTER = "02";
    /**
     * 初次评估/贷时评估
     **/
    public static final String EVAL_TYPE_DEAL = "03";
    /**
     * 其他评估
     **/
    public static final String EVAL_TYPE_OTHER = "99";


    /**
     * 押品外评
     **/
    public static final String EVAL_TYPE_OUT = "01";
    /**
     * 押品内评
     **/
    public static final String EVAL_TYPE_IN = "02";
    /**
     * 押品内外评
     **/
    public static final String EVAL_TYPE_INOUT = "03";
    /**
     * 押品在线评
     **/
    public static final String EVAL_TYPE_LINE = "04";

    /**
     * 成功
     **/
    public static final String EVAL_RESULT_SUCCESS = "1";
    /**
     * 失败
     **/
    public static final String EVAL_RESULT_FAIL = "0";

    /**
     * 同业机构准入新增
     **/
    public static final String STD_ZB_ZJTY_TY001 = "TY001";

    /**
     * 同业机构准入年审
     **/
    public static final String STD_ZB_ZJTY_TY002 = "TY002";

    /**
     * 同业机构准入调整
     **/
    public static final String STD_ZB_ZJTY_TY003 = "TY003";

    /**
     * 同业授信新增
     **/
    public static final String STD_ZB_ZJTY_TY004 = "TY004";

    /**
     * 同业授信变更
     **/
    public static final String STD_ZB_ZJTY_TY005 = "TY005";

    /**
     * 同业授信复议
     **/
    public static final String STD_ZB_ZJTY_TY006 = "TY006";

    /**
     * 同业授信批复变更
     **/
    public static final String STD_ZB_ZJTY_TY007 = "TY007";

    /**
     * 单笔投资授信新增（分支机构）
     **/
    public static final String STD_ZB_ZJTY_TY008 = "TY008";

    /**
     * 单笔投资授信变更（分支机构）
     **/
    public static final String STD_ZB_ZJTY_TY009 = "TY009";

    /**
     * 单笔投资授信复议（分支机构）
     **/
    public static final String STD_ZB_ZJTY_TY010 = "TY010";

    /**
     * 单笔投资授信新增（总行部门）
     **/
    public static final String STD_ZB_ZJTY_TY011 = "TY011";

    /**
     * 单笔投资授信变更（总行部门）
     **/
    public static final String STD_ZB_ZJTY_TY012 = "TY012";

    /**
     * 单笔投资授信复议（总行部门）
     **/
    public static final String STD_ZB_ZJTY_TY013 = "TY013";

    /**
     * 单笔投资授信新增（总行发起-债券池）
     **/
    public static final String STD_ZB_ZJTY_TY014 = "TY014";

    /**
     * 单笔投资授信变更（总行发起-债券池）
     **/
    public static final String STD_ZB_ZJTY_TY015 = "TY015";

    /**
     * 单笔投资授信复议（总行发起-债券池）
     **/
    public static final String STD_ZB_ZJTY_TY016 = "TY016";

    /**
     * 单笔投资授信批复变更
     **/
    public static final String STD_ZB_ZJTY_TY017 = "TY017";


    /**
     * 定义操作标识符  insert-新增
     **/
    public static final String OP_FLAG_INSERT = "insert";
    /**
     * 定义操作标识符  update-更新
     **/
    public static final String OP_FLAG_UPDATE = "update";

    /**
     * 担保合同引入担保品状态有效
     **/
    public static final String GRT_STATUS_1 = "1";
    /**
     * 担保合同引入担保品状态无效
     **/
    public static final String GRT_STATUS_0 = "0";

    /**
     * 业务类型标识位  01-单笔单批业务
     **/
    public static final String BIZ_TYPE_01 = "01";
    /**
     * 业务类型标识位  02-额度项下业务
     **/
    public static final String BIZ_TYPE_02 = "02";
    /**
     * 业务类型标识位  03-特殊业务
     **/
    public static final String BIZ_TYPE_03 = "03";

    /**
     * 关联关系标识位  1-正常
     **/
    public static final String CORRE_REL_1 = "1";
    /**
     * 关联关系标识位  2-新增
     **/
    public static final String CORRE_REL_2 = "2";
    /**
     * 关联关系标识位  3-解除
     **/
    public static final String CORRE_REL_3 = "3";
    /**
     * 关联关系标识位  4-续作
     **/
    public static final String CORRE_REL_4 = "4";
    /**
     * 关联关系标识位  5-解除中
     **/
    public static final String CORRE_REL_5 = "5";
    /**
     * 关联关系标识位  6-续作中
     **/
    public static final String CORRE_REL_6 = "6";

    /**
     * 关联类型  1-与授信关联
     **/
    public static final String R_TYPE_1 = "1";
    /**
     * 关联类型  1-与业务关联
     **/
    public static final String R_TYPE_2 = "2";

    /**
     * 担保合同状态标识符  00-登记
     **/
    public static final String GUAR_CONT_STATE_00 = "00";
    /**
     * 担保合同状态标识符  01-有效
     **/
    public static final String GUAR_CONT_STATE_01 = "101";
    /**
     * 担保合同状态标识符  02-注销
     **/
    public static final String GUAR_CONT_STATE_02 = "104";

    /**
     * 台账状态 1-正常
     **/
    public static final String ACC_STATUS_1 = "1";

    /**
     * 担保方式  guar_way  100-抵押
     **/
    public static final String GUAR_WAY_100 = "100";
    /**
     * 担保方式  guar_way  200-质押
     **/
    public static final String GUAR_WAY_200 = "200";
    /**
     * 担保方式  guar_way  300-保证
     **/
    public static final String GUAR_WAY_300 = "300";
    /**
     * 担保方式  guar_way  400-信用
     **/
    public static final String GUAR_WAY_400 = "400";

    /**
     * 字符串竖线分隔
     **/
    public static final String STR_VERTICAL_BAR_SEPARATION_TRANS = "\\|";
    public static final String STR_VERTICAL_BAR_SEPARATION = "|";
    public static final char UNDERLINE = '_';
    public static final String AND = "and";
    public static final String OR = "or";
    /**
     * 分隔符-逗号
     **/
    public static final String SEPARATOR_COMMA = ",";
    /**
     * 是
     **/
    public static final String YES = "Y";

    /**
     * xddb0011成功标识
     **/
    public static final String SUCCESS = "S";

    /**
     * xddb0011失败标识
     **/
    public static final String FAIL = "F";

    /**
     * xdxw0011失败message
     **/
    public static final String XDXW0011_FAIL_MESSAGE = "未找到对应的勘验信息，无法更新！";

    /**
     * xdxw0011 status
     **/
    public static final String XDXW0011_STATUS = "02,03,04";

    /**
     * xdxw0011 勘验状态有误
     **/
    public static final String XDXW0011_STATUS_ERROR_MESSAGE = "勘验状态有误！";

    /**
     * xdxw0014失败message
     **/
    public static final String XDXW0014_FAIL_MESSAGE = "未找到对应的优企贷共借人信息，无法更新！";

    /**
     * 是
     **/
    public static final String YES_MESSAGE = "操作成功";
    /**
     * 否
     **/
    public static final String NO = "N";

    /**
     * 操作失败
     **/
    public static final String NO_MESSAGE = "押品出库信息已存在";
    /**
     * 押品分类状态-启用
     **/
    public static final String GUARTYPESTATE_START = "0";
    /**
     * 押品分类状态-停用
     **/
    public static final String GUARTYPESTATE_STOP = "1";
    /**
     * 权重法下变现能力/代偿能力要求-高
     **/
    public static final String ABILITY_HIGH = "0";
    /**
     * 权重法下变现能力/代偿能力要求-低
     **/
    public static final String ABILITY_LOW = "1";
    /**
     * 合格认定结果-一类合格
     **/
    public static final String GUAR_IDENT_RESULT_QUALIFY_CLASS_ONE = "01";
    /**
     * 合格认定结果-二类合格
     **/
    public static final String GUAR_IDENT_RESULT_QUALIFY_CLASS_TWO = "02";
    /**
     * 合格认定结果-一类不合格
     **/
    public static final String GUAR_IDENT_RESULT_UNQUALIFY_CLASS_ONE = "03";
    /**
     * 合格认定结果-二类不合格
     **/
    public static final String GUAR_IDENT_RESULT_UNQUALIFY_CLASS_TWO = "04";
    /**
     * 合格认定评估类型-权重法
     **/
    public static final String ASSESSMENT_TYPE_PCT = "01";
    /**
     * 合格认定评估类型-内评初级法
     **/
    public static final String ASSESSMENT_TYPE_INNER = "02";
    /**
     * 押品登记办理状态-无需办理
     **/
    public static final String GUAR_REG_STATE_01 = "01";
    /**
     * 押品登记办理状态-需办理未办理
     **/
    public static final String GUAR_REG_STATE_02 = "02";
    /**
     * 押品登记办理状态-需办理已办理
     **/
    public static final String GUAR_REG_STATE_03 = "03";
    /**
     * 权重法下合格缓释工具类别-以特户、封金或保证金等形式特定化后的现金
     **/
    public static final String GUAR_PCT_TOOL_01 = "01";
    /**
     * 权重法下合格缓释工具类别-黄金
     **/
    public static final String GUAR_PCT_TOOL_02 = "02";
    /**
     * 权重法下合格缓释工具类别-银行存单
     **/
    public static final String GUAR_PCT_TOOL_03 = "03";
    /**
     * 权重法下合格缓释工具类别-我国财政部发行的国债
     **/
    public static final String GUAR_PCT_TOOL_04 = "04";
    /**
     * 权重法下合格缓释工具类别-中国人民银行发行的票据
     **/
    public static final String GUAR_PCT_TOOL_05 = "05";
    /**
     * 权重法下合格缓释工具类别-我国政策性银行、公共部门实体、商业银行发行的债券、票据和承兑的汇票
     **/
    public static final String GUAR_PCT_TOOL_06 = "06";
    /**
     * 权重法下合格缓释工具类别-金融资产管理公司为收购国有银行而定向发行的债券
     **/
    public static final String GUAR_PCT_TOOL_07 = "07";
    /**
     * 权重法下合格缓释工具类别-评级为BBB-（含BBB-）以上国家或地区政府和中央银行发行的债券
     **/
    public static final String GUAR_PCT_TOOL_08 = "08";
    /**
     * 权重法下合格缓释工具类别-注册地所在国家或地区的评级在A-（含A-)以上的境外商业银行和公共部门实体发行的债券、票据和承兑的汇票
     **/
    public static final String GUAR_PCT_TOOL_09 = "09";
    /**
     * 权重法下合格缓释工具类别-多边开发银行、国际清算银行和国际货币基金组织发行的债券
     **/
    public static final String GUAR_PCT_TOOL_10 = "10";
    /**
     * 权重法下合格缓释工具类别-我国中央政府、中国人民银行、政策性银行、公共部门实体和商业银行保证
     **/
    public static final String GUAR_PCT_TOOL_11 = "11";
    /**
     * 权重法下合格缓释工具类别-评级为BBB-（含BBB-）以上国家或地区政府和中央银行保证
     **/
    public static final String GUAR_PCT_TOOL_12 = "12";
    /**
     * 权重法下合格缓释工具类别-注册地所在国家或地区的评级在A-（含A-）以上的境外商业银行和公共部门实体保证
     **/
    public static final String GUAR_PCT_TOOL_13 = "13";
    /**
     * 权重法下合格缓释工具类别-多边开发银行、国际清算银行和国际货币基金组织保证
     **/
    public static final String GUAR_PCT_TOOL_14 = "14";
    /**
     * 押品评估方式 外部评估
     **/
    public static final String GUAR_EVAL_IN_OUT_TYPE_OUT = "01";
    /**
     * 押品评估方式 内部评估
     **/
    public static final String GUAR_EVAL_IN_OUT_TYPE_IN = "02";
    /**
     * 押品评估方式 内外部合作评估
     **/
    public static final String GUAR_EVAL_IN_OUT_TYPE_IN_OR_OUT = "03";
    /**
     * 接口返回值 成功
     **/
    public static final String GUAR_CLIENT_RETURN_CODE_SUCCESS = "000000";
    /**
     * 接口返回值 失败
     **/
    public static final String GUAR_CLIENT_RETURN_CODE_FAIL = "999999";
    /**
     * 权证状态：07-出借已记账
     **/
    public static final String CERTI_STATE_07 = "07";

    /**
     * 押品所在业务阶段 已创建
     **/
    public static final String GUAR_BUSISTATE_01 = "01";

    /**
     * 操作类型 新增
     **/
    public static final String OPR_TYPE_01 = "01";
    /**
     * 操作类型 删除
     **/
    public static final String OPR_TYPE_02 = "02";

    /**
     * 重置押品所在业务阶段
     **/
    public static final String OPE_TYPE_ONRESET = "onReset";


    /**
     * 审批状态  000-待发起
     **/
    public static final String STD_ZB_APP_ST_000 = "000";
    /**
     * 审批状态 111-审批中
     **/
    public static final String STD_ZB_APP_ST_111 = "111";
    /**
     * 审批状态 990-取消
     **/
    public static final String STD_ZB_APP_ST_990 = "990";
    /**
     * 审批状态 991-拿回
     **/
    public static final String STD_ZB_APP_ST_991 = "991";
    /**
     * 审批状态 992-打回
     **/
    public static final String STD_ZB_APP_ST_992 = "992";
    /**
     * 审批状态 996-自行退出
     **/
    public static final String STD_ZB_APP_ST_996 = "996";
    /**
     * 审批状态 997-审批通过
     **/
    public static final String STD_ZB_APP_ST_997 = "997";
    /**
     * 审批状态 998-否决
     **/
    public static final String STD_ZB_APP_ST_998 = "998";
    /**
     * 出具报告类型 01-审查报告
     */
    public static final String STD_ISSUE_REPORT_TYPE_01 = "01";
    /**
     * 出具报告类型 02-核查报告
     */
    public static final String STD_ISSUE_REPORT_TYPE_02 = "02";
    /**
     * 出具报告类型 03-拟批复
     */
    public static final String STD_ISSUE_REPORT_TYPE_03 = "03";


    /**
     * 权证出库类型 01-押品部分出库
     **/
    public static final String STD_WARRANT_OUT_TYPE_01 = "01";
    /**
     * 权证出库类型 02-押品置换出库
     **/
    public static final String STD_WARRANT_OUT_TYPE_02 = "02";
    /**
     * 权证出库类型 03-权证更新出库
     **/
    public static final String STD_WARRANT_OUT_TYPE_03 = "03";
    /**
     * 权证出库类型 04-权证借阅（诉讼）
     **/
    public static final String STD_WARRANT_OUT_TYPE_04 = "04";
    /**
     * 权证出库类型 05-权证借阅（非诉讼）
     **/
    public static final String STD_WARRANT_OUT_TYPE_05 = "05";
    /**
     * 权证出库类型 06-其他
     **/
    public static final String STD_WARRANT_OUT_TYPE_06 = "06";


    /**
     * 出库类型 01-结清出库
     **/
    public static final String STD_OUT_TYPE_01 = "01";
    /**
     * 出库类型 01-非结清出库
     **/
    public static final String STD_OUT_TYPE_02 = "02";

    /**
     * 入库申请类型 01-本地纸质权证
     **/
    public static final String STD_APP_TYPE_GUAR_WARRANT_01 = "01";
    /**
     * 入库申请类型 02-本地有价证券及异地纸质权证
     **/
    public static final String STD_APP_TYPE_GUAR_WARRANT_02 = "02";

    public static final String BIZ_TYPE_IQP_INTEREST_PLAN = "IQP_REPAY_INTEREST_CHG";

    /**
     * 定义操作标识符  onStorage-权证待入库
     **/
    public static final String OPT_TYPE_ON_STORAGE = "onStorage";
    /**
     * 定义操作标识符  onOutBound-权证正常出库
     **/
    public static final String OPT_TYPE_ON_OUTBOUND = "onOutBound";
    /**
     * 定义操作标识符  onReturn-权证归还入库
     **/
    public static final String OPT_TYPE_ON_RETURN = "onReturn";
    /**
     * 定义操作标识符  onRenewal-换证入库
     **/
    public static final String OPT_TYPE_ON_RENEWAL = "onRenewal";
    /**
     * 定义操作标识符  onView-权证查看
     **/
    public static final String OPT_TYPE_ON_VIEW = "onView";

    /**
     * 权证状态    待入库
     **/
    public static final String IQP_CERTI_STATU_01 = "01";
    /**
     * 权证状态    入库在途
     **/
    public static final String IQP_CERTI_STATU_02 = "02";
    /**
     * 权证状态    入库未记账
     **/
    public static final String IQP_CERTI_STATU_03 = "03";
    /**
     * 权证状态    入库已记账
     **/
    public static final String IQP_CERTI_STATU_04 = "04";
    /**
     * 权证状态    出借在途
     **/
    public static final String IQP_CERTI_STATU_05 = "05";
    /**
     * 权证状态    出借未记账
     **/
    public static final String IQP_CERTI_STATU_06 = "06";
    /**
     * 权证状态    出借已记账
     **/
    public static final String IQP_CERTI_STATU_07 = "07";
    /**
     * 权证状态    出库在途
     **/
    public static final String IQP_CERTI_STATU_08 = "08";
    /**
     * 权证状态    出库未记账
     **/
    public static final String IQP_CERTI_STATU_09 = "09";
    /**
     * 权证状态    出库已记账
     **/
    public static final String IQP_CERTI_STATU_10 = "10";
    /**
     * 权证状态    换证出库
     **/
    public static final String IQP_CERTI_STATU_11 = "11";
    /**
     * 权证状态    换证出库在途
     **/
    public static final String IQP_CERTI_STATU_12 = "12";

    /**
     * 权证出入库类型    正常出库
     **/
    public static final String IQP_INOUT_APPTYPE_01 = "01";
    /**
     * 权证出入库类型    出借
     **/
    public static final String IQP_INOUT_APPTYPE_02 = "02";
    /**
     * 权证出入库类型    换证出库
     **/
    public static final String IQP_INOUT_APPTYPE_03 = "03";
    /**
     * 权证出入库类型    取出还贷
     **/
    public static final String IQP_INOUT_APPTYPE_04 = "04";
    /**
     * 权证出入库类型    正常入库
     **/
    public static final String IQP_INOUT_APPTYPE_05 = "05";
    /**
     * 权证出入库类型    归还入库
     **/
    public static final String IQP_INOUT_APPTYPE_06 = "06";
    /**
     * 权证出入库类型    换证入库
     **/
    public static final String IQP_INOUT_APPTYPE_07 = "07";
    /**
     * 权证出入库类型    移行出库
     **/
    public static final String IQP_INOUT_APPTYPE_08 = "08";
    /**
     * 权证出入库类型    移行入库
     **/
    public static final String IQP_INOUT_APPTYPE_09 = "09";

    /**
     * 权证类型  01 权利证明（原始权证）
     **/
    public static final String CERTI_CATALOG_01 = "01";
    /**
     * 权证类型  02 他项权证
     **/
    public static final String CERTI_CATALOG_02 = "02";

    /**
     * 业务标识位 01-单笔单批业务申请，02-额度项下申请，03-特殊业务申请
     **/
    public static final String BIZ_TYPE_UQ = "02";
    /**
     * 业务标识位 01-单笔单批业务申请，02-额度项下申请，03-特殊业务申请
     **/
    public static final String BIZ_TYPE_SPECIL = "03";
    /**
     * 业务标识位 01-单笔单批业务申请，02-额度项下申请，03-特殊业务申请
     **/
    public static final String BIZ_TYPE_SIGNLE = "01";

    /**
     * 节点标识 '0': '授信审批通过前'
     **/
    public static final String NODE_SIGN_0 = "0";
    /**
     * 节点标识 '1': '授信审批通过后,合同签订前'
     **/
    public static final String NODE_SIGN_1 = "1";
    /**
     * 节点标识 '2': '合同签订后,放款审批通过前'
     **/
    public static final String NODE_SIGN_2 = "2";
    /**
     * 节点标识 '3': '放款审批通过后'
     **/
    public static final String NODE_SIGN_3 = "3";

    /**
     * 授信与业务关系类型 00- 自有额度
     **/
    public static final String IQP_LMT_REL_TYPE_SELF = "00";
    /**
     * 授信与业务关系类型 01- 商圈
     **/
    public static final String IQP_LMT_REL_TYPE_SQ = "01";
    /**
     * 授信与业务关系类型 02- 担保公司
     **/
    public static final String IQP_LMT_REL_TYPE_FIN = "02";
    /**
     * 授信与业务关系类型 03- 合作方
     **/
    public static final String IQP_LMT_REL_TYPE_COOPER = "03";

    /**
     * 贷款形式 1-新增贷款
     **/
    public static final String IQP_LOAN_MODAL_1 = "1";
    /**
     * 贷款形式 2-收回再贷
     **/
    public static final String IQP_LOAN_MODAL_2 = "2";
    /**
     * 贷款形式 3-借新还旧
     **/
    public static final String IQP_LOAN_MODAL_3 = "3";
    /**
     * 贷款形式 4-资产重组
     **/
    public static final String IQP_LOAN_MODAL_4 = "4";
    /**
     * 贷款形式 5-转入
     **/
    public static final String IQP_LOAN_MODAL_5 = "5";
    /**
     * 贷款形式 6-其他
     **/
    public static final String IQP_LOAN_MODAL_6 = "6";

    /**
     * 借据关系数据字典  1-续贷通
     **/
    public static final String IQP_BILL_REL_1 = "1";
    /**
     * 借据关系数据字典  2-收回再贷
     **/
    public static final String IQP_BILL_REL_2 = "2";
    /**
     * 借据关系数据字典  3-借新还旧
     **/
    public static final String IQP_BILL_REL_3 = "3";
    /**
     * 借据关系数据字典  4-展期
     **/
    public static final String IQP_BILL_REL_4 = "4";
    /**
     * 借据关系数据字典  5-转入
     **/
    public static final String IQP_BILL_REL_5 = "5";

    /**
     * 合同状态标识位  100-未生效
     **/
    public static final String IQP_CONT_STS_100 = "100";
    /**
     * 合同状态标识位  200-生效
     **/
    public static final String IQP_CONT_STS_200 = "200";
    /**
     * 合同状态标识位  500-中止
     **/
    public static final String IQP_CONT_STS_500 = "500";
    /**
     * 合同状态标识位  600-注销
     **/
    public static final String IQP_CONT_STS_600 = "600";
    /**
     * 合同状态标识位 700-撤回
     **/
    public static final String IQP_CONT_STS_700 = "700";
    /**
     * 合同状态标识位  800-作废
     **/
    public static final String IQP_CONT_STS_800 = "800";

    /**
     * 借据台账状态 0-放款未确认
     **/
    public static final String IQP_ACC_STATUS_0 = "0";
    /**
     * 借据台账状态 1-正常
     **/
    public static final String IQP_ACC_STATUS_1 = "1";
    /**
     * 借据台账状态 8-核销
     **/
    public static final String IQP_ACC_STATUS_8 = "8";
    /**
     * 借据台账状态 9-结清
     **/
    public static final String IQP_ACC_STATUS_9 = "9";

    /**
     * 第三方接口查询类型  thQuery-第三方额度查询
     **/
    public static final String TH_QUERY_TYPE_1 = "thQuery";

    /**
     * 业务申请期限类型  年-001
     **/
    public static final String IQP_TERM_TYPE_YEAR = "001";
    /**
     * 业务申请期限类型  月-002
     **/
    public static final String IQP_TERM_TYPE_MONTH = "002";
    /**
     * 业务申请期限类型  日-003
     **/
    public static final String IQP_TERM_TYPE_DAY = "003";

    /**
     * 贷款5级分类标志 10 正常
     **/
    public static final String LOAN_FIVE_CLASS = "10";

    /**
     * 是否复议字段  Y-是
     **/
    public static final String IS_RESONSID_Y = "Y";
    /**
     * 是否复议字段  N-否
     **/
    public static final String IS_RESONSID_N = "N";

    /**
     * 特殊业务申请  特殊业务类型-1-续通贷
     **/
    public static final String SPECIAL_BIZ_TYPE_1 = "1";
    /**
     * 特殊业务申请  特殊业务类型-2-借新还旧
     **/
    public static final String SPECIAL_BIZ_TYPE_2 = "2";

    /**
     * 第三方额度校验  校验阶段  新增
     **/
    public static final String TH_LMT_CHECK_STAGE_INSERT = "insert";
    /**
     * 第三方额度校验  校验阶段  修改
     **/
    public static final String TH_LMT_CHECK_STAGE_UPDATE = "update";


    /**
     * 保存操作标志位  保存-save
     **/
    public static final String SAVE_OP_FLAG_SAVE = "save";
    /**
     * 保存操作标志位  暂存-tempSave
     **/
    public static final String SAVE_OP_FLAG_TEMP = "tempSave";
    /**
     * 保存操作标志位  提交-commit
     **/
    public static final String SAVE_OP_FLAG_COMMIT = "commit";

    /**
     * 审批通过生成合同数据-源表  主表表名  iqp_loan_app
     **/
    public static final String IQP_CTRLOANCONT_IQP_LOAN_APP = "iqp_loan_app";
    /**
     * 审批完成生成合同数据-源表  辅助表   iqp_loan_app_assist
     **/
    public static final String IQP_CTRLOANCONT_IQP_LOAN_APP_ASSIST = "iqp_loan_app_assist";
    /**
     * 审批完成生成合同数据-源表 还款表   iqp_loan_app_repay
     **/
    public static final String IQP_CTRLOANCONT_IQP_LOAN_APP_REPAY = "iqp_loan_app_repay";
    /**
     * 审批完成生成合同数据-目标表 合同主表   ctr_loan_cont
     **/
    public static final String IQP_CTR_LOAN_CONT = "ctr_loan_cont";

    /**
     * 审批通过需要操作的业务子表  保证金表 com_bail
     **/
    public static final String IQP_SUBLIST_COM_BAIL = "com_bail";
    /**
     * 审批通过需要操作的业务子表  账户信息表 iqp_acct
     **/
    public static final String IQP_SUBLIST_IQP_ACCT = "iqp_acct";
    /**
     * 审批通过需要操作的业务子表  业务申请人关系表 iqp_appl_appt_rel
     **/
    public static final String IQP_SUBLIST_IQP_AAR = "iqp_appl_appt_rel";
    /**
     * 审批通过需要操作的业务子表  借据关系表 iqp_bill_rel
     **/
    public static final String IQP_SUBLIST_BILL_REL = "iqp_bill_rel";
    /**
     * 审批通过需要操作的业务子表  iqp_car  车产表
     **/
    public static final String IQP_SUBLIST_IQP_CAR = "iqp_car";
    /**
     * 审批通过需要操作的业务子表  iqp_csgn  委托贷款
     **/
    public static final String IQP_SUBLIST_IQP_CSGN = "iqp_csgn";
    /**
     * 审批通过需要操作的业务子表  iqp_guar_biz_rel_app  担保合同与业务关系表
     **/
    public static final String IQP_SUBLIST_IQP_GBRA = "iqp_guar_biz_rel_app";
    /**
     * 审批通过需要操作的业务子表  iqp_house  房产信息
     **/
    public static final String IQP_SUBLIST_IQP_HOUSE = "iqp_house";
    /**
     * 审批通过需要操作的业务子表  iqp_lmt_rel  业务与授信/第三方额度关系表
     **/
    public static final String IQP_SUBLIST_IQP_LMT_REL = "iqp_lmt_rel";
    /**
     * 审批通过需要操作的业务子表  grt_guar_biz_rst_rel  担保和业务关系结果表
     **/
    public static final String IQP_SUBLIST_GRT_GUAR_BIZ = "grt_guar_biz_rst_rel";
    /**
     * 审批通过需要操作的业务子表  ctr_lmt_rel  授信与合同关联表(包括授信与第三方额度)
     **/
    public static final String IQP_SUBLIST_CTR_LMT_REL = "ctr_lmt_rel";

    /**
     * 支付方式 0-自主支付
     **/
    public static final String PAY_WAY_INDEPENTMENTPAYMENT = "0";
    /**
     * 支付方式 1-受托支付
     **/
    public static final String PAY_WAY_ENTRUSTEDPAYMENT = "1";

    /**
     * 账户属性 1-放款账号
     **/
    public static final String ID_ATTR_PVP = "1";
    /**
     * 账户属性 2-借款人印花税账号
     **/
    public static final String ID_ATTR_BSD = "2";
    /**
     * 账户属性 3-还款账号
     **/
    public static final String ID_ATTR_REPAY = "3";
    /**
     * 账户属性 4-受托支付账号
     **/
    public static final String ID_ATTR_EP = "4";
    /**
     * 账户属性 5-委托人印花税账号
     **/
    public static final String ID_ATTR_PSD = "5";
    /**
     * 账户属性 6-费用账号
     **/
    public static final String ID_ATTR_FEE = "6";

    /**
     * 审批模式 - 全流程状态  00-待发起
     **/
    public static final String WF_STATUS_00 = "00";
    /**
     * 审批模式 - 全流程状态  01-审批中
     **/
    public static final String WF_STATUS_01 = "01";
    /**
     * 审批模式 - 全流程状态  02-审批通过
     **/
    public static final String WF_STATUS_02 = "02";
    /**
     * 审批模式 - 全流程状态  03-审批拒绝
     **/
    public static final String WF_STATUS_03 = "03";
    /**
     * 审批模式 - 全流程状态  04-已签约
     **/
    public static final String WF_STATUS_04 = "04";
    /**
     * 审批模式 - 全流程状态  05-放款审批中
     **/
    public static final String WF_STATUS_05 = "05";
    /**
     * 审批模式 - 全流程状态  06-放款审批通过
     **/
    public static final String WF_STATUS_06 = "06";
    /**
     * 审批模式 - 全流程状态  07-放款审批拒绝
     **/
    public static final String WF_STATUS_07 = "07";
    /**
     * 审批模式 - 全流程状态  08-放款记账成功
     **/
    public static final String WF_STATUS_08 = "08";
    /**
     * 审批模式 - 全流程状态  09-放款记账未明
     **/
    public static final String WF_STATUS_09 = "09";
    /**
     * 审批模式 - 全流程状态  10-放款记账失败
     **/
    public static final String WF_STATUS_10 = "10";
    /**
     * 审批模式 - 全流程状态  11-冲账成功
     **/
    public static final String WF_STATUS_11 = "11";
    /**
     * 审批模式 - 全流程状态  12-冲账未明
     **/
    public static final String WF_STATUS_12 = "12";


    /**
     * 十二级分类 A1-正常一级
     **/
    public static final String TWELVE_CLASS_A1 = "A1";
    /**
     * 十二级分类 A2-正常二级
     **/
    public static final String TWELVE_CLASS_A2 = "A2";
    /**
     * 十二级分类 A3-正常三级
     **/
    public static final String TWELVE_CLASS_A3 = "A3";
    /**
     * 十二级分类 A4-正常四级
     **/
    public static final String TWELVE_CLASS_A4 = "A4";
    /**
     * 十二级分类 B1-关注一级
     **/
    public static final String TWELVE_CLASS_B1 = "B1";
    /**
     * 十二级分类 B2-关注二级
     **/
    public static final String TWELVE_CLASS_B2 = "B2";
    /**
     * 十二级分类 B3-关注三级
     **/
    public static final String TWELVE_CLASS_B3 = "B3";
    /**
     * 十二级分类 C1-次级一级
     **/
    public static final String TWELVE_CLASS_C1 = "C1";
    /**
     * 十二级分类 C2-次级二级
     **/
    public static final String TWELVE_CLASS_C2 = "C2";
    /**
     * 十二级分类 D1-可疑一级
     **/
    public static final String TWELVE_CLASS_D1 = "D1";
    /**
     * 十二级分类 D2-可疑二级
     **/
    public static final String TWELVE_CLASS_D2 = "D2";
    /**
     * 十二级分类 E-损失级
     **/
    public static final String TWELVE_CLASS_E = "E";

    /**
     * 授权状态  00-未授权
     **/
    public static final String AUTH_STATUS_00 = "00";
    /**
     * 授权状态  01-授权失败
     **/
    public static final String AUTH_STATUS_01 = "01";
    /**
     * 授权状态  02-已授权
     **/
    public static final String AUTH_STATUS_02 = "02";
    /**
     * 授权状态  03-授权已撤销
     **/
    public static final String AUTH_STATUS_03 = "03";
    /**
     * 授权状态  04-失败已重发
     **/
    public static final String AUTH_STATUS_04 = "04";

    /**
     * 授权类型  01-出账
     **/
    public static final String AUTHORIZE_TYPE_01 = "01";
    /**
     * 授权类型  02-受托支付
     **/
    public static final String AUTHORIZE_TYPE_02 = "02";
    /**
     * 授权类型  03-押品记账
     **/
    public static final String AUTHORIZE_TYPE_03 = "03";
    /**
     * 授权类型  04-展期业务
     **/
    public static final String AUTHORIZE_TYPE_04 = "04";
    /**
     * 授权类型  05-账号变更
     **/
    public static final String AUTHORIZE_TYPE_05 = "05";
    /**
     * 授权类型  06-利率调整
     **/
    public static final String AUTHORIZE_TYPE_06 = "06";
    /**
     * 授权类型  07-期限信息调整
     **/
    public static final String AUTHORIZE_TYPE_07 = "07";
    /**
     * 授权类型  08-还款方式变更
     **/
    public static final String AUTHORIZE_TYPE_08 = "08";
    /**
     * 授权类型  09-还款间隔周期变更
     **/
    public static final String AUTHORIZE_TYPE_09 = "09";
    /**
     * 授权类型  10-还款计划变更
     **/
    public static final String AUTHORIZE_TYPE_10 = "10";
    /**
     * 授权类型  11-利息减免
     **/
    public static final String AUTHORIZE_TYPE_11 = "11";
    /**
     * 授权类型  12-还款日
     **/
    public static final String AUTHORIZE_TYPE_12 = "12";
    /**
     * 授权类型  13-退汇修改
     **/
    public static final String AUTHORIZE_TYPE_13 = "13";

    /**
     * 是否立即发起受托支付  Y-是
     **/
    public static final String IS_CFIRM_PAY_Y = "Y";
    /**
     * 是否立即发起受托支付  N-否
     **/
    public static final String IS_CFIRM_PAY_N = "N";

    /**
     * 未受托支付类型  01-退汇
     **/
    public static final String UNTRU_PAY_TYPE_01 = "01";
    /**
     * 未受托支付类型  02-非立即受托支付
     **/
    public static final String UNTRU_PAY_TYPE_02 = "02";
    /**
     * 未受托支付类型  03-超时未支付
     **/
    public static final String UNTRU_PAY_TYPE_03 = "03";


    /**
     * 展期协议状态  001-未生效
     **/
    public static final String EXT_CTR_STATUS_001 = "001";
    /**
     * 展期协议状态  002-生效
     **/
    public static final String EXT_CTR_STATUS_002 = "002";
    /**
     * 展期协议状态  003-作废
     **/
    public static final String EXT_CTR_STATUS_003 = "003";
    /**
     * 展期协议状态  004-撤销
     **/
    public static final String EXT_CTR_STATUS_004 = "004";

    /**
     * 婚姻状态 02-已婚
     **/
    public static final String MARR_STAT_02 = "02";
    /**
     * 共借人关系类型  00-主借人
     **/
    public static final String IQP_APPL_APPT_REL_00 = "00";
    /**
     * 共借人关系类型  01-共借人
     **/
    public static final String IQP_APPL_APPT_REL_01 = "01";
    /**
     * 共借人关系类型  02-主借人配偶
     **/
    public static final String IQP_APPL_APPT_REL_02 = "02";
    /**
     * 共借人关系类型  03-共借人配偶
     **/
    public static final String IQP_APPL_APPT_REL_03 = "03";


    /**
     * 额度协议状态 001-未生效
     **/
    public static final String CTR_STATUS_001 = "001";
    /**
     * 额度协议状态 002-生效
     **/
    public static final String CTR_STATUS_002 = "002";
    /**
     * 额度协议状态 003-已注销
     **/
    public static final String CTR_STATUS_003 = "003";

    /**
     * 账户状态  1-正常
     **/
    public static final String ACCT_STATUS_1 = "1";
    /**
     * 账户状态  2-作废
     **/
    public static final String ACCT_STATUS_2 = "2";

    /**
     * 状态 0-失效
     **/
    public static final String OPR_STATUS_0 = "0";
    /**
     * 状态 1-生效
     **/
    public static final String OPR_STATUS_1 = "1";


    /**
     * 合作方类型 001-房地产开发商
     **/
    public static final String COOP_TYPE_001 = "001";
    /**
     * 合作方类型 002-汽车经销商
     **/
    public static final String COOP_TYPE_002 = "002";
    /**
     * 合作方类型 003-购置机械设备担保
     **/
    public static final String COOP_TYPE_003 = "003";
    /**
     * 合作方类型 004-核心企业担保
     **/
    public static final String COOP_TYPE_004 = "004";
    /**
     * 合作方类型 005-其他合作方
     **/
    public static final String COOP_TYPE_005 = "005";
    /**
     * 合作方类型 006-保险公司
     **/
    public static final String COOP_TYPE_006 = "006";

    /**
     * 保证金来源  0-无
     **/
    public static final String BAIL_SOUR_0 = "0";
    /**
     * 保证金来源  1-自有保证金
     **/
    public static final String BAIL_SOUR_1 = "1";
    /**
     * 保证金来源  2-担保公司保证金
     **/
    public static final String BAIL_SOUR_2 = "2";

    /**
     * 担保合同类型  最高额担保合同  01
     **/
    public static final String GTR_CONT_TYPE_MAX = "01";
    /**
     * 担保合同类型  一般担保合同  00
     **/
    public static final String GTR_CONT_TYPE_NORMAL = "00";


    /**
     * 担保合同操作标识位-importN 一般担保合同引入
     **/
    public static final String OPT_TYPE_IMPORTN = "importN";
    /**
     * 担保合同操作标识位-importMax 最高额担保合同引入
     **/
    public static final String OPT_TYPE_IMPORTMAX = "importMax";
    /**
     * 担保合同操作标识位-updateMax 最高额页面修改
     **/
    public static final String OPT_TYPE_UPDATEM = "updateMax";
    /**
     * 担保合同操作标识位-addN 一般担保合同新增
     **/
    public static final String OPT_TYPE_ADDN = "addN";
    /**
     * 担保合同操作标识位-updateN 一般担保合同修改
     **/
    public static final String OPT_TYPE_UPDATEN = "updateN";
    /**
     * 担保合同操作标识位-SIGN 担保签订
     **/
    public static final String OPT_TYPE_SIGN = "SIGN";
    /**
     * 担保合同操作标识位-onLogout 担保注销
     **/
    public static final String OPT_TYPE_ONLOGOUT = "onLogout";
    /**
     * 担保合同操作标识位-onLogout 担保中止
     **/
    public static final String OPT_TYPE_ONSUSPEND = "onSuspend";
    /**
     * 担保合同操作标识位-onLogout 担保作废
     **/
    public static final String OPT_TYPE_ONINVALID = "onInvalid";
    /**
     * 担保合同操作标识位-onLogout 担保撤回
     **/
    public static final String OPT_TYPE_ONRECALL = "onRecall";
    /**
     * 担保合同操作标识位-onPrint 担保打印
     **/
    public static final String OPT_TYPE_ONPRINT = "onPrint";
    /**
     * 担保合同页面 业务类型  bizTyp  IQP-业务申请
     **/
    public static final String BIZ_TYP_IQP = "IQP";
    /**
     * 担保合同页面 业务类型  bizTyp  CHG-担保变更申请
     **/
    public static final String BIZ_TYP_CHG = "CHG";
    /**
     * 担保合同页面 业务类型  bizTyp  LMT-授信申请
     **/
    public static final String BIZ_TYP_LMT = "LMT";

    /**
     * 是否阶段担保  N-否
     **/
    public static final String IS_PER_GUR_N = "N";
    /**
     * 是否阶段担保  Y-是
     **/
    public static final String IS_PER_GUR_Y = "Y";

    /**
     * 映射关系表名 额度台账
     **/
    public static final String LMT_ACC_TABLE_NAME = "lmt_acc";
    /**
     * 映射关系表名 额度分项
     **/
    public static final String LMT_SUB_TABLE_NAME = "lmt_sub";
    /**
     * 映射关系表名 额度申请
     **/
    public static final String LMT_INDIV_APP_TABLE_NAME = "lmt_indiv_app";
    /**
     * 映射关系表名 额度协议
     **/
    public static final String LMT_CTR_TABLE_NAME = "lmt_ctr";

    /**
     * 额度类型 01-新增
     **/
    public static final String LMT_TYPE_01 = "01";
    /**
     * 额度类型 02-续授信
     **/
    public static final String LMT_TYPE_02 = "02";
    /**
     * 额度类型 03-新增复议
     **/
    public static final String LMT_TYPE_03 = "03";
    /**
     * 额度类型 04-续授信复议
     **/
    public static final String LMT_TYPE_04 = "04";

    /**
     * 额度协议状态  001-预生效
     **/
    public static final String AGR_STATUS_001 = "001";
    /**
     * 额度协议状态  002-生效
     **/
    public static final String AGR_STATUS_002 = "002";
    /**
     * 额度协议状态  003-注销
     **/
    public static final String AGR_STATUS_003 = "003";
    /**
     * 额度协议状态  004-撤销
     **/
    public static final String AGR_STATUS_004 = "004";

    /**
     * 额度台账状态 001-预生效
     **/
    public static final String ACC_STATUS_001 = "001";
    /**
     * 额度台账状态 002-生效
     **/
    public static final String ACC_STATUS_002 = "002";
    /**
     * 额度台账状态 003-注销
     **/
    public static final String ACC_STATUS_003 = "003";

    /**
     * 额度分项类型  limit_type  01-循环额度
     **/
    public static final String LIMIT_TYPE_01 = "01";
    /**
     * 额度分项类型  limit_type  02-一次性额度
     **/
    public static final String LIMIT_TYPE_02 = "02";

    /**
     * 额度分项类别  STD_ZB_LMT_SUB_TYP  01-一般授信
     **/
    public static final String STD_ZB_LMT_SUB_TYP_01 = "01";

    /**
     * 额度期限类型  lmt_term_type  年-001
     **/
    public static final String LMT_TERM_TYPE_001 = "001";
    /**
     * 额度期限类型  lmt_term_type  月-002
     **/
    public static final String LMT_TERM_TYPE_002 = "002";
    /**
     * 额度期限类型  lmt_term_type  日-003
     **/
    public static final String LMT_TERM_TYPE_003 = "003";

    /**
     * 低风险业务类型  10-低风险
     **/
    public static final String LOW_RISK_TYPE_10 = "10";
    /**
     * 低风险业务类型  20-非风险
     **/
    public static final String LOW_RISK_TYPE_20 = "20";

    /**
     * 是否被拒绝  Y-是
     **/
    public static final String IS_REJECT_Y = "Y";
    /**
     * 是否被拒绝  N-否
     **/
    public static final String IS_REJECT_N = "N";

    /**
     * 放款方式 STD_ZB_PUTOUT_TYP 1 自助放款
     **/
    public static final String STD_ZB_PUTOUT_TYP_1 = "1";
    /**
     * 放款方式 STD_ZB_PUTOUT_TYP 2 柜面放款
     **/
    public static final String STD_ZB_PUTOUT_TYP_2 = "2";

    public static final String GGCR_OPRTYPE_ADD = "01";//新增操作标识位
    public static final String GGCR_OPRTYPE_DELETE = "02";//删除操作标识位

    /**
     * 状态
     **/
    public static final String GGCR_STATUS_EFFCT = "1";//有效
    public static final String GGCR_STATUS_INVALID = "0";//无效


    /**
     * 放款申请-数据源表
     **/
    public static final String APPLY_SOURCE_TABLE_NAME = "ctr_loan_cont";
    /**
     * 放款申请-数据目标表
     **/
    public static final String APPLY_DIST_TABLE_NAME = "pvp_loan_app";
    /**
     * 额度关系数据为空提示消息
     **/
    public static final String PVP_QUERY_LMTRELNULL_MSG = "额度关系数据不存在！";
    /**
     * 放款到期日校验-合同到期日后的月数参数值
     **/
    public static final String PVP_END_DATE_DEF_CONT_MONTH = "6";

    /**
     * 数据流映射  账户表名  iqp_acct
     **/
    public static final String PVP_IQP_ACCT_TABLE_NAME = "iqp_acct";
    /**
     * 数据流映射  账号信息与借据关系表  iqp_acct_no_bill_rel
     **/
    public static final String PVP_IQP_ACCT_NO_BILL_REL_TABLE_NAME = "iqp_acct_no_bill_rel";
    /**
     * 未受托表-支付状态  00-未支付
     **/
    public static final String PVP_TRU_PAY_INFO_PAY_STATUS_00 = "00";
    /**
     * 未受托表-支付状态  01-支付失败
     **/
    public static final String PVP_TRU_PAY_INFO_PAY_STATUS_01 = "01";
    /**
     * 未受托表-支付状态  02-支付成功
     **/
    public static final String PVP_TRU_PAY_INFO_PAY_STATUS_02 = "02";
    /**
     * 未受托表-支付状态  03-支付已重发
     **/
    public static final String PVP_TRU_PAY_INFO_PAY_STATUS_03 = "03";

    /**
     * 数字0
     **/
    public static final String NUM_ZERO = "0";

    /**
     * 数字1
     */
    public static final Integer INT_ONE = 1;

    /**
     * 数字2
     */
    public static final Integer INT_TWO = 2;

    /**
     * CODE_YXD 优享贷
     */
    public static final String CODE_YXD = "PW010004";

    /**
     * CODE_ZXD 增享贷
     */
    public static final String CODE_ZXD = "SC010014";
    /**
     * CODE_HXD
     */
    public static final String CODE_HXD = "SC060001";
    /**
     * string 360
     */
    public static final String NUM_360 = "360";

    /**
     * --已结清
     **/
    public static final String CODE_JQ = "01";
    /**
     * 所有
     **/
    public static final String CODE_ALL = "02";

    /**
     * XDXW0057:queryType 1-批复额度
     **/
    public static final String LMT_1 = "1";

    /**
     * XDXW0057:queryType 2-不存在关联生效合同的批复额度
     **/
    public static final String LMT_2 = "2";

    /**
     * xdxw0064查询类型-按流水号查询
     **/
    public static final String CX_LSH = "01";
    /**
     * xdxw0064查询类型-按证件号查询
     **/
    public static final String CX_ZJH = "02";

    /**
     * 抵押权人-姓名/名称
     */
    public static final String MONAME = "江苏张家港农村商业银行股份有限公司";
    /**
     * 抵押权人-证件类型-代码
     */
    public static final String DOTYCO = "7";
    /**
     * 抵押权人-证件类型-名称
     */
    public static final String DOTYNA = "营业执照";
    /**
     * 抵押权人-证件号
     */
    public static final String MODOMU = "91320000732252238K";
    /**
     * 抵押权人-法人/负责人
     */
    public static final String MONALR = "季颖";
    /**
     * 抵押权人-电话
     */
    public static final String MOPHON = "0512-56968081";
    /**
     * 抵押权人代理人-证件类型-代码
     */
    public static final String MOAGTC = "1";
    /**
     * 抵押权人代理人-证件类型-名称
     */
    public static final String MOAGTN = "身份证";

    /**
     * 押品编号前两个字段 YP是注销, DW、DY是新建
     */
    public static final String DW = "DW";
    public static final String DY = "DY";
    public static final String YP = "YP";

    /**
     * 期限类型 年
     */
    public static final String STD_TERM_TYPE_Y = "Y";
    /**
     * 期限类型 月
     */
    public static final String STD_TERM_TYPE_M = "M";
    /**
     * 期限类型 日
     */
    public static final String STD_TERM_TYPE_D = "D";


    /**
     * STD_ZB_ORG_ADMIT_TYPE:申请类型
     */
    public static final class STD_ZB_ORG_ADMIT_TYPE {
        /**
         * 新增
         */
        public static final String ADD = "01";
        /**
         * 年审
         */
        public static final String YEAR_VERIFY = "02";
        /**
         * 调整
         */
        public static final String ADJUST = "03";
    }

    /**
     * 主键id
     */
    public static final String PK_VALUE = "PK_VALUE";

    /**
     * 同业机构序列号
     */
    public static final String BIZ_SERNO = "BIZ_SERNO";

    /**
     * 列表类型
     */
    public static final class LIST_TYPE {
        /**
         * 申请列表
         */
        public static final String APPLY = "01";
        /**
         * 历史列表
         */
        public static final String HISTORY = "02";
    }

    /**
     * 列表展示申请类型
     */
    public static final class INVEST_SHOW_TYPE {
        /**
         * 复议、新增、续作
         */
        public static final String APPLY = "01";
        /**
         * 变更
         */
        public static final String CHANGE = "02";
    }

    /**
     * 担保方式（担保类型）
     */
    public static final class STD_ZB_ASSURE_MEANS {
        /**
         * 信用
         */
        public static final String XIN_YONG = "00";
        /**
         * 抵押
         */
        public static final String DI_YA = "10";
        /**
         * 质押
         */
        public static final String ZHI_YA = "20";
        /**
         * 低风险质押
         */
        public static final String FI_FENG_XIAN_ZHI_YA = "21";
        /**
         * 保证
         */
        public static final String BAO_ZHENG = "30";
        /**
         * 全额保证金
         */
        public static final String QUAN_E_BAO_ZHENG_JIN = "40";

        /**
         * 共同借款人
         */
        public static final String STD_ACCUSED_ROLE_01 = "01";

        /**
         * 线下担保人
         */
        public static final String STD_ACCUSED_ROLE_02 = "02";


    }
    /*************** 授信品种编号 ***************/
    /**
     * 债券池
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4001 = "4001";
    /**
     * 债券投资
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4002 = "4002";
    /**
     * 资产-其他
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4003 = "4003";
    /**
     * 资产-债权融资计划、理财直融工具
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4004 = "4004";
    /**
     * 净值型产品
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4005 = "4005";
    /**
     * 资产证券化产品（标准）
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4006 = "4006";
    /**
     * 资产证券化产品（非标）
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4007 = "4007";
    /**
     * 其他标准化债权投资
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4008 = "4008";
    /**
     * 债务融资工具（投资）
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4009 = "4009";
    /**
     * 理财直融工具（投资）
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4010 = "4010";
    /**
     * 结构化融资
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4011 = "4011";
    /**
     * 其他非标债权投资
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_4012 = "4012";
    /**
     * 16010101	债权融资计划（承销）
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_16010101 = "16010101";
    /**
     * 16020101	理财直融工具（承销）
     */
    public static final String STD_ZB_PRD_BIZ_TYPE_16020101 = "16020101";

    /******************************/
    /************** 额度分类 *****************/
    /**
     * 资金业务存量授信
     */
    public static final String STD_ZB_LMT_STATS_TYPE_11 = "11";
    /**
     * 债券投资存量业务
     */
    public static final String STD_ZB_LMT_STATS_TYPE_13 = "13";
    /**
     * 敞口存量授信
     */
    public static final String STD_ZB_LMT_STATS_TYPE_21 = "21";
    /**
     * 低风险存量授信
     */
    public static final String STD_ZB_LMT_STATS_TYPE_22 = "22";

    /******************** 高管类别(同业) ****************************/

    /**
     * 法定代表人
     */
    public static final String STD_ZB_INTBANK_MRG_CLS_01 = "01";
    /**
     * 控制人
     */
    public static final String STD_ZB_INTBANK_MRG_CLS_02 = "02";
    /**
     * 经办人
     */
    public static final String STD_ZB_INTBANK_MRG_CLS_03 = "03";
    /**
     * 受益人
     */
    public static final String STD_ZB_INTBANK_MRG_CLS_04 = "04";

    /********************** 控制人类型(同业高管) ************************/
    /**
     * 实际控制人
     */
    public static final String STD_TYPE_CONTROLLER_01 = "01";

    /**
     * 控股方
     */
    public static final String STD_TYPE_CONTROLLER_02 = "02";

    /****************************  客户大类 **************************/
    /**
     * 对私
     */
    public static final String STD_ZB_CUS_CATALOG_1 = "1";
    /**
     * 对公 (即法人客户)
     */
    public static final String STD_ZB_CUS_CATALOG_2 = "2";
    /**
     * 同业
     */
    public static final String STD_ZB_CUS_CATALOG_3 = "3";
    /**
     * 集团
     */
    public static final String STD_ZB_CUS_CATALOG_4 = "4";

    /************************ 批复台账状态 *************************/
    /**
     * 生效
     */
    public static final String STD_REPLY_STATUS_01 = "01";
    /**
     * 失效
     */
    public static final String STD_REPLY_STATUS_02 = "02";

    /************************ 授信期限 ***************************/
    /**
     * 授信期限 12
     */
    public static final Integer LMT_TERM_12 = 12;
    /**
     * 授信期限 3
     */
    public static final Integer LMT_TERM_3 = 3;
    /******************************************************/

    /************************ 产品债项评级结果 ********************/
    /**
     * 产品债项评级结果  AAA
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_001 = "001";
    /**
     * 产品债项评级结果  AA+
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_002 = "002";
    /**
     * 产品债项评级结果  AA
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_003 = "003";
    /**
     * 产品债项评级结果  AA-
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_004 = "004";
    /**
     * 产品债项评级结果  A+
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_005 = "005";
    /**
     * 产品债项评级结果  A
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_006 = "006";
    /**
     * 产品债项评级结果  A-
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_007 = "007";
    /**
     * 产品债项评级结果  BBB+
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_008 = "008";
    /**
     * 产品债项评级结果  BBB
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_009 = "009";
    /**
     * 产品债项评级结果  BBB-
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_010 = "010";
    /**
     * 产品债项评级结果  BB
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_011 = "011";
    /**
     * 产品债项评级结果  B
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_012 = "012";
    /**
     * 产品债项评级结果  CCC
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_013 = "013";
    /**
     * 产品债项评级结果  CC
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_014 = "014";
    /**
     * 产品债项评级结果  C
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_015 = "015";
    /**
     * 产品债项评级结果  D
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_016 = "016";
    /**
     * 产品债项评级结果  无
     */
    public static final String STD_ZB_DEBT_EVAL_RESULT_017 = "017";

    /*************************  *****************************/

    /**
     * 可转债
     */
    public static final String STD_ZB_NORM_INVEST_TYPE_V = "V";
    /**
     * 置换票据(可交换债券)
     */
    public static final String STD_ZB_NORM_INVEST_TYPE_Z1 = "Z1";

    /*******************************************************/

    /**
     * 机构准入申请类型 03 调整
     */
    public static final String STD_ZB_ORG_ADMIT_TYPE_03 = "03";
    /**
     * 机构准入申请类型 02 年审
     */
    public static final String STD_ZB_ORG_ADMIT_TYPE_02 = "02";
    /**
     * 机构准入申请类型 01 准入
     */
    public static final String STD_ZB_ORG_ADMIT_TYPE_01 = "01";


    /*********************************************************/

    /**
     * 币种 CNY 人民币元
     */
    public static final String STD_ZB_CUR_TYP_CNY = "CNY";

    /**
     * 核查报告模式	1	使用系统核查报告填写
     */
    public static final String STD_INDGT_REPORT_MODE_1 = "1";

    /**
     * 核查报告模式	2	上传WORD版核查报告
     */
    public static final String STD_INDGT_REPORT_MODE_2 = "2";

    public static final String STD_ZB_YES_NO_Y = "1";
    public static final String STD_ZB_YES_NO_N = "0";

    /**
     * [{"key":"02","value":"授信变更"},{"key":"08","value":"额度调剂"},{"key":"06","value":"授信再议"},{"key":"01","value":"授信新增"},
     * {"key":"07","value":"预授信细化"},{"key":"03","value":"授信续作"},{"key":"04","value":"授信复审"},{"key":"05","value":"授信复议"}]
     */
    /**
     * 授信新增
     */
    public static final String STD_SX_LMT_TYPE_01 = "01";
    /**
     * 授信变更
     */
    public static final String STD_SX_LMT_TYPE_02 = "02";
    /**
     * 授信续作
     */
    public static final String STD_SX_LMT_TYPE_03 = "03";
    /**
     * 授信复议
     */
    public static final String STD_SX_LMT_TYPE_05 = "05";

    /**
     * 业务场景 1 授信申请
     */
    public static final String BIZ_SCENCE_1 = "1";
    /**
     * 业务场景 2 业务申请
     */
    public static final String BIZ_SCENCE_2 = "3";
    /**
     * 业务场景 3 出账申请
     */
    public static final String BIZ_SCENCE_3 = "3";


    /**
     * 授信批复合作方平台
     */
    public static final String XW_PLATFORM_1002 = "1002";

    /**
     * 渠道来源
     */
    public static final String XW_CHANNEL_TYPE_11 = "11";

    /**
     * 零售风控内部产品代码-惠享贷
     */
    public static final String LSFK_PRD_CODE_HXD = "1002000020";

    /**
     * 零售风控内部产品代码-优享贷
     */
    public static final String LSFK_PRD_CODE_YXD = "1002000019";

    /**
     * 批复状态 00 批复通过  01 批复拒绝   03 合同已签订
     */
    public static final String XW_APP_STATUS_00 = "00";
    public static final String XW_APP_STATUS_01 = "01";
    public static final String XW_APP_STATUS_03 = "03";

    /**
     * 底层资产状态 01 有效  02 无效
     */
    public static final String STD_ZB_STATUS_01 = "01";
    public static final String STD_ZB_STATUS_02 = "02";

    /**
     * 申请类型 01 存量周转
     * 02 新增
     * 03 存量周转加新增
     * 04 存量不周转新增
     * 05：不良处置落实
     * 06：不良处置周转
     * 07：不良处置借新还旧"
     */
    public static final String XW_APPLY_TYPE_00 = "00";
    public static final String XW_APPLY_TYPE_01 = "01";
    public static final String XW_APPLY_TYPE_02 = "02";
    public static final String XW_APPLY_TYPE_03 = "03";
    public static final String XW_APPLY_TYPE_04 = "04";
    public static final String XW_APPLY_TYPE_05 = "05";
    public static final String XW_APPLY_TYPE_06 = "06";
    public static final String XW_APPLY_TYPE_07 = "07";

    /**
     * 2021年5月20日14:52:28
     * 担保方式
     * 30 保证
     * 10 抵押
     * 20 质押
     * 00 信用
     * 21 低风险质押
     * 40 全额保证金
     */
    public static final String XW_GUARA_WAY_00 = "00";
    public static final String XW_GUARA_WAY_10 = "10";
    public static final String XW_GUARA_WAY_20 = "20";
    public static final String XW_GUARA_WAY_21 = "21";
    public static final String XW_GUARA_WAY_30 = "30";
    public static final String XW_GUARA_WAY_40 = "40";

    /**
     * 2021年5月20日14:52:28
     * 随机随还 is_sjsh
     * 01 否
     * 02 是
     */
    public static final String XW_IS_SJSH_01 = "01";
    public static final String XW_IS_SJSH_02 = "02";

    /**
     * 2021年5月20日14:52:28
     * 还款方式 repay_way
     * A001 按月结息，到期还本
     * A002 等额本息
     * A009 利随本清
     * A013 按月结息，按季还本
     * A014 按月结息，按半年还本
     * A015 按月结息，按年还本
     * A016 按月结息按226还本
     * A017 前6个月按月结息后6个月等额本息
     * A018 前4个月按月结息后8个月等额本息
     * A020 按月结息按334还本
     * A021 按月结息按433还本
     * A022 10年期等额本息
     * A031 5年期等额本息
     */
    public static final String XW_REPAY_WAY_A001 = "A001";
    public static final String XW_REPAY_WAY_A002 = "A002";
    public static final String XW_REPAY_WAY_A009 = "A009";
    public static final String XW_REPAY_WAY_A013 = "A013";
    public static final String XW_REPAY_WAY_A014 = "A014";
    public static final String XW_REPAY_WAY_A015 = "A015";
    public static final String XW_REPAY_WAY_A016 = "A016";
    public static final String XW_REPAY_WAY_A017 = "A017";
    public static final String XW_REPAY_WAY_A018 = "A018";
    public static final String XW_REPAY_WAY_A020 = "A020";
    public static final String XW_REPAY_WAY_A021 = "A021";
    public static final String XW_REPAY_WAY_A022 = "A022";
    public static final String XW_REPAY_WAY_A031 = "A031";
    /**
     * @创建人 WH
     * @创建时间 2021/5/21 16:58
     * @注释 所属条线 BelgLine
     * 01 小微
     * 02 零售
     * 03 对公
     */
    public static final String BELGLINE_XW_01 = "01";
    public static final String BELGLINE_LS_02 = "02";
    public static final String BELGLINE_DG_03 = "03";

    /**
     * XDHT0041查询受托记录状态
     * 1-非审批通过非否决
     * 2-审批通过
     */
    public static final String STATUS_CODE_1 = "1";
    public static final String STATUS_CODE_2 = "2";
    /**
     * @创建人 WH
     * @创建时间 2021/5/26 14:44
     * @注释 数据来源  00人工新增  01系统推送
     */
    public static final String STD_DATA_SOUR_00 = "00";
    public static final String STD_DATA_SOUR_01 = "01";
    /**
     * IrFloatType 利率调整方式
     * pr加点 00   加百分比 01
     */
    public static final String IR_FLOAT_TYPE_00 = "00";
    public static final String IR_FLOAT_TYPE_01 = "01";
    /**
     * @创建人 lixmXDXT0015
     * @创建时间 2021/6/05 14:44
     * @注释 数据来源  00未生效  01生效 02失效
     */
    public static final String STD_CUS_LIST_STATUS_00 = "00";
    public static final String STD_CUS_LIST_STATUS_01 = "01";
    public static final String STD_CUS_LIST_STATUS_02 = "02";

    /**
     * @创建人 lixm
     * @创建时间 2021/6/05 14:44
     * @注释 数据来源  0本期   1上期
     */
    public static final String STD_BELG_YEAR_0 = "0";
    public static final String STD_BELG_YEAR_1 = "1";

    /**
     * @创建人 zoubiao
     * @创建时间 2021/6/07 20:44
     * 操作类型
     * 01-签订前(生成pdf)
     * 02-签订后
     * 03-出账(更新)
     * 04-出账(查询)
     * 05-支用
     * 06-作废
     * 07-借据(查询)
     * 08-惠享贷共借人签约
     */
    public static final String XW_OP_TYPE_01 = "01";
    public static final String XW_OP_TYPE_02 = "02";
    public static final String XW_OP_TYPE_03 = "03";
    public static final String XW_OP_TYPE_04 = "04";
    public static final String XW_OP_TYPE_05 = "05";
    public static final String XW_OP_TYPE_06 = "06";
    public static final String XW_OP_TYPE_07 = "07";
    public static final String XW_OP_TYPE_08 = "08";

    /**
     * @创建人 yumeng
     * @创建时间 2021/6/08 16:00
     * 流水号key
     */
    public static final String KHGL06_SERNO = "serno";

    /**
     * @创建人 yumeng
     * @创建时间 2021/6/08 16:00
     * 操作类型key
     */
    public static final String KHGL06_OPR_TYPE = "oprType";

    /**
     * @创建人 yumeng
     * @创建时间 2021/6/08 16:00
     * 操作类型值
     */
    public static final String KHGL06_OPR_TYPE_VALUE = "01";

    /**
     * @创建人 yumeng
     * @创建时间 2021/6/08 16:00
     * 客户移交-客户移交
     */
    public static final String KHGL06_HANDOVER_MODE = "1";

    /**
     * 同业机构准入流程（TYSX01）出具审查报告节点
     * add by zhangjw 20210608
     */
    public static final String TYSX01_01 = "170_14,170_19,";
    /**
     * 同业机构准入流程（TYSX01）出具批复节点
     * add by zhangjw 20210608
     */
    public static final String TYSX01_03 = "170_20,";

    /**
     * 资金业务授信申报审批流程（TYSX02）出具审查报告节点
     * add by zhangjw 20210609
     */
    public static final String TYSX02_01 = "137_18,137_30,";
    /**
     * 资金业务授信申报审批流程（TYSX02）出具批复节点
     * add by zhangjw 20210609
     */
    public static final String TYSX02_03 = "137_37,137_130,137_123,137_67,";

    /**
     * 投行业务授信审批流程-分支机构（TYSX04）出具审查报告节点
     * add by zhangjw 20210609
     */
    public static final String TYSX04_01 = "164_32,164_36,";
    /**
     * 投行业务授信审批流程-分支机构（TYSX04）出具核查报告节点
     * add by zhangjw 20210609
     */
    public static final String TYSX04_02 = "164_13,";
    /**
     * 投行业务授信审批流程-分支机构（TYSX04）出具批复节点
     * add by zhangjw 20210609
     */
    public static final String TYSX04_03 = "164_98,164_102,164_53,";
    /**
     * 投行业务授信审批流程-分支机构（TYSX04）协办客户经理
     * add by jiangyl 20210628
     */
    public static final String TYSX04_44 = "164_7";

    /**
     * 资金业务授信申报审批流程-债券池（TYSX06）出具审查报告节点
     * add by zhangjw 20210609
     */
    public static final String TYSX06_01 = "161_30,";
    /**
     * 资金业务授信申报审批流程-债券池（TYSX06）出具批复节点
     * add by zhangjw 20210609
     */
    public static final String TYSX06_03 = "161_37,161_119,161_122,161_67,";

    /**
     * 同业机构准入流程（TYSX01）发起节点
     * add by zhangjw 20210609
     */
    public static final String TYSX01_START = "170_4";
    /**
     * 资金业务授信申报审批流程（TYSX02）发起节点
     * add by zhangjw 20210609
     */
    public static final String TYSX02_START = "137_16";
    /**
     * 投行业务授信审批流程-分支机构（TYSX04）发起节点
     * add by zhangjw 20210609
     */
    public static final String TYSX04_START = "164_6";

    /**
     * 资金业务授信申报审批流程-债券池（TYSX06）发起节点
     * add by zhangjw 20210609
     */
    public static final String TYSX06_START = "161_2";

    /**
     * 一般贷款出账审批流程(244_4)发起节点
     **/
    public static final String DGYX04_START = "244_4";

    /**
     * 合同申请审批流程(237_4)发起节点
     **/
    public static final String DGYX01_START = "237_4";

    /**
     * 贴现协议申请审批流程(241_3)发起节点
     **/
    public static final String DGYX02_START = "241_3";

    /**
     * 对公内评低准入例外审批流程(651_2)发起节点
     **/
    public static final String DGSX06_START = "651_4";
    /**
     * 客户经理确认状态
     * add by hubp 20210616
     * 00 待确认   01 已确认  02 无需确认
     */
    public static final String STD_CONFIRM_STATUS_00 = "00";
    public static final String STD_CONFIRM_STATUS_01 = "01";
    public static final String STD_CONFIRM_STATUS_02 = "02";

    /**
     * C12111	国有股份制商业银行	国有商业银行	03	国有商业银行
     */
    public static final String STD_ZB_INTBANK_TYPE_C12111 = "C12111";

    /**
     * C12141	邮储	国有商业银行	21	邮政储蓄银行
     */
    public static final String STD_ZB_INTBANK_TYPE_C12141 = "C12141";
    //
    /**
     * C12112	其他股份制商业银行	全国性股份行	04	股份制商业银行
     */
    public static final String STD_ZB_INTBANK_TYPE_C12112 = "C12112";

    /**
     * C121321	江苏省内农村商业银行	省内农商行	06	农村商业银行
     */
    public static final String STD_ZB_INTBANK_TYPE_C121321 = "C121321";

    /**
     * C121311	江苏省内城市商业银行	省内城商行	05	城市商业银行
     */
    public static final String STD_ZB_INTBANK_TYPE_C121311 = "C121311";

    /**
     * E10000	证券公司	证券公司	22	证券公司
     */
    public static final String STD_ZB_INTBANK_TYPE_E10000 = "E10000";

    /**
     * C121312	江苏省外城市商业银行	其他银行
     * C121322	江苏省外农村商业银行	其他银行
     * C12140	其他商业银行	        其他银行
     * C121421	江苏省内民营银行	    其他银行
     * C121422	江苏省外民营银行	    其他银行
     */
    public static final String STD_ZB_INTBANK_Others = "C121312,C121322,C12140,C121421,C121422,";


    //合作方流程bizType

    public static final String COOP_PLAN_HZ001 = "HZ001";
    public static final String COOP_PLAN_HZ002 = "HZ002";
    public static final String COOP_PLAN_HZ003 = "HZ003";
    public static final String COOP_PLAN_HZ004 = "HZ004";

    //合作方/项目准入-分支机构（寿光）
    public static final String SGB01 = "SGB01";

    //合作方/项目变更-分支机构（寿光）
    public static final String SGB02 = "SGB02";

    //合作方/项目准入-普惠金融部（寿光）
    public static final String SGB08 = "SGB08";

    //合作方/项目变更-普惠金融部（寿光）
    public static final String SGB09 = "SGB09";

    //合作方/项目准入-普惠金融部（东海）
    public static final String DHB08 = "DHB08";

    //合作方/项目变更-普惠金融部（东海）
    public static final String DHB09 = "DHB09";

    //合作方/项目准入-分支机构（东海）
    public static final String DHB01 = "DHB01";

    //合作方/项目变更-分支机构（东海）
    public static final String DHB02 = "DHB02";

    /**
     * 担保方式  20 质押
     */
    public static final String STD_ZB_GUAR_WAY_20 = "20";

    /**
     * 票据前缀
     */
    public static final String ZY03_PRFIX = "ZY03";

    /**
     * 个人经营性贷款-产品
     */
    public static final String GE_REN_JING_YING_XING_DAI_KUAN_PRD = "20010102,20020101,022011,022004,SC010008,SC020010,SC020009," +
            "SC010014,SC060001,023001,SC010004,SC010003,SC050001,SC010020,SC020004,SC030041,SC030024,SC010016," +
            "SC030026,SC030013,SC010018,SC010019,SC010010,";

    /**
     * 其他个人经营性贷款  个人经营性物业贷款
     */
    public static final String GE_REN_JING_YING_XING_DAI_KUAN_PRD_022011 = "022011,022004,";

    /**
     * 个人经营性贷款-产品(其他个人经营性贷款) 房抵e点贷
     */
    public static final String GE_REN_JING_YING_XING_DAI_KUAN_PRD_022011_P034 = "P034";

    /**
     * 个人经营性贷款 个人经营性物业贷款
     */
    public static final String GE_REN_JING_YING_XING_DAI_KUAN_LMT_PREFIX = "20010102,20020101,";
    /**
     * 特色产品属性 诚易融
     */
    public static final String CHENG_YI_RONG = "P016";
    /**
     * 担保方式 00 信用
     */
    public static final String STD_ZB_GUAR_WAY_00 = "00";

    /**
     * 是否大额授信 标准化
     * -4001 债券池
     * -4002 债券投资
     * -4008 其他标准化债权投资
     */
    public static final String TOU_RONG_LEI_STANDARD = "4001,4002,4008";

    /**
     * 是否大额授信 非标准化
     * <p>
     * -4004 资产-债权融资计划、理财直融工具
     * -4007 资产证券化产品（非标）
     * -4009 债务融资工具（投资）
     * -4010 理财直融工具（投资）
     * -4011 结构化融资
     * -4012    其他非标债权投资
     */
    public static final String TOU_RONG_LEI_NO_STANDARD = "4004,4007,4009,4010,4011,4012";

    /**
     * 是否大额授信 特定载体
     * -4003 资产计划（资产-其他）
     * -4005 净值型产品
     * -4006 资产证券化产品（标准）
     */
    public static final String TOU_RONG_LEI_UNIQUE = "4003,4005,4006";

    /**
     * 存在底层资产的业务类型
     * 4006 资产证券化产品（标准）
     * 4007 资产证券化产品（非标）
     * 4003 资产计划（资产-其他）
     * 4005 净值型产品
     */
    public static final String HAVE_BASIC_ASSET_LMT_BIZ_TYPE = "4006,4007,4003,4005";


    /**
     * 1 银行类机构
     * 2 非金融企业债券投资
     * 3 非银类机构同业业务
     * 4 企业信用
     * 5 非标资产
     */
    /**
     * 大额主责任人类型 银行类机构
     */
    public static final String STD_ZB_LARGE_MAIN_MANAGER_TYPE_1 = "1";
    /**
     * 大额主责任人类型 非金融企业债券投资
     */
    public static final String STD_ZB_LARGE_MAIN_MANAGER_TYPE_2 = "2";

    /**
     * 大额主责任人类型 非银类机构同业业务
     */
    public static final String STD_ZB_LARGE_MAIN_MANAGER_TYPE_3 = "3";

    /**
     * 大额主责任人类型 企业信用
     */
    public static final String STD_ZB_LARGE_MAIN_MANAGER_TYPE_4 = "4";

    /**
     * 大额主责任人类型 非标资产
     */
    public static final String STD_ZB_LARGE_MAIN_MANAGER_TYPE_5 = "5";

    /**
     * 底层资产标准值 3亿元
     */
    public static final String STD_ZB_BASIC_ASSET_NORMAL_TYPE_03 = "03";

    public static final Long STD_ZB_BASIC_ASSET_NORMAL_TYPE_03_VAL = 300_000_000L;

    /**
     * 底层资产标准值 5亿元
     */
    public static final String STD_ZB_BASIC_ASSET_NORMAL_TYPE_05 = "05";

    public static final Long STD_ZB_BASIC_ASSET_NORMAL_TYPE_05_VAL = 500_000_000L;


    /**
     * 合同类型 1 一般合同
     */
    public static final String STD_CONT_TYPE_1 = "1";
    /**
     * 合同类型 2 最高额合同
     */
    public static final String STD_CONT_TYPE_2 = "2";

    /**
     * 合同类型 3 最高额授信协议
     */
    public static final String STD_CONT_TYPE_3 = "3";


    public static final String PROFESSIONAL_LOAN_PREFIX_1401 = "1401";

    public static final String PROFESSIONAL_LOAN_PREFIX_1402 = "1402";

    /**
     * 消息提示类型  业务审批退回提醒
     */
    public static final String STD_WB_NOTICE_TYPE_1 = "1";
    /**
     * 消息提示类型  业务终审通过提醒
     */
    public static final String STD_WB_NOTICE_TYPE_2 = "2";
    /**
     * 消息提示类型  业务终审否决提醒
     */
    public static final String STD_WB_NOTICE_TYPE_3 = "3";

    /**
     * 消息提示交换机
     */
    public static final String MESSAGE_EXCHANGE = "CMIS_CFG_TOPIC";
    /**
     * 消息提示路由key
     */
    public static final String MESSAGE_KEY = "MESSAGE";

    /**
     * 其他事项申报授信相关类型  人民币利率定价申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_01 = "01";
    /**
     * 其他事项申报授信相关类型  外币利率定价申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_02 = "02";
    /**
     * 其他事项申报授信相关类型  保证金存款特惠利率申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_03 = "03";
    /**
     * 其他事项申报授信相关类型  银票手续费优惠申请表
     */
    public static final String STD_LMT_OTHER_APP_TYPE_04 = "04";
    /**
     * 其他事项申报授信相关类型  贴现优惠利率申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_05 = "05";
    /**
     * 其他事项申报授信相关类型  银票签发业务计划申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_06 = "06";
    /**
     * 其他事项申报授信相关类型  中行代签电票申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_07 = "07";
    /**
     * 其他事项申报授信相关类型  特殊贷款用信备案申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_08 = "08";
    /**
     * 其他事项申报授信相关类型  免追加担保申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_09 = "09";
    /**
     * 其他事项申报授信相关类型  授信抵质押物价值认定申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_10 = "10";
    /**
     * 其他事项申报授信相关类型  其他事项申请
     */
    public static final String STD_LMT_OTHER_APP_TYPE_11 = "11";
    /**
     * 其他事项申报授信相关类型  银票签发及全资质押类业务
     */
    public static final String STD_LMT_OTHER_APP_TYPE_12 = "12";


    /**
     * 审批模式 51 信贷管理部负责人权限
     */
    public static final String STD_APPR_MODE_51 = "51";

    /**
     * 审批模式 52-信贷管理部分管行长权限
     */
    public static final String STD_APPR_MODE_52 = "52";

    /**
     * 审批模式 53-投委会权限
     */
    public static final String STD_APPR_MODE_53 = "53";

    /*** 债项等级 010-1级     */
    public static final String STD_DEBT_GRADE_010 = "010";
    /*** 债项等级 020-2级     */
    public static final String STD_DEBT_GRADE_020 = "020";
    /*** 债项等级 030-3级     */
    public static final String STD_DEBT_GRADE_030 = "030";
    /*** 债项等级 040-4级     */
    public static final String STD_DEBT_GRADE_040 = "040";
    /*** 债项等级 050-5级     */
    public static final String STD_DEBT_GRADE_050 = "050";
    /*** 债项等级 060-6级     */
    public static final String STD_DEBT_GRADE_060 = "060";
    /*** 债项等级 070-7级     */
    public static final String STD_DEBT_GRADE_070 = "070";
    /*** 债项等级 080-8级     */
    public static final String STD_DEBT_GRADE_080 = "080";
    /*** 债项等级 090-9级     */
    public static final String STD_DEBT_GRADE_090 = "090";
    /*** 债项等级 100-10级     */
    public static final String STD_DEBT_GRADE_100 = "100";
    /*** 债项等级 888-评级成功     */
    public static final String STD_DEBT_GRADE_888 = "010";

    public static final int NUM_TEN = 10;

    /**
     * 其他事项权限类型  1-支行权限
     **/
    public static final String AUTHORITY_ZHHZ = "1";
    /**
     * 其他事项权限类型  2-分行权限
     **/
    public static final String AUTHORITY_FHHZ = "2";
    /**
     * 其他事项权限类型  3-负责人权限
     **/
    public static final String AUTHORITY_FZR = "3";
    /**
     * 其他事项权限类型  Y-权限内
     **/
    public static final String AUTHORITY_Y = "Y";
    /**
     * 其他事项权限类型  N-权限外
     **/
    public static final String AUTHORITY_N = "N";

    /**
     * 岗位  JRB08-资产管理部投资经理
     **/
    public static final String DUTY_JRB08 = "JRB08";
    /**
     * 岗位  JRB07-资金融市场部投资经理
     **/
    public static final String DUTY_JRB07 = "JRB07";

    /**
     * 岗位  JRB01-金融市场总部风险合规部负责人
     **/
    public static final String DUTY_JRB04 = "JRB04";
    /**
     * 岗位  JRB10-金融市场总部总裁
     **/
    public static final String DUTY_JRB10 = "JRB10";

    /**
     * 权证入库模式 01-电子权证自动入库模式
     **/
    public static final String STD_WARRANT_IN_TYPE_01 = "01";

    /**
     * 权证入库模式 02-纸质权证集中入库模式
     **/
    public static final String STD_WARRANT_IN_TYPE_02 = "02";

    /**
     * 权证入库模式 03-纸质权证柜面入库模式
     **/
    public static final String STD_WARRANT_IN_TYPE_03 = "03";

    /**
     * 权证入库模式 04-电子权证手工入库模式
     **/
    public static final String STD_WARRANT_IN_TYPE_04 = "04";

    /**
     * 票据系统
     **/
    public static final String SYS_NO_PJP = "PJP";

    /**
     * 权证状态 01--待入库
     */
    public static final String STD_ZB_CERTI_STATE_01 = "01";

    /**
     * 权证状态 02--入库在途
     */
    public static final String STD_ZB_CERTI_STATE_02 = "02";

    /**
     * 权证状态 03--入库未记账
     */
    public static final String STD_ZB_CERTI_STATE_03 = "03";

    /**
     * 权证状态 04--入库已记账
     */
    public static final String STD_ZB_CERTI_STATE_04 = "04";

    /**
     * 权证状态 05--出借在途
     */
    public static final String STD_ZB_CERTI_STATE_05 = "05";

    /**
     * 权证状态 06--出借未记账
     */
    public static final String STD_ZB_CERTI_STATE_06 = "06";

    /**
     * 权证状态 07--出借已记账
     */
    public static final String STD_ZB_CERTI_STATE_07 = "07";

    /**
     * 权证状态 08--出库在途
     */
    public static final String STD_ZB_CERTI_STATE_08 = "08";

    /**
     * 权证状态 09--出库未记账
     */
    public static final String STD_ZB_CERTI_STATE_09 = "09";

    /**
     * 权证状态 10--出库已记账
     */
    public static final String STD_ZB_CERTI_STATE_10 = "10";

    /**
     * 权证状态 11--换证出库
     */
    public static final String STD_ZB_CERTI_STATE_11 = "11";

    /**
     * 权证状态 12--换证出库在途
     */
    public static final String STD_ZB_CERTI_STATE_12 = "12";

    /**
     * 权证状态 13--入库冲正
     */
    public static final String STD_ZB_CERTI_STATE_13 = "13";


    /**
     * 额度适用业务条线 01--公司金融条线
     */
    public static final String STD_ZB_SUIT_BIZ_LINE_01 = "01";
    /**
     * 额度适用业务条线 02--小企业条线
     */
    public static final String STD_ZB_SUIT_BIZ_LINE_02 = "02";
    /**
     * 额度适用业务条线 03--小微金融条线
     */
    public static final String STD_ZB_SUIT_BIZ_LINE_03 = "03";
    /**
     * 额度适用业务条线 04--零售条线
     */
    public static final String STD_ZB_SUIT_BIZ_LINE_04 = "04";
    /**
     * 额度适用业务条线 05--网络金融条线
     */
    public static final String STD_ZB_SUIT_BIZ_LINE_05 = "05";
    /**
     * 额度适用业务条线 06--资金业务条线
     */
    public static final String STD_ZB_SUIT_BIZ_LINE_06 = "06";

    /**
     * 区域 01--本地机构
     */
    public static final String STD_ORG_AREA_TYPE_01 = "01";

    /**
     * 区域 02--异地机构
     */
    public static final String STD_ORG_AREA_TYPE_02 = "02";

    /**
     * 集中作业档案池任务类型 01--接收
     */
    public static final String STD_FILE_TASK_TYPE_01 = "01";

    /**
     * 集中作业档案池任务类型 02--暂存
     */
    public static final String STD_FILE_TASK_TYPE_02 = "02";

    /**
     * 集中作业档案池任务类型 03--派发
     */
    public static final String STD_FILE_TASK_TYPE_03 = "03";

    /**
     * 集中作业档案池任务类型 04--暂存及派发
     */
    public static final String STD_FILE_TASK_TYPE_04 = "04";

    /**
     * 集中作业档案池档案任务操作类型 01--纯指令
     */
    public static final String STD_OPT_TYPE_01 = "01";

    /**
     * 集中作业档案池档案任务操作类型 02--非纯指令
     */
    public static final String STD_OPT_TYPE_02 = "02";

    /**
     * 同业授信额度分项类型-同业管理类3006
     */
    public static final String LMT_INTBANK_BIZ_TYPE_3006 = "3006";

    /**
     * 权证出入库申请类型 01--入库申请
     */
    public static final String STD_WARRANT_APP_TYPE_01 = "01";

    /**
     * 权证出入库申请类型 02--出库申请
     */
    public static final String STD_WARRANT_APP_TYPE_02 = "02";

    /**
     * 权证出入库申请类型 03--权证续借
     */
    public static final String STD_WARRANT_APP_TYPE_03 = "03";

    /**
     * 权证出库原因细类 01--押品部分出库
     */
    public static final String STD_WARRANT_OUT_TYPE_SUB_01 = "01";

    /**
     * 权证出库原因细类 02--押品置换出库
     */
    public static final String STD_WARRANT_OUT_TYPE_SUB_02 = "02";

    /**
     * 权证出库原因细类 03--权证更新出库
     */
    public static final String STD_WARRANT_OUT_TYPE_SUB_03 = "03";

    /**
     * 权证出库原因细类 04--权证借阅（诉讼）
     */
    public static final String STD_WARRANT_OUT_TYPE_SUB_04 = "04";

    /**
     * 权证出库原因细类 05--权证借阅（非诉讼）
     */
    public static final String STD_WARRANT_OUT_TYPE_SUB_05 = "05";

    /**
     * 权证出库原因细类 99--其他
     */
    public static final String STD_WARRANT_OUT_TYPE_SUB_99 = "99";

    /**
     * 投资业务信贷管理部权限判断 直接上会的品种
     * <p>
     * -4004 资产-债权融资计划、理财直融工具
     * -4007 资产证券化产品（非标）
     * -4009 债务融资工具（投资）
     * -4010 理财直融工具（投资）
     * -4011 结构化融资
     * -4012 其他非标债权投资
     * -4003 资产-其他
     * -4005 净值型产品
     */
    public static final String SIG_INVEST_XDGLB_ROTE = "4004,4007,4009,4010,4011,4012,4005,";

    /**
     * 集中作业档案池任务类型 HKJJY--还款即解押资料
     */
    public static final String STD_BIZ_SUB_TYPE_HKJJY = "HKJJY";

    /**
     * 凭证状态 01--未用
     */
    public static final String STD_ZB_CERTI_STATUS_01 = "01";

    /**
     * 凭证状态 02--已用
     */
    public static final String STD_ZB_CERTI_STATUS_02 = "02";

    /**
     * 凭证状态 03--作废
     */
    public static final String STD_ZB_CERTI_STATUS_03 = "03";

    /**
     * 凭证状态 04--修改审批中
     */
    public static final String STD_ZB_CERTI_STATUS_04 = "04";

    /**
     * 凭证状态 05--作废审批中
     */
    public static final String STD_ZB_CERTI_STATUS_05 = "05";

    /**
     * 凭证修改类型 01--空白凭证修改
     */
    public static final String STD_ZB_CERTI_APP_TYPE_01 = "01";

    /**
     * 凭证修改类型 02--空白凭证作废
     */
    public static final String STD_ZB_CERTI_APP_TYPE_02 = "02";


    /**
     * 微业贷责任人ID
     */
    public static final String WYD_MANAGER_ID = "88888888";
    /**
     * 微业贷责任机构ID
     */
    public static final String WYD_MANAGER_BR_ID = "006043";
    /**
     * 微业贷账务机构ID
     */
    public static final String WYD_FINA_BR_ID = "019801";
    /**
     * 微业贷账务机构名称
     */
    public static final String WYD_FINA_BR_NAME = "张家港分行清算中心";

    /**
     * 担保合同类型 A--一般担保合同
     */
    public static final String STD_ZB_GUAR_CONT_TYPE_A = "A";

    /**
     * 担保合同类型 B--最高额担保合同
     */
    public static final String STD_ZB_GUAR_CONT_TYPE_B = "B";

    /**
     * 抵质押新增向导标识 01--抵押
     */
    public static final String STD_GRT_FLAG_01 = "01";

    /**
     * 抵质押新增向导标识 02--质押
     */
    public static final String STD_GRT_FLAG_02 = "02";


    /**
     * 担保类型细分 10--存单质押
     */
    public static final String STD_ZB_DB_DETAIL_10 = "10";

    /**
     * 担保类型细分 11--国债质押
     */
    public static final String STD_ZB_DB_DETAIL_11 = "11";

    /**
     * 担保类型细分 12--银票质押
     */
    public static final String STD_ZB_DB_DETAIL_12 = "12";

    /**
     * 担保类型细分 13--全额保证金
     */
    public static final String STD_ZB_DB_DETAIL_13 = "13";

    /**
     * 担保类型细分 14--理财一级质押（含结构性存款）
     */
    public static final String STD_ZB_DB_DETAIL_14 = "14";

    /**
     * 担保类型细分 15--信用证受益权质押
     */
    public static final String STD_ZB_DB_DETAIL_15 = "15";


    /**
     * 137_30,164_36,161_30, 170_19（TYSX01） 信贷管理部风险派驻岗节点编号
     */
    public static final String Xdglbfxpzg_NodeIds = "137_30,164_36,161_30,170_19,";

    /**
     * 调查报告类型 C01  公司通用版
     */
    public static final String STD_RPT_TYPE_C01 = "C01";
    /**
     * 调查报告类型 C02  公司简化版
     */
    public static final String STD_RPT_TYPE_C02 = "C02";
    /**
     * 调查报告类型 C03  公司低风险版
     */
    public static final String STD_RPT_TYPE_C03 = "C03";
    /**
     * 调查报告类型 C04  经营性物业（企业）
     */
    public static final String STD_RPT_TYPE_C04 = "C04";
    /**
     * 调查报告类型 C05  房地产开发贷
     */
    public static final String STD_RPT_TYPE_C05 = "C05";
    /**
     * 调查报告类型 C06  固定资产贷款
     */
    public static final String STD_RPT_TYPE_C06 = "C06";
    /**
     * 调查报告类型 C07  融资租赁公司
     */
    public static final String STD_RPT_TYPE_C07 = "C07";
    /**
     * 调查报告类型 C08  公司混合版（固定资产贷款）
     */
    public static final String STD_RPT_TYPE_C08 = "C08";
    /**
     * 调查报告类型 C09  公司混合版（经营性物业）
     */
    public static final String STD_RPT_TYPE_C09 = "C09";
    /**
     * 调查报告类型 C10  小企业通用版
     */
    public static final String STD_RPT_TYPE_C10 = "C10";
    /**
     * 调查报告类型 C11  小企业标准化产品
     */
    public static final String STD_RPT_TYPE_C11 = "C11";
    /**
     * 调查报告类型 C12  小企业混合版（固定资产贷款）
     */
    public static final String STD_RPT_TYPE_C12 = "C12";
    /**
     * 调查报告类型 C13  小企业混合版（经营性物业）
     */
    public static final String STD_RPT_TYPE_C13 = "C13";
    /**
     * 调查报告类型 C14  小企业混合版（普通与单户标准化混）
     */
    public static final String STD_RPT_TYPE_C14 = "C14";
    /**
     * 调查报告类型 C15  委托贷款
     */
    public static final String STD_RPT_TYPE_C15 = "C15";
    /**
     * 调查报告类型 I01  个人经营性贷款(有企业)
     */
    public static final String STD_RPT_TYPE_I01 = "I01";
    /**
     * 调查报告类型 I02  个人经营性贷款(无企业)
     */
    public static final String STD_RPT_TYPE_I02 = "I02";
    /**
     * 调查报告类型 I03  经营性物业(个人)
     */
    public static final String STD_RPT_TYPE_I03 = "I03";
    /**
     * 调查报告类型 I04  个人标准化产品（有企业）
     */
    public static final String STD_RPT_TYPE_I04 = "I04";
    /**
     * 调查报告类型 I05  个人标准化产品（无企业）
     */
    public static final String STD_RPT_TYPE_I05 = "I05";

    /**
     * 财务报表标识  1 以最近一期报表分析
     */
    public static final String STD_FNC_FLAG_1 = "1";

    /**
     * 财务报表标识  2 以近两年一期报表分析
     */
    public static final String STD_FNC_FLAG_2 = "2";

    /**
     * 财务报表标识  3 以最近一期自制和税务报表分析
     */
    public static final String STD_FNC_FLAG_3 = "3";

    /**
     * 财务报表标识  4 小企业版
     */
    public static final String STD_FNC_FLAG_4 = "4";

    /**
     * 财务报表标识  5 小企业标准化产品版
     */
    public static final String STD_FNC_FLAG_5 = "5";

    /**
     * 财务报表标识  6 小企业版最近一期自制报表分析
     */
    public static final String STD_FNC_FLAG_6 = "6";
    /**
     * 押品信息同步接口xdjzzy 操作标识 01--新增
     */
    public static final String OPER_FLAG_01 = "01";

    /**
     * 押品信息同步接口xdjzzy 操作标识 02--修改
     */
    public static final String OPER_FLAG_02 = "02";

    /**
     * 担保性质  10--对外担保
     */
    public static final String STD_ZB_GUAR_CHA_10 = "10";

    /**
     * 担保性质  20--对内担保
     */
    public static final String STD_ZB_GUAR_CHA_20 = "20";

    /**
     * 普通贷款合同
     */
    public static final String STD_ZB_CTR_LOAN_CONT = "ctrLoanCont";
    /**
     * 最高额授信协议
     */
    public static final String STD_ZB_CTR_HIGH_AMT_AGR_CONT = "ctrHighAmtAgrCont";
    /**
     * 银承合同
     */
    public static final String STD_ZB_CTR_ACCP_CONT = "ctrAccpCont";
    /**
     * 开证合同
     */
    public static final String STD_ZB_CTR_TF_LOC_CONT = "ctrTfLocCont";
    /**
     * 保函合同
     */
    public static final String STD_ZB_CTR_CVRG_CONT = "ctrCvrgCont";
    /**
     * 委托贷款合同
     */
    public static final String STD_ZB_CTR_ENTRUST_LOAN_CONT = "ctrEntrustLoanCont";
    /**
     * 贴现合同
     */
    public static final String STD_ZB_CTR_DISC_CONT = "ctrDiscCont";

    /**
     * 科目类型 01-资产
     */
    public static final String STD_RPT_SUB_TYPE_01 = "01";

    /**
     * 科目类型 02-负债
     */
    public static final String STD_RPT_SUB_TYPE_02 = "02";

    /**
     * 合作方类型 4 - 集群贷市场方
     */
    public static final String STD_PARTNER_TYPE_4 = "4";

    /**
     * 金融机构代码  STD_ZB_INSTU_CDE  C1115632000023- 张家港农村商业银行
     */
    public static final String STD_ZB_INSTU_CDE_ZJG = "C1115632000023";
    /**
     * 金融机构代码  STD_ZB_INSTU_CDE  C1102137000013- 寿光村镇银行
     */
    public static final String STD_ZB_INSTU_CDE_SG = "C1102137000013";
    /**
     * 金融机构代码  STD_ZB_INSTU_CDE  C1100832000011- 东海村镇银行
     */
    public static final String STD_ZB_INSTU_CDE_DH = "C1100832000011";

    /**
     * 担保类型  STD_GUAR_TYPE  - 主担保
     */
    public static final String STD_GUAR_TYPE_101 = "101";
    /**
     * 担保类型  STD_GUAR_TYPE  - 追加担保
     */
    public static final String STD_GUAR_TYPE_102 = "102";

    /**
     * 抵质押物种类 STD_ZB_GUAR_TYPE_CD 28--本行理财产品
     */
    public static final String STD_ZB_GUAR_TYPE_CD_28 ="28";

    /**
     * 抵质押物种类 STD_ZB_GUAR_TYPE_CD 03--房产/房地产
     */
    public static final String STD_ZB_GUAR_TYPE_CD_03 = "03";

    /**
     * 抵质押物种类 STD_ZB_GUAR_TYPE_CD 17--存单
     */
    public static final String STD_ZB_GUAR_TYPE_CD_17 ="17";

    /**
     * 担保方式 STD_ZB_GUAR_WAY_21 21--低风险质押
     */
    public static final String STD_ZB_GUAR_WAY_21 = "21";
}

