package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 资产保全的错误异常</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EcnEnum {

    ECN060001("60001", "传入值为空"),
    ECN060002("60002", "删除失败"),
    ECN069999("9999", "系统异常!"),
    ECN060000("0000", "操作成功!"),
    ECN060003("60003","该笔借据已存在"),
    ECN060004("60004", "清收计划导入失败"),
    ECN060005("60005", "数据不存在"),
    ECN060006("60006", "接口查询错误"),
    ECN060007("60007", "连接到FTP服务器发生异常"),
    ECN060008("60008", "FTP Server:[{}]'s User:[{}] 登录失败!"),
    ECN060009("60009", "下载任务下所有文件至本地处理目录方法异常,异常信息为[{}]"),
    ECN060010("60010", "下载远程FTP服务器文件至本地发生异常,异常信息为[{}]"),
    ECN060011("60011", "上传文件至FTP服务器发生异常,异常信息为[{}]"),
    ECN060012("60012", "创建文件夹到FTP失败,异常信息为[{}]"),
    ECN060013("60013", "同步核心核销记账信息失败"),
    ECN060014("60014", "服务器核心文件不存在"),
    ECN060015("60015", "核心登记失败"),
    ECN060016("60016", "该信用卡信息已存在,最新介质信用卡卡号"),
    ECN060017("60017", "Excel导入格式错误"),
    ECN060018("60018", "证件号码错误"),
    ECN060019("60019","导入的当前这笔借据不存在"),
    ECN060020("60020","贷款余额不能超过5万"),
    ECN060021("60021","核销总金额不能超过5万"),
    ECN060022("60022","逾期本金总金额、核销利息、核销费用合计等于核销总金额"),
    ECN060023("60023","借据不匹配"),
    ECN060024("60024","存在在途任务，不可重复发起"),
    ECN060025("60025","已完成的催收任务不能删除"),


    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EcsEnum enumData : EnumSet.allOf(EcsEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EcnEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EcnEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
