package cn.com.yusys.yusp.enums.common;

import cn.com.yusys.yusp.enums.cache.CacheKeyEnum;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 短信枚举</br>
 *
 * @author 王玉坤
 * @version 1.0
 * @since 2021年9月25日 23:56:54
 */
public enum MessageNoEnums {
    // 您好，您管户下的${cusName}客户下${prdName}业务（借据编号：${billNo}）已经结清，请关注！
    MSG_HK_M_0001("MSG_HK_M_0001", "实时结清通知短信"),

    /************************小微短信息配置开始*********************/
    // 您好，您管户下的${cusName}客户申请的${prdName}已完成合同签订，请关注！
    MSG_XW_M_0001("MSG_XW_M_0001", "合同签订成功"),
    // 友情提醒：尊敬的${cusName}，您所申请的${prdName}已完成合同签订，如有疑问请咨询96065或经办客户经理，如非本人请转告或忽略!
    MSG_XW_C_0001("MSG_XW_C_0001", "合同签订成功"),
    // 您好，${cusName}客户办理的${prdName}已分配至您名下，请关注！
    MSG_XW_M_0003("MSG_XW_M_0003", "优企贷分配到客户经理"),
    // 您好，您管户下的${cusName}客户申请的${prdName}已通过出账审批，请关注！
    MSG_XW_M_0013("MSG_XW_M_0013", "出账审批通过");
    /************************小微短信息配置结束*********************/
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (CacheKeyEnum enumData : EnumSet.allOf(CacheKeyEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private MessageNoEnums(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return keyValue.get(key);
    }

    public static String getKey(String dataValue) {
        String key = null;
        for (MessageNoEnums enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String getValue() {
        return keyValue.get(key);
    }
}
