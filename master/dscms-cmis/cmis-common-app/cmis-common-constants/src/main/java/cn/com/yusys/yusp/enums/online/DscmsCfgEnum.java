package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中参数服务的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsCfgEnum {
    CFG_DISC_ORG_LMT_GETLISTBYQUERYMAP("getListByQueryMap", "获取贴现限额配置"),//获取贴现限额配置
    TRADE_CODE_CMISCFG0001("cmiscfg0001", "新增首页提醒事项"),//新增首页提醒事项
    TRADE_CODE_CMISCFG0002("cmiscfg0002", "根据产品编号查询产品扩展属性配置"),//根据产品编号查询产品扩展属性配置
    TRADE_CODE_CMISCFG0003("cmiscfg0003", "根据行号查询上级行号"),//根据行号查询上级行号
    TRADE_CODE_CMISCFG0004("cmiscfg0004", "根据分支机构获取总行行号或BICCODE"),//根据分支机构获取总行行号或BICCODE
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsCfgEnum enumData : EnumSet.allOf(DscmsCfgEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsCfgEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsCfgEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
