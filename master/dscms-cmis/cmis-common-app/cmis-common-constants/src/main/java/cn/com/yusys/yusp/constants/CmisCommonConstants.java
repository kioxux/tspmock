package cn.com.yusys.yusp.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * cmis信贷平台公共常量类
 */
public class CmisCommonConstants {

    /**
     * 默认的分隔符
     **/
    public final static String DEF_SPILIT_COMMMA = ",";

    /**
     * 出账申请发送微信类型
     **/
    public final static String PVP_SEND_MESSAGE_TYPE_0001 = "MSG_HT_M_0001";

    /**
     * 出账申请发送微信类型
     **/
    public final static String PVP_SEND_MESSAGE_TYPE_0002 = "MSG_HT_M_0002";

    /**
     * 申请退回/审批通过发送短信类型
     **/
    public final static String MSG_CF_M_0016 = "MSG_CF_M_0016";

    /**
     * 新信贷系统简称
     **/
    public final static String SYS_SHORT_NAME = "DSCMIS";

    /** * 新信贷金融机构代码-张家港农村商业银行 **/
    public final static String INSTUCDE_001 = "C1115632000023";
    /** * 新信贷金融机构代码-寿光村镇银行 **/
    public final static String INSTUCDE_002 = "C1102137000013";
    /** * 新信贷金融机构代码-东海村镇银行 **/
    public final static String INSTUCDE_003 = "C1100832000011";

    /** * 新信贷机构代码开头-寿光村镇银行 **/
    public final static String INSTUCDE_80 = "80";
    /** * 新信贷金融机构代码-东海村镇银行 **/
    public final static String INSTUCDE_81 = "81";

    /** * 机构类别-0-总行部室 **/
    public final static String ORG_LEVEL_0 = "0";
    /** * 机构类别-1-异地支行（有分行） **/
    public final static String ORG_LEVEL_1 = "1";
    /** * 机构类别-2-异地支行（无分行） **/
    public final static String ORG_LEVEL_2 = "2";
    /** * 机构类别-3-异地分行 **/
    public final static String ORG_LEVEL_3 = "3";
    /** * 机构类别-4-中心支行 **/
    public final static String ORG_LEVEL_4 = "4";
    /** * 机构类别-5-综合支行 **/
    public final static String ORG_LEVEL_5 = "5";
    /** * 机构类别-6-对公支行 **/
    public final static String ORG_LEVEL_6 = "6";
    /** * 机构类别-7-零售支行 **/
    public final static String ORG_LEVEL_7 = "7";
    /** * 机构类别-8-小额贷款管理部 **/
    public final static String ORG_LEVEL_8 = "8";
    /** * 机构类别-9-小贷分中心 **/
    public final static String ORG_LEVEL_9 = "9";
    /** * 机构类别-A-村镇银行 **/
    public final static String ORG_LEVEL_A = "A";

    /**
     * 默认map对象大小
     **/
    public static final int DEF_MAP_SIZE = 4;

    public static final String RISK_PREVENT_BEAN_REDIS_KEY = "RISK_PREVENT_BEAN_SET"; // 风险拦截实现类redis中的key

    public static final String RISK_PREVENT_REDIS_VALUE_SEPERATOR = "."; // 风险拦截实现类redis中的值分隔符号

    /**
     * 操作标识 01-新增
     **/
    public final static String OPR_TYPE_ADD = "01";
    /**
     * 操作标识 02-删除
     **/
    public final static String OPR_TYPE_DELETE = "02";

    /**
     * 币种  CNY-人民币
     **/
    public final static String CUR_TYPE_CNY = "CNY";


    /**
     * 流程状态标识位 000-待发起
     **/
    public final static String WF_STATUS_000 = "000";
    /**
     * 流程状态标识位 111-审批中
     **/
    public final static String WF_STATUS_111 = "111";
    /**
     * 流程状态标识位 990-取消
     **/
    public final static String WF_STATUS_990 = "990";
    /**
     * 流程状态标识位 991-拿回
     **/
    public final static String WF_STATUS_991 = "991";
    /**
     * 流程状态标识位 992-打回
     **/
    public final static String WF_STATUS_992 = "992";
    /**
     * 流程状态标识位 993-再议
     **/
    public final static String WF_STATUS_993 = "993";
    /**
     * 流程状态标识位 996-自行退出
     **/
    public final static String WF_STATUS_996 = "996";
    /**
     * 流程状态标识位 997-通过
     **/
    public final static String WF_STATUS_997 = "997";
    /**
     * 流程状态标识位 998-拒绝
     **/
    public final static String WF_STATUS_998 = "998";

    /*** 出账状态 未出账 **/
    public static final String TRADE_STATUS_1 = "1";
    /*** 出账状态 已出账 **/
    public static final String TRADE_STATUS_2 = "2";

    /*** 同业机构准入申请表序列号 **/
    public static final String INTBANK_ADMIT_APP_SEQ = "INTBANK_ADMIT_APP_SEQ";



    /**
     * 不允许重复申请相同业务的流程状态List 包括 待发起、审批中、追回、打回
     **/
    public final static String WF_STATUS_CAN_NOT_APPLY_SAME =
            WF_STATUS_000 + DEF_SPILIT_COMMMA +
                    WF_STATUS_111 + DEF_SPILIT_COMMMA +
                    WF_STATUS_991 + DEF_SPILIT_COMMMA +
                    WF_STATUS_992;

    /**
     * 待发起 退回 审批中
     */
    public final static String WF_STATUS_000992111 =
            WF_STATUS_000 + DEF_SPILIT_COMMMA +
                    WF_STATUS_992 + DEF_SPILIT_COMMMA +
                    WF_STATUS_111;

    /**
     * 通过 否决 自行退出
     */
    public final static String WF_STATUS_996997998 =
            WF_STATUS_996 + DEF_SPILIT_COMMMA +
                    WF_STATUS_997 + DEF_SPILIT_COMMMA
                    + WF_STATUS_998;

    /**
     * 自行退出 通过 否决 打回 审批中
     **/
    public final static String WF_STATUS =
            WF_STATUS_996 + DEF_SPILIT_COMMMA +
                    WF_STATUS_997 + DEF_SPILIT_COMMMA +
                    WF_STATUS_998 + DEF_SPILIT_COMMMA +
                    WF_STATUS_992 + DEF_SPILIT_COMMMA +
                    WF_STATUS_111;


    /**
     * 待发起 打回 审批中
     */
    public final static String WF_STATUS_APP_LIST =
            WF_STATUS_000 + DEF_SPILIT_COMMMA +
                    WF_STATUS_111 + DEF_SPILIT_COMMMA +
                    WF_STATUS_992 ;

    /**
     * 通过 否决 自行退出
     */
    public final static String WF_STATUS_HIS_LIST =
            WF_STATUS_997 + DEF_SPILIT_COMMMA +
                    WF_STATUS_998 + DEF_SPILIT_COMMMA +
                    WF_STATUS_996 ;

    /**
     * 合作方授信协议操作类型 optType 新增-01
     **/
    public final static String LMT_COOP_CTR_OPT_TYPE_01 = "01";
    /**
     * 合作方授信协议操作类型 optType 更新-02
     **/
    public final static String LMT_COOP_CTR_OPT_TYPE_02 = "02";

    /**
     * 共享范围 1-全行
     **/
    public static final String SHARED_SCOPE_1 = "1";
    /**
     * 共享范围 2-支行
     **/
    public static final String SHARED_SCOPE_2 = "2";


    /**
     * 预约下载数据源 - biz
     **/
    public static final String ORDER_DOWN_BIZ = "1";
    /**
     * 预约下载数据源 - guar
     **/
    public static final String ORDER_DOWN_GUAR = "2";
    /**
     * 预约下载数据源 - lmt
     **/
    public static final String ORDER_DOWN_LMT = "3";
    /**
     * 预约下载数据源 - cus
     **/
    public static final String ORDER_DOWN_CUS = "4";

    /**
     * 授信类型 授信新增
     **/
    public static final String LMT_TYPE_01 = "01";
    /**
     * 授信类型 授信变更
     **/
    public static final String LMT_TYPE_02 = "02";
    /**
     * 授信类型 授信续作
     **/
    public static final String LMT_TYPE_03 = "03";
    /**
     * 授信类型 授信复审
     **/
    public static final String LMT_TYPE_04 = "04";
    /**
     * 授信类型 授信复议
     **/
    public static final String LMT_TYPE_05 = "05";
    /**
     * 授信类型 授信再议
     **/
    public static final String LMT_TYPE_06 = "06";
    /**
     * 授信类型 预授信细化
     **/
    public static final String LMT_TYPE_07 = "07";
    /**
     * 授信类型 额度调剂
     **/
    public static final String LMT_TYPE_08 = "08";

    /**
     * 授信类型 授信类型属于新增列表显示
     **/
    public static final String LMT_TYPE_APP_LIST_STATUS =
            LMT_TYPE_01 + DEF_SPILIT_COMMMA +
                    LMT_TYPE_03 + DEF_SPILIT_COMMMA +
                    LMT_TYPE_04 + DEF_SPILIT_COMMMA +
                    LMT_TYPE_05 + DEF_SPILIT_COMMMA +
                    LMT_TYPE_06 + DEF_SPILIT_COMMMA ;

    /**
     * 主键流水号
     **/
    public static final String PK_VALUE = "PK_VALUE";
    /**
     * 授信申请流水号序列号
     **/
    public static final String LMT_SERNO = "LMT_SERNO";

    /**
     * 业务申请流水号序列号
     **/
    public static final String IQP_SERNO = "IQP_SERNO";

    /**
     * 授信审批流水号序列号
     **/
    public static final String LMT_APPR_SERNO = "LMT_APPR_SERNO";

    /**
     * 授信批复流水号序列号
     **/
    public static final String LMT_REPAY_SERNO = "LMT_REPAY_SERNO";

    /**
     * 授信批复台账序列号
     **/
    public static final String LMT_ACC = "LMT_ACC";
    /**
     * 授信批复台账额度序列号
     **/
    public static final String LMT_ACC_DETAILS = "LMT_ACC_DETAILS";

    /**
     * 授信台账序列号
     **/
    public static final String LMT_ACC_NO = "LMT_ACC_NO";

    /**
     * 授信批复变更流水号
     **/
    public static final String LMT_CHG_SERNO = "LMT_CHG_SERNO";

    /**
     * 授信批复分项变更流水号
     **/
    public static final String LMT_SUB_CHG_SERNO = "LMT_SUB_CHG_SERNO";

    /**
     * 授信批复分项产品变更流水号
     **/
    public static final String LMT_SUB_PRD_CHG_SERNO = "LMT_SUB_PRD_CHG_SERNO";

    /**
     * 小微授信流水号
     **/
    public static final String LMT_XD_SERNO = "LMT_XD_SERNO";

    /**
     * 授信分项流水号序列号
     **/
    public static final String LMT_SUB_SERNO = "LMT_SUB_SEQ";

    /**
     * 风险分类流水号
     **/
    public static final String PSP_TASK_NO = "PSP_TASK_NO";

    /**
     * 合作方协议序列号
     **/
    public static final String LMT_COOP_CTR_NO = "LMT_COOP_CTR_NO";


    /**
     * 流动资金额度测算序列号
     **/
    public static final String LMT_HIGH_CURFUND_EVAL = "LMT_HIGH_CURFUND_EVAL";

    public static final String IS_PRTCPT_CURT_DECLARE = "1";

    /**
     * 公共字段是否
     */
    public static final String STD_ZB_YES_NO_Y = "1";

    public static final String STD_ZB_YES_NO_N = "0";


    /*** 担保方式 信用 **/
    public static final String GUAR_MODE_00 = "00";
    /*** 担保方式 抵押 **/
    public static final String GUAR_MODE_10 = "10";
    /*** 担保方式 质押 **/
    public static final String GUAR_MODE_20 = "20";
    /*** 担保方式 低风险抵押**/
    public static final String GUAR_MODE_21 = "21";
    /*** 担保方式 保证 **/
    public static final String GUAR_MODE_30 = "30";
    /*** 担保方式 全额保证金 **/
    public static final String GUAR_MODE_40 = "40";
    /*** 担保方式 低风险 **/
    public static final String GUAR_MODE_60 = "60";
    /**
     * 接口调用成功标志位
     **/
    public final static String INTERFACE_SUCCESS_CODE = "000000";
    /**
     * 接口调用异常标志位
     **/
    public final static String INTERFACE_FAIL_CODE = "999999";

    //常用的分隔符 下划线  _
    public final static String COMMON_SPLIT_UNDERLINE = "_";
    //常用的分隔符 等号  =
    public final static String COMMON_SPLIT_EQUALS = "=";
    //常用的分隔符 等号  ,
    public final static String COMMON_SPLIT_COMMA = ",";


    /**
     * 是否  是-1
     **/
    public final static String YES_NO_1 = "1";
    /**
     * 是否  否-0
     **/
    public final static String YES_NO_0 = "0";

    /**
     * cfg参数配置表对应的类型  10-授信批复有效期(天)
     **/
    public final static String CFG_BIZ_PARAM_10 = "10";
    /**
     * cfg参数配置表对应的类型  20-用信宽限期（天）
     **/
    public final static String CFG_BIZ_PARAM_20 = "20";
    /**
     * cfg参数配置表对应的类型  30-展期次数（次）
     **/
    public final static String CFG_BIZ_PARAM_30 = "30";

    /**
     * 押品所在业务阶段  01-已创建
     **/
    public final static String GUAR_BUSISTATE_01 = "01";
    /**
     * 押品所在业务阶段  02-授信审批中
     **/
    public final static String GUAR_BUSISTATE_02 = "02";
    /**
     * 押品所在业务阶段  03-与授信关联
     **/
    public final static String GUAR_BUSISTATE_03 = "03";
    /**
     * 押品所在业务阶段  04-业务审批中
     **/
    public final static String GUAR_BUSISTATE_04 = "04";
    /**
     * 押品所在业务阶段  05-与业务关联
     **/
    public final static String GUAR_BUSISTATE_05 = "05";
    /**
     * 押品所在业务阶段  06-与业务关联(产生借据号)
     **/
    public final static String GUAR_BUSISTATE_06 = "06";
    /**
     * 押品所在业务阶段  11-清收处置
     **/
    public final static String GUAR_BUSISTATE_11 = "11";

    /**
     * 台账状态  0-已关闭
     **/
    public final static String ACC_STATUS_0 = "0";
    /**
     * 台账状态  1-正常
     **/
    public final static String ACC_STATUS_1 = "1";
    /**
     * 台账状态  2-逾期
     **/
    public final static String ACC_STATUS_2 = "2";
    /**
     * 台账状态  3-呆滞
     **/
    public final static String ACC_STATUS_3 = "3";
    /**
     * 台账状态  4-呆账
     **/
    public final static String ACC_STATUS_4 = "4";
    /**
     * 台账状态  5-已核销
     **/
    public final static String ACC_STATUS_5 = "5";
    /**
     * 台账状态  6-未出账
     **/
    public final static String ACC_STATUS_6 = "6";
    /**
     * 台账状态  7-作废
     **/
    public final static String ACC_STATUS_7 = "7";
    /**
     * 台账状态  8-转让
     **/
    public final static String ACC_STATUS_8 = "8";

    /**
     * 台账状态(除未出账)
     **/
    public final static String ACC_STATUS_EXIST =
            ACC_STATUS_0 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_1 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_2 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_3 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_4 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_5 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_7 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_8;

    /**
     * 查询类型
     */
    public final static String QUERY_TYPE_01 = "01";
    public final static String QUERY_TYPE_02 = "02";
    public final static String QUERY_TYPE_03 = "03";
    public final static String QUERY_TYPE_04 = "04";
    public final static String QUERY_TYPE_05 = "05";
    /**
     * 是否
     */
    public final static String STD_ZB_YES = "Y";
    public final static String STD_ZB_NO = "N";

    /*** 合同状态 未生效 **/
    public static final String CONT_STATUS_100 = "100";
    /*** 合同状态 生效 **/
    public static final String CONT_STATUS_200 = "200";
    /*** 合同状态 中止 **/
    public static final String CONT_STATUS_500 = "500";
    /*** 合同状态 注销 **/
    public static final String CONT_STATUS_600 = "600";
    /*** 合同状态 撤回 **/
    public static final String CONT_STATUS_700 = "700";
    /*** 合同状态 作废 **/
    public final static String CONT_STATUS_800 = "800";

    /*** 额度操作类型 冻结 **/
    public static final String STD_ZB_LMT_ACC_OPER_TYPE_01 = "01";
    /*** 额度操作类型 解冻 **/
    public static final String STD_ZB_LMT_ACC_OPER_TYPE_02 = "02";
    /*** 额度操作类型 终止 **/
    public static final String STD_ZB_LMT_ACC_OPER_TYPE_03 = "03";

    /*** 生效 批复状态 **/
    public static final String STD_XD_REPLY_STATUS_01 = "01";
    /*** 失效 批复状态 **/
    public static final String STD_XD_REPLY_STATUS_02 = "02";
    /*** 重新办理 批复状态 **/
    public static final String STD_XD_REPLY_STATUS_03 = "03";

    /**
     * 合同状态(除未生效)
     **/
    public final static String CONT_STATUS_OTHER =
            CONT_STATUS_200 + DEF_SPILIT_COMMMA +
                    CONT_STATUS_600 + DEF_SPILIT_COMMMA +
                    CONT_STATUS_500 + DEF_SPILIT_COMMMA +
                    CONT_STATUS_800 + DEF_SPILIT_COMMMA +
                    CONT_STATUS_700;

    /**
     * 个人授信协议注销--合同状态  500-中止600-注销700-撤回800-作废
     **/
    public final static String LMT_CTR_CANCEL_CONT_STATUS = "500,600,700,800";

    /**
     * 01-新增
     **/
    public static final String ADD_OPR = "01";

    /**
     * 02-删除
     **/
    public static final String DELETE_OPR = "02";
    /**
     * 对公对私类型
     */
    public final static String DGDSLX_COM = "1";
    public final static String DGDSLX_INDIV = "2";

    /**
     * 是否标志  Y-是
     **/
    public final static String STD_IS_FLAG_Y = "Y";
    /**
     * 是否标志  N-否
     **/
    public final static String STD_IS_FLAG_N = "N";
    /**
     * 查询方式01：按个人查询 ，02：按公司查询
     */
    public final static String QUERY_TYPE_INDIV = "01";
    public final static String QUERY_TYPE_COM = "02";

    //默认的;分隔符
    public final static String DEF_SPLIT_SEMICOLON = ";";
    //默认的_分隔符
    public final static String DEF_SPLIT_UNDERLINE = "_";
    //默认的=分隔符
    public final static String DEF_SPLIT_EQUALS = "=";

    /***业务类型 01 最高额授信协议**/
    public static final String STD_BUSI_TYPE_01 = "01";
    /***业务类型 02 普通贷款合同**/
    public static final String STD_BUSI_TYPE_02 = "02";
    /***业务类型 03 贴现协议**/
    public static final String STD_BUSI_TYPE_03 = "03";
    /***业务类型 04 贸易融资合同**/
    public static final String STD_BUSI_TYPE_04 = "04";
    /***业务类型 05 福费廷合同**/
    public static final String STD_BUSI_TYPE_05 = "05";
    /***业务类型 06 开证合同**/
    public static final String STD_BUSI_TYPE_06 = "06";
    /***业务类型 07 银承合同**/
    public static final String STD_BUSI_TYPE_07 = "07";
    /***业务类型 08 保函合同**/
    public static final String STD_BUSI_TYPE_08 = "08";
    /***业务类型 09 委托贷款合同**/
    public static final String STD_BUSI_TYPE_09 = "09";
    /***业务类型 10 资产池协议**/
    public static final String STD_BUSI_TYPE_10 = "10";

    /**
     * 所属年份
     */
    public static final String BELG_YEAR_LAST = "1";//上年
    public static final String BELG_YEAR_CURR = "0";//本期


    /**
     * 征信查询流水号
     **/
    public static final String CRQL_SERNO = "CRQL_SERNO";

    /**
     * 征信授权书编号
     **/
    public static final String AUTHBOOK_NO_SEQ = "AUTHBOOK_NO_SEQ";

    /***担保合同状态 未生效**/
    public static final String GUAR_CONT_STATE_100 = "100";

    /***担保合同状态 生效**/
    public static final String GUAR_CONT_STATE_101 = "101";

    /***担保合同状态 注销**/
    public static final String GUAR_CONT_STATE_104 = "104";

    /***担保合同状态 作废**/
    public static final String GUAR_CONT_STATE_107 = "107";

    /***出入类型 1 入池**/
    public static final String INOUT_TYPE_1 = "1";
    /***出入类型 0 出池**/
    public static final String INOUT_TYPE_0 = "0";

    /** 公共字段-是*/
    public static final String STD_ZB_YES_NO_1 = "1";
    /** 公共字段-否*/
    public static final String STD_ZB_YES_NO_0 = "0";

    /***关联关系 1 生效**/
    public static final String STD_ZB_CORRE_REL_1 = "1";
    /***关联关系 2 新增**/
    public static final String STD_ZB_CORRE_REL_2 = "2";
    /***关联关系 3 解除**/
    public static final String STD_ZB_CORRE_REL_3 = "3";

    /***担保合同类型 A 一般担保合同**/
    public static final String STD_ZB_GUAR_CONT_TYPE_A = "A";
    /***担保合同类型 B 最高额担保合同**/
    public static final String STD_ZB_GUAR_CONT_TYPE_B = "B";

    /** 操作成功标志位 S 成功 */
    public static final String OP_FLAG_S = "S";
    /** 操作成功标志位 F 失败 */
    public static final String OP_FLAG_F = "F";
    /** 描述信息 操作成功 */
    public static final String OP_MSG_S = "操作成功！";
    /** 描述信息 操作失败 */
    public static final String OP_MSG_F = "操作失败！";

    /** 授信终审机构类型 01 支行 */
    public static final String STD_FINAL_APPR_BR_TYPE_01 = "01";
    /** 授信终审机构类型 02 分行 */
    public static final String STD_FINAL_APPR_BR_TYPE_02 = "02";
    /** 授信终审机构类型 03 总行 */
    public static final String STD_FINAL_APPR_BR_TYPE_03 = "03";

    /** 操作类型 01 */
    public static final String OP_TYPE_01 = "01";
    /** 操作类型 02 */
    public static final String OP_TYPE_02 = "02";

    /** 授信业务类型 14040101 委托贷款 */
    public static final String BIZ_TPYE_14020301 = "14020301";
    /** 授信业务类型 20040101 个人委托贷款 */
    public static final String BIZ_TPYE_20040101 = "20040101";

    /** 系统参数配置 标准化产品
     * '信保贷','科技贷','南通信保通','徐州徐信保','即墨政银保','无锡园区保','宿迁园区保','征信贷','优税贷','高企贷','结息贷','诚易融','红领贷','巾帼荣誉贷' */
    public static final String STD_PRD = "STD_PRD";

    /** 系统参数配置 全行标准化产品 信贷管理部核查岗分配规则使用
     *  省心快贷、集群贷、外贸贷、征信贷、优税贷、诚易融 */
    public static final String BANK_STD_PRD = "BANK_STD_PRD";

    /** 系统参数配置 单一客户标准化产品分项分项总额 */
    public static final String SINGLE_CUS_STD_PRD_LMT_TOTAL_AMT = "SINGLE_CUS_STD_PRD_LMT_TOTAL_AMT";
    /** 系统参数配置 集团客户标准化产品授信分项总额 */
    public static final String GRP_CUS_STD_PRD_LMT_TOTAL_AMT = "GRP_CUS_STD_PRD_LMT_TOTAL_AMT";
    /** 超短贷工作日*/
    public static final String WORK_DAYS = "WORK_DAYS";
    /** 系统参数配置 资产池超短贷开关 */
    public static final String ASPL_OPEN_LOAN = "ASPL_OPEN_LOAN";
    /** 系统参数配置 资产池银票开关 */
    public static final String ASPL_OPEN_ACCP = "ASPL_OPEN_ACCP";
    /** 本行资本净额 */
    public static final String BANK_CAPITAL_NET = "BANK_CAPITAL_NET";
    /** 本行资本净值 */
    public static final String BANK_CAPITAL_VALUE = "BANK_CAPITAL_VALUE";
    /** 复议次数 */
    public static final String FY_TIMES = "FY_TIMES";
    /** 复议限制时间 */
    public static final String FY_DATE = "FY_DATE";
    /** 财务指标分析比重配置 */
    public static final String FNC_ASSET_PERCENT = "FNC_ASSET_PERCENT";

    /** 银票 银行本票 银行汇票 银行承兑汇票纸票 银行承兑汇票电票 人民银行发行的票据 人民银行发行的票据 央行票据 */
    public static final String BANK_VOUCHER = "BANK_VOUCHER";

    /** 自然人关联交易预计总额度上限 */
    public static final String NATURE_REL_TOTAL_AMT = "NATURE_REL_TOTAL_AMT";

    /** 棉印染精加工,毛染整精加工,麻染整精加工,丝印染精加工,化纤织物染整精加工,皮革鞣制加工,毛皮鞣制加工,木竹浆制造,非木竹浆制造,炼焦,无机酸制造,无机碱制造,电石*,甲醇*,有机硅单体*,黄磷*,氮肥制造,磷肥制造,电石法聚氯乙烯*,斜交轮胎*,力车胎*,水泥制造,平板玻璃制造,多晶硅*,炼铁,炼钢,铁合金冶炼,铝冶炼,金属船舶制造
        C1713,C1723,C1733,C1743,C1752,C1910,C1931,C2211,C2212,C2520,C2611,C2612,C2613,C2614,C2614,C2619,C2621,C2622,C2651,C2911,C2911,C3011,C3041,C3099,C3110,C3120,C3150,C3216,C3731
     */
    public static final String TWO_HIGH_ONE_LEFT_IND = "TWO_HIGH_ONE_LEFT_IND";

    /** 二代支付正常的响应码  */
    public static final String SUCCESS_TOPPACC_CODE = "SUCCESS_TOPPACC_CODE";
    /** 二代支付错误的响应码  */
    public static final String ERROR_TOPPACC_CODE = "ERROR_TOPPACC_CODE";


    /**    STD_CONT_TYPE	合同类型	1	一般合同*/
    public static final String STD_CONT_TYPE_1 = "1";
    /**    STD_CONT_TYPE	合同类型	2	最高额合同*/
    public static final String STD_CONT_TYPE_2 = "2";
    /**    STD_CONT_TYPE	合同类型	3	最高额授信协议*/
    public static final String STD_CONT_TYPE_3 = "3";

    /**    STD_DISC_CONT_TYPE	贴现协议类型	01	一般贴现协议*/
    public static final String STD_DISC_CONT_TYPE_01 = "01";
    /**    STD_DISC_CONT_TYPE	贴现协议类型	02  贴现额度协议*/
    public static final String STD_DISC_CONT_TYPE_02 = "02";

    /**    STD_GUARANTY_TYPE	保证担保形式		单人担保*/
    public static final String STD_GUARANTY_TYPE_30001 = "30001";
    /**    STD_GUARANTY_TYPE	保证担保形式		多人分保*/
    public static final String STD_GUARANTY_TYPE_30002 = "30002";
    /**    STD_GUARANTY_TYPE	保证担保形式		多人联保*/
    public static final String STD_GUARANTY_TYPE_30003 = "30003";

    /**    STD_GUARANTY_TYPE	保证担保形式		保证-单人担保*/
    public static final String STD_GUARANTY_TYPE_0401 = "0401";
    /**    STD_GUARANTY_TYPE	保证担保形式		保证-多人分保*/
    public static final String STD_GUARANTY_TYPE_0402 = "0402";
    /**    STD_GUARANTY_TYPE	保证担保形式		保证-多人联保*/
    public static final String STD_GUARANTY_TYPE_0403 = "0403";
    /**    STD_GUARANTY_TYPE	保证担保形式		保证-多人单保*/
    public static final String STD_GUARANTY_TYPE_0404 = "0404";


    //核心通信交易码
    public static final String DKAJ = "CRE600";     //按揭贷款出账
    public static final String FRZHTZ = "CRE402";	//法人账户透支出账
    public static final String DKCZ = "CRE400";		//贷款出账
    public static final String STZF = "CRE401";		//受托支付-交易对手帐号登记
    public static final String JKKZ = "CRE910";		//进口开证
    public static final String WTDK = "CRE920";		//委托贷款
    public static final String THDB = "CRE930";		//提货担保
    public static final String HKJH = "CRE420";		//还款计划
    public static final String TXCZ = "CRE900";		//直、贴现出账
    public static final String YCCZ = "CRE800";		//银承出账
    public static final String BHCZ = "CRE700";		//保函出账
    public static final String DKZQ = "CRE500";		//贷款展期出账
    public static final String CASH="CRE927";         //垫款发送核心

    //系统标识 XDG--信贷管理系统
    public static final String STD_PERIPHERAL_SYS_XDG = "XDG";

    //系统标识 RCP--零售智能风控
    public static final String STD_PERIPHERAL_SYS_RCP = "RCP";

    //系统标识 GJP--国结
    public static final String STD_PERIPHERAL_SYS_GJP = "GJP";

    //系统标识 CCP--对公智能风控
    public static final String STD_PERIPHERAL_SYS_CCP = "CCP";

    //系统标识 SYS--核心
    public static final String STD_PERIPHERAL_SYS_SYS = "SYS";

    //系统标识 ESB--ESB系统
    public static final String STD_PERIPHERAL_SYS_ESB = "ESB";

    //系统标识 COM--comstar
    public static final String STD_PERIPHERAL_SYS_COM = "COM";

    /** STD_ZB_LMT_TYPE 额度类型	01 单一客户授信 */
    public static final String STD_ZB_LMT_TYPE_01 ="01";
    /** STD_ZB_LMT_TYPE 额度类型	02 集团客户授信 */
    public static final String STD_ZB_LMT_TYPE_02 ="02";
    /** STD_ZB_LMT_TYPE 额度类型	03 合作方授信 */
    public static final String STD_ZB_LMT_TYPE_03 ="03";
    /** STD_ZB_LMT_TYPE 额度类型	04 产品授信 */
    public static final String STD_ZB_LMT_TYPE_04 ="04";
    /** STD_ZB_LMT_TYPE 额度类型	05 主体授信 */
    public static final String STD_ZB_LMT_TYPE_05 ="05";

    /** STD_ZB_BIZ_ATTR 交易属性	1 合同 */
    public static final String STD_ZB_BIZ_ATTR_1 ="1";
    /** STD_ZB_BIZ_ATTR 交易属性	2 台账 */
    public static final String STD_ZB_BIZ_ATTR_2 ="2";

    public static Map<String, String> commonSignMap = new HashMap<String, String>();
    static {
        commonSignMap.put("O-1","992");//打回
        commonSignMap.put("O-2","992");//退回
        commonSignMap.put("O-8","998");//否决
        commonSignMap.put("O-12","997");//同意
    }

    /** STD_DRFT_TYPE 票据种类	1 银行承兑汇票 */
    public static final String STD_DRFT_TYPE_1 ="1";
    /** STD_DRFT_TYPE 票据种类	2 商业承兑汇票 */
    public static final String STD_DRFT_TYPE_2 ="2";

    /**
     * 03	对公业务条线
     * 01	小微金融条线
     * 02	零售条线
     * 05	网络金融条线
     * 06	资金业务条线
     */
    /** STD_BELG_LINE 所属条线	01 小微 */
    public static final String STD_BELG_LINE_01 = "01";
    /** STD_BELG_LINE 所属条线	02 零售 */
    public static final String STD_BELG_LINE_02 = "02";
    /** STD_BELG_LINE 所属条线	03 对公 */
    public static final String STD_BELG_LINE_03 = "03";
    /** STD_BELG_LINE 所属条线	06 资产池 */
    public static final String STD_BELG_LINE_06 = "06";
    /** STD_BELG_LINE 所属条线	05 网络金融条线 */
    public static final String STD_BELG_LINE_05 = "05";

    /**
     * 操作范围 01-部分
     **/
    public final static String LMT_OPER_OPT_TYPE_01 = "01";
    /**
     * 操作阀内 02-全部
     **/
    public final static String LMT_OPER_OPT_TYPE_02 = "02";

    /** STD_ZB_STATUS 担保合同和担保人(物)的关系【状态】	1 生效 */
    public static final String STD_ZB_STATUS_1 = "1";
    /** STD_ZB_STATUS 担保合同和担保人(物)的关系【状态】	0 失效 */
    public static final String STD_ZB_STATUS_0 = "0";

    /** STD_COND_TYPE 条件类型	01 用信条件及其他限制性条件 */
    public static final String STD_COND_TYPE_01 = "01";
    /** STD_COND_TYPE 条件类型	02 贷后管理要求 */
    public static final String STD_COND_TYPE_02 = "02";
    /** STD_COND_TYPE 条件类型	03 风控建议 */
    public static final String STD_COND_TYPE_03 = "03";

    /** STD_ACC_ACCP_STATUS 银票台账状态	0 未生效 */
    public static final String STD_ACC_ACCP_STATUS_0 = "0";
    /** STD_ACC_ACCP_STATUS 银票台账状态	1 正常 */
    public static final String STD_ACC_ACCP_STATUS_1 = "1";
    /** STD_ACC_ACCP_STATUS 银票台账状态	2 未用退回 */
    public static final String STD_ACC_ACCP_STATUS_2 = "2";
    /** STD_ACC_ACCP_STATUS 银票台账状态	3 结清 */
    public static final String STD_ACC_ACCP_STATUS_3 = "3";
    /** STD_ACC_ACCP_STATUS 银票台账状态	4 到期垫款 */
    public static final String STD_ACC_ACCP_STATUS_4 = "4";
    /** STD_ACC_ACCP_STATUS 银票台账状态	5 支付垫款 */
    public static final String STD_ACC_ACCP_STATUS_5 = "5";
    /** STD_ACC_ACCP_STATUS 银票台账状态	6 到期无垫款 */
    public static final String STD_ACC_ACCP_STATUS_6 = "6";
    /** STD_ACC_ACCP_STATUS 银票台账状态	7 作废 */
    public static final String STD_ACC_ACCP_STATUS_7 = "7";

    /**STD_ORG_TYPE 总行部室 0  */
    public static final String STD_ORG_TYPE_0 = "0"; // 总行部室
    public static final String STD_ORG_TYPE_1 = "1"; // 异地支行（有分行）
    public static final String STD_ORG_TYPE_2 = "2"; // 异地支行（无分行）
    public static final String STD_ORG_TYPE_3 = "3"; // 异地分行
    public static final String STD_ORG_TYPE_4 = "4"; // 中心支行
    public static final String STD_ORG_TYPE_5 = "5"; // 综合支行
    public static final String STD_ORG_TYPE_6 = "6"; // 对公支行
    public static final String STD_ORG_TYPE_7 = "7"; // 零售支行
    public static final String STD_ORG_TYPE_8 = "8"; // 小额贷款管理部
    public static final String STD_ORG_TYPE_9 = "9"; // 小贷分中心
    public static final String STD_ORG_TYPE_A = "A"; // 村镇银行

    /** 财务指标分组序列 STD_SX_FINAN_INDIC_GROUP */
    public static final String STD_SX_FINAN_INDIC_GROUP_01 = "01"; // 调查报告序列
    public static final String STD_SX_FINAN_INDIC_GROUP_02 = "02"; // 资产负债表序列
    public static final String STD_SX_FINAN_INDIC_GROUP_03 = "03"; // 损益表序列
    public static final String STD_SX_FINAN_INDIC_GROUP_04 = "04"; // 财务指标表序列

    /** 自定义财务指标分析项目编号 */
    public static final String STD_CW_ITEM_ID_01 = "ITEM0001"; // 开票销售
    public static final String STD_CW_ITEM_ID_02 = "ITEM0002"; // 缴纳增值税
    public static final String STD_CW_ITEM_ID_03 = "ITEM0003"; // 缴纳所得税
    public static final String STD_CW_ITEM_ID_07 = "ITEM0007"; // 用电量
    public static final String STD_CW_ITEM_ID_08 = "ITEM0008"; // 工人人数
    public static final String STD_CW_ITEM_ID_11 = "ITEM0011"; // 银行融资总额
    public static final String STD_CW_ITEM_ID_12 = "ITEM0012"; // 融资银行数量

    /** 自定义财务指标分析项目名称 */
    public static final String STD_CW_ITEM_NAME_01 = "开票销售"; // 开票销售
    public static final String STD_CW_ITEM_NAME_02 = "缴纳增值税"; // 缴纳增值税
    public static final String STD_CW_ITEM_NAME_03 = "缴纳所得税"; // 缴纳所得税
    public static final String STD_CW_ITEM_NAME_07 = "用电量"; // 用电量
    public static final String STD_CW_ITEM_NAME_08 = "工人人数"; // 工人人数
    public static final String STD_CW_ITEM_NAME_11 = "银行融资总额"; // 银行融资总额
    public static final String STD_CW_ITEM_NAME_12 = "融资银行数量"; // 融资银行数量

    // 财务指标分析-->资产负债样式编号 固定字段编号以及名称
    public static final String STD_BS_ITEM_ID_01 = "ITEMBS01"; // 固定资产净值
    public static final String STD_BS_ITEM_ID_02 = "ITEMBS02"; // 存货
    public static final String STD_BS_ITEM_ID_03 = "ITEMBS03"; // 应收账款
    public static final String STD_BS_ITEM_ID_04 = "ITEMBS04"; // 应付账款
    public static final String STD_BS_ITEM_ID_05 = "ITEMBS05"; // 其他应收款
    public static final String STD_BS_ITEM_ID_06 = "ITEMBS06"; // 其他应付款
    public static final String STD_BS_ITEM_NAME_01 = "固定资产净值"; // 固定资产净值
    public static final String STD_BS_ITEM_NAME_02 = "存货"; // 存货
    public static final String STD_BS_ITEM_NAME_03 = "应收账款"; // 应收账款
    public static final String STD_BS_ITEM_NAME_04 = "应付账款"; // 应付账款
    public static final String STD_BS_ITEM_NAME_05 = "其他应收款"; // 其他应收款
    public static final String STD_BS_ITEM_NAME_06 = "其他应付款"; // 其他应付款

    // 损益表分析-->损益表样式编号 固定字段编号以及名称
    public static final String STD_IS_ITEM_ID_01 = "ITEMIS01"; // 营业收入
    public static final String STD_IS_ITEM_ID_02 = "ITEMIS02"; // 净利润
    public static final String STD_IS_ITEM_NAME_01 = "营业收入"; // 营业收入
    public static final String STD_IS_ITEM_NAME_02 = "净利润"; // 净利润

    // 财务指标表分析-->财务指标表样式编号 固定字段编号以及名称
    public static final String STD_SL_ITEM_ID_01 = "ITEMIS01"; // 毛利率
    public static final String STD_SL_ITEM_ID_02 = "ITEMIS02"; // 销售净利润率
    public static final String STD_SL_ITEM_ID_03 = "ITEMIS03"; // 总资产周转率
    public static final String STD_SL_ITEM_ID_04 = "ITEMIS04"; // 固定资产周转率
    public static final String STD_SL_ITEM_ID_05 = "ITEMIS05"; // 应收账款周转率
    public static final String STD_SL_ITEM_ID_06 = "ITEMIS06"; // 应收账款周转天数
    public static final String STD_SL_ITEM_ID_07 = "ITEMIS07"; // 存货周转率
    public static final String STD_SL_ITEM_ID_08 = "ITEMIS08"; // 存货周转天数
    public static final String STD_SL_ITEM_ID_09 = "ITEMIS09"; // 资产负债率
    public static final String STD_SL_ITEM_ID_10 = "ITEMIS10"; // 利息保障倍数
    public static final String STD_SL_ITEM_ID_11 = "ITEMIS11"; // 流动比率
    public static final String STD_SL_ITEM_ID_12 = "ITEMIS12"; // 速动比率
    public static final String STD_SL_ITEM_ID_13 = "ITEMIS13"; // 营业收入增长率
    public static final String STD_SL_ITEM_ID_14 = "ITEMIS14"; // 毛利润增长率
    public static final String STD_SL_ITEM_ID_15 = "ITEMIS15"; // 净利润增长率
    public static final String STD_SL_ITEM_ID_16 = "ITEMIS16"; // 销贷比
    public static final String STD_SL_ITEM_NAME_01 = "毛利率"; // 毛利率
    public static final String STD_SL_ITEM_NAME_02 = "销售净利润率"; // 销售净利润率
    public static final String STD_SL_ITEM_NAME_03 = "总资产周转率"; // 总资产周转率
    public static final String STD_SL_ITEM_NAME_04 = "固定资产周转率"; // 固定资产周转率
    public static final String STD_SL_ITEM_NAME_05 = "应收账款周转率"; // 应收账款周转率
    public static final String STD_SL_ITEM_NAME_06 = "应收账款周转天数"; // 应收账款周转天数
    public static final String STD_SL_ITEM_NAME_07 = "存货周转率"; // 存货周转率
    public static final String STD_SL_ITEM_NAME_08 = "存货周转天数"; // 存货周转天数
    public static final String STD_SL_ITEM_NAME_09 = "资产负债率"; // 资产负债率
    public static final String STD_SL_ITEM_NAME_10 = "利息保障倍数"; // 利息保障倍数
    public static final String STD_SL_ITEM_NAME_11 = "流动比率"; // 流动比率
    public static final String STD_SL_ITEM_NAME_12 = "速动比率"; // 速动比率
    public static final String STD_SL_ITEM_NAME_13 = "营业收入增长率"; // 营业收入增长率
    public static final String STD_SL_ITEM_NAME_14 = "毛利润增长率"; // 毛利润增长率
    public static final String STD_SL_ITEM_NAME_15 = "净利润增长率"; // 净利润增长率
    public static final String STD_SL_ITEM_NAME_16 = "销贷比"; // 销贷比


    /** STD_LOAN_MODAL 贷款形式	1 新增 */
    public static final String STD_LOAN_MODAL_1 = "1";
    /** STD_LOAN_MODAL 贷款形式	3 借新还旧 */
    public static final String STD_LOAN_MODAL_3 = "3";
    /** STD_LOAN_MODAL 贷款形式	4 无缝对接 */
    public static final String STD_LOAN_MODAL_4 = "4";
    /** STD_LOAN_MODAL 贷款形式	6 无还本续贷 */
    public static final String STD_LOAN_MODAL_6 = "6";
    /** STD_LOAN_MODAL 贷款形式	8 小企业无还本续贷 */
    public static final String STD_LOAN_MODAL_8 = "8";
    /** STD_LOAN_MODAL 贷款形式	9 其他 */
    public static final String STD_LOAN_MODAL_9 = "9";

    /** STD_SPPL_TYPE 影像补扫类型 01 放款影像补扫 **/
    public static final String STD_SPPL_TYPE_01 = "01";
    /** STD_SPPL_TYPE 影像补扫类型 02 合同审核影像补扫 **/
    public static final String STD_SPPL_TYPE_02 = "02";
    /** STD_SPPL_TYPE 影像补扫类型 03 授信类型资料补录 **/
    public static final String STD_SPPL_TYPE_03 = "03";
    /** STD_SPPL_TYPE 影像补扫类型 04 合作方准入影像补扫 **/
    public static final String STD_SPPL_TYPE_04 = "04";
    /** STD_SPPL_TYPE 影像补扫类型 05 合作方协议签订影像补扫 **/
    public static final String STD_SPPL_TYPE_05 = "05";
    /** STD_SPPL_TYPE 影像补扫类型 06 征信影像补扫 **/
    public static final String STD_SPPL_TYPE_06 = "06";
    /** STD_SPPL_TYPE 影像补扫类型 07 展期申请影像补扫 **/
    public static final String STD_SPPL_TYPE_07 = "07";
    /** STD_SPPL_TYPE 影像补扫类型 08 担保变更申请影像补扫 **/
    public static final String STD_SPPL_TYPE_08 = "08";
    /** STD_SPPL_TYPE 影像补扫类型 09 展期协议影像补扫 **/
    public static final String STD_SPPL_TYPE_09 = "09";
    /** STD_SPPL_TYPE 影像补扫类型 10 担保变更协议影像补扫 **/
    public static final String STD_SPPL_TYPE_10 = "10";

    /** STD_SPPL_BIZ_TYPE 补扫业务品种 01 一般贷款出账 **/
    public static final String STD_SPPL_BIZ_TYPE_01 = "01";
    /** STD_SPPL_BIZ_TYPE 补扫业务品种 02 银承出账 **/
    public static final String STD_SPPL_BIZ_TYPE_02 = "02";
    /** STD_SPPL_BIZ_TYPE 补扫业务品种 03 零售放款 **/
    public static final String STD_SPPL_BIZ_TYPE_03 = "03";
    /** STD_SPPL_BIZ_TYPE 补扫业务品种 04 小微放款 **/
    public static final String STD_SPPL_BIZ_TYPE_04 = "04";
    /** STD_SPPL_BIZ_TYPE 补扫业务品种 05 单户授信 **/
    public static final String STD_SPPL_BIZ_TYPE_05 = "05";
    /** STD_SPPL_BIZ_TYPE 补扫业务品种 06 集团授信 **/
    public static final String STD_SPPL_BIZ_TYPE_06 = "06";
    /** STD_SPPL_BIZ_TYPE 补扫业务品种 07 零售业务授信 **/
    public static final String STD_SPPL_BIZ_TYPE_07 = "07";
    /** STD_SPPL_BIZ_TYPE 补扫业务品种 08 委托贷款出账 **/
    public static final String STD_SPPL_BIZ_TYPE_08 = "08";


    // 复审表相关参数
    public static final String STD_SX_FSB_TABLE_02 = "refTable2";
    public static final String STD_SX_FSB_TABLE_03 = "refTable3";
    public static final String STD_SX_FSB_TABLE_04 = "refTable4";
    public static final String STD_SX_FSB_TABLE_05 = "refTable5";
    // 开票销售/缴纳增值税/缴纳所得税/税务报表净利润/自制报表销售/自制报表净利润
    public static final String STD_SX_FSB_ITEMNAME_01 = "开票销售";
    public static final String STD_SX_FSB_ITEMNAME_02 = "缴纳增值税";
    public static final String STD_SX_FSB_ITEMNAME_03 = "缴纳所得税";
    public static final String STD_SX_FSB_ITEMNAME_04 = "税务报表净利润";
    public static final String STD_SX_FSB_ITEMNAME_05 = "自制报表销售";
    public static final String STD_SX_FSB_ITEMNAME_06 = "自制报表净利润";
    public static final String STD_SX_FSB_ITEMNAME_07 = "净资产情况";
    public static final String STD_SX_FSB_ITEMNAME_08 = "个人银行借款";
    public static final String STD_SX_FSB_ITEMNAME_09 = "民间借款";
    public static final String STD_SX_FSB_ITEMNAME_10 = "对外担保";


    /** STD_REPAY_MODE 还款方式 A001 按期付息到期还本 **/
    public static final String STD_REPAY_MODE_A001 = "A001";
    /** STD_REPAY_MODE 还款方式 A002 等额本息 **/
    public static final String STD_REPAY_MODE_A002 = "A002";
    /** STD_REPAY_MODE 还款方式 A003 等额本金 **/
    public static final String STD_REPAY_MODE_A003 = "A003";
    /** STD_REPAY_MODE 还款方式 A009 利随本清 **/
    public static final String STD_REPAY_MODE_A009 = "A009";
    /** STD_REPAY_MODE 还款方式 A012 按226比例还款 **/
    public static final String STD_REPAY_MODE_A012 = "A012";
    /** STD_REPAY_MODE 还款方式 A013 按月还息按季还本 **/
    public static final String STD_REPAY_MODE_A013 = "A013";
    /** STD_REPAY_MODE 还款方式 A014 按月还息按半年还本 **/
    public static final String STD_REPAY_MODE_A014 = "A014";
    /** STD_REPAY_MODE 还款方式 A015 按月还息,按年还本 **/
    public static final String STD_REPAY_MODE_A015 = "A015";
    /** STD_REPAY_MODE 还款方式 A016 新226 **/
    public static final String STD_REPAY_MODE_A016 = "A016";
    /** STD_REPAY_MODE 还款方式 A017 前6月按月还息，后6个月等额本息 **/
    public static final String STD_REPAY_MODE_A017 = "A017";
    /** STD_REPAY_MODE 还款方式 A018 前4个月按月还息，后8个月等额本息 **/
    public static final String STD_REPAY_MODE_A018 = "A018";
    /** STD_REPAY_MODE 还款方式 A019 第一年按月还息，接下来等额本息 **/
    public static final String STD_REPAY_MODE_A019 = "A019";
    /** STD_REPAY_MODE 还款方式 A020 334比例还款 **/
    public static final String STD_REPAY_MODE_A020 = "A020";
    /** STD_REPAY_MODE 还款方式 A021 433比例还款 **/
    public static final String STD_REPAY_MODE_A021 = "A021";
    /** STD_REPAY_MODE 还款方式 A022 10年期等额本息 **/
    public static final String STD_REPAY_MODE_A022 = "A022";
    /** STD_REPAY_MODE 还款方式 A023 按月付息，定期还本 **/
    public static final String STD_REPAY_MODE_A023 = "A023";
    /** STD_REPAY_MODE 还款方式 A030 定制还款 **/
    public static final String STD_REPAY_MODE_A030 = "A030";
    /** STD_REPAY_MODE 还款方式 A031 5年期等额本息 **/
    public static final String STD_REPAY_MODE_A031 = "A031";
    /** STD_REPAY_MODE 还款方式 A040 按期付息,按计划还本 **/
    public static final String STD_REPAY_MODE_A040 = "A040";
    /** STD_REPAY_MODE 还款方式 A041 其他方式 **/
    public static final String STD_REPAY_MODE_A041 = "A041";

    /** STD_GUAR_TYPE 担保类型 101 主担保合同 **/
    public static final String STD_GUAR_TYPE_101 = "101";
    /** STD_GUAR_TYPE 担保类型 102 追加担保合同 **/
    public static final String STD_GUAR_TYPE_102 = "102";

    /** STD_ASSURE_CUS_TYPE 担保人类型 **/
    public static final String STD_ASSURE_CUS_TYPE_10001 = "10001";// 担保公司
    public static final String STD_ASSURE_CUS_TYPE_10002 = "10002";// 个人
    public static final String STD_ASSURE_CUS_TYPE_10003 = "10003";// 企业

    /** STD_GUAR_TYPE 支付方式 0 自主支付 **/
    public static final String STD_RAY_MODE_0 = "0";
    /** STD_GUAR_TYPE 支付方式 1 受托支付 **/
    public static final String STD_RAY_MODE_1 = "1";

    /** 流程影像 **/
    /* 委托贷款 */
    public static final String TOP_CODE_DGYX04_WT = "WTDKCZJB;WTDKDYHT;WTDKZYHT;WTDKBZDBHT;WTDKDCCZ;";
    /* 普通贷款 */
    public static final String TOP_CODE_DGYX04_DK = "DKCZJB;DKDY;DKZY;DKBZDB;LDZJDK;XMDK;JYXWYDK;FRAJDK;YTDK;DKDCCZ;";

    /* 授权状态 未出帐 0*/
    public static final String STD_AUTH_STATUS_0 = "0";
    /* 授权状态 已发送核心【未知】 1*/
    public static final String STD_AUTH_STATUS_1 = "1";
    /* 授权状态 已发送核心校验成功 2*/
    public static final String STD_AUTH_STATUS_2 = "2";
    /* 授权状态 已发送核心校验失败 3*/
    public static final String STD_AUTH_STATUS_3 = "3";
    /* 授权状态 出账已撤销 4*/
    public static final String STD_AUTH_STATUS_4 = "4";
    /* 授权状态 已记帐已撤销 5*/
    public static final String STD_AUTH_STATUS_5 = "5";
    /* 授权状态 抹账被核心拒绝 6*/
    public static final String STD_AUTH_STATUS_6 = "6";
    /* 授权状态 抹帐 7*/
    public static final String STD_AUTH_STATUS_7 = "7";
    /* 授权状态 已发送核心未冲正 8*/
    public static final String STD_AUTH_STATUS_8 = "8";
    /* 授权状态 已发送核心冲正成功 9*/
    public static final String STD_AUTH_STATUS_9 = "9";
    /* 授权状态 冲正申请 A*/
    public static final String STD_AUTH_STATUS_A = "9";

    /**
     * 台账状态(除作废和已关闭)
     **/
    public final static String ACC_STATUS_OTHER =
            ACC_STATUS_1 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_2 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_3 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_4 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_5 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_6 + DEF_SPILIT_COMMMA +
                    ACC_STATUS_8;

    /* 抵质押标识 抵押 01*/
    public static final String STD_GRT_FLAG_01 = "01";
    /* 抵质押标识 质押 02*/
    public static final String STD_GRT_FLAG_02 = "02";

    //资产保全诉讼流水号
    public static final String NPAM_APP_SERNO = "NPAM_APP_SERNO";
    //资产保全诉讼案件流水号
    public static final String NPAM_CASE_SERNO = "NPAM_CASE_SERNO";
    //资产保全催收规则配置流水号
    public static final String NPAM_CPBP_SERNO = "NPAM_CPBP_SERNO";
    //资产保全律师编号
    public static final String NPAM_LAWYER_NO = "NPAM_LAWYER_NO";
    //资产保全单户转让申请流水号
    public static final String NPAM_SIG_PTAI_SERNO = "NPAM_SIG_PTAI_SERNO";
    //资产保全催收回执流水号
    public static final String NPAM_PBREI_SERNO = "NPAM_PBREI_SERNO";
    //资产保全人工指派流水号
    public static final String NPAM_TASK_NO = "NPAM_TASK_NO";
    //资产保全批量核销申请流水号
    public static final String NPAM_PBDWAB_SERNO = "NPAM_PBDWAB_SERNO";
    //资产保全清收明细流水号
    public static final String NPAM_PPD_SERNO = "NPAM_PPD_SERNO";
    //资产保全抵债资产处置流水号
    public static final String NPAM_PAPAI_SERNO = "NPAM_PAPAI_SERNO";
    //资产保全批量转让申请流水号
    public static final String NPAM_BAT_PTAI_SERNO = "NPAM_BAT_PTAI_SERNO";
    //资产保全以物抵债申请流水号
    public static final String NPAM_PDRAI_SERNO = "NPAM_PDRAI_SERNO";
    //资产保全信用卡核销流水号
    public static final String NPAM_PCWA_SERNO = "NPAM_PCWA_SERNO";
    //资产保全费用管理流水号
    public static final String NPAM_PERI_SERNO = "NPAM_PERI_SERNO";
    //资产保全任务处理流水号
    public static final String NPAM_DO_TASK_NO = "NPAM_DO_TASK_NO";
    //资产保全破产案件流水号
    public static final String NPAM_BROKE_CASE_NO = "NPAM_BROKE_CASE_NO";
    //资产保全单户核销申请流水号
    public static final String NPAM_PBDWAS_SERNO = "NPAM_PBDWAS_SERNO";
    //资产保全时效管理流水号
    public static final String NPAM_PPI_SERNO = "NPAM_PPI_SERNO";
    //资产保全债权减免申请流水号
    public static final String NPAM_PDCRAI_SERNO = "NPAM_PDCRAI_SERNO";
    //资产保全仲裁案件流水号
    public static final String NPAM_ARBITRATE_CASE_NO = "NPAM_ARBITRATE_CASE_NO";
    //资产保全核销记账流水号
    public static final String NPAM_PBDWRA_SERNO = "NPAM_PBDWRA_SERNO";
    //资产保全债权减免记账流水号
    public static final String NPAM_PDCRRAI_SERNO = "NPAM_PDCRRAI_SERNO";

    /**
     * 交易对手名称白名单
     */
    public static final String TOPP_ACCT_NAME = "TOPP_ACCT_NAME";
}

