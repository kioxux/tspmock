package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;


/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 公共的错误异常枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EpbEnum {

    EPB090001("90001", "获取数据库连接失败"),
    EPB090002("90002", "数据库操作异常"),
    EPB090003("90003", "数据库中数据字段不符"),
    EPB090004("90004", "数据库中未查询到符合条件的记录"),
    EPB090005("90005", "全局流水重复,请重新发起."),

    EPB099999("9999", "系统异常"),
    //追加
    EPB090006("90006", "删除失败"),
    EPB090007("90007", "主键为空"),

    EPB090008("90008", "系统检测到证件号、客户号、合同号都为空!"),
    EPB090009("90009", "系统检测到存在为空的必输字段!"),
    /**
     * 惠押押专用系统异常
     */
    EPBHYY9999("BA-BI-RE01-99", "系统异常"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EpbEnum enumData : EnumSet.allOf(EpbEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EpbEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EpbEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
