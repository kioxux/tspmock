package cn.com.yusys.yusp.constants;

public class SQLKeyWordsConstant {

    public static final String SELECT = " SELECT ";
    public static final String FROM = " FROM ";
    public static final String WHERE = " WHERE ";
    public static final String ORDER_BY = " ORDER BY ";
    public static final String GROUP_BY = " GROUP BY ";
    public static final String AS = " AS ";


    public static final String DOT = ".";
    public static final String COMMA = ",";
    public static final String SPACE = " ";
    public static final String SINGLE_QUOTES = "'";
    public static final String OPEN_PAREN = "(";
    public static final String CLOSE_PAREN = ")";
    public static final String PERCENT = "%";
    public static final String UNDERLINE = "_";
    //转义字符,在mysql 中，下划线 _ 代表 全部 基本上等同于 * 因此对sql 用 \ 进行转义
    public static final String TRANSFER_CHARACTER = "\\";


    public static final String ON = " ON ";
    public static final String EQUAL = " = ";

    public static final String COND_OPERATION_IN = "IN";
    public static final String COND_OPERATION_LIKE = "LIKE";


}
