package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-出账管理的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizCzEnum {
    /* XDCZ0002 电子保函注销 枚举 start*/
    XDCZ0002_ZX_FLAG_1("1", "退保"),//退保
    XDCZ0002_ZX_FLAG_2("2", "注销"),//注销
    /* XDCZ0002 电子保函注销 枚举 end  */
    /**
     * 操作成功标志位 字典项  S成功 F失败
     */
    OP_FLAG_S("S", "成功"),//成功
    OP_FLAG_F("F", "失败"),//失败
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizCzEnum enumData : EnumSet.allOf(DscmsBizCzEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizCzEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizCzEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
