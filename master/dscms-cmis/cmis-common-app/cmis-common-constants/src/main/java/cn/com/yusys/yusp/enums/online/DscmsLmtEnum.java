package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中额度服务的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsLmtEnum {
    /**
     * 交易码 开始
     **/
    TEST_ENUM("TEST", "中文描述"),//中文描述

    TRADE_CODE_CMISLMT0001("cmislmt0001", "单一客户额度同步"),
    TRADE_CODE_CMISLMT0002("cmislmt0002", "集团额度同步"),
    TRADE_CODE_CMISLMT0003("cmislmt0003", "同业额度同步"),
    TRADE_CODE_CMISLMT0004("cmislmt0004", "合作方额度同步"),
    TRADE_CODE_CMISLMT0005("cmislmt0005", "资金业务额度同步"),
    TRADE_CODE_CMISLMT0006("cmislmt0006", "额度冻结"),
    TRADE_CODE_CMISLMT0007("cmislmt0007", "额度解冻"),
    TRADE_CODE_CMISLMT0008("cmislmt0008", "额度提前终止"),
    TRADE_CODE_CMISLMT0009("cmislmt0009", "占用分项校验"),
    TRADE_CODE_CMISLMT0010("cmislmt0010", "合同下出账申请校验"),
    TRADE_CODE_CMISLMT0011("cmislmt0011", "占用分项交易"),
    TRADE_CODE_CMISLMT0012("cmislmt0012", "合同恢复"),
    TRADE_CODE_CMISLMT0013("cmislmt0013", "合同下出账交易"),
    TRADE_CODE_CMISLMT0014("cmislmt0014", "台账恢复"),
    TRADE_CODE_CMISLMT0015("cmislmt0015", "客户额度查询"),
    TRADE_CODE_CMISLMT0016("cmislmt0016", "合作方客户额度分项查询"),
    TRADE_CODE_CMISLMT0017("cmislmt0017", "客户移交"),
    TRADE_CODE_CMISLMT0018("cmislmt0018", "额度结构查询"),
    TRADE_CODE_CMISLMT0019("cmislmt0019", "综合授信额度校验(查询客户是否有有效的贷款或者非标业务)"),
    TRADE_CODE_CMISLMT0020("cmislmt0020", "承兑行白名单额度查询"),
    TRADE_CODE_CMISLMT0021("cmislmt0021", "对公授信信息查询"),
    TRADE_CODE_CMISLMT0022("cmislmt0022", "客户分类额度查询"),
    TRADE_CODE_CMISLMT0023("cmislmt0023", "个人额度查询（新微贷）"),
    TRADE_CODE_CMISLMT0024("cmislmt0024", "承兑行白名单额度占用"),
    TRADE_CODE_CMISLMT0025("cmislmt0025", "承兑行白名单额度恢复"),
    TRADE_CODE_CMISLMT0026("cmislmt0026", "额度分项信息查询"),
    TRADE_CODE_CMISLMT0027("cmislmt0027", "合作方客户额度查询"),
    TRADE_CODE_CMISLMT0028("cmislmt0028", "删除额度分项数据"),
    TRADE_CODE_CMISLMT0029("cmislmt0029", "更新台账编号"),
    TRADE_CODE_CMISLMT0030("cmislmt0030", "查询客户标准资产授信余额"),
    TRADE_CODE_CMISLMT0031("cmislmt0031", "查询客户是否存在有效的非标额度"),
    TRADE_CODE_CMISLMT0032("cmislmt0032", "查询客户授信余额（不包含穿透化额度）"),
    TRADE_CODE_CMISLMT0033("cmislmt0033", "国结票据出账额度校验"),
    TRADE_CODE_CMISLMT0034("cmislmt0034", "国结票据出账额度占用"),
    TRADE_CODE_CMISLMT0035("cmislmt0035", "用信到期日变更通知"),
    TRADE_CODE_CMISLMT0036("cmislmt0036", "根据分项信息，查询起始日到期日期限"),
    TRADE_CODE_CMISLMT0037("cmislmt0037", "判断批复向下是否发生占用"),
    TRADE_CODE_CMISLMT0038("cmislmt0038", "根据额度品种编号查找适用产品编号"),
    TRADE_CODE_CMISLMT0039("cmislmt0039", "获取合作方额度台账项下总用信敞口余额"),
    TRADE_CODE_CMISLMT0040("cmislmt0040", "集团认定、集团成员退出或集团解散"),
    TRADE_CODE_CMISLMT0041("cmislmt0041", "低风险额度反向生成"),
    TRADE_CODE_CMISLMT0042("cmislmt0042", "根据额度品种编号获取产品扩展属性"),
    TRADE_CODE_CMISLMT0043("cmislmt0043", "获取客户及其集团成员的单笔投资业务授信余额"),
    TRADE_CODE_CMISLMT0044("cmislmt0044", "获取查询机构的贴现限额"),
    TRADE_CODE_CMISLMT0045("cmislmt0045", "校验客户合同项下是否存在未结清业务"),
    TRADE_CODE_CMISLMT0046("cmislmt0046", "根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明"),
    TRADE_CODE_CMISLMT0047("cmislmt0047", "根据分项编号和用信产品编号，查询分项向下是否存在适用改产品的明"),
    TRADE_CODE_CMISLMT0048("cmislmt0048", "根据分项编号，判断是否可以做最高额协议或一般最高额合同"),
    TRADE_CODE_CMISLMT0049("cmislmt0049", "根据机构编号获取分支机构额度管控信息"),
    TRADE_CODE_CMISLMT0050("cmislmt0050", "根据条线部门和区域获取业务条线额度管控信息"),
    TRADE_CODE_CMISLMT0051("cmislmt0051", "获取机构贴现限额管控信息"),
    TRADE_CODE_CMISLMT0052("cmislmt0052", "查询单一客户及所在集团存量敞口余额"),
    TRADE_CODE_CMISLMT0053("cmislmt0053", "查询多客户量敞口金额和敞口余额"),
    TRADE_CODE_CMISLMT0054("cmislmt0054", "查询客户授信总金额(不包含合作方)"),
    TRADE_CODE_CMISLMT0055("cmislmt0055", "获取客户有效的特定目的载体投资额度列表"),
    TRADE_CODE_CMISLMT0056("cmislmt0056", "获取客户有效的低风险额度分项明细编号"),
    TRADE_CODE_CMISLMT0057("cmislmt0057", "线上产品自动生成合同台账"),
    TRADE_CODE_CMISLMT0058("cmislmt0058", "转贴现业务额度恢复"),
    TRADE_CODE_CMISLMT0059("cmislmt0059", "获取客户或分项明细用信综合总额和用信敞口余额（不包含合作方）"),
    TRADE_CODE_CMISLMT0060("cmislmt0060", "获取合同总已用及敞口已用金额"),
    TRADE_CODE_CMISLMT0061("cmislmt0061", "根据客户号获取客户综合授信批复编号，到期日，起始日，期限"),
    TRADE_CODE_CMISLMT0062("cmislmt0062", "根据客户编号获取最新大额风险值"),
    TRADE_CODE_CMISLMT0063("cmislmt0063", "根据分项编号查询向下非失效状态的分项明细"),
    TRADE_CODE_CMISLMT0064("cmislmt0064", "关联交易计算个人客户余额"),
    TRADE_CODE_CMISLMT0065("cmislmt0065", "获取客户指定品种或指定项目的授信余额"),
    TRADE_CODE_CMISLMT0066("cmislmt0066", "获取合同占用额度分项编号-单一客户"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsLmtEnum enumData : EnumSet.allOf(DscmsLmtEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsLmtEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsLmtEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
