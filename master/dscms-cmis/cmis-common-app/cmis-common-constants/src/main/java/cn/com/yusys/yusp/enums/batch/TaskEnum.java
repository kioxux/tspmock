package cn.com.yusys.yusp.enums.batch;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 批量任务的枚举类
 *
 * @author leehuang
 * @version 1.0
 * @since 2020年2月20日 上午9:56:54
 */
public enum TaskEnum {
    BATSTART_TASK("BATSTART", "调度运行开始-更新批量相关表"),
    /* 批前备份日表任务 开始 */
    BAKD0000_TASK("BAKD0000", "批前备份日表任务-删除N天前数据"),
    BAKD0001_TASK("BAKD0001", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKD0002_TASK("BAKD0002", "批前备份日表任务-备份银承台账票据明细[ACC_ACCP_DRFT_SUB] "),
    BAKD0003_TASK("BAKD0003", "批前备份日表任务-备份保函台账[ACC_CVRS]"),
    BAKD0004_TASK("BAKD0004", "批前备份日表任务-备份贴现台账[ACC_DISC]"),
    BAKD0005_TASK("BAKD0005", "批前备份日表任务-备份委托贷款台账[ACC_ENTRUST_LOAN]"),
    BAKD0006_TASK("BAKD0006", "批前备份日表任务-备份贷款台账信息表[ACC_LOAN]"),
    BAKD0007_TASK("BAKD0007", "批前备份日表任务-备份开证台账[ACC_TF_LOC]"),
    BAKD0008_TASK("BAKD0008", "批前备份日表任务-备份合作方授信台账信息[APPR_COOP_INFO]"),
    BAKD0009_TASK("BAKD0009", "批前备份日表任务-备份合作方授信分项信息[APPR_COOP_SUB_INFO]"),
    BAKD0010_TASK("BAKD0010", "批前备份日表任务-备份批复额度分项基础信息[APPR_LMT_SUB_BASIC_INFO]"),
    BAKD0011_TASK("BAKD0011", "批前备份日表任务-备份批复主信息[APPR_STR_MTABLE_INFO]"),
    BAKD0012_TASK("BAKD0012", "批前备份日表任务-备份批复适用机构[APPR_STR_ORG_INFO]"),
    BAKD0013_TASK("BAKD0013", "批前备份日表任务-备份合同占用关系信息[CONT_ACC_REL]"),
    BAKD0014_TASK("BAKD0014", "批前备份日表任务-备份银承合同详情[CTR_ACCP_CONT]"),
    BAKD0015_TASK("BAKD0015", "批前备份日表任务-备份资产池协议[CTR_ASPL_DETAILS]"),
    BAKD0016_TASK("BAKD0016", "批前备份日表任务-备份保函协议详情[CTR_CVRG_CONT]"),
    BAKD0017_TASK("BAKD0017", "批前备份日表任务-备份贴现协议详情[CTR_DISC_CONT]"),
    BAKD0018_TASK("BAKD0018", "批前备份日表任务-备份贴现协议汇票明细[CTR_DISC_PORDER_SUB]"),
    BAKD0019_TASK("BAKD0019", "批前备份日表任务-备份委托贷款合同详情[CTR_ENTRUST_LOAN_CONT]"),
    BAKD0020_TASK("BAKD0020", "批前备份日表任务-备份最高额授信协议[CTR_HIGH_AMT_AGR_CONT]"),
    BAKD0021_TASK("BAKD0021", "批前备份日表任务-备份贷款合同表[CTR_LOAN_CONT]"),
    BAKD0022_TASK("BAKD0022", "批前备份日表任务-备份行名行号对照表[CFG_BANK_INFO]"),
    BAKD0023_TASK("BAKD0023", "批前备份日表任务-备份开证合同详情[CTR_TF_LOC_CONT]"),
    BAKD0024_TASK("BAKD0024", "批前备份日表任务-备份权证台账[GUAR_WARRANT_INFO]"),
    BAKD0025_TASK("BAKD0025", "批前备份日表任务-备份担保合同表[GRT_GUAR_CONT]"),
    BAKD0026_TASK("BAKD0026", "批前备份日表任务-备份分项占用关系信息[LMT_CONT_REL]"),
    BAKD0027_TASK("BAKD0027", "批前备份日表任务-备份白名单额度信息[LMT_WHITE_INFO]"),
    BAKD0028_TASK("BAKD0028", "批前备份日表任务-备份白名单额度信息历史[LMT_WHITE_INFO_HISTORY]"),
    BAKD0029_TASK("BAKD0029", "批前备份日表任务-备份我行代开他行保函[TMP_GJP_LMT_CVRG_RZKZ]"),
    BAKD0030_TASK("BAKD0030", "批前备份日表任务-备份我行代开他行信用证[TMP_GJP_LMT_PEER_RZKZ]"),
    BAKD0031_TASK("BAKD0031", "批前备份日表任务-备份福费廷同业类登记簿[TMP_GJP_LMT_FFT_RZKZ]"),

    BAKMD001_TASK("BAKMD001", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD002_TASK("BAKMD002", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD003_TASK("BAKMD003", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD004_TASK("BAKMD004", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD005_TASK("BAKMD005", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD006_TASK("BAKMD006", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD007_TASK("BAKMD007", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD008_TASK("BAKMD008", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD009_TASK("BAKMD009", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD010_TASK("BAKMD010", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD011_TASK("BAKMD011", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD012_TASK("BAKMD012", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD013_TASK("BAKMD013", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD014_TASK("BAKMD014", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD015_TASK("BAKMD015", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD016_TASK("BAKMD016", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD017_TASK("BAKMD017", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD018_TASK("BAKMD018", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD019_TASK("BAKMD019", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD020_TASK("BAKMD020", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD021_TASK("BAKMD021", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD022_TASK("BAKMD022", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD023_TASK("BAKMD023", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD024_TASK("BAKMD024", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD025_TASK("BAKMD025", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD026_TASK("BAKMD026", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD027_TASK("BAKMD027", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD028_TASK("BAKMD028", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD029_TASK("BAKMD029", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD030_TASK("BAKMD030", "批前备份日表任务-备份银承台账[ACC_ACCP]"),
    BAKMD031_TASK("BAKMD031", "批前备份日表任务-备份银承台账[ACC_ACCP]"),

    /* 批前备份日表任务 结束 */
    /* 文件处理任务 开始 */
    FPT00001_TASK("FPT00001", "文件处理任务-风险预警系统-预警信号处置表[fpt_risk_sign]"),
    FPT00002_TASK("FPT00002", "文件处理任务-风险预警系统-有效客户信息表[fpt_r_cust_busi_sum]"),
    FPT00003_TASK("FPT00003", "文件处理任务-风险预警系统-黑名单客户表[fpt_m_pp_customer_black]"),

    CORE0001_TASK("CORE0001", "文件处理任务-核心系统-贷款账户主表[core_klna_dkzhzb]"),
    CORE0002_TASK("CORE0002", "文件处理任务-核心系统-贷款账户昨日余额表[core_klna_dkzhzr]"),
    CORE0003_TASK("CORE0003", "文件处理任务-核心系统-贷款账户放款表[core_klnb_dkfksx]"),
    CORE0004_TASK("CORE0004", "文件处理任务-核心系统-贷款账户还款表[core_klnb_dkhksx]"),
    CORE0005_TASK("CORE0005", "文件处理任务-核心系统-贷款账户计息表[core_klnb_dkjxsx]"),
    CORE0006_TASK("CORE0006", "文件处理任务-核心系统-贷款账户基础表[core_klnb_dkjcsx]"),
    CORE0007_TASK("CORE0007", "文件处理任务-核心系统-贷款账户期供表[core_klnb_dkzhqg]"),
    CORE0008_TASK("CORE0008", "文件处理任务-核心系统-贷款账户上日余额表[core_klna_dkzhsr]"),
    CORE0009_TASK("CORE0009", "文件处理任务-核心系统-贷款还款计划表[core_klnb_dkhkjh]"),
    CORE0010_TASK("CORE0010", "文件处理任务-核心系统-贷款客户账户交易明细表[core_klnl_dkkhmx]"),
    CORE0011_TASK("CORE0011", "文件处理任务-核心系统-贷款账户会计核算表[core_klnb_dkkjsx]"),
    CORE0012_TASK("CORE0012", "文件处理任务-核心系统-贷款核销登记簿[core_klnb_dkhxdj]"),
    CORE0013_TASK("CORE0013", "文件处理任务-核心系统-贷款期供交易明细表[core_klnl_dkqgmx]"),
    CORE0014_TASK("CORE0014", "文件处理任务-核心系统-靠档利率表[core_kitp_kdlldm]"),
    CORE0015_TASK("CORE0015", "文件处理任务-核心系统-换卡登记簿[core_kcdb_hkdjxx]"),
    CORE0016_TASK("CORE0016", "文件处理任务-核心系统-节假日表[core_kapp_jiejrb]"),

    //零售智能风控
    RCP00001_TASK("RCP00001", "文件处理任务-零售智能风控系统-蚂蚁借呗放款（合约）明细历史表[rcp_ant_loan_detail_his]"),
    RCP00002_TASK("RCP00002", "文件处理任务-零售智能风控系统-蚂蚁借呗放款（合约）明细临时表[rcp_ant_loan_detail_temp]"),
    RCP00003_TASK("RCP00003", "文件处理任务-零售智能风控系统-马上金融贷款借据信息历史表[rcp_ms_loan_detail_his]"),
    RCP00004_TASK("RCP00004", "文件处理任务-零售智能风控系统-马上金融贷款借据信息临时表[rcp_ms_loan_detail_temp]"),
    RCP00005_TASK("RCP00005", "文件处理任务-零售智能风控系统-马上金融贷款放款明细信息历史表[rcp_ms_loan_detail_file_his]"),
    RCP00006_TASK("RCP00006", "文件处理任务-零售智能风控系统-五级分类[rcp_five_classify]"),
    RCP00007_TASK("RCP00007", "文件处理任务-零售智能风控系统-马上金融贷款放款明细信息临时表[rcp_ms_loan_detail_file_temp]"),
    RCP00008_TASK("RCP00008", "文件处理任务-零售智能风控系统-支用合同信息[rcp_loan_cont_info]"),
    RCP00009_TASK("RCP00009", "文件处理任务-零售智能风控系统-马上金融客户信息历史表[rcp_ms_cus_info_his]"),
    RCP00010_TASK("RCP00010", "文件处理任务-零售智能风控系统-蚂蚁核销记录表[rcp_ant_write_off]"),
    RCP00011_TASK("RCP00011", "文件处理任务-零售智能风控系统-客户信息[rcp_cus_info]"),
    RCP00012_TASK("RCP00012", "文件处理任务-零售智能风控系统-蚂蚁借呗还款（合约）明细临时表[rcp_ant_repay_loan_detail_temp]"),
    RCP00013_TASK("RCP00013", "文件处理任务-零售智能风控系统-蚂蚁借呗还款（合约）明细历史表[rcp_ant_repay_loan_detail_his]"),
    RCP00014_TASK("RCP00014", "文件处理任务-零售智能风控系统-蚂蚁借呗日终（合约）信息落地表[rcp_ant_loan_daily_temp]"),
    RCP00015_TASK("RCP00015", "文件处理任务-零售智能风控系统-马上金融贷款还款明细信息临时表[rcp_ms_repay_detail_file_temp]"),
    RCP00016_TASK("RCP00016", "文件处理任务-零售智能风控系统-马上金融贷款还款明细信息历史表[rcp_ms_repay_detail_file_his]"),
    RCP00017_TASK("RCP00017", "文件处理任务-零售智能风控系统-蚂蚁借呗助贷核销记录表[rcp_ant_write_off_sl]"),
    RCP00018_TASK("RCP00018", "文件处理任务-零售智能风控系统-蚂蚁借呗还款（合约）明细（助贷+新联营）临时表[rcp_ant_repay_loan_dt_sl_temp]"),
    RCP00019_TASK("RCP00019", "文件处理任务-零售智能风控系统-蚂蚁借呗还款（合约）明细（助贷+新联营）历史表[rcp_ant_repay_loan_dt_sl_his]"),
    RCP00020_TASK("RCP00020", "文件处理任务-零售智能风控系统-蚂蚁借呗放款（合约）明细（助贷+新联营）临时表[rcp_ant_loan_detail_sl_temp]"),
    RCP00021_TASK("RCP00021", "文件处理任务-零售智能风控系统-蚂蚁借呗日终（合约）信息落地表[rcp_ant_loan_daily_sl_temp]"),
    RCP00022_TASK("RCP00022", "文件处理任务-零售智能风控系统-苏宁联合贷借据信息临时表[rcp_sn_bill_no_temp]"),
    RCP00023_TASK("RCP00023", "文件处理任务-零售智能风控系统-苏宁联合贷还款明细信息临时表[rcp_sn_repay_plan_file_temp]"),
    RCP00024_TASK("RCP00024", "文件处理任务-零售智能风控系统-苏宁联合贷放款明细信息临时表[rcp_sn_loan_detail_file_temp]"),
    RCP00025_TASK("RCP00025", "文件处理任务-零售智能风控系统-蚂蚁借呗放款（合约）明细（助贷+新联营）临时表[bat_s_rcp_ant_loan_detail_sl_his]"),

    //非零内评
    FLS00001_TASK("FLS00001", "文件处理任务-非零评级系统-合同表[fls_app_contract_info]"),
    FLS00002_TASK("FLS00002", "文件处理任务-非零评级系统-借据表[fls_app_loan_bill]"),
    FLS00003_TASK("FLS00003", "文件处理任务-非零评级系统-专业贷款评级[fls_loan_customer_exp]"),

    GJP00001_TASK("GJP00001", "文件处理任务-国际结算系统-国结进口证业务发信贷台账表[gjp_tfb_cms_lc_info_log]"),
    GJP00002_TASK("GJP00002", "文件处理任务-国际结算系统-国结保函业务发信贷台账流水表[gjp_tfb_cms_lg_info_log]"),
    GJP00003_TASK("GJP00003", "文件处理任务-国际结算系统-保证金信贷交互日志表[gjp_tfb_mgn_account_log]"),
    GJP00004_TASK("GJP00004", "文件处理任务-国际结算系统-国结国内证业务发信贷台账流水表[gjp_tfb_cms_dl_info_log]"),
    GJP00005_TASK("GJP00005", "文件处理任务-国际结算系统-牌价信息维护模块表[gjp_tfb_rate_fxs_our]"),
    GJP00006_TASK("GJP00006", "文件处理任务-国际结算系统-进口信用证开立主表[gjp_tfb_lcim_lc_iss_ml]"),
    GJP00007_TASK("GJP00007", "文件处理任务-国际结算系统-买方信用证开立主表[gjp_tfb_dlde_dl_iss_ml]"),
    GJP00008_TASK("GJP00008", "文件处理任务-国际结算系统-客户信息表[gjp_tfb_par_cust]"),
    GJP00009_TASK("GJP00009", "文件处理任务-国际结算系统-进口保函主表[gjp_tfb_grim_ml]"),
    GJP00010_TASK("GJP00010", "文件处理任务-国际结算系统-国内保函开立主表[gjp_tfb_dgdm_dg_ml]"),
    GJP00011_TASK("GJP00011", "文件处理任务-国际结算系统-国内证福费廷主表[gjp_tfb_ift_ml]"),
    GJP00012_TASK("GJP00012", "文件处理任务-国际结算系统-交易附属信息表[gjp_tfb_com_bizinfo]"),
    GJP00013_TASK("GJP00013", "文件处理任务-国际结算系统-保证金登记主表[gjp_tfb_com_mgn_reg]"),
    GJP00014_TASK("GJP00014", "文件处理任务-国际结算系统-保证金关联明细表[gjp_tfb_com_mgn_item]"),

    PJP00001_TASK("PJP00001", "文件处理任务-综合票据系统-承兑清单表[pjp_bt_acception]"),
    PJP00002_TASK("PJP00002", "文件处理任务-综合票据系统-承兑批次表[pjp_bt_acception_batch]"),
    PJP00003_TASK("PJP00003", "文件处理任务-综合票据系统-票据基本信息表(大库表)[pjp_cd_edraft]"),
    PJP00004_TASK("PJP00004", "文件处理任务-综合票据系统-保证金记录表[pjp_bt_accpt_paycash_translog]"),
    PJP00005_TASK("PJP00005", "文件处理任务-综合票据系统-贴现买入(清单)[pjp_bt_discount]"),
    PJP00006_TASK("PJP00006", "文件处理任务-综合票据系统-贴现买入(批次)[pjp_bt_discount_batch]"),
    PJP00007_TASK("PJP00007", "文件处理任务-综合票据系统-机构信息表[pjp_t_department]"),
    PJP00008_TASK("PJP00008", "文件处理任务-综合票据系统-转贴现买入明细[pjp_bt_rediscount_buyin]"),
    PJP00009_TASK("PJP00009", "文件处理任务-综合票据系统-转贴现卖出明细[pjp_bt_rediscount_sell]"),
    PJP00010_TASK("PJP00010", "文件处理任务-综合票据系统-转贴现买入批次[pjp_bt_rediscount_buyin_batch]"),
    PJP00011_TASK("PJP00011", "文件处理任务-综合票据系统-转贴现卖出批次[pjp_bt_rediscount_sell_batch]"),


    YPP00001_TASK("YPP00001", "文件处理任务-押品缓释系统-担保权证信息表[ypp_t_guar_certi_rela]"),
    YPP00002_TASK("YPP00002", "文件处理任务-押品缓释系统-押品基本信息表[ypp_t_guar_base_info] "),
    YPP00003_TASK("YPP00003", "文件处理任务-押品缓释系统-建设用地使用权[ypp_t_inf_build_use]"),
    YPP00004_TASK("YPP00004", "文件处理任务-押品缓释系统-土地承包经营权[ypp_t_inf_cont_mana_rights]"),
    YPP00005_TASK("YPP00005", "文件处理任务-押品缓释系统-商业用房和工业用房[ypp_t_inf_business_industry_housr]"),
    YPP00006_TASK("YPP00006", "文件处理任务-押品缓释系统-居住用房[ypp_t_inf_living_room]"),
    YPP00007_TASK("YPP00007", "文件处理任务-押品缓释系统-其他用房[ypp_t_inf_other_house]"),
    YPP00008_TASK("YPP00008", "文件处理任务-押品缓释系统-在建工程[ypp_t_inf_buil_project]"),

    RLP00001_TASK("RLP00001", "文件处理任务-人力资源系统-人力资源员工信息表[rlp_staff_info]"),

    SBS00001_TASK("SBS00001", "文件处理任务-Comstar系统-Comstar全量交易明细表[sbs_detail_zjg]"),
    SBS00002_TASK("SBS00002", "文件处理任务-Comstar系统-Comstar额度已用表[sbs_available_zjg]"),

    DJK00001_TASK("DJK00001", "文件处理任务-通联系统-卡片信息文件[djk_data_card]"),
    DJK00002_TASK("DJK00002", "文件处理任务-通联系统-客户基本信息文件[djk_data_customer_org]"),
    DJK00003_TASK("DJK00003", "文件处理任务-通联系统-分期信息文件[djk_data_loan]"),
    DJK00004_TASK("DJK00004", "文件处理任务-通联系统-账户基本信息文件[djk_data_account]"),

    /* 文件处理任务 结束 */
    /* 加工任务 开始 */
    CMIS0101_TASK("CMIS0101", "加工任务-业务处理-贷款台账处理 "),
    CMIS0102_TASK("CMIS0102", "加工任务-业务处理-保函台账处理"),
    CMIS0103_TASK("CMIS0103", "加工任务-业务处理-银承台账处理"),
    CMIS0104_TASK("CMIS0104", "加工任务-业务处理-贴现台账处理"),
    CMIS0105_TASK("CMIS0105", "加工任务-业务处理-信用证台账处理"),
    CMIS0106_TASK("CMIS0106", "加工任务-业务处理-垫款台账处理"),
    CMIS0107_TASK("CMIS0107", "加工任务-业务处理-当天未出账业务处理"),
    CMIS0108_TASK("CMIS0108", "加工任务-业务处理-业务合同处理"),
    CMIS0109_TASK("CMIS0109", "加工任务-业务处理-国结额度数据处理"),
    CMIS0110_TASK("CMIS0110", "加工任务-业务处理-资产池协议处理"),
    CMIS0111_TASK("CMIS0111", "加工任务-业务处理-档案日终处理"),
    CMIS0112_TASK("CMIS0112", "加工任务-业务处理-风险提醒生成"),
    CMIS0113_TASK("CMIS0113", "加工任务-业务处理-汇率同步"),
    CMIS0114_TASK("CMIS0114", "加工任务-业务处理-lpr利率同步"),
    CMIS0115_TASK("CMIS0115", "加工任务-业务处理-风险预警处理"),
    CMIS0116_TASK("CMIS0116", "加工任务-业务处理-其他业务处理"),
    CMIS0117_TASK("CMIS0117", "加工任务-业务处理-统计报表生成"),
    CMIS0118_TASK("CMIS0118", "加工任务-业务处理-其他事项申报日终"),
    CMIS0119_TASK("CMIS0119", "加工任务-业务处理-任务加急"),
    CMIS0120_TASK("CMIS0120", "加工任务-业务处理-零售智能风控蚂蚁借呗"),
    CMIS0121_TASK("CMIS0121", "加工任务-业务处理-零售智能风控马上金融"),
    CMIS0122_TASK("CMIS0122", "加工任务-业务处理-零售智能风控马上金融"),
    CMIS0123_TASK("CMIS0123", "加工任务-业务处理-资金同业授信客户准入名单年审提醒"),
    CMIS0124_TASK("CMIS0124", "加工任务-业务处理-授信每年复审提醒"),
    CMIS0125_TASK("CMIS0125", "加工任务-业务处理-资金同业批复状态"),
    CMIS0126_TASK("CMIS0126", "加工任务-业务处理-非零内评数据更新"),
    CMIS0127_TASK("CMIS0127", "加工任务-业务处理-非零内评数据更新"),
    CMIS0128_TASK("CMIS0128", "加工任务-业务处理-零售智能风控蚂蚁借呗改造"),
    CMIS0129_TASK("CMIS0129", "加工任务-业务处理-零售智能风控苏宁联合贷"),
    CMIS0130_TASK("CMIS0130", "加工任务-业务处理-同行业排行处理"),
    CMIS0150_TASK("CMIS0150", "加工任务-业务处理-网金数据初始化"),
    CMIS0151_TASK("CMIS0151", "加工任务-业务处理-网金数据初始化到历史表"),


    CMIS020C_TASK("CMIS020C", "加工任务-额度处理-批前插入分项占用关系信息加工表"),
    CMIS020D_TASK("CMIS020D", "加工任务-额度处理-批前插入合同占用关系信息加工表"),
    CMIS020E_TASK("CMIS020E", "加工任务-额度处理-批前插入批复额度分项基础信息加工表"),
    CMIS020F_TASK("CMIS020F", "加工任务-额度处理-批前插入白名单额度信息表加工表"),
    CMIS020G_TASK("CMIS020G", "加工任务-额度处理-批前插入合作方授信分项信息加工表"),
    CMIS020H_TASK("CMIS020H", "加工任务-额度处理-批前备份台账临时表"),
    CMIS0200_TASK("CMIS0200", "加工任务-额度处理-批前台账比对和将额度相关表备份到额度相关临时表"),

    CMIS0201_TASK("CMIS0201", "加工任务-额度处理-额度关系占用"),
    CMIS0202_TASK("CMIS0202", "加工任务-额度处理-表内占用总余额"),
    CMIS0203_TASK("CMIS0203", "加工任务-额度处理-表内占用敞口余额"),
    CMIS0204_TASK("CMIS0204", "加工任务-额度处理-表外占用总余额"),
    CMIS0205_TASK("CMIS0205", "加工任务-额度处理-表外占用敞口余额"),
    CMIS0206_TASK("CMIS0206", "加工任务-额度处理-占用授信总金额"),
    CMIS0207_TASK("CMIS0207", "加工任务-额度处理-占用授信总敞口金额"),
    CMIS0208_TASK("CMIS0208", "加工任务-额度处理-台账占用合同关系处理"),
    CMIS0209_TASK("CMIS0209", "加工任务-额度处理-同业交易对手额度占用"),
    CMIS020A_TASK("CMIS020A", "加工任务-额度处理-担保公司占用关系处理"),
    CMIS020B_TASK("CMIS020B", "加工任务-额度处理-更新台账占用合同关系处理"),
    CMIS0210_TASK("CMIS0210", "加工任务-额度处理-普通授信额度占用处理"),
    CMIS0211_TASK("CMIS0211", "加工任务-额度处理-Comstar额度处理"),
    CMIS0212_TASK("CMIS0212", "加工任务-额度处理-合作方额度处理"),
    CMIS0213_TASK("CMIS0213", "加工任务-额度处理-授信分项计算用信金额"),
    CMIS0214_TASK("CMIS0214", "加工任务-额度处理-授信分项计算可出账金额"),
    CMIS0215_TASK("CMIS0215", "加工任务-额度处理-XXXX"),
    CMIS0220_TASK("CMIS0220", "加工任务-额度处理-批后额度比对结果处理"),
    CMIS0221_TASK("CMIS0221", "加工任务-额度处理-将额度相关临时表更新到额度相关表"),
    CMIS0222_TASK("CMIS0222", "加工任务-额度处理-授信分项状态处理"),
    CMIS0223_TASK("CMIS0223", "加工任务-额度处理-将额度相关临时表全量插入到额度正式表"),
    CMIS0230_TASK("CMIS0230", "加工任务-额度处理-全行贷款额度分配更新"),

    CMIS0301_TASK("CMIS0301", "加工任务-客户处理-客户信息处理"),
    CMIS0302_TASK("CMIS0302", "加工任务-客户处理-名单生成"),
    CMIS0303_TASK("CMIS0303", "加工任务-客户处理-名单生成-增享贷名单"),
    CMIS0304_TASK("CMIS0304", "加工任务-客户处理-名单生成-唤醒贷名单"),

    CMIS0400_TASK("CMIS0400", "加工任务-贷后管理-清理首次检查临时表"),
    CMIS0401_TASK("CMIS0401", "加工任务-贷后管理-对公经营性首次检查"),
    CMIS0402_TASK("CMIS0402", "加工任务-贷后管理-个人经营性首次检查"),
    CMIS0403_TASK("CMIS0403", "加工任务-贷后管理-个人消费性首次检查"),
    CMIS0404_TASK("CMIS0404", "加工任务-贷后管理-小微经营性首次检查"),
    CMIS0405_TASK("CMIS0405", "加工任务-贷后管理-小微消费性首次检查"),
    CMIS0406_TASK("CMIS0406", "加工任务-贷后管理-首次检查子表数据生成"),
    CMIS0410_TASK("CMIS0410", "加工任务-贷后管理-定期检查清理贷后临时表数据"),
    CMIS0411_TASK("CMIS0411", "加工任务-贷后管理-对公定期检查"),
    CMIS0412_TASK("CMIS0412", "加工任务-贷后管理-个人经营性定期检查"),
    CMIS0413_TASK("CMIS0413", "加工任务-贷后管理-个人消费性定期检查"),
    CMIS0414_TASK("CMIS0414", "加工任务-贷后管理-小微经营性定期检查"),
    CMIS0415_TASK("CMIS0415", "加工任务-贷后管理-小微消费性定期检查"),
    CMIS0416_TASK("CMIS0416", "加工任务-贷后管理-投后定期检查"),
    CMIS0417_TASK("CMIS0417", "加工任务-贷后管理-定期检查子表数据生成"),

    CMIS417A_TASK("CMIS417A", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查借据信息]"),
    CMIS417B_TASK("CMIS417B", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查保证人检查]"),
    CMIS417C_TASK("CMIS417C", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查财务状况检查]"),
    CMIS417D_TASK("CMIS417D", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查财务状况检查]"),
    CMIS417E_TASK("CMIS417E", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查贷后管理建议落实情况]"),
    CMIS417F_TASK("CMIS417F", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查贷后管理建议落实情况]"),
    CMIS417G_TASK("CMIS417G", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查经营状况检查（通用）]"),
    CMIS417H_TASK("CMIS417H", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查融资情况分析]"),
    CMIS417I_TASK("CMIS417I", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查预警信号表]"),
    CMIS417J_TASK("CMIS417J", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查正常类本行融资情况]"),
    CMIS417K_TASK("CMIS417K", "加工任务-贷后管理-贷后检查子表数据生成[插入定期检查正常类对外担保分析]"),
    CMIS417L_TASK("CMIS417L", "加工任务-贷后管理-贷后检查子表数据生成[插入对公定期检查不良类关联企业表]"),
    CMIS417M_TASK("CMIS417M", "加工任务-贷后管理-贷后检查子表数据生成[插入对公定期检查经营性物业贷款检查]"),
    CMIS417N_TASK("CMIS417N", "加工任务-贷后管理-贷后检查子表数据生成[插入对公定期检查经营状况检查（房地产开发贷）]"),
    CMIS417O_TASK("CMIS417O", "加工任务-贷后管理-贷后检查子表数据生成[插入对公定期检查经营状况检查（建筑业）]"),
    CMIS417P_TASK("CMIS417P", "加工任务-贷后管理-贷后检查子表数据生成[插入对公定期检查经营状况检查（制造业）]"),
    CMIS417Q_TASK("CMIS417Q", "加工任务-贷后管理-贷后检查子表数据生成[插入对公、个人客户基本信息分析表]"),
    CMIS417R_TASK("CMIS417R", "加工任务-贷后管理-贷后检查子表数据生成[插入投后定期检查存量授信情况]"),
    CMIS417S_TASK("CMIS417S", "加工任务-贷后管理-贷后检查子表数据生成[插入投后定期检查债券池用信业务情况]"),
    CMIS417T_TASK("CMIS417T", "加工任务-贷后管理-贷后检查子表数据生成[插入资金业务客户基本信息分析表]"),

    CMIS0418_TASK("CMIS0418", "加工任务-贷后管理-定期检查[贷后临时表数据回插]"),
    CMIS418A_TASK("CMIS418A", "加工任务-贷后管理-定期检查[贷后检查结果表]回插"),
    CMIS418B_TASK("CMIS418B", "加工任务-贷后管理-定期检查[定期检查保证人检查表]回插"),
    CMIS418C_TASK("CMIS418C", "加工任务-贷后管理-定期检查[定期检查财务状况检查表]回插"),
    CMIS418D_TASK("CMIS418D", "加工任务-贷后管理-定期检查[定期检查贷后管理建议落实情况表]回插"),
    CMIS418E_TASK("CMIS418E", "加工任务-贷后管理-定期检查[定期检查抵质押物检查]回插"),
    CMIS418F_TASK("CMIS418F", "加工任务-贷后管理-定期检查[定期检查经营状况检查（通用）]回插"),
    CMIS418G_TASK("CMIS418G", "加工任务-贷后管理-定期检查[定期检查融资情况分析]回插"),
    CMIS418H_TASK("CMIS418H", "加工任务-贷后管理-定期检查[定期检查预警信号表]回插"),
    CMIS418I_TASK("CMIS418I", "加工任务-贷后管理-定期检查[定期检查正常类本行融资情况]回插"),
    CMIS418J_TASK("CMIS418J", "加工任务-贷后管理-定期检查[定期检查正常类对外担保分析]回插"),
    CMIS418K_TASK("CMIS418K", "加工任务-贷后管理-定期检查[对公定期检查不良类关联企业表]回插"),
    CMIS418L_TASK("CMIS418L", "加工任务-贷后管理-定期检查[对公定期检查经营性物业贷款检查]回插"),
    CMIS418M_TASK("CMIS418M", "加工任务-贷后管理-定期检查[对公定期检查经营状况检查（房地产开发贷）]回插"),
    CMIS418N_TASK("CMIS418N", "加工任务-贷后管理-定期检查[对公定期检查经营状况检查（建筑业）]回插"),
    CMIS418O_TASK("CMIS418O", "加工任务-贷后管理-定期检查[对公定期检查经营状况检查（制造业）]回插"),
    CMIS418P_TASK("CMIS418P", "加工任务-贷后管理-定期检查[对公、个人客户基本信息分析表]回插"),
    CMIS418Q_TASK("CMIS418Q", "加工任务-贷后管理-定期检查[投后定期检查存量授信情况]回插"),
    CMIS418R_TASK("CMIS418R", "加工任务-贷后管理-定期检查[投后定期检查债券池用信业务情况]回插"),
    CMIS418S_TASK("CMIS418S", "加工任务-贷后管理-定期检查[资金业务客户基本信息分析表]回插"),

    CMIS0420_TASK("CMIS0420", "加工任务-贷后管理-清理贷后临时表数据"),
    CMIS0421_TASK("CMIS0421", "加工任务-贷后管理-公司客户风险分类"),
    CMIS0422_TASK("CMIS0422", "加工任务-贷后管理-个人经营性风险分类"),
    CMIS0423_TASK("CMIS0423", "加工任务-贷后管理-个人消费性风险分类"),
    CMIS0424_TASK("CMIS0424", "加工任务-贷后管理-业务变更风险分类"),
    CMIS0425_TASK("CMIS0425", "加工任务-贷后管理-风险分类借据信息[对公专项贷款风险分类借据信息]数据生成"),
    CMIS425A_TASK("CMIS425A", "加工任务-贷后管理-风险分类借据信息[个人经营性分类]数据生成"),
    CMIS425B_TASK("CMIS425B", "加工任务-贷后管理-风险分类借据信息[个人消费分类]数据生成"),
    CMIS425C_TASK("CMIS425C", "加工任务-贷后管理-风险分类担保合同分析数据生成"),
    CMIS425D_TASK("CMIS425D", "加工任务-贷后管理-风险分类保证人检查数据生成"),
    CMIS425E_TASK("CMIS425E", "加工任务-贷后管理-风险分类抵质押物检查数据生成"),
    CMIS425F_TASK("CMIS425F", "加工任务-贷后管理-企业财务情况数据生成"),
    CMIS425G_TASK("CMIS425G", "加工任务-贷后管理-影响偿还因素分析数据生成"),
    CMIS425H_TASK("CMIS425H", "加工任务-贷后管理-风险分类任务表数据生成"),

    CMIS0426_TASK("CMIS0426", "加工任务-贷后管理-风险任务自动分类"),
    CMIS0427_TASK("CMIS0427", "加工任务-贷后管理-贷后催收任务"),
    CMIS0428_TASK("CMIS0428", "加工任务-贷后管理-生成自动化贷后对公名单"),
    CMIS0429_TASK("CMIS0429", "加工任务-贷后管理-生成自动化贷后个人名单"),
    CMIS0430_TASK("CMIS0430", "加工任务-贷后管理-自动化贷后规则"),
    CMIS0431_TASK("CMIS0431", "加工任务-贷后管理-风险分类任务临时表数据回插"),
    CMIS431A_TASK("CMIS431A", "加工任务-贷后管理-风险分类借据信息临时表数据回插"),
    CMIS431B_TASK("CMIS431B", "加工任务-贷后管理-担保合同分析临时表数据回插"),
    CMIS431C_TASK("CMIS431C", "加工任务-贷后管理-风险分类保证人检查临时表数据回插"),
    CMIS431D_TASK("CMIS431D", "加工任务-贷后管理-风险分类抵质押物检查临时表数据回插"),
    CMIS431E_TASK("CMIS431E", "加工任务-贷后管理-影响偿还因素分析临时表数据回插"),

    CMIS0501_TASK("CMIS0501", "加工任务-系统管理-更新用户管理表"),
    CMIS0601_TASK("CMIS0601", "加工任务-系统管理-短信生成"),
    CMIS0602_TASK("CMIS0602", "加工任务-批量管理-提醒事项推送"),
    CMIS0701_TASK("CMIS0701", "加工任务-配置管理-更新行名行号对照表"),

    FLS01001_TASK("FLS01001", "加工到内评债项全量对公客户信息-非零内评-内评债项全量对公客户信息[bat_fls_t_zxpj_cmpent]"),
    FLS01002_TASK("FLS01002", "加工到内评债项全量同业客户信息-非零内评-内评债项全量同业客户信息[bat_fls_t_zxpj_sameent]"),
    FLS01003_TASK("FLS01003", "加工到内评债项全量对公客户信息-非零内评-内评债项全量专业贷款信息[bat_fls_t_zxpj_loanent]"),
    FLS01004_TASK("FLS01004", "加工到内评债项全量对公客户信息-非零内评-内评债项全量债项逾期信息[bat_fls_t_zxpj_duebillapply]"),
    FLS01005_TASK("FLS01005", "加工到内评债项全量对公客户信息-非零内评-内评债项全量汇率信息[bat_fls_t_zxpj_currate]"),
    FLS01006_TASK("FLS01006", "加工到内评债项全量对公客户信息-非零内评-内评债项全量综合授信信息[bat_fls_t_zxpj_integratecredit]"),
    FLS01007_TASK("FLS01007", "加工到内评债项全量对公客户信息-非零内评-内评债项全量最高额授信协议信息数据[bat_fls_t_zxpj_upapply]"),
    FLS01008_TASK("FLS01008", "加工到内评债项全量对公客户信息-非零内评-内评债项全量合同信息数据[bat_fls_t_zxpj_contract]"),
    FLS01009_TASK("FLS01009", "加工到内评债项全量对公客户信息-非零内评-内评债项全量非垫款借据信息[bat_fls_t_zxpj_accloan]"),
    FLS01010_TASK("FLS01010", "加工到内评债项全量对公客户????-非零内评-内评债项全量交易对手信息[bat_fls_t_zxpj_dealcustomer]"),
    FLS01011_TASK("FLS01011", "加工到内评债项全量对公客户信息-非零内评-内评债项全量承兑企业信息[bat_fls_t_zxpj_acceptcompany]"),
    FLS01012_TASK("FLS01012", "加工到内评债项全量对公客户信息-非零内评-内评债项全量贴现企业信息[bat_fls_t_zxpj_discountcompany]"),
    FLS01013_TASK("FLS01013", "加工到内评债项全量对公客户信息-非零内评-内评债项全量担保合同信息数据[bat_fls_t_zxpj_guaranteecontract]"),
    FLS01014_TASK("FLS01014", "加工到内评债项全量对公客户信息-非零内评-内评债项全量质押物信息[bat_fls_t_zxpj_pledge]"),
    FLS01015_TASK("FLS01015", "加工到内评债项全量对公客户信息-非零内评-内评债项全量抵押物信息[bat_fls_t_zxpj_mortgage]"),
    FLS01016_TASK("FLS01016", "加工到内评债项全量对公客户信息-非零内评-内评债项全量保证人信息[bat_fls_t_zxpj_assureperson]"),
    FLS01017_TASK("FLS01017", "加工到内评债项全量对公客户信息-非零内评-内评债项全量保证金信息[bat_fls_t_zxpj_assureacc]"),
    FLS01018_TASK("FLS01018", "加工到内评债项全量对公客户信息-非零内评-内评债项授信信息[bat_fls_t_zxpj_limitplemort]"),
    FLS01019_TASK("FLS01019", "加工到内评债项全量对公客户信息-非零内评-内评债项全量业务合同和担保合同关联信息[bat_fls_t_zxpj_businessassure]"),
    FLS01020_TASK("FLS01020", "加工到内评债项全量对公客户信息-非零内评-内评债项全量担保合同和抵质押物关联信息[bat_fls_t_zxpj_guarantrrplemort]"),
    FLS01021_TASK("FLS01021", "加工到内评债项全量对公客户信息-非零内评-内评债项全量授信台账分项额度[bat_fls_t_zxpj_subentryamt]"),
    FLS01022_TASK("FLS01022", "加工到内评债项全量对公客户信息-非零内评-内评债项全量授信台账产品额度信息[bat_fls_t_zxpj_prodectamt]"),
    FLS01023_TASK("FLS01023", "加工到内评债项全量对公客户信息-非零内评-内评债项全量垫款借据信息[bat_fls_t_zxpj_padaccloan]"),

    CORE0101_TASK("CORE0101", "文件处理任务-核心系统-营改增信息[bat_core_t_ygz]"),

    /* 文件处理任务-由表至文件  开始*/
    EULD0001_TASK("EULD0001", "文件处理任务-由表至文件-通知卸数平台卸非零内评相关数据"),
    EULD0002_TASK("EULD0002", "文件处理任务-由表至文件-通知卸数平台卸新信贷相关数据"),
    /* 文件处理任务-由表至文件  结束*/

    /* 中间任务-文件处理任务结束后暂停 开始*/
    MID00001_TASK("MID00001", "中间任务-文件处理任务结束后暂停"),
    /* 中间任务-文件处理任务结束后暂停  结束*/


    C2D00001_TASK("C2D00001", "文件处理任务-二代征信系统-企业信用报告查询记录[c2d_ceq_resultinfo]"),
    C2D00002_TASK("C2D00002", "文件处理任务-二代征信系统-借贷账户基本信息[c2d_ed01a_esal_acc_inf]"),
    C2D00003_TASK("C2D00003", "文件处理任务-二代征信系统-还款表现[c2d_ed01b_acc_pay_inf]"),
    C2D00004_TASK("C2D00004", "文件处理任务-二代征信系统-还款表现明细[c2d_ed01b_ac_pay_inf_sub]"),
    C2D00005_TASK("C2D00005", "文件处理任务-二代征信系统-未结清信贷交易汇总信息[c2d_eb02a_ebbu_unloatran]"),
    C2D00006_TASK("C2D00006", "文件处理任务-二代征信系统-未结清担保交易汇总明细信息[c2d_eb03ah_essu_unclearsub]"),
    C2D00007_TASK("C2D00007", "文件处理任务-二代征信系统-借款账户信息-基本信息[c2d_pd01a_basicinfoseg]"),
    C2D00008_TASK("C2D00008", "文件处理任务-二代征信系统-最近5年内的历史表现信息[c2d_pd01e_repaystat60]"),
    C2D00009_TASK("C2D00009", "文件处理任务-二代征信系统-最近5年内的历史表现信息[c2d_pd01e_repaystat60sub]"),
    C2D00010_TASK("C2D00010", "文件处理任务-二代征信系统-最近一次月度表现信息[c2d_pd01c_month_information]"),
    C2D00011_TASK("C2D00011", "文件处理任务-二代征信系统-个人信用报告查询记录[c2d_cpq_resultinfo]"),

    GAPS0001_TASK("GAPS0001", "文件处理任务-二代支付系统-行名行号表[gaps_ccms_partyinf]"),
    ZNSD0001_TASK("ZNSD0001", "文件处理任务-智能审贷-黑白名单表[znsd_bc_cus_gb_list]"),

    RIS00001_TASK("RIS00001", "文件处理任务-大额风险暴露-客户维度风险暴露[ris_khfx_jgbx]"),
    RIS00002_TASK("RIS00002", "文件处理任务-大额风险暴露-产品信息[ris_cpxx_ckhf]"),
    RIS00003_TASK("RIS00003", "文件处理任务-大额风险暴露-客户维度风险暴露汇总[ris_khfx_jgbx_jk]"),

    ODS00001_TASK("ODS00001", "文件处理任务-数仓-机构上月对公贷款余额[ods_fto_dgdkye_m]"),
    ODS00002_TASK("ODS00002", "文件处理任务-数仓-机构上日对公贷款余额[ods_fto_dgdkye_d]"),
    ODS00003_TASK("ODS00003", "文件处理任务-数仓-业务条线上月贷款余额[ods_fto_ywtx_dkye_m]"),
    ODS00004_TASK("ODS00004", "文件处理任务-数仓-业务条线上日贷款余额[ods_fto_ywtx_dkye_d]"),

    /* 加工任务 结束 */
    BATEND_TASK("BATEND", "调度运行完成-更新批量相关表"),
    ;

    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<String, String>();
        for (TaskEnum enumData : EnumSet.allOf(TaskEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private TaskEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(String key) {
        return keyValue.get(key);
    }

    public static String getKey(String dataValue) {
        String key = null;
        for (TaskEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String getValue() {
        return keyValue.get(this.key);
    }
}
