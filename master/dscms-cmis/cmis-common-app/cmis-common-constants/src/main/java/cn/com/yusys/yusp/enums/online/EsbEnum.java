package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * ESB的枚举</br>
 *
 * @author mochi
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EsbEnum {

    /**
     * 服务名称 开始
     **/
    SERVICE_NAME_ESB_TRADE_CLIENT("EsbTradeClient", "新信贷与ESB交互交易请求方"),
    /**服务名称 结束**/
    /**
     * 柜员号 开始
     **/
    USERID_CIIS2ND("80010422", "二代征信系统柜员号"),//二代征信系统柜员号
    USERID_CORE("zjgrcb", "核心系统柜员号"),//核心系统柜员号
    USERID_ECIF("ZR001854", "ECIF系统柜员号"),//ECIF系统柜员号  20211114-00053  二次修改
    USERID_FXYJXT("80010422", "风险预警系统柜员号"),//零风险预警系统柜员号
    USERID_IRS("80010422", "非零内评系统柜员号"),//非零内评系统柜员号
    USERID_RIRCP("80010422", "零售智能风控系统柜员号"),//零售智能风控系统柜员号
    USERID_CIRCP("80010422", "对公智能风控系统柜员号"),//零售智能风控系统柜员号
    USERID_SZZX("80010422", "苏州征信系统柜员号"),//苏州征信系统统柜员号
    USERID_XWYWGLPT("80010422", "小微业务管理平台柜员号"),//小微业务管理平台柜员号
    USERID_YPQZXT("80010422", "押品权证系统柜员号"),//押品权证系统柜员号
    USERID_YPXT("80010422", "押品系统柜员号"),//押品系统柜员号
    USERID_ZNWDSPXT("80010422", "智能微贷审批系统柜员号"),//智能微贷审批系统柜员号
    USERID_OUTERDATA("0170901", "外部数据平台系统柜员号"),//外部数据平台系统柜员号
    USERID_LSNP("0170901", "零售内评系统柜员号"),//外零售内评系统柜员号
    USERID_GJJS("0170901", "国际结算系统柜员号"),//国际结算系统柜员号
    USERID_YK("80010422", "印控系统柜员号"),//印控系统柜员号
    USERID_ZFP("zjgrcb", "二代支付系统柜员号"),//二代支付系统柜员号
    USERID_GAP("zjgrcb", "中间业务系统柜员号"),
    USERID_LC("zjgrcb", "理财系统柜员号"),//核心系统柜员号
    USERID_XWD("80010422", "新微贷系统柜员号"),//新微贷系统柜员号
    USERID_PJ("zjgrcb", "票据系统柜员号"),//票据系统柜员号
    /**柜员号 结束**/

    /**
     * 部门号 开始
     **/
    BRCHNO_CIIS2ND("019000", "二代征信系统部门号"),//二代征信系统部门号
    BRCHNO_CORE("009801", "核心系统部门号"),//核心系统部门号
    BRCHNO_ECIF("019801", "ECIF系统部门号"),//ECIF系统部门号   20211114-00053  二次修改
    BRCHNO_FXYJXT("019803", "风险预警系统部门号"),//零风险预警系统部门号
    BRCHNO_IRS("019803", "非零内评系统部门号"),//非零内评系统部门号
    BRCHNO_RIRCP("019803", "零售智能风控系统部门号"),//零售智能风控系统部门号
    BRCHNO_CIRCP("019803", "对公智能风控系统部门号"),//零售智能风控系统部门号
    BRCHNO_SZZX("019803", "苏州征信系统部门号"),//苏州征信系统统部门号
    BRCHNO_XWYWGLPT("019803", "小微业务管理平台部门号"),//小微业务管理平台部门号
    BRCHNO_YPQZXT("019803", "押品权证系统部门号"),//押品权证系统部门号
    BRCHNO_YPXT("019803", "押品系统部门号"),//押品系统部门号
    BRCHNO_ZNWDSPXT("019803", "智能微贷审批系统部门号"),//智能微贷审批系统部门号
    BRCHNO_OUTERDATA("017001", "外部数据平台系统部门号"),//外部数据平台系统部门号
    BRCHNO_LSNP("017001", "零售内评系统部门号"),//零售内评系统部门号
    BRCHNO_GJJS("017001", "国际结算系统部门号"),//国际结算系统部门号
    BRCHNO_YK("019803", "印控系统部门号"),//印控系统部门号
    BRCHNO_ZFP("009801", "二代支付系统部门号"),//二代支付系统部门号
    BRCHNO_GAP("009801", "中间业务系统部门号"),
    BRCHNO_LC("009801", "理财系统部门号"),//核心系统部门号
    BRCHNO_XWD("019803", "新微贷系统部门号"),//新微贷系统部门号
    BRCHNO_PJ("009801", "票据系统部门号"),//票据系统柜员号
    /**部门号 结束**/
    /**
     * 渠道 开始
     **/
    SERVTP_EBS("EBS", "企业网银"),//企业网银
    SERVTP_MBL("MBL", "手机银行"),//手机银行
    SERVTP_NET("NET", "个人网银"),//个人网银
    SERVTP_WXP("WXP", "微信银行"),//微信银行
    SERVTP_DOC("DOC", "文件管理系统"),//文件管理系统
    SERVTP_XDG("XDG", "信贷管理系统"),//信贷管理系统
    SERVTP_EBT("EBT", "企业手机银行"),//企业手机银行
    SERVTP_NNT("NNT", "柜面"),//柜面
    SERVTP_SNT("SNT", "智能超柜系统"),//智能超柜系统
    SERVTP_ATM("ATM", "ATM系统"),//ATM系统
    SERVTP_ICS("ICS", "智能客服"),//智能客服
    SERVTP_RCP("RCP", "零售智能网贷系统"),//零售智能网贷系统
    SERVTP_XDW("XDW", "信贷中转处理系统"),//信贷中转处理系统
    SERVTP_YPP("YPP", "押品缓释系统"),//押品缓释系统
    SERVTP_GJP("GJP", "国际业务系统"),//国际业务系统
    SERVTP_CCP("CCP", "对公智能风控"),//对公智能风控
    SERVTP_HLW("HLW", "直销银行"),//直销银行
    SERVTP_YXT("YXT", "营销过程管理平台"),//营销过程管理平台
    SERVTP_BSP("BSP", "中台"),//中台
    SERVTP_XWH("XWH", "小微微信公众号"),//小微微信公众号
    SERVTP_YDY("YDY", "移动金融平台"),//移动金融平台
    SERVTP_ECF("ECF", "ECIF系统"),//ECIF系统
    SERVTP_XVP("XVP", "小微业务管理平台"),//小微业务管理平台
    SERVTP_OAM("OAM", "办公自动化系统"),//办公自动化系统
    SERVTP_SYS("SYS", "核心系统"),//核心系统
    SERVTP_USC("USC", "统一资金授信系统"),//统一资金授信系统
    SERVTP_OTF("OTF", "中台外围前置"),//中台外围前置
    SERVTP_GAP("GAP", "中间业务系统（资金清算类)"),//中间业务系统（资金清算类)
    SERVTP_ZFP("ZFP", "二代支付系统"),//二代支付系统
    SERVTP_PJP("PJP", "综合票据系统"),//综合票据系统
    SERVTP_LCP("LCP", "理财销售系统"),//理财销售系统
    SERVTP_RLP("RLP", "人力资源系统"),//人力资源系统
    SERVTP_DYY("DYY", "单笔验印系统"),//单笔验印系统
    SERVTP_ZNT("ZNT", "智能厅堂"),//智能厅堂
    SERVTP_SZF("SZF", "超级网银"),//超级网银
    SERVTP_ZGL("ZGL", "账户管理系统"),//账户管理系统
    SERVTP_ZWD("ZWD", "智能微贷审批系统"),//智能微贷审批系统
    SERVTP_DJK("DJK", "贷记卡非金融系统"),//贷记卡非金融系统
    SERVTP_FUN("FUN", "基金系统"),//基金系统
    SERVTP_ZZS("ZZS", "营改增系统"),//营改增系统
    SERVTP_HZF("HZF", "聚合支付平台"),//聚合支付平台
    SERVTP_FLS("FLS", "非零售内部评级系统"),//非零售内部评级系统
    SERVTP_REK("REK", "融e开对公开户预约系统"),//融e开对公开户预约系统
    SERVTP_JFP("JFP", "积分系统"),//积分系统
    SERVTP_VBK("VBK", "视频银行"),//视频银行
    SERVTP_SMC("SMC", "开放银行"),//开放银行
    SERVTP_LSP("LSP", "零售内部评级系统"),//零售内部评级系统
    SERVTP_WGH("WGH", "网格化系统"),//网格化系统
    SERVTP_IBS("IBS", "云从人脸"),//云从人脸
    SERVTP_EID("EID", "eID网络身份认证"),//eID网络身份认证
    SERVTP_HED("HED", "头寸系统"),//头寸系统
    SERVTP_YQZ("YQZ", "银联前置系统"),//银联前置系统
    SERVTP_YQS("YQS", "银联清算系统"),//银联清算系统
    SERVTP_ESB("ESB", "ESB系统"),//ESB系统
    SERVTP_PSP("PSP", "POSP系统"),//POSP系统
    SERVTP_CAL("CAL", "呼叫中心"),//呼叫中心
    SERVTP_ZXP("ZXP", "征信管理系统"),//征信管理系统
    SERVTP_GKI("GKI", "管理会计系统"),//管理会计系统
    SERVTP_YQD("YQD", "优企贷"),//优企贷
    SERVTP_PMS("PMS", "资管理财信息报送平台"),//资管理财信息报送平台
    SERVTP_FPT("FPT", "运营风险预警平台"),//运营风险预警平台
    SERVTP_DXT("DXT", "监控平台（网络运维）"),//监控平台（网络运维）
    SERVTP_XCX("XCX", "零售小程序"),//零售小程序
    SERVTP_DHK("DHK", "环控平台（网络运维）"),//环控平台（网络运维）
    SERVTP_RZP("RZP", "日终调度系统"),//日终调度系统
    SERVTP_CWP("CWP", "财务操作系统"),//财务操作系统
    SERVTP_ZXF("ZXF", "直销支付平台"),//直销支付平台
    SERVTP_COM("COM", "资金管理系统"),//资金管理系统
    SERVTP_SBS("SBS", "comstar外汇系统"),//comstar外汇系统
    SERVTP_ICP("ICP", "IC卡应用系统"),//IC卡应用系统
    SERVTP_GXP("GXP", "GXP系统"),//GXP系统
    SERVTP_ZHD("ZHD", "自助回单系统"),//自助回单系统
    SERVTP_GFD("GFD", "光伏贷系统"),//光伏贷系统
    SERVTP_DZD("DZD", "银科对账系统"),//银科对账系统
    SERVTP_XMH("XMH", "新门户"),//新门户
    SERVTP_KGP("KGP", "科技管理平台"),//科技管理平台

    /** 渠道 结束 **/
    /**
     * 交易码 开始
     **/

    /**
     * 票据系统 开始
     **/
    TRADE_CODE_XDPJ14("xdpj14", "信贷签约通知"),//信贷签约通知
    TRADE_CODE_XDPJ12("xdpj12", "票据池推送保证金账号"),//票据池推送保证金账号
    TRADE_CODE_XDPJ15("xdpj15", "发票补录信息推送（信贷调票据）"),//发票补录信息推送（信贷调票据）
    TRADE_CODE_XDPJ22("xdpj22", "根据批次号查询票号和票面金额"),//根据批次号查询票号和票面金额
    TRADE_CODE_XDPJ03("xdpj03", "票据承兑签发审批请求"),//票据承兑签发审批请求
    TRADE_CODE_XDPJ004("xdpj004", "承兑签发审批结果综合服务接口"),
    TRADE_CODE_XDPJ21("xdpj21", "查询银票出账保证金账号信息（新信贷调票据）"),//查询银票出账保证金账号信息（新信贷调票据）
    TRADE_CODE_XDPJ23("xdpj23", "查询批次出账票据信息（新信贷调票据）"),//查询批次出账票据信息（新信贷调票据）
    TRADE_CODE_XDPJ24("xdpj24", "从票据系统获取当日到期票的日出备款金额"),//从票据系统获取当日到期票的日出备款金额
    TRADE_CODE_FKPJ35("fkpj35", "资产池出池解质押"),//资产池出池解质押
    /* 票据系统结束 */

    /**
     * 短信平台 开始
     **/
    TRADE_CODE_SENDDX("senddx", "短信/微信发送批量接口"),//短信/微信发送批量接口
    /** 短信平台 结束 **/
    /**
     * Comstar系统 开始
     **/
    TRADE_CODE_COM001("com001", "额度同步"),//额度同步
    /** Comstar系统 结束 **/
    /**
     * OCR系统 开始
     */
    TRADE_CODE_CBOCR1("cbocr1", "新增批次报表数据"),
    TRADE_CODE_CBOCR2("cbocr2", "识别任务的模板匹配结果列表查询"),
    /**
     * 档案系统开始
     */
    TRADE_CODE_DOC000("doc000", "入库接口"),
    TRADE_CODE_DOC002("doc002", "入库查询"),
    TRADE_CODE_DOC003("doc003", "出库"),
    TRADE_CODE_DOC005("doc005", "申请出库"),
    TRADE_CODE_DOC004("doc004", "调阅待出库查询"),
    TRADE_CODE_DOC006("doc006", "出库归还"),
    /**
     档案系统结束
     */
    /**
     * 风险预警系统 开始
     **/
    TRADE_CODE_DBSXCX("dbsxcx", "查询风险预警系统待办事项"),//查询风险预警系统待办事项

    TRADE_CODE_DJHHMD("djhhmd", "查询客户风险预警等级与是否黑灰名单"),//查询客户风险预警等级与是否黑灰名单

    TRADE_CODE_SFYXKH("sfyxkh", "查询客户是否为风险预警系统有效客户"),//查询客户是否为风险预警系统有效客户

    TRADE_CODE_LSFXBG("lsfxbg", "查询客户项下历史风险预警报告"),//查询客户项下历史风险预警报告

    TRADE_CODE_YXYJXH("yxyjxh", "查询客户项下有效预警信号"),//查询客户项下有效预警信号

    TRADE_CODE_LSBGXH("lsbgxh", "查询历史风险预警报告中保存的预警信号"),//查询历史风险预警报告中保存的预警信号

    TRADE_CODE_CXKHXH("cxkhxh", "查询某日某客户生成的预警信号"),//查询某日某客户生成的预警信号

    TRADE_CODE_HHMDKH("hhmdkh", "查询客户是否为黑灰名单客户"),//查询客户是否为黑灰名单客户

    TRADE_CODE_YJCUST("yjcust", "查询客户在风险预警系统是否有在途任务"),//查询客户在风险预警系统是否有在途任务
    /** 风险预警系统 结束 **/

    /**
     * ECIF 开始
     **/
    TRADE_CODE_S10501("s10501", "对私客户清单查询"),//对私客户清单查询

    TRADE_CODE_S00102("s00102", "对私客户创建及维护"),//对私客户创建及维护

    TRADE_CODE_G00102("g00102", "对公客户综合信息维护"),//对公客户综合信息维护

    TRADE_CODE_G10501("g10501", "对公及同业客户清单查询"),//对公及同业客户清单查询

    TRADE_CODE_G00202("g00202", "同业客户开户"),//同业客户开户

    TRADE_CODE_G00203("g00203", "同业客户维护"),//同业客户维护

    TRADE_CODE_G11003("g11003", "客户集团信息维护 (new)"),//客户集团信息维护 (new)

    TRADE_CODE_G11004("g11004", "客户集团信息查询（new）"),//客户集团信息查询（new）

    TRADE_CODE_G00101("g00101", "对公客户综合信息查询"),//对公客户综合信息查询

    TRADE_CODE_G10001("g10001", "客户群组信息查询"),

    TRADE_CODE_G10002("g10002", "客户群组信息维护"),
    /** ECIF 结束 **/
    /**
     * 二代征信 开始
     **/
    TRADE_CODE_CREDI1("credi1", "ESB信贷查询接口"),//ESB信贷查询接口

    TRADE_CODE_CRED12("cred12", "ESB通用查询接口"),//ESB通用查询接口

    TRADE_CODE_CREDXX("credxx", "线下查询接口"),//线下查询接口

    TRADE_CODE_CREDZB("credzb", "指标通用接口"),//指标通用接口
    /** 二代征信 结束 **/
    /**
     * 苏州地方征信 开始
     **/
    TRADE_CODE_ZXCX006("dfzx01", "信贷查询地方征信接口"),//信贷查询地方征信接口

    /**
     * 苏州地方征信 结束
     **/
    TRADE_CODE_WXP001("wxp001", "信贷将审批结果推送给移动端"),
    TRADE_CODE_WXP002("wxp002", "信贷将授信额度推送给移动端"),
    TRADE_CODE_WXP003("wxp003", "信贷将放款标识推送给移动端"),

    /**
     * 零售内评系统 开始
     **/
    TRADE_CODE_LSNP01("lsnp01", "信贷业务零售评级"),//信贷业务零售评级

    TRADE_CODE_LSNP02("lsnp02", "信用卡业务零售评级"),//信用卡业务零售评级

    TRADE_CODE_LSNP03("lsnp03", "小贷业务零售评级"),//小贷业务零售评级
    /** 零售内评系统 结束**/
    /**
     * 对公智能风控 开始
     **/
    TRADE_CODE_FB1161("fb1161", "借据信息同步"),//借据信息同步

    TRADE_CODE_FB1166("fb1166", "面签邀约结果推送"),//面签邀约结果推送

    TRADE_CODE_FB1167("fb1167", "企业征信查询通知"),//企业征信查询通知

    TRADE_CODE_FB1168("fb1168", "抵押查封结果推送"),//抵押查封结果推送

    TRADE_CODE_FB1170("fb1170", "最高额借款合同信息推送"),//最高额借款合同信息推送

    TRADE_CODE_FB1174("fb1174", "房抵e点贷尽调结果通知"),//房抵e点贷尽调结果通知

    TRADE_CODE_FB1203("fb1203", "质押物金额覆盖校验"),//质押物金额覆盖校验

    TRADE_CODE_FB1205("fb1205", "放款信息推送"),//放款信息推送

    TRADE_CODE_FB1213("fb1213", "押品出库通知"),//押品出库通知

    TRADE_CODE_FB1214("fb1214", "房产信息修改同步"),//房产信息修改同步

    TRADE_CODE_FB1146("fb1146", "受托信息审核"),//受托信息审核

    TRADE_CODE_FB1147("fb1147", "无还本续贷申请"),//无还本续贷申请

    TRADE_CODE_FB1149("fb1149", "无还本续贷借据更新"),//无还本续贷借据更新

    TRADE_CODE_FB1150("fb1150", "省心快贷plus授信申请"),//省心快贷plus授信申请
    /** 对公智能风控 结束**/

    /**
     * 二代支付系统 开始
     **/
    TRADE_CODE_HVPYLS("hvpyls", "大额往账列表查询"),//大额往账列表查询
    TRADE_CODE_DJZTCX("djztcx", "贷记入账状态查询申请往帐"),//贷记入账状态查询申请往帐
    TRADE_CODE_HVPSMR("hvpsmr", "大额往帐一体化"),//大额往帐一体化

    /**二代支付系统 结束**/

    /**
     * 零售智能风控 开始
     **/
    TRADE_CODE_FBXW01("fbxw01", "授信申请提交"),//授信申请提交

    TRADE_CODE_FBXW02("fbxw02", "终审结果同步"),//终审结果同步

    TRADE_CODE_FBXW03("fbxw03", "增享贷风控测算"),//增享贷风控测算

    TRADE_CODE_FBYD02("fbyd02", "终审申请提交"),//终审申请提交

    TRADE_CODE_FBYD33("fbyd33", "预授信申请提交"),//预授信申请提交

    TRADE_CODE_FBYD34("fbyd34", "支用模型校验"),//支用模型校验

    TRADE_CODE_FBYD36("fbyd36", "授信列表查询"),//授信列表查询

    TRADE_CODE_FKYX01("fkyx01", "优享贷客户经理分配通知接口"),//优享贷客户经理分配通知接口

    TRADE_CODE_FBXW04("fbxw04", "惠享贷规则审批申请接口"),//惠享贷规则审批申请接口

    TRADE_CODE_FBXW05("fbxw05", "惠享贷批复同步接口"),//惠享贷批复同步接口

    TRADE_CODE_FBXW06("fbxw06", "利率定价测算提交接口"),//利率定价测算提交接口

    TRADE_CODE_FBXW07("fbxw07", "优惠利率申请接口"),//优惠利率申请接口

    TRADE_CODE_FBXW08("fbxw08", "授信审批作废"),//授信审批作废

    TRADE_CODE_FBXD01("fbxd01", "惠享贷授信申请件数取得"),//惠享贷授信申请件数取得

    TRADE_CODE_FBXD02("fbxd02", "根据核心客户号取得客户手机号码"),//根据核心客户号取得客户手机号码

    TRADE_CODE_FBXD03("fbxd03", "从风控获取客户详细信息"),//从风控获取客户详细信息

    TRADE_CODE_FBXD04("fbxd04", "查找指定数据日期的放款合约明细记录历史表（利翃）一览"),//查找指定数据日期的放款合约明细记录历史表（利翃）一览

    TRADE_CODE_FBXD05("fbxd05", "查找指定数据日期的放款合约明细记录历史表（利翃）一览信息的借据号、借据金额、借据余额"),//查找指定数据日期的放款合约明细记录历史表（利翃）一览信息的借据号、借据金额、借据余额

    TRADE_CODE_FBXD06("fbxd06", "获取该笔借据的最新五级分类以及数据日期"),//获取该笔借据的最新五级分类以及数据日期

    TRADE_CODE_FBXD07("fbxd07", "获取指定数据日期存在还款记录的借据一览信息，包括借据号、借据金额、借据余额"),//获取指定数据日期存在还款记录的借据一览信息，包括借据号、借据金额、借据余额

    TRADE_CODE_FBXD08("fbxd08", "查找（利翃）实还正常本金和逾期本金之和"),//查找（利翃）实还正常本金和逾期本金之和

    TRADE_CODE_FBXD09("fbxd09", "查询日初（合约）信息历史表（利翃）总数据量"),//查询日初（合约）信息历史表（利翃）总数据量

    TRADE_CODE_FBXD10("fbxd10", "查询日初（合约）信息历史表（利翃）的合约状态和结清日期"),//查询日初（合约）信息历史表（利翃）的合约状态和结清日期

    TRADE_CODE_FBXD11("fbxd11", "获取还款记录的借据一览信息，包括借据号、借据金额、借据余额"),//获取还款记录的借据一览信息，包括借据号、借据金额、借据余额

    TRADE_CODE_FBXD13("fbxd13", "查询客户核心编号，客户名称对应的放款和还款信息"),//查询客户核心编号，客户名称对应的放款和还款信息

    TRADE_CODE_FBXD14("fbxd14", "查询还款（合约）明细历史表（利翃）中的全量借据一览"),//查询还款（合约）明细历史表（利翃）中的全量借据一览

    TRADE_CODE_FBXD15("fbxd15", "还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和"),//还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和

    TRADE_CODE_FBXD16("fbxd16", "取得蚂蚁核销记录表的核销借据号一览"),//取得蚂蚁核销记录表的核销借据号一览

    /** 零售智能风控 结束 **/

    /**
     * 非零售内评系统 开始
     **/
    TRADE_CODE_IRS10("xirs10", "同步公司客户信息"),//同步公司客户信息

    TRADE_CODE_IRS11("xirs11", "同步同业客户信息"),//同步同业客户信息

    TRADE_CODE_IRS15("xirs15", "评级在途申请标识"),//评级在途申请标识

    TRADE_CODE_IRS18("xirs18", "评级主办权更新"),//评级主办权更新

    TRADE_CODE_IRS19("xirs19", "专业贷款基本信息同步"),//专业贷款基本信息同步

    TRADE_CODE_IRS21("xirs21", "单一客户限额测算信息同步"),//单一客户限额测算信息同步

    TRADE_CODE_IRS27("xirs27", "工作台提示条数"),//工作台提示条数

    TRADE_CODE_IRS28("xirs28", "财务信息同步"),//财务信息同步

    TRADE_CODE_IRS97("xirs97", "新增授信时债项评级"),//新增授信时债项评级

    TRADE_CODE_IRS98("xirs98", "授信申请债项评级"),//授信申请债项评级

    TRADE_CODE_IRS99("xirs99", "业务申请债项评级"),//业务申请债项评级
    /** 非零售内评系统 开始 **/
    /**
     * 押品系统 开始
     **/
    TRADE_CODE_EVALYP("evalyp", "押品我行确认价值同步接口"),//押品我行确认价值同步接口

    TRADE_CODE_CXYPBH("cxypbh", "通过票据号码查询押品编号"),//通过票据号码查询押品编号

    TRADE_CODE_CONINF("coninf", "信贷担保合同信息同步"),//信贷担保合同信息同步

    TRADE_CODE_LMTINF("lmtinf", "信贷授信协议信息同步"),//信贷授信协议信息同步

    TRADE_CODE_BUSINF("businf", "信贷业务合同信息同步"),//信贷业务合同信息同步

    TRADE_CODE_BUSCON("buscon", "信贷业务与押品关联关系信息同步"),//信贷业务与押品关联关系信息同步

    TRADE_CODE_CONTRA("contra", "押品与担保合同关系同步"),//押品与担保合同关系同步

    TRADE_CODE_BUCONT("bucont", "业务与担保合同关系"),//业务与担保合同关系

    TRADE_CODE_CERTIS("certis", "权证状态同步"),//权证状态同步

    TRADE_CODE_CETINF("cetinf", "权证信息同步"),//权证信息同步

    TRADE_CODE_MANAGE("manage", "管护权移交信息同步"),//管护权移交信息同步

    TRADE_CODE_DEPOST("depost", "保证金信息同步"),//保证金信息同步

    TRADE_CODE_DEALSY("dealsy", "押品处置信息同步"),//押品处置信息同步

    TRADE_CODE_SPSYYP("spsyyp", "信贷审批结果同步接口"),//信贷审批结果同步接口

    TRADE_CODE_BILLYP("billyp", "票据信息同步接口"),//票据信息同步接口

    TRADE_CODE_YHXXTB("yhxxtb", "用户信息同步接口"),//用户信息同步接口

    TRADE_CODE_BDCQCX("bdcqcx", "不动产信息查询"),//不动产信息查询

    TRADE_CODE_YPZTBG("ypztbg", "押品状态变更"),//押品状态变更

    TRADE_CODE_ZCXXCX("zcxxcx", "资产信息查询"),//资产信息查询

    TRADE_CODE_XDYPGYRCX("xddb06", "查询共有人信息"),//资产信息查询

    TRADE_CODE_XDYPJBXXCX("xddb05", "查询基本信息"),//查询基本信息

    TRADE_CODE_XDYPBDCCX("xddb01", "查询不动产信息"),//查询不动产信息

    TRADE_CODE_XDYPZYWCX("xddb04", "查询质押物信息"),//查询质押物信息

    TRADE_CODE_XDYPDYWCX("xddb02", "查询抵押物信息"),//查询抵押物信息

    TRADE_CODE_XDYPCDPJCX("xddb03", "查询存单票据信息"),//查询存单票据信息

    TRADE_CODE_CREDITYPIS("credis", "信用证信息同步"),//信用证信息同步

    TRADE_CODE_XDYPXXTBYR("xdjzzy", "押品信息同步及引入"),//押品信息同步及引入

    TRADE_CODE_ZTOD01("ztod01", "融E开-工商数据查询"),//融E开-工商数据查询

    TRADE_CODE_GUARST("guarst", "押品状态变更推送"),//押品状态变更推送

    TRADE_CODE_CBOCR3("cbocr3", "获取cbocr3财报识别信息"),

    TRADE_CODE_XDDB01("xddb01", "查询不动产信息"),

    TRADE_CODE_XDDB02("xddb02", "查询抵押物信息"),

    TRADE_CODE_XDDB03("xddb03", "查询存单票据信息"),

    TRADE_CODE_XDDB04("xddb04", "查询质押物信息"),

    TRADE_CODE_XDDB05("xddb05", "查询基本信息"),

    TRADE_CODE_XDDB06("xddb06", "查询共有人信息"),

    TRADE_CODE_CREDIS("credis", "信用证信息同步"),

    TRADE_CODE_DEPOIS("depois", "存单信息同步"),

    TRADE_CODE_XDJZZY("xdjzzy", "押品信息同步及引入"),

    TRADE_CODE_GYYPBH("gyypbh", "通过共有人编号查询押品编号"),

    TRADE_CODE_YPZTCX("ypztcx", "信贷押品状态查询"),

    /** 押品系统 结束 **/
    /**
     * 押品权证系统 开始
     **/
    TRADE_CODE_YPRKSQ("yprksq", "入库申请"),//入库申请

    TRADE_CODE_YPCKSQ("ypcksq", "出库申请"),//出库申请

    TRADE_CODE_ZPDBDA("zpdbda", "承兑汇票申请"),//承兑汇票申请

    TRADE_CODE_BRIDSQ("bridsq", "获取账户机构号及名称"),//获取账户机构号及名称

    TRADE_CODE_CWM001("cwm001", "本异地借阅出库"),//本异地借阅出库

    TRADE_CODE_CWM002("cwm002", "本异地借阅归还"),//本异地借阅归还

    TRADE_CODE_CWM003("cwm003", "押品权证入库接口"),//押品权证入库接口

    TRADE_CODE_CWM004("cwm004", "押品权证出库接口"),//押品权证出库接口

    TRADE_CODE_CWM007("cwm007", "押品状态查询接口"),//押品状态查询接口
    /** 押品权证系统 结束 **/
    /**
     * 智能微贷审批系统 开始
     **/
    TRADE_CODE_ZNSP0001("ZNSP0001", "提交调查报告"),//提交调查报告

    TRADE_CODE_ZNSP04("znsp04", "自动审批调查报告"),//自动审批调查报告

    TRADE_CODE_ZNSP07("znsp07", "调查表数据修改接口"),//调查表数据修改接口

    TRADE_CODE_ZNSP08("znsp08", "客户调查撤销接口"),//客户调查撤销接口

    TRADE_CODE_ZNSP05("znsp05", "风险拦截截接口"),//风险拦截截接口

    TRADE_CODE_ZNSP06("znsp06", "被拒绝的线上产品推送接口"),//被拒绝的线上产品推送接口
    /** 智能微贷审批系统 结束 **/

    /**
     * 小微公众号开始
     */
    TRADE_CODE_XWH001("xwh001", "借据台账信息接收"),//借据台账信息接收

    TRADE_CODE_XWH003("xwh003", "核销锁定接口"),

    TRADE_CODE_YX0003("yx0003", "市民贷优惠券核销锁定"),

    /**
     * 小微业务管理平台 开始
     **/
    TRADE_CODE_WXD001("wxd001", "直销银行请求V平台贷款申请接口"),//直销银行请求V平台贷款申请接口

    TRADE_CODE_WXD002("wxd002", "直销银行请求V平台执行结果接口"),//直销银行请求V平台执行结果接口

    TRADE_CODE_WXD003("wxd003", "信贷系统请求V平台二级准入接口"),//信贷系统请求V平台二级准入接口

    TRADE_CODE_WXD004("wxd004", "信贷系统获取征信报送监管信息接口"),//信贷系统获取征信报送监管信息接口

    TRADE_CODE_XDYD04("xdyd04", "信贷系统请求小V平台优抵贷产品二级准入接口"),//信贷系统请求小V平台优抵贷产品二级准入接口

    TRADE_CODE_XDYD05("xdyd05", "信贷系统请求小V平台新旧序列号替换接口"),//信贷系统请求小V平台新旧序列号替换接口

    TRADE_CODE_WXD006("wxd006", "直销系统请求小V平台优农贷支用模型接口"),//直销系统请求小V平台优农贷支用模型接口

    TRADE_CODE_WXD007("wxd007", "直销系统请求小V平台综合决策管理列表查询"),//直销系统请求小V平台综合决策管理列表查询

    TRADE_CODE_GZH001("gzh001", "微信公众号请求小V平台申请优惠利率接口"),//微信公众号请求小V平台申请优惠利率接口

    TRADE_CODE_YDOA01("ydoa01", "优企、优农拒绝原因查询"),//优企、优农拒绝原因查询

    TRADE_CODE_WXD008("wxd008", "直销银行请求小V平台支用申请接口"),//直销银行请求小V平台支用申请接口

    TRADE_CODE_WXD009("wxd009", "信贷系统请求小V平台推送合同信息接口"),//信贷系统请求小V平台推送合同信息接口

    TRADE_CODE_XWD001("xwd001", "贷款申请接口"),

    TRADE_CODE_XWD008("xwd008", "新信贷同步用户账号"),

    TRADE_CODE_XWD009("xwd009", "利率申请接口"),

    TRADE_CODE_XWD010("xwd010", "利率撤销接口"),

    TRADE_CODE_XWD013("xwd013", "新信贷获取调查信息接口"),

    TRADE_CODE_WXD010("wxd010", "直销银行请求小V平台获取青岛税务授权id接口"),//直销银行请求小V平台获取青岛税务授权id接口

    TRADE_CODE_DHXD01("dhxd01", "信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口"),//信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口

    TRADE_CODE_DHXD02("dhxd02", "信贷系统请求小V平台贷后预警回传定期检查任务结果"),//信贷系统请求小V平台贷后预警回传定期检查任务结果

    /**
     * 核心系统 开始
     **/
    TRADE_CODE_MBT999("mbt999", "V5通用记账"),//V5通用记账

    TRADE_CODE_MBT951("mbt951", "批量文件处理申请"),

    TRADE_CODE_MBT952("mbt952", "批量结果确认处理"),

    TRADE_CODE_DA3301("da3301", "抵债资产入账"),//抵债资产入账

    TRADE_CODE_DA3304("da3304", "抵债资产机构转移"),//抵债资产机构转移

    TRADE_CODE_DA3305("da3305", "待变现抵债资产销账"),//待变现抵债资产销账

    TRADE_CODE_DA3306("da3306", "抵债资产拨备计提"),//抵债资产拨备计提

    TRADE_CODE_DA3307("da3307", "抵债资产出租处理"),//抵债资产出租处理

    TRADE_CODE_DA3302("da3302", "抵债资产处置"),//抵债资产处置

    TRADE_CODE_DA3308("da3308", "抵债资产费用管理"),//抵债资产费用管理

    TRADE_CODE_DA3300("da3300", "交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理"),//交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理

    TRADE_CODE_DA3303("da3303", "抵债资产信息维护"),//抵债资产信息维护

    TRADE_CODE_DA3320("da3320", "查询抵债资产信息以及与贷款、费用、出租的关联信息"),//查询抵债资产信息以及与贷款、费用、出租的关联信息

    TRADE_CODE_DA3322("da3322", "抵债资产明细查询"),

    TRADE_CODE_DA3321("da3321", "抵债资产模糊查询"),//抵债资产模糊查询

    TRADE_CODE_DP2099("dp2099", "保证金账户查询"),//保证金账户查询

    TRADE_CODE_DP2021("dp2021", "客户账号-子账号互查"),

    TRADE_CODE_DP2098("dp2098", "待清算账户查询"),

    TRADE_CODE_DP2667("dp2667", "组合账户特殊账户查询"),//组合账户特殊账户查询

    TRADE_CODE_DP2200("dp2200", "保证金账户部提"),//保证金账户部提

    TRADE_CODE_DP2352("dp2352", "组合账户子账户开立"),//组合账户子账户开立

    TRADE_CODE_IB1241("ib1241", "外围自动冲正(出库撤销)"),//外围自动冲正(出库撤销)

    TRADE_CODE_IB1243("ib1243", "通用电子记帐交易（多借多贷-多币种）"),//通用电子记帐交易（多借多贷-多币种）

    TRADE_CODE_IB1253("ib1253", "根据账号查询帐户信息"),//根据账号查询帐户信息

    TRADE_CODE_CO3200("co3200", "抵质押物开户"),//抵质押物开户

    TRADE_CODE_CO3201("co3201", "抵质押物信息修改"),//抵质押物信息修改

    TRADE_CODE_CO3202("co3202", "抵质押物出入库"),//抵质押物出入库

    TRADE_CODE_CO3203("co3203", "抵质押物关联关系维护"),//抵质押物关联关系维护

    TRADE_CODE_CO3204("co3204", "抵质押物机构转移"),//抵质押物机构转移

    TRADE_CODE_CO3205("co3205", "抵质押物组信息维护"),//抵质押物组信息维护

    TRADE_CODE_CO3220("co3220", "抵质押物关联贷款查询"),//抵质押物关联贷款查询

    TRADE_CODE_CO3221("co3221", "抵质押物组合查询"),//抵质押物组合查询

    TRADE_CODE_CO3222("co3222", "抵质押物单笔查询"),//抵质押物单笔查询

    TRADE_CODE_CO3223("co3223", "抵质押物组单笔查询"),//抵质押物组单笔查询

    TRADE_CODE_CO3224("co3224", "抵质押物风险敞口查询"),//抵质押物风险敞口查询

    TRADE_CODE_CO3225("co3225", "抵质押物明细查询"),//抵质押物明细查询

    TRADE_CODE_LN3000("ln3000", "贷款产品维护"),//贷款产品维护

    TRADE_CODE_LN3001("ln3001", "贷款产品拷贝"),//贷款产品拷贝

    TRADE_CODE_LN3002("ln3002", "贷款产品删除"),//贷款产品删除

    TRADE_CODE_LN3003("ln3003", "贷款产品分类维护"),//贷款产品分类维护

    TRADE_CODE_LN3005("ln3005", "贷款产品查询"),//贷款产品查询

    TRADE_CODE_LN3006("ln3006", "贷款产品组合查询"),//贷款产品组合查询

    TRADE_CODE_LN3007("ln3007", "资产产品币种查询"),//资产产品币种查询

    TRADE_CODE_LN3008("ln3008", "贷款产品变更明细查询"),//贷款产品变更明细查询

    TRADE_CODE_LN3009("ln3009", "贷款产品差异化服务登记"),//贷款产品差异化服务登记

    TRADE_CODE_LN3010("ln3010", "互联网贷款产品维护"),//互联网贷款产品维护

    TRADE_CODE_LN3011("ln3011", "互联网贷款产品拷贝"),//互联网贷款产品拷贝

    TRADE_CODE_LN3012("ln3012", "互联网贷款产品删除"),//互联网贷款产品删除

    TRADE_CODE_LN3013("ln3013", "互联网贷款产品信息查询"),//互联网贷款产品信息查询

    TRADE_CODE_LN3014("ln3014", "产品视图"),//产品视图

    TRADE_CODE_LN3015("ln3015", "额度应用规则维护"),//额度应用规则维护

    TRADE_CODE_LN3016("ln3016", "存贷组合规则维护"),//存贷组合规则维护

    TRADE_CODE_LN3017("ln3017", "存贷组合规则查询"),//存贷组合规则查询

    TRADE_CODE_LN3018("ln3018", "额度应用规则查询"),//额度应用规则查询

    TRADE_CODE_LN3019("ln3019", "借据号生成"),//借据号生成

    TRADE_CODE_LN3020("ln3020", "贷款开户"),//贷款开户

    TRADE_CODE_LN3021("ln3021", "简单贷款开户"),//简单贷款开户

    TRADE_CODE_LN3022("ln3022", "按揭贷款开户"),//按揭贷款开户

    TRADE_CODE_LN3023("ln3023", "委托贷款开户"),//委托贷款开户

    TRADE_CODE_LN3024("ln3024", "特殊贷款开户"),//特殊贷款开户

    TRADE_CODE_LN3025("ln3025", "垫款开户"),//垫款开户

    TRADE_CODE_LN3026("ln3026", "银团贷款开户"),//银团贷款开户

    TRADE_CODE_LN3027("ln3027", "互联网贷款开户"),//互联网贷款开户

    TRADE_CODE_LN3028("ln3028", "贸融ABS柜面备款"),//贸融ABS柜面备款

    TRADE_CODE_LN3029("ln3029", "费用维护"),//费用维护

    TRADE_CODE_LN3030("ln3030", "贷款资料维护"),//贷款资料维护

    TRADE_CODE_LN3031("ln3031", "贷款资料维护关于还款"),//贷款资料维护关于还款

    TRADE_CODE_LN3032("ln3032", "受托支付信息维护"),//受托支付信息维护

    TRADE_CODE_LN3033("ln3033", "批量测试交易"),//批量测试交易

    TRADE_CODE_LN3034("ln3034", "还款控制维护"),//还款控制维护

    TRADE_CODE_LN3035("ln3035", "贷款录入数据查询"),//贷款录入数据查询

    TRADE_CODE_LN3036("ln3036", "贷款录入信息查询"),//贷款录入信息查询

    TRADE_CODE_LN3037("ln3037", "贷款文件导入登记簿查询"),//贷款文件导入登记簿查询

    TRADE_CODE_LN3038("ln3038", "贷款资料变更明细查询"),//贷款资料变更明细查询

    TRADE_CODE_LN3039("ln3039", "质押还贷批扣预约与取消"),//质押还贷批扣预约与取消

    TRADE_CODE_LN3040("ln3040", "贷款发放"),//贷款发放

    TRADE_CODE_LN3041("ln3041", "贷款归还"),//贷款归还

    TRADE_CODE_LN3042("ln3042", "贷款特殊归还"),//贷款特殊归还

    TRADE_CODE_LN3043("ln3043", "贷款指定期供归还"),//贷款指定期供归还

    TRADE_CODE_LN3044("ln3044", "贷款形态转移"),//贷款形态转移

    TRADE_CODE_LN3045("ln3045", "贷款核销处理"),//贷款核销处理

    TRADE_CODE_LN3046("ln3046", "贷款核销归还"),//贷款核销归还

    TRADE_CODE_LN3047("ln3047", "质押还贷"),//质押还贷

    TRADE_CODE_LN3048("ln3048", "贷款本息调账"),//贷款本息调账

    TRADE_CODE_LN3049("ln3049", "贷款受托支付解付"),//贷款受托支付解付

    TRADE_CODE_LN3050("ln3050", "贴息归还"),//贴息归还

    TRADE_CODE_LN3051("ln3051", "贷款加按揭"),//贷款加按揭

    TRADE_CODE_LN3052("ln3052", "贷款挪用利率惩罚"),//贷款挪用利率惩罚

    TRADE_CODE_LN3053("ln3053", "还款计划调整"),//还款计划调整

    TRADE_CODE_LN3054("ln3054", "还款方式调整"),//还款方式调整

    TRADE_CODE_LN3055("ln3055", "贷款利率调整"),//贷款利率调整

    TRADE_CODE_LN3056("ln3056", "印花税缴纳"),//印花税缴纳

    TRADE_CODE_LN3057("ln3057", "贷款期限变更"),//贷款期限变更

    TRADE_CODE_LN3058("ln3058", "委托贷款豁免"),//委托贷款豁免

    TRADE_CODE_LN3059("ln3059", "贷款简单信息维护"),//贷款简单信息维护

    TRADE_CODE_LN3060("ln3060", "转让证券化借据组合查询"),//转让证券化借据组合查询

    TRADE_CODE_LN3061("ln3061", "资产转让协议登记"),//资产转让协议登记

    TRADE_CODE_LN3062("ln3062", "资产转让借据维护"),//资产转让借据维护

    TRADE_CODE_LN3063("ln3063", "资产转让处理"),//资产转让处理

    TRADE_CODE_LN3064("ln3064", "资产转让内部借据信息查询"),//资产转让内部借据信息查询

    TRADE_CODE_LN3065("ln3065", "资产转让资金划转"),//资产转让资金划转

    TRADE_CODE_LN3066("ln3066", "资产转让借据筛选"),//资产转让借据筛选

    TRADE_CODE_LN3067("ln3067", "资产转让赎回单笔"),//资产转让赎回单笔

    TRADE_CODE_LN3068("ln3068", "资产转让信息查询"),//资产转让信息查询

    TRADE_CODE_LN3069("ln3069", "资产转让证券化组合查询"),//资产转让证券化组合查询

    TRADE_CODE_LN3070("ln3070", "贷款指令取消"),//贷款指令取消

    TRADE_CODE_LN3071("ln3071", "按揭项目维护"),//按揭项目维护

    TRADE_CODE_LN3072("ln3072", "计费维护"),//计费维护

    TRADE_CODE_LN3073("ln3073", "贷款定制期供计划维护"),//贷款定制期供计划维护

    TRADE_CODE_LN3074("ln3074", "贷款预展期"),//贷款预展期

    TRADE_CODE_LN3075("ln3075", "逾期贷款展期"),//逾期贷款展期

    TRADE_CODE_LN3076("ln3076", "贷款账本息调整"),//贷款账本息调整

    TRADE_CODE_LN3077("ln3077", "客户账本息调整"),//客户账本息调整

    TRADE_CODE_LN3078("ln3078", "贷款机构变更"),//贷款机构变更

    TRADE_CODE_LN3079("ln3079", "贷款产品变更"),//贷款产品变更

    TRADE_CODE_LN3080("ln3080", "贷款文件传输"),//贷款文件传输

    TRADE_CODE_LN3081("ln3081", "鑫转贷交易维护"),//鑫转贷交易维护

    TRADE_CODE_LN3082("ln3082", "贷款处理模式转换"),//贷款处理模式转换

    TRADE_CODE_LN3083("ln3083", "银团协议查询"),//银团协议查询

    TRADE_CODE_LN3084("ln3084", "银团协议维护"),//银团协议维护

    TRADE_CODE_LN3085("ln3085", "贷款本息减免"),//贷款本息减免

    TRADE_CODE_LN3086("ln3086", "贷款额度续签"),//贷款额度续签

    TRADE_CODE_LN3087("ln3087", "存抵贷签约"),//存抵贷签约

    TRADE_CODE_LN3088("ln3088", "存抵贷信息维护"),//存抵贷信息维护

    TRADE_CODE_LN3089("ln3089", "存抵贷解约"),//存抵贷解约

    TRADE_CODE_LN3090("ln3090", "客户预约还款"),//客户预约还款

    TRADE_CODE_LN3091("ln3091", "待付款指令查询"),//待付款指令查询

    TRADE_CODE_LN3092("ln3092", "付款登记查询"),//付款登记查询

    TRADE_CODE_LN3093("ln3093", "付款借据本息调整"),//付款借据本息调整

    TRADE_CODE_LN3094("ln3094", "贷款流水冲正控制维护"),//贷款流水冲正控制维护

    TRADE_CODE_LN3095("ln3095", "资产转让手工记损益"),//资产转让手工记损益

    TRADE_CODE_LN3096("ln3096", "微秒贷通用记账"),//微秒贷通用记账

    TRADE_CODE_LN3097("ln3097", "资产转让付款记录明细查询"),//资产转让付款记录明细查询

    TRADE_CODE_LN3098("ln3098", "联合贷款划付本息"),//联合贷款划付本息

    TRADE_CODE_LN3099("ln3099", "贷款欠款信息查询"),//贷款欠款信息查询

    TRADE_CODE_LN3100("ln3100", "贷款信息查询"),//贷款信息查询

    TRADE_CODE_LN3101("ln3101", "贷款概要信息查询"),//贷款概要信息查询

    TRADE_CODE_LN3102("ln3102", "贷款期供查询试算"),//贷款期供查询试算

    TRADE_CODE_LN3103("ln3103", "贷款账户交易明细查询"),//贷款账户交易明细查询

    TRADE_CODE_LN3104("ln3104", "客户账交易明细查询"),//客户账交易明细查询

    TRADE_CODE_LN3105("ln3105", "贷款期供交易明细查询"),//贷款期供交易明细查询

    TRADE_CODE_LN3106("ln3106", "贷款利率变更明细查询"),//贷款利率变更明细查询

    TRADE_CODE_LN3107("ln3107", "贷款形态转移明细查询"),//贷款形态转移明细查询

    TRADE_CODE_LN3108("ln3108", "贷款组合查询"),//贷款组合查询

    TRADE_CODE_LN3109("ln3109", "贷款贴息交易明细查询"),//贷款贴息交易明细查询

    TRADE_CODE_LN3110("ln3110", "贷款还款计划试算"),//贷款还款计划试算

    TRADE_CODE_LN3111("ln3111", "贷款归还试算"),//贷款归还试算

    TRADE_CODE_LN3112("ln3112", "贷款展期查询"),//贷款展期查询

    TRADE_CODE_LN3113("ln3113", "贷款计息历史查询"),//贷款计息历史查询

    TRADE_CODE_LN3114("ln3114", "贷款提前还款期供试算"),//贷款提前还款期供试算

    TRADE_CODE_LN3115("ln3115", "贷款账户变更明细查询"),//贷款账户变更明细查询

    TRADE_CODE_LN3116("ln3116", "贷款应收贴息查询"),//贷款应收贴息查询

    TRADE_CODE_LN3117("ln3117", "贷款按揭项目查询"),//贷款按揭项目查询

    TRADE_CODE_LN3118("ln3118", "贷款以存抵贷信息查询"),//贷款以存抵贷信息查询

    TRADE_CODE_LN3119("ln3119", "贷款账户关联明细查询"),//贷款账户关联明细查询

    TRADE_CODE_LN3120("ln3120", "信管交易结果查询"),//信管交易结果查询

    TRADE_CODE_LN3121("ln3121", "客户回单打印"),//客户回单打印

    TRADE_CODE_LN3122("ln3122", "贷款每日计息明细查询"),//贷款每日计息明细查询

    TRADE_CODE_LN3123("ln3123", "受托支付查询"),//受托支付查询

    TRADE_CODE_LN3124("ln3124", "快捷贷款组合查询"),//快捷贷款组合查询

    TRADE_CODE_LN3125("ln3125", "循环贷款概要信息查询"),//循环贷款概要信息查询

    TRADE_CODE_LN3126("ln3126", "贷款费用交易明细查询"),//贷款费用交易明细查询

    TRADE_CODE_LN3127("ln3127", "贷款差异化服务明细"),//贷款差异化服务明细

    TRADE_CODE_LN3128("ln3128", "贷款机构变更查询"),//贷款机构变更查询

    TRADE_CODE_LN3129("ln3129", "贷款利息回单"),//贷款利息回单

    TRADE_CODE_LN3130("ln3130", "还款控制查询"),//还款控制查询

    TRADE_CODE_LN3131("ln3131", "贷款汇率折算明细查询"),//贷款汇率折算明细查询

    TRADE_CODE_LN3132("ln3132", "公积金代扣文件查询"),//公积金代扣文件查询

    TRADE_CODE_LN3133("ln3133", "贷款账户统计信息查询"),//贷款账户统计信息查询

    TRADE_CODE_LN3134("ln3134", "贷款费用计划查询"),//贷款费用计划查询

    TRADE_CODE_LN3135("ln3135", "贷款定制期供计划查询"),//贷款定制期供计划查询

    TRADE_CODE_LN3136("ln3136", "贷款计提明细查询"),//贷款计提明细查询

    TRADE_CODE_LN3137("ln3137", "贷款鑫转贷信息查询"),//贷款鑫转贷信息查询

    TRADE_CODE_LN3138("ln3138", "等额等本贷款推算"),//等额等本贷款推算

    TRADE_CODE_LN3139("ln3139", "欠息明细试算"),//欠息明细试算

    TRADE_CODE_LN3140("ln3140", "贷款费用代收登记薄查询"),//贷款费用代收登记薄查询

    TRADE_CODE_LN3141("ln3141", "内部借据归还明细查询"),//内部借据归还明细查询

    TRADE_CODE_LN3142("ln3142", "银团协议查询"),//银团协议查询

    TRADE_CODE_LN3143("ln3143", "客户还款试算"),//客户还款试算

    TRADE_CODE_LN3144("ln3144", "额度续签查询"),//额度续签查询

    TRADE_CODE_LN3145("ln3145", "存抵贷签约组合查询"),//存抵贷签约组合查询

    TRADE_CODE_LN3146("ln3146", "存抵贷签约信息查询"),//存抵贷签约信息查询

    TRADE_CODE_LN3147("ln3147", "签约收益试算查询"),//签约收益试算查询

    TRADE_CODE_LN3148("ln3148", "组合转账"),//组合转账

    TRADE_CODE_LN3150("ln3150", "信贷资产融通产品配置"),//信贷资产融通产品配置

    TRADE_CODE_LN3153("ln3153", "信贷资产融通产品单笔查询"),//信贷资产融通产品单笔查询

    TRADE_CODE_LN3154("ln3154", "信贷资产融通产品组合查询"),//信贷资产融通产品组合查询

    TRADE_CODE_LN3156("ln3156", "信贷资产融通协议登记"),//信贷资产融通协议登记

    TRADE_CODE_LN3157("ln3157", "协议信息维护"),//协议信息维护

    TRADE_CODE_LN3158("ln3158", "融通协议贷款维护"),//融通协议贷款维护

    TRADE_CODE_LN3159("ln3159", "资金划转账号维护"),//资金划转账号维护

    TRADE_CODE_LN3160("ln3160", "资产证券化信息查询"),//资产证券化信息查询

    TRADE_CODE_LN3161("ln3161", "资产证券化协议登记"),//资产证券化协议登记

    TRADE_CODE_LN3162("ln3162", "资产证券化借据维护"),//资产证券化借据维护

    TRADE_CODE_LN3163("ln3163", "资产证券化处理"),//资产证券化处理

    TRADE_CODE_LN3164("ln3164", "资产证券化重封包"),//资产证券化重封包

    TRADE_CODE_LN3165("ln3165", "资产证券化资金划转"),//资产证券化资金划转

    TRADE_CODE_LN3166("ln3166", "资产证券化借据筛选"),//资产证券化借据筛选

    TRADE_CODE_LN3167("ln3167", "资产证券化赎回单笔"),//资产证券化赎回单笔

    TRADE_CODE_LN3168("ln3168", "资产证券化回购清仓"),//资产证券化回购清仓

    TRADE_CODE_LN3169("ln3169", "资产证券化处理文件导入"),//资产证券化处理文件导入

    TRADE_CODE_LN3170("ln3170", "实际利率计算"),//实际利率计算

    TRADE_CODE_LN3171("ln3171", "贷款减值转换"),//贷款减值转换

    TRADE_CODE_LN3172("ln3172", "贷款拨备计提"),//贷款拨备计提

    TRADE_CODE_LN3173("ln3173", "贷款折现回拨"),//贷款折现回拨

    TRADE_CODE_LN3174("ln3174", "资产证券化欠款借据查询"),//资产证券化欠款借据查询

    TRADE_CODE_LN3175("ln3175", "资产证券化借据附表查询"),//资产证券化借据附表查询

    TRADE_CODE_LN3176("ln3176", "贷款多账户还款"),//贷款多账户还款

    TRADE_CODE_LN3177("ln3177", "资产转让付款状态维护"),//资产转让付款状态维护

    TRADE_CODE_LN3180("ln3180", "贷款新准则账户信息查询"),//贷款新准则账户信息查询

    TRADE_CODE_LN3181("ln3181", "贷款分户账交易明细查询"),//贷款分户账交易明细查询

    TRADE_CODE_LN3182("ln3182", "贷款拨备计提明细查询"),//贷款拨备计提明细查询

    TRADE_CODE_LN3183("ln3183", "贷款减值测试历史明细查询"),//贷款减值测试历史明细查询

    TRADE_CODE_LN3184("ln3184", "贷款五级分类查询"),//贷款五级分类查询

    TRADE_CODE_LN3185("ln3185", "贷款费用代收登记簿维护"),//贷款费用代收登记簿维护

    TRADE_CODE_LN3190("ln3190", "参数名称查询"),//参数名称查询

    TRADE_CODE_LN3191("ln3191", "计算期数到期日"),//计算期数到期日

    TRADE_CODE_LN3192("ln3192", "计算期限日期"),//计算期限日期

    TRADE_CODE_LN3193("ln3193", "资产交易指令单笔查询"),//资产交易指令单笔查询

    TRADE_CODE_LN3194("ln3194", "数据定义查询"),//数据定义查询

    TRADE_CODE_LN3195("ln3195", "业务品种参数"),//业务品种参数

    TRADE_CODE_LN3196("ln3196", "子账户序号查询"),//子账户序号查询

    TRADE_CODE_LN3197("ln3197", "提前还款罚金试算"),//提前还款罚金试算

    TRADE_CODE_LN3198("ln3198", "报文信息查询"),//报文信息查询

    TRADE_CODE_LN3199("ln3199", "委托贷款信息查询"),//委托贷款信息查询

    TRADE_CODE_LN3210("ln3210", "贷款利息优惠计算"),//贷款利息优惠计算

    TRADE_CODE_LN3211("ln3211", "贷款利息清单查询"),//贷款利息清单查询

    TRADE_CODE_LN3212("ln3212", "借据欠款明细查询"),//借据欠款明细查询

    TRADE_CODE_LN3213("ln3213", "贷款自动转逾期查询"),//贷款自动转逾期查询

    TRADE_CODE_LN3214("ln3214", "表内逾期利息转表外查询"),//表内逾期利息转表外查询

    TRADE_CODE_LN3215("ln3215", "贷款还款清单查询"),//贷款还款清单查询

    TRADE_CODE_LN3216("ln3216", "贷款开户录入数据修改处理"),//贷款开户录入数据修改处理

    TRADE_CODE_LN3217("ln3217", "委托贷款客户还款明细查询"),//委托贷款客户还款明细查询

    TRADE_CODE_LN3218("ln3218", "贷款录入信息查询供国结使用"),//贷款录入信息查询供国结使用

    TRADE_CODE_LN3219("ln3219", "贷款欠款信息查询柜面"),//贷款欠款信息查询柜面

    TRADE_CODE_LN3220("ln3220", "贷款自动追缴登记及维护"),//贷款自动追缴登记及维护

    TRADE_CODE_LN3221("ln3221", "贷款自动追缴查询"),//贷款自动追缴查询

    TRADE_CODE_LN3230("ln3230", "产品变更录入数据查询交易"),//产品变更录入数据查询交易

    TRADE_CODE_LN3231("ln3231", "贷款文件处理查询"),//贷款文件处理查询

    TRADE_CODE_LN3232("ln3232", "财政资金借款还款处理"),//财政资金借款还款处理

    TRADE_CODE_LN3233("ln3233", "财政资金借款查询"),//财政资金借款查询

    TRADE_CODE_LN3234("ln3234", "借新还旧信息查询"),//借新还旧信息查询

    TRADE_CODE_LN3235("ln3235", "还款方式转换"),//还款方式转换

    TRADE_CODE_LN3236("ln3236", "贷款账隔日冲正"),//贷款账隔日冲正

    TRADE_CODE_LN3237("ln3237", "查询客户借据余额"),//查询客户借据余额

    TRADE_CODE_LN3238("ln3238", "贷款到期列表查询"),//贷款到期列表查询

    TRADE_CODE_LN3239("ln3239", "借据信息列表查询"),//借据信息列表查询

    TRADE_CODE_LN3240("ln3240", "存贷通签约"),//存贷通签约

    TRADE_CODE_LN3241("ln3241", "存贷通解约"),//存贷通解约

    TRADE_CODE_LN3242("ln3242", "存贷通信息查询"),//存贷通信息查询

    TRADE_CODE_LN3243("ln3243", "停息贷款利息试算"),//停息贷款利息试算

    TRADE_CODE_LN3244("ln3244", "贷款客户经理批量移交"),//贷款客户经理批量移交

    TRADE_CODE_LN3245("ln3245", "核销登记簿查询"),//核销登记簿查询

    TRADE_CODE_LN3246("ln3246", "贷款还款计划明细查询"),//贷款还款计划明细查询

    TRADE_CODE_LN3248("ln3248", "委托清收变更"),

    TRADE_CODE_LN3247("ln3247", "贷款还款周期预定变更"),

    TRADE_CODE_LN3249("ln3249", "贷款指定日期利息试算"),

    TRADE_CODE_LN3251("ln3251", "贷款自动追缴明细查询"),

    TRADE_CODE_LN3310("ln3310", "根据证件查询受托支付"),//根据证件查询受托支付

    TRADE_CODE_LC3820("lc3820", "国内信用证开户"),//国内信用证开户

    TRADE_CODE_LC3821("lc3821", "信用证信息修改"),//信用证信息修改

    TRADE_CODE_LC3822("lc3822", "信用证担保信息维护"),//信用证担保信息维护

    TRADE_CODE_LC3823("lc3823", "信用证打印"),//信用证打印

    TRADE_CODE_LC3824("lc3824", "信用证到单通知"),//信用证到单通知

    TRADE_CODE_LC3825("lc3825", "信用证通知"),//信用证通知

    TRADE_CODE_LC3826("lc3826", "通知行查询打印"),//通知行查询打印

    TRADE_CODE_LC3827("lc3827", "信用证交单通知"),//信用证交单通知

    TRADE_CODE_LC3828("lc3828", "信用证承兑"),//信用证承兑

    TRADE_CODE_LC3829("lc3829", "信用证议付"),//信用证议付

    TRADE_CODE_LC3830("lc3830", "信用证议付归还"),//信用证议付归还

    TRADE_CODE_LC3831("lc3831", "国内信用证付款"),//国内信用证付款

    TRADE_CODE_LC3832("lc3832", "信用证注销"),//信用证注销

    TRADE_CODE_LC3833("lc3833", "信用证回款"),//信用证回款

    TRADE_CODE_LC3850("lc3850", "信用证合同查询"),//信用证合同查询

    TRADE_CODE_LC3851("lc3851", "信用证合同组合查询"),//信用证合同组合查询

    TRADE_CODE_LC3852("lc3852", "信用证信息查询"),//信用证信息查询

    TRADE_CODE_LC3853("lc3853", "信用证组合承兑查询"),//信用证组合承兑查询

    TRADE_CODE_LC3854("lc3854", "信用承兑交易单笔查询"),//信用承兑交易单笔查询

    TRADE_CODE_LC3855("lc3855", "信用证议付信息查询"),//信用证议付信息查询

    TRADE_CODE_LC3856("lc3856", "信用证货物合同组合查询"),//信用证货物合同组合查询

    TRADE_CODE_OB3600("ob3600", "资产表外业务开户"),//资产表外业务开户

    TRADE_CODE_OB3601("ob3601", "资产表外业务信息修改"),//资产表外业务信息修改

    TRADE_CODE_OB3602("ob3602", "资产表外业务核销"),//资产表外业务核销

    TRADE_CODE_OB3603("ob3603", "保函赔付交易"),//保函赔付交易

    TRADE_CODE_OB3619("ob3619", "资产表外业务录入数据查询"),//资产表外业务录入数据查询

    TRADE_CODE_OB3620("ob3620", "资产表外业务单笔查询"),//资产表外业务单笔查询

    TRADE_CODE_OB3621("ob3621", "表外业务组合查询"),//表外业务组合查询

    TRADE_CODE_OB3622("ob3622", "资产表外业务账务明细查询"),//资产表外业务账务明细查询

    TRADE_CODE_BB3799("bb3799", "资产移植测试"),//资产移植测试

    TRADE_CODE_DP2280("dp2280", "子账户序号查询"),//子账户序号查询

    TRADE_CODE_S00101("s00101", "对私客户综合信息查询"),//对私客户综合信息查询

    /**
     * 慧押押 开始
     **/
    TRADE_CODE_BARE01("bare01", "押品状态查询"),//押品状态查询

    /**
     * 影像系统 开始
     **/
    TRADE_CODE_IMAGETOKEN("imagetoken", "通过用户名和密码获取token"),//通过用户名和密码获取token
    TRADE_CODE_IMAGEIMAGEDATASIZE("imageimagedatasize", "图像数量查询接口"),//通过用户名和密码获取token
    TRADE_CODE_YXLJCX("yxljcx", "影像图像路径查询"),

    /**
     * 影像系统 开始
     **/
    TRADE_CODE_COMMON_IMAGETOKEN("common/imagetoken", "默认统一用户及密码获取token"),//默认统一用户及密码获取token

    /**
     * 影像系统 开始
     **/
    TRADE_CODE_COMMON_IMAGEURL("imageurl", "默认统一用户及密码获取影像地址"),//默认统一用户及密码获取影像地址

    /**
     * 外部数据平台 开始
     **/
    TRADE_CODE_IDCHECK("idCheck", "个人身份核查"),//个人身份核查

    TRADE_CODE_ZSNEW("zsnew", "企业工商信息查询"),//企业工商信息查询

    TRADE_CODE_QYSSXX("qyssxx", "涉诉信息查询接口"),//涉诉信息查询接口

    TRADE_CODE_SXBZXR("sxbzxr", "失信被执行人查询接口"),//失信被执行人查询接口

    TRADE_CODE_SLNO01("slno01", "双录流水号查询"),//双录流水号查询
    /** 外部数据平台 结束 **/

    /**
     * 国际结算 开始
     **/
    TRADE_CODE_XDGJ07("xdgj07", "信贷发送台账信息"),//信贷发送台账信息

    TRADE_CODE_XDGJ08("xdgj08", "信贷还款信息"),//信贷还款信息

    TRADE_CODE_XDGJ09("xdgj09", "信贷查询牌价信息"),//信贷查询牌价信息

    TRADE_CODE_XDGJ10("xdgj10", "信贷获取国结信用证信息"),//信贷获取国结信用证信息

    /**
     * 国际结算 结束
     **/

    /* 人力资源系统开始 */
    TRADE_CODE_XXDREL("xxdrel", "查询人员基本信息岗位信息家庭信息"),//查询人员基本信息岗位信息家庭信息

    TRADE_CODE_XXDENT("xxdent", "新入职人员信息登记"),//新入职人员信息登记
    /* 人力资源系统结束 */

    /*GAPS系统开始*/
    TRADE_CODE_IDCHEK("idchek", "身份证核查"),
    TRADE_CODE_MFZJCR("mfzjcr", "买方资金存入"),
    TRADE_CODE_CLJCTZ("cljctz", "连云港存量房列表"),
    /*GAPS系统结束*/

    /* 印控系统开始 */
    TRADE_CODE_YKY001("yky001", "用印申请"),
    /* 印控系统结束 */

    /* 理财系统开始 */
    TRADE_CODE_LC0323("lc0323", "查询理财是否冻结"),
    /* 理财系统结束 */

    /* 营销过程管理平台开始 */
    TRADE_CODE_YXGC01("yxgc01", "贷后任务推送"),
    /* 营销过程管理平台结束 */

    /* 中间业务系统开始*/
    TRADE_CODE_CLZJCX("clzjcx", "中间业务系统"),
    TRADE_CODE_SQFYYZ("sqfyyz", "行方验证房源信息"),//行方验证房源信息
    TRADE_CODE_JCXXCX("jcxxcx", "缴存信息查询"),//缴存信息查询
    TRADE_CODE_CLFXCX("clfxcx", "协议信息查询"),//协议信息查询
    /*中间业务系统结束*/

    /*BATCH服务 开始*/
    TRADE_CODE_SINGLERUN4USE("singlerun4use", "运行单个任务"),
    /*BATCH服务结束*/
    TRADE_CODE_ZCOVERALLBUSINESSINCOME("zcOverallBusinessIncome", "资产池业务总体收益情况详情"),
    TRADE_CODE_XDHXQUERYTOTALLIST("xdhxQueryTotalList", "信贷客户核心业绩统计查询列表"),
    TRADE_CODE_INCOME("income", "资产收益明细查询"),
    TRADE_CODE_QUOTA("quota", "客户我行授用信分布"),

    /*数据中台服务 开始*/

    /*数据中台服务 结束*/
    // /** 交易码 结束 **/

    ;

    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EsbEnum enumData : EnumSet.allOf(EsbEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EsbEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EsbEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
