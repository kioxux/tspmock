package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统作为服务端的枚举</br>
 *
 * @author mochi
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum DscmsEnum {
    /**
     * cmisbatch0004 相关枚举
     */
    CMISBATCH0004_QUERYTYPE_10("10", "上次还款日(schkriqi)-到期日期(daoqriqi)>10"),
    CMISBATCH0004_QUERYTYPE_05("05", "上次还款日(schkriqi)-到期日期(daoqriqi)>5"),
    CMISBATCH0004_QUERYTYPE_0("0", "无查询条件"),
    /**
     * 操作成功标志位 字典项  S成功 F失败
     */
    OP_FLAG_S("S", "成功"),//成功
    OP_FLAG_F("F", "失败"),//失败

    TRADE_CODE_CMISLMT0001("cmisLmt0001", "单一客户额度同步"),
    TRADE_CODE_CMISLMT0019("cmisLmt0019", "校验客户是否存在有效综合授信额度"),
    TRADE_CODE_XDED0014("xded0014", "台账恢复接口"),
    TRADE_CODE_XDED0023("xded0023", "个人额度查询（新微贷）"),
    TRADE_CODE_XDED0029("xded0029", "更新台账编号"),
    TRADE_CODE_XDED0033("xded0033", "国结票据出账额度校验"),
    TRADE_CODE_XDED0034("xded0034", "国结票据出账额度占用"),
    TRADE_CODE_XDED0058("xded0058", "转贴现业务额度恢复"),
    /***
     * 服务间交易码 开始
     */
    TRADE_CODE_CMISBIZ0001("cmisbiz0001", "单一客户授信复审"),
    TRADE_CODE_CMISBIZ0002("cmisbiz0002", "集团客户授信复审"),
    TRADE_CODE_CMISBIZ0003("cmisbiz0003", "根据机构号和业务条线获取额度"),
    TRADE_CODE_CMISCUS0001("cmiscus0001", "集团成员列表查询"),//集团成员列表查询
    TRADE_CODE_CMISCUS0003("cmiscus0003", "查询法人关联客户列表"),//查询法人关联客户列表
    TRADE_CODE_CMISCUS0004("cmiscus0004", "个人关联客户列表查询"),//个人关联客户列表查询
    TRADE_CODE_CMISCUS0005("cmiscus0005", "客户销售总额查询"),//客户销售总额查询
    TRADE_CODE_CMISCUS0006("cmiscus0006", "查询客户基本信息"),//查询客户基本信息
    TRADE_CODE_CMISCUS0007("cmiscus0007", "查询个人客户基本信息"),//查询个人客户基本信息
    TRADE_CODE_CMISCUS0008("cmiscus0008", "查询根据客户编号查询法人名称,名称，证件类型，证件号，电话"),//查询根据客户编号查询法人名称,名称，证件类型，证件号，电话
    TRADE_CODE_CMISCUS0009("cmiscus0008", "查询对公客户基本信息"),//查询对公客户基本信息
    TRADE_CODE_CMISCUS0010("cmiscus0010", "个人社会关系信息"),//查询对公客户基本信息
    TRADE_CODE_CMISCUS0011("cmiscus0011", "优农贷名单信息查询"),//优农贷名单信息查询
    TRADE_CODE_CMISCUS0012("cmiscus0012", "对公高管信息查询"),//对公高管信息查询
    TRADE_CODE_CMISCUS0013("cmiscus0013", "根据证件号查询社会关系信息"),//根据证件号查询社会关系信息
    TRADE_CODE_CMISCUS0014("cmiscus0014", "法人客户及股东信息查询"),//法人客户及股东信息查询
    TRADE_CODE_CMISCUS0015("cmiscus0015", "个人客户单位信息维护"),//个人客户单位信息维护
    TRADE_CODE_CMISCUS0016("cmiscus0016", "个人无还本续贷表信息查询"),//个人无还本续贷表信息查询
    TRADE_CODE_CMISCUSLSTZXD("cmiscuslstzxd", "增享贷白名单"),//增享贷白名单
    TRADE_CODE_CMISCUS0017("cmiscus0017", "查询客户去年12期财报中最大的主营业务收入"),//查询客户去年12期财报中最大的主营业务收入
    TRADE_CODE_CMISCUS0018("cmiscus0018", "更新增享贷白名单"),//更新增享贷白名单
    TRADE_CODE_CMISCUS0019("cmiscus0019", "查询优农贷名单信息"),//查询优农贷名单信息
    TRADE_CODE_CMISCUS0020("cmiscus0020", "根据客户号查询关联方名单信息记录数"),//根据客户号查询关联方名单信息记录数
    TRADE_CODE_CMISCUS0021("cmiscus0021", "优农贷（经营信息）记录生成"),//优农贷（经营信息）记录生成
    TRADE_CODE_CMISCUS0022("cmiscus0022", "优农贷（经营信息）查询"),//查询
    TRADE_CODE_CMISCUS0023("cmiscus0023", "根据同业客户行号查找客户号"),//查询
    TRADE_CODE_CMISCUS0024("cmiscus0024", "根据行号或BICCODE获取同业客户号"),//查询
    TRADE_CODE_CMISCUS0025("cmiscus0025", "优农贷名单详细信息查询"),//查询
    TRADE_CODE_CMISCUS0026("cmiscus0026", "客户联系信息维护"),//客户联系信息维护

    TRADE_CODE_CMISBATCH0001("cmisbatch0001", "查詢[调度运行管理]信息"),// 查詢[调度运行管理]信息
    TRADE_CODE_CMISBATCH0002("cmisbatch0002", "查询[核心系统-历史表-贷款账户主表]信息"),// 查询[核心系统-历史表-贷款账户主表]信息
    TRADE_CODE_CMISBATCH0003("cmisbatch0003", "查询[贷款还款计划表]和[贷款期供交易明细]关联信息"),// 查询[贷款还款计划表]和[贷款期供交易明细]关联信息
    TRADE_CODE_CMISBATCH0004("cmisbatch0004", "查询[贷款账户主表]和[贷款账户还款表]关联信息"),//  查询[贷款账户主表]和[贷款账户还款表]关联信息
    TRADE_CODE_CMISBATCH0005("cmisbatch0005", "更新客户移交相关表"),//  更新客户移交相关表
    TRADE_CODE_CMISBATCH0006("cmisbatch0006", "不定期检查任务信息生成"),//  不定期检查任务信息生成
    TRADE_CODE_CMISBATCH0007("cmisbatch0007", "不定期分类任务信息生成"),//  不定期分类任务信息生成
    TRADE_CODE_CMISBATCH0008("cmisbatch0008", "任务加急初始化数据"),//  任务加急初始化数据
    TRADE_CODE_CMISBATCH0009("cmisbatch0009", "查询[贷款账户期供表]信息"),//  查询[贷款账户期供表]信息
    TRADE_CODE_CMISBATCH0010("cmisbatch0010", "提供日终调度平台调起批量任务"),//  提供日终调度平台调起批量任务
    TRADE_CODE_CMISBATCH0011("cmisbatch0011", "提供日终调度平台查询批量任务状态"),//  提供日终调度平台查询批量任务状态
    /** 服务端交易码 结束 **/
    /**
     * 服务端交易码 开始
     **/
    TRADE_CODE_XDPL0001("xdpl0001", "提供日终调度平台调起批量任务"),//  提供日终调度平台调起批量任务
    TRADE_CODE_XDPL0002("xdpl0002", "提供日终调度平台查询批量任务状态"),//  提供日终调度平台查询批量任务状态

    TRADE_CODE_BABIGC("babigc", "获取抵押信息详情"),//获取抵押信息详情
    TRADE_CODE_BABIUR("babiur", "押品状态变更推送"),//押品状态变更推送
    TRADE_CODE_BABIAV("babiav", "获取抵押登记双录音视频信息列表"),//获取抵押登记双录音视频信息列表

    TRADE_CODE_XDKH0001("xdkh0001", "个人客户基本信息查询"),//个人客户基本信息查询
    TRADE_CODE_XDKH0002("xdkh0002", "对公客户基本信息查询"),//对公客户基本信息查询
    TRADE_CODE_XDKH0004("xdkh0004", "查询客户配偶信息"),//查询客户配偶信息
    TRADE_CODE_XDKH0005("xdkh0005", "同业客户信息查询"),//同业客户信息查询
    TRADE_CODE_XDKH0007("xdkh0007", "查询客户是否本行员工及直属亲属"),//查询客户是否本行员工及直属亲属
    TRADE_CODE_XDKH0008("xdkh0008", "集团关联信息查询"),//集团关联信息查询
    TRADE_CODE_XDKH0011("xdkh0011", "对私客户信息同步"),//对私客户信息同步
    TRADE_CODE_XDKH0012("xdkh0012", "对公客户信息同步"),//对公客户信息同步
    TRADE_CODE_XDKH0013("xdkh0013", "优享贷客户白名单信息查询"),//优享贷客户白名单信息查询
    TRADE_CODE_XDKH0014("xdkh0014", "省心E付白名单信息维护"),//省心E付白名单信息维护
    TRADE_CODE_XDKH0015("xdkh0015", "农拍档接受白名单维护"),//农拍档接受白名单维护
    TRADE_CODE_XDKH0016("xdkh0016", "查询是否我行关系人"),//查询是否我行关系人
    TRADE_CODE_XDKH0018("xdkh0018", "临时客户信息维护"),//临时客户信息维护
    TRADE_CODE_XDKH0019("xdkh0019", "客户查询并开户"),//客户查询并开户
    TRADE_CODE_XDKH0020("xdkh0020", "小微平台请求信贷线上开户"),//小微平台请求信贷线上开户
    TRADE_CODE_XDKH0021("xdkh0021", "客户信息生成"),//客户信息生成
    TRADE_CODE_XDKH0024("xdkh0024", "优企贷、优农贷客户基本信息查询"),//优企贷、优农贷客户基本信息查询
    TRADE_CODE_XDKH0025("xdkh0025", "优企贷、优农贷公司客户信息查询"),//优企贷、优农贷公司客户信息查询
    TRADE_CODE_XDKH0026("xdkh0026", "优企贷、优农贷行内关联人基本信息查询"),//优企贷、优农贷行内关联人基本信息查询
    TRADE_CODE_XDKH0027("xdkh0027", "优企贷、优农贷行内关联自然人基本信息查询"),//优企贷、优农贷行内关联自然人基本信息查询
    TRADE_CODE_XDKH0028("xdkh0028", "优农贷黑名单查询"),//优农贷黑名单查询
    TRADE_CODE_XDKH0029("xdkh0029", "客户信息查询（评级）"),//客户信息查询（评级）
    TRADE_CODE_XDKH0030("xdkh0030", "公司客户评级相关信息同步"),//公司客户评级相关信息同步
    TRADE_CODE_XDKH0031("xdkh0031", "同业客户评级相关信息同步"),//同业客户评级相关信息同步
    TRADE_CODE_XDKH0032("xdkh0032", "信息锁定标志同步"),//信息锁定标志同步
    TRADE_CODE_XDKH0033("xdkh0033", "客户评级结果同步"),//客户评级结果同步
    TRADE_CODE_XDKH0023("xdkh0023", "还款试算计划查询日期"),
    TRADE_CODE_XDKH0034("xdkh0034", "查询企业在我行客户评级信息"),
    TRADE_CODE_XDKH0035("xdkh0035", "违约认定、重生认定评级信息同步"),
    TRADE_CODE_XDKH0036("xdkh0036", "网银对公客户查询接口"),
    TRADE_CODE_XDKH0037("xdkh0037", "微业贷新开户信息入库"),

    TRADE_CODE_XDLS0001("xdls0001", "房贷要素查询"),//房贷要素查询
    TRADE_CODE_XDLS0002("xdls0002", "市民贷联系人信息查询"),//市民贷联系人信息查询
    TRADE_CODE_XDLS0003("xdls0003", "信贷授信协议合同金额查询"),//信贷授信协议合同金额查询
    TRADE_CODE_XDLS0004("xdls0004", "查询信贷有无授信历史"),//查询信贷有无授信历史
    TRADE_CODE_XDLS0005("xdls0005", "智能风控调用信贷通知"),//智能风控调用信贷通知
    TRADE_CODE_XDLS0006("xdls0006", "白领贷额度查询"),

    TRADE_CODE_XDXW0001("xdxw0001", "房屋估价信息同步"),//房屋估价信息同步
    TRADE_CODE_XDXW0002("xdxw0002", "小微贷前调查信息查询"),//小微贷前调查信息查询
    TRADE_CODE_XDXW0003("xdxw0003", "小微贷前调查信息维护"),//小微贷前调查信息维护
    TRADE_CODE_XDXW0004("xdxw0004", "小微营业额校验查询"),//小微营业额校验查询
    TRADE_CODE_XDXW0005("xdxw0005", "小微营业额信息维护"),//小微营业额信息维护
    TRADE_CODE_XDXW0006("xdxw0006", "联系人信息维护"),//联系人信息维护
    TRADE_CODE_XDXW0007("xdxw0007", "批复信息查询"),//批复信息查询
    TRADE_CODE_XDXW0008("xdxw0008", "优农贷黑白名单校验"),//优农贷黑白名单校验
    TRADE_CODE_XDXW0009("xdxw0009", "担保人人数查询"),//担保人人数查询
    TRADE_CODE_XDXW0010("xdxw0010", "勘验列表信息查询"),//勘验列表信息查询
    TRADE_CODE_XDXW0011("xdxw0011", "提交勘验信息"),//提交勘验信息
    TRADE_CODE_XDXW0012("xdxw0012", "小贷授信申请文本生成pdf"),
    TRADE_CODE_XDXW0013("xdxw0013", "优企贷共借人、合同信息查询"),//优企贷共借人、合同信息查询
    TRADE_CODE_XDXW0014("xdxw0014", "优企贷共借人签订"),//优企贷共借人签订
    TRADE_CODE_XDXW0015("xdxw0015", "查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”"),//查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
    TRADE_CODE_XDXW0016("xdxw0016", "统一管控额度查询（总额度）"),//统一管控额度查询（总额度）
    TRADE_CODE_XDXW0017("xdxw0017", "远程抵押物勘验待办列表查询"),//远程抵押物勘验待办列表查询
    TRADE_CODE_XDXW0018("xdxw0018", "抵押物信息列表查询（云评估）"),//抵押物信息列表查询（云评估）
    TRADE_CODE_XDXW0019("xdxw0019", "推送决策审批结果（产生信贷批复）"),//推送决策审批结果（产生信贷批复）
    TRADE_CODE_XDXW0020("xdxw0020", "客户调查撤销查询"),//客户调查撤销查询
    TRADE_CODE_XDXW0021("xdxw0021", "根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）"),//根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
    TRADE_CODE_XDXW0022("xdxw0022", "根据证件号查询信贷系统的申请信息"),//根据证件号查询信贷系统的申请信息
    TRADE_CODE_XDXW0023("xdxw0023", "在小贷是否有调查申请记录"),//在小贷是否有调查申请记录
    TRADE_CODE_XDXW0024("xdxw0024", "查询是否有信贷记录"),//查询是否有信贷记录
    TRADE_CODE_XDXW0025("xdxw0025", "借据下抵押物信息列表查询"),//借据下抵押物信息列表查询
    TRADE_CODE_XDXW0026("xdxw0026", "学区信息列表查询（分页）"),//学区信息列表查询（分页）
    TRADE_CODE_XDXW0027("xdxw0027", "客户调查信息详情查看"),//客户调查信息详情查看
    TRADE_CODE_XDXW0028("xdxw0028", "根据客户号查询我行现有融资情况"),//根据客户号查询我行现有融资情况
    TRADE_CODE_XDXW0029("xdxw0029", "根据客户号查询融资总额、总余额、担保方式（包含结清）"),//根据客户号查询融资总额、总余额、担保方式（包含结清）
    TRADE_CODE_XDXW0030("xdxw0030", "根据云估计流水号查询房屋信息"),//根据云估计流水号查询房屋信息
    TRADE_CODE_XDXW0031("xdxw0031", "查询现场勘验流水信息"),//查询现场勘验流水信息
    TRADE_CODE_XDXW0032("xdxw0032", "查询信贷系统的审批历史信息"),//查询信贷系统的审批历史信息
    TRADE_CODE_XDXW0033("xdxw0033", "根据流水号查询征信报告关联信息"),//根据流水号查询征信报告关联信息
    TRADE_CODE_XDXW0034("xdxw0034", "根据流水号查询无还本续贷基本信息"),//根据流水号查询无还本续贷基本信息
    TRADE_CODE_XDXW0035("xdxw0035", "根据流水号查询无还本续贷抵押信息"),//根据流水号查询无还本续贷抵押信息
    TRADE_CODE_XDXW0036("xdxw0036", "查询优抵贷调查结论"),//查询优抵贷调查结论
    TRADE_CODE_XDXW0037("xdxw0037", "查询借款人是否存在未完成的贷后检查任务"),//查询借款人是否存在未完成的贷后检查任务
    TRADE_CODE_XDXW0038("xdxw0038", "查询调查表和其他关联信息"),//查询调查表和其他关联信息
    TRADE_CODE_XDXW0039("xdxw0039", "提交决议"),//提交决议
    TRADE_CODE_XDXW0040("xdxw0040", "风控推送面签信息至信贷系统"),//风控推送面签信息至信贷系统
    TRADE_CODE_XDXW0041("xdxw0041", "小企业无还本续贷审批结果维护"),//小企业无还本续贷审批结果维护
    TRADE_CODE_XDXW0042("xdxw0042", "惠享贷模型结果维护"),//惠享贷模型结果维护
    TRADE_CODE_XDXW0043("xdxw0043", "无还本续贷待办接收"),//无还本续贷待办接收
    TRADE_CODE_XDXW0044("xdxw0044", "优抵贷待办事项维护"),//优抵贷待办事项维护
    TRADE_CODE_XDXW0045("xdxw0045", "优抵贷客户调查撤销"),//优抵贷客户调查撤销
    TRADE_CODE_XDXW0046("xdxw0046", "优享贷批复结果反馈"),//优享贷批复结果反馈
    TRADE_CODE_XDXW0047("xdxw0047", "增享贷2.0风控模型A任务推送"),//增享贷2.0风控模型A任务推送
    TRADE_CODE_XDXW0048("xdxw0048", "增享贷2.0风控模型B生成批复"),//增享贷2.0风控模型B生成批复
    TRADE_CODE_XDXW0049("xdxw0049", "智能风控删除通知"),//智能风控删除通知
    TRADE_CODE_XDXW0050("xdxw0050", "资产负债表信息查询"),//资产负债表信息查询
    TRADE_CODE_XDXW0051("xdxw0051", "根据业务唯一编号查询无还本续贷贷销售收入"),//根据业务唯一编号查询无还本续贷贷销售收入
    TRADE_CODE_XDXW0052("xdxw0052", "查询优抵贷客户调查表"),//查询优抵贷客户调查表
    TRADE_CODE_XDXW0053("xdxw0053", "查询调查表客户基本信息"),//查询调查表客户基本信息
    TRADE_CODE_XDXW0054("xdxw0054", "查询优抵贷损益表明细"),//查询优抵贷损益表明细
    TRADE_CODE_XDXW0055("xdxw0055", "查询优抵贷经营地址"),//查询优抵贷经营地址
    TRADE_CODE_XDXW0056("xdxw0056", "根据流水号查询管户信息"),//根据流水号查询管户信息
    TRADE_CODE_XDXW0057("xdxw0057", "根据核心客户号查询经营性贷款批复额度"),//根据核心客户号查询经营性贷款批复额度
    TRADE_CODE_XDXW0058("xdxw0058", "根据流水号查询借据号"),//根据流水号查询借据号
    TRADE_CODE_XDXW0059("xdxw0059", "根据调查流水号查询抵押率"),//根据调查流水号查询抵押率
    TRADE_CODE_XDXW0060("xdxw0060", "客户及配偶信用类小微业务贷款授信金额"),//客户及配偶信用类小微业务贷款授信金额
    TRADE_CODE_XDXW0061("xdxw0061", "通过无还本续贷调查表编号查询配偶核心客户号"),//通过无还本续贷调查表编号查询配偶核心客户号
    TRADE_CODE_XDXW0062("xdxw0062", "小微平台提交推送待办消息，信贷生成批复，客户经理继续审核"),//小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
    TRADE_CODE_XDXW0063("xdxw0063", "调查基本信息查询"),//调查基本信息查询
    TRADE_CODE_XDXW0064("xdxw0064", "优企贷、优农贷授信调查信息查询"),//优企贷、优农贷授信调查信息查询
    TRADE_CODE_XDXW0065("xdxw0065", "调查报告审批结果信息查询"),//调查报告审批结果信息查询
    TRADE_CODE_XDXW0066("xdxw0066", "调查基本信息查询"),//调查基本信息查询
    TRADE_CODE_XDXW0067("xdxw0067", "授信调查经营情况信息查询"),//授信调查经营情况信息查询
    TRADE_CODE_XDXW0068("xdxw0068", "授信调查结论信息查询"),//授信调查结论信息查询
    TRADE_CODE_XDXW0070("xdxw0070", "授信侧面调查信息查询"),//授信侧面调查信息查询
    TRADE_CODE_XDXW0071("xdxw0071", "查询优抵贷抵质押品信息"),//查询优抵贷抵质押品信息
    TRADE_CODE_XDXW0072("xdxw0072", "根据调查表编号前往信贷查找客户经理信息"),//根据调查表编号前往信贷查找客户经理信息
    TRADE_CODE_XDXW0074("xdxw0074", "根据客户身份证号查询线上产品申请记录"),//根据客户身份证号查询线上产品申请记录
    TRADE_CODE_XDXW0076("xdxw0076", "渠道端查询个人客户我的授信"),//渠道端查询个人客户我的授信
    TRADE_CODE_XDXW0077("xdxw0077", "渠道端查询我的贷款"),//渠道端查询我的贷款
    TRADE_CODE_XDXW0078("xdxw0078", "推送云评估信息"),//推送云评估信息
    TRADE_CODE_XDXW0079("xdxw0079", "小微续贷白名单查询"),//小微续贷白名单查询
    TRADE_CODE_XDXW0080("xdxw0080", "优惠利率申请结果通知"),//优惠利率申请结果通知
    TRADE_CODE_XDXW0081("xdxw0081", "检查是否有优农贷、优企贷、惠享贷合同"),//检查是否有优农贷、优企贷、惠享贷合同
    TRADE_CODE_XDXW0082("xdxw0082", "查询上一笔抵押贷款的余额接口（增享贷）"),//查询上一笔抵押贷款的余额接口（增享贷）
    TRADE_CODE_XDXW0083("xdxw0083", "还款账号变更（优企贷）"),//还款账号变更（优企贷）
    TRADE_CODE_XDXW0084("xdxw0084", "共借人送达地址确认书文本生成PDF接口"),//共借人送达地址确认书文本生成PDF接口
    TRADE_CODE_XDXW0085("xdxw0085", "共借人声明文本生产PDF接口"),//共借人声明文本生产PDF接口
    TRADE_CODE_XDXW0086("xdxw0086", "涉税保密信息查询委托授权书文本生产PDF接口"),//涉税保密信息查询委托授权书文本生产PDF接口
    TRADE_CODE_XDSX0001("xdsx0001", "公司客户评级相关信息同步"),//公司客户评级相关信息同步
    TRADE_CODE_XDSX0002("xdsx0002", "同业客户评级相关信息同步"),//同业客户评级相关信息同步
    TRADE_CODE_XDSX0003("xdsx0003", "信息锁定标志同步"),//信息锁定标志同步
    TRADE_CODE_XDSX0004("xdsx0004", "客户评级结果同步"),//客户评级结果同步
    TRADE_CODE_XDSX0005("xdsx0005", "违约认定、重生认定评级信息同步"),//违约认定、重生认定评级信息同步
    TRADE_CODE_XDSX0006("xdsx0006", "专业贷款评级结果同步"),//专业贷款评级结果同步
    TRADE_CODE_XDSX0007("xdsx0007", "授信业务授权同步、资本占用率参数表同步"),//授信业务授权同步、资本占用率参数表同步
    TRADE_CODE_XDSX0008("xdsx0008", "单一客户人工限额同步"),//单一客户人工限额同步
    TRADE_CODE_XDSX0009("xdsx0009", "客户准入级别同步"),//客户准入级别同步
    TRADE_CODE_XDSX0010("xdsx0010", "行业组合限额同步"),//行业组合限额同步
    TRADE_CODE_XDSX0011("xdsx0011", "查询优惠（省心E付）"),//查询优惠（省心E付）
    TRADE_CODE_XDSX0012("xdsx0012", "网银推送省心快贷审核任务至信贷"),//网银推送省心快贷审核任务至信贷
    TRADE_CODE_XDSX0013("xdsx0013", "对公授信信息查询（含授信分项信息）"),//对公授信信息查询（含授信分项信息）
    TRADE_CODE_XDSX0014("xdsx0014", "授信作废"),//授信作废
    TRADE_CODE_XDSX0015("xdsx0015", "已批复的授信申请信息、押品信息以及合同信息同步"),//已批复的授信申请信息、押品信息以及合同信息同步
    TRADE_CODE_XDSX0016("xdsx0016", "抵押查封信息同步"),//抵押查封信息同步
    TRADE_CODE_XDSX0017("xdsx0017", "对公授信信息查询"),//对公授信信息查询
    TRADE_CODE_XDSX0018("xdsx0018", "房抵e点贷无还本续贷审核"),//房抵e点贷无还本续贷审核
    TRADE_CODE_XDSX0019("xdsx0019", "风控发送信贷审核受托信息"),//风控发送信贷审核受托信息
    TRADE_CODE_XDSX0020("xdsx0020", "担保公司合作协议查询"),//担保公司合作协议查询
    TRADE_CODE_XDSX0021("xdsx0021", "查询企业在我行客户评级信息"),//查询企业在我行客户评级信息
    TRADE_CODE_XDSX0022("xdsx0022", "推送苏州地方征信给信贷"),//推送苏州地方征信给信贷
    TRADE_CODE_XDSX0023("xdsx0023", "保函协议查询"),//保函协议查询
    TRADE_CODE_XDSX0024("xdsx0024", "省心快贷plus授信，风控自动审批结果推送信贷"),//省心快贷plus授信，风控自动审批结果推送信贷
    TRADE_CODE_XDSX0025("xdsx0025", "省心快贷plus授信，风控自动审批结果推送信贷"),//省心快贷plus授信，风控自动审批结果推送信贷
    TRADE_CODE_XDSX0026("xdsx0026", "风控推送面签信息至信贷系统"),//风控推送面签信息至信贷系统

    TRADE_CODE_XDHT0001("xdht0001", "查询省心E付合同信息"),//查询省心E付合同信息
    TRADE_CODE_XDHT0002("xdht0002", "银承协议查询"),//银承协议查询
    TRADE_CODE_XDHT0003("xdht0003", "信贷贴现合同号查询"),//信贷贴现合同号查询
    TRADE_CODE_XDHT0004("xdht0004", "合同信息列表查询（贸易融资）"),//合同信息列表查询（贸易融资）
    TRADE_CODE_XDHT0005("xdht0005", "合同面签信息列表查询"),//合同面签信息列表查询
    TRADE_CODE_XDHT0006("xdht0006", "面签双录流水同步"),//面签双录流水同步
    TRADE_CODE_XDHT0007("xdht0007", "借款合同/担保合同列表信息查询"),//借款合同/担保合同列表信息查询
    TRADE_CODE_XDHT0008("xdht0008", "借款合同双录流水同步"),//借款合同双录流水同步
    TRADE_CODE_XDHT0009("xdht0009", "银承合同信息列表查询"),//银承合同信息列表查询
    TRADE_CODE_XDHT0010("xdht0010", "查询符合条件的省心快贷合同"),//查询符合条件的省心快贷合同
    TRADE_CODE_XDHT0011("xdht0011", "借款担保合同签订/放款查询"),//借款担保合同签订/放款查询
    TRADE_CODE_XDHT0012("xdht0012", "借款担保合同签订/支用"),//借款担保合同签订/支用
    TRADE_CODE_XDHT0013("xdht0013", "合同信息查询(微信小程序)"),//合同信息查询(微信小程序)
    TRADE_CODE_XDHT0014("xdht0014", "合同签订查询VX"),//合同签订查询VX
    TRADE_CODE_XDHT0015("xdht0015", "合同房产人员信息查询"),//合同房产人员信息查询
    TRADE_CODE_XDHT0016("xdht0016", "合同签订维护（优抵贷不见面抵押签约）"),//合同签订维护（优抵贷不见面抵押签约）
    TRADE_CODE_XDHT0017("xdht0017", "借款、担保合同PDF生成"),//借款、担保合同PDF生成
    TRADE_CODE_XDHT0018("xdht0018", "担保合同文本生成pdf"),//担保合同文本生成pdf
    TRADE_CODE_XDHT0019("xdht0019", "合同生成"),//合同生成
    TRADE_CODE_XDHT0020("xdht0020", "合同信息查看"),//合同信息查看
    TRADE_CODE_XDHT0021("xdht0021", "合同签订"),//合同签订
    TRADE_CODE_XDHT0022("xdht0022", "合同信息列表查询"),//合同信息列表查询
    TRADE_CODE_XDHT0023("xdht0023", "合同生成（优享贷、增享贷）"),//合同生成（优享贷、增享贷）
    TRADE_CODE_XDHT0035("xdht0035", "根据业务品种查询合同主表信息一览"),//根据业务品种查询合同主表信息一览
    TRADE_CODE_XDHT0024("xdht0024", "合同详情查看（优享贷、增享贷）"),//合同详情查看（优享贷、增享贷）
    TRADE_CODE_XDHT0025("xdht0025", "合同签订/撤销（优享贷和增享贷）"),//合同签订/撤销（优享贷和增享贷）
    TRADE_CODE_XDHT0026("xdht0026", "根据客户号获取信贷合同信息"),//根据客户号获取信贷合同信息
    TRADE_CODE_XDHT0027("xdht0027", "合同状态查询"),//合同状态查询
    TRADE_CODE_XDHT0028("xdht0028", "根据合同号取得客户经理电话"),//根据合同号取得客户经理电话
    TRADE_CODE_XDHT0029("xdht0029", "客户非小微业务经营性贷款总金额查询"),//客户非小微业务经营性贷款总金额查询
    TRADE_CODE_XDHT0030("xdht0030", "查询客户我行合作次数"),//查询客户我行合作次数
    TRADE_CODE_XDHT0031("xdht0031", "根据核心客户号查询经营性贷款合同额度"),//根据核心客户号查询经营性贷款合同额度
    TRADE_CODE_XDHT0032("xdht0032", "根据核心客户号查询我行信用类合同金额汇总"),//根据核心客户号查询我行信用类合同金额汇总
    TRADE_CODE_XDHT0033("xdht0033", "根据核心客户号查询小贷是否具有客户调查合同并返回合同金额"),//根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
    TRADE_CODE_XDHT0034("xdht0034", "根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额"),//根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
    TRADE_CODE_XDHT0036("xdht0036", "有效合同信息列表查询"),//有效合同信息列表查询
    TRADE_CODE_XDHT0037("xdht0037", "大家e贷、乐悠金根据客户号查询房贷首付款比例"),//大家e贷、乐悠金根据客户号查询房贷首付款比例
    TRADE_CODE_XDHT0038("xdht0038", "乐悠金根据核心客户号查询房贷首付款比例进行额度计算"),//乐悠金根据核心客户号查询房贷首付款比例进行额度计算
    TRADE_CODE_XDHT0040("xdht0040", "修改受托信息"),//修改受托信息
    TRADE_CODE_XDHT0041("xdht0041", "查询受托记录状态"),//查询受托记录状态
    TRADE_CODE_XDHT0042("xdht0042", "优企贷、优农贷合同信息查询"),//优企贷、优农贷合同信息查询
    TRADE_CODE_XDHT0043("xdht0043", "小贷借款合同文本生成pdf"),//小贷借款合同文本生成pdf
    TRADE_CODE_XDHT0044("xdht0044", "否是房群客户查询"),//否是房群客户查询
    TRADE_CODE_XDHT0045("xdht0045", "查询信贷客户名称合同状态"),//否是房群客户查询

    TRADE_CODE_XDCZ0001("xdcz0001", "电子保函开立"),//电子保函开立
    TRADE_CODE_XDCZ0002("xdcz0002", "电子保函注销"),//电子保函注销
    TRADE_CODE_XDCZ0003("xdcz0003", "电子银行承兑汇票出账申请"),//电子银行承兑汇票出账申请
    TRADE_CODE_XDCZ0004("xdcz0004", "承兑签发审批结果综合服务"),//承兑签发审批结果综合服务
    TRADE_CODE_XDCZ0005("xdcz0005", "资产池入池接口"),//资产池入池接口
    TRADE_CODE_XDCZ0006("xdcz0006", "出账记录详情查看"),//出账记录详情查看
    TRADE_CODE_XDCZ0007("xdcz0007", "查询敞口额度及保证金校验"),//查询敞口额度及保证金校验
    TRADE_CODE_XDCZ0008("xdcz0008", "代开保函"),//代开保函
    TRADE_CODE_XDCZ0009("xdcz0009", "代开信用证"),//代开信用证
    TRADE_CODE_XDCZ0010("xdcz0010", "企业网银省心E付放款"),//企业网银省心E付放款
    TRADE_CODE_XDCZ0011("xdcz0011", "省心E付放款记录列表查询"),//省心E付放款记录列表查询
    TRADE_CODE_XDCZ0012("xdcz0012", "网银推送相关受托支付信息"),//网银推送相关受托支付信息
    TRADE_CODE_XDCZ0013("xdcz0013", "企业网银出票申请额度校验"),//企业网银出票申请额度校验
    TRADE_CODE_XDCZ0014("xdcz0014", "企业网银银票查询影像补录批次"),//企业网银银票查询影像补录批次
    TRADE_CODE_XDCZ0015("xdcz0015", "银票影像补录同步"),//银票影像补录同步
    TRADE_CODE_XDCZ0016("xdcz0016", "支用列表查询(微信小程序)"),//支用列表查询(微信小程序)
    TRADE_CODE_XDCZ0017("xdcz0017", "支用(微信小程序)"),//支用(微信小程序)
    TRADE_CODE_XDCZ0018("xdcz0018", "家速贷、极速快贷贷款申请"),//家速贷、极速快贷贷款申请
    TRADE_CODE_XDCZ0019("xdcz0019", "小贷额度支用申请书生成pdf"),//小贷额度支用申请书生成pdf
    TRADE_CODE_XDCZ0020("xdcz0020", "小贷用途承诺书文本生成pdf"),//小贷用途承诺书文本生成pdf
    TRADE_CODE_XDCZ0021("xdcz0021", "支用申请"),//支用申请
    TRADE_CODE_XDCZ0022("xdcz0022", "风控发送相关信息至信贷进行支用校验"),//风控发送相关信息至信贷进行支用校验
    TRADE_CODE_XDCZ0023("xdcz0023", "小企业无还本续贷放款"),//小企业无还本续贷放款
    TRADE_CODE_XDCZ0024("xdcz0024", "推送在线保函预约信息"),//推送在线保函预约信息
    TRADE_CODE_XDCZ0025("xdcz0025", "未中标业务注销"),//未中标业务注销
    TRADE_CODE_XDCZ0026("xdcz0026", "风控调用信贷放款"),//风控调用信贷放款
    TRADE_CODE_XDCZ0027("xdcz0027", "审批失败支用申请( 登记支用失败)"),//审批失败支用申请( 登记支用失败)
    TRADE_CODE_XDCZ0028("xdcz0028", "生成商贷账号数据文件"),//生成商贷账号数据文件
    TRADE_CODE_XDCZ0029("xdcz0029", "网银推送省心快贷审核任务至信贷"),//网银推送省心快贷审核任务至信贷
    TRADE_CODE_XDCZ0030("xdcz0030", "校验额度是否足额，合同是否足额"),//校验额度是否足额，合同是否足额
    TRADE_CODE_XDCZ0031("xdcz0031", "根据借据号查询次数"),//根据借据号查询次数

    TRADE_CODE_XDTZ0001("xdtz0001", "客户信息查询(贷款类)"),//客户信息查询(贷款类)
    TRADE_CODE_XDTZ0002("xdtz0002", "查询优企贷客户贷款合同信息一览"),//查询优企贷客户贷款合同信息一览
    TRADE_CODE_XDTZ0003("xdtz0003", "查询小微借据余额"),//查询小微借据余额
    TRADE_CODE_XDTZ0004("xdtz0004", "在查询经营性贷款借据信息"),//在查询经营性贷款借据信息
    TRADE_CODE_XDTZ0005("xdtz0005", "根据流水号查询是否放款标记"),//根据流水号查询是否放款标记
    TRADE_CODE_XDTZ0006("xdtz0006", "根据证件号查询借据信息"),//根据证件号查询借据信息
    TRADE_CODE_XDTZ0007("xdtz0007", "根据客户号获取非信用方式发放贷款的最长到期日"),//根据客户号获取非信用方式发放贷款的最长到期日
    TRADE_CODE_XDTZ0008("xdtz0008", "根据客户号获取正常周转次数"),//根据客户号获取正常周转次数
    TRADE_CODE_XDTZ0009("xdtz0009", "查询客户经理不良率"),//查询客户经理不良率
    TRADE_CODE_XDTZ0010("xdtz0010", "根据身份证号获取借据信息"),//根据身份证号获取借据信息
    TRADE_CODE_XDTZ0011("xdtz0011", "借据明细查询"),//借据明细查询
    TRADE_CODE_XDTZ0012("xdtz0012", "根据借款人证件号，判断配偶经营性贷款是否存在余额"),//根据借款人证件号，判断配偶经营性贷款是否存在余额
    TRADE_CODE_XDTZ0013("xdtz0013", "查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）"),//查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
    TRADE_CODE_XDTZ0014("xdtz0014", "保证金补交/冲补交结果通知服务"),//保证金补交/冲补交结果通知服务
    TRADE_CODE_XDTZ0015("xdtz0015", "贴现记账结果通知"),//贴现记账结果通知
    TRADE_CODE_XDTZ0016("xdtz0016", "通知信贷系统更新贴现台账状态"),//通知信贷系统更新贴现台账状态
    TRADE_CODE_XDTZ0017("xdtz0017", "票据更换（通知信贷更改票据暂用额度台账）"),//票据更换（通知信贷更改票据暂用额度台账）
    TRADE_CODE_XDTZ0018("xdtz0018", "额度占用释放接口"),//额度占用释放接口
    TRADE_CODE_XDTZ0019("xdtz0019", "台账入账"),//台账入账
    TRADE_CODE_XDTZ0020("xdtz0020", "台账入账（保函）"),//台账入账（保函）
    TRADE_CODE_XDTZ0021("xdtz0021", "台账入账（贸易融资福费廷）"),//台账入账（贸易融资\福费廷）
    TRADE_CODE_XDTZ0022("xdtz0022", "保证金台账入账"),//保证金台账入账
    TRADE_CODE_XDTZ0023("xdtz0023", "保证金等级入账"),//保证金等级入账
    TRADE_CODE_XDTZ0024("xdtz0024", "根据多个借据号到贷款台账中取得相应的贷款余额，五级分类一览"),//根据多个借据号到贷款台账中取得相应的贷款余额，五级分类一览
    TRADE_CODE_XDTZ0025("xdtz0025", "查询指定贷款开始日的优企贷客户贷款余额合计"),//查询指定贷款开始日的优企贷客户贷款余额合计
    TRADE_CODE_XDTZ0026("xdtz0026", "获取信贷营销裂变数据"),//获取信贷营销裂变数据
    TRADE_CODE_XDTZ0027("xdtz0027", "根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款"),//根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
    TRADE_CODE_XDTZ0029("xdtz0029", "查询指定票号在信贷台账中是否已贴现"),//查询指定票号在信贷台账中是否已贴现
    TRADE_CODE_XDTZ0030("xdtz0030", "查看信贷贴现台账中票据是否已经存在"),//查看信贷贴现台账中票据是否已经存在
    TRADE_CODE_XDTZ0031("xdtz0031", "根据合同号获取借据信息"),//根据合同号获取借据信息
    TRADE_CODE_XDTZ0032("xdtz0032", "查询客户所担保的行内当前贷款逾期件数"),//查询客户所担保的行内当前贷款逾期件数
    TRADE_CODE_XDTZ0033("xdtz0033", "查询客户所担保的行内贷款五级分类非正常状态件数"),//查询客户所担保的行内贷款五级分类非正常状态件数
    TRADE_CODE_XDTZ0034("xdtz0034", "根据客户号查询申请人是否有行内信用记录"),//根据客户号查询申请人是否有行内信用记录
    TRADE_CODE_XDTZ0035("xdtz0035", "根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录"),//根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
    TRADE_CODE_XDTZ0036("xdtz0036", "根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数"),//根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
    TRADE_CODE_XDTZ0038("xdtz0038", "台账信息通用列表查询"),//台账信息通用列表查询
    TRADE_CODE_XDTZ0037("xdtz0037", "无还本续贷额度状态更新"),//无还本续贷额度状态更新
    TRADE_CODE_XDTZ0039("xdtz0039", "根据企业名称查询申请企业在本行是否存在当前逾期贷款"),//根据企业名称查询申请企业在本行是否存在当前逾期贷款
    TRADE_CODE_XDTZ0040("xdtz0040", "申请人在本行当前逾期贷款数量"),//申请人在本行当前逾期贷款数量
    TRADE_CODE_XDTZ0041("xdtz0041", "根据客户号前往信贷查找房贷借据信息"),//根据客户号前往信贷查找房贷借据信息
    TRADE_CODE_XDTZ0042("xdtz0042", "申请人行内贷款五级分类异常笔数"),//申请人行内贷款五级分类异常笔数
    TRADE_CODE_XDTZ0043("xdtz0043", "统计客户行内信用类贷款余额"),//统计客户行内信用类贷款余额
    TRADE_CODE_XDTZ0044("xdtz0044", "查询正常客户房贷客群借据信息（排除已结清）"),//查询正常客户房贷客群借据信息（排除已结清）
    TRADE_CODE_XDTZ0045("xdtz0045", "商贷分户实时查询"),//商贷分户实时查询
    TRADE_CODE_XDTZ0046("xdtz0046", "根据借据号获取共同借款人信息"),//根据借据号获取共同借款人信息
    TRADE_CODE_XDTZ0047("xdtz0047", "借据信息查询（按证件号）"),//借据信息查询（按证件号）
    TRADE_CODE_XDTZ0048("xdtz0048", "小贷借款借据文本生成pdf"),//小贷借款借据文本生成pdf
    TRADE_CODE_XDTZ0049("xdtz0049", "客户贷款信息查询"),//客户贷款信息查询
    TRADE_CODE_XDTZ0050("xdtz0050", "对私客户关联业务检查"),//对私客户关联业务检查
    TRADE_CODE_XDTZ0051("xdtz0051", "对公客户关联业务检查"),//对公客户关联业务检查
    TRADE_CODE_XDTZ0052("xdtz0052", "根据借据号查询申请人行内还款（利息、本金）该笔借据次数"),//根据借据号查询申请人行内还款（利息、本金）该笔借据次数
    TRADE_CODE_XDTZ0053("xdtz0053", "个人社会关系查询"),//个人社会关系查询
    TRADE_CODE_XDTZ0054("xdtz0054", "将需要修改的受托支付账号生成待修改记录"),//将需要修改的受托支付账号生成待修改记录
    TRADE_CODE_XDTZ0055("xdtz0055", "查询退回受托信息"),//查询退回受托信息
    TRADE_CODE_XDTZ0056("xdtz0056", ""),//
    TRADE_CODE_XDTZ0057("xdtz0057", ""),//
    TRADE_CODE_XDTZ0058("xdtz0058", "台账信息通用列表查询"),//台账信息通用列表查询
    TRADE_CODE_XDTZ0059("xdtz0059", "更新信贷台账信息"),//更新信贷台账信息
    TRADE_CODE_XDTZ0060("xdtz0060", "查询我行有未结清贷款"),//查询我行有未结清贷款
    TRADE_CODE_XDTZ0061("xdtz0061", "小微电子签约借据号生成"),//小微电子签约借据号生成
    TRADE_CODE_XDTZ0062("xdtz0062", "查询客户的个人消费贷款"),//查询客户的个人消费贷款
    TRADE_CODE_XDTZ0063("xdtz0063", "他行退汇冻结编号更新"),//他行退汇冻结编号更新

    TRADE_CODE_XDDH0001("xddh0001", "查询贷后任务是否完成现场检查"),//查询贷后任务是否完成现场检查
    TRADE_CODE_XDDH0002("xddh0002", "新增主动还款申请记录"),//新增主动还款申请记录
    TRADE_CODE_XDDH0003("xddh0003", "主动还款申请记录列表查询"),//主动还款申请记录列表查询
    TRADE_CODE_XDDH0004("xddh0004", "查询还款是否在途"),//查询还款是否在途
    TRADE_CODE_XDDH0005("xddh0005", "提前还款"),//提前还款
    TRADE_CODE_XDDH0006("xddh0006", "提前还款试算查询"),//提前还款试算查询
    TRADE_CODE_XDDH0007("xddh0007", "还款记录列表查询"),//还款记录列表查询
    TRADE_CODE_XDDH0008("xddh0008", "还款计划列表查询"),//还款计划列表查询
    TRADE_CODE_XDDH0010("xddh0010", "推送优享贷预警信息"),//推送优享贷预警信息
    TRADE_CODE_XDDH0009("xddh0009", "还款日期卡控"),
    TRADE_CODE_XDDH0011("xddh0011", "小微平台贷后预警定期任务数据推送，由信贷生成定期任务"),
    TRADE_CODE_XDDH0012("xddh0012", "优农贷贷后预警通知"),
    TRADE_CODE_XDDH0013("xddh0013", "查询提前还款授权信息列表"),//查询提前还款授权信息列表
    TRADE_CODE_XDDH0015("xddh0015", "LPR确认数据修改"),//LPR确认数据修改
    TRADE_CODE_XDDH0016("xddh0016", "提前还款申请"),//提前还款申请
    TRADE_CODE_XDDH0017("xddh0017", "贷后任务完成接收"),//贷后任务完成接收
    TRADE_CODE_XDDB0001("xddb0001", "查询押品是否按揭"),//查询押品是否按揭
    TRADE_CODE_XDDB0002("xddb0002", "押品状态查询"),//押品状态查询
    TRADE_CODE_XDDB0003("xddb0003", "押品信息同步"),//押品信息同步
    TRADE_CODE_XDDB0004("xddb0004", "提醒任务处理结果实时同步"),//提醒任务处理结果实时同步
    TRADE_CODE_XDDB0005("xddb0005", "重估任务分配同步"),//重估任务分配同步
    TRADE_CODE_XDDB0006("xddb0006", "信贷押品状态查询"),//信贷押品状态查询
    TRADE_CODE_XDDB0007("xddb0007", "电票质押请求"),//电票质押请求
    TRADE_CODE_XDDB0008("xddb0008", "查询在线抵押信息"),//查询在线抵押信息
    TRADE_CODE_XDDB0009("xddb0009", "双录流水号与押品编号关系维护"),//双录流水号与押品编号关系维护
    TRADE_CODE_XDDB0010("xddb0010", "信贷押品列表查询"),//信贷押品列表查询
    TRADE_CODE_XDDB0011("xddb0011", "提前出库押品信息维护"),//提前出库押品信息维护
    TRADE_CODE_XDDB0012("xddb0012", "抵押登记获取押品信息"),//抵押登记获取押品信息
    TRADE_CODE_XDDB0013("xddb0013", "抵押登记不动产登记证明入库"),//抵押登记不动产登记证明入库
    TRADE_CODE_XDDB0014("xddb0014", "抵押登记注销风控"),//抵押登记注销风控
    TRADE_CODE_XDDB0015("xddb0015", "押品状态变更推送"),//押品状态变更推送
    TRADE_CODE_XDDB0016("xddb0016", "押品信息查询"),//押品信息查询
    TRADE_CODE_XDDB0017("xddb0017", "获取抵押登记双录音视频信息列表"),//获取抵押登记双录音视频信息列表
    TRADE_CODE_XDDB0018("xddb0018", "押品状态同步"),//押品状态同步
    TRADE_CODE_XDDB0019("xddb0019", "解质押押品校验"),//解质押押品校验
    TRADE_CODE_XDDB0020("xddb0020", "根据客户名查询抵押物类型"),
    TRADE_CODE_XDDB0021("xddb0021", "押品共有人信息查询"),
    TRADE_CODE_XDDB0022("xddb0022", "根据押品编号查询核心及信贷系统有无押品数据"),
    TRADE_CODE_XDDB0023("xddb0023", "押品出入库状态修改"),
    TRADE_CODE_XDDB0024("xddb0024", "抵押登记获取押品信息"),//抵押登记获取押品信息
    TRADE_CODE_XDXT0001("xdxt0001", "信贷同步人力资源"),//信贷同步人力资源
    TRADE_CODE_XDXT0002("xdxt0002", "根据直营团队类型查询客户经理工号"),
    TRADE_CODE_XDXT0003("xdxt0003", "分页查询小微客户经理"),//分页查询小微客户经理
    TRADE_CODE_XDXT0004("xdxt0004", "根据工号获取所辖区域"),//根据工号获取所辖区域
    TRADE_CODE_XDXT0005("xdxt0005", "查询客户经理所在分部编号"),//查询客户经理所在分部编号
    TRADE_CODE_XDXT0006("xdxt0006", "查询信贷用户的特定岗位信息"),//查询信贷用户的特定岗位信息
    TRADE_CODE_XDXT0007("xdxt0007", "根据客户经理所在机构号查询机构名称"),//根据客户经理所在机构号查询机构名称
    TRADE_CODE_XDXT0008("xdxt0008", "根据客户经理号查询账务机构号"),//根据客户经理号查询账务机构号
    TRADE_CODE_XDXT0009("xdxt0009", "客户经理是否为小微客户经理"),//客户经理是否为小微客户经理
    TRADE_CODE_XDXT0010("xdxt0010", "根据分中心负责人工号查询客户经理名单，包括工号、姓名"),//根据分中心负责人工号查询客户经理名单，包括工号、姓名
    TRADE_CODE_XDXT0011("xdxt0011", "客户经理信息详情查看"),//客户经理信息详情查看
    TRADE_CODE_XDXT0012("xdxt0012", "根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码"),//根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
    TRADE_CODE_XDXT0013("xdxt0013", "树形字典通用列表查询"),//树形字典通用列表查询
    TRADE_CODE_XDXT0014("xdxt0014", "字典项对象通用列表查询"),//字典项对象通用列表查询
    TRADE_CODE_XDXT0015("xdxt0015", "用户机构角色信息列表查询"),

    TRADE_CODE_XDZC0001("xdzc0001", "资产池协议列表查询接口"),//资产池协议列表查询接口
    TRADE_CODE_XDZC0002("xdzc0002", "资产池协议激活接口"),//资产池协议激活接口
    TRADE_CODE_XDZC0003("xdzc0003", "资产池协议详情查看接口"),//资产池协议详情查看接口
    TRADE_CODE_XDZC0004("xdzc0004", "客户资产清单查询接口"),//客户资产清单查询接口
    TRADE_CODE_XDZC0005("xdzc0005", "资产池入池接口"),//资产池入池接口
    TRADE_CODE_XDZC0006("xdzc0006", "资产池出池校验接口"),//资产池出池校验接口
    TRADE_CODE_XDZC0007("xdzc0007", "资产池出池接口"),//资产池出池接口
    TRADE_CODE_XDZC0008("xdzc0008", "资产池出票校验接口"),//资产池出票校验接口
    TRADE_CODE_XDZC0009("xdzc0009", "资产池出票交易接口"),//资产池出票交易接口
    TRADE_CODE_XDZC0010("xdzc0010", "资产池超短贷校验接口"),//资产池超短贷校验接口
    TRADE_CODE_XDZC0011("xdzc0011", "资产池超短贷放款接口"),//资产池超短贷放款接口
    TRADE_CODE_XDZC0012("xdzc0012", "资产池承兑行白名单校验"),//资产池承兑行白名但校验
    TRADE_CODE_XDZC0013("xdzc0013", "贸易合同资料上传接口"),//贸易合同资料上传接口
    TRADE_CODE_XDZC0014("xdzc0014", "资产池台账查询"),//资产池台账查询
    TRADE_CODE_XDZC0015("xdzc0015", "保证金查询接口"),//保证金查询接口
    TRADE_CODE_XDZC0016("xdzc0016", "购销合同查询"),//购销合同查询
    TRADE_CODE_XDZC0017("xdzc0017", "购销合同申请撤回"),//购销合同申请撤回
    TRADE_CODE_XDZC0018("xdzc0018", "资产汇总查询"),//资产汇总查询
    TRADE_CODE_XDZC0019("xdzc0019", "历史出入池记录查询"),//历史出入池记录查询
    TRADE_CODE_XDZC0020("xdzc0020", "出入池详情查询"),//出入池详情查询
    TRADE_CODE_XDZC0021("xdzc0021", "融资汇总查询"),//融资汇总查询
    TRADE_CODE_XDZC0022("xdzc0022", "发票补录"),//发票补录
    TRADE_CODE_XDZC0023("xdzc0023", "资产池白名单"),//资产池白名单
    TRADE_CODE_XDZC0024("xdzc0024", "票据池资料补全查询"),//票据池资料补全查询
    TRADE_CODE_XDZC0025("xdzc0025", "资产池业务开关检查"),
    TRADE_CODE_XDZC0026("xdzc0026", "资产池主动还款接口"),
    TRADE_CODE_XDDA0001("xdda0001", "扫描人信息登记"),//扫描人信息登记
    TRADE_CODE_XDDA0002("xdda0002", "同步档案人状态"),//同步档案人状态
    TRADE_CODE_XDDA0003("xdda0003", "同步档案台账以及借阅明细状态"),//同步档案台账以及借阅明细状态

    TRADE_CODE_XDCA0001("xdca0001", "信用卡调额申请"),//信用卡调额申请

    TRADE_CODE_XDCA0002("xdca0002", "大额分期试算接口"),//大额分期试算接口

    TRADE_CODE_XDCA0003("xdca0003", "大额分期合同列表查询"),//大额分期合同列表查询

    TRADE_CODE_XDCA0004("xdca0004", "大额分期合同详情接口"),//大额分期合同详情接口

    TRADE_CODE_XDCA0005("xdca0005", "大额分期合同签订接口"),//大额分期合同签订接口

    TRADE_CODE_XDCA0006("xdca0006", "信用卡审批结果查询接口"),//信用卡审批结果查询接口

    TRADE_CODE_XDZX0001("xdzx0001", "查询征信业务流水号"),//查询征信业务流水号

    TRADE_CODE_XDZX0002("xdzx0002", "征信授权查看"),//征信授权查看
    TRADE_CODE_XDZX0003("xdzx0003", "征信查询授权状态同步"),//征信查询授权状态同步
    TRADE_CODE_XDZX0004("xdzx0004", "授权结果反馈接口"),//授权结果反馈接口
    TRADE_CODE_XDQT0001("xdqt0001", "根据产品号查询产品名称"),//根据产品号查询产品名称

    TRADE_CODE_XDQT0003("xdqt0003", "企业网银推送预约信息"),//企业网银推送预约信息

    TRADE_CODE_XDQT0004("xdqt0004", "贷款申请预约（个人客户）"),//贷款申请预约（个人客户）

    TRADE_CODE_XDQT0005("xdqt0005", "贷款申请预约（企业客户）"),//贷款申请预约（企业客户）

    TRADE_CODE_XDQT0006("xdqt0006", "当日跑批状态查询"),//当日跑批状态查询

    TRADE_CODE_XDQT0007("xdqt0007", "新微贷产品信息同步"),//新微贷产品信息同步

    TRADE_CODE_XDQT0008("xdqt0008", "乐悠金卡关联人查询"),//乐悠金卡关联人查询

    TRADE_CODE_XDQT0009("xdqt0009", "微业贷信贷文件处理通知"),//微业贷信贷文件处理通知

    TRADE_CODE_XDWB0001("xdwb0001", "省联社金融服务平台借据信息查询接口"),//省联社金融服务平台借据信息查询接口

    
    TRADE_CODE_XD17242("xd17242", "省联社核准信用卡审批结果接口"),//省联社核准信用卡审批结果接口

    TRADE_CODE_XDCX0001("xdcx0001", "根据行号查询同业客户号"),//根据行号查询同业客户号

    /**
     * 服务端交易码 结束
     **/

    TRADE_CODE_GETADMINBYLOGINCODE("AdminSmUserService.getByLoginCode", "通过账号获取用户信息"),
    TRADE_CODE_GETORGBYORGCODE("adminSmOrgService.getByOrgCode", "通过机构代码获取机构信息"),
    TRADE_CODE_FKPJ35("fkpj35", "票据承兑签发审批请求");
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EsbEnum enumData : EnumSet.allOf(EsbEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
