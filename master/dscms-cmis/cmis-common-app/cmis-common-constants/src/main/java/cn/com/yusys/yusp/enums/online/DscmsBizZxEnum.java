package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-征信服务的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizZxEnum {
    /**
     * 指标通用接口中指标编码 相关枚举 开始
     **/
    RULE_CODE_R001("R001", "个人根据业务号获取指定报告"),//个人根据业务号获取指定报告
    RULE_CODE_R002("R002", "企业根据业务号获取指定报告"),//企业根据业务号获取指定报告
    RULE_CODE_R003("R003", "个人多少天内最新本地征信"),//个人多少天内最新本地征信
    RULE_CODE_R004("R004", "企业多少天内最新本地征信"),//企业多少天内最新本地征信
    RULE_CODE_R005("R005", "个人获取客户最新授权"),//个人获取客户最新授权
    RULE_CODE_R006("R006", "企业获取客户最新授权"),//企业获取客户最新授权
    RULE_CODE_R007("R007", "小微优企贷风险提示"),//小微优企贷风险提示
    RULE_CODE_R008("R008", "零售风险提示"),//零售风险提示
    RULE_CODE_R009("R009", "小微征信信息"),//小微征信信息
    RULE_CODE_R010("R010", "小微企业征信信息"),//小微企业征信信息
    /** 指标通用接口中指标编码 相关枚举 结束**/
    /**
     * 影像系统相关枚举 开始
     **/
    // s:1-采集 2-查看
    IMAGE_S_1("1", "采集"),
    IMAGE_S_2("2", "查看"),
    // 索引信息json串，该值根据各个业务模块生成
    INDEX_CONTID("contid", "合同编号"),
    // 第三方系统业务类型根节点编码，该值根据各个业务模块生成,TODO 和行内科技确定 不同业务场景该值
    TOP_OUTSYSTEM_CODE_DEFAULT("", ""),
    // 第三方系统业务类型子节点编码，该节点填写，只能查看所列节点的影像；不填写，显示所有根节点下的子节点，该值根据各个业务模块生成,TODO 和行内科技确定 不同业务场景该值
    OUTSYSTEM_CODE_DEFAULT("", ""),
    /** 影像系统相关枚举 结束  **/

    /**
     * 二代征信系统相关枚举 开始
     **/
    // 二代征信影像接口url（个人）
    REPORT_TYPE_PER("cpqReport", "个人"),
    // 二代征信影像接口url（对公）
    REPORT_TYPE_ENT("ceqReport", "对公"),
    /** 二代征信统相关枚举 结束  **/
    /**流程状态标识位 000-待发起**/
    WF_STATUS_000 ("000","待发起"),
    /**流程状态标识位 111-审批中**/
    WF_STATUS_111("111","审批中"),
    /**流程状态标识位 990-取消**/
    WF_STATUS_990("990","取消"),
    /**流程状态标识位 991-拿回**/
    WF_STATUS_991("991","拿回"),
    /**流程状态标识位 992-打回**/
    WF_STATUS_992("992","打回"),
    /**流程状态标识位 997-通过**/
    WF_STATUS_997("997","通过"),
    /**流程状态标识位 998-拒绝**/
     WF_STATUS_998("998","拒绝"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizZxEnum enumData : EnumSet.allOf(DscmsBizZxEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizZxEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizZxEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
