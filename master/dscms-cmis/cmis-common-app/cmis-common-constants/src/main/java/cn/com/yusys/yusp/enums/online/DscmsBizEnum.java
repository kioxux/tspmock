package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中流程作业服务枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizEnum {
    /**
     * 合作方额度申请
     **/
    WF_LMT_COOP_APP("WF_LMT_COOP_APP", "合作方额度申请"),
    /**
     * 单笔单批业务申请
     **/
    IQP_SINGLE_BATCH_APP("IQP_SINGLE_BATCH_APP", "单笔单批业务申请"),
    /**
     * 额度项下业务申请
     **/
    IQP_SPECIAL_APP("IQP_SPECIAL_APP", "特殊业务申请"),
    /**
     * 特殊业务申请
     **/
    IQP_UNDER_QUATO_APP("IQP_UNDER_QUATO_APP", "额度项下业务申请"),
    /**
     * 放款申请流程
     **/
    PVP_LOAN_APP("PVP_LOAN_APP", "放款申请流程"),
    /**
     * 个人额度申请
     **/
    LMT_INDIV_APP("LMT_INDIV_APP", "放款申请流程"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizEnum enumData : EnumSet.allOf(DscmsBizEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
