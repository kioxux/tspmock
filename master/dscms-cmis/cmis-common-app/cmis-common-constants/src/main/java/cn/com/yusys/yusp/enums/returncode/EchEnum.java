package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * ECH08：代表批量管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 业务服务的错误异常枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EchEnum {
    ECH080001("80001", "批量管理服务异常"),
    ECH080002("80002", "传递的FTP连接参数对象(batTransferCfg)为空"),
    ECH080003("80003", "连接到FTP服务器发生异常"),
    ECH080004("80004", "FTP Server:[{}]'s User:[{}] 登录失败!"),
    ECH080005("80005", "下载任务下所有文件至本地处理目录方法异常,异常信息为[{}]"),
    ECH080006("80006", "下载远程FTP服务器文件至本地发生异常,异常信息为[{}]"),
    ECH080007("80007", "下载远程FTP服务器文件至本地发生异常,异常信息为[{}]"),
    ECH080008("80008", "调度运行管理(BAT_CONTROL_RUN)未配置"),
    ECH080009("80009", "营业日期为[{}]的调度任务状态为[{}],任务还未成功"),
    ECH080010("80010", "任务配置信息表(BAT_TASK_CFG)未配置,请及时检查!"),
    ECH080011("80011", "组装文件目录错误,异常信息为[{}],请及时检查!"),
    ECH080012("80012", "客户移交范围配置表(CFG_CUS_HAND_OVER)未配置,请及时检查!"),
    ECH080013("80013", "批量任务为:[调度运行开始-更新批量相关表]运行失败,请及时检查!"),
    ECH080014("80014", "任务日期:[\" + taskDate + \"],任务编号:[\" + pendingTaskNo + \"],任务名称:[\" + pendingTaskName + \"]调用异常，异常信息为:[\" + e.getMessage() + \"]"),
    ECH080015("80015", "检查转换后的文件中的数据条数和数据库中的数据条数是否相等存在异常，请及时检查!"),
    ECH080016("80016", "调度运行管理(BAT_CONTROL_RUN)中状态不是运行中，请及时检查!"),
    ECH089999("89999", "批量任务执行异常"),
    ;

    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EcsEnum enumData : EnumSet.allOf(EcsEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EchEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EchEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
