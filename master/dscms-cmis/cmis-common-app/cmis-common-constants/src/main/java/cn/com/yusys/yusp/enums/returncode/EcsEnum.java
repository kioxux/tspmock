package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 客户服务的错误异常枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EcsEnum {

    ECS040001("40001", "查询类型queryType为空！"),
    ECS040002("40002", "客户编号为空！"),
    ECS040003("40003", "证件类型为空!"),
    ECS040004("40004", "证件号码为空!"),
    ECS040005("40005", "客户姓名为空!"),
    ECS040006("40006", "无该集团成员信息！"),
    ECS040007("40007", "对公对私类型为空！"),
    ECS040008("40008", "客户名称或客户编号为空！"),
    ECS040009("40009", "信贷系统不允许开立ECIF不存在的正式客户！"),
    ECS040010("40010", "客户编号和证件类型、证件号码不能都为空!"),
    ECS040011("40011", "集团编号和成员客户编号不能都为空!"),
    ECS040012("40012", "未查询到集团编号"),
    ECS040013("40013", "不是法人客户"),
    ECS040014("40014", "客户端异常，异常信息[{}]"),
    ECS040015("40015", "未识别操作类型！"),

    CUS_CLIENT_DEF_SUCCESS("000000", "操作成功"),
    CUS_CLIENT_DEF_EXCEPTION("999999", "操作失败！"),
    E_CUS_CLIENT_PARAMS_EXCEPTION("CLE00001", "入参为空！"),
    E_CUS_CLIENT_CUSNULL_EXCEPTION("CLE00001", "未获取到客户信息！"),
    E_CUS_CLIENT_CPBR_SUCCESS("000000", "不宜贷款户信息为空"),
    E_CUS_HANDLE_EXCEPTION("CLE000008", "业务申请流程处理异常！"),
    //接口删除操作
    E_CUS_DELETE_FAILED("CDF000001", "删除失败！"),
    E_CUS_HANDOVER_CONFIG_CHECK("CUSE0001", "没有匹配到符合条件的客户移交配置数据"),
    E_CUS_HANDOVER_GUIDE_INSERT("CUSE0002", "插入失败，失败原因：插入条数不为1"),
    E_CUS_INFO_ISNULL("CUSE0003", "为空"),
    E_CUS_INFO_INVALID("CUSE0004", "无效"),
    E_CUS_INFO_EXIST("CUSE0005", "存在该证件号码的正式客户，请核对!"),
    E_CUS_INFO_EXIST_TMP("CUSE0008", "存在该证件号码的临时客户，请核对!"),
    E_CUS_INFO_NOT_EXIST("CUSE0006", "信息不存在"),
    E_CUS_INFO_NOT_CUS_STATE_10("CUSE0007", "该客户的状态不为“正式客户”"),

    // 客户关系人关系
    E_INDIV_CUS_REL("999999", "保存失败，该客户已存在配偶！"),
    E_SAVE_SUCCESS("000000", "保存成功！"),
    E_SAVE_FAIL("999999", "保存失败！"),
    E_PASS_PARAM_FAIL("999999", "传递的个人社会关系信息参数为空！"),
    /**
     * 默认成功
     **/
    CUS_GRP_SUCCESS_DEF("ECS040000", "操作成功"),
    CUS_GRP_EXCEPTION_DEF("ECS049999", "操作异常！"),

    /**
     * 在途校验
     **/
    CUS_GRP_APP_CHECK_DEF("ECS040001", "该客户已发起集团认定申请或集团客户修改，请核实！"),
    CUS_GRP_CHECK_DEF("ECS040002", "该客户已存在集团客户中!"),
    CUS_GRP_MEMBER_APP_CHECK_DEF("ECS040003", "该客户已存在本集团成员客户!"),
    CUS_GRP_APP_INWAY_CHECK_DEF("ECS040003", "该集团已存在审批中业务!"),

    /**
     * 系统sql报错
     **/
    CUS_GRP_SQL_INSERT_DEF("ECS045001", "SQL执行插入报错!"),
    CUS_GRP_SQL_UPDATE_DEF("ECS045002", "SQL执行更新报错!"),
    CUS_GRP_SQL_DELETE_DEF("ECS045003", "SQL执行删除报错!"),
    CUS_GRP_SQL_SELECT_DEF("ECS045004", "SQL执行查询报错!"),
    CUS_GRP_SQL_SELECT_DATA_DEF("ECS045005", "SQL查询，未查询到数据!"),
    CUS_GRP_SQL_ERR_DEF("ECS049999", "SQL执行报错!"),
    CUS_GRP_MEMBER_CHECK_DEF("ECS040010", "该核心客户保存集团成员申请表失败！"),

    E_FNC_STAT_TYPE_NOT_EXIST("FNCE0000", "未识别的财务报表种类信息！"),
    E_FNC_STAT_PRD_STYLE_NOT_EXIST("FNCE0001", "未识别的财务报表周期类型！"),
    E_FNC_CHK_FRM_TRUE("FNCE0002", "公式校验不通过！"),
    E_FNC_OUTPUT_ERROR("FNCE0003", "导出财务报表报错！"),
    E_FNC_IMPORT_REEOR01("FNCE0004", "导入文件格式识别失败！"),
    E_FNC_IMPORT_REEOR02("FNCE0005", "导入文件中存在sheet页为空白页！"),
    /***
     * 客户信息
     */
    CUS_INDIV_CHECK_RESULT00("00", "身份校验成功！"),
    CUS_INDIV_CHECK_RESULT01("01", "联网核查校验身份信息不通过！"),
    CUS_INDIV_QUERY_RESULT("01", "个人客户信息查询失败！"),
    CUS_INDIV_CREATE_RESULT("01", "个人客户维护失败！"),
    CUS_INDIV_QUERY("999999", "开户失败，请到柜面完成正式客户开户！"),
    CUS_INDIV_CHECK_HYZK("CUS001", "婚姻状况为已婚，必须录入配偶信息！")
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EcsEnum enumData : EnumSet.allOf(EcsEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EcsEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EcsEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
