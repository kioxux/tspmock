package cn.com.yusys.yusp.enums.batch;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 贷记卡分期信息文件字段枚举类
 */
public enum DjkDataLoanFieldEnums {
    LOAN_ID("loanId", 9),//分期计划 ID
    ACCT_NO("acctNo", 9),//账户编号
    REF_NBR("refNbr", 23),//交易参考号
    LOGICAL_CARD_NO("logicalCardNo", 19),//卡号
    REGISTER_DATE("registerDate", 8),//分期注册日期
    LOAN_TYPE("loanType", 1),//分期类型
    LOAN_STATUS("loanStatus", 1),//分期状态
    LOAN_INIT_TERM("loanInitTerm", 3),//分期总期数
    CURR_TERM("currTerm", 3),//当前期数
    LOAN_INIT_PRIN("loanInitPrin", 15),//分期总本金
    LOAN_FIXED_PMT_PRIN("loanFixedPmtPrin", 15),//分期每期应还本金
    LOAN_FIRST_TERM_PRIN("loanFirstTermPrin", 15),//分期首期应还本金
    LOAN_FINAL_TERM_PRIN("loanFinalTermPrin", 15),//分期末期应还本金
    LOAN_INIT_FEE1("loanInitFee1", 15),//分期总手续费
    LOAN_FIXED_FEE1("loanFixedFee1", 15),//分期每期手续费
    LOAN_FIRST_TERM_FEE1("loanFirstTermFee1", 15),//分期首期手续费
    LOAN_FINAL_TERM_FEE1("loanFinalTermFee1", 15),//分期末期手续费
    PAID_OUT_DATE("paidOutDate", 8),//还清日期
    LOAN_CURR_BAL("loanCurrBal", 15),//分期当前总余额
    LOAN_BAL_XFROUT("loanBalXfrout", 15),//分期未到期本金
    LOAN_CODE("loanCode", 4),//分期计划代码
    ACCT_TYPE("acctType", 1),//账户类型
    REMAIN_TERM("remainTerm", 2),//剩余期数
    TERMINATE_REASON_CD("terminateReasonCd", 1),//分期中止原因代码
    FEE_PAID("feePaid", 15),//已偿手续费
    TERMINATE_DATE("terminateDate", 8),//分期提前中止日期
    ADV_PMT_IND("advPmtInd", 2),//提前还款标志
    MARKETER_NAME("marketerName", 80),//营销人员姓名
    MARKETER_ID("marketerId", 20),//营销人员编号
    MARKETER_BRANCH_ID("marketerBranchId", 9),//营销人员所属分行
    LOAN_PRIN_APP("loanPrinApp", 8),//分期款项用途
    GUARANTY("guaranty", 1),//是否抵押
    CHANNEL_ID("channelId", 2),//渠道编号
    UNPAY_PRIN("unpayPrin", 15),//未还分期本金总额
    UNPAY_TXNFEE("unpayTxnfee", 15),//未还分期交易费
    UNPAY_INTEREST("unpayInterest", 15),//未还分期利息
    FILLER("filler", 35);//预留域

    public static Map<String, Integer> fieldLength;

    static {
        fieldLength = new TreeMap<String, Integer>();
        for (DjkDataLoanFieldEnums enumData : EnumSet.allOf(DjkDataLoanFieldEnums.class)) {
            fieldLength.put(enumData.field, enumData.length);
        }
    }

    public String field;//字段名
    public Integer length;//长度

    DjkDataLoanFieldEnums(String field, Integer length) {
        this.field = field;
        this.length = length;
    }

    public static Integer lookup(String field) {
        return fieldLength.get(field);
    }

    public static String getField(Integer dataLength) {
        String field = null;
        for (DjkDataLoanFieldEnums enumData : values()) {
            if (enumData.length.equals(dataLength)) {
                field = enumData.field;
                break;
            }
        }
        return field;
    }

    public final Integer getLength() {
        return fieldLength.get(this.field);
    }
}
