package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-对公授信的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizSxEnum {
    TEST_ENUM("TEST", "中文描述"),//中文描述

    /**
     * 风控发信贷根据客户号查询授信信息返回状态枚举 开始
     **/
    CRD_STATUS_01("01", "存量客户在我行有授信业务，但不包含房抵e点授信"),
    CRD_STATUS_02("02", "存量客户在我行未有过授信，或授信已结束"),
    CRD_STATUS_03("03", "存量客户在我行已授信房抵e点贷，但未签订有效的最高额借款合同"),
    CRD_STATUS_04("04", "存量客户在我行已授信房抵e点贷，并签订了有效的最高额借款合同"),
    CRD_STATUS_05("05", "存量客户关联企业在我行已授信省心快贷业务"),
    CRD_STATUS_06("06", "非存量客户"),
    CRD_STATUS_07("07", "存量客户，存在授信申请记录，但授信未生效"),


    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizSxEnum enumData : EnumSet.allOf(DscmsBizSxEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizSxEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizSxEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
