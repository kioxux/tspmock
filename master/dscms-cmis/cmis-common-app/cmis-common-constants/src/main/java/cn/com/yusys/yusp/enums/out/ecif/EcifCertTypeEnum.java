package cn.com.yusys.yusp.enums.out.ecif;

import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * ECIF 证件类型转义
 */
public enum EcifCertTypeEnum {
    /**
     130	户口簿                             Q	组织机构代码
     112	军官证                             M	营业执照
     100	居民身份证                         R	统一社会信用代码
     161	港澳居民往来内地通行证             N	批文
     220	统一社会信用代码                   O	开户证明
     113	文职干部证                         U	登记证书
     111	士兵证                             V	境外企业代码
     142	外国护照                           P	对公其他
     140	中国护照                           	对私证件类型
     210	组织机构代码证                     A	身份证
     200	营业执照                           B	护照
     162	台湾居民往来大陆通行证             C	户口簿
     122	警官证                             D	港澳居民来往内地通行证
     299	其它证件（公司）                   E	台湾居民来往大陆通行证
     0	0                                    F	边民出入境通行证
     199	其它证件（个人）                   G	军官证
     H	士兵证
     I	军事院校学员证
     J	军人离休干部荣誉证
     K	军官退休证
     L	军人文职干部退休证
     S	外国人永久居住证
     W	港澳台居住证
     T	对私其他
     */
    //key是信贷码值，value是ecif码值
    CERT_TYPE_ENUM210("210", "Q"),
    CERT_TYPE_ENUM220("220", "R"),
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EcifCertTypeEnum enumData : EnumSet.allOf(EcifCertTypeEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EcifCertTypeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(String key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EcifCertTypeEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
