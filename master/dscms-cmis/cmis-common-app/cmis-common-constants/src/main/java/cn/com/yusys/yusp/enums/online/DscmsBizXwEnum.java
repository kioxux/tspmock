package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中业务服务-小微业务的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsBizXwEnum {
    /* XDXW0043	 无还本续贷待办接收 枚举 start*/
    XDXW0043_FLAG_01("01", "优转"),// 优转
    XDXW0043_FLAG_02("02", "普转"),// 普转
    XDXW0043_FLAG_98("98", "异常规则拒绝"),// 异常规则拒绝
    XDXW0043_FLAG_99("99", "正常规则拒绝"),// 正常规则拒绝
    /* XDXW0043	 无还本续贷待办接收 枚举 end  */
    /**
     * 操作成功标志位 字典项  S成功 F失败
     */
    OP_FLAG_S("S", "成功"),//成功
    OP_FLAG_F("F", "失败"),//失败
    OP_FLAG_0000("0000", "成功"),//成功
    OP_FLAG_9999("9999", "失败"),//失败
    /**
     * 查询类型相关枚举 开始
     **/
    QUERY_TYPE_01("01", "按授信调查流水号查询"),//按授信调查流水号查询
    QUERY_TYPE_02("02", "按申请编号查询"),//按申请编号查询

    /**
     * 成功失败标识枚举 开始
     **/
    SUCCESS("S", "成功"),//成功
    FAIL("F", "失败"),//失败

    /**
     * 优抵贷客户调查撤销描述信息枚举 开始
     **/
    MESSAGE_01("01", "已签订合同！无法作废！"),//已签订合同！无法作废！
    MESSAGE_02("02", "已签订合同！无法否决！"),//已签订合同！无法否决！
    MESSAGE_03("03", "不存在对应的授信调查报告,请检查！"),//不存在对应的授信调查报告
    MESSAGE_04("04", "不存在对应的模型审批结果表记录,请检查！"),//不存在对应的授信调查报告

    /**
     * 优抵贷客户调查撤销操作标识枚举 开始
     **/
    OPFLG_01("01", "撤回"),//撤回
    OPFLG_02("02", "否决"),//否决
    OPFLG_03("03", "通过"),//

    /**
     * 优农贷黑白名单校验标识枚举 开始
     **/
    CHECK_RESULT_01("01", "01白名单"),
    CHECK_RESULT_04("04", "04已通知区域主管进行业务分配"),
    CHECK_RESULT_05("05", "05信息正在完善中，请勿重复提交"),
    CHECK_RESULT_06("06", "06经营地址不在我行业务范围内"),
    CHECK_RESULT_00("00", "配偶双方不能同时申请"),

    /**
     * 区域名称枚举 开始
     **/
    AREA_01("01", "苏州"),
    AREA_02("02", "张家港"),
    AREA_03("03", "杨舍镇"),
    AREA_04("04", "市区"),
    AREA_05("05", "塘桥镇"),
    AREA_06("06", "凤凰镇"),
    AREA_07("07", "塘桥"),
    AREA_08("08", "金港镇"),
    AREA_09("09", "大新镇"),
    AREA_10("10", "后塍镇"),
    AREA_11("11", "张家港保税区"),
    AREA_12("12", "金港"),
    AREA_13("13", "锦丰镇"),
    AREA_14("14", "乐余镇"),
    AREA_15("15", "南丰镇"),
    AREA_16("16", "张家港市现代农业示范园区"),
    AREA_17("17", "沙洲"),
    AREA_18("18", "区"),
    AREA_19("19", "常熟"),
    AREA_20("20", "昆山"),
    AREA_21("21", "无锡"),
    AREA_22("22", "江阴"),
    AREA_23("23", "宜兴"),
    AREA_24("24", "镇江"),
    AREA_25("25", "丹阳"),
    AREA_26("26", "徐州"),
    AREA_27("27", "云龙"),
    AREA_28("28", "邳州"),
    AREA_29("29", "宿迁"),
    AREA_30("30", "宿豫"),
    AREA_31("31", "连云港"),
    AREA_32("32", "新浦"),
    AREA_33("33", "南通"),
    AREA_34("34", "崇川区"),
    AREA_35("35", "崇川"),
    AREA_36("36", "通州区"),
    AREA_37("37", "通州"),
    AREA_38("38", "港闸区"),
    AREA_39("39", "开发区"),
    AREA_40("40", "海门"),
    AREA_41("41", "如皋"),
    AREA_42("42", "启东"),
    AREA_43("43", "即墨"),
    AREA_44("44", "张家港经济技术开发区"),

    /**
     * 支行机构枚举 开始
     **/
    ORG_CODE_127000("127000", "苏州分行营业部管理部"),
    ORG_CODE_127100("127100", "吴江支行营业部管理部"),
    ORG_CODE_107000("107000", "昆山支行营业部管理部"),
    ORG_CODE_097000("097000", "常熟支行营业部管理部"),
    ORG_CODE_016000("016000", "小额贷款管理部"),
    ORG_CODE_016001("016001", "小额贷款中心"),

    /**
     * 接口(XDXW0009)担保人人数查询返回信息枚举 开始
     **/
    RETURN_MESSAGE_01("xdxw000901", "该房产对应的最高额担保合同中担保人人数大于3，请走线下优抵贷！"),
    RETURN_MESSAGE_02("xdxw000902", "该业务非苏州大市范围内业务！"),
    RETURN_MESSAGE_03("xdxw000903", "请到信贷系统维护合同用章编码、抵押用章编码！"),

    /**
     * 接口(XDXW0039)决议信息返回信息枚举 开始
     **/
    RETURN_MESSAGE_3901("xdxw003901", "未传有效数据"),
    RETURN_MESSAGE_3902("xdxw003902", "该客户税贷审批已经生成决议，请勿重复提交！"),
    RETURN_MESSAGE_3903("xdxw003903", "税贷审批处理成功！"),
    RETURN_MESSAGE_3904("xdxw003904", "该决议下已经有放款，无法删除！"),
    RETURN_MESSAGE_3905("xdxw003905", "该客户调查已经生成决议，请勿重复提交！"),

    /**
     * 接口(XDXW0039)类型内容枚举 开始
     **/
    OPTIONFLAG_01("01", "提交"),
    OPTIONFLAG_02("02", "作废"),
    OPTIONFLAG_03("03", "东方税银审批结果入账"),


    /**
     * 接口(XDXW0039)是否内容枚举 开始
     **/
    YESNO_01("01", "是"),
    YESNO_02("02", "否"),
    YESNO_1("2", "是"),
    YESNO_2("2", "否"),

    /**
     * opr_type 新增\删除
     **/
    ADD_01("01", "新增"),
    DELETE_02("02", "删除"),

    /**
     * 产品枚举 开始
     **/
    YQD_TYPE_01("SC010008", "优企贷"),//优企贷
    YQD_TYPE_02("SC020010", "优农贷"),//优农贷
    YDD_TYPE_SC020009("SC020009", "优抵贷"),//优抵贷
    YXD_TYPE_PW010004("PW010004", "优享贷"),//优享贷


    /**
     * 申请渠道枚举 开始
     **/
    APP_CHNL_00("00", "微信小程序"),//微信小程序
    APP_CHNL_01("01", "手机银行"),//手机银行
    APP_CHNL_02("02", "PC端"),//PC端
    APP_CHNL_03("03", "直销银行"),//直销银行
    APP_CHNL_04("04", "移动OA"),//移动OA
    APP_CHNL_05("05", "PAD端"),//PAD端


    /**
     * 分配状态枚举 开始
     **/
    DIVIS_STATUS_100("100", "已分配"),//已分配
    DIVIS_STATUS_101("101", "未分配"),//未分配
    DIVIS_STATUS_110("110", "重新分配"),//重新分配

    /**
     * 确认状态枚举 开始
     **/
    CONFIRM_STATUS_00("00", "待确认"),//待确认
    CONFIRM_STATUS_01("01", "已确认"),//已确认
    CONFIRM_STATUS_02("02", "无需确认"),//无需确认

    /**
     * 所属条线枚举 开始
     **/
    BELG_LINE_01("01", "小微"),//小微
    BELG_LINE_02("02", "零售"),//零售
    BELG_LINE_03("03", "对公"),//对公
    BELG_LINE_04("04", "资产池"),//资产池

    /**
     * 数据来源枚举 开始
     **/
    DATA_SOURCE_00("00", "人工新增"),//人工新增
    DATA_SOURCE_01("01", "系统推送"),//系统推送


    /**
     * 确认状态枚举 开始
     **/
    CONFIRMSTATUS_00("00", "待确认"),//待确认
    CONFIRMSTATUS_01("01", "已确认"),//已确认

    /**
     * 小贷企业证件类型枚举 开始
     **/
    CERT_TYPE_A("A", "身份证"),//身份证
    CERT_TYPE_R("R", "统一社会信用代码"),//统一社会信用代码
    CERT_TYPE_P2("P2", "中征码"),//中征码
    CERT_TYPE_M("M", "营业执照"),//营业执照
    CERT_TYPE_Q("Q", "组织机构代码"),//组织机构代码
    CERT_TYPE_P1("P1", "机构信用代码"),//机构信用代码

    /**
     * 小微调查报告类型枚举 开始
     **/
    SURVEY_TYPE_1("1", "综合消费贷"),//综合消费贷
    SURVEY_TYPE_2("2", "车易贷"),//车易贷
    SURVEY_TYPE_3("3", "经营贷二"),//经营贷二
    SURVEY_TYPE_4("4", "经营贷一"),//经营贷一
    SURVEY_TYPE_5("5", "创易贷"),//车易贷
    SURVEY_TYPE_6("6", "经营贷三"),//经营贷三
    SURVEY_TYPE_7("7", "无还本续贷普转"),//无还本续贷-普转
    SURVEY_TYPE_8("8", "优农贷"),//优农贷
    SURVEY_TYPE_9("9", "优抵贷尽职调查表"),//优抵贷尽职调查报告
    SURVEY_TYPE_10("10", "增享贷"),//增享贷
    SURVEY_TYPE_11("11", "惠享贷"),//惠享贷
    SURVEY_TYPE_12("12", "优企贷"),//优企贷
    SURVEY_TYPE_13("13", "优享贷"),//优享贷
    SURVEY_TYPE_14("14", "无还本续贷优转调查表"),//无还本优转
    SURVEY_TYPE_15("15", "优抵贷税务调查表"),//优抵贷税务调查报告

    /***
     * 接口(xdxw0048) 模型结果枚举
     */
    MODEL_RST_STATUS_1("1", "审批通过"),
    MODEL_RST_STATUS_2("2", "审批拒绝"),
    MODEL_RST_STATUS_3("3", "审批异常"),
    RETURN_MESSAGE_4801("xdxw004801", "该流水对应的模型审批结果已存在，请勿重复操作！"),
    RETURN_MESSAGE_4802("xdxw004802", "该流水对应的客户授信调查主表不存在！"),
    RETURN_MESSAGE_4803("xdxw004803", "该流水对应的调查报告基本信息不存在！"),

    /******************小微增享贷白名单办理状态开始*****************/
    XW_ZXD_APPLY_STATUS_00("00", "初始化"),
    XW_ZXD_APPLY_STATUS_01("01", "日终准入拒绝"),
    XW_ZXD_APPLY_STATUS_02("02", "待办"),
    XW_ZXD_APPLY_STATUS_03("03", "办理中"),
    XW_ZXD_APPLY_STATUS_04("04", "拒绝"),
    XW_ZXD_APPLY_STATUS_05("05", "办结"),
    XW_ZXD_APPLY_STATUS_07("07", "失效"),
    /******************小微增享贷白名单办理状态结束*****************/

    /******************小微风控模型审批结果开始*****************/
    XW_MODEL_RST_STATUS_000("000", "待发起"),
    XW_MODEL_RST_STATUS_001("001", "失效"),
    XW_MODEL_RST_STATUS_002("002", "拒绝"),
    XW_MODEL_RST_STATUS_003("003", "通过"),
    XW_MODEL_RST_STATUS_004("004", "审批中"),
    XW_MODEL_RST_STATUS_007("007", "异常"),
    XW_MODEL_RST_STATUS_10("10", "白名单审批通过"),
    XW_MODEL_RST_STATUS_20("20", "征信规则审批中"),
    XW_MODEL_RST_STATUS_50("50", "征信规则审批拒绝"),
    XW_MODEL_RST_STATUS_40("40", "征信规则审批通过"),
    XW_MODEL_RST_STATUS_30("30", "征信规则审批异常"),
    /******************小微风控模型审批结果结束*****************/
    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsBizXwEnum enumData : EnumSet.allOf(DscmsBizXwEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsBizXwEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsBizXwEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
