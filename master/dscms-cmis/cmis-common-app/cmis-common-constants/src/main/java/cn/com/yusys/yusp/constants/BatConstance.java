package cn.com.yusys.yusp.constants;

/**
 * 公共常量类
 *
 * @author weijd
 * @date 2019-11-20 09:50
 */
public class BatConstance {

    public static final String ENCODING_GBK = "GBK";
    public static final String ENCODING_UTF8 = "UTF-8";

    /**
     * Spring Batch配置参数
     **/
    public static final int CHUNK_SIZE = 10000;
    public static final int POOL_SIZE = 96;
}
