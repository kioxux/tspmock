package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 信贷系统中客户服务的枚举</br>
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月25日 23:56:54
 */
public enum DscmsCusEnum {
    /**
     * 识别方式:</br>
     * 1- 按证件类型、证件号码查询</br>
     * 2- 按证件号码查询</br>
     * 3- 按客户编号查询</br>
     * 4- 按客户名称模糊查询</br>
     */
    RESOTP_1("1", "按证件类型、证件号码查询"),
    RESOTP_2("2", "按证件号码查询"),
    RESOTP_3("3", "按客户编号查询"),
    RESOTP_4("4", "按客户名称模糊查询"),

    /**
     * 区分标识:</br>
     * 1公司客户</br>
     * 2同业客户</br>
     * 3专业贷款</br>
     */
    DTGH_FLAG_01("1", "公司客户"),
    DTGH_FLAG_02("2", "同业客户"),
    DTGH_FLAG_03("3", "专业贷款"),

    /**
     * 返回结果:</br>
     * S-成功</br>
     * F-失败</br>
     */
    RETURN_SUCCESS("S", "成功"),
    RETURN_FAIL("F", "失败"),

    ;
    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (DscmsCusEnum enumData : EnumSet.allOf(DscmsCusEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private DscmsCusEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (DscmsCusEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
