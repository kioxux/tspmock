package cn.com.yusys.yusp.enums.online;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * GXP的枚举</br>
 *
 * @author mochi
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum GxpEnum {
    /**
     * 交易码 开始
     **/
    TRADE_CODE_D15011("d15011", "永久额度调整"),// 永久额度调整
    TRADE_CODE_D13160("d13160", "大额现金分期申请"),//大额现金分期申请
    TRADE_CODE_D13162("d13162", "分期审核"),//分期审核
    TRADE_CODE_D14020("d14020", "卡片信息查询"),// 卡片信息查询
    TRADE_CODE_D13087("d13087", "现金（大额）放款接口"),
    TRADE_CODE_D12011("d12011", "账单交易明细查询"),// 账单交易明细查询
    TRADE_CODE_D12000("d12000", "账户信息查询"),
    TRADE_CODE_D12050("d12050", "账单列表查询"),// 账单列表查询
    TRADE_CODE_D11005("d11005", "信用卡申请"),//d11005
    TRADE_CODE_D11010("d11010", "客户基本资料查询"),
    TRADE_CODE_QYWX("qywx", "企业微信"),// 企业微信
    /** 交易码 结束 **/
    /**
     * 交易状态 开始
     */
    TRANSTATE_S("S", "交易成功"),//交易成功
    TRANSTATE_F("F", "交易失败"),//交易失败
    /** 交易状态结束 **/
    /**
     * 服务名称 开始
     **/
    SERVICE_NAME_GXP_TRADE_CLIENT("GxpTradeClient", "新信贷与GXP交互交易请求方"),
    SERVICE_NAME_QYWX_TRADE_CLIENT("QywxTradeClient", "新信贷与企业微信交互交易请求方"),
    /**服务名称 结束**/

    /**
     * 柜员号 开始
     **/
    USERID_TONGLIAN("80010422", "通联系统柜员号"),//通联系统柜员号
    /**柜员号 结束**/

    /**
     * 部门号 开始
     **/
    BRCHNO_TONGLIAN("009801", "通联系统部门号"),// 通联系统部门号
    /**部门号 结束**/

    /**
     * 渠道 开始
     **/
    SERVTP_EBS("EBS", "企业网银"),//企业网银
    SERVTP_MBL("MBL", "手机银行"),//手机银行
    SERVTP_NET("NET", "个人网银"),//个人网银
    SERVTP_WXP("WXP", "微信银行"),//微信银行
    SERVTP_DOC("DOC", "文件管理系统"),//文件管理系统
    SERVTP_XDG("XDG", "信贷管理系统"),//信贷管理系统
    SERVTP_EBT("EBT", "企业手机银行"),//企业手机银行
    SERVTP_NNT("NNT", "柜面"),//柜面
    SERVTP_SNT("SNT", "智能超柜系统"),//智能超柜系统
    SERVTP_ATM("ATM", "ATM系统"),//ATM系统
    SERVTP_ICS("ICS", "智能客服"),//智能客服
    SERVTP_RCP("RCP", "零售智能网贷系统"),//零售智能网贷系统
    SERVTP_XDW("XDW", "信贷中转处理系统"),//信贷中转处理系统
    SERVTP_YPP("YPP", "押品缓释系统"),//押品缓释系统
    SERVTP_GJP("GJP", "国际业务系统"),//国际业务系统
    SERVTP_CCP("CCP", "对公智能风控"),//对公智能风控
    SERVTP_HLW("HLW", "直销银行"),//直销银行
    SERVTP_YXT("YXT", "营销过程管理平台"),//营销过程管理平台
    SERVTP_BSP("BSP", "中台"),//中台
    SERVTP_XWH("XWH", "小微微信公众号"),//小微微信公众号
    SERVTP_YDY("YDY", "移动金融平台"),//移动金融平台
    SERVTP_ECF("ECF", "ECIF系统"),//ECIF系统
    SERVTP_XVP("XVP", "小微业务管理平台"),//小微业务管理平台
    SERVTP_OAM("OAM", "办公自动化系统"),//办公自动化系统
    SERVTP_SYS("SYS", "核心系统"),//核心系统
    SERVTP_USC("USC", "统一资金授信系统"),//统一资金授信系统
    SERVTP_OTF("OTF", "中台外围前置"),//中台外围前置
    SERVTP_GAP("GAP", "中间业务系统（资金清算类)"),//中间业务系统（资金清算类)
    SERVTP_ZFP("ZFP", "二代支付系统"),//二代支付系统
    SERVTP_PJP("PJP", "综合票据系统"),//综合票据系统
    SERVTP_LCP("LCP", "理财销售系统"),//理财销售系统
    SERVTP_RLP("RLP", "人力资源系统"),//人力资源系统
    SERVTP_DYY("DYY", "单笔验印系统"),//单笔验印系统
    SERVTP_ZNT("ZNT", "智能厅堂"),//智能厅堂
    SERVTP_SZF("SZF", "超级网银"),//超级网银
    SERVTP_ZGL("ZGL", "账户管理系统"),//账户管理系统
    SERVTP_ZWD("ZWD", "智能微贷审批系统"),//智能微贷审批系统
    SERVTP_DJK("DJK", "贷记卡非金融系统"),//贷记卡非金融系统
    SERVTP_FUN("FUN", "基金系统"),//基金系统
    SERVTP_ZZS("ZZS", "营改增系统"),//营改增系统
    SERVTP_HZF("HZF", "聚合支付平台"),//聚合支付平台
    SERVTP_FLS("FLS", "非零售内部评级系统"),//非零售内部评级系统
    SERVTP_REK("REK", "融e开对公开户预约系统"),//融e开对公开户预约系统
    SERVTP_JFP("JFP", "积分系统"),//积分系统
    SERVTP_VBK("VBK", "视频银行"),//视频银行
    SERVTP_SMC("SMC", "开放银行"),//开放银行
    SERVTP_LSP("LSP", "零售内部评级系统"),//零售内部评级系统
    SERVTP_WGH("WGH", "网格化系统"),//网格化系统
    SERVTP_IBS("IBS", "云从人脸"),//云从人脸
    SERVTP_EID("EID", "eID网络身份认证"),//eID网络身份认证
    SERVTP_HED("HED", "头寸系统"),//头寸系统
    SERVTP_YQZ("YQZ", "银联前置系统"),//银联前置系统
    SERVTP_YQS("YQS", "银联清算系统"),//银联清算系统
    SERVTP_ESB("ESB", "ESB系统"),//ESB系统
    SERVTP_PSP("PSP", "POSP系统"),//POSP系统
    SERVTP_CAL("CAL", "呼叫中心"),//呼叫中心
    SERVTP_ZXP("ZXP", "征信管理系统"),//征信管理系统
    SERVTP_GKI("GKI", "管理会计系统"),//管理会计系统
    SERVTP_YQD("YQD", "优企贷"),//优企贷
    SERVTP_PMS("PMS", "资管理财信息报送平台"),//资管理财信息报送平台
    SERVTP_FPT("FPT", "运营风险预警平台"),//运营风险预警平台
    SERVTP_DXT("DXT", "监控平台（网络运维）"),//监控平台（网络运维）
    SERVTP_XCX("XCX", "零售小程序"),//零售小程序
    SERVTP_DHK("DHK", "环控平台（网络运维）"),//环控平台（网络运维）
    SERVTP_RZP("RZP", "日终调度系统"),//日终调度系统
    SERVTP_CWP("CWP", "财务操作系统"),//财务操作系统
    SERVTP_ZXF("ZXF", "直销支付平台"),//直销支付平台
    SERVTP_COM("COM", "资金管理系统"),//资金管理系统
    SERVTP_SBS("SBS", "comstar外汇系统"),//comstar外汇系统
    SERVTP_ICP("ICP", "IC卡应用系统"),//IC卡应用系统
    SERVTP_GXP("GXP", "GXP系统"),//GXP系统
    SERVTP_ZHD("ZHD", "自助回单系统"),//自助回单系统
    SERVTP_GFD("GFD", "光伏贷系统"),//光伏贷系统
    SERVTP_DZD("DZD", "银科对账系统"),//银科对账系统
    SERVTP_XMH("XMH", "新门户"),//新门户
    SERVTP_KGP("KGP", "科技管理平台"),//科技管理平台
    /** 渠道 结束 **/


    ;

    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (GxpEnum enumData : EnumSet.allOf(GxpEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private GxpEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (GxpEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
