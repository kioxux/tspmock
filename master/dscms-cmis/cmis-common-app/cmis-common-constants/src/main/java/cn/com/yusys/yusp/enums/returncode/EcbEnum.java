package cn.com.yusys.yusp.enums.returncode;

import java.util.EnumSet;
import java.util.Map;
import java.util.TreeMap;

/**
 * 异常码说明:</br>
 * 0000:交易成功，和行内保持一致；</br>
 * 错误码共9位，其中前3位是英文字母，第4-5位和端口匹配，第6-9位是服务编码，取值范围位0001-9999；</br>
 * <p>
 * ECB01：代表流程作业服务的错误异常；</br>
 * ECF02：代表参数管理服务的错误异常；</br>
 * EPS03：代表贷后管理服务的错误异常；</br>
 * ECS04：代表客户管理服务的错误异常；</br>
 * ESP05：代表通讯转换服务的错误异常；</br>
 * ECN06：代表资产保全服务的错误异常；</br>
 * ECL07：代表额度管理服务的错误异常；</br>
 * EPB09：代表公共的错误异常；</br>
 * </p>
 * 业务服务的错误异常枚举</br>
 *
 * @author guyh
 * @author leehuang
 * @version 1.0
 * @since 2021年3月31日 23:56:54
 */
public enum EcbEnum {

    ECB010000("0000", "交易成功!"),
    ECB010001("10001", "传入参数异常!"),
    ECB010002("10002", "已存在在途的授信申请!"),
    ECB010003("10003", "流水号生成异常!"),
    ECB010004("10004", "登录用户信息异常!"),
    ECB010005("10005", "流程已处于审批中状态，请勿重复提交!"),
    ECB010006("10006", "不满足提交状态，禁止提交!"),
    ECB010007("10007", "适用品种额度不得高于分项授信额度，无法添加!"),
    ECB010008("10008", "该产品已存在于该分项下，无法添加!"),
    ECB010009("10009", "调查编号为空！"),
    ECB010010("10010", "合同编号为空！"),
    ECB010011("10011", "核心流水号为空！"),
    ECB010012("10012", "借据号为空！"),
    ECB010013("10013", "查询方式为空！"),
    ECB010014("10014", "客户编号为空！"),
    ECB010015("10015", "根据电票系统银承协议编号[{}]信贷系统没有获取到生效的银承协议!"),
    ECB010016("10016", "信贷系统已存在该笔贴现!"),
    ECB010017("10017", "查询无数据!"),
    ECB010018("10018", "根据押品编号获取押品信息为空！"),
    ECB010019("10019", "接口【bdcqcx】发送【押品系统】异常！"),
    ECB010020("10020", "客户经理存在未完成的申报填写!"),
    ECB010021("10021", "存在客户经理未完成的集团授信填报!"),
    ECB010022("10022", "查询人员ID为空！"),
    ECB010023("10023", "电话为空！"),
    ECB010024("10024", "该客户在信贷系统已存在面签信息，不可重复推送！"),
    ECB010025("10025", "该申请下已存在低风险分项"),
    ECB010026("10026", "该品种已关联该笔借据"),
    ECB010027("10027", "分项授信额度不得低于适用品种额度"),
    ECB010028("10028", "还款账号与放款时预留不一致"),//还款账号与放款时预留不一致
    ECB010029("10029", "当前日期提前还款截止日期,贷款不能支用及归还"),//每月25日至月底,贷款不能支用及归还
    ECB010030("10030", "每日17点到凌晨5点,贷款不能支用及归还"),//每日17点到凌晨5点,贷款不能支用及归还
    ECB010031("10031", "还款账号为空"),//还款账号为空
    ECB010032("10032", "根据借据号[{}]未查询保函台账信息"),//根据借据号[{}]未查询保函台账信息"
    ECB010033("10033", "校验身份证号码[{}]未通过"),//校验身份证号码[{}]未通过
    ECB010034("10034", "合同到期日不能大于用户60岁生日"),//合同到期日不能大于用户60岁生日
    ECB010035("10035", "合同到期日不能大于用户55岁生日"),//合同到期日不能大于用户55岁生日
    ECB010036("10036", "该客户所在企业有逾期、欠息贷款"),//该客户所在企业有逾期、欠息贷款
    ECB010037("10037", "客户授信调查已存在"),//客户授信调查已存在
    ECB010038("10038", "该客户所在企业贷款历史总额已超过5千万，无法申请工资贷"),// 该客户所在企业贷款历史总额已超过5千万，无法申请工资贷
    ECB010039("10039", "合同金额与审批结果不一致，请核实"),//合同金额与审批结果不一致，请核实
    ECB010040("10040", "合同利率与审批结果不一致，请核实"),//合同利率与审批结果不一致，请核实
    ECB010041("10041", "您不满足优农贷准入要求，请联系客户经理维护您的个人信息"),//您不满足优农贷准入要求，请联系客户经理维护您的个人信息
    ECB010042("10042", "当前授信敞口额度不允许增加"),//客户授信调查已存在
    ECB010043("10043", "根据电票系统银承协议编号[{}]和客户号[{}]信贷系统没有获取到生效的银承协议"),// 根据电票系统银承协议编号[{}]和客户号[{}]信贷系统没有获取到生效的银承协议
    ECB010044("10044", "替补借款人账号未输入"),//替补借款人账号未输入
    ECB010045("10045", "合同号[{}]已被占用"),//合同号[{}]已被占用
    ECB010046("10046", "客户经理工号为空！"),
    ECB010047("10047", "客户经理证件号为空！"),
    ECB010048("10048", "客户证件号为空！"),
    ECB010049("10049", "客户经理名称为空！"),
    ECB010050("10050", "业务流水号为空！"),
    ECB010051("10051", "产品编号为空！"),
    ECB010052("10052", "流程类型对应业务类型非预期！"),
    ECB010053("10053", "该笔合同额度已被占用完,不能出账!"),
    ECB010054("10054", "查询集团客户成员列表异常!"),
    ECB010055("10055", "该抵押物类型不符，不属于：10001，10002，10003，10040！"),
    ECB010056("10056", "该抵押物[{}]未查询到所在区域！"),
    ECB010057("10057", "该抵押物[{}]非苏州地区抵押物！"),
    ECB010058("10058", "该客户信贷系统没有获取到可用的银承协议!"),
    ECB010059("10059", "出账申请数据查询异常!"),
    ECB010060("10060", "客户基本信息查询异常!"),
    ECB010061("10061", "当前合同编号已存在在途的合同影像审核业务!"),
    ECB010062("10062", "授信申请数据查询异常!"),
    ECB010063("10063", "授信审批数据查询异常!"),
    ECB010064("10064", "授信批复数据查询异常!"),
    ECB010065("10065", "授信批复台账数据查询异常!"),
    ECB010066("10066", "授信批复变更数据查询异常!"),
    ECB010067("10067", "更新授信批复变更分项数据异常!"),
    ECB010068("10068", "集团成员客户授信期限不得大于集团客户的授信期限!"),
    ECB010069("10069", "集团授信申请数据查询异常!"),
    ECB010070("10070", "集团授信审批数据查询异常!"),
    ECB010071("10071", "集团授信批复数据查询异常!"),
    ECB010072("10072", "集团授信批复台账数据查询异常!"),
    ECB010073("10073", "集团授信批复变更数据查询异常!"),
    ECB010074("10074", "客户授信申请数据查询异常!"),
    ECB010075("10075", "未识别的其他事项审批类型!"),
    ECB010076("10076", "单一客户限额测算异常!"),
    ECB010077("10077", "单一客户债项评级测算异常!"),
    ECB010078("10078", "查询额度系统额度品种对应产品为空!"),
    ECB010079("10079", "业务申请流程处理异常-申请主表不存在！"),
    ECB010080("10080", "业务申请流程处理异常-辅助信息表不存在！"),
    ECB010081("10081", "业务申请流程处理异常-还款信息表不存在！"),
    ECB010082("10082", "业务申请流程处理异常-落地合同主表异常！"),
    ECB010083("10083", "集团客户申请与成员客户关系数据查询异常!"),
    ECB010084("10084", "债项评级信息查询异常!"),
    ECB010085("10085", "该批复编号已存在在途的集团批复变更申请!"),
    ECB010086("10086", "该集团客户项下成员客户信息查询异常!"),
    ECB010087("10087", "已存在当前期数的还款计划，请不要重复添加！"),
    ECB010088("10088", "还款日期不可以小于当前日期,请重新选择！"),
    ECB010089("10089", "当前授信申请信息已不满足授信续作处理,请重新选择！"),
    ECB010090("10090", "当前集团授信申报的授信类型识别失败！"),
    ECB010091("10091", "当前集团授信申报存在子成员流程属于审批中，不允许删除！"),
    ECB010092("10092", "查询成员客户授信信息异常！"),
    ECB010093("10093", "该客户下已存在生效的台账信息,请重新选择！"),
    ECB010094("10094", "该批复编号已存在在途的单一客户批复变更申请!"),
    ECB010095("10095", "当前集团项下存在未审批通过的成员客户申报信息！"),
    ECB010096("10096", "授信申请期限为空！"),
    ECB010097("10097", "授信申请分项期限为空！"),
    ECB010098("10098", "当前数据不符合授信复审规则:每年对期限大于1年并且存在循环授信的分项的授信发起复审。请重新选择！"),
    ECB010099("10099", "集团调剂后的金额与原授信批复金额不一致！"),
    ECB010100("10100", "当前授信调查报告信息不完善！"),
    ECB010101("10101", "当前客户的信息中财务报表类型为空,请维护后再重试！"),
    ECB010102("10102", "获取财报参数配置中财报类型数据异常,请维护后再重试！"),
    ECB010103("10103", "授信调剂不在授信申报的登记调剂关系中！"),
    ECB019999("19999", "系统异常!"),
    ECB020000("20000", "当前授信复议不符合复议规则,请重新选择数据进行操作！"),
    ECB020001("20001", "该档案已添加!"),
    EDS990001("EDS990001", "数据库操作异常!"),
    ECB020002("20002", "已存在在途的产品授信申请!"),
    ECB020003("20003", "已存在在途的同业客户授信申请!"),
    ECB020004("20004", "核心接口调用失败!"),
    ECB020005("20005", "该批复向下存在有效的占用关系，不允许发起复议!"),
    ECB020006("20006", "集团成员客户授信敞口不得大于集团客户的授信敞口!"),
    ECB020007("20007", "查询该币种的汇率为空!"),
    ECB020008("20008", "已选客户不存在综合授信额度，不允许发起申请!"),
    ECB020009("20009", "该数据非当前客户最新一笔授信申请，无法执行该操作!"),
    ECB020010("20010", "当前客户名下不存在有生效的资产池授信批复!"),
    ECB020011("20011", "已选客户存在生效的资产池协议，不允许新增!"),
    ECB020012("20012", "未查询到有效的授信批复台账"),
    ECB020013("20013", "恢复额度失败!"),
    ECB020014("20014", "引入分项下抵质押物失败!"),
    ECB020015("20015", "已选抵质押物已存在,请勿重复引用!"),
    ECB020016("20016", "当前客户存在未到期的授信，请重新选择"),
    ECB020017("20017", "贴现协议签订同步票据系统失败！"),
    ECB020018("20018", "请先在面签基本信息菜单中维护面签时间！"),
    ECB020019("20019", "请先在面签基本信息菜单中为维护面签地址！"),
    ECB020020("20020", "产品类型属性在该担保方式下不适用！"),
    ECB020021("20021", "放款金额大于合同可用余额！"),
    ECB020022("20022", "该协议申请下没有保证金账户，请确认！"),
    ECB020023("20023", "该协议申请下没有结算账户，请确认！"),
    ECB020024("20024", "业务变更异常！"),
    ECB020025("20025", "合同不存在！"),
    ECB020026("20026", "该客户业务存在异常情况，请确认是否予以业务准入！"),
    ECB020027("20027", "请先维护客户信息中的婚姻情况！"),
    ECB020028("200256", "请确认是否存在已生效的优企贷、优农贷或惠享贷产品！"),
    ECB020029("20029", "引入用信条件落实情况异常，请确认关联的授信批复是否存在用信条件落实情况信息！"),
    ECB020030("20030", "在线查封查验通过，无需人工审核，自动完成审批！"),
    ECB020032("20032", "该合同下关联的抵押物有未入库的，请核对信息！"),
    ECB020031("20031", "查封查验未查询到，即将进入人工查封查验！"),
    ECB020033("20033", "存在查封冻结记录，无法放款！"),
    ECB020034("20034", "合同注销失败！"),
    ECB020035("20035", "该笔授信无法进行内评低准入例外审批！"),
    ECB020036("20036", "该笔授信已进行内评低准入例外审批！"),
    ECB020037("20037", "已存在生效保证金账户，请确认！"),
    ECB020038("20038", "该笔借据已做过两次小企业无还本续贷，不允许再做！"),
    ECB020039("20039", "该笔小企业无还本续贷没有关联借据，请确认！"),
    ECB020040("20040", "产品额度期限不可超过授信申请期限"),
    ECB020041("20041", "当前授信分项下存在未结清的业务,不可删除!"),
    ECB020042("20042", "当前授信分项适用产品下存在未结清的业务,不可删除!"),
    ECB020043("20043", "根据借据未找到对应的台账信息！"),
    ECB020044("20044", "该笔出账申请未引入借据！"),
    ECB020045("20045", "项目编号不允许为空"),
    ECB020046("20046", "该项目编号存在在途的授信申报流程，不可复议！"),
    ECB020047("20047", "授信分项中不存在该产品[{}]！"),
    ECB020048("20048", "该笔保证担保合同[{}]未关联保证人！"),
    ECB020049("20049", "当前分项品种额度超出分项额度,请重试！"),
    ECB020050("20050", "当前分项额度小于其项下的分项品种额度之和,请重试！"),
    ECB020051("20051", "当前分项额度小于其项下的单个分项品种额度,请重试！"),
    ECB020052("20052", "当前授信申请授信期限未填写，请完善后添加适应品种"),
    ECB020053("20053", "该合同下未获取到对应的担保合同信息！"),
    ECB020054("20054", "根据合同编号未查询到合同信息！"),
    ECB020055("20055", "合同下存在未结清的业务，不允许注销！"),
    ECB020056("20056", "该客户下存在在途业务，不允许新增！"),
    ECB020057("20057", "该授信申请只含预授信/低风险/委托贷款业务，无需进行评级！"),
    ECB020058("20058", "客户信息缺失！"),
    ECB020059("20059", "信贷系统人民币利率定价申请未维护！请维护！"),
    ECB020060("20060", "授权信息数据查询异常!"),
    ECB020061("20061", "额度状态校验失败!"),
    ECB020062("20062", "该笔合同申请未关联担保合同！请确认！"),
    ECB020063("20063", "担保合同信息未完善！请确认！"),
    ECB020064("20064", "合同影像审核申请新增异常！"),
    ECB020065("20065", "当前类型下授信期限不允许延长！"),
    ECB020067("20067", "请注意集团客户授信申请的期限需大于等于成员客户授信申请期限！"),
    ECB020068("20068", "通过产品额度编号查询产品额度信息异常！"),
    ECB020069("20069", "通过分项额度编号查询分项额度信息异常！"),
    ECB020070("20070", "根据受理编号查询受托支付登记簿数据异常！"),
    ECB020071("20071", "当前客户本行即期信用等级缺失！"),
    /**
     * 惠押押异常枚举
     */
    ECBHYY001("BA-BI-AV01-01","抵押物编号为空"),
    // AccLoanDefEnums
    ACC_LOAN_QUERT_EXCEPTION("BWF000001", "该笔合同未发生放款！"),

    // BizCorreManExceptionDefEnums
    BIZ_CORRE_MAN_SUCCESS("000000", "success"),//默认成功
    //新增模块
    BIZ_CORRE_MAN_ADD_PARAM_EXP("BCMADD000001", "参数异常！"),
    BIZ_CORRE_MAN_ADD_MAINEXISTS_EXP("BCMADD000002", "主办人员已存在！"),
    BIZ_CORRE_MAN_ADD_EXISTS_EXP("BCMADD000003", "办理人员已存在对应办理类型的记录！"),
    BIZ_CORRE_MAN_ADD_FAILED_EXP("BCMADD000004", "新增数据失败！"),
    //修改模块
    BIZ_CORRE_MAN_UPDATE_MAINEXISTS_EXP("BCMADD000003", "主办人员已存在！"),

    //    BizWorkflowDefEnums
    /**
     * 默认成功
     **/
    BIZWORKFLOW_SUCCESS_DEF("000000", "操作成功"),
    BIZWORKFLOW_EXCEPTION_DEF("999999", "操作异常"),
    BIZWORKFLOW_GETNEXTNODEINFONULL_EXCEPTION("BWF000001", "未获取到流程下一处理节点用户信息！"),
    // CommonDefEnums
    /**
     * 默认成功
     **/
    COMMON_SUCCESS_DEF("000000", "操作成功"),
    COMMON_EXCEPTION_DEF("999999", "操作异常"),

    //通用异常定义
    E_CLIENTRTNNULL_EXPCETION("C000001", "接口返回为空！"),

    //数据流异常定义
    E_GETMAPPING_SOURCELISTDATANULL_EXCEPTION("GETMAPPING000001", "源表数据集合为空！"),
    E_GETMAPPING_SOURCEDATANULL_EXCEPTION("GETMAPPING000002", "源表list集合中存在为null的数据！"),
    E_GETMAPPING_SOURCEMAPNULL_EXCEPTION("GETMAPPING000003", "转化后源表数据集合为空！"),
    E_GETMAPPING_EXCEPTION("GETMAPPING000004", "获取源表与目标表的映射关系为空！"),
    E_GETMAPPING_DISTMAPLISTNULL_EXCEPTION("GETMAPPING000005", "获取目标表数据list集合数据为空！"),
    E_GETMAPPING_DISTMAPNULL_EXCEPTION("GETMAPPING000006", "目标表list集合中存在为null的数据！"),
    E_GETMAPPING_GUARCONTNONULL_EXCEPTION("GETMAPPING000007", "获取list数据为空！"),

    //参数查询异常定义,
    E_GETCFGPARAM_PARAMS_EXPCETION("GC000001", "查询参数配置-入参为空！"),
    E_GETCFGPARAM_RTNPARAMSNULL_EXPCETION("GC000002", "查询参数配置-返回的参数配置为空！"),

    //常用获取用户的方法异常定义
    E_GETUSER_TOKENNULL_EXCEPTION("EGU000001", "获取当前用户信息异常！"),
    E_GETUSER_USERNULL_EXCEPTION("EGU000001", "获取当前用户信息为空！"),

    // CtlLoanContDefEnums 合同模块异常枚举类
    /**
     * 默认成功
     **/
    CTR_SUCCESS_DEF("000000", "操作成功"),
    CTR_EXCEPTION_DEF("999999", "操作异常"),
    LOG_OUT_EXCEPTION_PVP("OUT000001", "存在在途的出账业务，不可注销"),
    LOG_OUT_EXCEPTION_ACC("OUT000002", "存在未结清的台账业务，不可注销"),
    ON_RESIGN_EXCEPTION_PVP("ONRESIGN000001", "存在在途的出账业务，不可重签"),
    ON_RESIGN_EXCEPTION_ACC("ONRESIGN000002", "存在未结清的台账业务，不可重签"),
    CTR_ASPL_EXISTS_EXCEPTION("CEA000001", "该客户存在生效的资产池协议！"),

    /**查询异常**/
    /**
     * 额度关系数据为空提示消息
     **/
    CTR_QUERY_LMTRELNULL_MSG("000000", "额度关系数据不存在！"),
    E_CTR_QUERY_PARAMS_EXCEPTION("PVPQE0001", "查询数据异常！入参缺失！"),
    E_CTR_QUERY_CLIENTCALL_EXCEPTION("PVPQE0003", "调用额度查询接口异常！"),
    E_CTR_QUERY_CLIENTRETURNNULL_EXCEPTION("PVPQE0004", "未获取到额度接口返回的数据！"),
    CTR_CONT_INSUR_EXCEPTION("CCI00001", "更新合同公证、保险信息异常！"),
    // CtrLoanContDefEnums 合同模块异常枚举类
    /**
     * 默认成功
     **/
//        CTR_SUCCESS_DEF("000000","操作成功"),
//        CTR_EXCEPTION_DEF("999999","操作异常"),
//
//        /**查询异常**/
//        /**额度关系数据为空提示消息**/
//        CTR_QUERY_LMTRELNULL_MSG("000000","额度关系数据不存在！"),
//        E_CTR_QUERY_PARAMS_EXCEPTION("PVPQE0001","查询数据异常！入参缺失！"),
//        E_CTR_QUERY_CLIENTCALL_EXCEPTION("PVPQE0003","调用额度查询接口异常！"),
//        E_CTR_QUERY_CLIENTRETURNNULL_EXCEPTION("PVPQE0004","未获取到额度接口返回的数据！"),
//        CTR_CONT_INSUR_EXCEPTION("CCI00001","更新合同公证、保险信息异常！"),

    UPDATE_NULL_EXCEPTION("UPDATE0000001", "不存在该笔合同！"),
    GRT_NULL_EXCEPTION("GRT0000001", "数据获取异常！"),
    UPDATE_CTRLOANCONT_EXCEPTION("UPDATE0000002", "合同更新异常！"),
    UPDATE_IQPGUARBIZRELAPP_EXCEPTION("UPDATE0000003", "合同业务子表更新异常！"),
    GRT_CONT_RECALL_EXCEPTION("RECALL0000001", "已放款的合同不能撤回！"),
    GRT_CONT_LOGOUT_EXCEPTION("LOGOUT0000001", "未结清的合同不能注销！"),
    GRT_CONT_IQPBILLREL_EXCEPTION("LOGOUT0000001", "当前不存在业务与借据关系！"),
    GRT_CONT_SUSPEND_EXCEPTION("SUSPEND0000001", "已结清的合同不能中止！"),
    GRT_CONT_PRINT_EXCEPTION("PRINT0000001", "当前合同未打印，请先进行打印！"),
    GRT_IQP_CONT_SAVE_PARAMS_EXCEPTION("GRTIQPSAVE000001", "担保合同数据保存-参数缺失！"),
    GRT_IQP_CONT_SAVE_GETGUARAMT_EXCEPTION("GRTIQPSAVE000002", "担保合同数据保存-获取押品总价值异常！"),
    GRT_IQP_CONT_SAVE_GUARAMTSMALL_EXCEPTION("GRTIQPSAVE000003", "担保合同数据保存-担保合同金额超出押品总价值！请调整担保合同金额或增加押品数据！"),
    GRT_IQP_CONT_SAVE_SAVECONTDATA_EXCEPTION("GRTIQPSAVE000004", "担保合同数据保存-担保合同数据保存出现异常！"),
    GRT_IQP_CONT_SAVE_SAVERELDATA_EXCEPTION("GRTIQPSAVE000005", "担保合同数据保存-担保合同数据保存出现异常！"),

    // EvalClientExceptionDefEnums 接口估值模块异常定义
    EVAL_CLIENT_DEF_SUCCESS("000000", "操作成功"),
    EVAL_CLIENT_DEF_EXCEPTION("999999", "操作失败！"),
    EVAL_CLIENT_PARAMS_EXCEPTION("CLE00001", "入参为空！"),
    EVAL_CLIENT_CUSNULL_EXCEPTION("CLE00001", "未获取到信息！"),
    EVAL_HANDLE_EXCEPTION("CLE000008", "业务申请流程处理异常！"),
    // GrtGuarContRelDefEnums 押品与担保合同关系枚举类
    GGCR_DEFAULT_SUCCESS("000000", "操作成功！"),
    GGCR_DEFAULT_EXCEPTION("999999", "操作异常！"),
    GGCR_INSERTOUT_PARAMS_EXCEPTION("INSERTOUT000001", "外部接口新增落地-参数异常！"),
    GGCR_INSERTOUT_EXISTS_EXCEPTION("INSERTOUT000002", "外部接口新增落地-关系数据已存在！"),
    GGCR_INSERTOUT_INSERTFAIL_EXCEPTION("INSERTOUT000003", "外部接口新增落地-保存异常！"),
    GGCR_GETGUARAMT_PARAM_EXCEPTION("GETAMT000001", "获取押品总价值-参数异常！"),
    GGCR_GETGUARAMT_GUARNOTEXISTS_EXCEPTION("GETAMT000002", "押品关系数据不存在！"),
    GGCR_GETGUARAMT_GUARINTERNULL_EXCEPTION("GETAMT000003", "获取押品总金额接口返回为空！"),

    //GrtGuarExceptionDefEnums 担保合同模块异常定义
    /**
     * 默认成功标志位
     **/
    GRTGUAR_SUCCESS_DEF("000000", "操作成功"),
    /**
     * 默认异常标志位
     **/
    GRTGUAR_EXCEPTION_DEF("999999", "操作异常"),
    QUERT_GUARCONTNO_EXCEPTION("QUERT0000001", "担保合同查询异常！"),
    /**
     * 删除异常
     **/
    GRT_GUAR_CONT_NO_DELETE("GRTDEL0001", "删除失败！存在关联业务！"),
    /**
     * 注销异常
     **/
    GRT_GUAR_CONT_NO_ONLOGOUT("GRTDEL0001", "担保合同关联有未结清的贷款合同不能注销"),
    /**
     * 打印异常
     **/
    GRT_GUAR_ONPRINT_DELETE("ONPRINT0001", "打印异常！"),

    GRT_CHECK_IGBANOTEXISTS_EXCEPTION("GRTAMTCHECK000003", "担保与押品关系数据不存在！"),
    GRT_CONT_IMPORT_NOGUAR_EXCEPTION("GRTIMP000001", "担保合同重置引入失败"),
    GRT_CONT_SAVE_NOGUAR_EXCEPTION("GRTSAVE000001", "合同编号为空"),
    GRT_CONT_IQPGUARBIZ_EXCEPTION("IQPQUERY000001", "存在合同与业务关联关系"),
    GRT_CONT_GUARAMT_EXCEPTION("GRTGUARAMT00001", "本次担保总金额大于合同敞口金额"),
    GRT_CONT_UPDATE_EXCEPTION("GRTUPDATE00001", "担保合同更新失败"),
    GRT_CONT_THISGUARAMT_EXCEPTION("GRTTHISGUARAMT00001", "获取本次担保变更项下的本次担保总金额失败"),

    /**
     * 担保变更异常
     **/
    GRTDE_IQPGUARBIZRELAPP_EXCEPTION("GRTDEL0003", "担保变更-更新业务子表异常！"),
    GRTDE_GRTGUARCHGAPP_EXCEPTION("GRTDEL0004", "存在在途的担保变更申请！"),
    AFTEREND_GRTLOANCONT_EXCEPTION("AFTERENDE000001", "审批通过更新合同异常-合同主表不存在！"),
    AFTEREND_GRTGUARCHGAPP_EXCEPTION("AFTERENDE000002", "审批通过更新合同异常-担保变更主表不存在！"),
    AFTEREND_IQPGUARBIZRELAPP_EXCEPTION("AFTERENDE000003", "审批通过更新合同异常-业务子表不存在！"),
    AFTEREND_INSETRBIZRSTREL_EXCEPTION("AFTERENDE000004", "审批通过更新合同异常_更新业务结果表异常！"),
    AFTEREND_UPDATEIQPSTATUS_EXCEPTION("AFTERENDE000005", "审批通过更新合同异常-更新担保变更申请状态异常！"),
    /**
     * 业务申请审批
     **/
    AFTEREND_IQPLOANAPPAPPRMODE_EXCEPTION("AFTERENDE000006", "业务申请审批-审批模式主表不存在！"),
    UPDATE_IQPLOANAPPAPPRMODE_EXCEPTION("UPDATE000003", "业务申请审批-审批模式更新异常！"),

    // GuarExceptionDefEnums
    E_GUAR_TYPE_STATE_INVALID("GUARE0001", "无效的担保分类状态"),
    E_GUAR_TYPE_NULL_INVALID("GUARE0002", "无效的担保分类"),
    E_GUAR_BASE_CHECK_DELETE("GUARE0003", "当前押品存在业务/授信/借据相关联的数据，不允许删除。"),
    E_GUAR_BASECOUNT_CHECK_DELETE("GUARE0003", "当前担保分类存在押品，不允许删除。"),
    E_GUAR_UPDATE_FAIL("GUARE0004", "更新失败，更新条数不为1；更新条数："),
//        E_CLIENTRTNNULL_EXPCETION("C000001","接口返回为空！"),
//        GRT_CHECK_IGBANOTEXISTS_EXCEPTION("GRTAMTCHECK000003","担保与押品关系数据不存在！"),

    E_CERTICATALOGNULL_EXPCETION("GUARE0005", "未配置适用权证！"),

    /**
     * 张家港新增异常码
     **/
    E_GUAR_SUCCESS_EXPCETION("ECG060000", "成功！"),
    E_GUAR_PARAM_EXPCETION("ECG060001", "传入参数为空"),
    E_GUAR_INSERT_EXPCETION("ECG060020", "SQL数据库插入失败！"),
    E_GUAR_SELECT_GUARWAR_EXPCETION("ECG060021", "SQL查询担保权证信息失败！"),
    E_GUAR_SELECT_GUARBASE_EXPCETION("ECG060022", "SQL查询押品基本信息失败！"),
    E_GUAR_WARRANT_INWAY_EXPCETION("ECG060030", "押品已存在在途申请！"),

    /********抵押登记申请异常 GuarRegisterAppEnmus********************/
    /**
     * 默认成功
     **/
    GUAR_REG_APP_SUCCESS_DEF("000000", "操作成功"),
    GUAR_REG_APP_EXCEPTION_DEF("999999", "操作异常！"),

    /**
     * 修改异常
     **/
    E_GUAR_REG_APP_UPDATE_PARAM_FAILED("GUAREG000001", "修改抵押登记申请参数缺失！"),
    E_GUAR_REG_APP_UPDATE_ASSIST_FAILED("GUAREG000002", "修改抵押登记申请-落地辅助信息失败！"),
    E_GUAR_REG_APP_UPDATE_AFTER_FAILED("GUAREG000003", "修改抵押登记申请-抵押登记申请表后信息失败！"),
    E_GUAR_REG_APP_UPDATE_FAILED("GUAREG000004", "修改抵押登记申请-落地申请主表信息失败！"),

    //IqpCeriInoutAppDefEnmus
    IQP_CERI_GETSERNO_EXCEPTION("GETO000001", "获取权证流水号异常！"),
    IQP_CERI_GETGUARNO_EXCEPTION("GETO000002", "获取权证押品编号异常！"),
    IQP_CERI_INSERT_EXCEPTION("SELECTO000001", "权证信息不存在！"),
    IQP_CERI_UPDATRE_EXCEPTION("UPDATEO000001", "更新权证出入库异常！"),
    IQP_CERI_SAVE_EXCEPTION("SAVEO000001", "保存失败！"),
    IQP_CERI_GETGUARCERTI_EXCEPTION("GRTO000003", "获取权证主键异常！"),
    GUAR_CERTI_RELA_SELECTCERTICATALOG_EXCEPTION("SELECTO000002", "权证待入库申请押品项下无他项权证，！"),
    /**
     * 流程审批异常
     **/
    AFTEREND_IQPCERTIINOUTREL_EXCEPTION("AFTERENDE000001", "审批通过更新权证出入库-获取权证主键异常！"),
    AFTEREND_IQPCERTIINOUTAPP_EXCEPTION("AFTERENDE000002", "审批通过更新权证出入库-更新权证申请状态异常！"),
    AFTEREND_GUARCERTIRELA_EXCEPTION("AFTERENDE000003", "审批通过更新权证出入库-权证出入库信息表不存在！"),
    AFTEREND_IQPCERTIINOUTAPP_INOUT_APPTYPE_EXCEPTION("AFTERENDE000004", "审批通过更新权证出入库-未知操作!"),
    GUAR_GUARCERTIRELA_EXCEPTION("AFTERENDE000005", "审批通过更新权证出入库-更新担保权证异常！"),
    GUAR_NULL_EXCEPTION("AFTERENDE000005", "接口返回异常！"),
    /**
     * 异常编码
     **/
    GET_CERTI_STATU_EXCEPTION("GETO000003", "请选择未记账的权证申请！"),
    /**
     * 变更申请业务模块异常定义 IqpChgDefEnums
     */

    /**
     * 默认成功
     **/
    IQP_CHG_SUCCESS_DEF("000000", "操作成功"),
    IQP_CHG_EXCEPTION_DEF("999999", "操作异常！"),

    /**
     * 新增异常
     **/
    E_IQP_CHG_INSERT_PARAM_FILED("IQPCHGIE0001", "新增数据失败！参数异常！"),
    E_IQP_CHG_INSERT_EXISTS("IQPCHGIE0002", "新增数据失败！该数据已存在！"),
    E_IQP_CHG_INSERT_EXISTS_BILL("IQPCHGIE0003", "新增数据失败！该借据号已存在停息/恢复计息申请！"),
    E_IQP_CHG_INSERT_CHECK_ACC_LOAN_FAIL("IQPCHGIC0001", "新增数据检查失败！借据信息异常！"),

    /**
     * 更新失败
     **/
    E_IQP_CHG_UPDATE_ERROR("IQPCHGUE001", "保存数据失败!"),

    /**
     * 生成主键异常
     **/
    E_IQP_CHG_GEN_PK_ERROR("IQPCHGGPK001", "生成流水号异常!"),

    E_GETUSER_EXCEPTION("IQPCHGE0004", "获取当前登录用户为空！"),
    /**
     * 通用的错误-映射错误
     **/
//    E_GETMAPPING_EXCEPTION("AFTERENDE000008", "获取源表与目标表的映射关系为空！"),

    /**
     * 校验在途的变更申请业务异常
     **/
    IQP_RATE_CHG_CHECK_EXCEPTION_1("IQPCHGCE0001", "该借据正在进行利率调整业务！"),
    IQP_TEAM_CHG_CHECK_EXCEPTION_2("IQPCHGCE0002", "该借据正在进行期限信息调整业务！"),
    IQP_REPAY_WAY_CHG_CHECK_EXCEPTION_3("IQPCHGCE0003", "该借据正在进行还款方式变更申请业务！"),
    IQP_REPAY_TERM_CHG_CHECK_EXCEPTION_4("IQPCHGCE0004", "该借据正在进行还款间隔周期变更申请业务！"),
    IQP_REPAY_DATE_CHG_CHECK_EXCEPTION_5("IQPCHGCE0005", "该借据正在进行还款日变更申请业务！"),
    IQP_REPAY_INTEREST_CHG_CHECK_EXCEPTION_6("IQPCHGCE0006", "该借据正在进行利息减免变更申请业务！"),


    IQP_BILL_ACCT_CHG_CHECK_EXCEPTION_7("IQPCHGCE0007", "该借据正在进行借据账号变更申请业务！"),
    IQP_ACCT_CHG_CONT_CHECK_EXCEPTION_8("IQPCHGCE0008", "该借据正在进行合同账号变更申请业务！"),
    IQP_ACCT_CHG_CONT_CHECK_EXCEPTION_9("IQPCHGCE0009", "该合同正在进行合同账号变更申请业务！"),
    IQP_ACCT_CHG_CONT_CHECK_EXCEPTION_10("IQPCHGCE0010", "该合同项下借据正在进行借据账号变更申请业务！"),
    /**
     * 校验借据信息
     **/
    CHECK_LOAN_IS_SURV("CHECKLOANISSURV0001", "借据信息不存在！"),
    IQP_BILL_ACCT_CHG_ADD_EXCEPTION_1("IQPBILLACCTADD0001", "新增借据账号变更业务失败！"),
    IQP_BILL_ACCT_CHG_ADD_EXCEPTION_2("IQPBILLACCTADD0002", "该借据项下没有还款账号信息可变更！"),
    IQP_ACCT_CHG_CONT_ADD_EXCEPTION_3("IQPACCTCONTADD0003", "新增合同账号变更业务失败！"),
    IQP_ACCT_CHG_CONT_ADD_EXCEPTION_4("IQPACCTCONTADD0004", "该合同项下没有账号信息可变更！"),

    /**
     * 更新借据信息
     **/
    IQP_REPAY_UPDATE_EXCEPTION_1("IQPRUE0001", "更新贷款台账还款方式信息异常！"),
    IQP_REPAY_UPDATE_EXCEPTION_2("IQPRUE0002", "更新贷款台账还款间隔周期信息异常！"),

    /**
     * 更新呆账核销申请信息失败
     **/
    IQP_WRITEOFF_APP_UPDATE_EXCEPTION_1("IQPWOAUE0001", "更新呆账核销申请信息汇总金额失败！"),


    /**
     * 单笔单批、额度项下、特殊申请模块异常定义 IqpExceptionDefEnums
     */
    /**
     * 默认成功
     **/
    IQP_SUCCESS_DEF("000000", "操作成功"),
    IQP_EXCEPTION_DEF("999999", "操作异常！"),
    /**
     * 新增异常
     **/
    //新增时异常定义
    E_IQP_INSERT_PARAMSNULL_EXCEPTION("IQPIE0006", "未获取到申请信息！"),
    E_IQP_INSERT_INSERTAPPFAILED_EXCEPTION("IQPIE0007", "新增主表数据异常！"),
    E_IQP_INSERT_PARAM_FAILED("IQPIE0001", "新增数据失败！参数异常！"),
    E_IQP_EXISTS("IQPIE0002", "新增数据失败！该数据已存在"),
    E_IQP_INSERT_FAILED("IQPIE0003", "新增数据落地失败！"),
    E_IQP_INSERT_BIZMANA_FAILED("IQPIE0004", "新增主办人员数据异常！"),
    E_IQP_INSERT_GERUSER_FAILED("IQPIE0005", "获取当前登录用户信息异常"),
    E_IQP_INSERT_ASPLAPP_FAILED("IQPIE0006", "新增数据失败！已选客户不允许新增！"),
    E_CTR_ASPLDETAILS_FAILED("IQPIE0007", "生成资产池协议失败！"),
    E_CTR_ASPLDETAILSCHANGE_FAILED("IQPIE0007", "资产池协议变更失败！"),
    E_CTR_ASPLDETAILSCHANGE_NOT_EXISTS("IQPIE0008", "新增数据失败！已选协议不允许变更！"),


    /**
     * 查询异常
     **/
    E_IQP_NOT_EXISTS_FAILED("IQPQE0001", "查询的数据不存在！"),
    E_IQP_BILL_NOT_EXISTS_FAILED("IQPQE0002", "借据数据不存在！"),
    E_IQP_LMT_NOT_EXISTS_FAILED("IQPQE0003", "授信数据不存在！"),
    E_IQP_LMT_QUERY_FAILED("IQPQE0004", "授信额度台账查询失败！"),
    E_IQP_TH_QUERY_FAILED("IQPQE000005", "第三方额度信息查询失败！"),

    /**
     * 删除异常
     **/
    E_IQP_DELETE_PARAM_FAILED("IQPDE000001", "删除额度项下业务参数缺失！"),
    E_IQP_DELETE_FAILED("IQPDE000009", "删除额度项下业务申请失败！"),

    /**
     * 修改异常
     **/
    E_IQP_UPDATE_PARAM_FAILED("IQPUE000001", "修改额度项下申请参数缺失！"),
    E_IQP_UPDATE_ASSIST_FAILED("IQPUE000002", "修改额度项下申请-落地辅助信息失败！"),
    E_IQP_UPDATE_REPAY_FAILED("IQPUE000003", "修改额度项下申请-落地还款信息失败！"),
    E_IQP_UPDATE_FAILED("IQPUE000004", "修改额度项下申请-落地申请主表信息失败！"),
    E_IQP_CHANGE_CTRASPLDETAILS_FAILED("IQPUE000005", "资产池协议变更失败！"),

    /**
     * 特殊业务申请模块报错信息
     **/
    SPECIAL_IQP_BILLPARAMS_EXCEPTION("SIQPBILL000001", "特殊业务申请-借据关系数据保存参数异常！"),
    SPECIAL_IQP_BILLEXISTS_EXCEPTION("SIQPBILL000002", "特殊业务申请-借据关系数据已存在！"),
    SPECIAL_IQP_BILLSAVE_EXCEPTION("SIQPBILL000003", "特殊业务申请-保存业务关系数据失败！！"),

    /**
     * 申请金额与担保金额 报错信息
     **/
    APPAMT_CHECK_PARAM_EXCEPTION("APPAMTCHECK000001", "校验申请金额信息-参数异常！"),
    APPAMT_CHECK_APPNOTEXISTS_EXCEPTION("APPAMTCHECK000002", "校验申请金额信息-申请主表不存在！"),
    APPAMT_CHECK_IGBANOTEXISTS_EXCEPTION("APPAMTCHECK000003", "业务与担保关系数据不存在！"),
    APPAMT_CHECK_GETTOTALAMTNULL_EXCEPTION("APPAMTCHECK000004", "获取业务下总担保金额异常！"),
    APPAMT_CHECK_GUARAMTSMALL_EXCEPTION("APPAMTCHECK000005", "引入押品的总价值无法覆盖担保合同担保金额！"),

    /**
     * 账户信息校验
     **/
    E_IQP_ACCT_ERROR1("ACCTERROR01", "已确认支付方式 自主支付,没有添加 放款账户信息"),
    E_IQP_ACCT_ERROR2("ACCTERROR02", "已确认支付方式 受托支付,没有添加 受托支付账户信息"),
    /**
     * 第三方额度校验
     **/
    THAMT_CHECK_PARAM_EXCEPTION("THAMTCHECK000001", "第三方额度信息校验-参数异常！"),
    THAMT_CHECK_RESULT_EXCEPTION("THAMTCHECK000002", "第三方额度信息校验-未获取到校验结果！"),

    THAMT_CHECK_CIPARAM_EXCEPTION("THAMTCICHECK000001", "校验合作方额度-参数缺失！"),
    THAMT_CHECK_CILMT_EXCEPTION("THAMTCICHECK000002", "该笔合作方额度的授信额度已用完！"),
    THAMT_CHECK_CISIG_EXCEPTION("THAMTCICHECK000003", "该客户申请已超该笔合作方额度的单户限额！"),
    THAMT_CHECK_CULMT_EXCEPTION("THAMTCICHECK000004", "该客户申请已超该笔合作方额度的授信额度！"),

    THAMT_CHECK_FIPARAM_EXCEPTION("THAMTFICHECK000001", "校验融资担保额度-参数缺失！"),
    THAMT_CHECK_FISINGLE_EXCEPTION("THAMTFICHECK000002", "该客户申请已超该笔融资担保额度的单户限额！"),
    THAMT_CHECK_FIGUARTOTLSPAC_EXCEPTION("THAMTFICHECK000003", "该笔融资担保额度的担保总敞口限额已用完！"),
    THAMT_CHECK_FITOTLAMT_EXCEPTION("THAMTFICHECK000004", "该笔融资担保额度的产品总限额已用完！"),
    THAMT_CHECK_FISIGAMT_EXCEPTION("THAMTFICHECK000005", "该客户申请已超该笔融资担保额度的产品单户限额！"),
    THAMT_CHECK_FIONE_EXCEPTION("THAMTFICHECK000006", "本次申请的申请金额超过该笔融资担保额度的产品单次限额！"),


    /**
     * 流程业务处理异常定义
     **/
    E_IQP_PARAMS_EXCEPTION("IQPWH0001", "业务申请流程处理异常-参数缺失！"),
    E_IQP_IQPNOTEIXSTS_EXCEPTION("IQPWH000003", "业务申请流程处理异常-申请主表不存在！"),
    E_IQP_IQPASSISTNOTEIXSTS_EXCEPTION("IQPWH000003", "业务申请流程处理异常-辅助信息表不存在！"),
    E_IQP_IQPREPAYNOTEIXSTS_EXCEPTION("IQPWHE000004", "业务申请流程处理异常-还款信息表不存在！"),
    E_IQP_CTRLOANCONTINSERT_EXCEPTION("IQPWH000005", "业务申请流程处理异常-落地合同主表异常！"),
    E_IQP_HANDLESUBLIST_EXCEPTION("IQPWH000006", "业务申请流程处理异常-处理业务子表异常！业务子表"),
    E_IQP_UPDATEIQPSTATUS_EXCEPTION("IQPWH000007", "业务申请流程处理异常-更新业务主表申请状态异常！"),
    E_IQP_HANDLE_EXCEPTION("IQPWH000008", "业务申请流程处理异常！"),
    E_IQP_UPDWFSTATUS_EXCEPTION("IQPWH000009", "业务申请流程业务处理异常-更新全流程状态异常！"),
    E_IQP_UPDIQPGUARREL_EXCEPTION("IQPWH000010", "业务申请流程业务处理异常-更新担保合同关系异常！"),
    E_IQP_ORIGICTRLOANCONT_EXCEPTION("IQPWH0000011", "业务申请流程处理异常-续签合同时，更新原合同异常！"),

    /**
     * 业务校验异常定义
     **/
    E_IQP_BIZDATACHECK_PARAMS_EXCEPTION("BIZDC000001", "业务校验-参数异常！"),
    E_IQP_BIZDATACHECK_GUARRELNULL_EXCEPTION("BIZDC000002", "业务校验-未获取到担保合同关系数据！"),
    E_IQP_BIZDATACHECK_GUARAMTSMALL_EXCEPTION("BIZDC000003", "担保金额无法覆盖申请金额！"),
    E_IQP_BIZDATACHECK_GUARCHECK_EXCEPTION("BIZDC000004", "校验申请金额与担保金额异常！"),
    E_IQP_BIZDATACHECK_CUSBLACKCHECKTRUE("BIZDC000005", "该客户为不宜贷款户！"),
    E_IQP_BIZDATACHECK_CUSBLACKCALLNULL_EXCEPTION("BIZDC000006", "校验不宜贷款户调用接口失败！"),
    E_IQP_BIZDATACHECK_CUSBLACKCHECK_EXCEPTION("BIZDC000007", "校验不宜贷款户异常！"),
    E_IQP_BIZDATACHECK_APPTAPPLNULL_EXCEPTION("BIZDC000008", "未获取到共同申请人数据！"),
    E_IQP_BIZDATACHECK_RISKOPENAMTNULL_EXCEPTION("BIZDC000009", "风险敞口金额为空！"),
    E_IQP_BIZDATACHECK_SECURITYAMTNULL_EXCEPTION("BIZDC000010", "保证金金额为空！"),
    E_IQP_BIZDATACHECK_COMBAILNULL_EXCEPTION("BIZDC000011", "未获取到保证金列表数据"),
    E_IQP_BIZDATACHECK_COMBAILSMALL_EXCEPTION("BIZDC000012", "保证金列表数据无法覆盖保证金金额"),
    E_IQP_BIZDATACHECK_IQPACCTNULL_EXCEPTION("BIZDC000013", "未获取到账户列表数据！"),
    E_IQP_BIZDATACHECK_IQPPVPACCTNULL_EXCEPTION("BIZDC000014", "放款账户为空或放款账户数大于1！"),
    E_IQP_BIZDATACHECK_IQPREPAYACCTNULL_EXCEPTION("BIZDC000015", "还款账户为空或还款账户数大于3！"),
    E_IQP_BIZDATACHECK_IQPEPACCTNULL_EXCEPTION("BIZDC000016", "账户列表受托账户为空！"),
    E_IQP_BIZDATACHECK_BILLLISTNULL_EXCEPTION("BIZDC000017", "借据列表数据为空！"),
    E_IQP_BIZDATACHECK_BILLAMTSMALL_EXCEPTION("BIZDC000018", "申请金额超出借据总余额！"),
    E_IQP_BIZDATACHECK_LMTCHECKPARAMS_EXCEPTION("BIZDC000019", "授信额度校验参数缺失！"),
    E_IQP_BIZDATACHECK_LMTAVAIBLEAMTZERO_EXCEPTION("BIZDC000020", "授信可提用额度为零！"),
    E_IQP_BIZDATACHECK_LMTAVAIBLEAMTSMALL_EXCEPTION("BIZDC000021", "申请金额超出授信可提用金额！"),
    E_IQP_BIZDATACHECK_GETLMTQUERYNULL_EXCEPTION("BIZDC000022", "未获取到授信接口返回数据！"),
    E_IQP_BIZDATACHECK_GETLMTCTRNULL_EXCEPTION("BIZDC000023", "授信协议数据为空！"),
    E_IQP_BIZDATACHECK_MORELMTTOTLAMT_EXCEPTION("BIZDC000024", "本次申请金额加上授信已用额度超出授信协议总额度！"),
    E_IQP_BIZDATACHECK_MORELMTREOLVAMT_EXCEPTION("BIZDC000025", "本次申请金额加上授信已用额度超出授信协议循环额度！"),
    E_IQP_BIZDATACHECK_MORELMTONEAMT_EXCEPTION("BIZDC000026", "本次申请金额加上授信已用额度超出授信协议一次性额度！"),
    E_IQP_BIZDATACHECK_GUARWAY_EXCEPTION("BIZDC000027", "担保合同担保方式无法覆盖业务申请主担保方式！"),
    E_IQP_BIZDATACHECK_LMTACCLIMITTYPE_EXCEPTION("BIZDC000028", "额度分项额度类型为空！"),

    /**
     * 权证出入库错误枚举类 IqpGBRAExceptionDefEnums
     */
    IQP_GBRA_SUCCESS("000000", "success"),
    IQP_GBRA_IMPORT_PARAM_EXCEPTION("IMPORT000001", "引入担保合同-参数异常！"),
    IQP_GBRA_IMPORT_EXISTS_EXCEPTION("IMPORT000002", "引入担保合同-担保合同已存在！"),
    IQP_GBRA_IMPORT_EXISTSOTHER_EXCEPTION("IMPORT000003", "引入担保合同-存在有效的关系数据！"),
    IQP_GBRA_IMPORT_ZEROAMT_EXCEPTION("IMPORT000004", "引入担保合同-可用金额为零！"),

    IQP_GBRA_GETAMT_PARAM_EXCEPTION("GETAMT000001", "获取担保合同额度信息-参数异常！"),

    IQP_GBRA_DEL_PARAM_EXCEPTION("DEL000001", "解除担保合同关系-参数异常！"),
    IQP_GBRA_DEL_FAILED("DEL000002", "解除担保合同关系-解除失败！"),

    IQP_GBRA_UPDATE_PARAM_EXCEPTION("UPDATE0000001", "参数异常！"),
    IQP_GBRA_UPDATE_APPNOTEXISTS_EXCEPTION("UPDATE0000002", "申请主表不存在！"),
    IQP_GBRA_UPDATE_APPTERM_EXCEPTION("UPDATE0000003", "获取申请期限内容异常！"),
    IQP_GBRA_UPDATE_APPTERMNOTCOVER_EXCEPTION("UPDATE0000004", "担保期限无法覆盖申请期限！"),
    IQP_GBRA_UPDATE_IMPORTADD_EXCEPTION("UPDATE0000005", "引入担保合同-新增关系异常！"),
    IQP_GBRA_UPDATE_IMPORTUPDATE_EXCEPTION("UPDATE0000006", "引入担保合同-更新关系异常！"),
    IQP_GBRA_UPDATE_GRTCONTADD_EXCEPTION("UPDATE0000007", "引入担保合同-更新关系异常！"),
    IQP_GBRA_UPDATE_CONT_FAILED("UPDATE0000008", "更新合同数据异常！"),
    IQP_GBRA_UPDATE_REL_FAILED("UPDATE0000009", "更新合同关系数据异常！"),
    IQP_GBRA_UPDATE_GETAPPTERM_EXCEPTION("UPDATE0000010", "计算额度分项到期日异常，请检查数据录入是否完整！"),
    IQP_GBRA_IMPORT_NOGUAR_EXCEPTION("IMPORT000004", "引入合同-参数异常！"),
    IQP_GBRA_IMPORT_WARRANT_EXISTS_EXCEPTION("IMPORT000002", "该权证编号已存在！"),
    IQP_GBRA_REL_WARRANT_EXISTS_EXCEPTION("IMPORT000005", "该权证编号对应的押品未查询到生效的担保合同！"),
    // LmtCoopAppExceptionDefEnums 合作方额度申请异常常量类
    /**
     * 删除记录参数异常
     **/
    DELETE_PRARM_NULL("LCAD000009", "请求参数缺失！"),

    /**
     * 合作方申请关联信息逻辑删除
     **/
    LMT_COOP_APP_DELETE_FAILED("LCAD000001", "合作方申请信息逻辑删除失败！"),
    LMT_BUILDING_PRO_DELETE_FAILED("LBPD000001", "楼盘项目信息逻辑删除失败！"),
    LMT_MACHINE_EQUIP_DELETE_FAILED("LMED000001", "机械设备信息逻辑删除失败！"),
    LMT_CAR_PRO_DELETE_FAILED("LCPD000001", "汽车项目信息逻辑删除失败！"),

    LMT_COOP_PLAN_APP("LCPD000001", "该合作方有在途的合作协议申请！"),
    COOP_COLONY_WHITE_LST("CCWLD000001", "该合作方案编号下客户已经存在！"),
    LMT_COOP_PLAN_APP_ALTER("LCPD000002", "该合作方有在途的合作准入申请！"),
    LMT_COOP_PLAN_APP_APPLY("LCPD000003", "该合作方有在途的合作准入变更申请！"),
    LMT_COOP_PLAN_APP_ORG("LCPD000004", "未添加合作方适用机构信息！"),
    LMT_COOP_PLAN_APP_ESPEC("LCPD000005", "未添加合作方特殊限额信息！！"),
    LMT_COOP_PLAN_APP_PRO("LCPD000006", "未添加合作方项目信息！！"),
    LMT_COOP_PLAN_APP_PTI("LCPD000007", "未添加合作方资质信息！！"),
    LMT_COOP_PLAN_APP_BG1("LCPD000008", "合作方案变更未查询到原合作方案台账信息！"),
    LMT_COOP_PLAN_APP_BG2("LCPD000009", "合作方案变更查询到多条合作方案台账信息！"),
    LMT_COOP_PLAN_APP_BG3("LCPD000010", "合作方案变更未查询到原合作方案批复信息！"),
    /**
     * 合作方协议生成异常
     **/
    LMT_COOP_APP_CTR_FAILED("LCAC000001", "合作方协议生成失败！"),
    LMT_COOP_APP_CTR_FAILED_02("LCAC000002", "合作方协议生成失败，未获取到合作方案信息！"),
    // LmtFinGuarAppExceptionDefEnums
    /**
     * 担保公司协议生成异常
     **/
    LMT_FIN_GUAR_TO_CTR_FAILED("LFGTC000001", "担保公司协议生成异常！"),
    //LmtFinSpAppDefEnums {
    IS_EXIST_LMT_FIN_SP_APP_RECORD("LFSAEXCEPTION001", "该融资协议编号项下已存在进入流程的担保公司授信代偿申请！"),
    //    LmtFrUrfAppDefEnums {
    GET_LMT_FR_URF_APP_EXCEPTION("GET000001", "该授信协议项下有效的额度台账在途中"),
    GET_LMT_FR_URF_APP_SERNO_EXCEPTION("GET000002", "额度冻结解冻申请主表数据不存在！"),

    LMT_FR_URF_APP_REL_EXCEPTION("LRUAR000001", "当前该笔额度台账已引用！"),
    LMT_FR_URF_APP_REL_AFTEREND_EXCEPTION("LRUAR000002", "当前该笔额度台账流程审批中无法引入！"),

    LMT_FR_URF_APP_APPTYPE_UNFROZE_EXCEPTION("LRUAR000003", "该笔台账已解冻无法引入！"),

    LMT_FR_URF_APP_APPTYPE_FROZE_EXCEPTION("LRUAR000004", "该笔台账已冻结无法引入！"),

    LMT_RE_ACC_OPER_APP_FRO_EXISTS_EXCEPTION("LRAOAF000001", "该数据已存在冻结申请中的数据无法进行同类操作！"),
    LMT_RE_ACC_OPER_APP_THAW_EXISTS_EXCEPTION("LRAOAF000002", "该数据已存在解冻申请中的数据无法进行同类操作！"),
    LMT_RE_ACC_OPER_APP_TERM_EXISTS_EXCEPTION("LRAOAF000003", "该数据已存在终止申请中的数据无法进行同类操作！"),
    LMT_RE_ACC_OPER_APP_TYPE_EXCEPTION_4("LRAOAF000004", "获取当前额度数据操作状态异常,请确认操作类型！"),

    LMT_FR_URF_APP_GETLMTLISTNULL_EXCEPTION("IRUA000001", "获取额度台账集合为空！"),
    LMT_FR_URF_APP_REL_NOTIMPROT_EXCEPTION("LRUAR000002", "当前该笔授信协议项下未引用额度台账！"),

    UPDATE_LMT_FR_URF_APP_REL_EXCEPTION("UPDATE000001", "更新额度冻结/解冻关系表异常！"),
    UPDATE_LMT_FR_URF_APP_EXCEPTION("UPDATE000002", "新增额度冻结/解冻异常！"),

    LMT_FROZE_AMT_ADD_EXCEPTION("LRAA000001", "额度冻结额度台账冻结解冻金额异常！"),
    LMT_FROZE_AMT_COMPARETO_EXCEPTION("LRAA000001", "当前授信解冻申请本次解冻金额大于授信已冻金额！"),

    AFTEREND_LMTFRURFAPP_EXCEPTION("AFTERENDE000001", "审批通过更新额度冻结解冻申请主表异常-主表不存在！"),
    //    AFTEREND_UPDATEIQPSTATUS_EXCEPTION("AFTERENDE000002", "审批通过更新额度冻结解冻申请主表异常-更新冻结解冻申请主表申请状态异常！"),
    E_LMT_FR_URF_APP_EXCEPTION("IQPWH000008", "流程处理异常！"),

    LMTACC_CLIENT_PARAMS_EXCEPTION("LCLC000001", "获取额度协议接口参数异常！"),

    /**
     * 个人额度申请-信息提示枚举类
     * 个人额度申请
     * 额度分项
     * LmtIndivAppDefEnum
     */
    //默认成功
    LIA_DEF_SUCCESS("000000", "操作成功"),
    //默认异常
    LIA_DEF_EXCEPTION("999999", "操作异常！"),

    //新增时异常定义
    LIA_INSERT_PARAMSNULL_EXCEPTION("I000001", "未获取到申请信息！"),
    LIA_INSERT_PARAMSMISS_EXCEPTION("I000002", "申请信息缺失！"),
    LIA_INSERT_INSERTAPPFAILED_EXCEPTION("I000003", "新增主表数据异常！"),
    LIA_INSERT_INSERTBIZCOREFAILED_EXCEPTION("I000004", "新增主办人数据异常！"),
    LIA_INSERT_GETLMTACCLISTCLIENTNULL_EXCEPTION("I000005", "获取额度台账集合接口返回对象为空！"),
    LIA_INSERT_GETLMTLISTNULL_EXCEPTION("I000006", "获取额度台账集合为空！"),
    LIA_INSERT_TRANSLMTLISTNULL_EXCEPTION("I000007", "转化额度台账集合为空！"),
    LIA_INSERT_TRANSRTNLISTNULL_EXCEPTION("I000008", "转化后返回集合为空！"),
    LIA_INSERT_INSERTLMTSUB_EXCEPTION("I000009", "批量添加额度分项数据异常！"),
    LIA_INSERT_GRTGUARLISTNULL_EXCEPTION("I000010", "获取担保合同关系结果表数据为空！"),
    LIA_INSERT_INSERTIQPGUAR_EXCEPTION("I000010", "保存业务与担保合同关系表数据为空！"),

    //是否存在在途的额度申请/生效的额度协议校验异常定义
    LEC_PARAMS_EXCEPTION("LEC000001", "在途额度申请/生效额度协议校验-参数异常！"),
    LEC_EXISTSAPP_EXCEPTION("LEC000002", "当前客户存在在途的授信申请！"),
    LEC_CLIENTNULL_EXCEPTION("LEC000003", "在途额度申请/生效额度协议校验-接口调用失败！"),
    LEC_CLIENTRTNNULL_EXCEPTION("LEC000004", "在途额度申请/生效额度协议校验-接口返回为空！"),
    LEC_EXISTSCTR_EXCEPTION("LEC000005", "当前客户已存在有效授信"),
    LEC_CHECK_EXCEPTION("LEC000006", "在途额度申请/生效额度协议校验-未获取到校验返回信息！"),
    LEC_CHECKAPP_PARAM_EXCEPTION("LEC000007", "在途额度冻结/解冻申请校验-参数！"),
    LEC_CHECKAPP_EXCEPTION("LEC000008", "在途额度冻结/解冻申请校验-未获取到校验返回信息！"),
    LEC_CHECKAPP_EXISTS_EXCEPTION("LEC000009", "当前额度协议存在额度冻结/解冻的申请流程!"),

    //个人额度申请删除异常定义
    LIA_DELETE_PARAMS_EXCEPTION("D000001", "删除失败！参数缺失！"),
    LIA_DELETE_FAILED_EXCEPTION("D000002", "删除失败！"),
    LIA_DELETE_CLIENTRTNNULL_EXCEPTION("D000003", "调用接口删除返回为空！"),

    //额度分项修改时异常定义
    LS_UPDATE_PARAMS_EXCEPTION("LSU000001", "额度分项修改-参数异常！"),
    LS_UPDATE_GETGUARLISTNULL_EXCEPTION("LSU000002", "未引入担保合同！"),
    LS_UPDATE_GUARAMTSMALL_EXCEPTION("LSU000003", "额度分项修改-引入的担保合同的本次担保总额无法覆盖本次授信额度！"),
    LS_UPDATE_UPDATELSDATA_EXCEPTION("LSU000004", "额度分项修改-修改数据失败！"),
    LS_UPDATE_GUARCONTNULL_EXCEPTION("LSU000005", "额度分项修改-未获取到引入的担保合同数据！"),
    LS_UPDATE_GUARCONTTERMSMALL_EXCEPTION("LSU000006", "引入的担保合同担保期限无法覆盖额度分项授信期限！"),
    LS_UPDATE_GETLMTSUB_EXCEPTION("LSU000007", "额度分项数据获取异常！请检查额度分项数据是否录入完整！"),

    //额度分项删除时异常定义
    LS_DELETE_DELGUAR_EXCEPTION("LSD000001", "额度分项删除-删除担保合同关系失败！"),
    LS_DELETE_DELLS_EXCEPTION("LSD000002", "额度分项删除失败！"),

    //查询是否存在拒绝历史
    LQ_REFUSED_PARAMS_EXCEPTION("LQ000001", "查询拒绝历史异常！参数缺失！"),

    //个人额度申请-修改保存异常定义
    LU_UPDATE_PARAMS_EXCEPTION("LU000001", "参数缺失！"),
    LU_UPDATE_LMTSUBNULL_EXCEPTION("LU000002", "未获取到额度分项数据！"),
    LU_UPDATE_LMTSUBRELAMTSMALL_EXCEPTION("LU000003", "额度分项循环总额度小于额度申请循环额度！"),
    LU_UPDATE_LMTSUBONEAMTSMALL_EXCEPTION("LU000004", "额度分项一次性总额度小于额度申请一次性额度！"),
    LU_UPDATE_LMTTOTALAMTSMALL_EXCEPTION("LU000005", "额度申请授信总额大于循环额度与一次性额度的总和！"),
    LU_UPDATE_FAILED_EXCEPTION("LU000006", "额度申请更新失败！"),
    LU_UPDATE_ENDDATESMALLCURDATE_EXCEPTION("LU000007", "授信到期日小于当前系统日期！"),
    LU_UPDATE_SUBENDDATESMALLCURDATE_EXCEPTION("LU000008", "额度分项到期日小于当前系统日期！"),
    LU_UPDATE_ENDDATESMALLSUBENDDATE_EXCEPTION("LU000009", "授信到期日小于额度分项到期日！"),
    LU_UPDATE_GETSUBLMT_EXCEPTION("LU000010", "获取额度分项信息异常，请检查额度分项信息录入是否完整！"),
    LU_UPDATE_LMTSUBRAMTSMALL_EXCEPTION("LU000011", "额度分项循环额度大于额度申请循环额度！"),
    LU_UPDATE_LMTSUBOAMTSMALL_EXCEPTION("LU000012", "额度分项一次性额度大于额度申请一次性额度！"),


    //个人额度申请流程通过异常定义
    LWF_ENDHANDLE_UPDATE_STATUS_EXCEPTION("LWFE000001", "更新额度申请主表异常！"),
    LWF_ENDHANDLE_APPNULL_EXCEPTION("LWFE000002", "申请主表数据为空！"),
    LWF_ENDHANDLE_LMTSUBNULL_EXCEPTION("LWFE000003", "额度分项数据为空！"),
    LWF_ENDHANDLE_TRANSLMTAPP_EXCEPTION("LWFE000004", "转化额度申请主表数据异常！"),
    LWF_ENDHANDLE_TRANSLMTSUBNULL_EXCEPTION("LWFE000005", "额度分项列表数据中存在为空的对象！"),
    LWF_ENDHANDLE_TRANSLMTACCNULL_EXCEPTION("LWFE000006", "转化额度分项列表数据异常！"),
    LWF_ENDHANDLE_LMTCLIENTNULL_EXCEPTION("LWFE000007", "调用额度服务接口返回为空！"),
    LWF_ENDHANDLE_GETCFGPARAMCLIENTNULL_EXCEPTION("LWFE000008", "调用参数读取接口返回为空！"),
    LWF_ENDHANDLE_LMTSUBGUARCONTNULL_EXCEPTION("LWFE000009", "额度分项下的生效担保合同为空！"),
    LWF_ENDHANDLE_TRANSGUAR_EXCEPTION("LWFE000010", "转化担保合同关系集合中存在为空的数据！"),
    LWF_ENDHANDLE_TRANSGUARLISTNULL_EXCEPTION("LWFE000011", "转化后的担保合同关系结果集合为空！"),
    LWF_ENDHANDLE_SAVEGUARLIST_EXCEPTION("LWFE000012", "保存担保合同关系结果集合异常！"),
    LWF_ENDHANDLE_UPDATEIQPLMT_EXCEPTION("LWFE000013", "更新业务与额度关系表数据异常！"),
    LWF_ENDHANDLE_UPDATECTRLMT_EXCEPTION("LWFE000014", "更新合同与额度关系表数据异常！"),

    //个人额度申请流程拒绝异常定义
    LWF_REFUSEHANDLE_LMTSUBNULL_EXCEPTION("LWFR000001", "额度分项数据为空！"),
    LWF_REFUSEHANDLE_UPDATEGUAR_EXCEPTION("LWFR000002", "更新业务与担保合同关系数据异常！"),
    LWF_REFUSEHANDLE_UPDATE_STATUS_EXCEPTION("LWFR000003", "更新额度申请主表异常！"),

    //个人额度申请复议异常定义
    LIA_REVIEW_PARAMS_EXCEPTION("LIAR000001", "额度申请复议-未获取到需要复议的申请信息！"),
    LIA_REVIEW_REVIEWEXISTS_EXCEPTION("LIAR000002", "该笔申请已存在复议申请！"),
    LIA_REVIEW_CHECK_EXCEPTION("LIAR000003", "在途额度申请/生效额度协议校验-未获取到校验返回信息！"),
    LIA_REVIEW_APPNOTEXISTS_EXCEPTION("LIAR000004", "需要复议的申请数据不存在！"),
    LIA_REVIEW_INSERTAPP_EXCEPTION("LIAR000005", "新增复议申请数据失败！"),
    LIA_REVIEW_LMTSUBNULL_EXCEPTION("LIAR000006", "复议申请-额度分享数据不存在！"),
    LIA_REVIEW_INSERTLMTSUB_EXCEPTION("LIAR000007", "新增复议申请额度分项数据失败！"),
    LIA_REVIEW_IQPGUARNULL_EXCEPTION("LIAR000008", "复议申请-额度分项下与担保合同关系数据为空！"),
    LIA_REVIEW_INSERTIQPGUAR_EXCEPTION("LIAR000009", "复议申请-保存额度分项下与担保合同关系数据异常！"),
    LIA_REVIEW_BIZCORRENULL_EXCEPTION("LIAR000010", "复议申请-办理人员数据为空！"),
    LIA_REVIEW_INSERTBIZCORRE_EXCEPTION("LIAR000011", "复议申请-办理人员数据为空！"),


    // PvpDefEnums
    /**
     * 默认成功
     **/
    PVP_SUCCESS_DEF("000000", "操作成功"),
    PVP_EXCEPTION_DEF("999999", "操作异常"),
    /**
     * 新增异常
     **/
    E_PVP_INSERT_PARAM_FAILED("PVPIE0001", "新增数据失败！参数异常！"),
    E_PVP_WFEXISTS("PVPIE0002", "该笔合同已存在待审批的放款申请记录！"),
    E_PVP_WFENDEXISTS("PVPIE0003", "该笔合同已存在生效的借据记录！"),
    E_PVP_GETUSER_EXCEPTION("PVPIE0004", "获取当前登录用户为空！"),

    /**
     * 查询异常
     **/
    E_PVP_QUERY_PARAMS_EXCEPTION("PVPQE0001", "查询数据异常！入参缺失！"),
    E_PVP_QUERY_CONTNULL_EXCEPTION("PVPQE0002", "未获取到合同数据！"),
    E_PVP_QUERY_CLIENTCALL_EXCEPTION("PVPQE0003", "调用额度查询接口异常！"),
    E_PVP_QUERY_CLIENTRETURNNULL_EXCEPTION("PVPQE0004", "未获取到额度接口返回的数据！"),

    /**
     * 更新异常
     **/
    E_PVP_UPDATE_PARAMS_EXCEPTION("PVPUE0001", "保存数据时参数缺失！"),
    E_PVP_UPDATE_CONTDATESMALL_EXCEPTION("PVPUE0002", "放款到期日与合同到期日差值超过半年"),
    E_PVP_UPDATE_LMTDATESMALL_EXCEPTION("PVPUE0003", "放款到期日超过授信额度到期日！"),
    E_PVP_UPDATE_THLMTDATESMALL_EXCEPTION("PVPUE0004", "放款到期日超过第三方额度到期日！"),
    E_PVP_UPDATE_CONTNULL_EXCEPTION("PVPUE0005", "获取合同信息为空！"),
    E_PVP_UPDATE_CONTAMTZERO_EXCEPTION("PVPUE0006", "未获取到合同余额信息或合同余额为0！"),
    E_PVP_UPDATE_CONTAMTSMALL_EXCEPTION("PVPUE0007", "合同余额无法覆盖放款金额！"),
    E_PVP_UPDATE_ACCTNULL_EXCEPTION("PVPUE0008", "获取账户信息为空！"),
    E_PVP_UPDATE_ACCTAMTNOTEQUALS_EXCEPTION("PVPUE0009", "放款金额与受托支付总金额不相等！"),
    E_PVP_UPDATE_BIZDATACHECK_FAILED("PVPUE0010", "放款申请业务数据校验不通过！"),
    E_PVP_UPDATE_FAILED("PVPUE0011", "放款申请保存数据失败！"),
    E_PVP_UPDATE_GUARCONTNULL("PVPUE0012", "未获取到担保合同结果表数据！"),
    E_PVP_UPDATE_GUARNULL("PVPUE0013", "未获取到押品信息数据！"),
    E_PVP_UPDATE_CERTINULL("PVPUE0014", "未获取到权证信息数据！"),
    E_PVP_UPDATE_CERTIINEFFECT("PVPUE0015", "担保合同下存在无有效权证的押品！"),
    E_PVP_UPDATE_CERTIINAPPR("PVPUE0016", "担保合同下存在未完成入库的押品！"),
    E_PVP_UPDATE_OTHERCERTIINCHECK("PVPUE0017", "担保合同检验-押品下入库已记账的他项权证数不等于引用该押品的生效担保合同数"),

    /**
     * 流程处理异常
     **/
    E_PVP_WFHAND_PARAMS_EXCEPTION("PVPWH0001", "放款流程业务处理异常-放款申请流水号为空！"),
    E_PVP_WFHAND_PVPNULL_EXCEPTION("PVPWH0002", "放款流程业务处理异常-未获取到放款申请信息！"),
    E_PVP_WFHAND_UPDPVPSTATUS_EXCEPTION("PVPWH0003", "放款流程业务处理异常-更新放款申请状态异常！"),
    E_PVP_WFHAND_CONTNULL_EXCEPTION("PVPWH0004", "放款流程业务处理异常-未获取到业务合同数据！"),
    E_PVP_WFHAND_UPDWFSTATUS_EXCEPTION("PVPWH0005", "放款流程业务处理异常-更新全流程状态异常！"),
    E_PVP_WFHAND_GETACCLOAN_EXCEPTION("PVPWH0006", "放款流程业务处理异常-获取借据数据参数异常！"),
    E_PVP_WFHAND_INSERTACCLOAN_EXCEPTION("PVPWH0007", "放款流程业务处理异常-保存借据数据异常！"),
    E_PVP_WFHAND_GETPA_EXCEPTION("PVPWH0008", "放款流程业务处理异常-生成授权信息参数异常！"),
    E_PVP_WFHAND_GETACCT_EXCEPTION("PVPWH0009", "放款流程业务处理异常-生成授权信息未获取到账户信息！"),
    E_PVP_WFHAND_INSERTACCT_EXCEPTION("PVPWH0010", "放款流程业务处理异常-保存授权信息失败！"),
    E_PVP_WFHAND_GETPTPI_EXCEPTION("PVPWH0011", "放款流程业务处理异常-生成受托支付信息参数异常！"),
    E_PVP_WFHAND_INSERTPTPI_EXCEPTION("PVPWH0012", "放款流程业务处理异常-保存受托支付信息失败！"),
    E_PVP_WFHAND_GETACCTLISTNULL_EXCEPTION("PVPWH0013", "放款流程业务处理异常-获取账户列表数据为空！"),
    E_PVP_WFHAND_TRANSACCTLISTEXCEPTION_EXCEPTION("PVPWH0014", "放款流程业务处理异常-转化账户数据异常！"),
    E_PVP_WFHAND_INSERTACCTLISTEXCEPTION_EXCEPTION("PVPWH0015", "放款流程业务处理异常-转化账户数据异常！"),

    //TaskPoolRuleEunms 项目池关联分配规则相关的字典常量
    E_TASKPOOL_EXCEPTION_01("TP999901", "项目池未关联分配规则"),
    E_TASKPOOL_EXCEPTION_02("TP999902", "查询岗位下的用户失败"),

    E_IQP_LOAN_EXT_DEFAULT("999999", "删除失败"),

    /*
     * 资产池相关
     * */
    E_EXPORTASPLACCEXCEL_EXCEPTION_01("999999", "池额度台账导出异常"),
    E_EXPORTINPOOLACCEXCEL_EXCEPTION_01("999999", "池内业务台账导出异常"),
    /*
     * 影像缺扫相关
     *
     * */
    DISI_SUCCESS_DEF("000000", "操作成功"),
    DISI_EXCEPTION_DEF("999999", "操作异常！"),
    E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_01("DOCIMAIE0001","新增主表数据异常"),
    E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_02("DOCIMAIE0002","新增失败，存在系统发起的补扫任务！"),
    E_DOC_IMAGE_SPPL_INFO_INSERTEXCEPTION_03("DOCIMAIE0003","新增失败，存在人工发起的补扫任务！"),
    E_DOC_IMAGE_SPPL_INFO_UPDATEEXCEPTION_01("DOCIMAUE0001","更新主表数据异常"),
    E_DOC_IMAGE_SPPL_INFO_UPDATEEXCEPTION_02("DOCIMAUE0002","更新主表数据参数异常"),
    /*
     * 档案参数配置相关
     *cfgdocparamslist
     * */
    CDPL_SUCCESS_DEF("000000", "操作成功"),
    CDPL_EXCEPTION_DEF("999999", "操作异常！"),
    E_CFG_DOC_PARAMS_LIST_INSERTEXCEPTION_01("CFGDOCIE0001","新增主表数据异常"),
    E_CFG_DOC_PARAMS_LIST_UPDATEEXCEPTION_01("CFGDOCUE0001","更新主表数据异常"),
    E_CFG_DOC_PARAMS_LIST_UPDATEEXCEPTION_02("CFGDOCUE0002","更新主表数据参数异常"),
    /*
     * 资产池名单相关
     * */
    E_IMPORTEXCEL_EXCEPTION_01("999999", "资产池承兑行名单导入异常"),
    E_OUTPORTEXCEL_EXCEPTION_01("999999", "资产池承兑行名单导出异常"),
    E_IMPORTEXCEL_EXCEPTION_02("999999", "资产池白名单导入异常"),
    E_OUTPORTEXCEL_EXCEPTION_02("999999", "资产池白名单导出异常"),
    E_OUTPORTEXCEL_EXCEPTION_03("999999", "资产池白名单导出异常"),
    /*
     * 超短贷款
     * */
    E_WORK_DAY_02("999999","工作日校验不通过"),

    /*
     * 临时档案异地机构不生成  PB09
     * */
    E_CentralFileTask_YD("EPB090001","临时档案异地机构不生成");


    public static Map<String, String> keyValue;

    static {
        keyValue = new TreeMap<>();
        for (EcbEnum enumData : EnumSet.allOf(EcbEnum.class)) {
            keyValue.put(enumData.key, enumData.value);
        }
    }

    public String key;
    public String value;

    private EcbEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public static String lookup(int key) {
        return (String) keyValue.get(key);
    }

    public static String key(String dataValue) {
        String key = null;
        for (EcbEnum enumData : values()) {
            if (enumData.value.equals(dataValue)) {
                key = enumData.key;
                break;
            }
        }
        return key;
    }

    public final String value() {
        return (String) keyValue.get(key);
    }
}
