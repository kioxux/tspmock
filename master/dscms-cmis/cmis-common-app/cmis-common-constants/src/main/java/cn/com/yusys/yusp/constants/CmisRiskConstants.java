package cn.com.yusys.yusp.constants;

/**
 * cmis信贷平台公共常量类
 */
public class CmisRiskConstants {

    /** 风险拦截等级 1 不校验 */
    public static final String RISK_LEVEL_1 = "1";
    /** 风险拦截等级 2 提示 */
    public static final String RISK_LEVEL_2 = "2";
    /** 风险拦截等级 3 拦截 */
    public static final String RISK_LEVEL_3 = "3";

    /** 风险拦截执行结果 0 通过 */
    public static final String RISK_RESULT_TYPE_0 = "1";
    /** 风险拦截执行结果 1 不通过 */
    public static final String RISK_RESULT_TYPE_1 = "3";


    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0002 业务申请类型为空！*/
    public static final String RISK_ERROR_COMMON_0002 = "业务申请类型为空！";
    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0002 没有匹配到对应需要风险拦截的业务申请类型bizType！*/
    public static final String RISK_ERROR_COMMON_00021 = "根据业务申请类型bizType没有匹配到对应的拦截内容！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0003 通过授信申请流水号未获取到对应的授信申请信息！*/
    public static final String RISK_ERROR_COMMON_0003 = "通过授信申请流水号未获取到对应的授信申请信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_0004 通过授信申请流水号未获取到对应的授信申请分项明细！ */
    public static final String RISK_ERROR_COMMON_0004 = "通过授信申请流水号未获取到对应的授信申请分项明细！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_000301 通过授信审批流水号未获取到对应的授信审批信息！*/
    public static final String RISK_ERROR_COMMON_000301 = "通过授信审批流水号未获取到对应的授信审批信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_000401 通过授信审批流水号未获取到对应的授信审批分项明细！ */
    public static final String RISK_ERROR_COMMON_000401 = "通过授信审批流水号未获取到对应的授信审批分项明细！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0005 通过申请分项流水号未获取到对应的授信申请分项适用品种明细信息！*/
    public static final String RISK_ERROR_COMMON_0005 = "通过申请分项流水号未获取到对应的授信申请分项适用品种明细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_000501 通过审批分项流水号未获取到对应的授信审批分项适用品种明细信息！*/
    public static final String RISK_ERROR_COMMON_000501 = "通过审批分项流水号未获取到对应的授信审批分项适用品种明细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0006 通过集团授信申请流水号未获取到对应的集团成员信息！*/
    public static final String RISK_ERROR_COMMON_0006 = "通过集团授信申请流水号未获取到对应的集团成员信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0007 通过合同申请流水号未获取到对应的合同申请信息！*/
    public static final String RISK_ERROR_COMMON_0007 = "通过合同申请流水号未获取到对应的合同申请信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0008 通过合同申请流水号未获取到对应的合同申请信息！*/
    public static final String RISK_ERROR_COMMON_0008 = "通过申请流水号未获取到对应的关联抵质押物信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0009 通过申请流水号未获取到对应的保证担保信息信息！*/
    public static final String RISK_ERROR_COMMON_0009 = "通过申请流水号未获取到对应的保证担保信息信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0010 通过客户号未获取到客户模块的客户基本信息！*/
    public static final String RISK_ERROR_COMMON_0010 = "通过客户号未获取到客户模块的客户基本信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0011 通过申请流水号未获取到对应的保证担保信息信息！*/
    public static final String RISK_ERROR_COMMON_0011 = "通过客户号未获取到客户模块的对私客户详细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0012 通过客户号未获取到客户模块的对公客户详细信息！*/
    public static final String RISK_ERROR_COMMON_0012 = "通过客户号未获取到客户模块的对公客户详细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0013 授信品种必须存在对应的分项适用品种明细信息！*/
    public static final String RISK_ERROR_COMMON_0013 = "授信品种必须存在对应的分项适用品种明细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0014 通过担保合同号未获取到对应的担保信息！*/
    public static final String RISK_ERROR_COMMON_0014 = "通过担保合同号未获取到对应的担保合同信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_0105 通过合同编号获取最高额担保合同信息失败 */
    public static final String RISK_ERROR_COMMON_0015 = "通过担保合同合同编号未获取到担保合同信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_0016 通过合同编号未获取到对应的合同信息 */
    public static final String RISK_ERROR_COMMON_0016 = "通过合同编号未获取到合同信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_0017 通过放款流水号未获取到出账申请信息 */
    public static final String RISK_ERROR_COMMON_0017 = "通过放款流水号未获取到出账申请信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_018 传入的客户号ID为空！*/
    public static final String RISK_ERROR_COMMON_018 = "传入的客户号ID为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_019 通过担保合同编号未获取到对应的抵质押物信息 */
    public static final String RISK_ERROR_COMMON_019 = "通过担保合同编号未获取到对应的抵质押物信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_0020 通过征信查询流水号未获取到征信报告查询信息 */
    public static final String RISK_ERROR_COMMON_0020 = "通过征信查询流水号未获取到征信报告查询信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_0021 客户id为空 */
    public static final String RISK_ERROR_COMMON_0021 = "客户id为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_COMMON_0022 通过客户id获取客户信息失败 */
    public static final String RISK_ERROR_COMMON_0022 = "通过客户id获取客户信息失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0023 通过授信申请流水号未获取到对应的授信申请分项信息！*/
    public static final String RISK_ERROR_COMMON_0023 = "通过授信申请流水号未获取到对应的授信申请分项信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_0024 通过授信申请分项流水号未获取到对应的授信申请分项的品种信息！*/
    public static final String RISK_ERROR_COMMON_0024 = "通过授信申请分项流水号未获取到对应的授信申请分项的品种信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_014 获取系统配置参数失败！*/
    public static final String RISK_ERROR_COMMON_014 = "获取系统配置参数失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_015 不适用此项检查！*/
    public static final String RISK_ERROR_COMMON_015 = "不适用此项检查！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_COMMON_015 数据异常！*/
    public static final String RISK_ERROR_COMMON_016 = "数据异常！";


    /**
     * 风险拦截执行结果描述 RISK_ERROR_000101 所属条线异常
     */
    public static final String RISK_ERROR_00101="所属条线异常！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00102 授信申请担保信息校验时，担保方式不等于信用时，其下必须关联与其匹配的担保品
     */
    public static final String RISK_ERROR_00102="授信申请担保信息校验时，担保方式不等于信用时，其下必须关联与其匹配的担保品！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00125 授信申请担保信息校验时，担保方式不等于信用时，其下必须关联主担保
     */
    public static final String RISK_ERROR_00125="授信申请担保信息校验时，担保方式不等于信用时，其下必须关联主担保信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_000103 授信申请担保信息校验时，担保方式不等于信用时，担保品不能重复，担保品不能重复
     */
    public static final String RISK_ERROR_00103="授信申请担保信息校验时，担保方式不等于信用时，担保品不能重复！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_000104 担保物评估价值金额之和必须大于等于申请金额
     */
    public static final String RISK_ERROR_00104="小微或者零售授信申请时，担保物评估价值金额之和必须大于等于申请金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_000104 担保物评估价值金额之和必须大于等于申请金额
     */
    public static final String RISK_ERROR_00105="对公授信申请时，担保物评估价值金额之和必须大于等于对应的授信分项金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00205 保证人担保金额之和必须大于等于对应的授信分项金额
     */
    public static final String RISK_ERROR_00205="对公授信申请时，保证人担保金额之和必须大于等于对应的授信分项金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00106 单人担保，必须大于等于对应的授信分项金额
     */
    public static final String RISK_ERROR_00106="保证方式=单人担保，保证金额（主担保）必须大于等于对应的授信分项金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00107 单人担保，保证人数必须等于1
     */
    public static final String RISK_ERROR_00107="主担保方式为单人担保时，主担保保证人数必须等于1！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00207 单人担保，保证人数必须等于1
     */
    public static final String RISK_ERROR_00207="追加担保方式为单人担保时，追加担保保证人数必须等于1！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00108 保证方式=多人分保
     */
    public static final String RISK_ERROR_00108="保证方式=多人分保，保证人数量必须大于1！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00208 主担保方式只能有一种
     */
    public static final String RISK_ERROR_00208="主担保方式只能有一种！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00209 追加担保方式只能有一种
     */
    public static final String RISK_ERROR_00209="追加担保方式只能有一种！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00109 单人担保，保证人数必须等于1
     */
    public static final String RISK_ERROR_00109="保证方式=多人分保，保证人保证金额之和必须大于等于对应的授信分项金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00110 保证方式=多人联保，保证人数量大于1
     */
    public static final String RISK_ERROR_00110="保证方式=多人联保，保证人数量必须大于1！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00110 保证方式=多人联保，且每个保证人保证金额必须大于等于对应的授信分项金额
     */
    public static final String RISK_ERROR_00111="保证方式=多人联保，且每个保证人保证金额必须大于等于对应的授信分项金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00112 保证方式不在范围之内（单人担保、多人分保、多人联保）！
     */
    public static final String RISK_ERROR_00112="保证方式不在范围之内（单人担保、多人分保、多人联保、多人担保）！！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00113 主合同的担保方式=信用且担保合同列表存在“是否追加担保=是”的担保合同，担保合同必须存在担保品
     */
    public static final String RISK_ERROR_00113="主合同的担保方式=信用且担保合同列表存在“是否追加担保=是”的担保合同，担保合同必须存在担保品！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00114 担保方式=低风险时，查询关联方名单信息
     */
    public static final String RISK_ERROR_00114="担保方式=低风险时，查询关联方名单信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00115 担保方式=低风险时，客户如果在关联方名单内，则必须有担保品或者保证金比例是100%
     */
    public static final String RISK_ERROR_00115="担保方式=低风险时，客户如果在关联方名单内，则必须有担保品或者保证金比例是100%！";

    /** 风险拦截执行结果描述 RISK_ERROR_00401 未在系统配置里配置集团客户标准化产品授信分项总额 */
    public static final String RISK_ERROR_00401 = "未在系统配置里配置集团客户标准化产品授信分项额度！";

    /** 风险拦截执行结果描述 RISK_ERROR_00401 该集团客户下不存在标准化产品授信分项 */
    public static final String RISK_ERROR_00402 = "该集团客户下不存在标准化产品授信分项！";

    /** 风险拦截执行结果描述 RISK_ERROR_00403 集团客户所有成员的标准化产品的授信分项总额超额*/
    public static final String RISK_ERROR_00403 = "集团客户所有成员的标准化产品的授信分项总额超额！";

    /** 风险拦截执行结果描述 RISK_ERROR_00401 未在系统配置里配置集团客户标准化产品授信分项总额 */
    public static final String RISK_ERROR_00404 = "未在系统配置里配置个人客户标准化产品授信分项额度！";

    /** 风险拦截执行结果描述 RISK_ERROR_00405 该单一客户下不存在标准化产品授信分项 */
    public static final String RISK_ERROR_00405 = "该单一客户下不存在标准化产品授信分项！";

    /** 风险拦截执行结果描述 RISK_ERROR_00406 单一客户所有标准化产品的授信分项总额超额 */
    public static final String RISK_ERROR_00406 = "单一客户所有标准化产品的授信分项总额超额！";

    /** 风险拦截执行结果描述 RISK_ERROR_00701 当前客户已经存在审批中或退回的白领易贷通业务申请 */
    public static final String RISK_ERROR_00701 = "当前客户已经存在审批中或退回的白领易贷通业务申请！";

    /** 风险拦截执行结果描述 RISK_ERROR_00702 当前客户存在在途或生效的白领易贷通合同 */
    public static final String RISK_ERROR_00702 = "当前客户存在在途或生效的白领易贷通合同！";

    /** 风险拦截执行结果描述 RISK_ERROR_00703 当前客户存在在途或生效的白领易贷通借据 */
    public static final String RISK_ERROR_00703 = "当前客户存在在途或生效的白领易贷通借据！";

    /** 风险拦截执行结果描述 RISK_ERROR_00801 借款人在客户模块的信用评级到期日必须大于等于当前日期 */
    public static final String RISK_ERROR_00801 = "借款人在客户模块的信用评级到期日必须大于等于当前日期！";

    /** 风险拦截执行结果描述 RISK_ERROR_00802  借款人在客户模块的信用评级不存在*/
    public static final String RISK_ERROR_00802 = "借款人在客户模块的信用评级不存在！";

    /** 风险拦截执行结果描述 RISK_ERROR_00803  借款人在客户模块的信用评级到期日为空*/
    public static final String RISK_ERROR_00803 = "借款人在客户模块的信用评级到期日为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_00804  非零内评到期日期转换后日期为空*/
    public static final String RISK_ERROR_00804 = "非零内评到期日期转换后日期为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_00805  借款人在客户模块的信用评级到期日必须大于等于当前日期*/
    public static final String RISK_ERROR_00805 = "借款人在客户模块的信用评级到期日必须大于等于当前日期！";

    /** 风险拦截执行结果描述 RISK_ERROR_000901 当前授信人员信息获取失败！ */
    public static final String RISK_ERROR_00901= " 当前授信人员信息获取失败！ ";

    /** 风险拦截执行结果描述 RISK_ERROR_000902 当前授信人员出生日期获取失败！ */
    public static final String RISK_ERROR_00902= " 当前授信人员出生日期获取失败！ ";

    /** 风险拦截执行结果描述 RISK_ERROR_000903 借款人以个人名义申请的经营性贷款，年龄超过65周岁！  */
    public static final String RISK_ERROR_00903= " 借款人以个人名义申请的经营性贷款，年龄超过65周岁！ ";

    /** 风险拦截执行结果描述 RISK_ERROR_000904 借款人年龄计算失败！  */
    public static final String RISK_ERROR_00904= " 借款人年龄计算失败！ ";

    /** 风险拦截执行结果描述 RISK_ERROR_0081 同一授信项下，不能同时存在保函和在线保函 */
    public static final String RISK_ERROR_01001 = "同一授信项下，不能同时存在保函和在线保函！";

    /** 风险拦截执行结果描述 RISK_ERROR_0083 授信分项项下必须存在业务品种 */
    public static final String RISK_ERROR_01002 = "同一个授信分项项下，授信品种+产品类型属性不能重复！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_001101 借款人以个人名义申请的经营性贷款，年龄超过65周岁！
     */
    public static final String RISK_ERROR_01101="担保方式为信用，借款人存在关联方表中！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_001102 联方表接口查询失败！
     */
    public static final String RISK_ERROR_01102="关联方表接口查询失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_01301 本次授信敞口大于单一客户限额！
     */
    public static final String RISK_ERROR_01301 = " 本次授信敞口大于单一客户限额！ ";

    /** 风险拦截执行结果描述 RISK_ERROR_001401 宽限期必须为0 */
    public static final String RISK_ERROR_001401 = "宽限期必须为0！";

    /** 风险拦截执行结果描述 RISK_ERROR_001402 只能是循环授信（即合同类型为最高额合同） */
    public static final String RISK_ERROR_001402 = "只能是循环授信（即合同类型为最高额合同）！";

    /** 风险拦截执行结果描述 RISK_ERROR_001403 担保方式=质押，押品不能是理财产品 */
    public static final String RISK_ERROR_001403 = "担保方式=质押，押品不能是理财产品！";

    /** 风险拦截执行结果描述 RISK_ERROR_001404 还款方式=按月付息到期还本或利随本清，贷款期限必须小于等于1年 */
    public static final String RISK_ERROR_001404 = "还款方式=按月付息到期还本或利随本清，贷款期限必须小于等于1年！";

    /** 风险拦截执行结果描述 RISK_ERROR_001404 通过放款编号未获取到放款详细信息或信息有缺 */
    public static final String RISK_ERROR_001405 = "通过放款流水号未获取到放款详细信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_001404 通过放款编号未获取到放款详细信息或信息有缺 */
    public static final String RISK_ERROR_001406 = "通过放款流水号获取还款方式为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_001501 外贸贷授信分项总额（循环+非循环）不能超过1000万 */
    public static final String RISK_ERROR_001501 = "外贸贷授信分项总额（循环+非循环）不能超过1000万！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_01901 该客户项下存在在途的优企贷、优农贷授信！
     */
    public static final String RISK_ERROR_01901="该客户项下存在在途的优企贷、优农贷授信！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_01902 该客户项下存在在生效的优企贷、优农贷授信！
     */
    public static final String RISK_ERROR_01902="该客户项下存在在生效的优企贷、优农贷授信！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02001 征信查询列表必须存在借款人经营企业的征信数据
     */
    public static final String RISK_ERROR_02001="征信查询列表必须存在借款人经营企业的征信数据！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02101 合同担保方式不存在
     */
    public static final String RISK_ERROR_02101="合同担保方式不存在！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02102 借款合同尚未关联担保合同
     */
    public static final String RISK_ERROR_02102="借款合同尚未关联担保合同！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02103 担保合同列表不存在“是否追加担保=否”的担保合同
     */
    public static final String RISK_ERROR_02103="担保合同列表不存在“是否追加担保=否”的担保合同！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02104 主担保（非追加）合同金额之和必须大于等于借款合同金额
     */
    public static final String RISK_ERROR_02104="主担保（非追加）合同金额之和必须大于等于最高额借款合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02105 主担保（非追加）合同金额必须等于借款合同金额
     */
    public static final String RISK_ERROR_02105="主担保（非追加）合同金额必须等于一般借款合同金额！";
    /**
     * 风险拦截执行结果描述 RISK_ERROR_02106 最高额担保合同时，担保合同起始日应小于等于主合同起始日
     */
    public static final String RISK_ERROR_02106="最高额担保合同，担保合同起始日应小于等于主合同起始日";
    /**
     * 风险拦截执行结果描述 RISK_ERROR_02107 最高额担保合同时，担保合同到期日应大于等于主合同到期日
     */
    public static final String RISK_ERROR_02107="最高额担保合同，担保合同到期日应大于等于主合同到期日";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02108 一般担保合同，担保合同起始日应等于主合同起始日
     */
    public static final String RISK_ERROR_02108="一般担保合同，担保合同起始日应等于主合同起始日";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02109 一般担保合同，担保合同到期日应等于主合同到期日
     */
    public static final String RISK_ERROR_02109="一般担保合同，担保合同到期日应等于主合同到期日";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02201 担保合同必须存在担保品！
     */
    public static final String RISK_ERROR_02201="担保合同必须存在担保品！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02202 担保品金额之和必须大于等于对应的担保合同金额！
     */
    public static final String RISK_ERROR_02202="担保品金额之和必须大于等于对应的担保合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02203 单人担保，保证人数必须等于1！！
     */
    public static final String RISK_ERROR_02203="单人担保，保证人数必须等于1！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02204 单人担保，担保金额必须等于对应的担保合同金额！
     */
    public static final String RISK_ERROR_02204="单人担保，担保金额必须等于对应的担保合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02205 多人分保，保证人数必须大于1
     */
    public static final String RISK_ERROR_02205="多人分保，保证人数必须大于1！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02206 多人分保，担保金额之和必须等于担保合同金额！
     */
    public static final String RISK_ERROR_02206="多人分保，担保金额之和必须等于担保合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02207 多人分保，保证人数必须大于1
     */
    public static final String RISK_ERROR_02207="多人联保，保证人数必须大于1！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02208 多人联保，每个担保金额必须等于担保合同金额！
     */
    public static final String RISK_ERROR_02208="多人联保，每个担保金额必须等于担保合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02209 通过担保品id未获取到担保品信息！
     */
    public static final String RISK_ERROR_02209="通过担保品id未获取到担保品信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02210 合同没有关联担保合同！
     */
    public static final String RISK_ERROR_02210="合同没有关联担保合同！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02211 零售合同，没有关联对应的合同信息！
     */
    public static final String RISK_ERROR_02211="零售合同，没有关联对应的合同信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02212 ：单人保证：担保品有且只能有一个，且担保金额必须等于对应的担保合同金额
     */
    public static final String RISK_ERROR_02212="：单人保证：担保品有且只能有一个，且担保金额必须不小于对应的担保合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02213 ：多人分保：担保品必须大于1且担保金额之和必须等于担保合同金额。
     */
    public static final String RISK_ERROR_02213="：多人分保：担保品必须大于1且担保金额之和必须等于担保合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02214 ：多人担保：担保品必须大于1且每个担保金额必须等于担保合同金额
     */
    public static final String RISK_ERROR_02214="：多人担保：担保品必须大于1且每个担保金额必须不小于担保合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02301 最高额担保合同金额必须大于等于最高额担保合同项下合同金额之和！
     */
    public static final String RISK_ERROR_02301="最高额担保合同金额必须大于等于最高额担保合同项下合同金额之和！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02302 根据担保合同未在担保合同和业务的关系表中查到对应数据！
     */
    public static final String RISK_ERROR_02302="根据担保合同未在担保合同和业务的关系表中查到对应数据！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02401 合同项下的借据余额+合同项下在途放款申请金额+当前放款申请金额，必须小于等于合同最高可用金额！
     */
    public static final String RISK_ERROR_02401 = "合同项下的借据余额+合同项下在途放款申请金额+当前放款申请金额，必须小于等于合同最高可用金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02402 合同信息获取失败！
     */
    public static final String RISK_ERROR_02402 = "合同信息获取失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02403 金额计算报错！
     */
    public static final String RISK_ERROR_02403 = "金额计算报错！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02404 合同最高可放金额为空，校验失败！
     */
    public static final String RISK_ERROR_02404 = "合同最高可放金额为空，校验失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002501 房抵e点贷,合同期限<=授信期限!
     */
    public static final String RISK_ERROR_02501 = "房抵e点贷,合同期限<=授信期限!";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002502 房抵e点贷,合同到期日<=授信到期日
     */
    public static final String RISK_ERROR_02502 = "房抵e点贷,合同到期日<=授信到期日!";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002503 授信起始日<=合同起始日<=授信到期日
     */
    public static final String RISK_ERROR_02503 = "授信起始日<=合同起始日<=授信到期日!";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002504 合同到期日必须小于等于授信到期日+宽限期
     */
    public static final String RISK_ERROR_02504 = "合同到期日必须小于等于授信到期日+宽限期!";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002505 授信信息获取失败
     */
    public static final String RISK_ERROR_02505 = "授信信息获取失败!";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002506 合同环节日期校验失败
     */
    public static final String RISK_ERROR_02506 = "合同环节日期校验失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02507 根据分项信息，查询起始日到期日期限失败
     */
    public static final String RISK_ERROR_02507 = "根据分项信息，查询起始日到期日期限失败!";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02507 诚易融产品，授信关联合同中最早到期合同到期日小于申请合同起始日！
     */
    public static final String RISK_ERROR_02508 = "诚易融产品，授信关联合同中最早到期合同到期日小于申请合同起始日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02509 合同期限不能大于授信期限
     */
    public static final String RISK_ERROR_02509 = "合同期限不能大于授信期限!";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02510 未查询到产品信息
     */
    public static final String RISK_ERROR_02510 = "未查询到产品信息";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02601 通过授信批复编号未获取到授信品种明细类型信息
     */
    public static final String RISK_ERROR_02601 = "通过授信批复编号未获取到授信品种明细类型信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02602 授信额度类型不在（循环、非循环）之内
     */
    public static final String RISK_ERROR_02602 = "授信额度类型不在（循环、非循环）之内！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02603 合同起始日、放款起始日、放款到期日、合同到期日都不能为空
     */
    public static final String RISK_ERROR_02603 = "合同起始日、放款起始日、放款到期日、合同到期日都不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02604 合同起始日<=放款起始日 并且 放款到期日<=合同到期日
     */
    public static final String RISK_ERROR_02604 = "合同起始日<=放款起始日 并且 放款到期日<=合同到期日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02605 放款起始日<放款到期日
     */
    public static final String RISK_ERROR_02605 = "放款起始日<放款到期日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02606 放款期限<=授信期限
     */
    public static final String RISK_ERROR_02606 = "放款期限<=授信期限！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02607 最高额合同且授信额度为循环或者非循环时，合同起始日<=放款起始日
     */
    public static final String RISK_ERROR_02607 = "最高额合同且授信额度为循环或者非循环时，合同起始日<=放款起始日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02608 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
     */
    public static final String RISK_ERROR_02608 = "最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02609 最高额合同且授信额度为循环或者非循环时，放款起始日<放款到期日
     */
    public static final String RISK_ERROR_02609 = "最高额合同时，宽限期未取得！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02610 最高额合同时，授信到期日取得失败
     */
    public static final String RISK_ERROR_02610 = "最高额合同时，授信到期日取得失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02611 最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期
     */
    public static final String RISK_ERROR_02611 = "最高额合同且授信额度为循环或者非循环时，放款到期日<=授信到期日+宽限期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02611 通过授信台账分项适用品种编号查询授批复台账分项适用品种信息查询异常
     */
    public static final String RISK_ERROR_02612 = "通过授信台账分项适用品种编号查询授批复台账分项适用品种信息查询异常！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02613 最高额授信协议合同时，通过授信批复编号未获取到授信明细信息！
     */
    public static final String RISK_ERROR_02613 = "最高额授信协议合同时，通过授信批复编号未获取到授信明细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02614 最高额合同时，授信到期日取得失败
     */
    public static final String RISK_ERROR_02614 = "最高额合同时，授信到期日取得失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02615 特定产品:诚易融时，获取授信到期日失败
     */
    public static final String RISK_ERROR_02615 = "特定产品:诚易融时，获取授信到期日失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02616 特定产品:诚易融时，获取放款到期日失败
     */
    public static final String RISK_ERROR_02616 = "特定产品:诚易融时，获取放款到期日失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02617 特定产品:诚易融时，放款到期日<=授信到期日
     */
    public static final String RISK_ERROR_02617 = "特定产品:诚易融时，放款到期日<=授信到期日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02618 特定产品:结息贷、优税贷，贷款期限为空
     */
    public static final String RISK_ERROR_02618 = "特定产品:结息贷、优税贷，贷款期限为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02619 特定产品:结息贷、优税贷，单笔提款期限不超过1年
     */
    public static final String RISK_ERROR_02619 = "特定产品:结息贷、优税贷，单笔提款期限不超过1年！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02620 特定产品:结息贷、优税贷，提款到期日、合同到期日不能为空
     */
    public static final String RISK_ERROR_02620 = "特定产品:结息贷、优税贷，提款到期日、合同到期日不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02621 特定产品:结息贷、优税贷，提款到期日、合同到期日不能为空
     */
    public static final String RISK_ERROR_02621 = "特定产品:结息贷、优税贷，提款到期日、合同到期日不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02622 根据分项信息，查询起始日到期日期限失败
     */
    public static final String RISK_ERROR_02622 = "根据分项信息，查询起始日到期日期限失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02623 特定产品:房抵e点贷和省心快贷时，未查到对应的合同影像审核记录
     */
    public static final String RISK_ERROR_02623 = "特定产品:房抵e点贷和省心快贷时，未查到对应的合同影像审核记录！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02624 特定产品:房抵e点贷业务及最高额合同的省心快贷业务需进行合同影像审核
     */
    public static final String RISK_ERROR_02624 = "特定产品:房抵e点贷业务及最高额合同的省心快贷业务需进行合同影像审核！";

    /** 风险拦截执行结果描述 RISK_ERROR_02701 通讯异常等信息 */
    public static final String RISK_ERROR_02701 = "与押品系统通讯异常等信息！请线下查询！";

    /** 风险拦截执行结果描述 RISK_ERROR_02702 获取担保合同信息失败 */
    public static final String RISK_ERROR_02702 = "获取担保合同信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_02703 该合同下未获取到对应的担保合同信息 */
    public static final String RISK_ERROR_02703 = "该合同下未获取到对应的担保合同信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_02704 未查询到查封信息，请线下查询！ */
    public static final String RISK_ERROR_02704 = "未查询到查封信息，请线下查询！";

    /** 风险拦截执行结果描述 RISK_ERROR_02705 未查询到信息，请线下查询！ */
    public static final String RISK_ERROR_02705 = "未查询到信息，请线下查询！";

    /** 风险拦截执行结果描述 RISK_ERROR_02706 抵押物类型非不动产类型！ */
    public static final String RISK_ERROR_02706 = "抵押物类型非不动产类型！";


    /**
     * 风险拦截执行结果描述 RISK_ERROR_2801 借款人必须是正式客户
     */
    public static final String RISK_ERROR_02801="借款人必须是正式客户！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02802 担保关联信息表中未获取到担保合同号
     */
    public static final String RISK_ERROR_02802="担保关联信息表中未获取到担保合同号！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02803 主担保人（非追加）必须是正式客户
     */
    public static final String RISK_ERROR_02803="主担保人（非追加）必须是正式客户！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02901 额度系统-额度校验，返回内容为空
     */
    public static final String RISK_ERROR_02901="额度系统-额度校验，返回内容为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02902 无还本续贷的时候，偿还借据数据不能为空
     */
    public static final String RISK_ERROR_02902="借新还旧、无还本续贷、小企业无还本续贷业务，偿还借据数据不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02903 借据金额之和须和无还本续贷金额相等
     */
    public static final String RISK_ERROR_02903="偿还借据金额须与本次出账金额相等！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_02904 保证金大于出票金额
     */
    public static final String RISK_ERROR_02904="保证金大于出票金额！";

    /** 风险拦截执行结果描述 RISK_ERROR_03001 存在在途的授信申报 */
    public static final String RISK_ERROR_03001 = "存在在途的单一授信申报（授信新增、授信续作、授信复审、授信复议、授信再议）！";

    /** 风险拦截执行结果描述 RISK_ERROR_03002 存在在途的单一授信变更 */
    public static final String RISK_ERROR_03002 = "存在在途的单一授信变更！";

    /** 风险拦截执行结果描述 RISK_ERROR_03003 存在在途的单一预授信细化 */
    public static final String RISK_ERROR_03003 = "存在在途的单一授信细化！";

    /** 风险拦截执行结果描述 RISK_ERROR_03004 存在在途的授信申报 */
    public static final String RISK_ERROR_03004 = "存在在途的单一授信批复变更！";

    /** 风险拦截执行结果描述 RISK_ERROR_03005 存在在途的授信申报 */
    public static final String RISK_ERROR_03005 = "存在在途的单一预授信额度调剂！";

    /** 风险拦截执行结果描述 RISK_ERROR_03006 存在在途的集团授信申报 */
    public static final String RISK_ERROR_03006 = "存在在途的集团授信申报（授信新增、授信续作、授信复审、授信复议、授信再议）！";

    /** 风险拦截执行结果描述 RISK_ERROR_03007 存在在途的集团授信变更 */
    public static final String RISK_ERROR_03007 = "存在在途的集团授信变更！";

    /** 风险拦截执行结果描述 RISK_ERROR_03008 存在在途的集团预授信细化 */
    public static final String RISK_ERROR_03008 = "存在在途的集团预授信细化！";

    /** 风险拦截执行结果描述 RISK_ERROR_03009 存在在途的集团预授信批复变更 */
    public static final String RISK_ERROR_03009 = "存在在途的集团预授信批复变更！";

    /** 风险拦截执行结果描述 RISK_ERROR_03010 存在在途的集团预授信额度调剂 */
    public static final String RISK_ERROR_03010 = "存在在途的集团预授信额度调剂！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03101 支付方式=受托支付时，交易对手账号信息不能未空
     */
    public static final String RISK_ERROR_03101="支付方式=受托支付时，交易对手账号信息不能未空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03102 支付方式=受托支付时，交易对手账号不能重复
     */
    public static final String RISK_ERROR_03102="支付方式=受托支付时，交易对手账号不能重复！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03103 支付方式=受托支付时，交易对手账号不能为空
     */
    public static final String RISK_ERROR_03103="支付方式=受托支付时，交易对手账号不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03104 支付方式=受托支付时，交易对手账号不能与贷款发放账号重复
     */
    public static final String RISK_ERROR_03104="支付方式=受托支付时，交易对手账号不能与贷款发放账号重复！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03105 支付方式=受托支付时，交易对手账号名称不能为空
     */
    public static final String RISK_ERROR_03105="支付方式=受托支付时，交易对手账号名称不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03106 支付方式=受托支付时，交易对手名称存在“基金、证券、国债、账务公司、期货、经纪、担保、土地”
     */
    public static final String RISK_ERROR_03106="支付方式=受托支付时，交易对手名称存在“基金、证券、国债、账务公司、期货、经纪、担保、土地”！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03107 支付方式=受托支付时，交易对手名称存在“基金、证券、国债、账务公司、期货、经纪、担保、土地”
     */
    public static final String RISK_ERROR_03107="支付方式=受托支付时，交易对手不存在“置业、地产、房地产、不动产、房产、房屋”或者产品也不为园区置业贷、个人按揭类消费贷款、法人按揭类贷款！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03108 支付方式=受托支付时，交易对手账号不能重复
     */
    public static final String RISK_ERROR_03108="支付方式=受托支付时，交易对手账号不能重复！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03109 支付方式=受托支付时，受托支付金额须与出账金额一致
     */
    public static final String RISK_ERROR_03109="支付方式=受托支付时，受托支付金额须与出账金额一致！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03201 证件有效期为空
     */
    public static final String RISK_ERROR_03201="证件有效期为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03202 借款人企业营业执照若已过期
     */
    public static final String RISK_ERROR_03202="借款人企业营业执照已过期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03301 担保方式=低风险质押时，押品只能是存单/国债/银票/一级理财/信用证收益权
     */
    public static final String RISK_ERROR_03301="担保方式=低风险质押时，押品只能是存单/国债/银票/一级理财/信用证收益权！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03302 合同担保方式不存在
     */
    public static final String RISK_ERROR_03302="合同担保方式不存在！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_03303 借款合同尚未关联担保合同
     */
    public static final String RISK_ERROR_03303="借款合同尚未关联担保合同！";

    /** 风险拦截执行结果描述 RISK_ERROR_04101 发起复议次数大于设置的参数*/
    public static final String RISK_ERROR_04101 = "发起复议次数大于设置的参数！";

    /** 风险拦截执行结果描述 RISK_ERROR_004102 "获取授信申请数据失败*/
    public static final String RISK_ERROR_04102 = "获取复议授信申请数据失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_004103 "根据流水号查出多笔单一客户申请信息！*/
    public static final String RISK_ERROR_04103 = "根据流水号查出多笔单一客户申请信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_004104 "复审任务发起次数超过三次！*/
    public static final String RISK_ERROR_04104 = "复审任务发起次数超过三次！";

    /** 风险拦截执行结果描述 RISK_ERROR_04105 "复议天数为0时，不允许发起复议！*/
    public static final String RISK_ERROR_04105 = "复议天数为0时，不允许发起复议！";

    /** 风险拦截执行结果描述 RISK_ERROR_04106 复议截止日期必须大于当前日期*/
    public static final String RISK_ERROR_04106 = "复议截止日期必须大于或等于当前日期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_04601 合同项下的押品未在权证台账信息查到或者未入库，授信到期日取得失败
     */
    public static final String RISK_ERROR_04601 = "合同项下的押品未在权证台账信息查到或者未入库！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_05901 合同项下的押品未在权证台账信息查到或者未入库，授信到期日取得失败
     */
    public static final String RISK_ERROR_05901 = "合同项下的押品未在权证台账信息查到或者未入库！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_05902 授信流水号为空
     */
    public static final String RISK_ERROR_05902 = "通过权证编号未查到权证台账信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_05903 通过担保合同编号未查到担保合同与押品及权证关联关系
     */
    public static final String RISK_ERROR_05903 = "通过担保合同编号未查到担保合同与押品及权证关联关系！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_05904 核心担保编号不能为空
     */
    public static final String RISK_ERROR_05904 = "核心担保编号不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06702 放款参数“先放款后抵押”缺失
     */
    public static final String RISK_ERROR_06702 = "放款参数“先放款后抵押”缺失！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_05801 该客户行业分类是两高一剩行业
     */
    public static final String RISK_ERROR_05801 = "该客户行业分类是两高一剩行业！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_05802 该客户行业分类为空
     */
    public static final String RISK_ERROR_05802 = "该客户行业分类为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06101 根据申请流水号未获取到展期借据明细信息
     */
    public static final String RISK_ERROR_06101 = "根据申请流水号未获取到展期借据明细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06102 贷款到期日不能为空
     */
    public static final String RISK_ERROR_06102 = "贷款到期日不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06102 展期后到期日不能为空
     */
    public static final String RISK_ERROR_06103 = "展期后到期日不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06104 展期期限不得小于原到期日
     */
    public static final String RISK_ERROR_06104 = "展期期限不得小于原到期日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06201 根据申请流水号未获取到展期借据明细信息
     */
    public static final String RISK_ERROR_06201 = "根据申请流水号未获取到展期借据明细信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06202 贷款到期日不能为空
     */
    public static final String RISK_ERROR_06202 = "贷款到期日不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06203 展期后到期日不能为空
     */
    public static final String RISK_ERROR_06203 = "展期后到期日不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06204 贷款起始日不能为空
     */
    public static final String RISK_ERROR_06204 = "贷款起始日不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06205 原贷款期限小于等于1年（含），展期期限需小于等于原贷款期限
     */
    public static final String RISK_ERROR_06205 = "原贷款期限小于等于1年（含），展期期限需小于等于原贷款期限！";

    /**
     * 原贷款期限在1年以上（不含1年）5年以下（含5年）的贷款，展期期限需小于等于原贷款期限的一半；
     */
    public static final String RISK_ERROR_06206 = "原贷款期限在1年以上（不含1年）5年以下（含5年）的贷款，展期期限需小于等于原贷款期限的一半；！";

    /**
     * 原贷款期限在1年以上（不含1年）5年以下（含5年）的贷款，展期期限需小于等于原贷款期限的一半；
     */
    public static final String RISK_ERROR_06207 = "原贷款期限在1年以上（不含1年）5年以下（含5年）的贷款，展期期限需小于等于原贷款期限的一半；！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_04601 合同项下的押品未在权证台账信息查到或者未入库，授信到期日取得失败
     */
    public static final String RISK_ERROR_06801 = "征询查询时，信息不全（客户号、证件类型、证件号码、查询原因）！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_06802 对同一个客户，在同一天中，同一证件类型、同一查询原因的征信查询，只允许发起一次
     */
    public static final String RISK_ERROR_06802 = "对同一个客户，在同一天中，同一证件类型、同一查询原因的征信查询，只允许发起一次！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_08001 担保方式与产品类型属性不匹配
     */
    public static final String RISK_ERROR_08001 = "担保方式与产品类型属性不匹配！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_08401 省心快贷自动化审批仅支持企业
     */
    public static final String RISK_ERROR_0840101 = "省心快贷自动化审批要素校验_省心快贷自动化审批仅支持企业！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_08402 省心快贷自动化审批仅支持申请额度小于等于500万
     */
    public static final String RISK_ERROR_0840102 = "省心快贷自动化审批要素校验_省心快贷自动化审批仅支持申请额度小于等于500万！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_08403 省心快贷自动化审批仅支持授信3年以内（含）
     */
    public static final String RISK_ERROR_0840103 = "省心快贷自动化审批要素校验_省心快贷自动化审批仅支持授信3年以内（含）！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_08404 省心快贷自动化审批无宽限期
     */
    public static final String RISK_ERROR_0840104 = "省心快贷自动化审批要素校验_省心快贷自动化审批无宽限期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840201 省心快贷有且仅有一个授信明细
     */
    public static final String RISK_ERROR_0840201 = "省心快贷自动化审批单一上报校验_省心快贷有且仅有一个授信明细！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840202 省心快贷有且仅有一个授信明细，且为循环额度
     */
    public static final String RISK_ERROR_0840202 = "省心快贷自动化审批单一上报校验_省心快贷有且仅有一个授信明细，且为循环额度！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840203 省心快贷有且仅有一个授信明细，且担保方式录入为抵押
     */
    public static final String RISK_ERROR_0840203 = "省心快贷自动化审批单一上报校验_省心快贷有且仅有一个授信明细，且担保方式录入为抵押！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840204 省心快贷自动化审批单一上报校验_省心快贷授信品种为流动资金贷款（短期流动资金贷款、中长期流动资金贷款）
     */
    public static final String RISK_ERROR_0840204 = "省心快贷自动化审批单一上报校验_省心快贷授信品种为流动资金贷款（短期流动资金贷款、中长期流动资金贷款）！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840205 省心快贷有且仅有一个授信分项品种明细
     */
    public static final String RISK_ERROR_0840205 = "省心快贷自动化审批单一上报校验_省心快贷有且仅有一个授信分项品种明细！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840206 省心快贷自动化审批单一上报校验_省心快贷授信品种为流动资金贷款（短期流动资金贷款、中长期流动资金贷款）
     */
    public static final String RISK_ERROR_0840206 = "省心快贷自动化审批单一上报校验_省心快贷授信产品类型属性应为省心快贷！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840207 省心快贷自动化审批无宽限期
     */
    public static final String RISK_ERROR_0840207 = "省心快贷自动化审批单一上报校验_不属于省心快贷的分项信息,分项授信额度不能大于0！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840301 省心快贷自动化审批仅支持引入抵押物一抵
     */
    public static final String RISK_ERROR_0840301 = "省心快贷自动化审批抵押物校验_省心快贷自动化审批仅支持引入抵押物一抵！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840302 省心快贷自动化审批不支持该抵押物类型
     */
    public static final String RISK_ERROR_0840302 = "省心快贷自动化审批抵押物校验_省心快贷自动化审批不支持该抵押物类型！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840303 调查报告抵质押物信息不全
     */
    public static final String RISK_ERROR_0840303 = "省心快贷自动化审批抵押物校验_调查报告抵质押物信息不全！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840401 省心快贷未取得法人代表信息
     */
    public static final String RISK_ERROR_0840401 = "省心快贷自动化审批贷款主体校验_省心快贷未取得法人代表信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840402 省心快贷未取得实际控制人信息
     */
    public static final String RISK_ERROR_0840402 = "省心快贷自动化审批贷款主体校验_省心快贷未取得实际控制人信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840403 省心快贷法人代表是对公客户
     */
    public static final String RISK_ERROR_0840403 = "省心快贷自动化审批贷款主体校验_省心快贷法人代表是对公客户！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840404 省心快贷法人代表客户信息获取失败
     */
    public static final String RISK_ERROR_0840404 = "省心快贷自动化审批贷款主体校验_省心快贷法人代表客户信息获取失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840405 法人代表婚姻情况是已婚时，必须包含配偶数据
     */
    public static final String RISK_ERROR_0840405 = "省心快贷自动化审批贷款主体校验_法人代表婚姻情况是已婚时，必须包含配偶数据！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840406 个人客户信息模块未获取到法人代表信息的联系信息，必须包含配偶数据
     */
    public static final String RISK_ERROR_0840406 = "省心快贷自动化审批贷款主体校验_个人客户信息模块未获取到法人代表信息的联系信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840407 法人代表信息手机号码不得为空
     */
    public static final String RISK_ERROR_0840407 = "省心快贷自动化审批贷款主体校验_省心快贷自动化审批贷款主体校验_法人代表信息手机号码不得为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840408 个人客户信息模块未获取到法人代表配偶的联系信息
     */
    public static final String RISK_ERROR_0840408 = "省心快贷自动化审批贷款主体校验_个人客户信息模块未获取到法人代表配偶的联系信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840409 法人代表配偶手机号码不得为空
     */
    public static final String RISK_ERROR_0840409 = "省心快贷自动化审批贷款主体校验_法人代表配偶手机号码不得为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840501 未检测到有效的企业征信查询，请检查并发起！
     */
    public static final String RISK_ERROR_0840501 = "省心快贷自动化审批征信引入校验_未检测到有效的企业征信查询，请检查并发起！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840502 未检测到有效的企业征信查询，请检查并发起！
     */
    public static final String RISK_ERROR_0840502 = "省心快贷自动化审批征信引入校验_未检测到有效的法人代表征信查询征信查询，请检查并发起！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840503 法人存在配偶的时候，未检测到有效的法人代表配偶的征信查询征信查询，请检查并发起
     */
    public static final String RISK_ERROR_0840503 = "省心快贷自动化审批征信引入校验_法人存在配偶的时候，未检测到有效的法人代表配偶的征信查询征信查询，请检查并发起！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840504 录入的法人代表和实际控制人必须是同一个人！
     */
    public static final String RISK_ERROR_0840504 = "省心快贷自动化审批征信引入校验_录入的法人代表和实际控制人必须是同一个人！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0840505 省心快贷自动化审批征信引入校验_未检测到法人配偶信息！
     */
    public static final String RISK_ERROR_0840505 = "省心快贷自动化审批征信引入校验_未检测到法人配偶信息！";


    /** 风险拦截执行结果描述 RISK_ERROR_0840601 根据客户号未查询到对应的评级信息 */
    public static final String RISK_ERROR_0840601 = "根据客户号未查询到对应的评级信息！";


    /** 风险拦截执行结果描述 RISK_ERROR_0840602 借款企业的内部评级不得低于BB级 */
    public static final String RISK_ERROR_0840602 = "借款企业的内部评级不得低于BB级！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840701 根据客户号未查询到客户模块的客户信息 */
    public static final String RISK_ERROR_0840701 = "根据客户号未查询到客户模块的客户信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840702 公司成立日期为空 */
    public static final String RISK_ERROR_0840702 = "公司成立日期为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840703 补充完善财务报表信息 */
    public static final String RISK_ERROR_0840703 = "补充完善财务报表信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840704 财务报表近两年内不得连续亏损。 */
    public static final String RISK_ERROR_0840704 = "财务报表近两年内不得连续亏损！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840705 财务报表资产负债率不得超过75%。 */
    public static final String RISK_ERROR_0840705 = "财务报表资产负债率不得超过75%！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840706 未查询到符合条件的财务报表信息。 */
    public static final String RISK_ERROR_0840706 = "未查询到符合条件的财务报表信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840707 未查询到符合条件的财务报表资产负债率信息。 */
    public static final String RISK_ERROR_0840707 = "未查询到符合条件的财务报表资产负债率信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840801 请补充调查报告和评分卡 */
    public static final String RISK_ERROR_0840801 = "省心快贷评分卡校验_请补充调查报告和评分卡！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840802 企业有无违规排污，有无被环保部门查处和处罚 */
    public static final String RISK_ERROR_0840802 = "省心快贷评分卡校验_评分卡【企业有无违规排污，有无被环保部门查处和处罚】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840803 企业员工人数是否稳定，员工待遇是否合理 */
    public static final String RISK_ERROR_0840803 = "省心快贷评分卡校验_评分卡【企业员工人数是否稳定，员工待遇是否合理】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840804 企业有无被税务机关查处和处罚 */
    public static final String RISK_ERROR_0840804 = "省心快贷评分卡校验_评分卡【企业有无被税务机关查处和处罚】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840805 有无异常工商股权变更情况 */
    public static final String RISK_ERROR_0840805 = "省心快贷评分卡校验_评分卡【有无异常工商股权变更情况】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840806 请补充调查报告和评分卡 */
    public static final String RISK_ERROR_0840806 = "省心快贷评分卡校验_评分卡【实际控制人有无吸毒、赌博等不良嗜好，其信用卡是否经常在境外大额支付等】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840807 实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为 */
    public static final String RISK_ERROR_0840807 = "省心快贷评分卡校验_评分卡【实际控制人是否存在炒房、炒原材料、炒股票期货等投机行为】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840808 实际控制人是否参与民间融资、投资高风险行业等行为 */
    public static final String RISK_ERROR_0840808 = "省心快贷评分卡校验_评分卡【实际控制人是否参与民间融资、投资高风险行业等行为】内容不满足不予准入！";
    /** 风险拦截执行结果描述 RISK_ERROR_0840809 有无其他影响企业稳定经营的情况 */
    public static final String RISK_ERROR_0840809 = "省心快贷评分卡校验_评分卡【有无其他影响企业稳定经营的情况】内容不满足不予准入！";


    /** 风险拦截执行结果描述 RISK_ERROR_0840810 评分卡分数未满70分不予准入 */
    public static final String RISK_ERROR_0840810 = "省心快贷评分卡校验_评分卡分数未满70分不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840811 评分卡【是否9类易燃易爆行业】内容不满足不予准入 */
    public static final String RISK_ERROR_0840811 = "省心快贷评分卡校验_评分卡【是否9类易燃易爆行业】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840812 是否两高一剩行业为 */
    public static final String RISK_ERROR_0840812 = "省心快贷评分卡校验_评分卡【是否两高一剩行业】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840813 省心快贷评分卡校验_调查报告可行性分析录入信息不全 */
    public static final String RISK_ERROR_0840813 = "省心快贷评分卡校验_调查报告可行性分析录入信息不全！";

    /** 风险拦截执行结果描述 RISK_ERROR_0840814 省心快贷评分卡校验_测算最高额流动资金是否覆盖贷款申请额度说明未录入 */
    public static final String RISK_ERROR_0840814 = "省心快贷评分卡校验_测算最高额流动资金是否覆盖贷款申请额度说明未录入！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09201 未查询到关联的征信报告
     */
    public static final String RISK_ERROR_09201 = "未查询到关联的征信报告！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09202 报告生成日期为空或者报告生成日期与授信业务提交日期相差超过1个月
     */
    public static final String RISK_ERROR_09202 = "报告生成日期为空或者报告生成日期与授信业务提交日期相差超过1个月！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09401 传入的客户号不能为空
     */
    public static final String RISK_ERROR_09401 = "传入的客户号不能为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09601 通过授信台账分项适用品种编号查询授批复台账分项适用品种信息查询异常
     */
    public static final String RISK_ERROR_09601 = "通过授信台账分项适用品种编号查询授批复台账分项适用品种信息查询异常！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09602 对应授信品种利率不存在，请进行人民币定价申请或完成审批
     */
    public static final String RISK_ERROR_09602 = "对应授信品种利率不存在，请进行人民币定价申请或完成审批！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10101 根据申请流水号未获取到合作方案申请信息
     */
    public static final String RISK_ERROR_10101 = "根据申请流水号未获取到合作方案申请信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10102 该合作方存在预警信息，请确认
     */
    public static final String RISK_ERROR_10102 = "该合作方存在预警信息，请确认！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10301 根据申请流水号未获取到权证出库详情信息
     */
    public static final String RISK_ERROR_10301 = "根据申请流水号未获取到权证出库详情信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10302 该权证出库申请未关联押品信息
     */
    public static final String RISK_ERROR_10302 = "该权证出库申请未关联押品信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10303 押品与授信关联关系未接触
     */
    public static final String RISK_ERROR_10303 = "押品与授信关联关系未解除！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10304 根据担保合同号未查到主合同的合同号
     */
    public static final String RISK_ERROR_10304 = "根据担保合同号未查到主合同的合同号！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10305 根据主合同号未查到主合同信息
     */
    public static final String RISK_ERROR_10305 = "根据主合同号未查到主合同信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10306 授信额度编号未取得
     */
    public static final String RISK_ERROR_10306 = "授信额度编号未取得！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10307 根据授信额度编号未查到额度分项信息查询
     */
    public static final String RISK_ERROR_10307 = "根据授信额度编号未查到额度分项信息查询！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10308 授信分项下用信敞口余额小于等于授信分项下授信金额
     */
    public static final String RISK_ERROR_10308 = "授信分项下用信敞口余额小于等于授信分项下授信金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10309 通过核心担保编号未查询到入库信息
     */
    public static final String RISK_ERROR_10309 = "通过核心担保编号未查询到入库信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10310 证明权利或事项不是预告登记，无法出库
     */
    public static final String RISK_ERROR_10310 = "证明权利或事项不是预告登记，无法出库！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10311 权证更新出库时，根据主合同号未查到零售合同信息
     */
    public static final String RISK_ERROR_10311 = "权证更新出库时，根据主合同号未查到零售合同信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10701 调用查询黑名单客户表失败
     */
    public static final String RISK_ERROR_10701 = "调用查询黑名单客户表失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10702 合作方在风险预警管理平台黑名单列表中
     */
    public static final String RISK_ERROR_10702 = "合作方在风险预警管理平台黑名单列表中！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10901 还款方式选择按期付息按计划还本的时候，需要填写还本计划
     */
    public static final String RISK_ERROR_10901 = "还款方式选择按期付息按计划还本的时候，需要填写还本计划！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10902 还本计划中的数据不完整，日期或者金额为空
     */
    public static final String RISK_ERROR_10902 = "还本计划中的数据不完整，日期或者金额为空！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10903 还款期数大于2期
     */
    public static final String RISK_ERROR_10903 = "还款期数大于2期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10904 还款日期大于合同开始日期
     */
    public static final String RISK_ERROR_10904 = "还款日期大于合同开始日期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10905 还款日期小于等于合同到期日
     */
    public static final String RISK_ERROR_10905 = "还款日期小于等于合同到期日！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10906 最后一期还款日期等于合同到日期日期
     */
    public static final String RISK_ERROR_10906 = "最后一期还款日期等于合同到日期日期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10907 还款金额不能小于或者等于0
     */
    public static final String RISK_ERROR_10907 = "还款金额不能小于或者等于0！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10908 还款金额不能等于借据金额
     */
    public static final String RISK_ERROR_10908 = "还款金额不能等于借据金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10909 还款计划的期数越大还款日期越大
     */
    public static final String RISK_ERROR_10909 = "还款计划的期数越大还款日期越大！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10910 所有期数还款金额之和等于合同金额
     */
    public static final String RISK_ERROR_10910 = "所有期数还款金额之和等于合同金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10912 还款金额不能等于授信额度
     */
    public static final String RISK_ERROR_10912 = "还款金额不能等于授信额度！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10913 所有期数还款金额之和等于授信额度
     */
    public static final String RISK_ERROR_10913 = "所有期数还款金额之和等于授信额度！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10914 还款日期须大于营业日期
     */
    public static final String RISK_ERROR_10914 = "还款日期须大于营业日期！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_11401 个人客户时，获取个人社会关系信息失败
     */
    public static final String RISK_ERROR_11401 = "借款人为个人时，未获取个人社会关系信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_11402 借款人为个人时，配偶、成年子女作为高管（法人代表、实际控制人、出资人）的企业，在我行存在存量授信
     */
    public static final String RISK_ERROR_11402 = "借款人为个人时，配偶、成年子女作为高管（实际控制人）的企业，在我行存在存量授信！";
    /**
     * 风险拦截执行结果描述 RISK_ERROR_11403 借款人为个人时，配偶、成年子女作为高管（法人代表、实际控制人、出资人）的企业，在我行存在存量授信
     */
    public static final String RISK_ERROR_11403 = "借款人为个人时，配偶、成年子女作为高管（法人代表）的企业，在我行存在存量授信！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_11406 企业法人代表及其配偶/实际控制人及其配偶、成年子女/出资人在我行有存量经营性业务
     */
    public static final String RISK_ERROR_11406 = "企业法人代表及其配偶/实际控制人及其配偶、成年子女/出资人在我行有存量经营性业务！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_11407 借款人为企业时，校验其法人代表及其配偶、实际控制人及其配偶及成年子女、出资人所担任高管的企业在我行是否有存量经营性业务
     */
    public static final String RISK_ERROR_11407 = "企业法人代表及其配偶/实际控制人及其配偶、成年子女/出资人所担任高管的企业在我行有存量经营性业务！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_11404 借款人为企业时，公司客户高管信息获取失败
     */
    public static final String RISK_ERROR_11404 = "借款人为企业时，公司客户高管信息获取失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_11405 借款人为企业时，法人代表及实际控制人的社会关系失败
     */
    public static final String RISK_ERROR_11405 = "借款人为企业时，未获取到法人代表及实际控制人的社会关系！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11501  未查询到合作方详细信息！
     */
    public static final String RISK_ERROR_11501 = "未查询到合作方详细信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11502  合作方账户信息（合作方类型、保证金账号、保证金账号子序号）不能为空！
     */
    public static final String RISK_ERROR_11502 = "合作方账户信息（合作方类型、保证金账号、保证金账号子序号）不能为空！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11503  未查询到合作方保证金余额信息！
     */
    public static final String RISK_ERROR_11503 = "未查询到合作方保证金余额信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11504  前往额度系统查询合作方下关联的所有贷款金额失败！
     */
    public static final String RISK_ERROR_11504 = "前往额度系统查询合作方下关联的所有贷款金额失败！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11505  前往额度系统查询合作方下关联的所有贷款金额失败！
     */
    public static final String RISK_ERROR_11505 = "前往额度系统查询合作方下关联的所有贷款金额失败！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11506  合作方保证金账户余额不能小于合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例
     */
    public static final String RISK_ERROR_11506 = "合作方保证金账户余额不能小于合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11507  合作方保证金账户余额不能小于合作方台账中保证金账户最低金额
     */
    public static final String RISK_ERROR_11507 = "合作方保证金账户余额不能小于合作方台账中保证金账户最低金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10911 还款日期不允许重复
     */
    public static final String RISK_ERROR_10911 = "还款日期不允许重复！";

    /** 风险拦截执行结果描述 RISK_ERROR_0000 校验通过 */
    public static final String RISK_ERROR_0000 = "校验通过！";

    /** 风险拦截执行结果描述 RISK_ERROR_0001 业务流水号为空 */
    public static final String RISK_ERROR_0001 = "业务流水号为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_0002 授信分项查询异常 */
    public static final String RISK_ERROR_0002 = "授信分项查询异常！";

    /** 风险拦截执行结果描述 RISK_ERROR_0003 授信分项品种查询异常 */
    public static final String RISK_ERROR_0003 = "授信分项品种查询异常！";

    /** 风险拦截执行结果描述 RISK_ERROR_0004 单个授信品种的授信额度大于授信分项的授信额度 */
    public static final String RISK_ERROR_000401 = "单个授信品种的授信额度大于授信分项的授信额度！";

    /** 风险拦截执行结果描述 RISK_ERROR_0004 单个授信品种的授信额度大于授信分项的授信额度 */
    public static final String RISK_ERROR_000402 = "授信分项的授信额度需要大于等于分项下授信品种的授信额度之和！";

    /** 风险拦截执行结果描述 RISK_ERROR_0005 查询授信申请数据异常 */
    public static final String RISK_ERROR_0005 = "查询授信申请数据异常！";

    /** 风险拦截执行结果描述 RISK_ERROR_0006 查询在途校验返回失败 */
    public static final String RISK_ERROR_0006 = "查询在途校验返回失败！";


    /** 风险拦截执行结果描述 RISK_ERROR_0068 单一客户所有标准化产品的授信为在途状态 */
    public static final String RISK_ERROR_0068 = "单一客户所有标准化产品的授信为在途状态！";

    /** 风险拦截执行结果描述 RISK_ERROR_0069 单一客户所有标准化产品的合同申请为在途状态 */
    public static final String RISK_ERROR_0069 = "单一客户所有标准化产品的合同申请为在途状态！";

    /** 风险拦截执行结果描述 RISK_ERROR_0070 单一客户所有标准化产品的中有未结清的借据 */
    public static final String RISK_ERROR_0070 = "单一客户所有标准化产品的中有未结清的借据！";


    /** 风险拦截执行结果描述 RISK_ERROR_0011 宽限期必须为0 */
    public static final String RISK_ERROR_0011 = "宽限期必须为0！";

    /** 风险拦截执行结果描述 RISK_ERROR_0012 只能是循环授信（即合同类型为最高额合同） */
    public static final String RISK_ERROR_0012 = "只能是循环授信（即合同类型为最高额合同）！";

    /** 风险拦截执行结果描述 RISK_ERROR_0013 若担保方式=质押，押品不能是理财产品 */
    public static final String RISK_ERROR_0013 = "若担保方式=质押，押品不能是理财产品！";

    /** 风险拦截执行结果描述 RISK_ERROR_001601 借款人在我行的内部评级（即期信用等级）低于或等于CCC */
    public static final String RISK_ERROR_001601 = "借款人在我行的内部评级（即期信用等级）低于或等于CCC！";

    /** 风险拦截执行结果描述 RISK_ERROR_001602 根据客户号未查询到对应的评级信息 */
    public static final String RISK_ERROR_001602 = "根据客户号未查询到对应的评级信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_001603 财报科目净利润连续2年小于等于0 */
    public static final String RISK_ERROR_001603 = "财报科目净利润连续2年小于等于0！";

    /** 风险拦截执行结果描述 RISK_ERROR_001604 上一年度或者当期资产负债表的资产负债率大于85% */
    public static final String RISK_ERROR_001604 = "上一年度或者当期资产负债表的资产负债率大于85%！";

    /** 风险拦截执行结果描述 RISK_ERROR_001605 未获取到财报科目净利润与资产负债率信息 */
    public static final String RISK_ERROR_001605 = "未获取到财报科目净利润与资产负债率信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_001701 诚易融：授信分项（循环+非循环）总额大于200万 */
    public static final String RISK_ERROR_001701 = "诚易融：授信分项（循环+非循环）总额大于200万！";

    /** 风险拦截执行结果描述 RISK_ERROR_001702 简易融：授信分项（循环+非循环）总额大于500万 */
    public static final String RISK_ERROR_001702 = "简易融：授信分项（循环+非循环）总额大于500万！";

    /** 风险拦截执行结果描述 RISK_ERROR_001703 宿迁园区保：授信分项（循环+非循环）总额大于1000万 */
    public static final String RISK_ERROR_001703 = "宿迁园区保：授信分项（循环+非循环）总额大于1000万！";

    /** 风险拦截执行结果描述 RISK_ERROR_001704 省心快贷：授信分项（循环+非循环）总额大于1000万*/
    public static final String RISK_ERROR_001704 = "省心快贷：授信分项（循环+非循环）总额大于1000万！";

    /** 风险拦截执行结果描述 RISK_ERROR_001705 外贸贷：授信分项（循环+非循环）总额大于1000万 */
    public static final String RISK_ERROR_001705 = "外贸贷：授信分项（循环+非循环）总额大于1000万！";

    /** 风险拦截执行结果描述 RISK_ERROR_001706 结息贷：授信分项（循环+非循环）总额大于200万 */
    public static final String RISK_ERROR_001706 = "结息贷：授信分项（循环+非循环）总额大于200万！";

    /** 风险拦截执行结果描述 RISK_ERROR_001707 优税贷：授信分项（循环+非循环）总额大于300万 */
    public static final String RISK_ERROR_001707 = "优税贷：授信分项（循环+非循环）总额大于300万！";

    /** 风险拦截执行结果描述 RISK_ERROR_1801 集群贷的时候，合作方对应的客户不在白名单管控内 */
    public static final String RISK_ERROR_1801 = "集群贷的时候，合作方对应的客户不在白名单管控内！";

    /** 风险拦截执行结果描述 RISK_ERROR_1802根据业务申请流水号未查询到对应的押品信息 */
    public static final String RISK_ERROR_1802 = "根据业务申请流水号未查询到对应的押品信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_1803 集群贷时，合作方账号不能为空 */
    public static final String RISK_ERROR_1803 = "集群贷时，合作方账号不能为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_1804 集群贷的时候，合作方准入台账未查到记录 */
    public static final String RISK_ERROR_1804 = "集群贷的时候，合作方准入台账未查到记录！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_1501 通过授信申请流水号未获取到对应的授信申请信息！*/
    public static final String RISK_ERROR_1503 = "通过授信申请流水号未获取到对应的授信申请信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_2701 借款人不是对公客户 */
    public static final String RISK_ERROR_1501 = "借款人不是对公客户！";

    /** 风险拦截执行结果描述 RISK_ERROR_2701 外贸贷授信限额不能超过一千万 */
    public static final String RISK_ERROR_1502 = "外贸贷授信限额不能超过一千万！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_1504 通过授信申请流水号未获取到对应的集团成员信息！*/
    public static final String RISK_ERROR_1504 = "通过授信申请流水号未获取到对应的集团成员信息！";



    /**
     * 风险拦截执行结果描述 RISK_ERROR_00102 授信申请担保信息校验时，担保方式不等于信用时，其下必须关联与其匹配的担保品
     */
    public static final String RISK_ERROR_000116="授信申请担保信息校验时，对应的审批类型（bizType不存在）！";





    /**
     * 风险拦截执行结果描述 RISK_ERROR_3201 借款人企业营业执照过期！
     */
    public static final String RISK_ERROR_3201 = "借款人企业营业执照过期！";



    /**
     * 风险拦截执行结果描述 RISK_ERROR_001302 单一客户限额校验计算失败！
     */
    public static final String RISK_ERROR_001302 = " 单一客户限额校验计算失败！ ";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0301 客户为个人客户，则担保方式不能为信用！
     */
    public static final String RISK_ERROR_0301 = "当前借款人或其家庭成员存在于人力资源系统，且客户为个人客户，则担保方式不能为信用！！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0302 人力资源系统，存在当前借款人！
     */
    public static final String RISK_ERROR_0302 = " 人力资源系统，存在当前借款人！ ";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0303 人力资源系统，存在当前借款人家庭成员信息！
     */
    public static final String RISK_ERROR_0303 = " 人力资源系统，存在当前借款人家庭成员信息！ ";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0303 人力资源系统，存在当前借款人家庭成员信息！
     */
    public static final String RISK_ERROR_0304 = " 服务调用异常！ ";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0305 查询客户是否为关联人失败！
     */
    public static final String RISK_ERROR_0305 = "查询客户是否为关联人失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_0501 请联系公司金融总部！
     */
    public static final String RISK_ERROR_0501 = "请联系公司金融总部！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_1401 通过合同编号获取申请信息失败！
     */
    public static final String RISK_ERROR_1401 = "通过合同编号获取申请信息失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_1402 通过申请流水号获取台账信息失败！
     */
    public static final String RISK_ERROR_1402 = "通过申请流水号获取台账信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_00099客户id为空 */
    public static final String RISK_ERROR_0099 = "客户id为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_0100获取企业客户信息失败 */
    public static final String RISK_ERROR_0100 = "通过客户id获取企业客户信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0101通过申请流水号获取合同申请信息失败 */
    public static final String RISK_ERROR_0101 = "通过申请流水号获取合同申请信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0102查询业务与押品关系失败 */
    public static final String RISK_ERROR_0102 = "查询业务与押品关系失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0103查询客户授信调查信息失败 */
    public static final String RISK_ERROR_0103 = "查询客户授信调查信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_010301 查询客户企业信息失败 */
    public static final String RISK_ERROR_010301 = "查询客户企业信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0104通过合同编号获取合同申请信息失败 */
    public static final String RISK_ERROR_0104 = "通过合同编号获取合同申请信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0105通过合同编号获取最高额担保合同信息失败 */
    public static final String RISK_ERROR_0105 = "通过合同编号获取最高额担保合同信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0106获取抵质押信息失败 */
    public static final String RISK_ERROR_0106 = "获取抵质押信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0107获取授信分项与合同申请信息失败 */
    public static final String RISK_ERROR_0107 = "获取授信分项与合同申请信息失败 ！";

    /** 风险拦截执行结果描述 RISK_ERROR_0108获取客户信息失败 */
    public static final String RISK_ERROR_0108 = "通过客户id获取客户信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_03401 */
    public static final String RISK_ERROR_03401 = "个人住房按揭类产品在放款当天不存在报告生成日期为当天的报告，不允许放款！";

    /** 风险拦截执行结果描述 RISK_ERROR_03402 */
    public static final String RISK_ERROR_03402 = "主借款人征信信息不存在！";

    /** 风险拦截执行结果描述 RISK_ERROR_03403 */
    public static final String RISK_ERROR_03403 = "存在主借款人或共同借款人征信未查询！";

    /** 风险拦截执行结果描述 RISK_ERROR_03404 */
    public static final String RISK_ERROR_03404 = "流程编号获取失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_03405 授信业务提交日期与征信报告生成日期超过一个月*/
    public static final String RISK_ERROR_03405 = "授信业务提交日期与征信报告生成日期超过一个月！";

    /** 风险拦截执行结果描述 RISK_ERROR_03406 日期格式转换异常*/
    public static final String RISK_ERROR_03406 = "日期格式转换异常！";

    /** 风险拦截执行结果描述 RISK_ERROR_0035_01 结清出库时，权证关联合同必须为结清状态*/
    public static final String RISK_ERROR_0035_01 = "结清出库时，权证关联合同必须为结清状态！";

    /** 风险拦截执行结果描述 RISK_ERROR_0035_02 未结清出库时，权证关联合同必须为非结清状态*/
    public static final String RISK_ERROR_0035_02 = "未结清出库时，权证关联合同必须为非结清状态！";

    /** 风险拦截执行结果描述 RISK_ERROR_0035_03 未结清出库时，权证关联的借款台账状态既没有已核销也没有转让*/
    public static final String RISK_ERROR_0035_03 = "权证出库原因细类为其他时，权证关联的借款台账状态既没有已核销也没有转让！";

    /** 风险拦截执行结果描述 RISK_ERROR_0036 权证更新出库时，权证关联的合同对应的产品必须为个人一手住房按揭贷款或个人一手商用房按揭贷款*/
    public static final String RISK_ERROR_0036 = "权证更新出库时，权证关联的合同对应的产品必须为个人一手住房按揭贷款或个人一手商用房按揭贷款！";

    /** 风险拦截执行结果描述 RISK_ERROR_0037 结清出库时，权证对应合同不能有在途的出账申请*/
    public static final String RISK_ERROR_0037 = "结清出库时，权证对应合同不能有在途的出账申请！";

    /** 风险拦截执行结果描述 RISK_ERROR_03801 合同金额大于分项金额*/
    public static final String RISK_ERROR_03801 = "合同金额大于分项金额！";

    /** 风险拦截执行结果描述 RISK_ERROR_03802 获取最高协议申请信息失败*/
    public static final String RISK_ERROR_03802 = "获取最高协议申请信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_03803 额度信息获取失败*/
    public static final String RISK_ERROR_03803 = "额度信息获取失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_03901 未录入保证金账户*/
    public static final String RISK_ERROR_03901 = "未录入保证金账户！";

    /** 风险拦截执行结果描述 RISK_ERROR_03901 通过业务流水号流水号未查询到对应的合同申情详情*/
    public static final String RISK_ERROR_03902 = "通过业务流水号流水号未查询到对应的合同申情详情！";

    /** 风险拦截执行结果描述 RISK_ERROR_04001 根据授信申请细化流水号未找到对应的全第二还款来源*/
    public static final String RISK_ERROR_04001 = "根据授信申请细化流水号未找到对应的第二还款来源！";

    /** 风险拦截执行结果描述 RISK_ERROR_0046 权证信息未录入*/
    public static final String RISK_ERROR_046 = "权证信息未录入！";

    /** 风险拦截执行结果描述 RISK_ERROR_04501 通过申请流水号获取产品ID失败*/
    public static final String RISK_ERROR_04501 = "通过申请流水号获取产品ID失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_04501 未获取到还款能力分析结论！*/
    public static final String RISK_ERROR_05201 = "未获取到还款能力分析结论！";

    /** 风险拦截执行结果描述 RISK_ERROR_04501 还款能力分析结论信息不全*/
    public static final String RISK_ERROR_05202 = "还款能力分析结论信息不全！";

    /** 风险拦截执行结果描述 RISK_ERROR_04501 该笔贷款月支出与家庭月收入比不能大于50%*/
    public static final String RISK_ERROR_05203 = "该笔贷款月支出与家庭月收入比不能大于50%！";

    /** 风险拦截执行结果描述 RISK_ERROR_04501 所有消费贷款月支出与家庭月收入比不能大于55%*/
    public static final String RISK_ERROR_05204 = "所有消费贷款月支出与家庭月收入比不能大于55%！";

    /** 风险拦截执行结果描述 RISK_ERROR_04502 通过申请流水号未获取完整房屋信息！请补充*/
    public static final String RISK_ERROR_04502 = "通过申请流水号未获取完整房屋信息！请补充！";

    /** 风险拦截执行结果描述 RISK_ERROR_04503 住房贷款首付款比例小于20%*/
    public static final String RISK_ERROR_04503 = "住房贷款首付款比例小于20%！";

    /** 风险拦截执行结果描述 RISK_ERROR_04504 贷款金额+首付金额>房屋总价*/
    public static final String RISK_ERROR_04504 = "贷款金额+首付金额>房屋总价！";

    /** 风险拦截执行结果描述 RISK_ERROR_04505 通过申请流水号获取申请信息失败*/
    public static final String RISK_ERROR_04505 = "通过申请流水号获取申请信息失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_04505 个人住房按揭类贷款，担保方式不能为保证*/
    public static final String RISK_ERROR_04517 = "个人住房按揭类贷款，担保方式不能为保证！";

    /** 风险拦截执行结果描述 RISK_ERROR_04506 非按揭类贷款首付款比例小于30%*/
    public static final String RISK_ERROR_04506 = "非按揭类贷款首付款比例小于30%！";

    /** 风险拦截执行结果描述 RISK_ERROR_04518 如果为公积金组合贷款，首付+商贷+公积金贷款不能大于房价*/
    public static final String RISK_ERROR_04518 = "如果为公积金组合贷款，首付+商贷+公积金贷款不能大于房价！";
    /** 风险拦截执行结果描述 RISK_ERROR_04507 展期期限不得小于原到期日*/
    public static final String RISK_ERROR_04507 = "展期期限不得小于原到期日！";
    /** 风险拦截执行结果描述 RISK_ERROR_04508 原贷款期限小于等于1年（含），展期期限需小于等于原贷款期限*/
    public static final String RISK_ERROR_04508 = "原贷款期限小于等于1年（含），展期期限需小于等于原贷款期限！";
    /** 风险拦截执行结果描述 RISK_ERROR_04509 原贷款期限在1年以上（不含1年）5年以下（含5年）的贷款，展期期限需小于等于原贷款期限的一半*/
    public static final String RISK_ERROR_04509 = "原贷款期限在1年以上（不含1年）5年以下（含5年）的贷款，展期期限需小于等于原贷款期限的一半！";
    /** 风险拦截执行结果描述 RISK_ERROR_04510 原贷款期限在5年（不含）以上的贷款，展期期限累计不得超过3年*/
    public static final String RISK_ERROR_04510 = "原贷款期限在5年（不含）以上的贷款，展期期限累计不得超过3年！";

    /** 风险拦截执行结果描述 RISK_ERROR_06701 调查报告类型为空！*/
    public static final String RISK_ERROR_06701 = "调查报告类型为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_07101 授信申请信息查询失败！*/
    public static final String RISK_ERROR_07101 = "授信申请信息查询失败！";
    /** 风险拦截执行结果描述 RISK_ERROR_07102 穿透到底层查询底层额度信息失败！*/
    public static final String RISK_ERROR_07102 = "穿透到底层查询底层额度信息失败！";
    /** 风险拦截执行结果描述 RISK_ERROR_07103 穿透到底层,是否申报底层授信不允许为空！*/
    public static final String RISK_ERROR_07103 = "穿透到底层,是否申报底层授信不允许为空！";
    /** 风险拦截执行结果描述 RISK_ERROR_07104 产品授信，未查到需占用的同业投资类额度和同业管理类额度！*/
    public static final String RISK_ERROR_07104 = "产品授信，未查到需占用的同业投资类额度和同业管理类额度！";
    /** 风险拦截执行结果描述 RISK_ERROR_07105 产品授信，查到多条同业投资类额度或同业管理类额度！*/
    public static final String RISK_ERROR_07105 = "产品授信，查到多条同业投资类额度或同业管理类额度！";
    /** 风险拦截执行结果描述 RISK_ERROR_07106 产品授信，查到多条同业投资类额度或同业管理类额度！*/
    public static final String RISK_ERROR_07106 = "产品授信，占用同业投资类额度或同业管理类额度状态不可用！";
    /** 风险拦截执行结果描述 RISK_ERROR_07107 产品授信，查到多条同业投资类额度或同业管理类额度！*/
    public static final String RISK_ERROR_07107 = "产品授信，占用同业投资类额度或同业管理类额度可用余额不足！";
    /** 风险拦截执行结果描述 RISK_ERROR_07108 产品授信，查到多条同业投资类额度或同业管理类额度！*/
    public static final String RISK_ERROR_07108 = "产品授信，调用CmisLmt0015接口查询同业投资类额度或同业管理类额度失败！";

    /** 风险拦截执行结果描述 RISK_ERROR_0076 权证置换出库时，押品关联担保合同必须为解除状态*/
    public static final String RISK_ERROR_0076 = "权证出库原因细类为押品置换出库或部分出库时，押品关联担保合同必须为解除状态！";

    /**
     * 审查核查报告校验  RISK_ERROR_06601  审查报告未填写，请填写保存后继续操作！
     */
    public static final String RISK_ERROR_06601 = "审查报告未填写，请填写保存后继续操作！";

    /**
     * 审查核查报告校验 RISK_ERROR_06602 核查报告未填写，请填写保存后继续操作！
     */
    public static final String RISK_ERROR_06602 = "核查报告未填写，请填写保存后继续操作！";

    /**
     * 审查核查报告校验 RISK_ERROR_06603 批复报告未填写，请填写保存后继续操作！
     */
    public static final String RISK_ERROR_06603 = "批复报告未填写，请填写保存后继续操作！";

    /**
     * 审查核查报告校验 RISK_ERROR_06603 核查报告模式 2 上传WORD版核查报告！
     */
    public static final String RISK_ERROR_06604 = "核查报告模式为 WORD版核查报告，请上传核查报告！";

    /**
     * 审查核查报告校验 RISK_ERROR_06605 主体分析未填写，请填写后继续操作！
     */
    public static final String RISK_ERROR_06605 = "主体分析未填写，请填写后继续操作！";

    /** 风险拦截执行结果描述 RISK_ERROR_05701 未定制还本计划！*/
    public static final String RISK_ERROR_05701 = "未定制还本计划！";

    /** 风险拦截执行结果描述 RISK_ERROR_05702 还本金额与出账金额不一致！*/
    public static final String RISK_ERROR_05702 = "还本金额与出账金额不一致！";

    /** 风险拦截执行结果描述 RISK_ERROR_07301 未完善交易对手信息！*/
    public static final String RISK_ERROR_07301 = "未完善交易对手信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_07302 交易对手总额与出账金额不一致！*/
    public static final String RISK_ERROR_07302 = "交易对手总额与出账金额不一致！";

    /** 风险拦截执行结果描述 RISK_ERROR_07302 借款用途必须为购车，贷款类别必须为消费-汽车贷款！*/
    public static final String RISK_ERROR_07501 = "借款用途必须为购车，贷款类别必须为消费-汽车贷款，担保方式细分为抵押-其他！";

    /** 风险拦截执行结果描述 RISK_ERROR_07302 借款用途类型，贷款类别细分或担保方式细分不能为空！*/
    public static final String RISK_ERROR_07502 = "借款用途类型，贷款类别细分或担保方式细分不能为空！";
    /**
     * 风险拦截执行结果描述 RISK_ERROR_08101 XXX页面要素未录入完整，请补充完整!
     */
    public static final String RISK_ERROR_08101 = "页面要素未录入完整，请补充完整!";

    /** 风险拦截执行结果描述 RISK_ERROR_07302 个人商用房贷款 申请金额最高不得大于房价的50%！*/
    public static final String RISK_ERROR_08201 = "个人商用房贷款 申请金额最高不得大于房价的50%！";

    /** 风险拦截执行结果描述 RISK_ERROR_07302 房屋信息不全，请补充！*/
    public static final String RISK_ERROR_08202 = "房屋信息不全，请补充！";

    /** 风险拦截执行结果描述 RISK_ERROR_07302 个人商用房贷款最长期限120个月！*/
    public static final String RISK_ERROR_08203 = "个人商用房贷款最长期限120个月！";
    /** 风险拦截执行结果描述 RISK_ERROR_08301 若支付方式为受托支付，交易对手账号不可为空！*/
    public static final String RISK_ERROR_08301 = "若支付方式为受托支付，交易对手账号不可为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_08302 若支付方式为受托支付，交易对手账号与发放贷款账号不能重复！*/
    public static final String RISK_ERROR_08302 = "若支付方式为受托支付，交易对手账号与发放贷款账号不能重复！";

    /** 风险拦截执行结果描述 RISK_ERROR_08303 受托交易金额之和必须等于放款金额！*/
    public static final String RISK_ERROR_08303 = "受托交易金额之和必须等于放款金额！";

    /** 风险拦截执行结果描述 RISK_ERROR_08304 受受托账号存在相同账号！*/
    public static final String RISK_ERROR_08304 = "受托账号存在相同账号！";

    /** 风险拦截执行结果描述 RISK_ERROR_08305 受托交易金额之和必须等于合同金额！*/
    public static final String RISK_ERROR_08305 = "受托交易金额之和必须等于合同金额！";
    /** 风险拦截执行结果描述 RISK_ERROR_04511 信保贷,徐信保,无锡园区保,宿迁园区保,南通信保通,青岛即墨政银保产品出账环节受托支付只能为是*/
    public static final String RISK_ERROR_04511 = "该产品是否受托支付只能为是！";
    /** 风险拦截执行结果描述 RISK_ERROR_04512 信保贷,企业信用评分是否650分（含）以上*/
    public static final String RISK_ERROR_04512 = "信保贷产品企业信用评分在650分（含）以下！";
    /** 风险拦截执行结果描述 RISK_ERROR_04513 贷款起始日、授信复批日都不能为空*/
    public static final String RISK_ERROR_04513 = "贷款起始日、授信复批日都不能为空！";
    /** 风险拦截执行结果描述 RISK_ERROR_04514 借据起始日超过授信批复日30天（含）以上，未引入人行征信报告及苏州地方征信报告*/
    public static final String RISK_ERROR_04514 = "借据起始日超过授信批复日30天（含）以上，未引入人行征信报告及苏州地方征信报告！";
    /** 风险拦截执行结果描述 RISK_ERROR_04515 征信报告生成日期为空！*/
    public static final String RISK_ERROR_04515 = "征信报告生成日期为空！";
    /** 风险拦截执行结果描述 RISK_ERROR_04516 征信报告生成日期超过放款起始日30日（含）*/
    public static final String RISK_ERROR_04516 = "征信报告生成日期超过放款起始日30日（含）！";

    /** 风险拦截执行结果描述 RISK_ERROR_04516 业务申请金额要小于或等于第三方的可用额度*/
    public static final String RISK_ERROR_07801 = "业务申请金额要小于或等于第三方的可用额度！";

    /** 风险拦截执行结果描述 RISK_ERROR_04516 放款金额要小于或等于第三方的可用额度*/
    public static final String RISK_ERROR_07802 = "放款金额要小于或等于第三方的可用额度！";

    /** 风险拦截执行结果描述 RISK_ERROR_05301 通过授信申请流水号未获取到对应的房屋信息*/
    public static final String RISK_ERROR_05301 = "通过授信申请流水号未获取到对应的房屋信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_05302 放款金额要小于或等于第三方的可用额度*/
    public static final String RISK_ERROR_05302 = "贷款期限不可为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_05303 个人和住房按揭贷款期限最长不超过30年*/
    public static final String RISK_ERROR_05303 = "个人和住房按揭贷款期限最长不超过30年！";

    /** 风险拦截执行结果描述 RISK_ERROR_05304 客户证件号码不可为空*/
    public static final String RISK_ERROR_05304 = "客户证件号码不可为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_05305 贷款期限+客户年龄不能大于70周岁*/
    public static final String RISK_ERROR_05305 = "贷款期限+客户年龄不能大于70周岁！";

    /** 风险拦截执行结果描述 RISK_ERROR_05306 房龄不可为空*/
    public static final String RISK_ERROR_05306 = "房龄不可为空！";

    /** 风险拦截执行结果描述 RISK_ERROR_05307 放款金额要小于或等于第三方的可用额度*/
    public static final String RISK_ERROR_05307 = "贷款期限+房龄必需小于或等于50年！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09801 机构信息获取失败！
     */
    public static final String RISK_ERROR_09801 = "机构信息获取失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09802 本次贷款放款申请金额 超过了 当月剩余可投放对公贷款余额！
     */
    public static final String RISK_ERROR_09802 = "本次贷款放款申请金额 超过了 当月剩余可投放对公贷款余额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09803 当月剩余可投放对公贷款余额获取失败！
     */
    public static final String RISK_ERROR_09803 = "当月剩余可投放对公贷款余额获取失败！";
    /**
     * 风险拦截执行结果描述 RISK_ERROR_10001 借款人名下存在已核销呆账！
     */
    public static final String RISK_ERROR_10001 = "借款人名下存在已核销呆账！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10002 借款人名下存在五级分类为损失的消费贷款！
     */
    public static final String RISK_ERROR_10002 = "借款人名下存在五级分类为损失的消费贷款！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10003 借款人客户编号获取失败！
     */
    public static final String RISK_ERROR_10003 = "借款人客户编号获取失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09901 机构信息获取失败！
     */
    public static final String RISK_ERROR_09901 = "机构信息获取失败！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_09902 业务条线及机构类型对应当月剩余可投放贷款余额
     */
    public static final String RISK_ERROR_09902 = "本业务条线及机构类型对应当月剩余可投放贷款余额获取失败！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_09903 贷款放款申请时，判断本次贷款放款申请金额>业务条线及机构类型对应当月剩余可投放贷款余额
     */
    public static final String RISK_ERROR_09903 = "本次贷款放款申请金额 超过了 业务条线及机构类型对应当月剩余可投放贷款余额！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_099035 风控自动化审批尚未完成，请耐心等待
     */
    public static final String RISK_ERROR_09905 = "风控自动化审批尚未完成，请耐心等待";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10501  未获取到债项评级信息
     */
    public static final String RISK_ERROR_10501 = "通过授信申请流水号未获取到对应的债项评级信息！";
    /**
     *  风险拦截执行结果描述 RISK_ERROR_10502  未获取到债项评级信息
     */
    public static final String RISK_ERROR_10502 = "该客户下的违约概率*违约损失率>5.5%，且该笔授信申请数据不在内评低准入例外审批表中为审批通过状态";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10503  违约概率PD为空！
     */
    public static final String RISK_ERROR_10503 = "违约概率PD为空！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10504  违约概率PD为空！
     */
    public static final String RISK_ERROR_10504 = "违约损失率LGD为空！";

     /* 风险拦截执行结果描述 RISK_ERROR_09904 通过贷款申请流水号未获取到对应的贷款申请信息！*/
    public static final String RISK_ERROR_09904 = "通过贷款申请流水号未获取到对应的贷款申请信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10401  未查询到合作方详细信息！
     */
    public static final String RISK_ERROR_10401 = "未查询到合作方详细信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10402  未查询到合作方保证金余额信息！
     */
    public static final String RISK_ERROR_10402 = "未查询到合作方保证金余额信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10403  前往额度系统查询合作方下关联的所有贷款金额失败！
     */
    public static final String RISK_ERROR_10403 = "前往额度系统查询合作方下关联的所有贷款金额失败！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10404  合作方保证金账户余额不能小于合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例
     */
    public static final String RISK_ERROR_10404 = "合作方保证金账户余额不能小于合作方下关联的所有贷款金额 * 合作方保证金比例 + 本笔贷款发放金额 * 合作方保证金比例";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10405  未查询到合作方详细信息！
     */
    public static final String RISK_ERROR_10405 = "未查询到合作方账户信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_10406  本次还款计划设置还款金额之和不等于本次申请放款金额！
     */
    public static final String RISK_ERROR_10406 = "本次还款计划设置还款金额之和不等于本次申请放款金额！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11001  批复编号不能为空！
     */
    public static final String RISK_ERROR_11001 = "批复编号不能为空！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11002  通过授信批复编号未查询到授信批复信息！
     */
    public static final String RISK_ERROR_11002 = "通过授信批复编号未查询到授信批复信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11003  放款申请还款方式与授信批复还款方式不一致！
     */
    public static final String RISK_ERROR_11003 = "放款申请还款方式与授信批复还款方式不一致！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11101  经查询，暂无协办客户经理，请先维护客户经理分成比例录入！
     */
    public static final String RISK_ERROR_11101 = "经查询，暂无协办客户经理，请先维护客户经理分成比例录入！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11201  合同项下存在抵押顺位或是否浮动抵押未选择的担保合同！
     */
    public static final String RISK_ERROR_11201 = "合同项下存在抵押顺位或是否浮动抵押未选择的担保合同！";

    /** 风险拦截执行结果描述 RISK_ERROR_05401 拍卖贷担保方式只能为抵押、保证、质押3种！*/
    public static final String RISK_ERROR_05401 = "拍卖贷担保方式只能为抵押、保证、质押3种！";

    /** 风险拦截执行结果描述 RISK_ERROR_05402 拍卖贷选用公积金组合贷款，担保方式是只能是保证！*/
    public static final String RISK_ERROR_05402 = "拍卖贷选用公积金组合贷款，担保方式是只能是保证！";

    /** 风险拦截执行结果描述 RISK_ERROR_05403 非拍卖贷住房按揭类，担保方式只能是抵押！*/
    public static final String RISK_ERROR_05403 = "非拍卖贷住房按揭类，担保方式只能是抵押！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11202  小企业无还本续贷业务限额10亿！
     */
    public static final String RISK_ERROR_11202 = "小企业无还本续贷业务限额10亿！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_11203  额度状态校验！
     */
    public static final String RISK_ERROR_11203 = "额度只能是【正常】状态！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_05001  贷款金额必须小于等于抵押物评估金额的70%！
     */
    public static final String RISK_ERROR_05001 = "贷款金额必须小于等于抵押物评估金额的70%！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_05002  通过申请流水号未查询到关联担保品！
     */
    public static final String RISK_ERROR_05002 = "通过申请流水号未查询到关联担保品！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_01190  通过出账流水号未找到或找到多笔关联的借据！
     */
    public static final String RISK_ERROR_01190 = "通过出账流水号未找到或找到多笔关联的借据！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_01191  通过申请流水号未查询到关联的测算信息！
     */
    public static final String RISK_ERROR_01191 = "通过申请流水号未查询到关联的测算信息！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_01192  借新本金必须小于等于出账金额！
     */
    public static final String RISK_ERROR_01192 = "借新本金必须等于本次出账金额！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_01193  借新本金+自还本金必须等于未到期本金+拖欠本金！
     */
    public static final String RISK_ERROR_01193 = "借新本金+自还本金必须等于未到期本金+拖欠本金！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_01194  该客户用信需经信贷管理部审核！
     */
    public static final String RISK_ERROR_01194 = "该客户用信需经信贷管理部审核！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_012201  信保贷信息不完整！
     */
    public static final String RISK_ERROR_012201 = "信保贷信息不完整！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_012301  保证金信息缺失！
     */
    public static final String RISK_ERROR_012301 = "保证金信息缺失！";

    /**
     *  风险拦截执行结果描述 RISK_ERROR_012302  保证金金额不正确！
     */
    public static final String RISK_ERROR_012302 = "保证金金额不正确！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_011701 根据合同号未找到合同信息
     */
    public static final String RISK_ERROR_011701="根据合同号未找到合同信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_00091 通过授信分项编号未获取到分项下品种信息！*/
    public static final String RISK_ERROR_00091 = "通过授信分项编号未获取到分项下品种信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_00092 借款人以个人名义申请的经营性贷款，若贷款到期日时点借款人年龄超过65周岁！  */
    public static final String RISK_ERROR_00092= "借款人以个人名义申请的经营性贷款，若贷款到期日时点借款人年龄超过65周岁！";

    /** 风险拦截执行结果描述 RISK_ERROR_00093 未查询到征信贷评分卡信息！  */
    public static final String RISK_ERROR_00093= "未查询到征信贷评分卡信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_00094 征信贷产品授信额度大于征信评分卡中通过的借款人预期借款额度！  */
    public static final String RISK_ERROR_00094= "征信贷产品授信额度大于征信评分卡中通过的借款人预期借款额度！";

    /** 风险拦截执行结果描述 RISK_ERROR_00095 该客户存在到期未完成的压降任务！  */
    public static final String RISK_ERROR_00095= "该客户存在到期未完成的压降任务！";

    /** 风险拦截执行结果描述 RISK_ERROR_1901 借款人名下存在已生效优企贷或优农贷 */
    public static final String RISK_ERROR_1901 = "借款人名下存在已生效优企贷或优农贷批复信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_1902 未查到借款人证件信息 */
    public static final String RISK_ERROR_1902 = "未查到借款人证件信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002301 最高额担保合同可用金额不足！
     */
    public static final String RISK_ERROR_002301="最高额担保合同可用金额不足！";

    /** 风险拦截执行结果描述 RISK_ERROR_0012 银票存在于贴现台账，不得重复使用 */
    public static final String RISK_ERROR_00012 = "银票存在于贴现台账，不得重复使用！";

    /** 风险拦截执行结果描述 RISK_ERROR_012701 银承合同申请未录入票据明细 */
    public static final String RISK_ERROR_012701 = "银承合同申请未录入票据明细！";

    /** 风险拦截执行结果描述 RISK_ERROR_012702 银承合同申请票据总金额不等于申请金额 */
    public static final String RISK_ERROR_012702 = "银承合同申请票据总金额不等于申请金额！";

    /** 风险拦截执行结果描述 RISK_ERROR_012901 借款人在风险预警管理平台黑名单列表中 */
    public static final String RISK_ERROR_012901 = "借款人在风险预警管理平台黑名单列表中！";

    /** 风险拦截执行结果描述 RISK_ERROR_013001 请补充调查报告和评分卡 */
    public static final String RISK_ERROR_013001 = "结息贷评分卡校验_请补充调查报告和评分卡！";

    /** 风险拦截执行结果描述 RISK_ERROR_013002 评分卡分数未满70分不予准入 */
    public static final String RISK_ERROR_013002 = "结息贷评分卡校验_评分卡分数未满70分不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013003 实际控制人有吸毒、赌博等不良嗜好，其信用卡经常在境外大额支付等 若是，则拦截 */
    public static final String RISK_ERROR_013003 = "结息贷评分卡校验_评分卡【实际控制人有吸毒、赌博等不良嗜好，其信用卡经常在境外大额支付等】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013004 企业及实际控制人贷款银行超过3家 */
    public static final String RISK_ERROR_013004 = "结息贷评分卡校验_评分卡【企业及实际控制人贷款银行超过3家】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013005 企业及实际控制人他行信用贷款授信额+我行信用贷款+信用卡用信额+结息贷申请额大于500万元 */
    public static final String RISK_ERROR_013005 = "结息贷评分卡校验_评分卡【企业及实际控制人他行信用贷款授信额+我行信用贷款+信用卡用信额+结息贷申请额大于500万元】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013006 企业自制报表资产负债率超过75% */
    public static final String RISK_ERROR_013006 = "结息贷评分卡校验_评分卡【企业自制报表资产负债率超过75%】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013007 企业成立时间<=3年% */
    public static final String RISK_ERROR_013007 = "结息贷评分卡校验_评分卡【企业成立时间<=3年%】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013101 请补充调查报告和评分卡 */
    public static final String RISK_ERROR_013101 = "优税贷评分卡校验_请补充调查报告和评分卡！";

    /** 风险拦截执行结果描述 RISK_ERROR_013102 评分卡分数未满70分不予准入 */
    public static final String RISK_ERROR_013102 = "优税贷评分卡校验_评分卡分数未满70分不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013103 纳税评级小于B，且上年度纳税总额小于10万元，则拦截 */
    public static final String RISK_ERROR_013103 = "优税贷评分卡校验_评分卡【纳税评级小于B，且上年度纳税总额小于10万元】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013104 存在工商，税务，银行等不良记录 */
    public static final String RISK_ERROR_013104 = "优税贷评分卡校验_评分卡【存在工商，税务，银行等不良记录】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013105 银行家数超过3家或资产负债率超过75% */
    public static final String RISK_ERROR_013105 = "优税贷评分卡校验_评分卡【银行家数超过3家或资产负债率超过75%】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013106 企业或法人及其家庭没有在业务申办行本地有住宅，商铺，厂房等实物资产 是则拦截 */
    public static final String RISK_ERROR_013106 = "优税贷评分卡校验_评分卡【企业或法人及其家庭没有在业务申办行本地有住宅，商铺，厂房等实物资产 是则拦截】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013107 企业成立时间<=3年% */
    public static final String RISK_ERROR_013107 = "优税贷评分卡校验_评分卡【企业成立时间<=3年%】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013108 企业及实际控制人他行信用贷款余额，我行信用贷款余额，信用卡用信额，优税贷申请额(含小贷)合计是否高于500万元 */
    public static final String RISK_ERROR_013108 = "优税贷评分卡校验_评分卡【企业及实际控制人他行信用贷款余额，我行信用贷款余额，信用卡用信额，优税贷申请额(含小贷)合计是否高于500万元】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013109 实际控制人有吸毒、赌博等不良嗜好，其信用卡经常在境外大额支付等 */
    public static final String RISK_ERROR_013109 = "优税贷评分卡校验_评分卡【实际控制人有吸毒、赌博等不良嗜好，其信用卡经常在境外大额支付等】内容不满足不予准入！";

    /** 风险拦截执行结果描述 RISK_ERROR_013109 担保合同信息未完善 */
    public static final String RISK_ERROR_0023001 = "担保合同信息未完善！";

    /** 风险拦截执行结果描述 RISK_ERROR_011501 根据业务申请流水号未查询到对应的出账申请信息*/
    public static final String RISK_ERROR_011501 = "根据业务申请流水号未查询到对应的出账申请信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_009701 合同申请的执行年利率不能小于对应授信品种利率
     */
    public static final String RISK_ERROR_009701 = "合同申请的执行年利率不能小于对应授信品种利率！";

    /** 风险拦截执行结果描述 RISK_ERROR_0013301 未填写参与行信息！*/
    public static final String RISK_ERROR_0013301 = "未填写参与行信息！";

    /** 风险拦截执行结果描述 RISK_ERROR_0013302 参与比例必须小于等于100！*/
    public static final String RISK_ERROR_0013302 = "参与比例必须小于等于100%！";

    /** 风险拦截执行结果描述 RISK_ERROR_0013303 参与行合同总金额必须等于银团总金额！*/
    public static final String RISK_ERROR_0013303 = "所有参与行合同总金额必须等于银团总金额！";

    /** 风险拦截执行结果描述 RISK_ERROR_0013304 参与行总参与比例必须等于100%！*/
    public static final String RISK_ERROR_0013304 = "所有参与行总参与比例必须等于100%！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_010601 省心快贷自动化审批未通过，提交人工审批！
     */
    public static final String RISK_ERROR_010601 = "省心快贷自动化审批未通过，提交人工审批！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_010602 非省心快贷自动化审批模式，拦截项不校验！
     */
    public static final String RISK_ERROR_010602 = "非省心快贷自动化审批模式，拦截项不校验！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_011702 特定产品:房抵e点贷业务须通知风控推送合同且通过影像审核流程！请确认！
     */
    public static final String RISK_ERROR_011702 = "特定产品:房抵e点贷业务须通知风控推送合同且通过影像审核流程！请确认！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_002201  押品信息不完整，请前往押品系统完善押品信息！
     */
    public static final String RISK_ERROR_002201 = "押品信息不完整，请前往押品系统完善押品信息！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_10915 所有期数还款金额之和等于出账金额
     */
    public static final String RISK_ERROR_10915 = "所有期数还款金额之和等于出账金额！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_009702 贷款出账申请的执行年利率不能小于对应授信品种利率
     */
    public static final String RISK_ERROR_009702 = "贷款出账申请的执行年利率不能小于对应授信品种利率！";

    /**
     * 风险拦截执行结果描述 RISK_ERROR_009703 委托贷款出账申请的执行年利率不能小于对应授信品种利率
     */
    public static final String RISK_ERROR_009703 = "委托贷款出账申请的执行年利率不能小于对应授信品种利率！";
}

