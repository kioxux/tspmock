package cn.com.yusys.yusp.dto.client.http.ciis2nd.callciis2nd;

// 二代征信影像url调用参数
public class CallCiis2ndReqDto {

    /**
     * URL前缀，示例http://10.28.122.198/zjg-ws/controller/
     */
    private String prefixUrl;

    /**
     * 报告类型（个人、对公）枚举类 CmisDocEnum
     */
    private String reportType;

    /**
     * 业务明细ID
     */
    private String reqId;

    /**
     * 部门名称
     */
    private String orgName;

    /**
     * 查看人工号
     */
    private String userCode;

    /**
     * 查看人名称
     */
    private String userName;

    public CallCiis2ndReqDto() {
        // do nothing
    }

    public String getPrefixUrl() {
        return prefixUrl;
    }

    public void setPrefixUrl(String prefixUrl) {
        this.prefixUrl = prefixUrl;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
