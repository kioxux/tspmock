package cn.com.yusys.yusp.dto.server;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷作为服务端交易返回Dto
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class TradeServerRespDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 响应码
     */
    @JsonProperty(value = "erorcd")
    private String erorcd;

    /**
     * 响应信息
     */
    @JsonProperty(value = "erortx")
    private String erortx;

    /**
     * 全局流水
     */
    @JsonProperty(value = "datasq")
    private String datasq;

    public TradeServerRespDto() {
        // do nothing
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    @Override
    public String toString() {
        return "TradeServerRespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", datasq='" + datasq + '\'' +
                '}';
    }
}
