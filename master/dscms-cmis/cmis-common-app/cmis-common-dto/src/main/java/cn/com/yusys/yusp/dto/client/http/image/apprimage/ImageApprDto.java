package cn.com.yusys.yusp.dto.client.http.image.apprimage;

import java.io.Serializable;

/**
 * @version 1.0.0
 * @项目名称: cmis-common模块
 * @类名称: ImageApprDto
 * @类描述: #服务类
 * @功能描述:
 * @创建人: 王玉坤
 * @创建时间: 2021-10-08 20:46:41
 * @修改备注:
 * @修改记录: 修改时间    修改人员    修改原因
 * -------------------------------------------------------------
 * @Copyright (c) 宇信科技-版权所有
 */
public class ImageApprDto implements Serializable {
    private static final long serialVersionUID = 1L;
    // 任务编号
    private String docId;
    // 审批状态1通过-1不通过3作废
    private String isApproved;
    // 审批意见
    private String approval;
    // 文件类型根节点
    private String outcode;
    // 审批人员
    private String opercode;
    // 批次号
    private String batchNo;

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(String isApproved) {
        this.isApproved = isApproved;
    }

    public String getApproval() {
        return approval;
    }

    public void setApproval(String approval) {
        this.approval = approval;
    }

    public String getOutcode() {
        return outcode;
    }

    public void setOutcode(String outcode) {
        this.outcode = outcode;
    }

    public String getOpercode() {
        return opercode;
    }

    public void setOpercode(String opercode) {
        this.opercode = opercode;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }
}
