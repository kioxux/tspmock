package cn.com.yusys.yusp.dto.client.http.image.callimage;

import java.util.Map;

// 传递json值，json值规范参照【2.2.2 para数据报文格式】
public class Para {

    /**
     * 索引信息json串，该值根据各个业务模块生成
     */
    private Map<String, Object> index;

    /**
     * 第三方系统业务类型根节点编码，该值根据各个业务模块生成
     */
    private String top_outsystem_code;

    /**
     * 第三方系统业务类型子节点编码，该节点填写，只能查看所列节点的影像；不填写，显示所有根节点下的子节点，该值根据各个业务模块生成
     */
    private String outsystem_code;
    /**
     *  操作类型：	0.正常-显示红色	2.补录-显示粉红色	3.退回-显示橘黄色
     */
    private String opType;

    /**
     * 第三方系统业务类型子节点参数控制
     */
    private Map<String, Object> version;

    public Map<String, Object> getVersion() {
        return version;
    }

    public void setVersion(Map<String, Object> version) {
        this.version = version;
    }

    public Para() {
        // do nothing
    }

    public Map<String, Object> getIndex() {
        return index;
    }

    public void setIndex(Map<String, Object> index) {
        this.index = index;
    }

    public String getTop_outsystem_code() {
        return top_outsystem_code;
    }

    public void setTop_outsystem_code(String top_outsystem_code) {
        this.top_outsystem_code = top_outsystem_code;
    }

    public String getOutsystem_code() {
        return outsystem_code;
    }

    public void setOutsystem_code(String outsystem_code) {
        this.outsystem_code = outsystem_code;
    }

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }
}
