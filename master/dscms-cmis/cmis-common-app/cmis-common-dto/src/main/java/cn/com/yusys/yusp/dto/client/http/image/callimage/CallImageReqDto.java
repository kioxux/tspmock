package cn.com.yusys.yusp.dto.client.http.image.callimage;

import java.util.List;

/**
 * 请求DTO：获取影像采集和查看页面
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class CallImageReqDto {

    /**
     * URL前缀，示例http://10.28.122.198/image.html
     */
    private String prefixUrl;

    private List<Para> para;

    /**
     * Token值
     */
    private String authorization;

    /**
     * 1-采集 2-查看
     */
    private String s;

    /**
     * 权限按钮控制，默认没有任何权限；每个权限用分号;隔开
     */
    private String authority;

    public CallImageReqDto() {
        // do nothing
    }

    public String getPrefixUrl() {
        return prefixUrl;
    }

    public void setPrefixUrl(String prefixUrl) {
        this.prefixUrl = prefixUrl;
    }

    public List<Para> getPara() {
        return para;
    }

    public void setPara(List<Para> para) {
        this.para = para;
    }

    public String getAuthorization() {
        return authorization;
    }

    public void setAuthorization(String authorization) {
        this.authorization = authorization;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "CallImageReqDto{" +
                "prefixUrl='" + prefixUrl + '\'' +
                ", para=" + para +
                ", Authorization='" + authorization + '\'' +
                ", s='" + s + '\'' +
                ", authority='" + authority + '\'' +
                '}';
    }
}