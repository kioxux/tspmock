package cn.com.yusys.yusp.dto.client.http.ypxt.callguar;

import java.util.Map;

// 押品系统url调用参数
public class CallGuarReqDto {

    /**
     * URL前缀，示例http://10.28.42.97:7001/cms/callPage.do
     */
    private String prefixUrl;

    /**
     * 调用方法
     */
    private String callMethod;

    /**
     * 参数
     */
    private Map<String, Object> parameter;

    public CallGuarReqDto() {
        // do nothing
    }

    public String getPrefixUrl() {
        return prefixUrl;
    }

    public void setPrefixUrl(String prefixUrl) {
        this.prefixUrl = prefixUrl;
    }

    public String getCallMethod() {
        return callMethod;
    }

    public void setCallMethod(String callMethod) {
        this.callMethod = callMethod;
    }

    public Map<String, Object> getParameter() {
        return parameter;
    }

    public void setParameter(Map<String, Object> parameter) {
        this.parameter = parameter;
    }

    @Override
    public String toString() {
        return "CallGuarReqDto{" +
                "prefixUrl='" + prefixUrl + '\'' +
                ", callMethod='" + callMethod + '\'' +
                ", parameter=" + parameter +
                '}';
    }

}
