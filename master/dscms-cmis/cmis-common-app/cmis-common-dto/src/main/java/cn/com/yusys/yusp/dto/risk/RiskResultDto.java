package cn.com.yusys.yusp.dto.risk;

import java.io.Serializable;

/**
 * 风险拦截返回结果封装体
 *
 * @author mashun
 * @since 2021-06-21 21:30:44
 */
public class RiskResultDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private String riskResultType;

    private String riskResultDesc;

    public RiskResultDto() {
        // do nothing
    }

    public String getRiskResultType() {
        return riskResultType;
    }

    public void setRiskResultType(String riskResultType) {
        this.riskResultType = riskResultType;
    }

    public String getRiskResultDesc() {
        return riskResultDesc;
    }

    public void setRiskResultDesc(String riskResultDesc) {
        this.riskResultDesc = riskResultDesc;
    }

    @Override
    public String toString() {
        return "RiskResultDto{" +
                "riskResultType='" + riskResultType + '\'' +
                ", riskResultDesc='" + riskResultDesc + '\'' +
                '}';
    }
}
