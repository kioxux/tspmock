package cn.com.yusys.yusp.cmis.commons.annonation;

import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.util.SpringContextUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;

import java.io.IOException;
import java.util.Objects;

/**
 * @author danyu
 * 基于redis的vo字段翻译，比如 loginCode admin 翻译为userName 系统管理员
 */
public class RedisCacheTranslatorSerializer extends JsonSerializer<String> implements ContextualSerializer {

    /**
     * 处理缓存默认使用的RedisTemplate
     */
    private static final String REDIS_TEMPLATE_NAME = "yuspRedisTemplate";

    /**
     * 代表redisCacheKey, 在fieldName没有值的情况下也代表要翻译到哪个字段
     */
    private String redisCacheKey;

    private String refFieldName;//在注解中给fieldName属性赋了值的情况下，翻译结果字段名取fieldName的值，否则翻译结果属性名取redisCacheKey的值

    /**
     * 必须要保留无参构造方法
     */
    public RedisCacheTranslatorSerializer() {
        this("");
    }

    public RedisCacheTranslatorSerializer(String redisCacheKey) {
        this.redisCacheKey = redisCacheKey;
    }

    public RedisCacheTranslatorSerializer(String redisCacheKey, String refFieldName) {
        this.redisCacheKey = redisCacheKey;
        this.refFieldName = refFieldName;
    }

    @Override
    public void serialize(String value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        // 使用hash类型存储字典 例如: redisKey -> userName  HashKey -> 40 HashValue -> 系统管理员
        YuspRedisTemplate stringRedisTemplate = SpringContextUtils.getBean(REDIS_TEMPLATE_NAME);
        String name = value;
        if (stringRedisTemplate.hget(redisCacheKey, value) != null) {
            name = String.valueOf(stringRedisTemplate.hget(redisCacheKey, value));
        }
        jsonGenerator.writeString(value);
        String fieldName = "";
        if (StringUtils.isEmpty(refFieldName)) {
            fieldName = redisCacheKey;
        } else {
            fieldName = refFieldName;
        }
        jsonGenerator.writeStringField(fieldName, name);
    }

    @Override
    public JsonSerializer<?> createContextual(SerializerProvider serializerProvider, BeanProperty beanProperty) throws JsonMappingException {
        // 为空直接跳过
        if (beanProperty != null) {
            // 非 String 类直接跳过
            if (Objects.equals(beanProperty.getType().getRawClass(), String.class)) {
                // 取注解对象
                RedisCacheTranslator redisDictTranslator = beanProperty.getAnnotation(RedisCacheTranslator.class);
                if (redisDictTranslator == null) {
                    redisDictTranslator = beanProperty.getContextAnnotation(RedisCacheTranslator.class);
                }
                // 如果能得到注解，就将注解的 redisCacheKey 传入 RedisDictTranslatorSerializer
                if (redisDictTranslator != null) {
                    return new RedisCacheTranslatorSerializer(redisDictTranslator.redisCacheKey(), redisDictTranslator.refFieldName());
                }
            }
            return serializerProvider.findValueSerializer(beanProperty.getType(), beanProperty);
        }
        return serializerProvider.findNullValueSerializer(null);
    }

}