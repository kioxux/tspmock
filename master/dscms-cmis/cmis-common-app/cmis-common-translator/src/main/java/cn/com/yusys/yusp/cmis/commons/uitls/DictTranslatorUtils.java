package cn.com.yusys.yusp.cmis.commons.uitls;

import cn.com.yusys.yusp.cmis.commons.enums.SystemCacheEnum;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.SpringContextUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Map;

/**
 * 通过key查缓存value以及通过value去查缓存key
 *
 * @author zhangsm
 * @since 2021/5/19
 **/
public class DictTranslatorUtils {

    private static StringRedisTemplate instance;

    /**
     * 处理缓存默认使用的RedisTemplate
     */
    private static final String REDIS_TEMPLATE_NAME = "stringRedisTemplate";

    private DictTranslatorUtils() {
        throw new IllegalStateException("Utility class");
    }

    private static synchronized void init() {
        if (instance == null) {
            instance = SpringContextUtils.getBean(REDIS_TEMPLATE_NAME);
        }
    }

    private static StringRedisTemplate instance() {
        if (instance == null) {
            init();
        }
        return instance;
    }

    /**
     * 通过缓存的type和key获取value
     *
     * @param dictType 字典项类型
     * @param dictKey  字典Key
     * @return 字典值
     */
    public static String findValueByDictKey(String dictType, String dictKey) {
        Assert.notNull(dictType, "字典项类型不允许为空！");
        Assert.notNull(dictKey, "字典Key不允许为空！");
        String value = "";
        BoundHashOperations<String, Object, Object> dataDict =
                instance().opsForHash().getOperations().boundHashOps(SystemCacheEnum.CACHE_KEY_DATADICT.key);
        Object object = dataDict.get(dictType);
        if (object instanceof String) {
            List<Map<String, String>> listMap =
                    (List<Map<String, String>>) ObjectMapperUtils.toObject(StringUtils.replaceObjNull(object), List.class);
            for (Map<String, String> map : listMap) {
                if (map.get("key").equals(dictKey)) {
                    value = map.get("value");
                    return value;
                }
            }
        }
        return value;
    }

    /**
     * 通过缓存的type和value获取key
     *
     * @param dictType  字典项类型
     * @param dictValue 字典值
     * @return 字典key
     */
    public static String findKeyByDictValue(String dictType, String dictValue) {
        Assert.notNull(dictType, "字典项类型不允许为空！");
        Assert.notNull(dictValue, "字典值不允许为空！");
        String dictKey = "";
        BoundHashOperations<String, Object, Object> dataDict =
                instance().opsForHash().getOperations().boundHashOps(SystemCacheEnum.CACHE_KEY_DATADICT.key);
        Object object = dataDict.get(dictType);
        if (object instanceof String) {
            List<Map<String, String>> listMap =
                    (List<Map<String, String>>) ObjectMapperUtils.toObject(StringUtils.replaceObjNull(object), List.class);
            for (Map<String, String> map : listMap) {
                if (map.get("value").equals(dictValue)) {
                    dictKey = map.get("key");
                    return dictKey;
                }
            }
        }
        return dictKey;
    }


}
