package cn.com.yusys.yusp.cmis.commons.config;

import cn.com.yusys.yusp.cmis.commons.web.filter.CmisDataAuthorityWebFilter;
import cn.com.yusys.yusp.commons.autoconfigure.cache.redis.YuRedisAutoConfiguration;
import cn.com.yusys.yusp.commons.autoconfigure.data.authority.DataAuthorityAutoConfiguration;
import cn.com.yusys.yusp.commons.autoconfigure.data.authority.DataAuthorityProperties;
import cn.com.yusys.yusp.commons.data.authority.DataAuthorizationService;
import cn.com.yusys.yusp.commons.data.authority.UserInformationService;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.util.collection.MapUtils;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.Map;

/**
 * 提供数据授权组件使用的一些自动配置属性, 主要用于该组件在{@code SpringBoot}环境下启动的一些自动配置
 *
 * @author yangzai
 * @since 2021/6/2
 **/
@Configuration
@AutoConfigureAfter({DataAuthorityAutoConfiguration.class, YuRedisAutoConfiguration.class})
public class CmisDataAuthorityAutoConfiguration {

    private DataAuthorityProperties dataAuthorityProperties;

    public CmisDataAuthorityAutoConfiguration(DataAuthorityProperties dataAuthorityProperties) {
        this.dataAuthorityProperties = dataAuthorityProperties;
    }

    /**
     * 向容器注册所需的数据授权对象的{@code Web Filter}, 用于处理通过Http请求过来的授权配置信息
     *
     * @param dataAuthorizationConfig 数据授权配置信息获取接口
     * @param userService             用户信息获取接口对象
     * @return
     */
    @Bean
    @ConditionalOnBean(value = {DataAuthorizationService.class, UserInformationService.class, YuspRedisTemplate.class})
    public FilterRegistrationBean<Filter> cmisDataAuthorityWebFilter(DataAuthorizationService dataAuthorizationConfig,
                                                                     UserInformationService userService, YuspRedisTemplate yuspRedisTemplate) {
        FilterRegistrationBean<Filter> filterBean = new FilterRegistrationBean<>();
        filterBean.addUrlPatterns("/*");
        filterBean.setOrder(1);
        filterBean.setName(CmisDataAuthorityWebFilter.class.getSimpleName());
        Map<String, String> initMap = MapUtils.newHashMap();
        initMap.put(CmisDataAuthorityWebFilter.INIT_IGNORE_URLS_KEY, dataAuthorityProperties.getIgnoredApi());
        filterBean.setInitParameters(initMap);
        filterBean.setFilter(new CmisDataAuthorityWebFilter(dataAuthorizationConfig, userService, yuspRedisTemplate));
        return filterBean;
    }

}
