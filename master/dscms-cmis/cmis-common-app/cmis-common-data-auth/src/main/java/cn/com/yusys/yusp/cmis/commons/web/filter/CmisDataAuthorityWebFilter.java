package cn.com.yusys.yusp.cmis.commons.web.filter;


import cn.com.yusys.yusp.commons.data.authority.DataAuthorityContext;
import cn.com.yusys.yusp.commons.data.authority.DataAuthorizationInfo;
import cn.com.yusys.yusp.commons.data.authority.DataAuthorizationService;
import cn.com.yusys.yusp.commons.data.authority.UserInformation;
import cn.com.yusys.yusp.commons.data.authority.UserInformationService;
import cn.com.yusys.yusp.commons.redis.template.YuspRedisTemplate;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.UrlPathHelper;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 数据授权Web过滤器, 用于匹配当前请求用户和对应URI是否有配置相应的数据授权信息, 如果有, 则将对应的数据权限将放置在数据权限的上下文中。
 * 用于后续{@code Mybatis}数据权限拦截器做对应的数据权限处理
 * <p>
 * 使用场景:<br/>
 * 仅限机构树（查询当前用户所属机构及其所有下级机构）
 * 只要引入该组件, 所有的Http请求均需要通过此拦截器进行数据授权信息的匹配处理
 *
 * @author yangzai
 * @since 2021/6/2
 **/
public class CmisDataAuthorityWebFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmisDataAuthorityWebFilter.class);

    /**
     * 拦截器初始化对象中存储的忽略URI配置清单的Key
     */
    public static final String INIT_IGNORE_URLS_KEY = "ignoreUrls";

    /**
     * 查询当前机构及其下级所有机构集合
     */
    public static final String ORG_TREE_KEY = "_orgTree";

    /**
     * 缓存中机构树集合List<String>
     */
    public static final String REDIS_ORG_LIST_KEY = "orgTree";

    /**
     * 区域中心负责人下的小微客户经理Key
     */
    public static final String REDIS_KEY_AREA_XW_USER = "areaXwUser";

    /**
     * 区域中心负责人及分部部长下的机构Key
     */
    public static final String REDIS_KEY_AREA_XW_ORG = "areaXwOrg";

    /**
     * 集中作业总行本地机构（除小微）下的所有用户Key
     */
    public static final String REDIS_KEY_JZZY_USER = "jzzyUser";

    /**
     * 所有小微客户经理Key
     */
    public static final String REDIS_KEY_ALL_XW_USER = "allXwUser";

    /**
     * 区域中心负责人下的小微客户经理Key
     */
    public static final String DATAAUTH_AREA_XW_USER_KEY = "_areaXwUser";

    /**
     * 区域中心负责人下的小微客户经理Key
     */
    public static final String DATAAUTH_AREA_XW_ORG_KEY = "_areaXwOrg";

    /**
     * 所有小微客户经理Key
     */
    public static final String DATAAUTH_ALL_XW_USER_KEY = "_allXwUser";

    /**
     * 集中作业总行本地机构（除小微）下的所有用户Key
     */
    public static final String DATAAUTH_JZZY_USER_KEY = "_jzzyUser";

    /**
     * 机构下小微客户经理Key
     **/
    public static final String REDIS_KEY_ORG_XW_USER = "orgXwUser";

    /**
     * 机构下小微客户经理Key
     **/
    public static final String DATAAUTH_ORG_XW_USER = "_orgXwUser";

    /**
     * 机构下非小微用户Key
     **/
    public static final String REDIS_KEY_ORG_NOT_XW_USER = "orgNotXwUser";

    /**
     * 机构下非小微用户Key
     **/
    public static final String DATAAUTH_ORG_NOT_XW_USER = "_orgNotXwUser";


    private final YuspRedisTemplate yuspRedisTemplate;

    /**
     * 数据授权信息获取服务接口, 用于匹配当前用户和请求的URI是否有对应的数据权限配置信息
     */
    private final DataAuthorizationService dataAuthorizationService;

    /**
     * 用户信息获取服务接口, 用于获取用户的相关信息
     */
    private final UserInformationService userService;

    /**
     * URL获取工具, 用于从{@link ServletRequest}对象中获取对应的请求URI路径
     */
    private final UrlPathHelper pathHelper = new UrlPathHelper();

    /**
     * URI路径匹配信息, 用于匹配当前请求的URI是否为需要忽略处理的URI
     */
    private final AntPathMatcher matcher = new AntPathMatcher();

    /**
     * 忽略处理的URI清单, 通过拦截器初始化对象获取对应的配置
     */
    private List<String> ignoreUrls;

    public CmisDataAuthorityWebFilter(DataAuthorizationService dataAuthorizationService,
                                      UserInformationService userService, YuspRedisTemplate yuspRedisTemplate) {
        this.dataAuthorizationService = dataAuthorizationService;
        this.userService = userService;
        this.yuspRedisTemplate = yuspRedisTemplate;
    }

    /**
     * 从配置中获取并初始化忽略处理的URI清单
     */
    @Override
    public void init(FilterConfig filterConfig) {
        LOGGER.info("Create cmis access permission interceptor!");
        if (null == filterConfig.getInitParameter(INIT_IGNORE_URLS_KEY)) {
            return;
        }
        ignoreUrls = Collections.unmodifiableList(Arrays.asList(
                filterConfig.getInitParameter(INIT_IGNORE_URLS_KEY).split(","))
                .parallelStream()
                .filter(Objects::nonNull)
                .map(String::trim)
                .collect(Collectors.toList()));
    }

    /**
     * 当前web过滤器处理逻辑如下:
     * <li>1:从{@link ServletRequest}中获取当前请求的URI路径
     * <li>2: 判断是否为配置需要忽略处理的URI路径, 为需要忽略的不做当前过滤器主逻辑, 直接走后续过滤器
     * <li>3: 通过{@link UserInformationService}对象获取当前登录的用户信息
     * <li>4: 如果获取的用户信息对象和用户id为空则直接走web拦截器后续过滤链
     * <li>5: 用户信息对应不为空且用户id不为空,  则通过{@link DataAuthorizationService}对象匹配当期的请求URI, 请求Method和用户id存在的数据授权信息
     * <li>6: 如果存在对应的数据授权信息则将数据授权信息和用户信息放置到{@link DataAuthorityContext}线程上线文对象中, 以便后续{@code Mybatis}数据权限
     * 拦截器处理
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String httpMethod = request.getMethod();
        String requestUri = pathHelper.getLookupPathForRequest(request);

        boolean result = true;
        // 不做过滤
        if (null != ignoreUrls && !ignoreUrls.isEmpty()) {
            result = ignoreUrls.parallelStream().anyMatch(ctrl -> matcher.match(ctrl, requestUri));
            result = !result;
        }

        if (result) {
            UserInformation userInformation = userService.getUserInformation();
            // 用户信息为空或用户ID为空, 忽略数据权限处理 
            if (Objects.nonNull(userInformation) && userInformation.getUserId() != null) {

                // 根据请求API和登录用户匹配当前请求中是否有对应的数据授权信息 
                List<DataAuthorizationInfo> dataAuthorizationInfoList =
                        dataAuthorizationService.matching(requestUri, httpMethod, userInformation.getUserId());

                // 匹配成功, 则将数据授权信息和用户信息放置到{@link DataAuthorityContext}线程上线文对象中
                if (Objects.nonNull(dataAuthorizationInfoList) && !dataAuthorizationInfoList.isEmpty()) {
                    addUserInformationToContext(userInformation);
                } else {
                    LOGGER.debug("The current request API[{}], " +
                                    "The login user[{}] is not configured with the corresponding data authorization information",
                            requestUri, userInformation.getUserId());
                }
            } else {
                LOGGER.debug("The current request API[{}] does not obtain information about the logged in user", requestUri);
            }
        }
        try {
            // 执行后续web过滤链
            filterChain.doFilter(servletRequest, servletResponse);
        } finally {
            // 清理线程上线文中对象信息, 如果执行过程中报错, 在finally中处理用于保证一定可以清除
            DataAuthorityContext.getCurrentContext().unset();
        }

    }

    /**
     * 将获取的用户机构树信息放到线程上下文中, 用于数据权限{@code Mybatis}拦截器处理时做数据权限模板中的可变参数替换操作
     *
     * @param userInformation 用户信息对象
     */
    private void addUserInformationToContext(UserInformation userInformation) {
        List<String> currentOrgCodeList = userInformation.getOrganizationIds();// 当前用户所属的机构码，最多只有一条
        if (!CollectionUtils.isEmpty(currentOrgCodeList)) {
            String currentOrgCode = currentOrgCodeList.get(0);// 当前用户所属机构的机构码
            Object hget = yuspRedisTemplate.hget(REDIS_ORG_LIST_KEY, currentOrgCode);
            if (Objects.nonNull(hget)) {
                List<String> orgTreeList = (List<String>) ObjectMapperUtils.instance().convertValue(hget, List.class);
                DataAuthorityContext context = DataAuthorityContext.getCurrentContext();
                context.set(ORG_TREE_KEY, orgTreeList);
            }
        }
        String currentUserCode = userInformation.getUserId();
        if(StringUtils.nonBlank(currentUserCode)){
            Object areaXwUserHget = yuspRedisTemplate.hget(REDIS_KEY_AREA_XW_USER, currentUserCode);
            if(Objects.nonNull(areaXwUserHget)){
                List<String> areaXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(areaXwUserHget, List.class);
                DataAuthorityContext context = DataAuthorityContext.getCurrentContext();
                context.set(DATAAUTH_AREA_XW_USER_KEY, areaXwUserList);
            }

            Object areaXwOrgHget = yuspRedisTemplate.hget(REDIS_KEY_AREA_XW_ORG, currentUserCode);
            if(Objects.nonNull(areaXwOrgHget)){
                List<String> areaXwOrgList = (List<String>) ObjectMapperUtils.instance().convertValue(areaXwOrgHget, List.class);
                DataAuthorityContext context = DataAuthorityContext.getCurrentContext();
                context.set(DATAAUTH_AREA_XW_ORG_KEY, areaXwOrgList);
            }

            Object allXwHget = yuspRedisTemplate.hget(REDIS_KEY_ALL_XW_USER, currentUserCode);
            if(Objects.nonNull(allXwHget)){
                List<String> allXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(allXwHget, List.class);
                DataAuthorityContext context = DataAuthorityContext.getCurrentContext();
                context.set(DATAAUTH_ALL_XW_USER_KEY, allXwUserList);
            }

            Object jzzyHget = yuspRedisTemplate.hget(REDIS_KEY_JZZY_USER, currentUserCode);
            if(Objects.nonNull(jzzyHget)){
                List<String> jzzyUserList = (List<String>) ObjectMapperUtils.instance().convertValue(jzzyHget, List.class);
                DataAuthorityContext context = DataAuthorityContext.getCurrentContext();
                context.set(DATAAUTH_JZZY_USER_KEY, jzzyUserList);
            }

            Object orgXwUser = yuspRedisTemplate.hget(REDIS_KEY_ORG_XW_USER, currentUserCode);
            if(Objects.nonNull(orgXwUser)){
                List<String> orgXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(orgXwUser, List.class);
                DataAuthorityContext context = DataAuthorityContext.getCurrentContext();
                context.set(DATAAUTH_ORG_XW_USER, orgXwUserList);
            }

            Object orgNotXwUser = yuspRedisTemplate.hget(REDIS_KEY_ORG_NOT_XW_USER, currentUserCode);
            if(Objects.nonNull(orgNotXwUser)){
                List<String> orgNotXwUserList = (List<String>) ObjectMapperUtils.instance().convertValue(orgNotXwUser, List.class);
                DataAuthorityContext context = DataAuthorityContext.getCurrentContext();
                context.set(DATAAUTH_ORG_NOT_XW_USER, orgNotXwUserList);
            }
        }
    }

}