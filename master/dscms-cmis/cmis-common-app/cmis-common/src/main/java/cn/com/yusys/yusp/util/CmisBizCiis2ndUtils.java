package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.dto.client.http.ciis2nd.callciis2nd.CallCiis2ndReqDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * 工具类
 * 二代征信通过url跳转查看征信报告
 */
public class CmisBizCiis2ndUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmisBizCiis2ndUtils.class);

    /**
     * "请使用POST方式发起请求
     * http://ip:port/zjg-ws/controller/showReport/cpqReport?reqId=8a9caa136e2b2788016e2b5032d30003&orgName=信贷&userCode=018007&userName=陆易成
     * "
     * "请使用POST方式发起请求
     * http://ip:port/zjg-ws/controller/showReport/ceqReport?reqId=8a9caa136e2b2788016e2b5032d30003&orgName=信贷&userCode=018007&userName=陆易成
     * "
     *
     * @param callCiis2ndReqDto
     * @return
     */
    public static String callciis2nd(CallCiis2ndReqDto callCiis2ndReqDto) {
        // 二代征信查看征信报告url
        String ciis2ndUrl;
        // url前缀
        String prefixUrl = callCiis2ndReqDto.getPrefixUrl();
        // 业务明细ID
        String reqId = callCiis2ndReqDto.getReqId();
        // 部门名称
        String orgName = callCiis2ndReqDto.getOrgName();
        // 查看人工号
        String userCode = callCiis2ndReqDto.getUserCode();
        // 查看人名称
        String userName = callCiis2ndReqDto.getUserName();
        // 报告类型（若为空则默认为个人报告）
        String reportType = Optional.ofNullable(callCiis2ndReqDto.getReportType()).orElse(DscmsBizZxEnum.REPORT_TYPE_PER.key).trim();
        LOGGER.info("URL前缀:[{}]", prefixUrl);
        LOGGER.info("报告类型:[{}]", reportType);
        LOGGER.info("业务明细ID:[{}]", reqId);
        LOGGER.info("查看人工号:[{}]", userCode);
        LOGGER.info("查看人名称:[{}]", userName);
        // SYMBOL_QUESTION("?", "问号"),
        String symbolQuestion = DscmsBizDbEnum.SYMBOL_QUESTION.key;
        //  SYMBOL_EQUAL("=","等于号"),
        String symbolEqual = DscmsBizDbEnum.SYMBOL_EQUAL.key;
        //  SYMBOL_AND("&", "与"),
        String symbolAnd = DscmsBizDbEnum.SYMBOL_AND.key;
        ciis2ndUrl = prefixUrl // url前缀
                .concat(reportType) // 报告类型
                .concat(symbolQuestion).concat("reqId").concat(symbolEqual).concat(reqId) // 业务明细ID
                .concat(symbolAnd).concat("orgName").concat(symbolEqual).concat(orgName) // 部门名称
                .concat(symbolAnd).concat("userCode").concat(symbolEqual).concat(userCode) // 查看人工号
                .concat(symbolAnd).concat("userName").concat(symbolEqual).concat(userName).trim(); // 查看人名称
        LOGGER.info("拼装后的二代征信URL为[{}]", ciis2ndUrl);
        return ciis2ndUrl;
    }

    public static void main(String[] args) {
        CallCiis2ndReqDto callCiis2ndReqDto = new CallCiis2ndReqDto();
        callCiis2ndReqDto.setPrefixUrl("http://ip:port/zjg-ws/controller/showReport/");
        callCiis2ndReqDto.setReportType("cpqReport");
        callCiis2ndReqDto.setReqId("8a9caa136e2b2788016e2b5032d30003");
        callCiis2ndReqDto.setOrgName("信贷");
        callCiis2ndReqDto.setUserCode("018007");
        callCiis2ndReqDto.setUserName("陆易成");
        callciis2nd(callCiis2ndReqDto);
    }
}
