package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.util.StringUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 * 从token的payload中解析用户信息
 *
 * @author gaolq
 */
public class TokenUtil {

    private TokenUtil() {
        throw new IllegalStateException("Utility class");
    }

    public static String getSysId(String token) {
        return (String) getPayload(token).get("sysId");
    }

    public static String getLoginCode(String token) {
        return (String) getPayload(token).get("user_name");
    }

    private static Map<String, Object> getPayload(String token) {

        Map<String, Object> payloadMap = new HashMap<>();

        if (null == token || StringUtils.isBlank(token)) {
            return payloadMap;
        }

        String payloadPart = token.split("\\.")[1];
        String payloadStr = new String(Base64.getDecoder().decode(payloadPart));

        ObjectMapper mapper = new ObjectMapper();
        try {
            payloadMap = mapper.readValue(payloadStr, new TypeReference<Map<String, Object>>() {
            });
        } catch (JsonProcessingException e) {
            return new HashMap<>();
        }

        return payloadMap;
    }

}
