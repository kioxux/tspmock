package cn.com.yusys.yusp.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 文件下载操作类
 *
 * @author yangzai
 * @since 2021/4/27
 **/
public class FileDownloadUtils {

    public static void setDonwloadFileName(String downaloadFileName, HttpServletRequest request, HttpServletResponse response) throws UnsupportedEncodingException {
        String userAgent = request.getHeader("User-Agent").toLowerCase();//获取浏览器名（IE/Chome/firefox）
        if (userAgent.contains("firefox")) {
            downaloadFileName = new String(downaloadFileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1); // firefox浏览器
        } else if (userAgent.contains("msie")) {
            downaloadFileName = URLEncoder.encode(downaloadFileName, StandardCharsets.UTF_8.toString());// IE浏览器
        } else if (userAgent.contains("chrome")) {
            downaloadFileName = new String(downaloadFileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);// chrome
        } else {
            downaloadFileName = new String(downaloadFileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
        }
        response.setHeader("Content-Disposition", "attachment; fileName=" + downaloadFileName + ";");
    }

}