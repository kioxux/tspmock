package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.constants.CmisCommonConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

/**
 * 公共组件处理
 *
 * @author wangyk
 * @since 2021/5/16
 **/
public class CmisCommonUtils {
    private static Logger logger = LoggerFactory.getLogger(CmisCommonUtils.class);

    /**
     * @param id
     * @return java.lang.String
     * @author 王玉坤
     * @date 2021/5/16 15:04
     * @version 1.0.0
     * @desc 根据身份证判断性别 男：M ，女：F
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public static String getGenderByIDToFM(String id) {
        if (id.length() != 18 && id.length() != 15) {
            String message = "身份证输入有误！";
            return message;
        } else {
            if (id.length() == 18) {
                String value = id.substring(id.length() - 2, id.length() - 1);
                int gender = Integer.parseInt(value) % 2;
                return gender == 0 ? "F" : "M";
            } else {
                String value = id.substring(id.length() - 1, id.length());
                int gender = Integer.parseInt(value) % 2;
                return gender == 0 ? "F" : "M";
            }
        }
    }

    /**
     * @param id
     * @return java.lang.String
     * @author 王玉坤
     * @date 2021/5/16 15:04
     * @version 1.0.0
     * @desc 根据身份证查出生年月日
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public static String getBirthdayByID(String id) {
        if (id.length() != 18) {
            String message = "身份证输入有误！";
            return message;
        } else {
            String birthday = id.substring(6, 14);
            return birthday;
        }
    }

    /**
     * @param birthDate
     * @return int
     * @author 王玉坤
     * @date 2021/5/16 15:14
     * @version 1.0.0
     * @desc 根据出生年月计算年龄
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public static String getAge(Date birthDate, String openDay) throws Exception {
        if (birthDate == null) {
            throw new Exception("出生日期不能为null");
        }
        int age = 0;
        Date now = new SimpleDateFormat("yyyy-MM-dd").parse(openDay);
        SimpleDateFormat formatY = new SimpleDateFormat("yyyy");
        SimpleDateFormat formatM = new SimpleDateFormat("MM");
        SimpleDateFormat formatD = new SimpleDateFormat("dd");
        String birthY = formatY.format(birthDate);
        String thisY = formatY.format(now);
        String birthM = formatM.format(birthDate);
        String thisM = formatM.format(now);
        String birthD = formatD.format(birthDate);
        String thisD = formatD.format(now);
        age = Integer.parseInt(thisY) - Integer.parseInt(birthY);

        if (birthM.compareTo(thisM) > 0) {//如果出生月大于当前月，则年龄=年份差-1；
            age = age - 1;
        }
        if (thisM.compareTo(birthM) == 0) {//如果出生月等于当前月并且出生日大于当前日，则年龄=年份差-1；
            if (birthD.compareTo(thisD) > 0) {
                age = age - 1;
            }
        }
        if (age < 0) {
            age = 0;
        }
        return String.valueOf(age);
    }

    /**
     * @param managerBrId
     * @return 组织机构
     * @author lihh
     * @date 2021/5/16 15:14
     * @version 1.0.0
     * @desc 根据管护机构获取银行机构代码
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public static String getInstucde(String managerBrId) {
        if (managerBrId.startsWith(CmisCommonConstants.INSTUCDE_80)) {
            return CmisCommonConstants.INSTUCDE_002;
        } else if (managerBrId.startsWith(CmisCommonConstants.INSTUCDE_81)) {
            return CmisCommonConstants.INSTUCDE_003;
        }
        return CmisCommonConstants.INSTUCDE_001;
    }

    /**
     * @param startDate, endDate
     * @return int
     * @author 王玉坤
     * @date 2021/9/18 21:56
     * @version 1.0.0
     * @desc 根据日期计算相差月份 yyyy-MM-dd
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public static int getBetweenMonth(String startDate, String endDate) throws Exception {
        logger.info("计算日期相差月份开始：起始日期【{}】、到期日期【{}】", startDate, endDate);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Calendar oldDate = Calendar.getInstance();
        Calendar newDate = Calendar.getInstance();

        oldDate.setTime(sdf.parse(startDate));
        newDate.setTime(sdf.parse(endDate));

        // 定义月份
        double month;

        // 相差年数
        int year = oldDate.get(Calendar.YEAR) - newDate.get(Calendar.YEAR);

        if (year < 0) {
            year = -year;
            month = year * 12 -calMonthAndDay(oldDate, newDate);
        } else {
            month = year * 12 -calMonthAndDay(oldDate, newDate);
        }
        logger.info("计算日期相差月份结束：起始日期【{}】、到期日期【{}】，相差月份【{}】", startDate, endDate, month);
        return (int)Math.ceil(month);
    }

    /**
     * @param oldDate, newDate
     * @return double
     * @author 王玉坤
     * @date 2021/9/18 23:51
     * @version 1.0.0
     * @desc 计算日期之间相差月份
     *       1、日期同是月底，整月计算
     *       2、日期同天，整月计算
     *       3、不同天，不是月底，除以31
     * @修改历史: 修改时间    修改人员    修改原因
     */
    public static double calMonthAndDay(Calendar oldDate, Calendar newDate) {
        double month;
        if (oldDate.getActualMaximum(Calendar.DAY_OF_MONTH) == oldDate.get(Calendar.DAY_OF_MONTH)
        && newDate.getActualMaximum(Calendar.DAY_OF_MONTH) == newDate.get(Calendar.DAY_OF_MONTH)) {
            // 日期同天
            month = oldDate.get(Calendar.MONTH) - newDate.get(Calendar.MONTH);
        } else if (oldDate.get(Calendar.DAY_OF_MONTH) == newDate.get(Calendar.DAY_OF_MONTH)){
            // 日期同天
            month = oldDate.get(Calendar.MONTH) - newDate.get(Calendar.MONTH);
        } else {
            double day = oldDate.get(Calendar.DAY_OF_MONTH) - newDate.get(Calendar.DAY_OF_MONTH);
            month = oldDate.get(Calendar.MONTH) - newDate.get(Calendar.MONTH) + day/31;
        }
        return month;
    }

    public static void main(String[] args) throws Exception {
        System.out.println(getBetweenMonth("2021-09-19", "2021-09-20"));
    }
}
