package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.util.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import java.math.BigDecimal;
/**
 * @作者:lizx
 * @方法名称: Comm4CalFormulaService
 * @方法描述:  金额相关计算公司，抽取到该方法中
 * @参数与返回说明: 
 * @算法描述: 无
 * @日期：2021/8/3 20:16
 * @算法描述: 无
*/
@Service
public class Comm4CalFormulaUtils {

    private static final Logger logger = LoggerFactory.getLogger(Comm4CalFormulaUtils.class);

    /**
     * @方法名称: checkSpacLimit
     * @方法描述:产品授信计算获取占用底层额度金额
     * @参数与返回说明: 
     * @算法描述: 产品授信金额*底层资产余额/(拟/实际发行规模（原币）)
     * @日期：2021/8/3 20:21
     * @param prdLmtAmt: 产品授信金额
     * @param basicAssetBal: 底层资产余额
     * @param actualIssuAmt: (拟/实际发行规模（原币）)
     * @return: java.math.BigDecimal
     * @算法描述: 无
    */
    public BigDecimal getPrdLmtUseAmt(BigDecimal prdLmtAmt, BigDecimal basicAssetBal, BigDecimal actualIssuAmt) {
        //产品授信金额*底层资产余额/(拟/实际发行规模（原币）)
        return NumberUtils.divide( NumberUtils.multiply(prdLmtAmt, basicAssetBal), actualIssuAmt) ;
    }
}

