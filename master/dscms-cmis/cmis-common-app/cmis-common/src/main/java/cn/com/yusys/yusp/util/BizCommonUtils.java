package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.ObjectMapperUtils;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.flow.ClientCons;
import cn.com.yusys.yusp.flow.api.WorkflowCoreClient;
import cn.com.yusys.yusp.flow.dto.NextNodeInfoDto;
import cn.com.yusys.yusp.flow.dto.WFException;
import cn.com.yusys.yusp.flow.dto.result.ResultInstanceDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 流程专用工具类
 */
@Component
public class BizCommonUtils {

    private static BizCommonUtils bizCommonUtils;
    @Autowired
    private WorkflowCoreClient workflowCoreClient;//工作流接口
    @Autowired
    private AmqpTemplate amqpTemplate;//消息队列模板

    public BizCommonUtils() {
    }

    /**
     * 通过入参的流程数据信息，判断下一节点是否包含初始节点用户
     * 若是下一节点用户中包含初始用户，针对打回以及退回操作，需要将业务主表的流程状态修改为992-打回
     *
     * @param resultInstanceDto
     * @return
     */
    public static boolean isFirstNodeCheck(ResultInstanceDto resultInstanceDto) {
        boolean checkFlag = false;
        List<NextNodeInfoDto> nextNodeUserInfo = resultInstanceDto.getNextNodeInfos();
        if (CollectionUtils.isEmpty(nextNodeUserInfo)) {
            throw new YuspException(EcbEnum.BIZWORKFLOW_GETNEXTNODEINFONULL_EXCEPTION.key, EcbEnum.BIZWORKFLOW_GETNEXTNODEINFONULL_EXCEPTION.value);
        }
        //循环下一节点用户信息判断是否存在初始节点
        for (NextNodeInfoDto nextNodeInfoDto : nextNodeUserInfo) {
            //调用接口判断下一节点是否为初始节点
            checkFlag = bizCommonUtils.workflowCoreClient.isFirstNode(nextNodeInfoDto.getNextNodeId()).getData().booleanValue();
            //若是初始节点，则跳出循环
            if (checkFlag) {
                break;
            }
        }

        return checkFlag;
    }

    /**
     * 通过入参的流程数据信息，判断当前是否包含初始节点用户
     * 若是下一节点用户中包含初始用户，针对打回以及退回操作，需要将业务主表的流程状态修改为992-打回
     *
     * @param resultInstanceDto
     * @return
     */
    public static boolean checkSubmitNodeIsFirst (ResultInstanceDto resultInstanceDto) {
        return bizCommonUtils.workflowCoreClient.isFirstNode(resultInstanceDto.getNodeId()).getData().booleanValue();
    }

    @PostConstruct
    public void init() {
        bizCommonUtils = this;
        bizCommonUtils.workflowCoreClient = this.workflowCoreClient;
        bizCommonUtils.amqpTemplate = this.amqpTemplate;
    }

    /**
     * 封装流程异常处理以及发送消息队列的方法
     *
     * @param e            后业务处理的异常
     * @param instanceInfo 流程入参信息
     */
    public void getExceptionMessageAndSendExptionMQ(Exception e, ResultInstanceDto instanceInfo) throws JsonProcessingException {
        WFException exception = new WFException();
        exception.setPkId(StringUtils.uuid(true));
        exception.setBizId(instanceInfo.getBizId());//业务流水号
        exception.setBizType(instanceInfo.getBizType());//流程类型
        exception.setFlowName(instanceInfo.getFlowName());//流程名称
        exception.setInstanceId(instanceInfo.getInstanceId());//流程实例号
        exception.setNodeId(instanceInfo.getNodeId());//流程处理节点
        exception.setNodeName(instanceInfo.getNodeName());//流程节点名称
        exception.setUserId(instanceInfo.getCurrentUserId());//流程当前用户
        exception.setOpType(instanceInfo.getCurrentOpType());//流程处理类型
        exception.setFlowId(Long.parseLong(instanceInfo.getFlowId()));//流程编号
        exception.setBizParam(ObjectMapperUtils.instance().writeValueAsString(instanceInfo));//流程入参信息
        //流程异常类型，暂未添加该字段
        //exception.setExceptionType(e.getClass().getSimpleName());
        exception.setExceptionInfo("错误类型：" + e.getClass().getSimpleName() + "\r\n错误原因：" + e.getMessage() + "\r\n错误位置："
                + ObjectMapperUtils.instance().writeValueAsString(e.getStackTrace()[0]));//异常信息
        exception.setExceptionTime(DateUtils.getCurrDateTimeStr());
        // 后业务处理失败时，将异常信息保存到异常表中
        bizCommonUtils.amqpTemplate.convertAndSend(ClientCons.queue_exception, exception);
    }
}
