package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.dto.client.http.ypxt.callguar.CallGuarReqDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 押品管理中调用押品系统的工具类
 *
 * @author leehuang
 */
public class CmisBizGuarUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CmisBizGuarUtils.class);

    //固定参数
    private static String fixParameter = "|URLUSER|BB2C3CB8A662BFA3D3EB7335F99F59B1";

    public static void main(String[] args) {
        try {
            CallGuarReqDto callGuarReqDto = new CallGuarReqDto();
            callGuarReqDto.setPrefixUrl("http://10.85.10.31:7001/cms/callPage.do");
            callGuarReqDto.setCallMethod(DscmsBizDbEnum.CALLMETHOD_TGUAREXISTQRY.key);
            // 参数，参数中需要将以下所有值均设置值，如果没有值则设置为StringUtils.EMPTY
            Map<String, Object> parameter = new HashMap<>();
            parameter.put("Address", URLEncoder.encode(URLEncoder.encode("啊实打实的", "utf-8"), "utf-8"));//地址，传二次编码过来
            parameter.put("Area", "100");//面积
            parameter.put("BorrowNo", "1");//保证人时为担保合同借款人
            parameter.put("BusNo", "1");//担保品编号,业务流水号
            parameter.put("CustNo", "1");//押品所有人编号
            parameter.put("DataFlag", DscmsBizDbEnum.DATAFLAG_01.key);//数据修改标识
            parameter.put("EvalAmt", "10000");//评估金额
            parameter.put("EvalType", DscmsBizDbEnum.EVALTYPE_01.key);//评估方式
            parameter.put("GenDate", "2021-04-15");//分配日期
            parameter.put("GrtFlg", DscmsBizDbEnum.GRTFLG_1.key);//主担保或副担保
            parameter.put("GuarNo", "1");//押品统一编号
            parameter.put("GuarWay", DscmsBizDbEnum.GUARWAY_BZ.key);//担保方式标识
            parameter.put("OperOrg", "1");//当前操作用户所属机构
            parameter.put("OperUser", "1");//当前操作用户ID
            parameter.put("OutSys", "1");//是否外围url调用
            parameter.put("PageMark", DscmsBizDbEnum.PAGEMARK_03.key);//页面跳转标识
            parameter.put("Property", DscmsBizDbEnum.Property_010.key);//物业情况
            parameter.put("SystemFlag", DscmsBizDbEnum.SYSTEMFLAG_01.key);//请求的系统标识
            parameter.put("SystemNo", DscmsBizDbEnum.SYSTEMNO_01.key);//请求的系统标识
            parameter.put("Usage", "");//使用情况
            parameter.put("WarnType", "1");//待处理任务类型
            parameter.put("WarnTypeCode", "1");//待处理任务分类
            callGuarReqDto.setParameter(parameter);
            String callguar = callguar(callGuarReqDto);
            LOGGER.info("---URL为--- {}", callguar);
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * 获取押品系统的跳转页面URL
     * http://10.28.42.97:7001/cms/callPage.do?parameter=tGuarDetailInfo|admin|F6FDFFE48C908DEB0F4C3BD36C032E72|01|admin|3113|DY|||||222222|02
     *
     * @param callGuarReqDto
     * @return
     */
    public static String callguar(CallGuarReqDto callGuarReqDto) {
        // 调用押品系统url
        String callGuarUrl;
        String prefixUrl = callGuarReqDto.getPrefixUrl();//URL前缀
        // 调用方法
        String callMethod = callGuarReqDto.getCallMethod();
        // 参数
        Map<String, Object> parameter = callGuarReqDto.getParameter();
        callGuarUrl = prefixUrl.concat(DscmsBizDbEnum.SYMBOL_QUESTION.key).concat(generateParameterUrl(callMethod, parameter));
        return callGuarUrl;
    }

    // 根据调用方法生成参数URL
    private static String generateParameterUrl(String callMethod, Map<String, Object> parameter) {
        String parameterUrl = StringUtils.EMPTY;
        String address = Optional.ofNullable((String) parameter.get("Address")).orElse(StringUtils.EMPTY);//地址
        String area = Optional.ofNullable((String) parameter.get("Area")).orElse(StringUtils.EMPTY);//面积
        String borrowNo = Optional.ofNullable((String) parameter.get("BorrowNo")).orElse(StringUtils.EMPTY);//保证人时为担保合同借款人编号
        String busNo = Optional.ofNullable((String) parameter.get("BusNo")).orElse(StringUtils.EMPTY);//担保品编号,业务流水号
        String custNo = Optional.ofNullable((String) parameter.get("CustNo")).orElse(StringUtils.EMPTY);//押品所有人编号
        String dataFlag = Optional.ofNullable((String) parameter.get("DataFlag")).orElse(StringUtils.EMPTY);//数据修改标识
        String evalAmt = Optional.ofNullable((String) parameter.get("EvalAmt")).orElse(StringUtils.EMPTY);//评估金额
        String evalType = Optional.ofNullable((String) parameter.get("EvalType")).orElse(DscmsBizDbEnum.EVALTYPE_03.key);//评估方式
        String genDate = Optional.ofNullable((String) parameter.get("GenDate")).orElse(StringUtils.EMPTY);//分配日期
        String grtFlg = Optional.ofNullable((String) parameter.get("GrtFlg")).orElse(StringUtils.EMPTY);//主担保或副担保
        String guarNo = Optional.ofNullable((String) parameter.get("GuarNo")).orElse(StringUtils.EMPTY);//押品统一编号
        String guarWay = Optional.ofNullable((String) parameter.get("GuarWay")).orElse(StringUtils.EMPTY);//担保方式标识
        String operOrg = Optional.ofNullable((String) parameter.get("OperOrg")).orElse(StringUtils.EMPTY);//当前操作用户所属机构
        String operUser = Optional.ofNullable((String) parameter.get("OperUser")).orElse(StringUtils.EMPTY);//当前操作用户ID
        String outSys = Optional.ofNullable((String) parameter.get("OutSys")).orElse(StringUtils.EMPTY);//是否外围url调用
        String pageMark = Optional.ofNullable((String) parameter.get("PageMark")).orElse(StringUtils.EMPTY);//页面跳转标识
        String property = Optional.ofNullable((String) parameter.get("Property")).orElse(StringUtils.EMPTY);//物业情况
        String systemFlag = Optional.ofNullable((String) parameter.get("SystemFlag")).orElse(StringUtils.EMPTY);//请求的系统标识
        String systemNo = Optional.ofNullable((String) parameter.get("SystemNo")).orElse(StringUtils.EMPTY);//请求的系统标识
        String usage = Optional.ofNullable((String) parameter.get("Usage")).orElse(StringUtils.EMPTY);//使用情况
        String warnType = Optional.ofNullable((String) parameter.get("WarnType")).orElse(StringUtils.EMPTY);//待处理任务类型
        String warnTypeCode = Optional.ofNullable((String) parameter.get("WarnTypeCode")).orElse(StringUtils.EMPTY);//待处理任务分类
        String symbolOr = DscmsBizDbEnum.SYMBOL_OR.key;//或
        // parameter=tGuarDetailInfo|admin|F6FDFFE48C908DEB0F4C3BD36C032E72|01|admin|3113|DY|||||222222|02
        parameterUrl = parameterUrl.concat("parameter").concat(DscmsBizDbEnum.SYMBOL_EQUAL.key);

        if (Objects.equals(callMethod, DscmsBizDbEnum.CALLMETHOD_TGUAREXISTQRY.key)) {
            // 押品新增界面
            // SystemNo：请求的系统标识
            // OperUser：当前操作用户ID
            // OperOrg：当前操作用户所属机构
            // GuarWay：担保方式标识
            // BusNo:业务流水号
            // GrtFlg:主担保或副担保
            // Usage:使用情况
            // Property:物业情况
            // EvalAmt：评估金额
            // Area：面积
            // CustNo：押品所有人编号
            // EvalType：评估方式
            // Address
            // BorrowNo
            parameterUrl = parameterUrl.concat(DscmsBizDbEnum.CALLMETHOD_TGUAREXISTQRY.key).concat(fixParameter)
                    .concat(symbolOr).concat(systemNo) // 请求的系统标识
                    .concat(symbolOr).concat(operUser) //当前操作用户ID
                    .concat(symbolOr).concat(operOrg) // 当前操作用户ID
                    .concat(symbolOr).concat(guarWay) // 担保方式标识
                    .concat(symbolOr).concat(busNo) // 业务流水号
                    .concat(symbolOr).concat(grtFlg) // 主担保或副担保
                    .concat(symbolOr).concat(usage) // 使用情况
                    .concat(symbolOr).concat(property) // 物业情况
                    .concat(symbolOr).concat(evalAmt) // 评估金额
                    .concat(symbolOr).concat(area) // 面积
                    .concat(symbolOr).concat(custNo) // 押品所有人编号
                    .concat(symbolOr).concat(evalType) // 评估方式
                    .concat(symbolOr).concat(address) // 地址
                    .concat(symbolOr).concat(borrowNo) // 担保合同借款人编号
            ;
        } else if (Objects.equals(callMethod, DscmsBizDbEnum.CALLMETHOD_TGUARDETAILINFO.key)) {
            //查看押品详细信息界面
            // SystemNo：请求的系统标识
            // OperUser：当前操作用户ID
            // OperOrg：当前操作用户所属机构
            // GuarNo：押品统一编号
            // DataFlag：数据修改标识
            // PageMark：页面跳转标识
            parameterUrl = parameterUrl.concat(DscmsBizDbEnum.CALLMETHOD_TGUARDETAILINFO.key).concat(fixParameter)
                    .concat(symbolOr).concat(systemNo) // 请求的系统标识
                    .concat(symbolOr).concat(operUser) // 当前操作用户ID
                    .concat(symbolOr).concat(operOrg) // 当前操作用户所属机构
                    .concat(symbolOr).concat(guarNo) // 押品统一编号
                    .concat(symbolOr).concat(dataFlag) // 数据修改标识
                    .concat(symbolOr).concat(pageMark) // 页面跳转标识
            ;
        } else if (Objects.equals(callMethod, DscmsBizDbEnum.CALLMETHOD_TGUARUPDATE.key)) {
            //押品信息修改界面
            // SystemNo：请求的系统标识
            // OperUser：当前操作用户ID
            // OperOrg：当前操作用户所属机构
            // GuarNo：押品统一编号
            // BusNo: 担保品编号
            // EvalAmt：评估金额
            // EvalType：评估方式
            // DataFlag：数据修改标识
            // PageMark：页面跳转标识
            parameterUrl = parameterUrl.concat(DscmsBizDbEnum.CALLMETHOD_TGUARUPDATE.key).concat(fixParameter)
                    .concat(symbolOr).concat(systemNo) // 请求的系统标识
                    .concat(symbolOr).concat(operUser) // 当前操作用户ID
                    .concat(symbolOr).concat(operOrg) // 当前操作用户所属机构
                    .concat(symbolOr).concat(guarNo) // 押品统一编号
                    .concat(symbolOr).concat(busNo) // 担保品编号
                    .concat(symbolOr).concat(evalAmt) // 评估金额
                    .concat(symbolOr).concat(evalType) // 评估方式
                    .concat(symbolOr).concat(dataFlag) // 数据修改标识
                    .concat(symbolOr).concat(pageMark) // 页面跳转标识
            ;
        } else if (Objects.equals(callMethod, DscmsBizDbEnum.CALLMETHOD_TGUARFIRSTEVAL.key)) {
            // 客户经理进行押品价值的初估
            // SystemFlag：请求的系统标识
            // OperUser：当前操作用户ID
            // OperOrg：当前操作用户所属机构
            // GuarNo：押品统一编号
            parameterUrl = parameterUrl.concat(DscmsBizDbEnum.CALLMETHOD_TGUARFIRSTEVAL.key).concat(fixParameter)
                    .concat(symbolOr).concat(systemFlag) // 请求的系统标识
                    .concat(symbolOr).concat(operUser) // 当前操作用户ID
                    .concat(symbolOr).concat(operOrg) // 当前操作用户所属机构
                    .concat(symbolOr).concat(guarNo) // 押品统一编号
            ;
        } else if (Objects.equals(callMethod, DscmsBizDbEnum.CALLMETHOD_TGUARREEVALAPPLY.key)) {
            // 客户经理进行押品价值的重估
            // SystemFlag：请求的系统标识
            // OperUser：当前操作用户ID
            // OperOrg：当前操作用户所属机构
            // GuarNo：押品统一编号
            parameterUrl = parameterUrl.concat(DscmsBizDbEnum.CALLMETHOD_TGUARREEVALAPPLY.key).concat(fixParameter)
                    .concat(symbolOr).concat(systemFlag) // 请求的系统标识
                    .concat(symbolOr).concat(operUser) // 当前操作用户ID
                    .concat(symbolOr).concat(operOrg) // 当前操作用户所属机构
                    .concat(symbolOr).concat(guarNo) // 押品统一编号
            ;
        } else if (Objects.equals(callMethod, DscmsBizDbEnum.CALLMETHOD_TCOLLWARNING.key)) {
            // 待处理任务界面
            // SystemFlag：请求的系统标识
            // OperUser：当前操作用户ID
            // OperOrg：当前操作用户所属机构
            // GuarNo：押品统一编号
            // WarnType：待处理任务类型
            // WarnTypeCode：待处理任务分类
            // GenDate：分配日期
            parameterUrl = parameterUrl.concat(DscmsBizDbEnum.CALLMETHOD_TCOLLWARNING.key).concat(fixParameter)
                    .concat(symbolOr).concat(systemFlag) // 请求的系统标识
                    .concat(symbolOr).concat(operUser) // 当前操作用户ID
                    .concat(symbolOr).concat(operOrg) // 当前操作用户所属机构
                    .concat(symbolOr).concat(guarNo) // 押品统一编号
                    .concat(symbolOr).concat(warnType) // 待处理任务类型
                    .concat(symbolOr).concat(warnTypeCode) // 待处理任务分类
                    .concat(symbolOr).concat(genDate) // 分配日期
            ;
        } else if (Objects.equals(callMethod, DscmsBizDbEnum.CALLMETHOD_TWELCOME.key)) {
            // 押品系统首页
            // SystemNo：请求的系统标识
            // OperUser：当前操作用户ID
            // OperOrg：当前操作用户所属机构
            // OutSys:是否外围url调用
            parameterUrl = parameterUrl.concat(DscmsBizDbEnum.CALLMETHOD_TWELCOME.key).concat(fixParameter)
                    .concat(symbolOr).concat(systemNo) // 请求的系统标识
                    .concat(symbolOr).concat(operUser) // 当前操作用户ID
                    .concat(symbolOr).concat(operOrg) // 当前操作用户所属机构
                    .concat(symbolOr).concat(outSys) // 是否外围url调用
            ;
        }
        return parameterUrl;
    }
}
