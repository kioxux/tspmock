package cn.com.yusys.yusp.intercept;

import cn.com.yusys.yusp.annotation.SeqId;
import cn.com.yusys.yusp.commons.session.user.User;
import cn.com.yusys.yusp.commons.session.util.SessionUtils;
import cn.com.yusys.yusp.commons.util.date.DateUtils;
import cn.com.yusys.yusp.sequence.client.SequenceTemplateService;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

/**
 * @author tangxun
 * @version 1.0.0
 * @since 2021/4/20 10:33 下午
 */
@Intercepts({
        @Signature(type = Executor.class, method = "update", args = {MappedStatement.class, Object.class})
})
@Component
public class ZrcbankIntercept implements Interceptor, ApplicationContextAware {

    private static final Logger log = LoggerFactory.getLogger(ZrcbankIntercept.class);

    private ApplicationContext applicationContext;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        Object[] args = invocation.getArgs();
        //参数
        MappedStatement mappedStatement = (MappedStatement) args[0];
        User userInfo = SessionUtils.getUserInformation();
        String openday = stringRedisTemplate.opsForValue().get("openDay");
        //更新序列号
        if ("INSERT".equalsIgnoreCase(mappedStatement.getSqlCommandType().name())) {
            Set<Object> set = getEntitySet(args[1]);
            for (Object obj : set) {
                Field[] fields = obj.getClass().getDeclaredFields();
                for (Field field : fields) {
                    //存在SeqId注解且域值为空
                    field.setAccessible(true);
                    if (field.isAnnotationPresent(SeqId.class) && (field.get(obj) == null || "".equals(field.get(obj)))) {
                        SeqId seqId = field.getAnnotation(SeqId.class);
                        //获取序列号
                        SequenceTemplateService sequenceTemplateService = applicationContext.getBean(SequenceTemplateService.class);
                        String pkId = sequenceTemplateService.getSequenceTemplate(seqId.value(), new HashMap<>(1));
                        field.set(obj, pkId);
                    }
                }
                if (Objects.isNull(userInfo)) {
                    if (log.isDebugEnabled()) {
                        log.debug("userInfo is null,can't set inputId and inputBrId");
                    }
                } else {
                    setField(obj, "inputId", userInfo.getLoginCode());
                    setField(obj, "inputBrId", userInfo.getOrg().getCode());
                    setField(obj, "managerId", userInfo.getLoginCode());
                    setField(obj, "managerBrId", userInfo.getOrg().getCode());
                }
                setField(obj, "createTime", DateUtils.getCurrDate());
                setField(obj, "inputDate", openday);
            }
        }
        //更新时间
        if ("INSERT".equalsIgnoreCase(mappedStatement.getSqlCommandType().name())
                || "UPDATE".equalsIgnoreCase(mappedStatement.getSqlCommandType().name())) {
            Set<Object> set = getEntitySet(args[1]);
            for (Object obj : set) {
                if (Objects.isNull(userInfo)) {
                    if (log.isDebugEnabled()) {
                        log.debug("userInfo is null,can't set updId and updBrId");
                    }
                } else {
                    setField(obj, "updId", userInfo.getLoginCode());
                    setField(obj, "updBrId", userInfo.getOrg().getCode());
                }
                //可能冗余字段
                setField(obj, "updDate", openday);
                setField(obj, "updateTime", DateUtils.getCurrDate());
            }
        }
        return invocation.proceed();
    }

    /**
     * 设置属性
     *
     * @param obj
     * @param name
     * @param value
     */
    public void setField(Object obj, String name, Object value) {
        Field field = null;
        try {
            field = obj.getClass().getDeclaredField(name);
            field.setAccessible(true);
            if (field.get(obj) == null || "".equals(field.get(obj))) {
                field.set(obj, value);
            } else if ("updDate".equals(name)) {
                field.set(obj, value);
            }
        } catch (IllegalArgumentException e) {
            //时间格式不对
            try {
                field.set(obj, DateUtils.formatDate((Date) value, "yyyy-MM-dd HH:mm:ss"));
            } catch (Exception e1) {

            }
        } catch (NoSuchFieldException | IllegalAccessException e) {

        }
    }

    private Set<Object> getEntitySet(Object obj) {
        Set<Object> set = new HashSet<>();
        if (obj instanceof Map) {
            if (((Map) obj).containsKey("list")) {
                Collection values = (Collection) ((Map) obj).get("list");
                for (Object value : values) {
                    if (value instanceof Collection) {
                        set.addAll((Collection) value);
                    } else {
                        set.add(value);
                    }
                }
            } else if (((Map) obj).containsKey("collection")) {
                Collection values = (Collection) ((Map) obj).get("collection");
                for (Object value : values) {
                    if (value instanceof Collection) {
                        set.addAll((Collection) value);
                    } else {
                        set.add(value);
                    }
                }
            }
        } else {
            set.add(obj);
        }
        return set;
    }


    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
        return;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }
}
