package cn.com.yusys.yusp.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

import org.apache.commons.lang.StringEscapeUtils;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.constants.CmisCusConstants;
import cn.com.yusys.yusp.enums.returncode.EcsEnum;

/**
 * 此类的作用是为方便应用中常量的管理，提供更加有效的常量存储机制，方便应用系统中类的调用，避免 程序中过多的出现硬编码。
 *
 * @author xuchao
 * @since 20210511
 */
public class PUBUtilTools {
    /**
     * ecif证件类型字典项映射
     *
     * @param ecifCertType
     * @return
     */
    public static String changeCertTypeFromEcif(String ecifCertType) {
        String certType = "";
        if (StringUtils.isEmpty(ecifCertType)) {
            return certType;
        }
        if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_A)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_10;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_B)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_12;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_C)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_11;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_D)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_15;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_E)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_16;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_G)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_13;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_H)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_14;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_S)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_18;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_T) || ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_F)
                || ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_I) || ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_J)
                || ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_K) || ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_L)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_28;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_Q)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_20;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_V)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_22;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_U)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_23;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_M)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_24;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_R)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_25;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_N)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_26;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_P) || ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_O)
                || ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_U)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_27;
        } else if (ecifCertType.equals(CmisCusConstants.STD_ECIF_CERT_TYP_W)) {
            certType = CmisCusConstants.STD_ZB_CERT_TYP_29;
        }
        return certType;
    }

    /**
     * String日期八位转成十位
     *
     * @param date
     * @return
     */
    public static String changeGjDate2CMIS(String date) {
        if (date == null || "".equals(date)) {
            return "";
        } else if (date.length() == 8) {
            return date.substring(0, 4) + "-" + date.substring(4, 6) + "-" + date.substring(6, 8);
        } else {
            return date;
        }
    }

    /**
     * 将实体类中的为空的字段设置为null
     *
     * @param source
     */
    public static <T> T setNullValue(T source) throws Exception {
        Field[] fields = source.getClass().getDeclaredFields();
        for (Field field :
                fields) {
            if (field.getGenericType().toString().equals("class java.lang.String")) {
                field.setAccessible(true);
                Object obj = field.get(source);
                if (obj != null && "".equals(obj)) {
                    field.set(source, null);
                } else if (obj != null) {
                    String str = obj.toString();
                    str = StringEscapeUtils.escapeSql(str);
                    field.set(source, str.replace("\\", "\\" + "\\").replace("(", "\\(").replace(")", "\\)")
                            .replace("%", "\\%").replace("*", "*\\").replace("[", "\\[").replace("]", "\\]")
                            .replace("|", "\\|").replace(".", "\\.".replace("$", "\\$").replace("+", "\\+").trim())
                    );
                }
            }
        }
        return source;
    }

    /**
     * ecif转信贷币种 TODO 待确认
     *
     * @param ecifCurType
     * @return
     */
    public static String changeCurTypeFromEcif(String ecifCurType) {
        String curType = "";
        if (ecifCurType == null) {
            return curType;
        }
        if (ecifCurType.equals(CmisCusConstants.STD_ECIF_CUR_TYPE_CHF)) {
            curType = CmisCusConstants.STD_ZB_CUR_TYP_SFR;
        } else {
            curType = ecifCurType;
        }
        return curType;
    }


    /**
     * ecif转信贷 经营状况
     *
     * @param ecifBusiState
     * @return
     */
    public static String changeBusiStateFromEcif(String ecifBusiState) {
        String busiState = "";
        if (ecifBusiState == null) {
            return busiState;
        }
        if (ecifBusiState.equals(CmisCusConstants.STD_ECIF_OPT_ST_110)) {
            busiState = CmisCusConstants.STD_ZB_OPT_ST_200;
        } else if (ecifBusiState.equals(CmisCusConstants.STD_ECIF_OPT_ST_200)) {
            busiState = CmisCusConstants.STD_ZB_OPT_ST_300;
        } else if (ecifBusiState.equals(CmisCusConstants.STD_ECIF_OPT_ST_210)) {
            busiState = CmisCusConstants.STD_ZB_OPT_ST_400;
        } else if (ecifBusiState.equals(CmisCusConstants.STD_ECIF_OPT_ST_300)) {
            busiState = CmisCusConstants.STD_ZB_OPT_ST_500;
        } else {
            busiState = ecifBusiState;
        }
        return busiState;
    }

    /**
     * 转换抵质押品类型对应核算分类代码
     */
    public static String changeloanGlRoleCde(String gageType) {
        String coreCurrency = "";
        if (gageType == null) {
            return coreCurrency;
        }
        //抵押
        if ("10001".equals(gageType)) {
            coreCurrency = "DZY002";
        } else if ("10002".equals(gageType)) {
            coreCurrency = "DZY005";
        } else if ("20001".equals(gageType)) {
            coreCurrency = "DZY007";
        } else if ("20002".equals(gageType)) {
            coreCurrency = "DZY007";
        } else if ("10003".equals(gageType)) {
            coreCurrency = "DZY001";
        } else if ("10004".equals(gageType)) {
            coreCurrency = "DZY006";
        } else if ("10005".equals(gageType)) {
            coreCurrency = "DZY004";
        } else if ("10006".equals(gageType)) {
            coreCurrency = "DZY006";
        } else if ("10007".equals(gageType)) {
            coreCurrency = "DZY003";
        } else if ("10008".equals(gageType)) {
            coreCurrency = "DZY007";
        } else if ("10009".equals(gageType)) {
            coreCurrency = "DZY007";
        } else if ("10010".equals(gageType)) {
            coreCurrency = "DZY007";
        } else if ("10040".equals(gageType)) {
            coreCurrency = "DZY027";//不动产
        }
        //质押
        else if ("10024".equals(gageType)) {
            coreCurrency = "DZY008";
        } else if ("10025".equals(gageType)) {
            coreCurrency = "DZY008";
        } else if ("10011".equals(gageType)) {
            coreCurrency = "DZY019";
            //押品改造后，10012仅代表债券，国债变更为10042
        } else if ("10012".equals(gageType) || "10042".equals(gageType)) {
            coreCurrency = "DZY011";
        } else if ("10013".equals(gageType)) {
            coreCurrency = "DZY019";
        } else if ("10014".equals(gageType)) {
            coreCurrency = "DZY008";
        } else if ("10015".equals(gageType)) {
            coreCurrency = "DZY013";
        } else if ("10016".equals(gageType)) {
            coreCurrency = "DZY015";
        } else if ("10017".equals(gageType)) {
            coreCurrency = "DZY012";
        } else if ("10018".equals(gageType)) {
            coreCurrency = "DZY017";
        } else if ("10019".equals(gageType)) {
            coreCurrency = "DZY019";
        } else if ("10021".equals(gageType)) {
            coreCurrency = "DZY019";
        } else if ("10022".equals(gageType)) {
            coreCurrency = "DZY016";
        } else if ("10023".equals(gageType)) {
            coreCurrency = "DZY016";
        } else if ("10028".equals(gageType)) {
            coreCurrency = "DZY025";
        } else if ("10029".equals(gageType)) {
            coreCurrency = "DZY016";
        } else if ("10026".equals(gageType)) {
            coreCurrency = "DZY016";
        } else if ("10030".equals(gageType)) {
            coreCurrency = "DZY020";
        } else if ("10031".equals(gageType)) {
            coreCurrency = "DZY021";
        } else if ("10032".equals(gageType)) {
            coreCurrency = "DZY022";
        } else if ("10033".equals(gageType)) {
            coreCurrency = "DZY023";
        } else if ("10034".equals(gageType)) {
            coreCurrency = "DZY024";
        } else if ("10041".equals(gageType)) {
            coreCurrency = "DZY004";
        }
        return coreCurrency;
    }

    //新核心币种转换
    public static String currencyType(String currency) {
        String msgcu = "";
        if ("CNY".equals(currency)) {//人民币
            msgcu = "01";
        } else if ("GBP".equals(currency)) {//英镑
            msgcu = "12";
        } else if ("HKD".equals(currency)) {//港币
            msgcu = "13";
        } else if ("USD".equals(currency)) {//美元
            msgcu = "14";
        } else if ("CHF".equals(currency)) {//瑞士法郎
            msgcu = "15";
        } else if ("SGD".equals(currency)) {//新加坡元
            msgcu = "18";
        } else if ("SEK".equals(currency)) {//瑞典克郎
            msgcu = "21";
        } else if ("DKK".equals(currency)) {//丹麦克郎
            msgcu = "22";
        } else if ("NOK".equals(currency)) {//挪威克郎
            msgcu = "23";
        } else if ("JPY".equals(currency)) {//日元
            msgcu = "27";
        } else if ("CAD".equals(currency)) {//加元
            msgcu = "28";
        } else if ("AUD".equals(currency)) {//澳大利亚元
            msgcu = "29";
        } else if ("EUR".equals(currency)) {//欧元
            msgcu = "38";
        } else if ("MOP".equals(currency)) {//澳门元
            msgcu = "81";
        }
        return msgcu;
    }

    /**
     * 信贷是否1,0 转化为Y,N
     *
     * @param flag
     * @return
     */
    public static String changeYesNo(String flag) {
        if (Objects.equals(flag, "1")) {
            return "Y";
        } else if (Objects.equals(flag, "0")) {
            return "N";
        } else {
            return flag;
        }
    }

    /**
     * 担保方式:信贷系统转押品系统
     * 信贷:01抵押 02质押  03保证
     * 押品:BZ-保证 DY-抵押ZY-质押
     *
     * @param flag
     * @return
     */
    public static String changeGuarWay(String flag) {
        if (Objects.equals(flag, "01")) {
            return "DY";
        } else if (Objects.equals(flag, "02")) {
            return "ZY";
        } else if (Objects.equals(flag, "03")) {
            return "BZ";
        } else {
            return flag;
        }
    }

    /**
     * @param data
     * @param flag flag true 标示启用非空校验
     * @return
     */
    public static String getString(String data, Boolean flag) {
        if ("".equals(data) || data == null) {
            if (flag) {
                throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"参数为空!\"" + EcsEnum.E_SAVE_FAIL.value);
            } else {
                data = "";
            }
        }
        return data;
    }

    public static String getString(String data) {
        return getString(data, false);
    }

    public static BigDecimal getString(BigDecimal data, Boolean flag) {
        if (data == null) {
            if (flag) {
                throw new YuspException(EcsEnum.E_CUS_INFO_ISNULL.key, "\"参数为空!\"" + EcsEnum.E_SAVE_FAIL.value);
            } else {
                data = new BigDecimal("0");
            }
        }
        return data;
    }

    public static BigDecimal getString(BigDecimal data) {
        return getString(data, false);
    }

    public static BigDecimal stringToBigDecimal(String data) {
        if (data == null || "".equals(data)) {
            data = "0";
        }
        return new BigDecimal(data);
    }

    public static BigDecimal integerToBigDecimal(Integer data) {
        if (data == null) {
            data = 0;
        }
        return new BigDecimal(data);
    }

    /**
     * (信贷到核算的字典映射)
     * 转换抵质押品类型对应核算抵质押物种类
     *
     * @param guarTypeCd
     * @return
     */
    public static String changeGuarTypeCd(String guarTypeCd) {
        String coreDzywzlei = "";
        if (guarTypeCd == null) {
            return coreDzywzlei;
        }

        if ("DY01".equals(guarTypeCd)) {
            coreDzywzlei = "03";//中文注释为核算的描述 03--房产/房地产
        } else if ("DY02".equals(guarTypeCd)) {
            coreDzywzlei = "01";//01--土地使用权
        } else if ("DY03".equals(guarTypeCd)) {
            coreDzywzlei = "01";//01--土地使用权
        } else if ("DY04".equals(guarTypeCd)) {
            coreDzywzlei = "40";//40--不动产
        } else if ("DY05".equals(guarTypeCd)) {
            coreDzywzlei = "05";//05--机械设备
        } else if ("DY06".equals(guarTypeCd)) {
            coreDzywzlei = "02";//02--在建工程
        } else if ("DY07".equals(guarTypeCd)) {
            coreDzywzlei = "04";//04--机动车辆（抵押）
        } else if ("DY08".equals(guarTypeCd)) {
            coreDzywzlei = "10";//04--机动车辆（抵押）
        } else if ("DY99".equals(guarTypeCd)) {
            coreDzywzlei = "09";//09--抵押其他类
        } else if ("ZY11".equals(guarTypeCd)) {
            coreDzywzlei = "48";//48--机动车辆(质押）
        } else if ("ZY12".equals(guarTypeCd)) {
            coreDzywzlei = "41";//41--存货（质押）
        } else if ("ZY99".equals(guarTypeCd)) {
            coreDzywzlei = "21";//21--质押其他类
        } else if ("ZY08".equals(guarTypeCd)) {
            coreDzywzlei = "15";//15--仓单/提单
        } else if ("ZY09".equals(guarTypeCd)) {
            coreDzywzlei = "15";//15--仓单/提单
        } else if ("ZY10".equals(guarTypeCd)) {
            coreDzywzlei = "15";//15--仓单/提单
        } else if ("ZY05".equals(guarTypeCd)) {
            coreDzywzlei = "16";//16--股权/股票/基金
        } else if ("ZY02".equals(guarTypeCd)) {
            coreDzywzlei = "11";//16--股权/股票/基金
        } else if ("ZY01".equals(guarTypeCd)) {
            coreDzywzlei = "17";//17--存单
        } else if ("ZY03".equals(guarTypeCd)) {
            coreDzywzlei = "14";//14--票据
        } else if ("ZY04".equals(guarTypeCd)) {
            coreDzywzlei = "12";//12--债券
        }

        return coreDzywzlei;
    }

    /**
     * (信贷到核算的字典映射)
     * 根据核算抵质押物种类获取对应核算分类代码
     *
     * @param dzywzlei
     * @return
     */
    public static String changeCoreDzywzlei(String dzywzlei) {
        String coreYewusx01 = "DZY019";//记账余额属性 默认质押其他类
        if (dzywzlei == null) {
            return coreYewusx01;
        }
        //抵押
        if ("01".equals(dzywzlei)) {
            coreYewusx01 = "DZY002";
        } else if ("02".equals(dzywzlei)) {
            coreYewusx01 = "DZY005";
        } else if ("03".equals(dzywzlei)) {
            coreYewusx01 = "DZY001";
        } else if ("04".equals(dzywzlei)) {
            coreYewusx01 = "DZY006";
        } else if ("05".equals(dzywzlei)) {
            coreYewusx01 = "DZY004";
        } else if ("06".equals(dzywzlei)) {
            coreYewusx01 = "DZY006";
        } else if ("07".equals(dzywzlei)) {
            coreYewusx01 = "DZY003";
        } else if ("08".equals(dzywzlei)) {
            coreYewusx01 = "DZY007";
        } else if ("09".equals(dzywzlei)) {
            coreYewusx01 = "DZY007";
        } else if ("10".equals(dzywzlei)) {
            coreYewusx01 = "DZY007";
        } else if ("40".equals(dzywzlei)) {
            coreYewusx01 = "DZY027";//不动产
        }
        //质押
        else if ("24".equals(dzywzlei)) {
            coreYewusx01 = "DZY008";
        } else if ("25".equals(dzywzlei)) {
            coreYewusx01 = "DZY008";
        } else if ("11".equals(dzywzlei)) {
            coreYewusx01 = "DZY019";
            //押品改造后，12仅代表债券，国债变更为42
        } else if ("12".equals(dzywzlei) || "42".equals(dzywzlei)) {
            coreYewusx01 = "DZY011";
        } else if ("13".equals(dzywzlei)) {
            coreYewusx01 = "DZY019";
        } else if ("14".equals(dzywzlei)) {
            coreYewusx01 = "DZY008";
        } else if ("15".equals(dzywzlei)) {
            coreYewusx01 = "DZY013";
        } else if ("16".equals(dzywzlei)) {
            coreYewusx01 = "DZY015";
        } else if ("17".equals(dzywzlei)) {
            coreYewusx01 = "DZY012";
        } else if ("18".equals(dzywzlei)) {
            coreYewusx01 = "DZY017";
        } else if ("19".equals(dzywzlei)) {
            coreYewusx01 = "DZY019";
        } else if ("21".equals(dzywzlei)) {
            coreYewusx01 = "DZY019";
        } else if ("22".equals(dzywzlei)) {
            coreYewusx01 = "DZY016";
        } else if ("23".equals(dzywzlei)) {
            coreYewusx01 = "DZY016";
        } else if ("28".equals(dzywzlei)) {
            coreYewusx01 = "DZY025";
        } else if ("29".equals(dzywzlei)) {
            coreYewusx01 = "DZY016";
        } else if ("26".equals(dzywzlei)) {
            coreYewusx01 = "DZY016";
        } else if ("30".equals(dzywzlei)) {
            coreYewusx01 = "DZY020";
        } else if ("31".equals(dzywzlei)) {
            coreYewusx01 = "DZY021";
        } else if ("32".equals(dzywzlei)) {
            coreYewusx01 = "DZY022";
        } else if ("33".equals(dzywzlei)) {
            coreYewusx01 = "DZY023";
        } else if ("34".equals(dzywzlei)) {
            coreYewusx01 = "DZY024";
        } else if ("41".equals(dzywzlei)) {
            coreYewusx01 = "DZY004";
        }

        return coreYewusx01;
    }
    
    public static String getServsq()throws RuntimeException{
		SimpleDateFormat sdf =new SimpleDateFormat("yyyyMMddhhmmss");
		String nowday = sdf.format(new Date());
		StringBuilder str = new StringBuilder();
		Random random = new Random();
		for(int i=0; i<8; i++) {
			str.append(random.nextInt(10));
		}
		String servsq = "XDO" + nowday + "02" + str;
		return servsq;
	}

    /**
     * 证件类型新老码值转换
     * @param certType
     * @return
     */
    public static String changeCertType(String certType) {
        String certTypeXD = certType;
        if ("10".equals(certType)) {//居民身份证
            certTypeXD = "A";
        } else if ("11".equals(certType)) {//户口簿
            certTypeXD = "C";
        } else if ("12".equals(certType)) {//护照
            certTypeXD = "B";
        } else if ("13".equals(certType)) {//军官证
            certTypeXD = "G";
        } else if ("14".equals(certType)) {//士兵证
            certTypeXD = "H";
        } else if ("15".equals(certType)) {//港澳居民来往内地通行证
            certTypeXD = "D";
        } else if ("16".equals(certType)) {//台湾居民来往内地通行证
            certTypeXD = "E";
        } else if ("17".equals(certType)) {//临时身份证
            certTypeXD = "X";
        } else if ("18".equals(certType)) {//外国人居留证
            certTypeXD = "S";
        } else if ("19".equals(certType)) {//警官证
            certTypeXD = "Y";
        } else if ("1X".equals(certType)) {//其他证件
            certTypeXD = "T";
        } else if ("20".equals(certType)) {//组织机构代码
            certTypeXD = "Q";
        } else if ("21".equals(certType)) {//港澳台身份证
            certTypeXD = "W";
        } else if ("22".equals(certType)) {//境外企业代码
            certTypeXD = "V";
        } else if ("23".equals(certType)) {//回乡证
            certTypeXD = "T";
        } else if ("24".equals(certType)) {//营业执照
            certTypeXD = "M";
        }
        return certTypeXD;
    }
}
