package cn.com.yusys.yusp.aop;

import cn.com.yusys.yusp.cmis.commons.uitls.OcaTranslatorUtils;
import cn.com.yusys.yusp.commons.mapper.domain.BaseDomain;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author tangxun
 * @version 1.0.0
 * @since 2021/4/20 9:43 下午
 */
@Aspect
@Component
public class UserAndOrgNameAspect {

    private static final Logger log = LoggerFactory.getLogger(UserAndOrgNameAspect.class);
    private static final HashSet<String> USER_SET = new HashSet<>();
    private static final HashSet<String> ORG_SET = new HashSet<>();
    protected ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public UserAndOrgNameAspect() {
        init();
    }

    /**
     * @author tangxun
     * @date 2020/9/16 15:48
     * @version 1.0.0
     * @desc 将需要翻译的字段放到这里 需要翻译的用户字段名 放到 USER_SET 需要翻译的机构字段名ORG_SET @修改历史: 修改时间 修改人员 修改原因
     */
    public void init() {
        // 需要翻译的用户字段名
        USER_SET.add("inputId");
        USER_SET.add("managerId");
        USER_SET.add("updId");
        USER_SET.add("bizRightsAarBrId");
        USER_SET.add("handoverId");
        USER_SET.add("receiverId");
        USER_SET.add("prcId");
        USER_SET.add("huser");
        USER_SET.add("mainMgr");
        USER_SET.add("lastUpdateId");
        USER_SET.add("marId");
        USER_SET.add("outId");
        // 需要翻译的机构字段名
        ORG_SET.add("mainBrId");
        ORG_SET.add("inputBrId");
        ORG_SET.add("managerBrId");
        ORG_SET.add("updBrId");
        ORG_SET.add("belgOrg");
        ORG_SET.add("handOrg");
        ORG_SET.add("receiverOrg");
        ORG_SET.add("handoverBrId");
        ORG_SET.add("receiverBrId");
        ORG_SET.add("bizRightsAarBrIdOrg");
        ORG_SET.add("lastUpdateBrId");
        ORG_SET.add("aptiLiceRegiOrg");
        ORG_SET.add("finaBrNo");
        ORG_SET.add("managerBrNo");
        ORG_SET.add("orgId");
    }

    /**
     * 切点
     */
    @Pointcut("@annotation(org.springframework.web.bind.annotation.PostMapping)")
    public void pointCut() {
        // Do nothing because of pointCut
    }

    @AfterReturning(returning = "rvt", pointcut = "pointCut()")
    public Object translate(Object rvt) {
        if (rvt instanceof ResultDto<?>) {
            try {
                ResultDto<?> resultDto = (ResultDto<?>) rvt;
                Object data = resultDto.getData();
                if (data instanceof List<?>) {
                    List<Object> list = (List<Object>) data;
                    if (!list.isEmpty()) {
                        dealList(list);
                    }
                }
                if (data instanceof BaseDomain) {
                    dealDomain((ResultDto<Object>) resultDto);
                }
            } catch (Exception e) {
                log.warn("机构用户翻译失败:{}", e.getMessage());
            }
        }
        return rvt;
    }

    /**
     * @param resultDto
     * @return void
     * @author tangxun
     * @date 2020/9/5 12:19 下午
     * @version 1.0.0
     * @desc @修改历史: 修改时间 修改人员 修改原因
     */
    public void dealDomain(ResultDto<Object> resultDto) {
        Object domain = resultDto.getData();
        // 需要翻译的字段
        HashSet<String> user = new HashSet<>();
        HashSet<String> orgs = new HashSet<>();
        // 取到需要翻译的字段
        getUserAndOrgkeys(domain, user, orgs);
        Map<String, String> orgsDict = new HashMap<>(16);
        Map<String, String> namesDict = new HashMap<>(16);
        // 取到需要翻译的字段的值
        // 列表展示操作转译为中文
        if (!user.isEmpty()) {
            namesDict = getObjIdCode(domain, user).stream()
                    .collect(Collectors.toMap(code -> code, OcaTranslatorUtils::getUserName));
        }
        if (!orgs.isEmpty()) {
            orgsDict = getObjIdCode(domain, orgs).stream()
                    .collect(Collectors.toMap(code -> code, OcaTranslatorUtils::getOrgName));
        }

        if (!user.isEmpty() || !orgs.isEmpty()) {
            domainTrans(domain, resultDto, user, orgs, namesDict, orgsDict);
        }
    }

    /**
     * domain转换
     *
     * @param domain    对象
     * @param resultDto 响应对象
     * @param user      字段名
     * @param orgs      字段名
     * @param namesDict 用户名缓存
     * @param orgsDict  机构名缓存
     */
    private void domainTrans(
            Object domain,
            ResultDto<Object> resultDto,
            HashSet<String> user,
            HashSet<String> orgs,
            Map<String, String> namesDict,
            Map<String, String> orgsDict) {
        Map<String, Object> map = objectMapper.convertValue(domain, Map.class);
        for (String key : user) {
            String userCode = Objects.toString(map.get(key));
            if (userCode.contains(",")) {
                String[] arr = userCode.split(",");
                StringBuilder sb = new StringBuilder();
                for (String s : arr) {
                    sb.append(Optional.ofNullable(namesDict.get(s)).orElse(s));
                    sb.append(",");
                }
                map.put(key + "Name", sb.substring(0, sb.length() - 1));
            } else {
                map.put(key + "Name", Optional.ofNullable(namesDict.get(userCode)).orElse(userCode));
            }
        }
        for (String key : orgs) {
            map.put(key + "Name", Optional.ofNullable(orgsDict.get(map.get(key))).orElse(Objects.toString(map.get(key))));
        }
        resultDto.setData(map);
    }

    /**
     * @param list 返回类型
     * @author tangxun
     * @date 2020/9/4 4:16 下午
     * @desc 处理列表 @修改历史: 修改时间 修改人员 修改原因
     */
    public void dealList(List<Object> list) {
        Object item = list.get(0);
        // 需要翻译的字段
        HashSet<String> user = new HashSet<>();
        HashSet<String> orgs = new HashSet<>();
        // 取到需要翻译的字段
        getUserAndOrgkeys(item, user, orgs);
        Map<String, String> orgsDict = null;
        Map<String, String> namesDict = null;
        // 取到需要翻译的字段的值
        // 列表展示操作转译为中文
        if (!user.isEmpty()) {
            namesDict = getCodeList(list, user).stream().collect(Collectors.toMap(code -> code, OcaTranslatorUtils::getUserName));
        }
        if (!orgs.isEmpty()) {
            orgsDict = getCodeList(list, orgs).stream().collect(Collectors.toMap(code -> code, OcaTranslatorUtils::getOrgName));
        }
        if (orgsDict != null || namesDict != null) {
            List<Map<String, Object>> result = listTrans(list, user, orgs, namesDict, orgsDict);
            list.clear();
            list.addAll(result);
        }
    }

    /**
     * 列表翻译
     *
     * @param list      返回类型
     * @param user      字段名
     * @param orgs      字段名
     * @param namesDict 用户名缓存
     * @param orgsDict  机构名缓存
     * @return 处理后的列表
     */
    private List<Map<String, Object>> listTrans(
            List<Object> list,
            HashSet<String> user,
            HashSet<String> orgs,
            Map<String, String> namesDict,
            Map<String, String> orgsDict) {
        return list.stream()
                .map(obj -> {
                    Map<String, Object> map = objectMapper.convertValue(obj, Map.class);
                    for (String key : user) {
                        String userCode = Objects.toString(map.get(key));
                        if (userCode.contains(",")) {
                            String[] arr = userCode.split(",");
                            StringBuilder sb = new StringBuilder();
                            for (String s : arr) {
                                sb.append(Optional.ofNullable(namesDict.get(s)).orElse(s));
                                sb.append(",");
                            }
                            map.put(key + "Name", sb.substring(0, sb.length() - 1));
                        } else {
                            map.put(key + "Name", Optional.ofNullable(namesDict.get(userCode)).orElse(userCode));
                        }
                    }
                    for (String key : orgs) {
                        map.put(key + "Name", Optional.ofNullable(orgsDict.get(map.get(key))).orElse(Objects.toString(map.get(key))));
                    }
                    return map;
                })
                .collect(Collectors.toList());
    }

    /**
     * @param item, user, orgs
     * @author tangxun
     * @date 2020/9/16 11:01
     * @desc @修改历史: 修改时间 修改人员 修改原因
     */
    private void getUserAndOrgkeys(Object item, HashSet<String> user, HashSet<String> orgs) {
        if (item instanceof Map) {
            dealMap(((Map) item).keySet(), user, orgs);
        } else {
            Field[] fields = item.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (USER_SET.contains(field.getName())) {
                    user.add(field.getName());
                }
                if (ORG_SET.contains(field.getName())) {
                    orgs.add(field.getName());
                }
            }
        }
    }

    private void dealMap(Set<Object> keySet, HashSet<String> user, HashSet<String> orgs) {
        for (Object o : keySet) {
            String name = Objects.toString(o);
            if (USER_SET.contains(name)) {
                user.add(name);
            }
            if (ORG_SET.contains(name)) {
                orgs.add(name);
            }
        }
    }

    /**
     * @param list   返回的数据
     * @param keySet 待翻译的字段名
     * @return java.util.List<java.lang.String>
     * @author tangxun
     * @date 2020/9/4 4:34 下午
     * @version 1.0.0
     * @desc @修改历史: 修改时间 修改人员 修改原因
     */
    public List<String> getCodeList(List<Object> list, Set<String> keySet) {
        return list.stream()
                .map(obj -> getObjIdCode(obj, keySet))
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    public Set<String> getObjIdCode(Object obj, Set<String> keySet) {
        Set<String> res = new HashSet<>();
        keySet.forEach(field -> {
            String getMethod = "get" + field.substring(0, 1).toUpperCase() + field.substring(1);
            try {
                String code;
                if (obj instanceof Map) {
                    code = Optional.ofNullable(((Map) obj).get(field)).orElse("").toString();
                } else {
                    code = Optional.ofNullable(obj.getClass().getDeclaredMethod(getMethod).invoke(obj))
                            .orElse("")
                            .toString();
                }
                String separator = ",";
                if (code.contains(separator)) {
                    res.addAll(Arrays.asList(code.split(",")));
                } else {
                    res.add(code);
                }
            } catch (Exception e) {
                log.error("获取字段值报错", e);
            }
        });
        return res;
    }

}