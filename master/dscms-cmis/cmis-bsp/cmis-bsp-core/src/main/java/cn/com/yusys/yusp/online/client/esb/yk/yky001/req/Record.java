package cn.com.yusys.yusp.online.client.esb.yk.yky001.req;


public class Record {
    private static final long serialVersionUID = 1L;
    private String tradeCode;//    印章编号
    private String tradeCodeName;//    印章名称
    private String applyNum;//    印章数量

    public String getTradeCode() {
        return tradeCode;
    }

    public void setTradeCode(String tradeCode) {
        this.tradeCode = tradeCode;
    }

    public String getTradeCodeName() {
        return tradeCodeName;
    }

    public void setTradeCodeName(String tradeCodeName) {
        this.tradeCodeName = tradeCodeName;
    }

    public String getApplyNum() {
        return applyNum;
    }

    public void setApplyNum(String applyNum) {
        this.applyNum = applyNum;
    }

    @Override
    public String toString() {
        return "Record{" +
                "tradeCode='" + tradeCode + '\'' +
                ", tradeCodeName='" + tradeCodeName + '\'' +
                ", applyNum='" + applyNum + '\'' +
                '}';
    }

}
