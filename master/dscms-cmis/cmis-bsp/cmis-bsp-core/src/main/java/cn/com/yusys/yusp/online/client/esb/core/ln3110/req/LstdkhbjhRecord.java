package cn.com.yusys.yusp.online.client.esb.core.ln3110.req;

import java.math.BigDecimal;

/**
 * 请求Service：贷款还本计划
 */
public class LstdkhbjhRecord {
    private String dzhhkzhl;//定制还款种类
    private String xzuetqhk;//需足额提前还款
    private String dzhkriqi;//定制还款日期
    private BigDecimal huanbjee;//还本金额
    private String tqhkhxfs;//提前还款还息方式
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号

    public String getDzhhkzhl() {
        return dzhhkzhl;
    }

    public void setDzhhkzhl(String dzhhkzhl) {
        this.dzhhkzhl = dzhhkzhl;
    }

    public String getXzuetqhk() {
        return xzuetqhk;
    }

    public void setXzuetqhk(String xzuetqhk) {
        this.xzuetqhk = xzuetqhk;
    }

    public String getDzhkriqi() {
        return dzhkriqi;
    }

    public void setDzhkriqi(String dzhkriqi) {
        this.dzhkriqi = dzhkriqi;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public String getTqhkhxfs() {
        return tqhkhxfs;
    }

    public void setTqhkhxfs(String tqhkhxfs) {
        this.tqhkhxfs = tqhkhxfs;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    @Override
    public String toString() {
        return "Lstdkhbjh{" +
                "dzhhkzhl='" + dzhhkzhl + '\'' +
                ", xzuetqhk='" + xzuetqhk + '\'' +
                ", dzhkriqi='" + dzhkriqi + '\'' +
                ", huanbjee=" + huanbjee +
                ", tqhkhxfs='" + tqhkhxfs + '\'' +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                '}';
    }
}
