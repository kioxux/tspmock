package cn.com.yusys.yusp.online.client.esb.ypxt.contra.resp;

/**
 * 响应Service：押品与担保合同关系同步（处理码contra）
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:49
 */
public class Service {
    private String erorcd;//	响应码	否	char(4)	是	0000表示成功其它表示失败	erorcd
    private String erortx;//	响应信息	否	varchar(128)	否	失败详情	erortx

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "ContraRespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                '}';
    }
}
