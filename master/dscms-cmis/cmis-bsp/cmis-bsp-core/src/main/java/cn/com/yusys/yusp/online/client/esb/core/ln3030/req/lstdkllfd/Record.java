package cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkllfd;

import java.math.BigDecimal;

/**
 * 请求Service：贷款利率分段
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private Integer xuhaoooo;//序号
    private String lilvfdfs;//利率浮动方式
    private String lilvleix;//利率类型
    private String lilvtzfs;//利率调整方式
    private String qishriqi;//起始日期
    private String daoqriqi;//到期日期
    private BigDecimal lilvfdzh;//利率浮动值
    private BigDecimal zhchlilv;//正常利率
    private String lilvtzzq;//利率调整周期
    private String zclilvbh;//正常利率编号

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "xuhaoooo='" + xuhaoooo + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                '}';
    }
}
