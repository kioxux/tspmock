package cn.com.yusys.yusp.online.client.esb.irs.irs97.resp;


/**
 * 响应Service：新增授信时债项评级接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日 下午1:22:06
 */
public class Irs97RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Irs97RespService{" +
                "service=" + service +
                '}';
    }
}
