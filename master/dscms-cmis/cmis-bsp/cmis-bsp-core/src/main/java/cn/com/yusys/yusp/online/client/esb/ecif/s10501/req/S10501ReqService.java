package cn.com.yusys.yusp.online.client.esb.ecif.s10501.req;

/**
 * 请求Service：对私客户清单查询
 */
public class S10501ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
