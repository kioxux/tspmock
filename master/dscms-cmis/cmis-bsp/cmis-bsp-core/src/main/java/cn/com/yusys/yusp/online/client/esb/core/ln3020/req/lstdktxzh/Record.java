package cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdktxzh;

import java.math.BigDecimal;

public class Record {
    private BigDecimal tiexibil;//贴息比率
    private String shengxrq;//生效日期
    private String daoqriqi;//到期日期
    private String tiexizhh;//贴息账号
    private String txzhhzxh;//贴息账号子序号
    private String txzhhmch;//贴息账户名称
    private String fxjxbzhi;//贴息计复利标志
    private String zdkoukbz;//自动扣款标志

    public BigDecimal getTiexibil() {
        return tiexibil;
    }

    public void setTiexibil(BigDecimal tiexibil) {
        this.tiexibil = tiexibil;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getTiexizhh() {
        return tiexizhh;
    }

    public void setTiexizhh(String tiexizhh) {
        this.tiexizhh = tiexizhh;
    }

    public String getTxzhhzxh() {
        return txzhhzxh;
    }

    public void setTxzhhzxh(String txzhhzxh) {
        this.txzhhzxh = txzhhzxh;
    }

    public String getTxzhhmch() {
        return txzhhmch;
    }

    public void setTxzhhmch(String txzhhmch) {
        this.txzhhmch = txzhhmch;
    }

    public String getFxjxbzhi() {
        return fxjxbzhi;
    }

    public void setFxjxbzhi(String fxjxbzhi) {
        this.fxjxbzhi = fxjxbzhi;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    @Override
    public String toString() {
        return "Record{" +
                "tiexibil=" + tiexibil +
                ", shengxrq='" + shengxrq + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", tiexizhh='" + tiexizhh + '\'' +
                ", txzhhzxh='" + txzhhzxh + '\'' +
                ", txzhhmch='" + txzhhmch + '\'' +
                ", fxjxbzhi='" + fxjxbzhi + '\'' +
                ", zdkoukbz='" + zdkoukbz + '\'' +
                '}';
    }
}
