package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * GXP相关Builder
 *
 * @author leehuang
 * @version 1.0
 * @since 2021/5/25 21:56
 */
public class GxpBuilder {
    private static Logger logger = LoggerFactory.getLogger(GxpBuilder.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");

    /**
     * GXP请求Head Builder
     *
     * @author leehuang
     * @version 1.0
     * @since 2021/5/25 21:56
     */
    public static GxpReqHead gxpReqHeadBuilder(String tradeCodeKey, String tradeCodeValue, String servtpKey, String servtpValue, String userid, String brchno) {
        logger.info(TradeLogConstants.GXP_BEGIN_PREFIX_LOGGER, tradeCodeKey, tradeCodeValue);
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, servtpKey, servtpValue);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(servtpKey);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, servtpKey, servtpValue, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

        LocalDateTime now = LocalDateTime.now();
        String trandt = tranDateFormtter.format(now);//    交易日期
        String tranti = tranTimestampFormatter.format(now);//    交易时间

        GxpReqHead gxpReqHead = GenericBuilder.of(GxpReqHead::new)
                .with(GxpReqHead::setPrcscd, tradeCodeKey) //业务处理码 长度一般为6位，且是字母开头，一般字母为小写
                .with(GxpReqHead::setServtp, servtpKey)//渠道
                .with(GxpReqHead::setTransq, servsq)//交易流水号
                .with(GxpReqHead::setUserid, userid)//柜员号
                .with(GxpReqHead::setBrchno, brchno)//机构号
                .with(GxpReqHead::setTrandt, trandt)//交易日期 YYYYMMDD
                .with(GxpReqHead::setTranti, tranti)//交易时间 HHmmss
                .build();
        logger.info(TradeLogConstants.GXP_END_PREFIX_LOGGER, servtpKey, servtpValue, JSON.toJSONString(gxpReqHead));
        return gxpReqHead;
    }

}
