package cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/16 20:48
 * @since 2021/6/16 20:48
 */
public class Body {
    private String cardno;//卡号
    private String crcycd;//币种
    private String custnm;//姓名
    private String prctnm;//产品名称
    private BigDecimal cdtlmt;//信用额度
    private BigDecimal tplimt;//临时额度
    private BigDecimal remtbl;//账单剩余应还款额
    private String bgdate;//临时额度开始日期
    private String eddate;//临时额度结束日期
    private BigDecimal chlmrt;//取现额度比例
    private BigDecimal otrate;//授权超限比例
    private BigDecimal lnlmrt;//额度内分期额度比例
    private BigDecimal stcrbl;//当前余额
    private BigDecimal cashbl;//取现余额
    private BigDecimal prcpbl;//本金余额
    private BigDecimal loanbl;//额度内分期余额
    private BigDecimal duptam;//争议金额
    private BigDecimal begnbl;//期初余额
    private BigDecimal pmtdue;//到期还款日余额
    private BigDecimal qlgcbl;//账单实时剩余应还款额
    private String gcdyfl;//是否已全额还款
    private BigDecimal atcsot;//账户层取现额度
    private BigDecimal avlbot;//综合信用额度
    private String stdate;//创建日期
    private String ownbch;//发卡网点
    private String billcl;//账单周期
    private String stmflg;//账单标志
    private String mailad;//账单寄送地址标志
    private String stmdtp;//账单介质类型
    private String bkcode;//锁定码
    private String agescd;//账龄
    private BigDecimal unmcdb;//未匹配借记金额
    private BigDecimal unmccs;//未匹配取现金额
    private BigDecimal unmccr;//未匹配贷记金额
    private String duaflg;//本币溢缴款还外币指示
    private BigDecimal pmtamt;//上笔还款金额
    private String pmtdat;//上一还款日期
    private String ltsmdt;//上个账单日期
    private String ntsmdt;//下个账单日期
    private String pmdudt;//到期还款日期
    private String dddate;//约定还款日期
    private String grcdat;//宽限日期
    private String clsdat;//最终销户日期
    private String frsmdt;//首个账单日期
    private String cancdt;//销卡销户日期
    private String offdat;//转呆账日期
    private String purcdt;//首次消费日期
    private BigDecimal purcam;//首次消费金额
    private BigDecimal toduam;//最小还款额
    private String dlcrin;//是否存在双币标识
    private String dlcrcd;//外币币种
    private BigDecimal tlrate;//随用金费率
    private String acctno;//账户编号

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getPrctnm() {
        return prctnm;
    }

    public void setPrctnm(String prctnm) {
        this.prctnm = prctnm;
    }

    public BigDecimal getCdtlmt() {
        return cdtlmt;
    }

    public void setCdtlmt(BigDecimal cdtlmt) {
        this.cdtlmt = cdtlmt;
    }

    public BigDecimal getTplimt() {
        return tplimt;
    }

    public void setTplimt(BigDecimal tplimt) {
        this.tplimt = tplimt;
    }

    public BigDecimal getRemtbl() {
        return remtbl;
    }

    public void setRemtbl(BigDecimal remtbl) {
        this.remtbl = remtbl;
    }

    public String getBgdate() {
        return bgdate;
    }

    public void setBgdate(String bgdate) {
        this.bgdate = bgdate;
    }

    public String getEddate() {
        return eddate;
    }

    public void setEddate(String eddate) {
        this.eddate = eddate;
    }

    public BigDecimal getChlmrt() {
        return chlmrt;
    }

    public void setChlmrt(BigDecimal chlmrt) {
        this.chlmrt = chlmrt;
    }

    public BigDecimal getOtrate() {
        return otrate;
    }

    public void setOtrate(BigDecimal otrate) {
        this.otrate = otrate;
    }

    public BigDecimal getLnlmrt() {
        return lnlmrt;
    }

    public void setLnlmrt(BigDecimal lnlmrt) {
        this.lnlmrt = lnlmrt;
    }

    public BigDecimal getStcrbl() {
        return stcrbl;
    }

    public void setStcrbl(BigDecimal stcrbl) {
        this.stcrbl = stcrbl;
    }

    public BigDecimal getCashbl() {
        return cashbl;
    }

    public void setCashbl(BigDecimal cashbl) {
        this.cashbl = cashbl;
    }

    public BigDecimal getPrcpbl() {
        return prcpbl;
    }

    public void setPrcpbl(BigDecimal prcpbl) {
        this.prcpbl = prcpbl;
    }

    public BigDecimal getLoanbl() {
        return loanbl;
    }

    public void setLoanbl(BigDecimal loanbl) {
        this.loanbl = loanbl;
    }

    public BigDecimal getDuptam() {
        return duptam;
    }

    public void setDuptam(BigDecimal duptam) {
        this.duptam = duptam;
    }

    public BigDecimal getBegnbl() {
        return begnbl;
    }

    public void setBegnbl(BigDecimal begnbl) {
        this.begnbl = begnbl;
    }

    public BigDecimal getPmtdue() {
        return pmtdue;
    }

    public void setPmtdue(BigDecimal pmtdue) {
        this.pmtdue = pmtdue;
    }

    public BigDecimal getQlgcbl() {
        return qlgcbl;
    }

    public void setQlgcbl(BigDecimal qlgcbl) {
        this.qlgcbl = qlgcbl;
    }

    public String getGcdyfl() {
        return gcdyfl;
    }

    public void setGcdyfl(String gcdyfl) {
        this.gcdyfl = gcdyfl;
    }

    public BigDecimal getAtcsot() {
        return atcsot;
    }

    public void setAtcsot(BigDecimal atcsot) {
        this.atcsot = atcsot;
    }

    public BigDecimal getAvlbot() {
        return avlbot;
    }

    public void setAvlbot(BigDecimal avlbot) {
        this.avlbot = avlbot;
    }

    public String getStdate() {
        return stdate;
    }

    public void setStdate(String stdate) {
        this.stdate = stdate;
    }

    public String getOwnbch() {
        return ownbch;
    }

    public void setOwnbch(String ownbch) {
        this.ownbch = ownbch;
    }

    public String getBillcl() {
        return billcl;
    }

    public void setBillcl(String billcl) {
        this.billcl = billcl;
    }

    public String getStmflg() {
        return stmflg;
    }

    public void setStmflg(String stmflg) {
        this.stmflg = stmflg;
    }

    public String getMailad() {
        return mailad;
    }

    public void setMailad(String mailad) {
        this.mailad = mailad;
    }

    public String getStmdtp() {
        return stmdtp;
    }

    public void setStmdtp(String stmdtp) {
        this.stmdtp = stmdtp;
    }

    public String getBkcode() {
        return bkcode;
    }

    public void setBkcode(String bkcode) {
        this.bkcode = bkcode;
    }

    public String getAgescd() {
        return agescd;
    }

    public void setAgescd(String agescd) {
        this.agescd = agescd;
    }

    public BigDecimal getUnmcdb() {
        return unmcdb;
    }

    public void setUnmcdb(BigDecimal unmcdb) {
        this.unmcdb = unmcdb;
    }

    public BigDecimal getUnmccs() {
        return unmccs;
    }

    public void setUnmccs(BigDecimal unmccs) {
        this.unmccs = unmccs;
    }

    public BigDecimal getUnmccr() {
        return unmccr;
    }

    public void setUnmccr(BigDecimal unmccr) {
        this.unmccr = unmccr;
    }

    public String getDuaflg() {
        return duaflg;
    }

    public void setDuaflg(String duaflg) {
        this.duaflg = duaflg;
    }

    public BigDecimal getPmtamt() {
        return pmtamt;
    }

    public void setPmtamt(BigDecimal pmtamt) {
        this.pmtamt = pmtamt;
    }

    public String getPmtdat() {
        return pmtdat;
    }

    public void setPmtdat(String pmtdat) {
        this.pmtdat = pmtdat;
    }

    public String getLtsmdt() {
        return ltsmdt;
    }

    public void setLtsmdt(String ltsmdt) {
        this.ltsmdt = ltsmdt;
    }

    public String getNtsmdt() {
        return ntsmdt;
    }

    public void setNtsmdt(String ntsmdt) {
        this.ntsmdt = ntsmdt;
    }

    public String getPmdudt() {
        return pmdudt;
    }

    public void setPmdudt(String pmdudt) {
        this.pmdudt = pmdudt;
    }

    public String getDddate() {
        return dddate;
    }

    public void setDddate(String dddate) {
        this.dddate = dddate;
    }

    public String getGrcdat() {
        return grcdat;
    }

    public void setGrcdat(String grcdat) {
        this.grcdat = grcdat;
    }

    public String getClsdat() {
        return clsdat;
    }

    public void setClsdat(String clsdat) {
        this.clsdat = clsdat;
    }

    public String getFrsmdt() {
        return frsmdt;
    }

    public void setFrsmdt(String frsmdt) {
        this.frsmdt = frsmdt;
    }

    public String getCancdt() {
        return cancdt;
    }

    public void setCancdt(String cancdt) {
        this.cancdt = cancdt;
    }

    public String getOffdat() {
        return offdat;
    }

    public void setOffdat(String offdat) {
        this.offdat = offdat;
    }

    public String getPurcdt() {
        return purcdt;
    }

    public void setPurcdt(String purcdt) {
        this.purcdt = purcdt;
    }

    public BigDecimal getPurcam() {
        return purcam;
    }

    public void setPurcam(BigDecimal purcam) {
        this.purcam = purcam;
    }

    public BigDecimal getToduam() {
        return toduam;
    }

    public void setToduam(BigDecimal toduam) {
        this.toduam = toduam;
    }

    public String getDlcrin() {
        return dlcrin;
    }

    public void setDlcrin(String dlcrin) {
        this.dlcrin = dlcrin;
    }

    public String getDlcrcd() {
        return dlcrcd;
    }

    public void setDlcrcd(String dlcrcd) {
        this.dlcrcd = dlcrcd;
    }

    public BigDecimal getTlrate() {
        return tlrate;
    }

    public void setTlrate(BigDecimal tlrate) {
        this.tlrate = tlrate;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    @Override
    public String toString() {
        return "Service{" +
                "cardno='" + cardno + '\'' +
                "crcycd='" + crcycd + '\'' +
                "custnm='" + custnm + '\'' +
                "prctnm='" + prctnm + '\'' +
                "cdtlmt='" + cdtlmt + '\'' +
                "tplimt='" + tplimt + '\'' +
                "remtbl='" + remtbl + '\'' +
                "bgdate='" + bgdate + '\'' +
                "eddate='" + eddate + '\'' +
                "chlmrt='" + chlmrt + '\'' +
                "otrate='" + otrate + '\'' +
                "lnlmrt='" + lnlmrt + '\'' +
                "stcrbl='" + stcrbl + '\'' +
                "cashbl='" + cashbl + '\'' +
                "prcpbl='" + prcpbl + '\'' +
                "loanbl='" + loanbl + '\'' +
                "duptam='" + duptam + '\'' +
                "begnbl='" + begnbl + '\'' +
                "pmtdue='" + pmtdue + '\'' +
                "qlgcbl='" + qlgcbl + '\'' +
                "gcdyfl='" + gcdyfl + '\'' +
                "atcsot='" + atcsot + '\'' +
                "avlbot='" + avlbot + '\'' +
                "stdate='" + stdate + '\'' +
                "ownbch='" + ownbch + '\'' +
                "billcl='" + billcl + '\'' +
                "stmflg='" + stmflg + '\'' +
                "mailad='" + mailad + '\'' +
                "stmdtp='" + stmdtp + '\'' +
                "bkcode='" + bkcode + '\'' +
                "agescd='" + agescd + '\'' +
                "unmcdb='" + unmcdb + '\'' +
                "unmccs='" + unmccs + '\'' +
                "unmccr='" + unmccr + '\'' +
                "duaflg='" + duaflg + '\'' +
                "pmtamt='" + pmtamt + '\'' +
                "pmtdat='" + pmtdat + '\'' +
                "ltsmdt='" + ltsmdt + '\'' +
                "ntsmdt='" + ntsmdt + '\'' +
                "pmdudt='" + pmdudt + '\'' +
                "dddate='" + dddate + '\'' +
                "grcdat='" + grcdat + '\'' +
                "clsdat='" + clsdat + '\'' +
                "frsmdt='" + frsmdt + '\'' +
                "cancdt='" + cancdt + '\'' +
                "offdat='" + offdat + '\'' +
                "purcdt='" + purcdt + '\'' +
                "purcam='" + purcam + '\'' +
                "toduam='" + toduam + '\'' +
                "dlcrin='" + dlcrin + '\'' +
                "dlcrcd='" + dlcrcd + '\'' +
                "tlrate='" + tlrate + '\'' +
                "acctno='" + acctno + '\'' +
                '}';
    }
}
