package cn.com.yusys.yusp.online.client.esb.irs.xirs10.req;

/**
 * 请求Service：公司客户信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Xirs10ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Xirs10ReqService{" +
				"service=" + service +
				'}';
	}
}
