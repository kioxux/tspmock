package cn.com.yusys.yusp.online.client.esb.core.ln3106.resp;

import java.math.BigDecimal;

public class Record {
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String kehmingc;//客户名称
    private Integer jiximxxh;//计息明细序号
    private String yngyjigo;//营业机构
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String dkqixian;//贷款期限(月)
    private String nyuelilv;//年/月利率标识
    private BigDecimal yzhxlilv;//原执行利率
    private BigDecimal zhchlilv;//正常利率
    private BigDecimal yshjlilv;//原实际利率
    private BigDecimal shijlilv;//实际利率
    private String yuqinyll;//逾期年月利率
    private BigDecimal yyuqlilv;//原逾期利率
    private BigDecimal yuqililv;//逾期利率
    private String fulilvny;//复利利率年月标识
    private BigDecimal yfullilv;//原复利利率
    private BigDecimal fulililv;//复利利率
    private BigDecimal yjznyllv;//原挤占挪用利率
    private BigDecimal jznylilv;//挤占挪用利率
    private String huobdhao;//货币代号
    private BigDecimal zhchbjin;//正常本金
    private BigDecimal yuqibjin;//逾期本金
    private BigDecimal dzhibjin;//呆滞本金
    private BigDecimal daizbjin;//呆账本金
    private BigDecimal dkuanjij;//贷款基金
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal csyjlixi;//催收应计利息
    private BigDecimal ysqianxi;//应收欠息
    private BigDecimal csqianxi;//催收欠息
    private BigDecimal ysyjfaxi;//应收应计罚息
    private BigDecimal csyjfaxi;//催收应计罚息
    private BigDecimal yshofaxi;//应收罚息
    private BigDecimal cshofaxi;//催收罚息
    private BigDecimal yingjifx;//应计复息
    private BigDecimal fuxiiiii;//复息
    private BigDecimal yingjitx;//应计贴息
    private BigDecimal yingshtx;//应收贴息
    private BigDecimal tiexfuli;//贴息复利
    private BigDecimal daitanlx;//待摊利息
    private BigDecimal hexiaobj;//核销本金
    private BigDecimal hexiaolx;//核销利息
    private String jiaoyirq;//交易日期
    private String jiaoyijg;//交易机构
    private String jiaoyigy;//交易柜员
    private String jiaoyils;//交易流水
    private String jiaoyisj;//交易事件
    private String shjshuom;//事件说明
    private String jiaoyima;//交易码
    private String zhaiyoms;//摘要


    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public Integer getJiximxxh() {
        return jiximxxh;
    }

    public void setJiximxxh(Integer jiximxxh) {
        this.jiximxxh = jiximxxh;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getYzhxlilv() {
        return yzhxlilv;
    }

    public void setYzhxlilv(BigDecimal yzhxlilv) {
        this.yzhxlilv = yzhxlilv;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getYshjlilv() {
        return yshjlilv;
    }

    public void setYshjlilv(BigDecimal yshjlilv) {
        this.yshjlilv = yshjlilv;
    }

    public BigDecimal getShijlilv() {
        return shijlilv;
    }

    public void setShijlilv(BigDecimal shijlilv) {
        this.shijlilv = shijlilv;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYyuqlilv() {
        return yyuqlilv;
    }

    public void setYyuqlilv(BigDecimal yyuqlilv) {
        this.yyuqlilv = yyuqlilv;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getYfullilv() {
        return yfullilv;
    }

    public void setYfullilv(BigDecimal yfullilv) {
        this.yfullilv = yfullilv;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public BigDecimal getYjznyllv() {
        return yjznyllv;
    }

    public void setYjznyllv(BigDecimal yjznyllv) {
        this.yjznyllv = yjznyllv;
    }

    public BigDecimal getJznylilv() {
        return jznylilv;
    }

    public void setJznylilv(BigDecimal jznylilv) {
        this.jznylilv = jznylilv;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getDkuanjij() {
        return dkuanjij;
    }

    public void setDkuanjij(BigDecimal dkuanjij) {
        this.dkuanjij = dkuanjij;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getTiexfuli() {
        return tiexfuli;
    }

    public void setTiexfuli(BigDecimal tiexfuli) {
        this.tiexfuli = tiexfuli;
    }

    public BigDecimal getDaitanlx() {
        return daitanlx;
    }

    public void setDaitanlx(BigDecimal daitanlx) {
        this.daitanlx = daitanlx;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getShjshuom() {
        return shjshuom;
    }

    public void setShjshuom(String shjshuom) {
        this.shjshuom = shjshuom;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", jiximxxh=" + jiximxxh +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", dkqixian='" + dkqixian + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", yzhxlilv=" + yzhxlilv +
                ", zhchlilv=" + zhchlilv +
                ", yshjlilv=" + yshjlilv +
                ", shijlilv=" + shijlilv +
                ", yuqinyll='" + yuqinyll + '\'' +
                ", yyuqlilv=" + yyuqlilv +
                ", yuqililv=" + yuqililv +
                ", fulilvny='" + fulilvny + '\'' +
                ", yfullilv=" + yfullilv +
                ", fulililv=" + fulililv +
                ", yjznyllv=" + yjznyllv +
                ", jznylilv=" + jznylilv +
                ", huobdhao='" + huobdhao + '\'' +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", dzhibjin=" + dzhibjin +
                ", daizbjin=" + daizbjin +
                ", dkuanjij=" + dkuanjij +
                ", ysyjlixi=" + ysyjlixi +
                ", csyjlixi=" + csyjlixi +
                ", ysqianxi=" + ysqianxi +
                ", csqianxi=" + csqianxi +
                ", ysyjfaxi=" + ysyjfaxi +
                ", csyjfaxi=" + csyjfaxi +
                ", yshofaxi=" + yshofaxi +
                ", cshofaxi=" + cshofaxi +
                ", yingjifx=" + yingjifx +
                ", fuxiiiii=" + fuxiiiii +
                ", yingjitx=" + yingjitx +
                ", yingshtx=" + yingshtx +
                ", tiexfuli=" + tiexfuli +
                ", daitanlx=" + daitanlx +
                ", hexiaobj=" + hexiaobj +
                ", hexiaolx=" + hexiaolx +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jiaoyisj='" + jiaoyisj + '\'' +
                ", shjshuom='" + shjshuom + '\'' +
                ", jiaoyima='" + jiaoyima + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                '}';
    }
}
