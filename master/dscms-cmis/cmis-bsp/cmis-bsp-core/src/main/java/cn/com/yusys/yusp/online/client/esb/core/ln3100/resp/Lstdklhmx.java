package cn.com.yusys.yusp.online.client.esb.core.ln3100.resp;


import java.math.BigDecimal;

/**
 * 响应Service：贷款联合放款
 *
 * @author chenyong
 * @version 1.0
 */
public class Lstdklhmx {
    private String canyfdma;//参与方代码
    private String canyfhum;//参与方户名
    private String lianhfsh;//联合方式
    private String lhdkleix;//联合贷款类型
    private BigDecimal canyjine;//参与金额
    private BigDecimal canybili;//参与比例
    private String zjingjbz;//资金归集标志
    private String zjlyzhao;//资金来源账号
    private String zjlyzzxh;//资金来源账号子序号
    private String zjzrzhao;//资金转入账号
    private String zjzrzzxh;//资金转入账号子序号
    private String canyjjha;//参与借据号
    private String bjghrzzh;//本金归还入账账号
    private String bjghrzxh;//本金归还入账账号子序号
    private String lxghrzzh;//利息归还入账账号
    private String lxghrzxh;//利息归还入账账号子序号
    private BigDecimal yishbenj;//已收回本金
    private BigDecimal yishlixi;//已收回利息
    private BigDecimal dhzhbenj;//待划转本金
    private BigDecimal dhzhlixi;//待划转利息

    public String getCanyfdma() {
        return canyfdma;
    }

    public void setCanyfdma(String canyfdma) {
        this.canyfdma = canyfdma;
    }

    public String getCanyfhum() {
        return canyfhum;
    }

    public void setCanyfhum(String canyfhum) {
        this.canyfhum = canyfhum;
    }

    public String getLianhfsh() {
        return lianhfsh;
    }

    public void setLianhfsh(String lianhfsh) {
        this.lianhfsh = lianhfsh;
    }

    public String getLhdkleix() {
        return lhdkleix;
    }

    public void setLhdkleix(String lhdkleix) {
        this.lhdkleix = lhdkleix;
    }

    public BigDecimal getCanyjine() {
        return canyjine;
    }

    public void setCanyjine(BigDecimal canyjine) {
        this.canyjine = canyjine;
    }

    public BigDecimal getCanybili() {
        return canybili;
    }

    public void setCanybili(BigDecimal canybili) {
        this.canybili = canybili;
    }

    public String getZjingjbz() {
        return zjingjbz;
    }

    public void setZjingjbz(String zjingjbz) {
        this.zjingjbz = zjingjbz;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public String getCanyjjha() {
        return canyjjha;
    }

    public void setCanyjjha(String canyjjha) {
        this.canyjjha = canyjjha;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public BigDecimal getYishbenj() {
        return yishbenj;
    }

    public void setYishbenj(BigDecimal yishbenj) {
        this.yishbenj = yishbenj;
    }

    public BigDecimal getYishlixi() {
        return yishlixi;
    }

    public void setYishlixi(BigDecimal yishlixi) {
        this.yishlixi = yishlixi;
    }

    public BigDecimal getDhzhbenj() {
        return dhzhbenj;
    }

    public void setDhzhbenj(BigDecimal dhzhbenj) {
        this.dhzhbenj = dhzhbenj;
    }

    public BigDecimal getDhzhlixi() {
        return dhzhlixi;
    }

    public void setDhzhlixi(BigDecimal dhzhlixi) {
        this.dhzhlixi = dhzhlixi;
    }

    @Override
    public String toString() {
        return "Service{" +
                "canyfdma='" + canyfdma + '\'' +
                "canyfhum='" + canyfhum + '\'' +
                "lianhfsh='" + lianhfsh + '\'' +
                "lhdkleix='" + lhdkleix + '\'' +
                "canyjine='" + canyjine + '\'' +
                "canybili='" + canybili + '\'' +
                "zjingjbz='" + zjingjbz + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "zjzrzhao='" + zjzrzhao + '\'' +
                "zjzrzzxh='" + zjzrzzxh + '\'' +
                "canyjjha='" + canyjjha + '\'' +
                "bjghrzzh='" + bjghrzzh + '\'' +
                "bjghrzxh='" + bjghrzxh + '\'' +
                "lxghrzzh='" + lxghrzzh + '\'' +
                "lxghrzxh='" + lxghrzxh + '\'' +
                "yishbenj='" + yishbenj + '\'' +
                "yishlixi='" + yishlixi + '\'' +
                "dhzhbenj='" + dhzhbenj + '\'' +
                "dhzhlixi='" + dhzhlixi + '\'' +
                '}';
    }
}
