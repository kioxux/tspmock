package cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 11:14
 * @since 2021/6/4 11:14
 */
public class Record {
    private String subTaskId;//子任务id
    private String templateId;//模板id
    private String templateName;//模板名称
    private String recogStatus;//子任务识别状态，0待识别，1识别中，2识别完成，3识别失败
    private String checkStatus;//子任务核对状态，0待核对，1核对中，2核对完成
    private String balanceStatus;//子任务试算平衡状态，0试算平衡，1试算不平衡
    private String totalFileNum;//子任务下文件总数
    private String isMatched;//是否匹配标志，0匹配到模板的子任务，1未匹配模板的子任务

    public String getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(String subTaskId) {
        this.subTaskId = subTaskId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getRecogStatus() {
        return recogStatus;
    }

    public void setRecogStatus(String recogStatus) {
        this.recogStatus = recogStatus;
    }

    public String getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(String checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getBalanceStatus() {
        return balanceStatus;
    }

    public void setBalanceStatus(String balanceStatus) {
        this.balanceStatus = balanceStatus;
    }

    public String getTotalFileNum() {
        return totalFileNum;
    }

    public void setTotalFileNum(String totalFileNum) {
        this.totalFileNum = totalFileNum;
    }

    public String getIsMatched() {
        return isMatched;
    }

    public void setIsMatched(String isMatched) {
        this.isMatched = isMatched;
    }

    @Override
    public String toString() {
        return "Record{" +
                "subTaskId='" + subTaskId + '\'' +
                ", templateId='" + templateId + '\'' +
                ", templateName='" + templateName + '\'' +
                ", recogStatus='" + recogStatus + '\'' +
                ", checkStatus='" + checkStatus + '\'' +
                ", balanceStatus='" + balanceStatus + '\'' +
                ", totalFileNum='" + totalFileNum + '\'' +
                ", isMatched='" + isMatched + '\'' +
                '}';
    }
}
