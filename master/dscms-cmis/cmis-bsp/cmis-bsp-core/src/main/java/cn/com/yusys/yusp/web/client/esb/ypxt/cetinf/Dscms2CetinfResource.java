package cn.com.yusys.yusp.web.client.esb.ypxt.cetinf;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.CetinfReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.Record;
import cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.resp.CetinfRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(cetinf)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2CetinfResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2CetinfResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 权证信息同步接口（处理码cetinf）
     *
     * @param cetinfReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cetinf:权证信息同步接口")
    @PostMapping("/cetinf")
    protected @ResponseBody
    ResultDto<CetinfRespDto> cetinf(@Validated @RequestBody CetinfReqDto cetinfReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CETINF.key, EsbEnum.TRADE_CODE_CETINF.value, JSON.toJSONString(cetinfReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.List list = new cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.List();
        CetinfReqService cetinfReqService = new CetinfReqService();
        CetinfRespService cetinfRespService = new CetinfRespService();
        CetinfRespDto cetinfRespDto = new CetinfRespDto();
        ResultDto<CetinfRespDto> cetinfResultDto = new ResultDto<CetinfRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将CetinfReqDto转换成reqService
            BeanUtils.copyProperties(cetinfReqDto, reqService);
            if (CollectionUtils.nonEmpty(cetinfReqDto.getList())) {
                List<CetinfListInfo> cetinfListInfos = Optional.ofNullable(cetinfReqDto.getList()).orElseGet(() -> new ArrayList<>());
                List<Record> recordList = new ArrayList<Record>();
                for (CetinfListInfo cetinfListInfo : cetinfListInfos) {
                    cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req.Record();
                    BeanUtils.copyProperties(cetinfListInfo, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CETINF.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(EsbEnum.BRCHNO_YPXT.key);//    全局流水
            cetinfReqService.setService(reqService);
            // 将cetinfReqService转换成cetinfReqServiceMap
            Map cetinfReqServiceMap = beanMapUtil.beanToMap(cetinfReqService);
            context.put("tradeDataMap", cetinfReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CETINF.key, EsbEnum.TRADE_CODE_CETINF.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CETINF.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CETINF.key, EsbEnum.TRADE_CODE_CETINF.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cetinfRespService = beanMapUtil.mapToBean(tradeDataMap, CetinfRespService.class, CetinfRespService.class);
            respService = cetinfRespService.getService();

            //  将CetinfRespDto封装到ResultDto<CetinfRespDto>
            cetinfResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            cetinfResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成CetinfRespDto
                BeanUtils.copyProperties(respService, cetinfRespDto);
                cetinfResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cetinfResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cetinfResultDto.setCode(EpbEnum.EPB099999.key);
                cetinfResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CETINF.key, EsbEnum.TRADE_CODE_CETINF.value, e.getMessage());
            cetinfResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cetinfResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cetinfResultDto.setData(cetinfRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CETINF.key, EsbEnum.TRADE_CODE_CETINF.value, JSON.toJSONString(cetinfResultDto));
        return cetinfResultDto;
    }
}
