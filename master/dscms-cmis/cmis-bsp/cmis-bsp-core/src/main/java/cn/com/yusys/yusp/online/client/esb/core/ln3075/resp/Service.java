package cn.com.yusys.yusp.online.client.esb.core.ln3075.resp;

import java.math.BigDecimal;

/**
 * 响应Service：逾期贷款展期
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水


    private BigDecimal zhanqixh;//展期序号
    private String zhanqirq;//展期日期
    private BigDecimal zhanqije;//展期金额
    private String zhanqhth;//展期合同号
    private String zhanqdqr;//展期到期日
    private String lilvtzzq;//利率调整周期
    private String lilvtzfs;//利率调整方式
    private String lilvleix;//利率类型
    private BigDecimal lilvfdzh;//利率浮动值
    private String lilvfdfs;//利率浮动方式
    private String kehuzwmc;//客户名
    private String kehuhaoo;//客户号
    private String kaihriqi;//开户日期
    private String kaihguiy;//开户柜员
    private String jiaoyils;//交易流水
    private String huobdhao;//货币代号
    private String hetongbh;//合同编号
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String daikczbz;//业务操作标志

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public BigDecimal getZhanqixh() {
        return zhanqixh;
    }

    public void setZhanqixh(BigDecimal zhanqixh) {
        this.zhanqixh = zhanqixh;
    }

    public String getZhanqirq() {
        return zhanqirq;
    }

    public void setZhanqirq(String zhanqirq) {
        this.zhanqirq = zhanqirq;
    }

    public BigDecimal getZhanqije() {
        return zhanqije;
    }

    public void setZhanqije(BigDecimal zhanqije) {
        this.zhanqije = zhanqije;
    }

    public String getZhanqhth() {
        return zhanqhth;
    }

    public void setZhanqhth(String zhanqhth) {
        this.zhanqhth = zhanqhth;
    }

    public String getZhanqdqr() {
        return zhanqdqr;
    }

    public void setZhanqdqr(String zhanqdqr) {
        this.zhanqdqr = zhanqdqr;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zhanqixh=" + zhanqixh +
                ", zhanqirq='" + zhanqirq + '\'' +
                ", zhanqije=" + zhanqije +
                ", zhanqhth='" + zhanqhth + '\'' +
                ", zhanqdqr='" + zhanqdqr + '\'' +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                '}';
    }
}
