package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerhouselist;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:10
 * @since 2021/7/9 14:10
 */
public class Record {
    private String rearea;//登记簿概要信息-建筑面积
    private String reinar;//登记簿概要信息-套内建筑面积
    private String reosha;//登记簿概要信息-分摊建筑面积
    private String reouse;//登记簿概要信息-房屋用途
    private String reonat;//登记簿概要信息-房屋性质
    private String reotla;//登记簿概要信息-总层数
    private String reosla;//登记簿概要信息-所在层
    private String locate;//登记簿概要信息-坐落

    public String getRearea() {
        return rearea;
    }

    public void setRearea(String rearea) {
        this.rearea = rearea;
    }

    public String getReinar() {
        return reinar;
    }

    public void setReinar(String reinar) {
        this.reinar = reinar;
    }

    public String getReosha() {
        return reosha;
    }

    public void setReosha(String reosha) {
        this.reosha = reosha;
    }

    public String getReouse() {
        return reouse;
    }

    public void setReouse(String reouse) {
        this.reouse = reouse;
    }

    public String getReonat() {
        return reonat;
    }

    public void setReonat(String reonat) {
        this.reonat = reonat;
    }

    public String getReotla() {
        return reotla;
    }

    public void setReotla(String reotla) {
        this.reotla = reotla;
    }

    public String getReosla() {
        return reosla;
    }

    public void setReosla(String reosla) {
        this.reosla = reosla;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    @Override
    public String toString() {
        return "Record{" +
                "rearea='" + rearea + '\'' +
                ", reinar='" + reinar + '\'' +
                ", reosha='" + reosha + '\'' +
                ", reouse='" + reouse + '\'' +
                ", reonat='" + reonat + '\'' +
                ", reotla='" + reotla + '\'' +
                ", reosla='" + reosla + '\'' +
                ", locate='" + locate + '\'' +
                '}';
    }
}
