package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp;

import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.assetsLiabilitieList.Record;

import java.util.List;

public class AssetsLiabilitieList {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.assetsLiabilitieList.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "AssetsLiabilitieList{" +
                "record=" + record +
                '}';
    }
}
