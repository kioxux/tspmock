package cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.req;

import cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.req.Service;

/**
 * 请求Service：贷后任务推送接口
 *
 * @author wrw
 * @version 1.0
 * @since 2021年8月28日 10:55
 */
public class Yxgc01ReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
