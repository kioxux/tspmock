package cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp;

/**
 * 响应Service：分期审核
 *
 * @author chenyong
 * @version 1.0
 */
public class Body {
    private String cardno;//卡号
    private String rgstid;//分期申请号
    private String loanit;//分期总本金
    private String lnrgst;//分期注册状态

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getRgstid() {
        return rgstid;
    }

    public void setRgstid(String rgstid) {
        this.rgstid = rgstid;
    }

    public String getLoanit() {
        return loanit;
    }

    public void setLoanit(String loanit) {
        this.loanit = loanit;
    }

    public String getLnrgst() {
        return lnrgst;
    }

    public void setLnrgst(String lnrgst) {
        this.lnrgst = lnrgst;
    }

    @Override
    public String toString() {
        return "Service{" +
                "cardno='" + cardno + '\'' +
                "rgstid='" + rgstid + '\'' +
                "loanit='" + loanit + '\'' +
                "lnrgst='" + lnrgst + '\'' +
                '}';
    }
}
