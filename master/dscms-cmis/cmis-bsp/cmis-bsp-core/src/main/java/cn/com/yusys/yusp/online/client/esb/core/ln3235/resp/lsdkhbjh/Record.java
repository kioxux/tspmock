package cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh;

import java.math.BigDecimal;

/**
 * 响应Service：还款方式转换
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String dzhhkzhl;//定制还款种类
    private String xzuetqhk;//需足额提前还款
    private String dzhkriqi;//定制还款日期
    private BigDecimal huanbjee;//还本金额
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String tqhkhxfs;//还息方式
    private String hkyujrgz;//还款遇假日规则
    private String sfyxkuxq;//是否有宽限期
    private Integer kuanxqts;//宽限期天数
    private String kxqjjrgz;//宽限期节假日规则

    public String getDzhhkzhl() {
        return dzhhkzhl;
    }

    public void setDzhhkzhl(String dzhhkzhl) {
        this.dzhhkzhl = dzhhkzhl;
    }

    public String getXzuetqhk() {
        return xzuetqhk;
    }

    public void setXzuetqhk(String xzuetqhk) {
        this.xzuetqhk = xzuetqhk;
    }

    public String getDzhkriqi() {
        return dzhkriqi;
    }

    public void setDzhkriqi(String dzhkriqi) {
        this.dzhkriqi = dzhkriqi;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getTqhkhxfs() {
        return tqhkhxfs;
    }

    public void setTqhkhxfs(String tqhkhxfs) {
        this.tqhkhxfs = tqhkhxfs;
    }

    public String getHkyujrgz() {
        return hkyujrgz;
    }

    public void setHkyujrgz(String hkyujrgz) {
        this.hkyujrgz = hkyujrgz;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dzhhkzhl='" + dzhhkzhl + '\'' +
                ", xzuetqhk='" + xzuetqhk + '\'' +
                ", dzhkriqi='" + dzhkriqi + '\'' +
                ", huanbjee=" + huanbjee +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", tqhkhxfs='" + tqhkhxfs + '\'' +
                ", hkyujrgz='" + hkyujrgz + '\'' +
                ", sfyxkuxq='" + sfyxkuxq + '\'' +
                ", kuanxqts=" + kuanxqts +
                ", kxqjjrgz='" + kxqjjrgz + '\'' +
                '}';
    }
}
