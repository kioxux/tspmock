package cn.com.yusys.yusp.web.server.biz.xddb0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0012.req.Xddb0012ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0012.resp.Xddb0012RespDto;
import cn.com.yusys.yusp.dto.server.xddb0012.req.Xddb0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.Xddb0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵押登记获取押品信息
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDDB0012:抵押登记获取押品信息")
@RestController
// TODO /api/tradesystem 待确认
@RequestMapping("/api/dscms")
public class Xddb0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0012Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0012
     * 交易描述：抵押登记获取押品信息
     *
     * @param xddb0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记获取押品信息")
    @PostMapping("/xddb0012")
    //@Idempotent({"xddb0012", "#xddb0012ReqDto.datasq"})
    protected @ResponseBody
    Xddb0012RespDto xddb0012(@Validated @RequestBody Xddb0012ReqDto xddb0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012ReqDto));
        Xddb0012DataReqDto xddb0012DataReqDto = new Xddb0012DataReqDto();// 请求Data： 抵押登记获取押品信息
        Xddb0012DataRespDto xddb0012DataRespDto = new Xddb0012DataRespDto();// 响应Data：抵押登记获取押品信息
        Xddb0012RespDto xddb0012RespDto = new Xddb0012RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0012.req.Data reqData = null; // 请求Data：抵押登记获取押品信息
        cn.com.yusys.yusp.dto.server.biz.xddb0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0012.resp.Data();// 响应Data：抵押登记获取押品信息

        try {
            // 从 xddb0012ReqDto获取 reqData
            reqData = xddb0012ReqDto.getData();
            // 将 reqData 拷贝到xddb0012DataReqDto
            BeanUtils.copyProperties(reqData, xddb0012DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataReqDto));
            ResultDto<Xddb0012DataRespDto> xddb0012DataResultDto = dscmsBizDbClientService.xddb0012(xddb0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0012RespDto.setErorcd(Optional.ofNullable(xddb0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0012RespDto.setErortx(Optional.ofNullable(xddb0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0012DataResultDto.getCode())) {
                xddb0012DataRespDto = xddb0012DataResultDto.getData();
                xddb0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, e.getMessage());
            xddb0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0012RespDto.setDatasq(servsq);

        xddb0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012RespDto));
        return xddb0012RespDto;
    }
}
