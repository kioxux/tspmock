package cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp;

/**
 * 响应Service：查询日初（合约）信息历史表（利翃）的合约状态和结清日期
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String status;//合约状态
    private String clear_date;//结清日期

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClear_date() {
        return clear_date;
    }

    public void setClear_date(String clear_date) {
        this.clear_date = clear_date;
    }

    @Override
    public String toString() {
        return "Record{" +
                "status='" + status + '\'' +
                ", clear_date='" + clear_date + '\'' +
                '}';
    }
}
