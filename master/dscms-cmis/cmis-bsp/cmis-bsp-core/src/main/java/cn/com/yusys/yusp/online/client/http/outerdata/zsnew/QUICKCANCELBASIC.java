package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	简易注销-基本信息
@JsonPropertyOrder(alphabetic = true)
public class QUICKCANCELBASIC implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CANCELRESULT")
    private String CANCELRESULT;//	简易注销结果
    @JsonProperty(value = "CANCELRESULTDATE")
    private String CANCELRESULTDATE;//	核准日期
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;//	统一社会信用代码
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//	企业名称
    @JsonProperty(value = "NOTICE_FROMDATE")
    private String NOTICE_FROMDATE;//	公告开始日期
    @JsonProperty(value = "NOTICE_TODATE")
    private String NOTICE_TODATE;//	公告截止日期
    @JsonProperty(value = "REGNO")
    private String REGNO;//	注册号
    @JsonProperty(value = "REGORG")
    private String REGORG;//	登记机关
    @JsonProperty(value = "REGORGCODE")
    private String REGORGCODE;//	登记机关代码

    @JsonIgnore
    public String getCANCELRESULT() {
        return CANCELRESULT;
    }

    @JsonIgnore
    public void setCANCELRESULT(String CANCELRESULT) {
        this.CANCELRESULT = CANCELRESULT;
    }

    @JsonIgnore
    public String getCANCELRESULTDATE() {
        return CANCELRESULTDATE;
    }

    @JsonIgnore
    public void setCANCELRESULTDATE(String CANCELRESULTDATE) {
        this.CANCELRESULTDATE = CANCELRESULTDATE;
    }

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getNOTICE_FROMDATE() {
        return NOTICE_FROMDATE;
    }

    @JsonIgnore
    public void setNOTICE_FROMDATE(String NOTICE_FROMDATE) {
        this.NOTICE_FROMDATE = NOTICE_FROMDATE;
    }

    @JsonIgnore
    public String getNOTICE_TODATE() {
        return NOTICE_TODATE;
    }

    @JsonIgnore
    public void setNOTICE_TODATE(String NOTICE_TODATE) {
        this.NOTICE_TODATE = NOTICE_TODATE;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getREGORG() {
        return REGORG;
    }

    @JsonIgnore
    public void setREGORG(String REGORG) {
        this.REGORG = REGORG;
    }

    @JsonIgnore
    public String getREGORGCODE() {
        return REGORGCODE;
    }

    @JsonIgnore
    public void setREGORGCODE(String REGORGCODE) {
        this.REGORGCODE = REGORGCODE;
    }

    @Override
    public String toString() {
        return "QUICKCANCELBASIC{" +
                "CANCELRESULT='" + CANCELRESULT + '\'' +
                ", CANCELRESULTDATE='" + CANCELRESULTDATE + '\'' +
                ", CREDITCODE='" + CREDITCODE + '\'' +
                ", ENTNAME='" + ENTNAME + '\'' +
                ", NOTICE_FROMDATE='" + NOTICE_FROMDATE + '\'' +
                ", NOTICE_TODATE='" + NOTICE_TODATE + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", REGORG='" + REGORG + '\'' +
                ", REGORGCODE='" + REGORGCODE + '\'' +
                '}';
    }
}
