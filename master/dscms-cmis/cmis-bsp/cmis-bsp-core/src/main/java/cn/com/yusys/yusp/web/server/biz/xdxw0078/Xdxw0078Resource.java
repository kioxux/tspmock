package cn.com.yusys.yusp.web.server.biz.xdxw0078;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0078.req.Xdxw0078ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0078.resp.Xdxw0078RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0078.req.Xdxw0078DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0078.resp.Xdxw0078DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:勘验任务查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0078:勘验任务查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0078Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0078Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0078
     * 交易描述：推送云评估信息
     *
     * @param xdxw0078ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送云评估信息")
    @PostMapping("/xdxw0078")
    @Idempotent({"xdxw0078", "#xdxw0078ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0078RespDto xdxw0078(@Validated @RequestBody Xdxw0078ReqDto xdxw0078ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078ReqDto));
        Xdxw0078DataReqDto xdxw0078DataReqDto = new Xdxw0078DataReqDto();// 请求Data： 推送云评估信息
        Xdxw0078DataRespDto xdxw0078DataRespDto = new Xdxw0078DataRespDto();// 响应Data：推送云评估信息
		Xdxw0078RespDto xdxw0078RespDto = new Xdxw0078RespDto();
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdxw0078.req.Data reqData = null; // 请求Data：推送云评估信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0078.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0078.resp.Data();// 响应Data：推送云评估信息
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0078ReqDto获取 reqData
            reqData = xdxw0078ReqDto.getData();
            // 将 reqData 拷贝到xdxw0078DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0078DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataReqDto));
            ResultDto<Xdxw0078DataRespDto> xdxw0078DataResultDto = dscmsBizXwClientService.xdxw0078(xdxw0078DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0078RespDto.setErorcd(Optional.ofNullable(xdxw0078DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0078RespDto.setErortx(Optional.ofNullable(xdxw0078DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0078DataResultDto.getCode())) {
                xdxw0078DataRespDto = xdxw0078DataResultDto.getData();
                xdxw0078RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0078RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0078DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0078DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, e.getMessage());
            xdxw0078RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0078RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0078RespDto.setDatasq(servsq);

        xdxw0078RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0078.key, DscmsEnum.TRADE_CODE_XDXW0078.value, JSON.toJSONString(xdxw0078RespDto));
        return xdxw0078RespDto;
    }
}
