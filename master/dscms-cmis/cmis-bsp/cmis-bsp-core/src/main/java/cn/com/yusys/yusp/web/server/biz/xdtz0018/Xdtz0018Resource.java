package cn.com.yusys.yusp.web.server.biz.xdtz0018;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0018.req.Xdtz0018ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0018.resp.Xdtz0018RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0018.req.Xdtz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0018.resp.Xdtz0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:额度占用释放接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0018:额度占用释放接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0018Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0018
     * 交易描述：额度占用释放接口
     *
     * @param xdtz0018ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("额度占用释放接口")
    @PostMapping("/xdtz0018")
    //@Idempotent({"xdcatz0018", "#xdtz0018ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0018RespDto xdtz0018(@Validated @RequestBody Xdtz0018ReqDto xdtz0018ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018ReqDto));
        Xdtz0018DataReqDto xdtz0018DataReqDto = new Xdtz0018DataReqDto();// 请求Data： 额度占用释放接口
        Xdtz0018DataRespDto xdtz0018DataRespDto = new Xdtz0018DataRespDto();// 响应Data：额度占用释放接口
        Xdtz0018RespDto xdtz0018RespDto = new Xdtz0018RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0018.req.Data reqData = null; // 请求Data：额度占用释放接口
        cn.com.yusys.yusp.dto.server.biz.xdtz0018.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0018.resp.Data();// 响应Data：额度占用释放接口
        try {
            // 从 xdtz0018ReqDto获取 reqData
            reqData = xdtz0018ReqDto.getData();
            // 将 reqData 拷贝到xdtz0018DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0018DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataReqDto));
            ResultDto<Xdtz0018DataRespDto> xdtz0018DataResultDto = dscmsBizTzClientService.xdtz0018(xdtz0018DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0018RespDto.setErorcd(Optional.ofNullable(xdtz0018DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0018RespDto.setErortx(Optional.ofNullable(xdtz0018DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0018DataResultDto.getCode())) {
                xdtz0018DataRespDto = xdtz0018DataResultDto.getData();
                xdtz0018RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0018RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0018DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0018DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, e.getMessage());
            xdtz0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0018RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0018RespDto.setDatasq(servsq);

        xdtz0018RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0018.key, DscmsEnum.TRADE_CODE_XDTZ0018.value, JSON.toJSONString(xdtz0018RespDto));
        return xdtz0018RespDto;
    }
}
