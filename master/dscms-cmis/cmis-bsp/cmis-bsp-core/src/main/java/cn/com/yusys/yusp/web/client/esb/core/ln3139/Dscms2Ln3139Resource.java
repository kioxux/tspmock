package cn.com.yusys.yusp.web.client.esb.core.ln3139;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3139.req.Ln3139ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3139.resp.Ln3139RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3139.req.Ln3139ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3139.resp.Ln3139RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3139)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3139Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3139Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3139
     * 交易描述：通过贷款借据号和贷款账号的交易日期试算贷款欠息明细。
     *
     * @param ln3139ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3139:通过贷款借据号和贷款账号的交易日期试算贷款欠息明细。")
    @PostMapping("/ln3139")
    protected @ResponseBody
    ResultDto<Ln3139RespDto> ln3139(@Validated @RequestBody Ln3139ReqDto ln3139ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3139.key, EsbEnum.TRADE_CODE_LN3139.value, JSON.toJSONString(ln3139ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3139.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3139.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3139.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3139.resp.Service();
        Ln3139ReqService ln3139ReqService = new Ln3139ReqService();
        Ln3139RespService ln3139RespService = new Ln3139RespService();
        Ln3139RespDto ln3139RespDto = new Ln3139RespDto();
        ResultDto<Ln3139RespDto> ln3139ResultDto = new ResultDto<Ln3139RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将ln3139ReqDto转换成reqService
            BeanUtils.copyProperties(ln3139ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3139.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3139ReqService.setService(reqService);
            // 将ln3139ReqService转换成ln3139ReqServiceMap
            Map ln3139ReqServiceMap = beanMapUtil.beanToMap(ln3139ReqService);
            context.put("tradeDataMap", ln3139ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3139.key, EsbEnum.TRADE_CODE_LN3139.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3139.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3139.key, EsbEnum.TRADE_CODE_LN3139.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3139RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3139RespService.class, Ln3139RespService.class);
            respService = ln3139RespService.getService();
            //  将respService转换成Ln3139RespDto
            BeanUtils.copyProperties(respService, ln3139RespDto);
            ln3139ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3139ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, ln3139RespDto);
                ln3139ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3139ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3139ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3139ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3139.key, EsbEnum.TRADE_CODE_LN3139.value, e.getMessage());
            ln3139ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3139ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3139ResultDto.setData(ln3139RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3139.key, EsbEnum.TRADE_CODE_LN3139.value, JSON.toJSONString(ln3139ResultDto));
        return ln3139ResultDto;
    }
}
