package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询银票出账保证金账号信息（新信贷调票据）
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erortx;//响应代码
    private String retcod;//响应信息
    private cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.List list;

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getRetcod() {
        return retcod;
    }

    public void setRetcod(String retcod) {
        this.retcod = retcod;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erortx='" + erortx + '\'' +
                ", retcod='" + retcod + '\'' +
                ", list=" + list +
                '}';
    }
}
