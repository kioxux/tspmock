package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 清算信息
 */
@JsonPropertyOrder(alphabetic = true)
public class LIQUIDATION implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "LIGPRINCIPAL")
    private String LIGPRINCIPAL;//	清算负责人
    @JsonProperty(value = "LIQMEN")
    private String LIQMEN;//	清算组成员

    @JsonIgnore
    public String getLIGPRINCIPAL() {
        return LIGPRINCIPAL;
    }

    @JsonIgnore
    public void setLIGPRINCIPAL(String LIGPRINCIPAL) {
        this.LIGPRINCIPAL = LIGPRINCIPAL;
    }

    @JsonIgnore
    public String getLIQMEN() {
        return LIQMEN;
    }

    @JsonIgnore
    public void setLIQMEN(String LIQMEN) {
        this.LIQMEN = LIQMEN;
    }

    @Override
    public String toString() {
        return "LIQUIDATION{" +
                "LIGPRINCIPAL='" + LIGPRINCIPAL + '\'' +
                ", LIQMEN='" + LIQMEN + '\'' +
                '}';
    }
}
