package cn.com.yusys.yusp.web.server.biz.xdtz0017;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0017.req.Xdtz0017ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0017.resp.Xdtz0017RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0017.req.Xdtz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0017.resp.Xdtz0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:票据更换（通知信贷更改票据暂用额度台账）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0017:票据更换（通知信贷更改票据暂用额度台账）")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0017Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0017
     * 交易描述：票据更换（通知信贷更改票据暂用额度台账）
     *
     * @param xdtz0017ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("票据更换（通知信贷更改票据暂用额度台账）")
    @PostMapping("/xdtz0017")
    //@Idempotent({"xdcatz0017", "#xdtz0017ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0017RespDto xdtz0017(@Validated @RequestBody Xdtz0017ReqDto xdtz0017ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017ReqDto));
        Xdtz0017DataReqDto xdtz0017DataReqDto = new Xdtz0017DataReqDto();// 请求Data： 票据更换（通知信贷更改票据暂用额度台账）
        Xdtz0017DataRespDto xdtz0017DataRespDto = new Xdtz0017DataRespDto();// 响应Data：票据更换（通知信贷更改票据暂用额度台账）
        Xdtz0017RespDto xdtz0017RespDto = new Xdtz0017RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0017.req.Data reqData = null; // 请求Data：票据更换（通知信贷更改票据暂用额度台账）
        cn.com.yusys.yusp.dto.server.biz.xdtz0017.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0017.resp.Data();// 响应Data：票据更换（通知信贷更改票据暂用额度台账）
        try {
            // 从 xdtz0017ReqDto获取 reqData
            reqData = xdtz0017ReqDto.getData();
            // 将 reqData 拷贝到xdtz0017DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0017DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017DataReqDto));
            ResultDto<Xdtz0017DataRespDto> xdtz0017DataResultDto = dscmsBizTzClientService.xdtz0017(xdtz0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0017RespDto.setErorcd(Optional.ofNullable(xdtz0017DataResultDto.getCode()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdtz0017RespDto.setErortx(Optional.ofNullable(xdtz0017DataResultDto.getMessage()).orElse(SuccessEnum.CMIS_SUCCSESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0017DataResultDto.getCode())) {
                xdtz0017DataRespDto = xdtz0017DataResultDto.getData();
                xdtz0017RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0017RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0017DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0017DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, e.getMessage());
            xdtz0017RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0017RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0017RespDto.setDatasq(servsq);

        xdtz0017RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0017.key, DscmsEnum.TRADE_CODE_XDTZ0017.value, JSON.toJSONString(xdtz0017RespDto));
        return xdtz0017RespDto;
    }
}
