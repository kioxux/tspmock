package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerfacilitylist;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:13
 * @since 2021/7/9 14:13
 */
public class Registerfacilitylist {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerfacilitylist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Registerfacilitylist{" +
                "record=" + record +
                '}';
    }
}
