package cn.com.yusys.yusp.web.client.esb.rircp.fbxw06;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.req.Fbxw06ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.resp.Fbxw06RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw06Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw06Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB利率定价测算提交接口（处理码fbxw06）
     *
     * @param fbxw06ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw06:ESB利率定价测算提交接口")
    @PostMapping("/fbxw06")
    protected @ResponseBody
    ResultDto<Fbxw06RespDto> fbxw06(@Validated @RequestBody Fbxw06ReqDto fbxw06ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value, JSON.toJSONString(fbxw06ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.resp.Service();
        Fbxw06ReqService fbxw06ReqService = new Fbxw06ReqService();
        Fbxw06RespService fbxw06RespService = new Fbxw06RespService();
        Fbxw06RespDto fbxw06RespDto = new Fbxw06RespDto();
        ResultDto<Fbxw06RespDto> fbxw06ResultDto = new ResultDto<Fbxw06RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw06ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw06ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW06.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw06ReqService.setService(reqService);
            // 将fbxw06ReqService转换成fbxw06ReqServiceMap
            Map fbxw06ReqServiceMap = beanMapUtil.beanToMap(fbxw06ReqService);
            context.put("tradeDataMap", fbxw06ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW06.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw06RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw06RespService.class, Fbxw06RespService.class);
            respService = fbxw06RespService.getService();

            //  将Fbxw06RespDto封装到ResultDto<Fbxw06RespDto>
            fbxw06ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw06ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw06RespDto
                BeanUtils.copyProperties(respService, fbxw06RespDto);
                fbxw06ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw06ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw06ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw06ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value, e.getMessage());
            fbxw06ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw06ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw06ResultDto.setData(fbxw06RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW06.key, EsbEnum.TRADE_CODE_FBXW06.value, JSON.toJSONString(fbxw06ResultDto));
        return fbxw06ResultDto;
    }
}
