package cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.req;

/**
 * 请求Service：新入职人员信息登记
 *
 * @author leehuang
 * @version 1.0
 */
public class XxdentReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "XxdentReqService{" +
                "service=" + service +
                '}';
    }
}
