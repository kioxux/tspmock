package cn.com.yusys.yusp.web.client.http.ocr;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.ocr.req.Cbocr3ReqDto;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.Cbocr3RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.outerdata.ocr.req.Cbocr3ReqService;
import cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.Cbocr3RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(cbocr3)")
@RestController
@RequestMapping("/api/dscms2outerdata")
public class Dscms2Cbocr3Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Cbocr3Resource.class);
    @Autowired
    private RestTemplate restTemplate;

    @Value("${application.ocr.url}")
    private String ocrSysUrl;

    /**
     * 获取cbocr3财报识别信息
     *
     * @param cbocr3ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cbocr3:获取cbocr3财报识别信息")
    @PostMapping("/cbocr3")
    protected @ResponseBody
    ResultDto<Cbocr3RespDto> cbocr3(@Validated @RequestBody Cbocr3ReqDto cbocr3ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR3.key, EsbEnum.TRADE_CODE_CBOCR3.value, JSON.toJSONString(cbocr3ReqDto));

        Cbocr3ReqService cbocr3ReqService = new Cbocr3ReqService();
        Cbocr3RespService cbocr3RespService = new Cbocr3RespService();
        Cbocr3RespDto cbocr3RespDto = new Cbocr3RespDto();
        ResultDto<Cbocr3RespDto> cbocr3ResultDto = new ResultDto<Cbocr3RespDto>();
        try {
            // 将Cbocr3ReqDto转换成Cbocr3ReqService
            BeanUtils.copyProperties(cbocr3ReqDto, cbocr3ReqService);
            cbocr3ReqService.setPrcscd(EsbEnum.TRADE_CODE_IDCHECK.key);//    交易码
            cbocr3ReqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            cbocr3ReqService.setServsq(servsq);//    渠道流水
            cbocr3ReqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            cbocr3ReqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(cbocr3ReqService), headers);
            //String url = "http://10.28.124.15:8192/financialReport/subTask/queryList";// TODO SIT环境的测试URL
            String url = ocrSysUrl + "/financialReport/subTask/queryList";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, JSON.toJSONString(cbocr3ReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, responseEntity);

            String responseStr = responseEntity.getBody();
            //cbocr3RespService = (Cbocr3RespService) JSON.parseObject(responseStr, Cbocr3RespService.class);
            cbocr3RespDto = (Cbocr3RespDto) JSON.parseObject(responseStr, Cbocr3RespDto.class);
            cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.Data cbocr3RespData = new cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.Data();
            //  将Cbocr3RespDto封装到ResultDto<Cbocr3RespDto>
            if (Objects.equals(SuccessEnum.OCR_SUCCESS_CODE.key, cbocr3RespDto.getCode())) {
                logger.info("********调用接口成功financialReport/subTask/queryList***************");
                cbocr3ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cbocr3ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            } else {
            	cbocr3ResultDto.setCode(EpbEnum.EPB099999.key);
                cbocr3ResultDto.setMessage(cbocr3RespDto.getMessage());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, e.getMessage());
            cbocr3ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cbocr3ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cbocr3ResultDto.setData(cbocr3RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR3.key, EsbEnum.TRADE_CODE_CBOCR3.value, JSON.toJSONString(cbocr3ResultDto));
        return cbocr3ResultDto;
    }
}
