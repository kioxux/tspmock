package cn.com.yusys.yusp.web.server.biz.xdxw0030;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0030.req.Xdxw0030ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0030.resp.Xdxw0030RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0030.req.Xdxw0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0030.resp.Xdxw0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据云估计流水号查询房屋信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0030:根据云估计流水号查询房屋信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0030Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0030
     * 交易描述：根据云估计流水号查询房屋信息
     *
     * @param xdxw0030ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据云估计流水号查询房屋信息")
    @PostMapping("/xdxw0030")
    //@Idempotent({"xdcaxw0030", "#xdxw0030ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0030RespDto xdxw0030(@Validated @RequestBody Xdxw0030ReqDto xdxw0030ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030ReqDto));
        Xdxw0030DataReqDto xdxw0030DataReqDto = new Xdxw0030DataReqDto();// 请求Data： 根据云估计流水号查询房屋信息
        Xdxw0030DataRespDto xdxw0030DataRespDto = new Xdxw0030DataRespDto();// 响应Data：根据云估计流水号查询房屋信息
        Xdxw0030RespDto xdxw0030RespDto = new Xdxw0030RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0030.req.Data reqData = null; // 请求Data：根据云估计流水号查询房屋信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0030.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0030.resp.Data();// 响应Data：根据云估计流水号查询房屋信息
        try {
            // 从 xdxw0030ReqDto获取 reqData
            reqData = xdxw0030ReqDto.getData();
            // 将 reqData 拷贝到xdxw0030DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0030DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataReqDto));
            ResultDto<Xdxw0030DataRespDto> xdxw0030DataResultDto = dscmsBizXwClientService.xdxw0030(xdxw0030DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0030RespDto.setErorcd(Optional.ofNullable(xdxw0030DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0030RespDto.setErortx(Optional.ofNullable(xdxw0030DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0030DataResultDto.getCode())) {
                xdxw0030DataRespDto = xdxw0030DataResultDto.getData();
                xdxw0030RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0030RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0030DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0030DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, e.getMessage());
            xdxw0030RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0030RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0030RespDto.setDatasq(servsq);

        xdxw0030RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0030.key, DscmsEnum.TRADE_CODE_XDXW0030.value, JSON.toJSONString(xdxw0030RespDto));
        return xdxw0030RespDto;
    }
}
