package cn.com.yusys.yusp.online.client.esb.core.ln3079.req;

/**
 * 请求Service：贷款产品变更
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3079ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3079ReqService{" +
				"service=" + service +
				'}';
	}
}
