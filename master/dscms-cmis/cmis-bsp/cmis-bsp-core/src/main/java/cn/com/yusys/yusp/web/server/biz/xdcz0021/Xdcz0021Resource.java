package cn.com.yusys.yusp.web.server.biz.xdcz0021;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0021.req.Xdcz0021ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0021.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0021.resp.Xdcz0021RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0021.req.Xdcz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0021.resp.Xdcz0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:支用申请
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0021:支用申请")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0021Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0021
     * 交易描述：支用申请
     *
     * @param xdcz0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("支用申请")
    @PostMapping("/xdcz0021")
   //@Idempotent({"xdcz0021", "#xdcz0021ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0021RespDto xdcz0021(@Validated @RequestBody Xdcz0021ReqDto xdcz0021ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, JSON.toJSONString(xdcz0021ReqDto));
        Xdcz0021DataReqDto xdcz0021DataReqDto = new Xdcz0021DataReqDto();// 请求Data： 电子保函开立
        Xdcz0021DataRespDto xdcz0021DataRespDto = new Xdcz0021DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0021.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0021RespDto xdcz0021RespDto = new Xdcz0021RespDto();
        try {
            // 从 xdcz0021ReqDto获取 reqData
            reqData = xdcz0021ReqDto.getData();
            // 将 reqData 拷贝到xdcz0021DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0021DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, JSON.toJSONString(xdcz0021DataReqDto));
            ResultDto<Xdcz0021DataRespDto> xdcz0021DataResultDto = dscmsBizCzClientService.xdcz0021(xdcz0021DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, JSON.toJSONString(xdcz0021DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0021RespDto.setErorcd(Optional.ofNullable(xdcz0021DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0021RespDto.setErortx(Optional.ofNullable(xdcz0021DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0021DataResultDto.getCode())) {
                xdcz0021DataRespDto = xdcz0021DataResultDto.getData();
                xdcz0021RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0021RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0021DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, e.getMessage());
            xdcz0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0021RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0021RespDto.setDatasq(servsq);

        xdcz0021RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0021.key, DscmsEnum.TRADE_CODE_XDCZ0021.value, JSON.toJSONString(xdcz0021RespDto));
        return xdcz0021RespDto;
    }
}
