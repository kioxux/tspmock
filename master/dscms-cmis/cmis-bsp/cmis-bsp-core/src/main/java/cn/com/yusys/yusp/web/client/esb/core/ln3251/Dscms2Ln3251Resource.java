package cn.com.yusys.yusp.web.client.esb.core.ln3251;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.req.Ln3251ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.resp.Ln3251RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.resp.Lstkhzmx;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3251.req.Ln3251ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3251.resp.List;
import cn.com.yusys.yusp.online.client.esb.core.ln3251.resp.Ln3251RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3251)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3251Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3251Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();


    /**
     * @param reqDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<Ln3251RespDto>
     * @author 王玉坤
     * @date 2021/8/27 22:21
     * @version 1.0.0
     * @desc 贷款自动追缴明细查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @ApiOperation("ln3251:贷款自动追缴明细查询")
    @PostMapping("/ln3251")
    protected @ResponseBody
    ResultDto<Ln3251RespDto> ln3251(@Validated @RequestBody Ln3251ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3251.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3251.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3251.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3251.resp.Service();
        Ln3251ReqService ln3251ReqService = new Ln3251ReqService();
        Ln3251RespService ln3251RespService = new Ln3251RespService();
        Ln3251RespDto ln3251RespDto = new Ln3251RespDto();
        ResultDto<Ln3251RespDto> ln3251ResultDto = new ResultDto<Ln3251RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {

            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3251.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3251ReqService.setService(reqService);
            // 将ln3251ReqService转换成ln3251ReqServiceMap
            Map ln3251ReqServiceMap = beanMapUtil.beanToMap(ln3251ReqService);
            context.put("tradeDataMap", ln3251ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3251.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3251RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3251RespService.class, Ln3251RespService.class);
            respService = ln3251RespService.getService();

            //  将Ln3251RespDto封装到ResultDto<Ln3251RespDto>
            ln3251ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3251ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3251RespDto
                BeanUtils.copyProperties(respService, ln3251RespDto);
                List list = Optional.ofNullable(respService.getList()).orElse(new List());
                if (CollectionUtils.nonEmpty(list.getRecord())) {
                    java.util.List<Lstkhzmx> targetList = list.getRecord().stream().map(record -> {
                        Lstkhzmx target = new Lstkhzmx();
                        BeanUtils.copyProperties(record, target);
                        return target;
                    }).collect(Collectors.toList());
                    ln3251RespDto.setLstkhzmx_ARRAY(targetList);
                }

                ln3251ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3251ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3251ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3251ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value, e.getMessage());
            ln3251ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3251ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3251ResultDto.setData(ln3251RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3251.key, EsbEnum.TRADE_CODE_LN3251.value, JSON.toJSONString(ln3251ResultDto));

        return ln3251ResultDto;
    }
}
