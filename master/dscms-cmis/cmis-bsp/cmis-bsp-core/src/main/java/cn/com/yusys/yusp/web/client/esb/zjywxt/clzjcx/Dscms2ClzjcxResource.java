package cn.com.yusys.yusp.web.client.esb.zjywxt.clzjcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.req.ClzjcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.resp.ClzjcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.req.ClzjcxReqService;
import cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.resp.ClzjcxRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2zjywxt")
public class Dscms2ClzjcxResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2ClzjcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：clzjcx
     * 交易描述：
     *
     * @param clzjcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/clzjcx")
    protected @ResponseBody
    ResultDto<ClzjcxRespDto> clzjcx(@Validated @RequestBody ClzjcxReqDto clzjcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLZJCX.key, EsbEnum.TRADE_CODE_CLZJCX.value, JSON.toJSONString(clzjcxReqDto));
        cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.resp.Service();
        ClzjcxReqService clzjcxReqService = new ClzjcxReqService();
        ClzjcxRespService clzjcxRespService = new ClzjcxRespService();
        ClzjcxRespDto clzjcxRespDto = new ClzjcxRespDto();
        ResultDto<ClzjcxRespDto> clzjcxResultDto = new ResultDto<ClzjcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将clzjcxReqDto转换成reqService
            BeanUtils.copyProperties(clzjcxReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CLZJCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_GAP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_GAP.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            clzjcxReqService.setService(reqService);
            // 将clzjcxReqService转换成clzjcxReqServiceMap
            Map clzjcxReqServiceMap = beanMapUtil.beanToMap(clzjcxReqService);
            context.put("tradeDataMap", clzjcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLZJCX.key, EsbEnum.TRADE_CODE_CLZJCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CLZJCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLZJCX.key, EsbEnum.TRADE_CODE_CLZJCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            clzjcxRespService = beanMapUtil.mapToBean(tradeDataMap, ClzjcxRespService.class, ClzjcxRespService.class);
            respService = clzjcxRespService.getService();

            clzjcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            clzjcxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成ClzjcxRespDto
                BeanUtils.copyProperties(respService, clzjcxRespDto);
                clzjcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                clzjcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                clzjcxResultDto.setCode(EpbEnum.EPB099999.key);
                clzjcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLZJCX.key, EsbEnum.TRADE_CODE_CLZJCX.value, e.getMessage());
            clzjcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            clzjcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        clzjcxResultDto.setData(clzjcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLZJCX.key, EsbEnum.TRADE_CODE_CLZJCX.value, JSON.toJSONString(clzjcxResultDto));
        return clzjcxResultDto;
    }
}
