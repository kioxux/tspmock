package cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.req;

/**
 * 请求Service：贷记入账状态查询申请往帐
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String hvflag;//大小额交易标志
    private String functp;//业务类型
    private String subprc;//前台交易码
    private String oribusinum;//原业务受理编号
    private String businum;//业务受理编号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getHvflag() {
        return hvflag;
    }

    public void setHvflag(String hvflag) {
        this.hvflag = hvflag;
    }

    public String getFunctp() {
        return functp;
    }

    public void setFunctp(String functp) {
        this.functp = functp;
    }

    public String getSubprc() {
        return subprc;
    }

    public void setSubprc(String subprc) {
        this.subprc = subprc;
    }

    public String getOribusinum() {
        return oribusinum;
    }

    public void setOribusinum(String oribusinum) {
        this.oribusinum = oribusinum;
    }

    public String getBusinum() {
        return businum;
    }

    public void setBusinum(String businum) {
        this.businum = businum;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", hvflag='" + hvflag + '\'' +
                ", functp='" + functp + '\'' +
                ", subprc='" + subprc + '\'' +
                ", oribusinum='" + oribusinum + '\'' +
                ", businum='" + businum + '\'' +
                '}';
    }
}
