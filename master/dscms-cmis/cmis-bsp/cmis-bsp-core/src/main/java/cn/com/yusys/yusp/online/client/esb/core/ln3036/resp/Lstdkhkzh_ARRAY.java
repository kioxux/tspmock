package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkzh.Record;

import java.util.List;

public class Lstdkhkzh_ARRAY {
    private static final long serialVersionUID = 1L;
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkzh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhkzh_ARRAY{" +
                "record=" + record +
                '}';
    }
}
