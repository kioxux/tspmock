package cn.com.yusys.yusp.online.client.esb.circp.fb1168.req;

/**
 * 请求Service：抵押查封结果推送
 *
 * @author code-generator
 * @version 1.0
 */
public class Fb1168ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
