package cn.com.yusys.yusp.online.client.esb.circp.fb1166.resp;

/**
 * 响应Service：面签邀约结果推送
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1166RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1166RespService{" +
                "service=" + service +
                '}';
    }
}
