package cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.resp;

/**
 * 响应Service：押品状态变更
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:52
 */
public class Service {

    private String erorcd;// 响应码
    private String erortx;// 响应信息
    private String guarno;// 押品编号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    @Override
    public String toString() {
        return "YpztbgRespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", guarno='" + guarno + '\'' +
                '}';
    }
}
