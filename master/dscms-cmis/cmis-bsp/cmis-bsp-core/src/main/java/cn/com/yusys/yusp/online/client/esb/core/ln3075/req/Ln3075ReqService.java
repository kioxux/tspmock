package cn.com.yusys.yusp.online.client.esb.core.ln3075.req;

/**
 * 请求Service：逾期贷款展期
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3075ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
