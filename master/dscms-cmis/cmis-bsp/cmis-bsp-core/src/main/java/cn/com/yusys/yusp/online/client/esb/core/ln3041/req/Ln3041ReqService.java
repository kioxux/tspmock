package cn.com.yusys.yusp.online.client.esb.core.ln3041.req;

/**
 * 请求Service：支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还；
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3041ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}


