package cn.com.yusys.yusp.online.client.esb.core.ln3160.req;

/**
 * 请求Service：资产证券化信息查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3160ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3160ReqService{" +
                "service=" + service +
                '}';
    }
}
