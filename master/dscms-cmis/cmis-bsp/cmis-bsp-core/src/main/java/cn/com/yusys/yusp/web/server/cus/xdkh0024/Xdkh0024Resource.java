package cn.com.yusys.yusp.web.server.cus.xdkh0024;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0024.req.Xdkh0024ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0024.resp.Xdkh0024RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0024.req.Xdkh0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0024.resp.Xdkh0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷、优农贷客户基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0024:优企贷、优农贷客户基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0024Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0024
     * 交易描述：优企贷、优农贷客户基本信息查询
     *
     * @param xdkh0024ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷客户基本信息查询")
    @PostMapping("/xdkh0024")
    //@Idempotent({"xdcakh0024", "#xdkh0024ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0024RespDto xdkh0024(@Validated @RequestBody Xdkh0024ReqDto xdkh0024ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024ReqDto));
        Xdkh0024DataReqDto xdkh0024DataReqDto = new Xdkh0024DataReqDto();// 请求Data： 优企贷、优农贷客户基本信息查询
        Xdkh0024DataRespDto xdkh0024DataRespDto = new Xdkh0024DataRespDto();// 响应Data：优企贷、优农贷客户基本信息查询
        Xdkh0024RespDto xdkh0024RespDto = new Xdkh0024RespDto();

        cn.com.yusys.yusp.dto.server.cus.xdkh0024.req.Data reqData = null; // 请求Data：优企贷、优农贷客户基本信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0024.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0024.resp.Data();// 响应Data：优企贷、优农贷客户基本信息查询

        try {
            // 从 xdkh0024ReqDto获取 reqData
            reqData = xdkh0024ReqDto.getData();
            // 将 reqData 拷贝到xdkh0024DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0024DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataReqDto));
            ResultDto<Xdkh0024DataRespDto> xdkh0024DataResultDto = dscmsCusClientService.xdkh0024(xdkh0024DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0024RespDto.setErorcd(Optional.ofNullable(xdkh0024DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0024RespDto.setErortx(Optional.ofNullable(xdkh0024DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0024DataResultDto.getCode())) {
                xdkh0024DataRespDto = xdkh0024DataResultDto.getData();
                xdkh0024RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0024RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0024DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0024DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, e.getMessage());
            xdkh0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0024RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0024RespDto.setDatasq(servsq);

        xdkh0024RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0024.key, DscmsEnum.TRADE_CODE_XDKH0024.value, JSON.toJSONString(xdkh0024RespDto));
        return xdkh0024RespDto;
    }
}