package cn.com.yusys.yusp.web.client.esb.core.ln3030;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdkhkfs;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdkhkzh;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdkllfd;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdksfsj;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdkstzf;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdktxzh;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdkwtxx;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdkzhbz;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Lstdkzhlm;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3030.req.*;
import cn.com.yusys.yusp.online.client.esb.core.ln3030.resp.Ln3030RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:贷款资料维护
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3030)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3030Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Ln3030Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3030
     * 交易描述：贷款资料维护
     *
     * @param ln3030ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3030:贷款资料维护")
    @PostMapping("/ln3030")
    protected @ResponseBody
    ResultDto<Ln3030RespDto> ln3030(@Validated @RequestBody Ln3030ReqDto ln3030ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3030.key, EsbEnum.TRADE_CODE_LN3030.value, JSON.toJSONString(ln3030ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3030.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3030.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3030.resp.Service();
        Ln3030ReqService ln3030ReqService = new Ln3030ReqService();
        Ln3030RespService ln3030RespService = new Ln3030RespService();
        Ln3030RespDto ln3030RespDto = new Ln3030RespDto();
        ResultDto<Ln3030RespDto> ln3030ResultDto = new ResultDto<Ln3030RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3030ReqDto转换成reqService
            BeanUtils.copyProperties(ln3030ReqDto, reqService);
            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkbjfd())) {
                List<Lstdkbjfd> lstdkbjfdList = Optional.ofNullable(ln3030ReqDto.getLstdkbjfd()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkbjfd.Record> dkbjfdRecords = lstdkbjfdList.parallelStream().map(dkbjfd -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkbjfd.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkbjfd.Record();
                    BeanUtils.copyProperties(dkbjfd, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkbjfd_ARRAY lstdkbjfd = new Lstdkbjfd_ARRAY();
                lstdkbjfd.setRecord(dkbjfdRecords);
                reqService.setLstdkbjfd_ARRAY(lstdkbjfd);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkfkjh())) {
                List<Lstdkfkjh> lstdkfkjhList = Optional.ofNullable(ln3030ReqDto.getLstdkfkjh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkfkjh.Record> dkfkjhRecords = lstdkfkjhList.parallelStream().map(dkfkjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkfkjh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkfkjh.Record();
                    BeanUtils.copyProperties(dkfkjh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkfkjh_ARRAY lstdkfkjh = new Lstdkfkjh_ARRAY();
                lstdkfkjh.setRecord(dkfkjhRecords);
                reqService.setLstdkfkjh_ARRAY(lstdkfkjh);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkfwdj())) {
                List<Lstdkfwdj> lstdkfwdjList = Optional.ofNullable(ln3030ReqDto.getLstdkfwdj()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkfwdj.Record> dkfwdjRecords = lstdkfwdjList.parallelStream().map(dkfwdj -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkfwdj.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkfwdj.Record();
                    BeanUtils.copyProperties(dkfwdj, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkfwdj_ARRAY lstdkfwdj = new Lstdkfwdj_ARRAY();
                lstdkfwdj.setRecord(dkfwdjRecords);
                reqService.setLstdkfwdj_ARRAY(lstdkfwdj);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkhbjh())) {
                List<Lstdkhbjh> lstdkhbjhList = Optional.ofNullable(ln3030ReqDto.getLstdkhbjh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhbjh.Record> dkhbjhRecords = lstdkhbjhList.parallelStream().map(dkhbjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhbjh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhbjh.Record();
                    BeanUtils.copyProperties(dkhbjh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhbjh_ARRAY lstdkhbjh = new Lstdkhbjh_ARRAY();
                lstdkhbjh.setRecord(dkhbjhRecords);
                reqService.setLstdkhbjh_ARRAY(lstdkhbjh);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkhkfs())) {
                List<Lstdkhkfs> lstdkhkfsList = Optional.ofNullable(ln3030ReqDto.getLstdkhkfs()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhkfs.Record> dkhkfsRecords = lstdkhkfsList.parallelStream().map(dkhkfs -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhkfs.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhkfs.Record();
                    BeanUtils.copyProperties(dkhkfs, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhkfs_ARRAY lstdkhkfs = new Lstdkhkfs_ARRAY();
                lstdkhkfs.setRecord(dkhkfsRecords);
                reqService.setLstdkhkfs_ARRAY(lstdkhkfs);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkhkzh())) {
                List<Lstdkhkzh> lstdkhkzhList = Optional.ofNullable(ln3030ReqDto.getLstdkhkzh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhkzh.Record> dkhkzhRecords = lstdkhkzhList.parallelStream().map(dkhkzh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhkzh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhkzh.Record();
                    BeanUtils.copyProperties(dkhkzh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhkzh_ARRAY lstdkhkzh = new Lstdkhkzh_ARRAY();
                lstdkhkzh.setRecord(dkhkzhRecords);
                reqService.setLstdkhkzh_ARRAY(lstdkhkzh);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkllfd())) {
                List<Lstdkllfd> lstdkllfdList = Optional.ofNullable(ln3030ReqDto.getLstdkllfd()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkllfd.Record> dkllfdRecords = lstdkllfdList.parallelStream().map(dkllfd -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkllfd.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkllfd.Record();
                    BeanUtils.copyProperties(dkllfd, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkllfd_ARRAY lstdkllfd = new Lstdkllfd_ARRAY();
                lstdkllfd.setRecord(dkllfdRecords);
                reqService.setLstdkllfd_ARRAY(lstdkllfd);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdksfsj())) {
                List<Lstdksfsj> lstdksfsjList = Optional.ofNullable(ln3030ReqDto.getLstdksfsj()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdksfsj.Record> dksfsjRecords = lstdksfsjList.parallelStream().map(dksfsj -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdksfsj.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdksfsj.Record();
                    BeanUtils.copyProperties(dksfsj, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdksfsj_ARRAY lstdksfsj = new Lstdksfsj_ARRAY();
                lstdksfsj.setRecord(dksfsjRecords);
                reqService.setLstdksfsj_ARRAY(lstdksfsj);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkstzf())) {
                List<Lstdkstzf> lstdkstzfList = Optional.ofNullable(ln3030ReqDto.getLstdkstzf()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkstzf.Record> dkstzfRecords = lstdkstzfList.parallelStream().map(dkstzf -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkstzf.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkstzf.Record();
                    BeanUtils.copyProperties(dkstzf, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkstzf_ARRAY lstdkstzf = new Lstdkstzf_ARRAY();
                lstdkstzf.setRecord(dkstzfRecords);
                reqService.setLstdkstzf_ARRAY(lstdkstzf);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdktxzh())) {
                List<Lstdktxzh> lstdktxzhList = Optional.ofNullable(ln3030ReqDto.getLstdktxzh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdktxzh.Record> dktxzhRecords = lstdktxzhList.parallelStream().map(dktxzh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdktxzh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdktxzh.Record();
                    BeanUtils.copyProperties(dktxzh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdktxzh_ARRAY lstdktxzh = new Lstdktxzh_ARRAY();
                lstdktxzh.setRecord(dktxzhRecords);
                reqService.setLstdktxzh_ARRAY(lstdktxzh);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkwtxx())) {
                List<Lstdkwtxx> lstdkwtxxList = Optional.ofNullable(ln3030ReqDto.getLstdkwtxx()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkwtxx.Record> dkwtxxRecords = lstdkwtxxList.parallelStream().map(dkwtxx -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkwtxx.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkwtxx.Record();
                    BeanUtils.copyProperties(dkwtxx, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkwtxx_ARRAY lstdkwtxx = new Lstdkwtxx_ARRAY();
                lstdkwtxx.setRecord(dkwtxxRecords);
                reqService.setLstdkwtxx_ARRAY(lstdkwtxx);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkzhbz())) {
                List<Lstdkzhbz> lstdkzhbzList = Optional.ofNullable(ln3030ReqDto.getLstdkzhbz()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkzhbz.Record> dkzhbzRecords = lstdkzhbzList.parallelStream().map(dkzhbz -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkzhbz.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkzhbz.Record();
                    BeanUtils.copyProperties(dkzhbz, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkzhbz_ARRAY lstdkzhbz = new Lstdkzhbz_ARRAY();
                lstdkzhbz.setRecord(dkzhbzRecords);
                reqService.setLstdkzhbz_ARRAY(lstdkzhbz);
            }

            if (CollectionUtils.nonEmpty(ln3030ReqDto.getLstdkzhlm())) {
                List<Lstdkzhlm> lstdkzhlmList = Optional.ofNullable(ln3030ReqDto.getLstdkzhlm()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkzhlm.Record> dkzhlmRecords = lstdkzhlmList.parallelStream().map(dkzhlm -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkzhlm.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkzhlm.Record();
                    BeanUtils.copyProperties(dkzhlm, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkzhlm_ARRAY lstdkzhlm = new Lstdkzhlm_ARRAY();
                lstdkzhlm.setRecord(dkzhlmRecords);
                reqService.setLstdkzhlm_ARRAY(lstdkzhlm);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3030.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3030ReqService.setService(reqService);
            // 将ln3030ReqService转换成ln3030ReqServiceMap
            Map ln3030ReqServiceMap = beanMapUtil.beanToMap(ln3030ReqService);
            context.put("tradeDataMap", ln3030ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3030.key, EsbEnum.TRADE_CODE_LN3030.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3030.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3030.key, EsbEnum.TRADE_CODE_LN3030.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3030RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3030RespService.class, Ln3030RespService.class);
            respService = ln3030RespService.getService();

            ln3030ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            ln3030ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3030RespDto
                BeanUtils.copyProperties(respService, ln3030RespDto);
                ln3030ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3030ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3030ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3030ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3030.key, EsbEnum.TRADE_CODE_LN3030.value, e.getMessage());
            ln3030ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3030ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3030ResultDto.setData(ln3030RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3030.key, EsbEnum.TRADE_CODE_LN3030.value, JSON.toJSONString(ln3030ResultDto));
        return ln3030ResultDto;
    }
}
