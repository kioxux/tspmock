package cn.com.yusys.yusp.web.client.esb.circp.fb1146;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.req.Fb1146ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.resp.Fb1146RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1146.req.Fb1146ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1146.resp.Fb1146RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1146）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1146Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1146Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1146
     * 交易描述：受托信息审核
     *
     * @param fb1146ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1146")
    protected @ResponseBody
    ResultDto<Fb1146RespDto> fb1146(@Validated @RequestBody Fb1146ReqDto fb1146ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1146.key, EsbEnum.TRADE_CODE_FB1146.value, JSON.toJSONString(fb1146ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1146.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1146.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1146.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1146.resp.Service();
        Fb1146ReqService fb1146ReqService = new Fb1146ReqService();
        Fb1146RespService fb1146RespService = new Fb1146RespService();
        Fb1146RespDto fb1146RespDto = new Fb1146RespDto();
        ResultDto<Fb1146RespDto> fb1146ResultDto = new ResultDto<Fb1146RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1146ReqDto转换成reqService
            BeanUtils.copyProperties(fb1146ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1146.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1146ReqService.setService(reqService);
            // 将fb1146ReqService转换成fb1146ReqServiceMap
            Map fb1146ReqServiceMap = beanMapUtil.beanToMap(fb1146ReqService);
            context.put("tradeDataMap", fb1146ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1146.key, EsbEnum.TRADE_CODE_FB1146.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1146.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1146.key, EsbEnum.TRADE_CODE_FB1146.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1146RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1146RespService.class, Fb1146RespService.class);
            respService = fb1146RespService.getService();

            fb1146ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1146ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1146RespDto
                BeanUtils.copyProperties(respService, fb1146RespDto);

                fb1146ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1146ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1146ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1146ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1146.key, EsbEnum.TRADE_CODE_FB1146.value, e.getMessage());
            fb1146ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1146ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1146ResultDto.setData(fb1146RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1146.key, EsbEnum.TRADE_CODE_FB1146.value, JSON.toJSONString(fb1146ResultDto));
        return fb1146ResultDto;
    }
}
