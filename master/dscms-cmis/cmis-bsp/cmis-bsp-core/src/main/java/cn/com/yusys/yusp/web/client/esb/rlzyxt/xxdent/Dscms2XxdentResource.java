package cn.com.yusys.yusp.web.client.esb.rlzyxt.xxdent;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentList;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.req.XxdentReqService;
import cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.resp.XxdentRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:新入职人员信息登记
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2rlzyxt")
public class Dscms2XxdentResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2XxdentResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xxdent
     * 交易描述：新入职人员信息登记
     *
     * @param xxdentReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xxdent")
    protected @ResponseBody
    ResultDto<XxdentRespDto> xxdent(@Validated @RequestBody XxdentReqDto xxdentReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDENT.key, EsbEnum.TRADE_CODE_XXDENT.value, JSON.toJSONString(xxdentReqDto));
        cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.req.Service();
        cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.resp.Service();
        XxdentReqService xxdentReqService = new XxdentReqService();
        XxdentRespService xxdentRespService = new XxdentRespService();
        XxdentRespDto xxdentRespDto = new XxdentRespDto();
        ResultDto<XxdentRespDto> xxdentResultDto = new ResultDto<XxdentRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xxdentReqDto转换成reqService
            BeanUtils.copyProperties(xxdentReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XXDENT.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            xxdentReqService.setService(reqService);
            // 将xxdentReqService转换成xxdentReqServiceMap
            Map xxdentReqServiceMap = beanMapUtil.beanToMap(xxdentReqService);
            context.put("tradeDataMap", xxdentReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDENT.key, EsbEnum.TRADE_CODE_XXDENT.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XXDENT.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDENT.key, EsbEnum.TRADE_CODE_XXDENT.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xxdentRespService = beanMapUtil.mapToBean(tradeDataMap, XxdentRespService.class, XxdentRespService.class);
            respService = xxdentRespService.getService();
            //  将respService转换成XxdentRespDto
            xxdentResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xxdentResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd01RespDto
                BeanUtils.copyProperties(respService, xxdentRespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.resp.List xxdentRespList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.resp.List());
                respService.setList(xxdentRespList);
                if (CollectionUtils.nonEmpty(xxdentRespList.getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.resp.Record> records = Optional.ofNullable(xxdentRespList.getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<XxdentList> xxdentLists = records.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentList xxdentList = new cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentList();
                        BeanUtils.copyProperties(record, xxdentList);
                        return xxdentList;
                    }).collect(Collectors.toList());
                    xxdentRespDto.setXxdentList(xxdentLists);
                }
                xxdentResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xxdentResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xxdentResultDto.setCode(EpbEnum.EPB099999.key);
                xxdentResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDENT.key, EsbEnum.TRADE_CODE_XXDENT.value, e.getMessage());
            xxdentResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xxdentResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xxdentResultDto.setData(xxdentRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDENT.key, EsbEnum.TRADE_CODE_XXDENT.value, JSON.toJSONString(xxdentResultDto));
        return xxdentResultDto;
    }
}
