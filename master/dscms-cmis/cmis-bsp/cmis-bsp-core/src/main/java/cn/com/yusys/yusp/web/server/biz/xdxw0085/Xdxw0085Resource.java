package cn.com.yusys.yusp.web.server.biz.xdxw0085;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0085.req.Xdxw0085ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0085.resp.Xdxw0085RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0085.req.Xdxw0085DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0085.resp.Xdxw0085DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:共借人声明文本生产PDF接口
 *
 * @author zr
 * @version 1.0
 */
@Api(tags = "XDXW0085:共借人声明文本生产PDF接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0085Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0085Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0085
     * 交易描述：共借人声明文本生产PDF接口
     *
     * @param xdxw0085ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("共借人声明文本生产PDF接口")
    @PostMapping("/xdxw0085")
    @Idempotent({"xdxw0085", "#xdxw0085ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0085RespDto xdxw0085(@Validated @RequestBody Xdxw0085ReqDto xdxw0085ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085ReqDto));
        Xdxw0085DataReqDto xdxw0085DataReqDto = new Xdxw0085DataReqDto();// 请求Data： 勘验任务状态同步
        Xdxw0085DataRespDto xdxw0085DataRespDto = new Xdxw0085DataRespDto();// 响应Data：勘验任务状态同步
		Xdxw0085RespDto xdxw0085RespDto = new Xdxw0085RespDto();
        // 此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0085.req.Data reqData = null; // 请求Data：勘验任务状态同步
		cn.com.yusys.yusp.dto.server.biz.xdxw0085.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0085.resp.Data();// 响应Data：勘验任务状态同步
        // 此处包导入待确定 结束
        try {
            // 从 xdxw0085ReqDto获取 reqData
            reqData = xdxw0085ReqDto.getData();
            // 将 reqData 拷贝到xdxw0085DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0085DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataReqDto));
            ResultDto<Xdxw0085DataRespDto> xdxw0085DataResultDto = dscmsBizXwClientService.xdxw0085(xdxw0085DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0085RespDto.setErorcd(Optional.ofNullable(xdxw0085DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0085RespDto.setErortx(Optional.ofNullable(xdxw0085DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0085DataResultDto.getCode())) {
                xdxw0085DataRespDto = xdxw0085DataResultDto.getData();
                xdxw0085RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0085RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0085DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0085DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, e.getMessage());
            xdxw0085RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0085RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0085RespDto.setDatasq(servsq);

        xdxw0085RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0085.key, DscmsEnum.TRADE_CODE_XDXW0085.value, JSON.toJSONString(xdxw0085RespDto));
        return xdxw0085RespDto;
    }
}
