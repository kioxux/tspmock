package cn.com.yusys.yusp.web.server.biz.xdca0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdca0002.req.Xdca0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdca0002.resp.Xdca0002RespDto;
import cn.com.yusys.yusp.dto.server.xdca0002.req.Xdca0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0002.resp.Xdca0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCaClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:大额分期申请（试算）接口
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0002:大额分期申请（试算）接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdca0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0002Resource.class);

	@Autowired
	private DscmsBizCaClientService dscmsBizCaClientService;

    /**
     * 交易码：xdca0002
     * 交易描述：大额分期申请（试算）接口
     *
     * @param xdca0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期申请（试算）接口")
    @PostMapping("/xdca0002")
//   @Idempotent({"xdcaca0002", "#xdca0002ReqDto.datasq"})
    protected @ResponseBody
	Xdca0002RespDto xdca0002(@Validated @RequestBody Xdca0002ReqDto xdca0002ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002ReqDto));
        Xdca0002DataReqDto xdca0002DataReqDto = new Xdca0002DataReqDto();// 请求Data： 大额分期申请（试算）接口
        Xdca0002DataRespDto xdca0002DataRespDto = new Xdca0002DataRespDto();// 响应Data：大额分期申请（试算）接口
		Xdca0002RespDto xdca0002RespDto = new Xdca0002RespDto();
        // 此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdca0002.req.Data reqData = null; // 请求Data：大额分期申请（试算）接口
		cn.com.yusys.yusp.dto.server.biz.xdca0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdca0002.resp.Data();// 响应Data：大额分期申请（试算）接口
        //  此处包导入待确定 结束
        try {
            // 从 xdca0002ReqDto获取 reqData
            reqData = xdca0002ReqDto.getData();
            // 将 reqData 拷贝到xdca0002DataReqDto
            BeanUtils.copyProperties(reqData, xdca0002DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002DataReqDto));
            ResultDto<Xdca0002DataRespDto> xdca0002DataResultDto = dscmsBizCaClientService.xdca0002(xdca0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdca0002RespDto.setErorcd(Optional.ofNullable(xdca0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdca0002RespDto.setErortx(Optional.ofNullable(xdca0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdca0002DataResultDto.getCode())) {
                xdca0002DataRespDto = xdca0002DataResultDto.getData();
                xdca0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdca0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdca0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdca0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, e.getMessage());
            xdca0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdca0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdca0002RespDto.setDatasq(servsq);

        xdca0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0002.key, DscmsEnum.TRADE_CODE_XDCA0002.value, JSON.toJSONString(xdca0002RespDto));
        return xdca0002RespDto;
    }
}
