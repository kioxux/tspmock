package cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp;

/**
 * 响应Service：查询人员基本信息岗位信息家庭信息
 * @author lihh
 * @version 1.0             
 */      
public class XxdrelRespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
