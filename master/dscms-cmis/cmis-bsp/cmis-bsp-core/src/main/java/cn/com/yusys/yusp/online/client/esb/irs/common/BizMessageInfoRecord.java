package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.math.BigDecimal;

/**
 * 响应Service：响应信息域元素:业务层债项评级结果
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class BizMessageInfoRecord {
    private String serialno; // 业务流水号
    private String guaranteeGrade; // 债项等级
    private BigDecimal ead; // 违约风险暴露
    private BigDecimal lgd; // 违约损失率

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getGuaranteeGrade() {
        return guaranteeGrade;
    }

    public void setGuaranteeGrade(String guaranteeGrade) {
        this.guaranteeGrade = guaranteeGrade;
    }

    public BigDecimal getEad() {
        return ead;
    }

    public void setEad(BigDecimal ead) {
        this.ead = ead;
    }

    public BigDecimal getLgd() {
        return lgd;
    }

    public void setLgd(BigDecimal lgd) {
        this.lgd = lgd;
    }

    @Override
    public String toString() {
        return "BizMessageInfo{" +
                "serialno='" + serialno + '\'' +
                ", guaranteeGrade='" + guaranteeGrade + '\'' +
                ", ead=" + ead +
                ", lgd=" + lgd +
                '}';
    }
}
