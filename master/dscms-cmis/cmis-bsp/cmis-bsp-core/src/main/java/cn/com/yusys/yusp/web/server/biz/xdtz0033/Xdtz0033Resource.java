package cn.com.yusys.yusp.web.server.biz.xdtz0033;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0033.req.Xdtz0033ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0033.resp.Xdtz0033RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0033.req.Xdtz0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0033.resp.Xdtz0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户所担保的行内贷款五级分类非正常状态件数
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDTZ0033:查询客户所担保的行内贷款五级分类非正常状态件数")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0033Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0033
     * 交易描述：查询客户所担保的行内贷款五级分类非正常状态件数
     *
     * @param xdtz0033ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户所担保的行内贷款五级分类非正常状态件数")
    @PostMapping("/xdtz0033")
    //@Idempotent({"xdcatz0033", "#xdtz0033ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0033RespDto xdtz0033(@Validated @RequestBody Xdtz0033ReqDto xdtz0033ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, JSON.toJSONString(xdtz0033ReqDto));
        Xdtz0033DataReqDto xdtz0033DataReqDto = new Xdtz0033DataReqDto();// 请求Data： 查询客户所担保的行内贷款五级分类非正常状态件数
        Xdtz0033DataRespDto xdtz0033DataRespDto = new Xdtz0033DataRespDto();// 响应Data：查询客户所担保的行内贷款五级分类非正常状态件数
        Xdtz0033RespDto xdtz0033RespDto = new Xdtz0033RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0033.req.Data reqData = null; // 请求Data：查询客户所担保的行内贷款五级分类非正常状态件数
        cn.com.yusys.yusp.dto.server.biz.xdtz0033.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0033.resp.Data();// 响应Data：查询客户所担保的行内贷款五级分类非正常状态件数
        try {
            // 从 xdtz0033ReqDto获取 reqData
            reqData = xdtz0033ReqDto.getData();
            // 将 reqData 拷贝到xdtz0033DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0033DataReqDto);
            // 调用服务
            ResultDto<Xdtz0033DataRespDto> xdtz0033DataResultDto = dscmsBizTzClientService.xdtz0033(xdtz0033DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdtz0033RespDto.setErorcd(Optional.ofNullable(xdtz0033DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0033RespDto.setErortx(Optional.ofNullable(xdtz0033DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0033DataResultDto.getCode())) {
                xdtz0033RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0033RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdtz0033DataRespDto = xdtz0033DataResultDto.getData();
                // 将 xdtz0033DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0033DataRespDto, respData);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, e.getMessage());
            xdtz0033RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0033RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0033RespDto.setDatasq(servsq);
        xdtz0033RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0033.key, DscmsEnum.TRADE_CODE_XDTZ0033.value, JSON.toJSONString(xdtz0033RespDto));
        return xdtz0033RespDto;
    }
}
