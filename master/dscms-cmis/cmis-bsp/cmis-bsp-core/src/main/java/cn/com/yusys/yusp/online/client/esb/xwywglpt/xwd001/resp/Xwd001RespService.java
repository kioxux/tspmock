package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.resp;

/**
 * @类名 Xwd001RespService
 * @描述 响应Service：新微贷贷款申请
 * @作者 黄勃
 * @时间 2021/10/26 19:34
 **/
public class Xwd001RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwd001RespService{" +
                "service=" + service +
                '}';
    }
}
