package cn.com.yusys.yusp.web.client.esb.core.ln3248;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3248.req.Ln3248ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3248.resp.Ln3248RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3248.req.Ln3248ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3248.resp.Ln3248RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3248)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3248Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3248Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();


    /**
     * 委托清收变更
     *
     * @param reqDto Ln3248ReqDto
     * @return ResultDto<Ln3248RespDto>
     * @throws Exception
     */
    @ApiOperation("ln3248:根贷款还款计划明细查询")
    @PostMapping("/ln3248")
    protected @ResponseBody
    ResultDto<Ln3248RespDto> ln3248(@Validated @RequestBody Ln3248ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3248.key, EsbEnum.TRADE_CODE_LN3248.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3248.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3248.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3248.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3248.resp.Service();
        Ln3248ReqService ln3248ReqService = new Ln3248ReqService();
        Ln3248RespService ln3248RespService = new Ln3248RespService();
        Ln3248RespDto ln3248RespDto = new Ln3248RespDto();
        ResultDto<Ln3248RespDto> ln3248ResultDto = new ResultDto<Ln3248RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {

            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3248.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3248ReqService.setService(reqService);
            // 将ln3248ReqService转换成ln3248ReqServiceMap
            Map ln3248ReqServiceMap = beanMapUtil.beanToMap(ln3248ReqService);
            context.put("tradeDataMap", ln3248ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3248.key, EsbEnum.TRADE_CODE_LN3248.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3248.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3248.key, EsbEnum.TRADE_CODE_LN3248.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3248RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3248RespService.class, Ln3248RespService.class);
            respService = ln3248RespService.getService();

            //  将Ln3248RespDto封装到ResultDto<Ln3248RespDto>
            ln3248ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3248ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3248RespDto
                BeanUtils.copyProperties(respService, ln3248RespDto);

                ln3248ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3248ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3248ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3248ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3248.key, EsbEnum.TRADE_CODE_LN3248.value, e.getMessage());
            ln3248ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3248ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3248ResultDto.setData(ln3248RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3248.key, EsbEnum.TRADE_CODE_LN3248.value, JSON.toJSONString(ln3248ResultDto));

        return ln3248ResultDto;
    }
}
