package cn.com.yusys.yusp.online.client.esb.core.ln3032.req;

/**
 * 请求Service：受托支付信息维护
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3032ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
