package cn.com.yusys.yusp.web.client.esb.fxyjxt.lsfxbg;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.lsfxbg.LsfxbgReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.lsfxbg.LsfxbgRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.req.LsfxbgReqService;
import cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.resp.LsfxbgRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:BSP封装调用客户风险预警系统的接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "BSP封装调用客户风险预警系统的接口处理类(lsfxbg)")
@RestController
@RequestMapping("/api/dscms2fxyjxt")
public class Dscms2LsfxbgResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2LsfxbgResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：lsfxbg
     * 交易描述：告查询客户项下历史风险预警报
     *
     * @param lsfxbgReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("lsfxbg:告查询客户项下历史风险预警报")
    @PostMapping("/lsfxbg")
    protected @ResponseBody
    ResultDto<LsfxbgRespDto> lsfxbg(@Validated @RequestBody LsfxbgReqDto lsfxbgReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSFXBG.key, EsbEnum.TRADE_CODE_LSFXBG.value, JSON.toJSONString(lsfxbgReqDto));
        cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.req.Service();
        cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.resp.Service();
        LsfxbgReqService lsfxbgReqService = new LsfxbgReqService();
        LsfxbgRespService lsfxbgRespService = new LsfxbgRespService();
        LsfxbgRespDto lsfxbgRespDto = new LsfxbgRespDto();
        ResultDto<LsfxbgRespDto> lsfxbgResultDto = new ResultDto<LsfxbgRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将lsfxbgReqDto转换成reqService
            BeanUtils.copyProperties(lsfxbgReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LSFXBG.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_FXYJXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_FXYJXT.key);//    部门号
            lsfxbgReqService.setService(reqService);
            // 将lsfxbgReqService转换成lsfxbgReqServiceMap
            Map lsfxbgReqServiceMap = beanMapUtil.beanToMap(lsfxbgReqService);
            context.put("tradeDataMap",lsfxbgReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSFXBG.key, EsbEnum.TRADE_CODE_LSFXBG.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LSFXBG.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSFXBG.key, EsbEnum.TRADE_CODE_LSFXBG.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            lsfxbgRespService = beanMapUtil.mapToBean(tradeDataMap, LsfxbgRespService.class, LsfxbgRespService.class);
            respService = lsfxbgRespService.getService();

            lsfxbgResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            lsfxbgResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成LsfxbgRespDto
                BeanUtils.copyProperties(respService, lsfxbgRespDto);
                lsfxbgResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                lsfxbgResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                lsfxbgResultDto.setCode(EpbEnum.EPB099999.key);
                lsfxbgResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSFXBG.key, EsbEnum.TRADE_CODE_LSFXBG.value, e.getMessage());
            lsfxbgResultDto.setCode(EpbEnum.EPB099999.key);//9999
            lsfxbgResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        lsfxbgResultDto.setData(lsfxbgRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSFXBG.key, EsbEnum.TRADE_CODE_LSFXBG.value, JSON.toJSONString(lsfxbgResultDto));
        return lsfxbgResultDto;
    }
}
