package cn.com.yusys.yusp.online.client.esb.core.ln3083.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 15:02
 * @since 2021/5/28 15:02
 */
public class Lstytczfe {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3083.resp.Record> recordList;

    public List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        return "Lstytczfe{" +
                "recordList=" + recordList +
                '}';
    }
}
