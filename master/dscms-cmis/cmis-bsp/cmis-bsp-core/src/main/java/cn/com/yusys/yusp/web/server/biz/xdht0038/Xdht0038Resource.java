package cn.com.yusys.yusp.web.server.biz.xdht0038;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0038.req.Xdht0038ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0038.resp.Xdht0038RespDto;
import cn.com.yusys.yusp.dto.server.xdht0038.req.Xdht0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0038.resp.Xdht0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:乐悠金根据核心客户号查询房贷首付款比例进行额度计算
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0038:乐悠金根据核心客户号查询房贷首付款比例进行额度计算")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0038Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0038Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0038
     * 交易描述：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
     *
     * @param xdht0038ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("乐悠金根据核心客户号查询房贷首付款比例进行额度计算")
    @PostMapping("/xdht0038")
    //@Idempotent({"xdcaht0038", "#xdht0038ReqDto.datasq"})
    protected @ResponseBody
    Xdht0038RespDto xdht0038(@Validated @RequestBody Xdht0038ReqDto xdht0038ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038ReqDto));
        Xdht0038DataReqDto xdht0038DataReqDto = new Xdht0038DataReqDto();// 请求Data： 乐悠金根据核心客户号查询房贷首付款比例进行额度计算
        Xdht0038DataRespDto xdht0038DataRespDto = new Xdht0038DataRespDto();// 响应Data：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
        Xdht0038RespDto xdht0038RespDto = new Xdht0038RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0038.req.Data reqData = null; // 请求Data：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
        cn.com.yusys.yusp.dto.server.biz.xdht0038.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0038.resp.Data();// 响应Data：乐悠金根据核心客户号查询房贷首付款比例进行额度计算
        try {
            // 从 xdht0038ReqDto获取 reqData
            reqData = xdht0038ReqDto.getData();
            // 将 reqData 拷贝到xdht0038DataReqDto
            BeanUtils.copyProperties(reqData, xdht0038DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038DataReqDto));
            ResultDto<Xdht0038DataRespDto> xdht0038DataResultDto = dscmsBizHtClientService.xdht0038(xdht0038DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0038RespDto.setErorcd(Optional.ofNullable(xdht0038DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0038RespDto.setErortx(Optional.ofNullable(xdht0038DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0038DataResultDto.getCode())) {
                xdht0038DataRespDto = xdht0038DataResultDto.getData();
                xdht0038RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0038RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0038DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0038DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, e.getMessage());
            xdht0038RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0038RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0038RespDto.setDatasq(servsq);

        xdht0038RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0038.key, DscmsEnum.TRADE_CODE_XDHT0038.value, JSON.toJSONString(xdht0038RespDto));
        return xdht0038RespDto;
    }
}
