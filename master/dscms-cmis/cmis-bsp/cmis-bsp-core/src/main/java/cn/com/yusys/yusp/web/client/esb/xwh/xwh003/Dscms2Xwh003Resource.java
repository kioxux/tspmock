package cn.com.yusys.yusp.web.client.esb.xwh.xwh003;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.req.Xwh003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.resp.Xwh003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwh.xwh003.req.Xwh003ReqService;
import cn.com.yusys.yusp.online.client.esb.xwh.xwh003.resp.Xwh003RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:核销锁定接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装小微公众号平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwh")
public class Dscms2Xwh003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xwh003Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xwh003
     * 交易描述：核销锁定接口
     *
     * @param xwh003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xwh003:核销锁定接口")
    @PostMapping("/xwh003")
    protected @ResponseBody
    ResultDto<Xwh003RespDto> xwh003(@Validated @RequestBody Xwh003ReqDto xwh003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH003.key, EsbEnum.TRADE_CODE_XWH003.value, JSON.toJSONString(xwh003ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwh.xwh003.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwh.xwh003.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwh.xwh003.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwh.xwh003.resp.Service();
        Xwh003ReqService xwh003ReqService = new Xwh003ReqService();
        Xwh003RespService xwh003RespService = new Xwh003RespService();
        Xwh003RespDto xwh003RespDto = new Xwh003RespDto();
        ResultDto<Xwh003RespDto> xwh003ResultDto = new ResultDto<Xwh003RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xwh003ReqDto转换成reqService
            BeanUtils.copyProperties(xwh003ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XWH003.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            xwh003ReqService.setService(reqService);
            // 将xwh003ReqService转换成xwh003ReqServiceMap
            Map xwh003ReqServiceMap = beanMapUtil.beanToMap(xwh003ReqService);
            context.put("tradeDataMap", xwh003ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH003.key, EsbEnum.TRADE_CODE_XWH003.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XWH003.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH003.key, EsbEnum.TRADE_CODE_XWH003.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xwh003RespService = beanMapUtil.mapToBean(tradeDataMap, Xwh003RespService.class, Xwh003RespService.class);
            respService = xwh003RespService.getService();

            xwh003ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xwh003ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, xwh003RespDto);
                xwh003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xwh003ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xwh003ResultDto.setCode(EpbEnum.EPB099999.key);
                xwh003ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH003.key, EsbEnum.TRADE_CODE_XWH003.value, e.getMessage());
            xwh003ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xwh003ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xwh003ResultDto.setData(xwh003RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH003.key, EsbEnum.TRADE_CODE_XWH003.value, JSON.toJSONString(xwh003ResultDto));
        return xwh003ResultDto;
    }
}
