package cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req;

/**
 * 请求Service：短信/微信发送批量接口
 *
 * @author hjk
 * @version 1.0
 */
public class Service {
    private String prcscd;//处理码                 ,  接口交易码区分交易
    private String servtp;//渠道                   ,  交易渠道
    private String servsq;//渠道流水               ,  由发起渠道生成的唯一标识
    private String sendti;//发送时间               ,  格式：YYYYMMDDHHMMSS（只对短信生效）
    private String infopt;//信息平台,dx:短信 wx1:微信 wx2:寿光微信 wxN：其它微信（此字段可扩展，结合微信签约时登记） sgdx:寿光短信 dhdx:东海短信
    private cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.List list;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getSendti() {
        return sendti;
    }

    public void setSendti(String sendti) {
        this.sendti = sendti;
    }

    public String getInfopt() {
        return infopt;
    }

    public void setInfopt(String infopt) {
        this.infopt = infopt;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", sendti='" + sendti + '\'' +
                ", infopt='" + infopt + '\'' +
                ", list=" + list +
                '}';
    }
}
