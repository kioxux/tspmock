package cn.com.yusys.yusp.online.client.esb.irs.xirs11.req;

/**
 * 请求Service：同业客户信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xirs11ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

