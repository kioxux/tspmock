package cn.com.yusys.yusp.web.server.biz.xdxw0058;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0058.req.Xdxw0058ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0058.resp.Xdxw0058RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0058.req.Xdxw0058DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0058.resp.Xdxw0058DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据流水号查询借据号
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0058:根据流水号查询借据号")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0058Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0058Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0058
     * 交易描述：根据流水号查询借据号
     *
     * @param xdxw0058ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询借据号")
    @PostMapping("/xdxw0058")
    //@Idempotent({"xdcaxw0058", "#xdxw0058ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0058RespDto xdxw0058(@Validated @RequestBody Xdxw0058ReqDto xdxw0058ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, JSON.toJSONString(xdxw0058ReqDto));
        Xdxw0058DataReqDto xdxw0058DataReqDto = new Xdxw0058DataReqDto();// 请求Data： 根据流水号查询借据号
        Xdxw0058DataRespDto xdxw0058DataRespDto = new Xdxw0058DataRespDto();// 响应Data：根据流水号查询借据号
        Xdxw0058RespDto xdxw0058RespDto = new Xdxw0058RespDto();// 响应Data：根据流水号查询借据号

        cn.com.yusys.yusp.dto.server.biz.xdxw0058.req.Data reqData = null; // 请求Data：根据流水号查询借据号
        cn.com.yusys.yusp.dto.server.biz.xdxw0058.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0058.resp.Data();// 响应Data：根据流水号查询借据号
        try {
            // 从 xdxw0058ReqDto获取 reqData
            reqData = xdxw0058ReqDto.getData();
            // 将 reqData 拷贝到xdxw0058DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0058DataReqDto);
            ResultDto<Xdxw0058DataRespDto> xdxw0058DataResultDto = dscmsBizXwClientService.xdxw0058(xdxw0058DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdxw0058RespDto.setErorcd(Optional.ofNullable(xdxw0058DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0058RespDto.setErortx(Optional.ofNullable(xdxw0058DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0058DataResultDto.getCode())) {
                xdxw0058RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0058RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0058DataRespDto = xdxw0058DataResultDto.getData();
                // 将 xdxw0058DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0058DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, e.getMessage());
            xdxw0058RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0058RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0058RespDto.setDatasq(servsq);

        xdxw0058RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0058.key, DscmsEnum.TRADE_CODE_XDXW0058.value, JSON.toJSONString(xdxw0058RespDto));
        return xdxw0058RespDto;
    }
}
