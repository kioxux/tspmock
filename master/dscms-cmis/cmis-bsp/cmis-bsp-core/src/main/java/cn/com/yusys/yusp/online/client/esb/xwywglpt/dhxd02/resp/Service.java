package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.resp;

/**
 * 响应Service：信贷系统请求小V平台贷后预警回传定期检查任务结果
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//返回码
    private String erortx;//返回提示

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                '}';
    }
}
