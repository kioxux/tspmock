package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcphkfs.Record;

import java.util.List;

/**
 * 响应Service：贷款产品还款方式组合对象
 *
 * @author lihh
 * @version 1.0
 */
public class Lstcphkfs_ARRAY {

    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstcphkfs{" +
                "record=" + record +
                '}';
    }
}
