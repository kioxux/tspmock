package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 保证人信息(AssurePersonInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:12
 */
public class AssurePersonInfo {

    private List<AssurePersonInfoRecord> record; // 保证人信息

    public List<AssurePersonInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<AssurePersonInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "AssurePersonInfo{" +
                "record=" + record +
                '}';
    }
}
