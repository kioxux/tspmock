package cn.com.yusys.yusp.web.server.biz.xdxw0041;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0041.req.Xdxw0041ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0041.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0041.resp.Xdxw0041RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0041.req.Xdxw0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0041.resp.Xdxw0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小企业无还本续贷审批结果维护
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0041:小企业无还本续贷审批结果维护")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0041Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0041Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0041
     * 交易描述：小企业无还本续贷审批结果维护
     *
     * @param xdxw0041ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小企业无还本续贷审批结果维护")
    @PostMapping("/xdxw0041")
    //@Idempotent({"xdcaxw0041", "#xdxw0041ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0041RespDto xdxw0041(@Validated @RequestBody Xdxw0041ReqDto xdxw0041ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041ReqDto));
        Xdxw0041DataReqDto xdxw0041DataReqDto = new Xdxw0041DataReqDto();// 请求Data： 小企业无还本续贷审批结果维护
        Xdxw0041DataRespDto xdxw0041DataRespDto = new Xdxw0041DataRespDto();// 响应Data：小企业无还本续贷审批结果维护
        cn.com.yusys.yusp.dto.server.biz.xdxw0041.req.Data reqData = null; // 请求Data：小企业无还本续贷审批结果维护
        cn.com.yusys.yusp.dto.server.biz.xdxw0041.resp.Data respData = new Data();// 响应Data：小企业无还本续贷审批结果维护
		Xdxw0041RespDto xdxw0041RespDto = new Xdxw0041RespDto();
		try {
            // 从 xdxw0041ReqDto获取 reqData
            reqData = xdxw0041ReqDto.getData();
            // 将 reqData 拷贝到xdxw0041DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0041DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataReqDto));
            ResultDto<Xdxw0041DataRespDto> xdxw0041DataResultDto = dscmsBizXwClientService.xdxw0041(xdxw0041DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0041RespDto.setErorcd(Optional.ofNullable(xdxw0041DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0041RespDto.setErortx(Optional.ofNullable(xdxw0041DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0041DataResultDto.getCode())) {
                xdxw0041DataRespDto = xdxw0041DataResultDto.getData();
                xdxw0041RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0041RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0041DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0041DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, e.getMessage());
            xdxw0041RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0041RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0041RespDto.setDatasq(servsq);

        xdxw0041RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0041.key, DscmsEnum.TRADE_CODE_XDXW0041.value, JSON.toJSONString(xdxw0041RespDto));
        return xdxw0041RespDto;
    }
}
