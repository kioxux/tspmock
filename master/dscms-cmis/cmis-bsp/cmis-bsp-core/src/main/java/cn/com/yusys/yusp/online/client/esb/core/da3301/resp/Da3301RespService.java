package cn.com.yusys.yusp.online.client.esb.core.da3301.resp;

/**
 * 响应Service：抵债资产入账
 * @author lihh
 * @version 1.0             
 */      
public class Da3301RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
