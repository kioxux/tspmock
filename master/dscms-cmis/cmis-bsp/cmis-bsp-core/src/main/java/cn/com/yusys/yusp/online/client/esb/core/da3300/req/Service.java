package cn.com.yusys.yusp.online.client.esb.core.da3300.req;

import java.math.BigDecimal;

/**
 * 请求Service：交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String dkkhczbz;//开户操作标志
    private String caozfshi;//操作方式
    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String ruzjigou;//入账机构
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String dzwzleib;//抵债物资类别
    private String cqzmzlei;//产权证明种类
    private String huobdhao;//货币代号
    private BigDecimal dbgdzcje;//代保管抵债资产金额
    private String zhaiyoms;//摘要
    private String dzzcdanw;//抵债资产单位
    private long dzzcshul;//抵债资产数量
    private BigDecimal pingjiaz;//评估价值
    private BigDecimal dzzcrzjz;//抵债资产入账价值
    private String beizhuuu;//备注信息

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getCaozfshi() {
        return caozfshi;
    }

    public void setCaozfshi(String caozfshi) {
        this.caozfshi = caozfshi;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getDzwzleib() {
        return dzwzleib;
    }

    public void setDzwzleib(String dzwzleib) {
        this.dzwzleib = dzwzleib;
    }

    public String getCqzmzlei() {
        return cqzmzlei;
    }

    public void setCqzmzlei(String cqzmzlei) {
        this.cqzmzlei = cqzmzlei;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getDbgdzcje() {
        return dbgdzcje;
    }

    public void setDbgdzcje(BigDecimal dbgdzcje) {
        this.dbgdzcje = dbgdzcje;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getDzzcdanw() {
        return dzzcdanw;
    }

    public void setDzzcdanw(String dzzcdanw) {
        this.dzzcdanw = dzzcdanw;
    }

    public long getDzzcshul() {
        return dzzcshul;
    }

    public void setDzzcshul(long dzzcshul) {
        this.dzzcshul = dzzcshul;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDzzcrzjz() {
        return dzzcrzjz;
    }

    public void setDzzcrzjz(BigDecimal dzzcrzjz) {
        this.dzzcrzjz = dzzcrzjz;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", dkkhczbz='" + dkkhczbz + '\'' +
                ", caozfshi='" + caozfshi + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", ruzjigou='" + ruzjigou + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", dzwzleib='" + dzwzleib + '\'' +
                ", cqzmzlei='" + cqzmzlei + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", dbgdzcje=" + dbgdzcje +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", dzzcdanw='" + dzzcdanw + '\'' +
                ", dzzcshul=" + dzzcshul +
                ", pingjiaz=" + pingjiaz +
                ", dzzcrzjz=" + dzzcrzjz +
                ", beizhuuu='" + beizhuuu + '\'' +
                '}';
    }
}
