package cn.com.yusys.yusp.web.server.biz.xdht0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0002.req.Xdht0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0002.resp.Xdht0002RespDto;
import cn.com.yusys.yusp.dto.server.xdht0002.req.Xdht0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0002.resp.Xdht0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:银承协议查询接口
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0002:银承协议查询接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0002Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0002
     * 交易描述：银承协议查询接口
     *
     * @param xdht0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("银承协议查询接口")
    @PostMapping("/xdht0002")
    //@Idempotent({"xdht0002", "#xdht0002ReqDto.datasq"})
    protected @ResponseBody
    Xdht0002RespDto xdht0002(@Validated @RequestBody Xdht0002ReqDto xdht0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002ReqDto));
        Xdht0002DataReqDto xdht0002DataReqDto = new Xdht0002DataReqDto();// 请求Data： 银承协议查询接口
        Xdht0002DataRespDto xdht0002DataRespDto = new Xdht0002DataRespDto();// 响应Data：银承协议查询接口
        Xdht0002RespDto xdht0002RespDto = new Xdht0002RespDto();// 响应Data：银承协议查询接口
        cn.com.yusys.yusp.dto.server.biz.xdht0002.req.Data reqData = null; // 请求Data：银承协议查询接口
        cn.com.yusys.yusp.dto.server.biz.xdht0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0002.resp.Data();// 响应Data：银承协议查询接口
        try {
            // 从 xdht0002ReqDto获取 reqData
            reqData = xdht0002ReqDto.getData();
            // 将 reqData 拷贝到xdht0002DataReqDto
            BeanUtils.copyProperties(reqData, xdht0002DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002ReqDto));
            ResultDto<Xdht0002DataRespDto> xdht0002DataResultDto = dscmsBizHtClientService.xdht0002(xdht0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002DataResultDto));

            // 从返回值中获取响应码和响应消息
            xdht0002RespDto.setErorcd(Optional.ofNullable(xdht0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0002RespDto.setErortx(Optional.ofNullable(xdht0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0002DataResultDto.getCode())) {
                xdht0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0002DataRespDto = xdht0002DataResultDto.getData();
                // 将 xdht0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, e.getMessage());
            xdht0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0002RespDto.setDatasq(servsq);

        xdht0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0002.key, DscmsEnum.TRADE_CODE_XDHT0002.value, JSON.toJSONString(xdht0002RespDto));
        return xdht0002RespDto;
    }
}
