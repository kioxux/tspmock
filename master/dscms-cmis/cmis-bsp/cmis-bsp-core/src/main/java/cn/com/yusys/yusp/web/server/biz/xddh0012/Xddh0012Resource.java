package cn.com.yusys.yusp.web.server.biz.xddh0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0012.req.Xddh0012ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0012.resp.Xddh0012RespDto;
import cn.com.yusys.yusp.dto.server.xddh0012.req.Xddh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0012.resp.Xddh0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:推送优享贷预警信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0012:推送优享贷预警信息")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0012Resource.class);
	@Autowired
	private DscmsBizDhClientService dscmsBizDhClientService;
    /**
     * 交易码：xddh0012
     * 交易描述：推送优享贷预警信息
     *
     * @param xddh0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送优享贷预警信息")
    @PostMapping("/xddh0012")
    @Idempotent({"xddh0012", "#xddh0012ReqDto.datasq"})
    protected @ResponseBody
	Xddh0012RespDto xddh0012(@Validated @RequestBody Xddh0012ReqDto xddh0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, JSON.toJSONString(xddh0012ReqDto));
        Xddh0012DataReqDto xddh0012DataReqDto = new Xddh0012DataReqDto();// 请求Data： 推送优享贷预警信息
        Xddh0012DataRespDto xddh0012DataRespDto = new Xddh0012DataRespDto();// 响应Data：推送优享贷预警信息
		Xddh0012RespDto xddh0012RespDto=new Xddh0012RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0012.req.Data reqData = null; // 请求Data：推送优享贷预警信息
        cn.com.yusys.yusp.dto.server.biz.xddh0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0012.resp.Data();// 响应Data：推送优享贷预警信息
        try {
            // 从 xddh0012ReqDto获取 reqData
            reqData = xddh0012ReqDto.getData();
            // 将 reqData 拷贝到xddh0012DataReqDto
            BeanUtils.copyProperties(reqData, xddh0012DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, JSON.toJSONString(xddh0012DataReqDto));
            ResultDto<Xddh0012DataRespDto> xddh0012DataResultDto = dscmsBizDhClientService.xddh0012(xddh0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, JSON.toJSONString(xddh0012DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0012RespDto.setErorcd(Optional.ofNullable(xddh0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0012RespDto.setErortx(Optional.ofNullable(xddh0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0012DataResultDto.getCode())) {
                xddh0012DataRespDto = xddh0012DataResultDto.getData();
                xddh0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, e.getMessage());
            xddh0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0012RespDto.setDatasq(servsq);

        xddh0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0012.key, DscmsEnum.TRADE_CODE_XDDH0012.value, JSON.toJSONString(xddh0012RespDto));
        return xddh0012RespDto;
    }
}
