package cn.com.yusys.yusp.web.server.biz.xdzc0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0010.req.Xdzc0010ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0010.resp.Xdzc0010RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0010.req.Xdzc0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0010.resp.Xdzc0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池超短贷校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0010:资产池超短贷校验接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0010Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0010
     * 交易描述：资产池超短贷校验接口
     *
     * @param xdzc0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池超短贷校验接口")
    @PostMapping("/xdzc0010")
    //@Idempotent({"xdzc010", "#xdzc0010ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0010RespDto xdzc0010(@Validated @RequestBody Xdzc0010ReqDto xdzc0010ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, JSON.toJSONString(xdzc0010ReqDto));
        Xdzc0010DataReqDto xdzc0010DataReqDto = new Xdzc0010DataReqDto();// 请求Data： 资产池超短贷校验接口
        Xdzc0010DataRespDto xdzc0010DataRespDto = new Xdzc0010DataRespDto();// 响应Data：资产池超短贷校验接口
        Xdzc0010RespDto xdzc0010RespDto = new Xdzc0010RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdzc0010.req.Data reqData = null; // 请求Data：资产池超短贷校验接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0010.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0010.resp.Data();// 响应Data：资产池超短贷校验接口

        try {
            // 从 xdzc0010ReqDto获取 reqData
            reqData = xdzc0010ReqDto.getData();
            // 将 reqData 拷贝到xdzc0010DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0010DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, JSON.toJSONString(xdzc0010DataReqDto));
            ResultDto<Xdzc0010DataRespDto> xdzc0010DataResultDto = dscmsBizZcClientService.xdzc0010(xdzc0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, JSON.toJSONString(xdzc0010DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0010RespDto.setErorcd(Optional.ofNullable(xdzc0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0010RespDto.setErortx(Optional.ofNullable(xdzc0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0010DataResultDto.getCode())) {
                xdzc0010DataRespDto = xdzc0010DataResultDto.getData();
                xdzc0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0010DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0010DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, e.getMessage());
            xdzc0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0010RespDto.setDatasq(servsq);

        xdzc0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0010.key, DscmsEnum.TRADE_CODE_XDZC0010.value, JSON.toJSONString(xdzc0010RespDto));
        return xdzc0010RespDto;
    }
}
