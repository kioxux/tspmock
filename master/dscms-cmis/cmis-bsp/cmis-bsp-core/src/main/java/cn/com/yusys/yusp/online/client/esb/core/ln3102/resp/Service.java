package cn.com.yusys.yusp.online.client.esb.core.ln3102.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款期供查询试算
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private BigDecimal hetongje;//合同金额
    private BigDecimal jiejuuje;//借据金额
    private String nyuelilv;//年/月利率标识
    private BigDecimal zhxnlilv;//执行年利率
    private String huankfsh;//还款方式
    private String dechligz;//等额处理规则
    private Integer zongqish;//总期数
    private Integer benqqish;//本期期数
    private Integer yuqiqish;//逾期期数
    private Integer shyuqish;//剩余期数
    private Integer yihqishu;//已还期数
    private BigDecimal leijyhbj;//累计已还本金
    private BigDecimal leijyhlx;//累计已还利息
    private String daikxtai;//贷款形态
    private String dkzhhzht;//贷款账户状态
    private String yjfyjzht;//应计非应计状态
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal meiqhbje;//每期还本金额
    private BigDecimal baoliuje;//保留金额
    private String hkzhouqi;//还款周期
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String schkriqi;//上次还款日
    private BigDecimal zhchbjin;//正常本金
    private BigDecimal hexiaobj;//核销本金
    private BigDecimal hexiaolx;//核销利息
    private Integer benqizqs;//本期子期数
    private String jiaoyirq;//交易日期
    private BigDecimal leijyqbj;//累计逾期本金
    private BigDecimal leijyqqx;//累计逾期欠息
    private BigDecimal leijcsbj;//累计产生本金
    private BigDecimal leijcslx;//累计产生利息
    private BigDecimal yingjitx;//应计贴息
    private BigDecimal tiexfuli;//贴息复利
    private BigDecimal yingshtx;//应收贴息
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String wjmingch;//文件名
    private Integer zongbish;//总笔数
    private Lstdkzqg lstdkzqg_ARRAY;//贷款账户期供
    private Lstdkhkzh lstdkhkzh_ARRAY;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getZhxnlilv() {
        return zhxnlilv;
    }

    public void setZhxnlilv(BigDecimal zhxnlilv) {
        this.zhxnlilv = zhxnlilv;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public Integer getZongqish() {
        return zongqish;
    }

    public void setZongqish(Integer zongqish) {
        this.zongqish = zongqish;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getYuqiqish() {
        return yuqiqish;
    }

    public void setYuqiqish(Integer yuqiqish) {
        this.yuqiqish = yuqiqish;
    }

    public Integer getShyuqish() {
        return shyuqish;
    }

    public void setShyuqish(Integer shyuqish) {
        this.shyuqish = shyuqish;
    }

    public Integer getYihqishu() {
        return yihqishu;
    }

    public void setYihqishu(Integer yihqishu) {
        this.yihqishu = yihqishu;
    }

    public BigDecimal getLeijyhbj() {
        return leijyhbj;
    }

    public void setLeijyhbj(BigDecimal leijyhbj) {
        this.leijyhbj = leijyhbj;
    }

    public BigDecimal getLeijyhlx() {
        return leijyhlx;
    }

    public void setLeijyhlx(BigDecimal leijyhlx) {
        this.leijyhlx = leijyhlx;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public BigDecimal getLeijyqbj() {
        return leijyqbj;
    }

    public void setLeijyqbj(BigDecimal leijyqbj) {
        this.leijyqbj = leijyqbj;
    }

    public BigDecimal getLeijyqqx() {
        return leijyqqx;
    }

    public void setLeijyqqx(BigDecimal leijyqqx) {
        this.leijyqqx = leijyqqx;
    }

    public BigDecimal getLeijcsbj() {
        return leijcsbj;
    }

    public void setLeijcsbj(BigDecimal leijcsbj) {
        this.leijcsbj = leijcsbj;
    }

    public BigDecimal getLeijcslx() {
        return leijcslx;
    }

    public void setLeijcslx(BigDecimal leijcslx) {
        this.leijcslx = leijcslx;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getTiexfuli() {
        return tiexfuli;
    }

    public void setTiexfuli(BigDecimal tiexfuli) {
        this.tiexfuli = tiexfuli;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getWjmingch() {
        return wjmingch;
    }

    public void setWjmingch(String wjmingch) {
        this.wjmingch = wjmingch;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public Lstdkzqg getLstdkzqg_ARRAY() {
        return lstdkzqg_ARRAY;
    }

    public void setLstdkzqg_ARRAY(Lstdkzqg lstdkzqg_ARRAY) {
        this.lstdkzqg_ARRAY = lstdkzqg_ARRAY;
    }

    public Lstdkhkzh getLstdkhkzh_ARRAY() {
        return lstdkhkzh_ARRAY;
    }

    public void setLstdkhkzh_ARRAY(Lstdkhkzh lstdkhkzh_ARRAY) {
        this.lstdkhkzh_ARRAY = lstdkhkzh_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", hetongje=" + hetongje +
                ", jiejuuje=" + jiejuuje +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", zhxnlilv=" + zhxnlilv +
                ", huankfsh='" + huankfsh + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", zongqish=" + zongqish +
                ", benqqish=" + benqqish +
                ", yuqiqish=" + yuqiqish +
                ", shyuqish=" + shyuqish +
                ", yihqishu=" + yihqishu +
                ", leijyhbj=" + leijyhbj +
                ", leijyhlx=" + leijyhlx +
                ", daikxtai='" + daikxtai + '\'' +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", yjfyjzht='" + yjfyjzht + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", baoliuje=" + baoliuje +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", schkriqi='" + schkriqi + '\'' +
                ", zhchbjin=" + zhchbjin +
                ", hexiaobj=" + hexiaobj +
                ", hexiaolx=" + hexiaolx +
                ", benqizqs=" + benqizqs +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", leijyqbj=" + leijyqbj +
                ", leijyqqx=" + leijyqqx +
                ", leijcsbj=" + leijcsbj +
                ", leijcslx=" + leijcslx +
                ", yingjitx=" + yingjitx +
                ", tiexfuli=" + tiexfuli +
                ", yingshtx=" + yingshtx +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", wjmingch='" + wjmingch + '\'' +
                ", zongbish=" + zongbish +
                ", lstdkzqg_ARRAY=" + lstdkzqg_ARRAY +
                ", lstdkhkzh_ARRAY=" + lstdkhkzh_ARRAY +
                '}';
    }
}
