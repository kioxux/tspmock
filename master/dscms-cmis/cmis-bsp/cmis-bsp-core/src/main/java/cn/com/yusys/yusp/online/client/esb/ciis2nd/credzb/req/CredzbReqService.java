package cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.req;

/**
 * 请求Service：指标通用接口
 *
 * @author leehuang
 * @version 1.0
 */
public class CredzbReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "CredzbReqService{" +
                "service=" + service +
                '}';
    }
}
