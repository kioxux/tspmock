package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.req;

/**
 * 请求Service：查询抵押物信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xddb02ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
