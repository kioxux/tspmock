package cn.com.yusys.yusp.web.client.esb.ciis2nd.credzb;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.*;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.req.CredzbReqService;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.resp.CredzbRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用二代征信系统的接口处理类
 **/
@Api(tags = "BSP封装调用二代征信系统的接口处理类（credzb）")
@RestController
@RequestMapping("/api/dscms2ciis2nd")
public class Dscms2CredzbResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2CredzbResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 指标通用接口（处理码credzb）
     *
     * @param credzbReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("credzb:指标通用接口")
    @PostMapping("/credzb")
    protected @ResponseBody
    ResultDto<CredzbRespDto> credzb(@Validated @RequestBody CredzbReqDto credzbReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value, JSON.toJSONString(credzbReqDto));
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.req.Service();
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.resp.Service();
        CredzbReqService credzbReqService = new CredzbReqService();
        CredzbRespService credzbRespService = new CredzbRespService();
        CredzbRespDto credzbRespDto = new CredzbRespDto();
        ResultDto<CredzbRespDto> credzbResultDto = new ResultDto<CredzbRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将CredzbReqDto转换成reqService
            BeanUtils.copyProperties(credzbReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CREDZB.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setBrchno(credzbReqDto.getBrchno());//    部门号
            LocalDateTime now = LocalDateTime.now();
            credzbReqService.setService(reqService);
            // 将credzbReqService转换成credzbReqServiceMap
            Map credzbReqServiceMap = beanMapUtil.beanToMap(credzbReqService);
            context.put("tradeDataMap", credzbReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CREDZB.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            credzbRespService = beanMapUtil.mapToBean(tradeDataMap, CredzbRespService.class, CredzbRespService.class);
            respService = credzbRespService.getService();

            credzbResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            credzbResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将CredzbRespDto封装到ResultDto<CredzbRespDto>
                BeanUtils.copyProperties(respService, credzbRespDto);
                String ruleCode = credzbRespDto.getRuleCode();//指标编码
                String credzbRespResult = credzbRespDto.getResult();//查询结果
                R001 r001 = new R001();//个人根据业务号获取指定报告
                R002 r002 = new R002();//企业根据业务号获取指定报告
                R003 r003 = new R003();//个人多少天内最新本地征信
                R004 r004 = new R004();//企业多少天内最新本地征信
                R005 r005 = new R005();//个人获取客户最新授权
                R006 r006 = new R006();//企业获取客户最新授权
                R007 r007 = new R007();//小微优企贷风险提示
                R008 r008 = new R008();//零售风险提示
                R009 r009 = new R009();//小微征信信息
                R010 r010 = new R010();//小微企业征信信息
                if (Objects.nonNull(credzbRespResult)) {
                    logger.info("解析查询结果到指标编码对象开始");
                    if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R001.key)) {
                        r001 = (R001) JSON.parseObject(credzbRespResult, R001.class);
                        credzbRespDto.setR001(r001);//个人根据业务号获取指定报告
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R002.key)) {
                        r002 = (R002) JSON.parseObject(credzbRespResult, R002.class);
                        credzbRespDto.setR002(r002);//企业根据业务号获取指定报告
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R003.key)) {
                        r003 = (R003) JSON.parseObject(credzbRespResult, R003.class);
                        credzbRespDto.setR003(r003);//个人多少天内最新本地征信
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R004.key)) {
                        r004 = (R004) JSON.parseObject(credzbRespResult, R004.class);
                        credzbRespDto.setR004(r004);//企业多少天内最新本地征信
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R005.key)) {
                        r005 = (R005) JSON.parseObject(credzbRespResult, R005.class);
                        credzbRespDto.setR005(r005);//个人获取客户最新授权
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R006.key)) {
                        r006 = (R006) JSON.parseObject(credzbRespResult, R006.class);
                        credzbRespDto.setR006(r006);//企业获取客户最新授权
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R007.key)) {
                        r007 = (R007) JSON.parseObject(credzbRespResult, R007.class);
                        credzbRespDto.setR007(r007);//小微优企贷风险提示
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R008.key)) {
                        r008 = (R008) JSON.parseObject(credzbRespResult, R008.class);
                        credzbRespDto.setR008(r008);//零售风险提示
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R009.key)) {
                        r009 = (R009) JSON.parseObject(credzbRespResult, R009.class);
                        credzbRespDto.setR009(r009);//小微征信信息
                    } else if (Objects.equals(ruleCode, DscmsBizZxEnum.RULE_CODE_R010.key)) {
                        r010 = (R010) JSON.parseObject(credzbRespResult, R010.class);
                        credzbRespDto.setR010(r010);//小微企业征信信息
                    }
                    logger.info("解析查询结果到指标编码对象结束");
                }
                credzbResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                credzbResultDto.setMessage(Optional.ofNullable(credzbResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            } else {
                credzbResultDto.setCode(EpbEnum.EPB099999.key);
                credzbResultDto.setMessage(respService.getErortx());
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value, e.getMessage());
            credzbResultDto.setCode(EpbEnum.EPB099999.key);//9999
            credzbResultDto.setMessage(e.getMessage());// 业务异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value, e.getMessage());
            credzbResultDto.setCode(EpbEnum.EPB099999.key);//9999
            credzbResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        credzbResultDto.setData(credzbRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDZB.key, EsbEnum.TRADE_CODE_CREDZB.value, JSON.toJSONString(credzbResultDto));
        return credzbResultDto;
    }
}
