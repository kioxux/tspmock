package cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.resp;

/**
 * 响应Service：缴存信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class JcxxcxRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "JcxxcxRespService{" +
                "service=" + service +
                '}';
    }
}

