package cn.com.yusys.yusp.online.client.esb.core.ln3066.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 14:53
 * @since 2021/6/7 14:53
 */
public class Results {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3066.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Results{" +
                "record=" + record +
                '}';
    }
}
