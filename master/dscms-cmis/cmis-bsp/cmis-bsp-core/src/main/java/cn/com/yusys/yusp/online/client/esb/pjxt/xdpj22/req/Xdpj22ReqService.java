package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.req;

/**
 * 请求Service：根据批次号查询票号和票面金额
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdpj22ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdpj22ReqService{" +
                "service=" + service +
                '}';
    }
}
