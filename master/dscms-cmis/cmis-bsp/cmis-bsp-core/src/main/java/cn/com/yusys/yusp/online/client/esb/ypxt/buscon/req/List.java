package cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/29 16:17
 * @since 2021/5/29 16:17
 */
public class List {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
