package cn.com.yusys.yusp.online.client.esb.core.ln3123.req;

/**
 * 请求Service：受托支付查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021年4月21日 上午11:22:06
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间
    private String datasq;//全局流水

    private String dkjiejuh;//贷款借据号
    private String qudaohao;//发布系统/渠道
    private String dkzhangh;//贷款账号
    private Integer xuhaoooo;//序号
    private String stzfriqi;//受托支付日期
    private String stdjriqi;//受托登记日期
    private String djiebhao;//冻结编号
    private String zjlyzhao;//资金来源账号
    private String zjlyzzxh;//资金来源账号子序号
    private String zjzrzhao;//资金转入账号
    private String zjzrzzxh;//资金转入账号子序号
    private String dfzhangh;//对方账号
    private String dfzhhzxh;//对方账号子序号
    private String stzfclzt;//受托支付处理状态
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getQudaohao() {
        return qudaohao;
    }

    public void setQudaohao(String qudaohao) {
        this.qudaohao = qudaohao;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getStzfriqi() {
        return stzfriqi;
    }

    public void setStzfriqi(String stzfriqi) {
        this.stzfriqi = stzfriqi;
    }

    public String getStdjriqi() {
        return stdjriqi;
    }

    public void setStdjriqi(String stdjriqi) {
        this.stdjriqi = stdjriqi;
    }

    public String getDjiebhao() {
        return djiebhao;
    }

    public void setDjiebhao(String djiebhao) {
        this.djiebhao = djiebhao;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public String getDfzhangh() {
        return dfzhangh;
    }

    public void setDfzhangh(String dfzhangh) {
        this.dfzhangh = dfzhangh;
    }

    public String getDfzhhzxh() {
        return dfzhhzxh;
    }

    public void setDfzhhzxh(String dfzhhzxh) {
        this.dfzhhzxh = dfzhhzxh;
    }

    public String getStzfclzt() {
        return stzfclzt;
    }

    public void setStzfclzt(String stzfclzt) {
        this.stzfclzt = stzfclzt;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", qudaohao='" + qudaohao + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", xuhaoooo=" + xuhaoooo +
                ", stzfriqi='" + stzfriqi + '\'' +
                ", stdjriqi='" + stdjriqi + '\'' +
                ", djiebhao='" + djiebhao + '\'' +
                ", zjlyzhao='" + zjlyzhao + '\'' +
                ", zjlyzzxh='" + zjlyzzxh + '\'' +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                ", dfzhangh='" + dfzhangh + '\'' +
                ", dfzhhzxh='" + dfzhhzxh + '\'' +
                ", stzfclzt='" + stzfclzt + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                '}';
    }
}
