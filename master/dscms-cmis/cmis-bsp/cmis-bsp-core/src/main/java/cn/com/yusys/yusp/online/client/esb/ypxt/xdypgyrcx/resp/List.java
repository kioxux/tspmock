package cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp;


import java.io.Serializable;

public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    private java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.Record> record;

    public java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.Record> getRecord() {
        return record;
    }
    public void setRecord(java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
