package cn.com.yusys.yusp.web.server.biz.xdxw0084;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0084.req.Xdxw0084ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0084.resp.Xdxw0084RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0084.req.Xdxw0084DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0084.resp.Xdxw0084DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:共借人送达地址确认书文本生成PDF
 *
 * @author zr
 * @version 1.0
 */
@Api(tags = "XDXW0084:共借人送达地址确认书文本生成PDF接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0084Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0084Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0084
     * 交易描述：共借人送达地址确认书文本生成PDF
     *
     * @param xdxw0084ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("共借人送达地址确认书文本生成PDF")
    @PostMapping("/xdxw0084")
    @Idempotent({"xdxw0084", "#xdxw0084ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0084RespDto xdxw0084(@Validated @RequestBody Xdxw0084ReqDto xdxw0084ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084ReqDto));
        Xdxw0084DataReqDto xdxw0084DataReqDto = new Xdxw0084DataReqDto();// 请求Data： 勘验任务状态同步
        Xdxw0084DataRespDto xdxw0084DataRespDto = new Xdxw0084DataRespDto();// 响应Data：勘验任务状态同步
		Xdxw0084RespDto xdxw0084RespDto = new Xdxw0084RespDto();
        // 此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0084.req.Data reqData = null; // 请求Data：勘验任务状态同步
		cn.com.yusys.yusp.dto.server.biz.xdxw0084.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0084.resp.Data();// 响应Data：勘验任务状态同步
        // 此处包导入待确定 结束
        try {
            // 从 xdxw0084ReqDto获取 reqData
            reqData = xdxw0084ReqDto.getData();
            // 将 reqData 拷贝到xdxw0084DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0084DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataReqDto));
            ResultDto<Xdxw0084DataRespDto> xdxw0084DataResultDto = dscmsBizXwClientService.xdxw0084(xdxw0084DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0084RespDto.setErorcd(Optional.ofNullable(xdxw0084DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0084RespDto.setErortx(Optional.ofNullable(xdxw0084DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0084DataResultDto.getCode())) {
                xdxw0084DataRespDto = xdxw0084DataResultDto.getData();
                xdxw0084RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0084RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0084DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0084DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, e.getMessage());
            xdxw0084RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0084RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0084RespDto.setDatasq(servsq);

        xdxw0084RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0084.key, DscmsEnum.TRADE_CODE_XDXW0084.value, JSON.toJSONString(xdxw0084RespDto));
        return xdxw0084RespDto;
    }
}
