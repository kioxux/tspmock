package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registermortgagelist;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:18
 * @since 2021/7/9 14:18
 */
public class Registermortgagelist {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registermortgagelist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Registermortgagelist{" +
                "record=" + record +
                '}';
    }
}
