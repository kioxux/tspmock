package cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.resp;

/**
 * 响应Service：未备注
 *
 * @author chenyong
 * @version 1.0
 */
public class ClzjcxRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
