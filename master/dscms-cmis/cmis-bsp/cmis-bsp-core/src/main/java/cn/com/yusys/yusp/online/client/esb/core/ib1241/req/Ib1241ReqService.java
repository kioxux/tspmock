package cn.com.yusys.yusp.online.client.esb.core.ib1241.req;

/**
 * 请求Service：外围自动冲正(出库撤销)
 *
 * @author zhangpeng
 * @version 1.0
 */
public class Ib1241ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}


