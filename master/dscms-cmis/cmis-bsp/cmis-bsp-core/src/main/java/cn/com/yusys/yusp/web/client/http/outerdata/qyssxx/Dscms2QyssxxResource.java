package cn.com.yusys.yusp.web.client.http.outerdata.qyssxx;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.req.QyssxxReqService;
import cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.QyssxxRespService;
import cn.com.yusys.yusp.web.client.http.outerdata.idcheck.Dscms2IdCheckResource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(qyssxx)")
@RestController
@RequestMapping("/api/dscms2outerdata")
public class Dscms2QyssxxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2IdCheckResource.class);
    @Autowired
    private RestTemplate restTemplate;
    @Value("${application.outerdata.url}")
    private String outerdataUrl;

    /**
     * 企业工商信息查询
     *
     * @param qyssxxReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("qyssxx:企业工商信息查询")
    @PostMapping("/qyssxx")
    protected @ResponseBody
    ResultDto<QyssxxRespDto> qyssxx(@Validated @RequestBody QyssxxReqDto qyssxxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, JSON.toJSONString(qyssxxReqDto));

        QyssxxReqService qyssxxReqService = new QyssxxReqService();
        QyssxxRespService qyssxxRespService = new QyssxxRespService();
        QyssxxRespDto qyssxxRespDto = new QyssxxRespDto();
        ResultDto<QyssxxRespDto> qyssxxResultDto = new ResultDto<QyssxxRespDto>();
        try {
            //  将QyssxxReqDto转换成QyssxxReqService
            BeanUtils.copyProperties(qyssxxReqDto, qyssxxReqService);

            qyssxxReqService.setPrcscd(EsbEnum.TRADE_CODE_QYSSXX.key);//    交易码
            qyssxxReqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            qyssxxReqService.setServsq(servsq);//    渠道流水
            qyssxxReqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            qyssxxReqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(qyssxxReqService), headers);
            //String url = "http://10.28.122.177:7006/edataApi/data";
            String url = outerdataUrl + "/edataApi/data";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, JSON.toJSONString(qyssxxReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, responseEntity.getBody());

            String responseStr = responseEntity.getBody();
            qyssxxRespService = (QyssxxRespService) JSON.parseObject(responseStr, QyssxxRespService.class);
            cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.Data qyssxxRespData = new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.Data();
            cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Data qyssxxRespDtoData = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Data();
            //  将QyssxxRespDto封装到ResultDto<QyssxxRespDto>
            qyssxxResultDto.setCode(Optional.ofNullable(qyssxxRespService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            qyssxxResultDto.setMessage(Optional.ofNullable(qyssxxRespService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, qyssxxRespService.getErorcd())) {
                qyssxxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                qyssxxResultDto.setMessage(SuccessEnum.SUCCESS.value);
                qyssxxRespData = qyssxxRespService.getData();

                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Data qyssxxRespServiceData = Optional.ofNullable(qyssxxRespData.getData()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Data());
                //  将QyssxxRespService转换成QyssxxRespDto
                BeanUtils.copyProperties(qyssxxRespData, qyssxxRespDto);
                /* 对返回的Data 单独处理 开始*/
                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Civil civilRespService = Optional.ofNullable(qyssxxRespServiceData.getCivil()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Civil());//	民事案件
                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Criminal criminalRespService = Optional.ofNullable(qyssxxRespServiceData.getCriminal()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Criminal());//	刑事案件
                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Administrative administrativeRespService = Optional.ofNullable(qyssxxRespServiceData.getAdministrative()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Administrative());//	行政案件
                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Implement implementRespService = Optional.ofNullable(qyssxxRespServiceData.getImplement()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Implement());//执行案件
                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Bankrupt bankruptRespService = Optional.ofNullable(qyssxxRespServiceData.getBankrupt()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Bankrupt());//	强制清算与破产案件
                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Count countRespService = Optional.ofNullable(qyssxxRespServiceData.getCount()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Count());
                cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Preservation preservationRespService = Optional.ofNullable(qyssxxRespServiceData.getPreservation()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Preservation());

                cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Civil civilRespDto = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Civil();//	民事案件
                cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Criminal criminalRespDto = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Criminal();//	刑事案件
                cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Administrative administrativeRespDto = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Administrative();//	行政案件
                cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Implement implementRespDto = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Implement();//执行案件
                cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Bankrupt bankruptRespDto = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Bankrupt();//	强制清算与破产案件
                cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Count countRespDto = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Count();
                cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Preservation preservationRespDto = new cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.Preservation();

                BeanUtils.copyProperties(civilRespService, civilRespDto);
                BeanUtils.copyProperties(criminalRespService, criminalRespDto);
                BeanUtils.copyProperties(administrativeRespService, administrativeRespDto);
                BeanUtils.copyProperties(implementRespService, implementRespDto);
                BeanUtils.copyProperties(bankruptRespService, bankruptRespDto);
                BeanUtils.copyProperties(countRespService, countRespDto);
                BeanUtils.copyProperties(preservationRespService, preservationRespDto);

                qyssxxRespDtoData.setCivil(civilRespDto);
                qyssxxRespDtoData.setCriminal(criminalRespDto);
                qyssxxRespDtoData.setAdministrative(administrativeRespDto);
                qyssxxRespDtoData.setImplement(implementRespDto);
                qyssxxRespDtoData.setBankrupt(bankruptRespDto);
                qyssxxRespDtoData.setCount(countRespDto);
                qyssxxRespDtoData.setPreservation(preservationRespDto);
                /* 对返回的Data 单独处理 结束*/
                qyssxxRespDto.setData(qyssxxRespDtoData);
            } else {
                qyssxxResultDto.setCode(EpbEnum.EPB099999.key);
                qyssxxResultDto.setMessage(qyssxxRespService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, e.getMessage());
            qyssxxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            qyssxxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        qyssxxResultDto.setData(qyssxxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QYSSXX.key, EsbEnum.TRADE_CODE_QYSSXX.value, JSON.toJSONString(qyssxxResultDto));

        return qyssxxResultDto;
    }
}
