package cn.com.yusys.yusp.online.client.esb.core.dp2021.req;

/**
 * 请求Service：客户账号-子账号互查
 *
 * @author chenyong
 * @version 1.0
 */
public class Dp2021ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Dp2021ReqService{" +
                "service=" + service +
                '}';
    }
}

