package cn.com.yusys.yusp.web.server.biz.xdls0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdls0003.req.Xdls0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdls0003.resp.Xdls0003RespDto;
import cn.com.yusys.yusp.dto.server.xdls0003.req.Xdls0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0003.resp.Xdls0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizLsClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷授信协议合同金额查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDLS0003:信贷授信协议合同金额查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdls0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdls0003Resource.class);
    @Autowired
    private DscmsBizLsClientService dscmsBizLsClientService;
    /**
     * 交易码：xdls0003
     * 交易描述：信贷授信协议合同金额查询
     *
     * @param xdls0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷授信协议合同金额查询")
    @PostMapping("/xdls0003")
    //@Idempotent({"xdcals0003", "#xdls0003ReqDto.datasq"})
    protected @ResponseBody
    Xdls0003RespDto xdls0003(@Validated @RequestBody Xdls0003ReqDto xdls0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, JSON.toJSONString(xdls0003ReqDto));
        Xdls0003DataReqDto xdls0003DataReqDto = new Xdls0003DataReqDto();// 请求Data： 信贷授信协议合同金额查询
        Xdls0003DataRespDto xdls0003DataRespDto = new Xdls0003DataRespDto();// 响应Data：信贷授信协议合同金额查询
        Xdls0003RespDto xdls0003RespDto = new Xdls0003RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdls0003.req.Data reqData = null; // 请求Data：信贷授信协议合同金额查询
        cn.com.yusys.yusp.dto.server.biz.xdls0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdls0003.resp.Data();// 响应Data：信贷授信协议合同金额查询

        try {
            // 从 xdls0003ReqDto获取 reqData
            reqData = xdls0003ReqDto.getData();
            // 将 reqData 拷贝到xdls0003DataReqDto
            BeanUtils.copyProperties(reqData, xdls0003DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, JSON.toJSONString(xdls0003DataReqDto));
            ResultDto<Xdls0003DataRespDto> xdls0003DataResultDto = dscmsBizLsClientService.xdls0003(xdls0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, JSON.toJSONString(xdls0003DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdls0003RespDto.setErorcd(Optional.ofNullable(xdls0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdls0003RespDto.setErortx(Optional.ofNullable(xdls0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdls0003DataResultDto.getCode())) {
                xdls0003DataRespDto = xdls0003DataResultDto.getData();
                xdls0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdls0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdls0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdls0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, e.getMessage());
            xdls0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdls0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdls0003RespDto.setDatasq(servsq);

        xdls0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0003.key, DscmsEnum.TRADE_CODE_XDLS0003.value, JSON.toJSONString(xdls0003RespDto));
        return xdls0003RespDto;
    }
}
