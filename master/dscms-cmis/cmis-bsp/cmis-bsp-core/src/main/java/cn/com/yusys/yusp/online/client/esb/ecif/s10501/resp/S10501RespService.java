package cn.com.yusys.yusp.online.client.esb.ecif.s10501.resp;

/**
 * 响应Service：对私客户清单查询
 */
public class S10501RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
