package cn.com.yusys.yusp.online.client.esb.core.ln3077.req;

import java.math.BigDecimal;

/**
 * 请求Service：客户账本息调整
 */
public class Service {
	private String prcscd;//    交易码
	private String servtp;//    渠道
	private String servsq;//    渠道流水
	private String userid;//    柜员号
	private String brchno;//    部门号
	private String servdt;//    交易日期
	private String servti;//    交易时间
	private String datasq;//    全局流水

    private String daikczbz;//业务操作标志
    private String dkjiejuh;//贷款借据号
    private BigDecimal zhchbjin;//正常本金
    private BigDecimal yuqibjin;//逾期本金
    private BigDecimal dzhibjin;//呆滞本金
    private BigDecimal daizbjin;//呆账本金
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal csyjlixi;//催收应计利息
    private BigDecimal ysqianxi;//应收欠息
    private BigDecimal csqianxi;//催收欠息
    private BigDecimal ysyjfaxi;//应收应计罚息
    private BigDecimal csyjfaxi;//催收应计罚息
    private BigDecimal yshofaxi;//应收罚息
    private BigDecimal cshofaxi;//催收罚息
    private BigDecimal yingjifx;//应计复息
    private BigDecimal fuxiiiii;//复息
    private BigDecimal yingjitx;//应计贴息
    private BigDecimal yingshtx;//应收贴息
    private BigDecimal hexiaobj;//核销本金
    private BigDecimal hexiaolx;//核销利息
    private BigDecimal yihxbjlx;//已核销本金利息
    private BigDecimal yingshfj;//应收罚金
    private BigDecimal huankjee;//还款金额
    private BigDecimal hexiaoje;//核销来源金额
    private String hexiaozh;//核销来源账号
    private String hexiaozx;//核销来源账号子序号
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public BigDecimal getHexiaoje() {
        return hexiaoje;
    }

    public void setHexiaoje(BigDecimal hexiaoje) {
        this.hexiaoje = hexiaoje;
    }

    public String getHexiaozh() {
        return hexiaozh;
    }

    public void setHexiaozh(String hexiaozh) {
        this.hexiaozh = hexiaozh;
    }

    public String getHexiaozx() {
        return hexiaozx;
    }

    public void setHexiaozx(String hexiaozx) {
        this.hexiaozx = hexiaozx;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", datasq='" + datasq + '\'' +
				", daikczbz='" + daikczbz + '\'' +
				", dkjiejuh='" + dkjiejuh + '\'' +
				", zhchbjin=" + zhchbjin +
				", yuqibjin=" + yuqibjin +
				", dzhibjin=" + dzhibjin +
				", daizbjin=" + daizbjin +
				", ysyjlixi=" + ysyjlixi +
				", csyjlixi=" + csyjlixi +
				", ysqianxi=" + ysqianxi +
				", csqianxi=" + csqianxi +
				", ysyjfaxi=" + ysyjfaxi +
				", csyjfaxi=" + csyjfaxi +
				", yshofaxi=" + yshofaxi +
				", cshofaxi=" + cshofaxi +
				", yingjifx=" + yingjifx +
				", fuxiiiii=" + fuxiiiii +
				", yingjitx=" + yingjitx +
				", yingshtx=" + yingshtx +
				", hexiaobj=" + hexiaobj +
				", hexiaolx=" + hexiaolx +
				", yihxbjlx=" + yihxbjlx +
				", yingshfj=" + yingshfj +
				", huankjee=" + huankjee +
				", hexiaoje=" + hexiaoje +
				", hexiaozh='" + hexiaozh + '\'' +
				", hexiaozx='" + hexiaozx + '\'' +
				", huankzhh='" + huankzhh + '\'' +
				", hkzhhzxh='" + hkzhhzxh + '\'' +
				'}';
	}
}
