package cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req;

/**
 * 请求Service：押品信息同步及引入
 */
public class XdypxxtbyrReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "XdypxxtbyrReqService{" +
                "service=" + service +
                '}';
    }
}
