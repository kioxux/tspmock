package cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req;

/**
 * 请求Message：账单列表查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class D12050ReqMessage {
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D15011ReqMessage{" +
                "message=" + message +
                '}';
    }
}
