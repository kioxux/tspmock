package cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/6 11:33
 * @since 2021/7/6 11:33
 */
public class List {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
