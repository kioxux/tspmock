package cn.com.yusys.yusp.web.server.biz.xdxw0061;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0061.req.Xdxw0061ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0061.resp.Xdxw0061RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0061.req.Xdxw0061DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0061.resp.Xdxw0061DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:通过无还本续贷调查表编号查询配偶核心客户号
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0061:通过无还本续贷调查表编号查询配偶核心客户号")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0061Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0061Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0061
     * 交易描述：通过无还本续贷调查表编号查询配偶核心客户号
     *
     * @param xdxw0061ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("通过无还本续贷调查表编号查询配偶核心客户号")
    @PostMapping("/xdxw0061")
    //@Idempotent({"xdcaxw0061", "#xdxw0061ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0061RespDto xdxw0061(@Validated @RequestBody Xdxw0061ReqDto xdxw0061ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, JSON.toJSONString(xdxw0061ReqDto));
        Xdxw0061DataReqDto xdxw0061DataReqDto = new Xdxw0061DataReqDto();// 请求Data： 通过无还本续贷调查表编号查询配偶核心客户号
        Xdxw0061DataRespDto xdxw0061DataRespDto = new Xdxw0061DataRespDto();// 响应Data：通过无还本续贷调查表编号查询配偶核心客户号
        Xdxw0061RespDto xdxw0061RespDto = new Xdxw0061RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdxw0061.req.Data reqData = null; // 请求Data：通过无还本续贷调查表编号查询配偶核心客户号
        cn.com.yusys.yusp.dto.server.biz.xdxw0061.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0061.resp.Data();// 响应Data：通过无还本续贷调查表编号查询配偶核心客户号
        try {
            // 从 xdxw0061ReqDto获取 reqData
            reqData = xdxw0061ReqDto.getData();
            // 将 reqData 拷贝到xdxw0061DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0061DataReqDto);

            ResultDto<Xdxw0061DataRespDto> xdxw0061DataResultDto = dscmsBizXwClientService.xdxw0061(xdxw0061DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdxw0061RespDto.setErorcd(Optional.ofNullable(xdxw0061DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0061RespDto.setErortx(Optional.ofNullable(xdxw0061DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdxw0061RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0061DataResultDto.getCode())) {
                xdxw0061RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0061RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0061DataRespDto = xdxw0061DataResultDto.getData();
                // 将 xdxw0061DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0061DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, e.getMessage());
            xdxw0061RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0061RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0061RespDto.setDatasq(servsq);
        xdxw0061RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0061.key, DscmsEnum.TRADE_CODE_XDXW0061.value, JSON.toJSONString(xdxw0061RespDto));
        return xdxw0061RespDto;
    }
}
