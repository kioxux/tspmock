package cn.com.yusys.yusp.web.client.esb.rircp.fbxd02;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02.Fbxd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02.Fbxd02RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.req.Fbxd02ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.resp.Fbxd02RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd02）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd02Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd02Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd02
     * 交易描述：为正式客户更新信贷客户信息手机号码
     *
     * @param fbxd02ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd02")
    protected @ResponseBody
    ResultDto<Fbxd02RespDto> fbxd02(@Validated @RequestBody Fbxd02ReqDto fbxd02ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD02.key, EsbEnum.TRADE_CODE_FBXD02.value, JSON.toJSONString(fbxd02ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.resp.Service();
        Fbxd02ReqService fbxd02ReqService = new Fbxd02ReqService();
        Fbxd02RespService fbxd02RespService = new Fbxd02RespService();
        Fbxd02RespDto fbxd02RespDto = new Fbxd02RespDto();
        ResultDto<Fbxd02RespDto> fbxd02ResultDto = new ResultDto<Fbxd02RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd02ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd02ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD02.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fbxd02ReqService.setService(reqService);
            // 将fbxd02ReqService转换成fbxd02ReqServiceMap
            Map fbxd02ReqServiceMap = beanMapUtil.beanToMap(fbxd02ReqService);
            context.put("tradeDataMap", fbxd02ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD02.key, EsbEnum.TRADE_CODE_FBXD02.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD02.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD02.key, EsbEnum.TRADE_CODE_FBXD02.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd02RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd02RespService.class, Fbxd02RespService.class);
            respService = fbxd02RespService.getService();

            fbxd02ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd02ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, fbxd02RespDto);
                fbxd02ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd02ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd02ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd02ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD02.key, EsbEnum.TRADE_CODE_FBXD02.value, e.getMessage());
            fbxd02ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd02ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd02ResultDto.setData(fbxd02RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD02.key, EsbEnum.TRADE_CODE_FBXD02.value, JSON.toJSONString(fbxd02ResultDto));
        return fbxd02ResultDto;
    }
}
