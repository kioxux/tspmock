package cn.com.yusys.yusp.web.server.biz.xdxw0039;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0039.req.Xdxw0039ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0039.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0039.resp.Xdxw0039RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0039.req.Xdxw0039DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0039.resp.Xdxw0039DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提交决议
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0039:提交决议")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0039Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0039Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0039
     * 交易描述：提交决议
     *
     * @param xdxw0039ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提交决议")
    @PostMapping("/xdxw0039")
    //@Idempotent({"xdcaxw0039", "#xdxw0039ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0039RespDto xdxw0039(@Validated @RequestBody Xdxw0039ReqDto xdxw0039ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039ReqDto));
        Xdxw0039DataReqDto xdxw0039DataReqDto = new Xdxw0039DataReqDto();// 请求Data： 提交决议
        Xdxw0039DataRespDto xdxw0039DataRespDto = new Xdxw0039DataRespDto();// 响应Data：提交决议
        cn.com.yusys.yusp.dto.server.biz.xdxw0039.req.Data reqData = null; // 请求Data：提交决议
        cn.com.yusys.yusp.dto.server.biz.xdxw0039.resp.Data respData = new Data();// 响应Data：提交决议
		Xdxw0039RespDto xdxw0039RespDto = new Xdxw0039RespDto();
		try {
            // 从 xdxw0039ReqDto获取 reqData
            reqData = xdxw0039ReqDto.getData();
            // 将 reqData 拷贝到xdxw0039DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0039DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039DataReqDto));
            ResultDto<Xdxw0039DataRespDto> xdxw0039DataResultDto = dscmsBizXwClientService.xdxw0039(xdxw0039DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0039RespDto.setErorcd(Optional.ofNullable(xdxw0039DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0039RespDto.setErortx(Optional.ofNullable(xdxw0039DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0039DataResultDto.getCode())) {
                xdxw0039DataRespDto = xdxw0039DataResultDto.getData();
                xdxw0039RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0039RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0039DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0039DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, e.getMessage());
            xdxw0039RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0039RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0039RespDto.setDatasq(servsq);

        xdxw0039RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0039.key, DscmsEnum.TRADE_CODE_XDXW0039.value, JSON.toJSONString(xdxw0039RespDto));
        return xdxw0039RespDto;
    }
}
