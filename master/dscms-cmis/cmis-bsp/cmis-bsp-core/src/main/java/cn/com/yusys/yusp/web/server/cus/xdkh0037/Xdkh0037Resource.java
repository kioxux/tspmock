package cn.com.yusys.yusp.web.server.cus.xdkh0037;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0037.req.Xdkh0037ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0037.resp.Data;
import cn.com.yusys.yusp.dto.server.cus.xdkh0037.resp.Xdkh0037RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0037.req.Xdkh0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0037.resp.Xdkh0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:微业贷新开户信息入库
 *
 * @author zdl
 * @version 1.0
 */
@Api(tags = "XDKH0037:微业贷新开户信息入库")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0037Resource.class);

	@Autowired
	private DscmsCusClientService dscmsCusClientService;
    /**
     * 交易码：xdkh0037
     * 交易描述：微业贷新开户信息入库
     *
     * @param xdkh0037ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("微业贷新开户信息入库")
    @PostMapping("/xdkh0037")
    //@Idempotent({"xdcakh0037", "#xdkh0037ReqDto.datasq"})
    protected @ResponseBody
	Xdkh0037RespDto xdkh0037(@Validated @RequestBody Xdkh0037ReqDto xdkh0037ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037ReqDto));
        Xdkh0037DataReqDto xdkh0037DataReqDto = new Xdkh0037DataReqDto();// 请求Data： 微业贷新开户信息入库
        Xdkh0037DataRespDto xdkh0037DataRespDto = new Xdkh0037DataRespDto();// 响应Data：微业贷新开户信息入库
        cn.com.yusys.yusp.dto.server.cus.xdkh0037.req.Data reqData = null; // 请求Data：微业贷新开户信息入库
        Data respData = new Data();// 响应Data：微业贷新开户信息入库
		Xdkh0037RespDto xdkh0037RespDto = new Xdkh0037RespDto();
		try {
            // 从 xdkh0037ReqDto获取 reqData
            reqData = xdkh0037ReqDto.getData();
            // 将 reqData 拷贝到xdkh0037DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0037DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037DataReqDto));
            ResultDto<Xdkh0037DataRespDto> xdkh0037DataResultDto = dscmsCusClientService.xdkh0037(xdkh0037DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0037RespDto.setErorcd(Optional.ofNullable(xdkh0037DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0037RespDto.setErortx(Optional.ofNullable(xdkh0037DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0037DataResultDto.getCode())) {
                xdkh0037DataRespDto = xdkh0037DataResultDto.getData();
                xdkh0037RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0037RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0037DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0037DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, e.getMessage());
            xdkh0037RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0037RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0037RespDto.setDatasq(servsq);

        xdkh0037RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0037.key, DscmsEnum.TRADE_CODE_XDKH0037.value, JSON.toJSONString(xdkh0037RespDto));
        return xdkh0037RespDto;
    }
}
