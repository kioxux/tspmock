package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询批次出账票据信息（新信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
public class Service {

    private String erortx;//交易返回信息
    private String erorcd;//交易返回代码
    private List list;//list
    private Integer totalRecordNum;//总数据
    private BigDecimal totleAmount;//总金额

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public Integer getTotalRecordNum() {
        return totalRecordNum;
    }

    public void setTotalRecordNum(Integer totalRecordNum) {
        this.totalRecordNum = totalRecordNum;
    }

    public BigDecimal getTotleAmount() {
        return totleAmount;
    }

    public void setTotleAmount(BigDecimal totleAmount) {
        this.totleAmount = totleAmount;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erortx='" + erortx + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", list=" + list +
                ", totalRecordNum=" + totalRecordNum +
                ", totleAmount=" + totleAmount +
                '}';
    }
}
