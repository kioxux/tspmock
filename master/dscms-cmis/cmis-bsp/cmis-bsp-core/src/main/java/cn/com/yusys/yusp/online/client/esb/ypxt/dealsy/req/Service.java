package cn.com.yusys.yusp.online.client.esb.ypxt.dealsy.req;

import java.math.BigDecimal;

/**
 * 请求Service：押品处置信息同步
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String sernum;//流水号
    private String yptybh;//押品统一编号
    private String ypczfs;//押品处置方式
    private String xjhslx;//现金回收来源类型
    private String xjhsbh;//现金回收来源编号
    private String dzzcbh;//抵债资产编号
    private String dzzclx;//抵债资产类型
    private BigDecimal yzdzje;//以资抵债金额
    private BigDecimal czhsze;//处置回收金额总额
    private String czhslx;//处置回收金额类型
    private BigDecimal czhsje;//处置回收金额
    private String isflag;//是否处置完毕
    private BigDecimal zjfyze;//处置直接费用总额
    private String zjfylx;//处置直接费用类型
    private BigDecimal zjfyje;//处置直接费用金额
    private String czrqyp;//处置日期
    private String remark;//备注
    private String jbrbhy;//经办人
    private String jbjgyp;//经办机构
    private String jbrqyp;//经办日期
    private String operat;//操作

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getSernum() {
        return sernum;
    }

    public void setSernum(String sernum) {
        this.sernum = sernum;
    }

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getYpczfs() {
        return ypczfs;
    }

    public void setYpczfs(String ypczfs) {
        this.ypczfs = ypczfs;
    }

    public String getXjhslx() {
        return xjhslx;
    }

    public void setXjhslx(String xjhslx) {
        this.xjhslx = xjhslx;
    }

    public String getXjhsbh() {
        return xjhsbh;
    }

    public void setXjhsbh(String xjhsbh) {
        this.xjhsbh = xjhsbh;
    }

    public String getDzzcbh() {
        return dzzcbh;
    }

    public void setDzzcbh(String dzzcbh) {
        this.dzzcbh = dzzcbh;
    }

    public String getDzzclx() {
        return dzzclx;
    }

    public void setDzzclx(String dzzclx) {
        this.dzzclx = dzzclx;
    }

    public BigDecimal getYzdzje() {
        return yzdzje;
    }

    public void setYzdzje(BigDecimal yzdzje) {
        this.yzdzje = yzdzje;
    }

    public BigDecimal getCzhsze() {
        return czhsze;
    }

    public void setCzhsze(BigDecimal czhsze) {
        this.czhsze = czhsze;
    }

    public String getCzhslx() {
        return czhslx;
    }

    public void setCzhslx(String czhslx) {
        this.czhslx = czhslx;
    }

    public BigDecimal getCzhsje() {
        return czhsje;
    }

    public void setCzhsje(BigDecimal czhsje) {
        this.czhsje = czhsje;
    }

    public String getIsflag() {
        return isflag;
    }

    public void setIsflag(String isflag) {
        this.isflag = isflag;
    }

    public BigDecimal getZjfyze() {
        return zjfyze;
    }

    public void setZjfyze(BigDecimal zjfyze) {
        this.zjfyze = zjfyze;
    }

    public String getZjfylx() {
        return zjfylx;
    }

    public void setZjfylx(String zjfylx) {
        this.zjfylx = zjfylx;
    }

    public BigDecimal getZjfyje() {
        return zjfyje;
    }

    public void setZjfyje(BigDecimal zjfyje) {
        this.zjfyje = zjfyje;
    }

    public String getCzrqyp() {
        return czrqyp;
    }

    public void setCzrqyp(String czrqyp) {
        this.czrqyp = czrqyp;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getJbrbhy() {
        return jbrbhy;
    }

    public void setJbrbhy(String jbrbhy) {
        this.jbrbhy = jbrbhy;
    }

    public String getJbjgyp() {
        return jbjgyp;
    }

    public void setJbjgyp(String jbjgyp) {
        this.jbjgyp = jbjgyp;
    }

    public String getJbrqyp() {
        return jbrqyp;
    }

    public void setJbrqyp(String jbrqyp) {
        this.jbrqyp = jbrqyp;
    }

    public String getOperat() {
        return operat;
    }

    public void setOperat(String operat) {
        this.operat = operat;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "sernum='" + sernum + '\'' +
                "yptybh='" + yptybh + '\'' +
                "ypczfs='" + ypczfs + '\'' +
                "xjhslx='" + xjhslx + '\'' +
                "xjhsbh='" + xjhsbh + '\'' +
                "dzzcbh='" + dzzcbh + '\'' +
                "dzzclx='" + dzzclx + '\'' +
                "yzdzje='" + yzdzje + '\'' +
                "czhsze='" + czhsze + '\'' +
                "czhslx='" + czhslx + '\'' +
                "czhsje='" + czhsje + '\'' +
                "isflag='" + isflag + '\'' +
                "zjfyze='" + zjfyze + '\'' +
                "zjfylx='" + zjfylx + '\'' +
                "zjfyje='" + zjfyje + '\'' +
                "czrqyp='" + czrqyp + '\'' +
                "remark='" + remark + '\'' +
                "jbrbhy='" + jbrbhy + '\'' +
                "jbjgyp='" + jbjgyp + '\'' +
                "jbrqyp='" + jbrqyp + '\'' +
                "operat='" + operat + '\'' +
                '}';
    }
}
