package cn.com.yusys.yusp.online.client.esb.xwh.xwh003.resp;

/**
 * 响应Service：核销锁定接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwh003RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwh003RespService{" +
                "service=" + service +
                '}';
    }
}
