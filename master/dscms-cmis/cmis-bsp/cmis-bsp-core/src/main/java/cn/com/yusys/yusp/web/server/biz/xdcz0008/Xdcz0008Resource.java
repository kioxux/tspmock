package cn.com.yusys.yusp.web.server.biz.xdcz0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0008.req.Xdcz0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0008.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0008.resp.Xdcz0008RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0008.req.Xdcz0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0008.resp.Xdcz0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:代开保函
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0008:代开保函")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0008Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0008
     * 交易描述：代开保函
     *
     * @param xdcz0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("代开保函")
    @PostMapping("/xdcz0008")
//   @Idempotent({"xdcz0008", "#xdcz0008ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0008RespDto xdcz0008(@Validated @RequestBody Xdcz0008ReqDto xdcz0008ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, JSON.toJSONString(xdcz0008ReqDto));
        Xdcz0008DataReqDto xdcz0008DataReqDto = new Xdcz0008DataReqDto();// 请求Data： 电子保函开立
        Xdcz0008DataRespDto xdcz0008DataRespDto = new Xdcz0008DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0008.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0008RespDto xdcz0008RespDto = new Xdcz0008RespDto();
        try {
            // 从 xdcz0008ReqDto获取 reqData
            reqData = xdcz0008ReqDto.getData();
            // 将 reqData 拷贝到xdcz0008DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0008DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, JSON.toJSONString(xdcz0008DataReqDto));
            ResultDto<Xdcz0008DataRespDto> xdcz0008DataResultDto = dscmsBizCzClientService.xdcz0008(xdcz0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, JSON.toJSONString(xdcz0008DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0008RespDto.setErorcd(Optional.ofNullable(xdcz0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0008RespDto.setErortx(Optional.ofNullable(xdcz0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0008DataResultDto.getCode())) {
                xdcz0008DataRespDto = xdcz0008DataResultDto.getData();
                xdcz0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, e.getMessage());
            xdcz0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0008RespDto.setDatasq(servsq);

        xdcz0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0008.key, DscmsEnum.TRADE_CODE_XDCZ0008.value, JSON.toJSONString(xdcz0008RespDto));
        return xdcz0008RespDto;
    }
}
