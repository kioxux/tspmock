package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj21;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.req.Xdpj21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp.Xdpj21RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.req.Xdpj21ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.Record;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.Xdpj21RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:信贷签约通知
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj21Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj21Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdpj21
     * 交易描述：信贷签约通知
     *
     * @param xdpj21ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdpj21")
    protected @ResponseBody
    ResultDto<Xdpj21RespDto> xdpj21(@Validated @RequestBody Xdpj21ReqDto xdpj21ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ21.key, EsbEnum.TRADE_CODE_XDPJ21.value, JSON.toJSONString(xdpj21ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.req.Service();
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.Service();
        Xdpj21ReqService xdpj21ReqService = new Xdpj21ReqService();
        Xdpj21RespService xdpj21RespService = new Xdpj21RespService();
        Xdpj21RespDto xdpj21RespDto = new Xdpj21RespDto();
        ResultDto<Xdpj21RespDto> xdpj21ResultDto = new ResultDto<Xdpj21RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdpj21ReqDto转换成reqService
            BeanUtils.copyProperties(xdpj21ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ21.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            xdpj21ReqService.setService(reqService);
            // 将xdpj21ReqService转换成xdpj21ReqServiceMap
            Map xdpj21ReqServiceMap = beanMapUtil.beanToMap(xdpj21ReqService);
            context.put("tradeDataMap", xdpj21ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ21.key, EsbEnum.TRADE_CODE_XDPJ21.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ21.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ21.key, EsbEnum.TRADE_CODE_XDPJ21.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdpj21RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj21RespService.class, Xdpj21RespService.class);
            respService = xdpj21RespService.getService();

            xdpj21ResultDto.setCode(Optional.ofNullable(respService.getRetcod()).orElse(SuccessEnum.SUCCESS.key));
            xdpj21ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getRetcod())) {
                //  将respService转换成Xdpj21RespDto
                BeanUtils.copyProperties(respService, xdpj21RespDto);
                List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp.List> targetList = new ArrayList<>();
                cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.List serviceList = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.List());
                if (CollectionUtils.nonEmpty(serviceList.getRecordList())) {
                    List<Record> recordList = respService.getList().getRecordList();
                    cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp.List target = new cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp.List();
                    for (Record record : recordList) {
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    xdpj21RespDto.setList(targetList);
                }

                xdpj21ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdpj21ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdpj21ResultDto.setCode(EpbEnum.EPB099999.key);
                xdpj21ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ21.key, EsbEnum.TRADE_CODE_XDPJ21.value, e.getMessage());
            xdpj21ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdpj21ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdpj21ResultDto.setData(xdpj21RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ21.key, EsbEnum.TRADE_CODE_XDPJ21.value, JSON.toJSONString(xdpj21ResultDto));
        return xdpj21ResultDto;
    }
}
