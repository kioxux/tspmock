package cn.com.yusys.yusp.web.client.esb.fxyjxt.djhhmd;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd.DjhhmdReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd.DjhhmdRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.req.DjhhmdReqService;
import cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.resp.DjhhmdRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:BSP封装调用客户风险预警系统的接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "BSP封装调用客户风险预警系统的接口处理类(djhhmd)")
@RestController
@RequestMapping("/api/dscms2fxyjxt")
public class Dscms2DjhhmdResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2DjhhmdResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * 交易码：djhhmd
     * 交易描述：查询客户风险预警等级与是否黑灰名单
     *
     * @param djhhmdReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("djhhmd:查询客户风险预警等级与是否黑灰名单")
    @PostMapping("/djhhmd")
    protected @ResponseBody
    ResultDto<DjhhmdRespDto> djhhmd(@Validated @RequestBody DjhhmdReqDto djhhmdReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJHHMD.key, EsbEnum.TRADE_CODE_DJHHMD.value, JSON.toJSONString(djhhmdReqDto));
        cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.req.Service();
        cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.resp.Service();
        DjhhmdReqService djhhmdReqService = new DjhhmdReqService();
        DjhhmdRespService djhhmdRespService = new DjhhmdRespService();
        DjhhmdRespDto djhhmdRespDto = new DjhhmdRespDto();
        ResultDto<DjhhmdRespDto> djhhmdResultDto = new ResultDto<DjhhmdRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将djhhmdReqDto转换成reqService
            BeanUtils.copyProperties(djhhmdReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DJHHMD.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_FXYJXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_FXYJXT.key);//    部门号
            djhhmdReqService.setService(reqService);
            // 将djhhmdReqService转换成djhhmdReqServiceMap
            Map djhhmdReqServiceMap = beanMapUtil.beanToMap(djhhmdReqService);
            context.put("tradeDataMap", djhhmdReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJHHMD.key, EsbEnum.TRADE_CODE_DJHHMD.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DJHHMD.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJHHMD.key, EsbEnum.TRADE_CODE_DJHHMD.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            djhhmdRespService = beanMapUtil.mapToBean(tradeDataMap, DjhhmdRespService.class, DjhhmdRespService.class);
            respService = djhhmdRespService.getService();

            djhhmdResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            djhhmdResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成DjhhmdRespDto
                BeanUtils.copyProperties(respService, djhhmdRespDto);
                djhhmdResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                djhhmdResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                djhhmdResultDto.setCode(EpbEnum.EPB099999.key);
                djhhmdResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJHHMD.key, EsbEnum.TRADE_CODE_DJHHMD.value, e.getMessage());
            djhhmdResultDto.setCode(EpbEnum.EPB099999.key);//9999
            djhhmdResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        djhhmdResultDto.setData(djhhmdRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJHHMD.key, EsbEnum.TRADE_CODE_DJHHMD.value, JSON.toJSONString(djhhmdResultDto));
        return djhhmdResultDto;
    }
}
