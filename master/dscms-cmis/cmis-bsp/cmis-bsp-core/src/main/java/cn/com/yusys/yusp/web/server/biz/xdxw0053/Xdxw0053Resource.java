package cn.com.yusys.yusp.web.server.biz.xdxw0053;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0053.req.Xdxw0053ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0053.resp.Xdxw0053RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0053.req.Xdxw0053DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0053.resp.Xdxw0053DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询经营性贷款客户基本信息
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDXW0053:查询经营性贷款客户基本信息")
@RestController
@RequestMapping("/api/dscms")
public class    Xdxw0053Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0053Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0053
     * 交易描述：查询经营性贷款客户基本信息
     *
     * @param xdxw0053ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询经营性贷款客户基本信息")
    @PostMapping("/xdxw0053")
    //@Idempotent({"xdcaxw0053", "#xdxw0053ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0053RespDto xdxw0053(@Validated @RequestBody Xdxw0053ReqDto xdxw0053ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(xdxw0053ReqDto));
        Xdxw0053DataReqDto xdxw0053DataReqDto = new Xdxw0053DataReqDto();// 请求Data： 查询经营性贷款客户基本信息
        Xdxw0053DataRespDto xdxw0053DataRespDto = new Xdxw0053DataRespDto();// 响应Data：查询经营性贷款客户基本信息
        Xdxw0053RespDto xdxw0053RespDto = new Xdxw0053RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0053.req.Data reqData = null; // 请求Data：查询经营性贷款客户基本信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0053.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0053.resp.Data();// 响应Data：查询经营性贷款客户基本信息
        try {
            // 从 xdxw0053ReqDto获取 reqData
            reqData = xdxw0053ReqDto.getData();
            // 将 reqData 拷贝到xdxw0053DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0053DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(xdxw0053DataReqDto));
            ResultDto<Xdxw0053DataRespDto> xdxw0053DataResultDto = dscmsBizXwClientService.xdxw0053(xdxw0053DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(xdxw0053DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0053RespDto.setErorcd(Optional.ofNullable(xdxw0053DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0053RespDto.setErortx(Optional.ofNullable(xdxw0053DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0053DataResultDto.getCode())) {
                xdxw0053RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0053RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0053DataRespDto = xdxw0053DataResultDto.getData();
                // 将 xdxw0053DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0053DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, e.getMessage());
            xdxw0053RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0053RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0053RespDto.setDatasq(servsq);
        xdxw0053RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0053.key, DscmsEnum.TRADE_CODE_XDXW0053.value, JSON.toJSONString(xdxw0053RespDto));
        return xdxw0053RespDto;
    }
}
