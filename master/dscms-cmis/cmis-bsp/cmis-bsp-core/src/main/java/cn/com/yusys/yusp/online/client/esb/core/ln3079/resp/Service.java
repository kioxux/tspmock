package cn.com.yusys.yusp.online.client.esb.core.ln3079.resp;

/**
 * 响应Service：贷款产品变更
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String daikczbz;//业务操作标志
    private String dkzhangh;//贷款账号
    private String xinchpdm;//新产品代码
    private String xinchpmc;//新产品名称
    private String dkjiejuh;//贷款借据号
    private String kehuhaoo;//客户号
    private String hetongbh;//合同编号
    private String kehmingc;//客户名称
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String kuaijilb;//会计类别
    private String jiaoyigy;//交易柜员
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getXinchpdm() {
        return xinchpdm;
    }

    public void setXinchpdm(String xinchpdm) {
        this.xinchpdm = xinchpdm;
    }

    public String getXinchpmc() {
        return xinchpmc;
    }

    public void setXinchpmc(String xinchpmc) {
        this.xinchpmc = xinchpmc;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", xinchpdm='" + xinchpdm + '\'' +
                ", xinchpmc='" + xinchpmc + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", kuaijilb='" + kuaijilb + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}
