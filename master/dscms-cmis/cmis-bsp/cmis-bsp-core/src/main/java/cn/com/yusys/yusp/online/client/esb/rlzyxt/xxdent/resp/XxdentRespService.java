package cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.resp;

/**
 * 响应Service：新入职人员信息登记
 *
 * @author leehuang
 * @version 1.0
 */
public class XxdentRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "XxdentRespService{" +
                "service=" + service +
                '}';
    }
}
