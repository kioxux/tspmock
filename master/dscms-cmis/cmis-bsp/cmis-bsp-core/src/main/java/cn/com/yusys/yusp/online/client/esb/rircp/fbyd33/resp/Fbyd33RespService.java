package cn.com.yusys.yusp.online.client.esb.rircp.fbyd33.resp;

/**
 * 响应Service：预授信申请提交
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbyd33RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Fbyd33RespService{" +
				"service=" + service +
				'}';
	}
}
