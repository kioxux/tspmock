package cn.com.yusys.yusp.online.client.esb.ypxt.businf.resp;

/**
 * 响应Service：信贷业务合同信息同步
 * @author lihh
 * @version 1.0             
 */      
public class BusinfRespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
