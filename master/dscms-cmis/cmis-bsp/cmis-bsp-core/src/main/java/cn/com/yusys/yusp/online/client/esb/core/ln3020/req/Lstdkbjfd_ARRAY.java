package cn.com.yusys.yusp.online.client.esb.core.ln3020.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkbjfd.Record;

import java.util.List;

/**
 * 请求Service：本金分段登记
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Lstdkbjfd_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkbjfd.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkbjfd_ARRAY{" +
                "record=" + record +
                '}';
    }
}