package cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.resp;

/**
 * 响应Service：协议信息查询
 *
 * @author code-generator
 * @version 1.0
 */
public class ClfxcxRespService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}                      
