package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.req;

/**
 * 请求Service：风险拦截截接口
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号

    private String cus_name;//客户名称
    private String cert_no;//证件号
    private String cus_mgr_no;//客户经理工号
    private String check_type;//校验类型
    private String cus_mgr_name;//客户经理名称

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public String getCus_mgr_no() {
        return cus_mgr_no;
    }

    public void setCus_mgr_no(String cus_mgr_no) {
        this.cus_mgr_no = cus_mgr_no;
    }

    public String getCheck_type() {
        return check_type;
    }

    public void setCheck_type(String check_type) {
        this.check_type = check_type;
    }

    public String getCus_mgr_name() {
        return cus_mgr_name;
    }

    public void setCus_mgr_name(String cus_mgr_name) {
        this.cus_mgr_name = cus_mgr_name;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cert_no='" + cert_no + '\'' +
                ", cus_mgr_no='" + cus_mgr_no + '\'' +
                ", check_type='" + check_type + '\'' +
                ", cus_mgr_name='" + cus_mgr_name + '\'' +
                '}';
    }
}
