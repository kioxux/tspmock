package cn.com.yusys.yusp.web.server.cus.xdkh0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0008.req.Xdkh0008ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0008.resp.Xdkh0008RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0008.req.Xdkh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0008.resp.Xdkh0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;
/**
 * 接口处理类:集团关联信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0008:集团关联信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0008Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0008
     * 交易描述：集团关联信息查询
     *
     * @param xdkh0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("集团关联信息查询")
    @PostMapping("/xdkh0008")
    //@Idempotent({"xdcakh0008", "#xdkh0008ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0008RespDto xdkh0008(@Validated @RequestBody Xdkh0008ReqDto xdkh0008ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008ReqDto));
        Xdkh0008DataReqDto xdkh0008DataReqDto = new Xdkh0008DataReqDto();// 请求Data： 集团关联信息查询
        Xdkh0008DataRespDto xdkh0008DataRespDto = new Xdkh0008DataRespDto();// 响应Data：集团关联信息查询
        Xdkh0008RespDto xdkh0008RespDto = new Xdkh0008RespDto();// 响应DTO：集团关联信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0008.req.Data reqData = null; // 请求Data：集团关联信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0008.resp.Data();// 响应Data：集团关联信息查询
        try {
            // 从 xdkh0008ReqDto获取 reqData
            reqData = xdkh0008ReqDto.getData();
            // 将 reqData 拷贝到xdkh0008DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0008DataReqDto);
            // 调用服务：cmis_cus
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008ReqDto));
            ResultDto<Xdkh0008DataRespDto> xdkh0008DataResultDto = dscmsCusClientService.xdkh0008(xdkh0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0008RespDto.setErorcd(Optional.ofNullable(xdkh0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0008RespDto.setErortx(Optional.ofNullable(xdkh0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0008DataResultDto.getCode())) {
                xdkh0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0008DataRespDto = xdkh0008DataResultDto.getData();
                // 将 xdkh0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, e.getMessage());
            xdkh0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0008RespDto.setDatasq(servsq);

        xdkh0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0008.key, DscmsEnum.TRADE_CODE_XDKH0008.value, JSON.toJSONString(xdkh0008RespDto));
        return xdkh0008RespDto;
    }
}
