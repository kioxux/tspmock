package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.req;

/**
 * 请求Service：利率申请接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwd009ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwd009ReqService{" +
                "service=" + service +
                '}';
    }
}
