package cn.com.yusys.yusp.web.server.biz.xdcz0028;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0028.req.Xdcz0028ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0028.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0028.resp.Xdcz0028RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0028.req.Xdcz0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0028.resp.Xdcz0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:生成商贷账号数据文件
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0028:生成商贷账号数据文件")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0028Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0028
     * 交易描述：生成商贷账号数据文件
     *
     * @param xdcz0028ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("生成商贷账号数据文件")
    @PostMapping("/xdcz0028")
//    @Idempotent({"xdcz0028", "#xdcz0028ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0028RespDto xdcz0028(@Validated @RequestBody Xdcz0028ReqDto xdcz0028ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, JSON.toJSONString(xdcz0028ReqDto));
        Xdcz0028DataReqDto xdcz0028DataReqDto = new Xdcz0028DataReqDto();// 请求Data： 生成商贷账号数据文件
        Xdcz0028DataRespDto xdcz0028DataRespDto = new Xdcz0028DataRespDto();// 响应Data：生成商贷账号数据文件

        cn.com.yusys.yusp.dto.server.biz.xdcz0028.req.Data reqData = null; // 请求Data：生成商贷账号数据文件
        Data respData = new Data();// 响应Data：生成商贷账号数据文件

        Xdcz0028RespDto xdcz0028RespDto = new Xdcz0028RespDto();
        try {
            // 从 xdcz0028ReqDto获取 reqData
            reqData = xdcz0028ReqDto.getData();
            // 将 reqData 拷贝到xdcz0028DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0028DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, JSON.toJSONString(xdcz0028DataReqDto));
            ResultDto<Xdcz0028DataRespDto> xdcz0028DataResultDto = dscmsBizCzClientService.xdcz0028(xdcz0028DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, JSON.toJSONString(xdcz0028DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0028RespDto.setErorcd(Optional.ofNullable(xdcz0028DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0028RespDto.setErortx(Optional.ofNullable(xdcz0028DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0028DataResultDto.getCode())) {
                xdcz0028DataRespDto = xdcz0028DataResultDto.getData();
                xdcz0028RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0028RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0028DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0028DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, e.getMessage());
            xdcz0028RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0028RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0028RespDto.setDatasq(servsq);

        xdcz0028RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0028.key, DscmsEnum.TRADE_CODE_XDCZ0028.value, JSON.toJSONString(xdcz0028RespDto));
        return xdcz0028RespDto;
    }
}
