package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 司法协助详情
 */
@JsonPropertyOrder(alphabetic = true)
public class JUDICIALAIDDETAIL implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CFPERIOD")
    private String CFPERIOD;//	续行冻结期限
    @JsonProperty(value = "CFROFROM")
    private String CFROFROM;//	续行冻结期限自
    @JsonProperty(value = "CFROTO")
    private String CFROTO;//	续行冻结期限至
    @JsonProperty(value = "COURTNAME")
    private String COURTNAME;//	执行法院
    @JsonProperty(value = "CUR")
    private String CUR;//	币种
    @JsonProperty(value = "EXECUTION")
    private String EXECUTION;//	执行事项
    @JsonProperty(value = "EXPIRATION_DATE")
    private String EXPIRATION_DATE;//	失效日期
    @JsonProperty(value = "EXPIRATION_REASON")
    private String EXPIRATION_REASON;//	失效原因
    @JsonProperty(value = "EX_NOTICE_NO")
    private String EX_NOTICE_NO;//	协助执行通知书文号
    @JsonProperty(value = "FPERIOD")
    private String FPERIOD;//	冻结期限
    @JsonProperty(value = "FREEZE_DATE")
    private String FREEZE_DATE;//	解冻日期
    @JsonProperty(value = "FREEZE_FLAG")
    private String FREEZE_FLAG;//	股权冻结状态
    @JsonProperty(value = "FROFROM")
    private String FROFROM;//	冻结期限自
    @JsonProperty(value = "FROTO")
    private String FROTO;//	冻结期限至
    @JsonProperty(value = "ID_TYPE")
    private String ID_TYPE;//	证件类型
    @JsonProperty(value = "INAME")
    private String INAME;//	被执行人
    @JsonProperty(value = "INAME_TYPE")
    private String INAME_TYPE;//	被执行人类型
    @JsonProperty(value = "JUDGMENT_NO")
    private String JUDGMENT_NO;//	执行裁定书文号
    @JsonProperty(value = "LICENCE_NO")
    private String LICENCE_NO;//	证照编号
    @JsonProperty(value = "LICENCE_TYPE")
    private String LICENCE_TYPE;//	证照类型
    @JsonProperty(value = "MARKET_CREDIT_CODE")
    private String MARKET_CREDIT_CODE;//	被冻结股权所在市场统一社会信用代码
    @JsonProperty(value = "MARKET_NAME")
    private String MARKET_NAME;//	被冻结股权所在市场主体名称
    @JsonProperty(value = "PARENT_ID")
    private String PARENT_ID;//	被执行人ID
    @JsonProperty(value = "PUBLICDATE")
    private String PUBLICDATE;//	公示日期
    @JsonProperty(value = "REGIST_NO")
    private String REGIST_NO;//	被冻结股权所在市场主体注册号
    @JsonProperty(value = "SHAREAM")
    private String SHAREAM;//	股权数额
    @JsonProperty(value = "SHAREAM_UNIT")
    private String SHAREAM_UNIT;//	股权数额单位

    @JsonIgnore
    public String getCFPERIOD() {
        return CFPERIOD;
    }

    @JsonIgnore
    public void setCFPERIOD(String CFPERIOD) {
        this.CFPERIOD = CFPERIOD;
    }

    @JsonIgnore
    public String getCFROFROM() {
        return CFROFROM;
    }

    @JsonIgnore
    public void setCFROFROM(String CFROFROM) {
        this.CFROFROM = CFROFROM;
    }

    @JsonIgnore
    public String getCFROTO() {
        return CFROTO;
    }

    @JsonIgnore
    public void setCFROTO(String CFROTO) {
        this.CFROTO = CFROTO;
    }

    @JsonIgnore
    public String getCOURTNAME() {
        return COURTNAME;
    }

    @JsonIgnore
    public void setCOURTNAME(String COURTNAME) {
        this.COURTNAME = COURTNAME;
    }

    @JsonIgnore
    public String getCUR() {
        return CUR;
    }

    @JsonIgnore
    public void setCUR(String CUR) {
        this.CUR = CUR;
    }

    @JsonIgnore
    public String getEXECUTION() {
        return EXECUTION;
    }

    @JsonIgnore
    public void setEXECUTION(String EXECUTION) {
        this.EXECUTION = EXECUTION;
    }

    @JsonIgnore
    public String getEXPIRATION_DATE() {
        return EXPIRATION_DATE;
    }

    @JsonIgnore
    public void setEXPIRATION_DATE(String EXPIRATION_DATE) {
        this.EXPIRATION_DATE = EXPIRATION_DATE;
    }

    @JsonIgnore
    public String getEXPIRATION_REASON() {
        return EXPIRATION_REASON;
    }

    @JsonIgnore
    public void setEXPIRATION_REASON(String EXPIRATION_REASON) {
        this.EXPIRATION_REASON = EXPIRATION_REASON;
    }

    @JsonIgnore
    public String getEX_NOTICE_NO() {
        return EX_NOTICE_NO;
    }

    @JsonIgnore
    public void setEX_NOTICE_NO(String EX_NOTICE_NO) {
        this.EX_NOTICE_NO = EX_NOTICE_NO;
    }

    @JsonIgnore
    public String getFPERIOD() {
        return FPERIOD;
    }

    @JsonIgnore
    public void setFPERIOD(String FPERIOD) {
        this.FPERIOD = FPERIOD;
    }

    @JsonIgnore
    public String getFREEZE_DATE() {
        return FREEZE_DATE;
    }

    @JsonIgnore
    public void setFREEZE_DATE(String FREEZE_DATE) {
        this.FREEZE_DATE = FREEZE_DATE;
    }

    @JsonIgnore
    public String getFREEZE_FLAG() {
        return FREEZE_FLAG;
    }

    @JsonIgnore
    public void setFREEZE_FLAG(String FREEZE_FLAG) {
        this.FREEZE_FLAG = FREEZE_FLAG;
    }

    @JsonIgnore
    public String getFROFROM() {
        return FROFROM;
    }

    @JsonIgnore
    public void setFROFROM(String FROFROM) {
        this.FROFROM = FROFROM;
    }

    @JsonIgnore
    public String getFROTO() {
        return FROTO;
    }

    @JsonIgnore
    public void setFROTO(String FROTO) {
        this.FROTO = FROTO;
    }

    @JsonIgnore
    public String getID_TYPE() {
        return ID_TYPE;
    }

    @JsonIgnore
    public void setID_TYPE(String ID_TYPE) {
        this.ID_TYPE = ID_TYPE;
    }

    @JsonIgnore
    public String getINAME() {
        return INAME;
    }

    @JsonIgnore
    public void setINAME(String INAME) {
        this.INAME = INAME;
    }

    @JsonIgnore
    public String getINAME_TYPE() {
        return INAME_TYPE;
    }

    @JsonIgnore
    public void setINAME_TYPE(String INAME_TYPE) {
        this.INAME_TYPE = INAME_TYPE;
    }

    @JsonIgnore
    public String getJUDGMENT_NO() {
        return JUDGMENT_NO;
    }

    @JsonIgnore
    public void setJUDGMENT_NO(String JUDGMENT_NO) {
        this.JUDGMENT_NO = JUDGMENT_NO;
    }

    @JsonIgnore
    public String getLICENCE_NO() {
        return LICENCE_NO;
    }

    @JsonIgnore
    public void setLICENCE_NO(String LICENCE_NO) {
        this.LICENCE_NO = LICENCE_NO;
    }

    @JsonIgnore
    public String getLICENCE_TYPE() {
        return LICENCE_TYPE;
    }

    @JsonIgnore
    public void setLICENCE_TYPE(String LICENCE_TYPE) {
        this.LICENCE_TYPE = LICENCE_TYPE;
    }

    @JsonIgnore
    public String getMARKET_CREDIT_CODE() {
        return MARKET_CREDIT_CODE;
    }

    @JsonIgnore
    public void setMARKET_CREDIT_CODE(String MARKET_CREDIT_CODE) {
        this.MARKET_CREDIT_CODE = MARKET_CREDIT_CODE;
    }

    @JsonIgnore
    public String getMARKET_NAME() {
        return MARKET_NAME;
    }

    @JsonIgnore
    public void setMARKET_NAME(String MARKET_NAME) {
        this.MARKET_NAME = MARKET_NAME;
    }

    @JsonIgnore
    public String getPARENT_ID() {
        return PARENT_ID;
    }

    @JsonIgnore
    public void setPARENT_ID(String PARENT_ID) {
        this.PARENT_ID = PARENT_ID;
    }

    @JsonIgnore
    public String getPUBLICDATE() {
        return PUBLICDATE;
    }

    @JsonIgnore
    public void setPUBLICDATE(String PUBLICDATE) {
        this.PUBLICDATE = PUBLICDATE;
    }

    @JsonIgnore
    public String getREGIST_NO() {
        return REGIST_NO;
    }

    @JsonIgnore
    public void setREGIST_NO(String REGIST_NO) {
        this.REGIST_NO = REGIST_NO;
    }

    @JsonIgnore
    public String getSHAREAM() {
        return SHAREAM;
    }

    @JsonIgnore
    public void setSHAREAM(String SHAREAM) {
        this.SHAREAM = SHAREAM;
    }

    @JsonIgnore
    public String getSHAREAM_UNIT() {
        return SHAREAM_UNIT;
    }

    @JsonIgnore
    public void setSHAREAM_UNIT(String SHAREAM_UNIT) {
        this.SHAREAM_UNIT = SHAREAM_UNIT;
    }

    @Override
    public String toString() {
        return "JUDICIALAIDDETAIL{" +
                "CFPERIOD='" + CFPERIOD + '\'' +
                ", CFROFROM='" + CFROFROM + '\'' +
                ", CFROTO='" + CFROTO + '\'' +
                ", COURTNAME='" + COURTNAME + '\'' +
                ", CUR='" + CUR + '\'' +
                ", EXECUTION='" + EXECUTION + '\'' +
                ", EXPIRATION_DATE='" + EXPIRATION_DATE + '\'' +
                ", EXPIRATION_REASON='" + EXPIRATION_REASON + '\'' +
                ", EX_NOTICE_NO='" + EX_NOTICE_NO + '\'' +
                ", FPERIOD='" + FPERIOD + '\'' +
                ", FREEZE_DATE='" + FREEZE_DATE + '\'' +
                ", FREEZE_FLAG='" + FREEZE_FLAG + '\'' +
                ", FROFROM='" + FROFROM + '\'' +
                ", FROTO='" + FROTO + '\'' +
                ", ID_TYPE='" + ID_TYPE + '\'' +
                ", INAME='" + INAME + '\'' +
                ", INAME_TYPE='" + INAME_TYPE + '\'' +
                ", JUDGMENT_NO='" + JUDGMENT_NO + '\'' +
                ", LICENCE_NO='" + LICENCE_NO + '\'' +
                ", LICENCE_TYPE='" + LICENCE_TYPE + '\'' +
                ", MARKET_CREDIT_CODE='" + MARKET_CREDIT_CODE + '\'' +
                ", MARKET_NAME='" + MARKET_NAME + '\'' +
                ", PARENT_ID='" + PARENT_ID + '\'' +
                ", PUBLICDATE='" + PUBLICDATE + '\'' +
                ", REGIST_NO='" + REGIST_NO + '\'' +
                ", SHAREAM='" + SHAREAM + '\'' +
                ", SHAREAM_UNIT='" + SHAREAM_UNIT + '\'' +
                '}';
    }
}
