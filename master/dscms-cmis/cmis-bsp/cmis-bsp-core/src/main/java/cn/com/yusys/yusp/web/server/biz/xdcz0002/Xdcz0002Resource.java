package cn.com.yusys.yusp.web.server.biz.xdcz0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0002.req.Xdcz0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0002.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0002.resp.Xdcz0002RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0002.req.Xdcz0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0002.resp.Xdcz0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:电子保函注销
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0002:电子保函注销")
@RestController
// TODO /api/tradesystem 待确认
@RequestMapping("/api/dscms")
public class Xdcz0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.biz.xdcz0002.Xdcz0002Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0002
     * 交易描述：电子保函注销
     *
     * @param xdcz0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("电子保函开立")
    @PostMapping("/xdcz0002")
//   @Idempotent({"xdcz0002", "#xdcz0002ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0002RespDto xdcz0002(@Validated @RequestBody Xdcz0002ReqDto xdcz0002ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002ReqDto));
        Xdcz0002DataReqDto xdcz0002DataReqDto = new Xdcz0002DataReqDto();// 请求Data： 电子保函开立
        Xdcz0002DataRespDto xdcz0002DataRespDto = new Xdcz0002DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0002.req.Data reqData = null; // 请求Data：电子保函开立
        cn.com.yusys.yusp.dto.server.biz.xdcz0002.resp.Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0002RespDto xdcz0002RespDto = new Xdcz0002RespDto();
        try {
            // 从 xdcz0002ReqDto获取 reqData
            reqData = xdcz0002ReqDto.getData();
            // 将 reqData 拷贝到xdcz0002DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0002DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002DataReqDto));
            ResultDto<Xdcz0002DataRespDto> xdcz0002DataResultDto = dscmsBizCzClientService.xdcz0002(xdcz0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0002RespDto.setErorcd(Optional.ofNullable(xdcz0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0002RespDto.setErortx(Optional.ofNullable(xdcz0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0002DataResultDto.getCode())) {
                xdcz0002DataRespDto = xdcz0002DataResultDto.getData();
                xdcz0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, e.getMessage());
            xdcz0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0002RespDto.setDatasq(servsq);

        xdcz0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0002.key, DscmsEnum.TRADE_CODE_XDCZ0002.value, JSON.toJSONString(xdcz0002RespDto));
        return xdcz0002RespDto;
    }
}
