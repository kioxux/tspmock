package cn.com.yusys.yusp.web.server.biz.xdsx0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0006.req.Xdsx0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0006.resp.Xdsx0006RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.req.Xdsx0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0006.resp.Xdsx0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:专业贷款评级结果同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0006:专业贷款评级结果同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0006Resource.class);
    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0006
     * 交易描述：专业贷款评级结果同步
     *
     * @param xdsx0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdsx0006:专业贷款评级结果同步")
    @PostMapping("/xdsx0006")
    //@Idempotent({"xdcasx0006", "#xdsx0006ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0006RespDto xdsx0006(@Validated @RequestBody Xdsx0006ReqDto xdsx0006ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006ReqDto));
        Xdsx0006DataReqDto xdsx0006DataReqDto = new Xdsx0006DataReqDto();// 请求Data： 专业贷款评级结果同步
        Xdsx0006DataRespDto xdsx0006DataRespDto = new Xdsx0006DataRespDto();// 响应Data：专业贷款评级结果同步
        Xdsx0006RespDto xdsx0006RespDto = new Xdsx0006RespDto();// 响应Dto：专业贷款评级结果同步
        cn.com.yusys.yusp.dto.server.biz.xdsx0006.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdsx0006.req.Data(); // 请求Data：专业贷款评级结果同步
        cn.com.yusys.yusp.dto.server.biz.xdsx0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0006.resp.Data();// 响应Data：专业贷款评级结果同步
        try {
            // 从 xdsx0006ReqDto获取 reqData
            reqData = xdsx0006ReqDto.getData();
            // 将 reqData 拷贝到xdsx0006DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0006DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006DataReqDto));
            ResultDto<Xdsx0006DataRespDto> xdsx0006DataResultDto = dscmsBizSxClientService.xdsx0006(xdsx0006DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0006RespDto.setErorcd(Optional.ofNullable(xdsx0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0006RespDto.setErortx(Optional.ofNullable(xdsx0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdsx0006RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0006DataResultDto.getCode())) {
                xdsx0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdsx0006DataRespDto = xdsx0006DataResultDto.getData();
                // 将 xdsx0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, e.getMessage());
            xdsx0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0006RespDto.setDatasq(servsq);

        xdsx0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0006.key, DscmsEnum.TRADE_CODE_XDSX0006.value, JSON.toJSONString(xdsx0006RespDto));
        return xdsx0006RespDto;
    }
}
