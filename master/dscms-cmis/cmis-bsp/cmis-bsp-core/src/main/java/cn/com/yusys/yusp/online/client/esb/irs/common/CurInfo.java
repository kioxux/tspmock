package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:汇率信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class CurInfo {

    private List<CurInfoRecord> record; // 币种代码

    public List<CurInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<CurInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "CurInfo{" +
                "record=" + record +
                '}';
    }
}
