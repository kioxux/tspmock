package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp;

/**
 * 响应Service：查询银票出账保证金账号信息（新信贷调票据）
 *
 * @author code-generator
 * @version 1.0
 */
public class Xdpj21RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdpj21RespService{" +
                "service=" + service +
                '}';
    }
}

