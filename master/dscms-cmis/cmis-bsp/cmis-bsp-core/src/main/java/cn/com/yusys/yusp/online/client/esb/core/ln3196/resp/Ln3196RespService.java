package cn.com.yusys.yusp.online.client.esb.core.ln3196.resp;

/**
 * 响应Service：账号子序号查询交易，支持查询内部户、负债账户
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月15日 上午11:22:06
 */
public class Ln3196RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
