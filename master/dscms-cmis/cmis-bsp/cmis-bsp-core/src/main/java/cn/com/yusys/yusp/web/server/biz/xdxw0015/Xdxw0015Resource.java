package cn.com.yusys.yusp.web.server.biz.xdxw0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0015.req.Xdxw0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0015.resp.Xdxw0015RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0015.req.Xdxw0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0015.resp.Xdxw0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0015:查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0015Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0015
     * 交易描述：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
     *
     * @param xdxw0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”")
    @PostMapping("/xdxw0015")
    //@Idempotent({"xdcaxw0015", "#xdxw0015ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0015RespDto xdxw0015(@Validated @RequestBody Xdxw0015ReqDto xdxw0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015ReqDto));
        Xdxw0015DataReqDto xdxw0015DataReqDto = new Xdxw0015DataReqDto();// 请求Data： 查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
        Xdxw0015DataRespDto xdxw0015DataRespDto = new Xdxw0015DataRespDto();// 响应Data：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
        Xdxw0015RespDto xdxw0015RespDto = new Xdxw0015RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0015.req.Data reqData = null; // 请求Data：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
        cn.com.yusys.yusp.dto.server.biz.xdxw0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0015.resp.Data();// 响应Data：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
        try {
            // 从 xdxw0015ReqDto获取 reqData
            reqData = xdxw0015ReqDto.getData();
            // 将 reqData 拷贝到xdxw0015DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0015DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015DataReqDto));
            ResultDto<Xdxw0015DataRespDto> xdxw0015DataResultDto = dscmsBizXwClientService.xdxw0015(xdxw0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0015RespDto.setErorcd(Optional.ofNullable(xdxw0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0015RespDto.setErortx(Optional.ofNullable(xdxw0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0015DataResultDto.getCode())) {
                xdxw0015DataRespDto = xdxw0015DataResultDto.getData();
                xdxw0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, e.getMessage());
            xdxw0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0015RespDto.setDatasq(servsq);

        xdxw0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0015.key, DscmsEnum.TRADE_CODE_XDXW0015.value, JSON.toJSONString(xdxw0015RespDto));
        return xdxw0015RespDto;
    }
}
