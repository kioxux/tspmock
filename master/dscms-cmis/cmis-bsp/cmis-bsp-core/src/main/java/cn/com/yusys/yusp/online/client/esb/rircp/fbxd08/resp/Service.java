package cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查找（利翃）实还正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码
    private String erortx; // 响应信息
    private BigDecimal total_shbj;//本次实还正常本金金额（单位元）+本次实还逾期本金金额（单位元）

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public BigDecimal getTotal_shbj() {
        return total_shbj;
    }

    public void setTotal_shbj(BigDecimal total_shbj) {
        this.total_shbj = total_shbj;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", total_shbj=" + total_shbj +
                '}';
    }
}
