package cn.com.yusys.yusp.online.client.esb.core.ln3251.req;

/**
 * 请求Service：贷款自动追缴明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3251ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3251ReqService{" +
                "service=" + service +
                '}';
    }
}
