package cn.com.yusys.yusp.online.client.esb.core.ln3045.req;

import java.math.BigDecimal;

/**
 * 请求Service：贷款核销处理
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String daikczbz;//业务操作标志
    private String dkjiejuh;//贷款借据号
    private String dkzhangh;//贷款账号
    private String huobdhao;//货币代号
    private BigDecimal hexiaobj;//核销本金
    private BigDecimal hexiaolx;//核销利息
    private String hexiaozh;//核销来源账号
    private String hexiaozx;//核销来源账号子序号
    private String daikghfs;//贷款归还方式
    private String hxzdkkbz;//核销自动扣款标志
    private String beizhuuu;//核销备注
    private String jixibzhi;//计息标志

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public String getHexiaozh() {
        return hexiaozh;
    }

    public void setHexiaozh(String hexiaozh) {
        this.hexiaozh = hexiaozh;
    }

    public String getHexiaozx() {
        return hexiaozx;
    }

    public void setHexiaozx(String hexiaozx) {
        this.hexiaozx = hexiaozx;
    }

    public String getDaikghfs() {
        return daikghfs;
    }

    public void setDaikghfs(String daikghfs) {
        this.daikghfs = daikghfs;
    }

    public String getHxzdkkbz() {
        return hxzdkkbz;
    }

    public void setHxzdkkbz(String hxzdkkbz) {
        this.hxzdkkbz = hxzdkkbz;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getJixibzhi() {
        return jixibzhi;
    }

    public void setJixibzhi(String jixibzhi) {
        this.jixibzhi = jixibzhi;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", hexiaobj=" + hexiaobj +
                ", hexiaolx=" + hexiaolx +
                ", hexiaozh='" + hexiaozh + '\'' +
                ", hexiaozx='" + hexiaozx + '\'' +
                ", daikghfs='" + daikghfs + '\'' +
                ", hxzdkkbz='" + hxzdkkbz + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", jixibzhi='" + jixibzhi + '\'' +
                '}';
    }
}
