package cn.com.yusys.yusp.online.client.esb.irs.xirs15.resp;

/**
 * 响应Service：评级在途申请标识
 *
 * @author chenyong
 * @version 1.0
 */
public class Xirs15RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xirs15RespService{" +
                "service=" + service +
                '}';
    }
}
