package cn.com.yusys.yusp.web.server.biz.xddb0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0007.req.Xddb0007ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0007.resp.Xddb0007RespDto;
import cn.com.yusys.yusp.dto.server.xddb0007.req.Xddb0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0007.resp.Xddb0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:电票质押请求
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "xddb0007:电票质押请求")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0007Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0007
     * 交易描述：电票质押请求
     *
     * @param xddb0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddb0007:电票质押请求")
    @PostMapping("/xddb0007")
  //  @Idempotent({"xddb0007", "#xddb0007ReqDto.datasq"})
    protected @ResponseBody
    Xddb0007RespDto xddb0007(@Validated @RequestBody Xddb0007ReqDto xddb0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, JSON.toJSONString(xddb0007ReqDto));
        Xddb0007DataReqDto xddb0007DataReqDto = new Xddb0007DataReqDto();// 请求Data： 电票质押请求
        Xddb0007DataRespDto xddb0007DataRespDto = new Xddb0007DataRespDto();// 响应Data：电票质押请求
        Xddb0007RespDto xddb0007RespDto = new Xddb0007RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0007.req.Data reqData = null; // 请求Data：电票质押请求
        cn.com.yusys.yusp.dto.server.biz.xddb0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0007.resp.Data();// 响应Data：电票质押请求
        try {
            // 从 xddb0007ReqDto获取 reqData
            reqData = xddb0007ReqDto.getData();
            // 将 reqData 拷贝到xddb0007DataReqDto
            BeanUtils.copyProperties(reqData, xddb0007DataReqDto);

            ResultDto<Xddb0007DataRespDto> xddb0007DataResultDto = dscmsBizDbClientService.xddb0007(xddb0007DataReqDto);
            // 从返回值中获取响应码和响应消息
            xddb0007RespDto.setErorcd(Optional.ofNullable(xddb0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0007RespDto.setErortx(Optional.ofNullable(xddb0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0007DataResultDto.getCode())) {
                xddb0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xddb0007DataRespDto = xddb0007DataResultDto.getData();
                // 将 xddb0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, e.getMessage());
            xddb0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0007RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0007RespDto.setDatasq(servsq);
        xddb0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0007.key, DscmsEnum.TRADE_CODE_XDDB0007.value, JSON.toJSONString(xddb0007RespDto));
        return xddb0007RespDto;
    }
}
