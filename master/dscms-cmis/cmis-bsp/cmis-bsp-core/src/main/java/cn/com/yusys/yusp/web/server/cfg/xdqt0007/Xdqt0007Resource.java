package cn.com.yusys.yusp.web.server.cfg.xdqt0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdqt0007.req.Xdqt0007ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdqt0007.resp.Xdqt0007RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdqt0007.req.Xdqt0007DataReqDto;
import cn.com.yusys.yusp.server.xdqt0007.resp.Xdqt0007DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:新微贷产品信息同步
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0007:新微贷产品信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0007Resource.class);

    @Autowired
	private DscmsCfgQtClientService dscmsCfgQtClientService;

    /**
     * 交易码：xdqt0007
     * 交易描述：新微贷产品信息同步
     *
     * @param xdqt0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("新微贷产品信息同步")
    @PostMapping("/xdqt0007")
   // @Idempotent({"xdcaqt0007", "#xdqt0007ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0007RespDto xdqt0007(@Validated @RequestBody Xdqt0007ReqDto xdqt0007ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, JSON.toJSONString(xdqt0007ReqDto));
        Xdqt0007DataReqDto xdqt0007DataReqDto = new Xdqt0007DataReqDto();// 请求Data： 新微贷产品信息同步
        Xdqt0007DataRespDto xdqt0007DataRespDto = new Xdqt0007DataRespDto();// 响应Data：新微贷产品信息同步
        cn.com.yusys.yusp.dto.server.cfg.xdqt0007.req.Data reqData = null; // 请求Data：新微贷产品信息同步
        cn.com.yusys.yusp.dto.server.cfg.xdqt0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdqt0007.resp.Data();// 响应Data：新微贷产品信息同步
		Xdqt0007RespDto xdqt0007RespDto = new Xdqt0007RespDto();
        try {
            // 从 xdqt0007ReqDto获取 reqData
            reqData = xdqt0007ReqDto.getData();
            // 将 reqData 拷贝到xdqt0007DataReqDto
            BeanUtils.copyProperties(reqData, xdqt0007DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, JSON.toJSONString(xdqt0007DataReqDto));
            ResultDto<Xdqt0007DataRespDto> xdqt0007DataResultDto = dscmsCfgQtClientService.xdqt0007(xdqt0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, JSON.toJSONString(xdqt0007DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0007RespDto.setErorcd(Optional.ofNullable(xdqt0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0007RespDto.setErortx(Optional.ofNullable(xdqt0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0007DataResultDto.getCode())) {
                xdqt0007DataRespDto = xdqt0007DataResultDto.getData();
                xdqt0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, e.getMessage());
            xdqt0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0007RespDto.setDatasq(servsq);

        xdqt0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0007.key, DscmsEnum.TRADE_CODE_XDQT0007.value, JSON.toJSONString(xdqt0007RespDto));
        return xdqt0007RespDto;
    }
}
