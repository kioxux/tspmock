package cn.com.yusys.yusp.web.client.esb.core.ln3020;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Ln3020ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Ln3020RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Ln3020ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.Ln3020RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3020)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3020Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3020Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3020
     * 交易描述：贷款开户
     *
     * @param ln3020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3020:贷款开户")
    @PostMapping("/ln3020")
    protected @ResponseBody
    ResultDto<Ln3020RespDto> ln3020(@Validated @RequestBody Ln3020ReqDto ln3020ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value, JSON.toJSONString(ln3020ReqDto, SerializerFeature.WriteMapNullValue));
        cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.Service();
        Ln3020ReqService ln3020ReqService = new Ln3020ReqService();
        Ln3020RespService ln3020RespService = new Ln3020RespService();
        Ln3020RespDto ln3020RespDto = new Ln3020RespDto();
        ResultDto<Ln3020RespDto> ln3020ResultDto = new ResultDto<Ln3020RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3020ReqDto转换成reqService
            BeanUtils.copyProperties(ln3020ReqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkbjfd())) {//本金分段登记
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkbjfd> ln3020ReqDtoLstdkbjfdList = Optional.ofNullable(ln3020ReqDto.getLstdkbjfd()).orElseGet(() -> new ArrayList<>());//本金分段登记
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkbjfd_ARRAY ln3020Reqlstdkbjfd_ARRAY = Optional.ofNullable(reqService.getLstdkbjfd_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkbjfd_ARRAY());//本金分段登记
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkbjfd.Record> lstdkbjfdRecords = ln3020ReqDtoLstdkbjfdList.parallelStream().map(ln3020ReqDtoLstdkbjfd -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkbjfd.Record lstdkbjfdRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkbjfd.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkbjfd, lstdkbjfdRecord);
                    return lstdkbjfdRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkbjfd_ARRAY.setRecord(lstdkbjfdRecords);
                reqService.setLstdkbjfd_ARRAY(ln3020Reqlstdkbjfd_ARRAY);

            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkdygl())) {//抵押关联信息
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkdygl> ln3020ReqDtoLstdkdyglList = Optional.ofNullable(ln3020ReqDto.getLstdkdygl()).orElseGet(() -> new ArrayList<>());//抵押关联信息
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkdygl_ARRAY ln3020Reqlstdkdygl_ARRAY = Optional.ofNullable(reqService.getLstdkdygl_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkdygl_ARRAY());//抵押关联信息
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkdygl.Record> lstdkdyglRecords = ln3020ReqDtoLstdkdyglList.parallelStream().map(ln3020ReqDtoLstdkdygl -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkdygl.Record lstdkdyglRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkdygl.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkdygl, lstdkdyglRecord);
                    return lstdkdyglRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkdygl_ARRAY.setRecord(lstdkdyglRecords);
                reqService.setLstdkdygl_ARRAY(ln3020Reqlstdkdygl_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkfkjh())) {//贷款放款计划
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkfkjh> ln3020ReqDtoLstdkfkjhList = Optional.ofNullable(ln3020ReqDto.getLstdkfkjh()).orElseGet(() -> new ArrayList<>());//贷款放款计划
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkfkjh_ARRAY ln3020Reqlstdkfkjh_ARRAY = Optional.ofNullable(reqService.getLstdkfkjh_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkfkjh_ARRAY());//贷款放款计划
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkfkjh.Record> lstdkfkjhRecords = ln3020ReqDtoLstdkfkjhList.parallelStream().map(ln3020ReqDtoLstdkfkjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkfkjh.Record lstdkfkjhRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkfkjh.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkfkjh, lstdkfkjhRecord);
                    return lstdkfkjhRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkfkjh_ARRAY.setRecord(lstdkfkjhRecords);
                reqService.setLstdkfkjh_ARRAY(ln3020Reqlstdkfkjh_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkhbjh())) {//贷款还本计划
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkhbjh> ln3020ReqDtoLstdkhbjhList = Optional.ofNullable(ln3020ReqDto.getLstdkhbjh()).orElseGet(() -> new ArrayList<>());//贷款还本计划
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkhbjh_ARRAY ln3020Reqlstdkhbjh_ARRAY = Optional.ofNullable(reqService.getLstdkhbjh_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkhbjh_ARRAY());//贷款还本计划
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhbjh.Record> lstdkhbjhRecords = ln3020ReqDtoLstdkhbjhList.parallelStream().map(ln3020ReqDtoLstdkhbjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhbjh.Record lstdkhbjhRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhbjh.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkhbjh, lstdkhbjhRecord);
                    return lstdkhbjhRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkhbjh_ARRAY.setRecord(lstdkhbjhRecords);
                reqService.setLstdkhbjh_ARRAY(ln3020Reqlstdkhbjh_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkhkzh())) {//贷款多还款账户
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkhkzh> ln3020ReqDtoLstdkhkzhList = Optional.ofNullable(ln3020ReqDto.getLstdkhkzh()).orElseGet(() -> new ArrayList<>());//贷款多还款账户
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkhkzh_ARRAY ln3020Reqlstdkhkzh_ARRAY = Optional.ofNullable(reqService.getLstdkhkzh_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkhkzh_ARRAY());//贷款多还款账户
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhkzh.Record> lstdkhkzhRecords = ln3020ReqDtoLstdkhkzhList.parallelStream().map(ln3020ReqDtoLstdkhkzh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhkzh.Record lstdkhkzhRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhkzh.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkhkzh, lstdkhkzhRecord);
                    return lstdkhkzhRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkhkzh_ARRAY.setRecord(lstdkhkzhRecords);
                reqService.setLstdkhkzh_ARRAY(ln3020Reqlstdkhkzh_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdklhmx())) {//联合贷款
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdklhmx> ln3020ReqDtoLstdklhmxList = Optional.ofNullable(ln3020ReqDto.getLstdklhmx()).orElseGet(() -> new ArrayList<>());//联合贷款
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdklhmx_ARRAY ln3020Reqlstdklhmx_ARRAY = Optional.ofNullable(reqService.getLstdklhmx_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdklhmx_ARRAY());//联合贷款
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdklhmx.Record> lstdklhmxRecords = ln3020ReqDtoLstdklhmxList.parallelStream().map(ln3020ReqDtoLstdklhmx -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdklhmx.Record lstdklhmxRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdklhmx.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdklhmx, lstdklhmxRecord);
                    return lstdklhmxRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdklhmx_ARRAY.setRecord(lstdklhmxRecords);
                reqService.setLstdklhmx_ARRAY(ln3020Reqlstdklhmx_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkllfd())) {//贷款利率分段
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkllfd> ln3020ReqDtoLstdkllfdList = Optional.ofNullable(ln3020ReqDto.getLstdkllfd()).orElseGet(() -> new ArrayList<>());//贷款利率分段
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkllfd_ARRAY ln3020Reqlstdkllfd_ARRAY = Optional.ofNullable(reqService.getLstdkllfd_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkllfd_ARRAY());//贷款利率分段
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkllfd.Record> lstdkllfdRecords = ln3020ReqDtoLstdkllfdList.parallelStream().map(ln3020ReqDtoLstdkllfd -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkllfd.Record lstdkllfdRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkllfd.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkllfd, lstdkllfdRecord);
                    return lstdkllfdRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkllfd_ARRAY.setRecord(lstdkllfdRecords);
                reqService.setLstdkllfd_ARRAY(ln3020Reqlstdkllfd_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdksfsj())) {//贷款收费事件
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdksfsj> ln3020ReqDtoLstdksfsjList = Optional.ofNullable(ln3020ReqDto.getLstdksfsj()).orElseGet(() -> new ArrayList<>());//贷款收费事件
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdksfsj_ARRAY ln3020Reqlstdksfsj_ARRAY = Optional.ofNullable(reqService.getLstdksfsj_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdksfsj_ARRAY());//贷款收费事件
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdksfsj.Record> lstdksfsjRecords = ln3020ReqDtoLstdksfsjList.parallelStream().map(ln3020ReqDtoLstdksfsj -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdksfsj.Record lstdksfsjRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdksfsj.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdksfsj, lstdksfsjRecord);
                    return lstdksfsjRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdksfsj_ARRAY.setRecord(lstdksfsjRecords);
                reqService.setLstdksfsj_ARRAY(ln3020Reqlstdksfsj_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdktxzh())) {//贷款多贴息账户
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdktxzh> ln3020ReqDtoLstdktxzhList = Optional.ofNullable(ln3020ReqDto.getLstdktxzh()).
                        orElseGet(() -> new ArrayList<>());//贷款多贴息账户
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdktxzh_ARRAY ln3020Reqlstdktxzh_ARRAY = Optional.ofNullable(reqService.getLstdktxzh_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdktxzh_ARRAY());//贷款多贴息账户
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdktxzh.Record> lstdktxzhRecords = ln3020ReqDtoLstdktxzhList.parallelStream().map(ln3020ReqDtoLstdktxzh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdktxzh.Record lstdktxzhRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdktxzh.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdktxzh, lstdktxzhRecord);
                    return lstdktxzhRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdktxzh_ARRAY.setRecord(lstdktxzhRecords);
                reqService.setLstdktxzh_ARRAY(ln3020Reqlstdktxzh_ARRAY);

            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkwtxx())) {//贷款多委托人账户
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkwtxx> ln3020ReqDtoLstdkwtxxList = Optional.ofNullable(ln3020ReqDto.getLstdkwtxx()).orElseGet(() -> new ArrayList<>());//贷款多委托人账户
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkwtxx_ARRAY ln3020Reqlstdkwtxx_ARRAY = Optional.ofNullable(reqService.getLstdkwtxx_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkwtxx_ARRAY());//贷款多委托人账户
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkwtxx.Record> lstdkwtxxRecords = ln3020ReqDtoLstdkwtxxList.parallelStream().map(ln3020ReqDtoLstdkwtxx -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkwtxx.Record lstdkwtxxRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkwtxx.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkwtxx, lstdkwtxxRecord);
                    return lstdkwtxxRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkwtxx_ARRAY.setRecord(lstdkwtxxRecords);
                reqService.setLstdkwtxx_ARRAY(ln3020Reqlstdkwtxx_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkstzf())) {//贷款受托支付
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkstzf> ln3020ReqDtoLstdkstzfList = Optional.ofNullable(ln3020ReqDto.getLstdkstzf()).orElseGet(() -> new ArrayList<>());//贷款受托支付
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkstzf_ARRAY ln3020Reqlstdkstzf_ARRAY = Optional.ofNullable(reqService.getLstdkstzf_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkstzf_ARRAY());//贷款受托支付
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkstzf.Record> lstdkstzfRecords = ln3020ReqDtoLstdkstzfList.parallelStream().map(ln3020ReqDtoLstdkstzf -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkstzf.Record lstdkstzfRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkstzf.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkstzf, lstdkstzfRecord);
                    return lstdkstzfRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkstzf_ARRAY.setRecord(lstdkstzfRecords);
                reqService.setLstdkstzf_ARRAY(ln3020Reqlstdkstzf_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkzhbz())) {//贷款保证人信息
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkzhbz> ln3020ReqDtoLstdkzhbzList = Optional.ofNullable(ln3020ReqDto.getLstdkzhbz()).orElseGet(() -> new ArrayList<>());//贷款保证人信息
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkzhbz_ARRAY ln3020Reqlstdkzhbz_ARRAY = Optional.ofNullable(reqService.getLstdkzhbz_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkzhbz_ARRAY());//贷款保证人信息
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhbz.Record> lstdkzhbzRecords = ln3020ReqDtoLstdkzhbzList.parallelStream().map(ln3020ReqDtoLstdkzhbz -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhbz.Record lstdkzhbzRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhbz.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkzhbz, lstdkzhbzRecord);
                    return lstdkzhbzRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkzhbz_ARRAY.setRecord(lstdkzhbzRecords);
                reqService.setLstdkzhbz_ARRAY(ln3020Reqlstdkzhbz_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkzhlm())) {//贷款账户联名
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkzhlm> ln3020ReqDtoLstdkzhlmList = Optional.ofNullable(ln3020ReqDto.getLstdkzhlm()).orElseGet(() -> new ArrayList<>());//贷款账户联名
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkzhlm_ARRAY ln3020Reqlstdkzhlm_ARRAY = Optional.ofNullable(reqService.getLstdkzhlm_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkzhlm_ARRAY());//贷款账户联名
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhlm.Record> lstdkzhlmRecords = ln3020ReqDtoLstdkzhlmList.parallelStream().map(ln3020ReqDtoLstdkzhlm -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhlm.Record lstdkzhlmRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhlm.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkzhlm, lstdkzhlmRecord);
                    return lstdkzhlmRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkzhlm_ARRAY.setRecord(lstdkzhlmRecords);
                reqService.setLstdkzhlm_ARRAY(ln3020Reqlstdkzhlm_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdkzhzy())) {//账户质押信息
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdkzhzy> ln3020ReqDtoLstdkzhzyList = Optional.ofNullable(ln3020ReqDto.getLstdkzhzy()).orElseGet(() -> new ArrayList<>());//账户质押信息
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkzhzy_ARRAY ln3020Reqlstdkzhzy_ARRAY = Optional.ofNullable(reqService.getLstdkzhzy_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdkzhzy_ARRAY());//账户质押信息
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhzy.Record> lstdkzhzyRecords = ln3020ReqDtoLstdkzhzyList.parallelStream().map(ln3020ReqDtoLstdkzhzy -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhzy.Record lstdkzhzyRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhzy.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdkzhzy, lstdkzhzyRecord);
                    return lstdkzhzyRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdkzhzy_ARRAY.setRecord(lstdkzhzyRecords);
                reqService.setLstdkzhzy_ARRAY(ln3020Reqlstdkzhzy_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstdzqgjh())) {//贷款定制期供计划表
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstdzqgjh> ln3020ReqDtoLstdzqgjhList = Optional.ofNullable(ln3020ReqDto.getLstdzqgjh()).orElseGet(() -> new ArrayList<>());//贷款定制期供计划表
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdzqgjh_ARRAY ln3020Reqlstdzqgjh_ARRAY = Optional.ofNullable(reqService.getLstdzqgjh_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstdzqgjh_ARRAY());//贷款定制期供计划表
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdzqgjh.Record> lstdzqgjhRecords = ln3020ReqDtoLstdzqgjhList.parallelStream().map(ln3020ReqDtoLstdzqgjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdzqgjh.Record lstdzqgjhRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdzqgjh.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstdzqgjh, lstdzqgjhRecord);
                    return lstdzqgjhRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstdzqgjh_ARRAY.setRecord(lstdzqgjhRecords);
                reqService.setLstdzqgjh_ARRAY(ln3020Reqlstdzqgjh_ARRAY);
            }
            if (CollectionUtils.nonEmpty(ln3020ReqDto.getLstydkjjh())) {//借新还旧列表
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Lstydkjjh> ln3020ReqDtoLstydkjjhList = Optional.ofNullable(ln3020ReqDto.getLstydkjjh()).orElseGet(() -> new ArrayList<>());//借新还旧列表
                cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstydkjjh_ARRAY ln3020Reqlstydkjjh_ARRAY = Optional.ofNullable(reqService.getLstydkjjh_ARRAY()).
                        orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.Lstydkjjh_ARRAY());//借新还旧列表
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstydkjjh.Record> lstydkjjhRecords = ln3020ReqDtoLstydkjjhList.parallelStream().map(ln3020ReqDtoLstydkjjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstydkjjh.Record lstydkjjhRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstydkjjh.Record();
                    BeanUtils.copyProperties(ln3020ReqDtoLstydkjjh, lstydkjjhRecord);
                    return lstydkjjhRecord;
                }).collect(Collectors.toList());
                ln3020Reqlstydkjjh_ARRAY.setRecord(lstydkjjhRecords);
                reqService.setLstydkjjh_ARRAY(ln3020Reqlstydkjjh_ARRAY);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3020.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(ln3020ReqDto.getZhngjigo());//    部门号,取账务机构
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3020ReqService.setService(reqService);

            // 将ln3020ReqService转换成ln3020ReqServiceMap
            Map ln3020ReqServiceMap = beanMapUtil.beanToMap(ln3020ReqService);
            context.put("tradeDataMap", ln3020ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3020.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3020RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3020RespService.class, Ln3020RespService.class);
            respService = ln3020RespService.getService();
            ln3020ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3020ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, ln3020RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.Lstdkstzf_ARRAY lstdkstzf_ARRAY = Optional.ofNullable(respService.getLstdkstzf_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.Lstdkstzf_ARRAY());
                respService.setLstdkstzf_ARRAY(lstdkstzf_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkstzf_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.lstdkstzf.Record> ln3020RespLstdkstzfRecordList = Optional.ofNullable(respService.getLstdkstzf_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Lstdkstzf> ln3020RespDtoLstdkstzfList = ln3020RespLstdkstzfRecordList.parallelStream().map(
                            ln3020RespLstdkstzfRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Lstdkstzf ln3020RespDtoLstdkstzf = new cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Lstdkstzf();
                                BeanUtils.copyProperties(ln3020RespLstdkstzfRecord, ln3020RespDtoLstdkstzf);
                                return ln3020RespDtoLstdkstzf;
                            }).collect(Collectors.toList());//贷款受托支付
                    ln3020RespDto.setLstdkstzf(ln3020RespDtoLstdkstzfList);
                }

                ln3020ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3020ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3020ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3020ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value, e.getMessage());
            ln3020ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3020ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3020ResultDto.setData(ln3020RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3020.key, EsbEnum.TRADE_CODE_LN3020.value, JSON.toJSONString(ln3020ResultDto, SerializerFeature.WriteMapNullValue));
        return ln3020ResultDto;
    }
}
