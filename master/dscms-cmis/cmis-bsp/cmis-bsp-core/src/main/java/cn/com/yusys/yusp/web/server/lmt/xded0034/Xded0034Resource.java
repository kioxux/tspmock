package cn.com.yusys.yusp.web.server.lmt.xded0034;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.req.CmisLmt0034ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0034.resp.CmisLmt0034RespDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0034.req.DealBizList;
import cn.com.yusys.yusp.dto.server.lmt.xded0034.req.OccRelList;
import cn.com.yusys.yusp.dto.server.lmt.xded0034.req.Xded0034ReqDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0034.resp.Xded0034RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:国结票据出账额度占用
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDED0034:国结票据出账额度占用")
@RestController
@RequestMapping("/api/dscms")
public class Xded0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xded0034Resource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：XDED0034
     * 交易描述：国结票据出账额度占用
     *
     * @param xded0034ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("国结票据出账额度占用")
    @PostMapping("/xded0034")
    //  @Idempotent({"xded0034", "#xded0034ReqDto.datasq"})
    protected @ResponseBody
    Xded0034RespDto cmisLmt0034(@Validated @RequestBody Xded0034ReqDto xded0034ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0034.key, DscmsEnum.TRADE_CODE_XDED0034.value, JSON.toJSONString(xded0034ReqDto));
        CmisLmt0034ReqDto cmisLmt0034ReqDto = new CmisLmt0034ReqDto();// 请求Data： 国结票据出账额度占用
        CmisLmt0034RespDto cmisLmt0034RespDto = new CmisLmt0034RespDto();// 响应Data：国结票据出账额度占用

        Xded0034RespDto xded0034RespDto = new Xded0034RespDto();

        cn.com.yusys.yusp.dto.server.lmt.xded0034.req.Data reqData = null; // 请求Data：国结票据出账额度占用
        cn.com.yusys.yusp.dto.server.lmt.xded0034.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.xded0034.resp.Data();// 响应Data：国结票据出账额度占用

        try {
            // 从 Xded0034Dto获取 reqData
            reqData = xded0034ReqDto.getData();
            // 将 reqData 拷贝到cmisLmt0034DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0034ReqDto);
            List<DealBizList> dealBizList = reqData.getDealBizList();
            List<OccRelList> occRelList = reqData.getOccRelList();
            if (CollectionUtils.nonEmpty(dealBizList)) {
                List<CmisLmt0034DealBizListReqDto> collect = dealBizList.parallelStream().map(deal -> {
                    CmisLmt0034DealBizListReqDto cmisLmt0034DealBizListReqDto = new CmisLmt0034DealBizListReqDto();
                    BeanUtils.copyProperties(deal, cmisLmt0034DealBizListReqDto);
                    return cmisLmt0034DealBizListReqDto;
                }).collect(Collectors.toList());
                cmisLmt0034ReqDto.setDealBizList(collect);
            }
            if (CollectionUtils.nonEmpty(occRelList)) {
                List<CmisLmt0034OccRelListReqDto> collect = occRelList.parallelStream().map(occ -> {
                    CmisLmt0034OccRelListReqDto cmisLmt0034OccRelListReqDto = new CmisLmt0034OccRelListReqDto();
                    BeanUtils.copyProperties(occ, cmisLmt0034OccRelListReqDto);
                    return cmisLmt0034OccRelListReqDto;
                }).collect(Collectors.toList());
                cmisLmt0034ReqDto.setOccRelList(collect);
            }

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, JSON.toJSONString(cmisLmt0034ReqDto));
            ResultDto<CmisLmt0034RespDto> cmisLmt0034RespDtoResultDto = cmisLmtClientService.cmislmt0034(cmisLmt0034ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0034.value, JSON.toJSONString(cmisLmt0034RespDtoResultDto));
            // 从返回值中获取响应码和响应消息
            xded0034RespDto.setErorcd(Optional.ofNullable(cmisLmt0034RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xded0034RespDto.setErortx(Optional.ofNullable(cmisLmt0034RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0034RespDtoResultDto.getCode())) {
                cmisLmt0034RespDto = cmisLmt0034RespDtoResultDto.getData();
                xded0034RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xded0034RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0034DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0034RespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0034.key, DscmsEnum.TRADE_CODE_XDED0034.value, e.getMessage());
            xded0034RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xded0034RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xded0034RespDto.setDatasq(servsq);

        xded0034RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0034.key, DscmsEnum.TRADE_CODE_XDED0034.value, JSON.toJSONString(xded0034RespDto));
        return xded0034RespDto;
    }
}
