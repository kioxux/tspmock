package cn.com.yusys.yusp.web.server.biz.xdxw0068;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0068.req.Xdxw0068ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0068.resp.Xdxw0068RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0068.req.Xdxw0068DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0068.resp.Xdxw0068DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:授信调查结论信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0068:授信调查结论信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0068Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0068Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0068
     * 交易描述：授信调查结论信息查询
     *
     * @param xdxw0068ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授信调查结论信息查询")
    @PostMapping("/xdxw0068")
    //@Idempotent({"xdcaxw0068", "#xdxw0068ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0068RespDto xdxw0068(@Validated @RequestBody Xdxw0068ReqDto xdxw0068ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068ReqDto));
        Xdxw0068DataReqDto xdxw0068DataReqDto = new Xdxw0068DataReqDto();// 请求Data： 授信调查结论信息查询
        Xdxw0068DataRespDto xdxw0068DataRespDto = new Xdxw0068DataRespDto();// 响应Data：授信调查结论信息查询
        Xdxw0068RespDto xdxw0068RespDto = new Xdxw0068RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0068.req.Data reqData = null; // 请求Data：授信调查结论信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0068.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0068.resp.Data();// 响应Data：授信调查结论信息查询
        try {
            // 从 xdxw0068ReqDto获取 reqData
            reqData = xdxw0068ReqDto.getData();
            // 将 reqData 拷贝到xdxw0068DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0068DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068DataReqDto));
            ResultDto<Xdxw0068DataRespDto> xdxw0068DataResultDto = dscmsBizXwClientService.xdxw0068(xdxw0068DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0068RespDto.setErorcd(Optional.ofNullable(xdxw0068DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0068RespDto.setErortx(Optional.ofNullable(xdxw0068DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0068DataResultDto.getCode())) {
                xdxw0068DataRespDto = xdxw0068DataResultDto.getData();
                xdxw0068RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0068RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0068DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0068DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, e.getMessage());
            xdxw0068RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0068RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0068RespDto.setDatasq(servsq);

        xdxw0068RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0068.key, DscmsEnum.TRADE_CODE_XDXW0068.value, JSON.toJSONString(xdxw0068RespDto));
        return xdxw0068RespDto;
    }
}
