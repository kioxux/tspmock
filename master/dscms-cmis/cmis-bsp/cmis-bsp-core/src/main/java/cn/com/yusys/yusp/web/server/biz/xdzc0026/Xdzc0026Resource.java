package cn.com.yusys.yusp.web.server.biz.xdzc0026;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0026.req.Xdzc0026ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0026.resp.Xdzc0026RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0026.req.Xdzc0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0026.resp.Xdzc0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池主动还款接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0026:资产池主动还款接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0026Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0026
     * 交易描述：资产池主动还款接口
     *
     * @param xdzc0026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池主动还款接口")
    @PostMapping("/xdzc0026")
    @Idempotent({"xdzc0026", "#xdzc0026ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0026RespDto xdzc0026(@Validated @RequestBody Xdzc0026ReqDto xdzc0026ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, JSON.toJSONString(xdzc0026ReqDto));
        Xdzc0026DataReqDto xdzc0026DataReqDto = new Xdzc0026DataReqDto();// 请求Data： 资产池主动还款接口
        Xdzc0026DataRespDto xdzc0026DataRespDto = new Xdzc0026DataRespDto();// 响应Data：资产池主动还款接口
        Xdzc0026RespDto xdzc0026RespDto = new Xdzc0026RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0026.req.Data reqData = null; // 请求Data：资产池主动还款接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0026.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0026.resp.Data();// 响应Data：资产池主动还款接口
        try {
            // 从 xdzc0026ReqDto获取 reqData
            reqData = xdzc0026ReqDto.getData();
            // 将 reqData 拷贝到xdzc0026DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0026DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, JSON.toJSONString(xdzc0026DataReqDto));
            ResultDto<Xdzc0026DataRespDto> xdzc0026DataResultDto = dscmsBizZcClientService.xdzc0026(xdzc0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, JSON.toJSONString(xdzc0026DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0026RespDto.setErorcd(Optional.ofNullable(xdzc0026DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0026RespDto.setErortx(Optional.ofNullable(xdzc0026DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0026DataResultDto.getCode())) {
                xdzc0026DataRespDto = xdzc0026DataResultDto.getData();
                xdzc0026RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0026RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0026DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0026DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, e.getMessage());
            xdzc0026RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0026RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0026RespDto.setDatasq(servsq);

        xdzc0026RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0026.key, DscmsEnum.TRADE_CODE_XDZC0026.value, JSON.toJSONString(xdzc0026RespDto));
        return xdzc0026RespDto;
    }
}
