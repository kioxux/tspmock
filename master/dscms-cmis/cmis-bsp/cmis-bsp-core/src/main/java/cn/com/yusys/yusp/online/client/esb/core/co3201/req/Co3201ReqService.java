package cn.com.yusys.yusp.online.client.esb.core.co3201.req;

/**
 * 请求Service：抵质押物信息修改
 *
 * @author leehuang
 * @version 1.0
 */
public class Co3201ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Co3201ReqService{" +
				"service=" + service +
				'}';
	}
}
