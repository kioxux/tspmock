package cn.com.yusys.yusp.online.client.esb.irs.irs28.req;

import cn.com.yusys.yusp.online.client.esb.irs.irs28.req.assetdebt.Record;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 16:20
 * @since 2021/5/27 16:20
 */
public class AssetDebt_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.irs.irs28.req.assetdebt.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "AssetDebt_ARRAY{" +
                "record=" + record +
                '}';
    }
}
