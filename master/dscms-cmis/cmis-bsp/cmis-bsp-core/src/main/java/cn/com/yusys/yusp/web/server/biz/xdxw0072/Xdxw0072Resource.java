package cn.com.yusys.yusp.web.server.biz.xdxw0072;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0072.req.Xdxw0072ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0072.resp.Xdxw0072RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0072.req.Xdxw0072DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0072.resp.Xdxw0072DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据调查表编号前往信贷查找客户经理信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0072:根据调查表编号前往信贷查找客户经理信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0072Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0072Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0072
     * 交易描述：根据调查表编号前往信贷查找客户经理信息
     *
     * @param xdxw0072ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据调查表编号前往信贷查找客户经理信息")
    @PostMapping("/xdxw0072")
    //@Idempotent({"xdcaxw0072", "#xdxw0072ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0072RespDto xdxw0072(@Validated @RequestBody Xdxw0072ReqDto xdxw0072ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, JSON.toJSONString(xdxw0072ReqDto));
        Xdxw0072DataReqDto xdxw0072DataReqDto = new Xdxw0072DataReqDto();// 请求Data： 根据调查表编号前往信贷查找客户经理信息
        Xdxw0072DataRespDto xdxw0072DataRespDto = new Xdxw0072DataRespDto();// 响应Data：根据调查表编号前往信贷查找客户经理信息
        Xdxw0072RespDto xdxw0072RespDto = new Xdxw0072RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0072.req.Data reqData = null; // 请求Data：根据调查表编号前往信贷查找客户经理信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0072.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0072.resp.Data();// 响应Data：根据调查表编号前往信贷查找客户经理信息
        try {
            // 从 xdxw0072ReqDto获取 reqData
            reqData = xdxw0072ReqDto.getData();
            // 将 reqData 拷贝到xdxw0072DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0072DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, JSON.toJSONString(xdxw0072DataReqDto));
            ResultDto<Xdxw0072DataRespDto> xdxw0072DataResultDto = dscmsBizXwClientService.xdxw0072(xdxw0072DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, JSON.toJSONString(xdxw0072DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0072RespDto.setErorcd(Optional.ofNullable(xdxw0072DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0072RespDto.setErortx(Optional.ofNullable(xdxw0072DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0072DataResultDto.getCode())) {
                xdxw0072DataRespDto = xdxw0072DataResultDto.getData();
                xdxw0072RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0072RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0072DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0072DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, e.getMessage());
            xdxw0072RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0072RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0072RespDto.setDatasq(servsq);

        xdxw0072RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0072.key, DscmsEnum.TRADE_CODE_XDXW0072.value, JSON.toJSONString(xdxw0072RespDto));
        return xdxw0072RespDto;
    }
}
