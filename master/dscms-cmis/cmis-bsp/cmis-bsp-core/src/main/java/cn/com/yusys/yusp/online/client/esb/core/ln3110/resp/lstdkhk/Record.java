package cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdkhk;


import java.math.BigDecimal;

/**
 * 响应Service：还款计划
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:00
 */
public class Record {

    private Integer benqqish;//本期期数
    private String qixiriqi;//起息日期
    private String huankriq;//还款日期
    private String kxqdqirq;//宽限期到期日
    private BigDecimal jixibenj;//计息本金
    private BigDecimal leijcslx;//累计产生利息
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal zhanghye;//账户余额

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getHuankriq() {
        return huankriq;
    }

    public void setHuankriq(String huankriq) {
        this.huankriq = huankriq;
    }

    public String getKxqdqirq() {
        return kxqdqirq;
    }

    public void setKxqdqirq(String kxqdqirq) {
        this.kxqdqirq = kxqdqirq;
    }

    public BigDecimal getJixibenj() {
        return jixibenj;
    }

    public void setJixibenj(BigDecimal jixibenj) {
        this.jixibenj = jixibenj;
    }

    public BigDecimal getLeijcslx() {
        return leijcslx;
    }

    public void setLeijcslx(BigDecimal leijcslx) {
        this.leijcslx = leijcslx;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    @Override
    public String toString() {
        return "Lstdkhk{" +
                "benqqish=" + benqqish +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", huankriq='" + huankriq + '\'' +
                ", kxqdqirq='" + kxqdqirq + '\'' +
                ", jixibenj=" + jixibenj +
                ", leijcslx=" + leijcslx +
                ", meiqhkze=" + meiqhkze +
                ", zhanghye=" + zhanghye +
                '}';
    }
}
