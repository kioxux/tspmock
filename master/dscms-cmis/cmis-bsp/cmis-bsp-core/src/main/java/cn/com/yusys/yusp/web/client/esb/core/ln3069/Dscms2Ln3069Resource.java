package cn.com.yusys.yusp.web.client.esb.core.ln3069;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3069.req.Ln3069ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp.Ln3069RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3069.req.Ln3069ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3069.resp.Ln3069RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3069.resp.Lstzczrxy;
import cn.com.yusys.yusp.online.client.esb.core.ln3069.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3069)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3069Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3069Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3069
     * 交易描述：资产转让/证券化组合查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3069:资产转让/证券化组合查询")
    @PostMapping("/ln3069")
    protected @ResponseBody
    ResultDto<Ln3069RespDto> ln3069(@Validated @RequestBody Ln3069ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3069.key, EsbEnum.TRADE_CODE_LN3069.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3069.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3069.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3069.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3069.resp.Service();
        Ln3069ReqService ln3069ReqService = new Ln3069ReqService();
        Ln3069RespService ln3069RespService = new Ln3069RespService();
        Ln3069RespDto ln3069RespDto = new Ln3069RespDto();
        ResultDto<Ln3069RespDto> ln3069ResultDto = new ResultDto<Ln3069RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3069ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3069.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            List<cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp.Lstzczrxy> lstzczrxyList = new ArrayList<>();
            Lstzczrxy lstzczrxy = Optional.ofNullable(respService.getLstzczrxy()).orElse(new Lstzczrxy());
            if (CollectionUtils.nonEmpty(lstzczrxy.getRecordList())) {
                List<Record> recordList = lstzczrxy.getRecordList();
                for (Record record : recordList) {
                    cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp.Lstzczrxy lstzczrxydto = new cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp.Lstzczrxy();
                    BeanUtils.copyProperties(record, lstzczrxydto);
                    lstzczrxyList.add(lstzczrxydto);
                }
                ln3069RespDto.setLstzczrxy(lstzczrxyList);
            }
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3069ReqService.setService(reqService);
            // 将ln3069ReqService转换成ln3069ReqServiceMap
            Map ln3069ReqServiceMap = beanMapUtil.beanToMap(ln3069ReqService);
            context.put("tradeDataMap", ln3069ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3069.key, EsbEnum.TRADE_CODE_LN3069.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3069.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3069.key, EsbEnum.TRADE_CODE_LN3069.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3069RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3069RespService.class, Ln3069RespService.class);
            respService = ln3069RespService.getService();

            ln3069ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3069ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3069RespDto
                BeanUtils.copyProperties(respService, ln3069RespDto);

                ln3069ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3069ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3069ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3069ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3069.key, EsbEnum.TRADE_CODE_LN3069.value, e.getMessage());
            ln3069ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3069ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3069ResultDto.setData(ln3069RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3069.key, EsbEnum.TRADE_CODE_LN3069.value, JSON.toJSONString(ln3069ResultDto));
        return ln3069ResultDto;
    }
}
