package cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.resp;

/**
 * 响应Service：押品信息同步及引入
 *
 * @version 1.0
 */
public class XdypxxtbyrRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "XdypxxtbyrRespService{" +
                "service=" + service +
                '}';
    }
}
