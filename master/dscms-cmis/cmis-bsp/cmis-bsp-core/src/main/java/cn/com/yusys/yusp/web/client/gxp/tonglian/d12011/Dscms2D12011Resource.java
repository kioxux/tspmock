package cn.com.yusys.yusp.web.client.gxp.tonglian.d12011;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.D12011RespMessage;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Record;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d12011)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D12011Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D12011Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 账单交易明细查询（处理码d12011）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d12011:账单交易明细查询")
    @PostMapping("/d12011")
    protected @ResponseBody
    ResultDto<D12011RespDto> d12011(@Validated @RequestBody D12011ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12011.key, GxpEnum.TRADE_CODE_D12011.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.D12011ReqMessage d12011ReqMessage = null;//请求Message：账单交易明细查询
        D12011RespMessage d12011RespMessage = new D12011RespMessage();//响应Message：账单交易明细查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.Message reqMessage = null;//请求Message：账单交易明细查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Message();//响应Message：账单交易明细查询
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：账单交易明细查询
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：账单交易明细查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.Body();//请求Body：账单交易明细查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body();//响应Body：账单交易明细查询

        D12011RespDto d12011RespDto = new D12011RespDto();//响应Dto：账单交易明细查询
        ResultDto<D12011RespDto> d12011ResultDto = new ResultDto<>();//响应ResultDto：账单交易明细查询

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D12011.key, GxpEnum.TRADE_CODE_D12011.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D12011ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.Message::setBody, reqBody).build();
            // 给d12011ReqMessage赋值
            d12011ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.D12011ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req.D12011ReqMessage::setMessage, reqMessage).build();

            // 将d12011ReqService转换成d12011ReqServiceMap
            Map d12011ReqMessageMap = beanMapUtil.beanToMap(d12011ReqMessage);
            context.put("tradeDataMap", d12011ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12011.key, GxpEnum.TRADE_CODE_D12011.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D12011.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12011.key, GxpEnum.TRADE_CODE_D12011.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            Map<String, Object> MessageMap = (Map<String, Object>) tradeDataMap.get("message");
            //Map<String, Object> bodyMap = (Map<String, Object>) MessageMap.get("body");
            //cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body body = beanMapUtil.mapToBean(bodyMap, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body.class, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body.class);//响应Message：账单交易明细查询
            //Map<String, Object> listMap = (Map<String, Object>) bodyMap.get("list");
            //cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.List list = beanMapUtil.mapToBean(listMap, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.List.class, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.List.class);//响应Message：账单交易明细查询
            d12011RespMessage = beanMapUtil.mapToBean(tradeDataMap, D12011RespMessage.class, D12011RespMessage.class);//响应Message：账单交易明细查询
            respMessage = d12011RespMessage.getMessage();//响应Message：账单交易明细查询
            gxpRespHead = respMessage.getHead();//响应Head：账单交易明细查询
            //  将D12011RespDto封装到ResultDto<D12011RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                //  将respBody转换成D12011RespDto
                respBody = respMessage.getBody();
                BeanUtils.copyProperties(respBody, d12011RespDto);
                Map<String, Object> bodyMap = (Map<String, Object>) MessageMap.get("body");
                cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body body = beanMapUtil.mapToBean(bodyMap, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body.class, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.Body.class);//响应Message：账单交易明细查询
                Map<String, Object> listMap = (Map<String, Object>) bodyMap.get("list");
                cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.List list = beanMapUtil.mapToBean(listMap, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.List.class, cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.List.class);//响应Message：账单交易明细查询
                list = Optional.ofNullable(list).orElse(new cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp.List());
                if (CollectionUtils.nonEmpty(list.getRecord())) {
                    List<Record> listRecord = list.getRecord();
                    List<cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.List> targetList = new ArrayList<>();
                    for (Record record : listRecord) {
                        cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.List target = new cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.List();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    d12011RespDto.setList(targetList);
                }

                d12011ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d12011ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d12011ResultDto.setCode(EpbEnum.EPB099999.key);
                d12011ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d12011ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12011.key, GxpEnum.TRADE_CODE_D12011.value, e.getMessage());
            d12011ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d12011ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d12011ResultDto.setData(d12011RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12011.key, GxpEnum.TRADE_CODE_D12011.value, JSON.toJSONString(d12011ResultDto));
        return d12011ResultDto;
    }
}
