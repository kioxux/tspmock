package cn.com.yusys.yusp.web.client.esb.core.ln3169;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3169.Ln3169ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3169.Ln3169RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3169.req.Ln3169ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3169.resp.Ln3169RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3169)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3169Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3169Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3169
     * 交易描述：资产证券化处理文件导入
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3169:资产证券化处理文件导入")
    @PostMapping("/ln3169")
    protected @ResponseBody
    ResultDto<Ln3169RespDto> ln3169(@Validated @RequestBody Ln3169ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3169.key, EsbEnum.TRADE_CODE_LN3169.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3169.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3169.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3169.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3169.resp.Service();
        Ln3169ReqService ln3169ReqService = new Ln3169ReqService();
        Ln3169RespService ln3169RespService = new Ln3169RespService();
        Ln3169RespDto ln3169RespDto = new Ln3169RespDto();
        ResultDto<Ln3169RespDto> ln3169ResultDto = new ResultDto<Ln3169RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3169ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3169.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3169ReqService.setService(reqService);
            // 将ln3169ReqService转换成ln3169ReqServiceMap
            Map ln3169ReqServiceMap = beanMapUtil.beanToMap(ln3169ReqService);
            context.put("tradeDataMap", ln3169ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3169.key, EsbEnum.TRADE_CODE_LN3169.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3169.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3169.key, EsbEnum.TRADE_CODE_LN3169.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3169RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3169RespService.class, Ln3169RespService.class);
            respService = ln3169RespService.getService();

            ln3169ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3169ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3169RespDto
                BeanUtils.copyProperties(respService, ln3169RespDto);
                ln3169ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3169ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3169ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3169ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3169.key, EsbEnum.TRADE_CODE_LN3169.value, e.getMessage());
            ln3169ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3169ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3169ResultDto.setData(ln3169RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3169.key, EsbEnum.TRADE_CODE_LN3169.value, JSON.toJSONString(ln3169ResultDto));
        return ln3169ResultDto;
    }
}
