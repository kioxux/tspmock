package cn.com.yusys.yusp.web.client.esb.core.ln3046;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3046.Ln3046ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3046.Ln3046RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3046.req.Ln3046ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3046.resp.Ln3046RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3046)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3046Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3046Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3046
     * 交易描述：贷款核销归还
     *
     * @param ln3046ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3046:贷款核销归还")
    @PostMapping("/ln3046")
    protected @ResponseBody
    ResultDto<Ln3046RespDto> ln3046(@Validated @RequestBody Ln3046ReqDto ln3046ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3046.key, EsbEnum.TRADE_CODE_LN3046.value, JSON.toJSONString(ln3046ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3046.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3046.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3046.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3046.resp.Service();

        Ln3046ReqService ln3046ReqService = new Ln3046ReqService();
        Ln3046RespService ln3046RespService = new Ln3046RespService();
        Ln3046RespDto ln3046RespDto = new Ln3046RespDto();
        ResultDto<Ln3046RespDto> ln3046ResultDto = new ResultDto<Ln3046RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3046ReqDto转换成reqService
            BeanUtils.copyProperties(ln3046ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3046.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3046ReqService.setService(reqService);
            // 将ln3046ReqService转换成ln3046ReqServiceMap
            Map ln3046ReqServiceMap = beanMapUtil.beanToMap(ln3046ReqService);
            context.put("tradeDataMap", ln3046ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3046.key, EsbEnum.TRADE_CODE_LN3046.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3046.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3046.key, EsbEnum.TRADE_CODE_LN3046.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3046RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3046RespService.class, Ln3046RespService.class);
            respService = ln3046RespService.getService();

            ln3046ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3046ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3046RespDto
                BeanUtils.copyProperties(respService, ln3046RespDto);
                ln3046ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3046ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3046ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3046ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3046.key, EsbEnum.TRADE_CODE_LN3046.value, e.getMessage());
            ln3046ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3046ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3046ResultDto.setData(ln3046RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3046.key, EsbEnum.TRADE_CODE_LN3046.value, JSON.toJSONString(ln3046ResultDto));
        return ln3046ResultDto;
    }
}
