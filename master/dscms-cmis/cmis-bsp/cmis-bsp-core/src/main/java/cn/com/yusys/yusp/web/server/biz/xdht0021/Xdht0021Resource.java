package cn.com.yusys.yusp.web.server.biz.xdht0021;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0021.req.Xdht0021ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0021.resp.Xdht0021RespDto;
import cn.com.yusys.yusp.dto.server.xdht0021.req.Xdht0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0021.resp.Xdht0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同签订
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0021:合同签订")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0021Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0021
     * 交易描述：合同签订
     *
     * @param xdht0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订")
    @PostMapping("/xdht0021")
    //@Idempotent({"xdcaht0021", "#xdht0021ReqDto.datasq"})
    protected @ResponseBody
    Xdht0021RespDto xdht0021(@Validated @RequestBody Xdht0021ReqDto xdht0021ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021ReqDto));
        Xdht0021DataReqDto xdht0021DataReqDto = new Xdht0021DataReqDto();// 请求Data： 合同签订
        Xdht0021DataRespDto xdht0021DataRespDto = new Xdht0021DataRespDto();// 响应Data：合同签订
        Xdht0021RespDto xdht0021RespDto = new Xdht0021RespDto();
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdht0021.req.Data reqData = null; // 请求Data：合同签订
        cn.com.yusys.yusp.dto.server.biz.xdht0021.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0021.resp.Data();// 响应Data：合同签订
        //  此处包导入待确定 结束
        try {
            // 从 xdht0021ReqDto获取 reqData
            reqData = xdht0021ReqDto.getData();
            // 将 reqData 拷贝到xdht0021DataReqDto
            BeanUtils.copyProperties(reqData, xdht0021DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataReqDto));
            ResultDto<Xdht0021DataRespDto> xdht0021DataResultDto = dscmsBizHtClientService.xdht0021(xdht0021DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0021RespDto.setErorcd(Optional.ofNullable(xdht0021DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0021RespDto.setErortx(Optional.ofNullable(xdht0021DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0021DataResultDto.getCode())) {
                xdht0021DataRespDto = xdht0021DataResultDto.getData();
                xdht0021RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0021RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0021DataRespDto, respData);
            } else {
                xdht0021DataRespDto = xdht0021DataResultDto.getData();
                xdht0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                xdht0021RespDto.setErortx(xdht0021DataRespDto.getOpMsg());// 系统异常
                // 将 xdht0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0021DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, e.getMessage());
            xdht0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0021RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0021RespDto.setDatasq(servsq);

        xdht0021RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0021.key, DscmsEnum.TRADE_CODE_XDHT0021.value, JSON.toJSONString(xdht0021RespDto));
        return xdht0021RespDto;
    }
}
