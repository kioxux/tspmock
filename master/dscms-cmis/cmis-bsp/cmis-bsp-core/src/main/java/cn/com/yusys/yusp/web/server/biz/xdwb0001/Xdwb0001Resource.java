package cn.com.yusys.yusp.web.server.biz.xdwb0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdwb0001.req.Xdwb0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdwb0001.resp.Xdwb0001RespDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.req.Xdwb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdwb0001.resp.Xdwb0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizWbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类: 省联社金融服务平台借据信息查询接口
 *
 * @author zcrbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDWB0001:省联社金融服务平台借据信息查询接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdwb0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdwb0001Resource.class);

	@Autowired
	private DscmsBizWbClientService dscmsBizWbClientService;

    /**
     * 交易码：xdwb0001
     * 交易描述：省联社金融服务平台借据信息查询接口
     *
     * @param xdwb0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授权结果反馈接口")
    @PostMapping("/xdwb0001")
    @Idempotent({"xdwb0001", "#xdwb0001ReqDto.datasq"})
    protected @ResponseBody
    Xdwb0001RespDto xdwb0001(@Validated @RequestBody Xdwb0001ReqDto xdwb0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001ReqDto));
        Xdwb0001DataReqDto xdwb0001DataReqDto = new Xdwb0001DataReqDto();// 请求Data
        Xdwb0001DataRespDto xdwb0001DataRespDto = new Xdwb0001DataRespDto();// 响应Data
        cn.com.yusys.yusp.dto.server.biz.xdwb0001.req.Data reqData = null; // 请求Data
        cn.com.yusys.yusp.dto.server.biz.xdwb0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdwb0001.resp.Data();// 响应Data
        Xdwb0001RespDto xdwb0001RespDto = new Xdwb0001RespDto();
		try {
            // 从 xdwb0001ReqDto获取 reqData
            reqData = xdwb0001ReqDto.getData();
            // 将 reqData 拷贝到xdwb0001DataReqDto
            BeanUtils.copyProperties(reqData, xdwb0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataReqDto));
            ResultDto<Xdwb0001DataRespDto> xdwb0001DataResultDto = dscmsBizWbClientService.xdwb0001(xdwb0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdwb0001RespDto.setErorcd(Optional.ofNullable(xdwb0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdwb0001RespDto.setErortx(Optional.ofNullable(xdwb0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdwb0001DataResultDto.getCode())) {
                xdwb0001DataRespDto = xdwb0001DataResultDto.getData();
                xdwb0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdwb0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdwb0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdwb0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, e.getMessage());
            xdwb0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdwb0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdwb0001RespDto.setDatasq(servsq);

        xdwb0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDWB0001.key, DscmsEnum.TRADE_CODE_XDWB0001.value, JSON.toJSONString(xdwb0001RespDto));
        return xdwb0001RespDto;
    }
}
