package cn.com.yusys.yusp.web.client.esb.ecif.g00101;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g00101.req.G00101ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g00101.resp.G00101RespService;
import cn.com.yusys.yusp.online.client.esb.ecif.g00101.resp.Record;
import cn.com.yusys.yusp.online.client.esb.ecif.g00101.resp.RelArrayInfo;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g00101)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G00101Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G00101Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：g00101
     * 交易描述：对公客户综合信息查询
     *
     * @param g00101ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g00101:对公客户综合信息查询")
    @PostMapping("/g00101")
    protected @ResponseBody
    ResultDto<G00101RespDto> g00101(@Validated @RequestBody G00101ReqDto g00101ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value, JSON.toJSONString(g00101ReqDto));
        cn.com.yusys.yusp.online.client.esb.ecif.g00101.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g00101.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g00101.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g00101.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g00101.req.List list = new cn.com.yusys.yusp.online.client.esb.ecif.g00101.req.List();

        G00101ReqService g00101ReqService = new G00101ReqService();
        G00101RespService g00101RespService = new G00101RespService();
        G00101RespDto g00101RespDto = new G00101RespDto();
        ResultDto<G00101RespDto> g00101ResultDto = new ResultDto<G00101RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将g00101ReqDto转换成reqService
            BeanUtils.copyProperties(g00101ReqDto, reqService);

       /*     if (CollectionUtils.nonEmpty(g00101ReqDto.getRelArrayInfo())) {
                List<RelArrayInfo> relArrayInfos = Optional.ofNullable(g00101ReqDto.getRelArrayInfo()).orElseGet(() -> new ArrayList<>());
                List<Record> recordList = new ArrayList<Record>();
                // 遍历record传值塞入listArrayInfo
                for (RelArrayInfo relArrayInfo : relArrayInfos) {
                    cn.com.yusys.yusp.online.client.esb.ecif.g00101.req.Record record = new cn.com.yusys.yusp.online.client.esb.ecif.g00101.req.Record();
                    BeanUtils.copyProperties(relArrayInfo, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }*/

            reqService.setPrcscd(EsbEnum.TRADE_CODE_G00101.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间

            g00101ReqService.setService(reqService);
            // 将g00101ReqService转换成g00101ReqServiceMap
            Map g00101ReqServiceMap = beanMapUtil.beanToMap(g00101ReqService);
            context.put("tradeDataMap", g00101ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G00101.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g00101RespService = beanMapUtil.mapToBean(tradeDataMap, G00101RespService.class, G00101RespService.class);
            respService = g00101RespService.getService();

            g00101ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g00101ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G00101RespDto
                BeanUtils.copyProperties(respService, g00101RespDto);
                RelArrayInfo relArrayInfo = Optional.ofNullable(respService.getRelArrayInfo()).orElse(new RelArrayInfo());
                if (CollectionUtils.nonEmpty(relArrayInfo.getRecord())) {
                    List<Record> recordList = relArrayInfo.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.ecif.g00101.RelArrayInfo> targetList = new ArrayList<>();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.ecif.g00101.RelArrayInfo target = new cn.com.yusys.yusp.dto.client.esb.ecif.g00101.RelArrayInfo();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    g00101RespDto.setRelArrayInfo(targetList);
                }
                g00101ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g00101ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                g00101ResultDto.setCode(EpbEnum.EPB099999.key);
                g00101ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value, e.getMessage());
            g00101ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g00101ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g00101ResultDto.setData(g00101RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00101.key, EsbEnum.TRADE_CODE_G00101.value, JSON.toJSONString(g00101ResultDto));
        return g00101ResultDto;
    }

}
