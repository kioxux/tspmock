package cn.com.yusys.yusp.web.client.esb.rircp.fbxd08;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd08.Fbxd08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd08.Fbxd08RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.req.Fbxd08ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.resp.Fbxd08RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd08）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd08Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd08Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd08
     * 交易描述：查找（利翃）实还正常本金和逾期本金之和
     *
     * @param fbxd08ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd08")
    protected @ResponseBody
    ResultDto<Fbxd08RespDto> fbxd08(@Validated @RequestBody Fbxd08ReqDto fbxd08ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD08.key, EsbEnum.TRADE_CODE_FBXD08.value, JSON.toJSONString(fbxd08ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.resp.Service();
        Fbxd08ReqService fbxd08ReqService = new Fbxd08ReqService();
        Fbxd08RespService fbxd08RespService = new Fbxd08RespService();
        Fbxd08RespDto fbxd08RespDto = new Fbxd08RespDto();
        ResultDto<Fbxd08RespDto> fbxd08ResultDto = new ResultDto<Fbxd08RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd08ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd08ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD08.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            fbxd08ReqService.setService(reqService);
            // 将fbxd08ReqService转换成fbxd08ReqServiceMap
            Map fbxd08ReqServiceMap = beanMapUtil.beanToMap(fbxd08ReqService);
            context.put("tradeDataMap", fbxd08ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD08.key, EsbEnum.TRADE_CODE_FBXD08.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD08.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD08.key, EsbEnum.TRADE_CODE_FBXD08.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd08RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd08RespService.class, Fbxd08RespService.class);
            respService = fbxd08RespService.getService();

            fbxd08ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd08ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd08RespDto
                BeanUtils.copyProperties(respService, fbxd08RespDto);
                fbxd08ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd08ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd08ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd08ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD08.key, EsbEnum.TRADE_CODE_FBXD08.value, e.getMessage());
            fbxd08ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd08ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd08ResultDto.setData(fbxd08RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD08.key, EsbEnum.TRADE_CODE_FBXD08.value, JSON.toJSONString(fbxd08ResultDto));
        return fbxd08ResultDto;
    }
}
