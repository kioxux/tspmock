package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.resp;

/**
 * 响应Service：本异地借阅出库接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class Cwm001RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
