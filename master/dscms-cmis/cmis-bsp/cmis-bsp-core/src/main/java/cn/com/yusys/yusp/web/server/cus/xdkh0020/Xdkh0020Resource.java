package cn.com.yusys.yusp.web.server.cus.xdkh0020;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0020.req.Xdkh0020ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0020.resp.Xdkh0020RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.req.Xdkh0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0020.resp.Xdkh0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小微平台请求信贷线上开户
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDKH0020:小微平台请求信贷线上开户")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0020Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0020
     * 交易描述：小微平台请求信贷线上开户
     *
     * @param xdkh0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微平台请求信贷线上开户")
    @PostMapping("/xdkh0020")
    //@Idempotent({"xdcakh0020", "#xdkh0020ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0020RespDto xdkh0020(@Validated @RequestBody Xdkh0020ReqDto xdkh0020ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value, JSON.toJSONString(xdkh0020ReqDto));
        Xdkh0020DataReqDto xdkh0020DataReqDto = new Xdkh0020DataReqDto();// 请求Data： 小微平台请求信贷线上开户
        Xdkh0020DataRespDto xdkh0020DataRespDto = new Xdkh0020DataRespDto();// 响应Data：小微平台请求信贷线上开户
        Xdkh0020RespDto xdkh0020RespDto = new Xdkh0020RespDto();// 响应DTO：小微平台请求信贷线上开户
        cn.com.yusys.yusp.dto.server.cus.xdkh0020.req.Data reqData = null; // 请求Data：小微平台请求信贷线上开户
        cn.com.yusys.yusp.dto.server.cus.xdkh0020.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0020.resp.Data();// 响应Data：小微平台请求信贷线上开户
        try {
            // 从 xdkh0020ReqDto获取 reqData
            reqData = xdkh0020ReqDto.getData();
            // 将 reqData 拷贝到xdkh0020DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0020DataReqDto);
            // 调用服务：
            ResultDto<Xdkh0020DataRespDto> xdkh0020DataResultDto = dscmsCusClientService.xdkh0020(xdkh0020DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdkh0020RespDto.setErorcd(Optional.ofNullable(xdkh0020DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0020RespDto.setErortx(Optional.ofNullable(xdkh0020DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0020DataResultDto.getCode())) {
                xdkh0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0020DataRespDto = xdkh0020DataResultDto.getData();
                // 将 xdkh0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0020DataRespDto, respData);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value, e.getMessage());
            xdkh0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0020RespDto.setErortx(e.getMessage());// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value, e.getMessage());
            xdkh0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0020RespDto.setDatasq(servsq);
        xdkh0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0020.key, DscmsEnum.TRADE_CODE_XDKH0020.value, JSON.toJSONString(xdkh0020RespDto));
        return xdkh0020RespDto;
    }
}
