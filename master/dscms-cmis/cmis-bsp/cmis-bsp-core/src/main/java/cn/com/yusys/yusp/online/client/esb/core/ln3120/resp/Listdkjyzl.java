package cn.com.yusys.yusp.online.client.esb.core.ln3120.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/31 15:07
 * @since 2021/5/31 15:07
 */
public class Listdkjyzl {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3120.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Listdkjyzl{" +
                "record=" + record +
                '}';
    }
}
