package cn.com.yusys.yusp.web.client.esb.circp.fb1147;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.req.Fb1147ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.resp.Fb1147RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1147.req.Fb1147ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1147.resp.Fb1147RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1147）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1147Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1147Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1147
     * 交易描述：无还本续贷申请
     *
     * @param fb1147ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1147")
    protected @ResponseBody
    ResultDto<Fb1147RespDto> fb1147(@Validated @RequestBody Fb1147ReqDto fb1147ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value, JSON.toJSONString(fb1147ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1147.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1147.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1147.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1147.resp.Service();
        Fb1147ReqService fb1147ReqService = new Fb1147ReqService();
        Fb1147RespService fb1147RespService = new Fb1147RespService();
        Fb1147RespDto fb1147RespDto = new Fb1147RespDto();
        ResultDto<Fb1147RespDto> fb1147ResultDto = new ResultDto<Fb1147RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1147ReqDto转换成reqService
            BeanUtils.copyProperties(fb1147ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1147.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1147ReqService.setService(reqService);
            // 将fb1147ReqService转换成fb1147ReqServiceMap
            Map fb1147ReqServiceMap = beanMapUtil.beanToMap(fb1147ReqService);
            context.put("tradeDataMap", fb1147ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1147.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1147RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1147RespService.class, Fb1147RespService.class);
            respService = fb1147RespService.getService();

            fb1147ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1147ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1147RespDto
                BeanUtils.copyProperties(respService, fb1147RespDto);

                fb1147ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1147ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1147ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1147ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value, e.getMessage());
            fb1147ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1147ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1147ResultDto.setData(fb1147RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1147.key, EsbEnum.TRADE_CODE_FB1147.value, JSON.toJSONString(fb1147ResultDto));
        return fb1147ResultDto;
    }
}
