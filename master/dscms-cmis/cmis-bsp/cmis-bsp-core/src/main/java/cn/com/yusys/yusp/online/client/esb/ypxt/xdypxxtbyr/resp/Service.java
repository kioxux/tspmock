package cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.resp;

/**
 * 响应Service：押品信息同步及引入
 *
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码,0000表示成功其它表示失败
    private String erortx; // 响应信息,失败详情
    private String returnCode; // 处理结果
    private String returnInfo; // 处理信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnInfo() {
        return returnInfo;
    }

    public void setReturnInfo(String returnInfo) {
        this.returnInfo = returnInfo;
    }

    private String guar_no;//权证编号及其他编号

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", returnInfo='" + returnInfo + '\'' +
                ", guar_no='" + guar_no + '\'' +
                '}';
    }
}
