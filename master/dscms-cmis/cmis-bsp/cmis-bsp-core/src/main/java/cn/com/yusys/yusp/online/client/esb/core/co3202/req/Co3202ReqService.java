package cn.com.yusys.yusp.online.client.esb.core.co3202.req;

/**
 * 请求Service：抵质押物出入库处理
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 17:12
 */
public class Co3202ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
