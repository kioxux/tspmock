package cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.resp;

import java.math.BigDecimal;

/**
 * 响应Service：缴存信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private String xybhsq;//协议编号
    private String qydm;//区域代码
    private BigDecimal tranam;//存款金额
    private BigDecimal yjcje;//已缴存金额
    private BigDecimal syjcje;//剩余缴存金额
    private String acctno;//买方账号
    private String acctsa;//买方户名
    private String acctba;//资金托管账号
    private String dataid;//资金托管账户名

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    public String getQydm() {
        return qydm;
    }

    public void setQydm(String qydm) {
        this.qydm = qydm;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public BigDecimal getYjcje() {
        return yjcje;
    }

    public void setYjcje(BigDecimal yjcje) {
        this.yjcje = yjcje;
    }

    public BigDecimal getSyjcje() {
        return syjcje;
    }

    public void setSyjcje(BigDecimal syjcje) {
        this.syjcje = syjcje;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctsa() {
        return acctsa;
    }

    public void setAcctsa(String acctsa) {
        this.acctsa = acctsa;
    }

    public String getAcctba() {
        return acctba;
    }

    public void setAcctba(String acctba) {
        this.acctba = acctba;
    }

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", xybhsq='" + xybhsq + '\'' +
                ", qydm='" + qydm + '\'' +
                ", tranam=" + tranam +
                ", yjcje=" + yjcje +
                ", syjcje=" + syjcje +
                ", acctno='" + acctno + '\'' +
                ", acctsa='" + acctsa + '\'' +
                ", acctba='" + acctba + '\'' +
                ", dataid='" + dataid + '\'' +
                '}';
    }
}
