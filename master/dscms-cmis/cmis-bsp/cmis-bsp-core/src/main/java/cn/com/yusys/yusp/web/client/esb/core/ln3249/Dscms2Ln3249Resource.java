package cn.com.yusys.yusp.web.client.esb.core.ln3249;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3249.req.Ln3249ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3249.resp.Ln3249RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3249.req.Ln3249ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3249.resp.Ln3249RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3249)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3249Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3249Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();


    /**
     * 委托清收变更
     *
     * @param reqDto Ln3249ReqDto
     * @return ResultDto<Ln3249RespDto>
     * @throws Exception
     */
    @ApiOperation("ln3249:贷款指定日期利息试算")
    @PostMapping("/ln3249")
    protected @ResponseBody
    ResultDto<Ln3249RespDto> ln3249(@Validated @RequestBody Ln3249ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3249.key, EsbEnum.TRADE_CODE_LN3249.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3249.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3249.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3249.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3249.resp.Service();
        Ln3249ReqService ln3249ReqService = new Ln3249ReqService();
        Ln3249RespService ln3249RespService = new Ln3249RespService();
        Ln3249RespDto ln3249RespDto = new Ln3249RespDto();
        ResultDto<Ln3249RespDto> ln3249ResultDto = new ResultDto<Ln3249RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {

            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3249.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3249ReqService.setService(reqService);
            // 将ln3249ReqService转换成ln3249ReqServiceMap
            Map ln3249ReqServiceMap = beanMapUtil.beanToMap(ln3249ReqService);
            context.put("tradeDataMap", ln3249ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3249.key, EsbEnum.TRADE_CODE_LN3249.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3249.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3249.key, EsbEnum.TRADE_CODE_LN3249.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3249RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3249RespService.class, Ln3249RespService.class);
            respService = ln3249RespService.getService();

            //  将Ln3249RespDto封装到ResultDto<Ln3249RespDto>
            ln3249ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3249ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3249RespDto
                BeanUtils.copyProperties(respService, ln3249RespDto);

                ln3249ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3249ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3249ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3249ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3249.key, EsbEnum.TRADE_CODE_LN3249.value, e.getMessage());
            ln3249ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3249ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3249ResultDto.setData(ln3249RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3249.key, EsbEnum.TRADE_CODE_LN3249.value, JSON.toJSONString(ln3249ResultDto));

        return ln3249ResultDto;
    }
}
