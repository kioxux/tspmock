package cn.com.yusys.yusp.web.server.biz.xdht0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0016.req.Xdht0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0016.resp.Xdht0016RespDto;
import cn.com.yusys.yusp.dto.server.xdht0016.req.Xdht0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0016.resp.Xdht0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同签订维护（优抵贷不见面抵押签约）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0016:合同签订维护（优抵贷不见面抵押签约）")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0016Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0016
     * 交易描述：合同签订维护（优抵贷不见面抵押签约）
     *
     * @param xdht0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订维护（优抵贷不见面抵押签约）")
    @PostMapping("/xdht0016")
    //@Idempotent({"xdcaht0016", "#xdht0016ReqDto.datasq"})
    protected @ResponseBody
    Xdht0016RespDto xdht0016(@Validated @RequestBody Xdht0016ReqDto xdht0016ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016ReqDto));
        Xdht0016DataReqDto xdht0016DataReqDto = new Xdht0016DataReqDto();// 请求Data： 合同签订维护（优抵贷不见面抵押签约）
        Xdht0016DataRespDto xdht0016DataRespDto = new Xdht0016DataRespDto();// 响应Data：合同签订维护（优抵贷不见面抵押签约）
        Xdht0016RespDto xdht0016RespDto = new Xdht0016RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0016.req.Data reqData = null; // 请求Data：合同签订维护（优抵贷不见面抵押签约）
        cn.com.yusys.yusp.dto.server.biz.xdht0016.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0016.resp.Data();// 响应Data：合同签订维护（优抵贷不见面抵押签约）
        try {
            // 从 xdht0016ReqDto获取 reqData
            reqData = xdht0016ReqDto.getData();
            // 将 reqData 拷贝到xdht0016DataReqDto
            BeanUtils.copyProperties(reqData, xdht0016DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataReqDto));
            ResultDto<Xdht0016DataRespDto> xdht0016DataResultDto = dscmsBizHtClientService.xdht0016(xdht0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0016RespDto.setErorcd(Optional.ofNullable(xdht0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0016RespDto.setErortx(Optional.ofNullable(xdht0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0016DataResultDto.getCode())) {
                xdht0016DataRespDto = xdht0016DataResultDto.getData();
                xdht0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0016DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, e.getMessage());
            xdht0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0016RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0016RespDto.setDatasq(servsq);

        xdht0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0016.key, DscmsEnum.TRADE_CODE_XDHT0016.value, JSON.toJSONString(xdht0016RespDto));
        return xdht0016RespDto;
    }
}
