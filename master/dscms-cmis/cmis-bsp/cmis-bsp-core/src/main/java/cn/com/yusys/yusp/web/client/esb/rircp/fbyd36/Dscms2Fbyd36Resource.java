package cn.com.yusys.yusp.web.client.esb.rircp.fbyd36;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.req.Fbyd36ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.resp.Fbyd36RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.req.Fbyd36ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.Fbyd36RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.Record;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 接口处理类:授信列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbyd36Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Fbyd36Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbyd36
     * 交易描述：授信列表查询
     *
     * @param fbyd36ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbyd36")
    protected @ResponseBody
    ResultDto<Fbyd36RespDto> fbyd36(@Validated @RequestBody Fbyd36ReqDto fbyd36ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD36.key, EsbEnum.TRADE_CODE_FBYD36.value, JSON.toJSONString(fbyd36ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.Service();
        Fbyd36ReqService fbyd36ReqService = new Fbyd36ReqService();
        Fbyd36RespService fbyd36RespService = new Fbyd36RespService();
        Fbyd36RespDto fbyd36RespDto = new Fbyd36RespDto();
        ResultDto<Fbyd36RespDto> fbyd36ResultDto = new ResultDto<Fbyd36RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbyd36ReqDto转换成reqService
            BeanUtils.copyProperties(fbyd36ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBYD36.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbyd36ReqService.setService(reqService);
            // 将fbyd36ReqService转换成fbyd36ReqServiceMap
            Map fbyd36ReqServiceMap = beanMapUtil.beanToMap(fbyd36ReqService);
            context.put("tradeDataMap", fbyd36ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD36.key, EsbEnum.TRADE_CODE_FBYD36.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBYD36.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD36.key, EsbEnum.TRADE_CODE_FBYD36.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbyd36RespService = beanMapUtil.mapToBean(tradeDataMap, Fbyd36RespService.class, Fbyd36RespService.class);
            respService = fbyd36RespService.getService();
            //  将respService转换成Fbyd36RespDto
            fbyd36ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            fbyd36ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成fbyd02RespDto
                BeanUtils.copyProperties(respService, fbyd36RespDto);
                List<cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.resp.List> targetList = new ArrayList<>();
                final cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.List list = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.List());
                if (CollectionUtils.nonEmpty(list.getRecord())) {
                    cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.resp.List target = new cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.resp.List();
                    List<cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.Record> recordList = list.getRecord();
                    for (cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp.Record record : recordList) {
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    fbyd36RespDto.setList(targetList);
                }
                fbyd36ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbyd36ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbyd36ResultDto.setCode(EpbEnum.EPB099999.key);
                fbyd36ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD36.key, EsbEnum.TRADE_CODE_FBYD36.value, e.getMessage());
            fbyd36ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbyd36ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbyd36ResultDto.setData(fbyd36RespDto);
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD36.key, EsbEnum.TRADE_CODE_FBYD36.value, JSON.toJSONString(fbyd36ReqDto));
        return fbyd36ResultDto;
    }
}
