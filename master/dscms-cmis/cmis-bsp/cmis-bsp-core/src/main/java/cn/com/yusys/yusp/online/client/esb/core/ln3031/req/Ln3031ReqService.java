package cn.com.yusys.yusp.online.client.esb.core.ln3031.req;

/**
 * 请求Service：贷款资料维护（关于还款）
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3031ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3031ReqService{" +
                "service=" + service +
                '}';
    }
}
