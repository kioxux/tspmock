package cn.com.yusys.yusp.web.client.http.outerdata.slno01;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.outerdata.slno01.req.Slno01ReqService;
import cn.com.yusys.yusp.online.client.http.outerdata.slno01.resp.Data;
import cn.com.yusys.yusp.online.client.http.outerdata.slno01.resp.Slno01RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import tk.mybatis.mapper.util.StringUtil;

import java.util.Objects;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(slno01)")
@RestController
@RequestMapping("/api/dscms2outerdata")
public class Dscms2Slno01Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Slno01Resource.class);
    @Autowired
    private RestTemplate restTemplate;

    @Value("${application.sl.url}")
    private String slUrl;

    /**
     * 双录流水号
     *
     * @param slno01ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("slno01:双录流水号")
    @PostMapping("/slno01")
    protected @ResponseBody
    ResultDto<Slno01RespDto> slno01(@Validated @RequestBody Slno01ReqDto slno01ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, JSON.toJSONString(slno01ReqDto));

        Slno01ReqService slno01ReqService = new Slno01ReqService();
        Slno01RespService slno01RespService = new Slno01RespService();
        Slno01RespDto slno01RespDto = new Slno01RespDto();
        ResultDto<Slno01RespDto> slno01ResultDto = new ResultDto<Slno01RespDto>();
        try {
            // 将Slno01ReqDto转换成Slno01ReqService
            BeanUtils.copyProperties(slno01ReqDto, slno01ReqService);
            slno01ReqService.setPrcscd(EsbEnum.TRADE_CODE_SLNO01.key);//    交易码
            slno01ReqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            slno01ReqService.setServsq(servsq);//    渠道流水
            slno01ReqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            slno01ReqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号

/*            HttpHeaders headers = new HttpHeaders();
//            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(slno01ReqService), headers);
            MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
//            map.add("jytype", slno01ReqService.getJytype());
//            map.add("guarid", slno01ReqService.getGuarid());
//            map.add("prcscd", slno01ReqService.getPrcscd());
//            map.add("prcscd", "slno01");
            map.add("msg_type", "check_exist");
            map.add("serial_number", slno01ReqService.getYxlsno());
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
            //String url = "http://10.28.121.196:8080/yxgcgl/EsbSocketApiController/getEsbApiResultForXML";// TODO SIT环境的测试URL
            //String url = slUrl + "/yxgcgl/EsbSocketApiController/getEsbApiResultForXML";
            //String url = "http://10.28.121.32:80" + "/wservice.action?jsonStr=";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, JSON.toJSONString(map));
            ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class, map);
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, responseEntity);*/


            //调用双录平台
            //String url = "http://10.28.121.32:80" + "/wservice.action?jsonStr=";//测试地址
            String url = slUrl + "/wservice.action?jsonStr=";//cmis-trade-application.yml 配置地址
            //请求参数
            JSONObject paraJson = new JSONObject();
            paraJson.put("msg_type", "queryVideoData");
            paraJson.put("xdserno", slno01ReqService.getYxlsno());
            paraJson.put("ywserno", slno01ReqService.getGuarid());
            String json = paraJson.toJSONString();
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, url + json);
            String responseEntity = restTemplate.getForObject(url + json, String.class, json);
            responseEntity = new String(responseEntity.getBytes("ISO8859-1"), "utf-8");
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, responseEntity);

            String yxzjls = "";//双录流水
            String insert_time = "";//插入时间
            JSONObject jsonObject = JSON.parseObject(responseEntity);
            String status = jsonObject.get("status").toString();//返回状态
            if ("0".equals(status)) {
                String data = jsonObject.get("data").toString();
                JSONArray dataObject = JSON.parseArray(data);
                for (int i = 0; i < dataObject.size(); i++) {
                    JSONObject data1 = dataObject.getJSONObject(0);
                    yxzjls = data1.getString("slyxserno");//获取影像流水
                    insert_time = data1.getString("insert_time");
                }
            }

            //将Slno01RespDto封装到ResultDto<Slno01RespDto>
            Data slno01RespData = new Data();
            if (StringUtil.isNotEmpty(status)) {
                slno01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                slno01ResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
                slno01RespData.setGuarno("");
                slno01RespData.setYxzjls(yxzjls);
                slno01RespData.setInsert_time(insert_time);
                BeanUtils.copyProperties(slno01RespData, slno01RespDto);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, e.getMessage());
            slno01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            slno01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        slno01ResultDto.setData(slno01RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SLNO01.key, EsbEnum.TRADE_CODE_SLNO01.value, JSON.toJSONString(slno01ResultDto));
        return slno01ResultDto;
    }
}
