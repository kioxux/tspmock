package cn.com.yusys.yusp.online.client.esb.gaps.idchek.resp;

/**
 * 响应Service：身份证核查
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述
    private String idcard;//身份证号
    private String idname;//姓名
    private String result;//核查结果
    private String office;//签发机关
    private String infotx;//图片返回

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getIdcard() {
        return idcard;
    }

    public void setIdcard(String idcard) {
        this.idcard = idcard;
    }

    public String getIdname() {
        return idname;
    }

    public void setIdname(String idname) {
        this.idname = idname;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getInfotx() {
        return infotx;
    }

    public void setInfotx(String infotx) {
        this.infotx = infotx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "idcard='" + idcard + '\'' +
                "idname='" + idname + '\'' +
                "result='" + result + '\'' +
                "office='" + office + '\'' +
                "infotx='" + infotx + '\'' +
                '}';
    }
}
