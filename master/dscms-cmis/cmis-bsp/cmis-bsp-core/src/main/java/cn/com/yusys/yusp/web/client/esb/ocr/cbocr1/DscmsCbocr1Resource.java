package cn.com.yusys.yusp.web.client.esb.ocr.cbocr1;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;

import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.Cbocr1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.resp.Cbocr1RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;

import cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.Cbocr1ReqService;
import cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp.Cbocr1RespService;
import cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp.Record;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:新增批次报表数据
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2ocr")
public class DscmsCbocr1Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsCbocr1Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：cbocr1
     * 交易描述：新增批次报表数据
     *
     * @param cbocr1ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/cbocr1")
    protected @ResponseBody
    ResultDto<Cbocr1RespDto> cbocr1(@Validated @RequestBody Cbocr1ReqDto cbocr1ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value, JSON.toJSONString(cbocr1ReqDto));
        cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.Service();
        cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp.Service();

        Cbocr1ReqService cbocr1ReqService = new Cbocr1ReqService();
        Cbocr1RespService cbocr1RespService = new Cbocr1RespService();
        Cbocr1RespDto cbocr1RespDto = new Cbocr1RespDto();
        ResultDto<Cbocr1RespDto> cbocr1ResultDto = new ResultDto<Cbocr1RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将cbocr1ReqDto转换成reqService
            BeanUtils.copyProperties(cbocr1ReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.List list = new cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.List();
            if (CollectionUtils.nonEmpty(cbocr1ReqDto.getList())) {
                List<cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List> originList = cbocr1ReqDto.getList();
                List<cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.Record> targetList = new ArrayList();
                for (cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.List origin : originList) {
                    cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.Record target = new cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req.Record();
                    BeanUtils.copyProperties(origin, target);
                    targetList.add(target);
                }
                list.setRecord(targetList);
                reqService.setList(list);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CBOCR1.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            cbocr1ReqService.setService(reqService);
            // 将cbocr1ReqService转换成cbocr1ReqServiceMap
            Map cbocr1ReqServiceMap = beanMapUtil.beanToMap(cbocr1ReqService);
            context.put("tradeDataMap", cbocr1ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CBOCR1.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cbocr1RespService = beanMapUtil.mapToBean(tradeDataMap, Cbocr1RespService.class, Cbocr1RespService.class);
            respService = cbocr1RespService.getService();

            if (Objects.equals(SuccessEnum.OCR_SUCCESS_CODE.key, respService.getErorcd())) {
                //  将respService转换成Cbocr1RespDto
                BeanUtils.copyProperties(respService, cbocr1RespDto);
                
                cbocr1ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cbocr1ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cbocr1ResultDto.setCode(EpbEnum.EPB099999.key);
                cbocr1ResultDto.setMessage(respService.getErortx());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value, e.getMessage());
            cbocr1ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cbocr1ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cbocr1ResultDto.setData(cbocr1RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value, JSON.toJSONString(cbocr1ResultDto));
        return cbocr1ResultDto;
    }
}
