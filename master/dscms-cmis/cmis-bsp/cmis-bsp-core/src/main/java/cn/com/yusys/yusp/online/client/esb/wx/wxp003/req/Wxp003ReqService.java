package cn.com.yusys.yusp.online.client.esb.wx.wxp003.req;

/**
 * 请求Service：信贷将放款标识推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
public class Wxp003ReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}                      
