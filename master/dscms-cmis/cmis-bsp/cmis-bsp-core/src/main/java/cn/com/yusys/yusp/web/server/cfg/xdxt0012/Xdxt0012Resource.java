package cn.com.yusys.yusp.web.server.cfg.xdxt0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0012.req.Xdxt0012ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0012.resp.Xdxt0012RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdxt0012.req.Xdxt0012DataReqDto;
import cn.com.yusys.yusp.server.xdxt0012.resp.Xdxt0012DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0012:根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0012Resource.class);
    @Autowired
    private DscmsCfgXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0012
     * 交易描述：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
     *
     * @param xdxt0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码")
    @PostMapping("/xdxt0012")
    //@Idempotent({"xdcaxt0012", "#xdxt0012ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0012RespDto xdxt0012(@Validated @RequestBody Xdxt0012ReqDto xdxt0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012ReqDto));
        Xdxt0012DataReqDto xdxt0012DataReqDto = new Xdxt0012DataReqDto();// 请求Data： 根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
        Xdxt0012DataRespDto xdxt0012DataRespDto = new Xdxt0012DataRespDto();// 响应Data：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
        Xdxt0012RespDto xdxt0012RespDto = new Xdxt0012RespDto();
        cn.com.yusys.yusp.dto.server.cfg.xdxt0012.req.Data reqData = null; // 请求Data：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码
        cn.com.yusys.yusp.dto.server.cfg.xdxt0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdxt0012.resp.Data();// 响应Data：根据（乡镇/街道+居委会/村委会） 组合 获取信贷行政区域代码

        try {
            // 从 xdxt0012ReqDto获取 reqData
            reqData = xdxt0012ReqDto.getData();
            // 将 reqData 拷贝到xdxt0012DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0012DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataReqDto));
            ResultDto<Xdxt0012DataRespDto> xdxt0012DataResultDto = dscmsXtClientService.xdxt0012(xdxt0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0012RespDto.setErorcd(Optional.ofNullable(xdxt0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0012RespDto.setErortx(Optional.ofNullable(xdxt0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0012DataResultDto.getCode())) {
                xdxt0012DataRespDto = xdxt0012DataResultDto.getData();
                xdxt0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, e.getMessage());
            xdxt0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0012RespDto.setDatasq(servsq);

        xdxt0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0012.key, DscmsEnum.TRADE_CODE_XDXT0012.value, JSON.toJSONString(xdxt0012RespDto));
        return xdxt0012RespDto;
    }
}
