package cn.com.yusys.yusp.online.client.esb.core.ln3078.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.List;

/**
 * 请求Service：贷款机构变更
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String daikczbz;//业务操作标志
    private String yewubhao;//业务编号
    private String zhrujgou;//转入机构
    private String zhchjgou;//转出机构
    private String dkgljgou;//贷款管理机构
    private String jgzhyibz;//机构转移标志
    private String shifoudy;//是否打印
    private cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.List list;//借据信息[LIST]

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public String getZhrujgou() {
        return zhrujgou;
    }

    public void setZhrujgou(String zhrujgou) {
        this.zhrujgou = zhrujgou;
    }

    public String getZhchjgou() {
        return zhchjgou;
    }

    public void setZhchjgou(String zhchjgou) {
        this.zhchjgou = zhchjgou;
    }

    public String getDkgljgou() {
        return dkgljgou;
    }

    public void setDkgljgou(String dkgljgou) {
        this.dkgljgou = dkgljgou;
    }

    public String getJgzhyibz() {
        return jgzhyibz;
    }

    public void setJgzhyibz(String jgzhyibz) {
        this.jgzhyibz = jgzhyibz;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", yewubhao='" + yewubhao + '\'' +
                ", zhrujgou='" + zhrujgou + '\'' +
                ", zhchjgou='" + zhchjgou + '\'' +
                ", dkgljgou='" + dkgljgou + '\'' +
                ", jgzhyibz='" + jgzhyibz + '\'' +
                ", shifoudy='" + shifoudy + '\'' +
                ", list=" + list +
                '}';
    }
}
