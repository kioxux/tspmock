package cn.com.yusys.yusp.online.client.esb.core.ln3026.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.lstytczfe.Record;

import java.util.List;

/**
 * 银团出资份额定价计息复合类型
 *
 * @author lihh
 * @version 1.0
 */
public class Lstytczfe_ARRAY {
    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstytczfe{" +
                "record=" + record +
                '}';
    }
}
