package cn.com.yusys.yusp.online.client.esb.circp.fb1147.resp;

/**
 * 响应Service：无还本续贷申请
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1147RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1147RespService{" +
                "service=" + service +
                '}';
    }
}
