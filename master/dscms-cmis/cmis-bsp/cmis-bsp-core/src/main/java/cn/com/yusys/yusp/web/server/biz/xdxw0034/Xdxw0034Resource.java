package cn.com.yusys.yusp.web.server.biz.xdxw0034;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0034.req.Xdxw0034ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0034.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0034.resp.Xdxw0034RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0034.req.Xdxw0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0034.resp.Xdxw0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据流水号查询无还本续贷基本信息
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0034:根据流水号查询无还本续贷基本信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0034Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0034
     * 交易描述：根据流水号查询无还本续贷基本信息
     *
     * @param xdxw0034ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询无还本续贷基本信息")
    @PostMapping("/xdxw0034")
    //@Idempotent({"xdcaxw0034", "#xdxw0034ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0034RespDto xdxw0034(@Validated @RequestBody Xdxw0034ReqDto xdxw0034ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034ReqDto));
        Xdxw0034DataReqDto xdxw0034DataReqDto = new Xdxw0034DataReqDto();// 请求Data： 根据流水号查询无还本续贷基本信息
        Xdxw0034DataRespDto xdxw0034DataRespDto = new Xdxw0034DataRespDto();// 响应Data：根据流水号查询无还本续贷基本信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0034.req.Data reqData = null; // 请求Data：根据流水号查询无还本续贷基本信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0034.resp.Data respData = new Data();// 响应Data：根据流水号查询无还本续贷基本信息
		Xdxw0034RespDto xdxw0034RespDto = new Xdxw0034RespDto();
		try {
            // 从 xdxw0034ReqDto获取 reqData
            reqData = xdxw0034ReqDto.getData();
            // 将 reqData 拷贝到xdxw0034DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0034DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034DataReqDto));
            ResultDto<Xdxw0034DataRespDto> xdxw0034DataResultDto = dscmsBizXwClientService.xdxw0034(xdxw0034DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0034RespDto.setErorcd(Optional.ofNullable(xdxw0034DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0034RespDto.setErortx(Optional.ofNullable(xdxw0034DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0034DataResultDto.getCode())) {
                xdxw0034DataRespDto = xdxw0034DataResultDto.getData();
                xdxw0034RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0034RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0034DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0034DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, e.getMessage());
            xdxw0034RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0034RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0034RespDto.setDatasq(servsq);

        xdxw0034RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0034.key, DscmsEnum.TRADE_CODE_XDXW0034.value, JSON.toJSONString(xdxw0034RespDto));
        return xdxw0034RespDto;
    }
}
