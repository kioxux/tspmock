package cn.com.yusys.yusp.web.client.esb.gcyx.yxgc01;


import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.req.Yxgc01ReqService;
import cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp.Yxgc01RespService;
import cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.req.BdcqcxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.resp.BdcqcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用过程营销平台的接口
 */
@Api(tags = "BSP封装调用过程营销平台的接口(yxgc01)")
@RestController
@RequestMapping("/api/dscms2gcyxpt")
public class Dscms2Yxgc01Resource {
    private static Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.client.esb.gcyx.yxgc01.Dscms2Yxgc01Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 贷后任务推送（处理码yxgc01）
     *
     * @return
     * @throws Exception
     */
    @ApiOperation("yxgc01:贷后任务推送")
    @PostMapping("/yxgc01")
    protected @ResponseBody
    ResultDto<Yxgc01RespDto> yxgc01(@Validated @RequestBody Yxgc01ReqDto yxgc01ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXGC01.key, EsbEnum.TRADE_CODE_YXGC01.value, JSON.toJSONString(yxgc01ReqDto));
        cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.req.Service();
        cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp.Service();
        Yxgc01ReqService yxgc01ReqService = new Yxgc01ReqService();
        Yxgc01RespService yxgc01RespService = new Yxgc01RespService();
        Yxgc01RespDto yxgc01RespDto = new Yxgc01RespDto();
        ResultDto<Yxgc01RespDto> yxgc01ResultDto = new ResultDto<Yxgc01RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将Yxgc01ReqDto转换成reqService
            BeanUtils.copyProperties(yxgc01ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_YXGC01.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            yxgc01ReqService.setService(reqService);
            // 将yxgc01ReqService转换成yxgc01ReqServiceMap
            Map yxgc01ReqServiceMap = beanMapUtil.beanToMap(yxgc01ReqService);
            context.put("tradeDataMap", yxgc01ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXGC01.key, EsbEnum.TRADE_CODE_YXGC01.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_YXGC01.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXGC01.key, EsbEnum.TRADE_CODE_YXGC01.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            yxgc01RespService = beanMapUtil.mapToBean(tradeDataMap, Yxgc01RespService.class, Yxgc01RespService.class);
            respService = yxgc01RespService.getService();

            //  将Yxgc01RespDto封装到ResultDto<Yxgc01RespDto>
            yxgc01ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            yxgc01ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Yxgc01RespDto
                BeanUtils.copyProperties(respService, yxgc01RespDto);
                yxgc01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                yxgc01ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                yxgc01ResultDto.setCode(EpbEnum.EPB099999.key);
                yxgc01ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXGC01.key, EsbEnum.TRADE_CODE_YXGC01.value, e.getMessage());
            yxgc01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            yxgc01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        yxgc01ResultDto.setData(yxgc01RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXGC01.key, EsbEnum.TRADE_CODE_YXGC01.value, JSON.toJSONString(yxgc01ResultDto));
        return yxgc01ResultDto;
    }
}
