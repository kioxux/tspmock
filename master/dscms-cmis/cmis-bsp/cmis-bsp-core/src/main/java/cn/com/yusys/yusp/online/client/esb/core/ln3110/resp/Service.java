package cn.com.yusys.yusp.online.client.esb.core.ln3110.resp;

import java.math.BigDecimal;

/**
 * 响应Service：用于贷款放款前进行还款计划试算
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private Integer zongqish;//总期数
    private BigDecimal leijlxsr;//累计利息收入
    private String huankfsh;//还款方式
    private String dechligz;//等额处理规则
    private Lstdkhk_ARRAY lstdkhk_ARRAY;
    private Lstdzqgjh_ARRAY lstdzqgjh_ARRAY;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Integer getZongqish() {
        return zongqish;
    }

    public void setZongqish(Integer zongqish) {
        this.zongqish = zongqish;
    }

    public BigDecimal getLeijlxsr() {
        return leijlxsr;
    }

    public void setLeijlxsr(BigDecimal leijlxsr) {
        this.leijlxsr = leijlxsr;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }


    public Lstdkhk_ARRAY getLstdkhk_ARRAY() {
        return lstdkhk_ARRAY;
    }

    public void setLstdkhk_ARRAY(Lstdkhk_ARRAY lstdkhk_ARRAY) {
        this.lstdkhk_ARRAY = lstdkhk_ARRAY;
    }
}
