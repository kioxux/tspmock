package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerseizurelist;

import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/7/9 16:11:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 16:11
 * @since 2021/7/9 16:11
 */
public class Registerseizurelist {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerseizurelist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Registerseizurelist{" +
                "record=" + record +
                '}';
    }
}
