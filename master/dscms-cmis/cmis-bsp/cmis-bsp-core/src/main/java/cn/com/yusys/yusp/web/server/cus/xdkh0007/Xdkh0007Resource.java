package cn.com.yusys.yusp.web.server.cus.xdkh0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0007.req.Xdkh0007ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0007.resp.Xdkh0007RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.req.Xdkh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0007.resp.Xdkh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户是否本行员工及直属亲属
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0007:查询客户是否本行员工及直属亲属")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0007Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0007
     * 交易描述：查询客户是否本行员工及直属亲属
     *
     * @param xdkh0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("同业客户信息查询")
    @PostMapping("/xdkh0007")
    //@Idempotent({"xdcakh0007", "#xdkh0007ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0007RespDto xdkh0007(@Validated @RequestBody Xdkh0007ReqDto xdkh0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, JSON.toJSONString(xdkh0007ReqDto));
        Xdkh0007DataReqDto xdkh0007DataReqDto = new Xdkh0007DataReqDto();// 请求Data： 查询客户是否本行员工及直属亲属
        Xdkh0007DataRespDto xdkh0007DataRespDto = new Xdkh0007DataRespDto();// 响应Data：查询客户是否本行员工及直属亲属
        Xdkh0007RespDto xdkh0007RespDto = new Xdkh0007RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0007.req.Data reqData = null; // 请求Data：查询客户是否本行员工及直属亲属
        cn.com.yusys.yusp.dto.server.cus.xdkh0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0007.resp.Data();// 响应Data：查询客户是否本行员工及直属亲属
        try {
            // 从 xdkh0007ReqDto获取 reqData
            reqData = xdkh0007ReqDto.getData();
            // 将 reqData 拷贝到xdkh0007DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0007DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER,DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value,JSON.toJSONString(xdkh0007ReqDto));
            ResultDto<Xdkh0007DataRespDto> xdkh0007DataResultDto = dscmsCusClientService.xdkh0007(xdkh0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER,DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value,JSON.toJSONString(xdkh0007DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0007RespDto.setErorcd(Optional.ofNullable(xdkh0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0007RespDto.setErortx(Optional.ofNullable(xdkh0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0007DataResultDto.getCode())) {
                xdkh0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0007DataRespDto = xdkh0007DataResultDto.getData();
                // 将 xdkh0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, e.getMessage());
            xdkh0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0007RespDto.setDatasq(servsq);

        xdkh0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0007.key, DscmsEnum.TRADE_CODE_XDKH0007.value, JSON.toJSONString(xdkh0007RespDto));
        return xdkh0007RespDto;
    }
}