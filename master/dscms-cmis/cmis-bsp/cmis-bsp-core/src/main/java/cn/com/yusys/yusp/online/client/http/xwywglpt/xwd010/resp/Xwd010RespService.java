package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.resp;

/**
 * 响应Service：利率撤销接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwd010RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwd010RespService{" +
                "service=" + service +
                '}';
    }
}

