package cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.resp;

/**
 * 响应Service：从风控获取客户详细信息
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Fbxd03RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd03RespService{" +
                "service=" + service +
                '}';
    }
}