package cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp;

import java.math.BigDecimal;

public class Record {
    private String relttp;//关系类型
    private String cstno1;//关系人客户编号
    private String jhrflg;//监护人标志
    private String realna;//关系人姓名
    private String idtftp;//证件类型
    private String idtfno;//证件号码
    private String prpsex;//性别
    private String borndt;//出生日期
    private String efctdt;//证件生效日期
    private String inefdt;//证件失效日期
    private String nation;//国籍
    private String ethnic;//民族
    private String propts;//居民性质
    private String educlv;//教育水平（学历）
    private String wkutna;//工作单位
    private String projob;//职业
    private String poston;//职务
    private BigDecimal income;//月收入
    private BigDecimal tbcome;//年收入
    private String homead;//联系地址
    private String hometl;//联系电话
    private String emailx;//邮箱

    public String getRelttp() {
        return relttp;
    }

    public void setRelttp(String relttp) {
        this.relttp = relttp;
    }

    public String getCstno1() {
        return cstno1;
    }

    public void setCstno1(String cstno1) {
        this.cstno1 = cstno1;
    }

    public String getJhrflg() {
        return jhrflg;
    }

    public void setJhrflg(String jhrflg) {
        this.jhrflg = jhrflg;
    }

    public String getRealna() {
        return realna;
    }

    public void setRealna(String realna) {
        this.realna = realna;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getPrpsex() {
        return prpsex;
    }

    public void setPrpsex(String prpsex) {
        this.prpsex = prpsex;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }

    public String getEfctdt() {
        return efctdt;
    }

    public void setEfctdt(String efctdt) {
        this.efctdt = efctdt;
    }

    public String getInefdt() {
        return inefdt;
    }

    public void setInefdt(String inefdt) {
        this.inefdt = inefdt;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getEthnic() {
        return ethnic;
    }

    public void setEthnic(String ethnic) {
        this.ethnic = ethnic;
    }

    public String getPropts() {
        return propts;
    }

    public void setPropts(String propts) {
        this.propts = propts;
    }

    public String getEduclv() {
        return educlv;
    }

    public void setEduclv(String educlv) {
        this.educlv = educlv;
    }

    public String getWkutna() {
        return wkutna;
    }

    public void setWkutna(String wkutna) {
        this.wkutna = wkutna;
    }

    public String getProjob() {
        return projob;
    }

    public void setProjob(String projob) {
        this.projob = projob;
    }

    public String getPoston() {
        return poston;
    }

    public void setPoston(String poston) {
        this.poston = poston;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getTbcome() {
        return tbcome;
    }

    public void setTbcome(BigDecimal tbcome) {
        this.tbcome = tbcome;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getHometl() {
        return hometl;
    }

    public void setHometl(String hometl) {
        this.hometl = hometl;
    }

    public String getEmailx() {
        return emailx;
    }

    public void setEmailx(String emailx) {
        this.emailx = emailx;
    }

    @Override
    public String toString() {
        return "Record{" +
                "relttp='" + relttp + '\'' +
                ", cstno1='" + cstno1 + '\'' +
                ", jhrflg='" + jhrflg + '\'' +
                ", realna='" + realna + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", prpsex='" + prpsex + '\'' +
                ", borndt='" + borndt + '\'' +
                ", efctdt='" + efctdt + '\'' +
                ", inefdt='" + inefdt + '\'' +
                ", nation='" + nation + '\'' +
                ", ethnic='" + ethnic + '\'' +
                ", propts='" + propts + '\'' +
                ", educlv='" + educlv + '\'' +
                ", wkutna='" + wkutna + '\'' +
                ", projob='" + projob + '\'' +
                ", poston='" + poston + '\'' +
                ", income=" + income +
                ", tbcome=" + tbcome +
                ", homead='" + homead + '\'' +
                ", hometl='" + hometl + '\'' +
                ", emailx='" + emailx + '\'' +
                '}';
    }
}
