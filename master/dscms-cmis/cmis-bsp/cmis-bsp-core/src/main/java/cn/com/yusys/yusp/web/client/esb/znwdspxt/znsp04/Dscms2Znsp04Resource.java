package cn.com.yusys.yusp.web.client.esb.znwdspxt.znsp04;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.req.Znsp04ReqService;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.resp.Znsp04RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用智能微贷审批系统的接口处理类
 **/
@Api(tags = "BSP封装调用智能微贷审批系统的接口处理类()")
@RestController
@RequestMapping("/api/dscms2znwdspxt")
public class Dscms2Znsp04Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Znsp04Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：znsp04
     * 交易描述：自动审批调查报告
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("znsp04:自动审批调查报告")
    @PostMapping("/znsp04")
    protected @ResponseBody
    ResultDto<Znsp04RespDto> znsp04(@Validated @RequestBody Znsp04ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.req.Service();
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.resp.Service();
        Znsp04ReqService znsp04ReqService = new Znsp04ReqService();
        Znsp04RespService znsp04RespService = new Znsp04RespService();
        Znsp04RespDto znsp04RespDto = new Znsp04RespDto();
        ResultDto<Znsp04RespDto> znsp04ResultDto = new ResultDto<Znsp04RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将znsp04ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_ZNSP04.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_ZNWDSPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ZNWDSPXT.key);//    部门号
            znsp04ReqService.setService(reqService);
            // 将znsp04ReqService转换成znsp04ReqServiceMap
            Map znsp04ReqServiceMap = beanMapUtil.beanToMap(znsp04ReqService);
            context.put("tradeDataMap", znsp04ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_ZNSP04.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            znsp04RespService = beanMapUtil.mapToBean(tradeDataMap, Znsp04RespService.class, Znsp04RespService.class);
            respService = znsp04RespService.getService();
            //  将respService转换成znsp04RespDto
            BeanUtils.copyProperties(respService, znsp04RespDto);
            znsp04ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            znsp04ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成znsp04RespDto
                BeanUtils.copyProperties(respService, znsp04RespDto);
                znsp04ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                znsp04ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                znsp04ResultDto.setCode(EpbEnum.EPB099999.key);
                znsp04ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value, e.getMessage());
            znsp04ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            znsp04ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        znsp04ResultDto.setData(znsp04RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP04.key, EsbEnum.TRADE_CODE_ZNSP04.value, JSON.toJSONString(znsp04ResultDto));
        return znsp04ResultDto;
    }
}
