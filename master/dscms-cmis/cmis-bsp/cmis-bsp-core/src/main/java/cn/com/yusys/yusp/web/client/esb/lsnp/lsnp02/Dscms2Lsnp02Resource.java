package cn.com.yusys.yusp.web.client.esb.lsnp.lsnp02;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.req.Lsnp02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.resp.Lsnp02RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.req.Lsnp02ReqService;
import cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.resp.Lsnp02RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信用卡业务零售评级
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2lsnp")
public class Dscms2Lsnp02Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Lsnp02Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：lsnp02
     * 交易描述：信用卡业务零售评级
     *
     * @param lsnp02ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/lsnp02")
    protected @ResponseBody
    ResultDto<Lsnp02RespDto> lsnp02(@Validated @RequestBody Lsnp02ReqDto lsnp02ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP02.key, EsbEnum.TRADE_CODE_LSNP02.value, JSON.toJSONString(lsnp02ReqDto));
		cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.req.Service();
		cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.resp.Service();
        Lsnp02ReqService lsnp02ReqService = new Lsnp02ReqService();
        Lsnp02RespService lsnp02RespService = new Lsnp02RespService();
        Lsnp02RespDto lsnp02RespDto = new Lsnp02RespDto();
        ResultDto<Lsnp02RespDto> lsnp02ResultDto = new ResultDto<Lsnp02RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将lsnp02ReqDto转换成reqService
			BeanUtils.copyProperties(lsnp02ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_LSNP02.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			reqService.setUserid(EsbEnum.USERID_LSNP.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_LSNP.key);//    部门号
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_LSP.key, EsbEnum.SERVTP_LSP.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_LSP.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_LSP.key, EsbEnum.SERVTP_LSP.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			lsnp02ReqService.setService(reqService);
			// 将lsnp02ReqService转换成lsnp02ReqServiceMap
			Map lsnp02ReqServiceMap = beanMapUtil.beanToMap(lsnp02ReqService);
			context.put("tradeDataMap", lsnp02ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP02.key, EsbEnum.TRADE_CODE_LSNP02.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LSNP02.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP02.key, EsbEnum.TRADE_CODE_LSNP02.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			lsnp02RespService = beanMapUtil.mapToBean(tradeDataMap, Lsnp02RespService.class, Lsnp02RespService.class);
			respService = lsnp02RespService.getService();
			//  将respService转换成Lsnp02RespDto
			lsnp02ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			lsnp02ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Lsnp01RespDto
				BeanUtils.copyProperties(respService, lsnp02RespDto);
				lsnp02ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				lsnp02ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				lsnp02ResultDto.setCode(EpbEnum.EPB099999.key);
				lsnp02ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP02.key, EsbEnum.TRADE_CODE_LSNP02.value, e.getMessage());
			lsnp02ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			lsnp02ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		lsnp02ResultDto.setData(lsnp02RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP02.key, EsbEnum.TRADE_CODE_LSNP02.value, JSON.toJSONString(lsnp02ResultDto));
        return lsnp02ResultDto;
    }
}
