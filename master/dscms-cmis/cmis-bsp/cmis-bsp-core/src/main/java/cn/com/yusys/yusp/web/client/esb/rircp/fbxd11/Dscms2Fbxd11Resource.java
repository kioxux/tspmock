package cn.com.yusys.yusp.web.client.esb.rircp.fbxd11;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.Fbxd11ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.Fbxd11RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.req.Fbxd11ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp.Fbxd11RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd11）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd11Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd11Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd11
     * 交易描述：获取还款记录的借据一览信息，包括借据号、借据金额、借据余额
     *
     * @param fbxd11ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd11")
    protected @ResponseBody
    ResultDto<Fbxd11RespDto> fbxd11(@Validated @RequestBody Fbxd11ReqDto fbxd11ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd11ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp.Service();
        Fbxd11ReqService fbxd11ReqService = new Fbxd11ReqService();
        Fbxd11RespService fbxd11RespService = new Fbxd11RespService();
        Fbxd11RespDto fbxd11RespDto = new Fbxd11RespDto();
        ResultDto<Fbxd11RespDto> fbxd11ResultDto = new ResultDto<Fbxd11RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd11ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd11ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD10.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd11ReqService.setService(reqService);
            // 将fbxd11ReqService转换成fbxd11ReqServiceMap
            Map fbxd11ReqServiceMap = beanMapUtil.beanToMap(fbxd11ReqService);
            context.put("tradeDataMap", fbxd11ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD10.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd11RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd11RespService.class, Fbxd11RespService.class);
            respService = fbxd11RespService.getService();

            fbxd11ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd11ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd11RespDto
                BeanUtils.copyProperties(respService, fbxd11RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp.List fbxd11RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp.List());
                respService.setList(fbxd11RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> fbxd11RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.List> fbxd11RespDtoLists = fbxd11RespRecordList.parallelStream().map(fbxd11RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.List fbxd11RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.List();
                        BeanUtils.copyProperties(fbxd11RespRecord, fbxd11RespDtoList);
                        return fbxd11RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd11RespDto.setList(fbxd11RespDtoLists);
                }
                fbxd11ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd11ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd11ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd11ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, e.getMessage());
            fbxd11ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd11ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd11ResultDto.setData(fbxd11RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd11ResultDto));
        return fbxd11ResultDto;
    }
}
