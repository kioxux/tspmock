package cn.com.yusys.yusp.online.client.esb.core.mbt952.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/16 15:43
 * @since 2021/6/16 15:43
 */
public class CplWjplxxOut {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.mbt952.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "CplWjplxxOut{" +
                "record=" + record +
                '}';
    }
}
