package cn.com.yusys.yusp.web.server.biz.xdht0043;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0043.req.Xdht0043ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0043.resp.Xdht0043RespDto;
import cn.com.yusys.yusp.dto.server.xdht0043.req.Xdht0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0043.resp.Xdht0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小贷借款合同文本生成pdf
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0043:小贷借款合同文本生成pdf")
@RestController
// TODO /api/tradesystem 待确认
@RequestMapping("/api/dscms")
public class Xdht0043Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0043Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0043
     * 交易描述：小贷借款合同文本生成pdf
     *
     * @param xdht0043ReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("小贷借款合同文本生成pdf")
    @PostMapping("/xdht0043")
    //@Idempotent({"xdcaht0043", "#xdht0043ReqDto.datasq"})
    protected @ResponseBody
    Xdht0043RespDto xdht0043(@Validated @RequestBody Xdht0043ReqDto xdht0043ReqDto ) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043ReqDto));
        Xdht0043DataReqDto xdht0043DataReqDto = new Xdht0043DataReqDto();// 请求Data： 小贷借款合同文本生成pdf
        Xdht0043DataRespDto xdht0043DataRespDto =  new Xdht0043DataRespDto();// 响应Data：小贷借款合同文本生成pdf
        Xdht0043RespDto xdht0043RespDto = new Xdht0043RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0043.req.Data reqData = null; // 请求Data：小贷借款合同文本生成pdf
        cn.com.yusys.yusp.dto.server.biz.xdht0043.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0043.resp.Data();// 响应Data：小贷借款合同文本生成pdf


        try {
            // 从 xdht0043ReqDto获取 reqData
            reqData = xdht0043ReqDto.getData();
            // 将 reqData 拷贝到xdht0043DataReqDto
            BeanUtils.copyProperties(reqData, xdht0043DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043DataReqDto));
            ResultDto<Xdht0043DataRespDto> xdht0043DataResultDto= dscmsBizHtClientService.xdht0043(xdht0043DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0043RespDto.setErorcd(Optional.ofNullable(xdht0043DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0043RespDto.setErortx(Optional.ofNullable(xdht0043DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,xdht0043DataResultDto.getCode())) {
                xdht0043DataRespDto = xdht0043DataResultDto.getData();
                xdht0043RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0043RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0043DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0043DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, e.getMessage());
            xdht0043RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0043RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0043RespDto.setDatasq(servsq);

        xdht0043RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0043.key, DscmsEnum.TRADE_CODE_XDHT0043.value, JSON.toJSONString(xdht0043RespDto));
        return xdht0043RespDto;
    }
}
