package cn.com.yusys.yusp.online.client.esb.circp.fb1146.req;

/**
 * 请求Service：受托信息审核
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1146ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1146ReqService{" +
                "service=" + service +
                '}';
    }
}
