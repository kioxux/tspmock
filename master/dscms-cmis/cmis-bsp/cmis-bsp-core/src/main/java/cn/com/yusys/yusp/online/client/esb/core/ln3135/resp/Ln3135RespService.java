package cn.com.yusys.yusp.online.client.esb.core.ln3135.resp;

import java.math.BigDecimal;

/**
 * 响应Service：通过贷款借据号和贷款账号查询贷款期供计划
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3135RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

