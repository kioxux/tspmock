package cn.com.yusys.yusp.online.client.esb.doc.doc004.req;

/**
 * 请求Service：调阅待出库查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Doc004ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Doc004ReqService{" +
                "service=" + service +
                '}';
    }
}
