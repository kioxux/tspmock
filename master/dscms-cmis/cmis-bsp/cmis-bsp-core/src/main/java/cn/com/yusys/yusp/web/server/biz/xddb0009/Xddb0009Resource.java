package cn.com.yusys.yusp.web.server.biz.xddb0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0009.req.Xddb0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0009.resp.Xddb0009RespDto;
import cn.com.yusys.yusp.dto.server.xddb0009.req.Xddb0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0009.resp.Xddb0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:双录流水号与押品编号关系维护
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDB0009:双录流水号与押品编号关系维护")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0009Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0009
     * 交易描述：双录流水号与押品编号关系维护
     *
     * @param xddb0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("双录流水号与押品编号关系维护")
    @PostMapping("/xddb0009")
    //@Idempotent({"xddb0009", "#xddb0009ReqDto.datasq"})
    protected @ResponseBody
    Xddb0009RespDto xddb0009(@Validated @RequestBody Xddb0009ReqDto xddb0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, JSON.toJSONString(xddb0009ReqDto));
        Xddb0009DataReqDto xddb0009DataReqDto = new Xddb0009DataReqDto();// 请求Data： 双录流水号与押品编号关系维护
        Xddb0009DataRespDto xddb0009DataRespDto = new Xddb0009DataRespDto();// 响应Data：双录流水号与押品编号关系维护
        Xddb0009RespDto xddb0009RespDto = new Xddb0009RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0009.req.Data reqData = null; // 请求Data：双录流水号与押品编号关系维护
        cn.com.yusys.yusp.dto.server.biz.xddb0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0009.resp.Data();// 响应Data：双录流水号与押品编号关系维护

        try {
            // 从 xddb0009ReqDto获取 reqData
            reqData = xddb0009ReqDto.getData();
            // 将 reqData 拷贝到xddb0009DataReqDto
            BeanUtils.copyProperties(reqData, xddb0009DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, JSON.toJSONString(xddb0009DataReqDto));
            ResultDto<Xddb0009DataRespDto> xddb0009DataResultDto = dscmsBizDbClientService.xddb0009(xddb0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, JSON.toJSONString(xddb0009DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0009RespDto.setErorcd(Optional.ofNullable(xddb0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0009RespDto.setErortx(Optional.ofNullable(xddb0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0009DataResultDto.getCode())) {
                xddb0009DataRespDto = xddb0009DataResultDto.getData();
                xddb0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, e.getMessage());
            xddb0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0009RespDto.setDatasq(servsq);

        xddb0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0009.key, DscmsEnum.TRADE_CODE_XDDB0009.value, JSON.toJSONString(xddb0009RespDto));
        return xddb0009RespDto;
    }
}
