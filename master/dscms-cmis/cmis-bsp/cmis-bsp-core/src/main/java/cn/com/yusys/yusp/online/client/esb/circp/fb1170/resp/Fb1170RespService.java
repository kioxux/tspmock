package cn.com.yusys.yusp.online.client.esb.circp.fb1170.resp;

/**
 * 响应Service：最高额借款合同信息推送
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1170RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1170RespService{" +
                "service=" + service +
                '}';
    }
}
