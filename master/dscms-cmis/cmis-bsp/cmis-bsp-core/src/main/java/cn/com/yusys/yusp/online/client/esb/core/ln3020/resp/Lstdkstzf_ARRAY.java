package cn.com.yusys.yusp.online.client.esb.core.ln3020.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.lstdkstzf.Record;

import java.util.List;

/**
 * 响应Dto：贷款受托支付
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Lstdkstzf_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.resp.lstdkstzf.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkstzf{" +
                "record=" + record +
                '}';
    }
}