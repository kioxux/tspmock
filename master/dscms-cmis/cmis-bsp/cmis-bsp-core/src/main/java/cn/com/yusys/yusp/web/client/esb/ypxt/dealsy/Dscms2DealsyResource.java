package cn.com.yusys.yusp.web.client.esb.ypxt.dealsy;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.dealsy.req.DealsyReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.dealsy.req.Service;
import cn.com.yusys.yusp.online.client.esb.ypxt.dealsy.resp.DealsyRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品处置信息同步
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用押品系统的接口(dealsy)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2DealsyResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2DealsyResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：dealsy
     * 交易描述：押品处置信息同步
     *
     * @param dealsyReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("dealsy:押品处置信息同步")
    @PostMapping("/dealsy")
    protected @ResponseBody
    ResultDto<DealsyRespDto> dealsy(@Validated @RequestBody DealsyReqDto dealsyReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEALSY.key, EsbEnum.TRADE_CODE_DEALSY.value, JSON.toJSONString(dealsyReqDto));
        Service reqService = new Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.dealsy.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.dealsy.resp.Service();
        DealsyReqService dealsyReqService = new DealsyReqService();
        DealsyRespService dealsyRespService = new DealsyRespService();
        DealsyRespDto dealsyRespDto = new DealsyRespDto();
        ResultDto<DealsyRespDto> dealsyResultDto = new ResultDto<DealsyRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将dealsyReqDto转换成reqService
            BeanUtils.copyProperties(dealsyReqDto, reqService);
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DEALSY.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            dealsyReqService.setService(reqService);
            // 将dealsyReqService转换成dealsyReqServiceMap
            Map dealsyReqServiceMap = beanMapUtil.beanToMap(dealsyReqService);
            context.put("tradeDataMap", dealsyReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEALSY.key, EsbEnum.TRADE_CODE_DEALSY.value, JSON.toJSONString(dealsyReqDto));
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_EVALYP.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEALSY.key, EsbEnum.TRADE_CODE_DEALSY.value, JSON.toJSONString(result));
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            dealsyRespService = beanMapUtil.mapToBean(tradeDataMap, DealsyRespService.class, DealsyRespService.class);
            respService = dealsyRespService.getService();
            //  将respService转换成DealsyRespDto
            dealsyResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            dealsyResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                BeanUtils.copyProperties(respService, dealsyRespDto);
                dealsyResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                dealsyResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                dealsyResultDto.setCode(EpbEnum.EPB099999.key);
                dealsyResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEALSY.key, EsbEnum.TRADE_CODE_DEALSY.value, e.getMessage());
            dealsyResultDto.setCode(EpbEnum.EPB099999.key);//9999
            dealsyResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        dealsyResultDto.setData(dealsyRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEALSY.key, EsbEnum.TRADE_CODE_DEALSY.value, JSON.toJSONString(dealsyReqDto));
        return dealsyResultDto;
    }
}
