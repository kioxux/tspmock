package cn.com.yusys.yusp.online.client.esb.doc.doc006.req;

/**
 * 请求Service：申请出库
 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String pkbappid;//借阅申请id
    private String inroomtime;//归还时间
    private String rqtem1;//请求备用字段1
    private String rqtem2;//请求备用字段2
    private String rqtem3;//请求备用字段3
    private String rqtem4;//请求备用字段4

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getPkbappid() {
        return pkbappid;
    }

    public void setPkbappid(String pkbappid) {
        this.pkbappid = pkbappid;
    }

    public String getInroomtime() {
        return inroomtime;
    }

    public void setInroomtime(String inroomtime) {
        this.inroomtime = inroomtime;
    }

    public String getRqtem1() {
        return rqtem1;
    }

    public void setRqtem1(String rqtem1) {
        this.rqtem1 = rqtem1;
    }

    public String getRqtem2() {
        return rqtem2;
    }

    public void setRqtem2(String rqtem2) {
        this.rqtem2 = rqtem2;
    }

    public String getRqtem3() {
        return rqtem3;
    }

    public void setRqtem3(String rqtem3) {
        this.rqtem3 = rqtem3;
    }

    public String getRqtem4() {
        return rqtem4;
    }

    public void setRqtem4(String rqtem4) {
        this.rqtem4 = rqtem4;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", pkbappid='" + pkbappid + '\'' +
                ", inroomtime='" + inroomtime + '\'' +
                ", rqtem1='" + rqtem1 + '\'' +
                ", rqtem2='" + rqtem2 + '\'' +
                ", rqtem3='" + rqtem3 + '\'' +
                ", rqtem4='" + rqtem4 + '\'' +
                '}';
    }
}
