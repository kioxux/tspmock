package cn.com.yusys.yusp.web.server.biz.xdxw0067;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0067.req.Xdxw0067ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0067.resp.Xdxw0067RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0067.req.Xdxw0067DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0067.resp.Xdxw0067DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0067:调查基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0067Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0067Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdxw0067
     * 交易描述：调查基本信息查询
     *
     * @param xdxw0067ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查基本信息查询")
    @PostMapping("/xdxw0067")
    //@Idempotent({"xdcaxw0067", "#xdxw0067ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0067RespDto xdxw0067(@Validated @RequestBody Xdxw0067ReqDto xdxw0067ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067ReqDto));
        Xdxw0067DataReqDto xdxw0067DataReqDto = new Xdxw0067DataReqDto();// 请求Data： 调查基本信息查询
        Xdxw0067DataRespDto xdxw0067DataRespDto = new Xdxw0067DataRespDto();// 响应Data：调查基本信息查询
        Xdxw0067RespDto xdxw0067RespDto = new Xdxw0067RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0067.req.Data reqData = null; // 请求Data：调查基本信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0067.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0067.resp.Data();// 响应Data：调查基本信息查询
        try {
            // 从 xdxw0067ReqDto获取 reqData
            reqData = xdxw0067ReqDto.getData();
            // 将 reqData 拷贝到xdxw0067DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0067DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataReqDto));
            ResultDto<Xdxw0067DataRespDto> xdxw0067DataResultDto = dscmsCusClientService.xdxw0067(xdxw0067DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0067RespDto.setErorcd(Optional.ofNullable(xdxw0067DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0067RespDto.setErortx(Optional.ofNullable(xdxw0067DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0067DataResultDto.getCode())) {
                xdxw0067DataRespDto = xdxw0067DataResultDto.getData();
                xdxw0067RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0067RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0067DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0067DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, e.getMessage());
            xdxw0067RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0067RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0067RespDto.setDatasq(servsq);

        xdxw0067RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0067.key, DscmsEnum.TRADE_CODE_XDXW0067.value, JSON.toJSONString(xdxw0067RespDto));
        return xdxw0067RespDto;
    }
}
