package cn.com.yusys.yusp.web.client.esb.core.ln3128;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3128.req.Ln3128ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3128.resp.Ln3128RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3128.req.Ln3128ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3128.resp.Ln3128RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3128.resp.Lstdkjgzy_ARRAY;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3128)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3128Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3128Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3128
     * 交易描述：贷款机构变更查询
     *
     * @param reqDto 请求体
     * @return ResultDto<Ln3128RespDto> 封装的返回结果
     * @throws Exception
     */
    @ApiOperation("ln3128:贷款机构变更查询")
    @PostMapping("/ln3128")
    protected @ResponseBody
    ResultDto<Ln3128RespDto> ln3128(@Validated @RequestBody Ln3128ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3128.key, EsbEnum.TRADE_CODE_LN3128.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3128.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3128.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3128.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3128.resp.Service();
        Ln3128ReqService ln3128ReqService = new Ln3128ReqService();
        Ln3128RespService ln3128RespService = new Ln3128RespService();
        Ln3128RespDto ln3128RespDto = new Ln3128RespDto();
        ResultDto<Ln3128RespDto> ln3128ResultDto = new ResultDto<Ln3128RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3128ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3128.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3128ReqService.setService(reqService);

            ln3128ReqService.setService(reqService);
            // 将ln3128ReqService转换成ln3128ReqServiceMap
            Map ln3128ReqServiceMap = beanMapUtil.beanToMap(ln3128ReqService);
            context.put("tradeDataMap", ln3128ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3128.key, EsbEnum.TRADE_CODE_LN3128.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3128.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3128.key, EsbEnum.TRADE_CODE_LN3128.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3128RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3128RespService.class, Ln3128RespService.class);
            respService = ln3128RespService.getService();

            ln3128ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3128ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3128RespDto
                BeanUtils.copyProperties(respService, ln3128RespDto);
                Lstdkjgzy_ARRAY lstdkjgzy = Optional.ofNullable(respService.getLstdkjgzy_ARRAY()).orElse(new Lstdkjgzy_ARRAY());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3128.resp.Record> records = lstdkjgzy.getRecord();
                if (CollectionUtils.nonEmpty(records)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3128.resp.Lstdkjgzy> respList = records.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3128.resp.Lstdkjgzy temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3128.resp.Lstdkjgzy();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3128RespDto.setLstdkjgzy(respList);
                }
                ln3128ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3128ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3128ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3128ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3128.key, EsbEnum.TRADE_CODE_LN3128.value, e.getMessage());
            ln3128ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3128ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3128ResultDto.setData(ln3128RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3128.key, EsbEnum.TRADE_CODE_LN3128.value, JSON.toJSONString(ln3128ResultDto));
        return ln3128ResultDto;
    }
}
