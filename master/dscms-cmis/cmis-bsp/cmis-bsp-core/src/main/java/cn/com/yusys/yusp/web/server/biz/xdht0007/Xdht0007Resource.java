package cn.com.yusys.yusp.web.server.biz.xdht0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0007.req.Xdht0007ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0007.resp.Xdht0007RespDto;
import cn.com.yusys.yusp.dto.server.xdht0007.req.Xdht0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0007.resp.Xdht0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借款合同/担保合同列表信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0007:借款合同/担保合同列表信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0007Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0007
     * 交易描述：借款合同/担保合同列表信息查询
     *
     * @param xdht0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借款合同/担保合同列表信息查询")
    @PostMapping("/xdht0007")
    //@Idempotent({"xdcaht0007", "#xdht0007ReqDto.datasq"})
    protected @ResponseBody
    Xdht0007RespDto xdht0007(@Validated @RequestBody Xdht0007ReqDto xdht0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007ReqDto));
        Xdht0007DataReqDto xdht0007DataReqDto = new Xdht0007DataReqDto();// 请求Data： 借款合同/担保合同列表信息查询
        Xdht0007DataRespDto xdht0007DataRespDto = new Xdht0007DataRespDto();// 响应Data：借款合同/担保合同列表信息查询
        cn.com.yusys.yusp.dto.server.biz.xdht0007.req.Data reqData = null; // 请求Data：借款合同/担保合同列表信息查询
        cn.com.yusys.yusp.dto.server.biz.xdht0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0007.resp.Data();// 响应Data：借款合同/担保合同列表信息查询
        Xdht0007RespDto xdht0007RespDto = new Xdht0007RespDto();
        try {
            // 从 xdht0007ReqDto获取 reqData
            reqData = xdht0007ReqDto.getData();
            // 将 reqData 拷贝到xdht0007DataReqDto
            BeanUtils.copyProperties(reqData, xdht0007DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataReqDto));
            ResultDto<Xdht0007DataRespDto> xdht0007DataResultDto = dscmsBizHtClientService.xdht0007(xdht0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0007RespDto.setErorcd(Optional.ofNullable(xdht0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0007RespDto.setErortx(Optional.ofNullable(xdht0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0007DataResultDto.getCode())) {
                xdht0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0007DataRespDto = xdht0007DataResultDto.getData();
                // 将 xdht0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, e.getMessage());
            xdht0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0007RespDto.setDatasq(servsq);

        xdht0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0007.key, DscmsEnum.TRADE_CODE_XDHT0007.value, JSON.toJSONString(xdht0007RespDto));
        return xdht0007RespDto;
    }
}
