package cn.com.yusys.yusp.web.server.biz.xdht0028;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0028.req.Xdht0028ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0028.resp.Xdht0028RespDto;
import cn.com.yusys.yusp.dto.server.xdht0028.req.Xdht0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0028.resp.Xdht0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据合同号取得客户经理电话号码
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0028:根据合同号取得客户经理电话号码")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0028Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0028
     * 交易描述：根据合同号取得客户经理电话号码
     *
     * @param xdht0028ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据合同号取得客户经理电话号码")
    @PostMapping("/xdht0028")
    //@Idempotent({"xdcaht0028", "#xdht0028ReqDto.datasq"})
    protected @ResponseBody
    Xdht0028RespDto xdht0028(@Validated @RequestBody Xdht0028ReqDto xdht0028ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028ReqDto));
        Xdht0028DataReqDto xdht0028DataReqDto = new Xdht0028DataReqDto();// 请求Data： 根据合同号取得客户经理电话号码
        Xdht0028DataRespDto xdht0028DataRespDto = new Xdht0028DataRespDto();// 响应Data：根据合同号取得客户经理电话号码
        Xdht0028RespDto xdht0028RespDto = new Xdht0028RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0028.req.Data reqData = null; // 请求Data：根据合同号取得客户经理电话号码
        cn.com.yusys.yusp.dto.server.biz.xdht0028.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0028.resp.Data();// 响应Data：根据合同号取得客户经理电话号码

        try {
            // 从 xdht0028ReqDto获取 reqData
            reqData = xdht0028ReqDto.getData();
            // 将 reqData 拷贝到xdht0028DataReqDto
            BeanUtils.copyProperties(reqData, xdht0028DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataReqDto));
            ResultDto<Xdht0028DataRespDto> xdht0028DataResultDto = dscmsBizHtClientService.xdht0028(xdht0028DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0028RespDto.setErorcd(Optional.ofNullable(xdht0028DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0028RespDto.setErortx(Optional.ofNullable(xdht0028DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0028DataResultDto.getCode())) {
                xdht0028DataRespDto = xdht0028DataResultDto.getData();
                xdht0028RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0028RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0028DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0028DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, e.getMessage());
            xdht0028RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0028RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0028RespDto.setDatasq(servsq);

        xdht0028RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0028.key, DscmsEnum.TRADE_CODE_XDHT0028.value, JSON.toJSONString(xdht0028RespDto));
        return xdht0028RespDto;
    }
}
