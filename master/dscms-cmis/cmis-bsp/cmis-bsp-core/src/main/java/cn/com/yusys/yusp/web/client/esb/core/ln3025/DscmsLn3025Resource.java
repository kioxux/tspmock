package cn.com.yusys.yusp.web.client.esb.core.ln3025;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3025.req.Ln3025ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3025.resp.Ln3025RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:垫款开户
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3025)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class DscmsLn3025Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsLn3025Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3025
     * 交易描述：垫款开户
     *
     * @param ln3025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3025:垫款开户")
    @PostMapping("/ln3025")
    protected @ResponseBody
    ResultDto<Ln3025RespDto> ln3025(@Validated @RequestBody Ln3025ReqDto ln3025ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value, JSON.toJSONString(ln3025ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.ln3025.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3025.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.ln3025.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3025.resp.Service();
        Ln3025ReqService ln3025ReqService = new Ln3025ReqService();
        Ln3025RespService ln3025RespService = new Ln3025RespService();
        Ln3025RespDto ln3025RespDto = new Ln3025RespDto();
        ResultDto<Ln3025RespDto> ln3025ResultDto = new ResultDto<Ln3025RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将ln3025ReqDto转换成reqService
			BeanUtils.copyProperties(ln3025ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3025.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(Optional.ofNullable(ln3025ReqDto.getBrchno()).orElse(EsbEnum.BRCHNO_CORE.key));//    部门号
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			ln3025ReqService.setService(reqService);
			// 将ln3025ReqService转换成ln3025ReqServiceMap
			Map ln3025ReqServiceMap = beanMapUtil.beanToMap(ln3025ReqService);
			context.put("tradeDataMap", ln3025ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3025.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			ln3025RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3025RespService.class, Ln3025RespService.class);
			respService = ln3025RespService.getService();
			//  将respService转换成Ln3025RespDto
			BeanUtils.copyProperties(respService, ln3025RespDto);
			ln3025ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			ln3025ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成S00101RespDto
				BeanUtils.copyProperties(respService, ln3025RespDto);

				ln3025ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				ln3025ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				ln3025ResultDto.setCode(EpbEnum.EPB099999.key);
				ln3025ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value, e.getMessage());
			ln3025ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			ln3025ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		ln3025ResultDto.setData(ln3025RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3025.key, EsbEnum.TRADE_CODE_LN3025.value, JSON.toJSONString(ln3025ResultDto));
        return ln3025ResultDto;
    }
}
