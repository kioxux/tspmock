package cn.com.yusys.yusp.web.server.biz.xdxw0060;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0060.req.Xdxw0060ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0060.resp.Xdxw0060RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0060.req.Xdxw0060DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0060.resp.Xdxw0060DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户及配偶信用类小微业务贷款授信金额
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0060:客户及配偶信用类小微业务贷款授信金额")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0060Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0060Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0060
     * 交易描述：客户及配偶信用类小微业务贷款授信金额
     *
     * @param xdxw0060ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户及配偶信用类小微业务贷款授信金额")
    @PostMapping("/xdxw0060")
    //@Idempotent({"xdcaxw0060", "#xdxw0060ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0060RespDto xdxw0060(@Validated @RequestBody Xdxw0060ReqDto xdxw0060ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060ReqDto));
        Xdxw0060DataReqDto xdxw0060DataReqDto = new Xdxw0060DataReqDto();// 请求Data： 客户及配偶信用类小微业务贷款授信金额
        Xdxw0060DataRespDto xdxw0060DataRespDto = new Xdxw0060DataRespDto();// 响应Data：客户及配偶信用类小微业务贷款授信金额
        Xdxw0060RespDto xdxw0060RespDto = new Xdxw0060RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdxw0060.req.Data reqData = null;// 请求Data：客户及配偶信用类小微业务贷款授信金额
        cn.com.yusys.yusp.dto.server.biz.xdxw0060.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0060.resp.Data();// 响应Data：客户及配偶信用类小微业务贷款授信金额
        try {
            // 从 xdxw0060ReqDto获取 reqData
            reqData = xdxw0060ReqDto.getData();
            // 将 reqData 拷贝到xdxw0060DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0060DataReqDto);

            ResultDto<Xdxw0060DataRespDto> xdxw0060DataResultDto = dscmsBizXwClientService.xdxw0060(xdxw0060DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdxw0060RespDto.setErorcd(Optional.ofNullable(xdxw0060DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0060RespDto.setErortx(Optional.ofNullable(xdxw0060DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0060DataResultDto.getCode())) {
                xdxw0060RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0060RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0060DataRespDto = xdxw0060DataResultDto.getData();
                // 将 xdxw0060DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0060DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, e.getMessage());
            xdxw0060RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0060RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0060RespDto.setDatasq(servsq);

        xdxw0060RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0060.key, DscmsEnum.TRADE_CODE_XDXW0060.value, JSON.toJSONString(xdxw0060RespDto));
        return xdxw0060RespDto;
    }
}
