package cn.com.yusys.yusp.web.server.biz.xdtz0051;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0051.req.Xdtz0051ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0051.resp.Xdtz0051RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0051.req.Xdtz0051DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0051.resp.Xdtz0051DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizTzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:对公客户关联业务检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0051:对公客户关联业务检查")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0051Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0051Resource.class);
	@Autowired
	private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0051
     * 交易描述：对公客户关联业务检查
     *
     * @param xdtz0051ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对公客户关联业务检查")
    @PostMapping("/xdtz0051")
    //@Idempotent({"xdcatz0051", "#xdtz0051ReqDto.datasq"})
    protected @ResponseBody
	Xdtz0051RespDto xdtz0051(@Validated @RequestBody Xdtz0051ReqDto xdtz0051ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, JSON.toJSONString(xdtz0051ReqDto));
        // 请求Data： 对公客户关联业务检查
        Xdtz0051DataReqDto xdtz0051DataReqDto = new Xdtz0051DataReqDto();
        // 响应Data：对公客户关联业务检查
        Xdtz0051DataRespDto xdtz0051DataRespDto = new Xdtz0051DataRespDto();
		Xdtz0051RespDto xdtz0051RespDto=new Xdtz0051RespDto();
        // 请求Data：对公客户关联业务检查
		cn.com.yusys.yusp.dto.server.biz.xdtz0051.req.Data reqData = null;
        cn.com.yusys.yusp.dto.server.biz.xdtz0051.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0051.resp.Data ();// 响应Data：对公客户关联业务检查
        try {
            // 从 xdtz0051ReqDto获取 reqData
            reqData = xdtz0051ReqDto.getData();
            // 将 reqData 拷贝到xdtz0051DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0051DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, JSON.toJSONString(xdtz0051DataReqDto));
            ResultDto<Xdtz0051DataRespDto> xdtz0051DataResultDto = dscmsBizTzClientService.xdtz0051(xdtz0051DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, JSON.toJSONString(xdtz0051DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0051RespDto.setErorcd(Optional.ofNullable(xdtz0051DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0051RespDto.setErortx(Optional.ofNullable(xdtz0051DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.SUCCESS.key, xdtz0051DataResultDto.getCode())) {
                xdtz0051DataRespDto = xdtz0051DataResultDto.getData();
                xdtz0051RespDto.setErorcd(DscmsBizTzEnum.DELLEET_SUCCESS.key);
                xdtz0051RespDto.setErortx(DscmsBizTzEnum.DELLEET_SUCCESS.value);
                // 将 xdtz0051DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0051DataRespDto, respData);
            }else{
                xdtz0051DataRespDto = xdtz0051DataResultDto.getData();
                BeanUtils.copyProperties(xdtz0051DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, e.getMessage());
            xdtz0051RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0051RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0051RespDto.setDatasq(servsq);

        xdtz0051RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0051.key, DscmsEnum.TRADE_CODE_XDTZ0051.value, JSON.toJSONString(xdtz0051RespDto));
        return xdtz0051RespDto;
    }
}
