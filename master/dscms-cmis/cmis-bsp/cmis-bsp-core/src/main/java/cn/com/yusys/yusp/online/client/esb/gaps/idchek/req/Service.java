package cn.com.yusys.yusp.online.client.esb.gaps.idchek.req;

/**
 * 请求Service：身份证核查
 */
public class Service {

    private String prcscd;//处理码
    private String servtp;//渠道码
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servsq;//渠道流水
    private String servdt;//请求方日期
    private String servti;//请求方时间
    private String ipaddr;//请求方IP
    private String mac;//请求方MAC
    private String sfhm;//身份证号码
    private String sfmc;//姓名
    private String yhhh;//发起行行号
    private String yhhm;//发起行名称
    private String flcmc;//分理处名称
    private String flcdz;//分理处地址
    private String flcdh;//分理处电话
    private String ywlx;//业务类型

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSfhm() {
        return sfhm;
    }

    public void setSfhm(String sfhm) {
        this.sfhm = sfhm;
    }

    public String getSfmc() {
        return sfmc;
    }

    public void setSfmc(String sfmc) {
        this.sfmc = sfmc;
    }

    public String getYhhh() {
        return yhhh;
    }

    public void setYhhh(String yhhh) {
        this.yhhh = yhhh;
    }

    public String getYhhm() {
        return yhhm;
    }

    public void setYhhm(String yhhm) {
        this.yhhm = yhhm;
    }

    public String getFlcmc() {
        return flcmc;
    }

    public void setFlcmc(String flcmc) {
        this.flcmc = flcmc;
    }

    public String getFlcdz() {
        return flcdz;
    }

    public void setFlcdz(String flcdz) {
        this.flcdz = flcdz;
    }

    public String getFlcdh() {
        return flcdh;
    }

    public void setFlcdh(String flcdh) {
        this.flcdh = flcdh;
    }

    public String getYwlx() {
        return ywlx;
    }

    public void setYwlx(String ywlx) {
        this.ywlx = ywlx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servsq='" + servsq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "ipaddr='" + ipaddr + '\'' +
                "mac='" + mac + '\'' +
                "sfhm='" + sfhm + '\'' +
                "sfmc='" + sfmc + '\'' +
                "yhhh='" + yhhh + '\'' +
                "yhhm='" + yhhm + '\'' +
                "flcmc='" + flcmc + '\'' +
                "flcdz='" + flcdz + '\'' +
                "flcdh='" + flcdh + '\'' +
                "ywlx='" + ywlx + '\'' +
                '}';
    }
}
