package cn.com.yusys.yusp.online.client.esb.rircp.fbxw03.req;

/**
 * 请求Service：增享贷风控测算
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午5:34:07
 */
public class Service {
        private String prcscd; // 处理码
        private String servtp; // 渠道
        private String servsq; // 渠道流水
        private String userid; // 柜员号
        private String brchno; // 部门号
        private String channel_type; // 渠道来源
        private String co_platform; // 合作平台
        private String prd_type; // 产品类别
        private String prd_code; // 产品代码（零售智能风控内部代码）
        private String apply_no; // 业务唯一编号
        private String op_flag; // 操作类型
        private String cust_id; // 客户号
        private String telphone; // 手机号码
        private String bill_no; // 借据号
        private String manager_id; // 客户经理号
        private String manager_br_id; // 管户机构
        private String cust_name; // 借款人姓名
        private String cert_code; // 借款人身份证号码
        private String spouse_name; // 配偶姓名（暂时改为了居住地址）
        private String spouse_cert_code; // 配偶身份证号码
        private String amount; // 申请金额
        private String manager_name; // 客户经理姓名
        private String amt; // 客户经理建议金额
        private String rate; // 客户经理建议利率
        private String repay_type; // 还款方式
        private String loan_purpose; // 贷款用途
        private String crd_term; // 贷款期限
        private String agri_flg; // 是否农户
        private String cert_type; // 证件类型
        private String loan_direction; // 贷款投向
        private String qq; // qq
        private String email; // 电子邮箱
        private String wechat; // 微信

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getApply_no() {
        return apply_no;
    }

    public void setApply_no(String apply_no) {
        this.apply_no = apply_no;
    }

    public String getOp_flag() {
        return op_flag;
    }

    public void setOp_flag(String op_flag) {
        this.op_flag = op_flag;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRepay_type() {
        return repay_type;
    }

    public void setRepay_type(String repay_type) {
        this.repay_type = repay_type;
    }

    public String getLoan_purpose() {
        return loan_purpose;
    }

    public void setLoan_purpose(String loan_purpose) {
        this.loan_purpose = loan_purpose;
    }

    public String getCrd_term() {
        return crd_term;
    }

    public void setCrd_term(String crd_term) {
        this.crd_term = crd_term;
    }

    public String getAgri_flg() {
        return agri_flg;
    }

    public void setAgri_flg(String agri_flg) {
        this.agri_flg = agri_flg;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getLoan_direction() {
        return loan_direction;
    }

    public void setLoan_direction(String loan_direction) {
        this.loan_direction = loan_direction;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", channel_type='" + channel_type + '\'' +
                ", co_platform='" + co_platform + '\'' +
                ", prd_type='" + prd_type + '\'' +
                ", prd_code='" + prd_code + '\'' +
                ", apply_no='" + apply_no + '\'' +
                ", op_flag='" + op_flag + '\'' +
                ", cust_id='" + cust_id + '\'' +
                ", telphone='" + telphone + '\'' +
                ", bill_no='" + bill_no + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", spouse_cert_code='" + spouse_cert_code + '\'' +
                ", amount='" + amount + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", amt='" + amt + '\'' +
                ", rate='" + rate + '\'' +
                ", repay_type='" + repay_type + '\'' +
                ", loan_purpose='" + loan_purpose + '\'' +
                ", crd_term='" + crd_term + '\'' +
                ", agri_flg='" + agri_flg + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", loan_direction='" + loan_direction + '\'' +
                ", qq='" + qq + '\'' +
                ", email='" + email + '\'' +
                ", wechat='" + wechat + '\'' +
                '}';
    }
}