package cn.com.yusys.yusp.online.client.esb.ypxt.xdypdywcx.req;

/**
 * 请求Service：查询抵押物信息
 * @author zhugenrong
 * @version 1.0
 */
public class XdypdywcxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}