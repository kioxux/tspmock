package cn.com.yusys.yusp.web.server.biz.xdtz0056;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0056.req.Xdtz0056ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0056.resp.Xdtz0056RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0056.req.Xdtz0056DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0056.resp.Xdtz0056DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询还款业务类型
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0056:查询还款业务类型")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0056Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0056Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0056
     * 交易描述：查询还款业务类型
     *
     * @param xdtz0056ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询还款业务类型")
    @PostMapping("/xdtz0056")
//@Idempotent({"xdtz0056", "#xdtz0056ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0056RespDto xdtz0056(@Validated @RequestBody Xdtz0056ReqDto xdtz0056ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, JSON.toJSONString(xdtz0056ReqDto));
        Xdtz0056DataReqDto xdtz0056DataReqDto = new Xdtz0056DataReqDto();// 请求Data： 查询还款业务类型
        Xdtz0056DataRespDto xdtz0056DataRespDto = new Xdtz0056DataRespDto();// 响应Data：查询还款业务类型
        Xdtz0056RespDto xdtz0056RespDto = new Xdtz0056RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0056.req.Data reqData = null; // 请求Data：查询还款业务类型
        cn.com.yusys.yusp.dto.server.biz.xdtz0056.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0056.resp.Data();// 响应Data：查询还款业务类型

        try {
            // 从 xdtz0056ReqDto获取 reqData
            reqData = xdtz0056ReqDto.getData();
            // 将 reqData 拷贝到xdtz0056DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0056DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, JSON.toJSONString(xdtz0056DataReqDto));
            ResultDto<Xdtz0056DataRespDto> xdtz0056DataResultDto = dscmsBizTzClientService.xdtz0056(xdtz0056DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, JSON.toJSONString(xdtz0056DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0056RespDto.setErorcd(Optional.ofNullable(xdtz0056DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0056RespDto.setErortx(Optional.ofNullable(xdtz0056DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0056DataResultDto.getCode())) {
                xdtz0056DataRespDto = xdtz0056DataResultDto.getData();
                xdtz0056RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0056RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0056DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0056DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, e.getMessage());
            xdtz0056RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0056RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0056RespDto.setDatasq(servsq);

        xdtz0056RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0056.key, DscmsEnum.TRADE_CODE_XDTZ0056.value, JSON.toJSONString(xdtz0056RespDto));
        return xdtz0056RespDto;
    }
}
