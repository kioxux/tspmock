package cn.com.yusys.yusp.online.client.esb.core.dp2352.resp;

import java.math.BigDecimal;

/**
 * 响应Service：组合账户子账户开立
 * @author lihh
 * @version 1.0             
 */      
public class Dp2352RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
