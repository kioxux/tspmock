package cn.com.yusys.yusp.online.client.esb.irs.irs18.req;

/**
 * 请求Service：评级主办权更新
 *
 * @author code-generator
 * @version 1.0
 */
public class Irs18ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}


