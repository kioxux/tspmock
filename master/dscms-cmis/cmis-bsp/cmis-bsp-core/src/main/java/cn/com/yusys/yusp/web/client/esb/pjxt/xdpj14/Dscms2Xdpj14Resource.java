package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj14;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.req.Xdpj14ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.resp.Xdpj14RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷签约通知
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj14Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj14Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdpj14
     * 交易描述：信贷签约通知
     *
     * @param xdpj14ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdpj14")
    protected @ResponseBody
    ResultDto<Xdpj14RespDto> xdpj14(@Validated @RequestBody Xdpj14ReqDto xdpj14ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value, JSON.toJSONString(xdpj14ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.req.Service();
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.resp.Service();
        Xdpj14ReqService xdpj14ReqService = new Xdpj14ReqService();
        Xdpj14RespService xdpj14RespService = new Xdpj14RespService();
        Xdpj14RespDto xdpj14RespDto = new Xdpj14RespDto();
        ResultDto<Xdpj14RespDto> xdpj14ResultDto = new ResultDto<Xdpj14RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdpj14ReqDto转换成reqService
            BeanUtils.copyProperties(xdpj14ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ14.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            xdpj14ReqService.setService(reqService);
            // 将xdpj14ReqService转换成xdpj14ReqServiceMap
            Map xdpj14ReqServiceMap = beanMapUtil.beanToMap(xdpj14ReqService);
            context.put("tradeDataMap", xdpj14ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ14.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdpj14RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj14RespService.class, Xdpj14RespService.class);
            respService = xdpj14RespService.getService();

            xdpj14ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xdpj14ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xdpj14RespDto
                BeanUtils.copyProperties(respService, xdpj14RespDto);
                xdpj14ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdpj14ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdpj14ResultDto.setCode(EpbEnum.EPB099999.key);
                xdpj14ResultDto.setMessage(respService.getErorcd());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value, e.getMessage());
            xdpj14ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdpj14ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdpj14ResultDto.setData(xdpj14RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ14.key, EsbEnum.TRADE_CODE_XDPJ14.value, JSON.toJSONString(xdpj14ResultDto));
        return xdpj14ResultDto;
    }
}
