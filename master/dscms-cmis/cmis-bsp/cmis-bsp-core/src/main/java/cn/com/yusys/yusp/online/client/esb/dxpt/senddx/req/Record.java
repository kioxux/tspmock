package cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req;

/**
 * 请求Service：短信/微信发送批量接口
 *
 * @author hjk
 * @version 1.0
 */
public class Record {
    private String tranam;//帐户金额          ,帐户金额（微信时必须，短信时可不送）
    private String trantp;//帐户交易名        ,帐户交易名（微信时必须，短信时可不送）
    private String onlnbl;//帐户余额          ,帐户余额（微信时必须，短信时可不送）
    private String mobile;//手机号/微信号     ,手机号/微信号
    private String smstxt;//发送内容          ,发送内容

    public String getTranam() {
        return tranam;
    }

    public void setTranam(String tranam) {
        this.tranam = tranam;
    }

    public String getTrantp() {
        return trantp;
    }

    public void setTrantp(String trantp) {
        this.trantp = trantp;
    }

    public String getOnlnbl() {
        return onlnbl;
    }

    public void setOnlnbl(String onlnbl) {
        this.onlnbl = onlnbl;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSmstxt() {
        return smstxt;
    }

    public void setSmstxt(String smstxt) {
        this.smstxt = smstxt;
    }

    @Override
    public String toString() {
        return "Record{" +
                "tranam='" + tranam + '\'' +
                ", trantp='" + trantp + '\'' +
                ", onlnbl='" + onlnbl + '\'' +
                ", mobile='" + mobile + '\'' +
                ", smstxt='" + smstxt + '\'' +
                '}';
    }
}
