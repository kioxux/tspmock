package cn.com.yusys.yusp.web.server.biz.xdzc0020;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0020.req.Xdzc0020ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0020.resp.Xdzc0020RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0020.req.Xdzc0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0020.resp.Xdzc0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:出入池详情查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0020:出入池详情查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0020Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0020
     * 交易描述：出入池详情查询
     *
     * @param xdzc0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("出入池详情查询")
    @PostMapping("/xdzc0020")
    //@Idempotent({"xdzc020", "#xdzc0020ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0020RespDto xdzc0020(@Validated @RequestBody Xdzc0020ReqDto xdzc0020ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, JSON.toJSONString(xdzc0020ReqDto));
        Xdzc0020DataReqDto xdzc0020DataReqDto = new Xdzc0020DataReqDto();// 请求Data： 出入池详情查询
        Xdzc0020DataRespDto xdzc0020DataRespDto = new Xdzc0020DataRespDto();// 响应Data：出入池详情查询
        Xdzc0020RespDto xdzc0020RespDto = new Xdzc0020RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0020.req.Data reqData = null; // 请求Data：出入池详情查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0020.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0020.resp.Data();// 响应Data：出入池详情查询
        try {
            // 从 xdzc0020ReqDto获取 reqData
            reqData = xdzc0020ReqDto.getData();
            // 将 reqData 拷贝到xdzc0020DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0020DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, JSON.toJSONString(xdzc0020DataReqDto));
            ResultDto<Xdzc0020DataRespDto> xdzc0020DataResultDto = dscmsBizZcClientService.xdzc0020(xdzc0020DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, JSON.toJSONString(xdzc0020DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0020RespDto.setErorcd(Optional.ofNullable(xdzc0020DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0020RespDto.setErortx(Optional.ofNullable(xdzc0020DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0020DataResultDto.getCode())) {
                xdzc0020DataRespDto = xdzc0020DataResultDto.getData();
                xdzc0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0020DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, e.getMessage());
            xdzc0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0020RespDto.setDatasq(servsq);

        xdzc0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0020.key, DscmsEnum.TRADE_CODE_XDZC0020.value, JSON.toJSONString(xdzc0020RespDto));
        return xdzc0020RespDto;
    }
}
