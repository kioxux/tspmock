package cn.com.yusys.yusp.online.client.esb.ecif.s10501.resp;
/**
 * 响应Service：清单客户信息_ARRAY
 */
public class Record {
    private String custno;//客户编号
    private String custna;//客户名称
    private String custst;//客户状态
    private String idtftp;//证件类型
    private String idtfno;//证件号码
    private String prpsex;//性别
    private String borndt;//出生日期
    private String nation;//国籍
    private String creabr;//开户机构
    private String userid;//开户柜员
    private String opendt;//开户时间
    private String homead;//地址
    private String locpho;//电话

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getPrpsex() {
        return prpsex;
    }

    public void setPrpsex(String prpsex) {
        this.prpsex = prpsex;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getCreabr() {
        return creabr;
    }

    public void setCreabr(String creabr) {
        this.creabr = creabr;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOpendt() {
        return opendt;
    }

    public void setOpendt(String opendt) {
        this.opendt = opendt;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getLocpho() {
        return locpho;
    }

    public void setLocpho(String locpho) {
        this.locpho = locpho;
    }

    @Override
    public String toString() {
        return "Record{" +
                "custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", custst='" + custst + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", prpsex='" + prpsex + '\'' +
                ", borndt='" + borndt + '\'' +
                ", nation='" + nation + '\'' +
                ", creabr='" + creabr + '\'' +
                ", userid='" + userid + '\'' +
                ", opendt='" + opendt + '\'' +
                ", homead='" + homead + '\'' +
                ", locpho='" + locpho + '\'' +
                '}';
    }
}
