package cn.com.yusys.yusp.web.server.cus.xdkh0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0013.req.Xdkh0013ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0013.resp.Xdkh0013RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0013.req.Xdkh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0013.resp.Xdkh0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优享贷客户白名单信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0013:优享贷客户白名单信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0013Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0013
     * 交易描述：优享贷客户白名单信息查询
     *
     * @param xdkh0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优享贷客户白名单信息查询")
    @PostMapping("/xdkh0013")
    //@Idempotent({"xdcakh0013", "#xdkh0013ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0013RespDto xdkh0013(@Validated @RequestBody Xdkh0013ReqDto xdkh0013ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(xdkh0013ReqDto));
        Xdkh0013DataReqDto xdkh0013DataReqDto = new Xdkh0013DataReqDto();// 请求Data： 优享贷客户白名单信息查询
        Xdkh0013DataRespDto xdkh0013DataRespDto = new Xdkh0013DataRespDto();// 响应Data：优享贷客户白名单信息查询
        Xdkh0013RespDto xdkh0013RespDto = new Xdkh0013RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0013.req.Data reqData = null; // 请求Data：优享贷客户白名单信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0013.resp.Data();// 响应Data：优享贷客户白名单信息查询
        try {
            // 从 xdkh0013ReqDto获取 reqData
            reqData = xdkh0013ReqDto.getData();
            // 将 reqData 拷贝到xdkh0013DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0013DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(xdkh0013DataReqDto));
            ResultDto<Xdkh0013DataRespDto> xdkh0013DataResultDto = dscmsCusClientService.xdkh0013(xdkh0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(xdkh0013DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0013RespDto.setErorcd(Optional.ofNullable(xdkh0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0013RespDto.setErortx(Optional.ofNullable(xdkh0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0013DataResultDto.getCode())) {
                xdkh0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0013DataRespDto = xdkh0013DataResultDto.getData();
                // 将 xdkh0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0013DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, e.getMessage());
            xdkh0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0013RespDto.setDatasq(servsq);

        xdkh0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0013.key, DscmsEnum.TRADE_CODE_XDKH0013.value, JSON.toJSONString(xdkh0013RespDto));
        return xdkh0013RespDto;
    }
}

