package cn.com.yusys.yusp.web.client.esb.irs.xirs27;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs27.req.Xirs27ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs27.resp.Xirs27RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.xirs27.req.Xirs27ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.xirs27.resp.Xirs27RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(xirs27)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Xirs27Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xirs27Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 工作台提示条数（处理码IRS27）
     *
     * @param xirs27ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xirs27:工作台提示条数")
    @PostMapping("/xirs27")
    protected @ResponseBody
    ResultDto<Xirs27RespDto> xirs27(@Validated @RequestBody Xirs27ReqDto xirs27ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS27.key, EsbEnum.TRADE_CODE_IRS27.value, JSON.toJSONString(xirs27ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.xirs27.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.xirs27.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.xirs27.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.xirs27.resp.Service();
        Xirs27ReqService xirs27ReqService = new Xirs27ReqService();
        Xirs27RespService xirs27RespService = new Xirs27RespService();
        Xirs27RespDto xirs27RespDto = new Xirs27RespDto();
        ResultDto<Xirs27RespDto> xirs27ResultDto = new ResultDto<Xirs27RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Xirs27ReqDto转换成reqService
            BeanUtils.copyProperties(xirs27ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS27.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xirs27ReqService.setService(reqService);
            // 将xirs27ReqService转换成xirs27ReqServiceMap
            Map xirs27ReqServiceMap = beanMapUtil.beanToMap(xirs27ReqService);
            context.put("tradeDataMap", xirs27ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS27.key, EsbEnum.TRADE_CODE_IRS27.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS27.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS27.key, EsbEnum.TRADE_CODE_IRS27.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xirs27RespService = beanMapUtil.mapToBean(tradeDataMap, Xirs27RespService.class, Xirs27RespService.class);
            respService = xirs27RespService.getService();

            //  将Xirs27RespDto封装到ResultDto<Xirs27RespDto>
            xirs27ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xirs27ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (respService.getErorcd().equals("success")) {
                //  将respService转换成Xirs27RespDto
                BeanUtils.copyProperties(respService, xirs27RespDto);
                xirs27ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xirs27ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xirs27ResultDto.setCode(EpbEnum.EPB099999.key);
                xirs27ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS27.key, EsbEnum.TRADE_CODE_IRS27.value, e.getMessage());
            xirs27ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xirs27ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xirs27ResultDto.setData(xirs27RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS27.key, EsbEnum.TRADE_CODE_IRS27.value, JSON.toJSONString(xirs27ResultDto));
        return xirs27ResultDto;

    }
}
