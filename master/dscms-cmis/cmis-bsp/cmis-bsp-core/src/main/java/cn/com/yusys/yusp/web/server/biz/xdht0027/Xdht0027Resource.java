package cn.com.yusys.yusp.web.server.biz.xdht0027;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0027.req.Xdht0027ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0027.resp.Xdht0027RespDto;
import cn.com.yusys.yusp.dto.server.xdht0027.req.Xdht0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0027.resp.Xdht0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户调查表编号取得贷款合同主表的合同状态
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0027:根据客户调查表编号取得贷款合同主表的合同状态")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0027Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0027
     * 交易描述：根据客户调查表编号取得贷款合同主表的合同状态
     *
     * @param xdht0027ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户调查表编号取得贷款合同主表的合同状态")
    @PostMapping("/xdht0027")
    //@Idempotent({"xdcaht0027", "#xdht0027ReqDto.datasq"})
    protected @ResponseBody
    Xdht0027RespDto xdht0027(@Validated @RequestBody Xdht0027ReqDto xdht0027ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027ReqDto));
        Xdht0027DataReqDto xdht0027DataReqDto = new Xdht0027DataReqDto();// 请求Data： 根据客户调查表编号取得贷款合同主表的合同状态
        Xdht0027DataRespDto xdht0027DataRespDto = new Xdht0027DataRespDto();// 响应Data：根据客户调查表编号取得贷款合同主表的合同状态
        Xdht0027RespDto xdht0027RespDto = new Xdht0027RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0027.req.Data reqData = null; // 请求Data：根据客户调查表编号取得贷款合同主表的合同状态
        cn.com.yusys.yusp.dto.server.biz.xdht0027.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0027.resp.Data();// 响应Data：根据客户调查表编号取得贷款合同主表的合同状态

        try {
            // 从 xdht0027ReqDto获取 reqData
            reqData = xdht0027ReqDto.getData();
            // 将 reqData 拷贝到xdht0027DataReqDto
            BeanUtils.copyProperties(reqData, xdht0027DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027DataReqDto));
            ResultDto<Xdht0027DataRespDto> xdht0027DataResultDto = dscmsBizHtClientService.xdht0027(xdht0027DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0027RespDto.setErorcd(Optional.ofNullable(xdht0027DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0027RespDto.setErortx(Optional.ofNullable(xdht0027DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0027DataResultDto.getCode())) {
                xdht0027DataRespDto = xdht0027DataResultDto.getData();
                xdht0027RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0027RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0027DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0027DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, e.getMessage());
            xdht0027RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0027RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0027RespDto.setDatasq(servsq);

        xdht0027RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0027.key, DscmsEnum.TRADE_CODE_XDHT0027.value, JSON.toJSONString(xdht0027RespDto));
        return xdht0027RespDto;
    }
}
