package cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.resp;

/**
 * 响应Service：终审申请提交
 *
 * @author code-generator
 * @version 1.0
 */
public class Fbyd02RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

