package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp;

/**
 * 响应Dto：账单交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class D12011RespMessage {
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D12011RespMessage{" +
                "message=" + message +
                '}';
    }
}
