package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerownerlist;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:19
 * @since 2021/7/9 14:19
 */
public class Registerownerlist {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerownerlist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Registerownerlist{" +
                "record=" + record +
                '}';
    }
}
