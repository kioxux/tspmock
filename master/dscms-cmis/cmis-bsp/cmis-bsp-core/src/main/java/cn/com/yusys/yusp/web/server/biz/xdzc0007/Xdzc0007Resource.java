package cn.com.yusys.yusp.web.server.biz.xdzc0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0007.req.Xdzc0007ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0007.resp.Xdzc0007RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.req.Xdzc0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0007.resp.Xdzc0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池出池接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0007:资产池出池接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0007Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0007
     * 交易描述：资产池出池接口
     *
     * @param xdzc0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池出池接口")
    @PostMapping("/xdzc0007")
    //@Idempotent({"xdzc007", "#xdzc0007ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0007RespDto xdzc0007(@Validated @RequestBody Xdzc0007ReqDto xdzc0007ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, JSON.toJSONString(xdzc0007ReqDto));
        Xdzc0007DataReqDto xdzc0007DataReqDto = new Xdzc0007DataReqDto();// 请求Data： 资产池出池接口
        Xdzc0007DataRespDto xdzc0007DataRespDto = new Xdzc0007DataRespDto();// 响应Data：资产池出池接口
        Xdzc0007RespDto xdzc0007RespDto = new Xdzc0007RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0007.req.Data reqData = null; // 请求Data：资产池出池接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0007.resp.Data();// 响应Data：资产池出池接口

        try {
            // 从 xdzc0007ReqDto获取 reqData
            reqData = xdzc0007ReqDto.getData();
            // 将 reqData 拷贝到xdzc0007DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, JSON.toJSONString(xdzc0007DataReqDto));
            ResultDto<Xdzc0007DataRespDto> xdzc0007DataResultDto = dscmsBizZcClientService.xdzc0007(xdzc0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, JSON.toJSONString(xdzc0007DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0007RespDto.setErorcd(Optional.ofNullable(xdzc0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0007RespDto.setErortx(Optional.ofNullable(xdzc0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0007DataResultDto.getCode())) {
                xdzc0007DataRespDto = xdzc0007DataResultDto.getData();
                xdzc0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, e.getMessage());
            xdzc0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0007RespDto.setDatasq(servsq);

        xdzc0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0007.key, DscmsEnum.TRADE_CODE_XDZC0007.value, JSON.toJSONString(xdzc0007RespDto));
        return xdzc0007RespDto;
    }
}
