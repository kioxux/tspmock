package cn.com.yusys.yusp.web.client.esb.core.ln3063;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Ln3063ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Ln3063RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3063.req.Ln3063ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3063.resp.Ln3063RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3063)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3063Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3063Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3063
     * 交易描述：资产转让处理
     *
     * @param ln3063ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3063:资产转让处理")
    @PostMapping("/ln3063")
    protected @ResponseBody
    ResultDto<Ln3063RespDto> ln3063(@Validated @RequestBody Ln3063ReqDto ln3063ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3063.key, EsbEnum.TRADE_CODE_LN3063.value, JSON.toJSONString(ln3063ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3063.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3063.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3063.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3063.resp.Service();

        Ln3063ReqService ln3063ReqService = new Ln3063ReqService();
        Ln3063RespService ln3063RespService = new Ln3063RespService();
        Ln3063RespDto ln3063RespDto = new Ln3063RespDto();
        ResultDto<Ln3063RespDto> ln3063ResultDto = new ResultDto<Ln3063RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3063ReqDto转换成reqService
            BeanUtils.copyProperties(ln3063ReqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(ln3063ReqDto.getLstzrjj())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3063.Lstzrjj> ln3063ReqLstzrjjDtoList = Optional.ofNullable(ln3063ReqDto.getLstzrjj())
                        .orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj.List ln3063ReqLstzrjjList
                        = Optional.ofNullable(reqService.getLstzrjj_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj.List());
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj.Record> ln3063ReqLstzrjjRecords = ln3063ReqLstzrjjDtoList.parallelStream()
                        .map(lstzrjj -> {
                            cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj.Record ln3063ReqLstzrjjRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj.Record();
                            BeanUtils.copyProperties(lstzrjj, ln3063ReqLstzrjjRecord);
                            return ln3063ReqLstzrjjRecord;
                        }).collect(Collectors.toList());
                ln3063ReqLstzrjjList.setRecord(ln3063ReqLstzrjjRecords);
                reqService.setLstzrjj_ARRAY(ln3063ReqLstzrjjList);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3063.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3063ReqService.setService(reqService);
            // 将ln3063ReqService转换成ln3063ReqServiceMap
            Map ln3063ReqServiceMap = beanMapUtil.beanToMap(ln3063ReqService);
            context.put("tradeDataMap", ln3063ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3063.key, EsbEnum.TRADE_CODE_LN3063.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3063.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3063.key, EsbEnum.TRADE_CODE_LN3063.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3063RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3063RespService.class, Ln3063RespService.class);
            respService = ln3063RespService.getService();

            ln3063ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3063ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3063RespDto
                BeanUtils.copyProperties(respService, ln3063RespDto);
                ln3063ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3063ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3063ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3063ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3063.key, EsbEnum.TRADE_CODE_LN3063.value, e.getMessage());
            ln3063ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3063ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3063ResultDto.setData(ln3063RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3063.key, EsbEnum.TRADE_CODE_LN3063.value, JSON.toJSONString(ln3063ResultDto));
        return ln3063ResultDto;
    }
}
