package cn.com.yusys.yusp.web.server.biz.xdzc0023;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0023.req.Xdzc0023ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0023.resp.Xdzc0023RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0023.req.Xdzc0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0023.resp.Xdzc0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池白名单
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0023:资产池白名单")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0023Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0023
     * 交易描述：资产池白名单
     *
     * @param xdzc0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池白名单")
    @PostMapping("/xdzc0023")
    //@Idempotent({"xdzc023", "#xdzc0023ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0023RespDto xdzc0023(@Validated @RequestBody Xdzc0023ReqDto xdzc0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, JSON.toJSONString(xdzc0023ReqDto));
        Xdzc0023DataReqDto xdzc0023DataReqDto = new Xdzc0023DataReqDto();// 请求Data： 资产池白名单
        Xdzc0023DataRespDto xdzc0023DataRespDto = new Xdzc0023DataRespDto();// 响应Data：资产池白名单
        Xdzc0023RespDto xdzc0023RespDto = new Xdzc0023RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0023.req.Data reqData = null; // 请求Data：资产池白名单
        cn.com.yusys.yusp.dto.server.biz.xdzc0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0023.resp.Data();// 响应Data：资产池白名单

        try {
            // 从 xdzc0023ReqDto获取 reqData
            reqData = xdzc0023ReqDto.getData();
            // 将 reqData 拷贝到xdzc0023DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0023DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, JSON.toJSONString(xdzc0023DataReqDto));
            ResultDto<Xdzc0023DataRespDto> xdzc0023DataResultDto = dscmsBizZcClientService.xdzc0023(xdzc0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, JSON.toJSONString(xdzc0023DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0023RespDto.setErorcd(Optional.ofNullable(xdzc0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0023RespDto.setErortx(Optional.ofNullable(xdzc0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0023DataResultDto.getCode())) {
                xdzc0023DataRespDto = xdzc0023DataResultDto.getData();
                xdzc0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0023DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, e.getMessage());
            xdzc0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0023RespDto.setDatasq(servsq);

        xdzc0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0023.key, DscmsEnum.TRADE_CODE_XDZC0023.value, JSON.toJSONString(xdzc0023RespDto));
        return xdzc0023RespDto;
    }
}
