package cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req;

import java.math.BigDecimal;
/**
 * 请求Service：信用证信息同步
 * @author zhugenrong
 * @version 1.0
 */
public class Record {
    private String guar_cus_id;//所有权人编号
    private String guar_cus_name;//所有权人名称
    private String guar_cert_type;//押品所有人证件类型
    private String guar_cert_code;//押品所有人证件号码
    private String guar_name;//抵质押品名称
    private String guar_type_cd;//担保分类代码
    private String create_sys;//创建系统
    private String account_manager;//管户人
    private String guar_lastupdate_date;//最后更新时间
    private String lastmodify_userid;//最后修改人
    private String lastmodify_orgid;//最后修改人机构
    private String guar_cus_type;//押品所有人类型
    private String common_assets_ind;//是否共有财产
    private String is_ownership_clear;//是否权属清晰
    private String insurance_ind;//是否需要办理保险
    private String relation_int;//是否实质正相关
    private BigDecimal legal_pri_payment;//法定优先受偿款
    private String def_effect_type;//担保权生效方式
    private String contract_justice_ind;//是否需要抵质押合同公证
    private String other_back_guar_ind;//他行是否已设定担保权
    private String if_deal;//是否抵债资产
    private String guar_borrower_rela;//抵质押物与借款人相关性
    private String shut_down_conv;//查封便利性
    private String legal_validity;//法律有效性
    private String guar_universality;//抵质押品通用性
    private String sale_state;//抵质押品变现能力
    private String price_volatility;//价格波动性
    private String pledge_register_ind;//是否进行人行质押登记
    private BigDecimal face_value_price;//票面金额
    private String cur_type;//币种
    private String payment_name;//付款人名称
    private String payment_inner_level;//付款方内部评级
    private String receipt_no;//发票编号(BP号)
    private String receipt_date;//发票日期
    private String special_account_no;//专用账户账号
    private String special_account_name;//专用账户名称
    private String account_receivable_ind;//应收账款是否由销售、出租、或提供服务产生债权
    private String securitization_ind;//是否与证券化、从属参与或信用衍生工具相关

    public String getGuar_cus_id() {
        return guar_cus_id;
    }

    public void setGuar_cus_id(String guar_cus_id) {
        this.guar_cus_id = guar_cus_id;
    }

    public String getGuar_cus_name() {
        return guar_cus_name;
    }

    public void setGuar_cus_name(String guar_cus_name) {
        this.guar_cus_name = guar_cus_name;
    }

    public String getGuar_cert_type() {
        return guar_cert_type;
    }

    public void setGuar_cert_type(String guar_cert_type) {
        this.guar_cert_type = guar_cert_type;
    }

    public String getGuar_cert_code() {
        return guar_cert_code;
    }

    public void setGuar_cert_code(String guar_cert_code) {
        this.guar_cert_code = guar_cert_code;
    }

    public String getGuar_name() {
        return guar_name;
    }

    public void setGuar_name(String guar_name) {
        this.guar_name = guar_name;
    }

    public String getGuar_type_cd() {
        return guar_type_cd;
    }

    public void setGuar_type_cd(String guar_type_cd) {
        this.guar_type_cd = guar_type_cd;
    }

    public String getCreate_sys() {
        return create_sys;
    }

    public void setCreate_sys(String create_sys) {
        this.create_sys = create_sys;
    }

    public String getAccount_manager() {
        return account_manager;
    }

    public void setAccount_manager(String account_manager) {
        this.account_manager = account_manager;
    }

    public String getGuar_lastupdate_date() {
        return guar_lastupdate_date;
    }

    public void setGuar_lastupdate_date(String guar_lastupdate_date) {
        this.guar_lastupdate_date = guar_lastupdate_date;
    }

    public String getLastmodify_userid() {
        return lastmodify_userid;
    }

    public void setLastmodify_userid(String lastmodify_userid) {
        this.lastmodify_userid = lastmodify_userid;
    }

    public String getLastmodify_orgid() {
        return lastmodify_orgid;
    }

    public void setLastmodify_orgid(String lastmodify_orgid) {
        this.lastmodify_orgid = lastmodify_orgid;
    }

    public String getGuar_cus_type() {
        return guar_cus_type;
    }

    public void setGuar_cus_type(String guar_cus_type) {
        this.guar_cus_type = guar_cus_type;
    }

    public String getCommon_assets_ind() {
        return common_assets_ind;
    }

    public void setCommon_assets_ind(String common_assets_ind) {
        this.common_assets_ind = common_assets_ind;
    }

    public String getIs_ownership_clear() {
        return is_ownership_clear;
    }

    public void setIs_ownership_clear(String is_ownership_clear) {
        this.is_ownership_clear = is_ownership_clear;
    }

    public String getInsurance_ind() {
        return insurance_ind;
    }

    public void setInsurance_ind(String insurance_ind) {
        this.insurance_ind = insurance_ind;
    }

    public String getRelation_int() {
        return relation_int;
    }

    public void setRelation_int(String relation_int) {
        this.relation_int = relation_int;
    }

    public BigDecimal getLegal_pri_payment() {
        return legal_pri_payment;
    }

    public void setLegal_pri_payment(BigDecimal legal_pri_payment) {
        this.legal_pri_payment = legal_pri_payment;
    }

    public String getDef_effect_type() {
        return def_effect_type;
    }

    public void setDef_effect_type(String def_effect_type) {
        this.def_effect_type = def_effect_type;
    }

    public String getContract_justice_ind() {
        return contract_justice_ind;
    }

    public void setContract_justice_ind(String contract_justice_ind) {
        this.contract_justice_ind = contract_justice_ind;
    }

    public String getOther_back_guar_ind() {
        return other_back_guar_ind;
    }

    public void setOther_back_guar_ind(String other_back_guar_ind) {
        this.other_back_guar_ind = other_back_guar_ind;
    }

    public String getIf_deal() {
        return if_deal;
    }

    public void setIf_deal(String if_deal) {
        this.if_deal = if_deal;
    }

    public String getGuar_borrower_rela() {
        return guar_borrower_rela;
    }

    public void setGuar_borrower_rela(String guar_borrower_rela) {
        this.guar_borrower_rela = guar_borrower_rela;
    }

    public String getShut_down_conv() {
        return shut_down_conv;
    }

    public void setShut_down_conv(String shut_down_conv) {
        this.shut_down_conv = shut_down_conv;
    }

    public String getLegal_validity() {
        return legal_validity;
    }

    public void setLegal_validity(String legal_validity) {
        this.legal_validity = legal_validity;
    }

    public String getGuar_universality() {
        return guar_universality;
    }

    public void setGuar_universality(String guar_universality) {
        this.guar_universality = guar_universality;
    }

    public String getSale_state() {
        return sale_state;
    }

    public void setSale_state(String sale_state) {
        this.sale_state = sale_state;
    }

    public String getPrice_volatility() {
        return price_volatility;
    }

    public void setPrice_volatility(String price_volatility) {
        this.price_volatility = price_volatility;
    }

    public String getPledge_register_ind() {
        return pledge_register_ind;
    }

    public void setPledge_register_ind(String pledge_register_ind) {
        this.pledge_register_ind = pledge_register_ind;
    }

    public BigDecimal getFace_value_price() {
        return face_value_price;
    }

    public void setFace_value_price(BigDecimal face_value_price) {
        this.face_value_price = face_value_price;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }

    public String getPayment_inner_level() {
        return payment_inner_level;
    }

    public void setPayment_inner_level(String payment_inner_level) {
        this.payment_inner_level = payment_inner_level;
    }

    public String getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(String receipt_no) {
        this.receipt_no = receipt_no;
    }

    public String getReceipt_date() {
        return receipt_date;
    }

    public void setReceipt_date(String receipt_date) {
        this.receipt_date = receipt_date;
    }

    public String getSpecial_account_no() {
        return special_account_no;
    }

    public void setSpecial_account_no(String special_account_no) {
        this.special_account_no = special_account_no;
    }

    public String getSpecial_account_name() {
        return special_account_name;
    }

    public void setSpecial_account_name(String special_account_name) {
        this.special_account_name = special_account_name;
    }

    public String getAccount_receivable_ind() {
        return account_receivable_ind;
    }

    public void setAccount_receivable_ind(String account_receivable_ind) {
        this.account_receivable_ind = account_receivable_ind;
    }

    public String getSecuritization_ind() {
        return securitization_ind;
    }

    public void setSecuritization_ind(String securitization_ind) {
        this.securitization_ind = securitization_ind;
    }

    @Override
    public String toString() {
        return "Record{" +
                "guar_cus_id='" + guar_cus_id + '\'' +
                ", guar_cus_name='" + guar_cus_name + '\'' +
                ", guar_cert_type='" + guar_cert_type + '\'' +
                ", guar_cert_code='" + guar_cert_code + '\'' +
                ", guar_name='" + guar_name + '\'' +
                ", guar_type_cd='" + guar_type_cd + '\'' +
                ", create_sys='" + create_sys + '\'' +
                ", account_manager='" + account_manager + '\'' +
                ", guar_lastupdate_date='" + guar_lastupdate_date + '\'' +
                ", lastmodify_userid='" + lastmodify_userid + '\'' +
                ", lastmodify_orgid='" + lastmodify_orgid + '\'' +
                ", guar_cus_type='" + guar_cus_type + '\'' +
                ", common_assets_ind='" + common_assets_ind + '\'' +
                ", is_ownership_clear='" + is_ownership_clear + '\'' +
                ", insurance_ind='" + insurance_ind + '\'' +
                ", relation_int='" + relation_int + '\'' +
                ", legal_pri_payment=" + legal_pri_payment +
                ", def_effect_type='" + def_effect_type + '\'' +
                ", contract_justice_ind='" + contract_justice_ind + '\'' +
                ", other_back_guar_ind='" + other_back_guar_ind + '\'' +
                ", if_deal='" + if_deal + '\'' +
                ", guar_borrower_rela='" + guar_borrower_rela + '\'' +
                ", shut_down_conv='" + shut_down_conv + '\'' +
                ", legal_validity='" + legal_validity + '\'' +
                ", guar_universality='" + guar_universality + '\'' +
                ", sale_state='" + sale_state + '\'' +
                ", price_volatility='" + price_volatility + '\'' +
                ", pledge_register_ind='" + pledge_register_ind + '\'' +
                ", face_value_price=" + face_value_price +
                ", cur_type='" + cur_type + '\'' +
                ", payment_name='" + payment_name + '\'' +
                ", payment_inner_level='" + payment_inner_level + '\'' +
                ", receipt_no='" + receipt_no + '\'' +
                ", receipt_date='" + receipt_date + '\'' +
                ", special_account_no='" + special_account_no + '\'' +
                ", special_account_name='" + special_account_name + '\'' +
                ", account_receivable_ind='" + account_receivable_ind + '\'' +
                ", securitization_ind='" + securitization_ind + '\'' +
                '}';
    }
}
