package cn.com.yusys.yusp.web.client.esb.core.ln3057;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3057.req.Ln3057ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3057.resp.Ln3057RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3057.req.Ln3057ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3057.resp.Ln3057RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3057)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3057Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3057Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * 交易码：ln3057
     * 交易描述：交易说明
     *
     * @param ln3057ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3057:主要实现对贷款期限的缩短和延长修改，实时生效")
    @PostMapping("/ln3057")
    protected @ResponseBody
    ResultDto<Ln3057RespDto> ln3057(@Validated @RequestBody Ln3057ReqDto ln3057ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3057.key, EsbEnum.TRADE_CODE_LN3057.value, JSON.toJSONString(ln3057ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3057.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3057.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3057.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3057.resp.Service();
        Ln3057ReqService ln3057ReqService = new Ln3057ReqService();
        Ln3057RespService ln3057RespService = new Ln3057RespService();
        Ln3057RespDto ln3057RespDto = new Ln3057RespDto();
        ResultDto<Ln3057RespDto> ln3057ResultDto = new ResultDto<Ln3057RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将ln3057ReqDto转换成reqService
            BeanUtils.copyProperties(ln3057ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3057.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3057ReqService.setService(reqService);
            // 将ln3057ReqService转换成ln3057ReqServiceMap
            Map ln3057ReqServiceMap = beanMapUtil.beanToMap(ln3057ReqService);
            context.put("tradeDataMap", ln3057ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3057.key, EsbEnum.TRADE_CODE_LN3057.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3057.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3057.key, EsbEnum.TRADE_CODE_LN3057.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3057RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3057RespService.class, Ln3057RespService.class);
            respService = ln3057RespService.getService();
            //  将respService转换成Ln3057RespDto
            BeanUtils.copyProperties(respService, ln3057RespDto);
            ln3057ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3057ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, ln3057RespDto);
                ln3057ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3057ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3057ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3057ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3057.key, EsbEnum.TRADE_CODE_LN3057.value, e.getMessage());
            ln3057ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3057ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3057ResultDto.setData(ln3057RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3057.key, EsbEnum.TRADE_CODE_LN3057.value, JSON.toJSONString(ln3057ResultDto));
        return ln3057ResultDto;
    }
}
