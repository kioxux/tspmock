package cn.com.yusys.yusp.online.client.esb.core.da3302.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵债资产处置
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String yngyjigo;//营业机构
    private String kehuhaoo;//客户号
    private String kehuzwmc;//客户名
    private String dzzcbxkx;//抵债资产变现款项账号
    private String dzzcbzxh;//抵债资产变现账号子序号
    private BigDecimal dzzcbxje;//抵债资产变现款金额
    private BigDecimal daizdzzc;//待转抵债资产
    private BigDecimal dcldzzic;//待处理抵债资产
    private BigDecimal dbxdzzic;//待变现抵债资产
    private BigDecimal bxssjine;//变现损失金额
    private BigDecimal yicldzzc;//已处置抵债资产
    private BigDecimal keyongje;//可用金额
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getDzzcbxkx() {
        return dzzcbxkx;
    }

    public void setDzzcbxkx(String dzzcbxkx) {
        this.dzzcbxkx = dzzcbxkx;
    }

    public String getDzzcbzxh() {
        return dzzcbzxh;
    }

    public void setDzzcbzxh(String dzzcbzxh) {
        this.dzzcbzxh = dzzcbzxh;
    }

    public BigDecimal getDzzcbxje() {
        return dzzcbxje;
    }

    public void setDzzcbxje(BigDecimal dzzcbxje) {
        this.dzzcbxje = dzzcbxje;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getBxssjine() {
        return bxssjine;
    }

    public void setBxssjine(BigDecimal bxssjine) {
        this.bxssjine = bxssjine;
    }

    public BigDecimal getYicldzzc() {
        return yicldzzc;
    }

    public void setYicldzzc(BigDecimal yicldzzc) {
        this.yicldzzc = yicldzzc;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", dzzcbxkx='" + dzzcbxkx + '\'' +
                ", dzzcbzxh='" + dzzcbzxh + '\'' +
                ", dzzcbxje=" + dzzcbxje +
                ", daizdzzc=" + daizdzzc +
                ", dcldzzic=" + dcldzzic +
                ", dbxdzzic=" + dbxdzzic +
                ", bxssjine=" + bxssjine +
                ", yicldzzc=" + yicldzzc +
                ", keyongje=" + keyongje +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}
