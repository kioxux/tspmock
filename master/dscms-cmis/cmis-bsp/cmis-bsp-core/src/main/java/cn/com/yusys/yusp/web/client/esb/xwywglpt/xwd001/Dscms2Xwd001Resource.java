package cn.com.yusys.yusp.web.client.esb.xwywglpt.xwd001;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.req.Xwd001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.resp.Xwd001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.req.Xwd001ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.resp.Xwd001RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @类名 Dscms2Xwd001Resource
 * @描述 BSP封装调用外部数据平台的接口
 * @作者 黄勃
 * @时间 2021/10/12 19:19
 **/
@Api(tags = "BSP封装调用外部数据平台的接口处理类(xwd001)")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Xwd001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xwd001Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 贷款申请接口
     *
     * @param xwd001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xwd001:贷款申请接口")
    @PostMapping("/xwd001")
    protected @ResponseBody
    ResultDto<Xwd001RespDto> xwd001(@Validated @RequestBody Xwd001ReqDto xwd001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD001.key, EsbEnum.TRADE_CODE_XWD001.value, JSON.toJSONString(xwd001ReqDto));

        cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.resp.Service();
        Xwd001ReqService xwd001ReqService = new Xwd001ReqService();
        Xwd001RespService xwd001RespService = new Xwd001RespService();
        Xwd001RespDto xwd001RespDto = new Xwd001RespDto();
        ResultDto<Xwd001RespDto> xwd001ResultDto = new ResultDto<Xwd001RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xwd001ReqDto转换成reqService
            BeanUtils.copyProperties(xwd001ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XWD001.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            xwd001ReqService.setService(reqService);
            // 将xwd001ReqService转换成xwd001ReqServiceMap
            Map xwd001ReqServiceMap = beanMapUtil.beanToMap(xwd001ReqService);
            context.put("tradeDataMap", xwd001ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD001.key, EsbEnum.TRADE_CODE_XWD001.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XWD001.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD001.key, EsbEnum.TRADE_CODE_XWD001.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xwd001RespService = beanMapUtil.mapToBean(tradeDataMap, Xwd001RespService.class, Xwd001RespService.class);
            respService = xwd001RespService.getService();

            xwd001ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xwd001ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, xwd001RespDto);
                xwd001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xwd001ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xwd001ResultDto.setCode(EpbEnum.EPB099999.key);
                xwd001ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD001.key, EsbEnum.TRADE_CODE_XWD001.value, e.getMessage());
            xwd001ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xwd001ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xwd001ResultDto.setData(xwd001RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD001.key, EsbEnum.TRADE_CODE_XWD001.value, JSON.toJSONString(xwd001ResultDto));
        return xwd001ResultDto;
    }
}
