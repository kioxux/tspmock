package cn.com.yusys.yusp.online.client.esb.core.ln3066.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 14:53
 * @since 2021/6/7 14:53
 */
public class Record {
    private String dkjiejuh;//贷款借据号
    private String huobdhao;//货币代号
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String kshchpdm;//可售产品代码
    private String kshchpmc;//可售产品名称
    private String kaihriqi;//开户日期
    private String daoqriqi;//到期日期
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String dkzhhzht;//贷款账户状态
    private String daikxtai;//贷款形态
    private String jzhdkbzh;//减值贷款标志
    private BigDecimal jiejuuje;//借据金额
    private BigDecimal zhchbjin;//正常本金
    private BigDecimal yuqibjin;//逾期本金
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal ysqianxi;//应收欠息
    private BigDecimal ysyjfaxi;//应收应计罚息
    private BigDecimal yshofaxi;//应收罚息
    private BigDecimal yijtdklx;//已计提贷款利息
    private BigDecimal lixishru;//利息收入

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getJzhdkbzh() {
        return jzhdkbzh;
    }

    public void setJzhdkbzh(String jzhdkbzh) {
        this.jzhdkbzh = jzhdkbzh;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getYijtdklx() {
        return yijtdklx;
    }

    public void setYijtdklx(BigDecimal yijtdklx) {
        this.yijtdklx = yijtdklx;
    }

    public BigDecimal getLixishru() {
        return lixishru;
    }

    public void setLixishru(BigDecimal lixishru) {
        this.lixishru = lixishru;
    }

    @Override
    public String toString() {
        return "Service{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "kshchpdm='" + kshchpdm + '\'' +
                "kshchpmc='" + kshchpmc + '\'' +
                "kaihriqi='" + kaihriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dkzhhzht='" + dkzhhzht + '\'' +
                "daikxtai='" + daikxtai + '\'' +
                "jzhdkbzh='" + jzhdkbzh + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "yuqibjin='" + yuqibjin + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "yijtdklx='" + yijtdklx + '\'' +
                "lixishru='" + lixishru + '\'' +
                '}';
    }
}
