package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 最高额授信协议信息(UpApplyInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
public class UpApplyInfo {

    private List<UpApplyInfoRecord> record; // 最高额授信协议信息

    public List<UpApplyInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<UpApplyInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "UpApplyInfo{" +
                "record=" + record +
                '}';
    }
}
