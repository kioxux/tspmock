package cn.com.yusys.yusp.web.server.biz.xdtz0048;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0048.req.Xdtz0048ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0048.resp.Xdtz0048RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0048.req.Xdtz0048DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0048.resp.Xdtz0048DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小贷借款借据文本生成pdf
 * @author zoubiao
 * @version 1.0
 */
@Api(tags = "XDTZ0048:小贷借款借据文本生成pdf")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0048Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0048Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0048
     * 交易描述：小贷借款借据文本生成pdf
     *
     * @param xdtz0048ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小贷借款借据文本生成pdf")
    @PostMapping("/xdtz0048")
    //@Idempotent({"xdtz0048", "#xdtz0048ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0048RespDto xdtz0048(@Validated @RequestBody Xdtz0048ReqDto xdtz0048ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048ReqDto));
        Xdtz0048DataReqDto xdtz0048DataReqDto = new Xdtz0048DataReqDto();// 请求Data： 小贷借款借据文本生成pdf
        Xdtz0048DataRespDto xdtz0048DataRespDto = new Xdtz0048DataRespDto();// 响应Data：小贷借款借据文本生成pdf
        Xdtz0048RespDto xdtz0048RespDto = new Xdtz0048RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0048.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdtz0048.req.Data(); // 请求Data：小贷借款借据文本生成pdf
        cn.com.yusys.yusp.dto.server.biz.xdtz0048.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0048.resp.Data();// 响应Data：小贷借款借据文本生成pdf
        try {
            // 从 xdtz0048ReqDto获取 reqData
            reqData = xdtz0048ReqDto.getData();
            // 将 reqData 拷贝到xdtz0048DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0048DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataReqDto));
            ResultDto<Xdtz0048DataRespDto> xdtz0048DataResultDto = dscmsBizTzClientService.xdtz0048(xdtz0048DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0048RespDto.setErorcd(Optional.ofNullable(xdtz0048DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0048RespDto.setErortx(Optional.ofNullable(xdtz0048DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0048DataResultDto.getCode())) {
                xdtz0048DataRespDto = xdtz0048DataResultDto.getData();
                xdtz0048RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0048RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0048DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0048DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, e.getMessage());
            xdtz0048RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0048RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0048RespDto.setDatasq(servsq);

        xdtz0048RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0048.key, DscmsEnum.TRADE_CODE_XDTZ0048.value, JSON.toJSONString(xdtz0048RespDto));
        return xdtz0048RespDto;
    }
}