package cn.com.yusys.yusp.online.client.esb.core.dp2021.resp;

/**
 * 响应Service：客户账号-子账号互查
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Lstacctinfo list;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Lstacctinfo getList() {
        return list;
    }

    public void setList(Lstacctinfo list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", list=" + list +
                '}';
    }
}
