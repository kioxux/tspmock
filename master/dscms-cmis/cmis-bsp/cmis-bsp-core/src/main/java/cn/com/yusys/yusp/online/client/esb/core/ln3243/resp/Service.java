package cn.com.yusys.yusp.online.client.esb.core.ln3243.resp;

import java.math.BigDecimal;

/**
 * 响应Service：停息贷款利息试算
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String dkjiejuh;//贷款借据号
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String huankfsh;//还款方式
    private BigDecimal daikuyue;//贷款余额
    private BigDecimal zhchbjin;//正常本金
    private BigDecimal yuqibjin;//逾期本金
    private String qishriqi;//起始日期
    private String zhzhriqi;//终止日期
    private BigDecimal qianxiii;//欠息
    private BigDecimal faxiiiii;//罚息
    private BigDecimal whqianxi;//未偿还欠息总额

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getDaikuyue() {
        return daikuyue;
    }

    public void setDaikuyue(BigDecimal daikuyue) {
        this.daikuyue = daikuyue;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public BigDecimal getQianxiii() {
        return qianxiii;
    }

    public void setQianxiii(BigDecimal qianxiii) {
        this.qianxiii = qianxiii;
    }

    public BigDecimal getFaxiiiii() {
        return faxiiiii;
    }

    public void setFaxiiiii(BigDecimal faxiiiii) {
        this.faxiiiii = faxiiiii;
    }

    public BigDecimal getWhqianxi() {
        return whqianxi;
    }

    public void setWhqianxi(BigDecimal whqianxi) {
        this.whqianxi = whqianxi;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", daikuyue=" + daikuyue +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", qianxiii=" + qianxiii +
                ", faxiiiii=" + faxiiiii +
                ", whqianxi=" + whqianxi +
                '}';
    }
}
