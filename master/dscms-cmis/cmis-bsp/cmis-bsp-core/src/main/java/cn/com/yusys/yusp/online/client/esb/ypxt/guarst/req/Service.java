package cn.com.yusys.yusp.online.client.esb.ypxt.guarst.req;

/**
 * 请求Service：押品状态变更推送
 */
public class Service {
	private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
	private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
	private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
	private String userid;//	柜员号	否	char(7)	是		userid
	private String brchno;//	部门号	否	char(5)	是		brchno
	private String datasq; //   全局流水
	private String servdt;//    交易日期
	private String servti;//    交易时间

    private String distco;//区县代码
    private String certnu;//不动产权证书号
    private String status;//押品状态

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getDistco() {
        return distco;
    }

    public void setDistco(String distco) {
        this.distco = distco;
    }

    public String getCertnu() {
        return certnu;
    }

    public void setCertnu(String certnu) {
        this.certnu = certnu;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", datasq='" + datasq + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", distco='" + distco + '\'' +
				", certnu='" + certnu + '\'' +
				", status='" + status + '\'' +
				'}';
	}
}
