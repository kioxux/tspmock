package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.resp;

/**
 * 响应Service：票据承兑签发审批请求
 *
 * @author lihh
 * @version 1.0
 */
public class Xdpj03RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
