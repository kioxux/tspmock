package cn.com.yusys.yusp.web.server.biz.xdht0017;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0017.req.Xdht0017ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0017.resp.Xdht0017RespDto;
import cn.com.yusys.yusp.dto.server.xdht0017.req.Xdht0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0017.resp.Xdht0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借款、担保合同PDF生成
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0017:借款、担保合同PDF生成")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0017Resource.class);

    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0017
     * 交易描述：借款、担保合同PDF生成
     *
     * @param xdht0017ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借款、担保合同PDF生成")
    @PostMapping("/xdht0017")
    //@Idempotent({"xdcaht0017", "#xdht0017ReqDto.datasq"})
    protected @ResponseBody
    Xdht0017RespDto xdht0017(@Validated @RequestBody Xdht0017ReqDto xdht0017ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017ReqDto));
        Xdht0017DataReqDto xdht0017DataReqDto = new Xdht0017DataReqDto();// 请求Data： 借款、担保合同PDF生成
        Xdht0017DataRespDto xdht0017DataRespDto = new Xdht0017DataRespDto();// 响应Data：借款、担保合同PDF生成
        Xdht0017RespDto xdht0017RespDto = new Xdht0017RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0017.req.Data reqData = null; // 请求Data：借款、担保合同PDF生成
        cn.com.yusys.yusp.dto.server.biz.xdht0017.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0017.resp.Data();// 响应Data：借款、担保合同PDF生成

        try {
            // 从 xdht0017ReqDto获取 reqData
            reqData = xdht0017ReqDto.getData();
            // 将 reqData 拷贝到xdht0017DataReqDto
            BeanUtils.copyProperties(reqData, xdht0017DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataReqDto));
            ResultDto<Xdht0017DataRespDto> xdht0017DataResultDto = dscmsBizHtClientService.xdht0017(xdht0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0017RespDto.setErorcd(Optional.ofNullable(xdht0017DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0017RespDto.setErortx(Optional.ofNullable(xdht0017DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0017DataResultDto.getCode())) {
                xdht0017DataRespDto = xdht0017DataResultDto.getData();
                xdht0017RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0017RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0017DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0017DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, e.getMessage());
            xdht0017RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0017RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0017RespDto.setDatasq(servsq);

        xdht0017RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0017.key, DscmsEnum.TRADE_CODE_XDHT0017.value, JSON.toJSONString(xdht0017RespDto));
        return xdht0017RespDto;
    }
}
