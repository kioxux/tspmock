package cn.com.yusys.yusp.web.server.biz.xdzc0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0006.req.Xdzc0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0006.resp.Xdzc0006RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0006.req.Xdzc0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0006.resp.Xdzc0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池出池校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0006:资产池出池校验接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0006Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0006
     * 交易描述：资产池出池校验接口
     *
     * @param xdzc0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池出池校验接口")
    @PostMapping("/xdzc0006")
    //@Idempotent({"xdzc006", "#xdzc0006ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0006RespDto xdzc0006(@Validated @RequestBody Xdzc0006ReqDto xdzc0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, JSON.toJSONString(xdzc0006ReqDto));
        Xdzc0006DataReqDto xdzc0006DataReqDto = new Xdzc0006DataReqDto();// 请求Data： 资产池出池校验接口
        Xdzc0006DataRespDto xdzc0006DataRespDto = new Xdzc0006DataRespDto();// 响应Data：资产池出池校验接口
        Xdzc0006RespDto xdzc0006RespDto = new Xdzc0006RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0006.req.Data reqData = null; // 请求Data：资产池出池校验接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0006.resp.Data();// 响应Data：资产池出池校验接口

        try {
            // 从 xdzc0006ReqDto获取 reqData
            reqData = xdzc0006ReqDto.getData();
            // 将 reqData 拷贝到xdzc0006DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0006DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, JSON.toJSONString(xdzc0006DataReqDto));
            ResultDto<Xdzc0006DataRespDto> xdzc0006DataResultDto = dscmsBizZcClientService.xdzc0006(xdzc0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, JSON.toJSONString(xdzc0006DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0006RespDto.setErorcd(Optional.ofNullable(xdzc0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0006RespDto.setErortx(Optional.ofNullable(xdzc0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0006DataResultDto.getCode())) {
                xdzc0006DataRespDto = xdzc0006DataResultDto.getData();
                xdzc0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, e.getMessage());
            xdzc0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0006RespDto.setDatasq(servsq);

        xdzc0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0006.key, DscmsEnum.TRADE_CODE_XDZC0006.value, JSON.toJSONString(xdzc0006RespDto));
        return xdzc0006RespDto;
    }
}
