package cn.com.yusys.yusp.web.server.biz.xdxw0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0006.req.Xdxw0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0006.resp.Xdxw0006RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0006.req.Xdxw0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0006.resp.Xdxw0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:联系人信息维护
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0006:联系人信息维护")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0006Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0006
     * 交易描述：联系人信息维护
     *
     * @param xdxw0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("联系人信息维护")
    @PostMapping("/xdxw0006")
    //@Idempotent({"xdcaxw0006", "#xdxw0006ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0006RespDto xdxw0006(@Validated @RequestBody Xdxw0006ReqDto xdxw0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006ReqDto));
        Xdxw0006DataReqDto xdxw0006DataReqDto = new Xdxw0006DataReqDto();// 请求Data： 联系人信息维护
        Xdxw0006DataRespDto xdxw0006DataRespDto = new Xdxw0006DataRespDto();// 响应Data：联系人信息维护
        Xdxw0006RespDto xdxw0006RespDto = new Xdxw0006RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0006.req.Data reqData = null; // 请求Data：联系人信息维护
        cn.com.yusys.yusp.dto.server.biz.xdxw0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0006.resp.Data();// 响应Data：联系人信息维护
        try {
            // 从 xdxw0006ReqDto获取 reqData
            reqData = xdxw0006ReqDto.getData();
            // 将 reqData 拷贝到xdxw0006DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0006DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006DataReqDto));
            ResultDto<Xdxw0006DataRespDto> xdxw0006DataResultDto = dscmsBizXwClientService.xdxw0006(xdxw0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0006RespDto.setErorcd(Optional.ofNullable(xdxw0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0006RespDto.setErortx(Optional.ofNullable(xdxw0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0006DataResultDto.getCode())) {
                xdxw0006DataRespDto = xdxw0006DataResultDto.getData();
                xdxw0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, e.getMessage());
            xdxw0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0006RespDto.setDatasq(servsq);

        xdxw0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0006.key, DscmsEnum.TRADE_CODE_XDXW0006.value, JSON.toJSONString(xdxw0006RespDto));
        return xdxw0006RespDto;
    }
}
