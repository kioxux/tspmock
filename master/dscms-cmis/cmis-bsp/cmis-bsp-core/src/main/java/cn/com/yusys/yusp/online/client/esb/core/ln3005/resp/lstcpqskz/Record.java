package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpqskz;

/**
 * 响应Service：产品缺省控制对象
 *
 * @author lihh
 * @version 1.0
 */
public class Record {

    private String cpjbiaom;//产品级表名
    private String chanpjzd;//产品级字段
    private String chpjzdms;//产品级字段描述
    private String zhjbiaom;//账户级表名
    private String zhhujizd;//账户级字段
    private String zhhjzdms;//账户级字段描述
    private String zhhuxggz;//账户修改规则
    private String zhhuxdfw;//账户级字段范围值

    public String getCpjbiaom() {
        return cpjbiaom;
    }

    public void setCpjbiaom(String cpjbiaom) {
        this.cpjbiaom = cpjbiaom;
    }

    public String getChanpjzd() {
        return chanpjzd;
    }

    public void setChanpjzd(String chanpjzd) {
        this.chanpjzd = chanpjzd;
    }

    public String getChpjzdms() {
        return chpjzdms;
    }

    public void setChpjzdms(String chpjzdms) {
        this.chpjzdms = chpjzdms;
    }

    public String getZhjbiaom() {
        return zhjbiaom;
    }

    public void setZhjbiaom(String zhjbiaom) {
        this.zhjbiaom = zhjbiaom;
    }

    public String getZhhujizd() {
        return zhhujizd;
    }

    public void setZhhujizd(String zhhujizd) {
        this.zhhujizd = zhhujizd;
    }

    public String getZhhjzdms() {
        return zhhjzdms;
    }

    public void setZhhjzdms(String zhhjzdms) {
        this.zhhjzdms = zhhjzdms;
    }

    public String getZhhuxggz() {
        return zhhuxggz;
    }

    public void setZhhuxggz(String zhhuxggz) {
        this.zhhuxggz = zhhuxggz;
    }

    public String getZhhuxdfw() {
        return zhhuxdfw;
    }

    public void setZhhuxdfw(String zhhuxdfw) {
        this.zhhuxdfw = zhhuxdfw;
    }

    @Override
    public String toString() {
        return "Record{" +
                "cpjbiaom='" + cpjbiaom + '\'' +
                "chanpjzd='" + chanpjzd + '\'' +
                "chpjzdms='" + chpjzdms + '\'' +
                "zhjbiaom='" + zhjbiaom + '\'' +
                "zhhujizd='" + zhhujizd + '\'' +
                "zhhjzdms='" + zhhjzdms + '\'' +
                "zhhuxggz='" + zhhuxggz + '\'' +
                "zhhuxdfw='" + zhhuxdfw + '\'' +
                '}';
    }
}
