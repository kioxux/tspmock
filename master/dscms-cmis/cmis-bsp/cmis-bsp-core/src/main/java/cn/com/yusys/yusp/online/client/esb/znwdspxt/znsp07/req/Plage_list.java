package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req;

import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.plage_list.Record;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/9/9 9:00
 * @since 2021/9/9 9:00
 */
public class Plage_list {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.plage_list.Record> record;


    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Plage_list{" +
                "record=" + record +
                '}';
    }
}
