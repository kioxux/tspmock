package cn.com.yusys.yusp.online.client.esb.core.dp2099.req;

/**
 * 请求Service：保证金账户查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Dp2099ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
