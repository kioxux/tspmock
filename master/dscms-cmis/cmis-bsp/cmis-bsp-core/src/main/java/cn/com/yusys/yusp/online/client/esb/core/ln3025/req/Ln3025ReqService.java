package cn.com.yusys.yusp.online.client.esb.core.ln3025.req;

/**
 * 请求Service：垫款开户
 * @author lihh
 * @version 1.0             
 */      
public class Ln3025ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
