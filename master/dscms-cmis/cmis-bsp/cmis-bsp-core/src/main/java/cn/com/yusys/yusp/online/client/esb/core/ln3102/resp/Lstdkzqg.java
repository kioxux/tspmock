package cn.com.yusys.yusp.online.client.esb.core.ln3102.resp;

import java.util.List;

/**
 * 响应Service：贷款账户期供
 *
 * @author lihh
 * @version 1.0
 */
public class Lstdkzqg {
    java.util.List<LstdkzqgRecord> record;

    public List<LstdkzqgRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LstdkzqgRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkzqg{" +
                "record=" + record +
                '}';
    }
}
