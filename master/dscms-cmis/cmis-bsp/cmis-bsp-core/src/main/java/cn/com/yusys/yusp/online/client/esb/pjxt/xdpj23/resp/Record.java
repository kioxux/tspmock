package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询批次出账票据信息（新信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
public class Record {

    private String pyeeNm;//收款人名称
    private String pyeeAcctId;//收款人账号
    private String payeeBankName;//收款人开户行全称
    private String isseDt;//出票日期
    private String dueDt;//到期日
    private String billno;//票号
    private BigDecimal fBillAmount;//票面金额
    private String drwrNm;//出票人名称
    private String drwrAcctId;//出票人账号
    private String drwrAcctSvcr;//出票人开户行行号
    private String drwrAcctSvcrName;//出票人开户行行名
    private String accptrNm;//承兑行名称
    private String accptrId;//承兑行账号
    private String accptrSvcr;//承兑行行号行号
    private String accptrSvcrName;//承兑行开户行名

    public String getPyeeNm() {
        return pyeeNm;
    }

    public void setPyeeNm(String pyeeNm) {
        this.pyeeNm = pyeeNm;
    }

    public String getPyeeAcctId() {
        return pyeeAcctId;
    }

    public void setPyeeAcctId(String pyeeAcctId) {
        this.pyeeAcctId = pyeeAcctId;
    }

    public String getPayeeBankName() {
        return payeeBankName;
    }

    public void setPayeeBankName(String payeeBankName) {
        this.payeeBankName = payeeBankName;
    }

    public String getIsseDt() {
        return isseDt;
    }

    public void setIsseDt(String isseDt) {
        this.isseDt = isseDt;
    }

    public String getDueDt() {
        return dueDt;
    }

    public void setDueDt(String dueDt) {
        this.dueDt = dueDt;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public BigDecimal getfBillAmount() {
        return fBillAmount;
    }

    public void setfBillAmount(BigDecimal fBillAmount) {
        this.fBillAmount = fBillAmount;
    }

    public String getDrwrNm() {
        return drwrNm;
    }

    public void setDrwrNm(String drwrNm) {
        this.drwrNm = drwrNm;
    }

    public String getDrwrAcctId() {
        return drwrAcctId;
    }

    public void setDrwrAcctId(String drwrAcctId) {
        this.drwrAcctId = drwrAcctId;
    }

    public String getDrwrAcctSvcr() {
        return drwrAcctSvcr;
    }

    public void setDrwrAcctSvcr(String drwrAcctSvcr) {
        this.drwrAcctSvcr = drwrAcctSvcr;
    }

    public String getDrwrAcctSvcrName() {
        return drwrAcctSvcrName;
    }

    public void setDrwrAcctSvcrName(String drwrAcctSvcrName) {
        this.drwrAcctSvcrName = drwrAcctSvcrName;
    }

    public String getAccptrNm() {
        return accptrNm;
    }

    public void setAccptrNm(String accptrNm) {
        this.accptrNm = accptrNm;
    }

    public String getAccptrId() {
        return accptrId;
    }

    public void setAccptrId(String accptrId) {
        this.accptrId = accptrId;
    }

    public String getAccptrSvcr() {
        return accptrSvcr;
    }

    public void setAccptrSvcr(String accptrSvcr) {
        this.accptrSvcr = accptrSvcr;
    }

    public String getAccptrSvcrName() {
        return accptrSvcrName;
    }

    public void setAccptrSvcrName(String accptrSvcrName) {
        this.accptrSvcrName = accptrSvcrName;
    }

    @Override
    public String toString() {
        return "Record{" +
                "pyeeNm='" + pyeeNm + '\'' +
                ", pyeeAcctId='" + pyeeAcctId + '\'' +
                ", payeeBankName='" + payeeBankName + '\'' +
                ", isseDt='" + isseDt + '\'' +
                ", dueDt='" + dueDt + '\'' +
                ", billno='" + billno + '\'' +
                ", fBillAmount=" + fBillAmount +
                ", drwrNm='" + drwrNm + '\'' +
                ", drwrAcctId='" + drwrAcctId + '\'' +
                ", drwrAcctSvcr='" + drwrAcctSvcr + '\'' +
                ", drwrAcctSvcrName='" + drwrAcctSvcrName + '\'' +
                ", accptrNm='" + accptrNm + '\'' +
                ", accptrId='" + accptrId + '\'' +
                ", accptrSvcr='" + accptrSvcr + '\'' +
                ", accptrSvcrName='" + accptrSvcrName + '\'' +
                '}';
    }
}
