package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.req;

/**
 * 请求Service：查询批次出账票据信息（新信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
public class Xdpj23ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
