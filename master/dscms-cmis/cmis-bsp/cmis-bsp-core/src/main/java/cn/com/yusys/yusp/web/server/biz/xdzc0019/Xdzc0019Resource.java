package cn.com.yusys.yusp.web.server.biz.xdzc0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0019.req.Xdzc0019ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0019.resp.Xdzc0019RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0019.req.Xdzc0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0019.resp.Xdzc0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:历史出入池记录查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0019:历史出入池记录查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0019Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0019
     * 交易描述：历史出入池记录查询
     *
     * @param xdzc0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("历史出入池记录查询")
    @PostMapping("/xdzc0019")
    //@Idempotent({"xdzc019", "#xdzc0019ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0019RespDto xdzc0019(@Validated @RequestBody Xdzc0019ReqDto xdzc0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, JSON.toJSONString(xdzc0019ReqDto));
        Xdzc0019DataReqDto xdzc0019DataReqDto = new Xdzc0019DataReqDto();// 请求Data： 历史出入池记录查询
        Xdzc0019DataRespDto xdzc0019DataRespDto = new Xdzc0019DataRespDto();// 响应Data：历史出入池记录查询
        Xdzc0019RespDto xdzc0019RespDto = new Xdzc0019RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0019.req.Data reqData = null; // 请求Data：历史出入池记录查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0019.resp.Data();// 响应Data：历史出入池记录查询

        try {
            // 从 xdzc0019ReqDto获取 reqData
            reqData = xdzc0019ReqDto.getData();
            // 将 reqData 拷贝到xdzc0019DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, JSON.toJSONString(xdzc0019DataReqDto));
            ResultDto<Xdzc0019DataRespDto> xdzc0019DataResultDto = dscmsBizZcClientService.xdzc0019(xdzc0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, JSON.toJSONString(xdzc0019DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0019RespDto.setErorcd(Optional.ofNullable(xdzc0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0019RespDto.setErortx(Optional.ofNullable(xdzc0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0019DataResultDto.getCode())) {
                xdzc0019DataRespDto = xdzc0019DataResultDto.getData();
                xdzc0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, e.getMessage());
            xdzc0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0019RespDto.setDatasq(servsq);

        xdzc0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0019.key, DscmsEnum.TRADE_CODE_XDZC0019.value, JSON.toJSONString(xdzc0019RespDto));
        return xdzc0019RespDto;
    }
}
