package cn.com.yusys.yusp.web.server.biz.xdzc0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0009.req.Xdzc0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0009.resp.Xdzc0009RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0009.req.Xdzc0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0009.resp.Xdzc0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池出票交易接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0009:资产池出票交易接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0009Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0009
     * 交易描述：资产池出票交易接口
     *
     * @param xdzc0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池出票交易接口")
    @PostMapping("/xdzc0009")
    //@Idempotent({"xdzc009", "#xdzc0009ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0009RespDto xdzc0009(@Validated @RequestBody Xdzc0009ReqDto xdzc0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, JSON.toJSONString(xdzc0009ReqDto));
        Xdzc0009DataReqDto xdzc0009DataReqDto = new Xdzc0009DataReqDto();// 请求Data： 资产池出票交易接口
        Xdzc0009DataRespDto xdzc0009DataRespDto = new Xdzc0009DataRespDto();// 响应Data：资产池出票交易接口
        Xdzc0009RespDto xdzc0009RespDto = new Xdzc0009RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdzc0009.req.Data reqData = null; // 请求Data：资产池出票交易接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0009.resp.Data();// 响应Data：资产池出票交易接口

        try {
            // 从 xdzc0009ReqDto获取 reqData
            reqData = xdzc0009ReqDto.getData();
            // 将 reqData 拷贝到xdzc0009DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0009DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, JSON.toJSONString(xdzc0009DataReqDto));
            ResultDto<Xdzc0009DataRespDto> xdzc0009DataResultDto = dscmsBizZcClientService.xdzc0009(xdzc0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, JSON.toJSONString(xdzc0009DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0009RespDto.setErorcd(Optional.ofNullable(xdzc0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0009RespDto.setErortx(Optional.ofNullable(xdzc0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0009DataResultDto.getCode())) {
                xdzc0009DataRespDto = xdzc0009DataResultDto.getData();
                xdzc0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, e.getMessage());
            xdzc0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0009RespDto.setDatasq(servsq);

        xdzc0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0009.key, DscmsEnum.TRADE_CODE_XDZC0009.value, JSON.toJSONString(xdzc0009RespDto));
        return xdzc0009RespDto;
    }
}
