package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp;

import java.math.BigDecimal;

/**
 * 响应Dto：账单交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String tncdno;//交易卡号
    private String tndate;//交易日期
    private String tntime;//交易时间
    private String tncode;//交易码
    private BigDecimal txnamt;//交易金额
    private BigDecimal pstamt;//入账币种金额
    private String ptcrcd;//入账币种
    private String ptdate;//入账日期
    private String authcd;//授权码
    private String tncrcd;//交易币种代码
    private String refnbr;//交易参考号
    private String tnstds;//账单交易描述
    private String aqatid;//受卡方标识码
    private String aqnead;//受理机构名称地址
    private String sttntp;//交易类型
    private String dbcrid;//借贷标志
    private String ordate;//随用金原交易日期
    private BigDecimal oriamt;//随用金原交易金额
    private String cuppay;//银联贷记交易标识
    private String cupsou;//资金来源
    private String cupame;//付款方名称
    private String cupdno;//付款方卡号
    private String cuptno;//付款方账号
    private String cupent;//附言



    public String getTncdno() {
        return tncdno;
    }

    public void setTncdno(String tncdno) {
        this.tncdno = tncdno;
    }

    public String getTndate() {
        return tndate;
    }

    public void setTndate(String tndate) {
        this.tndate = tndate;
    }

    public String getTntime() {
        return tntime;
    }

    public void setTntime(String tntime) {
        this.tntime = tntime;
    }

    public String getTncode() {
        return tncode;
    }

    public void setTncode(String tncode) {
        this.tncode = tncode;
    }

    public BigDecimal getTxnamt() {
        return txnamt;
    }

    public void setTxnamt(BigDecimal txnamt) {
        this.txnamt = txnamt;
    }

    public BigDecimal getPstamt() {
        return pstamt;
    }

    public void setPstamt(BigDecimal pstamt) {
        this.pstamt = pstamt;
    }

    public String getPtcrcd() {
        return ptcrcd;
    }

    public void setPtcrcd(String ptcrcd) {
        this.ptcrcd = ptcrcd;
    }

    public String getPtdate() {
        return ptdate;
    }

    public void setPtdate(String ptdate) {
        this.ptdate = ptdate;
    }

    public String getAuthcd() {
        return authcd;
    }

    public void setAuthcd(String authcd) {
        this.authcd = authcd;
    }

    public String getTncrcd() {
        return tncrcd;
    }

    public void setTncrcd(String tncrcd) {
        this.tncrcd = tncrcd;
    }

    public String getRefnbr() {
        return refnbr;
    }

    public void setRefnbr(String refnbr) {
        this.refnbr = refnbr;
    }

    public String getTnstds() {
        return tnstds;
    }

    public void setTnstds(String tnstds) {
        this.tnstds = tnstds;
    }

    public String getAqatid() {
        return aqatid;
    }

    public void setAqatid(String aqatid) {
        this.aqatid = aqatid;
    }

    public String getAqnead() {
        return aqnead;
    }

    public void setAqnead(String aqnead) {
        this.aqnead = aqnead;
    }

    public String getSttntp() {
        return sttntp;
    }

    public void setSttntp(String sttntp) {
        this.sttntp = sttntp;
    }

    public String getDbcrid() {
        return dbcrid;
    }

    public void setDbcrid(String dbcrid) {
        this.dbcrid = dbcrid;
    }

    public String getOrdate() {
        return ordate;
    }

    public void setOrdate(String ordate) {
        this.ordate = ordate;
    }

    public BigDecimal getOriamt() {
        return oriamt;
    }

    public void setOriamt(BigDecimal oriamt) {
        this.oriamt = oriamt;
    }

    public String getCuppay() {
        return cuppay;
    }

    public void setCuppay(String cuppay) {
        this.cuppay = cuppay;
    }

    public String getCupsou() {
        return cupsou;
    }

    public void setCupsou(String cupsou) {
        this.cupsou = cupsou;
    }

    public String getCupame() {
        return cupame;
    }

    public void setCupame(String cupame) {
        this.cupame = cupame;
    }

    public String getCupdno() {
        return cupdno;
    }

    public void setCupdno(String cupdno) {
        this.cupdno = cupdno;
    }

    public String getCuptno() {
        return cuptno;
    }

    public void setCuptno(String cuptno) {
        this.cuptno = cuptno;
    }

    public String getCupent() {
        return cupent;
    }

    public void setCupent(String cupent) {
        this.cupent = cupent;
    }

    @Override
    public String toString() {
        return "Record{" +
                "tncdno='" + tncdno + '\'' +
                ", tndate='" + tndate + '\'' +
                ", tntime='" + tntime + '\'' +
                ", tncode='" + tncode + '\'' +
                ", txnamt=" + txnamt +
                ", pstamt=" + pstamt +
                ", ptcrcd='" + ptcrcd + '\'' +
                ", ptdate='" + ptdate + '\'' +
                ", authcd='" + authcd + '\'' +
                ", tncrcd='" + tncrcd + '\'' +
                ", refnbr='" + refnbr + '\'' +
                ", tnstds='" + tnstds + '\'' +
                ", aqatid='" + aqatid + '\'' +
                ", aqnead='" + aqnead + '\'' +
                ", sttntp='" + sttntp + '\'' +
                ", dbcrid='" + dbcrid + '\'' +
                ", ordate='" + ordate + '\'' +
                ", oriamt=" + oriamt +
                ", cuppay='" + cuppay + '\'' +
                ", cupsou='" + cupsou + '\'' +
                ", cupame='" + cupame + '\'' +
                ", cupdno='" + cupdno + '\'' +
                ", cuptno='" + cuptno + '\'' +
                ", cupent='" + cupent + '\'' +
                '}';
    }
}
