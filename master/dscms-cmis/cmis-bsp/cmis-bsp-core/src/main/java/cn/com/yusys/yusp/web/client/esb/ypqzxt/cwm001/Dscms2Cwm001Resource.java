package cn.com.yusys.yusp.web.client.esb.ypqzxt.cwm001;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.Cwm001ReqService;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.Record;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.resp.Cwm001RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用押品权证系统的接口
 */
@Api(tags = "BSP封装调用押品权证系统的接口(cwm001)")
@RestController
@RequestMapping("/api/dscms2ypqzxt")
public class Dscms2Cwm001Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Cwm001Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 本异地借阅出库接口（处理码cwm001）
     *
     * @param cwm001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cwm001:本异地借阅出库接口")
    @PostMapping("/cwm001")
    protected @ResponseBody
    ResultDto<Cwm001RespDto> cwm001(@Validated @RequestBody Cwm001ReqDto cwm001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM001.key, EsbEnum.TRADE_CODE_CWM001.value, JSON.toJSONString(cwm001ReqDto));
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.List list = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.List();
        Cwm001ReqService cwm001ReqService = new Cwm001ReqService();
        Cwm001RespService cwm001RespService = new Cwm001RespService();
        Cwm001RespDto cwm001RespDto = new Cwm001RespDto();
        ResultDto<Cwm001RespDto> cwm001ResultDto = new ResultDto<Cwm001RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Cwm001ReqDto转换成reqService
            BeanUtils.copyProperties(cwm001ReqDto, reqService);
            if (CollectionUtils.nonEmpty(cwm001ReqDto.getList())) {
                List<Record> records = new ArrayList<Record>();
                List<cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.ListArrayInfo> listArrayInfos = cwm001ReqDto.getList();
                for (cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.ListArrayInfo listArrayInfo : listArrayInfos) {
                    cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req.Record();
                    BeanUtils.copyProperties(listArrayInfo, record);
                    records.add(record);
                }
                list.setRecord(records);
                reqService.setList(list);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CWM001.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPQZXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPQZXT.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            // TODO 生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+服务器编号(2位)+seq(8位)
            cwm001ReqService.setService(reqService);
            // 将cwm001ReqService转换成cwm001ReqServiceMap
            Map cwm001ReqServiceMap = beanMapUtil.beanToMap(cwm001ReqService);
            context.put("tradeDataMap", cwm001ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM001.key, EsbEnum.TRADE_CODE_CWM001.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CWM001.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM001.key, EsbEnum.TRADE_CODE_CWM001.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cwm001RespService = beanMapUtil.mapToBean(tradeDataMap, Cwm001RespService.class, Cwm001RespService.class);
            respService = cwm001RespService.getService();
            //  将respService转换成Cwm001RespDto
            BeanUtils.copyProperties(respService, cwm001RespDto);
            //  将Cwm001RespDto封装到ResultDto<Cwm001RespDto>
            cwm001ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            cwm001ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, cwm001RespDto);
                cwm001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cwm001ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cwm001ResultDto.setCode(EpbEnum.EPB099999.key);
                cwm001ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM001.key, EsbEnum.TRADE_CODE_CWM001.value, e.getMessage());
            cwm001ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cwm001ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cwm001ResultDto.setData(cwm001RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM001.key, EsbEnum.TRADE_CODE_CWM001.value, JSON.toJSONString(cwm001ResultDto));
        return cwm001ResultDto;
    }
}
