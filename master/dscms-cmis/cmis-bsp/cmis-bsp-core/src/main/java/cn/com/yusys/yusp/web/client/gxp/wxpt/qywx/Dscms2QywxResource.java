package cn.com.yusys.yusp.web.client.gxp.wxpt.qywx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxReqDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxRespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.wxpt.qywx.req.QywxSendDto;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

/**
 * BSP封装调用微信平台的接口
 **/
@Api(tags = "BSP封装调用微信平台的接口处理类")
@RestController
@RequestMapping("/api/dscms2wxpt")
public class Dscms2QywxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2QywxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 推送企业微信
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("qywx:推送企业微信")
    @PostMapping("/qywx")
    protected @ResponseBody
    ResultDto<QywxRespDto> qywx(@Validated @RequestBody QywxReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_QYWX.key, GxpEnum.TRADE_CODE_QYWX.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.wxpt.qywx.req.Hsiss hsiss = new cn.com.yusys.yusp.online.client.gxp.wxpt.qywx.req.Hsiss();
        QywxRespDto qywxRespDto = new QywxRespDto();
        QywxSendDto qywxSendDto = new QywxSendDto();
        ResultDto<QywxRespDto> qywxResultDto = new ResultDto<>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {

            //  将D12011ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, hsiss);
            // 给Hsiss赋值
            LocalDateTime now = LocalDateTime.now();
            hsiss.setTranti(tranDateFormtter.format(now));
            hsiss.setChanno("RLZY");
            hsiss.setLastsndtime(tranDateFormtter.format(now));
            hsiss.setByzd(StringUtils.EMPTY);
            hsiss.setByzd2(StringUtils.EMPTY);
            // 将hsiss转换成hsissMap
            qywxSendDto.setHsiss(hsiss);
            Map hsissMap = beanMapUtil.beanToMap(qywxSendDto);
            context.put("tradeDataMap", hsissMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_QYWX.key, GxpEnum.TRADE_CODE_QYWX.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_QYWX_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_QYWX.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_QYWX.key, GxpEnum.TRADE_CODE_QYWX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");

//            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
            qywxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            qywxResultDto.setMessage(SuccessEnum.SUCCESS.value);
//            } else {
//                qywxResultDto.setCode(EpbEnum.EPB099999.key);
//            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_QYWX.key, GxpEnum.TRADE_CODE_QYWX.value, e.getMessage());
            qywxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            qywxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        qywxResultDto.setData(qywxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_QYWX.key, GxpEnum.TRADE_CODE_QYWX.value, JSON.toJSONString(qywxResultDto));
        return qywxResultDto;
    }
}
