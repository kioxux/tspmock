package cn.com.yusys.yusp.web.client.esb.core.ln3123;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3123.Ln3123ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3123.Ln3123RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3123.Lstdkstzf;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3123.req.Ln3123ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3123.resp.Ln3123RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3123)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3123Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3123Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3123
     * 交易描述：受托支付查询
     *
     * @param reqDto 请求体
     * @return ResultDto<Ln3123RespDto> 封装的返回结果
     * @throws Exception
     */
    @ApiOperation("ln3123:受托支付查询")
    @PostMapping("/ln3123")
    protected @ResponseBody
    ResultDto<Ln3123RespDto> ln3123(@Validated @RequestBody Ln3123ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3123.key, EsbEnum.TRADE_CODE_LN3123.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3123.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3123.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3123.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3123.resp.Service();
        Ln3123ReqService ln3123ReqService = new Ln3123ReqService();
        Ln3123RespService ln3123RespService = new Ln3123RespService();
        Ln3123RespDto ln3123RespDto = new Ln3123RespDto();
        ResultDto<Ln3123RespDto> ln3123ResultDto = new ResultDto<Ln3123RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3123ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3123.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3123ReqService.setService(reqService);

            ln3123ReqService.setService(reqService);
            // 将ln3123ReqService转换成ln3123ReqServiceMap
            Map ln3123ReqServiceMap = beanMapUtil.beanToMap(ln3123ReqService);
            context.put("tradeDataMap", ln3123ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3123.key, EsbEnum.TRADE_CODE_LN3123.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3123.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3123.key, EsbEnum.TRADE_CODE_LN3123.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3123RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3123RespService.class, Ln3123RespService.class);
            respService = ln3123RespService.getService();

            ln3123ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3123ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3123RespDto
                BeanUtils.copyProperties(respService, ln3123RespDto);
                List<Lstdkstzf> lstdkstzfs = new ArrayList<Lstdkstzf>();
                if (respService.getLstdkstzf_ARRAY() != null && CollectionUtils.nonEmpty(respService.getLstdkstzf_ARRAY().getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3123.resp.Record> recordList = respService.getLstdkstzf_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3123.resp.Record record : recordList) {
                        Lstdkstzf lstdkstzf = new Lstdkstzf();
                        BeanUtils.copyProperties(record, lstdkstzf);
                        lstdkstzfs.add(lstdkstzf);
                    }
                }
                ln3123RespDto.setLstdkstzf(lstdkstzfs);
                ln3123ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3123ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3123ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3123ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3123.key, EsbEnum.TRADE_CODE_LN3123.value, e.getMessage());
            ln3123ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3123ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3123ResultDto.setData(ln3123RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3123.key, EsbEnum.TRADE_CODE_LN3123.value, JSON.toJSONString(ln3123ResultDto));
        return ln3123ResultDto;
    }
}
