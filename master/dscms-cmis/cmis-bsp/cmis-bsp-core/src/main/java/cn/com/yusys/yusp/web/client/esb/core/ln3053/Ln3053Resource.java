package cn.com.yusys.yusp.web.client.esb.core.ln3053;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.req.Ln3053ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.req.Lstdkzqg;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.resp.Ln3053RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.resp.Outdkzqg;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3053.req.Ln3053ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.Ln3053RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 接口处理类:还款计划调整
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreln")
public class Ln3053Resource {
    private static final Logger logger = LoggerFactory.getLogger(Ln3053Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3053
     * 交易描述：还款计划调整
     *
     * @param ln3053ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/ln3053")
    protected @ResponseBody
    ResultDto<Ln3053RespDto> ln3053(@Validated @RequestBody Ln3053ReqDto ln3053ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3053.key, EsbEnum.TRADE_CODE_LN3053.value, JSON.toJSONString(ln3053ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3053.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3053.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3053.req.List listR = new cn.com.yusys.yusp.online.client.esb.core.ln3053.req.List();
        Ln3053ReqService ln3053ReqService = new Ln3053ReqService();
        Ln3053RespService ln3053RespService = new Ln3053RespService();
        Ln3053RespDto ln3053RespDto = new Ln3053RespDto();
        ResultDto<Ln3053RespDto> ln3053ResultDto = new ResultDto<Ln3053RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        //  将ln3053ReqDto转换成reqService
        try {
            BeanUtils.copyProperties(ln3053ReqDto, reqService);
            if (CollectionUtils.nonEmpty(ln3053ReqDto.getLstdkzqg())) {
                List<Lstdkzqg> lstdkzqg = ln3053ReqDto.getLstdkzqg();
                List<cn.com.yusys.yusp.online.client.esb.core.ln3053.req.Record> recordList = new ArrayList<>();
                for (Lstdkzqg lstdkzqgdto : lstdkzqg) {
                    cn.com.yusys.yusp.online.client.esb.core.ln3053.req.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3053.req.Record();
                    BeanUtils.copyProperties(lstdkzqgdto, record);
                    recordList.add(record);
                }
                listR.setRecordList(recordList);
                reqService.setList(listR);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3053.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水

            ln3053ReqService.setService(reqService);
            // 将ln3053ReqService转换成ln3053ReqServiceMap
            Map ln3053ReqServiceMap = beanMapUtil.beanToMap(ln3053ReqService);
            context.put("tradeDataMap", ln3053ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3053.key, EsbEnum.TRADE_CODE_LN3053.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3053.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3053.key, EsbEnum.TRADE_CODE_LN3053.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3053RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3053RespService.class, Ln3053RespService.class);
            respService = ln3053RespService.getService();
            //  将respService转换成Ln3053RespDto
            BeanUtils.copyProperties(respService, ln3053RespDto);
            List<Outdkzqg> outdkzqgList = new ArrayList<>();
            cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.List Servicelist = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.List());
            if (CollectionUtils.nonEmpty(Servicelist.getRecordList())) {
                List<cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.Record> respList = respService.getList().getRecordList();
                for (cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.Record r : respList) {
                    Outdkzqg outdkzqg = new Outdkzqg();
                    BeanUtils.copyProperties(r, outdkzqg);
                    outdkzqgList.add(outdkzqg);
                }
            }
            ln3053RespDto.setOutdkzqg(outdkzqgList);
            ln3053ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            ln3053ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3053.key, EsbEnum.TRADE_CODE_LN3053.value, e.getMessage());
            ln3053ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3053ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3053ResultDto.setData(ln3053RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3053.key, EsbEnum.TRADE_CODE_LN3053.value, JSON.toJSONString(ln3053ResultDto));
        return ln3053ResultDto;
    }
}
