package cn.com.yusys.yusp.online.client.esb.comstar.resp;

/**
 * 响应Service：额度同步
 *
 * @author leehuang
 * @version 1.0
 */
public class Com001RespService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Com001RespService{" +
                "service=" + service +
                '}';
    }
}
