package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.req;

/**
 * 请求Service：大额往帐一体化
 *
 * @author chenyong
 * @version 1.0
 */
public class HvpsmrReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
