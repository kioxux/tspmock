package cn.com.yusys.yusp.online.client.esb.rircp.fbxw05.req;


/**
 * 请求Service：惠享贷批复同步接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午5:34:07
 */
public class Fbxw05ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
