package cn.com.yusys.yusp.web.server.biz.xdxw0059;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0059.req.Xdxw0059ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0059.resp.Xdxw0059RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0059.req.Xdxw0059DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0059.resp.Xdxw0059DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据业务唯一编号查询在信贷系统中的抵押率
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0059:根据业务唯一编号查询在信贷系统中的抵押率")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0059Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0059Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0059
     * 交易描述：根据业务唯一编号查询在信贷系统中的抵押率
     *
     * @param xdxw0059ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据业务唯一编号查询在信贷系统中的抵押率")
    @PostMapping("/xdxw0059")
    @Idempotent({"xdcaxw0059", "#xdxw0059ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0059RespDto xdxw0059(@Validated @RequestBody Xdxw0059ReqDto xdxw0059ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, JSON.toJSONString(xdxw0059ReqDto));
        Xdxw0059DataReqDto xdxw0059DataReqDto = new Xdxw0059DataReqDto();// 请求Data： 根据业务唯一编号查询在信贷系统中的抵押率
        Xdxw0059DataRespDto xdxw0059DataRespDto = new Xdxw0059DataRespDto();// 响应Data：根据业务唯一编号查询在信贷系统中的抵押率
        Xdxw0059RespDto xdxw0059RespDto = new Xdxw0059RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdxw0059.req.Data reqData = null; // 请求Data：根据业务唯一编号查询在信贷系统中的抵押率
        cn.com.yusys.yusp.dto.server.biz.xdxw0059.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0059.resp.Data();// 响应Data：根据业务唯一编号查询在信贷系统中的抵押率
        try {
            // 从 xdxw0059ReqDto获取 reqData
            reqData = xdxw0059ReqDto.getData();
            // 将 reqData 拷贝到xdxw0059DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0059DataReqDto);
            ResultDto<Xdxw0059DataRespDto> xdxw0059DataResultDto = dscmsBizXwClientService.xdxw0059(xdxw0059DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdxw0059RespDto.setErorcd(Optional.ofNullable(xdxw0059DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0059RespDto.setErortx(Optional.ofNullable(xdxw0059DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0059DataResultDto.getCode())) {
                xdxw0059RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0059RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0059DataRespDto = xdxw0059DataResultDto.getData();
                // 将 xdxw0059DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0059DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, e.getMessage());
            xdxw0059RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0059RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0059RespDto.setDatasq(servsq);

        xdxw0059RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0059.key, DscmsEnum.TRADE_CODE_XDXW0059.value, JSON.toJSONString(xdxw0059RespDto));
        return xdxw0059RespDto;
    }
}
