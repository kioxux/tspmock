package cn.com.yusys.yusp.web.server.biz.xdxw0082;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0082.req.Xdxw0082ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0082.resp.Xdxw0082RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0082.req.Xdxw0082DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0082.resp.Xdxw0082DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询上一笔抵押贷款的余额接口（增享贷）
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDXW0082:查询上一笔抵押贷款的余额接口（增享贷）")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0082Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0082Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0082
     * 交易描述：查询上一笔抵押贷款的余额接口（增享贷）
     *
     * @param xdxw0082ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询上一笔抵押贷款的余额接口（增享贷）")
    @PostMapping("/xdxw0082")
    @Idempotent({"xdxw0082", "#xdxw0082ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0082RespDto xdxw0082(@Validated @RequestBody Xdxw0082ReqDto xdxw0082ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082ReqDto));
        Xdxw0082DataReqDto xdxw0082DataReqDto = new Xdxw0082DataReqDto();// 请求Data： 勘验任务状态同步
        Xdxw0082DataRespDto xdxw0082DataRespDto = new Xdxw0082DataRespDto();// 响应Data：勘验任务状态同步
		Xdxw0082RespDto xdxw0082RespDto = new Xdxw0082RespDto();
        // 此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0082.req.Data reqData = null; // 请求Data：勘验任务状态同步
		cn.com.yusys.yusp.dto.server.biz.xdxw0082.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0082.resp.Data();// 响应Data：勘验任务状态同步
        // 此处包导入待确定 结束
        try {
            // 从 xdxw0082ReqDto获取 reqData
            reqData = xdxw0082ReqDto.getData();
            // 将 reqData 拷贝到xdxw0082DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0082DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataReqDto));
            ResultDto<Xdxw0082DataRespDto> xdxw0082DataResultDto = dscmsBizXwClientService.xdxw0082(xdxw0082DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0082RespDto.setErorcd(Optional.ofNullable(xdxw0082DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0082RespDto.setErortx(Optional.ofNullable(xdxw0082DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0082DataResultDto.getCode())) {
                xdxw0082DataRespDto = xdxw0082DataResultDto.getData();
                xdxw0082RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0082RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0082DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0082DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, e.getMessage());
            xdxw0082RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0082RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0082RespDto.setDatasq(servsq);

        xdxw0082RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0082.value, JSON.toJSONString(xdxw0082RespDto));
        return xdxw0082RespDto;
    }
}
