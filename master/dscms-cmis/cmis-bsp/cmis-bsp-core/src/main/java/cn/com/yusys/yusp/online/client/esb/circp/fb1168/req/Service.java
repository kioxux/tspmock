package cn.com.yusys.yusp.online.client.esb.circp.fb1168.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Service：抵押查封结果推送
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    private String dy_no;//抵押品编号
    private String bdc_no;//不动产证号
    private String jk_cus_name;//借款人名称
    private String yp_manager_id;//押品所有人名称
    private String yp_book_serno;//押品权证编号
    private String cx_cf_result;//查询查封结果
    private String cx_cf_time;//查询查封时间
    private String pre_app_no;//预授信流水号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    public String getDy_no() {
        return dy_no;
    }

    public void setDy_no(String dy_no) {
        this.dy_no = dy_no;
    }

    public String getBdc_no() {
        return bdc_no;
    }

    public void setBdc_no(String bdc_no) {
        this.bdc_no = bdc_no;
    }

    public String getJk_cus_name() {
        return jk_cus_name;
    }

    public void setJk_cus_name(String jk_cus_name) {
        this.jk_cus_name = jk_cus_name;
    }

    public String getYp_manager_id() {
        return yp_manager_id;
    }

    public void setYp_manager_id(String yp_manager_id) {
        this.yp_manager_id = yp_manager_id;
    }

    public String getYp_book_serno() {
        return yp_book_serno;
    }

    public void setYp_book_serno(String yp_book_serno) {
        this.yp_book_serno = yp_book_serno;
    }

    public String getCx_cf_result() {
        return cx_cf_result;
    }

    public void setCx_cf_result(String cx_cf_result) {
        this.cx_cf_result = cx_cf_result;
    }

    public String getCx_cf_time() {
        return cx_cf_time;
    }

    public void setCx_cf_time(String cx_cf_time) {
        this.cx_cf_time = cx_cf_time;
    }

    public String getPre_app_no() {
        return pre_app_no;
    }

    public void setPre_app_no(String pre_app_no) {
        this.pre_app_no = pre_app_no;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", dy_no='" + dy_no + '\'' +
                ", bdc_no='" + bdc_no + '\'' +
                ", jk_cus_name='" + jk_cus_name + '\'' +
                ", yp_manager_id='" + yp_manager_id + '\'' +
                ", yp_book_serno='" + yp_book_serno + '\'' +
                ", cx_cf_result='" + cx_cf_result + '\'' +
                ", cx_cf_time='" + cx_cf_time + '\'' +
                ", pre_app_no='" + pre_app_no + '\'' +
                '}';
    }
}
