package cn.com.yusys.yusp.web.server.biz.xdxw0080;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0080.resp.Xdxw0080RespDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0080.req.Xdxw0080ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0080.resp.Xdxw0080RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xdxw0080.req.Xdxw0080DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0080.resp.Xdxw0080DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优惠利率申请结果通知
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0080:优惠利率申请结果通知")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0080Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0080Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0080
     * 交易描述：优惠利率申请结果通知
     *
     * @param xdxw0080ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优惠利率申请结果通知")
    @PostMapping("/xdxw0080")
    @Idempotent({"xdxw0080", "#xdxw0080ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0080RespDto xdxw0080(@Validated @RequestBody Xdxw0080ReqDto xdxw0080ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080ReqDto));
        Xdxw0080DataReqDto xdxw0080DataReqDto = new Xdxw0080DataReqDto();// 请求Data： 优惠利率申请结果通知
        Xdxw0080DataRespDto xdxw0080DataRespDto = new Xdxw0080DataRespDto();// 响应Data：优惠利率申请结果通知
		Xdxw0080RespDto xdxw0080RespDto = new Xdxw0080RespDto();
        //  此处包导入待确定 开始
		//  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0080.req.Data reqData = null; // 请求Data：
		cn.com.yusys.yusp.dto.server.biz.xdxw0080.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0080.resp.Data();// 响应Data：
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0080ReqDto获取 reqData
            reqData = xdxw0080ReqDto.getData();
            // 将 reqData 拷贝到xdxw0080DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0080DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080DataReqDto));
            ResultDto<Xdxw0080DataRespDto> xdxw0080DataResultDto = dscmsBizXwClientService.xdxw0080(xdxw0080DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0080RespDto.setErorcd(Optional.ofNullable(xdxw0080DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0080RespDto.setErortx(Optional.ofNullable(xdxw0080DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0080DataResultDto.getCode())) {
                xdxw0080DataRespDto = xdxw0080DataResultDto.getData();
                xdxw0080RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0080RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0080DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0080DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, e.getMessage());
            xdxw0080RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0080RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0080RespDto.setDatasq(servsq);

        xdxw0080RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0080.key, DscmsEnum.TRADE_CODE_XDXW0080.value, JSON.toJSONString(xdxw0080RespDto));
        return xdxw0080RespDto;
    }
}
