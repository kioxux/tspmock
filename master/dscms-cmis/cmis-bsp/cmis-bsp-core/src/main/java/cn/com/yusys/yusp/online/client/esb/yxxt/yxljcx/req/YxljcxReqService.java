package cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.req;

/**
 * 请求Service：影像图像路径查询
 *
 * @author chenyong
 * @version 1.0
 */
public class YxljcxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "YxljcxReqService{" +
                "service=" + service +
                '}';
    }
}
