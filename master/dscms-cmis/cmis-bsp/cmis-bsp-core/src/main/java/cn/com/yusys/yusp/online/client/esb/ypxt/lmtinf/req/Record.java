package cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req;

import java.math.BigDecimal;

/**
 * 请求Service：信贷授信协议信息同步
 */
public class Record {
    private String sxxybh;//授信协议号
    private String sxkhbh;//授信客户码
    private BigDecimal sxjeyp;//授信金额
    private String sxbzyp;//授信币种
    private String sxqsrq;//授信起始日期
    private String sxdqrq;//授信到期日期
    private String sxxyzt;//授信协议状态
    private String khghjl;//管户客户经理
    private String khghjg;//管理机构

    public String getSxxybh() {
        return sxxybh;
    }

    public void setSxxybh(String sxxybh) {
        this.sxxybh = sxxybh;
    }

    public String getSxkhbh() {
        return sxkhbh;
    }

    public void setSxkhbh(String sxkhbh) {
        this.sxkhbh = sxkhbh;
    }

    public BigDecimal getSxjeyp() {
        return sxjeyp;
    }

    public void setSxjeyp(BigDecimal sxjeyp) {
        this.sxjeyp = sxjeyp;
    }

    public String getSxbzyp() {
        return sxbzyp;
    }

    public void setSxbzyp(String sxbzyp) {
        this.sxbzyp = sxbzyp;
    }

    public String getSxqsrq() {
        return sxqsrq;
    }

    public void setSxqsrq(String sxqsrq) {
        this.sxqsrq = sxqsrq;
    }

    public String getSxdqrq() {
        return sxdqrq;
    }

    public void setSxdqrq(String sxdqrq) {
        this.sxdqrq = sxdqrq;
    }

    public String getSxxyzt() {
        return sxxyzt;
    }

    public void setSxxyzt(String sxxyzt) {
        this.sxxyzt = sxxyzt;
    }

    public String getKhghjl() {
        return khghjl;
    }

    public void setKhghjl(String khghjl) {
        this.khghjl = khghjl;
    }

    public String getKhghjg() {
        return khghjg;
    }

    public void setKhghjg(String khghjg) {
        this.khghjg = khghjg;
    }

    @Override
    public String toString() {
        return "Record{" +
                "sxxybh='" + sxxybh + '\'' +
                ", sxkhbh='" + sxkhbh + '\'' +
                ", sxjeyp=" + sxjeyp +
                ", sxbzyp='" + sxbzyp + '\'' +
                ", sxqsrq='" + sxqsrq + '\'' +
                ", sxdqrq='" + sxdqrq + '\'' +
                ", sxxyzt='" + sxxyzt + '\'' +
                ", khghjl='" + khghjl + '\'' +
                ", khghjg='" + khghjg + '\'' +
                '}';
    }
}
