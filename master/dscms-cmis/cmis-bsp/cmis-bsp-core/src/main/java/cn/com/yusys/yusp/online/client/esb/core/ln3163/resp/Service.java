package cn.com.yusys.yusp.online.client.esb.core.ln3163.resp;

import java.math.BigDecimal;

/**
 * 响应Service：资产证券化处理
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String xieybhao;//协议编号
    private String xieyimch;//协议名称
    private Integer zongbish;//总笔数
    private BigDecimal benjheji;//本金合计
    private BigDecimal lixiheji;//利息合计
    private String bjyskzhh;//其他应收款账号(本金)
    private String qyskzhzh;//其他应收款账号子序号(本金)
    private BigDecimal benjinje;//本金金额
    private String lxyskzhh;//其他应收款账号(利息)
    private String lxyskzxh;//其他应收款账号子序号(利息)
    private BigDecimal lixijine;//利息金额
    private String bjysfzhh;//其他应付款账号(本金)
    private String bjysfzxh;//其他应付款账号子序号(本金)
    private BigDecimal yinghkbj;//应还本金
    private String lxysfzhh;//其他应付款账号(利息)
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    private BigDecimal yinghklx;//应还利息
    private String zchzhtai;//资产处理状态
    private String jiaoyirq;//交易日期
    private String jiaoyigy;//交易柜员
    private String yngyjigo;//营业机构
    private String jiaoyils;//交易流水
    private cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.List list;//借据列表[LIST]

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public BigDecimal getBenjheji() {
        return benjheji;
    }

    public void setBenjheji(BigDecimal benjheji) {
        this.benjheji = benjheji;
    }

    public BigDecimal getLixiheji() {
        return lixiheji;
    }

    public void setLixiheji(BigDecimal lixiheji) {
        this.lixiheji = lixiheji;
    }

    public String getBjyskzhh() {
        return bjyskzhh;
    }

    public void setBjyskzhh(String bjyskzhh) {
        this.bjyskzhh = bjyskzhh;
    }

    public String getQyskzhzh() {
        return qyskzhzh;
    }

    public void setQyskzhzh(String qyskzhzh) {
        this.qyskzhzh = qyskzhzh;
    }

    public BigDecimal getBenjinje() {
        return benjinje;
    }

    public void setBenjinje(BigDecimal benjinje) {
        this.benjinje = benjinje;
    }

    public String getLxyskzhh() {
        return lxyskzhh;
    }

    public void setLxyskzhh(String lxyskzhh) {
        this.lxyskzhh = lxyskzhh;
    }

    public String getLxyskzxh() {
        return lxyskzxh;
    }

    public void setLxyskzxh(String lxyskzxh) {
        this.lxyskzxh = lxyskzxh;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public String getBjysfzhh() {
        return bjysfzhh;
    }

    public void setBjysfzhh(String bjysfzhh) {
        this.bjysfzhh = bjysfzhh;
    }

    public String getBjysfzxh() {
        return bjysfzxh;
    }

    public void setBjysfzxh(String bjysfzxh) {
        this.bjysfzxh = bjysfzxh;
    }

    public BigDecimal getYinghkbj() {
        return yinghkbj;
    }

    public void setYinghkbj(BigDecimal yinghkbj) {
        this.yinghkbj = yinghkbj;
    }

    public String getLxysfzhh() {
        return lxysfzhh;
    }

    public void setLxysfzhh(String lxysfzhh) {
        this.lxysfzhh = lxysfzhh;
    }

    public String getQtyfzhzx() {
        return qtyfzhzx;
    }

    public void setQtyfzhzx(String qtyfzhzx) {
        this.qtyfzhzx = qtyfzhzx;
    }

    public BigDecimal getYinghklx() {
        return yinghklx;
    }

    public void setYinghklx(BigDecimal yinghklx) {
        this.yinghklx = yinghklx;
    }

    public String getZchzhtai() {
        return zchzhtai;
    }

    public void setZchzhtai(String zchzhtai) {
        this.zchzhtai = zchzhtai;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", zongbish=" + zongbish +
                ", benjheji=" + benjheji +
                ", lixiheji=" + lixiheji +
                ", bjyskzhh='" + bjyskzhh + '\'' +
                ", qyskzhzh='" + qyskzhzh + '\'' +
                ", benjinje=" + benjinje +
                ", lxyskzhh='" + lxyskzhh + '\'' +
                ", lxyskzxh='" + lxyskzxh + '\'' +
                ", lixijine=" + lixijine +
                ", bjysfzhh='" + bjysfzhh + '\'' +
                ", bjysfzxh='" + bjysfzxh + '\'' +
                ", yinghkbj=" + yinghkbj +
                ", lxysfzhh='" + lxysfzhh + '\'' +
                ", qtyfzhzx='" + qtyfzhzx + '\'' +
                ", yinghklx=" + yinghklx +
                ", zchzhtai='" + zchzhtai + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", list=" + list +
                '}';
    }
}
