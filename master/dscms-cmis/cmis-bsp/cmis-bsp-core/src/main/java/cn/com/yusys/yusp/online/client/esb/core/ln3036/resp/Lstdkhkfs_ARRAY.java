package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkfs.Record;

import java.util.List;

/**
 * 请求Dto：贷款组合还款方式
 *
 * @author code-generator
 * @version 1.0
 */

public class Lstdkhkfs_ARRAY {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkfs.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhkfs_ARRAY{" +
                "record=" + record +
                '}';
    }
}
