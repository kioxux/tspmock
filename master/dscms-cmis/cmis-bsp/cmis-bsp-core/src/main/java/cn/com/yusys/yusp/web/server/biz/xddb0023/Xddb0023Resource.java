package cn.com.yusys.yusp.web.server.biz.xddb0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0023.req.Xddb0023ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0023.resp.Xddb0023RespDto;
import cn.com.yusys.yusp.dto.server.xddb0023.req.Xddb0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0023.resp.Xddb0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 响应Dto：更新抵质押品状态
 *
 * @author zdl
 * @version 1.0
 */
@Api(tags = "XDDB0023:更新抵质押品状态")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0023Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0023
     * 交易描述：更新抵质押品状态
     *
     * @param xddb0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品共有人信息查询")
    @PostMapping("/xddb0023")
    //@Idempotent({"xddb0023", "#xddb0023ReqDto.datasq"})
    protected @ResponseBody
    Xddb0023RespDto xddb0023(@Validated @RequestBody Xddb0023ReqDto xddb0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023ReqDto));
        Xddb0023DataReqDto xddb0023DataReqDto = new Xddb0023DataReqDto();// 请求Data： 更新抵质押品状态
        Xddb0023DataRespDto xddb0023DataRespDto = new Xddb0023DataRespDto();// 响应Data：更新抵质押品状态
        Xddb0023RespDto xddb0023RespDto = new Xddb0023RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddb0023.req.Data reqData = null; // 请求Data：更新抵质押品状态
        cn.com.yusys.yusp.dto.server.biz.xddb0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0023.resp.Data();// 响应Data：更新抵质押品状态
        try {
            // 从 xddb0023ReqDto获取 reqData
            reqData = xddb0023ReqDto.getData();
            // 将 reqData 拷贝到xddb0023DataReqDto
            BeanUtils.copyProperties(reqData, xddb0023DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023DataReqDto));
            ResultDto<Xddb0023DataRespDto> xddb0023DataResultDto = dscmsBizDbClientService.xddb0023(xddb0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0023RespDto.setErorcd(Optional.ofNullable(xddb0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0023RespDto.setErortx(Optional.ofNullable(xddb0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0023DataResultDto.getCode())) {
                xddb0023DataRespDto = xddb0023DataResultDto.getData();
                xddb0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                //xddb0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xddb0023RespDto.setErortx("Xddb0023Service逻辑设计中");
                // 将 xddb0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0023DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, e.getMessage());
            xddb0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0023RespDto.setDatasq(servsq);

        xddb0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0023.key, DscmsEnum.TRADE_CODE_XDDB0023.value, JSON.toJSONString(xddb0023RespDto));
        return xddb0023RespDto;
    }
}
