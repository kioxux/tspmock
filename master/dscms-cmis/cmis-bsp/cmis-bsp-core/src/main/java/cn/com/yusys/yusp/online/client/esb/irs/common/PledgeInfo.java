package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:质押物信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:10
 */
public class PledgeInfo {
    private List<PledgeInfoRecord> record; // 质押物信息

    public List<PledgeInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<PledgeInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "PledgeInfo{" +
                "record=" + record +
                '}';
    }
}
