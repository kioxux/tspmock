package cn.com.yusys.yusp.online.client.esb.core.ln3245.req;

/**
 * 请求Service：核销登记簿查询
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3245ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
