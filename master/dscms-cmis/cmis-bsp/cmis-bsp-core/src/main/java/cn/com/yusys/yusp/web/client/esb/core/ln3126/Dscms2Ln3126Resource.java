package cn.com.yusys.yusp.web.client.esb.core.ln3126;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3126.req.Ln3126ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp.Ln3126RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3126.req.Ln3126ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3126.resp.Listdkfycx;
import cn.com.yusys.yusp.online.client.esb.core.ln3126.resp.Ln3126RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3126.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3126)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3126Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3126Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3126
     * 交易描述：贷款费用交易明细查询
     *
     * @param ln3126ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3126:贷款费用交易明细查询")
    @PostMapping("/ln3126")
    protected @ResponseBody
    ResultDto<Ln3126RespDto> ln3126(@Validated @RequestBody Ln3126ReqDto ln3126ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3126.key, EsbEnum.TRADE_CODE_LN3126.value, JSON.toJSONString(ln3126ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3126.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3126.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3126.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3126.resp.Service();
        Ln3126ReqService ln3126ReqService = new Ln3126ReqService();
        Ln3126RespService ln3126RespService = new Ln3126RespService();
        Ln3126RespDto ln3126RespDto = new Ln3126RespDto();
        ResultDto<Ln3126RespDto> ln3126ResultDto = new ResultDto<Ln3126RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3126ReqDto转换成reqService
            BeanUtils.copyProperties(ln3126ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3126.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3126ReqService.setService(reqService);
            // 将ln3126ReqService转换成ln3126ReqServiceMap
            Map ln3126ReqServiceMap = beanMapUtil.beanToMap(ln3126ReqService);
            context.put("tradeDataMap", ln3126ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3126.key, EsbEnum.TRADE_CODE_LN3126.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3126.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3126.key, EsbEnum.TRADE_CODE_LN3126.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3126RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3126RespService.class, Ln3126RespService.class);
            respService = ln3126RespService.getService();

            ln3126ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3126ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3126RespDto
                BeanUtils.copyProperties(respService, ln3126RespDto);
                Listdkfycx listdkfycx = Optional.ofNullable(respService.getListdkfycx()).orElse(new Listdkfycx());
                if (CollectionUtils.nonEmpty(listdkfycx.getRecord())) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp.Listdkfycx> targetList = new ArrayList<>();
                    List<Record> recordList = respService.getListdkfycx().getRecord();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp.Listdkfycx target = new cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp.Listdkfycx();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    ln3126RespDto.setListdkfycx(targetList);
                }
                ln3126ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3126ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3126ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3126ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3126.key, EsbEnum.TRADE_CODE_LN3126.value, e.getMessage());
            ln3126ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3126ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3126ResultDto.setData(ln3126RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3126.key, EsbEnum.TRADE_CODE_LN3126.value, JSON.toJSONString(ln3126ResultDto));
        return ln3126ResultDto;
    }
}
