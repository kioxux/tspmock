package cn.com.yusys.yusp.online.client.esb.core.da3306.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵债资产拨备计提
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private BigDecimal jitijine;//计提金额
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String jiaoyigy;//交易柜员

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getJitijine() {
        return jitijine;
    }

    public void setJitijine(BigDecimal jitijine) {
        this.jitijine = jitijine;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", jitijine=" + jitijine +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                '}';
    }
}
