package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req;

/**
 * 请求Dto：账单交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Body {
    private String cardno;//卡号
    private String crcycd;//币种
    private String sttntp;//交易类型
    private String stmtdt;//账单年月
    private String frtrow;//开始位置
    private String lstrow;//结束位置

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getSttntp() {
        return sttntp;
    }

    public void setSttntp(String sttntp) {
        this.sttntp = sttntp;
    }

    public String getStmtdt() {
        return stmtdt;
    }

    public void setStmtdt(String stmtdt) {
        this.stmtdt = stmtdt;
    }

    public String getFrtrow() {
        return frtrow;
    }

    public void setFrtrow(String frtrow) {
        this.frtrow = frtrow;
    }

    public String getLstrow() {
        return lstrow;
    }

    public void setLstrow(String lstrow) {
        this.lstrow = lstrow;
    }

    @Override
    public String toString() {
        return "D12011ReqDto{" +
                "cardno='" + cardno + '\'' +
                "crcycd='" + crcycd + '\'' +
                "sttntp='" + sttntp + '\'' +
                "stmtdt='" + stmtdt + '\'' +
                "frtrow='" + frtrow + '\'' +
                "lstrow='" + lstrow + '\'' +
                '}';
    }
}  
