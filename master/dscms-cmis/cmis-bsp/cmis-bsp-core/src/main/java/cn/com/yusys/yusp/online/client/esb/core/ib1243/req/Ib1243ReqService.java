package cn.com.yusys.yusp.online.client.esb.core.ib1243.req;

/**
 * 请求Service：通用电子记帐交易（多借多贷-多币种）
 * @author lihh
 * @version 1.0             
 */      
public class Ib1243ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
