package cn.com.yusys.yusp.online.client.esb.core.da3301.req.listnm0;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产入账
 */
public class Record {
    private String fygzzhao;//费用挂账账号
    private String zhanghmc;//账户户名
    private BigDecimal feiyfase;//费用发生额
    private String beizhuxx;//备注
    private String fygzzzxh;//费用挂账账号子序号

	public String getFygzzhao() {
		return fygzzhao;
	}

	public void setFygzzhao(String fygzzhao) {
		this.fygzzhao = fygzzhao;
	}

	public String getZhanghmc() {
		return zhanghmc;
	}

	public void setZhanghmc(String zhanghmc) {
		this.zhanghmc = zhanghmc;
	}

	public BigDecimal getFeiyfase() {
		return feiyfase;
	}

	public void setFeiyfase(BigDecimal feiyfase) {
		this.feiyfase = feiyfase;
	}

	public String getBeizhuxx() {
		return beizhuxx;
	}

	public void setBeizhuxx(String beizhuxx) {
		this.beizhuxx = beizhuxx;
	}

	public String getFygzzzxh() {
		return fygzzzxh;
	}

	public void setFygzzzxh(String fygzzzxh) {
		this.fygzzzxh = fygzzzxh;
	}

	@Override
	public String toString() {
		return "Record{" +
				"fygzzhao='" + fygzzhao + '\'' +
				", zhanghmc='" + zhanghmc + '\'' +
				", feiyfase=" + feiyfase +
				", beizhuxx='" + beizhuxx + '\'' +
				", fygzzzxh='" + fygzzzxh + '\'' +
				'}';
	}
}
