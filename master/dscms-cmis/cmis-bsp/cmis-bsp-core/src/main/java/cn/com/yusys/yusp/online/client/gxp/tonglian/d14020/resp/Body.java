package cn.com.yusys.yusp.online.client.gxp.tonglian.d14020.resp;

import java.math.BigDecimal;

/**
 * 响应Dto：卡片信息查询
 *
 * @author lihh
 * @version 1.0
 */
public class Body {
    private String cardno;//卡号
    private String cdhdnm;//姓名
    private String prctnm;//卡产品名称
    private String bcspin;//主附卡指示
    private String bccdno;//主卡卡号
    private String ownbch;//发卡网点
    private String stdate;//创建日期
    private String bkcode;//锁定码
    private String actvin;//是否已激活
    private String actvdt;//激活日期
    private String cancdt;//销卡销户日期
    private String pspnvr;//是否消费凭密
    private String rltnsp;//与主卡持卡人关系
    private String cdepdt;//卡片有效日期
    private String ntcddt;//下个年费收取日期
    private String rnewin;//续卡标识
    private String rnewcd;//续卡拒绝原因码
    private String nwcdis;//是否新发卡
    private String qpnext;//是否存在查询密码
    private String ppnext;//是否存在交易密码
    private Integer pintrs;//交易密码错误次数
    private Integer ipntrs;//查询密码错误次数
    private String ltptrs;//上次密码错时间
    private String ltiqpt;//上次查询密码错误时间
    private String ipnlmt;//查询密码是否超过错误次数
    private String plmtin;//交易密码是否超过错误次数
    private Integer cvv2_tries;//CVV2 错误次数
    private String cvv2_limiterr_ind;//CVV2 是否锁定
    private String cvv_tries;//CVV 错误次数
    private String cvv_limiterr_ind;//CVV 是否锁定
    private Integer icvv_tries;//ICVV 错误次数
    private String icvv_limiterr_ind;//ICVV 是否锁定
    private String cancel_reason;//销卡原因
    private Integer fee_reduce_cnt;//年费减免累计笔数
    private BigDecimal fee_reduce_amt;//年费减免累计金额
    private BigDecimal fee_reduce_max_amt;//年费减免单笔最大金额
    private Integer exp_tries;//有效期错误次数
    private String exp_limiterr_ind;//有效期是否锁定
    private String source;//申请来源
    private Integer cup_acct_info_verify_tries;//账户信息验证失败次数
    private String cup_acct_info_verify_limiter_ind;//账户信息验证是否锁定
    private String electronic_ind;//是否电子信用卡
    private String first_activate_date;//首次激活日期
    private String self_apply_ind;//自助申请标识
    private String first_card_no;//首发卡号

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCdhdnm() {
        return cdhdnm;
    }

    public void setCdhdnm(String cdhdnm) {
        this.cdhdnm = cdhdnm;
    }

    public String getPrctnm() {
        return prctnm;
    }

    public void setPrctnm(String prctnm) {
        this.prctnm = prctnm;
    }

    public String getBcspin() {
        return bcspin;
    }

    public void setBcspin(String bcspin) {
        this.bcspin = bcspin;
    }

    public String getBccdno() {
        return bccdno;
    }

    public void setBccdno(String bccdno) {
        this.bccdno = bccdno;
    }

    public String getOwnbch() {
        return ownbch;
    }

    public void setOwnbch(String ownbch) {
        this.ownbch = ownbch;
    }

    public String getStdate() {
        return stdate;
    }

    public void setStdate(String stdate) {
        this.stdate = stdate;
    }

    public String getBkcode() {
        return bkcode;
    }

    public void setBkcode(String bkcode) {
        this.bkcode = bkcode;
    }

    public String getActvin() {
        return actvin;
    }

    public void setActvin(String actvin) {
        this.actvin = actvin;
    }

    public String getActvdt() {
        return actvdt;
    }

    public void setActvdt(String actvdt) {
        this.actvdt = actvdt;
    }

    public String getCancdt() {
        return cancdt;
    }

    public void setCancdt(String cancdt) {
        this.cancdt = cancdt;
    }

    public String getPspnvr() {
        return pspnvr;
    }

    public void setPspnvr(String pspnvr) {
        this.pspnvr = pspnvr;
    }

    public String getRltnsp() {
        return rltnsp;
    }

    public void setRltnsp(String rltnsp) {
        this.rltnsp = rltnsp;
    }

    public String getCdepdt() {
        return cdepdt;
    }

    public void setCdepdt(String cdepdt) {
        this.cdepdt = cdepdt;
    }

    public String getNtcddt() {
        return ntcddt;
    }

    public void setNtcddt(String ntcddt) {
        this.ntcddt = ntcddt;
    }

    public String getRnewin() {
        return rnewin;
    }

    public void setRnewin(String rnewin) {
        this.rnewin = rnewin;
    }

    public String getRnewcd() {
        return rnewcd;
    }

    public void setRnewcd(String rnewcd) {
        this.rnewcd = rnewcd;
    }

    public String getNwcdis() {
        return nwcdis;
    }

    public void setNwcdis(String nwcdis) {
        this.nwcdis = nwcdis;
    }

    public String getQpnext() {
        return qpnext;
    }

    public void setQpnext(String qpnext) {
        this.qpnext = qpnext;
    }

    public String getPpnext() {
        return ppnext;
    }

    public void setPpnext(String ppnext) {
        this.ppnext = ppnext;
    }

    public Integer getPintrs() {
        return pintrs;
    }

    public void setPintrs(Integer pintrs) {
        this.pintrs = pintrs;
    }

    public Integer getIpntrs() {
        return ipntrs;
    }

    public void setIpntrs(Integer ipntrs) {
        this.ipntrs = ipntrs;
    }

    public String getLtptrs() {
        return ltptrs;
    }

    public void setLtptrs(String ltptrs) {
        this.ltptrs = ltptrs;
    }

    public String getLtiqpt() {
        return ltiqpt;
    }

    public void setLtiqpt(String ltiqpt) {
        this.ltiqpt = ltiqpt;
    }

    public String getIpnlmt() {
        return ipnlmt;
    }

    public void setIpnlmt(String ipnlmt) {
        this.ipnlmt = ipnlmt;
    }

    public String getPlmtin() {
        return plmtin;
    }

    public void setPlmtin(String plmtin) {
        this.plmtin = plmtin;
    }

    public Integer getCvv2_tries() {
        return cvv2_tries;
    }

    public void setCvv2_tries(Integer cvv2_tries) {
        this.cvv2_tries = cvv2_tries;
    }

    public String getCvv2_limiterr_ind() {
        return cvv2_limiterr_ind;
    }

    public void setCvv2_limiterr_ind(String cvv2_limiterr_ind) {
        this.cvv2_limiterr_ind = cvv2_limiterr_ind;
    }

    public String getCvv_tries() {
        return cvv_tries;
    }

    public void setCvv_tries(String cvv_tries) {
        this.cvv_tries = cvv_tries;
    }

    public String getCvv_limiterr_ind() {
        return cvv_limiterr_ind;
    }

    public void setCvv_limiterr_ind(String cvv_limiterr_ind) {
        this.cvv_limiterr_ind = cvv_limiterr_ind;
    }

    public Integer getIcvv_tries() {
        return icvv_tries;
    }

    public void setIcvv_tries(Integer icvv_tries) {
        this.icvv_tries = icvv_tries;
    }

    public String getIcvv_limiterr_ind() {
        return icvv_limiterr_ind;
    }

    public void setIcvv_limiterr_ind(String icvv_limiterr_ind) {
        this.icvv_limiterr_ind = icvv_limiterr_ind;
    }

    public String getCancel_reason() {
        return cancel_reason;
    }

    public void setCancel_reason(String cancel_reason) {
        this.cancel_reason = cancel_reason;
    }

    public Integer getFee_reduce_cnt() {
        return fee_reduce_cnt;
    }

    public void setFee_reduce_cnt(Integer fee_reduce_cnt) {
        this.fee_reduce_cnt = fee_reduce_cnt;
    }

    public BigDecimal getFee_reduce_amt() {
        return fee_reduce_amt;
    }

    public void setFee_reduce_amt(BigDecimal fee_reduce_amt) {
        this.fee_reduce_amt = fee_reduce_amt;
    }

    public BigDecimal getFee_reduce_max_amt() {
        return fee_reduce_max_amt;
    }

    public void setFee_reduce_max_amt(BigDecimal fee_reduce_max_amt) {
        this.fee_reduce_max_amt = fee_reduce_max_amt;
    }

    public Integer getExp_tries() {
        return exp_tries;
    }

    public void setExp_tries(Integer exp_tries) {
        this.exp_tries = exp_tries;
    }

    public String getExp_limiterr_ind() {
        return exp_limiterr_ind;
    }

    public void setExp_limiterr_ind(String exp_limiterr_ind) {
        this.exp_limiterr_ind = exp_limiterr_ind;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Integer getCup_acct_info_verify_tries() {
        return cup_acct_info_verify_tries;
    }

    public void setCup_acct_info_verify_tries(Integer cup_acct_info_verify_tries) {
        this.cup_acct_info_verify_tries = cup_acct_info_verify_tries;
    }

    public String getCup_acct_info_verify_limiter_ind() {
        return cup_acct_info_verify_limiter_ind;
    }

    public void setCup_acct_info_verify_limiter_ind(String cup_acct_info_verify_limiter_ind) {
        this.cup_acct_info_verify_limiter_ind = cup_acct_info_verify_limiter_ind;
    }

    public String getElectronic_ind() {
        return electronic_ind;
    }

    public void setElectronic_ind(String electronic_ind) {
        this.electronic_ind = electronic_ind;
    }

    public String getFirst_activate_date() {
        return first_activate_date;
    }

    public void setFirst_activate_date(String first_activate_date) {
        this.first_activate_date = first_activate_date;
    }

    public String getSelf_apply_ind() {
        return self_apply_ind;
    }

    public void setSelf_apply_ind(String self_apply_ind) {
        this.self_apply_ind = self_apply_ind;
    }

    public String getFirst_card_no() {
        return first_card_no;
    }

    public void setFirst_card_no(String first_card_no) {
        this.first_card_no = first_card_no;
    }

    @Override
    public String toString() {
        return "Body{" +
                "cardno='" + cardno + '\'' +
                "cdhdnm='" + cdhdnm + '\'' +
                "prctnm='" + prctnm + '\'' +
                "bcspin='" + bcspin + '\'' +
                "bccdno='" + bccdno + '\'' +
                "ownbch='" + ownbch + '\'' +
                "stdate='" + stdate + '\'' +
                "bkcode='" + bkcode + '\'' +
                "actvin='" + actvin + '\'' +
                "actvdt='" + actvdt + '\'' +
                "cancdt='" + cancdt + '\'' +
                "pspnvr='" + pspnvr + '\'' +
                "rltnsp='" + rltnsp + '\'' +
                "cdepdt='" + cdepdt + '\'' +
                "ntcddt='" + ntcddt + '\'' +
                "rnewin='" + rnewin + '\'' +
                "rnewcd='" + rnewcd + '\'' +
                "nwcdis='" + nwcdis + '\'' +
                "qpnext='" + qpnext + '\'' +
                "ppnext='" + ppnext + '\'' +
                "pintrs='" + pintrs + '\'' +
                "ipntrs='" + ipntrs + '\'' +
                "ltptrs='" + ltptrs + '\'' +
                "ltiqpt='" + ltiqpt + '\'' +
                "ipnlmt='" + ipnlmt + '\'' +
                "plmtin='" + plmtin + '\'' +
                "cvv2_tries='" + cvv2_tries + '\'' +
                "cvv2_limiterr_ind='" + cvv2_limiterr_ind + '\'' +
                "cvv_tries='" + cvv_tries + '\'' +
                "cvv_limiterr_ind='" + cvv_limiterr_ind + '\'' +
                "icvv_tries='" + icvv_tries + '\'' +
                "icvv_limiterr_ind='" + icvv_limiterr_ind + '\'' +
                "cancel_reason='" + cancel_reason + '\'' +
                "fee_reduce_cnt='" + fee_reduce_cnt + '\'' +
                "fee_reduce_amt='" + fee_reduce_amt + '\'' +
                "fee_reduce_max_amt='" + fee_reduce_max_amt + '\'' +
                "exp_tries='" + exp_tries + '\'' +
                "exp_limiterr_ind='" + exp_limiterr_ind + '\'' +
                "source='" + source + '\'' +
                "cup_acct_info_verify_tries='" + cup_acct_info_verify_tries + '\'' +
                "cup_acct_info_verify_limiter_ind='" + cup_acct_info_verify_limiter_ind + '\'' +
                "electronic_ind='" + electronic_ind + '\'' +
                "first_activate_date='" + first_activate_date + '\'' +
                "self_apply_ind='" + self_apply_ind + '\'' +
                "first_card_no='" + first_card_no + '\'' +
                '}';
    }
}  
