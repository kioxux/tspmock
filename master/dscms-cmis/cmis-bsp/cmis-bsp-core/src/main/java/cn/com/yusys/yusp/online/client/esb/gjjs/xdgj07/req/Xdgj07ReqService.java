package cn.com.yusys.yusp.online.client.esb.gjjs.xdgj07.req;

import java.math.BigDecimal;

/**
 * 请求Service：信贷发送台账信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdgj07ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}


