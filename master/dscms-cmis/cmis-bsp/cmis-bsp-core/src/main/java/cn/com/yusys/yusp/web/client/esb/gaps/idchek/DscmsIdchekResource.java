package cn.com.yusys.yusp.web.client.esb.gaps.idchek;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.req.IdchekReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.gaps.idchek.req.IdchekReqService;
import cn.com.yusys.yusp.online.client.esb.gaps.idchek.resp.IdchekRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:身份证核查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用GAPS系统的接口处理类(idchek)")
@RestController
@RequestMapping("/api/dscms2gaps")
public class DscmsIdchekResource {
	private static final Logger logger = LoggerFactory.getLogger(DscmsIdchekResource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
	private final BeanMapUtil beanMapUtil = new BeanMapUtil();

	/**
	 * 交易码：idchek
	 * 交易描述：身份证核查
	 *
	 * @param idchekReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("idchek:身份证核查")
	@PostMapping("/idchek")
	protected @ResponseBody
	ResultDto<IdchekRespDto> idchek(@Validated @RequestBody IdchekReqDto idchekReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value, JSON.toJSONString(idchekReqDto));
		cn.com.yusys.yusp.online.client.esb.gaps.idchek.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.gaps.idchek.req.Service();
		cn.com.yusys.yusp.online.client.esb.gaps.idchek.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.gaps.idchek.resp.Service();
		IdchekReqService idchekReqService = new IdchekReqService();
		IdchekRespService idchekRespService = new IdchekRespService();
		IdchekRespDto idchekRespDto = new IdchekRespDto();
		ResultDto<IdchekRespDto> idchekResultDto = new ResultDto<IdchekRespDto>();
		Map<String, Object> context = new HashMap<>();
		Map<String, Object> result = new HashMap<>();
		try {
			//  将idchekReqDto转换成reqService
			BeanUtils.copyProperties(idchekReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_IDCHEK.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			idchekReqService.setService(reqService);
			// 将idchekReqService转换成idchekReqServiceMap
			Map idchekReqServiceMap = beanMapUtil.beanToMap(idchekReqService);
			context.put("tradeDataMap", idchekReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IDCHEK.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			idchekRespService = beanMapUtil.mapToBean(tradeDataMap, IdchekRespService.class, IdchekRespService.class);
			respService = idchekRespService.getService();
			//  将respService转换成IdchekRespDto
			BeanUtils.copyProperties(respService, idchekRespDto);
			idchekResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			idchekResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成S00101RespDto
				BeanUtils.copyProperties(respService, idchekRespDto);

				idchekResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				idchekResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				idchekResultDto.setCode(EpbEnum.EPB099999.key);
				idchekResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value, e.getMessage());
			idchekResultDto.setCode(EpbEnum.EPB099999.key);//9999
			idchekResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		idchekResultDto.setData(idchekRespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHEK.key, EsbEnum.TRADE_CODE_IDCHEK.value, JSON.toJSONString(idchekResultDto));
		return idchekResultDto;
	}
}
