package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	动产抵押-注销信息
@JsonPropertyOrder(alphabetic = true)
public class MORTGAGECAN implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "MAB_CAN_DATE")
    private String MAB_CAN_DATE;//	注销日期
    @JsonProperty(value = "MAB_CAN_RES")
    private String MAB_CAN_RES;//	注销原因
    @JsonProperty(value = "MAB_REGNO")
    private String MAB_REGNO;//	登记编号

    @JsonIgnore
    public String getMAB_CAN_DATE() {
        return MAB_CAN_DATE;
    }

    @JsonIgnore
    public void setMAB_CAN_DATE(String MAB_CAN_DATE) {
        this.MAB_CAN_DATE = MAB_CAN_DATE;
    }

    @JsonIgnore
    public String getMAB_CAN_RES() {
        return MAB_CAN_RES;
    }

    @JsonIgnore
    public void setMAB_CAN_RES(String MAB_CAN_RES) {
        this.MAB_CAN_RES = MAB_CAN_RES;
    }

    @JsonIgnore
    public String getMAB_REGNO() {
        return MAB_REGNO;
    }

    @JsonIgnore
    public void setMAB_REGNO(String MAB_REGNO) {
        this.MAB_REGNO = MAB_REGNO;
    }

    @Override
    public String toString() {
        return "MORTGAGECAN{" +
                "MAB_CAN_DATE='" + MAB_CAN_DATE + '\'' +
                ", MAB_CAN_RES='" + MAB_CAN_RES + '\'' +
                ", MAB_REGNO='" + MAB_REGNO + '\'' +
                '}';
    }
}
