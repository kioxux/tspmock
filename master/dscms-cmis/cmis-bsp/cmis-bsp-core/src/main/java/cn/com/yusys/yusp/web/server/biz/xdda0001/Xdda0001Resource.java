package cn.com.yusys.yusp.web.server.biz.xdda0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdda0001.req.Xdda0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdda0001.resp.Xdda0001RespDto;
import cn.com.yusys.yusp.dto.server.xdda0001.req.Xdda0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdda0001.resp.Xdda0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDaClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:扫描人信息登记
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDDA0001:扫描人信息登记")
@RestController
@RequestMapping("/api/dscms")
public class Xdda0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdda0001Resource.class);

    @Autowired
	private DscmsBizDaClientService dscmsBizDaClientService;


    /**
     * 交易码：xdda0001
     * 交易描述：扫描人信息登记
     *
     * @param xdda0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("扫描人信息登记")
    @PostMapping("/xdda0001")
   // @Idempotent({"xdda0001", "#xdda0001ReqDto.datasq"})
    protected @ResponseBody
    Xdda0001RespDto xdda0001(@Validated @RequestBody Xdda0001ReqDto xdda0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001ReqDto));
        Xdda0001DataReqDto xdda0001DataReqDto = new Xdda0001DataReqDto();// 请求Data： 扫描人信息登记
        Xdda0001DataRespDto xdda0001DataRespDto = new Xdda0001DataRespDto();// 响应Data：扫描人信息登记
        cn.com.yusys.yusp.dto.server.biz.xdda0001.req.Data reqData = null; // 请求Data：扫描人信息登记
        cn.com.yusys.yusp.dto.server.biz.xdda0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdda0001.resp.Data();// 响应Data：扫描人信息登记
		Xdda0001RespDto xdda0001RespDto = new Xdda0001RespDto();
		try {
            // 从 xdda0001ReqDto获取 reqData
            reqData = xdda0001ReqDto.getData();
            // 将 reqData 拷贝到xdda0001DataReqDto
            BeanUtils.copyProperties(reqData, xdda0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataReqDto));
            ResultDto<Xdda0001DataRespDto> xdda0001DataResultDto = dscmsBizDaClientService.xdda0001(xdda0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdda0001RespDto.setErorcd(Optional.ofNullable(xdda0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdda0001RespDto.setErortx(Optional.ofNullable(xdda0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdda0001DataResultDto.getCode())) {
                xdda0001DataRespDto = xdda0001DataResultDto.getData();
                xdda0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdda0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdda0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdda0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, e.getMessage());
            xdda0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdda0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdda0001RespDto.setDatasq(servsq);

        xdda0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDA0001.key, DscmsEnum.TRADE_CODE_XDDA0001.value, JSON.toJSONString(xdda0001RespDto));
        return xdda0001RespDto;
    }
}
