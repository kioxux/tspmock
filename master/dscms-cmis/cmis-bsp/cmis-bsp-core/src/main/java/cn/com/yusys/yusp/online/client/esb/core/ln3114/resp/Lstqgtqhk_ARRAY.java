package cn.com.yusys.yusp.online.client.esb.core.ln3114.resp;

/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class Lstqgtqhk_ARRAY {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
