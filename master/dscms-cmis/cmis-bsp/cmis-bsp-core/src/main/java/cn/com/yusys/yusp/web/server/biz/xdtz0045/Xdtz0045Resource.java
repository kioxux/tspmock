package cn.com.yusys.yusp.web.server.biz.xdtz0045;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0045.req.Xdtz0045ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0045.resp.Xdtz0045RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0045.req.Xdtz0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0045.resp.Xdtz0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:商贷分户实时查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0045:商贷分户实时查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0045Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0045Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0045
     * 交易描述：商贷分户实时查询
     *
     * @param xdtz0045ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("商贷分户实时查询")
    @PostMapping("/xdtz0045")
    //@Idempotent({"xdcatz0045", "#xdtz0045ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0045RespDto xdtz0045(@Validated @RequestBody Xdtz0045ReqDto xdtz0045ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045ReqDto));
        Xdtz0045DataReqDto xdtz0045DataReqDto = new Xdtz0045DataReqDto();// 请求Data： 商贷分户实时查询
        Xdtz0045DataRespDto xdtz0045DataRespDto = new Xdtz0045DataRespDto();// 响应Data：商贷分户实时查询
        Xdtz0045RespDto xdtz0045RespDto = new Xdtz0045RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0045.req.Data reqData = null; // 请求Data：商贷分户实时查询
        cn.com.yusys.yusp.dto.server.biz.xdtz0045.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0045.resp.Data();// 响应Data：商贷分户实时查询
        try {
            // 从 xdtz0045ReqDto获取 reqData
            reqData = xdtz0045ReqDto.getData();
            // 将 reqData 拷贝到xdtz0045DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0045DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045DataReqDto));
            ResultDto<Xdtz0045DataRespDto> xdtz0045DataResultDto = dscmsBizTzClientService.xdtz0045(xdtz0045DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0045RespDto.setErorcd(Optional.ofNullable(xdtz0045DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0045RespDto.setErortx(Optional.ofNullable(xdtz0045DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0045DataResultDto.getCode())) {
                xdtz0045DataRespDto = xdtz0045DataResultDto.getData();
                xdtz0045RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0045RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0045DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0045DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, e.getMessage());
            xdtz0045RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0045RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0045RespDto.setDatasq(servsq);

        xdtz0045RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0045.key, DscmsEnum.TRADE_CODE_XDTZ0045.value, JSON.toJSONString(xdtz0045RespDto));
        return xdtz0045RespDto;
    }
}
