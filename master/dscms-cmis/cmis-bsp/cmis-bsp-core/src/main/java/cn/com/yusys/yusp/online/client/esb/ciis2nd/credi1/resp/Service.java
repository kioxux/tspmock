package cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.resp;

/**
 * 响应Service：ESB信贷查询接口（处理码credi1）
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 19:50
 */
public class Service {
    private String erorcd;//	响应码	;	是	;	"响应码 0000 查询成功  9999 查询失败 6666 查无此人"
    private String erortx;//	响应信息	;	是	;	响应信息
    private String zxSerno;//	交易流水号	;	是	;	本次查询交易流水号，区分查询交易的唯一标识
    private String id;//	业务明细ID	;	否	;	预留字段，未返回
    private String servsq;//	渠道流水 	;	否	;	预留字段，未返回
    private String docid;//	授权书id	;	否	;	预留字段，未返回
    private String repDate;//	报告生成日期	;	否	;	预留字段，未返回

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getZxSerno() {
        return zxSerno;
    }

    public void setZxSerno(String zxSerno) {
        this.zxSerno = zxSerno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getRepDate() {
        return repDate;
    }

    public void setRepDate(String repDate) {
        this.repDate = repDate;
    }

    @Override
    public String toString() {
        return "Credi1RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", zxSerno='" + zxSerno + '\'' +
                ", id='" + id + '\'' +
                ", servsq='" + servsq + '\'' +
                ", docid='" + docid + '\'' +
                ", repDate='" + repDate + '\'' +
                '}';
    }
}
