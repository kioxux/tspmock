package cn.com.yusys.yusp.online.client.esb.core.ln3246.resp;


/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class Ln3246RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
