package cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp;

import cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/11 15:52
 * @since 2021/8/11 15:52
 */
public class Message {
    private cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead head;
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp.Body body;

    public GxpRespHead getHead() {
        return head;
    }

    public void setHead(GxpRespHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
