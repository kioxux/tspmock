package cn.com.yusys.yusp.web.client.esb.core.ln3247;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3247.req.Ln3247ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3247.resp.Ln3247RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3247.req.Ln3247ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3247.resp.Ln3247RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3247)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3247Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3247Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();


    /**
     * 委托清收变更
     *
     * @param reqDto Ln3247ReqDto
     * @return ResultDto<Ln3247RespDto>
     * @throws Exception
     */
    @ApiOperation("ln3247:贷款还款周期预定变更")
    @PostMapping("/ln3247")
    protected @ResponseBody
    ResultDto<Ln3247RespDto> ln3247(@Validated @RequestBody Ln3247ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3247.key, EsbEnum.TRADE_CODE_LN3247.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3247.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3247.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3247.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3247.resp.Service();
        Ln3247ReqService ln3247ReqService = new Ln3247ReqService();
        Ln3247RespService ln3247RespService = new Ln3247RespService();
        Ln3247RespDto ln3247RespDto = new Ln3247RespDto();
        ResultDto<Ln3247RespDto> ln3247ResultDto = new ResultDto<Ln3247RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {

            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3247.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3247ReqService.setService(reqService);
            // 将ln3247ReqService转换成ln3247ReqServiceMap
            Map ln3247ReqServiceMap = beanMapUtil.beanToMap(ln3247ReqService);
            context.put("tradeDataMap", ln3247ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3247.key, EsbEnum.TRADE_CODE_LN3247.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3247.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3247.key, EsbEnum.TRADE_CODE_LN3247.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3247RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3247RespService.class, Ln3247RespService.class);
            respService = ln3247RespService.getService();

            //  将Ln3247RespDto封装到ResultDto<Ln3247RespDto>
            ln3247ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3247ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3247RespDto
                BeanUtils.copyProperties(respService, ln3247RespDto);

                ln3247ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3247ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3247ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3247ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3247.key, EsbEnum.TRADE_CODE_LN3247.value, e.getMessage());
            ln3247ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3247ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3247ResultDto.setData(ln3247RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3247.key, EsbEnum.TRADE_CODE_LN3247.value, JSON.toJSONString(ln3247ResultDto));

        return ln3247ResultDto;
    }
}
