package cn.com.yusys.yusp.online.client.esb.ecif.g00203.req;

import java.util.List;

/**
 * 请求Service：同业客户维护,关联信息_ARRAY
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class RelArrayInfo {
    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }
}
