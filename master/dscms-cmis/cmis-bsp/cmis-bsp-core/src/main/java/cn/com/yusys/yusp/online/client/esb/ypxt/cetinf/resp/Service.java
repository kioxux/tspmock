package cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.resp;

/**
 * 响应Service：权证信息同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:41
 */
public class Service {
    private String erorcd; // 响应码,0000表示成功其它表示失败
    private String erortx; // 响应信息,失败详情

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "CetinfRespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                '}';
    }
}
