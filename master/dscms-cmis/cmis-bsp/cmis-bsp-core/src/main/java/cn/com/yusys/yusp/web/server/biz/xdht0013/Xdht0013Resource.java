package cn.com.yusys.yusp.web.server.biz.xdht0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0013.req.Xdht0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0013.resp.Xdht0013RespDto;
import cn.com.yusys.yusp.dto.server.xdht0013.req.Xdht0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0013.resp.Xdht0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同信息查询(微信小程序)
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0013:合同信息查询(微信小程序)")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0013Resource.class);

    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0013
     * 交易描述：合同信息查询(微信小程序)
     *
     * @param xdht0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息查询(微信小程序)")
    @PostMapping("/xdht0013")
    //@Idempotent({"xdcaht0013", "#xdht0013ReqDto.datasq"})
    protected @ResponseBody
    Xdht0013RespDto xdht0013(@Validated @RequestBody Xdht0013ReqDto xdht0013ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013ReqDto));
        Xdht0013DataReqDto xdht0013DataReqDto = new Xdht0013DataReqDto();// 请求Data： 合同信息查询(微信小程序)
        Xdht0013DataRespDto xdht0013DataRespDto = new Xdht0013DataRespDto();// 响应Data：合同信息查询(微信小程序)
        Xdht0013RespDto xdht0013RespDto = new Xdht0013RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0013.req.Data reqData = null; // 请求Data：合同信息查询(微信小程序)
        cn.com.yusys.yusp.dto.server.biz.xdht0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0013.resp.Data();// 响应Data：合同信息查询(微信小程序)

        try {
            // 从 xdht0013ReqDto获取 reqData
            reqData = xdht0013ReqDto.getData();
            // 将 reqData 拷贝到xdht0013DataReqDto
            BeanUtils.copyProperties(reqData, xdht0013DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataReqDto));
            ResultDto<Xdht0013DataRespDto> xdht0013DataResultDto = dscmsBizHtClientService.xdht0013(xdht0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0013RespDto.setErorcd(Optional.ofNullable(xdht0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0013RespDto.setErortx(Optional.ofNullable(xdht0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0013DataResultDto.getCode())) {
                xdht0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0013DataRespDto = xdht0013DataResultDto.getData();
                // 将 xdht0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0013DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, e.getMessage());
            xdht0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0013RespDto.setDatasq(servsq);

        xdht0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0013.key, DscmsEnum.TRADE_CODE_XDHT0013.value, JSON.toJSONString(xdht0013RespDto));
        return xdht0013RespDto;
    }
}
