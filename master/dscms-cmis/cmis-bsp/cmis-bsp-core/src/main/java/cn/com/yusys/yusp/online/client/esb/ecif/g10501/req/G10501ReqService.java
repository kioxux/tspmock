package cn.com.yusys.yusp.online.client.esb.ecif.g10501.req;

/**
 * 请求Service：对公及同业客户清单查询
 */
public class G10501ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
