package cn.com.yusys.yusp.web.server.biz.xdcz0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0019.req.Xdcz0019ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0019.resp.Xdcz0019RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0019.req.Xdcz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0019.resp.Xdcz0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小贷额度支用申请书生成pdf
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0019:小贷额度支用申请书生成pdf")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0019Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0019
     * 交易描述：小贷额度支用申请书生成pdf
     *
     * @param xdcz0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小贷额度支用申请书生成pdf")
    @PostMapping("/xdcz0019")
   //@Idempotent({"xdcz0019", "#xdcz0019ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0019RespDto xdcz0019(@Validated @RequestBody Xdcz0019ReqDto xdcz0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019ReqDto));
        Xdcz0019DataReqDto xdcz0019DataReqDto = new Xdcz0019DataReqDto();// 请求Data： 小贷额度支用申请书生成pdf
        Xdcz0019DataRespDto xdcz0019DataRespDto = new Xdcz0019DataRespDto();// 响应Data：小贷额度支用申请书生成pdf

        cn.com.yusys.yusp.dto.server.biz.xdcz0019.req.Data reqData = null; // 请求Data：小贷额度支用申请书生成pdf
        cn.com.yusys.yusp.dto.server.biz.xdcz0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdcz0019.resp.Data();// 响应Data：电子保函开立

        Xdcz0019RespDto xdcz0019RespDto = new Xdcz0019RespDto();
        try {
            // 从 xdcz0019ReqDto获取 reqData
            reqData = xdcz0019ReqDto.getData();
            // 将 reqData 拷贝到xdcz0019DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0019DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019DataReqDto));
            ResultDto<Xdcz0019DataRespDto> xdcz0019DataResultDto = dscmsBizCzClientService.xdcz0019(xdcz0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0019RespDto.setErorcd(Optional.ofNullable(xdcz0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0019RespDto.setErortx(Optional.ofNullable(xdcz0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0019DataResultDto.getCode())) {
                xdcz0019DataRespDto = xdcz0019DataResultDto.getData();
                xdcz0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, e.getMessage());
            xdcz0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0019RespDto.setDatasq(servsq);

        xdcz0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0019.key, DscmsEnum.TRADE_CODE_XDCZ0019.value, JSON.toJSONString(xdcz0019RespDto));
        return xdcz0019RespDto;
    }
}
