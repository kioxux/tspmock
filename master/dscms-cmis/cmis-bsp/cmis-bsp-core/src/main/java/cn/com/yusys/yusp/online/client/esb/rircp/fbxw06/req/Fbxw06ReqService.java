package cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.req;

/**
 * 请求Service：利率定价测算提交接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午8:14:55
 */
public class Fbxw06ReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

}