package cn.com.yusys.yusp.web.client.esb.core.ln3243;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3243.Ln3243ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3243.Ln3243RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3243.req.Ln3243ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3243.resp.Ln3243RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3243)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3243Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3243Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3243
     * 交易描述：停息贷款利息试算
     *
     * @param ln3243ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3243:停息贷款利息试算")
    @PostMapping("/ln3243")
    protected @ResponseBody
    ResultDto<Ln3243RespDto> ln3243(@Validated @RequestBody Ln3243ReqDto ln3243ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3243.key, EsbEnum.TRADE_CODE_LN3243.value, JSON.toJSONString(ln3243ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3243.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3243.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3243.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3243.resp.Service();
        Ln3243ReqService ln3243ReqService = new Ln3243ReqService();
        Ln3243RespService ln3243RespService = new Ln3243RespService();
        Ln3243RespDto ln3243RespDto = new Ln3243RespDto();
        ResultDto<Ln3243RespDto> ln3243ResultDto = new ResultDto<Ln3243RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3243ReqDto转换成reqService
            BeanUtils.copyProperties(ln3243ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3243.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3243ReqService.setService(reqService);
            // 将ln3243ReqService转换成ln3243ReqServiceMap
            Map ln3243ReqServiceMap = beanMapUtil.beanToMap(ln3243ReqService);
            context.put("tradeDataMap", ln3243ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3243.key, EsbEnum.TRADE_CODE_LN3243.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3243.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3243.key, EsbEnum.TRADE_CODE_LN3243.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3243RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3243RespService.class, Ln3243RespService.class);
            respService = ln3243RespService.getService();

            ln3243ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3243ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3243RespDto
                BeanUtils.copyProperties(respService, ln3243RespDto);
                ln3243ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3243ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3243ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3243ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3243.key, EsbEnum.TRADE_CODE_LN3243.value, e.getMessage());
            ln3243ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3243ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3243ResultDto.setData(ln3243RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3243.key, EsbEnum.TRADE_CODE_LN3243.value, JSON.toJSONString(ln3243ResultDto));
        return ln3243ResultDto;
    }

}
