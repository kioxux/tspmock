package cn.com.yusys.yusp.online.client.esb.core.ln3020.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkdygl.Record;

import java.util.List;

/**
 * 请求Dto：抵押关联信息
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Lstdkdygl_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkdygl.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkdygl_ARRAY{" +
                "record=" + record +
                '}';
    }
}