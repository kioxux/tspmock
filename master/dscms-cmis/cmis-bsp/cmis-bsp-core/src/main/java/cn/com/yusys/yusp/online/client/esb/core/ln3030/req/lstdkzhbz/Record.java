package cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkzhbz;

import java.math.BigDecimal;

/**
 * 请求Service：贷款保证人信息
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String baozhfsh;//保证方式
    private String beizhuuu;//备注信息
    private String bzrkehuh;//保证人客户号
    private BigDecimal baozjine;//保证金额
    private String kehmingc;//客户名称
    private String danbzhao;//担保账号
    private String dbzhzxuh;//担保账号子序号

    public String getBaozhfsh() {
        return baozhfsh;
    }

    public void setBaozhfsh(String baozhfsh) {
        this.baozhfsh = baozhfsh;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getBzrkehuh() {
        return bzrkehuh;
    }

    public void setBzrkehuh(String bzrkehuh) {
        this.bzrkehuh = bzrkehuh;
    }

    public BigDecimal getBaozjine() {
        return baozjine;
    }

    public void setBaozjine(BigDecimal baozjine) {
        this.baozjine = baozjine;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getDanbzhao() {
        return danbzhao;
    }

    public void setDanbzhao(String danbzhao) {
        this.danbzhao = danbzhao;
    }

    public String getDbzhzxuh() {
        return dbzhzxuh;
    }

    public void setDbzhzxuh(String dbzhzxuh) {
        this.dbzhzxuh = dbzhzxuh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "baozhfsh='" + baozhfsh + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "bzrkehuh='" + bzrkehuh + '\'' +
                "baozjine='" + baozjine + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "danbzhao='" + danbzhao + '\'' +
                "dbzhzxuh='" + dbzhzxuh + '\'' +
                '}';
    }
}
