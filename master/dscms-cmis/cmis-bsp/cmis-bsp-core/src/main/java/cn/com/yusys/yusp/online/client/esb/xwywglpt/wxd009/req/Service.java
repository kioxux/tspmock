package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.req;

/**
 * 请求Service：信贷系统请求小V平台推送合同信息接口
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String reqid;//（小微）系统流水号
    private String cont_no;//合同编号
    private String cont_state;//合同状态
    private String cont_type;//合同类型
    private String loan_end_date;//合同到期日
    private String sign_date;//合同签订日

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getReqid() {
        return reqid;
    }

    public void setReqid(String reqid) {
        this.reqid = reqid;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "reqid='" + reqid + '\'' +
                "cont_no='" + cont_no + '\'' +
                "cont_state='" + cont_state + '\'' +
                "cont_type='" + cont_type + '\'' +
                "loan_end_date='" + loan_end_date + '\'' +
                "sign_date='" + sign_date + '\'' +
                '}';
    }
}
