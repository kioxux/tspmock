package cn.com.yusys.yusp.web.client.esb.znwdspxt.znsp05;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.req.Znsp05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.resp.Znsp05RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.req.Znsp05ReqService;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.resp.Znsp05RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用智能微贷审批系统的接口处理类
 **/
@Api(tags = "BSP封装调用智能微贷审批系统的接口处理类()")
@RestController
@RequestMapping("/api/dscms2znwdspxt")
public class Dscms2Znsp05Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Znsp05Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：znsp05
     * 交易描述：风险拦截截接口
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("znsp05:风险拦截截接口")
    @PostMapping("/znsp05")
    protected @ResponseBody
    ResultDto<Znsp05RespDto> znsp05(@Validated @RequestBody Znsp05ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP05.key, EsbEnum.TRADE_CODE_ZNSP05.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.req.Service();
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.resp.Service();
        Znsp05ReqService znsp05ReqService = new Znsp05ReqService();
        Znsp05RespService znsp05RespService = new Znsp05RespService();
        Znsp05RespDto znsp05RespDto = new Znsp05RespDto();
        ResultDto<Znsp05RespDto> znsp05ResultDto = new ResultDto<Znsp05RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将znsp05ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_ZNSP05.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_ZNWDSPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ZNWDSPXT.key);//    部门号
            znsp05ReqService.setService(reqService);
            // 将znsp05ReqService转换成znsp05ReqServiceMap
            Map znsp05ReqServiceMap = beanMapUtil.beanToMap(znsp05ReqService);
            context.put("tradeDataMap", znsp05ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP05.key, EsbEnum.TRADE_CODE_ZNSP05.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_ZNSP05.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP05.key, EsbEnum.TRADE_CODE_ZNSP05.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            znsp05RespService = beanMapUtil.mapToBean(tradeDataMap, Znsp05RespService.class, Znsp05RespService.class);
            respService = znsp05RespService.getService();
            //  将respService转换成znsp05RespDto
            BeanUtils.copyProperties(respService, znsp05RespDto);
            znsp05ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            znsp05ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成znsp05RespDto
                BeanUtils.copyProperties(respService, znsp05RespDto);
                znsp05ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                znsp05ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                znsp05ResultDto.setCode(EpbEnum.EPB099999.key);
                znsp05ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP05.key, EsbEnum.TRADE_CODE_ZNSP05.value, e.getMessage());
            znsp05ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            znsp05ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        znsp05ResultDto.setData(znsp05RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP05.key, EsbEnum.TRADE_CODE_ZNSP05.value, JSON.toJSONString(znsp05ResultDto));
        return znsp05ResultDto;
    }
}
