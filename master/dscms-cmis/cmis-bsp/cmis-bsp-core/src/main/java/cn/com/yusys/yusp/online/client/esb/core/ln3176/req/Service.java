package cn.com.yusys.yusp.online.client.esb.core.ln3176.req;

import java.math.BigDecimal;

/**
 * 请求Service：本交易用户贷款多个还款账户进行还款
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String daikczbz;//业务操作标志
    private String dkjiejuh;//贷款借据号
    private String dkzhangh;//贷款账号
    private String hetongbh;//合同编号
    private String kehuhaoo;//客户号
    private String kehuzwmc;//客户名
    private String huobdhao;//货币代号
    private String huankzle;//还款种类
    private String dktqhkzl;//提前还款种类
    private BigDecimal tqhktysx;//退客户预收息金额
    private String tqhktzfs;//提前还款调整计划方式
    private String tqhkhxfs;//提前还款还息方式
    private cn.com.yusys.yusp.online.client.esb.core.ln3176.req.LstLnHuankDZhhObj lstLnHuankDZhhObj;//多还款账户对象

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHuankzle() {
        return huankzle;
    }

    public void setHuankzle(String huankzle) {
        this.huankzle = huankzle;
    }

    public String getDktqhkzl() {
        return dktqhkzl;
    }

    public void setDktqhkzl(String dktqhkzl) {
        this.dktqhkzl = dktqhkzl;
    }

    public BigDecimal getTqhktysx() {
        return tqhktysx;
    }

    public void setTqhktysx(BigDecimal tqhktysx) {
        this.tqhktysx = tqhktysx;
    }

    public String getTqhktzfs() {
        return tqhktzfs;
    }

    public void setTqhktzfs(String tqhktzfs) {
        this.tqhktzfs = tqhktzfs;
    }

    public String getTqhkhxfs() {
        return tqhkhxfs;
    }

    public void setTqhkhxfs(String tqhkhxfs) {
        this.tqhkhxfs = tqhkhxfs;
    }

    public LstLnHuankDZhhObj getLstLnHuankDZhhObj() {
        return lstLnHuankDZhhObj;
    }

    public void setLstLnHuankDZhhObj(LstLnHuankDZhhObj lstLnHuankDZhhObj) {
        this.lstLnHuankDZhhObj = lstLnHuankDZhhObj;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", huankzle='" + huankzle + '\'' +
                ", dktqhkzl='" + dktqhkzl + '\'' +
                ", tqhktysx=" + tqhktysx +
                ", tqhktzfs='" + tqhktzfs + '\'' +
                ", tqhkhxfs='" + tqhkhxfs + '\'' +
                ", lstLnHuankDZhhObj=" + lstLnHuankDZhhObj +
                '}';
    }
}
