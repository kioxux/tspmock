package cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0;

import java.util.List;

/**
 * 响应Service：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Listnm0_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Listnm0_ARRAY{" +
                "record=" + record +
                '}';
    }
}
