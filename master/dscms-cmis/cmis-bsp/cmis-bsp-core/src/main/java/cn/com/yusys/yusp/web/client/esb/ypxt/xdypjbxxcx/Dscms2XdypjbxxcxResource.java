package cn.com.yusys.yusp.web.client.esb.ypxt.xdypjbxxcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypjbxxcx.req.XdypjbxxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypjbxxcx.resp.XdypjbxxcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypjbxxcx.req.XdypjbxxcxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypjbxxcx.resp.XdypjbxxcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(xdypjbxxcx)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2XdypjbxxcxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdypjbxxcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdypjbxxcx
     * 交易描述：查询共有人信息
     *
     * @param xdypjbxxcxReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdypjbxxcx:查询共有人信息")
    @PostMapping("/xdypjbxxcx")
    protected @ResponseBody
    ResultDto<XdypjbxxcxRespDto> xdypjbxxcx(@Validated @RequestBody XdypjbxxcxReqDto xdypjbxxcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPJBXXCX.key, EsbEnum.TRADE_CODE_XDYPJBXXCX.value, JSON.toJSONString(xdypjbxxcxReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypjbxxcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypjbxxcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypjbxxcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypjbxxcx.resp.Service();
        XdypjbxxcxReqService xdypjbxxcxReqService = new XdypjbxxcxReqService();
        XdypjbxxcxRespService xdypjbxxcxRespService = new XdypjbxxcxRespService();
        XdypjbxxcxRespDto xdypjbxxcxRespDto = new XdypjbxxcxRespDto();
        ResultDto<XdypjbxxcxRespDto> xdypjbxxcxResultDto = new ResultDto<XdypjbxxcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdypjbxxcxReqDto转换成reqService
            BeanUtils.copyProperties(xdypjbxxcxReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDYPJBXXCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xdypjbxxcxReqService.setService(reqService);
            // 将xdypjbxxcxReqService转换成xdypjbxxcxReqServiceMap
            Map xdypjbxxcxReqServiceMap = beanMapUtil.beanToMap(xdypjbxxcxReqService);
            context.put("tradeDataMap", xdypjbxxcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPJBXXCX.key, EsbEnum.TRADE_CODE_XDYPJBXXCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDYPJBXXCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPJBXXCX.key, EsbEnum.TRADE_CODE_XDYPJBXXCX.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");

            xdypjbxxcxRespService = beanMapUtil.mapToBean(tradeDataMap, XdypjbxxcxRespService.class, XdypjbxxcxRespService.class);
            respService = xdypjbxxcxRespService.getService();

            xdypjbxxcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdypjbxxcxResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypjbxxcxRespDto
                BeanUtils.copyProperties(respService, xdypjbxxcxRespDto);
                xdypjbxxcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdypjbxxcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdypjbxxcxResultDto.setCode(EpbEnum.EPB099999.key);
                xdypjbxxcxResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPJBXXCX.key, EsbEnum.TRADE_CODE_XDYPJBXXCX.value, e.getMessage());
            xdypjbxxcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdypjbxxcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdypjbxxcxResultDto.setData(xdypjbxxcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPJBXXCX.key, EsbEnum.TRADE_CODE_XDYPJBXXCX.value, JSON.toJSONString(xdypjbxxcxResultDto));
        return xdypjbxxcxResultDto;
    }
}
