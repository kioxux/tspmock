package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsyfw.Record;

import java.util.List;

/**
 * 响应Service：机构控制信息对象
 *
 * @author lihh
 * @version 1.0
 */
public class Lstcpsyfw_ARRAY {
    private List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsyfw.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstcpsyfw_ARRAY{" +
                "record=" + record +
                '}';
    }
}
