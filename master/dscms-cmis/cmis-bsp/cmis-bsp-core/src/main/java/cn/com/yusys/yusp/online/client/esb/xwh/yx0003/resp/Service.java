package cn.com.yusys.yusp.online.client.esb.xwh.yx0003.resp;

/**
 * 响应Service：市民贷优惠券核销锁定
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String guarno;//抵押物编号
    private String yxzjls;//影像主键流水号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    public String getYxzjls() {
        return yxzjls;
    }

    public void setYxzjls(String yxzjls) {
        this.yxzjls = yxzjls;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "guarno='" + guarno + '\'' +
                "yxzjls='" + yxzjls + '\'' +
                '}';
    }
}
