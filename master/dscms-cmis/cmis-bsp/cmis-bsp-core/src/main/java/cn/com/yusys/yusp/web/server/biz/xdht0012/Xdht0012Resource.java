package cn.com.yusys.yusp.web.server.biz.xdht0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0012.req.Xdht0012ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0012.resp.Xdht0012RespDto;
import cn.com.yusys.yusp.dto.server.xdht0012.req.Xdht0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0012.resp.Xdht0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借款担保合同签订/支用
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0012:借款担保合同签订/支用")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0012Resource.class);
	@Autowired
	private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0012
     * 交易描述：借款担保合同签订/支用
     *
     * @param xdht0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借款担保合同签订/支用")
    @PostMapping("/xdht0012")
    //@Idempotent({"xdcaht0012", "#xdht0012ReqDto.datasq"})
    protected @ResponseBody
	Xdht0012RespDto xdht0012(@Validated @RequestBody Xdht0012ReqDto xdht0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012ReqDto));
        Xdht0012DataReqDto xdht0012DataReqDto = new Xdht0012DataReqDto();// 请求Data： 借款担保合同签订/支用
        Xdht0012DataRespDto xdht0012DataRespDto = new Xdht0012DataRespDto();// 响应Data：借款担保合同签订/支用
		Xdht0012RespDto xdht0012RespDto = new Xdht0012RespDto();
		cn.com.yusys.yusp.dto.server.biz.xdht0012.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdht0012.req.Data(); // 请求Data：借款担保合同签订/支用
		cn.com.yusys.yusp.dto.server.biz.xdht0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0012.resp.Data();// 响应Data：借款担保合同签订/支用
        try {
            // 从 xdht0012ReqDto获取 reqData
            reqData = xdht0012ReqDto.getData();
            // 将 reqData 拷贝到xdht0012DataReqDto
            BeanUtils.copyProperties(reqData, xdht0012DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataReqDto));
            ResultDto<Xdht0012DataRespDto> xdht0012DataResultDto = dscmsBizHtClientService.xdht0012(xdht0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0012RespDto.setErorcd(Optional.ofNullable(xdht0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0012RespDto.setErortx(Optional.ofNullable(xdht0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0012DataResultDto.getCode())) {
                xdht0012DataRespDto = xdht0012DataResultDto.getData();
                xdht0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, e.getMessage());
            xdht0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0012RespDto.setDatasq(servsq);

        xdht0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0012.key, DscmsEnum.TRADE_CODE_XDHT0012.value, JSON.toJSONString(xdht0012RespDto));
        return xdht0012RespDto;
    }
}
