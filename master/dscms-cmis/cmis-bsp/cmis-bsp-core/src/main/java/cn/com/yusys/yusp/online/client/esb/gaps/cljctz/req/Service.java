package cn.com.yusys.yusp.online.client.esb.gaps.cljctz.req;

import java.math.BigDecimal;

/**
 * 请求Service：连云港存量房列表
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道码
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servsq;//渠道流水
    private String servdt;//请求方日期
    private String servti;//请求方时间

    private String ipaddr;//ip地址
    private String mac;//mac地址
    private String savvou;//监管协议号（缴款凭证编号）
    private String acctno;//监管账号
    private String acctna;//监管账号名称
    private BigDecimal tranam;//本次缴款金额
    private String trantp;//缴款类型
    private String savsrc;//缴存资金来源
    private String svbkna;//缴款银行名称
    private String svbkno;//缴款银行行号
    private String sernum;//流水号
    private String savtim;//缴款时间
    private String savdec;//缴款说明
    private String savimg;//缴款凭证图片
    private String pyerac;//付款账号
    private String pyerna;//付款账号名称
    private String byidna;//买方名称
    private String byidno;//买方证件号
    private String contno;//合同编号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getSavvou() {
        return savvou;
    }

    public void setSavvou(String savvou) {
        this.savvou = savvou;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctna() {
        return acctna;
    }

    public void setAcctna(String acctna) {
        this.acctna = acctna;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getTrantp() {
        return trantp;
    }

    public void setTrantp(String trantp) {
        this.trantp = trantp;
    }

    public String getSavsrc() {
        return savsrc;
    }

    public void setSavsrc(String savsrc) {
        this.savsrc = savsrc;
    }

    public String getSvbkna() {
        return svbkna;
    }

    public void setSvbkna(String svbkna) {
        this.svbkna = svbkna;
    }

    public String getSvbkno() {
        return svbkno;
    }

    public void setSvbkno(String svbkno) {
        this.svbkno = svbkno;
    }

    public String getSernum() {
        return sernum;
    }

    public void setSernum(String sernum) {
        this.sernum = sernum;
    }

    public String getSavtim() {
        return savtim;
    }

    public void setSavtim(String savtim) {
        this.savtim = savtim;
    }

    public String getSavdec() {
        return savdec;
    }

    public void setSavdec(String savdec) {
        this.savdec = savdec;
    }

    public String getSavimg() {
        return savimg;
    }

    public void setSavimg(String savimg) {
        this.savimg = savimg;
    }

    public String getPyerac() {
        return pyerac;
    }

    public void setPyerac(String pyerac) {
        this.pyerac = pyerac;
    }

    public String getPyerna() {
        return pyerna;
    }

    public void setPyerna(String pyerna) {
        this.pyerna = pyerna;
    }

    public String getByidna() {
        return byidna;
    }

    public void setByidna(String byidna) {
        this.byidna = byidna;
    }

    public String getByidno() {
        return byidno;
    }

    public void setByidno(String byidno) {
        this.byidno = byidno;
    }

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", mac='" + mac + '\'' +
                ", savvou='" + savvou + '\'' +
                ", acctno='" + acctno + '\'' +
                ", acctna='" + acctna + '\'' +
                ", tranam=" + tranam +
                ", trantp='" + trantp + '\'' +
                ", savsrc='" + savsrc + '\'' +
                ", svbkna='" + svbkna + '\'' +
                ", svbkno='" + svbkno + '\'' +
                ", sernum='" + sernum + '\'' +
                ", savtim='" + savtim + '\'' +
                ", savdec='" + savdec + '\'' +
                ", savimg='" + savimg + '\'' +
                ", pyerac='" + pyerac + '\'' +
                ", pyerna='" + pyerna + '\'' +
                ", byidna='" + byidna + '\'' +
                ", byidno='" + byidno + '\'' +
                ", contno='" + contno + '\'' +
                '}';
    }
}
