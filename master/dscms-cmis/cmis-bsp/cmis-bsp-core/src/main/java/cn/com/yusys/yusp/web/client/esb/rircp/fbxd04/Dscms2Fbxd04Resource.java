package cn.com.yusys.yusp.web.client.esb.rircp.fbxd04;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.Fbxd04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.Fbxd04RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.req.Fbxd04ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.Fbxd04RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd04）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd04Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd04Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd04
     * 交易描述：查找指定数据日期的放款合约明细记录历史表（利翃）一览
     *
     * @param fbxd04ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd04")
    protected @ResponseBody
    ResultDto<Fbxd04RespDto> fbxd04(@Validated @RequestBody Fbxd04ReqDto fbxd04ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD04.key, EsbEnum.TRADE_CODE_FBXD04.value, JSON.toJSONString(fbxd04ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.Service();
        Fbxd04ReqService fbxd04ReqService = new Fbxd04ReqService();
        Fbxd04RespService fbxd04RespService = new Fbxd04RespService();
        Fbxd04RespDto fbxd04RespDto = new Fbxd04RespDto();
        ResultDto<Fbxd04RespDto> fbxd04ResultDto = new ResultDto<Fbxd04RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd04ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd04ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD04.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            fbxd04ReqService.setService(reqService);
            // 将fbxd04ReqService转换成fbxd04ReqServiceMap
            Map fbxd04ReqServiceMap = beanMapUtil.beanToMap(fbxd04ReqService);
            context.put("tradeDataMap", fbxd04ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD04.key, EsbEnum.TRADE_CODE_FBXD04.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD04.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD04.key, EsbEnum.TRADE_CODE_FBXD04.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd04RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd04RespService.class, Fbxd04RespService.class);
            respService = fbxd04RespService.getService();

            fbxd04ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd04ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd04RespDto
                BeanUtils.copyProperties(respService, fbxd04RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.List fbxd04RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.List());
                respService.setList(fbxd04RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.Record> fbxd04RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.List> fbxd04RespDtoLists = fbxd04RespRecordList.parallelStream().map(fbxd04RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.List fbxd04RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.List();
                        BeanUtils.copyProperties(fbxd04RespRecord, fbxd04RespDtoList);
                        return fbxd04RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd04RespDto.setList(fbxd04RespDtoLists);
                }
                fbxd04ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd04ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd04ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd04ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD04.key, EsbEnum.TRADE_CODE_FBXD04.value, e.getMessage());
            fbxd04ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd04ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd04ResultDto.setData(fbxd04RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD04.key, EsbEnum.TRADE_CODE_FBXD04.value, JSON.toJSONString(fbxd04ResultDto));
        return fbxd04ResultDto;
    }
}
