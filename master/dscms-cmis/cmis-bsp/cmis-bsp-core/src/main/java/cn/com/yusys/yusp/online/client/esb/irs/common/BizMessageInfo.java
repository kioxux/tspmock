package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 响应Service：响应信息域元素:业务层债项评级结果
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class BizMessageInfo {
    private List<BizMessageInfoRecord> record; // 业务层债项评级结果

    public List<BizMessageInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<BizMessageInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "BizMessageInfo{" +
                "record=" + record +
                '}';
    }
}
