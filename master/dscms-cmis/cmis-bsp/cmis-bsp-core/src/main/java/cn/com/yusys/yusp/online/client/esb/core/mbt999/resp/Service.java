package cn.com.yusys.yusp.online.client.esb.core.mbt999.resp;

/**
 * 响应Service：V5通用记账
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述
    private String jioylius;//交易流水
    private String jiaoyirq;//交易日期
    private String qianzhls;//前置流水
    private String qianzhrq;//前置日期
    private String jiaoyima;//交易码
    private String qiatyima;//前台交易码
    private Integer jiaoyisj;//交易时间
    private String jioyguiy;//交易柜员
    private String shoqguiy;//授权柜员
    private String guazhnxh;//挂账序号
    private List list;//v5通用记账列表

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getJioylius() {
        return jioylius;
    }

    public void setJioylius(String jioylius) {
        this.jioylius = jioylius;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getQianzhls() {
        return qianzhls;
    }

    public void setQianzhls(String qianzhls) {
        this.qianzhls = qianzhls;
    }

    public String getQianzhrq() {
        return qianzhrq;
    }

    public void setQianzhrq(String qianzhrq) {
        this.qianzhrq = qianzhrq;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getQiatyima() {
        return qiatyima;
    }

    public void setQiatyima(String qiatyima) {
        this.qiatyima = qiatyima;
    }

    public Integer getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(Integer jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getJioyguiy() {
        return jioyguiy;
    }

    public void setJioyguiy(String jioyguiy) {
        this.jioyguiy = jioyguiy;
    }

    public String getShoqguiy() {
        return shoqguiy;
    }

    public void setShoqguiy(String shoqguiy) {
        this.shoqguiy = shoqguiy;
    }

    public String getGuazhnxh() {
        return guazhnxh;
    }

    public void setGuazhnxh(String guazhnxh) {
        this.guazhnxh = guazhnxh;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "jioylius='" + jioylius + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "qianzhls='" + qianzhls + '\'' +
                "qianzhrq='" + qianzhrq + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "qiatyima='" + qiatyima + '\'' +
                "jiaoyisj='" + jiaoyisj + '\'' +
                "jioyguiy='" + jioyguiy + '\'' +
                "shoqguiy='" + shoqguiy + '\'' +
                "guazhnxh='" + guazhnxh + '\'' +
                "list='" + list + '\'' +
                '}';
    }
}
