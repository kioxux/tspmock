package cn.com.yusys.yusp.web.server.biz.xdht0037;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0037.req.Xdht0037ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0037.resp.Xdht0037RespDto;
import cn.com.yusys.yusp.dto.server.xdht0037.req.Xdht0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0037.resp.Xdht0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:大家e贷、乐悠金根据客户号查询房贷首付款比例
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0037:大家e贷、乐悠金根据客户号查询房贷首付款比例")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0037Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0037
     * 交易描述：大家e贷、乐悠金根据客户号查询房贷首付款比例
     *
     * @param xdht0037ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大家e贷、乐悠金根据客户号查询房贷首付款比例")
    @PostMapping("/xdht0037")
//    @Idempotent({"xdcaht0037", "#xdht0037ReqDto.datasq"})
    protected @ResponseBody
    Xdht0037RespDto xdht0037(@Validated @RequestBody Xdht0037ReqDto xdht0037ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, JSON.toJSONString(xdht0037ReqDto));
        Xdht0037DataReqDto xdht0037DataReqDto = new Xdht0037DataReqDto();// 请求Data： 大家e贷、乐悠金根据客户号查询房贷首付款比例
        Xdht0037DataRespDto xdht0037DataRespDto = new Xdht0037DataRespDto();// 响应Data：大家e贷、乐悠金根据客户号查询房贷首付款比例
        cn.com.yusys.yusp.dto.server.biz.xdht0037.req.Data reqData = null; // 请求Data：大家e贷、乐悠金根据客户号查询房贷首付款比例
        Xdht0037RespDto xdht0037RespDto = new Xdht0037RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0037.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0037.resp.Data();// 响应Data：大家e贷、乐悠金根据客户号查询房贷首付款比例
        try {
            // 从 xdht0037ReqDto获取 reqData
            reqData = xdht0037ReqDto.getData();
            // 将 reqData 拷贝到xdht0037DataReqDto
            BeanUtils.copyProperties(reqData, xdht0037DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, JSON.toJSONString(xdht0037DataReqDto));
            ResultDto<Xdht0037DataRespDto> xdht0037DataResultDto = dscmsBizHtClientService.xdht0037(xdht0037DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, JSON.toJSONString(xdht0037DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0037RespDto.setErorcd(Optional.ofNullable(xdht0037DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0037RespDto.setErortx(Optional.ofNullable(xdht0037DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0037DataResultDto.getCode())) {
                xdht0037DataRespDto = xdht0037DataResultDto.getData();
                xdht0037RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0037RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0037DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0037DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, e.getMessage());
            xdht0037RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0037RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0037RespDto.setDatasq(servsq);

        xdht0037RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0037.key, DscmsEnum.TRADE_CODE_XDHT0037.value, JSON.toJSONString(xdht0037RespDto));
        return xdht0037RespDto;
    }
}
