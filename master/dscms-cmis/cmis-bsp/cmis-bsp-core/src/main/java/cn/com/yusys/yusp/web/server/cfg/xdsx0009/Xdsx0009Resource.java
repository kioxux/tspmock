package cn.com.yusys.yusp.web.server.cfg.xdsx0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdsx0009.req.Xdsx0009ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdsx0009.resp.Xdsx0009RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdsx0009.req.Xdsx0009DataReqDto;
import cn.com.yusys.yusp.server.xdsx0009.resp.Xdsx0009DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户准入级别同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0009:客户准入级别同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0009Resource.class);
    @Autowired
    private DscmsCfgSxClientService dscmsCfgSxClientService;

    /**
     * 交易码：xdsx0009
     * 交易描述：客户准入级别同步
     *
     * @param xdsx0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdsx0009:客户准入级别同步")
    @PostMapping("/xdsx0009")
    //@Idempotent({"xdcasx0009", "#xdsx0009ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0009RespDto xdsx0009(@Validated @RequestBody Xdsx0009ReqDto xdsx0009ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009ReqDto));
        Xdsx0009DataReqDto xdsx0009DataReqDto = new Xdsx0009DataReqDto();// 请求Data： 客户准入级别同步
        Xdsx0009DataRespDto xdsx0009DataRespDto = new Xdsx0009DataRespDto();// 响应Data：客户准入级别同步
        Xdsx0009RespDto xdsx0009RespDto = new Xdsx0009RespDto();// 响应Dto：客户准入级别同步
        cn.com.yusys.yusp.dto.server.cfg.xdsx0009.req.Data reqData = new cn.com.yusys.yusp.dto.server.cfg.xdsx0009.req.Data(); // 请求Data：客户准入级别同步
        cn.com.yusys.yusp.dto.server.cfg.xdsx0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdsx0009.resp.Data();// 响应Data：客户准入级别同步
        try {
            // 从 xdsx0009ReqDto获取 reqData
            reqData = xdsx0009ReqDto.getData();
            // 将 reqData 拷贝到xdsx0009DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0009DataReqDto);
            // 调用服务
            ResultDto<Xdsx0009DataRespDto> xdsx0009DataResultDto = dscmsCfgSxClientService.xdsx0009(xdsx0009DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdsx0009RespDto.setErorcd(Optional.ofNullable(xdsx0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0009RespDto.setErortx(Optional.ofNullable(xdsx0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdsx0009RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0009DataResultDto.getCode())) {
                xdsx0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdsx0009DataRespDto = xdsx0009DataResultDto.getData();
                // 将 xdsx0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, e.getMessage());
            xdsx0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0009RespDto.setDatasq(servsq);
        xdsx0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0009.key, DscmsEnum.TRADE_CODE_XDSX0009.value, JSON.toJSONString(xdsx0009RespDto));
        return xdsx0009RespDto;
    }
}
