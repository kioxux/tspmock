package cn.com.yusys.yusp.web.client.esb.comstar.com001;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Com001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Data;
import cn.com.yusys.yusp.dto.client.esb.com001.req.List;
import cn.com.yusys.yusp.dto.client.esb.com001.resp.Com001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.comstar.req.Com001ReqService;
import cn.com.yusys.yusp.online.client.esb.comstar.req.Service;
import cn.com.yusys.yusp.online.client.esb.comstar.resp.Com001RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Api(tags = "BSP封装调用外部数据平台的接口处理类(com001)")
@RestController
@RequestMapping("/api/dscms2comstar")
public class Dscms2Com001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Com001Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：com001
     * 交易描述：额度同步
     *
     * @param com001ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/com001")
    protected @ResponseBody
    ResultDto<Com001RespDto> com001(@Validated @RequestBody Com001ReqDto com001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value, JSON.toJSONString(com001ReqDto));
        Com001ReqService com001ReqService = new Com001ReqService();
        Com001RespService com001RespService = new Com001RespService();
        cn.com.yusys.yusp.online.client.esb.comstar.req.List targetList = new cn.com.yusys.yusp.online.client.esb.comstar.req.List();
        cn.com.yusys.yusp.online.client.esb.comstar.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.comstar.resp.Service();
        Com001RespDto com001RespDto = new Com001RespDto();
        ResultDto<Com001RespDto> com001ResultDto = new ResultDto<Com001RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            Service reqService = Optional.ofNullable(com001ReqService.getService()).orElse(new Service());
            //  将com001ReqDto转换成reqService
            BeanUtils.copyProperties(com001ReqDto, reqService);
            Data data = Optional.ofNullable(com001ReqDto.getData()).orElse(new Data());
            if (CollectionUtils.nonEmpty(data.getList())) {
                java.util.List<List> list = data.getList();
                java.util.List<cn.com.yusys.yusp.online.client.esb.comstar.req.Record> recordList = new ArrayList<>();
                for (List l : list) {
                    cn.com.yusys.yusp.online.client.esb.comstar.req.Record record = new cn.com.yusys.yusp.online.client.esb.comstar.req.Record();
                    BeanUtils.copyProperties(l, record);
                    recordList.add(record);
                }
                targetList.setRecord(recordList);
                reqService.setList(targetList);

            }


            reqService.setPrcscd(EsbEnum.TRADE_CODE_COM001.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_COM.key, EsbEnum.SERVTP_COM.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_COM.key, EsbEnum.SERVTP_COM.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水

            com001ReqService.setService(reqService);
            // 将com001ReqService转换成com001ReqServiceMap
            Map com001ReqServiceMap = beanMapUtil.beanToMap(com001ReqService);
            context.put("tradeDataMap", com001ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_COM001.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            com001RespService = beanMapUtil.mapToBean(tradeDataMap, Com001RespService.class, Com001RespService.class);
            respService = com001RespService.getService();
            com001ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            com001ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Lsnp01RespDto
                BeanUtils.copyProperties(respService, com001RespDto);
                com001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                com001ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                com001ResultDto.setCode(EpbEnum.EPB099999.key);
                com001ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value, e.getMessage());
            com001ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            com001ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }

        //  将respService转换成Com001RespDto
        com001ResultDto.setData(com001RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_COM001.key, EsbEnum.TRADE_CODE_COM001.value, JSON.toJSONString(com001ResultDto));
        return com001ResultDto;
    }
}
