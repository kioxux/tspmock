package cn.com.yusys.yusp.web.server.biz.xdxw0037;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0037.req.Xdxw0037ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0037.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0037.resp.Xdxw0037RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.req.Xdxw0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0037.resp.Xdxw0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsPspQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询借款人是否存在未完成的贷后检查任务
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0037:查询借款人是否存在未完成的贷后检查任务")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0037Resource.class);
    @Autowired
    private DscmsPspQtClientService dscmsPspQtClientService;
    /**
     * 交易码：xdxw0037
     * 交易描述：查询借款人是否存在未完成的贷后检查任务
     *
     * @param xdxw0037ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询借款人是否存在未完成的贷后检查任务")
    @PostMapping("/xdxw0037")
    //@Idempotent({"xdcaxw0037", "#xdxw0037ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0037RespDto xdxw0037(@Validated @RequestBody Xdxw0037ReqDto xdxw0037ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037ReqDto));
        Xdxw0037DataReqDto xdxw0037DataReqDto = new Xdxw0037DataReqDto();// 请求Data： 查询借款人是否存在未完成的贷后检查任务
        Xdxw0037DataRespDto xdxw0037DataRespDto = new Xdxw0037DataRespDto();// 响应Data：查询借款人是否存在未完成的贷后检查任务
        cn.com.yusys.yusp.dto.server.biz.xdxw0037.req.Data reqData = null; // 请求Data：查询借款人是否存在未完成的贷后检查任务
        cn.com.yusys.yusp.dto.server.biz.xdxw0037.resp.Data respData = new Data();// 响应Data：查询借款人是否存在未完成的贷后检查任务
		Xdxw0037RespDto xdxw0037RespDto = new Xdxw0037RespDto();
		try {
            // 从 xdxw0037ReqDto获取 reqData
            reqData = xdxw0037ReqDto.getData();
            // 将 reqData 拷贝到xdxw0037DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0037DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037DataReqDto));
            ResultDto<Xdxw0037DataRespDto> xdxw0037DataResultDto = dscmsPspQtClientService.xdxw0037(xdxw0037DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0037RespDto.setErorcd(Optional.ofNullable(xdxw0037DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0037RespDto.setErortx(Optional.ofNullable(xdxw0037DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0037DataResultDto.getCode())) {
                xdxw0037DataRespDto = xdxw0037DataResultDto.getData();
                xdxw0037RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0037RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0037DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0037DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, e.getMessage());
            xdxw0037RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0037RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0037RespDto.setDatasq(servsq);

        xdxw0037RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0037.key, DscmsEnum.TRADE_CODE_XDXW0037.value, JSON.toJSONString(xdxw0037RespDto));
        return xdxw0037RespDto;
    }
}
