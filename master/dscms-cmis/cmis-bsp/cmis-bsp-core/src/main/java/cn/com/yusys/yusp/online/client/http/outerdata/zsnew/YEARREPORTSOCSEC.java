package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	年报-社会保险信息
@JsonPropertyOrder(alphabetic = true)
public class YEARREPORTSOCSEC implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ANCHEID")
    private String ANCHEID;//	年报ID
    @JsonProperty(value = "SO110")
    private String SO110;//	城镇职工基本养老保险参保人数
    @JsonProperty(value = "SO210")
    private String SO210;//	失业保险参保人数
    @JsonProperty(value = "SO310")
    private String SO310;//	职工基本医疗保险参保人数
    @JsonProperty(value = "SO410")
    private String SO410;//	工伤保险参保人数
    @JsonProperty(value = "SO510")
    private String SO510;//	生育保险参保人数
    @JsonProperty(value = "TOTALPAYMENT_SO110")
    private String TOTALPAYMENT_SO110;//	参加城镇职工基本养老保险本期实际缴费金额
    @JsonProperty(value = "TOTALPAYMENT_SO210")
    private String TOTALPAYMENT_SO210;//	参加失业保险本期实际缴费金额
    @JsonProperty(value = "TOTALPAYMENT_SO310")
    private String TOTALPAYMENT_SO310;//	参加职工基本医疗保险本期实际缴费金额
    @JsonProperty(value = "TOTALPAYMENT_SO410")
    private String TOTALPAYMENT_SO410;//	参加工伤保险本期实际缴费金额
    @JsonProperty(value = "TOTALPAYMENT_SO510")
    private String TOTALPAYMENT_SO510;//	参加生育保险本期实际缴费金额
    @JsonProperty(value = "TOTALWAGES_SO110")
    private String TOTALWAGES_SO110;//	单位参加城镇职工基本养老保险缴费基数
    @JsonProperty(value = "TOTALWAGES_SO210")
    private String TOTALWAGES_SO210;//	单位参加失业保险缴费基数
    @JsonProperty(value = "TOTALWAGES_SO310")
    private String TOTALWAGES_SO310;//	单位参加职工基本医疗保险缴费基数
    @JsonProperty(value = "TOTALWAGES_SO410")
    private String TOTALWAGES_SO410;//	单位参加工伤保险缴费基数
    @JsonProperty(value = "TOTALWAGES_SO510")
    private String TOTALWAGES_SO510;//	单位参加生育保险缴费基数
    @JsonProperty(value = "UNPAIDSOCIALINS_SO110")
    private String UNPAIDSOCIALINS_SO110;//	单位参加城镇职工基本养老保险累计欠缴金额
    @JsonProperty(value = "UNPAIDSOCIALINS_SO210")
    private String UNPAIDSOCIALINS_SO210;//	单位参加失业保险累计欠缴金额
    @JsonProperty(value = "UNPAIDSOCIALINS_SO310")
    private String UNPAIDSOCIALINS_SO310;//	单位参加职工基本医疗保险累计欠缴金额
    @JsonProperty(value = "UNPAIDSOCIALINS_SO410")
    private String UNPAIDSOCIALINS_SO410;//	参加工伤保险累计欠缴金额
    @JsonProperty(value = "UNPAIDSOCIALINS_SO510")
    private String UNPAIDSOCIALINS_SO510;//	单位参加生育保险累计欠缴金额

    @JsonIgnore
    public String getANCHEID() {
        return ANCHEID;
    }

    @JsonIgnore
    public void setANCHEID(String ANCHEID) {
        this.ANCHEID = ANCHEID;
    }

    @JsonIgnore
    public String getSO110() {
        return SO110;
    }

    @JsonIgnore
    public void setSO110(String SO110) {
        this.SO110 = SO110;
    }

    @JsonIgnore
    public String getSO210() {
        return SO210;
    }

    @JsonIgnore
    public void setSO210(String SO210) {
        this.SO210 = SO210;
    }

    @JsonIgnore
    public String getSO310() {
        return SO310;
    }

    @JsonIgnore
    public void setSO310(String SO310) {
        this.SO310 = SO310;
    }

    @JsonIgnore
    public String getSO410() {
        return SO410;
    }

    @JsonIgnore
    public void setSO410(String SO410) {
        this.SO410 = SO410;
    }

    @JsonIgnore
    public String getSO510() {
        return SO510;
    }

    @JsonIgnore
    public void setSO510(String SO510) {
        this.SO510 = SO510;
    }

    @JsonIgnore
    public String getTOTALPAYMENT_SO110() {
        return TOTALPAYMENT_SO110;
    }

    @JsonIgnore
    public void setTOTALPAYMENT_SO110(String TOTALPAYMENT_SO110) {
        this.TOTALPAYMENT_SO110 = TOTALPAYMENT_SO110;
    }

    @JsonIgnore
    public String getTOTALPAYMENT_SO210() {
        return TOTALPAYMENT_SO210;
    }

    @JsonIgnore
    public void setTOTALPAYMENT_SO210(String TOTALPAYMENT_SO210) {
        this.TOTALPAYMENT_SO210 = TOTALPAYMENT_SO210;
    }

    @JsonIgnore
    public String getTOTALPAYMENT_SO310() {
        return TOTALPAYMENT_SO310;
    }

    @JsonIgnore
    public void setTOTALPAYMENT_SO310(String TOTALPAYMENT_SO310) {
        this.TOTALPAYMENT_SO310 = TOTALPAYMENT_SO310;
    }

    @JsonIgnore
    public String getTOTALPAYMENT_SO410() {
        return TOTALPAYMENT_SO410;
    }

    @JsonIgnore
    public void setTOTALPAYMENT_SO410(String TOTALPAYMENT_SO410) {
        this.TOTALPAYMENT_SO410 = TOTALPAYMENT_SO410;
    }

    @JsonIgnore
    public String getTOTALPAYMENT_SO510() {
        return TOTALPAYMENT_SO510;
    }

    @JsonIgnore
    public void setTOTALPAYMENT_SO510(String TOTALPAYMENT_SO510) {
        this.TOTALPAYMENT_SO510 = TOTALPAYMENT_SO510;
    }

    @JsonIgnore
    public String getTOTALWAGES_SO110() {
        return TOTALWAGES_SO110;
    }

    @JsonIgnore
    public void setTOTALWAGES_SO110(String TOTALWAGES_SO110) {
        this.TOTALWAGES_SO110 = TOTALWAGES_SO110;
    }

    @JsonIgnore
    public String getTOTALWAGES_SO210() {
        return TOTALWAGES_SO210;
    }

    @JsonIgnore
    public void setTOTALWAGES_SO210(String TOTALWAGES_SO210) {
        this.TOTALWAGES_SO210 = TOTALWAGES_SO210;
    }

    @JsonIgnore
    public String getTOTALWAGES_SO310() {
        return TOTALWAGES_SO310;
    }

    @JsonIgnore
    public void setTOTALWAGES_SO310(String TOTALWAGES_SO310) {
        this.TOTALWAGES_SO310 = TOTALWAGES_SO310;
    }

    @JsonIgnore
    public String getTOTALWAGES_SO410() {
        return TOTALWAGES_SO410;
    }

    @JsonIgnore
    public void setTOTALWAGES_SO410(String TOTALWAGES_SO410) {
        this.TOTALWAGES_SO410 = TOTALWAGES_SO410;
    }

    @JsonIgnore
    public String getTOTALWAGES_SO510() {
        return TOTALWAGES_SO510;
    }

    @JsonIgnore
    public void setTOTALWAGES_SO510(String TOTALWAGES_SO510) {
        this.TOTALWAGES_SO510 = TOTALWAGES_SO510;
    }

    @JsonIgnore
    public String getUNPAIDSOCIALINS_SO110() {
        return UNPAIDSOCIALINS_SO110;
    }

    @JsonIgnore
    public void setUNPAIDSOCIALINS_SO110(String UNPAIDSOCIALINS_SO110) {
        this.UNPAIDSOCIALINS_SO110 = UNPAIDSOCIALINS_SO110;
    }

    @JsonIgnore
    public String getUNPAIDSOCIALINS_SO210() {
        return UNPAIDSOCIALINS_SO210;
    }

    @JsonIgnore
    public void setUNPAIDSOCIALINS_SO210(String UNPAIDSOCIALINS_SO210) {
        this.UNPAIDSOCIALINS_SO210 = UNPAIDSOCIALINS_SO210;
    }

    @JsonIgnore
    public String getUNPAIDSOCIALINS_SO310() {
        return UNPAIDSOCIALINS_SO310;
    }

    @JsonIgnore
    public void setUNPAIDSOCIALINS_SO310(String UNPAIDSOCIALINS_SO310) {
        this.UNPAIDSOCIALINS_SO310 = UNPAIDSOCIALINS_SO310;
    }

    @JsonIgnore
    public String getUNPAIDSOCIALINS_SO410() {
        return UNPAIDSOCIALINS_SO410;
    }

    @JsonIgnore
    public void setUNPAIDSOCIALINS_SO410(String UNPAIDSOCIALINS_SO410) {
        this.UNPAIDSOCIALINS_SO410 = UNPAIDSOCIALINS_SO410;
    }

    @JsonIgnore
    public String getUNPAIDSOCIALINS_SO510() {
        return UNPAIDSOCIALINS_SO510;
    }

    @JsonIgnore
    public void setUNPAIDSOCIALINS_SO510(String UNPAIDSOCIALINS_SO510) {
        this.UNPAIDSOCIALINS_SO510 = UNPAIDSOCIALINS_SO510;
    }

    @Override
    public String toString() {
        return "YEARREPORTSOCSEC{" +
                "ANCHEID='" + ANCHEID + '\'' +
                ", SO110='" + SO110 + '\'' +
                ", SO210='" + SO210 + '\'' +
                ", SO310='" + SO310 + '\'' +
                ", SO410='" + SO410 + '\'' +
                ", SO510='" + SO510 + '\'' +
                ", TOTALPAYMENT_SO110='" + TOTALPAYMENT_SO110 + '\'' +
                ", TOTALPAYMENT_SO210='" + TOTALPAYMENT_SO210 + '\'' +
                ", TOTALPAYMENT_SO310='" + TOTALPAYMENT_SO310 + '\'' +
                ", TOTALPAYMENT_SO410='" + TOTALPAYMENT_SO410 + '\'' +
                ", TOTALPAYMENT_SO510='" + TOTALPAYMENT_SO510 + '\'' +
                ", TOTALWAGES_SO110='" + TOTALWAGES_SO110 + '\'' +
                ", TOTALWAGES_SO210='" + TOTALWAGES_SO210 + '\'' +
                ", TOTALWAGES_SO310='" + TOTALWAGES_SO310 + '\'' +
                ", TOTALWAGES_SO410='" + TOTALWAGES_SO410 + '\'' +
                ", TOTALWAGES_SO510='" + TOTALWAGES_SO510 + '\'' +
                ", UNPAIDSOCIALINS_SO110='" + UNPAIDSOCIALINS_SO110 + '\'' +
                ", UNPAIDSOCIALINS_SO210='" + UNPAIDSOCIALINS_SO210 + '\'' +
                ", UNPAIDSOCIALINS_SO310='" + UNPAIDSOCIALINS_SO310 + '\'' +
                ", UNPAIDSOCIALINS_SO410='" + UNPAIDSOCIALINS_SO410 + '\'' +
                ", UNPAIDSOCIALINS_SO510='" + UNPAIDSOCIALINS_SO510 + '\'' +
                '}';
    }
}
