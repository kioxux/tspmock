package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 客户信息(CustomerInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
public class CustomerInfo {

    private List<CustomerInfoRecord> record; // 客户信息

    public List<CustomerInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<CustomerInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "CustomerInfo{" +
                "record=" + record +
                '}';
    }
}
