package cn.com.yusys.yusp.web.client.esb.xwywglpt.xwd008;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.req.Xwd008ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.resp.Xwd008RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.req.Xwd008ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.resp.Xwd008RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:新信贷同步用户账号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装新微贷平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Xwd008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xwd008Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xwd008
     * 交易描述：新信贷同步用户账号
     *
     * @param xwd008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xwd008:新信贷同步用户账号")
    @PostMapping("/xwd008")
    protected @ResponseBody
    ResultDto<Xwd008RespDto> xwd008(@Validated @RequestBody Xwd008ReqDto xwd008ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD008.key, EsbEnum.TRADE_CODE_XWD008.value, JSON.toJSONString(xwd008ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.resp.Service();
        Xwd008ReqService xwd008ReqService = new Xwd008ReqService();
        Xwd008RespService xwd008RespService = new Xwd008RespService();
        Xwd008RespDto xwd008RespDto = new Xwd008RespDto();
        ResultDto<Xwd008RespDto> xwd008ResultDto = new ResultDto<Xwd008RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xwd008ReqDto转换成reqService
            BeanUtils.copyProperties(xwd008ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XWD008.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWD.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWD.key);//    部门号
            xwd008ReqService.setService(reqService);
            // 将xwd008ReqService转换成xwd008ReqServiceMap
            Map xwd008ReqServiceMap = beanMapUtil.beanToMap(xwd008ReqService);
            context.put("tradeDataMap", xwd008ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD008.key, EsbEnum.TRADE_CODE_XWD008.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XWD008.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD008.key, EsbEnum.TRADE_CODE_XWD008.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xwd008RespService = beanMapUtil.mapToBean(tradeDataMap, Xwd008RespService.class, Xwd008RespService.class);
            respService = xwd008RespService.getService();

            xwd008ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xwd008ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, xwd008RespDto);
                xwd008ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xwd008ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xwd008ResultDto.setCode(EpbEnum.EPB099999.key);
                xwd008ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD008.key, EsbEnum.TRADE_CODE_XWD008.value, e.getMessage());
            xwd008ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xwd008ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xwd008ResultDto.setData(xwd008RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD008.key, EsbEnum.TRADE_CODE_XWD008.value, JSON.toJSONString(xwd008ResultDto));
        return xwd008ResultDto;
    }
}
