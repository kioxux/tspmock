package cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.resp;
/**
 * 响应Service：惠享贷规则审批申请接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午7:54:29
 */
public class  Service {

    private String ols_tran_no; // 交易流水号
    private String ols_date; // 交易日期
    private String app_no; // 申请流水号
    private String erorcd; // 响应码
    private String erortx; // 响应信息

    public String getOls_tran_no() {
        return ols_tran_no;
    }

    public void setOls_tran_no(String ols_tran_no) {
        this.ols_tran_no = ols_tran_no;
    }

    public String getOls_date() {
        return ols_date;
    }

    public void setOls_date(String ols_date) {
        this.ols_date = ols_date;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "ols_tran_no='" + ols_tran_no + '\'' +
                ", ols_date='" + ols_date + '\'' +
                ", app_no='" + app_no + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                '}';
    }
}