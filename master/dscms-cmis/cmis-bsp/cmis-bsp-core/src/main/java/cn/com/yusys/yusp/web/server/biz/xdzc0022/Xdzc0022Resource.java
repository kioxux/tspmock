package cn.com.yusys.yusp.web.server.biz.xdzc0022;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0022.req.Xdzc0022ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0022.resp.Xdzc0022RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0022.req.Xdzc0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0022.resp.Xdzc0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:发票补录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0022:发票补录")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0022Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0022
     * 交易描述：发票补录
     *
     * @param xdzc0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("发票补录")
    @PostMapping("/xdzc0022")
    //@Idempotent({"xdzc022", "#xdzc0022ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0022RespDto xdzc0022(@Validated @RequestBody Xdzc0022ReqDto xdzc0022ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, JSON.toJSONString(xdzc0022ReqDto));
        Xdzc0022DataReqDto xdzc0022DataReqDto = new Xdzc0022DataReqDto();// 请求Data： 发票补录
        Xdzc0022DataRespDto xdzc0022DataRespDto = new Xdzc0022DataRespDto();// 响应Data：发票补录
        Xdzc0022RespDto xdzc0022RespDto = new Xdzc0022RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0022.req.Data reqData = null; // 请求Data：发票补录
        cn.com.yusys.yusp.dto.server.biz.xdzc0022.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0022.resp.Data();// 响应Data：发票补录

        try {
            // 从 xdzc0022ReqDto获取 reqData
            reqData = xdzc0022ReqDto.getData();
            // 将 reqData 拷贝到xdzc0022DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0022DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, JSON.toJSONString(xdzc0022DataReqDto));
            ResultDto<Xdzc0022DataRespDto> xdzc0022DataResultDto = dscmsBizZcClientService.xdzc0022(xdzc0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, JSON.toJSONString(xdzc0022DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0022RespDto.setErorcd(Optional.ofNullable(xdzc0022DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0022RespDto.setErortx(Optional.ofNullable(xdzc0022DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0022DataResultDto.getCode())) {
                xdzc0022DataRespDto = xdzc0022DataResultDto.getData();
                xdzc0022RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0022RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0022DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0022DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, e.getMessage());
            xdzc0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0022RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0022RespDto.setDatasq(servsq);

        xdzc0022RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0022.key, DscmsEnum.TRADE_CODE_XDZC0022.value, JSON.toJSONString(xdzc0022RespDto));
        return xdzc0022RespDto;
    }
}
