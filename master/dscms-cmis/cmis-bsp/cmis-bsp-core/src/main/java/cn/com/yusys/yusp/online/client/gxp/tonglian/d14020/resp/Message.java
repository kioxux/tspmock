package cn.com.yusys.yusp.online.client.gxp.tonglian.d14020.resp;

import cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead;

/**
 * 响应Message：永久额度调整
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Message {
    private GxpRespHead head;
    private Body body;

    public GxpRespHead getHead() {
        return head;
    }

    public void setHead(GxpRespHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
