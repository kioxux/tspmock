package cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Record {
    @JsonProperty(value = "TransDate")
    private String TransDate;//冻结日期
    @JsonProperty(value = "SerialNo")
    private String SerialNo;//冻结流水号
    @JsonProperty(value = "TransName")
    private String TransName;//交易名称
    @JsonProperty(value = "ClientNo")
    private String ClientNo;//客户编号
    @JsonProperty(value = "AssetAcc")
    private String AssetAcc;//理财账号
    @JsonProperty(value = "TACodeTA")
    private String TACodeTA;//代码
    @JsonProperty(value = "TANameTA")
    private String TANameTA;//名称
    @JsonProperty(value = "PrdName")
    private String PrdName;//产品名称
    @JsonProperty(value = "Vol")
    private String Vol;//份额
    @JsonProperty(value = "FrozenCause")
    private String FrozenCause;//冻结原因
    @JsonProperty(value = "LawNo")
    private String LawNo;//法律文书号
    @JsonProperty(value = "OrgName")
    private String OrgName;//执行机构名称
    @JsonProperty(value = "EndDate")
    private String EndDate;//冻结截止日期
    @JsonProperty(value = "BankAcc")
    private String BankAcc;//银行账号
    @JsonProperty(value = "MarginAcc")
    private String MarginAcc;//保证金账号
    @JsonProperty(value = "PrdCode")// 字段
    private String PrdCode;//产品代码
    @JsonProperty(value = "FrozenBranch")
    private String FrozenBranch;//冻结机
    @JsonProperty(value = "CardType")// 构号
    private String CardType;//卡种
    @JsonProperty(value = "UnFrozenSerial")
    private String UnFrozenSerial;//解冻
    @JsonProperty(value = "PrdType")// 流水号
    private String PrdType;//业务类型
    @JsonProperty(value = "MeasureNo")
    private String MeasureNo;//措施序号
    @JsonProperty(value = "RightOwner")
    private String RightOwner;//权利人
    @JsonProperty(value = "RightAmt")
    private String RightAmt;//权利金额/数
    @JsonProperty(value = "RightOwnerAddress")// 额
    private String RightOwnerAddress;//
    @JsonProperty(value = "RightOwnerTel")// 权利人通讯地址
    private String RightOwnerTel;//权利人
    @JsonProperty(value = "RightType")// 联系方式
    private String RightType;//权利类型

    @JsonIgnore
    public String getTransDate() {
        return TransDate;
    }

    @JsonIgnore
    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    @JsonIgnore
    public String getSerialNo() {
        return SerialNo;
    }

    @JsonIgnore
    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    @JsonIgnore
    public String getTransName() {
        return TransName;
    }

    @JsonIgnore
    public void setTransName(String transName) {
        TransName = transName;
    }

    @JsonIgnore
    public String getClientNo() {
        return ClientNo;
    }

    @JsonIgnore
    public void setClientNo(String clientNo) {
        ClientNo = clientNo;
    }

    @JsonIgnore
    public String getAssetAcc() {
        return AssetAcc;
    }

    @JsonIgnore
    public void setAssetAcc(String assetAcc) {
        AssetAcc = assetAcc;
    }

    @JsonIgnore
    public String getTACodeTA() {
        return TACodeTA;
    }

    @JsonIgnore
    public void setTACodeTA(String TACodeTA) {
        this.TACodeTA = TACodeTA;
    }

    @JsonIgnore
    public String getTANameTA() {
        return TANameTA;
    }

    @JsonIgnore
    public void setTANameTA(String TANameTA) {
        this.TANameTA = TANameTA;
    }

    @JsonIgnore
    public String getPrdName() {
        return PrdName;
    }

    @JsonIgnore
    public void setPrdName(String prdName) {
        PrdName = prdName;
    }

    @JsonIgnore
    public String getVol() {
        return Vol;
    }

    @JsonIgnore
    public void setVol(String vol) {
        Vol = vol;
    }

    @JsonIgnore
    public String getFrozenCause() {
        return FrozenCause;
    }

    @JsonIgnore
    public void setFrozenCause(String frozenCause) {
        FrozenCause = frozenCause;
    }

    @JsonIgnore
    public String getLawNo() {
        return LawNo;
    }

    @JsonIgnore
    public void setLawNo(String lawNo) {
        LawNo = lawNo;
    }

    @JsonIgnore
    public String getOrgName() {
        return OrgName;
    }

    @JsonIgnore
    public void setOrgName(String orgName) {
        OrgName = orgName;
    }

    @JsonIgnore
    public String getEndDate() {
        return EndDate;
    }

    @JsonIgnore
    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    @JsonIgnore
    public String getBankAcc() {
        return BankAcc;
    }

    @JsonIgnore
    public void setBankAcc(String bankAcc) {
        BankAcc = bankAcc;
    }

    @JsonIgnore
    public String getMarginAcc() {
        return MarginAcc;
    }

    @JsonIgnore
    public void setMarginAcc(String marginAcc) {
        MarginAcc = marginAcc;
    }

    @JsonIgnore
    public String getPrdCode() {
        return PrdCode;
    }

    @JsonIgnore
    public void setPrdCode(String prdCode) {
        PrdCode = prdCode;
    }

    @JsonIgnore
    public String getFrozenBranch() {
        return FrozenBranch;
    }

    @JsonIgnore
    public void setFrozenBranch(String frozenBranch) {
        FrozenBranch = frozenBranch;
    }

    @JsonIgnore
    public String getCardType() {
        return CardType;
    }

    @JsonIgnore
    public void setCardType(String cardType) {
        CardType = cardType;
    }

    @JsonIgnore
    public String getUnFrozenSerial() {
        return UnFrozenSerial;
    }

    @JsonIgnore
    public void setUnFrozenSerial(String unFrozenSerial) {
        UnFrozenSerial = unFrozenSerial;
    }

    @JsonIgnore
    public String getPrdType() {
        return PrdType;
    }

    @JsonIgnore
    public void setPrdType(String prdType) {
        PrdType = prdType;
    }

    @JsonIgnore
    public String getMeasureNo() {
        return MeasureNo;
    }

    @JsonIgnore
    public void setMeasureNo(String measureNo) {
        MeasureNo = measureNo;
    }

    @JsonIgnore
    public String getRightOwner() {
        return RightOwner;
    }

    @JsonIgnore
    public void setRightOwner(String rightOwner) {
        RightOwner = rightOwner;
    }

    @JsonIgnore
    public String getRightAmt() {
        return RightAmt;
    }

    @JsonIgnore
    public void setRightAmt(String rightAmt) {
        RightAmt = rightAmt;
    }

    @JsonIgnore
    public String getRightOwnerAddress() {
        return RightOwnerAddress;
    }

    @JsonIgnore
    public void setRightOwnerAddress(String rightOwnerAddress) {
        RightOwnerAddress = rightOwnerAddress;
    }

    @JsonIgnore
    public String getRightOwnerTel() {
        return RightOwnerTel;
    }

    @JsonIgnore
    public void setRightOwnerTel(String rightOwnerTel) {
        RightOwnerTel = rightOwnerTel;
    }

    @JsonIgnore
    public String getRightType() {
        return RightType;
    }

    @JsonIgnore
    public void setRightType(String rightType) {
        RightType = rightType;
    }

    @Override
    public String toString() {
        return "Record{" +
                "TransDate='" + TransDate + '\'' +
                ", SerialNo='" + SerialNo + '\'' +
                ", TransName='" + TransName + '\'' +
                ", ClientNo='" + ClientNo + '\'' +
                ", AssetAcc='" + AssetAcc + '\'' +
                ", TACodeTA='" + TACodeTA + '\'' +
                ", TANameTA='" + TANameTA + '\'' +
                ", PrdName='" + PrdName + '\'' +
                ", Vol='" + Vol + '\'' +
                ", FrozenCause='" + FrozenCause + '\'' +
                ", LawNo='" + LawNo + '\'' +
                ", OrgName='" + OrgName + '\'' +
                ", EndDate='" + EndDate + '\'' +
                ", BankAcc='" + BankAcc + '\'' +
                ", MarginAcc='" + MarginAcc + '\'' +
                ", PrdCode='" + PrdCode + '\'' +
                ", FrozenBranch='" + FrozenBranch + '\'' +
                ", CardType='" + CardType + '\'' +
                ", UnFrozenSerial='" + UnFrozenSerial + '\'' +
                ", PrdType='" + PrdType + '\'' +
                ", MeasureNo='" + MeasureNo + '\'' +
                ", RightOwner='" + RightOwner + '\'' +
                ", RightAmt='" + RightAmt + '\'' +
                ", RightOwnerAddress='" + RightOwnerAddress + '\'' +
                ", RightOwnerTel='" + RightOwnerTel + '\'' +
                ", RightType='" + RightType + '\'' +
                '}';
    }
}
