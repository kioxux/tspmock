package cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 */
public class Dylist {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Dylist{" +
                "record=" + record +
                '}';
    }
}
