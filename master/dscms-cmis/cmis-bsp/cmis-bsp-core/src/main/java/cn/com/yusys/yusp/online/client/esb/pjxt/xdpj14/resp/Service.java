package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.resp;

/**
 * 响应Service：信贷签约通知
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erortx;//交易返回信息
    private String erorcd;//交易返回代码

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erortx='" + erortx + '\'' +
                ", erorcd='" + erorcd + '\'' +
                '}';
    }
}
