package cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 14:19
 * @since 2021/8/10 14:19
 */
public class Dblist {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Dblist{" +
                "record=" + record +
                '}';
    }
}
