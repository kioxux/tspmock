package cn.com.yusys.yusp.web.server.biz.xdtz0022;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0022.req.Xdtz0022ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0022.resp.Xdtz0022RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0022.req.Xdtz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0022.resp.Xdtz0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:保证金台账入账
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0022:保证金台账入账")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0022Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0022
     * 交易描述：保证金台账入账
     *
     * @param xdtz0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金台账入账")
    @PostMapping("/xdtz0022")
    //@Idempotent({"xdcatz0022", "#xdtz0022ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0022RespDto xdtz0022(@Validated @RequestBody Xdtz0022ReqDto xdtz0022ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022ReqDto));
        Xdtz0022DataReqDto xdtz0022DataReqDto = new Xdtz0022DataReqDto();// 请求Data： 保证金台账入账
        Xdtz0022DataRespDto xdtz0022DataRespDto = new Xdtz0022DataRespDto();// 响应Data：保证金台账入账
        Xdtz0022RespDto xdtz0022RespDto = new Xdtz0022RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0022.req.Data reqData = null; // 请求Data：保证金台账入账
        cn.com.yusys.yusp.dto.server.biz.xdtz0022.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0022.resp.Data();// 响应Data：保证金台账入账
        try {
            // 从 xdtz0022ReqDto获取 reqData
            reqData = xdtz0022ReqDto.getData();
            // 将 reqData 拷贝到xdtz0022DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0022DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataReqDto));
            ResultDto<Xdtz0022DataRespDto> xdtz0022DataResultDto = dscmsBizTzClientService.xdtz0022(xdtz0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0022RespDto.setErorcd(Optional.ofNullable(xdtz0022DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0022RespDto.setErortx(Optional.ofNullable(xdtz0022DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0022DataResultDto.getCode())) {
                xdtz0022DataRespDto = xdtz0022DataResultDto.getData();
                xdtz0022RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0022RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0022DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0022DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, e.getMessage());
            xdtz0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0022RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0022RespDto.setDatasq(servsq);

        xdtz0022RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0022.key, DscmsEnum.TRADE_CODE_XDTZ0022.value, JSON.toJSONString(xdtz0022RespDto));
        return xdtz0022RespDto;
    }
}
