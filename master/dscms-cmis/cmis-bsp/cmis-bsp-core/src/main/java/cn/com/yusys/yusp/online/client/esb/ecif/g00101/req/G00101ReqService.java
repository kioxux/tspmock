package cn.com.yusys.yusp.online.client.esb.ecif.g00101.req;

/**
 * 请求Service：对公客户综合信息查询
 *
 * @author zhangpeng
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class G00101ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }


}
