package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 企业历史变更信息
 */
@JsonPropertyOrder(alphabetic = true)
public class ALTER implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ALTAF")
    private String ALTAF;//变更后内容
    @JsonProperty(value = "ALTBE")
    private String ALTBE;//变更前内容
    @JsonProperty(value = "ALTDATE")
    private String ALTDATE;//变更日期
    @JsonProperty(value = "ALTITEM")
    private String ALTITEM;//变更事项
    @JsonProperty(value = "id")
    private String id;
    @JsonProperty(value = "baseId")
    private String baseId;

    @JsonIgnore
    public String getALTAF() {
        return ALTAF;
    }

    @JsonIgnore
    public void setALTAF(String ALTAF) {
        this.ALTAF = ALTAF;
    }

    @JsonIgnore
    public String getALTBE() {
        return ALTBE;
    }

    @JsonIgnore
    public void setALTBE(String ALTBE) {
        this.ALTBE = ALTBE;
    }

    @JsonIgnore
    public String getALTDATE() {
        return ALTDATE;
    }

    @JsonIgnore
    public void setALTDATE(String ALTDATE) {
        this.ALTDATE = ALTDATE;
    }

    @JsonIgnore
    public String getALTITEM() {
        return ALTITEM;
    }

    @JsonIgnore
    public void setALTITEM(String ALTITEM) {
        this.ALTITEM = ALTITEM;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    @Override
    public String toString() {
        return "ALTER{" +
                "ALTAF='" + ALTAF + '\'' +
                ", ALTBE='" + ALTBE + '\'' +
                ", ALTDATE='" + ALTDATE + '\'' +
                ", ALTITEM='" + ALTITEM + '\'' +
                ", id='" + id + '\'' +
                ", baseId='" + baseId + '\'' +
                '}';
    }
}
