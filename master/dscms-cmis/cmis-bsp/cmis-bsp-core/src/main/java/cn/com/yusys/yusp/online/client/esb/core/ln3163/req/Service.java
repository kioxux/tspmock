package cn.com.yusys.yusp.online.client.esb.core.ln3163.req;

import java.math.BigDecimal;

/**
 * 请求Service：资产证券化处理
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String zichculi;//资产处理类型
    private String xieybhao;//协议编号
    private String xieyimch;//协议名称
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String huobdhao;//货币代号
    private BigDecimal xieyshje;//协议实际金额
    private String fengbriq;//封包日期
    private String jiebriqi;//解包日期
    private String ruchiriq;//入池日期
    private String huigriqi;//回购日期
    private String zchzhtai;//资产处理状态

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getZichculi() {
        return zichculi;
    }

    public void setZichculi(String zichculi) {
        this.zichculi = zichculi;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getXieyshje() {
        return xieyshje;
    }

    public void setXieyshje(BigDecimal xieyshje) {
        this.xieyshje = xieyshje;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getJiebriqi() {
        return jiebriqi;
    }

    public void setJiebriqi(String jiebriqi) {
        this.jiebriqi = jiebriqi;
    }

    public String getRuchiriq() {
        return ruchiriq;
    }

    public void setRuchiriq(String ruchiriq) {
        this.ruchiriq = ruchiriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public String getZchzhtai() {
        return zchzhtai;
    }

    public void setZchzhtai(String zchzhtai) {
        this.zchzhtai = zchzhtai;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zichculi='" + zichculi + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", xieyshje=" + xieyshje +
                ", fengbriq='" + fengbriq + '\'' +
                ", jiebriqi='" + jiebriqi + '\'' +
                ", ruchiriq='" + ruchiriq + '\'' +
                ", huigriqi='" + huigriqi + '\'' +
                ", zchzhtai='" + zchzhtai + '\'' +
                '}';
    }
}
