package cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询质押物信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String name;//名称
    private String amt;//价值
    private BigDecimal amount;//数量
    private String stock_no;//代码/号码
    private String end_date;//到期日
    private String cur_type;//币种

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getStock_no() {
        return stock_no;
    }

    public void setStock_no(String stock_no) {
        this.stock_no = stock_no;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", name='" + name + '\'' +
                ", amt='" + amt + '\'' +
                ", amount=" + amount +
                ", stock_no='" + stock_no + '\'' +
                ", end_date='" + end_date + '\'' +
                ", cur_type='" + cur_type + '\'' +
                '}';
    }
}
