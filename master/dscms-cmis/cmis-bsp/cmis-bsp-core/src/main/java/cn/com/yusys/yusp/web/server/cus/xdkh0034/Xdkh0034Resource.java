package cn.com.yusys.yusp.web.server.cus.xdkh0034;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0034.req.Xdkh0034ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0034.resp.Xdkh0034RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0034.req.Xdkh0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0034.resp.Xdkh0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询企业在我行客户评级信息
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDKH0034:客户评级结果同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0034Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0034
     * 交易描述：查询企业在我行客户评级信息
     *
     * @param xdkh0034ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询企业在我行客户评级信息")
    @PostMapping("/xdkh0034")
    //@Idempotent({"xdcakh0034", "#xdkh0034ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0034RespDto xdkh0034(@Validated @RequestBody Xdkh0034ReqDto xdkh0034ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, JSON.toJSONString(xdkh0034ReqDto));

        //  此处包导入待确定 开始
        Xdkh0034DataReqDto xdkh0034DataReqDto = new Xdkh0034DataReqDto();// 请求Data： 客户评级结果同步
        Xdkh0034DataRespDto xdkh0034DataRespDto = new Xdkh0034DataRespDto();// 响应Data：客户评级结果同步
        //  此处包导入待确定 结束
        Xdkh0034RespDto xdkh0034RespDto = new Xdkh0034RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0034.req.Data reqData = null; // 请求Data：客户评级结果同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0034.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0034.resp.Data();// 响应Data：客户评级结果同步
        try {
            // 从 xdkh0034ReqDto获取 reqData
            reqData = xdkh0034ReqDto.getData();
            // 将 reqData 拷贝到xdkh0034DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0034DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, JSON.toJSONString(xdkh0034DataReqDto));
            ResultDto<Xdkh0034DataRespDto> xdkh0034DataResultDto = dscmsCusClientService.xdkh0034(xdkh0034DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, JSON.toJSONString(xdkh0034DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0034RespDto.setErorcd(Optional.ofNullable(xdkh0034DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0034RespDto.setErortx(Optional.ofNullable(xdkh0034DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0034DataResultDto.getCode())) {
                xdkh0034DataRespDto = xdkh0034DataResultDto.getData();
                xdkh0034RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0034RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0034DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0034DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, e.getMessage());
            xdkh0034RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0034RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0034RespDto.setDatasq(servsq);

        xdkh0034RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0034.key, DscmsEnum.TRADE_CODE_XDKH0034.value, JSON.toJSONString(xdkh0034RespDto));
        return xdkh0034RespDto;
    }
}