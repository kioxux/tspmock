package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.req;

/**
 * 请求Service：查询不动产信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xddb01ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
