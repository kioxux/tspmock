package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdktxzh.Record;

import java.util.List;

// 贷款多贴息账户

public class Lstdktxzh_ARRAY {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdktxzh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdktxzh_ARRAY{" +
                "record=" + record +
                '}';
    }
}
