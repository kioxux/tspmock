package cn.com.yusys.yusp.web.client.esb.core.ln3160;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3160.Ln3160ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3160.Ln3160RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3160.req.Ln3160ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.Ln3160RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3160)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3160Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3160Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3160
     * 交易描述：资产证券化信息查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3160:资产证券化信息查询")
    @PostMapping("/ln3160")
    protected @ResponseBody
    ResultDto<Ln3160RespDto> ln3160(@Validated @RequestBody Ln3160ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3160.key, EsbEnum.TRADE_CODE_LN3160.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3160.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3160.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.Service();
        Ln3160ReqService ln3160ReqService = new Ln3160ReqService();
        Ln3160RespService ln3160RespService = new Ln3160RespService();
        Ln3160RespDto ln3160RespDto = new Ln3160RespDto();
        ResultDto<Ln3160RespDto> ln3160ResultDto = new ResultDto<Ln3160RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3160ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3160.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3160ReqService.setService(reqService);
            // 将ln3160ReqService转换成ln3160ReqServiceMap
            Map ln3160ReqServiceMap = beanMapUtil.beanToMap(ln3160ReqService);
            context.put("tradeDataMap", ln3160ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3160.key, EsbEnum.TRADE_CODE_LN3160.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3160.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3160.key, EsbEnum.TRADE_CODE_LN3160.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3160RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3160RespService.class, Ln3160RespService.class);
            respService = ln3160RespService.getService();

            ln3160ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3160ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3160RespDto
                BeanUtils.copyProperties(respService, ln3160RespDto);

                cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz.LstKlnb_dkzrhz ln3160RespLstKlnb_dkzrhz = Optional.ofNullable(respService.getLstKlnb_dkzrhz())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz.LstKlnb_dkzrhz());
                respService.setLstKlnb_dkzrhz(ln3160RespLstKlnb_dkzrhz);
                if (CollectionUtils.nonEmpty(respService.getLstKlnb_dkzrhz().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz.Record> lstKlnb_dkzrhzRecordList =
                            Optional.ofNullable(respService.getLstKlnb_dkzrhz().getRecord()).orElseGet(() -> new ArrayList<Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkzrhz> lstKlnb_dkzrhzList =
                            lstKlnb_dkzrhzRecordList.parallelStream().map(lstKlnb_dkzrhzRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkzrhz lstKlnb_dkzrhz = new cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkzrhz();
                                BeanUtils.copyProperties(lstKlnb_dkzrhzRecord, lstKlnb_dkzrhz);
                                return lstKlnb_dkzrhz;
                            }).collect(Collectors.toList());
                    ln3160RespDto.setLstKlnb_dkzrhz(lstKlnb_dkzrhzList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkxypc.LstKlnb_dkxypc ln3160RespLstKlnb_dkxypc = Optional.ofNullable(respService.getLstKlnb_dkxypc())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkxypc.LstKlnb_dkxypc());
                respService.setLstKlnb_dkxypc(ln3160RespLstKlnb_dkxypc);
                if (CollectionUtils.nonEmpty(respService.getLstKlnb_dkxypc().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkxypc.Record> lstKlnb_dkxypcRecordList =
                            Optional.ofNullable(respService.getLstKlnb_dkxypc().getRecord()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkxypc.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkxypc> lstKlnb_dkxypcList =
                            lstKlnb_dkxypcRecordList.parallelStream().map(lstKlnb_dkxypcRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkxypc lstKlnb_dkxypc = new cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkxypc();
                                BeanUtils.copyProperties(lstKlnb_dkxypcRecord, lstKlnb_dkxypc);
                                return lstKlnb_dkxypc;
                            }).collect(Collectors.toList());
                    ln3160RespDto.setLstKlnb_dkxypc(lstKlnb_dkxypcList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrjj.LstKlnb_dkzrjj ln3160RespLstKlnb_dkzrjj = Optional.ofNullable(respService.getLstKlnb_dkzrjj())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrjj.LstKlnb_dkzrjj());
                respService.setLstKlnb_dkzrjj(ln3160RespLstKlnb_dkzrjj);
                if (CollectionUtils.nonEmpty(respService.getLstKlnb_dkzrjj().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrjj.Record> lstKlnb_dkzrjjRecordList =
                            Optional.ofNullable(respService.getLstKlnb_dkzrjj().getRecord()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrjj.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkzrjj> lstKlnb_dkzrjjList =
                            lstKlnb_dkzrjjRecordList.parallelStream().map(lstKlnb_dkzrjjRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkzrjj lstKlnb_dkzrjj = new cn.com.yusys.yusp.dto.client.esb.core.ln3160.LstKlnb_dkzrjj();
                                BeanUtils.copyProperties(lstKlnb_dkzrjjRecord, lstKlnb_dkzrjj);
                                return lstKlnb_dkzrjj;
                            }).collect(Collectors.toList());
                    ln3160RespDto.setLstKlnb_dkzrjj(lstKlnb_dkzrjjList);
                }

                ln3160ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3160ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3160ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3160ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3160.key, EsbEnum.TRADE_CODE_LN3160.value, e.getMessage());
            ln3160ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3160ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3160ResultDto.setData(ln3160RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3160.key, EsbEnum.TRADE_CODE_LN3160.value, JSON.toJSONString(ln3160ResultDto));
        return ln3160ResultDto;
    }
}
