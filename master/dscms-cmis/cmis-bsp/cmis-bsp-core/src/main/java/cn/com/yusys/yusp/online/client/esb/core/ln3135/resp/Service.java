package cn.com.yusys.yusp.online.client.esb.core.ln3135.resp;

import java.util.List;

/**
 * 响应Service：通过贷款借据号和贷款账号查询贷款期供计划
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private java.util.List<Listnm> listnms;//贷款定制期供计划表

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public List<Listnm> getListnms() {
        return listnms;
    }

    public void setListnms(List<Listnm> listnms) {
        this.listnms = listnms;
    }
}
