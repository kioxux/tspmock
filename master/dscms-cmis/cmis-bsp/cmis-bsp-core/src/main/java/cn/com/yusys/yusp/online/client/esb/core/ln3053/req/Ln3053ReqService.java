package cn.com.yusys.yusp.online.client.esb.core.ln3053.req;

/**
 * 请求Service：还款计划调整
 * @author code-generator
 * @version 1.0             
 */      
public class Ln3053ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
