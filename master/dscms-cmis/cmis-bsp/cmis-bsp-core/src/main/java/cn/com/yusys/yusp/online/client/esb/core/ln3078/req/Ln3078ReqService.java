package cn.com.yusys.yusp.online.client.esb.core.ln3078.req;

/**
 * 请求Service：贷款机构变更
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3078ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3078ReqService{" +
                "service=" + service +
                '}';
    }
}
