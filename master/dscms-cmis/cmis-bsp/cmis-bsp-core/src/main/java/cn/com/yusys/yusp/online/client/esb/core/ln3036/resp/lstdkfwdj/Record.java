package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfwdj;

public class Record {
    private String fuwuzidu;//服务字段

    private String fuwudaim;//服务代码

    private String fuwuzdmc;//服务字段名称

    private String fuwudmmc;//服务代码名称

    public String getFuwuzidu() {
        return fuwuzidu;
    }

    public void setFuwuzidu(String fuwuzidu) {
        this.fuwuzidu = fuwuzidu;
    }

    public String getFuwudaim() {
        return fuwudaim;
    }

    public void setFuwudaim(String fuwudaim) {
        this.fuwudaim = fuwudaim;
    }

    public String getFuwuzdmc() {
        return fuwuzdmc;
    }

    public void setFuwuzdmc(String fuwuzdmc) {
        this.fuwuzdmc = fuwuzdmc;
    }

    public String getFuwudmmc() {
        return fuwudmmc;
    }

    public void setFuwudmmc(String fuwudmmc) {
        this.fuwudmmc = fuwudmmc;
    }

    @Override
    public String toString() {
        return "Record{" +
                "fuwuzidu='" + fuwuzidu + '\'' +
                ", fuwudaim='" + fuwudaim + '\'' +
                ", fuwuzdmc='" + fuwuzdmc + '\'' +
                ", fuwudmmc='" + fuwudmmc + '\'' +
                '}';
    }
}
