package cn.com.yusys.yusp.web.client.esb.core.ln3073;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3073.req.Ln3073ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3073.resp.Ln3073RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3073.req.Ln3073ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3073.resp.Ln3073RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3073)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3073Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3073Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3073
     * 交易描述：用于贷款定制期供计划的修改调整
     *
     * @param ln3073ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3073:用于贷款定制期供计划的修改调整")
    @PostMapping("/ln3073")
    protected @ResponseBody
    ResultDto<Ln3073RespDto> ln3073(@Validated @RequestBody Ln3073ReqDto ln3073ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3073.key, EsbEnum.TRADE_CODE_LN3073.value, JSON.toJSONString(ln3073ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3073.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3073.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3073.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3073.resp.Service();
        Ln3073ReqService ln3073ReqService = new Ln3073ReqService();
        Ln3073RespService ln3073RespService = new Ln3073RespService();
        Ln3073RespDto ln3073RespDto = new Ln3073RespDto();
        ResultDto<Ln3073RespDto> ln3073ResultDto = new ResultDto<Ln3073RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3073ReqDto转换成reqService
            BeanUtils.copyProperties(ln3073ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3073.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3073ReqService.setService(reqService);
            // 将ln3073ReqService转换成ln3073ReqServiceMap
            Map ln3073ReqServiceMap = beanMapUtil.beanToMap(ln3073ReqService);
            context.put("tradeDataMap", ln3073ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3073.key, EsbEnum.TRADE_CODE_LN3073.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3073.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3073.key, EsbEnum.TRADE_CODE_LN3073.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3073RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3073RespService.class, Ln3073RespService.class);
            respService = ln3073RespService.getService();
            //  将respService转换成Ln3073RespDto
            BeanUtils.copyProperties(respService, ln3073RespDto);
            ln3073ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3073ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, ln3073RespDto);
                ln3073ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3073ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3073ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3073ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3073.key, EsbEnum.TRADE_CODE_LN3073.value, e.getMessage());
            ln3073ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3073ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3073ResultDto.setData(ln3073RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3073.key, EsbEnum.TRADE_CODE_LN3073.value, JSON.toJSONString(ln3073ResultDto));
        return ln3073ResultDto;
    }

}
