package cn.com.yusys.yusp.web.server.biz.xdxw0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0013.req.Xdxw0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0013.resp.Xdxw0013RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0013.req.Xdxw0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0013.resp.Xdxw0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷共借人、合同信息查询
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms")
@Api(tags = "XDXW0013:优企贷共借人、合同信息查询")
public class Xdxw0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0013Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0013
     * 交易描述：优企贷共借人、合同信息查询
     *
     * @param xdxw0013ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdxw0013")
    //@Idempotent({"xdcaxw0013", "#xdxw0013ReqDto.datasq"})
    protected @ResponseBody
    @ApiOperation("优企贷共借人、合同信息查询")
    Xdxw0013RespDto xdxw0013(@Validated @RequestBody Xdxw0013ReqDto xdxw0013ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(xdxw0013ReqDto));
        Xdxw0013DataReqDto xdxw0013DataReqDto = new Xdxw0013DataReqDto();// 请求Data： 优企贷共借人、合同信息查询
        Xdxw0013RespDto xdxw0013RespDto = new Xdxw0013RespDto();//响应Dto：优企贷共借人、合同信息查询
        Xdxw0013DataRespDto xdxw0013DataRespDto = new Xdxw0013DataRespDto();// 响应Data：优企贷共借人、合同信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0013.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdxw0013.req.Data(); // 请求Data：优企贷共借人、合同信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0013.resp.Data();// 响应Data：优企贷共借人、合同信息查询
        try {
            // 从 xdxw0013ReqDto获取 reqData
            reqData = xdxw0013ReqDto.getData();
            // 将 reqData 拷贝到xdxw0013DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0013DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(xdxw0013DataReqDto));
            ResultDto<Xdxw0013DataRespDto> xdxw0013DataResultDto = dscmsBizXwClientService.xdxw0013(xdxw0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(xdxw0013DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0013RespDto.setErorcd(Optional.ofNullable(xdxw0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0013RespDto.setErortx(Optional.ofNullable(xdxw0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0013DataResultDto.getCode())) {
                xdxw0013DataRespDto = xdxw0013DataResultDto.getData();
                xdxw0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0013DataRespDto, respData);
            }
        } catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, e.getMessage());
            xdxw0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0013RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, e.getMessage());
            xdxw0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0013RespDto.setDatasq(servsq);
        xdxw0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0013.key, DscmsEnum.TRADE_CODE_XDXW0013.value, JSON.toJSONString(xdxw0013RespDto));
        return xdxw0013RespDto;
    }
}
