package cn.com.yusys.yusp.web.server.biz.xdtz0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0013.req.Xdtz0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0013.resp.Xdtz0013RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0013.req.Xdtz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0013.resp.Xdtz0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0013:查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0013Resource.class);
	@Autowired
	private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0013
     * 交易描述：查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
     *
     * @param xdtz0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）")
    @PostMapping("/xdtz0013")
    //@Idempotent({"xdcatz0013", "#xdtz0013ReqDto.datasq"})
    protected @ResponseBody
	Xdtz0013RespDto xdtz0013(@Validated @RequestBody Xdtz0013ReqDto xdtz0013ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013ReqDto));
        Xdtz0013DataReqDto xdtz0013DataReqDto = new Xdtz0013DataReqDto();// 请求Data： 查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
        Xdtz0013DataRespDto xdtz0013DataRespDto = new Xdtz0013DataRespDto();// 响应Data：查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
		Xdtz0013RespDto xdtz0013RespDto=new Xdtz0013RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0013.req.Data reqData = null; // 请求Data：查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
        cn.com.yusys.yusp.dto.server.biz.xdtz0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0013.resp.Data();// 响应Data：查询小微借据余额、结清日期、贷款类型（区分经营贷、消费贷等）
        try {
            // 从 xdtz0013ReqDto获取 reqData
            reqData = xdtz0013ReqDto.getData();
            // 将 reqData 拷贝到xdtz0013DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013DataReqDto));
            ResultDto<Xdtz0013DataRespDto> xdtz0013DataResultDto = dscmsBizTzClientService.xdtz0013(xdtz0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0013RespDto.setErorcd(Optional.ofNullable(xdtz0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0013RespDto.setErortx(Optional.ofNullable(xdtz0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0013DataResultDto.getCode())) {
                xdtz0013DataRespDto = xdtz0013DataResultDto.getData();
                xdtz0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0013DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, e.getMessage());
            xdtz0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0013RespDto.setDatasq(servsq);

        xdtz0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0013.key, DscmsEnum.TRADE_CODE_XDTZ0013.value, JSON.toJSONString(xdtz0013RespDto));
        return xdtz0013RespDto;
    }
}
