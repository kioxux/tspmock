package cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp;

/**
 * 响应Service：查找指定数据日期的放款合约明细记录历史表（利翃）一览
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd04RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd04RespService{" +
                "service=" + service +
                '}';
    }
}
