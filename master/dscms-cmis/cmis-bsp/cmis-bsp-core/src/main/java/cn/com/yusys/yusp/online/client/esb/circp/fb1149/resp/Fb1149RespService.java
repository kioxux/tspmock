package cn.com.yusys.yusp.online.client.esb.circp.fb1149.resp;

/**
 * 响应Service：无还本续贷借据更新
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1149RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
