package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.resp;

/**
 * 响应Service：查询不动产信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xddb02RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xddb02RespService{" +
                "service=" + service +
                '}';
    }
}
