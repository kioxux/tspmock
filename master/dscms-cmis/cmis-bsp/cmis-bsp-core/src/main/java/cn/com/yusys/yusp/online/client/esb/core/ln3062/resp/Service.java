package cn.com.yusys.yusp.online.client.esb.core.ln3062.resp;

import java.math.BigDecimal;

/**
 * 响应Service：资产转让借据维护
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {

    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String jiaoyirq;//交易日期
    private String jiaoyijg;//交易机构
    private String jiaoyigy;//交易柜员
    private String jiaoyils;//交易流水
    private String xieybhao;//协议编号
    private String xieyimch;//协议名称
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String zcrfshii;//资产融通方式
    private BigDecimal zcrtbili;//资产融通比例
    private BigDecimal xieyshje;//协议实际金额
    private BigDecimal xieyilil;//协议利率
    private String beizhuxx;//备注
    private String beizhuuu;//备注信息
    private cn.com.yusys.yusp.online.client.esb.core.ln3062.resp.LstKlnb_dkzrjj lstKlnb_dkzrjj;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getZcrfshii() {
        return zcrfshii;
    }

    public void setZcrfshii(String zcrfshii) {
        this.zcrfshii = zcrfshii;
    }

    public BigDecimal getZcrtbili() {
        return zcrtbili;
    }

    public void setZcrtbili(BigDecimal zcrtbili) {
        this.zcrtbili = zcrtbili;
    }

    public BigDecimal getXieyshje() {
        return xieyshje;
    }

    public void setXieyshje(BigDecimal xieyshje) {
        this.xieyshje = xieyshje;
    }

    public BigDecimal getXieyilil() {
        return xieyilil;
    }

    public void setXieyilil(BigDecimal xieyilil) {
        this.xieyilil = xieyilil;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public LstKlnb_dkzrjj getLstKlnb_dkzrjj() {
        return lstKlnb_dkzrjj;
    }

    public void setLstKlnb_dkzrjj(LstKlnb_dkzrjj lstKlnb_dkzrjj) {
        this.lstKlnb_dkzrjj = lstKlnb_dkzrjj;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", zcrfshii='" + zcrfshii + '\'' +
                ", zcrtbili=" + zcrtbili +
                ", xieyshje=" + xieyshje +
                ", xieyilil=" + xieyilil +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", lstKlnb_dkzrjj=" + lstKlnb_dkzrjj +
                '}';
    }
}
