package cn.com.yusys.yusp.web.client.esb.core.da3308;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3308.req.Da3308ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3308.resp.Da3308RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3308.req.Da3308ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3308.resp.Da3308RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(da3308)")
@RestController
@RequestMapping("/api/dscms2coreda")
public class Dscms2Da3308Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Da3308Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 抵债资产费用管理（处理码da3308）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("da3308:抵债资产费用管理")
    @PostMapping("/da3308")
    protected @ResponseBody
    ResultDto<Da3308RespDto> da3308(@Validated @RequestBody Da3308ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3308.key, EsbEnum.TRADE_CODE_DA3308.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3308.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3308.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3308.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3308.resp.Service();
        Da3308ReqService da3308ReqService = new Da3308ReqService();
        Da3308RespService da3308RespService = new Da3308RespService();
        Da3308RespDto da3308RespDto = new Da3308RespDto();
        ResultDto<Da3308RespDto> da3308ResultDto = new ResultDto<Da3308RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Da3308ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3308.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            da3308ReqService.setService(reqService);
            // 将da3308ReqService转换成da3308ReqServiceMap
            Map da3308ReqServiceMap = beanMapUtil.beanToMap(da3308ReqService);
            context.put("tradeDataMap", da3308ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3308.key, EsbEnum.TRADE_CODE_DA3308.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3308.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3308.key, EsbEnum.TRADE_CODE_DA3308.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3308RespService = beanMapUtil.mapToBean(tradeDataMap, Da3308RespService.class, Da3308RespService.class);
            respService = da3308RespService.getService();

            //  将Da3308RespDto封装到ResultDto<Da3308RespDto>
            da3308ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            da3308ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3308RespDto
                BeanUtils.copyProperties(respService, da3308RespDto);
                da3308ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3308ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3308ResultDto.setCode(EpbEnum.EPB099999.key);
                da3308ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3308.key, EsbEnum.TRADE_CODE_DA3308.value, e.getMessage());
            da3308ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3308ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3308ResultDto.setData(da3308RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3308.key, EsbEnum.TRADE_CODE_DA3308.value, JSON.toJSONString(da3308ResultDto));
        return da3308ResultDto;
    }
}
