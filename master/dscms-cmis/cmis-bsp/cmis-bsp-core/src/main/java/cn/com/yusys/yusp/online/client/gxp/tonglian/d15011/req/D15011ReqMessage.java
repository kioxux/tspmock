package cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req;

/**
 * 请求Message：永久额度调整
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class D15011ReqMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D15011ReqMessage{" +
                "message=" + message +
                '}';
    }
}
