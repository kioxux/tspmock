package cn.com.yusys.yusp.web.server.biz.xdht0041;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0041.req.Xdht0041ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0041.resp.Xdht0041RespDto;
import cn.com.yusys.yusp.dto.server.xdht0041.req.Xdht0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0041.resp.Xdht0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询受托记录状态
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0041:查询受托记录状态")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0041Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0041Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0041
     * 交易描述：查询受托记录状态
     *
     * @param xdht0041ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询受托记录状态")
    @PostMapping("/xdht0041")
    //@Idempotent({"xdcaht0041", "#xdht0041ReqDto.datasq"})
    protected @ResponseBody
    Xdht0041RespDto xdht0041(@Validated @RequestBody Xdht0041ReqDto xdht0041ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041ReqDto));
        Xdht0041DataReqDto xdht0041DataReqDto = new Xdht0041DataReqDto();// 请求Data： 查询受托记录状态
        Xdht0041DataRespDto xdht0041DataRespDto = new Xdht0041DataRespDto();// 响应Data：查询受托记录状态
        Xdht0041RespDto xdht0041RespDto = new Xdht0041RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0041.req.Data reqData = null; // 请求Data：查询受托记录状态
        cn.com.yusys.yusp.dto.server.biz.xdht0041.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0041.resp.Data();// 响应Data：查询受托记录状态
        try {
            // 从 xdht0041ReqDto获取 reqData
            reqData = xdht0041ReqDto.getData();
            // 将 reqData 拷贝到xdht0041DataReqDto
            BeanUtils.copyProperties(reqData, xdht0041DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041ReqDto));
            ResultDto<Xdht0041DataRespDto> xdht0041DataResultDto = dscmsBizHtClientService.xdht0041(xdht0041DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0041RespDto.setErorcd(Optional.ofNullable(xdht0041DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0041RespDto.setErortx(Optional.ofNullable(xdht0041DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0041DataResultDto.getCode())) {
                xdht0041RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0041RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0041DataRespDto = xdht0041DataResultDto.getData();
                // 将 xdht0041DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0041DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, e.getMessage());
            xdht0041RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0041RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0041RespDto.setDatasq(servsq);

        xdht0041RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, JSON.toJSONString(xdht0041RespDto));
        return xdht0041RespDto;
    }
}