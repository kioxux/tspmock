package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.resp;

/**
 * 响应Service：查询存单票据信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xddb03RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xddb03RespService{" +
                "service=" + service +
                '}';
    }
}
