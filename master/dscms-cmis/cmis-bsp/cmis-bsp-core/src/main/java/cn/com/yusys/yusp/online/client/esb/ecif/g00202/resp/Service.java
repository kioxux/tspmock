package cn.com.yusys.yusp.online.client.esb.ecif.g00202.resp;

/**
 * 响应Service：同业客户开户
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:21
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String custno;//客户编号
    private String tsywsq;//特殊业务流水号
    private String prcstx;//业务对比字段
    private String cropcd;//组织机构代码
    private String nnjytp;//交易类型
    private String nnjyna;//交易名称
    private String jiaoyils;//核心流水

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getTsywsq() {
        return tsywsq;
    }

    public void setTsywsq(String tsywsq) {
        this.tsywsq = tsywsq;
    }

    public String getPrcstx() {
        return prcstx;
    }

    public void setPrcstx(String prcstx) {
        this.prcstx = prcstx;
    }

    public String getCropcd() {
        return cropcd;
    }

    public void setCropcd(String cropcd) {
        this.cropcd = cropcd;
    }

    public String getNnjytp() {
        return nnjytp;
    }

    public void setNnjytp(String nnjytp) {
        this.nnjytp = nnjytp;
    }

    public String getNnjyna() {
        return nnjyna;
    }

    public void setNnjyna(String nnjyna) {
        this.nnjyna = nnjyna;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    @Override
    public String toString() {
        return "G00202RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", custno='" + custno + '\'' +
                ", tsywsq='" + tsywsq + '\'' +
                ", prcstx='" + prcstx + '\'' +
                ", cropcd='" + cropcd + '\'' +
                ", nnjytp='" + nnjytp + '\'' +
                ", nnjyna='" + nnjyna + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                '}';
    }
}
