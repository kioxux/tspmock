package cn.com.yusys.yusp.online.client.esb.core.co3222.resp;

import java.util.List;

/**
 * 响应Service：抵质押物单笔查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Lstnum2 {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum2Record> record;

    public List<Lstnum2Record> getRecord() {
        return record;
    }

    public void setRecord(List<Lstnum2Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstnum2{" +
                "record=" + record +
                '}';
    }
}
