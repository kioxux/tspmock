package cn.com.yusys.yusp.web.server.biz.xdxw0020;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0020.req.Xdxw0020ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0020.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0020.resp.Xdxw0020RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0020.req.Xdxw0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0020.resp.Xdxw0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户调查撤销
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0020:客户调查撤销")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0020Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0020
     * 交易描述：客户调查撤销
     *
     * @param xdxw0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户调查撤销")
    @PostMapping("/xdxw0020")
    @Idempotent({"xdcaxw0020", "#xdxw0020ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0020RespDto xdxw0020(@Validated @RequestBody Xdxw0020ReqDto xdxw0020ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020ReqDto));
        Xdxw0020DataReqDto xdxw0020DataReqDto = new Xdxw0020DataReqDto();// 请求Data： 客户调查撤销
        Xdxw0020DataRespDto xdxw0020DataRespDto = new Xdxw0020DataRespDto();// 响应Data：客户调查撤销

        cn.com.yusys.yusp.dto.server.biz.xdxw0020.req.Data reqData = null; // 请求Data：客户调查撤销
        cn.com.yusys.yusp.dto.server.biz.xdxw0020.resp.Data respData = new Data();// 响应Data：客户调查撤销

		Xdxw0020RespDto xdxw0020RespDto = new Xdxw0020RespDto();
		try {
            // 从 xdxw0020ReqDto获取 reqData
            reqData = xdxw0020ReqDto.getData();
            // 将 reqData 拷贝到xdxw0020DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0020DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020DataReqDto));
            ResultDto<Xdxw0020DataRespDto> xdxw0020DataResultDto = dscmsBizXwClientService.xdxw0020(xdxw0020DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0020RespDto.setErorcd(Optional.ofNullable(xdxw0020DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0020RespDto.setErortx(Optional.ofNullable(xdxw0020DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0020DataResultDto.getCode())) {
                xdxw0020DataRespDto = xdxw0020DataResultDto.getData();
                xdxw0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0020DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, e.getMessage());
            xdxw0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0020RespDto.setDatasq(servsq);

        xdxw0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0020.key, DscmsEnum.TRADE_CODE_XDXW0020.value, JSON.toJSONString(xdxw0020RespDto));
        return xdxw0020RespDto;
    }
}
