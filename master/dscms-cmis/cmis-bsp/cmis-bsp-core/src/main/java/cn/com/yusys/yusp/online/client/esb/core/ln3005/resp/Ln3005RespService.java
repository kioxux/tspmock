package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp;

/**
 * 响应Service：贷款产品查询
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3005RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3005RespService{" +
                "service=" + service +
                '}';
    }
}
