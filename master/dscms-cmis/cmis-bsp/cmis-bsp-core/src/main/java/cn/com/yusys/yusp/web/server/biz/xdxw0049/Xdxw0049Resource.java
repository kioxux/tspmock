package cn.com.yusys.yusp.web.server.biz.xdxw0049;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0049.req.Xdxw0049ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0049.resp.Xdxw0049RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0049.req.Xdxw0049DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0049.resp.Xdxw0049DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:智能风控删除通知
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0049:智能风控删除通知")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0049Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0049Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0049
     * 交易描述：智能风控删除通知
     *
     * @param xdxw0049ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("智能风控删除通知")
    @PostMapping("/xdxw0049")
    //@Idempotent({"xdcaxw0049", "#xdxw0049ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0049RespDto xdxw0049(@Validated @RequestBody Xdxw0049ReqDto xdxw0049ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049ReqDto));
        Xdxw0049DataReqDto xdxw0049DataReqDto = new Xdxw0049DataReqDto();// 请求Data： 智能风控删除通知
        Xdxw0049DataRespDto xdxw0049DataRespDto = new Xdxw0049DataRespDto();// 响应Data：智能风控删除通知
		Xdxw0049RespDto xdxw0049RespDto = new Xdxw0049RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0049.req.Data reqData = null; // 请求Data：智能风控删除通知
		cn.com.yusys.yusp.dto.server.biz.xdxw0049.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0049.resp.Data();// 响应Data：智能风控删除通知
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0049ReqDto获取 reqData
            reqData = xdxw0049ReqDto.getData();
            // 将 reqData 拷贝到xdxw0049DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0049DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataReqDto));
            ResultDto<Xdxw0049DataRespDto> xdxw0049DataResultDto = dscmsBizXwClientService.xdxw0049(xdxw0049DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0049RespDto.setErorcd(Optional.ofNullable(xdxw0049DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0049RespDto.setErortx(Optional.ofNullable(xdxw0049DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0049DataResultDto.getCode())) {
                xdxw0049DataRespDto = xdxw0049DataResultDto.getData();
                xdxw0049RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0049RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0049DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0049DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, e.getMessage());
            xdxw0049RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0049RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0049RespDto.setDatasq(servsq);

        xdxw0049RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0049.key, DscmsEnum.TRADE_CODE_XDXW0049.value, JSON.toJSONString(xdxw0049RespDto));
        return xdxw0049RespDto;
    }
}
