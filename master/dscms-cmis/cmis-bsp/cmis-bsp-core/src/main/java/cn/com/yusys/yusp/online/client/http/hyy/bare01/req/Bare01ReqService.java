package cn.com.yusys.yusp.online.client.http.hyy.bare01.req;

/**
 * 请求Service：押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Bare01ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
