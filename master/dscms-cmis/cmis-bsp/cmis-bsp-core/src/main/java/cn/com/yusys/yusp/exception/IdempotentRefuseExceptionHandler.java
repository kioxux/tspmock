package cn.com.yusys.yusp.exception;

import cn.com.yusys.yusp.commons.exception.web.handler.ExceptionHandler;
import cn.com.yusys.yusp.commons.idempotent.exception.IdempotentRefuseException;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * 幂等性组件异常处理类
 *
 * @version 1.0
 */
@Component
public class IdempotentRefuseExceptionHandler implements ExceptionHandler<IdempotentRefuseException> , Ordered {
    private static Logger logger = LoggerFactory.getLogger(IdempotentRefuseExceptionHandler.class);

    @Override
    public Object handleException(IdempotentRefuseException e) {
        logger.info(TradeLogConstants.IDEMPOTENT_EXCEPTION_BEGIN_PREFIX_LOGGER, e.getMessage());
        TradeServerRespDto tradeServerRespDto = new TradeServerRespDto();
        tradeServerRespDto.setErorcd(EpbEnum.EPB090005.key);
        tradeServerRespDto.setErortx(EpbEnum.EPB090005.value);//全局流水重复,请重新发起.
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        tradeServerRespDto.setDatasq(servsq);
        logger.info(TradeLogConstants.IDEMPOTENT_EXCEPTION_END_PREFIX_LOGGER, JSON.toJSONString(tradeServerRespDto));
        return tradeServerRespDto;
    }

    @Override
    public boolean isSupported(Exception e) {
        return e instanceof IdempotentRefuseException;
    }

    @Override
    public int getOrder() {
        return -1;
    }
}
