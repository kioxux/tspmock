package cn.com.yusys.yusp.web.server.biz.xdzc0025;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0025.req.Xdzc0025ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0025.resp.Xdzc0025RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0025.req.Xdzc0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0025.resp.Xdzc0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池业务开关检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0025:资产池业务开关检查")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0025Resource {
	private static final Logger logger = LoggerFactory.getLogger(Xdzc0025Resource.class);
	@Autowired
	private DscmsBizZcClientService dscmsBizZcClientService;

	/**
	 * 交易码：xdzc0025
	 * 交易描述：资产池业务开关检查
	 *
	 * @param xdzc0025ReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("资产池业务开关检查")
	@PostMapping("/xdzc0025")
	@Idempotent({"xdzc0025", "#xdzc0025ReqDto.datasq"})
	protected @ResponseBody
	Xdzc0025RespDto xdzc0025(@Validated @RequestBody Xdzc0025ReqDto xdzc0025ReqDto) throws Exception {

		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, JSON.toJSONString(xdzc0025ReqDto));
		Xdzc0025DataReqDto xdzc0025DataReqDto = new Xdzc0025DataReqDto();
		Xdzc0025DataRespDto xdzc0025DataRespDto = new Xdzc0025DataRespDto();
		Xdzc0025RespDto xdzc0025RespDto = new Xdzc0025RespDto();
		cn.com.yusys.yusp.dto.server.biz.xdzc0025.req.Data reqData = null;
		cn.com.yusys.yusp.dto.server.biz.xdzc0025.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0025.resp.Data();// 响应Data：票据池托收回款通知信贷

		try {
			// 从 xdzc0025ReqDto获取 reqData
			reqData = xdzc0025ReqDto.getData();
			// 将 reqData 拷贝到xdzc0025DataReqDto
			BeanUtils.copyProperties(reqData, xdzc0025DataReqDto);

			logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, JSON.toJSONString(xdzc0025DataReqDto));
			ResultDto<Xdzc0025DataRespDto> xdzc0025DataResultDto = dscmsBizZcClientService.xdzc0025(xdzc0025DataReqDto);
			logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, JSON.toJSONString(xdzc0025DataResultDto));
			// 从返回值中获取响应码和响应消息
			xdzc0025RespDto.setErorcd(Optional.ofNullable(xdzc0025DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
			xdzc0025RespDto.setErortx(Optional.ofNullable(xdzc0025DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

			if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0025DataResultDto.getCode())) {
				xdzc0025DataRespDto = xdzc0025DataResultDto.getData();
				xdzc0025RespDto.setErorcd(SuccessEnum.SUCCESS.key);
				xdzc0025RespDto.setErortx(SuccessEnum.SUCCESS.value);
				// 将 xdzc0025DataRespDto拷贝到 respData
				BeanUtils.copyProperties(xdzc0025DataRespDto, respData);
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, e.getMessage());
			xdzc0025RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
			xdzc0025RespDto.setErortx(e.getMessage());// 系统异常
		}
		logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
		String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
		logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
		xdzc0025RespDto.setDatasq(servsq);

		xdzc0025RespDto.setData(respData);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0025.key, DscmsEnum.TRADE_CODE_XDZC0025.value, JSON.toJSONString(xdzc0025RespDto));
		return xdzc0025RespDto;
	}
}
