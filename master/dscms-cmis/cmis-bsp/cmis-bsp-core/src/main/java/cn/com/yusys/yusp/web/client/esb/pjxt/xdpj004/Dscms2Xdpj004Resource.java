package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj004;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.req.Xdpj004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.resp.Xdpj004RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.req.Xdpj004ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.resp.Xdpj004RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:承兑签发审批结果综合服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj004Resource {
	private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj004Resource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
	private final BeanMapUtil beanMapUtil = new BeanMapUtil();

	/**
	 * 交易码：xdpj004
	 * 交易描述：承兑签发审批结果综合服务接口
	 *
	 * @param xdpj004ReqDto
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/xdpj004")
	protected @ResponseBody
	ResultDto<Xdpj004RespDto> xdpj004(@Validated @RequestBody Xdpj004ReqDto xdpj004ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ004.key, EsbEnum.TRADE_CODE_XDPJ004.value, JSON.toJSONString(xdpj004ReqDto));
		cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.req.Service();
		cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.resp.Service();
		Xdpj004ReqService xdpj004ReqService = new Xdpj004ReqService();
		Xdpj004RespService xdpj004RespService = new Xdpj004RespService();
		Xdpj004RespDto xdpj004RespDto = new Xdpj004RespDto();
		ResultDto<Xdpj004RespDto> xdpj004ResultDto = new ResultDto<Xdpj004RespDto>();
		Map<String, Object> context = new HashMap<>();
		Map<String, Object> result = new HashMap<>();
		try {
			//  将xdpj004ReqDto转换成reqService
			BeanUtils.copyProperties(xdpj004ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ004.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
			reqService.setDatasq(servsq);//    全局流水
			xdpj004ReqService.setService(reqService);
			// 将xdpj004ReqService转换成xdpj004ReqServiceMap
			Map xdpj004ReqServiceMap = beanMapUtil.beanToMap(xdpj004ReqService);
			context.put("tradeDataMap", xdpj004ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ004.key, EsbEnum.TRADE_CODE_XDPJ004.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ004.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ004.key, EsbEnum.TRADE_CODE_XDPJ004.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			xdpj004RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj004RespService.class, Xdpj004RespService.class);
			respService = xdpj004RespService.getService();
			//  将respService转换成Xdpj004RespDto
			BeanUtils.copyProperties(respService, xdpj004RespDto);
			xdpj004ResultDto.setCode(Optional.ofNullable(respService.getRetcod()).orElse(SuccessEnum.SUCCESS.key));
			xdpj004ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getRetcod())) {
				//  将respService转换成XdypbdccxRespDto
				BeanUtils.copyProperties(respService, xdpj004RespDto);
				xdpj004ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				xdpj004ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				xdpj004ResultDto.setCode(EpbEnum.EPB099999.key);
				xdpj004ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ004.key, EsbEnum.TRADE_CODE_XDPJ004.value, e.getMessage());
			xdpj004ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			xdpj004ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		xdpj004ResultDto.setData(xdpj004RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ004.key, EsbEnum.TRADE_CODE_XDPJ004.value, JSON.toJSONString(xdpj004ResultDto));
		return xdpj004ResultDto;
	}
}
