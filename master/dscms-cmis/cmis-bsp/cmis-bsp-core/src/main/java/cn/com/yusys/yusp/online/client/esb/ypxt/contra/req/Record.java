package cn.com.yusys.yusp.online.client.esb.ypxt.contra.req;

/**
 * 请求Service：押品与担保合同关系同步（处理码contra）
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class Record {

    private String dbhtbh;//	担保合同编号	是	varchar(50)	是		dbhtbh
    private String yptybh;//	押品统一编号	是	varchar(32)	是		yptybh
    private String sernbz;//	保证人流水号	是	varchar(40)	否		sernbz
    private String bzrzbh;//	保证人组编号	是	varchar(40)	否		bzrzbh
    private String isflag;//	是否有效	是	varchar(5)	是	1-有效；0-无效	isflag
    private String dbjkrb;

    public String getDbhtbh() {
        return dbhtbh;
    }

    public void setDbhtbh(String dbhtbh) {
        this.dbhtbh = dbhtbh;
    }

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getSernbz() {
        return sernbz;
    }

    public void setSernbz(String sernbz) {
        this.sernbz = sernbz;
    }

    public String getBzrzbh() {
        return bzrzbh;
    }

    public void setBzrzbh(String bzrzbh) {
        this.bzrzbh = bzrzbh;
    }

    public String getIsflag() {
        return isflag;
    }

    public void setIsflag(String isflag) {
        this.isflag = isflag;
    }

    public String getDbjkrb() {
        return dbjkrb;
    }

    public void setDbjkrb(String dbjkrb) {
        this.dbjkrb = dbjkrb;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dbhtbh='" + dbhtbh + '\'' +
                ", yptybh='" + yptybh + '\'' +
                ", sernbz='" + sernbz + '\'' +
                ", bzrzbh='" + bzrzbh + '\'' +
                ", isflag='" + isflag + '\'' +
                ", dbjkrb='" + dbjkrb + '\'' +
                '}';
    }
}
