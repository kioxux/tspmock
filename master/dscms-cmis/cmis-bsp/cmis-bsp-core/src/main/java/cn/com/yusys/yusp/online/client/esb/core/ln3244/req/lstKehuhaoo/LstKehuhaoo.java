package cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo;

import java.util.List;
/**
 * 请求Service：贷款客户经理批量移交
 */
public class LstKehuhaoo {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstKehuhaoo{" +
                "record=" + record +
                '}';
    }
}
