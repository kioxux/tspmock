package cn.com.yusys.yusp.web.server.biz.xdtz0039;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0039.req.Xdtz0039ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0039.resp.Xdtz0039RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0039.req.Xdtz0039DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0039.resp.Xdtz0039DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据企业名称查询申请企业在本行是否存在当前逾期贷款
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0039:根据企业名称查询申请企业在本行是否存在当前逾期贷款")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0039Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0039Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0039
     * 交易描述：根据企业名称查询申请企业在本行是否存在当前逾期贷款
     *
     * @param xdtz0039ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0039:根据客户号前往信贷查找房贷借据信息")
    @PostMapping("/xdtz0039")
    //@Idempotent({"xdcatz0039", "#xdtz0039ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0039RespDto xdtz0039(@Validated @RequestBody Xdtz0039ReqDto xdtz0039ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, JSON.toJSONString(xdtz0039ReqDto));
        Xdtz0039DataReqDto xdtz0039DataReqDto = new Xdtz0039DataReqDto();// 请求Data： 根据企业名称查询申请企业在本行是否存在当前逾期贷款
        Xdtz0039DataRespDto xdtz0039DataRespDto = new Xdtz0039DataRespDto();// 响应Data：根据企业名称查询申请企业在本行是否存在当前逾期贷款
        Xdtz0039RespDto xdtz0039RespDto = new Xdtz0039RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0039.req.Data reqData = null; // 请求Data：根据企业名称查询申请企业在本行是否存在当前逾期贷款
        cn.com.yusys.yusp.dto.server.biz.xdtz0039.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0039.resp.Data();// 响应Data：根据企业名称查询申请企业在本行是否存在当前逾期贷款
        try {
            // 从 xdtz0039ReqDto获取 reqData
            reqData = xdtz0039ReqDto.getData();
            // 将 reqData 拷贝到xdtz0039DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0039DataReqDto);
            // 调用服务
            ResultDto<Xdtz0039DataRespDto> xdtz0039DataResultDto = dscmsBizTzClientService.xdtz0039(xdtz0039DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdtz0039RespDto.setErorcd(Optional.ofNullable(xdtz0039DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0039RespDto.setErortx(Optional.ofNullable(xdtz0039DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdtz0039RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0039DataResultDto.getCode())) {
                xdtz0039RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0039RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdtz0039DataRespDto = xdtz0039DataResultDto.getData();
                // 将 xdtz0039DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0039DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, e.getMessage());
            xdtz0039RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0039RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0039RespDto.setDatasq(servsq);
        xdtz0039RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0039.key, DscmsEnum.TRADE_CODE_XDTZ0039.value, JSON.toJSONString(xdtz0039RespDto));
        return xdtz0039RespDto;
    }
}