package cn.com.yusys.yusp.web.client.esb.doc.doc000;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.req.Doc000ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.req.List_index;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.resp.Doc000RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.doc.doc000.req.Doc000ReqService;
import cn.com.yusys.yusp.online.client.esb.doc.doc000.resp.Doc000RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(doc000)")
@RestController
@RequestMapping("/api/dscms2doc")
public class Dscms2Doc000Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Doc000Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    public static Map<String, String> toUpperKey(Map<String, String> map) {
        Map<String, String> resultMap = new HashMap<>();
        Set<String> sets = map.keySet();
        for (String key : sets) {
            resultMap.put(key.toUpperCase(), map.get(key));
        }
        return resultMap;
    }

    /**
     * 入库接口（处理码doc000）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("doc000:入库接口")
    @PostMapping("/doc000")
    protected @ResponseBody
    ResultDto<Doc000RespDto> doc000(@Validated @RequestBody Doc000ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC000.key, EsbEnum.TRADE_CODE_DOC000.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.doc.doc000.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.doc.doc000.req.Service();
        cn.com.yusys.yusp.online.client.esb.doc.doc000.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.doc.doc000.resp.Service();
        cn.com.yusys.yusp.online.client.esb.doc.doc000.req.List_index listIndex = new cn.com.yusys.yusp.online.client.esb.doc.doc000.req.List_index();
        Doc000ReqService doc000ReqService = new Doc000ReqService();
        Doc000RespService doc000RespService = new Doc000RespService();
        Doc000RespDto doc000RespDto = new Doc000RespDto();
        ResultDto<Doc000RespDto> doc000ResultDto = new ResultDto<Doc000RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Doc000ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            if (CollectionUtils.nonEmpty(reqDto.getList_index())) {
                List<List_index> list_index = reqDto.getList_index();
                List<cn.com.yusys.yusp.online.client.esb.doc.doc000.req.Record> recordList = new ArrayList<>();
                for (List_index l : list_index) {
                    cn.com.yusys.yusp.online.client.esb.doc.doc000.req.Record record = new cn.com.yusys.yusp.online.client.esb.doc.doc000.req.Record();
                    BeanUtils.copyProperties(l, record);
                    recordList.add(record);
                }
                listIndex.setRecord(recordList);
                reqService.setList_index(listIndex);
            }
            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DOC000.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            doc000ReqService.setService(reqService);
            // 将doc000ReqService转换成doc000ReqServiceMap
            Map doc000ReqServiceMap = beanMapUtil.beanToMap(doc000ReqService);

            context.put("tradeDataMap", doc000ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC000.key, EsbEnum.TRADE_CODE_DOC000.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DOC000.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC000.key, EsbEnum.TRADE_CODE_DOC000.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            doc000RespService = beanMapUtil.mapToBean(tradeDataMap, Doc000RespService.class, Doc000RespService.class);
            respService = doc000RespService.getService();

            //  将Doc000RespDto封装到ResultDto<Doc000RespDto>
            doc000ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            doc000ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Doc000RespDto
                BeanUtils.copyProperties(respService, doc000RespDto);
                doc000ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                doc000ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                doc000ResultDto.setCode(EpbEnum.EPB099999.key);
                doc000ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC000.key, EsbEnum.TRADE_CODE_DOC000.value, e.getMessage());
            doc000ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            doc000ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        doc000ResultDto.setData(doc000RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC000.key, EsbEnum.TRADE_CODE_DOC000.value, JSON.toJSONString(doc000ResultDto));
        return doc000ResultDto;
    }
}
