package cn.com.yusys.yusp.web.server.biz.xdtz0055;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0055.req.Xdtz0055ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0055.resp.Xdtz0055RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0055.req.Xdtz0055DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0055.resp.Xdtz0055DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询退回受托信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0055:查询退回受托信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0055Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0055Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0055
     * 交易描述：查询退回受托信息
     *
     * @param xdtz0055ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询退回受托信息")
    @PostMapping("/xdtz0055")
//@Idempotent({"xdtz0055", "#xdtz0055ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0055RespDto xdtz0055(@Validated @RequestBody Xdtz0055ReqDto xdtz0055ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, JSON.toJSONString(xdtz0055ReqDto));
        Xdtz0055DataReqDto xdtz0055DataReqDto = new Xdtz0055DataReqDto();// 请求Data： 查询退回受托信息
        Xdtz0055DataRespDto xdtz0055DataRespDto = new Xdtz0055DataRespDto();// 响应Data：查询退回受托信息
        Xdtz0055RespDto xdtz0055RespDto = new Xdtz0055RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0055.req.Data reqData = null; // 请求Data：查询退回受托信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0055.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0055.resp.Data();// 响应Data：查询退回受托信息
        try {
            // 从 xdtz0055ReqDto获取 reqData
            reqData = xdtz0055ReqDto.getData();
            // 将 reqData 拷贝到xdtz0055DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0055DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, JSON.toJSONString(xdtz0055DataReqDto));
            ResultDto<Xdtz0055DataRespDto> xdtz0055DataResultDto = dscmsBizTzClientService.xdtz0055(xdtz0055DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, JSON.toJSONString(xdtz0055DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0055RespDto.setErorcd(Optional.ofNullable(xdtz0055DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0055RespDto.setErortx(Optional.ofNullable(xdtz0055DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0055DataResultDto.getCode())) {
                xdtz0055DataRespDto = xdtz0055DataResultDto.getData();
                xdtz0055RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0055RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0055DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0055DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, e.getMessage());
            xdtz0055RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0055RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0055RespDto.setDatasq(servsq);

        xdtz0055RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0055.key, DscmsEnum.TRADE_CODE_XDTZ0055.value, JSON.toJSONString(xdtz0055RespDto));
        return xdtz0055RespDto;
    }
}
