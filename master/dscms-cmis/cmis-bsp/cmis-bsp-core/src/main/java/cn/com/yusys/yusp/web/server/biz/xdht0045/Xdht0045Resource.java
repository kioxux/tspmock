package cn.com.yusys.yusp.web.server.biz.xdht0045;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0045.req.Xdht0045ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0045.resp.Xdht0045RespDto;
import cn.com.yusys.yusp.dto.server.xdht0045.req.Xdht0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0045.resp.Xdht0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
* 接口处理类:房群客户查询
*
* @author zhugenrong
* @version 1.0
*/
@Api(tags = "XDHT0045:查询信贷客户名称合同状态")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0045Resource {
        private static final Logger logger = LoggerFactory.getLogger(Xdht0045Resource.class);
        @Autowired
        private DscmsBizHtClientService dscmsBizHtClientService;

        /**
        * 交易码：xdht0045
        * 交易描述：查询信贷客户名称合同状态
        *
        * @param xdht0045ReqDto
        * @return
        * @throws Exception
        */
        @ApiOperation("xdht0045:查询信贷客户名称合同状态")
        @PostMapping("/xdht0045")
        //@Idempotent({"xdcaht0045", "#xdht0045ReqDto.datasq"})
        protected @ResponseBody
        Xdht0045RespDto xdht0045(@Validated @RequestBody Xdht0045ReqDto xdht0045ReqDto) throws Exception {
                logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, JSON.toJSONString(xdht0045ReqDto));
                Xdht0045DataReqDto xdht0045DataReqDto = new Xdht0045DataReqDto();// 请求Data
                Xdht0045DataRespDto xdht0045DataRespDto = new Xdht0045DataRespDto();// 响应Data
                Xdht0045RespDto xdht0045RespDto = new Xdht0045RespDto();
                cn.com.yusys.yusp.dto.server.biz.xdht0045.req.Data reqData = null; // 请求Data
                cn.com.yusys.yusp.dto.server.biz.xdht0045.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0045.resp.Data();// 响应Data
                try {
                        // 从 xdht0045ReqDto获取 reqData
                        reqData = xdht0045ReqDto.getData();
                        // 将 reqData 拷贝到xdht0045DataReqDto
                        BeanUtils.copyProperties(reqData, xdht0045DataReqDto);
                        // 调用服务
                        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, JSON.toJSONString(xdht0045DataReqDto));
                        ResultDto<Xdht0045DataRespDto> xdht0045DataResultDto = dscmsBizHtClientService.xdht0045(xdht0045DataReqDto);
                        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, JSON.toJSONString(xdht0045DataReqDto));
                        // 从返回值中获取响应码和响应消息
                        xdht0045RespDto.setErorcd(Optional.ofNullable(xdht0045DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
                        xdht0045RespDto.setErortx(Optional.ofNullable(xdht0045DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0045DataResultDto.getCode())) {
                                xdht0045RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                                xdht0045RespDto.setErortx(SuccessEnum.SUCCESS.value);
                                xdht0045DataRespDto = xdht0045DataResultDto.getData();
                                // 将 xdht0045DataRespDto拷贝到 respData
                                BeanUtils.copyProperties(xdht0045DataRespDto, respData);
                        }
                } catch (Exception e) {
                        logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, e.getMessage());
                        xdht0045RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                        xdht0045RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
                }
                logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
                String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
                logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
                xdht0045RespDto.setDatasq(servsq);

                xdht0045RespDto.setData(respData);
                logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0045.key, DscmsEnum.TRADE_CODE_XDHT0045.value, JSON.toJSONString(xdht0045RespDto));
                return xdht0045RespDto;
        }
}