package cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req;
/**
 * 请求Service：押品信息同步及引入
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
