package cn.com.yusys.yusp.online.client.esb.irs.xirs28.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Service：财务信息同步
 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String custid;//客户编号
    private String reportdate;//报表日期
    private String reportsope;//报表口径
    private String reportcurrency;//报表币种
    private String reportunit;//报表单位
    private String reportaudit;//审计标志
    private String auditunit;//审计单位
    private String auditopinion;//审计意见
    private String modelclass;//财务报表类别
    private String flag;//报表标识
    @JsonProperty(value = "AssetDebt")
    private AssetDebt AssetDebt;
    @JsonProperty(value = "Loss")
    private Loss Loss;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getReportdate() {
        return reportdate;
    }

    public void setReportdate(String reportdate) {
        this.reportdate = reportdate;
    }

    public String getReportsope() {
        return reportsope;
    }

    public void setReportsope(String reportsope) {
        this.reportsope = reportsope;
    }

    public String getReportcurrency() {
        return reportcurrency;
    }

    public void setReportcurrency(String reportcurrency) {
        this.reportcurrency = reportcurrency;
    }

    public String getReportunit() {
        return reportunit;
    }

    public void setReportunit(String reportunit) {
        this.reportunit = reportunit;
    }

    public String getReportaudit() {
        return reportaudit;
    }

    public void setReportaudit(String reportaudit) {
        this.reportaudit = reportaudit;
    }

    public String getAuditunit() {
        return auditunit;
    }

    public void setAuditunit(String auditunit) {
        this.auditunit = auditunit;
    }

    public String getAuditopinion() {
        return auditopinion;
    }

    public void setAuditopinion(String auditopinion) {
        this.auditopinion = auditopinion;
    }

    public String getModelclass() {
        return modelclass;
    }

    public void setModelclass(String modelclass) {
        this.modelclass = modelclass;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.AssetDebt getAssetDebt() {
        return AssetDebt;
    }

    @JsonIgnore
    public void setAssetDebt(cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.AssetDebt assetDebt) {
        AssetDebt = assetDebt;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.Loss getLoss() {
        return Loss;
    }

    @JsonIgnore
    public void setLoss(cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.Loss loss) {
        Loss = loss;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", custid='" + custid + '\'' +
                ", reportdate='" + reportdate + '\'' +
                ", reportsope='" + reportsope + '\'' +
                ", reportcurrency='" + reportcurrency + '\'' +
                ", reportunit='" + reportunit + '\'' +
                ", reportaudit='" + reportaudit + '\'' +
                ", auditunit='" + auditunit + '\'' +
                ", auditopinion='" + auditopinion + '\'' +
                ", modelclass='" + modelclass + '\'' +
                ", flag='" + flag + '\'' +
                ", AssetDebt=" + AssetDebt +
                ", Loss=" + Loss +
                '}';
    }
}
