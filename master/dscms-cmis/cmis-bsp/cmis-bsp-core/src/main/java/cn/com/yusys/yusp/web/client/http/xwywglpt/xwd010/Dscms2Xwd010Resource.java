package cn.com.yusys.yusp.web.client.http.xwywglpt.xwd010;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.req.Xwd010ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.resp.Xwd010RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.req.Xwd010ReqService;
import cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.resp.Xwd010RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用小微业务管理平台的接口
 */
@Api(tags = "BSP封装新微贷平台接口处理类(xwd010)")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Xwd010Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xwd010Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xwd010
     * 交易描述：利率撤销接口
     *
     * @param xwd010ReqDto
     * @return
     * @throws Exception
     * @modify 改成走esb交易
     */
    @PostMapping("/xwd010")
    protected @ResponseBody
    ResultDto<Xwd010RespDto> xwd010(@Validated @RequestBody Xwd010ReqDto xwd010ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD010.key, EsbEnum.TRADE_CODE_XWD010.value, JSON.toJSONString(xwd010ReqDto));
        cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.req.Service reqService = new cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.req.Service();
        cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.resp.Service respService = new cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.resp.Service();
        Xwd010ReqService xwd010ReqService = new Xwd010ReqService();
        Xwd010RespService xwd010RespService = new Xwd010RespService();
        Xwd010RespDto xwd010RespDto = new Xwd010RespDto();
        ResultDto<Xwd010RespDto> xwd010ResultDto = new ResultDto<Xwd010RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xwd010ReqDto转换成reqService
            BeanUtils.copyProperties(xwd010ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_XWD010.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_XWD.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWD.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            xwd010ReqService.setService(reqService);
            // 将xwd010ReqService转换成xwd010ReqServiceMap
            Map xwd010ReqServiceMap = beanMapUtil.beanToMap(xwd010ReqService);
            context.put("tradeDataMap", xwd010ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD010.key, EsbEnum.TRADE_CODE_XWD010.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XWD010.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD010.key, EsbEnum.TRADE_CODE_XWD010.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xwd010RespService = beanMapUtil.mapToBean(tradeDataMap, Xwd010RespService.class, Xwd010RespService.class);
            respService = xwd010RespService.getService();

            xwd010ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xwd010ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xwd010RespDto
                BeanUtils.copyProperties(respService, xwd010RespDto);
                xwd010ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xwd010ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xwd010ResultDto.setCode(EpbEnum.EPB099999.key);
                xwd010ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD010.key, EsbEnum.TRADE_CODE_XWD010.value, e.getMessage());
            xwd010ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xwd010ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xwd010ResultDto.setData(xwd010RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD010.key, EsbEnum.TRADE_CODE_XWD010.value, JSON.toJSONString(xwd010ResultDto));
        return xwd010ResultDto;
    }
}
