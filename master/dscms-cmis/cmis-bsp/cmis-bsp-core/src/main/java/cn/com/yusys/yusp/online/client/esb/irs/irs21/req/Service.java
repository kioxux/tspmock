package cn.com.yusys.yusp.online.client.esb.irs.irs21.req;

/**
 * 请求Service：单一客户限额测算信息同步
 *
 * @author zhugenrong
 * @version 1.0

 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String waibclma;//外部处理码
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String sxserialno;//	授信申请流水号
    private String custid;//	客户编号
    private String custname;//	客户名称
    private String extdebt;//	客户对外负债
    private String otherdebt;//	现有他行负债
    private String guarsum;//	对外抵押金额
    private String impsum;//	对外质押金额
    private String cashsum;//	对外保证金额

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getSxserialno() {
        return sxserialno;
    }

    public void setSxserialno(String sxserialno) {
        this.sxserialno = sxserialno;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getExtdebt() {
        return extdebt;
    }

    public void setExtdebt(String extdebt) {
        this.extdebt = extdebt;
    }

    public String getOtherdebt() {
        return otherdebt;
    }

    public void setOtherdebt(String otherdebt) {
        this.otherdebt = otherdebt;
    }

    public String getGuarsum() {
        return guarsum;
    }

    public void setGuarsum(String guarsum) {
        this.guarsum = guarsum;
    }

    public String getImpsum() {
        return impsum;
    }

    public void setImpsum(String impsum) {
        this.impsum = impsum;
    }

    public String getCashsum() {
        return cashsum;
    }

    public void setCashsum(String cashsum) {
        this.cashsum = cashsum;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getWaibclma() {
        return waibclma;
    }

    public void setWaibclma(String waibclma) {
        this.waibclma = waibclma;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", waibclma='" + waibclma + '\'' +
                ", datasq='" + datasq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", sxserialno='" + sxserialno + '\'' +
                ", custid='" + custid + '\'' +
                ", custname='" + custname + '\'' +
                ", extdebt='" + extdebt + '\'' +
                ", otherdebt='" + otherdebt + '\'' +
                ", guarsum='" + guarsum + '\'' +
                ", impsum='" + impsum + '\'' +
                ", cashsum='" + cashsum + '\'' +
                '}';
    }
}
