package cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.resp;

/**
 * 响应Message：现金（大额）放款接口
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 10:14
 * @since 2021/5/27 10:14
 */
public class Body {
    private String lentus;//放款状态
    private String b039;//行内放款 39 域响应码

    public String getLentus() {
        return lentus;
    }

    public void setLentus(String lentus) {
        this.lentus = lentus;
    }

    public String getB039() {
        return b039;
    }

    public void setB039(String b039) {
        this.b039 = b039;
    }

    @Override
    public String toString() {
        return "Service{" +
                "lentus='" + lentus + '\'' +
                "b039='" + b039 + '\'' +
                '}';
    }
}


