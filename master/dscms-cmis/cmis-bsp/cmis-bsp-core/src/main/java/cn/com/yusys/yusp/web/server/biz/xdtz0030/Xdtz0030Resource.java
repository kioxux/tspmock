package cn.com.yusys.yusp.web.server.biz.xdtz0030;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0030.req.Xdtz0030ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0030.resp.Xdtz0030RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0030.req.Xdtz0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0030.resp.Xdtz0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查看信贷贴现台账中票据是否已经存在
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0030:查看信贷贴现台账中票据是否已经存在")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0030Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0030
     * 交易描述：查看信贷贴现台账中票据是否已经存在
     *
     * @param xdtz0030ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查看信贷贴现台账中票据是否已经存在")
    @PostMapping("/xdtz0030")
    //@Idempotent({"xdcatz0030", "#xdtz0030ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0030RespDto xdtz0030(@Validated @RequestBody Xdtz0030ReqDto xdtz0030ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030ReqDto));
        Xdtz0030DataReqDto xdtz0030DataReqDto = new Xdtz0030DataReqDto();// 请求Data： 查看信贷贴现台账中票据是否已经存在
        Xdtz0030DataRespDto xdtz0030DataRespDto = new Xdtz0030DataRespDto();// 响应Data：查看信贷贴现台账中票据是否已经存在
        Xdtz0030RespDto xdtz0030RespDto = new Xdtz0030RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0030.req.Data reqData = null; // 请求Data：查看信贷贴现台账中票据是否已经存在
        cn.com.yusys.yusp.dto.server.biz.xdtz0030.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0030.resp.Data();// 响应Data：查看信贷贴现台账中票据是否已经存在
        try {
            // 从 xdtz0030ReqDto获取 reqData
            reqData = xdtz0030ReqDto.getData();
            // 将 reqData 拷贝到xdtz0030DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0030DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030DataReqDto));
            ResultDto<Xdtz0030DataRespDto> xdtz0030DataResultDto = dscmsBizTzClientService.xdtz0030(xdtz0030DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0030RespDto.setErorcd(Optional.ofNullable(xdtz0030DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0030RespDto.setErortx(Optional.ofNullable(xdtz0030DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0030DataResultDto.getCode())) {
                xdtz0030DataRespDto = xdtz0030DataResultDto.getData();
                xdtz0030RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0030RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0030DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0030DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, e.getMessage());
            xdtz0030RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0030RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0030RespDto.setDatasq(servsq);

        xdtz0030RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0030.key, DscmsEnum.TRADE_CODE_XDTZ0030.value, JSON.toJSONString(xdtz0030RespDto));
        return xdtz0030RespDto;
    }
}