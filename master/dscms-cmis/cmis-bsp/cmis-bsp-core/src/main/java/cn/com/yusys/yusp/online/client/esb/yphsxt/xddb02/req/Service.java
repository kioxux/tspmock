package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.req;

/**
 * 请求Service：查询抵押物信息
 */
public class Service {
    private String servtp;//渠道码
    private String prcscd;//交易码
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servsq;//渠道流水
    private String servdt;//请求方日期
    private String servti;//请求方时间
    private String ipaddr;//请求方IP
    private String mac;   //请求方MAC

    private String guaranty_id;//押品编号
    private String type;//类型

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Service{" +
                "servtp='" + servtp + '\'' +
                ", prcscd='" + prcscd + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", mac='" + mac + '\'' +
                ", guaranty_id='" + guaranty_id + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
