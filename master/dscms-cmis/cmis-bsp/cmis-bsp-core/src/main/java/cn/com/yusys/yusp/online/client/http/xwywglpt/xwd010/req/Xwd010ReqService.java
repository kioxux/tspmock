package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.req;

/**
 * 请求Service：利率撤销接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwd010ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwd010ReqService{" +
                "service=" + service +
                '}';
    }
}
