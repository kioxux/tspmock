package cn.com.yusys.yusp.web.server.biz.xdxw0071;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0071.req.Xdxw0071ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0071.resp.Xdxw0071RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0071.req.Xdxw0071DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0071.resp.Xdxw0071DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询优抵贷抵质押品信息
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDXW0071:查询优抵贷抵质押品信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0071Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0071Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0071
     * 交易描述：查询优抵贷抵质押品信息
     *
     * @param xdxw0071ReqDto
     * @return
     * @throws Exception
     */

    @ApiOperation("查询优抵贷抵质押品信息")
    @PostMapping("/xdxw0071")
    //@Idempotent({"xdcaxw0071", "#xdxw0071ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0071RespDto xdxw0071(@Validated @RequestBody Xdxw0071ReqDto xdxw0071ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071ReqDto));
        Xdxw0071DataReqDto xdxw0071DataReqDto = new Xdxw0071DataReqDto();// 请求Data： 查询优抵贷抵质押品信息
        Xdxw0071DataRespDto xdxw0071DataRespDto = new Xdxw0071DataRespDto();// 响应Data：查询优抵贷抵质押品信息
        Xdxw0071RespDto xdxw0071RespDto = new Xdxw0071RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdxw0071.req.Data reqData = null; // 请求Data：查询优抵贷抵质押品信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0071.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0071.resp.Data();// 响应Data：查询优抵贷抵质押品信息
        try {
            // 从 xdxw0071ReqDto获取 reqData
            reqData = xdxw0071ReqDto.getData();
            // 将 reqData 拷贝到xdxw0071DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0071DataReqDto);

            ResultDto<Xdxw0071DataRespDto> xdxw0071DataResultDto = dscmsBizXwClientService.xdxw0071(xdxw0071DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdxw0071RespDto.setErorcd(Optional.ofNullable(xdxw0071DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0071RespDto.setErortx(Optional.ofNullable(xdxw0071DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0071DataResultDto.getCode())) {
                xdxw0071RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0071RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0071DataRespDto = xdxw0071DataResultDto.getData();
                // 将 xdxw0071DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0071DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, e.getMessage());
            xdxw0071RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0071RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0071RespDto.setDatasq(servsq);
        xdxw0071RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0071.key, DscmsEnum.TRADE_CODE_XDXW0071.value, JSON.toJSONString(xdxw0071RespDto));
        return xdxw0071RespDto;
    }
}
