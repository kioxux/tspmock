package cn.com.yusys.yusp.online.client.esb.core.ln3066.req;

/**
 * 请求Service：资产转让借据筛选
 * @author code-generator
 * @version 1.0             
 */      
public class Ln3066ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
