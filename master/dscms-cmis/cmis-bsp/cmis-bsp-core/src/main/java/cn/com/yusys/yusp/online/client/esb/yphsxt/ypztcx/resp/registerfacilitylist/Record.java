package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerfacilitylist;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:13
 * @since 2021/7/9 14:13
 */
public class Record {
    private String affaar;//登记簿附属设施-建筑面积
    private String afinar;//登记簿附属设施-套内建筑面积
    private String afshar;//登记簿附属设施-分摊建筑面积
    private String affuse;//登记簿附属设施-用途
    private String affloc;//登记簿附属设施-坐落
    private String aflrsd;//登记簿附属设施-土地使用权开始时间
    private String aflred;//登记簿附属设施-土地使用权结束时间

    public String getAffaar() {
        return affaar;
    }

    public void setAffaar(String affaar) {
        this.affaar = affaar;
    }

    public String getAfinar() {
        return afinar;
    }

    public void setAfinar(String afinar) {
        this.afinar = afinar;
    }

    public String getAfshar() {
        return afshar;
    }

    public void setAfshar(String afshar) {
        this.afshar = afshar;
    }

    public String getAffuse() {
        return affuse;
    }

    public void setAffuse(String affuse) {
        this.affuse = affuse;
    }

    public String getAffloc() {
        return affloc;
    }

    public void setAffloc(String affloc) {
        this.affloc = affloc;
    }

    public String getAflrsd() {
        return aflrsd;
    }

    public void setAflrsd(String aflrsd) {
        this.aflrsd = aflrsd;
    }

    public String getAflred() {
        return aflred;
    }

    public void setAflred(String aflred) {
        this.aflred = aflred;
    }

    @Override
    public String toString() {
        return "Record{" +
                "affaar='" + affaar + '\'' +
                ", afinar='" + afinar + '\'' +
                ", afshar='" + afshar + '\'' +
                ", affuse='" + affuse + '\'' +
                ", affloc='" + affloc + '\'' +
                ", aflrsd='" + aflrsd + '\'' +
                ", aflred='" + aflred + '\'' +
                '}';
    }
}
