package cn.com.yusys.yusp.web.server.biz.xdxw0063;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0063.req.Xdxw0063ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0063.resp.Xdxw0063RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0063.req.Xdxw0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0063.resp.Xdxw0063DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0063:调查基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0063Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0063Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0063
     * 交易描述：调查基本信息查询
     *
     * @param xdxw0063ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查基本信息查询")
    @PostMapping("/xdxw0063")
    //@Idempotent({"xdcaxw0063", "#xdxw0063ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0063RespDto xdxw0063(@Validated @RequestBody Xdxw0063ReqDto xdxw0063ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(xdxw0063ReqDto));
        Xdxw0063DataReqDto xdxw0063DataReqDto = new Xdxw0063DataReqDto();// 请求Data： 调查基本信息查询
        Xdxw0063DataRespDto xdxw0063DataRespDto = new Xdxw0063DataRespDto();// 响应Data：调查基本信息查询
        Xdxw0063RespDto xdxw0063RespDto = new Xdxw0063RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0063.req.Data reqData = null; // 请求Data：调查基本信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0063.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0063.resp.Data();// 响应Data：调查基本信息查询
        try {
            // 从 xdxw0063ReqDto获取 reqData
            reqData = xdxw0063ReqDto.getData();
            // 将 reqData 拷贝到xdxw0063DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0063DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(xdxw0063DataReqDto));
            ResultDto<Xdxw0063DataRespDto> xdxw0063DataResultDto = dscmsBizXwClientService.xdxw0063(xdxw0063DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(xdxw0063DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0063RespDto.setErorcd(Optional.ofNullable(xdxw0063DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0063RespDto.setErortx(Optional.ofNullable(xdxw0063DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0063DataResultDto.getCode())) {
                xdxw0063DataRespDto = xdxw0063DataResultDto.getData();
                xdxw0063RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0063RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0063DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0063DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, e.getMessage());
            xdxw0063RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0063RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0063RespDto.setDatasq(servsq);

        xdxw0063RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0063.key, DscmsEnum.TRADE_CODE_XDXW0063.value, JSON.toJSONString(xdxw0063RespDto));
        return xdxw0063RespDto;
    }
}
