package cn.com.yusys.yusp.web.server.biz.xdht0020;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0020.req.Xdht0020ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0020.resp.Xdht0020RespDto;
import cn.com.yusys.yusp.dto.server.xdht0020.req.Xdht0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0020.resp.Xdht0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同信息查看
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDHT0019:合同信息查看")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0020Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0020
     * 交易描述：合同信息查看
     *
     * @param xdht0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息查看")
    @PostMapping("/xdht0020")
    //@Idempotent({"xdcaht0020", "#xdht0020ReqDto.datasq"})
    protected @ResponseBody
    Xdht0020RespDto xdht0020(@Validated @RequestBody Xdht0020ReqDto xdht0020ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020ReqDto));
        Xdht0020DataReqDto xdht0020DataReqDto = new Xdht0020DataReqDto();// 请求Data： 合同信息查看
        Xdht0020DataRespDto xdht0020DataRespDto = new Xdht0020DataRespDto();// 响应Data：合同信息查看
        Xdht0020RespDto xdht0020RespDto = new Xdht0020RespDto();
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdht0020.req.Data reqData = null; // 请求Data：合同信息查看
        cn.com.yusys.yusp.dto.server.biz.xdht0020.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0020.resp.Data();// 响应Data：合同信息查看
        //  此处包导入待确定 结束
        // 从 xdht0020ReqDto获取 reqData
        try {
            reqData = xdht0020ReqDto.getData();
            // 将 reqData 拷贝到xdht0020DataReqDto
            BeanUtils.copyProperties(reqData, xdht0020DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataReqDto));
            ResultDto<Xdht0020DataRespDto> xdht0020DataResultDto = dscmsBizHtClientService.xdht0020(xdht0020DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0020.key, DscmsEnum.TRADE_CODE_XDHT0020.value, JSON.toJSONString(xdht0020DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0020RespDto.setErorcd(Optional.ofNullable(xdht0020DataResultDto.getCode()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdht0020RespDto.setErortx(Optional.ofNullable(xdht0020DataResultDto.getMessage()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0020DataResultDto.getCode())) {
                xdht0020DataRespDto = xdht0020DataResultDto.getData();
                xdht0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0020DataRespDto, respData);
            }else{
                respData.setApplyAmt(BigDecimal.ZERO);
                respData.setAvlAmt(BigDecimal.ZERO);
                respData.setCertNo("");
                respData.setCnContNo("");
                respData.setContEndDate("");
                respData.setContNo("");
                respData.setContSignStatus("");
                respData.setContStartDate("");
                respData.setContType("");
                respData.setCurType("");
                respData.setCusName("");
                respData.setDayInt(BigDecimal.ZERO);
                respData.setLprBp(BigDecimal.ZERO);
                respData.setLprRate(BigDecimal.ZERO);
                respData.setPkId("");
                respData.setPreferCode("");
                respData.setLprBpType("");
                respData.setReplyContEndDate("");
                respData.setRepayDate("");
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, e.getMessage());
            xdht0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0020RespDto.setDatasq(servsq);

        xdht0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdht0020RespDto));
        return xdht0020RespDto;
    }
}
