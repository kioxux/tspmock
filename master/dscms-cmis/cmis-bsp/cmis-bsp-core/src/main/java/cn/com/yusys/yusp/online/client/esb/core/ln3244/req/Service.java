package cn.com.yusys.yusp.online.client.esb.core.ln3244.req;


import cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.LstHetongbh;
import cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.LstKehuhaoo;

/**
 * 请求Service：贷款客户经理批量移交
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.LstKehuhaoo lstKehuhaoo;//移交客户列表[LIST]
    private cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.LstHetongbh lstHetongbh;//移交合同列表[LIST]
    private String khjingli;//客户经理

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public LstKehuhaoo getLstKehuhaoo() {
        return lstKehuhaoo;
    }

    public void setLstKehuhaoo(LstKehuhaoo lstKehuhaoo) {
        this.lstKehuhaoo = lstKehuhaoo;
    }

    public LstHetongbh getLstHetongbh() {
        return lstHetongbh;
    }

    public void setLstHetongbh(LstHetongbh lstHetongbh) {
        this.lstHetongbh = lstHetongbh;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", lstKehuhaoo=" + lstKehuhaoo +
                ", lstHetongbh=" + lstHetongbh +
                ", khjingli='" + khjingli + '\'' +
                '}';
    }
}
