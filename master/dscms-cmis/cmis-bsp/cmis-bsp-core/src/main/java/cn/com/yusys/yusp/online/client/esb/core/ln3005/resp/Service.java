package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款产品查询
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述
    private String chanpdma;//产品代码
    private String huobdhao;//货币代号
    private String chanpmch;//产品名称
    private String shengxrq;//生效日期
    private String shxiaorq;//失效日期
    private String chanpzht;//产品状态
    private String daikduix;//贷款对象
    private String yewufenl;//业务分类
    private String yjfyjhes;//按应计非应计核算
    private String yiyldhes;//按一逾两呆核算
    private String dkxtkmhs;//形态分科目核算
    private String chanpzdm;//产品组代码
    private String chanpzmc;//产品组名称
    private String jigshybz;//机构适用标志
    private BigDecimal zuidfkje;//最大放款金额
    private BigDecimal zuixfkje;//最小放款金额
    private BigDecimal meiczdje;//每次最大金额
    private BigDecimal meiczxje;//每次最小金额
    private Integer fkzuidjg;//放款最大间隔(天)
    private Integer fkzuixjg;//放款最小间隔(天)
    private Integer zuidfkcs;//最大放款次数
    private Integer fkyouxqx;//放款有效期限(天)
    private String zidfkbzh;//自动放款标志
    private String zdfkjjms;//放款借据管理模式
    private String zhqifkbz;//周期性放款标志
    private String fkzhouqi;//放款周期
    private String fkfangsh;//放款金额方式
    private BigDecimal mcfkjebl;//每次放款金额或比例
    private String jxhjdkkz;//借新还旧控制
    private String jxhjhkkz;//借新还旧还款控制
    private String yxtsfkbz;//允许特殊放款标志
    private BigDecimal zuiglilv;//最高利率
    private BigDecimal zuidlilv;//最低利率
    private BigDecimal zuigshfu;//最高上浮
    private BigDecimal zuidixfu;//最低下浮
    private String zclilvbh;//正常利率编号
    private String lilvleix;//利率类型
    private String llqxkdfs;//利率期限靠档方式
    private String nyuelilv;//年/月利率标识
    private BigDecimal zhchlilv;//正常利率
    private String lilvtzfs;//利率调整方式
    private String lilvtzzq;//利率调整周期
    private String lilvfdfs;//利率浮动方式
    private BigDecimal lilvfdzh;//利率浮动值
    private String yuqillbh;//逾期利率编号
    private String yuqinyll;//逾期年月利率
    private BigDecimal yuqililv;//逾期利率
    private String yqfxfdfs;//逾期罚息浮动方式
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    private String jixiguiz;//计息规则
    private String ltjixigz;//零头计息规则
    private String zaoqixbz;//早起息标志
    private String wanqixbz;//晚起息标志
    private String yushxfsh;//预收息方式
    private String lixitxzq;//利息摊销周期
    private String meictxfs;//每次摊销方式
    private BigDecimal meictxbl;//每次摊销比例
    private String kxqzyqgz;//宽限期转逾期规则
    private Integer kxqzdcsh;//宽限期最大次数
    private String jixibzhi;//计息标志
    private String jfxibzhi;//计复息标志
    private String fxjxbzhi;//复息计息标志
    private String qigscfsh;//期供生成方式
    private String huankfsh;//还款方式
    private String dechligz;//等额处理规则
    private BigDecimal leijinzh;//累进值
    private Integer leijqjsh;//累进区间期数
    private String hkzhouqi;//还款周期
    private String duophkbz;//多频率还款标志
    private String huanbzhq;//还本周期
    private String yuqhkzhq;//逾期还款周期
    private String hkshxubh;//还款顺序编号
    private String yunxtqhk;//允许提前还款
    private String qxbgtzjh;//期限变更调整计划
    private String llbgtzjh;//利率变更调整计划
    private String dcfktzjh;//多次放款调整计划
    private String tqhktzjh;//提前还款调整计划
    private String zdkoukbz;//自动扣款标志
    private String zdjqdkbz;//自动结清贷款标志
    private String dhkzhhbz;//多还款账户标志
    private String qyxhdkbz;//签约循环贷款标志
    private String zuicdkqx;//最长贷款期限(月)
    private String zuiddkqx;//最短贷款期限(月)
    private String yunxdkzq;//允许贷款展期
    private String zhqxzekk;//展期需足额扣款
    private Integer zhqzdcsh;//展期最大次数
    private String zqgzbhao;//展期规则编号
    private String zbjrsygz;//贷款到期假日规则
    private String zidxtzhy;//自动形态转移
    private String lixizcgz;//利息转出规则
    private String lixizhgz;//利息转回规则
    private String hesuanfs;//核算方式
    private String ysywleix;//衍生业务类型
    private String dlhesfsh;//代理核算方式
    private String dlxxzdgz;//代理信息指定规则
    private String dlxxqzgz;//代理信息取值规则
    private String shfleixi;//收费类型
    private String tqhkfjbh;//提前还款罚金编号
    private String tqhkfjmc;//提前还款罚金名称
    private String zhchtqtz;//正常提前通知
    private Integer tzhtqtsh;//通知提前天数
    private String yqcshtzh;//逾期催收通知
    private Integer tzhjgtsh;//通知间隔天数
    private String lilvbgtz;//利率变更通知
    private String yuebgtzh;//余额变更通知
    private String beizhuxx;//备注
    private String yewusx01;//业务属性1
    private String yewusx02;//业务属性2
    private String yewusx03;//业务属性3
    private String yewusx04;//业务属性4
    private String yewusx05;//业务属性5
    private String yewusx06;//业务属性6
    private String yewusx07;//业务属性7
    private String yewusx08;//业务属性8
    private String yewusx09;//业务属性9
    private String yewusx10;//业务属性10
    private Lstcpkjlb_ARRAY lstcpkjlb_ARRAY;//贷款产品会计类别对象
    private Lstcpgndx_ARRAY lstcpgndx_ARRAY;//贷款产品功能对象
    private Lstcpsyfw_ARRAY lstcpsyfw_ARRAY;//机构控制信息对象
    private Lstcpqskz_ARRAY lstcpqskz_ARRAY;//产品缺省控制对象
    private Lstcphkfs_ARRAY lstcphkfs_ARRAY;//贷款产品还款方式组合对象
    private Lstcpsfsj_ARRAY lstcpsfsj_ARRAY;//贷款产品收费事件定义属性对象
    private Lstcpfwdj_ARRAY lstcpfwdj_ARRAY;//产品服务登记
    private String chanpmsh;//产品描述
    private String daikdxxf;//贷款对象细分
    private String bwchapbz;//表外产品
    private String xunhdaik;//循环贷款
    private String yansdaik;//衍生贷款
    private String chendaik;//承诺贷款
    private String butidaik;//补贴贷款
    private String yintdkbz;//银团贷款标志
    private String yintdkfs;//银团贷款方式
    private String yintleib;//银团类别
    private String yintnbcy;//内部银团成员类型
    private String yintwbcy;//外部银团成员类型
    private String jiangulx;//产品适用监管类型
    private String zhqizcqx;//展期最长期限(月)
    private String fangkulx;//放款类型
    private String fkzjclfs;//放款资金处理方式
    private String hntmifku;//允许对行内同名账户放款
    private String hnftmfku;//允许对行内非同名账户放款
    private String neibufku;//允许对内部账户放款
    private String lilvqixx;//利率期限
    private String yuqitzfs;//逾期利率调整方式
    private String yuqitzzq;//逾期利率调整周期
    private String fulilvbh;//复利利率编号
    private String fulilvny;//复利利率年月标识
    private BigDecimal fulililv;//复利利率
    private String fulitzfs;//复利利率调整方式
    private String fulitzzq;//复利利率调整周期
    private String fulifdfs;//复利利率浮动方式
    private BigDecimal fulifdzh;//复利利率浮动值
    private String jitiguiz;//计提规则
    private Integer zqxizdds;//早起息最大天数
    private Integer wqxizdds;//晚起息最大天数
    private String jixibjgz;//计息本金规则
    private String lixijsff;//利息计算方法
    private String jixitwgz;//计息头尾规则
    private BigDecimal jixizxje;//计息最小金额
    private String jixisrgz;//计息舍入规则
    private String srzxdanw;//舍入最小单位
    private String fdjixibz;//分段计息标志
    private String butijejs;//补贴金额计算方式
    private String tiaozhkf;//允许调整还款方式
    private String scihkrbz;//首次还款日模式
    private String mqihkfsh;//末期还款方式
    private String yunxsuoq;//允许缩期
    private Integer sqizdcsh;//缩期最大次数
    private String bzuekkfs;//不足额扣款方式
    private String yqbzkkfs;//逾期不足额扣款方式
    private String hntmihku;//允许行内同名账户还款
    private String hnftmhku;//允许行内非同名帐户还款
    private String nbuzhhku;//允许内部账户还款
    private Integer tiqhksdq;//提前还款锁定期
    private String sfyxkuxq;//是否有宽限期
    private Integer zdkuanxq;//最大宽限期天数
    private Integer kuanxqts;//宽限期天数
    private String kxqjixgz;//宽限期计息方式
    private String kxqhjxgz;//宽限期后计息规则
    private String kxqshxgz;//宽限期收息规则
    private String kxqjjrgz;//宽限期节假日规则
    private String ysfyywbm;//应收费用业务编码
    private String ysfjywbm;//应收罚金业务编码
    private String yffyywbm;//应付费用业务编码
    private String yffjywbm;//应付罚金业务编码
    private String dfbjywbm;//待付款本金业务编码
    private String dflxywbm;//待付款利息业务编码
    private String yywsywbm;//营业外收入业务编码
    private String yywzywbm;//营业外支出业务编码
    private String bjghywbm;//本金归还入账业务编码
    private String lxghywbm;//利息归还入账业务编码
    private String yqllcklx;//逾期利率参考类型
    private String flllcklx;//复利利率参考类型
    private String qglxleix;//期供利息类型
    private String wtckywbm;//委托存款业务编码
    private String shtzfhxm;//受托支付业务编码
    private String fkjzhfsh;//放款记账方式
    private String bwhesdma;//表外核算码
    private String yqzdzjbz;//逾期自动追缴标志

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getShxiaorq() {
        return shxiaorq;
    }

    public void setShxiaorq(String shxiaorq) {
        this.shxiaorq = shxiaorq;
    }

    public String getChanpzht() {
        return chanpzht;
    }

    public void setChanpzht(String chanpzht) {
        this.chanpzht = chanpzht;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getYjfyjhes() {
        return yjfyjhes;
    }

    public void setYjfyjhes(String yjfyjhes) {
        this.yjfyjhes = yjfyjhes;
    }

    public String getYiyldhes() {
        return yiyldhes;
    }

    public void setYiyldhes(String yiyldhes) {
        this.yiyldhes = yiyldhes;
    }

    public String getDkxtkmhs() {
        return dkxtkmhs;
    }

    public void setDkxtkmhs(String dkxtkmhs) {
        this.dkxtkmhs = dkxtkmhs;
    }

    public String getChanpzdm() {
        return chanpzdm;
    }

    public void setChanpzdm(String chanpzdm) {
        this.chanpzdm = chanpzdm;
    }

    public String getChanpzmc() {
        return chanpzmc;
    }

    public void setChanpzmc(String chanpzmc) {
        this.chanpzmc = chanpzmc;
    }

    public String getJigshybz() {
        return jigshybz;
    }

    public void setJigshybz(String jigshybz) {
        this.jigshybz = jigshybz;
    }

    public BigDecimal getZuidfkje() {
        return zuidfkje;
    }

    public void setZuidfkje(BigDecimal zuidfkje) {
        this.zuidfkje = zuidfkje;
    }

    public BigDecimal getZuixfkje() {
        return zuixfkje;
    }

    public void setZuixfkje(BigDecimal zuixfkje) {
        this.zuixfkje = zuixfkje;
    }

    public BigDecimal getMeiczdje() {
        return meiczdje;
    }

    public void setMeiczdje(BigDecimal meiczdje) {
        this.meiczdje = meiczdje;
    }

    public BigDecimal getMeiczxje() {
        return meiczxje;
    }

    public void setMeiczxje(BigDecimal meiczxje) {
        this.meiczxje = meiczxje;
    }

    public Integer getFkzuidjg() {
        return fkzuidjg;
    }

    public void setFkzuidjg(Integer fkzuidjg) {
        this.fkzuidjg = fkzuidjg;
    }

    public Integer getFkzuixjg() {
        return fkzuixjg;
    }

    public void setFkzuixjg(Integer fkzuixjg) {
        this.fkzuixjg = fkzuixjg;
    }

    public Integer getZuidfkcs() {
        return zuidfkcs;
    }

    public void setZuidfkcs(Integer zuidfkcs) {
        this.zuidfkcs = zuidfkcs;
    }

    public Integer getFkyouxqx() {
        return fkyouxqx;
    }

    public void setFkyouxqx(Integer fkyouxqx) {
        this.fkyouxqx = fkyouxqx;
    }

    public String getZidfkbzh() {
        return zidfkbzh;
    }

    public void setZidfkbzh(String zidfkbzh) {
        this.zidfkbzh = zidfkbzh;
    }

    public String getZdfkjjms() {
        return zdfkjjms;
    }

    public void setZdfkjjms(String zdfkjjms) {
        this.zdfkjjms = zdfkjjms;
    }

    public String getZhqifkbz() {
        return zhqifkbz;
    }

    public void setZhqifkbz(String zhqifkbz) {
        this.zhqifkbz = zhqifkbz;
    }

    public String getFkzhouqi() {
        return fkzhouqi;
    }

    public void setFkzhouqi(String fkzhouqi) {
        this.fkzhouqi = fkzhouqi;
    }

    public String getFkfangsh() {
        return fkfangsh;
    }

    public void setFkfangsh(String fkfangsh) {
        this.fkfangsh = fkfangsh;
    }

    public BigDecimal getMcfkjebl() {
        return mcfkjebl;
    }

    public void setMcfkjebl(BigDecimal mcfkjebl) {
        this.mcfkjebl = mcfkjebl;
    }

    public String getJxhjdkkz() {
        return jxhjdkkz;
    }

    public void setJxhjdkkz(String jxhjdkkz) {
        this.jxhjdkkz = jxhjdkkz;
    }

    public String getJxhjhkkz() {
        return jxhjhkkz;
    }

    public void setJxhjhkkz(String jxhjhkkz) {
        this.jxhjhkkz = jxhjhkkz;
    }

    public String getYxtsfkbz() {
        return yxtsfkbz;
    }

    public void setYxtsfkbz(String yxtsfkbz) {
        this.yxtsfkbz = yxtsfkbz;
    }

    public BigDecimal getZuiglilv() {
        return zuiglilv;
    }

    public void setZuiglilv(BigDecimal zuiglilv) {
        this.zuiglilv = zuiglilv;
    }

    public BigDecimal getZuidlilv() {
        return zuidlilv;
    }

    public void setZuidlilv(BigDecimal zuidlilv) {
        this.zuidlilv = zuidlilv;
    }

    public BigDecimal getZuigshfu() {
        return zuigshfu;
    }

    public void setZuigshfu(BigDecimal zuigshfu) {
        this.zuigshfu = zuigshfu;
    }

    public BigDecimal getZuidixfu() {
        return zuidixfu;
    }

    public void setZuidixfu(BigDecimal zuidixfu) {
        this.zuidixfu = zuidixfu;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public String getLtjixigz() {
        return ltjixigz;
    }

    public void setLtjixigz(String ltjixigz) {
        this.ltjixigz = ltjixigz;
    }

    public String getZaoqixbz() {
        return zaoqixbz;
    }

    public void setZaoqixbz(String zaoqixbz) {
        this.zaoqixbz = zaoqixbz;
    }

    public String getWanqixbz() {
        return wanqixbz;
    }

    public void setWanqixbz(String wanqixbz) {
        this.wanqixbz = wanqixbz;
    }

    public String getYushxfsh() {
        return yushxfsh;
    }

    public void setYushxfsh(String yushxfsh) {
        this.yushxfsh = yushxfsh;
    }

    public String getLixitxzq() {
        return lixitxzq;
    }

    public void setLixitxzq(String lixitxzq) {
        this.lixitxzq = lixitxzq;
    }

    public String getMeictxfs() {
        return meictxfs;
    }

    public void setMeictxfs(String meictxfs) {
        this.meictxfs = meictxfs;
    }

    public BigDecimal getMeictxbl() {
        return meictxbl;
    }

    public void setMeictxbl(BigDecimal meictxbl) {
        this.meictxbl = meictxbl;
    }

    public String getKxqzyqgz() {
        return kxqzyqgz;
    }

    public void setKxqzyqgz(String kxqzyqgz) {
        this.kxqzyqgz = kxqzyqgz;
    }

    public Integer getKxqzdcsh() {
        return kxqzdcsh;
    }

    public void setKxqzdcsh(Integer kxqzdcsh) {
        this.kxqzdcsh = kxqzdcsh;
    }

    public String getJixibzhi() {
        return jixibzhi;
    }

    public void setJixibzhi(String jixibzhi) {
        this.jixibzhi = jixibzhi;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFxjxbzhi() {
        return fxjxbzhi;
    }

    public void setFxjxbzhi(String fxjxbzhi) {
        this.fxjxbzhi = fxjxbzhi;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getZdjqdkbz() {
        return zdjqdkbz;
    }

    public void setZdjqdkbz(String zdjqdkbz) {
        this.zdjqdkbz = zdjqdkbz;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public String getQyxhdkbz() {
        return qyxhdkbz;
    }

    public void setQyxhdkbz(String qyxhdkbz) {
        this.qyxhdkbz = qyxhdkbz;
    }

    public String getZuicdkqx() {
        return zuicdkqx;
    }

    public void setZuicdkqx(String zuicdkqx) {
        this.zuicdkqx = zuicdkqx;
    }

    public String getZuiddkqx() {
        return zuiddkqx;
    }

    public void setZuiddkqx(String zuiddkqx) {
        this.zuiddkqx = zuiddkqx;
    }

    public String getYunxdkzq() {
        return yunxdkzq;
    }

    public void setYunxdkzq(String yunxdkzq) {
        this.yunxdkzq = yunxdkzq;
    }

    public String getZhqxzekk() {
        return zhqxzekk;
    }

    public void setZhqxzekk(String zhqxzekk) {
        this.zhqxzekk = zhqxzekk;
    }

    public Integer getZhqzdcsh() {
        return zhqzdcsh;
    }

    public void setZhqzdcsh(Integer zhqzdcsh) {
        this.zhqzdcsh = zhqzdcsh;
    }

    public String getZqgzbhao() {
        return zqgzbhao;
    }

    public void setZqgzbhao(String zqgzbhao) {
        this.zqgzbhao = zqgzbhao;
    }

    public String getZbjrsygz() {
        return zbjrsygz;
    }

    public void setZbjrsygz(String zbjrsygz) {
        this.zbjrsygz = zbjrsygz;
    }

    public String getZidxtzhy() {
        return zidxtzhy;
    }

    public void setZidxtzhy(String zidxtzhy) {
        this.zidxtzhy = zidxtzhy;
    }

    public String getLixizcgz() {
        return lixizcgz;
    }

    public void setLixizcgz(String lixizcgz) {
        this.lixizcgz = lixizcgz;
    }

    public String getLixizhgz() {
        return lixizhgz;
    }

    public void setLixizhgz(String lixizhgz) {
        this.lixizhgz = lixizhgz;
    }

    public String getHesuanfs() {
        return hesuanfs;
    }

    public void setHesuanfs(String hesuanfs) {
        this.hesuanfs = hesuanfs;
    }

    public String getYsywleix() {
        return ysywleix;
    }

    public void setYsywleix(String ysywleix) {
        this.ysywleix = ysywleix;
    }

    public String getDlhesfsh() {
        return dlhesfsh;
    }

    public void setDlhesfsh(String dlhesfsh) {
        this.dlhesfsh = dlhesfsh;
    }

    public String getDlxxzdgz() {
        return dlxxzdgz;
    }

    public void setDlxxzdgz(String dlxxzdgz) {
        this.dlxxzdgz = dlxxzdgz;
    }

    public String getDlxxqzgz() {
        return dlxxqzgz;
    }

    public void setDlxxqzgz(String dlxxqzgz) {
        this.dlxxqzgz = dlxxqzgz;
    }

    public String getShfleixi() {
        return shfleixi;
    }

    public void setShfleixi(String shfleixi) {
        this.shfleixi = shfleixi;
    }

    public String getTqhkfjbh() {
        return tqhkfjbh;
    }

    public void setTqhkfjbh(String tqhkfjbh) {
        this.tqhkfjbh = tqhkfjbh;
    }

    public String getTqhkfjmc() {
        return tqhkfjmc;
    }

    public void setTqhkfjmc(String tqhkfjmc) {
        this.tqhkfjmc = tqhkfjmc;
    }

    public String getZhchtqtz() {
        return zhchtqtz;
    }

    public void setZhchtqtz(String zhchtqtz) {
        this.zhchtqtz = zhchtqtz;
    }

    public Integer getTzhtqtsh() {
        return tzhtqtsh;
    }

    public void setTzhtqtsh(Integer tzhtqtsh) {
        this.tzhtqtsh = tzhtqtsh;
    }

    public String getYqcshtzh() {
        return yqcshtzh;
    }

    public void setYqcshtzh(String yqcshtzh) {
        this.yqcshtzh = yqcshtzh;
    }

    public Integer getTzhjgtsh() {
        return tzhjgtsh;
    }

    public void setTzhjgtsh(Integer tzhjgtsh) {
        this.tzhjgtsh = tzhjgtsh;
    }

    public String getLilvbgtz() {
        return lilvbgtz;
    }

    public void setLilvbgtz(String lilvbgtz) {
        this.lilvbgtz = lilvbgtz;
    }

    public String getYuebgtzh() {
        return yuebgtzh;
    }

    public void setYuebgtzh(String yuebgtzh) {
        this.yuebgtzh = yuebgtzh;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYewusx02() {
        return yewusx02;
    }

    public void setYewusx02(String yewusx02) {
        this.yewusx02 = yewusx02;
    }

    public String getYewusx03() {
        return yewusx03;
    }

    public void setYewusx03(String yewusx03) {
        this.yewusx03 = yewusx03;
    }

    public String getYewusx04() {
        return yewusx04;
    }

    public void setYewusx04(String yewusx04) {
        this.yewusx04 = yewusx04;
    }

    public String getYewusx05() {
        return yewusx05;
    }

    public void setYewusx05(String yewusx05) {
        this.yewusx05 = yewusx05;
    }

    public String getYewusx06() {
        return yewusx06;
    }

    public void setYewusx06(String yewusx06) {
        this.yewusx06 = yewusx06;
    }

    public String getYewusx07() {
        return yewusx07;
    }

    public void setYewusx07(String yewusx07) {
        this.yewusx07 = yewusx07;
    }

    public String getYewusx08() {
        return yewusx08;
    }

    public void setYewusx08(String yewusx08) {
        this.yewusx08 = yewusx08;
    }

    public String getYewusx09() {
        return yewusx09;
    }

    public void setYewusx09(String yewusx09) {
        this.yewusx09 = yewusx09;
    }

    public String getYewusx10() {
        return yewusx10;
    }

    public void setYewusx10(String yewusx10) {
        this.yewusx10 = yewusx10;
    }

    public Lstcpkjlb_ARRAY getLstcpkjlb_ARRAY() {
        return lstcpkjlb_ARRAY;
    }

    public void setLstcpkjlb_ARRAY(Lstcpkjlb_ARRAY lstcpkjlb_ARRAY) {
        this.lstcpkjlb_ARRAY = lstcpkjlb_ARRAY;
    }

    public Lstcpgndx_ARRAY getLstcpgndx_ARRAY() {
        return lstcpgndx_ARRAY;
    }

    public void setLstcpgndx_ARRAY(Lstcpgndx_ARRAY lstcpgndx_ARRAY) {
        this.lstcpgndx_ARRAY = lstcpgndx_ARRAY;
    }

    public Lstcpsyfw_ARRAY getLstcpsyfw_ARRAY() {
        return lstcpsyfw_ARRAY;
    }

    public void setLstcpsyfw_ARRAY(Lstcpsyfw_ARRAY lstcpsyfw_ARRAY) {
        this.lstcpsyfw_ARRAY = lstcpsyfw_ARRAY;
    }

    public Lstcpqskz_ARRAY getLstcpqskz_ARRAY() {
        return lstcpqskz_ARRAY;
    }

    public void setLstcpqskz_ARRAY(Lstcpqskz_ARRAY lstcpqskz_ARRAY) {
        this.lstcpqskz_ARRAY = lstcpqskz_ARRAY;
    }

    public Lstcphkfs_ARRAY getLstcphkfs_ARRAY() {
        return lstcphkfs_ARRAY;
    }

    public void setLstcphkfs_ARRAY(Lstcphkfs_ARRAY lstcphkfs_ARRAY) {
        this.lstcphkfs_ARRAY = lstcphkfs_ARRAY;
    }

    public Lstcpsfsj_ARRAY getLstcpsfsj_ARRAY() {
        return lstcpsfsj_ARRAY;
    }

    public void setLstcpsfsj_ARRAY(Lstcpsfsj_ARRAY lstcpsfsj_ARRAY) {
        this.lstcpsfsj_ARRAY = lstcpsfsj_ARRAY;
    }

    public Lstcpfwdj_ARRAY getLstcpfwdj_ARRAY() {
        return lstcpfwdj_ARRAY;
    }

    public void setLstcpfwdj_ARRAY(Lstcpfwdj_ARRAY lstcpfwdj_ARRAY) {
        this.lstcpfwdj_ARRAY = lstcpfwdj_ARRAY;
    }

    public String getChanpmsh() {
        return chanpmsh;
    }

    public void setChanpmsh(String chanpmsh) {
        this.chanpmsh = chanpmsh;
    }

    public String getDaikdxxf() {
        return daikdxxf;
    }

    public void setDaikdxxf(String daikdxxf) {
        this.daikdxxf = daikdxxf;
    }

    public String getBwchapbz() {
        return bwchapbz;
    }

    public void setBwchapbz(String bwchapbz) {
        this.bwchapbz = bwchapbz;
    }

    public String getXunhdaik() {
        return xunhdaik;
    }

    public void setXunhdaik(String xunhdaik) {
        this.xunhdaik = xunhdaik;
    }

    public String getYansdaik() {
        return yansdaik;
    }

    public void setYansdaik(String yansdaik) {
        this.yansdaik = yansdaik;
    }

    public String getChendaik() {
        return chendaik;
    }

    public void setChendaik(String chendaik) {
        this.chendaik = chendaik;
    }

    public String getButidaik() {
        return butidaik;
    }

    public void setButidaik(String butidaik) {
        this.butidaik = butidaik;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getYintdkfs() {
        return yintdkfs;
    }

    public void setYintdkfs(String yintdkfs) {
        this.yintdkfs = yintdkfs;
    }

    public String getYintleib() {
        return yintleib;
    }

    public void setYintleib(String yintleib) {
        this.yintleib = yintleib;
    }

    public String getYintnbcy() {
        return yintnbcy;
    }

    public void setYintnbcy(String yintnbcy) {
        this.yintnbcy = yintnbcy;
    }

    public String getYintwbcy() {
        return yintwbcy;
    }

    public void setYintwbcy(String yintwbcy) {
        this.yintwbcy = yintwbcy;
    }

    public String getJiangulx() {
        return jiangulx;
    }

    public void setJiangulx(String jiangulx) {
        this.jiangulx = jiangulx;
    }

    public String getZhqizcqx() {
        return zhqizcqx;
    }

    public void setZhqizcqx(String zhqizcqx) {
        this.zhqizcqx = zhqizcqx;
    }

    public String getFangkulx() {
        return fangkulx;
    }

    public void setFangkulx(String fangkulx) {
        this.fangkulx = fangkulx;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getHntmifku() {
        return hntmifku;
    }

    public void setHntmifku(String hntmifku) {
        this.hntmifku = hntmifku;
    }

    public String getHnftmfku() {
        return hnftmfku;
    }

    public void setHnftmfku(String hnftmfku) {
        this.hnftmfku = hnftmfku;
    }

    public String getNeibufku() {
        return neibufku;
    }

    public void setNeibufku(String neibufku) {
        this.neibufku = neibufku;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getJitiguiz() {
        return jitiguiz;
    }

    public void setJitiguiz(String jitiguiz) {
        this.jitiguiz = jitiguiz;
    }

    public Integer getZqxizdds() {
        return zqxizdds;
    }

    public void setZqxizdds(Integer zqxizdds) {
        this.zqxizdds = zqxizdds;
    }

    public Integer getWqxizdds() {
        return wqxizdds;
    }

    public void setWqxizdds(Integer wqxizdds) {
        this.wqxizdds = wqxizdds;
    }

    public String getJixibjgz() {
        return jixibjgz;
    }

    public void setJixibjgz(String jixibjgz) {
        this.jixibjgz = jixibjgz;
    }

    public String getLixijsff() {
        return lixijsff;
    }

    public void setLixijsff(String lixijsff) {
        this.lixijsff = lixijsff;
    }

    public String getJixitwgz() {
        return jixitwgz;
    }

    public void setJixitwgz(String jixitwgz) {
        this.jixitwgz = jixitwgz;
    }

    public BigDecimal getJixizxje() {
        return jixizxje;
    }

    public void setJixizxje(BigDecimal jixizxje) {
        this.jixizxje = jixizxje;
    }

    public String getJixisrgz() {
        return jixisrgz;
    }

    public void setJixisrgz(String jixisrgz) {
        this.jixisrgz = jixisrgz;
    }

    public String getSrzxdanw() {
        return srzxdanw;
    }

    public void setSrzxdanw(String srzxdanw) {
        this.srzxdanw = srzxdanw;
    }

    public String getFdjixibz() {
        return fdjixibz;
    }

    public void setFdjixibz(String fdjixibz) {
        this.fdjixibz = fdjixibz;
    }

    public String getButijejs() {
        return butijejs;
    }

    public void setButijejs(String butijejs) {
        this.butijejs = butijejs;
    }

    public String getTiaozhkf() {
        return tiaozhkf;
    }

    public void setTiaozhkf(String tiaozhkf) {
        this.tiaozhkf = tiaozhkf;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public String getYunxsuoq() {
        return yunxsuoq;
    }

    public void setYunxsuoq(String yunxsuoq) {
        this.yunxsuoq = yunxsuoq;
    }

    public Integer getSqizdcsh() {
        return sqizdcsh;
    }

    public void setSqizdcsh(Integer sqizdcsh) {
        this.sqizdcsh = sqizdcsh;
    }

    public String getBzuekkfs() {
        return bzuekkfs;
    }

    public void setBzuekkfs(String bzuekkfs) {
        this.bzuekkfs = bzuekkfs;
    }

    public String getYqbzkkfs() {
        return yqbzkkfs;
    }

    public void setYqbzkkfs(String yqbzkkfs) {
        this.yqbzkkfs = yqbzkkfs;
    }

    public String getHntmihku() {
        return hntmihku;
    }

    public void setHntmihku(String hntmihku) {
        this.hntmihku = hntmihku;
    }

    public String getHnftmhku() {
        return hnftmhku;
    }

    public void setHnftmhku(String hnftmhku) {
        this.hnftmhku = hnftmhku;
    }

    public String getNbuzhhku() {
        return nbuzhhku;
    }

    public void setNbuzhhku(String nbuzhhku) {
        this.nbuzhhku = nbuzhhku;
    }

    public Integer getTiqhksdq() {
        return tiqhksdq;
    }

    public void setTiqhksdq(Integer tiqhksdq) {
        this.tiqhksdq = tiqhksdq;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getZdkuanxq() {
        return zdkuanxq;
    }

    public void setZdkuanxq(Integer zdkuanxq) {
        this.zdkuanxq = zdkuanxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public String getKxqjixgz() {
        return kxqjixgz;
    }

    public void setKxqjixgz(String kxqjixgz) {
        this.kxqjixgz = kxqjixgz;
    }

    public String getKxqhjxgz() {
        return kxqhjxgz;
    }

    public void setKxqhjxgz(String kxqhjxgz) {
        this.kxqhjxgz = kxqhjxgz;
    }

    public String getKxqshxgz() {
        return kxqshxgz;
    }

    public void setKxqshxgz(String kxqshxgz) {
        this.kxqshxgz = kxqshxgz;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getYsfyywbm() {
        return ysfyywbm;
    }

    public void setYsfyywbm(String ysfyywbm) {
        this.ysfyywbm = ysfyywbm;
    }

    public String getYsfjywbm() {
        return ysfjywbm;
    }

    public void setYsfjywbm(String ysfjywbm) {
        this.ysfjywbm = ysfjywbm;
    }

    public String getYffyywbm() {
        return yffyywbm;
    }

    public void setYffyywbm(String yffyywbm) {
        this.yffyywbm = yffyywbm;
    }

    public String getYffjywbm() {
        return yffjywbm;
    }

    public void setYffjywbm(String yffjywbm) {
        this.yffjywbm = yffjywbm;
    }

    public String getDfbjywbm() {
        return dfbjywbm;
    }

    public void setDfbjywbm(String dfbjywbm) {
        this.dfbjywbm = dfbjywbm;
    }

    public String getDflxywbm() {
        return dflxywbm;
    }

    public void setDflxywbm(String dflxywbm) {
        this.dflxywbm = dflxywbm;
    }

    public String getYywsywbm() {
        return yywsywbm;
    }

    public void setYywsywbm(String yywsywbm) {
        this.yywsywbm = yywsywbm;
    }

    public String getYywzywbm() {
        return yywzywbm;
    }

    public void setYywzywbm(String yywzywbm) {
        this.yywzywbm = yywzywbm;
    }

    public String getBjghywbm() {
        return bjghywbm;
    }

    public void setBjghywbm(String bjghywbm) {
        this.bjghywbm = bjghywbm;
    }

    public String getLxghywbm() {
        return lxghywbm;
    }

    public void setLxghywbm(String lxghywbm) {
        this.lxghywbm = lxghywbm;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getQglxleix() {
        return qglxleix;
    }

    public void setQglxleix(String qglxleix) {
        this.qglxleix = qglxleix;
    }

    public String getWtckywbm() {
        return wtckywbm;
    }

    public void setWtckywbm(String wtckywbm) {
        this.wtckywbm = wtckywbm;
    }

    public String getShtzfhxm() {
        return shtzfhxm;
    }

    public void setShtzfhxm(String shtzfhxm) {
        this.shtzfhxm = shtzfhxm;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public String getBwhesdma() {
        return bwhesdma;
    }

    public void setBwhesdma(String bwhesdma) {
        this.bwhesdma = bwhesdma;
    }

    public String getYqzdzjbz() {
        return yqzdzjbz;
    }

    public void setYqzdzjbz(String yqzdzjbz) {
        this.yqzdzjbz = yqzdzjbz;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", shengxrq='" + shengxrq + '\'' +
                ", shxiaorq='" + shxiaorq + '\'' +
                ", chanpzht='" + chanpzht + '\'' +
                ", daikduix='" + daikduix + '\'' +
                ", yewufenl='" + yewufenl + '\'' +
                ", yjfyjhes='" + yjfyjhes + '\'' +
                ", yiyldhes='" + yiyldhes + '\'' +
                ", dkxtkmhs='" + dkxtkmhs + '\'' +
                ", chanpzdm='" + chanpzdm + '\'' +
                ", chanpzmc='" + chanpzmc + '\'' +
                ", jigshybz='" + jigshybz + '\'' +
                ", zuidfkje=" + zuidfkje +
                ", zuixfkje=" + zuixfkje +
                ", meiczdje=" + meiczdje +
                ", meiczxje=" + meiczxje +
                ", fkzuidjg=" + fkzuidjg +
                ", fkzuixjg=" + fkzuixjg +
                ", zuidfkcs=" + zuidfkcs +
                ", fkyouxqx=" + fkyouxqx +
                ", zidfkbzh='" + zidfkbzh + '\'' +
                ", zdfkjjms='" + zdfkjjms + '\'' +
                ", zhqifkbz='" + zhqifkbz + '\'' +
                ", fkzhouqi='" + fkzhouqi + '\'' +
                ", fkfangsh='" + fkfangsh + '\'' +
                ", mcfkjebl=" + mcfkjebl +
                ", jxhjdkkz='" + jxhjdkkz + '\'' +
                ", jxhjhkkz='" + jxhjhkkz + '\'' +
                ", yxtsfkbz='" + yxtsfkbz + '\'' +
                ", zuiglilv=" + zuiglilv +
                ", zuidlilv=" + zuidlilv +
                ", zuigshfu=" + zuigshfu +
                ", zuidixfu=" + zuidixfu +
                ", zclilvbh='" + zclilvbh + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", llqxkdfs='" + llqxkdfs + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", yuqillbh='" + yuqillbh + '\'' +
                ", yuqinyll='" + yuqinyll + '\'' +
                ", yuqililv=" + yuqililv +
                ", yqfxfdfs='" + yqfxfdfs + '\'' +
                ", yqfxfdzh=" + yqfxfdzh +
                ", jixiguiz='" + jixiguiz + '\'' +
                ", ltjixigz='" + ltjixigz + '\'' +
                ", zaoqixbz='" + zaoqixbz + '\'' +
                ", wanqixbz='" + wanqixbz + '\'' +
                ", yushxfsh='" + yushxfsh + '\'' +
                ", lixitxzq='" + lixitxzq + '\'' +
                ", meictxfs='" + meictxfs + '\'' +
                ", meictxbl=" + meictxbl +
                ", kxqzyqgz='" + kxqzyqgz + '\'' +
                ", kxqzdcsh=" + kxqzdcsh +
                ", jixibzhi='" + jixibzhi + '\'' +
                ", jfxibzhi='" + jfxibzhi + '\'' +
                ", fxjxbzhi='" + fxjxbzhi + '\'' +
                ", qigscfsh='" + qigscfsh + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", leijinzh=" + leijinzh +
                ", leijqjsh=" + leijqjsh +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", duophkbz='" + duophkbz + '\'' +
                ", huanbzhq='" + huanbzhq + '\'' +
                ", yuqhkzhq='" + yuqhkzhq + '\'' +
                ", hkshxubh='" + hkshxubh + '\'' +
                ", yunxtqhk='" + yunxtqhk + '\'' +
                ", qxbgtzjh='" + qxbgtzjh + '\'' +
                ", llbgtzjh='" + llbgtzjh + '\'' +
                ", dcfktzjh='" + dcfktzjh + '\'' +
                ", tqhktzjh='" + tqhktzjh + '\'' +
                ", zdkoukbz='" + zdkoukbz + '\'' +
                ", zdjqdkbz='" + zdjqdkbz + '\'' +
                ", dhkzhhbz='" + dhkzhhbz + '\'' +
                ", qyxhdkbz='" + qyxhdkbz + '\'' +
                ", zuicdkqx='" + zuicdkqx + '\'' +
                ", zuiddkqx='" + zuiddkqx + '\'' +
                ", yunxdkzq='" + yunxdkzq + '\'' +
                ", zhqxzekk='" + zhqxzekk + '\'' +
                ", zhqzdcsh=" + zhqzdcsh +
                ", zqgzbhao='" + zqgzbhao + '\'' +
                ", zbjrsygz='" + zbjrsygz + '\'' +
                ", zidxtzhy='" + zidxtzhy + '\'' +
                ", lixizcgz='" + lixizcgz + '\'' +
                ", lixizhgz='" + lixizhgz + '\'' +
                ", hesuanfs='" + hesuanfs + '\'' +
                ", ysywleix='" + ysywleix + '\'' +
                ", dlhesfsh='" + dlhesfsh + '\'' +
                ", dlxxzdgz='" + dlxxzdgz + '\'' +
                ", dlxxqzgz='" + dlxxqzgz + '\'' +
                ", shfleixi='" + shfleixi + '\'' +
                ", tqhkfjbh='" + tqhkfjbh + '\'' +
                ", tqhkfjmc='" + tqhkfjmc + '\'' +
                ", zhchtqtz='" + zhchtqtz + '\'' +
                ", tzhtqtsh=" + tzhtqtsh +
                ", yqcshtzh='" + yqcshtzh + '\'' +
                ", tzhjgtsh=" + tzhjgtsh +
                ", lilvbgtz='" + lilvbgtz + '\'' +
                ", yuebgtzh='" + yuebgtzh + '\'' +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", yewusx01='" + yewusx01 + '\'' +
                ", yewusx02='" + yewusx02 + '\'' +
                ", yewusx03='" + yewusx03 + '\'' +
                ", yewusx04='" + yewusx04 + '\'' +
                ", yewusx05='" + yewusx05 + '\'' +
                ", yewusx06='" + yewusx06 + '\'' +
                ", yewusx07='" + yewusx07 + '\'' +
                ", yewusx08='" + yewusx08 + '\'' +
                ", yewusx09='" + yewusx09 + '\'' +
                ", yewusx10='" + yewusx10 + '\'' +
                ", lstcpkjlb_ARRAY=" + lstcpkjlb_ARRAY +
                ", lstcpgndx_ARRAY=" + lstcpgndx_ARRAY +
                ", lstcpsyfw_ARRAY=" + lstcpsyfw_ARRAY +
                ", lstcpqskz_ARRAY=" + lstcpqskz_ARRAY +
                ", lstcphkfs_ARRAY=" + lstcphkfs_ARRAY +
                ", lstcpsfsj_ARRAY=" + lstcpsfsj_ARRAY +
                ", lstcpfwdj_ARRAY=" + lstcpfwdj_ARRAY +
                ", chanpmsh='" + chanpmsh + '\'' +
                ", daikdxxf='" + daikdxxf + '\'' +
                ", bwchapbz='" + bwchapbz + '\'' +
                ", xunhdaik='" + xunhdaik + '\'' +
                ", yansdaik='" + yansdaik + '\'' +
                ", chendaik='" + chendaik + '\'' +
                ", butidaik='" + butidaik + '\'' +
                ", yintdkbz='" + yintdkbz + '\'' +
                ", yintdkfs='" + yintdkfs + '\'' +
                ", yintleib='" + yintleib + '\'' +
                ", yintnbcy='" + yintnbcy + '\'' +
                ", yintwbcy='" + yintwbcy + '\'' +
                ", jiangulx='" + jiangulx + '\'' +
                ", zhqizcqx='" + zhqizcqx + '\'' +
                ", fangkulx='" + fangkulx + '\'' +
                ", fkzjclfs='" + fkzjclfs + '\'' +
                ", hntmifku='" + hntmifku + '\'' +
                ", hnftmfku='" + hnftmfku + '\'' +
                ", neibufku='" + neibufku + '\'' +
                ", lilvqixx='" + lilvqixx + '\'' +
                ", yuqitzfs='" + yuqitzfs + '\'' +
                ", yuqitzzq='" + yuqitzzq + '\'' +
                ", fulilvbh='" + fulilvbh + '\'' +
                ", fulilvny='" + fulilvny + '\'' +
                ", fulililv=" + fulililv +
                ", fulitzfs='" + fulitzfs + '\'' +
                ", fulitzzq='" + fulitzzq + '\'' +
                ", fulifdfs='" + fulifdfs + '\'' +
                ", fulifdzh=" + fulifdzh +
                ", jitiguiz='" + jitiguiz + '\'' +
                ", zqxizdds=" + zqxizdds +
                ", wqxizdds=" + wqxizdds +
                ", jixibjgz='" + jixibjgz + '\'' +
                ", lixijsff='" + lixijsff + '\'' +
                ", jixitwgz='" + jixitwgz + '\'' +
                ", jixizxje=" + jixizxje +
                ", jixisrgz='" + jixisrgz + '\'' +
                ", srzxdanw='" + srzxdanw + '\'' +
                ", fdjixibz='" + fdjixibz + '\'' +
                ", butijejs='" + butijejs + '\'' +
                ", tiaozhkf='" + tiaozhkf + '\'' +
                ", scihkrbz='" + scihkrbz + '\'' +
                ", mqihkfsh='" + mqihkfsh + '\'' +
                ", yunxsuoq='" + yunxsuoq + '\'' +
                ", sqizdcsh=" + sqizdcsh +
                ", bzuekkfs='" + bzuekkfs + '\'' +
                ", yqbzkkfs='" + yqbzkkfs + '\'' +
                ", hntmihku='" + hntmihku + '\'' +
                ", hnftmhku='" + hnftmhku + '\'' +
                ", nbuzhhku='" + nbuzhhku + '\'' +
                ", tiqhksdq=" + tiqhksdq +
                ", sfyxkuxq='" + sfyxkuxq + '\'' +
                ", zdkuanxq=" + zdkuanxq +
                ", kuanxqts=" + kuanxqts +
                ", kxqjixgz='" + kxqjixgz + '\'' +
                ", kxqhjxgz='" + kxqhjxgz + '\'' +
                ", kxqshxgz='" + kxqshxgz + '\'' +
                ", kxqjjrgz='" + kxqjjrgz + '\'' +
                ", ysfyywbm='" + ysfyywbm + '\'' +
                ", ysfjywbm='" + ysfjywbm + '\'' +
                ", yffyywbm='" + yffyywbm + '\'' +
                ", yffjywbm='" + yffjywbm + '\'' +
                ", dfbjywbm='" + dfbjywbm + '\'' +
                ", dflxywbm='" + dflxywbm + '\'' +
                ", yywsywbm='" + yywsywbm + '\'' +
                ", yywzywbm='" + yywzywbm + '\'' +
                ", bjghywbm='" + bjghywbm + '\'' +
                ", lxghywbm='" + lxghywbm + '\'' +
                ", yqllcklx='" + yqllcklx + '\'' +
                ", flllcklx='" + flllcklx + '\'' +
                ", qglxleix='" + qglxleix + '\'' +
                ", wtckywbm='" + wtckywbm + '\'' +
                ", shtzfhxm='" + shtzfhxm + '\'' +
                ", fkjzhfsh='" + fkjzhfsh + '\'' +
                ", bwhesdma='" + bwhesdma + '\'' +
                ", yqzdzjbz='" + yqzdzjbz + '\'' +
                '}';
    }
}
