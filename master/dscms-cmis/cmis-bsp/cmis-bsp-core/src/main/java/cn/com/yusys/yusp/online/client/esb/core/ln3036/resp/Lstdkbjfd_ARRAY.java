package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkbjfd.Record;

import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：本金分段登记
 *
 * @author code-generator
 * @version 1.0
 */
public class Lstdkbjfd_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkbjfd.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkbjfd_ARRAY{" +
                "record=" + record +
                '}';
    }
}
