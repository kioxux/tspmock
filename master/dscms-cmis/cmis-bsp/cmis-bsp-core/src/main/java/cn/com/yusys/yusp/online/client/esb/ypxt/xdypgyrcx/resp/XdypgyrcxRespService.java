package cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp;

/**
 * 响应Service：查询共有人信息
 *
 * @author code-generator
 * @version 1.0
 */
public class XdypgyrcxRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
