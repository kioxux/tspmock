package cn.com.yusys.yusp.web.client.esb.zjywxt.jcxxcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.req.JcxxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.resp.JcxxcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.req.JcxxcxReqService;
import cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.resp.JcxxcxRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2zjywxt")
public class Dscms2JcxxcxResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2JcxxcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：jcxxcx
     * 交易描述：
     *
     * @param jcxxcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/jcxxcx")
    protected @ResponseBody
    ResultDto<JcxxcxRespDto> jcxxcx(@Validated @RequestBody JcxxcxReqDto jcxxcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_JCXXCX.key, EsbEnum.TRADE_CODE_JCXXCX.value, JSON.toJSONString(jcxxcxReqDto));
        cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.resp.Service();
        JcxxcxReqService jcxxcxReqService = new JcxxcxReqService();
        JcxxcxRespService jcxxcxRespService = new JcxxcxRespService();
        JcxxcxRespDto jcxxcxRespDto = new JcxxcxRespDto();
        ResultDto<JcxxcxRespDto> jcxxcxResultDto = new ResultDto<JcxxcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将jcxxcxReqDto转换成reqService
            BeanUtils.copyProperties(jcxxcxReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_JCXXCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_GAP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_GAP.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            jcxxcxReqService.setService(reqService);
            // 将jcxxcxReqService转换成jcxxcxReqServiceMap
            Map jcxxcxReqServiceMap = beanMapUtil.beanToMap(jcxxcxReqService);
            context.put("tradeDataMap", jcxxcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_JCXXCX.key, EsbEnum.TRADE_CODE_JCXXCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_JCXXCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_JCXXCX.key, EsbEnum.TRADE_CODE_JCXXCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            jcxxcxRespService = beanMapUtil.mapToBean(tradeDataMap, JcxxcxRespService.class, JcxxcxRespService.class);
            respService = jcxxcxRespService.getService();

            jcxxcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            jcxxcxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成JcxxcxRespDto
                BeanUtils.copyProperties(respService, jcxxcxRespDto);
                jcxxcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                jcxxcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                jcxxcxResultDto.setCode(EpbEnum.EPB099999.key);
                jcxxcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_JCXXCX.key, EsbEnum.TRADE_CODE_JCXXCX.value, e.getMessage());
            jcxxcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            jcxxcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        jcxxcxResultDto.setData(jcxxcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_JCXXCX.key, EsbEnum.TRADE_CODE_JCXXCX.value, JSON.toJSONString(jcxxcxResultDto));
        return jcxxcxResultDto;
    }
}
