package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.math.BigDecimal;

/**
 * 请求Service：响应信息域元素:产品层债项评级结果
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class MessageInfoRecord {
    private String detail_serno; // 授信分项流水号
    private String pro_no; // 业务品种编号
    private String pro_name; // 业务品种名称
    private String guaranteeGrade; // 债项等级
    private BigDecimal ead; // 违约风险暴露
    private BigDecimal lgd; // 违约损失率

    public String getDetail_serno() {
        return detail_serno;
    }

    public void setDetail_serno(String detail_serno) {
        this.detail_serno = detail_serno;
    }

    public String getPro_no() {
        return pro_no;
    }

    public void setPro_no(String pro_no) {
        this.pro_no = pro_no;
    }

    public String getPro_name() {
        return pro_name;
    }

    public void setPro_name(String pro_name) {
        this.pro_name = pro_name;
    }

    public String getGuaranteeGrade() {
        return guaranteeGrade;
    }

    public void setGuaranteeGrade(String guaranteeGrade) {
        this.guaranteeGrade = guaranteeGrade;
    }

    public BigDecimal getEad() {
        return ead;
    }

    public void setEad(BigDecimal ead) {
        this.ead = ead;
    }

    public BigDecimal getLgd() {
        return lgd;
    }

    public void setLgd(BigDecimal lgd) {
        this.lgd = lgd;
    }

    @Override
    public String toString() {
        return "MessageInfoRecord{" +
                "detail_serno='" + detail_serno + '\'' +
                ", pro_no='" + pro_no + '\'' +
                ", pro_name='" + pro_name + '\'' +
                ", guaranteeGrade='" + guaranteeGrade + '\'' +
                ", ead=" + ead +
                ", lgd=" + lgd +
                '}';
    }
}
