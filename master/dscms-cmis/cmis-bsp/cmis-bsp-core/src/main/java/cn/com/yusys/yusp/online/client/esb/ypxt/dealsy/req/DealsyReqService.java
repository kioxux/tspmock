package cn.com.yusys.yusp.online.client.esb.ypxt.dealsy.req;

/**
 * 请求Service：押品处置信息同步
 *
 * @author leehuang
 * @version 1.0
 */
public class DealsyReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "DealsyReqService{" +
				"service=" + service +
				'}';
	}
}
