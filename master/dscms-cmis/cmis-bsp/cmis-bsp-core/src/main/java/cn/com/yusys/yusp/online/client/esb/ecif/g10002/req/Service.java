package cn.com.yusys.yusp.online.client.esb.ecif.g10002.req;

/**
 * 请求Service：客户群组信息维护
 */
public class Service {
	private String prcscd;//    交易码
	private String servtp;//    渠道
	private String servsq;//    渠道流水
	private String userid;//    柜员号
	private String brchno;//    部门号
	private String txdate;//    交易日期
	private String txtime;//    交易时间
	private String datasq;//    全局流水

	private String grouno;//群组编号
	private String grouna;//群组名称
	private String grouds;//群组描述
	private String acctbr;//管户机构
	private String acmang;//管户客户经理
	private String mxmang;//主办客户经理
	private String inptdt;//主办日期
	private String inptbr;//主办机构
	private String jinmlv;//紧密程度
	private String remark;//备注
	private String gpcsst;//群组客户状态

	private cn.com.yusys.yusp.online.client.esb.ecif.g10002.req.List list;

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getTxdate() {
		return txdate;
	}

	public void setTxdate(String txdate) {
		this.txdate = txdate;
	}

	public String getTxtime() {
		return txtime;
	}

	public void setTxtime(String txtime) {
		this.txtime = txtime;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getGrouno() {
		return grouno;
	}

	public void setGrouno(String grouno) {
		this.grouno = grouno;
	}

	public String getGrouna() {
		return grouna;
	}

	public void setGrouna(String grouna) {
		this.grouna = grouna;
	}

	public String getGrouds() {
		return grouds;
	}

	public void setGrouds(String grouds) {
		this.grouds = grouds;
	}

	public String getAcctbr() {
		return acctbr;
	}

	public void setAcctbr(String acctbr) {
		this.acctbr = acctbr;
	}

	public String getAcmang() {
		return acmang;
	}

	public void setAcmang(String acmang) {
		this.acmang = acmang;
	}

	public String getMxmang() {
		return mxmang;
	}

	public void setMxmang(String mxmang) {
		this.mxmang = mxmang;
	}

	public String getInptdt() {
		return inptdt;
	}

	public void setInptdt(String inptdt) {
		this.inptdt = inptdt;
	}

	public String getInptbr() {
		return inptbr;
	}

	public void setInptbr(String inptbr) {
		this.inptbr = inptbr;
	}

	public String getJinmlv() {
		return jinmlv;
	}

	public void setJinmlv(String jinmlv) {
		this.jinmlv = jinmlv;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getGpcsst() {
		return gpcsst;
	}

	public void setGpcsst(String gpcsst) {
		this.gpcsst = gpcsst;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", txdate='" + txdate + '\'' +
				", txtime='" + txtime + '\'' +
				", datasq='" + datasq + '\'' +
				", grouno='" + grouno + '\'' +
				", grouna='" + grouna + '\'' +
				", grouds='" + grouds + '\'' +
				", acctbr='" + acctbr + '\'' +
				", acmang='" + acmang + '\'' +
				", mxmang='" + mxmang + '\'' +
				", inptdt='" + inptdt + '\'' +
				", inptbr='" + inptbr + '\'' +
				", jinmlv='" + jinmlv + '\'' +
				", remark='" + remark + '\'' +
				", gpcsst='" + gpcsst + '\'' +
				", list=" + list +
				'}';
	}
}
