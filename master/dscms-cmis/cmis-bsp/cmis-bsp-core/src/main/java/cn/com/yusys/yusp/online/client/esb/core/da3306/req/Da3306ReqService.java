package cn.com.yusys.yusp.online.client.esb.core.da3306.req;

/**
 * 请求Service：抵债资产拨备计提
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3306ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Da3306ReqService{" +
				"service=" + service +
				'}';
	}
}
