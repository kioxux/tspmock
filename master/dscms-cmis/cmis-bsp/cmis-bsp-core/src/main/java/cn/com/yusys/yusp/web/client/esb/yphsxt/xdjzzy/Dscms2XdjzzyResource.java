package cn.com.yusys.yusp.web.client.esb.yphsxt.xdjzzy;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req.List;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req.XdjzzyReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.resp.XdjzzyRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.XdjzzyReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.resp.XdjzzyRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:查询基本信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2XdjzzyResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2XdjzzyResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdjzzy
     * 交易描述：查询基本信息
     *
     * @param xdjzzyReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdjzzy")
    protected @ResponseBody
    ResultDto<XdjzzyRespDto> xdjzzy(@Validated @RequestBody XdjzzyReqDto xdjzzyReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDJZZY.key, EsbEnum.TRADE_CODE_XDJZZY.value, JSON.toJSONString(xdjzzyReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.resp.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.List targetList = new cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.List();
        XdjzzyReqService xdjzzyReqService = new XdjzzyReqService();
        XdjzzyRespService xdjzzyRespService = new XdjzzyRespService();
        XdjzzyRespDto xdjzzyRespDto = new XdjzzyRespDto();
        ResultDto<XdjzzyRespDto> xdjzzyResultDto = new ResultDto<XdjzzyRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdjzzyReqDto转换成reqService
            BeanUtils.copyProperties(xdjzzyReqDto, reqService);
            if (CollectionUtils.nonEmpty(xdjzzyReqDto.getList())) {
                java.util.List<List> list = xdjzzyReqDto.getList();
                java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.Record> recordList = new ArrayList<>();
                for (List l : list) {
                    cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.Record target = new cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req.Record();
                    BeanUtils.copyProperties(l, target);
                    recordList.add(target);
                }
                targetList.setRecord(recordList);
                reqService.setList(targetList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDJZZY.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xdjzzyReqService.setService(reqService);
            // 将xdjzzyReqService转换成xdjzzyReqServiceMap
            Map xdjzzyReqServiceMap = beanMapUtil.beanToMap(xdjzzyReqService);
            context.put("tradeDataMap", xdjzzyReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDJZZY.key, EsbEnum.TRADE_CODE_XDJZZY.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDJZZY.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDJZZY.key, EsbEnum.TRADE_CODE_XDJZZY.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdjzzyRespService = beanMapUtil.mapToBean(tradeDataMap, XdjzzyRespService.class, XdjzzyRespService.class);
            respService = xdjzzyRespService.getService();
            //  将respService转换成XdjzzyRespDto
            xdjzzyResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xdjzzyResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xdjzzyRespDto);
                xdjzzyResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdjzzyResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdjzzyResultDto.setCode(EpbEnum.EPB099999.key);
                xdjzzyResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDJZZY.key, EsbEnum.TRADE_CODE_XDJZZY.value, e.getMessage());
            xdjzzyResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdjzzyResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdjzzyResultDto.setData(xdjzzyRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDJZZY.key, EsbEnum.TRADE_CODE_XDJZZY.value, JSON.toJSONString(xdjzzyResultDto));
        return xdjzzyResultDto;
    }
}
