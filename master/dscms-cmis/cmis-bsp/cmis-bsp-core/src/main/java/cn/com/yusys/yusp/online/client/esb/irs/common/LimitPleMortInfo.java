package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 授信分项额度与抵质押、保证人关系信息(LimitPleMortInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
public class LimitPleMortInfo {
    private List<LimitPleMortInfoRecord> record; // 授信分项额度与抵质押、保证人关系信息

    public List<LimitPleMortInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LimitPleMortInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LimitPleMortInfo{" +
                "record=" + record +
                '}';
    }
}