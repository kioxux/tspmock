package cn.com.yusys.yusp.web.client.esb.core.da3320;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3320.Da3320ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3320.Da3320RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3320.Lstdzzccz;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3320.req.Da3320ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3320.resp.Da3320RespService;
import cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0.Listnm0_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm1.Listnm1_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.da3320.resp.lstdzzccz.Lstdzzccz_ARRAY;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class Dscms2Da3320Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Da3320Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3320
     * 交易描述：查询抵债资产信息以及与贷款、费用、出租的关联信息
     *
     * @param da3320ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3320")
    protected @ResponseBody
    ResultDto<Da3320RespDto> da3320(@Validated @RequestBody Da3320ReqDto da3320ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3320.key, EsbEnum.TRADE_CODE_DA3320.value, JSON.toJSONString(da3320ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3320.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3320.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3320.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3320.resp.Service();
        Da3320ReqService da3320ReqService = new Da3320ReqService();
        Da3320RespService da3320RespService = new Da3320RespService();
        Da3320RespDto da3320RespDto = new Da3320RespDto();
        ResultDto<Da3320RespDto> da3320ResultDto = new ResultDto<Da3320RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3320ReqDto转换成reqService
            BeanUtils.copyProperties(da3320ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3320.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3320ReqService.setService(reqService);
            // 将da3320ReqService转换成da3320ReqServiceMap
            Map da3320ReqServiceMap = beanMapUtil.beanToMap(da3320ReqService);
            context.put("tradeDataMap", da3320ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3320.key, EsbEnum.TRADE_CODE_DA3320.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3320.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3320.key, EsbEnum.TRADE_CODE_DA3320.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3320RespService = beanMapUtil.mapToBean(tradeDataMap, Da3320RespService.class, Da3320RespService.class);
            respService = da3320RespService.getService();

            da3320ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3320ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3320RespDto
                BeanUtils.copyProperties(respService, da3320RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0.Listnm0_ARRAY listnm0_ARRAY = Optional.ofNullable(respService.getListnm0_ARRAY()).orElseGet(() -> new Listnm0_ARRAY());//费用明细
                respService.setListnm0_ARRAY(listnm0_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getListnm0_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0.Record> listnm0Records = Optional.ofNullable(respService.getListnm0_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.da3320.Listnm0> da3320Listnm0Dtos = listnm0Records.parallelStream().map(listnm0Record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.da3320.Listnm0 listnm0 = new cn.com.yusys.yusp.dto.client.esb.core.da3320.Listnm0();
                        BeanUtils.copyProperties(listnm0Record, listnm0);
                        return listnm0;
                    }).collect(Collectors.toList());
                    da3320RespDto.setListnm0(da3320Listnm0Dtos);
                }
                cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm1.Listnm1_ARRAY listnm1_ARRAY = Optional.ofNullable(respService.getListnm1_ARRAY()).orElseGet(() -> new Listnm1_ARRAY());//抵债资产与贷款关联信息
                respService.setListnm1_ARRAY(listnm1_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getListnm1_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm1.Record> listnm1Records = Optional.ofNullable(respService.getListnm1_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.da3320.Listnm1> da3320Listnm1Dtos = listnm1Records.parallelStream().map(listnm1Record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.da3320.Listnm1 listnm1 = new cn.com.yusys.yusp.dto.client.esb.core.da3320.Listnm1();
                        BeanUtils.copyProperties(listnm1Record, listnm1);
                        return listnm1;
                    }).collect(Collectors.toList());
                    da3320RespDto.setListnm1(da3320Listnm1Dtos);
                }
                cn.com.yusys.yusp.online.client.esb.core.da3320.resp.lstdzzccz.Lstdzzccz_ARRAY lstdzzccz_ARRAY = Optional.ofNullable(respService.getLstdzzccz_ARRAY()).orElseGet(() -> new Lstdzzccz_ARRAY());//抵债资产出租信息
                respService.setLstdzzccz_ARRAY(lstdzzccz_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdzzccz_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3320.resp.lstdzzccz.Record> lstdzzcczRecords = Optional.ofNullable(respService.getLstdzzccz_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.da3320.Lstdzzccz> lstdzzcczs = lstdzzcczRecords.parallelStream().map(lstdzzcczRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.core.da3320.Lstdzzccz lstdzzccz = new Lstdzzccz();
                        BeanUtils.copyProperties(lstdzzcczRecord, lstdzzccz);
                        return lstdzzccz;
                    }).collect(Collectors.toList());
                    da3320RespDto.setLstdzzccz(lstdzzcczs);
                }

                da3320ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3320ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3320ResultDto.setCode(EpbEnum.EPB099999.key);
                da3320ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3320.key, EsbEnum.TRADE_CODE_DA3320.value, e.getMessage());
            da3320ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3320ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3320ResultDto.setData(da3320RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3320.key, EsbEnum.TRADE_CODE_DA3320.value, JSON.toJSONString(da3320ResultDto));
        return da3320ResultDto;
    }
}
