package cn.com.yusys.yusp.online.client.esb.core.da3321.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵债资产模糊查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否
    private cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Listnm1 listnm1;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Listnm1 getListnm1() {
        return listnm1;
    }

    public void setListnm1(Listnm1 listnm1) {
        this.listnm1 = listnm1;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", listnm1=" + listnm1 +
                '}';
    }
}
