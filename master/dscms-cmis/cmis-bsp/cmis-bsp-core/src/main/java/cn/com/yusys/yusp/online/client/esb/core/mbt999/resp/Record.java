package cn.com.yusys.yusp.online.client.esb.core.mbt999.resp;

import java.math.BigDecimal;

/**
 * 响应Service：v5通用记账列表
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private Integer jioyxuho;//交易序号
    private String kehuzhao;//客户账号
    private String dxzhxhao;//待销账序号
    private String jiaoyijg;//交易机构
    private String zhwujgha;//账务机构号
    private String yewubima;//业务编码
    private String zhhaxhao;//账号序号
    private String jiaoyije;//交易金额
    private String huobdaih;//货币代号
    private String jiedaibz;//借贷标志
    private String mokuaiii;//模块
    private String zhhuzwmc;//账户名称
    private String shoufdma;//收费代码
    private String shfdmamc;//收费代码名称
    private String yingshfy;//应收费用
    private String shifujne;//实付金额
    private BigDecimal sffeiybz;//是否费用标志

    public Integer getJioyxuho() {
        return jioyxuho;
    }

    public void setJioyxuho(Integer jioyxuho) {
        this.jioyxuho = jioyxuho;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getDxzhxhao() {
        return dxzhxhao;
    }

    public void setDxzhxhao(String dxzhxhao) {
        this.dxzhxhao = dxzhxhao;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getZhwujgha() {
        return zhwujgha;
    }

    public void setZhwujgha(String zhwujgha) {
        this.zhwujgha = zhwujgha;
    }

    public String getYewubima() {
        return yewubima;
    }

    public void setYewubima(String yewubima) {
        this.yewubima = yewubima;
    }

    public String getZhhaxhao() {
        return zhhaxhao;
    }

    public void setZhhaxhao(String zhhaxhao) {
        this.zhhaxhao = zhhaxhao;
    }

    public String getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(String jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getJiedaibz() {
        return jiedaibz;
    }

    public void setJiedaibz(String jiedaibz) {
        this.jiedaibz = jiedaibz;
    }

    public String getMokuaiii() {
        return mokuaiii;
    }

    public void setMokuaiii(String mokuaiii) {
        this.mokuaiii = mokuaiii;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public String getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(String yingshfy) {
        this.yingshfy = yingshfy;
    }

    public String getShifujne() {
        return shifujne;
    }

    public void setShifujne(String shifujne) {
        this.shifujne = shifujne;
    }

    public BigDecimal getSffeiybz() {
        return sffeiybz;
    }

    public void setSffeiybz(BigDecimal sffeiybz) {
        this.sffeiybz = sffeiybz;
    }

    @Override
    public String toString() {
        return "Record{" +
                "jioyxuho='" + jioyxuho + '\'' +
                "kehuzhao='" + kehuzhao + '\'' +
                "dxzhxhao='" + dxzhxhao + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "zhwujgha='" + zhwujgha + '\'' +
                "yewubima='" + yewubima + '\'' +
                "zhhaxhao='" + zhhaxhao + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "jiedaibz='" + jiedaibz + '\'' +
                "mokuaiii='" + mokuaiii + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "shoufdma='" + shoufdma + '\'' +
                "shfdmamc='" + shfdmamc + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "shifujne='" + shifujne + '\'' +
                "sffeiybz='" + sffeiybz + '\'' +
                '}';
    }
}  
