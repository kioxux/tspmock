package cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.bankrupt;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Service：涉诉信息查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Count implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "count_total")
    private Integer count_total;//        案件总数
    @JsonProperty(value = "count_jie_total")
    private Integer count_jie_total;//            已结案总数
    @JsonProperty(value = "count_wei_total")
    private Integer count_wei_total;//            未结案总数
    @JsonProperty(value = "count_yuangao")
    private Integer count_yuangao;//          原告总数
    @JsonProperty(value = "count_jie_yuangao")
    private Integer count_jie_yuangao;//              原告已结案总数
    @JsonProperty(value = "count_wei_yuangao")
    private Integer count_wei_yuangao;//              原告未结案总数
    @JsonProperty(value = "count_beigao")
    private Integer count_beigao;//         被告总数
    @JsonProperty(value = "count_jie_beigao")
    private Integer count_jie_beigao;//             被告已结案总数
    @JsonProperty(value = "count_wei_beigao")
    private Integer count_wei_beigao;//             被告未结案总数
    @JsonProperty(value = "count_other")
    private Integer count_other;//        第三人总数
    @JsonProperty(value = "count_jie_other")
    private Integer count_jie_other;//            第三人已结案总数
    @JsonProperty(value = "count_wei_other")
    private Integer count_wei_other;//            第三人未结案总数
    @JsonProperty(value = "money_total")
    private BigDecimal money_total;//        涉案总金额
    @JsonProperty(value = "money_jie_total")
    private BigDecimal money_jie_total;//            已结案金额
    @JsonProperty(value = "money_wei_total")
    private BigDecimal money_wei_total;//            未结案金额
    @JsonProperty(value = "money_wei_percent")
    private BigDecimal money_wei_percent;//              未结案金额百分比
    @JsonProperty(value = "money_yuangao")
    private BigDecimal money_yuangao;//          原告金额
    @JsonProperty(value = "money_jie_yuangao")
    private BigDecimal money_jie_yuangao;//              原告已结金额
    @JsonProperty(value = "money_wei_yuangao")
    private BigDecimal money_wei_yuangao;//              原告未结案金额
    @JsonProperty(value = "money_beigao")
    private BigDecimal money_beigao;//         被告金额
    @JsonProperty(value = "money_jie_beigao")
    private BigDecimal money_jie_beigao;//             被告已结案金额
    @JsonProperty(value = "money_wei_beigao")
    private BigDecimal money_wei_beigao;//             被告未结案金额
    @JsonProperty(value = "money_other")
    private BigDecimal money_other;//        第三人金额
    @JsonProperty(value = "money_jie_other")
    private BigDecimal money_jie_other;//            第三人已结案金额
    @JsonProperty(value = "money_wei_other")
    private BigDecimal money_wei_other;//            第三人未结案金额
    @JsonProperty(value = "ay_stat")
    private String ay_stat;//    涉案案由分布
    @JsonProperty(value = "area_stat")
    private String area_stat;//      涉案地点分布
    @JsonProperty(value = "larq_stat")
    private String larq_stat;//      涉案时间分布
    @JsonProperty(value = "jafs_stat")
    private String jafs_stat;//      结案方式分布

    public Integer getCount_total() {
        return count_total;
    }

    public void setCount_total(Integer count_total) {
        this.count_total = count_total;
    }

    public Integer getCount_jie_total() {
        return count_jie_total;
    }

    public void setCount_jie_total(Integer count_jie_total) {
        this.count_jie_total = count_jie_total;
    }

    public Integer getCount_wei_total() {
        return count_wei_total;
    }

    public void setCount_wei_total(Integer count_wei_total) {
        this.count_wei_total = count_wei_total;
    }

    public Integer getCount_yuangao() {
        return count_yuangao;
    }

    public void setCount_yuangao(Integer count_yuangao) {
        this.count_yuangao = count_yuangao;
    }

    public Integer getCount_jie_yuangao() {
        return count_jie_yuangao;
    }

    public void setCount_jie_yuangao(Integer count_jie_yuangao) {
        this.count_jie_yuangao = count_jie_yuangao;
    }

    public Integer getCount_wei_yuangao() {
        return count_wei_yuangao;
    }

    public void setCount_wei_yuangao(Integer count_wei_yuangao) {
        this.count_wei_yuangao = count_wei_yuangao;
    }

    public Integer getCount_beigao() {
        return count_beigao;
    }

    public void setCount_beigao(Integer count_beigao) {
        this.count_beigao = count_beigao;
    }

    public Integer getCount_jie_beigao() {
        return count_jie_beigao;
    }

    public void setCount_jie_beigao(Integer count_jie_beigao) {
        this.count_jie_beigao = count_jie_beigao;
    }

    public Integer getCount_wei_beigao() {
        return count_wei_beigao;
    }

    public void setCount_wei_beigao(Integer count_wei_beigao) {
        this.count_wei_beigao = count_wei_beigao;
    }

    public Integer getCount_other() {
        return count_other;
    }

    public void setCount_other(Integer count_other) {
        this.count_other = count_other;
    }

    public Integer getCount_jie_other() {
        return count_jie_other;
    }

    public void setCount_jie_other(Integer count_jie_other) {
        this.count_jie_other = count_jie_other;
    }

    public Integer getCount_wei_other() {
        return count_wei_other;
    }

    public void setCount_wei_other(Integer count_wei_other) {
        this.count_wei_other = count_wei_other;
    }

    public BigDecimal getMoney_total() {
        return money_total;
    }

    public void setMoney_total(BigDecimal money_total) {
        this.money_total = money_total;
    }

    public BigDecimal getMoney_jie_total() {
        return money_jie_total;
    }

    public void setMoney_jie_total(BigDecimal money_jie_total) {
        this.money_jie_total = money_jie_total;
    }

    public BigDecimal getMoney_wei_total() {
        return money_wei_total;
    }

    public void setMoney_wei_total(BigDecimal money_wei_total) {
        this.money_wei_total = money_wei_total;
    }

    public BigDecimal getMoney_wei_percent() {
        return money_wei_percent;
    }

    public void setMoney_wei_percent(BigDecimal money_wei_percent) {
        this.money_wei_percent = money_wei_percent;
    }

    public BigDecimal getMoney_yuangao() {
        return money_yuangao;
    }

    public void setMoney_yuangao(BigDecimal money_yuangao) {
        this.money_yuangao = money_yuangao;
    }

    public BigDecimal getMoney_jie_yuangao() {
        return money_jie_yuangao;
    }

    public void setMoney_jie_yuangao(BigDecimal money_jie_yuangao) {
        this.money_jie_yuangao = money_jie_yuangao;
    }

    public BigDecimal getMoney_wei_yuangao() {
        return money_wei_yuangao;
    }

    public void setMoney_wei_yuangao(BigDecimal money_wei_yuangao) {
        this.money_wei_yuangao = money_wei_yuangao;
    }

    public BigDecimal getMoney_beigao() {
        return money_beigao;
    }

    public void setMoney_beigao(BigDecimal money_beigao) {
        this.money_beigao = money_beigao;
    }

    public BigDecimal getMoney_jie_beigao() {
        return money_jie_beigao;
    }

    public void setMoney_jie_beigao(BigDecimal money_jie_beigao) {
        this.money_jie_beigao = money_jie_beigao;
    }

    public BigDecimal getMoney_wei_beigao() {
        return money_wei_beigao;
    }

    public void setMoney_wei_beigao(BigDecimal money_wei_beigao) {
        this.money_wei_beigao = money_wei_beigao;
    }

    public BigDecimal getMoney_other() {
        return money_other;
    }

    public void setMoney_other(BigDecimal money_other) {
        this.money_other = money_other;
    }

    public BigDecimal getMoney_jie_other() {
        return money_jie_other;
    }

    public void setMoney_jie_other(BigDecimal money_jie_other) {
        this.money_jie_other = money_jie_other;
    }

    public BigDecimal getMoney_wei_other() {
        return money_wei_other;
    }

    public void setMoney_wei_other(BigDecimal money_wei_other) {
        this.money_wei_other = money_wei_other;
    }

    public String getAy_stat() {
        return ay_stat;
    }

    public void setAy_stat(String ay_stat) {
        this.ay_stat = ay_stat;
    }

    public String getArea_stat() {
        return area_stat;
    }

    public void setArea_stat(String area_stat) {
        this.area_stat = area_stat;
    }

    public String getLarq_stat() {
        return larq_stat;
    }

    public void setLarq_stat(String larq_stat) {
        this.larq_stat = larq_stat;
    }

    public String getJafs_stat() {
        return jafs_stat;
    }

    public void setJafs_stat(String jafs_stat) {
        this.jafs_stat = jafs_stat;
    }
}
