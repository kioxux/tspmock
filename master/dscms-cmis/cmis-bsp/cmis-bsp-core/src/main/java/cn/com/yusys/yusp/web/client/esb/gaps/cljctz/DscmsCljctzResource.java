package cn.com.yusys.yusp.web.client.esb.gaps.cljctz;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.req.CljctzReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.resp.CljctzRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.gaps.cljctz.req.CljctzReqService;
import cn.com.yusys.yusp.online.client.esb.gaps.cljctz.resp.CljctzRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:连云港存量房列表
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用GAPS系统的接口处理类(cljctz)")
@RestController
@RequestMapping("/api/dscms2gaps")
public class DscmsCljctzResource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsCljctzResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：cljctz
     * 交易描述：连云港存量房列表
     *
     * @param cljctzReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cljctz:连云港存量房列表")
    @PostMapping("/cljctz")
    protected @ResponseBody
    ResultDto<CljctzRespDto> cljctz(@Validated @RequestBody CljctzReqDto cljctzReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value, JSON.toJSONString(cljctzReqDto));
        cn.com.yusys.yusp.online.client.esb.gaps.cljctz.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.gaps.cljctz.req.Service();
        cn.com.yusys.yusp.online.client.esb.gaps.cljctz.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.gaps.cljctz.resp.Service();
        CljctzReqService cljctzReqService = new CljctzReqService();
        CljctzRespService cljctzRespService = new CljctzRespService();
        CljctzRespDto cljctzRespDto = new CljctzRespDto();
        ResultDto<CljctzRespDto> cljctzResultDto = new ResultDto<CljctzRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将cljctzReqDto转换成reqService
            BeanUtils.copyProperties(cljctzReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CLJCTZ.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            cljctzReqService.setService(reqService);
            // 将cljctzReqService转换成cljctzReqServiceMap
            Map cljctzReqServiceMap = beanMapUtil.beanToMap(cljctzReqService);
            context.put("tradeDataMap", cljctzReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CLJCTZ.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cljctzRespService = beanMapUtil.mapToBean(tradeDataMap, CljctzRespService.class, CljctzRespService.class);
            respService = cljctzRespService.getService();
            //  将respService转换成CljctzRespDto
            BeanUtils.copyProperties(respService, cljctzRespDto);
            cljctzResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            cljctzResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, cljctzRespDto);

                cljctzResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cljctzResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cljctzResultDto.setCode(EpbEnum.EPB099999.key);
                cljctzResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value, e.getMessage());
            cljctzResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cljctzResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cljctzResultDto.setData(cljctzRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLJCTZ.key, EsbEnum.TRADE_CODE_CLJCTZ.value, JSON.toJSONString(cljctzResultDto));
        return cljctzResultDto;
    }
}
