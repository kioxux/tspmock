package cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp;
/**
 * 响应Service：客户集团信息查询（new）接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 21:06
 */
public class Record {

    private String custno; // 集团成员客户号
    private String cortype; // 集团关系类型
    private String grpdesc; // 集团关系描述
    private String iscust; // 是否集团主客户
    private String mainno; // 主管人
    private String maiorg; // 主管机构
    private String socino; // 统一社会信用代码
    private String custst; // 成员状态

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCortype() {
        return cortype;
    }

    public void setCortype(String cortype) {
        this.cortype = cortype;
    }

    public String getGrpdesc() {
        return grpdesc;
    }

    public void setGrpdesc(String grpdesc) {
        this.grpdesc = grpdesc;
    }

    public String getIscust() {
        return iscust;
    }

    public void setIscust(String iscust) {
        this.iscust = iscust;
    }

    public String getMainno() {
        return mainno;
    }

    public void setMainno(String mainno) {
        this.mainno = mainno;
    }

    public String getMaiorg() {
        return maiorg;
    }

    public void setMaiorg(String maiorg) {
        this.maiorg = maiorg;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    @Override
    public String toString() {
        return "Record{" +
                "custno='" + custno + '\'' +
                ", cortype='" + cortype + '\'' +
                ", grpdesc='" + grpdesc + '\'' +
                ", iscust='" + iscust + '\'' +
                ", mainno='" + mainno + '\'' +
                ", maiorg='" + maiorg + '\'' +
                ", socino='" + socino + '\'' +
                ", custst='" + custst + '\'' +
                '}';
    }
}
