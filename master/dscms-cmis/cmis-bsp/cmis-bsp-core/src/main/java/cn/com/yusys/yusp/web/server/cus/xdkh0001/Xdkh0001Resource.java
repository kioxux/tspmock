package cn.com.yusys.yusp.web.server.cus.xdkh0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0001.req.Xdkh0001ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0001.resp.Xdkh0001RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.req.Xdkh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0001.resp.Xdkh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:个人客户基本信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0001:个人客户基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0001Resource.class);

    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0001
     * 交易描述：个人客户基本信息查询
     *
     * @param xdkh0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("个人客户基本信息查询")
    @PostMapping("/xdkh0001")
    //@Idempotent({"xdcakh0001", "#xdkh0001ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0001RespDto xdkh0001(@Validated @RequestBody Xdkh0001ReqDto xdkh0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001ReqDto));
        Xdkh0001DataReqDto xdkh0001DataReqDto = new Xdkh0001DataReqDto();// 请求Data： 个人客户基本信息查询
        Xdkh0001DataRespDto xdkh0001DataRespDto = null;// 响应Data：个人客户基本信息查询
        Xdkh0001RespDto xdkh0001RespDto = new Xdkh0001RespDto();// 响应DTO：个人客户信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0001.req.Data reqData = null; // 请求Data：个人客户基本信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0001.resp.Data();// 响应Data：个人客户基本信息查询
        try {
            // 从 xdkh0001ReqDto获取 reqData
            reqData = xdkh0001ReqDto.getData();
            // 将 reqData 拷贝到xdkh0001DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0001DataReqDto);
            // 调用服务：cmis_cus
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataReqDto));
            ResultDto<Xdkh0001DataRespDto> xdkh0001DataResultDto = dscmsCusClientService.xdkh0001(xdkh0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0001RespDto.setErorcd(Optional.ofNullable(xdkh0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0001RespDto.setErortx(Optional.ofNullable(xdkh0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0001DataResultDto.getCode())) {
                xdkh0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0001DataRespDto = xdkh0001DataResultDto.getData();
                // 将 xdkh0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, e.getMessage());
            xdkh0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0001RespDto.setDatasq(servsq);

        xdkh0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0001.key, DscmsEnum.TRADE_CODE_XDKH0001.value, JSON.toJSONString(xdkh0001RespDto));
        return xdkh0001RespDto;
    }
}

