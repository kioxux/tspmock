package cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.resp;

/**
 * 响应Service：查询质押物信息
 *
 * @author chenyong
 * @version 1.0
 */
public class XdypzywcxRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
