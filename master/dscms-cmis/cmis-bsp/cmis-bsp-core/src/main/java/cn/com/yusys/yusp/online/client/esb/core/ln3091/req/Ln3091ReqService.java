package cn.com.yusys.yusp.online.client.esb.core.ln3091.req;

/**
 * 请求Service：待付款指令查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3091ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3091ReqService{" +
				"service=" + service +
				'}';
	}
}
