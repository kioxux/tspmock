package cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj;

import java.math.BigDecimal;

/**
 * 请求Service：资产转让处理
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String jiejuhao;//借据号
    private BigDecimal sddjjine;//收到对价金额

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public BigDecimal getSddjjine() {
        return sddjjine;
    }

    public void setSddjjine(BigDecimal sddjjine) {
        this.sddjjine = sddjjine;
    }

    @Override
    public String toString() {
        return "Record{" +
                "jiejuhao='" + jiejuhao + '\'' +
                ", sddjjine=" + sddjjine +
                '}';
    }
}
