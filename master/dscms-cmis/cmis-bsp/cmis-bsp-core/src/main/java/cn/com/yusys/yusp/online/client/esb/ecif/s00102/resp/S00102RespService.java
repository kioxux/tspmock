package cn.com.yusys.yusp.online.client.esb.ecif.s00102.resp;

/**
 * 响应Service：对私客户综合信息维护
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class S00102RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
