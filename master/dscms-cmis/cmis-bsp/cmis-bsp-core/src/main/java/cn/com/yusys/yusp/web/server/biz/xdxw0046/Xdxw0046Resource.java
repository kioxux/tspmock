package cn.com.yusys.yusp.web.server.biz.xdxw0046;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0046.req.Xdxw0046ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0046.resp.Xdxw0046RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0046.req.Xdxw0046DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0046.resp.Xdxw0046DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优享贷批复结果反馈
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0046:优享贷批复结果反馈")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0046Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0046Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0046
     * 交易描述：优享贷批复结果反馈
     *
     * @param xdxw0046ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优享贷批复结果反馈")
    @PostMapping("/xdxw0046")
    //@Idempotent({"xdcaxw0046", "#xdxw0046ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0046RespDto xdxw0046(@Validated @RequestBody Xdxw0046ReqDto xdxw0046ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046ReqDto));
        Xdxw0046DataReqDto xdxw0046DataReqDto = new Xdxw0046DataReqDto();// 请求Data： 优享贷批复结果反馈
        Xdxw0046DataRespDto xdxw0046DataRespDto = new Xdxw0046DataRespDto();// 响应Data：优享贷批复结果反馈
		Xdxw0046RespDto xdxw0046RespDto = new Xdxw0046RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0046.req.Data reqData = null; // 请求Data：优享贷批复结果反馈
		cn.com.yusys.yusp.dto.server.biz.xdxw0046.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0046.resp.Data();// 响应Data：优享贷批复结果反馈
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0046ReqDto获取 reqData
            reqData = xdxw0046ReqDto.getData();
            // 将 reqData 拷贝到xdxw0046DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0046DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046DataReqDto));
            ResultDto<Xdxw0046DataRespDto> xdxw0046DataResultDto = dscmsBizXwClientService.xdxw0046(xdxw0046DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0046RespDto.setErorcd(Optional.ofNullable(xdxw0046DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0046RespDto.setErortx(Optional.ofNullable(xdxw0046DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0046DataResultDto.getCode())) {
                xdxw0046DataRespDto = xdxw0046DataResultDto.getData();
                xdxw0046RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0046RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0046DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0046DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, e.getMessage());
            xdxw0046RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0046RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0046RespDto.setDatasq(servsq);

        xdxw0046RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0046.key, DscmsEnum.TRADE_CODE_XDXW0046.value, JSON.toJSONString(xdxw0046RespDto));
        return xdxw0046RespDto;
    }
}
