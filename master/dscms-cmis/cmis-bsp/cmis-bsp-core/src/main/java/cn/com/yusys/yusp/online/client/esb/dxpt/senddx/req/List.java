package cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req;

/**
 * 请求Service：短信/微信发送批量接口
 *
 * @author hjk
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
