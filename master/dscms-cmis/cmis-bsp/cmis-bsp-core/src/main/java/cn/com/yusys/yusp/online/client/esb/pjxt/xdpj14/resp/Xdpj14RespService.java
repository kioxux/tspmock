package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.resp;

/**
 * 响应Service：信贷签约通知
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdpj14RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdpj14RespService{" +
                "service=" + service +
                '}';
    }
}
