package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfwdj.Record;

import java.util.List;

/**
 * 请求Dto：贷款服务登记
 *
 * @author code-generator
 * @version 1.0
 */
public class Lstdkfwdj_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfwdj.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkfwdj_ARRAY{" +
                "record=" + record +
                '}';
    }
}
