package cn.com.yusys.yusp.web.client.esb.xwywglpt.wxd007;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007.Wxd007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007.Wxd007RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.req.Wxd007ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.resp.Wxd007RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:直销系统请求小V平台综合决策管理列表查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "BSP封装小微业务管理平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Xwd007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xwd007Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：wxd007
     * 交易描述：直销系统请求小V平台综合决策管理列表查询
     *
     * @param wxd007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("wxd007:直销系统请求小V平台综合决策管理列表查询")
    @PostMapping("/wxd007")
    protected @ResponseBody
    ResultDto<Wxd007RespDto> wxd007(@Validated @RequestBody Wxd007ReqDto wxd007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD007.key, EsbEnum.TRADE_CODE_WXD007.value, JSON.toJSONString(wxd007ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.resp.Service();
        Wxd007ReqService wxd007ReqService = new Wxd007ReqService();
        Wxd007RespService wxd007RespService = new Wxd007RespService();
        Wxd007RespDto wxd007RespDto = new Wxd007RespDto();
        ResultDto<Wxd007RespDto> wxd007ResultDto = new ResultDto<Wxd007RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将wxd007ReqDto转换成reqService
            BeanUtils.copyProperties(wxd007ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_WXD007.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            wxd007ReqService.setService(reqService);
            // 将wxd007ReqService转换成wxd007ReqServiceMap
            Map wxd007ReqServiceMap = beanMapUtil.beanToMap(wxd007ReqService);
            context.put("tradeDataMap", wxd007ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD007.key, EsbEnum.TRADE_CODE_WXD007.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_WXD007.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD007.key, EsbEnum.TRADE_CODE_WXD007.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            wxd007RespService = beanMapUtil.mapToBean(tradeDataMap, Wxd007RespService.class, Wxd007RespService.class);
            respService = wxd007RespService.getService();

            wxd007ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            wxd007ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, wxd007RespDto);
                wxd007ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                wxd007ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                wxd007ResultDto.setCode(EpbEnum.EPB099999.key);
                wxd007ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD007.key, EsbEnum.TRADE_CODE_WXD007.value, e.getMessage());
            wxd007ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            wxd007ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        wxd007ResultDto.setData(wxd007RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD007.key, EsbEnum.TRADE_CODE_WXD007.value, JSON.toJSONString(wxd007ResultDto));
        return wxd007ResultDto;
    }
}
