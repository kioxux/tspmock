package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkllfd;

import java.math.BigDecimal;

public class Record {
    private String lilvfdfs;//利率浮动方式

    private String lilvleix;//利率类型

    private String lilvtzfs;//利率调整方式

    private BigDecimal lilvfdzh;//利率浮动值

    private BigDecimal zhchlilv;//正常利率

    private String lilvtzzq;//利率调整周期

    private String zclilvbh;//正常利率编号

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "lilvfdfs='" + lilvfdfs + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", zhchlilv=" + zhchlilv +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", zclilvbh='" + zclilvbh + '\'' +
                '}';
    }
}
