package cn.com.yusys.yusp.web.server.cus.xdkh0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0015.req.Xdkh0015ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0015.resp.Xdkh0015RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0015.req.Xdkh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0015.resp.Xdkh0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:农拍档接受白名单维护
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDKH0015:农拍档接受白名单维护")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0015Resource.class);
    @Resource
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0015
     * 交易描述：农拍档接受白名单维护
     *
     * @param xdkh0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("农拍档接受白名单维护")
    @PostMapping("/xdkh0015")
    //@Idempotent({"xdcakh0015", "#xdkh0015ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0015RespDto xdkh0015(@Validated @RequestBody Xdkh0015ReqDto xdkh0015ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, JSON.toJSONString(xdkh0015ReqDto));
        Xdkh0015DataReqDto xdkh0015DataReqDto = new Xdkh0015DataReqDto();// 请求Data： 农拍档接受白名单维护
        Xdkh0015DataRespDto xdkh0015DataRespDto = new Xdkh0015DataRespDto();// 响应Data：农拍档接受白名单维护
        Xdkh0015RespDto xdkh0015RespDto = new Xdkh0015RespDto();// 响应DTO：农拍档接受白名单维护
        cn.com.yusys.yusp.dto.server.cus.xdkh0015.req.Data reqData = null; // 请求Data：农拍档接受白名单维护
        cn.com.yusys.yusp.dto.server.cus.xdkh0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0015.resp.Data();// 响应Data：农拍档接受白名单维护
        try {
            // 从 xdkh0015ReqDto获取 reqData
            reqData = xdkh0015ReqDto.getData();
            // 将 reqData 拷贝到xdkh0015DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0015DataReqDto);
            // 调用服务：
            ResultDto<Xdkh0015DataRespDto> xdkh0015DataResultDto = dscmsCusClientService.xdkh0015(xdkh0015DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdkh0015RespDto.setErorcd(Optional.ofNullable(xdkh0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0015RespDto.setErortx(Optional.ofNullable(xdkh0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0015DataResultDto.getCode())) {
                xdkh0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0015DataRespDto = xdkh0015DataResultDto.getData();
                // 将 xdkh0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, e.getMessage());
            xdkh0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0015RespDto.setDatasq(servsq);
        xdkh0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0015.key, DscmsEnum.TRADE_CODE_XDKH0015.value, JSON.toJSONString(xdkh0015RespDto));
        return xdkh0015RespDto;
    }
}
