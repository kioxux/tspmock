package cn.com.yusys.yusp.online.client.esb.core.ln3020.req;

/**
 * 请求Service：贷款开户
 * @author zhugenrong
 * @version 1.0
 */
public class Ln3020ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3020ReqService{" +
                "service=" + service +
                '}';
    }
}