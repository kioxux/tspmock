package cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo;
/**
 * 请求Service：贷款客户经理批量移交
 */
public class Record {
    private String kehuhaoo;//客户号

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    @Override
    public String toString() {
        return "Record{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                '}';
    }
}
