package cn.com.yusys.yusp.web.client.gxp.tonglian.d11005;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.req.D11005ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.resp.D11005RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp.D11005RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d11005)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D11005Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D11005Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 信用卡申请
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d11005:信用卡申请")
    @PostMapping("/d11005")
    protected @ResponseBody
    ResultDto<D11005RespDto> d11005(@Validated @RequestBody D11005ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11005.key, GxpEnum.TRADE_CODE_D11005.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.D11005ReqMessage d11005ReqMessage = null;//请求Message：现金（大额）放款接口
        D11005RespMessage d11005RespMessage = new D11005RespMessage();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Message reqMessage = null;//请求Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp.Message();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Body();//请求Body：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp.Body();//响应Body：现金（大额）放款接口

        D11005RespDto d11005RespDto = new D11005RespDto();//响应Dto：现金（大额）放款接口
        ResultDto<D11005RespDto> d11005ResultDto = new ResultDto<>();//响应ResultDto：现金（大额）放款接口

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D11005.key, GxpEnum.TRADE_CODE_D11005.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D11005ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Message::setBody, reqBody).build();
            // 给d11005ReqMessage赋值
            d11005ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.D11005ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.D11005ReqMessage::setMessage, reqMessage).build();

            // 将d11005ReqService转换成d11005ReqServiceMap
            Map d11005ReqMessageMap = beanMapUtil.beanToMap(d11005ReqMessage);
            context.put("tradeDataMap", d11005ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11005.key, GxpEnum.TRADE_CODE_D11005.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D11005.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11005.key, GxpEnum.TRADE_CODE_D11005.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d11005RespMessage = beanMapUtil.mapToBean(tradeDataMap, D11005RespMessage.class, D11005RespMessage.class);//响应Message：现金（大额）放款接口
            respMessage = d11005RespMessage.getMessage();//响应Message：现金（大额）放款接口
            gxpRespHead = respMessage.getHead();//响应Head：现金（大额）放款接口
            //  将D11005RespDto封装到ResultDto<D11005RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                respBody = respMessage.getBody();
                //  将respBody转换成D11005RespDto
                BeanUtils.copyProperties(respBody, d11005RespDto);
                d11005ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d11005ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d11005ResultDto.setCode(EpbEnum.EPB099999.key);
                d11005ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d11005ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11005.key, GxpEnum.TRADE_CODE_D11005.value, e.getMessage());
            d11005ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d11005ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d11005ResultDto.setData(d11005RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11005.key, GxpEnum.TRADE_CODE_D11005.value, JSON.toJSONString(d11005ResultDto));
        return d11005ResultDto;
    }
}
