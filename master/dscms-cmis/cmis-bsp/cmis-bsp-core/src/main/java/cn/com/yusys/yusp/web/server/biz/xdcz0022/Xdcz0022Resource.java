package cn.com.yusys.yusp.web.server.biz.xdcz0022;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0022.req.Xdcz0022ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0022.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0022.resp.Xdcz0022RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0022.req.Xdcz0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0022.resp.Xdcz0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:风控发送相关信息至信贷进行支用校验
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0022:风控发送相关信息至信贷进行支用校验")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0022Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0022
     * 交易描述：风控发送相关信息至信贷进行支用校验
     *
     * @param xdcz0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发送相关信息至信贷进行支用校验")
    @PostMapping("/xdcz0022")
   //@Idempotent({"xdcz0022", "#xdcz0022ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0022RespDto xdcz0022(@Validated @RequestBody Xdcz0022ReqDto xdcz0022ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022ReqDto));
        Xdcz0022DataReqDto xdcz0022DataReqDto = new Xdcz0022DataReqDto();// 请求Data： 电子保函开立
        Xdcz0022DataRespDto xdcz0022DataRespDto = new Xdcz0022DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0022.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0022RespDto xdcz0022RespDto = new Xdcz0022RespDto();
        try {
            // 从 xdcz0022ReqDto获取 reqData
            reqData = xdcz0022ReqDto.getData();
            // 将 reqData 拷贝到xdcz0022DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0022DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataReqDto));
            ResultDto<Xdcz0022DataRespDto> xdcz0022DataResultDto = dscmsBizCzClientService.xdcz0022(xdcz0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0022RespDto.setErorcd(Optional.ofNullable(xdcz0022DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0022RespDto.setErortx(Optional.ofNullable(xdcz0022DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0022DataResultDto.getCode())) {
                xdcz0022DataRespDto = xdcz0022DataResultDto.getData();
                xdcz0022RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0022RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0022DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0022DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, e.getMessage());
            xdcz0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0022RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0022RespDto.setDatasq(servsq);

        xdcz0022RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0022.key, DscmsEnum.TRADE_CODE_XDCZ0022.value, JSON.toJSONString(xdcz0022RespDto));
        return xdcz0022RespDto;
    }
}
