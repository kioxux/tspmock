package cn.com.yusys.yusp.web.client.esb.core.ln3055;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.req.Ln3055ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.resp.Ln3055RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3055.req.Ln3055ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3055.resp.Ln3055RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3055)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3055Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3055Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3055
     * 交易描述：贷款利率调整
     *
     * @param ln3055ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3055:贷款利率调整")
    @PostMapping("/ln3055")
    protected @ResponseBody
    ResultDto<Ln3055RespDto> ln3055(@Validated @RequestBody Ln3055ReqDto ln3055ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3055.key, EsbEnum.TRADE_CODE_LN3055.value, JSON.toJSONString(ln3055ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3055.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3055.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3055.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3055.resp.Service();
        Ln3055ReqService ln3055ReqService = new Ln3055ReqService();
        Ln3055RespService ln3055RespService = new Ln3055RespService();
        Ln3055RespDto ln3055RespDto = new Ln3055RespDto();
        ResultDto<Ln3055RespDto> ln3055ResultDto = new ResultDto<Ln3055RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3055ReqDto转换成reqService
            BeanUtils.copyProperties(ln3055ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3055.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3055ReqService.setService(reqService);
            // 将ln3055ReqService转换成ln3055ReqServiceMap
            Map ln3055ReqServiceMap = beanMapUtil.beanToMap(ln3055ReqService);
            context.put("tradeDataMap", ln3055ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3055.key, EsbEnum.TRADE_CODE_LN3055.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3055.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3055.key, EsbEnum.TRADE_CODE_LN3055.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3055RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3055RespService.class, Ln3055RespService.class);
            respService = ln3055RespService.getService();

            ln3055ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3055ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3055RespDto
                BeanUtils.copyProperties(respService, ln3055RespDto);
                ln3055ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3055ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3055ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3055ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3055.key, EsbEnum.TRADE_CODE_LN3055.value, e.getMessage());
            ln3055ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3055ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3055ResultDto.setData(ln3055RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3055.key, EsbEnum.TRADE_CODE_LN3055.value, JSON.toJSONString(ln3055ResultDto));
        return ln3055ResultDto;
    }
}
