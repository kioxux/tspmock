package cn.com.yusys.yusp.web.client.esb.core.ln3075;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3075.req.Ln3075ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3075.resp.Ln3075RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3075.req.Ln3075ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3075.resp.Ln3075RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3075)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3075Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3075Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3075
     * 交易描述：逾期贷款展期
     *
     * @param ln3075ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3075:逾期贷款展期")
    @PostMapping("/ln3075")
    protected @ResponseBody
    ResultDto<Ln3075RespDto> ln3075(@Validated @RequestBody Ln3075ReqDto ln3075ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3075.key, EsbEnum.TRADE_CODE_LN3075.value, JSON.toJSONString(ln3075ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3075.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3075.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3075.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3075.resp.Service();
        Ln3075ReqService ln3075ReqService = new Ln3075ReqService();
        Ln3075RespService ln3075RespService = new Ln3075RespService();
        Ln3075RespDto ln3075RespDto = new Ln3075RespDto();
        ResultDto<Ln3075RespDto> ln3075ResultDto = new ResultDto<Ln3075RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3075ReqDto转换成reqService
            BeanUtils.copyProperties(ln3075ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3075.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3075ReqService.setService(reqService);
            // 将ln3075ReqService转换成ln3075ReqServiceMap
            Map ln3075ReqServiceMap = beanMapUtil.beanToMap(ln3075ReqService);
            Map service = (Map) ln3075ReqServiceMap.get("service");
            // bigdecimal字段转成map之后出现科学计数法问题解决 modify by chenyong 陈勇
            service.forEach((k, v) -> {
                if (v instanceof Double) {
                    BigDecimal bigDecimal = new BigDecimal(((Double) v));
                    service.put(k, bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });
            ln3075ReqServiceMap.put("service", service);

            context.put("tradeDataMap", ln3075ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3075.key, EsbEnum.TRADE_CODE_LN3075.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3075.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3075.key, EsbEnum.TRADE_CODE_LN3075.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3075RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3075RespService.class, Ln3075RespService.class);
            respService = ln3075RespService.getService();

            ln3075ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3075ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3075RespDto
                BeanUtils.copyProperties(respService, ln3075RespDto);
                ln3075ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3075ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3075ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3075ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3075.key, EsbEnum.TRADE_CODE_LN3075.value, e.getMessage());
            ln3075ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3075ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3075ResultDto.setData(ln3075RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3075.key, EsbEnum.TRADE_CODE_LN3075.value, JSON.toJSONString(ln3075ResultDto));
        return ln3075ResultDto;
    }
}
