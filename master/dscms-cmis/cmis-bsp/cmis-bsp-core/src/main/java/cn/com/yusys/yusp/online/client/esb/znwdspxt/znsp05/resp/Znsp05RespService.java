package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp05.resp;

/**
 * 响应Service：风险拦截截接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Znsp05RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
