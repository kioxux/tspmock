package cn.com.yusys.yusp.web.client.esb.ypxt.spsyyp;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.req.SpsyypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.resp.SpsyypRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.req.SpsyypReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.resp.SpsyypRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷审批结果同步接口
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2SpsyypResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2SpsyypResource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：spsyyp
     * 交易描述：信贷审批结果同步接口
     *
     * @param spsyypReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/spsyyp")
    protected @ResponseBody
    ResultDto<SpsyypRespDto> spsyyp(@Validated @RequestBody SpsyypReqDto spsyypReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SPSYYP.key, EsbEnum.TRADE_CODE_SPSYYP.value, JSON.toJSONString(spsyypReqDto));
		cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.req.Service();
		cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.resp.Service();
        SpsyypReqService spsyypReqService = new SpsyypReqService();
        SpsyypRespService spsyypRespService = new SpsyypRespService();
        SpsyypRespDto spsyypRespDto = new SpsyypRespDto();
        ResultDto<SpsyypRespDto> spsyypResultDto = new ResultDto<SpsyypRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将spsyypReqDto转换成reqService
			BeanUtils.copyProperties(spsyypReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_SPSYYP.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
			reqService.setDatasq(servsq);//    全局流水
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			spsyypReqService.setService(reqService);
			// 将spsyypReqService转换成spsyypReqServiceMap
			Map spsyypReqServiceMap = beanMapUtil.beanToMap(spsyypReqService);
			context.put("tradeDataMap", spsyypReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SPSYYP.key, EsbEnum.TRADE_CODE_SPSYYP.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_SPSYYP.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SPSYYP.key, EsbEnum.TRADE_CODE_SPSYYP.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			spsyypRespService = beanMapUtil.mapToBean(tradeDataMap, SpsyypRespService.class, SpsyypRespService.class);
			respService = spsyypRespService.getService();
			//  将respService转换成SpsyypRespDto
			spsyypResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			spsyypResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成XdypbdccxRespDto
				BeanUtils.copyProperties(respService, spsyypRespDto);
				spsyypResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				spsyypResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				spsyypResultDto.setCode(EpbEnum.EPB099999.key);
				spsyypResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SPSYYP.key, EsbEnum.TRADE_CODE_SPSYYP.value, e.getMessage());
			spsyypResultDto.setCode(EpbEnum.EPB099999.key);//9999
			spsyypResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		spsyypResultDto.setData(spsyypRespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SPSYYP.key, EsbEnum.TRADE_CODE_SPSYYP.value, JSON.toJSONString(spsyypResultDto));
        return spsyypResultDto;
    }
}
