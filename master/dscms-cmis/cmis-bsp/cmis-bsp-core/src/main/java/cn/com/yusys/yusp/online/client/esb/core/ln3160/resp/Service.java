package cn.com.yusys.yusp.online.client.esb.core.ln3160.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkxypc.LstKlnb_dkxypc;
import cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz.LstKlnb_dkzrhz;
import cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrjj.LstKlnb_dkzrjj;

import java.math.BigDecimal;

/**
 * 响应Service：资产证券化信息查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String xieybhao;//协议编号
    private String xieyimch;//协议名称
    private String zcxyleix;//资产协议类型
    private String zcrfshii;//资产融通方式
    private BigDecimal zcrtbili;//资产融通比例
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String huobdhao;//货币代号
    private BigDecimal xyzuigxe;//协议最高限额
    private BigDecimal xieyshje;//协议实际金额
    private BigDecimal xieyilil;//协议利率
    private BigDecimal xieyilix;//协议利息
    private String qiandriq;//签订日期
    private String fengbriq;//封包日期
    private String jiebriqi;//解包日期
    private String ruchiriq;//入池日期
    private String huigriqi;//回购日期
    private String zchzhtai;//资产处理状态
    private String jydsleix;//交易对手类型
    private String jydshmch;//交易对手名称
    private String jydszhao;//交易对手账号
    private String jydszhzh;//交易对手账号子序号
    private String jydszhmc;//交易对手账户名称
    private String zhkaihhh;//账户开户行行号
    private String zhkaihhm;//账户开户行行名
    private String nyuelilv;//年/月利率标识
    private String fysfzhqh;//费用是否证券化
    private String fjsfzhqh;//罚金是否证券化
    private String fuheztai;//复核状态
    private String ruzjigou;//入账机构
    private String zrjjfshi;//转让计价方式
    private String zjlyzhao;//资金来源账号
    private String zjlyzzxh;//资金来源账号子序号
    private String zrfkzoqi;//付款周期
    private String xcfkriqi;//下次付款日期
    private String scifukrq;//上次付款日期
    private String zrfukzhh;//对外付款账号
    private String zrfukzxh;//对外付款账号子序号
    private String zjguijbz;//自动归集标志
    private String bzesfzrr;//资金来源账号不足额是否转让
    private String chulizht;//处理状态
    private String zhaiyosm;//摘要说明
    private String beizhuxx;//备注
    private String xunhchbz;//循环池标志
    private cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrjj.LstKlnb_dkzrjj lstKlnb_dkzrjj;//资产转让借据信息[LIST]
    private cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz.LstKlnb_dkzrhz lstKlnb_dkzrhz;//资产转让资金划转账号登记簿[LIST]
    private cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkxypc.LstKlnb_dkxypc lstKlnb_dkxypc;//资产证券化协议批次登记簿[LIST]

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getZcxyleix() {
        return zcxyleix;
    }

    public void setZcxyleix(String zcxyleix) {
        this.zcxyleix = zcxyleix;
    }

    public String getZcrfshii() {
        return zcrfshii;
    }

    public void setZcrfshii(String zcrfshii) {
        this.zcrfshii = zcrfshii;
    }

    public BigDecimal getZcrtbili() {
        return zcrtbili;
    }

    public void setZcrtbili(BigDecimal zcrtbili) {
        this.zcrtbili = zcrtbili;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getXyzuigxe() {
        return xyzuigxe;
    }

    public void setXyzuigxe(BigDecimal xyzuigxe) {
        this.xyzuigxe = xyzuigxe;
    }

    public BigDecimal getXieyshje() {
        return xieyshje;
    }

    public void setXieyshje(BigDecimal xieyshje) {
        this.xieyshje = xieyshje;
    }

    public BigDecimal getXieyilil() {
        return xieyilil;
    }

    public void setXieyilil(BigDecimal xieyilil) {
        this.xieyilil = xieyilil;
    }

    public BigDecimal getXieyilix() {
        return xieyilix;
    }

    public void setXieyilix(BigDecimal xieyilix) {
        this.xieyilix = xieyilix;
    }

    public String getQiandriq() {
        return qiandriq;
    }

    public void setQiandriq(String qiandriq) {
        this.qiandriq = qiandriq;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getJiebriqi() {
        return jiebriqi;
    }

    public void setJiebriqi(String jiebriqi) {
        this.jiebriqi = jiebriqi;
    }

    public String getRuchiriq() {
        return ruchiriq;
    }

    public void setRuchiriq(String ruchiriq) {
        this.ruchiriq = ruchiriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public String getZchzhtai() {
        return zchzhtai;
    }

    public void setZchzhtai(String zchzhtai) {
        this.zchzhtai = zchzhtai;
    }

    public String getJydsleix() {
        return jydsleix;
    }

    public void setJydsleix(String jydsleix) {
        this.jydsleix = jydsleix;
    }

    public String getJydshmch() {
        return jydshmch;
    }

    public void setJydshmch(String jydshmch) {
        this.jydshmch = jydshmch;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public String getFysfzhqh() {
        return fysfzhqh;
    }

    public void setFysfzhqh(String fysfzhqh) {
        this.fysfzhqh = fysfzhqh;
    }

    public String getFjsfzhqh() {
        return fjsfzhqh;
    }

    public void setFjsfzhqh(String fjsfzhqh) {
        this.fjsfzhqh = fjsfzhqh;
    }

    public String getFuheztai() {
        return fuheztai;
    }

    public void setFuheztai(String fuheztai) {
        this.fuheztai = fuheztai;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getZrjjfshi() {
        return zrjjfshi;
    }

    public void setZrjjfshi(String zrjjfshi) {
        this.zrjjfshi = zrjjfshi;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZrfkzoqi() {
        return zrfkzoqi;
    }

    public void setZrfkzoqi(String zrfkzoqi) {
        this.zrfkzoqi = zrfkzoqi;
    }

    public String getXcfkriqi() {
        return xcfkriqi;
    }

    public void setXcfkriqi(String xcfkriqi) {
        this.xcfkriqi = xcfkriqi;
    }

    public String getScifukrq() {
        return scifukrq;
    }

    public void setScifukrq(String scifukrq) {
        this.scifukrq = scifukrq;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public String getZjguijbz() {
        return zjguijbz;
    }

    public void setZjguijbz(String zjguijbz) {
        this.zjguijbz = zjguijbz;
    }

    public String getBzesfzrr() {
        return bzesfzrr;
    }

    public void setBzesfzrr(String bzesfzrr) {
        this.bzesfzrr = bzesfzrr;
    }

    public String getChulizht() {
        return chulizht;
    }

    public void setChulizht(String chulizht) {
        this.chulizht = chulizht;
    }

    public String getZhaiyosm() {
        return zhaiyosm;
    }

    public void setZhaiyosm(String zhaiyosm) {
        this.zhaiyosm = zhaiyosm;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getXunhchbz() {
        return xunhchbz;
    }

    public void setXunhchbz(String xunhchbz) {
        this.xunhchbz = xunhchbz;
    }

    public LstKlnb_dkzrjj getLstKlnb_dkzrjj() {
        return lstKlnb_dkzrjj;
    }

    public void setLstKlnb_dkzrjj(LstKlnb_dkzrjj lstKlnb_dkzrjj) {
        this.lstKlnb_dkzrjj = lstKlnb_dkzrjj;
    }

    public LstKlnb_dkzrhz getLstKlnb_dkzrhz() {
        return lstKlnb_dkzrhz;
    }

    public void setLstKlnb_dkzrhz(LstKlnb_dkzrhz lstKlnb_dkzrhz) {
        this.lstKlnb_dkzrhz = lstKlnb_dkzrhz;
    }

    public LstKlnb_dkxypc getLstKlnb_dkxypc() {
        return lstKlnb_dkxypc;
    }

    public void setLstKlnb_dkxypc(LstKlnb_dkxypc lstKlnb_dkxypc) {
        this.lstKlnb_dkxypc = lstKlnb_dkxypc;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", zcxyleix='" + zcxyleix + '\'' +
                ", zcrfshii='" + zcrfshii + '\'' +
                ", zcrtbili=" + zcrtbili +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", xyzuigxe=" + xyzuigxe +
                ", xieyshje=" + xieyshje +
                ", xieyilil=" + xieyilil +
                ", xieyilix=" + xieyilix +
                ", qiandriq='" + qiandriq + '\'' +
                ", fengbriq='" + fengbriq + '\'' +
                ", jiebriqi='" + jiebriqi + '\'' +
                ", ruchiriq='" + ruchiriq + '\'' +
                ", huigriqi='" + huigriqi + '\'' +
                ", zchzhtai='" + zchzhtai + '\'' +
                ", jydsleix='" + jydsleix + '\'' +
                ", jydshmch='" + jydshmch + '\'' +
                ", jydszhao='" + jydszhao + '\'' +
                ", jydszhzh='" + jydszhzh + '\'' +
                ", jydszhmc='" + jydszhmc + '\'' +
                ", zhkaihhh='" + zhkaihhh + '\'' +
                ", zhkaihhm='" + zhkaihhm + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", fysfzhqh='" + fysfzhqh + '\'' +
                ", fjsfzhqh='" + fjsfzhqh + '\'' +
                ", fuheztai='" + fuheztai + '\'' +
                ", ruzjigou='" + ruzjigou + '\'' +
                ", zrjjfshi='" + zrjjfshi + '\'' +
                ", zjlyzhao='" + zjlyzhao + '\'' +
                ", zjlyzzxh='" + zjlyzzxh + '\'' +
                ", zrfkzoqi='" + zrfkzoqi + '\'' +
                ", xcfkriqi='" + xcfkriqi + '\'' +
                ", scifukrq='" + scifukrq + '\'' +
                ", zrfukzhh='" + zrfukzhh + '\'' +
                ", zrfukzxh='" + zrfukzxh + '\'' +
                ", zjguijbz='" + zjguijbz + '\'' +
                ", bzesfzrr='" + bzesfzrr + '\'' +
                ", chulizht='" + chulizht + '\'' +
                ", zhaiyosm='" + zhaiyosm + '\'' +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", xunhchbz='" + xunhchbz + '\'' +
                ", lstKlnb_dkzrjj=" + lstKlnb_dkzrjj +
                ", lstKlnb_dkzrhz=" + lstKlnb_dkzrhz +
                ", lstKlnb_dkxypc=" + lstKlnb_dkxypc +
                '}';
    }
}
