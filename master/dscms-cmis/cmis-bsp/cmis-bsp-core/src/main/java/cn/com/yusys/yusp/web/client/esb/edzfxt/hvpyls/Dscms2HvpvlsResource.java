package cn.com.yusys.yusp.web.client.esb.edzfxt.hvpyls;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.req.HvpylsReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.HvpylsRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.req.HvpylsReqService;
import cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp.HvpylsRespService;
import cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp.List;
import cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用二代支付系统的接口处理类（hvpyls）")
@RestController
@RequestMapping("/api/dscms2edzfxt")
public class Dscms2HvpvlsResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2HvpvlsResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：hvpyls
     * 交易描述：大额往账列表查询
     *
     * @param hvpylsReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/hvpyls")
    protected @ResponseBody
    ResultDto<HvpylsRespDto> hvpyls(@Validated @RequestBody HvpylsReqDto hvpylsReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPYLS.key, EsbEnum.TRADE_CODE_HVPYLS.value, JSON.toJSONString(hvpylsReqDto));
        cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.req.Service();
        cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp.Service();
        HvpylsReqService hvpylsReqService = new HvpylsReqService();
        HvpylsRespService hvpylsRespService = new HvpylsRespService();
        HvpylsRespDto hvpylsRespDto = new HvpylsRespDto();
        ResultDto<HvpylsRespDto> hvpylsResultDto = new ResultDto<HvpylsRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将hvpylsReqDto转换成reqService
            BeanUtils.copyProperties(hvpylsReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_HVPYLS.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_ZFP.key);//    柜员号
            reqService.setBrchno(hvpylsReqDto.getBrchno());//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            hvpylsReqService.setService(reqService);
            // 将hvpylsReqService转换成hvpylsReqServiceMap
            Map hvpylsReqServiceMap = beanMapUtil.beanToMap(hvpylsReqService);
            context.put("tradeDataMap", hvpylsReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPYLS.key, EsbEnum.TRADE_CODE_HVPYLS.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_HVPYLS.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPYLS.key, EsbEnum.TRADE_CODE_HVPYLS.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            hvpylsRespService = beanMapUtil.mapToBean(tradeDataMap, HvpylsRespService.class, HvpylsRespService.class);
            respService = hvpylsRespService.getService();

            hvpylsResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            hvpylsResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成HvpylsRespDto
                BeanUtils.copyProperties(respService, hvpylsRespDto);
                List list = Optional.ofNullable(respService.getList()).orElse(new List());
                if (CollectionUtils.nonEmpty(list.getRecord())) {
                    java.util.List<Record> orginlist = list.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.List> targetList = new ArrayList<>();
                    for (Record r : orginlist) {
                        cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.List target = new cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.List();
                        BeanUtils.copyProperties(r, target);
                        targetList.add(target);
                    }
                    hvpylsRespDto.setList(targetList);
                }


                hvpylsResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                hvpylsResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                hvpylsResultDto.setCode(EpbEnum.EPB099999.key);
                hvpylsResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPYLS.key, EsbEnum.TRADE_CODE_HVPYLS.value, e.getMessage());
            hvpylsResultDto.setCode(EpbEnum.EPB099999.key);//9999
            hvpylsResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        hvpylsResultDto.setData(hvpylsRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPYLS.key, EsbEnum.TRADE_CODE_HVPYLS.value, JSON.toJSONString(hvpylsResultDto));
        return hvpylsResultDto;
    }
}
