package cn.com.yusys.yusp.online.client.esb.comstar.req;

/**
 * 请求Service：额度同步
 *
 * @author leehuang
 * @version 1.0
 */
public class Com001ReqService {
    private String serno;//交易流水号
    private Service service;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Com001ReqService{" +
                "serno='" + serno + '\'' +
                ", service=" + service +
                '}';
    }
}