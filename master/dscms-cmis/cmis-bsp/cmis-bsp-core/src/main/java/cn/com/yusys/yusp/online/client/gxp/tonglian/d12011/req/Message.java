package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req;

import cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead;

/**
 * 请求Message：卡片信息查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Message {
    private GxpReqHead head;
    private Body body;

    public GxpReqHead getHead() {
        return head;
    }

    public void setHead(GxpReqHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
