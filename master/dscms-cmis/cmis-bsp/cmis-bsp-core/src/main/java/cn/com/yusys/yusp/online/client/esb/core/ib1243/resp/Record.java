package cn.com.yusys.yusp.online.client.esb.core.ib1243.resp;

import java.math.BigDecimal;


/**
 * 响应Service：交易账户信息输出
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String kehuzhao;//客户账号
    private String zhhaxhao;//账号序号
    private String zhanghao;//负债账号
    private BigDecimal zhanghye;//账户余额
    private String zhyyjgou;//账户营业机构
    private String zhhuzwmc;//账户名称
    private String beizhuxx;//备注信息
    private BigDecimal jiaoyije;//交易金额
    private String shoufdma;//收费代码
    private String shfdmamc;//收费代码名称
    private Integer shuliang;//数量
    private BigDecimal yingshfy;//应收费用
    private BigDecimal shifujne;//实付金额
    private String beiyonzd;//备用字段
    private String dxzhxhao;//待销账序号
    private String sffeiybz;//是否费用标志


    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaxhao() {
        return zhhaxhao;
    }

    public void setZhhaxhao(String zhhaxhao) {
        this.zhhaxhao = zhhaxhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public String getZhyyjgou() {
        return zhyyjgou;
    }

    public void setZhyyjgou(String zhyyjgou) {
        this.zhyyjgou = zhyyjgou;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public Integer getShuliang() {
        return shuliang;
    }

    public void setShuliang(Integer shuliang) {
        this.shuliang = shuliang;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getShifujne() {
        return shifujne;
    }

    public void setShifujne(BigDecimal shifujne) {
        this.shifujne = shifujne;
    }

    public String getBeiyonzd() {
        return beiyonzd;
    }

    public void setBeiyonzd(String beiyonzd) {
        this.beiyonzd = beiyonzd;
    }

    public String getDxzhxhao() {
        return dxzhxhao;
    }

    public void setDxzhxhao(String dxzhxhao) {
        this.dxzhxhao = dxzhxhao;
    }

    public String getSffeiybz() {
        return sffeiybz;
    }

    public void setSffeiybz(String sffeiybz) {
        this.sffeiybz = sffeiybz;
    }

    @Override
    public String toString() {
        return "Service{" +
                "kehuzhao='" + kehuzhao + '\'' +
                "zhhaxhao='" + zhhaxhao + '\'' +
                "zhanghao='" + zhanghao + '\'' +
                "zhanghye='" + zhanghye + '\'' +
                "zhyyjgou='" + zhyyjgou + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "shoufdma='" + shoufdma + '\'' +
                "shfdmamc='" + shfdmamc + '\'' +
                "shuliang='" + shuliang + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "shifujne='" + shifujne + '\'' +
                "beiyonzd='" + beiyonzd + '\'' +
                "dxzhxhao='" + dxzhxhao + '\'' +
                "sffeiybz='" + sffeiybz + '\'' +
                '}';
    }
}  
