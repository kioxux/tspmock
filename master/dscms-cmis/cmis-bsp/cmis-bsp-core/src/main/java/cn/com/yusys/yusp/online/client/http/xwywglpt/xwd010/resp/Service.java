package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd010.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/9/3 15:45
 * @since 2021/9/3 15:45
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private String code;//响应码
    private String message;//响应信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
