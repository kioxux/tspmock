package cn.com.yusys.yusp.online.client.esb.yphsxt.xdjzzy.req;

/**
 * 请求Service：押品信息同步及引入
 *
 * @author chenyong
 * @version 1.0
 */
public class XdjzzyReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "XdjzzyReqService{" +
                "service=" + service +
                '}';
    }
}
