package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.resp;

/**
 * 响应Service：发票补录信息推送（信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erortx;//响应代码
    private String retcod;//响应信息

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getRetcod() {
        return retcod;
    }

    public void setRetcod(String retcod) {
        this.retcod = retcod;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erortx='" + erortx + '\'' +
                "retcod='" + retcod + '\'' +
                '}';
    }
}
