package cn.com.yusys.yusp.web.server.oca.xdxt0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.oca.xdxt0014.req.Xdxt0014ReqDto;
import cn.com.yusys.yusp.dto.server.oca.xdxt0014.resp.Xdxt0014RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.req.Xdxt0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0014.resp.Xdxt0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:字典项对象通用列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0014:字典项对象通用列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0014Resource.class);
    @Autowired
    private DscmsXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0014
     * 交易描述：字典项对象通用列表查询
     *
     * @param xdxt0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("字典项对象通用列表查询")
    @PostMapping("/xdxt0014")
    //@Idempotent({"xdcaxt0014", "#xdxt0014ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0014RespDto xdxt0014(@Validated @RequestBody Xdxt0014ReqDto xdxt0014ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0014.key, DscmsEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014ReqDto));
        Xdxt0014DataReqDto xdxt0014DataReqDto = new Xdxt0014DataReqDto();// 请求Data： 字典项对象通用列表查询
        Xdxt0014DataRespDto xdxt0014DataRespDto = new Xdxt0014DataRespDto();// 响应Data：字典项对象通用列表查询
        Xdxt0014RespDto xdxt0014RespDto = new Xdxt0014RespDto();
        cn.com.yusys.yusp.dto.server.oca.xdxt0014.req.Data reqData = null; // 请求Data：字典项对象通用列表查询
        cn.com.yusys.yusp.dto.server.oca.xdxt0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.oca.xdxt0014.resp.Data();// 响应Data：字典项对象通用列表查询

        try {
            // 从 xdxt0014ReqDto获取 reqData
            reqData = xdxt0014ReqDto.getData();
            // 将 reqData 拷贝到xdxt0014DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0014DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0014.key, DscmsEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataReqDto));
            ResultDto<Xdxt0014DataRespDto> xdxt0014DataResultDto = dscmsXtClientService.xdxt0014(xdxt0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0014.key, DscmsEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0014RespDto.setErorcd(Optional.ofNullable(xdxt0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0014RespDto.setErortx(Optional.ofNullable(xdxt0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0014DataResultDto.getCode())) {
                xdxt0014DataRespDto = xdxt0014DataResultDto.getData();
                xdxt0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0014DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0014.key, DscmsEnum.TRADE_CODE_XDXT0014.value, e.getMessage());
            xdxt0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0014RespDto.setDatasq(servsq);

        xdxt0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0014.key, DscmsEnum.TRADE_CODE_XDXT0014.value, JSON.toJSONString(xdxt0014RespDto));
        return xdxt0014RespDto;
    }
}
