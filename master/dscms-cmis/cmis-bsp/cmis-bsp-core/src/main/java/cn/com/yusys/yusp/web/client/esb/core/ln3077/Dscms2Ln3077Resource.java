package cn.com.yusys.yusp.web.client.esb.core.ln3077;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.req.Ln3077ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.resp.Ln3077RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3077.req.Ln3077ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3077.resp.Ln3077RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3077)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3077Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3077Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3077
     * 交易描述：客户账本息调整
     *
     * @param ln3077ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3077:客户账本息调整")
    @PostMapping("/ln3077")
    protected @ResponseBody
    ResultDto<Ln3077RespDto> ln3077(@Validated @RequestBody Ln3077ReqDto ln3077ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value, JSON.toJSONString(ln3077ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3077.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3077.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3077.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3077.resp.Service();
        Ln3077ReqService ln3077ReqService = new Ln3077ReqService();
        Ln3077RespService ln3077RespService = new Ln3077RespService();
        Ln3077RespDto ln3077RespDto = new Ln3077RespDto();
        ResultDto<Ln3077RespDto> ln3077ResultDto = new ResultDto<Ln3077RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3077ReqDto转换成reqService
            BeanUtils.copyProperties(ln3077ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3077.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3077ReqService.setService(reqService);
            // 将ln3077ReqService转换成ln3077ReqServiceMap
            Map ln3077ReqServiceMap = beanMapUtil.beanToMap(ln3077ReqService);
            context.put("tradeDataMap", ln3077ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3077.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3077RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3077RespService.class, Ln3077RespService.class);
            respService = ln3077RespService.getService();

            ln3077ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3077ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3077RespDto
                BeanUtils.copyProperties(respService, ln3077RespDto);
                ln3077ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3077ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3077ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3077ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value, e.getMessage());
            ln3077ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3077ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3077ResultDto.setData(ln3077RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3077.key, EsbEnum.TRADE_CODE_LN3077.value, JSON.toJSONString(ln3077ResultDto));
        return ln3077ResultDto;
    }
}
