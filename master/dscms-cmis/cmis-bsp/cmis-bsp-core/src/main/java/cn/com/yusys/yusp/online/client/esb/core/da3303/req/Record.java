package cn.com.yusys.yusp.online.client.esb.core.da3303.req;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产信息维护
 */
public class Record {
    private String dkjiejuh;//贷款借据号
    private BigDecimal huankjee;//还款金额

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", huankjee=" + huankjee +
                '}';
    }
}
