package cn.com.yusys.yusp.web.client.esb.lsnp.lsnp01;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.LoanList;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.Lsnp01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.MortList;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.Lsnp01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.Lsnp01ReqService;
import cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.Lsnp01RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:信贷业务零售评级
 **/
@Api(tags = "BSP封装调用零售内评系统的接口处理类(lsnp01)")
@RestController
@RequestMapping("/api/dscms2lsnp")
public class Dscms2Lsnp01Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Lsnp01Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：lsnp01
     * 交易描述：信贷业务零售评级
     *
     * @param lsnp01ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("lsnp01:信贷业务零售评级")
    @PostMapping("/lsnp01")
    protected @ResponseBody
    ResultDto<Lsnp01RespDto> lsnp01(@Validated @RequestBody Lsnp01ReqDto lsnp01ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value, JSON.toJSONString(lsnp01ReqDto));
        cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.Service();
        cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.Service();
        Lsnp01ReqService lsnp01ReqService = new Lsnp01ReqService();
        Lsnp01RespService lsnp01RespService = new Lsnp01RespService();
        Lsnp01RespDto lsnp01RespDto = new Lsnp01RespDto();
        ResultDto<Lsnp01RespDto> lsnp01ResultDto = new ResultDto<Lsnp01RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将lsnp01ReqDto转换成reqService
            BeanUtils.copyProperties(lsnp01ReqDto, reqService);
            if (CollectionUtils.nonEmpty(lsnp01ReqDto.getList())) {
                List<cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.List> reqList = Optional.ofNullable(lsnp01ReqDto.getList()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.Record> records = Optional.of(reqList.parallelStream().map(req -> {
                    cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.Record service = new cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.Record();
                    BeanUtils.copyProperties(req, service);
                    List<LoanList> loanList = req.getLoanList();
                    List<MortList> mortList = req.getMortList();
//                    String loanDataStr = JSON.toJSONString(loanList);
//                    String mortDataStr = JSON.toJSONString(mortList);
                    String loanDataStr = JSON.toJSONString(loanList, SerializerFeature.WriteMapNullValue);
                    String mortDataStr = JSON.toJSONString(mortList, SerializerFeature.WriteMapNullValue);
                    service.setLoanData(loanDataStr);
                    service.setMortData(mortDataStr);
                    return service;
                }).collect(Collectors.toList())).orElse(new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.List list = new cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req.List();
                list.setRecord(records);
                reqService.setList(list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LSNP01.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_LSNP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_LSNP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_LSP.key, EsbEnum.SERVTP_LSP.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_LSP.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_LSP.key, EsbEnum.SERVTP_LSP.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水

            lsnp01ReqService.setService(reqService);
            // 将lsnp01ReqService转换成lsnp01ReqServiceMap
            Map lsnp01ReqServiceMap = beanMapUtil.beanToMap(lsnp01ReqService);

            context.put("tradeDataMap", lsnp01ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LSNP01.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            lsnp01RespService = beanMapUtil.mapToBean(tradeDataMap, Lsnp01RespService.class, Lsnp01RespService.class);
            respService = lsnp01RespService.getService();

            lsnp01ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            lsnp01ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Lsnp01RespDto
                BeanUtils.copyProperties(respService, lsnp01RespDto);
                // 响应报文示例：
//                  <list>
//                    <record>
//                        <busi_seq>SQ20213337440</busi_seq>
//                        <cred_sub_no>111</cred_sub_no>
//                        <riskData>
//                            <![CDATA[[{inpret_val:&quot;&quot;,inpret_val_risk_lvl:&quot;&quot;,apply_score:&quot;604&quot;,apply_score_risk_lvl:&quot;D&quot;,lmt_advice:&quot;540000.0&quot;,rule_risk_lvl:&quot;R&quot;,complex_risk_lvl:&quot;RD&quot;,cred_sub_no:&quot;111&quot;,busi_type:&quot;022040&quot;,limit_flag:&quot;0&quot;}]]]>
//                        </riskData>
//                    </record>
//                </list>
                cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.List lsnp01RespList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.List());
                respService.setList(lsnp01RespList);
                List<cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.Record> respRecordList = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.Record>());
                // 遍历record传值塞入
                List<cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.List> lsnp01RespDtoLists = new ArrayList<cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.List>();

                if (CollectionUtils.nonEmpty(respRecordList)) {
                    for (cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.Record respRecord : respRecordList) {
                        cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.List lsnp01RespDtoList = new cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.List();
                        BeanUtils.copyProperties(respRecord, lsnp01RespDtoList);
                        String riskDataStr = respRecord.getRiskData();
                        java.util.List<cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.RiskData> lsnp01RespRiskDataList = JSON.parseArray(riskDataStr, cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.RiskData.class);
                        java.util.List<cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.RiskData> riskDatas = new ArrayList<>();
                        if (CollectionUtils.nonEmpty(lsnp01RespRiskDataList)) {
                            for (cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp.RiskData lsnp01RespRiskData : lsnp01RespRiskDataList) {
                                cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.RiskData riskData = new cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.RiskData();
                                BeanUtils.copyProperties(lsnp01RespRiskData, riskData);
                                riskDatas.add(riskData);
                            }
                            lsnp01RespDtoList.setRiskData(riskDatas);
                        }
                        lsnp01RespDtoLists.add(lsnp01RespDtoList);
                    }
                    lsnp01RespDto.setList(lsnp01RespDtoLists);
                }
                lsnp01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                lsnp01ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                lsnp01ResultDto.setCode(EpbEnum.EPB099999.key);
                lsnp01ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value, e.getMessage());
            lsnp01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            lsnp01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        lsnp01ResultDto.setData(lsnp01RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LSNP01.key, EsbEnum.TRADE_CODE_LSNP01.value, JSON.toJSONString(lsnp01ResultDto));
        return lsnp01ResultDto;
    }
}
