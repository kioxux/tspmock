package cn.com.yusys.yusp.web.server.biz.xdsx0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0008.req.Xdsx0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0008.resp.Xdsx0008RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0008.req.Xdsx0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0008.resp.Xdsx0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:单一客户人工限额同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0008:单一客户人工限额同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0008Resource.class);
    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0008
     * 交易描述：单一客户人工限额同步
     *
     * @param xdsx0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdsx0008:单一客户人工限额同步")
    @PostMapping("/xdsx0008")
    //@Idempotent({"xdcasx0008", "#xdsx0008ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0008RespDto xdsx0008(@Validated @RequestBody Xdsx0008ReqDto xdsx0008ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008ReqDto));
        Xdsx0008DataReqDto xdsx0008DataReqDto = new Xdsx0008DataReqDto();// 请求Data： 单一客户人工限额同步
        Xdsx0008DataRespDto xdsx0008DataRespDto = new Xdsx0008DataRespDto();// 响应Data：单一客户人工限额同步
        Xdsx0008RespDto xdsx0008RespDto = new Xdsx0008RespDto();// 响应Dto：单一客户人工限额同步
        cn.com.yusys.yusp.dto.server.biz.xdsx0008.req.Data reqData = null; // 请求Data：单一客户人工限额同步
        cn.com.yusys.yusp.dto.server.biz.xdsx0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0008.resp.Data();// 响应Data：单一客户人工限额同步
        try {
            // 从 xdsx0008ReqDto获取 reqData
            reqData = xdsx0008ReqDto.getData();
            // 将 reqData 拷贝到xdsx0008DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0008DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008DataReqDto));
            ResultDto<Xdsx0008DataRespDto> xdsx0008DataResultDto = dscmsBizSxClientService.xdsx0008(xdsx0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0008RespDto.setErorcd(Optional.ofNullable(xdsx0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0008RespDto.setErortx(Optional.ofNullable(xdsx0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0008DataResultDto.getCode())) {
                xdsx0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdsx0008DataRespDto = xdsx0008DataResultDto.getData();
                // 将 xdsx0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, e.getMessage());
            xdsx0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0008RespDto.setDatasq(servsq);

        xdsx0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0008.key, DscmsEnum.TRADE_CODE_XDSX0008.value, JSON.toJSONString(xdsx0008RespDto));
        return xdsx0008RespDto;
    }
}