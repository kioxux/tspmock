package cn.com.yusys.yusp.web.client.esb.core.ln3006;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3006.req.Ln3006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3006.resp.Ln3006RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3006.req.Ln3006ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3006.resp.Listnm_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3006.resp.Ln3006RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:贷款产品组合查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3006)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Ln3006Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3006
     * 交易描述：贷款产品组合查询
     *
     * @param ln3006ReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("ln3006:贷款产品组合查询")
    @PostMapping("/ln3006")
    protected @ResponseBody
    ResultDto<Ln3006RespDto> ln3006(@Validated @RequestBody Ln3006ReqDto ln3006ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3006.key, EsbEnum.TRADE_CODE_LN3006.value, JSON.toJSONString(ln3006ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.ln3006.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3006.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.ln3006.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3006.resp.Service();
        Ln3006ReqService ln3006ReqService = new Ln3006ReqService();
        Ln3006RespService ln3006RespService = new Ln3006RespService();
        Ln3006RespDto ln3006RespDto = new Ln3006RespDto();
        ResultDto<Ln3006RespDto> ln3006ResultDto = new ResultDto<Ln3006RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将ln3006ReqDto转换成reqService
			BeanUtils.copyProperties(ln3006ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3006.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

			ln3006ReqService.setService(reqService);
			// 将ln3006ReqService转换成ln3006ReqServiceMap
			Map ln3006ReqServiceMap = beanMapUtil.beanToMap(ln3006ReqService);
			context.put("tradeDataMap", ln3006ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3006.key, EsbEnum.TRADE_CODE_LN3006.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3006.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3006.key, EsbEnum.TRADE_CODE_LN3006.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			ln3006RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3006RespService.class, Ln3006RespService.class);
			respService = ln3006RespService.getService();
			//  将respService转换成Ln3006RespDto
			ln3006ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			ln3006ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Ln3007RespDto
				BeanUtils.copyProperties(respService, ln3006RespDto);
				Listnm_ARRAY listnm = Optional.ofNullable(respService.getListnm_ARRAY()).orElse(new Listnm_ARRAY());
				List<cn.com.yusys.yusp.online.client.esb.core.ln3006.resp.Record> records = listnm.getRecord();
				if (CollectionUtils.nonEmpty(records)) {
					List<cn.com.yusys.yusp.dto.client.esb.core.ln3006.resp.Listnm> respListnm = records.parallelStream().map(record -> {
						cn.com.yusys.yusp.dto.client.esb.core.ln3006.resp.Listnm temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3006.resp.Listnm();
						BeanUtils.copyProperties(record, temp);
						return temp;
					}).collect(Collectors.toList());
					ln3006RespDto.setListnm(respListnm);
				}
				ln3006ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				ln3006ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				ln3006ResultDto.setCode(EpbEnum.EPB099999.key);
				ln3006ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3006.key, EsbEnum.TRADE_CODE_LN3006.value, e.getMessage());
			ln3006ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			ln3006ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		ln3006ResultDto.setData(ln3006RespDto);
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3006.key, EsbEnum.TRADE_CODE_LN3006.value, JSON.toJSONString(ln3006ResultDto));
        return ln3006ResultDto;
    }
}
