package cn.com.yusys.yusp.online.client.esb.core.ln3006.resp;

/**
 * 响应Service：贷款产品组合查询
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
	private String erorcd;//响应码
	private String erortx;// 响应信息
	private String servsq;// 渠道流水
	private String datasq;//全局流水

    private Integer zongbish;//总笔数
    private Listnm_ARRAY listnm_ARRAY;//产品组合查询输出

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public Integer getZongbish() {
		return zongbish;
	}

	public void setZongbish(Integer zongbish) {
		this.zongbish = zongbish;
	}

	public Listnm_ARRAY getListnm_ARRAY() {
		return listnm_ARRAY;
	}

	public void setListnm_ARRAY(Listnm_ARRAY listnm_ARRAY) {
		this.listnm_ARRAY = listnm_ARRAY;
	}

	@Override
	public String toString() {
		return "Service{" +
				"erorcd='" + erorcd + '\'' +
				", erortx='" + erortx + '\'' +
				", servsq='" + servsq + '\'' +
				", datasq='" + datasq + '\'' +
				", zongbish=" + zongbish +
				", listnm_ARRAY=" + listnm_ARRAY +
				'}';
	}
}
