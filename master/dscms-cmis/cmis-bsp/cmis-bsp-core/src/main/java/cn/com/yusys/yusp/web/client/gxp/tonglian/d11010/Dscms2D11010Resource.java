package cn.com.yusys.yusp.web.client.gxp.tonglian.d11010;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.req.D11010ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.resp.D11010RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp.D11010RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d11010)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D11010Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D11010Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 客户基本资料查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d11010:客户基本资料查询")
    @PostMapping("/d11010")
    protected @ResponseBody
    ResultDto<D11010RespDto> d11010(@Validated @RequestBody D11010ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11010.key, GxpEnum.TRADE_CODE_D11010.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.D11010ReqMessage d11010ReqMessage = null;//请求Message：现金（大额）放款接口
        D11010RespMessage d11010RespMessage = new D11010RespMessage();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.Message reqMessage = null;//请求Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp.Message();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.Body();//请求Body：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp.Body();//响应Body：现金（大额）放款接口

        D11010RespDto d11010RespDto = new D11010RespDto();//响应Dto：现金（大额）放款接口
        ResultDto<D11010RespDto> d11010ResultDto = new ResultDto<>();//响应ResultDto：现金（大额）放款接口

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D11010.key, GxpEnum.TRADE_CODE_D11010.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D11010ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.Message::setBody, reqBody).build();
            // 给d11010ReqMessage赋值
            d11010ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.D11010ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req.D11010ReqMessage::setMessage, reqMessage).build();

            // 将d11010ReqService转换成d11010ReqServiceMap
            Map d11010ReqMessageMap = beanMapUtil.beanToMap(d11010ReqMessage);
            context.put("tradeDataMap", d11010ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11010.key, GxpEnum.TRADE_CODE_D11010.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D11010.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11010.key, GxpEnum.TRADE_CODE_D11010.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d11010RespMessage = beanMapUtil.mapToBean(tradeDataMap, D11010RespMessage.class, D11010RespMessage.class);//响应Message：现金（大额）放款接口
            respMessage = d11010RespMessage.getMessage();//响应Message：现金（大额）放款接口
            gxpRespHead = respMessage.getHead();//响应Head：现金（大额）放款接口
            //  将D11010RespDto封装到ResultDto<D11010RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                respBody = respMessage.getBody();
                //  将respBody转换成D11010RespDto
                BeanUtils.copyProperties(respBody, d11010RespDto);
                d11010ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d11010ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d11010ResultDto.setCode(EpbEnum.EPB099999.key);
                d11010ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d11010ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11010.key, GxpEnum.TRADE_CODE_D11010.value, e.getMessage());
            d11010ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d11010ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d11010ResultDto.setData(d11010RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D11010.key, GxpEnum.TRADE_CODE_D11010.value, JSON.toJSONString(d11010ResultDto));
        return d11010ResultDto;
    }
}
