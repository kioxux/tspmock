package cn.com.yusys.yusp.web.server.biz.xdca0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdca0005.req.Xdca0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdca0005.resp.Xdca0005RespDto;
import cn.com.yusys.yusp.dto.server.xdca0005.req.Xdca0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0005.resp.Xdca0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCaClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:大额分期合同签订接口
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0005:大额分期合同签订接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdca0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0005Resource.class);

	@Autowired
	private DscmsBizCaClientService dscmsBizCaClientService;

    /**
     * 交易码：xdca0005
     * 交易描述：大额分期合同签订接口
     *
     * @param xdca0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期合同签订接口")
    @PostMapping("/xdca0005")
//   @Idempotent({"xdcaca0005", "#xdca0005ReqDto.datasq"})
    protected @ResponseBody
	Xdca0005RespDto xdca0005(@Validated @RequestBody Xdca0005ReqDto xdca0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005ReqDto));
        Xdca0005DataReqDto xdca0005DataReqDto = new Xdca0005DataReqDto();// 请求Data： 大额分期合同签订接口
        Xdca0005DataRespDto xdca0005DataRespDto = new Xdca0005DataRespDto();// 响应Data：大额分期合同签订接口
		Xdca0005RespDto xdca0005RespDto = new Xdca0005RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdca0005.req.Data reqData = null; // 请求Data：大额分期合同签订接口
		cn.com.yusys.yusp.dto.server.biz.xdca0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdca0005.resp.Data();// 响应Data：大额分期合同签订接口
        //  此处包导入待确定 结束
        try {
            // 从 xdca0005ReqDto获取 reqData
            reqData = xdca0005ReqDto.getData();
            // 将 reqData 拷贝到xdca0005DataReqDto
            BeanUtils.copyProperties(reqData, xdca0005DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005DataReqDto));
            ResultDto<Xdca0005DataRespDto> xdca0005DataResultDto = dscmsBizCaClientService.xdca0005(xdca0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdca0005RespDto.setErorcd(Optional.ofNullable(xdca0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdca0005RespDto.setErortx(Optional.ofNullable(xdca0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdca0005DataResultDto.getCode())) {
                xdca0005DataRespDto = xdca0005DataResultDto.getData();
                xdca0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdca0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdca0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdca0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, e.getMessage());
            xdca0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdca0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdca0005RespDto.setDatasq(servsq);

        xdca0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0005.key, DscmsEnum.TRADE_CODE_XDCA0005.value, JSON.toJSONString(xdca0005RespDto));
        return xdca0005RespDto;
    }
}
