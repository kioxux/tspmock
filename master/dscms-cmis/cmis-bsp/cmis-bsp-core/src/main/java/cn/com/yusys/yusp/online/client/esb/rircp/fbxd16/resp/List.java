package cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp;

/**
 * 响应Service：取得蚂蚁核销记录表的核销借据号一览
 *
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Record> record;

    public java.util.List<cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
