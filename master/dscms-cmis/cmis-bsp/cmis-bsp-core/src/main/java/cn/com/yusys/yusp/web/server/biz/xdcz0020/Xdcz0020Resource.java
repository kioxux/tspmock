package cn.com.yusys.yusp.web.server.biz.xdcz0020;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0020.req.Xdcz0020ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0020.resp.Xdcz0020RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0020.req.Xdcz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0020.resp.Xdcz0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小贷用途承诺书文本生成pdf
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0020:小贷用途承诺书文本生成pdf")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0020Resource.class);
    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;
    /**
     * 交易码：xdcz0020
     * 交易描述：小贷用途承诺书文本生成pdf
     *
     * @param xdcz0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小贷用途承诺书文本生成pdf")
    @PostMapping("/xdcz0020")
   //@Idempotent({"xdcz0020", "#xdcz0020ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0020RespDto xdcz0020(@Validated @RequestBody Xdcz0020ReqDto xdcz0020ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020ReqDto));
        Xdcz0020DataReqDto xdcz0020DataReqDto = new Xdcz0020DataReqDto();// 请求Data： 电子保函开立
        Xdcz0020DataRespDto xdcz0020DataRespDto = new Xdcz0020DataRespDto();// 响应Data：电子保函开立
        Xdcz0020RespDto xdcz0020RespDto = new Xdcz0020RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdcz0020.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdcz0020.req.Data(); // 请求Data：电子保函开立
        cn.com.yusys.yusp.dto.server.biz.xdcz0020.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdcz0020.resp.Data();// 响应Data：电子保函开立
        try {
            // 从 xdcz0020ReqDto获取 reqData
            reqData = xdcz0020ReqDto.getData();
            // 将 reqData 拷贝到xdcz0020DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0020DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataReqDto));
            ResultDto<Xdcz0020DataRespDto> xdcz0020DataResultDto = dscmsBizCzClientService.xdcz0020(xdcz0020DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0020RespDto.setErorcd(Optional.ofNullable(xdcz0020DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0020RespDto.setErortx(Optional.ofNullable(xdcz0020DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0020DataResultDto.getCode())) {
                xdcz0020DataRespDto = xdcz0020DataResultDto.getData();
                xdcz0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0020DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, e.getMessage());
            xdcz0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0020RespDto.setDatasq(servsq);

        xdcz0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0020.key, DscmsEnum.TRADE_CODE_XDCZ0020.value, JSON.toJSONString(xdcz0020RespDto));
        return xdcz0020RespDto;
    }
}
