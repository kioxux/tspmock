package cn.com.yusys.yusp.web.client.esb.core.ln3031;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3031.*;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3031.req.Ln3031ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3031.req.Lstdkhbjh_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3031.req.Lstdkhkfs_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3031.req.Lstdkhkzh_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3031.resp.Ln3031RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:贷款资料维护（关于还款）
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3031Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Ln3031Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3031
     * 交易描述：贷款资料维护（关于还款）
     *
     * @param ln3031ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3031:贷款资料维护")
    @PostMapping("/ln3031")
    protected @ResponseBody
    ResultDto<Ln3031RespDto> ln3031(@Validated @RequestBody Ln3031ReqDto ln3031ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3031.key, EsbEnum.TRADE_CODE_LN3031.value, JSON.toJSONString(ln3031ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3031.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3031.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3031.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3031.resp.Service();
        Ln3031ReqService ln3031ReqService = new Ln3031ReqService();
        Ln3031RespService ln3031RespService = new Ln3031RespService();
        Ln3031RespDto ln3031RespDto = new Ln3031RespDto();
        ResultDto<Ln3031RespDto> ln3031ResultDto = new ResultDto<Ln3031RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3031ReqDto转换成reqService
            BeanUtils.copyProperties(ln3031ReqDto, reqService);

            if (CollectionUtils.nonEmpty(ln3031ReqDto.getLstdkhbjh())){
                List<Lstdkhbjh> lstdkhbjhList = Optional.ofNullable(ln3031ReqDto.getLstdkhbjh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhbjh.Record> dkhbjhRecords = lstdkhbjhList.parallelStream().map(dkhbjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhbjh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhbjh.Record();
                    BeanUtils.copyProperties(dkhbjh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhbjh_ARRAY lstdkhbjh = new Lstdkhbjh_ARRAY();
                lstdkhbjh.setRecord(dkhbjhRecords);
                reqService.setLstdkhbjh_ARRAY(lstdkhbjh);
            }

            if (CollectionUtils.nonEmpty(ln3031ReqDto.getLstdkhkfs())){
                List<Lstdkhkfs> lstdkhkfsList = Optional.ofNullable(ln3031ReqDto.getLstdkhkfs()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhkfs.Record> dkhkfsRecords = lstdkhkfsList.parallelStream().map(dkhkfs -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhkfs.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhkfs.Record();
                    BeanUtils.copyProperties(dkhkfs, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhkfs_ARRAY lstdkhkfs = new Lstdkhkfs_ARRAY();
                lstdkhkfs.setRecord(dkhkfsRecords);
                reqService.setLstdkhkfs_ARRAY(lstdkhkfs);
            }

            if (CollectionUtils.nonEmpty(ln3031ReqDto.getLstdkhkzh())){
                List<Lstdkhkzh> lstdkhkzhList = Optional.ofNullable(ln3031ReqDto.getLstdkhkzh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhkzh.Record> dkhkzhRecords = lstdkhkzhList.parallelStream().map(dkhkzh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhkzh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhkzh.Record();
                    BeanUtils.copyProperties(dkhkzh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhkzh_ARRAY lstdkhkzh = new Lstdkhkzh_ARRAY();
                lstdkhkzh.setRecord(dkhkzhRecords);
                reqService.setLstdkhkzh_ARRAY(lstdkhkzh);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3031.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3031ReqService.setService(reqService);
            // 将ln3031ReqService转换成ln3031ReqServiceMap
            Map ln3031ReqServiceMap = beanMapUtil.beanToMap(ln3031ReqService);
            context.put("tradeDataMap", ln3031ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3031.key, EsbEnum.TRADE_CODE_LN3031.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3031.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3031.key, EsbEnum.TRADE_CODE_LN3031.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3031RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3031RespService.class, Ln3031RespService.class);
            respService = ln3031RespService.getService();
            //  将respService转换成ln3031RespDto
            BeanUtils.copyProperties(respService, ln3031RespDto);
            ln3031ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3031ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, ln3031RespDto);
                ln3031ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3031ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3031ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3031ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3031.key, EsbEnum.TRADE_CODE_LN3031.value, e.getMessage());
            ln3031ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3031ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3031ResultDto.setData(ln3031RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3031.key, EsbEnum.TRADE_CODE_LN3031.value, JSON.toJSONString(ln3031ResultDto));
        return ln3031ResultDto;
    }
}
