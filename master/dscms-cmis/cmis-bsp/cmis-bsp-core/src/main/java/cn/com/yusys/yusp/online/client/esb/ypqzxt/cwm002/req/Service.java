package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm002.req;

/**
 * 请求Service：本异地借阅出库接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:34
 */
public class Service {

    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //   全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String userName; // 柜员名称
    private String orgcode; // 操作员机构号
    private String orgname; // 操作员机构名称
    private String acctBrch; // 账务机构
    private String acctBrchName; // 账务机构名称
    private String gageId; // 核心担保品编号
    private String gageType; // 抵质押类型
    private String gageTypeName; // 抵质押类型名称
    private String gageUser; // 抵押人
    private String inType; // 入库类型
    private String isEstate; // 是否张家港地区不动产
    private String isElectronic; // 是否电子类押品
    private String isMortgage; // 是否住房按揭
    private String jyid; // 借阅ID
    private String maxAmt; // 权利价值
    private String pawn; // 抵押物名称
    private String remark; // 备注描述
    private String isyd; // 是否异地支行

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

    public String getIsyd() {
        return isyd;
    }

    public void setIsyd(String isyd) {
        this.isyd = isyd;
    }

    public String getAcctBrch() {
        return acctBrch;
    }

    public void setAcctBrch(String acctBrch) {
        this.acctBrch = acctBrch;
    }

    public String getAcctBrchName() {
        return acctBrchName;
    }

    public void setAcctBrchName(String acctBrchName) {
        this.acctBrchName = acctBrchName;
    }

    public String getGageId() {
        return gageId;
    }

    public void setGageId(String gageId) {
        this.gageId = gageId;
    }

    public String getGageType() {
        return gageType;
    }

    public void setGageType(String gageType) {
        this.gageType = gageType;
    }

    public String getGageTypeName() {
        return gageTypeName;
    }

    public void setGageTypeName(String gageTypeName) {
        this.gageTypeName = gageTypeName;
    }

    public String getGageUser() {
        return gageUser;
    }

    public void setGageUser(String gageUser) {
        this.gageUser = gageUser;
    }

    public String getInType() {
        return inType;
    }

    public void setInType(String inType) {
        this.inType = inType;
    }

    public String getIsEstate() {
        return isEstate;
    }

    public void setIsEstate(String isEstate) {
        this.isEstate = isEstate;
    }

    public String getIsElectronic() {
        return isElectronic;
    }

    public void setIsElectronic(String isElectronic) {
        this.isElectronic = isElectronic;
    }

    public String getIsMortgage() {
        return isMortgage;
    }

    public void setIsMortgage(String isMortgage) {
        this.isMortgage = isMortgage;
    }

    public String getJyid() {
        return jyid;
    }

    public void setJyid(String jyid) {
        this.jyid = jyid;
    }

    public String getMaxAmt() {
        return maxAmt;
    }

    public void setMaxAmt(String maxAmt) {
        this.maxAmt = maxAmt;
    }

    public String getPawn() {
        return pawn;
    }

    public void setPawn(String pawn) {
        this.pawn = pawn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", userName='" + userName + '\'' +
                ", orgcode='" + orgcode + '\'' +
                ", orgname='" + orgname + '\'' +
                ", acctBrch='" + acctBrch + '\'' +
                ", acctBrchName='" + acctBrchName + '\'' +
                ", gageId='" + gageId + '\'' +
                ", gageType='" + gageType + '\'' +
                ", gageTypeName='" + gageTypeName + '\'' +
                ", gageUser='" + gageUser + '\'' +
                ", inType='" + inType + '\'' +
                ", isEstate='" + isEstate + '\'' +
                ", isElectronic='" + isElectronic + '\'' +
                ", isMortgage='" + isMortgage + '\'' +
                ", jyid='" + jyid + '\'' +
                ", maxAmt='" + maxAmt + '\'' +
                ", pawn='" + pawn + '\'' +
                ", remark='" + remark + '\'' +
                ", isyd='" + isyd + '\'' +
                '}';
    }
}
