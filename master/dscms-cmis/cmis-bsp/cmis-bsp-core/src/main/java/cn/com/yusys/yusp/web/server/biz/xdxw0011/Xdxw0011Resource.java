package cn.com.yusys.yusp.web.server.biz.xdxw0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0011.req.Xdxw0011ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0011.resp.Xdxw0011RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0011.req.Xdxw0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0011.resp.Xdxw0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提交勘验信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0011:提交勘验信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0011Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0011
     * 交易描述：提交勘验信息
     *
     * @param xdxw0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提交勘验信息")
    @PostMapping("/xdxw0011")
    //@Idempotent({"xdcaxw0011", "#xdxw0011ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0011RespDto xdxw0011(@Validated @RequestBody Xdxw0011ReqDto xdxw0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011ReqDto));
        Xdxw0011DataReqDto xdxw0011DataReqDto = new Xdxw0011DataReqDto();// 请求Data： 提交勘验信息
        Xdxw0011DataRespDto xdxw0011DataRespDto = new Xdxw0011DataRespDto();// 响应Data：提交勘验信息
        Xdxw0011RespDto xdxw0011RespDto = new Xdxw0011RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0011.req.Data reqData = null; // 请求Data：提交勘验信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0011.resp.Data();// 响应Data：提交勘验信息
        try {
            // 从 xdxw0011ReqDto获取 reqData
            reqData = xdxw0011ReqDto.getData();
            // 将 reqData 拷贝到xdxw0011DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0011DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011DataReqDto));
            ResultDto<Xdxw0011DataRespDto> xdxw0011DataResultDto = dscmsBizXwClientService.xdxw0011(xdxw0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0011RespDto.setErorcd(Optional.ofNullable(xdxw0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0011RespDto.setErortx(Optional.ofNullable(xdxw0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0011DataResultDto.getCode())) {
                xdxw0011DataRespDto = xdxw0011DataResultDto.getData();
                xdxw0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0011DataRespDto, respData);
            }
        } catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, e.getMessage());
            xdxw0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0011RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, e.getMessage());
            xdxw0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0011RespDto.setDatasq(servsq);

        xdxw0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0011.key, DscmsEnum.TRADE_CODE_XDXW0011.value, JSON.toJSONString(xdxw0011RespDto));
        return xdxw0011RespDto;
    }
}
