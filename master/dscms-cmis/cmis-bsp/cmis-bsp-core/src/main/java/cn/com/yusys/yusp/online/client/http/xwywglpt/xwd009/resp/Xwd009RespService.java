package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.resp;

/**
 * 响应Service：利率申请接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Xwd009RespService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwd009RespService{" +
                "service=" + service +
                '}';
    }
}

