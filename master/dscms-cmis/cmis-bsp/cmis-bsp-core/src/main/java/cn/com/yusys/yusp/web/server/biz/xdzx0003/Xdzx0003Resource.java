package cn.com.yusys.yusp.web.server.biz.xdzx0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzx0003.req.Xdzx0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzx0003.resp.Xdzx0003RespDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.req.Xdzx0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0003.resp.Xdzx0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:征信查询授权状态同步
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZX0003:征信查询授权状态同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdzx0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzx0003Resource.class);
    @Autowired
    private DscmsBizZxClientService dscmsBizZxClientService;

    /**
     * 交易码：xdzx0003
     * 交易描述：征信查询授权状态同步
     *
     * @param xdzx0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("征信查询授权状态同步")
    @PostMapping("/xdzx0003")
    @Idempotent({"xdcazx0003", "#xdzx0003ReqDto.datasq"})
    protected @ResponseBody
    Xdzx0003RespDto xdzx0003(@Validated @RequestBody Xdzx0003ReqDto xdzx0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003ReqDto));
        Xdzx0003DataReqDto xdzx0003DataReqDto = new Xdzx0003DataReqDto();// 请求Data： 征信查询授权状态同步
        Xdzx0003DataRespDto xdzx0003DataRespDto = new Xdzx0003DataRespDto();// 响应Data：征信查询授权状态同步
        Xdzx0003RespDto xdzx0003RespDto = new Xdzx0003RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzx0003.req.Data reqData = null; // 请求Data：征信查询授权状态同步
        cn.com.yusys.yusp.dto.server.biz.xdzx0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzx0003.resp.Data();// 响应Data：征信查询授权状态同步
        try {
            // 从 xdzx0003ReqDto获取 reqData
            reqData = xdzx0003ReqDto.getData();
            // 将 reqData 拷贝到xdzx0003DataReqDto
            BeanUtils.copyProperties(reqData, xdzx0003DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataReqDto));
            ResultDto<Xdzx0003DataRespDto> xdzx0003DataResultDto = dscmsBizZxClientService.xdzx0003(xdzx0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzx0003RespDto.setErorcd(Optional.ofNullable(xdzx0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzx0003RespDto.setErortx(Optional.ofNullable(xdzx0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzx0003DataResultDto.getCode())) {
                xdzx0003DataRespDto = xdzx0003DataResultDto.getData();
                xdzx0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzx0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzx0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzx0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, e.getMessage());
            xdzx0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzx0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzx0003RespDto.setDatasq(servsq);

        xdzx0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0003.key, DscmsEnum.TRADE_CODE_XDZX0003.value, JSON.toJSONString(xdzx0003RespDto));
        return xdzx0003RespDto;
    }
}
