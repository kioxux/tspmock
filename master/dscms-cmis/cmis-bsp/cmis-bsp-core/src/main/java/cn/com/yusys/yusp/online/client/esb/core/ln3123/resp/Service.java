package cn.com.yusys.yusp.online.client.esb.core.ln3123.resp;

/**
 * 响应Service：受托支付查询
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private Lstdkstzf_ARRAY lstdkstzf_ARRAY;//贷款受托支付

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public Lstdkstzf_ARRAY getLstdkstzf_ARRAY() {
        return lstdkstzf_ARRAY;
    }

    public void setLstdkstzf_ARRAY(Lstdkstzf_ARRAY lstdkstzf_ARRAY) {
        this.lstdkstzf_ARRAY = lstdkstzf_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", lstdkstzf_ARRAY=" + lstdkstzf_ARRAY +
                '}';
    }
}
