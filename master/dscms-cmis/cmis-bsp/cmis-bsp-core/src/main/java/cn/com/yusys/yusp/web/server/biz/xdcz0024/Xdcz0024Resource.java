package cn.com.yusys.yusp.web.server.biz.xdcz0024;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0024.req.Xdcz0024ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0024.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0024.resp.Xdcz0024RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0024.req.Xdcz0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0024.resp.Xdcz0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:推送在线保函预约信息
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0024:推送在线保函预约信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0024Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0024
     * 交易描述：推送在线保函预约信息
     *
     * @param xdcz0024ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送在线保函预约信息")
    @PostMapping("/xdcz0024")
   //@Idempotent({"xdcz0024", "#xdcz0024ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0024RespDto xdcz0024(@Validated @RequestBody Xdcz0024ReqDto xdcz0024ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024ReqDto));
        Xdcz0024DataReqDto xdcz0024DataReqDto = new Xdcz0024DataReqDto();// 请求Data： 推送在线保函预约信息
        Xdcz0024DataRespDto xdcz0024DataRespDto = new Xdcz0024DataRespDto();// 响应Data：推送在线保函预约信息

        cn.com.yusys.yusp.dto.server.biz.xdcz0024.req.Data reqData = null; // 请求Data：推送在线保函预约信息
        Data respData = new Data();// 响应Data：推送在线保函预约信息

        Xdcz0024RespDto xdcz0024RespDto = new Xdcz0024RespDto();
        try {
            // 从 xdcz0024ReqDto获取 reqData
            reqData = xdcz0024ReqDto.getData();
            // 将 reqData 拷贝到xdcz0024DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0024DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024DataReqDto));
            ResultDto<Xdcz0024DataRespDto> xdcz0024DataResultDto = dscmsBizCzClientService.xdcz0024(xdcz0024DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0024RespDto.setErorcd(Optional.ofNullable(xdcz0024DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0024RespDto.setErortx(Optional.ofNullable(xdcz0024DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0024DataResultDto.getCode())) {
                xdcz0024DataRespDto = xdcz0024DataResultDto.getData();
                xdcz0024RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0024RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0024DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0024DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, e.getMessage());
            xdcz0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0024RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0024RespDto.setDatasq(servsq);

        xdcz0024RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0024.key, DscmsEnum.TRADE_CODE_XDCZ0024.value, JSON.toJSONString(xdcz0024RespDto));
        return xdcz0024RespDto;
    }
}
