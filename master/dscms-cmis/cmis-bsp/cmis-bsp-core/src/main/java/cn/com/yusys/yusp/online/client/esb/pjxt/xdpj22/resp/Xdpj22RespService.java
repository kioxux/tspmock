package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp;

/**
 * 响应Service：根据批次号查询票号和票面金额
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdpj22RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Xdpj22RespService{" +
				"service=" + service +
				'}';
	}
}
