package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp;

import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.Record;

public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.Record>  record;

    public java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
