package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/28 12:34
 * @since 2021/8/28 12:34
 */
public class List {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
