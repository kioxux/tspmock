package cn.com.yusys.yusp.web.server.biz.xdxw0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0019.req.Xdxw0019ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0019.resp.Xdxw0019RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0019.req.Xdxw0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0019.resp.Xdxw0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:推送决策审批结果（产生信贷批复）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0019:推送决策审批结果（产生信贷批复）")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0019Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0019
     * 交易描述：推送决策审批结果（产生信贷批复）
     *
     * @param xdxw0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送决策审批结果（产生信贷批复）")
    @PostMapping("/xdxw0019")
//    @Idempotent({"xdcaxw0019", "#xdxw0019ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0019RespDto xdxw0019(@Validated @RequestBody Xdxw0019ReqDto xdxw0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019ReqDto));
        Xdxw0019DataReqDto xdxw0019DataReqDto = new Xdxw0019DataReqDto();// 请求Data： 推送决策审批结果（产生信贷批复）
        Xdxw0019DataRespDto xdxw0019DataRespDto;// 响应Data：推送决策审批结果（产生信贷批复）
        Xdxw0019RespDto xdxw0019RespDto = new Xdxw0019RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0019.req.Data reqData = null; // 请求Data：推送决策审批结果（产生信贷批复）
        cn.com.yusys.yusp.dto.server.biz.xdxw0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0019.resp.Data();// 响应Data：推送决策审批结果（产生信贷批复）

        try {
            // 从 xdxw0019ReqDto获取 reqData
            reqData = xdxw0019ReqDto.getData();
            // 将 reqData 拷贝到xdxw0019DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0019DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataReqDto));
            ResultDto<Xdxw0019DataRespDto> xdxw0019DataResultDto = dscmsBizXwClientService.xdxw0019(xdxw0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0019RespDto.setErorcd(Optional.ofNullable(xdxw0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0019RespDto.setErortx(Optional.ofNullable(xdxw0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0019DataResultDto.getCode())) {
                xdxw0019DataRespDto = xdxw0019DataResultDto.getData();
                xdxw0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, e.getMessage());
            xdxw0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0019RespDto.setDatasq(servsq);

        xdxw0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0019.key, DscmsEnum.TRADE_CODE_XDXW0019.value, JSON.toJSONString(xdxw0019RespDto));
        return xdxw0019RespDto;
    }
}
