package cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.resp;

import java.math.BigDecimal;

/**
 * 响应Service：信贷查询牌价信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String rpp;//汇买价
    private String rsp;//汇卖价
    private BigDecimal cpp;//钞买价
    private BigDecimal csp;//钞卖价
    private BigDecimal baserate;//基准价
    private BigDecimal midrate;//中间价

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getRpp() {
        return rpp;
    }

    public void setRpp(String rpp) {
        this.rpp = rpp;
    }

    public String getRsp() {
        return rsp;
    }

    public void setRsp(String rsp) {
        this.rsp = rsp;
    }

    public BigDecimal getCpp() {
        return cpp;
    }

    public void setCpp(BigDecimal cpp) {
        this.cpp = cpp;
    }

    public BigDecimal getCsp() {
        return csp;
    }

    public void setCsp(BigDecimal csp) {
        this.csp = csp;
    }

    public BigDecimal getBaserate() {
        return baserate;
    }

    public void setBaserate(BigDecimal baserate) {
        this.baserate = baserate;
    }

    public BigDecimal getMidrate() {
        return midrate;
    }

    public void setMidrate(BigDecimal midrate) {
        this.midrate = midrate;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "rpp='" + rpp + '\'' +
                "rsp='" + rsp + '\'' +
                "cpp='" + cpp + '\'' +
                "csp='" + csp + '\'' +
                "baserate='" + baserate + '\'' +
                "midrate='" + midrate + '\'' +
                '}';
    }
}  
