package cn.com.yusys.yusp.online.client.esb.core.ln3108.resp;

/**
 * 响应Dto：贷款组合查询
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private Integer zongbish;//总笔数
    private List lstdkzhzb_ARRAY;//贷款账户主表

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List getLstdkzhzb_ARRAY() {
        return lstdkzhzb_ARRAY;
    }

    public void setLstdkzhzb_ARRAY(List lstdkzhzb_ARRAY) {
        this.lstdkzhzb_ARRAY = lstdkzhzb_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zongbish=" + zongbish +
                ", lstdkzhzb_ARRAY=" + lstdkzhzb_ARRAY +
                '}';
    }
}
