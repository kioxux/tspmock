package cn.com.yusys.yusp.online.client.esb.core.mbt999.req;

import java.math.BigDecimal;

/**
 * 请求Service：V5通用记账
 */
public class Service {
	private String prcscd;//处理码
	private String servtp;//渠道
	private String servsq;//渠道流水
	private String userid;//柜员号
	private String brchno;//部门号
	private String datasq;//全局流水
	private String servdt;//交易日期
	private String servti;//交易时间
	private String shebeilx;//设备类型
	private String shebeibh;//设备号
	private String qudaofwm;//渠道服务码
	private String xitongbs;//系统标识号
	private String qiatyima;//前台交易码
	private String huobdaih;//货币代号
	private String zhngha01;//账号01
	private String zhngha02;//账号02
	private String zhngha03;//账号03
	private String zhngha04;//账号04
	private String zhngha05;//账号05
	private String zhngha06;//账号06
	private String zhngha07;//账号07
	private String zhngha08;//账号08
	private String zhngha09;//账号09
	private String zhngha10;//账号10
	private String zhnghajf;//借方账号
	private String zhnghadf;//贷方账号
	private String zhnghaff;//付方账号
	private String zhnghasf;//收方账号
	private String jigouh01;//机构号01
	private String jigouh02;//机构号02
	private String jigouh03;//机构号03
	private String jigouh04;//机构号04
	private String jigouh05;//机构号05
	private String jigouh06;//机构号06
	private String jigouh07;//机构号07
	private String jigouh08;//机构号08
	private String jigouh09;//机构号09
	private String jigouh10;//机构号10
	private String jigouh11;//机构号11
	private String jigouh12;//机构号12
	private String jigouh13;//机构号13
	private BigDecimal jyjine01;//金额01
	private BigDecimal jyjine02;//金额02
	private BigDecimal jyjine03;//金额03
	private BigDecimal jyjine04;//金额04
	private BigDecimal jyjine05;//金额05
	private BigDecimal jyjine06;//金额06
	private BigDecimal jyjine07;//金额07
	private BigDecimal jyjine08;//金额08
	private BigDecimal jyjine09;//金额09
	private BigDecimal jyjine10;//金额10
	private BigDecimal jyjine11;//金额11
	private BigDecimal jyjine12;//金额12
	private BigDecimal jyjine13;//金额13
	private BigDecimal jyjine14;//金额14
	private BigDecimal jyjine15;//金额15
	private BigDecimal jyjine16;//金额16
	private BigDecimal jyjine17;//金额17
	private BigDecimal jyjine18;//金额18
	private BigDecimal jyjine19;//金额19
	private BigDecimal jyjine20;//金额20
	private BigDecimal jyjine21;//金额21
	private BigDecimal jyjine22;//金额22
	private BigDecimal jyjine23;//金额23
	private String pingzhzl;//凭证类型
	private String pingzhma;//凭证号码
	private String zhipleix;//支票类型
	private String zhiphaom;//支票号码
	private String chupriqi;//出票日期
	private String zhiqfshi;//支取方式
	private String jiaoymma;//交易密码
	private String yanmbzhi;//密码校验方式
	private String zhengjlx;//证件类型
	private String zhjhaoma;//证件号码
	private String mimammmm;//密码
	private String guazhnxh;//挂账序号
	private String zhaiyoms;//摘要描述
	private String jigouhao;//机构号
	private String yewubima;//业务编码
	private String qianzhrq;//前置日期
	private String qianzhls;//前置流水
	private Integer jiaoyisj;//交易时间
	private String jiedfxbs;//借贷方向标识
	private String jioyguiy;//交易柜员
	private String shoqguiy;//授权柜员
	private String chanpdma;//产品代码
	private String jzzhaiym;//记账摘要码
	private String zhaiyodm;//摘要代码
	private String beizhuxx;//备注信息
	private String laiwzhlx;//来往账类型
	private String fukrzhao;//付款人账号
	private String fukrzwmc;//付款人名称
	private String fukhzfho;//付款行行号
	private String fukhzwmc;//付款行名称
	private String shkrzhao;//收款人账号
	private String shkrzwmc;//收款人名称
	private String shkhzfho;//收款行行号
	private String shkhzwmc;//收款行名称
	private String piaojulx;//票据类型
	private String pjuhaoma;//票据号码
	private String feiybzhi;//费用标志
	private String shofkkbz;//收费扣款标志
	private String shofjely;//收费金额来源
	private String koukzhao;//扣款账号
	private BigDecimal shouxfei;//手续费
	private BigDecimal gongbfei;//工本费
	private BigDecimal youdifei;//邮电费
	private String jypiciho;//交易批次号
	public String  getPrcscd() { return prcscd; }
	public void setPrcscd(String prcscd ) { this.prcscd = prcscd;}
	public String  getServtp() { return servtp; }
	public void setServtp(String servtp ) { this.servtp = servtp;}
	public String  getServsq() { return servsq; }
	public void setServsq(String servsq ) { this.servsq = servsq;}
	public String  getUserid() { return userid; }
	public void setUserid(String userid ) { this.userid = userid;}
	public String  getBrchno() { return brchno; }
	public void setBrchno(String brchno ) { this.brchno = brchno;}
	public String  getDatasq() { return datasq; }
	public void setDatasq(String datasq ) { this.datasq = datasq;}
	public String  getServdt() { return servdt; }
	public void setServdt(String servdt ) { this.servdt = servdt;}
	public String  getServti() { return servti; }
	public void setServti(String servti ) { this.servti = servti;}
	public String  getShebeilx() { return shebeilx; }
	public void setShebeilx(String shebeilx ) { this.shebeilx = shebeilx;}
	public String  getShebeibh() { return shebeibh; }
	public void setShebeibh(String shebeibh ) { this.shebeibh = shebeibh;}
	public String  getQudaofwm() { return qudaofwm; }
	public void setQudaofwm(String qudaofwm ) { this.qudaofwm = qudaofwm;}
	public String  getXitongbs() { return xitongbs; }
	public void setXitongbs(String xitongbs ) { this.xitongbs = xitongbs;}
	public String  getQiatyima() { return qiatyima; }
	public void setQiatyima(String qiatyima ) { this.qiatyima = qiatyima;}
	public String  getHuobdaih() { return huobdaih; }
	public void setHuobdaih(String huobdaih ) { this.huobdaih = huobdaih;}
	public String  getZhngha01() { return zhngha01; }
	public void setZhngha01(String zhngha01 ) { this.zhngha01 = zhngha01;}
	public String  getZhngha02() { return zhngha02; }
	public void setZhngha02(String zhngha02 ) { this.zhngha02 = zhngha02;}
	public String  getZhngha03() { return zhngha03; }
	public void setZhngha03(String zhngha03 ) { this.zhngha03 = zhngha03;}
	public String  getZhngha04() { return zhngha04; }
	public void setZhngha04(String zhngha04 ) { this.zhngha04 = zhngha04;}
	public String  getZhngha05() { return zhngha05; }
	public void setZhngha05(String zhngha05 ) { this.zhngha05 = zhngha05;}
	public String  getZhngha06() { return zhngha06; }
	public void setZhngha06(String zhngha06 ) { this.zhngha06 = zhngha06;}
	public String  getZhngha07() { return zhngha07; }
	public void setZhngha07(String zhngha07 ) { this.zhngha07 = zhngha07;}
	public String  getZhngha08() { return zhngha08; }
	public void setZhngha08(String zhngha08 ) { this.zhngha08 = zhngha08;}
	public String  getZhngha09() { return zhngha09; }
	public void setZhngha09(String zhngha09 ) { this.zhngha09 = zhngha09;}
	public String  getZhngha10() { return zhngha10; }
	public void setZhngha10(String zhngha10 ) { this.zhngha10 = zhngha10;}
	public String  getZhnghajf() { return zhnghajf; }
	public void setZhnghajf(String zhnghajf ) { this.zhnghajf = zhnghajf;}
	public String  getZhnghadf() { return zhnghadf; }
	public void setZhnghadf(String zhnghadf ) { this.zhnghadf = zhnghadf;}
	public String  getZhnghaff() { return zhnghaff; }
	public void setZhnghaff(String zhnghaff ) { this.zhnghaff = zhnghaff;}
	public String  getZhnghasf() { return zhnghasf; }
	public void setZhnghasf(String zhnghasf ) { this.zhnghasf = zhnghasf;}
	public String  getJigouh01() { return jigouh01; }
	public void setJigouh01(String jigouh01 ) { this.jigouh01 = jigouh01;}
	public String  getJigouh02() { return jigouh02; }
	public void setJigouh02(String jigouh02 ) { this.jigouh02 = jigouh02;}
	public String  getJigouh03() { return jigouh03; }
	public void setJigouh03(String jigouh03 ) { this.jigouh03 = jigouh03;}
	public String  getJigouh04() { return jigouh04; }
	public void setJigouh04(String jigouh04 ) { this.jigouh04 = jigouh04;}
	public String  getJigouh05() { return jigouh05; }
	public void setJigouh05(String jigouh05 ) { this.jigouh05 = jigouh05;}
	public String  getJigouh06() { return jigouh06; }
	public void setJigouh06(String jigouh06 ) { this.jigouh06 = jigouh06;}
	public String  getJigouh07() { return jigouh07; }
	public void setJigouh07(String jigouh07 ) { this.jigouh07 = jigouh07;}
	public String  getJigouh08() { return jigouh08; }
	public void setJigouh08(String jigouh08 ) { this.jigouh08 = jigouh08;}
	public String  getJigouh09() { return jigouh09; }
	public void setJigouh09(String jigouh09 ) { this.jigouh09 = jigouh09;}
	public String  getJigouh10() { return jigouh10; }
	public void setJigouh10(String jigouh10 ) { this.jigouh10 = jigouh10;}
	public String  getJigouh11() { return jigouh11; }
	public void setJigouh11(String jigouh11 ) { this.jigouh11 = jigouh11;}
	public String  getJigouh12() { return jigouh12; }
	public void setJigouh12(String jigouh12 ) { this.jigouh12 = jigouh12;}
	public String  getJigouh13() { return jigouh13; }
	public void setJigouh13(String jigouh13 ) { this.jigouh13 = jigouh13;}
	public BigDecimal  getJyjine01() { return jyjine01; }
	public void setJyjine01(BigDecimal jyjine01 ) { this.jyjine01 = jyjine01;}
	public BigDecimal  getJyjine02() { return jyjine02; }
	public void setJyjine02(BigDecimal jyjine02 ) { this.jyjine02 = jyjine02;}
	public BigDecimal  getJyjine03() { return jyjine03; }
	public void setJyjine03(BigDecimal jyjine03 ) { this.jyjine03 = jyjine03;}
	public BigDecimal  getJyjine04() { return jyjine04; }
	public void setJyjine04(BigDecimal jyjine04 ) { this.jyjine04 = jyjine04;}
	public BigDecimal  getJyjine05() { return jyjine05; }
	public void setJyjine05(BigDecimal jyjine05 ) { this.jyjine05 = jyjine05;}
	public BigDecimal  getJyjine06() { return jyjine06; }
	public void setJyjine06(BigDecimal jyjine06 ) { this.jyjine06 = jyjine06;}
	public BigDecimal  getJyjine07() { return jyjine07; }
	public void setJyjine07(BigDecimal jyjine07 ) { this.jyjine07 = jyjine07;}
	public BigDecimal  getJyjine08() { return jyjine08; }
	public void setJyjine08(BigDecimal jyjine08 ) { this.jyjine08 = jyjine08;}
	public BigDecimal  getJyjine09() { return jyjine09; }
	public void setJyjine09(BigDecimal jyjine09 ) { this.jyjine09 = jyjine09;}
	public BigDecimal  getJyjine10() { return jyjine10; }
	public void setJyjine10(BigDecimal jyjine10 ) { this.jyjine10 = jyjine10;}
	public BigDecimal  getJyjine11() { return jyjine11; }
	public void setJyjine11(BigDecimal jyjine11 ) { this.jyjine11 = jyjine11;}
	public BigDecimal  getJyjine12() { return jyjine12; }
	public void setJyjine12(BigDecimal jyjine12 ) { this.jyjine12 = jyjine12;}
	public BigDecimal  getJyjine13() { return jyjine13; }
	public void setJyjine13(BigDecimal jyjine13 ) { this.jyjine13 = jyjine13;}
	public BigDecimal  getJyjine14() { return jyjine14; }
	public void setJyjine14(BigDecimal jyjine14 ) { this.jyjine14 = jyjine14;}
	public BigDecimal  getJyjine15() { return jyjine15; }
	public void setJyjine15(BigDecimal jyjine15 ) { this.jyjine15 = jyjine15;}
	public BigDecimal  getJyjine16() { return jyjine16; }
	public void setJyjine16(BigDecimal jyjine16 ) { this.jyjine16 = jyjine16;}
	public BigDecimal  getJyjine17() { return jyjine17; }
	public void setJyjine17(BigDecimal jyjine17 ) { this.jyjine17 = jyjine17;}
	public BigDecimal  getJyjine18() { return jyjine18; }
	public void setJyjine18(BigDecimal jyjine18 ) { this.jyjine18 = jyjine18;}
	public BigDecimal  getJyjine19() { return jyjine19; }
	public void setJyjine19(BigDecimal jyjine19 ) { this.jyjine19 = jyjine19;}
	public BigDecimal  getJyjine20() { return jyjine20; }
	public void setJyjine20(BigDecimal jyjine20 ) { this.jyjine20 = jyjine20;}
	public BigDecimal  getJyjine21() { return jyjine21; }
	public void setJyjine21(BigDecimal jyjine21 ) { this.jyjine21 = jyjine21;}
	public BigDecimal  getJyjine22() { return jyjine22; }
	public void setJyjine22(BigDecimal jyjine22 ) { this.jyjine22 = jyjine22;}
	public BigDecimal  getJyjine23() { return jyjine23; }
	public void setJyjine23(BigDecimal jyjine23 ) { this.jyjine23 = jyjine23;}
	public String  getPingzhzl() { return pingzhzl; }
	public void setPingzhzl(String pingzhzl ) { this.pingzhzl = pingzhzl;}
	public String  getPingzhma() { return pingzhma; }
	public void setPingzhma(String pingzhma ) { this.pingzhma = pingzhma;}
	public String  getZhipleix() { return zhipleix; }
	public void setZhipleix(String zhipleix ) { this.zhipleix = zhipleix;}
	public String  getZhiphaom() { return zhiphaom; }
	public void setZhiphaom(String zhiphaom ) { this.zhiphaom = zhiphaom;}
	public String  getChupriqi() { return chupriqi; }
	public void setChupriqi(String chupriqi ) { this.chupriqi = chupriqi;}
	public String  getZhiqfshi() { return zhiqfshi; }
	public void setZhiqfshi(String zhiqfshi ) { this.zhiqfshi = zhiqfshi;}
	public String  getJiaoymma() { return jiaoymma; }
	public void setJiaoymma(String jiaoymma ) { this.jiaoymma = jiaoymma;}
	public String  getYanmbzhi() { return yanmbzhi; }
	public void setYanmbzhi(String yanmbzhi ) { this.yanmbzhi = yanmbzhi;}
	public String  getZhengjlx() { return zhengjlx; }
	public void setZhengjlx(String zhengjlx ) { this.zhengjlx = zhengjlx;}
	public String  getZhjhaoma() { return zhjhaoma; }
	public void setZhjhaoma(String zhjhaoma ) { this.zhjhaoma = zhjhaoma;}
	public String  getMimammmm() { return mimammmm; }
	public void setMimammmm(String mimammmm ) { this.mimammmm = mimammmm;}
	public String  getGuazhnxh() { return guazhnxh; }
	public void setGuazhnxh(String guazhnxh ) { this.guazhnxh = guazhnxh;}
	public String  getZhaiyoms() { return zhaiyoms; }
	public void setZhaiyoms(String zhaiyoms ) { this.zhaiyoms = zhaiyoms;}
	public String  getJigouhao() { return jigouhao; }
	public void setJigouhao(String jigouhao ) { this.jigouhao = jigouhao;}
	public String  getYewubima() { return yewubima; }
	public void setYewubima(String yewubima ) { this.yewubima = yewubima;}
	public String  getQianzhrq() { return qianzhrq; }
	public void setQianzhrq(String qianzhrq ) { this.qianzhrq = qianzhrq;}
	public String  getQianzhls() { return qianzhls; }
	public void setQianzhls(String qianzhls ) { this.qianzhls = qianzhls;}
	public Integer  getJiaoyisj() { return jiaoyisj; }
	public void setJiaoyisj(Integer jiaoyisj ) { this.jiaoyisj = jiaoyisj;}
	public String  getJiedfxbs() { return jiedfxbs; }
	public void setJiedfxbs(String jiedfxbs ) { this.jiedfxbs = jiedfxbs;}
	public String  getJioyguiy() { return jioyguiy; }
	public void setJioyguiy(String jioyguiy ) { this.jioyguiy = jioyguiy;}
	public String  getShoqguiy() { return shoqguiy; }
	public void setShoqguiy(String shoqguiy ) { this.shoqguiy = shoqguiy;}
	public String  getChanpdma() { return chanpdma; }
	public void setChanpdma(String chanpdma ) { this.chanpdma = chanpdma;}
	public String  getJzzhaiym() { return jzzhaiym; }
	public void setJzzhaiym(String jzzhaiym ) { this.jzzhaiym = jzzhaiym;}
	public String  getZhaiyodm() { return zhaiyodm; }
	public void setZhaiyodm(String zhaiyodm ) { this.zhaiyodm = zhaiyodm;}
	public String  getBeizhuxx() { return beizhuxx; }
	public void setBeizhuxx(String beizhuxx ) { this.beizhuxx = beizhuxx;}
	public String  getLaiwzhlx() { return laiwzhlx; }
	public void setLaiwzhlx(String laiwzhlx ) { this.laiwzhlx = laiwzhlx;}
	public String  getFukrzhao() { return fukrzhao; }
	public void setFukrzhao(String fukrzhao ) { this.fukrzhao = fukrzhao;}
	public String  getFukrzwmc() { return fukrzwmc; }
	public void setFukrzwmc(String fukrzwmc ) { this.fukrzwmc = fukrzwmc;}
	public String  getFukhzfho() { return fukhzfho; }
	public void setFukhzfho(String fukhzfho ) { this.fukhzfho = fukhzfho;}
	public String  getFukhzwmc() { return fukhzwmc; }
	public void setFukhzwmc(String fukhzwmc ) { this.fukhzwmc = fukhzwmc;}
	public String  getShkrzhao() { return shkrzhao; }
	public void setShkrzhao(String shkrzhao ) { this.shkrzhao = shkrzhao;}
	public String  getShkrzwmc() { return shkrzwmc; }
	public void setShkrzwmc(String shkrzwmc ) { this.shkrzwmc = shkrzwmc;}
	public String  getShkhzfho() { return shkhzfho; }
	public void setShkhzfho(String shkhzfho ) { this.shkhzfho = shkhzfho;}
	public String  getShkhzwmc() { return shkhzwmc; }
	public void setShkhzwmc(String shkhzwmc ) { this.shkhzwmc = shkhzwmc;}
	public String  getPiaojulx() { return piaojulx; }
	public void setPiaojulx(String piaojulx ) { this.piaojulx = piaojulx;}
	public String  getPjuhaoma() { return pjuhaoma; }
	public void setPjuhaoma(String pjuhaoma ) { this.pjuhaoma = pjuhaoma;}
	public String  getFeiybzhi() { return feiybzhi; }
	public void setFeiybzhi(String feiybzhi ) { this.feiybzhi = feiybzhi;}
	public String  getShofkkbz() { return shofkkbz; }
	public void setShofkkbz(String shofkkbz ) { this.shofkkbz = shofkkbz;}
	public String  getShofjely() { return shofjely; }
	public void setShofjely(String shofjely ) { this.shofjely = shofjely;}
	public String  getKoukzhao() { return koukzhao; }
	public void setKoukzhao(String koukzhao ) { this.koukzhao = koukzhao;}
	public BigDecimal  getShouxfei() { return shouxfei; }
	public void setShouxfei(BigDecimal shouxfei ) { this.shouxfei = shouxfei;}
	public BigDecimal  getGongbfei() { return gongbfei; }
	public void setGongbfei(BigDecimal gongbfei ) { this.gongbfei = gongbfei;}
	public BigDecimal  getYoudifei() { return youdifei; }
	public void setYoudifei(BigDecimal youdifei ) { this.youdifei = youdifei;}
	public String  getJypiciho() { return jypiciho; }
	public void setJypiciho(String jypiciho ) { this.jypiciho = jypiciho;}
    @Override
    public String toString() {
	    return "Service{" +
	"prcscd='" + prcscd+ '\'' +
	"servtp='" + servtp+ '\'' +
	"servsq='" + servsq+ '\'' +
	"userid='" + userid+ '\'' +
	"brchno='" + brchno+ '\'' +
	"datasq='" + datasq+ '\'' +
	"servdt='" + servdt+ '\'' +
	"servti='" + servti+ '\'' +
	"shebeilx='" + shebeilx+ '\'' +
	"shebeibh='" + shebeibh+ '\'' +
	"qudaofwm='" + qudaofwm+ '\'' +
	"xitongbs='" + xitongbs+ '\'' +
	"qiatyima='" + qiatyima+ '\'' +
	"huobdaih='" + huobdaih+ '\'' +
	"zhngha01='" + zhngha01+ '\'' +
	"zhngha02='" + zhngha02+ '\'' +
	"zhngha03='" + zhngha03+ '\'' +
	"zhngha04='" + zhngha04+ '\'' +
	"zhngha05='" + zhngha05+ '\'' +
	"zhngha06='" + zhngha06+ '\'' +
	"zhngha07='" + zhngha07+ '\'' +
	"zhngha08='" + zhngha08+ '\'' +
	"zhngha09='" + zhngha09+ '\'' +
	"zhngha10='" + zhngha10+ '\'' +
	"zhnghajf='" + zhnghajf+ '\'' +
	"zhnghadf='" + zhnghadf+ '\'' +
	"zhnghaff='" + zhnghaff+ '\'' +
	"zhnghasf='" + zhnghasf+ '\'' +
	"jigouh01='" + jigouh01+ '\'' +
	"jigouh02='" + jigouh02+ '\'' +
	"jigouh03='" + jigouh03+ '\'' +
	"jigouh04='" + jigouh04+ '\'' +
	"jigouh05='" + jigouh05+ '\'' +
	"jigouh06='" + jigouh06+ '\'' +
	"jigouh07='" + jigouh07+ '\'' +
	"jigouh08='" + jigouh08+ '\'' +
	"jigouh09='" + jigouh09+ '\'' +
	"jigouh10='" + jigouh10+ '\'' +
	"jigouh11='" + jigouh11+ '\'' +
	"jigouh12='" + jigouh12+ '\'' +
	"jigouh13='" + jigouh13+ '\'' +
	"jyjine01='" + jyjine01+ '\'' +
	"jyjine02='" + jyjine02+ '\'' +
	"jyjine03='" + jyjine03+ '\'' +
	"jyjine04='" + jyjine04+ '\'' +
	"jyjine05='" + jyjine05+ '\'' +
	"jyjine06='" + jyjine06+ '\'' +
	"jyjine07='" + jyjine07+ '\'' +
	"jyjine08='" + jyjine08+ '\'' +
	"jyjine09='" + jyjine09+ '\'' +
	"jyjine10='" + jyjine10+ '\'' +
	"jyjine11='" + jyjine11+ '\'' +
	"jyjine12='" + jyjine12+ '\'' +
	"jyjine13='" + jyjine13+ '\'' +
	"jyjine14='" + jyjine14+ '\'' +
	"jyjine15='" + jyjine15+ '\'' +
	"jyjine16='" + jyjine16+ '\'' +
	"jyjine17='" + jyjine17+ '\'' +
	"jyjine18='" + jyjine18+ '\'' +
	"jyjine19='" + jyjine19+ '\'' +
	"jyjine20='" + jyjine20+ '\'' +
	"jyjine21='" + jyjine21+ '\'' +
	"jyjine22='" + jyjine22+ '\'' +
	"jyjine23='" + jyjine23+ '\'' +
	"pingzhzl='" + pingzhzl+ '\'' +
	"pingzhma='" + pingzhma+ '\'' +
	"zhipleix='" + zhipleix+ '\'' +
	"zhiphaom='" + zhiphaom+ '\'' +
	"chupriqi='" + chupriqi+ '\'' +
	"zhiqfshi='" + zhiqfshi+ '\'' +
	"jiaoymma='" + jiaoymma+ '\'' +
	"yanmbzhi='" + yanmbzhi+ '\'' +
	"zhengjlx='" + zhengjlx+ '\'' +
	"zhjhaoma='" + zhjhaoma+ '\'' +
	"mimammmm='" + mimammmm+ '\'' +
	"guazhnxh='" + guazhnxh+ '\'' +
	"zhaiyoms='" + zhaiyoms+ '\'' +
	"jigouhao='" + jigouhao+ '\'' +
	"yewubima='" + yewubima+ '\'' +
	"qianzhrq='" + qianzhrq+ '\'' +
	"qianzhls='" + qianzhls+ '\'' +
	"jiaoyisj='" + jiaoyisj+ '\'' +
	"jiedfxbs='" + jiedfxbs+ '\'' +
	"jioyguiy='" + jioyguiy+ '\'' +
	"shoqguiy='" + shoqguiy+ '\'' +
	"chanpdma='" + chanpdma+ '\'' +
	"jzzhaiym='" + jzzhaiym+ '\'' +
	"zhaiyodm='" + zhaiyodm+ '\'' +
	"beizhuxx='" + beizhuxx+ '\'' +
	"laiwzhlx='" + laiwzhlx+ '\'' +
	"fukrzhao='" + fukrzhao+ '\'' +
	"fukrzwmc='" + fukrzwmc+ '\'' +
	"fukhzfho='" + fukhzfho+ '\'' +
	"fukhzwmc='" + fukhzwmc+ '\'' +
	"shkrzhao='" + shkrzhao+ '\'' +
	"shkrzwmc='" + shkrzwmc+ '\'' +
	"shkhzfho='" + shkhzfho+ '\'' +
	"shkhzwmc='" + shkhzwmc+ '\'' +
	"piaojulx='" + piaojulx+ '\'' +
	"pjuhaoma='" + pjuhaoma+ '\'' +
	"feiybzhi='" + feiybzhi+ '\'' +
	"shofkkbz='" + shofkkbz+ '\'' +
	"shofjely='" + shofjely+ '\'' +
	"koukzhao='" + koukzhao+ '\'' +
	"shouxfei='" + shouxfei+ '\'' +
	"gongbfei='" + gongbfei+ '\'' +
	"youdifei='" + youdifei+ '\'' +
	"jypiciho='" + jypiciho+ '\'' +
	 '}';
	    }
	}
