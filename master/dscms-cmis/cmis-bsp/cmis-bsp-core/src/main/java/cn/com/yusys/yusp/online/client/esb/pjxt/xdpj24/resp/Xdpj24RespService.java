package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp;

/**
 * 响应Service：从票据系统获取当日到期票的日出备款金额
 *
 * @author chenyong
 * @version 1.0
 */
public class Xdpj24RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdpj24RespService{" +
                "service=" + service +
                '}';
    }
}
