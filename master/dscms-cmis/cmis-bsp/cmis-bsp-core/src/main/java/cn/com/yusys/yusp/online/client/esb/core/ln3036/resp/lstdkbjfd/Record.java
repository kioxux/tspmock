package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkbjfd;

import java.math.BigDecimal;

public class Record {
    private String bjdhkfsh;//本阶段还款方式

    private String qishriqi;//起始日期

    private String daoqriqi;//到期日期

    private Integer bjdhkqsh;//本阶段还款期数

    private BigDecimal bjdhbjee;//本阶段还本金额

    private String dechligz;//等额处理规则

    private BigDecimal leijinzh;//累进值

    private Integer leijqjsh;//累进区间期数

    private String hkzhouqi;//还款周期

    private Integer xuhaoooo;//序号

    private String jixiguiz;//计息规则

    public String getBjdhkfsh() {
        return bjdhkfsh;
    }

    public void setBjdhkfsh(String bjdhkfsh) {
        this.bjdhkfsh = bjdhkfsh;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public Integer getBjdhkqsh() {
        return bjdhkqsh;
    }

    public void setBjdhkqsh(Integer bjdhkqsh) {
        this.bjdhkqsh = bjdhkqsh;
    }

    public BigDecimal getBjdhbjee() {
        return bjdhbjee;
    }

    public void setBjdhbjee(BigDecimal bjdhbjee) {
        this.bjdhbjee = bjdhbjee;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    @Override
    public String toString() {
        return "Record{" +
                "bjdhkfsh='" + bjdhkfsh + '\'' +
                ", qishriqi='" + qishriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", bjdhkqsh=" + bjdhkqsh +
                ", bjdhbjee=" + bjdhbjee +
                ", dechligz='" + dechligz + '\'' +
                ", leijinzh=" + leijinzh +
                ", leijqjsh=" + leijqjsh +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", xuhaoooo=" + xuhaoooo +
                ", jixiguiz='" + jixiguiz + '\'' +
                '}';
    }
}
