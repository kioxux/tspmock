package cn.com.yusys.yusp.online.client.esb.ypxt.contra.resp;

/**
 * 响应Service：押品与担保合同关系同步（处理码contra）
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class ContraRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
