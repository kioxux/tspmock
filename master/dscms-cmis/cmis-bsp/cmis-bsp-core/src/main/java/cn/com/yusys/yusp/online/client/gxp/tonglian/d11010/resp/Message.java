package cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp;

import cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead;

/**
 * 响应Service：客户基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Message {
    private cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead head;
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp.Body body;

    public GxpRespHead getHead() {
        return head;
    }

    public void setHead(GxpRespHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}