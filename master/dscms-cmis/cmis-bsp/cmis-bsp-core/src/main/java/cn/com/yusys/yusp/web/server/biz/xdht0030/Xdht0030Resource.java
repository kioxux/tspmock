package cn.com.yusys.yusp.web.server.biz.xdht0030;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0030.req.Xdht0030ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0030.resp.Xdht0030RespDto;
import cn.com.yusys.yusp.dto.server.xdht0030.req.Xdht0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0030.resp.Xdht0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户我行合作次数
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0030:查询客户我行合作次数")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0030Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0030
     * 交易描述：查询客户我行合作次数
     *
     * @param xdht0030ReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("查询客户我行合作次数")
    @PostMapping("/xdht0030")
    //@Idempotent({"xdcaht0030", "#xdht0030ReqDto.datasq"})
    protected @ResponseBody
    Xdht0030RespDto xdht0030(@Validated @RequestBody Xdht0030ReqDto xdht0030ReqDto ) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, JSON.toJSONString(xdht0030ReqDto));
        Xdht0030DataReqDto xdht0030DataReqDto = new Xdht0030DataReqDto();// 请求Data： 查询客户我行合作次数
        Xdht0030DataRespDto xdht0030DataRespDto =  new Xdht0030DataRespDto();// 响应Data：查询客户我行合作次数
        Xdht0030RespDto xdht0030RespDto = new Xdht0030RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0030.req.Data reqData = null; // 请求Data：查询客户我行合作次数
        cn.com.yusys.yusp.dto.server.biz.xdht0030.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0030.resp.Data();// 响应Data：查询客户我行合作次数
        try {
            // 从 xdht0030ReqDto获取 reqData
            reqData = xdht0030ReqDto.getData();
            // 将 reqData 拷贝到xdht0030DataReqDto
            BeanUtils.copyProperties(reqData, xdht0030DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, JSON.toJSONString(xdht0030DataReqDto));
            ResultDto<Xdht0030DataRespDto> xdht0030DataResultDto= dscmsBizHtClientService.xdht0030(xdht0030DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, JSON.toJSONString(xdht0030DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0030RespDto.setErorcd(Optional.ofNullable(xdht0030DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0030RespDto.setErortx(Optional.ofNullable(xdht0030DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,xdht0030DataResultDto.getCode())) {
                xdht0030DataRespDto = xdht0030DataResultDto.getData();
                xdht0030RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0030RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0030DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0030DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, e.getMessage());
            xdht0030RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0030RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0030RespDto.setDatasq(servsq);

        xdht0030RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0030.key, DscmsEnum.TRADE_CODE_XDHT0030.value, JSON.toJSONString(xdht0030RespDto));
        return xdht0030RespDto;
    }
}