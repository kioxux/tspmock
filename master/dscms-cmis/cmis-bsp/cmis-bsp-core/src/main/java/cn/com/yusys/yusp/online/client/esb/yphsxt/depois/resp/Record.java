package cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/5 15:03
 * @since 2021/6/5 15:03
 */
public class Record {
    private String guar_no;//押品统一编号
    private String assetNo;

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "Record{" +
                "guar_no='" + guar_no + '\'' +
                ", assetNo='" + assetNo + '\'' +
                '}';
    }
}
