package cn.com.yusys.yusp.web.server.biz.xdtz0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0008.req.Xdtz0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0008.resp.Xdtz0008RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0008.req.Xdtz0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0008.resp.Xdtz0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号获取正常周转次数
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDTZ0008:根据客户号获取正常周转次数")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0008Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0008
     * 交易描述：根据客户号获取正常周转次数
     *
     * @param xdtz0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0008:根据客户号获取正常周转次数")
    @PostMapping("/xdtz0008")
    //@Idempotent({"xdcatz0008", "#xdtz0008ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0008RespDto xdtz0008(@Validated @RequestBody Xdtz0008ReqDto xdtz0008ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008ReqDto));

        Xdtz0008DataReqDto xdtz0008DataReqDto = new Xdtz0008DataReqDto();// 请求Data： 根据客户号获取正常周转次数
        Xdtz0008DataRespDto xdtz0008DataRespDto = new Xdtz0008DataRespDto();// 响应Data：根据客户号获取正常周转次数
        Xdtz0008RespDto xdtz0008RespDto = new Xdtz0008RespDto();// 响应Dto：客户准入级别同步
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdtz0008.req.Data reqData = null; // 请求Data：根据客户号获取正常周转次数
        cn.com.yusys.yusp.dto.server.biz.xdtz0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0008.resp.Data();
        // 响应Data：根据客户号获取正常周转次数
        // 从 xdtz0008ReqDto获取 reqData
        try {
            reqData = xdtz0008ReqDto.getData();
            // 将 reqData 拷贝到xdtz0008DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0008DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008DataReqDto));
            ResultDto<Xdtz0008DataRespDto> xdtz0008DataResultDto = dscmsBizTzClientService.xdtz0008(xdtz0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0008RespDto.setErorcd(Optional.ofNullable(xdtz0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0008RespDto.setErortx(Optional.ofNullable(xdtz0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0008DataResultDto.getCode())) {
                xdtz0008DataRespDto = xdtz0008DataResultDto.getData();
                xdtz0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, e.getMessage());
            xdtz0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0008RespDto.setDatasq(servsq);

        xdtz0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0008.key, DscmsEnum.TRADE_CODE_XDTZ0008.value, JSON.toJSONString(xdtz0008RespDto));
        return xdtz0008RespDto;
    }
}
