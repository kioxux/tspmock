package cn.com.yusys.yusp.web.server.biz.xdxw0043;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0043.req.Xdxw0043ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0043.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0043.resp.Xdxw0043RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0043.req.Xdxw0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0043.resp.Xdxw0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:无还本续贷待办接收
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0043:无还本续贷待办接收")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0043Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0043Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0043
     * 交易描述：无还本续贷待办接收
     *
     * @param xdxw0043ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("无还本续贷待办接收")
    @PostMapping("/xdxw0043")
    //@Idempotent({"xdcaxw0043", "#xdxw0043ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0043RespDto xdxw0043(@Validated @RequestBody Xdxw0043ReqDto xdxw0043ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043ReqDto));
        Xdxw0043DataReqDto xdxw0043DataReqDto = new Xdxw0043DataReqDto();// 请求Data： 无还本续贷待办接收
        Xdxw0043DataRespDto xdxw0043DataRespDto = new Xdxw0043DataRespDto();// 响应Data：无还本续贷待办接收
        cn.com.yusys.yusp.dto.server.biz.xdxw0043.req.Data reqData = null; // 请求Data：无还本续贷待办接收
        cn.com.yusys.yusp.dto.server.biz.xdxw0043.resp.Data respData = new Data();// 响应Data：无还本续贷待办接收
		Xdxw0043RespDto xdxw0043RespDto = new Xdxw0043RespDto();
		try {
            // 从 xdxw0043ReqDto获取 reqData
            reqData = xdxw0043ReqDto.getData();
            // 将 reqData 拷贝到xdxw0043DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0043DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043DataReqDto));
            ResultDto<Xdxw0043DataRespDto> xdxw0043DataResultDto = dscmsBizXwClientService.xdxw0043(xdxw0043DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0043RespDto.setErorcd(Optional.ofNullable(xdxw0043DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0043RespDto.setErortx(Optional.ofNullable(xdxw0043DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0043DataResultDto.getCode())) {
                xdxw0043DataRespDto = xdxw0043DataResultDto.getData();
                xdxw0043RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0043RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0043DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0043DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, e.getMessage());
            xdxw0043RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0043RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0043RespDto.setDatasq(servsq);

        xdxw0043RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0043.key, DscmsEnum.TRADE_CODE_XDXW0043.value, JSON.toJSONString(xdxw0043RespDto));
        return xdxw0043RespDto;
    }
}
