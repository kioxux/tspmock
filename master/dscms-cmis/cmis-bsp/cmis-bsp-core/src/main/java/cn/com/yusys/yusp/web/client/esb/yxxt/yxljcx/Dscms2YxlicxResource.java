package cn.com.yusys.yusp.web.client.esb.yxxt.yxljcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.req.YxljcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.resp.YxljcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.req.YxljcxReqService;
import cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.resp.YxljcxRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:影像图像路径查询
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yxxt")
public class Dscms2YxlicxResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2YxlicxResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：yxljcx
     * 交易描述：影像图像路径查询
     *
     * @param yxljcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/yxljcx")
    protected @ResponseBody
    ResultDto<YxljcxRespDto> yxljcx(@Validated @RequestBody YxljcxReqDto yxljcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXLJCX.key, EsbEnum.TRADE_CODE_YXLJCX.value, JSON.toJSONString(yxljcxReqDto));
        cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.resp.Service();
        YxljcxReqService yxljcxReqService = new YxljcxReqService();
        YxljcxRespService yxljcxRespService = new YxljcxRespService();
        YxljcxRespDto yxljcxRespDto = new YxljcxRespDto();
        ResultDto<YxljcxRespDto> yxljcxResultDto = new ResultDto<YxljcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将yxljcxReqDto转换成reqService
            BeanUtils.copyProperties(yxljcxReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_YXLJCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();

            yxljcxReqService.setService(reqService);
            // 将yxljcxReqService转换成yxljcxReqServiceMap
            Map yxljcxReqServiceMap = beanMapUtil.beanToMap(yxljcxReqService);
            context.put("tradeDataMap", yxljcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXLJCX.key, EsbEnum.TRADE_CODE_YXLJCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_YXLJCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXLJCX.key, EsbEnum.TRADE_CODE_YXLJCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            yxljcxRespService = beanMapUtil.mapToBean(tradeDataMap, YxljcxRespService.class, YxljcxRespService.class);
            respService = yxljcxRespService.getService();
            //  将respService转换成YxljcxRespDto
            yxljcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            yxljcxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, yxljcxRespDto);
                yxljcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                yxljcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                yxljcxResultDto.setCode(EpbEnum.EPB099999.key);
                yxljcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXLJCX.key, EsbEnum.TRADE_CODE_YXLJCX.value, e.getMessage());
            yxljcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            yxljcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        yxljcxResultDto.setData(yxljcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YXLJCX.key, EsbEnum.TRADE_CODE_YXLJCX.value, JSON.toJSONString(yxljcxResultDto));
        return yxljcxResultDto;
    }
}
