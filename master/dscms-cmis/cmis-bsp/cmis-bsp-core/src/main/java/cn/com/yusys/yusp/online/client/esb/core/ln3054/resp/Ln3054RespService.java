package cn.com.yusys.yusp.online.client.esb.core.ln3054.resp;

/**
 * 响应Service：调整贷款还款方式
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3054RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      

