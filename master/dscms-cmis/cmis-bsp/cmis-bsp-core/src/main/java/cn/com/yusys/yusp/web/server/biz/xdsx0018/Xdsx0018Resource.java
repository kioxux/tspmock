package cn.com.yusys.yusp.web.server.biz.xdsx0018;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0018.req.Xdsx0018ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0018.resp.Xdsx0018RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.req.Xdsx0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0018.resp.Xdsx0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:房抵e点贷无还本续贷审核
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDSX0018:房抵e点贷无还本续贷审核")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0018Resource.class);

    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;
    /**
     * 交易码：xdsx0018
     * 交易描述：房抵e点贷无还本续贷审核
     *
     * @param xdsx0018ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("房抵e点贷无还本续贷审核")
    @PostMapping("/xdsx0018")
    //@Idempotent({"xdcasx0018", "#xdsx0018ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0018RespDto xdsx0018(@Validated @RequestBody Xdsx0018ReqDto xdsx0018ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, JSON.toJSONString(xdsx0018ReqDto));
        Xdsx0018DataReqDto xdsx0018DataReqDto = new Xdsx0018DataReqDto();// 请求Data： 房抵e点贷无还本续贷审核
        Xdsx0018DataRespDto xdsx0018DataRespDto = new Xdsx0018DataRespDto();// 响应Data：房抵e点贷无还本续贷审核
        Xdsx0018RespDto xdsx0018RespDto = new Xdsx0018RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdsx0018.req.Data reqData = null; // 请求Data：房抵e点贷无还本续贷审核
        cn.com.yusys.yusp.dto.server.biz.xdsx0018.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0018.resp.Data();// 响应Data：房抵e点贷无还本续贷审核

        try {
            // 从 xdsx0018ReqDto获取 reqData
            reqData = xdsx0018ReqDto.getData();
            // 将 reqData 拷贝到xdsx0018DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0018DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, JSON.toJSONString(xdsx0018DataReqDto));
            ResultDto<Xdsx0018DataRespDto> xdsx0018DataResultDto = dscmsBizSxClientService.xdsx0018(xdsx0018DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, JSON.toJSONString(xdsx0018DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdsx0018RespDto.setErorcd(Optional.ofNullable(xdsx0018DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0018RespDto.setErortx(Optional.ofNullable(xdsx0018DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0018DataResultDto.getCode())) {
                xdsx0018DataRespDto = xdsx0018DataResultDto.getData();
                xdsx0018RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0018RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0018DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0018DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0018RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, e.getMessage());
            xdsx0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0018RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0018RespDto.setDatasq(servsq);

        xdsx0018RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0018.key, DscmsEnum.TRADE_CODE_XDSX0018.value, JSON.toJSONString(xdsx0018RespDto));
        return xdsx0018RespDto;
    }
}
