package cn.com.yusys.yusp.web.client.esb.core.ln3177;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3177.Ln3177ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3177.Ln3177RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3177.req.Ln3177ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3177.resp.Ln3177RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3177)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3177Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3177Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3177
     * 交易描述：资产转让付款状态维护
     *
     * @param ln3177ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3177:资产转让付款状态维护")
    @PostMapping("/ln3177")
    protected @ResponseBody
    ResultDto<Ln3177RespDto> ln3177(@Validated @RequestBody Ln3177ReqDto ln3177ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3177.key, EsbEnum.TRADE_CODE_LN3177.value, JSON.toJSONString(ln3177ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3177.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3177.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3177.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3177.resp.Service();
        Ln3177ReqService ln3177ReqService = new Ln3177ReqService();
        Ln3177RespService ln3177RespService = new Ln3177RespService();
        Ln3177RespDto ln3177RespDto = new Ln3177RespDto();
        ResultDto<Ln3177RespDto> ln3177ResultDto = new ResultDto<Ln3177RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3177ReqDto转换成reqService
            BeanUtils.copyProperties(ln3177ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3177.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3177ReqService.setService(reqService);
            // 将ln3177ReqService转换成ln3177ReqServiceMap
            Map ln3177ReqServiceMap = beanMapUtil.beanToMap(ln3177ReqService);
            context.put("tradeDataMap", ln3177ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3177.key, EsbEnum.TRADE_CODE_LN3177.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3177.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3177.key, EsbEnum.TRADE_CODE_LN3177.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3177RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3177RespService.class, Ln3177RespService.class);
            respService = ln3177RespService.getService();

            ln3177ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3177ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3177RespDto
                BeanUtils.copyProperties(respService, ln3177RespDto);
                ln3177ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3177ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3177ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3177ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3177.key, EsbEnum.TRADE_CODE_LN3177.value, e.getMessage());
            ln3177ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3177ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3177ResultDto.setData(ln3177RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3177.key, EsbEnum.TRADE_CODE_LN3177.value, JSON.toJSONString(ln3177ResultDto));
        return ln3177ResultDto;
    }

}
