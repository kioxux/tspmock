package cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdzqgjh;

public class Record {
    private String qishriqi;
    private String zwhkriqi;
    private String daoqriqi;

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZwhkriqi() {
        return zwhkriqi;
    }

    public void setZwhkriqi(String zwhkriqi) {
        this.zwhkriqi = zwhkriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    @Override
    public String toString() {
        return "Record{" +
                "qishriqi='" + qishriqi + '\'' +
                ", zwhkriqi='" + zwhkriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                '}';
    }
}
