package cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.resp;

/**
 * 响应Service：账户信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class D12000RespMessage {
	private Message message;

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "D12000RespMessage{" +
				"message=" + message +
				'}';
	}
}
