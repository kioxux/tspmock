package cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req;

/**
 * 请求Service：分期审核
 *
 * @author chenyong
 * @version 1.0
 */
public class D13162ReqMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D13162ReqMessage{" +
                "message=" + message +
                '}';
    }
}
