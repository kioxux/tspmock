package cn.com.yusys.yusp.online.client.esb.core.ln3085.resp;

/**
 * 响应Service：贷款本息减免
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3085RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
