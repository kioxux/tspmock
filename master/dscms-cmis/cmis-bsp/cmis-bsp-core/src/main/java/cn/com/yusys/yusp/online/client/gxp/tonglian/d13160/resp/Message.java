package cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp;

import cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead;

/**
 * <br>
 * 0.2ZRC:2021/5/27 9:37:<br>
 * 响应Message：大额现金分期申请
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 9:37
 * @since 2021/5/27 9:37
 */
public class Message {
    private cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead head;
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp.Body body;

    public GxpRespHead getHead() {
        return head;
    }

    public void setHead(GxpRespHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
