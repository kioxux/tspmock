package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:非垫款借据信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:10
 */
public class AccLoanInfo {

    private List<AccLoanInfoRecord> record; // 非垫款借据信息

    public List<AccLoanInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<AccLoanInfoRecord> record) {
        this.record = record;
    }


    @Override
    public String toString() {
        return "AccLoanInfo{" +
                "record=" + record +
                '}';
    }
}
