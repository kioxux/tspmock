package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.req;

/**
 * 请求Service：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 *
 * @author lihh
 * @version 1.0
 */
public class Dhxd01ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
