package cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.req;

/**
 * 请求Service：取得蚂蚁核销记录表的核销借据号一览
 * @author leehuang
 * @version 1.0             
 */      
public class Fbxd16ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
