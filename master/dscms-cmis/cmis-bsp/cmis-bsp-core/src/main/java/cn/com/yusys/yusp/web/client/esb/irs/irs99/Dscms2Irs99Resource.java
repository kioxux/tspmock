package cn.com.yusys.yusp.web.client.esb.irs.irs99;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.common.AssureAccInfo;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.common.BizMessageInfoRecord;
import cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfoRecord;
import cn.com.yusys.yusp.online.client.esb.irs.irs99.req.Irs99ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.irs99.resp.Irs99RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(irs99)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Irs99Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Irs99Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 债项评级（处理码IRS99）
     *
     * @param irs99ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("irs99:债项评级")
    @PostMapping("/irs99")
    protected @ResponseBody
    ResultDto<Irs99RespDto> irs99(@Validated @RequestBody Irs99ReqDto irs99ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS99.key, EsbEnum.TRADE_CODE_IRS99.value, JSON.toJSONString(irs99ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.irs99.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.irs99.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.irs99.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.irs99.resp.Service();
        Irs99ReqService irs99ReqService = new Irs99ReqService();
        Irs99RespService irs99RespService = new Irs99RespService();
        Irs99RespDto irs99RespDto = new Irs99RespDto();
        ResultDto<Irs99RespDto> irs99ResultDto = new ResultDto<Irs99RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Irs99ReqDto转换成reqService
            BeanUtils.copyProperties(irs99ReqDto, reqService);
            // 综合授信申请信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo limitApplyInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getLimitApplyInfo())) {
                List<LimitApplyInfoRecord> limitApplyInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo> limitApplyInfoDtos = irs99ReqDto.getLimitApplyInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo limitApplyInfoDto : limitApplyInfoDtos) {
                    LimitApplyInfoRecord limitApplyInfoRecord = new LimitApplyInfoRecord();
                    BeanUtils.copyProperties(limitApplyInfoDto, limitApplyInfoRecord);
                    limitApplyInfoRecords.add(limitApplyInfoRecord);
                }
                limitApplyInfo.setRecord(limitApplyInfoRecords);
            }
            reqService.setLimitApplyInfo(limitApplyInfo);
            // 分项额度信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo limitDetailsInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getLimitDetailsInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord> limitDetailsInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo> limitDetailsInfoDtos = irs99ReqDto.getLimitDetailsInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo limitDetailsInfoDto : limitDetailsInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord limitDetailsInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord();
                    BeanUtils.copyProperties(limitDetailsInfoDto, limitDetailsInfoRecord);
                    limitDetailsInfoRecords.add(limitDetailsInfoRecord);
                }
                limitDetailsInfo.setRecord(limitDetailsInfoRecords);
            }
            reqService.setLimitDetailsInfo(limitDetailsInfo);
            // 产品额度信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo limitProductInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getLimitProductInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord> limitProductInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo> limitProductInfoDtos = irs99ReqDto.getLimitProductInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo limitProductInfoDto : limitProductInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord limitProductInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord();
                    BeanUtils.copyProperties(limitProductInfoDto, limitProductInfoRecord);
                    limitProductInfoRecords.add(limitProductInfoRecord);
                }
                limitProductInfo.setRecord(limitProductInfoRecords);
            }
            reqService.setLimitProductInfo(limitProductInfo);
            // 客户信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo customerInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getCustomerInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord> customerInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo> customerInfoDtos = irs99ReqDto.getCustomerInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo customerInfoDto : customerInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord customerInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord();
                    BeanUtils.copyProperties(customerInfoDto, customerInfoRecord);
                    customerInfoRecords.add(customerInfoRecord);
                }
                customerInfo.setRecord(customerInfoRecords);
            }
            reqService.setCustomerInfo(customerInfo);
            // 授信分项额度与抵质押、保证人关系信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo limitPleMortInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getLimitPleMortInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord> limitPleMortInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo> limitPleMortInfoDtos = irs99ReqDto.getLimitPleMortInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo limitPleMortInfoDto : limitPleMortInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord limitPleMortInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord();
                    BeanUtils.copyProperties(limitPleMortInfoDto, limitPleMortInfoRecord);
                    limitPleMortInfoRecords.add(limitPleMortInfoRecord);
                }
                limitPleMortInfo.setRecord(limitPleMortInfoRecords);
            }
            reqService.setLimitPleMortInfo(limitPleMortInfo);
            // 质押物信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo pledgeInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getPledgeInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord> pledgeInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo> pledgeInfoDtos = irs99ReqDto.getPledgeInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo pledgeInfoDto : pledgeInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord pledgeInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord();
                    BeanUtils.copyProperties(pledgeInfoDto, pledgeInfoRecord);
                    pledgeInfoRecords.add(pledgeInfoRecord);
                }
                pledgeInfo.setRecord(pledgeInfoRecords);
            }
            reqService.setPledgeInfo(pledgeInfo);
            // 抵押物信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo mortgageInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getMortgageInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord> mortgageInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo> mortgageInfoDtos = irs99ReqDto.getMortgageInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo mortgageInfoDto : mortgageInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord mortgageInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord();
                    BeanUtils.copyProperties(mortgageInfoDto, mortgageInfoRecord);
                    mortgageInfoRecords.add(mortgageInfoRecord);
                }
                mortgageInfo.setRecord(mortgageInfoRecords);
            }
            reqService.setMortgageInfo(mortgageInfo);
            // 保证人信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo assurePersonInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getAssurePersonInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord> assurePersonInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo> assurePersonInfoDtos = irs99ReqDto.getAssurePersonInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo assurePersonInfoDto : assurePersonInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord assurePersonInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord();
                    BeanUtils.copyProperties(assurePersonInfoDto, assurePersonInfoRecord);
                    assurePersonInfoRecords.add(assurePersonInfoRecord);
                }
                assurePersonInfo.setRecord(assurePersonInfoRecords);
            }
            reqService.setAssurePersonInfo(assurePersonInfo);
            // 汇率信息
            cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo curInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getCurInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord> curInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo> curInfoDtos = irs99ReqDto.getCurInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo curInfoDto : curInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord curInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord();
                    BeanUtils.copyProperties(curInfoDto, curInfoRecord);
                    curInfoRecords.add(curInfoRecord);
                }
                curInfo.setRecord(curInfoRecords);
            }
            reqService.setCurInfo(curInfo);

            // 业务信息
            cn.com.yusys.yusp.online.client.esb.irs.common.BusinessInfo businessInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.BusinessInfo();
            if (CollectionUtils.nonEmpty(irs99ReqDto.getBusinessInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.BusinessInfoRecord> businessInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessInfo> businessInfoDtos = irs99ReqDto.getBusinessInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.BusinessInfo businessInfoDto : businessInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.BusinessInfoRecord businessInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.BusinessInfoRecord();
                    BeanUtils.copyProperties(businessInfoDto, businessInfoRecord);
                    businessInfoRecords.add(businessInfoRecord);
                }
                businessInfo.setRecord(businessInfoRecords);
            }
            reqService.setBusinessInfo(businessInfo);

            List<AssureAccInfo> assureAccInfoList = Optional.ofNullable(irs99ReqDto.getAssureAccInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(assureAccInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfo assureAccInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfoRecord> assureAccInfoRecords = assureAccInfoList.parallelStream().map(assureAccInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfoRecord();
                    BeanUtils.copyProperties(assureAccInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                assureAccInfoService.setRecord(assureAccInfoRecords);
                reqService.setAssureAccInfo(assureAccInfoService);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS99.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            irs99ReqService.setService(reqService);
            // 将irs99ReqService转换成irs99ReqServiceMap
            Map irs99ReqServiceMap = beanMapUtil.beanToMap(irs99ReqService);
            context.put("tradeDataMap", irs99ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS99.key, EsbEnum.TRADE_CODE_IRS99.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS99.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS99.key, EsbEnum.TRADE_CODE_IRS99.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            irs99RespService = beanMapUtil.mapToBean(tradeDataMap, Irs99RespService.class, Irs99RespService.class);
            respService = irs99RespService.getService();

            //  将Irs99RespDto封装到ResultDto<Irs99RespDto>
            irs99ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            irs99ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Irs99RespDto
                BeanUtils.copyProperties(respService, irs99RespDto);
                cn.com.yusys.yusp.online.client.esb.irs.common.BizMessageInfo bizMessageInfo = Optional.ofNullable(respService.getBizMessageInfo()).orElse(new cn.com.yusys.yusp.online.client.esb.irs.common.BizMessageInfo());
                List<BizMessageInfoRecord> bizMessageInfoRecords = Optional.ofNullable(bizMessageInfo.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(bizMessageInfoRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.irs.common.BizMessageInfo> bizMessageInfoResp = bizMessageInfoRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.irs.common.BizMessageInfo temp = new cn.com.yusys.yusp.dto.client.esb.irs.common.BizMessageInfo();
                        //BeanUtils.copyProperties(record, temp);
                        temp.setSerialno(record.getSerialno()); // 业务流水号
                        temp.setGuaranteegrade(record.getGuaranteeGrade()); // 债项等级
                        temp.setEad(record.getEad()); // 违约风险暴露
                        temp.setLgd(record.getLgd()); // 违约损失率
                        return temp;
                    }).collect(Collectors.toList());
                    irs99RespDto.setBizMessageInfo(bizMessageInfoResp);
                }
                irs99ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                irs99ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                irs99ResultDto.setCode(EpbEnum.EPB099999.key);
                irs99ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS99.key, EsbEnum.TRADE_CODE_IRS99.value, e.getMessage());
            irs99ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            irs99ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        irs99ResultDto.setData(irs99RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS99.key, EsbEnum.TRADE_CODE_IRS99.value, JSON.toJSONString(irs99ResultDto));
        return irs99ResultDto;

    }
}
