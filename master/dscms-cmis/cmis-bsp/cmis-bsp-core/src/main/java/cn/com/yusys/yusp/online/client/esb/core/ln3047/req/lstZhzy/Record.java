package cn.com.yusys.yusp.online.client.esb.core.ln3047.req.lstZhzy;

import java.math.BigDecimal;

/**
 * 响应Service：质押还贷
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private Integer xuhaoooo;//序号
    private String kehuzhao;//客户账号
    private String khzhhzxh;//客户账号子序号
    private String djiebhao;//冻结编号
    private String zjzrzhao;//资金转入账号
    private String zjzrzzxh;//资金转入账号子序号
    private BigDecimal zhiyajee;//账户余额

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getDjiebhao() {
        return djiebhao;
    }

    public void setDjiebhao(String djiebhao) {
        this.djiebhao = djiebhao;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public BigDecimal getZhiyajee() {
        return zhiyajee;
    }

    public void setZhiyajee(BigDecimal zhiyajee) {
        this.zhiyajee = zhiyajee;
    }

    @Override
    public String toString() {
        return "Record{" +
                "xuhaoooo=" + xuhaoooo +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                ", djiebhao='" + djiebhao + '\'' +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                ", zhiyajee=" + zhiyajee +
                '}';
    }
}
