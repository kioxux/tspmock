package cn.com.yusys.yusp.web.client.esb.core.dp2667;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.dp2667.req.Dp2667ReqService;
import cn.com.yusys.yusp.online.client.esb.core.dp2667.resp.Dp2667RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:组合账户特殊账户查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(dp2667)")
@RestController
@RequestMapping("/api/dscms2coredp")
public class Dscms2Dp2667Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dp2667Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");

    /**
     * 交易码：dp2667
     * 交易描述：组合账户特殊账户查询
     *
     * @param dp2667ReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("dp2667:组合账户特殊账户查询")
    @PostMapping("/dp2667")
    protected @ResponseBody
    ResultDto<Dp2667RespDto> dp2667(@Validated @RequestBody Dp2667ReqDto dp2667ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2667.key, EsbEnum.TRADE_CODE_DP2667.value, JSON.toJSONString(dp2667ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.dp2667.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.dp2667.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.dp2667.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.dp2667.resp.Service();
        Dp2667ReqService dp2667ReqService = new Dp2667ReqService();
        Dp2667RespService dp2667RespService = new Dp2667RespService();
        Dp2667RespDto dp2667RespDto = new Dp2667RespDto();
        ResultDto<Dp2667RespDto> dp2667ResultDto = new ResultDto<Dp2667RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将dp2667ReqDto转换成reqService
			BeanUtils.copyProperties(dp2667ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_DP2667.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			dp2667ReqService.setService(reqService);
			// 将dp2667ReqService转换成dp2667ReqServiceMap
			Map dp2667ReqServiceMap = beanMapUtil.beanToMap(dp2667ReqService);
			context.put("tradeDataMap", dp2667ReqServiceMap);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DP2667.key, context);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			dp2667RespService = beanMapUtil.mapToBean(tradeDataMap, Dp2667RespService.class, Dp2667RespService.class);
			respService = dp2667RespService.getService();
			//  将respService转换成Dp2667RespDto
			dp2667ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
			dp2667ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Ib1241RespDto
				BeanUtils.copyProperties(respService, dp2667RespDto);
				dp2667ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				dp2667ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				dp2667ResultDto.setCode(EpbEnum.EPB099999.key);
				dp2667ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2667.key, EsbEnum.TRADE_CODE_DP2667.value, e.getMessage());
			dp2667ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			dp2667ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}

		dp2667ResultDto.setData(dp2667RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2667.key, EsbEnum.TRADE_CODE_DP2667.value, JSON.toJSONString(dp2667ResultDto));
        return dp2667ResultDto;
    }
}
