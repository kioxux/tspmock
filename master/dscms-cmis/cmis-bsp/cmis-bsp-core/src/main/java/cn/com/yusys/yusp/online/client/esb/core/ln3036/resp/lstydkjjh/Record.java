package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstydkjjh;

import java.math.BigDecimal;

public class Record {
    private String ydkjiejh;//原贷款借据号

    private BigDecimal jxhuanbj;//借新本金

    private BigDecimal zihuanbj;//自还本金

    private BigDecimal zihuanlx;//自还利息

    public String getYdkjiejh() {
        return ydkjiejh;
    }

    public void setYdkjiejh(String ydkjiejh) {
        this.ydkjiejh = ydkjiejh;
    }

    public BigDecimal getJxhuanbj() {
        return jxhuanbj;
    }

    public void setJxhuanbj(BigDecimal jxhuanbj) {
        this.jxhuanbj = jxhuanbj;
    }

    public BigDecimal getZihuanbj() {
        return zihuanbj;
    }

    public void setZihuanbj(BigDecimal zihuanbj) {
        this.zihuanbj = zihuanbj;
    }

    public BigDecimal getZihuanlx() {
        return zihuanlx;
    }

    public void setZihuanlx(BigDecimal zihuanlx) {
        this.zihuanlx = zihuanlx;
    }

    @Override
    public String toString() {
        return "Record{" +
                "ydkjiejh='" + ydkjiejh + '\'' +
                ", jxhuanbj=" + jxhuanbj +
                ", zihuanbj=" + zihuanbj +
                ", zihuanlx=" + zihuanlx +
                '}';
    }
}
