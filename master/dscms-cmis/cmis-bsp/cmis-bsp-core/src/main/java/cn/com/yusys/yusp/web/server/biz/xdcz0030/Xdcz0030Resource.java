package cn.com.yusys.yusp.web.server.biz.xdcz0030;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0030.req.Xdcz0030ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0030.resp.Xdcz0030RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.req.Xdcz0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0030.resp.Xdcz0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:校验额度是否足额，合同是否足额
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDCZ0030:校验额度是否足额，合同是否足额")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0030Resource {
	private static final Logger logger = LoggerFactory.getLogger(Xdcz0030Resource.class);
	@Autowired
	private DscmsBizCzClientService dscmsBizCzClientService;

	/**
	 * 交易码：xdcz0030
	 * 交易描述：校验额度是否足额，合同是否足额
	 *
	 * @param xdcz0030ReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("校验额度是否足额，合同是否足额")
	@PostMapping("/xdcz0030")
	@Idempotent({"xdcz0030", "#xdcz0030ReqDto.datasq"})
	protected @ResponseBody
	Xdcz0030RespDto xdcz0030(@Validated @RequestBody Xdcz0030ReqDto xdcz0030ReqDto) throws Exception {

		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030ReqDto));
		Xdcz0030DataReqDto xdcz0030DataReqDto = new Xdcz0030DataReqDto();// 请求Data： 校验额度是否足额，合同是否足额
		Xdcz0030DataRespDto xdcz0030DataRespDto = new Xdcz0030DataRespDto();// 响应Data：校验额度是否足额，合同是否足额

		cn.com.yusys.yusp.dto.server.biz.xdcz0030.req.Data reqData = null; // 请求Data：校验额度是否足额，合同是否足额
		cn.com.yusys.yusp.dto.server.biz.xdcz0030.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdcz0030.resp.Data();// 响应Data：校验额度是否足额，合同是否足额
		Xdcz0030RespDto xdcz0030RespDto = new Xdcz0030RespDto();
		try {
			// 从 xdcz0030ReqDto获取 reqData
			reqData = xdcz0030ReqDto.getData();
			// 将 reqData 拷贝到xdcz0030DataReqDto
			BeanUtils.copyProperties(reqData, xdcz0030DataReqDto);

			logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataReqDto));
			ResultDto<Xdcz0030DataRespDto> xdcz0030DataResultDto = dscmsBizCzClientService.xdcz0030(xdcz0030DataReqDto);
			logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030DataResultDto));
			// 从返回值中获取响应码和响应消息
			xdcz0030RespDto.setErorcd(Optional.ofNullable(xdcz0030DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
			xdcz0030RespDto.setErortx(Optional.ofNullable(xdcz0030DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

			if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0030DataResultDto.getCode())) {
				xdcz0030DataRespDto = xdcz0030DataResultDto.getData();
				xdcz0030RespDto.setErorcd(SuccessEnum.SUCCESS.key);
				xdcz0030RespDto.setErortx(SuccessEnum.SUCCESS.value);
				// 将 xdcz0030DataRespDto拷贝到 respData
				BeanUtils.copyProperties(xdcz0030DataRespDto, respData);
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, e.getMessage());
			xdcz0030RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
			xdcz0030RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
		}
		logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
		String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
		logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
		xdcz0030RespDto.setDatasq(servsq);

		xdcz0030RespDto.setData(respData);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0030.key, DscmsEnum.TRADE_CODE_XDCZ0030.value, JSON.toJSONString(xdcz0030RespDto));
		return xdcz0030RespDto;
	}
}
