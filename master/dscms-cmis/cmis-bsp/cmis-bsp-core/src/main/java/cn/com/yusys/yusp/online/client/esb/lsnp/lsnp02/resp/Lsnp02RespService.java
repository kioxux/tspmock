package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.resp;

/**
 * 响应Service：信用卡业务零售评级
 *
 * @author lihh
 * @version 1.0
 */
public class Lsnp02RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
