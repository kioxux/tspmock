package cn.com.yusys.yusp.web.server.biz.xdzc0017;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0017.req.Xdzc0017ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0017.resp.Xdzc0017RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0017.req.Xdzc0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0017.resp.Xdzc0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:新增购销合同申请撤回
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0017:新增购销合同申请撤回")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0017Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0017
     * 交易描述：新增购销合同申请撤回
     *
     * @param xdzc0017ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("新增购销合同申请撤回")
    @PostMapping("/xdzc0017")
    //@Idempotent({"xdzc017", "#xdzc0017ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0017RespDto xdzc0017(@Validated @RequestBody Xdzc0017ReqDto xdzc0017ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, JSON.toJSONString(xdzc0017ReqDto));
        Xdzc0017DataReqDto xdzc0017DataReqDto = new Xdzc0017DataReqDto();// 请求Data： 新增购销合同申请撤回
        Xdzc0017DataRespDto xdzc0017DataRespDto = new Xdzc0017DataRespDto();// 响应Data：新增购销合同申请撤回
        Xdzc0017RespDto xdzc0017RespDto = new Xdzc0017RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0017.req.Data reqData = null; // 请求Data：新增购销合同申请撤回
        cn.com.yusys.yusp.dto.server.biz.xdzc0017.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0017.resp.Data();// 响应Data：新增购销合同申请撤回

        try {
            // 从 xdzc0017ReqDto获取 reqData
            reqData = xdzc0017ReqDto.getData();
            // 将 reqData 拷贝到xdzc0017DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0017DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, JSON.toJSONString(xdzc0017DataReqDto));
            ResultDto<Xdzc0017DataRespDto> xdzc0017DataResultDto = dscmsBizZcClientService.xdzc0017(xdzc0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, JSON.toJSONString(xdzc0017DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0017RespDto.setErorcd(Optional.ofNullable(xdzc0017DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0017RespDto.setErortx(Optional.ofNullable(xdzc0017DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0017DataResultDto.getCode())) {
                xdzc0017DataRespDto = xdzc0017DataResultDto.getData();
                xdzc0017RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0017RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0017DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0017DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, e.getMessage());
            xdzc0017RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0017RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0017RespDto.setDatasq(servsq);

        xdzc0017RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0017.key, DscmsEnum.TRADE_CODE_XDZC0017.value, JSON.toJSONString(xdzc0017RespDto));
        return xdzc0017RespDto;
    }
}
