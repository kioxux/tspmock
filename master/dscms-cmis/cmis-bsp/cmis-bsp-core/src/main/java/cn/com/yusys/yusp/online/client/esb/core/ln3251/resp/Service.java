package cn.com.yusys.yusp.online.client.esb.core.ln3251.resp;

/**
 * 响应Service：贷款自动追缴明细查询
 *
 * @author 王玉坤
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String zongbish;// 总笔数
    private List list; // 贷款客户帐明细查询输出

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getZongbish() {
        return zongbish;
    }

    public void setZongbish(String zongbish) {
        this.zongbish = zongbish;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zongbish='" + zongbish + '\'' +
                ", list=" + list +
                '}';
    }
}
