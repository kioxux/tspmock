package cn.com.yusys.yusp.web.client.esb.core.mbt952;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.req.Mbt952ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.Mbt952RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.mbt952.req.Mbt952ReqService;
import cn.com.yusys.yusp.online.client.esb.core.mbt952.resp.CplWjplxxOut;
import cn.com.yusys.yusp.online.client.esb.core.mbt952.resp.Mbt952RespService;
import cn.com.yusys.yusp.online.client.esb.core.mbt952.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:批量结果确认处理
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(mbt952)")
@RestController
@RequestMapping("/api/dscms2corembt")
public class Mbt952Resource {
    private static final Logger logger = LoggerFactory.getLogger(Mbt952Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：mbt952
     * 交易描述：批量结果确认处理
     *
     * @param mbt952ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("mbt952:批量结果确认处理")
    @PostMapping("/mbt952")
    protected @ResponseBody
    ResultDto<Mbt952RespDto> mbt952(@Validated @RequestBody Mbt952ReqDto mbt952ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT952.key, EsbEnum.TRADE_CODE_MBT952.value, JSON.toJSONString(mbt952ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.mbt952.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.mbt952.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.mbt952.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.mbt952.resp.Service();
        Mbt952ReqService mbt952ReqService = new Mbt952ReqService();
        Mbt952RespService mbt952RespService = new Mbt952RespService();
        Mbt952RespDto mbt952RespDto = new Mbt952RespDto();
        ResultDto<Mbt952RespDto> mbt952ResultDto = new ResultDto<Mbt952RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将mbt952ReqDto转换成reqService
            BeanUtils.copyProperties(mbt952ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_MBT952.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            mbt952ReqService.setService(reqService);
            // 将mbt952ReqService转换成mbt952ReqServiceMap
            Map mbt952ReqServiceMap = beanMapUtil.beanToMap(mbt952ReqService);
            context.put("tradeDataMap", mbt952ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT952.key, EsbEnum.TRADE_CODE_MBT952.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_MBT952.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT952.key, EsbEnum.TRADE_CODE_MBT952.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            mbt952RespService = beanMapUtil.mapToBean(tradeDataMap, Mbt952RespService.class, Mbt952RespService.class);
            respService = mbt952RespService.getService();
            //  将respService转换成Mbt952RespDto
            BeanUtils.copyProperties(respService, mbt952RespDto);
            mbt952ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            mbt952ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {

                BeanUtils.copyProperties(respService, mbt952RespDto);
                CplWjplxxOut cplWjplxxOut = Optional.ofNullable(respService.getList()).orElse(new CplWjplxxOut());
                if (CollectionUtils.nonEmpty(cplWjplxxOut.getRecord())) {
                    List<Record> recordList = cplWjplxxOut.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.CplWjplxxOut> targetList = new ArrayList<>();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.CplWjplxxOut target = new cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.CplWjplxxOut();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    mbt952RespDto.setCplWjplxxOut(targetList);
                }
                mbt952ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                mbt952ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                mbt952ResultDto.setCode(EpbEnum.EPB099999.key);
                mbt952ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT952.key, EsbEnum.TRADE_CODE_MBT952.value, e.getMessage());
            mbt952ResultDto.setCode(EpbEnum.EPB099999.key);//9529
            mbt952ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        mbt952ResultDto.setData(mbt952RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT952.key, EsbEnum.TRADE_CODE_MBT952.value, JSON.toJSONString(mbt952ResultDto));
        return mbt952ResultDto;
    }
}
