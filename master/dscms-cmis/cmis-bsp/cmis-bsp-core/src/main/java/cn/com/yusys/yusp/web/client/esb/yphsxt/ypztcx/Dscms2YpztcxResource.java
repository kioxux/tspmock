package cn.com.yusys.yusp.web.client.esb.yphsxt.ypztcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.req.YpztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.YpztcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.req.YpztcxReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.YpztcxRespService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerfacilitylist.Registerfacilitylist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerhouselist.Record;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerhouselist.Registerhouselist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerlandlist.Registerlandlist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registermortgagelist.Registermortgagelist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerownerlist.Registerownerlist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerseizurelist.Registerseizurelist;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:信贷押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2YpztcxResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2YpztcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ypztcx
     * 交易描述：信贷押品状态查询
     *
     * @param ypztcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/ypztcx")
    protected @ResponseBody
    ResultDto<YpztcxRespDto> ypztcx(@Validated @RequestBody YpztcxReqDto ypztcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTCX.key, EsbEnum.TRADE_CODE_YPZTCX.value, JSON.toJSONString(ypztcxReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.Service();
        YpztcxReqService ypztcxReqService = new YpztcxReqService();
        YpztcxRespService ypztcxRespService = new YpztcxRespService();
        YpztcxRespDto ypztcxRespDto = new YpztcxRespDto();
        ResultDto<YpztcxRespDto> ypztcxResultDto = new ResultDto<YpztcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ypztcxReqDto转换成reqService
            BeanUtils.copyProperties(ypztcxReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_YPZTCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ypztcxReqService.setService(reqService);
            // 将ypztcxReqService转换成ypztcxReqServiceMap
            Map ypztcxReqServiceMap = beanMapUtil.beanToMap(ypztcxReqService);
            context.put("tradeDataMap", ypztcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTCX.key, EsbEnum.TRADE_CODE_YPZTCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_YPZTCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTCX.key, EsbEnum.TRADE_CODE_YPZTCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ypztcxRespService = beanMapUtil.mapToBean(tradeDataMap, YpztcxRespService.class, YpztcxRespService.class);
            respService = ypztcxRespService.getService();
            //  将respService转换成YpztcxRespDto
            ypztcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            ypztcxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, ypztcxRespDto);

                //registerhouselist
                Registerhouselist registerhouselist = Optional.ofNullable(respService.getRegisterhouselist()).orElse(new Registerhouselist());
                if (CollectionUtils.nonEmpty(registerhouselist.getRecord())) {
                    List<Record> recordRegisterhouselist = registerhouselist.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerhouselist> targetList1 = new ArrayList<>();
                    for (cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerhouselist.Record r1 : recordRegisterhouselist) {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerhouselist target1 = new cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerhouselist();
                        BeanUtils.copyProperties(r1, target1);
                        targetList1.add(target1);
                    }
                    ypztcxRespDto.setRegisterhouselist(targetList1);
                }

                //registerlandlist
                Registerlandlist registerlandlist = Optional.ofNullable(respService.getRegisterlandlist()).orElse(new Registerlandlist());
                if (CollectionUtils.nonEmpty(registerlandlist.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerlandlist.Record> recordRegisterlandlist = registerlandlist.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerlandlist> targetList2 = new ArrayList<>();
                    for (cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerlandlist.Record r2 : recordRegisterlandlist) {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerlandlist target2 = new cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerlandlist();
                        BeanUtils.copyProperties(r2, target2);
                        targetList2.add(target2);
                    }
                    ypztcxRespDto.setRegisterlandlist(targetList2);
                }
                //registerownerlist
                Registerownerlist registerownerlist = Optional.ofNullable(respService.getRegisterownerlist()).orElse(new Registerownerlist());
                if (CollectionUtils.nonEmpty(registerownerlist.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerownerlist.Record> recordRegisterownerlist = registerownerlist.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerownerlist> targetList3 = new ArrayList<>();
                    for (cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerownerlist.Record r3 : recordRegisterownerlist) {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerownerlist target3 = new cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerownerlist();
                        BeanUtils.copyProperties(r3, target3);
                        targetList3.add(target3);
                    }
                    ypztcxRespDto.setRegisterownerlist(targetList3);
                }
                //Registerfacilitylist
                Registerfacilitylist registerfacilitylist = Optional.ofNullable(respService.getRegisterfacilitylist()).orElse(new Registerfacilitylist());
                if (CollectionUtils.nonEmpty(registerfacilitylist.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerfacilitylist.Record> recordRegisterfacilitylist = registerfacilitylist.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerfacilitylist> targetList4 = new ArrayList<>();
                    for (cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerfacilitylist.Record r4 : recordRegisterfacilitylist) {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerfacilitylist target4 = new cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerfacilitylist();
                        BeanUtils.copyProperties(r4, target4);
                        targetList4.add(target4);
                    }
                    ypztcxRespDto.setRegisterfacilitylist(targetList4);
                }

                //registermortgagelist
                Registermortgagelist registermortgagelist = Optional.ofNullable(respService.getRegistermortgagelist()).orElse(new Registermortgagelist());
                if (CollectionUtils.nonEmpty(registermortgagelist.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registermortgagelist.Record> recordRegistermortgagelist = registermortgagelist.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registermortgagelist> targetList5 = new ArrayList<>();
                    for (cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registermortgagelist.Record r5 : recordRegistermortgagelist) {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registermortgagelist target5 = new cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registermortgagelist();
                        BeanUtils.copyProperties(r5, target5);
                        targetList5.add(target5);
                    }
                    ypztcxRespDto.setRegistermortgagelist(targetList5);
                }
                //Registerseizurelist
                Registerseizurelist registerseizurelist = Optional.ofNullable(respService.getRegisterseizurelist()).orElse(new Registerseizurelist());
                if (CollectionUtils.nonEmpty(registerseizurelist.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerseizurelist.Record> recordRegisterseizurelist = registerseizurelist.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerseizurelist> targetList6 = new ArrayList<>();
                    for (cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerseizurelist.Record r6 : recordRegisterseizurelist) {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerseizurelist target6 = new cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.Registerseizurelist();
                        BeanUtils.copyProperties(r6, target6);
                        targetList6.add(target6);
                    }
                    ypztcxRespDto.setRegisterseizurelist(targetList6);
                }

                ypztcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ypztcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                //ypztcxResultDto.setCode(EpbEnum.EPB099999.key);
               /*   0000 - 交易成功(查到未查封)
                    0001 - 抵押物编号：""+guar+"" 未查询到信息！
                    5555 - 抵押物编号：""+guarno+"" 存在查封情况！
                    0002 - 合同编号："+mocenu+" 未查询到信息！
                    0003 - 抵押物类型非不动产类型！
                    9999 - 通讯异常等信息**/
                ypztcxResultDto.setCode(respService.getErorcd());
                ypztcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTCX.key, EsbEnum.TRADE_CODE_YPZTCX.value, e.getMessage());
            ypztcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ypztcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ypztcxResultDto.setData(ypztcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTCX.key, EsbEnum.TRADE_CODE_YPZTCX.value, JSON.toJSONString(ypztcxResultDto));
        return ypztcxResultDto;
    }
}
