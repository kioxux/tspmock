package cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.req;

/**
 * 请求Service：查询人员基本信息岗位信息家庭信息
 */
public class Service {
    private String prcscd;//	处理码	,	是	,	接口交易码区分交易
    private String servtp;//	渠道	,	是	,	交易渠道(暂未确定，只保存值)
    private String servsq;//	渠道流水	,	是	,	由发起渠道生成的唯一标识
    private String userid;//	柜员号	,	是	,
    private String brchno;//	部门号	,	是	,

    private String ctmgno;//工号
    private String cardid;//身份证号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCtmgno() {
        return ctmgno;
    }

    public void setCtmgno(String ctmgno) {
        this.ctmgno = ctmgno;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", ctmgno='" + ctmgno + '\'' +
                ", cardid='" + cardid + '\'' +
                '}';
    }
}
