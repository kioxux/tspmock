package cn.com.yusys.yusp.web.client.esb.xwh.yx0003;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.req.Yx0003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.resp.Yx0003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwh.yx0003.req.Yx0003ReqService;
import cn.com.yusys.yusp.online.client.esb.xwh.yx0003.resp.Yx0003RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:市民贷优惠券核销锁定
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装小微公众号平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwh")
public class Dscms2Yx0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Yx0003Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：yx0003
     * 交易描述：市民贷优惠券核销锁定
     *
     * @param yx0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("yx0003:市民贷优惠券核销锁定")
    @PostMapping("/yx0003")
    protected @ResponseBody
    ResultDto<Yx0003RespDto> yx0003(@Validated @RequestBody Yx0003ReqDto yx0003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value, JSON.toJSONString(yx0003ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwh.yx0003.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwh.yx0003.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwh.yx0003.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwh.yx0003.resp.Service();
        Yx0003ReqService yx0003ReqService = new Yx0003ReqService();
        Yx0003RespService yx0003RespService = new Yx0003RespService();
        Yx0003RespDto yx0003RespDto = new Yx0003RespDto();
        ResultDto<Yx0003RespDto> yx0003ResultDto = new ResultDto<Yx0003RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将yx0003ReqDto转换成reqService
            BeanUtils.copyProperties(yx0003ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_YX0003.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            yx0003ReqService.setService(reqService);
            // 将yx0003ReqService转换成yx0003ReqServiceMap
            Map yx0003ReqServiceMap = beanMapUtil.beanToMap(yx0003ReqService);
            context.put("tradeDataMap", yx0003ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_YX0003.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            yx0003RespService = beanMapUtil.mapToBean(tradeDataMap, Yx0003RespService.class, Yx0003RespService.class);
            respService = yx0003RespService.getService();

            yx0003ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            yx0003ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, yx0003RespDto);
                yx0003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                yx0003ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                yx0003ResultDto.setCode(EpbEnum.EPB099999.key);
                yx0003ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value, e.getMessage());
            yx0003ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            yx0003ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        yx0003ResultDto.setData(yx0003RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YX0003.key, EsbEnum.TRADE_CODE_YX0003.value, JSON.toJSONString(yx0003ResultDto));
        return yx0003ResultDto;
    }
}
