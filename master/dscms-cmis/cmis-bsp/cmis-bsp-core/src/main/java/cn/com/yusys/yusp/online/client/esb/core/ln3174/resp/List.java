package cn.com.yusys.yusp.online.client.esb.core.ln3174.resp;

/**
 * 响应Service：资产证券化欠款借据查询
 *
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }
}
