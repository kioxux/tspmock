package cn.com.yusys.yusp.online.client.esb.core.ln3128.resp;

import java.util.List;

public class Lstdkjgzy_ARRAY {

    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkjgzy{" +
                "record=" + record +
                '}';
    }
}
