package cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 20:42
 * @since 2021/6/4 20:42
 */
@JsonPropertyOrder(alphabetic = true)
public class StatisItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "statisItemId")
    private String statisItemId;//统计项id
    @JsonProperty(value = "statisItemValue")
    private String statisItemValue;//统计项值
    @JsonProperty(value = "statisItemName")
    private String statisItemName;
    @JsonProperty(value = "statisItemPosition")
    private String statisItemPosition;

    public String getStatisItemId() {
        return statisItemId;
    }

    public void setStatisItemId(String statisItemId) {
        this.statisItemId = statisItemId;
    }

    public String getStatisItemValue() {
        return statisItemValue;
    }

    public void setStatisItemValue(String statisItemValue) {
        this.statisItemValue = statisItemValue;
    }

    public String getStatisItemName() {
        return statisItemName;
    }

    public void setStatisItemName(String statisItemName) {
        this.statisItemName = statisItemName;
    }

    public String getStatisItemPosition() {
        return statisItemPosition;
    }

    public void setStatisItemPosition(String statisItemPosition) {
        this.statisItemPosition = statisItemPosition;
    }

    @Override
    public String toString() {
        return "StatisItem{" +
                "statisItemId='" + statisItemId + '\'' +
                ", statisItemValue='" + statisItemValue + '\'' +
                ", statisItemName='" + statisItemName + '\'' +
                ", statisItemPosition='" + statisItemPosition + '\'' +
                '}';
    }
}
