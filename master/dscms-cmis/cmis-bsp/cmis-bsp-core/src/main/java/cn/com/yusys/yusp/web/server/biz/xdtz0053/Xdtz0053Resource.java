package cn.com.yusys.yusp.web.server.biz.xdtz0053;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0053.req.Xdtz0053ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0053.resp.Xdtz0053RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.req.Xdtz0053DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0053.resp.Xdtz0053DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:个人社会关系查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDTZ0053:个人社会关系查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0053Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0053Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0053
     * 交易描述：个人社会关系查询
     *
     * @param xdtz0053ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("个人社会关系查询")
    @PostMapping("/xdtz0053")
    //@Idempotent({"xdcatz0053", "#xdtz0053ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0053RespDto xdtz0053(@Validated @RequestBody Xdtz0053ReqDto xdtz0053ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053ReqDto));
        Xdtz0053DataReqDto xdtz0053DataReqDto = new Xdtz0053DataReqDto();// 请求Data： 个人社会关系查询
        Xdtz0053DataRespDto xdtz0053DataRespDto = new Xdtz0053DataRespDto();// 响应Data：个人社会关系查询
        Xdtz0053RespDto xdtz0053RespDto = new Xdtz0053RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0053.req.Data reqData = null; // 请求Data：个人社会关系查询
        cn.com.yusys.yusp.dto.server.biz.xdtz0053.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0053.resp.Data();// 响应Data：个人社会关系查询
        try {
            // 从 xdtz0053ReqDto获取 reqData
            reqData = xdtz0053ReqDto.getData();
            // 将 reqData 拷贝到xdtz0053DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0053DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053DataReqDto));
            ResultDto<Xdtz0053DataRespDto> xdtz0053DataResultDto = dscmsBizTzClientService.xdtz0053(xdtz0053DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0053RespDto.setErorcd(Optional.ofNullable(xdtz0053DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0053RespDto.setErortx(Optional.ofNullable(xdtz0053DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0053DataResultDto.getCode())) {
                xdtz0053DataRespDto = xdtz0053DataResultDto.getData();
                xdtz0053RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0053RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0053DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0053DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, e.getMessage());
            xdtz0053RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0053RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0053RespDto.setDatasq(servsq);

        xdtz0053RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0053.key, DscmsEnum.TRADE_CODE_XDTZ0053.value, JSON.toJSONString(xdtz0053RespDto));
        return xdtz0053RespDto;
    }
}
