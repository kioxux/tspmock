package cn.com.yusys.yusp.web.server.biz.xdxw0055;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0055.req.Xdxw0055ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0055.resp.Xdxw0055RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0055.req.Xdxw0055DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0055.resp.Xdxw0055DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:经营地址查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0055:经营地址查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0055Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0055Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0055
     * 交易描述：经营地址查询
     *
     * @param xdxw0055ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("经营地址查询")
    @PostMapping("/xdxw0055")
    //@Idempotent({"xdcaxw0055", "#xdxw0055ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0055RespDto xdxw0055(@Validated @RequestBody Xdxw0055ReqDto xdxw0055ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, JSON.toJSONString(xdxw0055ReqDto));
        Xdxw0055DataReqDto xdxw0055DataReqDto = new Xdxw0055DataReqDto();// 请求Data： 经营地址查询
        Xdxw0055DataRespDto xdxw0055DataRespDto = new Xdxw0055DataRespDto();// 响应Data：经营地址查询
        Xdxw0055RespDto xdxw0055RespDto = new Xdxw0055RespDto();// 响应Data：经营地址查询

        cn.com.yusys.yusp.dto.server.biz.xdxw0055.req.Data reqData = null; // 请求Data：经营地址查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0055.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0055.resp.Data();// 响应Data：经营地址查询
        try {
            // 从 xdxw0055ReqDto获取 reqData
            reqData = xdxw0055ReqDto.getData();
            // 将 reqData 拷贝到xdxw0055DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0055DataReqDto);
            ResultDto<Xdxw0055DataRespDto> xdxw0055DataResultDto = dscmsBizXwClientService.xdxw0055(xdxw0055DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdxw0055RespDto.setErorcd(Optional.ofNullable(xdxw0055DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0055RespDto.setErortx(Optional.ofNullable(xdxw0055DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdxw0055RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0055DataResultDto.getCode())) {
                xdxw0055RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0055RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0055DataRespDto = xdxw0055DataResultDto.getData();
                // 将 xdxw0055DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0055DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, e.getMessage());
            xdxw0055RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0055RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0055RespDto.setDatasq(servsq);
        xdxw0055RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0055.key, DscmsEnum.TRADE_CODE_XDXW0055.value, JSON.toJSONString(xdxw0055RespDto));
        return xdxw0055RespDto;
    }
}
