package cn.com.yusys.yusp.web.server.biz.xdxw0032;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0032.req.Xdxw0032ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0032.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0032.resp.Xdxw0032RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0032.req.Xdxw0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0032.resp.Xdxw0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询信贷系统的审批历史信息
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0032:查询信贷系统的审批历史信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0032Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0032
     * 交易描述：查询信贷系统的审批历史信息
     *
     * @param xdxw0032ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询信贷系统的审批历史信息")
    @PostMapping("/xdxw0032")
    //@Idempotent({"xdcaxw0032", "#xdxw0032ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0032RespDto xdxw0032(@Validated @RequestBody Xdxw0032ReqDto xdxw0032ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032ReqDto));
        Xdxw0032DataReqDto xdxw0032DataReqDto = new Xdxw0032DataReqDto();// 请求Data： 查询信贷系统的审批历史信息
        Xdxw0032DataRespDto xdxw0032DataRespDto = new Xdxw0032DataRespDto();// 响应Data：查询信贷系统的审批历史信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0032.req.Data reqData = null; // 请求Data：查询信贷系统的审批历史信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0032.resp.Data respData = new Data();// 响应Data：查询信贷系统的审批历史信息
		Xdxw0032RespDto xdxw0032RespDto = new Xdxw0032RespDto();
		try {
            // 从 xdxw0032ReqDto获取 reqData
            reqData = xdxw0032ReqDto.getData();
            // 将 reqData 拷贝到xdxw0032DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0032DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032DataReqDto));
            ResultDto<Xdxw0032DataRespDto> xdxw0032DataResultDto = dscmsBizXwClientService.xdxw0032(xdxw0032DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0032RespDto.setErorcd(Optional.ofNullable(xdxw0032DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0032RespDto.setErortx(Optional.ofNullable(xdxw0032DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0032DataResultDto.getCode())) {
                xdxw0032DataRespDto = xdxw0032DataResultDto.getData();
                xdxw0032RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0032RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0032DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0032DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, e.getMessage());
            xdxw0032RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0032RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0032RespDto.setDatasq(servsq);

        xdxw0032RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0032.key, DscmsEnum.TRADE_CODE_XDXW0032.value, JSON.toJSONString(xdxw0032RespDto));
        return xdxw0032RespDto;
    }
}
