package cn.com.yusys.yusp.web.client.esb.yphsxt.xddb01;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.req.Xddb01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.resp.Xddb01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.req.Xddb01ReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.resp.Xddb01RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询不动产信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2Xddb01Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xddb01Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xddb01
     * 交易描述：查询不动产信息
     *
     * @param xddb01ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xddb01")
    protected @ResponseBody
    ResultDto<Xddb01RespDto> xddb01(@Validated @RequestBody Xddb01ReqDto xddb01ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(xddb01ReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.resp.Service();
        Xddb01ReqService xddb01ReqService = new Xddb01ReqService();
        Xddb01RespService xddb01RespService = new Xddb01RespService();
        Xddb01RespDto xddb01RespDto = new Xddb01RespDto();
        ResultDto<Xddb01RespDto> xddb01ResultDto = new ResultDto<Xddb01RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xddb01ReqDto转换成reqService
            BeanUtils.copyProperties(xddb01ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDDB01.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xddb01ReqService.setService(reqService);
            // 将xddb01ReqService转换成xddb01ReqServiceMap
            Map xddb01ReqServiceMap = beanMapUtil.beanToMap(xddb01ReqService);
            context.put("tradeDataMap", xddb01ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDDB01.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xddb01RespService = beanMapUtil.mapToBean(tradeDataMap, Xddb01RespService.class, Xddb01RespService.class);
            respService = xddb01RespService.getService();
            //  将respService转换成Xddb01RespDto
            xddb01ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xddb01ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xddb01RespDto);
                xddb01RespDto.setLand_up(respService.getLand_up());
                xddb01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb01ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xddb01ResultDto.setCode(EpbEnum.EPB099999.key);
                xddb01ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, e.getMessage());
            xddb01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xddb01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xddb01ResultDto.setData(xddb01RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB01.key, EsbEnum.TRADE_CODE_XDDB01.value, JSON.toJSONString(xddb01ResultDto));
        return xddb01ResultDto;
    }
}
