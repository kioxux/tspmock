package cn.com.yusys.yusp.web.client.esb.core.dp2352;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2352.Dp2352ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2352.Dp2352RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2352.Listnm01;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.dp2352.req.Dp2352ReqService;
import cn.com.yusys.yusp.online.client.esb.core.dp2352.req.Record;
import cn.com.yusys.yusp.online.client.esb.core.dp2352.resp.Dp2352RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:组合账户子账户开立
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(dp2352)")
@RestController
@RequestMapping("/api/dscms2coredp")
public class Dscms2Dp2352Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dp2352Resource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");

	private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：dp2352
     * 交易描述：组合账户子账户开立
     *
     * @param dp2352ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("dp2352:组合账户子账户开立")
    @PostMapping("/dp2352")
    protected @ResponseBody
    ResultDto<Dp2352RespDto> dp2352(@Validated @RequestBody Dp2352ReqDto dp2352ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2352.key, EsbEnum.TRADE_CODE_DP2352.value, JSON.toJSONString(dp2352ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.dp2352.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.dp2352.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.dp2352.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.dp2352.resp.Service();
        Dp2352ReqService dp2352ReqService = new Dp2352ReqService();
        Dp2352RespService dp2352RespService = new Dp2352RespService();
        Dp2352RespDto dp2352RespDto = new Dp2352RespDto();
        ResultDto<Dp2352RespDto> dp2352ResultDto = new ResultDto<Dp2352RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将dp2352ReqDto转换成reqService
			BeanUtils.copyProperties(dp2352ReqDto, reqService);
			List<Listnm01> listnm01 = dp2352ReqDto.getListnm01();
			if (CollectionUtils.nonEmpty(listnm01)) {
				List<Record> records = listnm01.parallelStream().map(req -> {
					Record record = new Record();
					BeanUtils.copyProperties(req, record);
					return record;
				}).collect(Collectors.toList());
				cn.com.yusys.yusp.online.client.esb.core.dp2352.req.List list = new cn.com.yusys.yusp.online.client.esb.core.dp2352.req.List();
				list.setRecord(records);
				reqService.setList(list);
			}
			reqService.setPrcscd(EsbEnum.TRADE_CODE_DP2352.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

			dp2352ReqService.setService(reqService);
			// 将dp2352ReqService转换成dp2352ReqServiceMap
			Map dp2352ReqServiceMap = beanMapUtil.beanToMap(dp2352ReqService);
			context.put("tradeDataMap", dp2352ReqServiceMap);

			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2352.key, EsbEnum.TRADE_CODE_DP2352.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DP2352.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2352.key, EsbEnum.TRADE_CODE_DP2352.value);

			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			dp2352RespService = beanMapUtil.mapToBean(tradeDataMap, Dp2352RespService.class, Dp2352RespService.class);
			respService = dp2352RespService.getService();
			//  将respService转换成Dp2352RespDto
			BeanUtils.copyProperties(respService, dp2352RespDto);
			dp2352ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			dp2352ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Ib1241RespDto
				BeanUtils.copyProperties(respService, dp2352RespDto);
				dp2352ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				dp2352ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				dp2352ResultDto.setCode(EpbEnum.EPB099999.key);
				dp2352ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2352.key, EsbEnum.TRADE_CODE_DP2352.value, e.getMessage());
			dp2352ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			dp2352ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		dp2352ResultDto.setData(dp2352RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2352.key, EsbEnum.TRADE_CODE_DP2352.value, JSON.toJSONString(dp2352ResultDto));
        return dp2352ResultDto;
    }
}
