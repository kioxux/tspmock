package cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.req;

/**
 * 请求Service：押品状态变更
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:11:50
 */
public class YpztbgReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
