package cn.com.yusys.yusp.online.client.esb.core.da3321.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 16:47
 * @since 2021/6/7 16:47
 */
public class Listnm1 {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Listnm1{" +
                "record=" + record +
                '}';
    }
}
