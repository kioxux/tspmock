package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.math.BigDecimal;

/**
 * 请求Service：交易请求信息域:汇率信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class CurInfoRecord {

    private String curtype; // 币种代码
    private BigDecimal curvalue; // 汇率

    public String getCurtype() {
        return curtype;
    }

    public void setCurtype(String curtype) {
        this.curtype = curtype;
    }

    public BigDecimal getCurvalue() {
        return curvalue;
    }

    public void setCurvalue(BigDecimal curvalue) {
        this.curvalue = curvalue;
    }

    @Override
    public String toString() {
        return "CurInfo{" +
                "curtype='" + curtype + '\'' +
                ", curvalue=" + curvalue +
                '}';
    }
}
