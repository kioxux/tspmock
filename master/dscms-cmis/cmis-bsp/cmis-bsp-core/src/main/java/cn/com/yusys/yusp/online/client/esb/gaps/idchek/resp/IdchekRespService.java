package cn.com.yusys.yusp.online.client.esb.gaps.idchek.resp;

/**
 * 响应Service：身份证核查
 *
 * @author chenyong
 * @version 1.0
 */
public class IdchekRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "IdchekRespService{" +
                "service=" + service +
                '}';
    }
}
