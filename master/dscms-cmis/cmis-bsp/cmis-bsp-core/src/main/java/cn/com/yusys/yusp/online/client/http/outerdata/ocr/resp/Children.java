package cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 20:58
 * @since 2021/6/4 20:58
 */
@JsonPropertyOrder(alphabetic = true)
public class Children implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "subjectId")
    private String subjectId;//科目id
    @JsonProperty(value = "subjectName")
    private String subjectName;//科目名称
    @JsonProperty(value = "subjectCode")
    private String subjectCode;//科目编码
    @JsonProperty(value = "operator")
    private String operator;//子科目运算符
    @JsonProperty(value = "tag")
    private String tag;//科目状态，0负值转换，1试算平衡，2试算不平衡 3比较规则全部通过 4比较规则未全部通过
    @JsonProperty(value = "matchStatus")
    private Integer matchStatus;//匹配状态（0已匹配、1低匹配、2未匹配）
    @JsonProperty(value = "checkStatus")
    private Integer checkStatus;//科目核对状态，0未核对，1已核对
    @JsonProperty(value = "statisItem")
    private java.util.List<cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.StatisItem> statisItem;//科目对应的统计项列表

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getMatchStatus() {
        return matchStatus;
    }

    public void setMatchStatus(Integer matchStatus) {
        this.matchStatus = matchStatus;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public List<StatisItem> getStatisItem() {
        return statisItem;
    }

    public void setStatisItem(List<StatisItem> statisItem) {
        this.statisItem = statisItem;
    }

    @Override
    public String toString() {
        return "Children{" +
                "subjectId='" + subjectId + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", subjectCode='" + subjectCode + '\'' +
                ", operator='" + operator + '\'' +
                ", tag='" + tag + '\'' +
                ", matchStatus=" + matchStatus +
                ", checkStatus=" + checkStatus +
                ", statisItem=" + statisItem +
                '}';
    }
}
