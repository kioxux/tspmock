package cn.com.yusys.yusp.web.server.biz.xdxw0028;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0028.req.Xdxw0028ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0028.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0028.resp.Xdxw0028RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0028.req.Xdxw0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0028.resp.Xdxw0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号查询我行现有融资情况
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0028:根据客户号查询我行现有融资情况")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0028Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0028
     * 交易描述：根据客户号查询我行现有融资情况
     *
     * @param xdxw0028ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询我行现有融资情况")
    @PostMapping("/xdxw0028")
    //@Idempotent({"xdcaxw0028", "#xdxw0028ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0028RespDto xdxw0028(@Validated @RequestBody Xdxw0028ReqDto xdxw0028ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028ReqDto));
        Xdxw0028DataReqDto xdxw0028DataReqDto = new Xdxw0028DataReqDto();// 请求Data： 根据客户号查询我行现有融资情况
        Xdxw0028DataRespDto xdxw0028DataRespDto = new Xdxw0028DataRespDto();// 响应Data：根据客户号查询我行现有融资情况
        cn.com.yusys.yusp.dto.server.biz.xdxw0028.req.Data reqData = null; // 请求Data：根据客户号查询我行现有融资情况
        cn.com.yusys.yusp.dto.server.biz.xdxw0028.resp.Data respData = new Data();// 响应Data：根据客户号查询我行现有融资情况
		Xdxw0028RespDto xdxw0028RespDto = new Xdxw0028RespDto();
		try {
            // 从 xdxw0028ReqDto获取 reqData
            reqData = xdxw0028ReqDto.getData();
            // 将 reqData 拷贝到xdxw0028DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0028DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028DataReqDto));
            ResultDto<Xdxw0028DataRespDto> xdxw0028DataResultDto = dscmsBizXwClientService.xdxw0028(xdxw0028DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0028RespDto.setErorcd(Optional.ofNullable(xdxw0028DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0028RespDto.setErortx(Optional.ofNullable(xdxw0028DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0028DataResultDto.getCode())) {
                xdxw0028DataRespDto = xdxw0028DataResultDto.getData();
                xdxw0028RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0028RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0028DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0028DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, e.getMessage());
            xdxw0028RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0028RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0028RespDto.setDatasq(servsq);

        xdxw0028RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0028.key, DscmsEnum.TRADE_CODE_XDXW0028.value, JSON.toJSONString(xdxw0028RespDto));
        return xdxw0028RespDto;
    }
}
