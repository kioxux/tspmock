package cn.com.yusys.yusp.web.server.biz.xddb0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0006.req.Xddb0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0006.resp.Xddb0006RespDto;
import cn.com.yusys.yusp.dto.server.xddb0006.req.Xddb0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0006.resp.Xddb0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷押品状态查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "xddb0006:信贷押品状态查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0006Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0006
     * 交易描述：信贷押品状态查询
     *
     * @param xddb0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddb0006:信贷押品状态查询")
    @PostMapping("/xddb0006")
    //@Idempotent({"xddb0006", "#xddb0006ReqDto.datasq"})
    protected @ResponseBody
    Xddb0006RespDto xddb0006(@Validated @RequestBody Xddb0006ReqDto xddb0006ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, JSON.toJSONString(xddb0006ReqDto));
        Xddb0006DataReqDto xddb0006DataReqDto = new Xddb0006DataReqDto();// 请求Data： 信贷押品状态查询
        Xddb0006DataRespDto xddb0006DataRespDto = new Xddb0006DataRespDto();// 响应Data：信贷押品状态查询
        Xddb0006RespDto xddb0006RespDto = new Xddb0006RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0006.req.Data reqData = null;// 请求Data：信贷押品状态查询
        cn.com.yusys.yusp.dto.server.biz.xddb0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0006.resp.Data();// 响应Data：信贷押品状态查询
        try {
            // 从 xddb0006ReqDto获取 reqData
            reqData = xddb0006ReqDto.getData();
            // 将 reqData 拷贝到xddb0006DataReqDto
            BeanUtils.copyProperties(reqData, xddb0006DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, JSON.toJSONString(xddb0006DataReqDto));
            ResultDto<Xddb0006DataRespDto> xddb0006DataResultDto = dscmsBizDbClientService.xddb0006(xddb0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, JSON.toJSONString(xddb0006DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0006RespDto.setErorcd(Optional.ofNullable(xddb0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0006RespDto.setErortx(Optional.ofNullable(xddb0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0006DataResultDto.getCode())) {
                xddb0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xddb0006DataRespDto = xddb0006DataResultDto.getData();
                // 将 xddb0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, e.getMessage());
            xddb0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0006RespDto.setDatasq(servsq);

        xddb0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0006.key, DscmsEnum.TRADE_CODE_XDDB0006.value, JSON.toJSONString(xddb0006RespDto));
        return xddb0006RespDto;
    }
}
