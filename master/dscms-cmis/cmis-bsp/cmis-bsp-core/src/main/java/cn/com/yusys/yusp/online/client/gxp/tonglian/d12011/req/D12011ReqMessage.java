package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.req;

/**
 * 请求Message：卡片信息查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class D12011ReqMessage {
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D12011ReqMessage{" +
                "message=" + message +
                '}';
    }
}
