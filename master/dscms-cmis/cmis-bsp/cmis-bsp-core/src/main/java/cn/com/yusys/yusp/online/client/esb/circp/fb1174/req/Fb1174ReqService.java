package cn.com.yusys.yusp.online.client.esb.circp.fb1174.req;

/**
 * 请求Service：房抵e点贷尽调结果通知
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1174ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1174ReqService{" +
                "service=" + service +
                '}';
    }
}
