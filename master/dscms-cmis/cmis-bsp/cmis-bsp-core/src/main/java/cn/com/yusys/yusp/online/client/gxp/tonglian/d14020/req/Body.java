package cn.com.yusys.yusp.online.client.gxp.tonglian.d14020.req;

/**
 * 请求Dto：卡片信息查询
 *
 * @author lihh
 * @version 1.0
 */
public class Body {
    private String cardno;//卡号

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    @Override
    public String toString() {
        return "Body{" +
                "cardno='" + cardno + '\'' +
                '}';
    }
}  
