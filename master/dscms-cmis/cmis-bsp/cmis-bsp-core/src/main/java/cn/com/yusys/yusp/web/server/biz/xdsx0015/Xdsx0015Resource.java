package cn.com.yusys.yusp.web.server.biz.xdsx0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0015.req.Xdsx0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0015.resp.Xdsx0015RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0015.req.Xdsx0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0015.resp.Xdsx0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:已批复的授信申请信息、押品信息以及合同信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0015:已批复的授信申请信息、押品信息以及合同信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0015Resource.class);
    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0015
     * 交易描述：已批复的授信申请信息、押品信息以及合同信息同步
     *
     * @param xdsx0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("已批复的授信申请信息、押品信息以及合同信息同步")
    @PostMapping("/xdsx0015")
    //@Idempotent({"xdcasx0015", "#xdsx0015ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0015RespDto xdsx0015(@Validated @RequestBody Xdsx0015ReqDto xdsx0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, JSON.toJSONString(xdsx0015ReqDto));
        Xdsx0015DataReqDto xdsx0015DataReqDto = new Xdsx0015DataReqDto();// 请求Data： 已批复的授信申请信息、押品信息以及合同信息同步
        Xdsx0015DataRespDto xdsx0015DataRespDto = new Xdsx0015DataRespDto();// 响应Data：已批复的授信申请信息、押品信息以及合同信息同步
        Xdsx0015RespDto xdsx0015RespDto = new Xdsx0015RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdsx0015.req.Data reqData = null; // 请求Data：已批复的授信申请信息、押品信息以及合同信息同步
        cn.com.yusys.yusp.dto.server.biz.xdsx0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0015.resp.Data();// 响应Data：已批复的授信申请信息、押品信息以及合同信息同步

        try {
            // 从 xdsx0015ReqDto获取 reqData
            reqData = xdsx0015ReqDto.getData();
            // 将 reqData 拷贝到xdsx0015DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0015DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, JSON.toJSONString(xdsx0015DataReqDto));
            ResultDto<Xdsx0015DataRespDto> xdsx0015DataResultDto = dscmsBizSxClientService.xdsx0015(xdsx0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, JSON.toJSONString(xdsx0015DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0015RespDto.setErorcd(Optional.ofNullable(xdsx0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0015RespDto.setErortx(Optional.ofNullable(xdsx0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0015DataResultDto.getCode())) {
                xdsx0015DataRespDto = xdsx0015DataResultDto.getData();
                xdsx0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0015DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0015RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, e.getMessage());
            xdsx0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0015RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0015RespDto.setDatasq(servsq);

        xdsx0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0015.key, DscmsEnum.TRADE_CODE_XDSX0015.value, JSON.toJSONString(xdsx0015RespDto));
        return xdsx0015RespDto;
    }
}
