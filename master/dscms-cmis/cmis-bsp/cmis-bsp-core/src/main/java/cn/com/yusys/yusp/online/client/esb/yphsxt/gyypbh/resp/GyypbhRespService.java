package cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.resp;

/**
 * 响应Service：通过共有人编号查询押品编号
 *
 * @author chenyong
 * @version 1.0
 */
public class GyypbhRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "GyypbhRespService{" +
                "service=" + service +
                '}';
    }
}

