package cn.com.yusys.yusp.online.client.esb.core.da3321.req;

/**
 * 请求Service：抵债资产模糊查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3321ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3321ReqService{" +
                "service=" + service +
                '}';
    }
}
