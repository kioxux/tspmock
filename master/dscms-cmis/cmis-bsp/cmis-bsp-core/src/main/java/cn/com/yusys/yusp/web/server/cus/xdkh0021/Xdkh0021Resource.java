package cn.com.yusys.yusp.web.server.cus.xdkh0021;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0021.req.Xdkh0021ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0021.resp.Xdkh0021RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.req.Xdkh0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0021.resp.Xdkh0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:在信贷系统中生成用户信息
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDKH0021:在信贷系统中生成用户信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0021Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0021
     * 交易描述：在信贷系统中生成用户信息
     *
     * @param xdkh0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("在信贷系统中生成用户信息")
    @PostMapping("/xdkh0021")
    //@Idempotent({"xdcakh0021", "#xdkh0021ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0021RespDto xdkh0021(@Validated @RequestBody Xdkh0021ReqDto xdkh0021ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0021.key, DscmsEnum.TRADE_CODE_XDKH0021.value, JSON.toJSONString(xdkh0021ReqDto));
        Xdkh0021DataReqDto xdkh0021DataReqDto = new Xdkh0021DataReqDto();// 请求Data： 在信贷系统中生成用户信息
        Xdkh0021DataRespDto xdkh0021DataRespDto = new Xdkh0021DataRespDto();// 响应Data：在信贷系统中生成用户信息
        Xdkh0021RespDto xdkh0021RespDto = new Xdkh0021RespDto();//响应Dto：在信贷系统中生成用户信息
        cn.com.yusys.yusp.dto.server.cus.xdkh0021.req.Data reqData = null; // 请求Data：在信贷系统中生成用户信息
        cn.com.yusys.yusp.dto.server.cus.xdkh0021.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0021.resp.Data();// 响应Data：在信贷系统中生成用户信息
        try {
            // 从 xdkh0021ReqDto获取 reqData
            reqData = xdkh0021ReqDto.getData();
            // 将 reqData 拷贝到xdkh0021DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0021DataReqDto);
            // 调用服务：
            ResultDto<Xdkh0021DataRespDto> xdkh0021DataResultDto = dscmsCusClientService.xdkh0021(xdkh0021DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdkh0021RespDto.setErorcd(Optional.ofNullable(xdkh0021DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0021RespDto.setErortx(Optional.ofNullable(xdkh0021DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0021DataResultDto.getCode())) {
                xdkh0021DataRespDto = xdkh0021DataResultDto.getData();

                xdkh0021RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0021RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0021DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0021.key, DscmsEnum.TRADE_CODE_XDKH0021.value, e.getMessage());
            xdkh0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0021RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0021RespDto.setDatasq(servsq);
        xdkh0021RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0021.key, DscmsEnum.TRADE_CODE_XDKH0021.value, JSON.toJSONString(xdkh0021RespDto));
        return xdkh0021RespDto;
    }
}
