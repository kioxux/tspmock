package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:担保合同与合同关联信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class BusinessAssureInfo {
    private List<BusinessAssureInfoRecord> record; // 担保合同与合同关联信息

    public List<BusinessAssureInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<BusinessAssureInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "BusinessAssureInfo{" +
                "record=" + record +
                '}';
    }
}
