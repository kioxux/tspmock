package cn.com.yusys.yusp.online.client.esb.wx.wxp001.req;

/**
 * 请求Service：信贷将审批结果推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
public class Wxp001ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Wxp001ReqService{" +
                "service=" + service +
                '}';
    }
}
