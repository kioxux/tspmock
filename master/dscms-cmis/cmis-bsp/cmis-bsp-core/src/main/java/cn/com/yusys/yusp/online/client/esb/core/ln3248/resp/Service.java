package cn.com.yusys.yusp.online.client.esb.core.ln3248.resp;

import java.math.BigDecimal;

/**
 * 响应Service：委托清收变更
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
	private String erorcd;//响应码
	private String erortx;// 响应信息
	private String servsq;// 渠道流水
	private String datasq;//全局流水


	private String dkzhangh;//贷款账号
	private String dkjiejuh;//贷款借据号
	private String hetongbh;//合同编号
	private String kehuhaoo;//客户号
	private String kehumnch;//客户名称
	private String yngyjigo;//营业机构
	private String zhngjigo;//账务机构
	private String chanpdma;//产品代码
	private String kuaijilb;//会计类别
	private String chanpmch;//产品名称
	private String kaihriqi;//开户日期
	private String qixiriqi;//起息日期
	private String daoqriqi;//到期日期
	private String dkqixian;//贷款期限(月)
	private String daikxtai;//贷款形态
	private String yjfyjzht;//应计非应计状态
	private String dkzhhzht;//贷款账户状态
	private String huobdhao;//货币代号
	private BigDecimal jiejuuje;//借据金额
	private BigDecimal zhchbjin;//正常本金
	private BigDecimal yuqibjin;//逾期本金
	private BigDecimal dzhibjin;//呆滞本金
	private BigDecimal daizbjin;//呆账本金
	private BigDecimal dkuanjij;//贷款基金
	private BigDecimal ysyjlixi;//应收应计利息
	private BigDecimal csyjlixi;//催收应计利息
	private BigDecimal ysqianxi;//应收欠息
	private BigDecimal csqianxi;//催收欠息
	private BigDecimal ysyjfaxi;//应收应计罚息
	private BigDecimal csyjfaxi;//催收应计罚息
	private BigDecimal yshofaxi;//应收罚息
	private BigDecimal cshofaxi;//催收罚息
	private BigDecimal yingjifx;//应计复息
	private BigDecimal fuxiiiii;//复息

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getDkzhangh() {
		return dkzhangh;
	}

	public void setDkzhangh(String dkzhangh) {
		this.dkzhangh = dkzhangh;
	}

	public String getDkjiejuh() {
		return dkjiejuh;
	}

	public void setDkjiejuh(String dkjiejuh) {
		this.dkjiejuh = dkjiejuh;
	}

	public String getHetongbh() {
		return hetongbh;
	}

	public void setHetongbh(String hetongbh) {
		this.hetongbh = hetongbh;
	}

	public String getKehuhaoo() {
		return kehuhaoo;
	}

	public void setKehuhaoo(String kehuhaoo) {
		this.kehuhaoo = kehuhaoo;
	}

	public String getKehumnch() {
		return kehumnch;
	}

	public void setKehumnch(String kehumnch) {
		this.kehumnch = kehumnch;
	}

	public String getYngyjigo() {
		return yngyjigo;
	}

	public void setYngyjigo(String yngyjigo) {
		this.yngyjigo = yngyjigo;
	}

	public String getZhngjigo() {
		return zhngjigo;
	}

	public void setZhngjigo(String zhngjigo) {
		this.zhngjigo = zhngjigo;
	}

	public String getChanpdma() {
		return chanpdma;
	}

	public void setChanpdma(String chanpdma) {
		this.chanpdma = chanpdma;
	}

	public String getKuaijilb() {
		return kuaijilb;
	}

	public void setKuaijilb(String kuaijilb) {
		this.kuaijilb = kuaijilb;
	}

	public String getChanpmch() {
		return chanpmch;
	}

	public void setChanpmch(String chanpmch) {
		this.chanpmch = chanpmch;
	}

	public String getKaihriqi() {
		return kaihriqi;
	}

	public void setKaihriqi(String kaihriqi) {
		this.kaihriqi = kaihriqi;
	}

	public String getQixiriqi() {
		return qixiriqi;
	}

	public void setQixiriqi(String qixiriqi) {
		this.qixiriqi = qixiriqi;
	}

	public String getDaoqriqi() {
		return daoqriqi;
	}

	public void setDaoqriqi(String daoqriqi) {
		this.daoqriqi = daoqriqi;
	}

	public String getDkqixian() {
		return dkqixian;
	}

	public void setDkqixian(String dkqixian) {
		this.dkqixian = dkqixian;
	}

	public String getDaikxtai() {
		return daikxtai;
	}

	public void setDaikxtai(String daikxtai) {
		this.daikxtai = daikxtai;
	}

	public String getYjfyjzht() {
		return yjfyjzht;
	}

	public void setYjfyjzht(String yjfyjzht) {
		this.yjfyjzht = yjfyjzht;
	}

	public String getDkzhhzht() {
		return dkzhhzht;
	}

	public void setDkzhhzht(String dkzhhzht) {
		this.dkzhhzht = dkzhhzht;
	}

	public String getHuobdhao() {
		return huobdhao;
	}

	public void setHuobdhao(String huobdhao) {
		this.huobdhao = huobdhao;
	}

	public BigDecimal getJiejuuje() {
		return jiejuuje;
	}

	public void setJiejuuje(BigDecimal jiejuuje) {
		this.jiejuuje = jiejuuje;
	}

	public BigDecimal getZhchbjin() {
		return zhchbjin;
	}

	public void setZhchbjin(BigDecimal zhchbjin) {
		this.zhchbjin = zhchbjin;
	}

	public BigDecimal getYuqibjin() {
		return yuqibjin;
	}

	public void setYuqibjin(BigDecimal yuqibjin) {
		this.yuqibjin = yuqibjin;
	}

	public BigDecimal getDzhibjin() {
		return dzhibjin;
	}

	public void setDzhibjin(BigDecimal dzhibjin) {
		this.dzhibjin = dzhibjin;
	}

	public BigDecimal getDaizbjin() {
		return daizbjin;
	}

	public void setDaizbjin(BigDecimal daizbjin) {
		this.daizbjin = daizbjin;
	}

	public BigDecimal getDkuanjij() {
		return dkuanjij;
	}

	public void setDkuanjij(BigDecimal dkuanjij) {
		this.dkuanjij = dkuanjij;
	}

	public BigDecimal getYsyjlixi() {
		return ysyjlixi;
	}

	public void setYsyjlixi(BigDecimal ysyjlixi) {
		this.ysyjlixi = ysyjlixi;
	}

	public BigDecimal getCsyjlixi() {
		return csyjlixi;
	}

	public void setCsyjlixi(BigDecimal csyjlixi) {
		this.csyjlixi = csyjlixi;
	}

	public BigDecimal getYsqianxi() {
		return ysqianxi;
	}

	public void setYsqianxi(BigDecimal ysqianxi) {
		this.ysqianxi = ysqianxi;
	}

	public BigDecimal getCsqianxi() {
		return csqianxi;
	}

	public void setCsqianxi(BigDecimal csqianxi) {
		this.csqianxi = csqianxi;
	}

	public BigDecimal getYsyjfaxi() {
		return ysyjfaxi;
	}

	public void setYsyjfaxi(BigDecimal ysyjfaxi) {
		this.ysyjfaxi = ysyjfaxi;
	}

	public BigDecimal getCsyjfaxi() {
		return csyjfaxi;
	}

	public void setCsyjfaxi(BigDecimal csyjfaxi) {
		this.csyjfaxi = csyjfaxi;
	}

	public BigDecimal getYshofaxi() {
		return yshofaxi;
	}

	public void setYshofaxi(BigDecimal yshofaxi) {
		this.yshofaxi = yshofaxi;
	}

	public BigDecimal getCshofaxi() {
		return cshofaxi;
	}

	public void setCshofaxi(BigDecimal cshofaxi) {
		this.cshofaxi = cshofaxi;
	}

	public BigDecimal getYingjifx() {
		return yingjifx;
	}

	public void setYingjifx(BigDecimal yingjifx) {
		this.yingjifx = yingjifx;
	}

	public BigDecimal getFuxiiiii() {
		return fuxiiiii;
	}

	public void setFuxiiiii(BigDecimal fuxiiiii) {
		this.fuxiiiii = fuxiiiii;
	}

	@Override
	public String toString() {
		return "Service{" +
				"erorcd='" + erorcd + '\'' +
				", erortx='" + erortx + '\'' +
				", servsq='" + servsq + '\'' +
				", datasq='" + datasq + '\'' +
				", dkzhangh='" + dkzhangh + '\'' +
				", dkjiejuh='" + dkjiejuh + '\'' +
				", hetongbh='" + hetongbh + '\'' +
				", kehuhaoo='" + kehuhaoo + '\'' +
				", kehumnch='" + kehumnch + '\'' +
				", yngyjigo='" + yngyjigo + '\'' +
				", zhngjigo='" + zhngjigo + '\'' +
				", chanpdma='" + chanpdma + '\'' +
				", kuaijilb='" + kuaijilb + '\'' +
				", chanpmch='" + chanpmch + '\'' +
				", kaihriqi='" + kaihriqi + '\'' +
				", qixiriqi='" + qixiriqi + '\'' +
				", daoqriqi='" + daoqriqi + '\'' +
				", dkqixian='" + dkqixian + '\'' +
				", daikxtai='" + daikxtai + '\'' +
				", yjfyjzht='" + yjfyjzht + '\'' +
				", dkzhhzht='" + dkzhhzht + '\'' +
				", huobdhao='" + huobdhao + '\'' +
				", jiejuuje=" + jiejuuje +
				", zhchbjin=" + zhchbjin +
				", yuqibjin=" + yuqibjin +
				", dzhibjin=" + dzhibjin +
				", daizbjin=" + daizbjin +
				", dkuanjij=" + dkuanjij +
				", ysyjlixi=" + ysyjlixi +
				", csyjlixi=" + csyjlixi +
				", ysqianxi=" + ysqianxi +
				", csqianxi=" + csqianxi +
				", ysyjfaxi=" + ysyjfaxi +
				", csyjfaxi=" + csyjfaxi +
				", yshofaxi=" + yshofaxi +
				", cshofaxi=" + cshofaxi +
				", yingjifx=" + yingjifx +
				", fuxiiiii=" + fuxiiiii +
				'}';
	}
}
