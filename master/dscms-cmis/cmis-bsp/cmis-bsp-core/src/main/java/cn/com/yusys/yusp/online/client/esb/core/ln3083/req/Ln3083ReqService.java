package cn.com.yusys.yusp.online.client.esb.core.ln3083.req;

/**
 * 请求Service：银团协议查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3083ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
