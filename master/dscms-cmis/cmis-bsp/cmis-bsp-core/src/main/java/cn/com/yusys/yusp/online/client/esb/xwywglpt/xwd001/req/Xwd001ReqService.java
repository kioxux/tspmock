package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.req;

/**
 * @类名 Xwd001ReqService
 * @描述 请求Service：新微贷贷款申请
 * @作者 黄勃
 * @时间 2021/10/26 19:33
 **/
public class Xwd001ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
