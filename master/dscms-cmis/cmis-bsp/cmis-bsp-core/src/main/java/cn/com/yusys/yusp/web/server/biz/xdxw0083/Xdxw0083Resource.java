package cn.com.yusys.yusp.web.server.biz.xdxw0083;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0083.req.Xdxw0083ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0083.resp.Xdxw0083RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0083.req.Xdxw0083DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0083.resp.Xdxw0083DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷还款账号变更
 *
 * @author zr
 * @version 1.0
 */
@Api(tags = "XDXW0083:优企贷还款账号变更")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0083Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0083Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0083
     * 交易描述：查询上一笔抵押贷款的余额接口（增享贷）
     *
     * @param xdxw0083ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷还款账号变更")
    @PostMapping("/xdxw0083")
    @Idempotent({"xdxw0083", "#xdxw0083ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0083RespDto xdxw0083(@Validated @RequestBody Xdxw0083ReqDto xdxw0083ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0082.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083ReqDto));
        Xdxw0083DataReqDto xdxw0083DataReqDto = new Xdxw0083DataReqDto();// 请求Data： 勘验任务状态同步
        Xdxw0083DataRespDto xdxw0083DataRespDto = new Xdxw0083DataRespDto();// 响应Data：勘验任务状态同步
		Xdxw0083RespDto xdxw0083RespDto = new Xdxw0083RespDto();
        // 此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0083.req.Data reqData = null; // 请求Data：勘验任务状态同步
		cn.com.yusys.yusp.dto.server.biz.xdxw0083.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0083.resp.Data();// 响应Data：勘验任务状态同步
        // 此处包导入待确定 结束
        try {
            // 从 xdxw0083ReqDto获取 reqData
            reqData = xdxw0083ReqDto.getData();
            // 将 reqData 拷贝到xdxw0083DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0083DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataReqDto));
            ResultDto<Xdxw0083DataRespDto> xdxw0083DataResultDto = dscmsBizXwClientService.xdxw0083(xdxw0083DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0083RespDto.setErorcd(Optional.ofNullable(xdxw0083DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0083RespDto.setErortx(Optional.ofNullable(xdxw0083DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0083DataResultDto.getCode())) {
                xdxw0083DataRespDto = xdxw0083DataResultDto.getData();
                xdxw0083RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0083RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0083DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0083DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, e.getMessage());
            xdxw0083RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0083RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0083RespDto.setDatasq(servsq);

        xdxw0083RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0083.key, DscmsEnum.TRADE_CODE_XDXW0083.value, JSON.toJSONString(xdxw0083RespDto));
        return xdxw0083RespDto;
    }
}
