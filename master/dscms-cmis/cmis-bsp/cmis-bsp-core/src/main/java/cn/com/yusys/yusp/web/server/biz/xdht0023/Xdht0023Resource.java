package cn.com.yusys.yusp.web.server.biz.xdht0023;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0023.req.Xdht0023ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0023.resp.Xdht0023RespDto;
import cn.com.yusys.yusp.dto.server.xdht0023.req.Xdht0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0023.resp.Xdht0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷系统信贷合同生成接口
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0023:信贷系统信贷合同生成接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0023Resource.class);
	@Resource
	private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0023
     * 交易描述：信贷系统信贷合同生成接口
     *
     * @param xdht0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷系统信贷合同生成接口")
    @PostMapping("/xdht0023")
    //@Idempotent({"xdcaht0023", "#xdht0023ReqDto.datasq"})
    protected @ResponseBody
	Xdht0023RespDto xdht0023(@Validated @RequestBody Xdht0023ReqDto xdht0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, JSON.toJSONString(xdht0023ReqDto));
        Xdht0023DataReqDto xdht0023DataReqDto = new Xdht0023DataReqDto();// 请求Data： 信贷系统信贷合同生成接口
        Xdht0023DataRespDto xdht0023DataRespDto = new Xdht0023DataRespDto();// 响应Data：信贷系统信贷合同生成接口
		Xdht0023RespDto xdht0023RespDto = new Xdht0023RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdht0023.req.Data reqData = null; // 请求Data：信贷系统信贷合同生成接口
		cn.com.yusys.yusp.dto.server.biz.xdht0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0023.resp.Data();// 响应Data：信贷系统信贷合同生成接口
        //  此处包导入待确定 结束
        try {
            // 从 xdht0023ReqDto获取 reqData
            reqData = xdht0023ReqDto.getData();
            // 将 reqData 拷贝到xdht0023DataReqDto
            BeanUtils.copyProperties(reqData, xdht0023DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, JSON.toJSONString(xdht0023DataReqDto));
            ResultDto<Xdht0023DataRespDto> xdht0023DataResultDto = dscmsBizHtClientService.xdht0023(xdht0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, JSON.toJSONString(xdht0023DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0023RespDto.setErorcd(Optional.ofNullable(xdht0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0023RespDto.setErortx(Optional.ofNullable(xdht0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0023DataResultDto.getCode())) {
                xdht0023DataRespDto = xdht0023DataResultDto.getData();
                xdht0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0023DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, e.getMessage());
            xdht0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0023RespDto.setDatasq(servsq);

        xdht0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0023.key, DscmsEnum.TRADE_CODE_XDHT0023.value, JSON.toJSONString(xdht0023RespDto));
        return xdht0023RespDto;
    }
}
