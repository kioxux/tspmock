package cn.com.yusys.yusp.online.client.esb.core.dp2098.resp;

import java.util.List;

public class Lstacctinfo {
    private String kehuhaoo;//客户号
    private Integer zongbshu;//总笔数
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.dp2098.resp.Record> record;

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstacctinfo{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                ", zongbshu=" + zongbshu +
                ", record=" + record +
                '}';
    }
}
