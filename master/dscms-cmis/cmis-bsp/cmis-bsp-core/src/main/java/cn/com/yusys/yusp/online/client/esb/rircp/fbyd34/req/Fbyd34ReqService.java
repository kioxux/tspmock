package cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.req;

/**
 * 请求Service：支用模型校验
 *
 * @author lihh
 * @version 1.0
 */
public class Fbyd34ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
