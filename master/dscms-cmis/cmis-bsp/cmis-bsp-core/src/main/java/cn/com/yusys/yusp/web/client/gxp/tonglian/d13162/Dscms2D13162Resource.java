package cn.com.yusys.yusp.web.client.gxp.tonglian.d13162;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.req.D13162ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.resp.D13162RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp.D13162RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d13162)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D13162Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D13162Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 分期审核（处理码d13162）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d13162:分期审核")
    @PostMapping("/d13162")
    protected @ResponseBody
    ResultDto<D13162RespDto> d13162(@Validated @RequestBody D13162ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13162.key, GxpEnum.TRADE_CODE_D13162.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.D13162ReqMessage d13162ReqMessage = null;//请求Message：分期审核
        D13162RespMessage d13162RespMessage = new D13162RespMessage();//响应Message：分期审核
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Message reqMessage = null;//请求Message：分期审核
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp.Message();//响应Message：分期审核
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：分期审核
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：分期审核
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Body();//请求Body：分期审核
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp.Body();//响应Body：分期审核

        D13162RespDto d13162RespDto = new D13162RespDto();//响应Dto：分期审核
        ResultDto<D13162RespDto> d13162ResultDto = new ResultDto<>();//响应ResultDto：分期审核

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D13162.key, GxpEnum.TRADE_CODE_D13162.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D13162ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Message::setBody, reqBody).build();
            // 给d13162ReqMessage赋值
            d13162ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.D13162ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.D13162ReqMessage::setMessage, reqMessage).build();

            // 将d13162ReqService转换成d13162ReqServiceMap
            Map d13162ReqMessageMap = beanMapUtil.beanToMap(d13162ReqMessage);
            context.put("tradeDataMap", d13162ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13162.key, GxpEnum.TRADE_CODE_D13162.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D13162.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13162.key, GxpEnum.TRADE_CODE_D13162.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d13162RespMessage = beanMapUtil.mapToBean(tradeDataMap, D13162RespMessage.class, D13162RespMessage.class);//响应Message：分期审核
            respMessage = d13162RespMessage.getMessage();//响应Message：分期审核
            gxpRespHead = respMessage.getHead();//响应Head：分期审核
            //  将D13162RespDto封装到ResultDto<D13162RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                respBody = respMessage.getBody();
                //  将respBody转换成D13162RespDto
                BeanUtils.copyProperties(respBody, d13162RespDto);
                d13162ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d13162ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d13162ResultDto.setCode(EpbEnum.EPB099999.key);
                d13162ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d13162ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13162.key, GxpEnum.TRADE_CODE_D13162.value, e.getMessage());
            d13162ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d13162ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d13162ResultDto.setData(d13162RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13162.key, GxpEnum.TRADE_CODE_D13162.value, JSON.toJSONString(d13162ResultDto));
        return d13162ResultDto;
    }
}
