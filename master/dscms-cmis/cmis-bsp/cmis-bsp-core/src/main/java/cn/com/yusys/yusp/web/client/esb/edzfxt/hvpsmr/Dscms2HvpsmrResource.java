package cn.com.yusys.yusp.web.client.esb.edzfxt.hvpsmr;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.req.HvpsmrReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.resp.HvpsmrRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.req.HvpsmrReqService;
import cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.resp.HvpsmrRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用二代支付系统的接口处理类（hvpsmr）")
@RestController
@RequestMapping("/api/dscms2edzfxt")
public class Dscms2HvpsmrResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2HvpsmrResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：hvpsmr
     * 交易描述：大额往帐一体化
     *
     * @param hvpsmrReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/hvpsmr")
    protected @ResponseBody
    ResultDto<HvpsmrRespDto> hvpsmr(@Validated @RequestBody HvpsmrReqDto hvpsmrReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPSMR.key, EsbEnum.TRADE_CODE_HVPSMR.value, JSON.toJSONString(hvpsmrReqDto));
        cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.req.Service();
        cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.resp.Service();
        HvpsmrReqService hvpsmrReqService = new HvpsmrReqService();
        HvpsmrRespService hvpsmrRespService = new HvpsmrRespService();
        HvpsmrRespDto hvpsmrRespDto = new HvpsmrRespDto();
        ResultDto<HvpsmrRespDto> hvpsmrResultDto = new ResultDto<HvpsmrRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将hvpsmrReqDto转换成reqService
            BeanUtils.copyProperties(hvpsmrReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_HVPSMR.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_ZFP.key);//    柜员号
            reqService.setBrchno(hvpsmrReqDto.getBrchno());//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq); // 全局流水号

            hvpsmrReqService.setService(reqService);
            // 将hvpsmrReqService转换成hvpsmrReqServiceMap
            Map hvpsmrReqServiceMap = beanMapUtil.beanToMap(hvpsmrReqService);
            context.put("tradeDataMap", hvpsmrReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPSMR.key, EsbEnum.TRADE_CODE_HVPSMR.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_HVPSMR.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPSMR.key, EsbEnum.TRADE_CODE_HVPSMR.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            hvpsmrRespService = beanMapUtil.mapToBean(tradeDataMap, HvpsmrRespService.class, HvpsmrRespService.class);
            respService = hvpsmrRespService.getService();

            hvpsmrResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            hvpsmrResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成HvpsmrRespDto
                BeanUtils.copyProperties(respService, hvpsmrRespDto);
                hvpsmrResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                hvpsmrResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                hvpsmrResultDto.setCode(EpbEnum.EPB099999.key);
                hvpsmrResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPSMR.key, EsbEnum.TRADE_CODE_HVPSMR.value, e.getMessage());
            hvpsmrResultDto.setCode(EpbEnum.EPB099999.key);//9999
            hvpsmrResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        hvpsmrResultDto.setData(hvpsmrRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HVPSMR.key, EsbEnum.TRADE_CODE_HVPSMR.value, JSON.toJSONString(hvpsmrResultDto));
        return hvpsmrResultDto;
    }
}
