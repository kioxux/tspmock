package cn.com.yusys.yusp.web.server.biz.xdxw0051;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0051.req.Xdxw0051ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0051.resp.Xdxw0051RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0051.req.Xdxw0051DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0051.resp.Xdxw0051DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据业务唯一编号查询无还本续贷贷销售收入
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0051:根据业务唯一编号查询无还本续贷贷销售收入")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0051Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0051Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0051
     * 交易描述：根据业务唯一编号查询无还本续贷贷销售收入
     *
     * @param xdxw0051ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据业务唯一编号查询无还本续贷贷销售收入")
    @PostMapping("/xdxw0051")
    //@Idempotent({"xdcaxw0051", "#xdxw0051ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0051RespDto xdxw0051(@Validated @RequestBody Xdxw0051ReqDto xdxw0051ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, JSON.toJSONString(xdxw0051ReqDto));
        Xdxw0051DataReqDto xdxw0051DataReqDto = new Xdxw0051DataReqDto();// 请求Data： 根据业务唯一编号查询无还本续贷贷销售收入
        Xdxw0051DataRespDto xdxw0051DataRespDto = new Xdxw0051DataRespDto();// 响应Data：根据业务唯一编号查询无还本续贷贷销售收入
        Xdxw0051RespDto xdxw0051RespDto = new Xdxw0051RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0051.req.Data reqData = null; // 请求Data：根据业务唯一编号查询无还本续贷贷销售收入
        cn.com.yusys.yusp.dto.server.biz.xdxw0051.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0051.resp.Data();// 响应Data：根据业务唯一编号查询无还本续贷贷销售收入
        try {
            // 从 xdxw0051ReqDto获取 reqData
            reqData = xdxw0051ReqDto.getData();
            // 将 reqData 拷贝到xdxw0051DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0051DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, JSON.toJSONString(xdxw0051DataReqDto));
            ResultDto<Xdxw0051DataRespDto> xdxw0051DataResultDto = dscmsBizXwClientService.xdxw0051(xdxw0051DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, JSON.toJSONString(xdxw0051DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0051RespDto.setErorcd(Optional.ofNullable(xdxw0051DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0051RespDto.setErortx(Optional.ofNullable(xdxw0051DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0051DataResultDto.getCode())) {
                xdxw0051DataRespDto = xdxw0051DataResultDto.getData();
                xdxw0051RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0051RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0051DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0051DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, e.getMessage());
            xdxw0051RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0051RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0051RespDto.setDatasq(servsq);

        xdxw0051RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0051.key, DscmsEnum.TRADE_CODE_XDXW0051.value, JSON.toJSONString(xdxw0051RespDto));
        return xdxw0051RespDto;
    }
}
