package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.req;

/**
 * 请求Service：承兑签发审批结果综合服务接口
 */
public class Service {
	private String prcscd;//    交易码
	private String servtp;//    渠道
	private String servsq;//    渠道流水
	private String userid;//    柜员号
	private String brchno;//    部门号
	private String txdate;//    交易日期
	private String txtime;//    交易时间
	private String datasq;//    全局流水

	private String pcSerno;//批次号
	private String handSecurityAmo;//已交保证金金额
	private String accountStatus;//出账状态
	private String isdraftpool;//票据池出票标记

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getTxdate() {
		return txdate;
	}

	public void setTxdate(String txdate) {
		this.txdate = txdate;
	}

	public String getTxtime() {
		return txtime;
	}

	public void setTxtime(String txtime) {
		this.txtime = txtime;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getPcSerno() {
		return pcSerno;
	}

	public void setPcSerno(String pcSerno) {
		this.pcSerno = pcSerno;
	}

	public String getHandSecurityAmo() {
		return handSecurityAmo;
	}

	public void setHandSecurityAmo(String handSecurityAmo) {
		this.handSecurityAmo = handSecurityAmo;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getIsdraftpool() {
		return isdraftpool;
	}

	public void setIsdraftpool(String isdraftpool) {
		this.isdraftpool = isdraftpool;
	}

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", txdate='" + txdate + '\'' +
				", txtime='" + txtime + '\'' +
				", datasq='" + datasq + '\'' +
				", pcSerno='" + pcSerno + '\'' +
				", handSecurityAmo='" + handSecurityAmo + '\'' +
				", accountStatus='" + accountStatus + '\'' +
				", isdraftpool='" + isdraftpool + '\'' +
				'}';
	}
}
