package cn.com.yusys.yusp.online.client.esb.core.ln3169.resp;

import java.math.BigDecimal;

/**
 * 响应Service：资产证券化处理文件导入
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String jiaoyirq;//交易日期
    private String zichculi;//资产处理类型
    private String jiaoyils;//交易流水
    private String lstzrjj;//借据列表[LIST]
    private String jiejuhao;//借据号
    private BigDecimal zczrjine;//资产转让金额
    private BigDecimal sddjjine;//收到对价金额
    private BigDecimal zfdjjine;//支付对价金额
    private String shfkztai;//收付款状态
    private String zjjjclzt;//资产借据处理状态

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getZichculi() {
        return zichculi;
    }

    public void setZichculi(String zichculi) {
        this.zichculi = zichculi;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getLstzrjj() {
        return lstzrjj;
    }

    public void setLstzrjj(String lstzrjj) {
        this.lstzrjj = lstzrjj;
    }

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public BigDecimal getZczrjine() {
        return zczrjine;
    }

    public void setZczrjine(BigDecimal zczrjine) {
        this.zczrjine = zczrjine;
    }

    public BigDecimal getSddjjine() {
        return sddjjine;
    }

    public void setSddjjine(BigDecimal sddjjine) {
        this.sddjjine = sddjjine;
    }

    public BigDecimal getZfdjjine() {
        return zfdjjine;
    }

    public void setZfdjjine(BigDecimal zfdjjine) {
        this.zfdjjine = zfdjjine;
    }

    public String getShfkztai() {
        return shfkztai;
    }

    public void setShfkztai(String shfkztai) {
        this.shfkztai = shfkztai;
    }

    public String getZjjjclzt() {
        return zjjjclzt;
    }

    public void setZjjjclzt(String zjjjclzt) {
        this.zjjjclzt = zjjjclzt;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", zichculi='" + zichculi + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", lstzrjj='" + lstzrjj + '\'' +
                ", jiejuhao='" + jiejuhao + '\'' +
                ", zczrjine=" + zczrjine +
                ", sddjjine=" + sddjjine +
                ", zfdjjine=" + zfdjjine +
                ", shfkztai='" + shfkztai + '\'' +
                ", zjjjclzt='" + zjjjclzt + '\'' +
                '}';
    }
}
