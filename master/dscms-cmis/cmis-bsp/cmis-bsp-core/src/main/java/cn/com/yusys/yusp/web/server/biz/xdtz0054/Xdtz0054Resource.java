package cn.com.yusys.yusp.web.server.biz.xdtz0054;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0054.req.Xdtz0054ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0054.resp.Xdtz0054RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.req.Xdtz0054DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0054.resp.Xdtz0054DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:将需要修改的受托支付账号生成修改记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0054:将需要修改的受托支付账号生成修改记录")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0054Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0054Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0054
     * 交易描述：将需要修改的受托支付账号生成修改记录
     *
     * @param xdtz0054ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("将需要修改的受托支付账号生成修改记录")
    @PostMapping("/xdtz0054")
    //@Idempotent({"xdcatz0054", "#xdtz0054ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0054RespDto xdtz0054(@Validated @RequestBody Xdtz0054ReqDto xdtz0054ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054ReqDto));
        Xdtz0054DataReqDto xdtz0054DataReqDto = new Xdtz0054DataReqDto();// 请求Data： 将需要修改的受托支付账号生成修改记录
        Xdtz0054DataRespDto xdtz0054DataRespDto = new Xdtz0054DataRespDto();// 响应Data：将需要修改的受托支付账号生成修改记录
        Xdtz0054RespDto xdtz0054RespDto = new Xdtz0054RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0054.req.Data reqData = null; // 请求Data：将需要修改的受托支付账号生成修改记录
        cn.com.yusys.yusp.dto.server.biz.xdtz0054.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0054.resp.Data();// 响应Data：将需要修改的受托支付账号生成修改记录
        try {
            // 从 xdtz0054ReqDto获取 reqData
            reqData = xdtz0054ReqDto.getData();
            // 将 reqData 拷贝到xdtz0054DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0054DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataReqDto));
            ResultDto<Xdtz0054DataRespDto> xdtz0054DataResultDto = dscmsBizTzClientService.xdtz0054(xdtz0054DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0054RespDto.setErorcd(Optional.ofNullable(xdtz0054DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0054RespDto.setErortx(Optional.ofNullable(xdtz0054DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0054DataResultDto.getCode())) {
                xdtz0054DataRespDto = xdtz0054DataResultDto.getData();
                xdtz0054RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0054RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0054DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0054DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, e.getMessage());
            xdtz0054RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0054RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0054RespDto.setDatasq(servsq);

        xdtz0054RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0054.value, JSON.toJSONString(xdtz0054RespDto));
        return xdtz0054RespDto;
    }
}
