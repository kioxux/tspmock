package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj22;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.req.Xdpj22ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.Xdpj22RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.req.Xdpj22ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.Xdpj22RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:根据批次号查询票号和票面金额
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj22Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj22Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdpj22
     * 交易描述：根据批次号查询票号和票面金额
     *
     * @param xdpj22ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdpj22")
    protected @ResponseBody
    ResultDto<Xdpj22RespDto> xdpj22(@Validated @RequestBody Xdpj22ReqDto xdpj22ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ22.key, EsbEnum.TRADE_CODE_XDPJ22.value, JSON.toJSONString(xdpj22ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.req.Service();
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.Service();
        Xdpj22ReqService xdpj22ReqService = new Xdpj22ReqService();
        Xdpj22RespService xdpj22RespService = new Xdpj22RespService();
        Xdpj22RespDto xdpj22RespDto = new Xdpj22RespDto();
        ResultDto<Xdpj22RespDto> xdpj22ResultDto = new ResultDto<Xdpj22RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdpj22ReqDto转换成reqService
            BeanUtils.copyProperties(xdpj22ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ22.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            xdpj22ReqService.setService(reqService);
            // 将xdpj22ReqService转换成xdpj22ReqServiceMap
            Map xdpj22ReqServiceMap = beanMapUtil.beanToMap(xdpj22ReqService);
            context.put("tradeDataMap", xdpj22ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ22.key, EsbEnum.TRADE_CODE_XDPJ22.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ22.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ22.key, EsbEnum.TRADE_CODE_XDPJ22.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdpj22RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj22RespService.class, Xdpj22RespService.class);
            respService = xdpj22RespService.getService();

            xdpj22ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xdpj22ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xdpj22RespDto
                BeanUtils.copyProperties(respService, xdpj22RespDto);
                cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.List serviceList = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.List());
                java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.Record> records = serviceList.getRecord();
                if (CollectionUtils.nonEmpty(records)) {
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List> respList = records.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List temp = new cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    xdpj22RespDto.setList(respList);
                }
                xdpj22ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdpj22ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdpj22ResultDto.setCode(EpbEnum.EPB099999.key);
                xdpj22ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ22.key, EsbEnum.TRADE_CODE_XDPJ22.value, e.getMessage());
            xdpj22ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdpj22ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdpj22ResultDto.setData(xdpj22RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ22.key, EsbEnum.TRADE_CODE_XDPJ22.value, JSON.toJSONString(xdpj22ResultDto));
        return xdpj22ResultDto;
    }
}
