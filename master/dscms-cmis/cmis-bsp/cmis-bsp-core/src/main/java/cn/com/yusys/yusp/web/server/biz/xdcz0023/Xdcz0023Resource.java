package cn.com.yusys.yusp.web.server.biz.xdcz0023;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0023.req.Xdcz0023ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0023.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0023.resp.Xdcz0023RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0023.req.Xdcz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0023.resp.Xdcz0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小企业无还本续贷放款
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0023:小企业无还本续贷放款")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0023Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0023
     * 交易描述：小企业无还本续贷放款
     *
     * @param xdcz0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小企业无还本续贷放款")
    @PostMapping("/xdcz0023")
   //@Idempotent({"xdcz0023", "#xdcz0023ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0023RespDto xdcz0023(@Validated @RequestBody Xdcz0023ReqDto xdcz0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023ReqDto));
        Xdcz0023DataReqDto xdcz0023DataReqDto = new Xdcz0023DataReqDto();// 请求Data： 小企业无还本续贷放款
        Xdcz0023DataRespDto xdcz0023DataRespDto = new Xdcz0023DataRespDto();// 响应Data：小企业无还本续贷放款

        cn.com.yusys.yusp.dto.server.biz.xdcz0023.req.Data reqData = null; // 请求Data：小企业无还本续贷放款
        Data respData = new Data();// 响应Data：小企业无还本续贷放款

        Xdcz0023RespDto xdcz0023RespDto = new Xdcz0023RespDto();
        try {
            // 从 xdcz0023ReqDto获取 reqData
            reqData = xdcz0023ReqDto.getData();
            // 将 reqData 拷贝到xdcz0023DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0023DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataReqDto));
            ResultDto<Xdcz0023DataRespDto> xdcz0023DataResultDto = dscmsBizCzClientService.xdcz0023(xdcz0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0023RespDto.setErorcd(Optional.ofNullable(xdcz0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0023RespDto.setErortx(Optional.ofNullable(xdcz0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0023DataResultDto.getCode())) {
                xdcz0023DataRespDto = xdcz0023DataResultDto.getData();
                xdcz0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0023DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, e.getMessage());
            xdcz0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0023RespDto.setDatasq(servsq);

        xdcz0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0023.key, DscmsEnum.TRADE_CODE_XDCZ0023.value, JSON.toJSONString(xdcz0023RespDto));
        return xdcz0023RespDto;
    }
}
