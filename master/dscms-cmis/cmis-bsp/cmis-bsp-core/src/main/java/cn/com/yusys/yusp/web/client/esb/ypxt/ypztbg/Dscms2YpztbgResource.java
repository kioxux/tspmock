package cn.com.yusys.yusp.web.client.esb.ypxt.ypztbg;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg.YpztbgReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg.YpztbgRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.req.YpztbgReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.resp.YpztbgRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口（ypztbg）")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2YpztbgResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2YpztbgResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 押品状态变更（处理码ypztbg）
     *
     * @param ypztbgReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ypztbg:押品状态变更")
    @PostMapping("/ypztbg")
    protected @ResponseBody
    ResultDto<YpztbgRespDto> ypztbg(@Validated @RequestBody YpztbgReqDto ypztbgReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTBG.key, EsbEnum.TRADE_CODE_YPZTBG.value, JSON.toJSONString(ypztbgReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.resp.Service();
        YpztbgReqService ypztbgReqService = new YpztbgReqService();
        YpztbgRespService ypztbgRespService = new YpztbgRespService();
        YpztbgRespDto ypztbgRespDto = new YpztbgRespDto();
        ResultDto<YpztbgRespDto> ypztbgResultDto = new ResultDto<YpztbgRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将YpztbgReqDto转换成reqService
            BeanUtils.copyProperties(ypztbgReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_YPZTBG.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(EsbEnum.BRCHNO_YPXT.key);//    全局流水

            ypztbgReqService.setService(reqService);
            // 将ypztbgReqService转换成ypztbgReqServiceMap
            Map ypztbgReqServiceMap = beanMapUtil.beanToMap(ypztbgReqService);
            context.put("tradeDataMap", ypztbgReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTBG.key, EsbEnum.TRADE_CODE_YPZTBG.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_YPZTBG.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTBG.key, EsbEnum.TRADE_CODE_YPZTBG.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ypztbgRespService = beanMapUtil.mapToBean(tradeDataMap, YpztbgRespService.class, YpztbgRespService.class);
            respService = ypztbgRespService.getService();

            //  将YpztbgRespDto封装到ResultDto<YpztbgRespDto>
            ypztbgResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ypztbgResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成YpztbgRespDto
                BeanUtils.copyProperties(respService, ypztbgRespDto);
                ypztbgResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ypztbgResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ypztbgResultDto.setCode(EpbEnum.EPB099999.key);
                ypztbgResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTBG.key, EsbEnum.TRADE_CODE_YPZTBG.value, e.getMessage());
            ypztbgResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ypztbgResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ypztbgResultDto.setData(ypztbgRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YPZTBG.key, EsbEnum.TRADE_CODE_YPZTBG.value, JSON.toJSONString(ypztbgResultDto));
        return ypztbgResultDto;
    }
}
