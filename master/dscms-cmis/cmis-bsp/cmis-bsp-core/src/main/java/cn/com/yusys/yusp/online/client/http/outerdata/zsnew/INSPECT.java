package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 抽查检查
 */
@JsonPropertyOrder(alphabetic = true)
public class INSPECT implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ISP_DATE")
    private String ISP_DATE;//	抽查日期
    @JsonProperty(value = "ISP_NOTICE")
    private String ISP_NOTICE;//	公告名称
    @JsonProperty(value = "ISP_NOTICEDATE")
    private String ISP_NOTICEDATE;//	公告日期
    @JsonProperty(value = "ISP_REGORG")
    private String ISP_REGORG;//	检查核实机关名称
    @JsonProperty(value = "ISP_REGORGCODE")
    private String ISP_REGORGCODE;//	检查核实机关代码
    @JsonProperty(value = "ISP_RESULT")
    private String ISP_RESULT;//	抽查结果
    @JsonProperty(value = "ISP_TYPE")
    private String ISP_TYPE;//	数据类型

    @JsonIgnore
    public String getISP_DATE() {
        return ISP_DATE;
    }

    @JsonIgnore
    public void setISP_DATE(String ISP_DATE) {
        this.ISP_DATE = ISP_DATE;
    }

    @JsonIgnore
    public String getISP_NOTICE() {
        return ISP_NOTICE;
    }

    @JsonIgnore
    public void setISP_NOTICE(String ISP_NOTICE) {
        this.ISP_NOTICE = ISP_NOTICE;
    }

    @JsonIgnore
    public String getISP_NOTICEDATE() {
        return ISP_NOTICEDATE;
    }

    @JsonIgnore
    public void setISP_NOTICEDATE(String ISP_NOTICEDATE) {
        this.ISP_NOTICEDATE = ISP_NOTICEDATE;
    }

    @JsonIgnore
    public String getISP_REGORG() {
        return ISP_REGORG;
    }

    @JsonIgnore
    public void setISP_REGORG(String ISP_REGORG) {
        this.ISP_REGORG = ISP_REGORG;
    }

    @JsonIgnore
    public String getISP_REGORGCODE() {
        return ISP_REGORGCODE;
    }

    @JsonIgnore
    public void setISP_REGORGCODE(String ISP_REGORGCODE) {
        this.ISP_REGORGCODE = ISP_REGORGCODE;
    }

    @JsonIgnore
    public String getISP_RESULT() {
        return ISP_RESULT;
    }

    @JsonIgnore
    public void setISP_RESULT(String ISP_RESULT) {
        this.ISP_RESULT = ISP_RESULT;
    }

    @JsonIgnore
    public String getISP_TYPE() {
        return ISP_TYPE;
    }

    @JsonIgnore
    public void setISP_TYPE(String ISP_TYPE) {
        this.ISP_TYPE = ISP_TYPE;
    }

    @Override
    public String toString() {
        return "INSPECT{" +
                "ISP_DATE='" + ISP_DATE + '\'' +
                ", ISP_NOTICE='" + ISP_NOTICE + '\'' +
                ", ISP_NOTICEDATE='" + ISP_NOTICEDATE + '\'' +
                ", ISP_REGORG='" + ISP_REGORG + '\'' +
                ", ISP_REGORGCODE='" + ISP_REGORGCODE + '\'' +
                ", ISP_RESULT='" + ISP_RESULT + '\'' +
                ", ISP_TYPE='" + ISP_TYPE + '\'' +
                '}';
    }
}
