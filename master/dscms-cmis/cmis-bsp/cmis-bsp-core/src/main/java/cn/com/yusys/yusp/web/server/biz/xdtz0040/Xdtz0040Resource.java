package cn.com.yusys.yusp.web.server.biz.xdtz0040;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0040.req.Xdtz0040ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0040.resp.Xdtz0040RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0040.req.Xdtz0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0040.resp.Xdtz0040DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:申请人在本行当前逾期贷款数量
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0040:申请人在本行当前逾期贷款数量")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0040Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0040Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0040
     * 交易描述：申请人在本行当前逾期贷款数量
     *
     * @param xdtz0040ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0040:申请人在本行当前逾期贷款数量")
    @PostMapping("/xdtz0040")
    //@Idempotent({"xdcatz0040", "#xdtz0040ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0040RespDto xdtz0040(@Validated @RequestBody Xdtz0040ReqDto xdtz0040ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0040.key, DscmsEnum.TRADE_CODE_XDTZ0040.value, JSON.toJSONString(xdtz0040ReqDto));
        Xdtz0040DataReqDto xdtz0040DataReqDto = new Xdtz0040DataReqDto();// 请求Data： 申请人在本行当前逾期贷款数量
        Xdtz0040DataRespDto xdtz0040DataRespDto = new Xdtz0040DataRespDto();// 响应Data：申请人在本行当前逾期贷款数量
        Xdtz0040RespDto xdtz0040RespDto = new Xdtz0040RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0040.req.Data reqData = null; // 请求Data：申请人在本行当前逾期贷款数量
        cn.com.yusys.yusp.dto.server.biz.xdtz0040.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0040.resp.Data();// 响应Data：申请人在本行当前逾期贷款数量
        try {
            // 从 xdtz0040ReqDto获取 reqData
            reqData = xdtz0040ReqDto.getData();
            // 将 reqData 拷贝到xdtz0040DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0040DataReqDto);
            // 调用服务
            ResultDto<Xdtz0040DataRespDto> xdtz0040DataResultDto = dscmsBizTzClientService.xdtz0040(xdtz0040DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdtz0040RespDto.setErorcd(Optional.ofNullable(xdtz0040DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0040RespDto.setErortx(Optional.ofNullable(xdtz0040DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdtz0040RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0040DataResultDto.getCode())) {
                xdtz0040RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0040RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdtz0040DataRespDto = xdtz0040DataResultDto.getData();
                // 将 xdtz0040DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0040DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0040.key, DscmsEnum.TRADE_CODE_XDTZ0040.value, e.getMessage());
            xdtz0040RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0040RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0040RespDto.setDatasq(servsq);

        xdtz0040RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0040.key, DscmsEnum.TRADE_CODE_XDTZ0040.value, JSON.toJSONString(xdtz0040RespDto));
        return xdtz0040RespDto;
    }
}
