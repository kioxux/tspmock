package cn.com.yusys.yusp.web.server.biz.xdtz0038;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0038.req.Xdtz0038ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0038.resp.Xdtz0038RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0038.req.Xdtz0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0038.resp.Xdtz0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:台账信息通用列表查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0038:台账信息通用列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0038Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0038Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0038
     * 交易描述：台账信息通用列表查询
     *
     * @param xdtz0038ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账信息通用列表查询")
    @PostMapping("/xdtz0038")
//    @Idempotent({"xdcatz0038", "#xdtz0038ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0038RespDto xdtz0038(@Validated @RequestBody Xdtz0038ReqDto xdtz0038ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038ReqDto));
        Xdtz0038DataReqDto xdtz0038DataReqDto = new Xdtz0038DataReqDto();// 请求Data： 台账信息通用列表查询
        Xdtz0038DataRespDto xdtz0038DataRespDto = new Xdtz0038DataRespDto();// 响应Data：台账信息通用列表查询
        Xdtz0038RespDto xdtz0038RespDto = new Xdtz0038RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0038.req.Data reqData = null; // 请求Data：台账信息通用列表查询
        cn.com.yusys.yusp.dto.server.biz.xdtz0038.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0038.resp.Data();// 响应Data：台账信息通用列表查询

        try {
            // 从 xdtz0038ReqDto获取 reqData
            reqData = xdtz0038ReqDto.getData();
            // 将 reqData 拷贝到xdtz0038DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0038DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038DataReqDto));
            ResultDto<Xdtz0038DataRespDto> xdtz0038DataResultDto = dscmsBizTzClientService.xdtz0038(xdtz0038DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0038RespDto.setErorcd(Optional.ofNullable(xdtz0038DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0038RespDto.setErortx(Optional.ofNullable(xdtz0038DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0038DataResultDto.getCode())) {
                xdtz0038DataRespDto = xdtz0038DataResultDto.getData();
                xdtz0038RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0038RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0038DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0038DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, e.getMessage());
            xdtz0038RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0038RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0038RespDto.setDatasq(servsq);

        xdtz0038RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0038.key, DscmsEnum.TRADE_CODE_XDTZ0038.value, JSON.toJSONString(xdtz0038RespDto));
        return xdtz0038RespDto;
    }
}
