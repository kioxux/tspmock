package cn.com.yusys.yusp.online.client.esb.doc.doc000.req;

/**
 * 请求Service：入库接口
 */
public class Service {

    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String waibclma;//外部处理码
    private String ipaddr;//请求方IP
    private String mac;//请求方MAC

    private String doc_id;
    private String inputDate;
    private String inroomtime;

    private String orgId;//机构号
    private String orgName;//机构名称
    private String typeCode;//档案类型；   信贷档案:0100050006
    private String fileCode;//档案代码；2001：放款资料A，2002：放款资料B
    private String fileName;//档案名称
    private String fileDesc;//备注
    private String resourceType;//档案来源；001:会计,002:信贷
    private String type;//交易类型；001:档案登记,002:档案登记撤销
    private String requestType;//默认 100001：信贷档案借口
    private String opeateUserId;//操作员号
    private String opeateDate;//操作日期
    private String fileNum;//档案编号：交易类型为002必传
    private String result;//对收到的信息进行处理的结果的反馈
    //private String indexMap;//抓取服务请求map字符串
    private String is_aj;//是否按揭
    private String is_yd;//是否异地
    private String kfbh;//库房（本地A 异地B 村镇C）
    private String real_mng_br_id;//真实的机构号
    private List_index list_index;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getWaibclma() {
        return waibclma;
    }

    public void setWaibclma(String waibclma) {
        this.waibclma = waibclma;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getInroomtime() {
        return inroomtime;
    }

    public void setInroomtime(String inroomtime) {
        this.inroomtime = inroomtime;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getFileCode() {
        return fileCode;
    }

    public void setFileCode(String fileCode) {
        this.fileCode = fileCode;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getOpeateUserId() {
        return opeateUserId;
    }

    public void setOpeateUserId(String opeateUserId) {
        this.opeateUserId = opeateUserId;
    }

    public String getOpeateDate() {
        return opeateDate;
    }

    public void setOpeateDate(String opeateDate) {
        this.opeateDate = opeateDate;
    }

    public String getFileNum() {
        return fileNum;
    }

    public void setFileNum(String fileNum) {
        this.fileNum = fileNum;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getIs_aj() {
        return is_aj;
    }

    public void setIs_aj(String is_aj) {
        this.is_aj = is_aj;
    }

    public String getIs_yd() {
        return is_yd;
    }

    public void setIs_yd(String is_yd) {
        this.is_yd = is_yd;
    }

    public String getKfbh() {
        return kfbh;
    }

    public void setKfbh(String kfbh) {
        this.kfbh = kfbh;
    }

    public String getReal_mng_br_id() {
        return real_mng_br_id;
    }

    public void setReal_mng_br_id(String real_mng_br_id) {
        this.real_mng_br_id = real_mng_br_id;
    }

    public List_index getList_index() {
        return list_index;
    }

    public void setList_index(List_index list_index) {
        this.list_index = list_index;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", waibclma='" + waibclma + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", mac='" + mac + '\'' +
                ", doc_id='" + doc_id + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", inroomtime='" + inroomtime + '\'' +
                ", orgId='" + orgId + '\'' +
                ", orgName='" + orgName + '\'' +
                ", typeCode='" + typeCode + '\'' +
                ", fileCode='" + fileCode + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileDesc='" + fileDesc + '\'' +
                ", resourceType='" + resourceType + '\'' +
                ", type='" + type + '\'' +
                ", requestType='" + requestType + '\'' +
                ", opeateUserId='" + opeateUserId + '\'' +
                ", opeateDate='" + opeateDate + '\'' +
                ", fileNum='" + fileNum + '\'' +
                ", result='" + result + '\'' +
                ", is_aj='" + is_aj + '\'' +
                ", is_yd='" + is_yd + '\'' +
                ", kfbh='" + kfbh + '\'' +
                ", real_mng_br_id='" + real_mng_br_id + '\'' +
                ", list_index=" + list_index +
                '}';
    }
}
