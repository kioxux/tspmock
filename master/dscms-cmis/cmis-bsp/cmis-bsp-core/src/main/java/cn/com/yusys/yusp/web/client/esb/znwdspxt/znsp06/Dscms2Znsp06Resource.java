package cn.com.yusys.yusp.web.client.esb.znwdspxt.znsp06;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.req.Znsp06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.resp.Znsp06RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp06.req.Znsp06ReqService;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp06.resp.Znsp06RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用智能微贷审批系统的接口处理类
 **/
@Api(tags = "BSP封装调用智能微贷审批系统的接口处理类()")
@RestController
@RequestMapping("/api/dscms2znwdspxt")
public class Dscms2Znsp06Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Znsp06Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：znsp06
     * 交易描述：被拒绝的线上产品推送接口
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("znsp06:被拒绝的线上产品推送接口")
    @PostMapping("/znsp06")
    protected @ResponseBody
    ResultDto<Znsp06RespDto> znsp06(@Validated @RequestBody Znsp06ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP06.key, EsbEnum.TRADE_CODE_ZNSP06.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp06.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp06.req.Service();
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp06.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp06.resp.Service();
        Znsp06ReqService znsp06ReqService = new Znsp06ReqService();
        Znsp06RespService znsp06RespService = new Znsp06RespService();
        Znsp06RespDto znsp06RespDto = new Znsp06RespDto();
        ResultDto<Znsp06RespDto> znsp06ResultDto = new ResultDto<Znsp06RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将znsp06ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_ZNSP06.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_ZNWDSPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ZNWDSPXT.key);//    部门号
            znsp06ReqService.setService(reqService);
            // 将znsp06ReqService转换成znsp06ReqServiceMap
            Map znsp06ReqServiceMap = beanMapUtil.beanToMap(znsp06ReqService);
            context.put("tradeDataMap", znsp06ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP06.key, EsbEnum.TRADE_CODE_ZNSP06.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_ZNSP06.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP06.key, EsbEnum.TRADE_CODE_ZNSP06.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            znsp06RespService = beanMapUtil.mapToBean(tradeDataMap, Znsp06RespService.class, Znsp06RespService.class);
            respService = znsp06RespService.getService();
            //  将respService转换成znsp06RespDto
            BeanUtils.copyProperties(respService, znsp06RespDto);
            znsp06ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            znsp06ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成znsp06RespDto
                BeanUtils.copyProperties(respService, znsp06RespDto);
                znsp06ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                znsp06ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                znsp06ResultDto.setCode(EpbEnum.EPB099999.key);
                znsp06ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP06.key, EsbEnum.TRADE_CODE_ZNSP06.value, e.getMessage());
            znsp06ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            znsp06ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        znsp06ResultDto.setData(znsp06RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP06.key, EsbEnum.TRADE_CODE_ZNSP06.value, JSON.toJSONString(znsp06ResultDto));
        return znsp06ResultDto;
    }
}
