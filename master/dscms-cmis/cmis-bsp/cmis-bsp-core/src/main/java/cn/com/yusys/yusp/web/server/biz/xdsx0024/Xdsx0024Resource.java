package cn.com.yusys.yusp.web.server.biz.xdsx0024;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0024.req.Xdsx0024ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0024.resp.Xdsx0024RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0024.req.Xdsx0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0024.resp.Xdsx0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:省心快贷plus授信，风控自动审批结果推送信贷
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0024:省心快贷plus授信，风控自动审批结果推送信贷")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0024Resource.class);
	@Autowired
	private DscmsBizSxClientService dscmsBizSxClientService;
    /**
     * 交易码：xdsx0024
     * 交易描述：省心快贷plus授信，风控自动审批结果推送信贷
     *
     * @param xdsx0024ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("省心快贷plus授信，风控自动审批结果推送信贷")
    @PostMapping("/xdsx0024")
   // @Idempotent({"xdcasx0024", "#xdsx0024ReqDto.datasq"})
    protected @ResponseBody
	Xdsx0024RespDto xdsx0024(@Validated @RequestBody Xdsx0024ReqDto xdsx0024ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, JSON.toJSONString(xdsx0024ReqDto));
        Xdsx0024DataReqDto xdsx0024DataReqDto = new Xdsx0024DataReqDto();// 请求Data： 省心快贷plus授信，风控自动审批结果推送信贷
        Xdsx0024DataRespDto xdsx0024DataRespDto = new Xdsx0024DataRespDto();// 响应Data：省心快贷plus授信，风控自动审批结果推送信贷
		Xdsx0024RespDto xdsx0024RespDto = new Xdsx0024RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdsx0024.req.Data reqData = null; // 请求Data：省心快贷plus授信，风控自动审批结果推送信贷
		cn.com.yusys.yusp.dto.server.biz.xdsx0024.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0024.resp.Data();// 响应Data：省心快贷plus授信，风控自动审批结果推送信贷
        //  此处包导入待确定 结束
        try {
            // 从 xdsx0024ReqDto获取 reqData
            reqData = xdsx0024ReqDto.getData();
            // 将 reqData 拷贝到xdsx0024DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0024DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, JSON.toJSONString(xdsx0024DataReqDto));
            ResultDto<Xdsx0024DataRespDto> xdsx0024DataResultDto = dscmsBizSxClientService.xdsx0024(xdsx0024DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, JSON.toJSONString(xdsx0024DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdsx0024RespDto.setErorcd(Optional.ofNullable(xdsx0024DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0024RespDto.setErortx(Optional.ofNullable(xdsx0024DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0024DataResultDto.getCode())) {
                xdsx0024DataRespDto = xdsx0024DataResultDto.getData();
                xdsx0024RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0024RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0024DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0024DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0024RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, e.getMessage());
            xdsx0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0024RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0024RespDto.setDatasq(servsq);

        xdsx0024RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0024.key, DscmsEnum.TRADE_CODE_XDSX0024.value, JSON.toJSONString(xdsx0024RespDto));
        return xdsx0024RespDto;
    }
}
