package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj23;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req.Xdpj23ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.Xdpj23RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.req.Xdpj23ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp.Xdpj23RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:票据承兑签发审批请求
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj23Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj23Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdpj23
     * 交易描述：票据承兑签发审批请求
     *
     * @param xdpj23ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdpj23")
    protected @ResponseBody
    ResultDto<Xdpj23RespDto> xdpj23(@Validated @RequestBody Xdpj23ReqDto xdpj23ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ23.key, EsbEnum.TRADE_CODE_XDPJ23.value, JSON.toJSONString(xdpj23ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.req.Service();
		cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp.Service();
        Xdpj23ReqService xdpj23ReqService = new Xdpj23ReqService();
        Xdpj23RespService xdpj23RespService = new Xdpj23RespService();
        Xdpj23RespDto xdpj23RespDto = new Xdpj23RespDto();
        ResultDto<Xdpj23RespDto> xdpj23ResultDto = new ResultDto<Xdpj23RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将xdpj23ReqDto转换成reqService
			BeanUtils.copyProperties(xdpj23ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ23.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
			reqService.setDatasq(servsq);//    全局流水
			xdpj23ReqService.setService(reqService);
			// 将xdpj23ReqService转换成xdpj23ReqServiceMap
			Map xdpj23ReqServiceMap = beanMapUtil.beanToMap(xdpj23ReqService);
			context.put("tradeDataMap", xdpj23ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ23.key, EsbEnum.TRADE_CODE_XDPJ23.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ23.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ23.key, EsbEnum.TRADE_CODE_XDPJ23.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			xdpj23RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj23RespService.class, Xdpj23RespService.class);
			respService = xdpj23RespService.getService();
			//  将respService转换成Xdpj23RespDto
			BeanUtils.copyProperties(respService, xdpj23RespDto);
			xdpj23ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			xdpj23ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成XdypbdccxRespDto
				BeanUtils.copyProperties(respService,xdpj23RespDto);
				cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp.List serviceList = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp.List());
				java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj23.resp.Record> records = serviceList.getRecord();
				if (CollectionUtils.nonEmpty(records)) {
					java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List> respList = records.parallelStream().map(record -> {
						cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List temp = new cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.List();
						BeanUtils.copyProperties(record, temp);
						return temp;
					}).collect(Collectors.toList());
					xdpj23RespDto.setList(respList);
				}
				xdpj23ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				xdpj23ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				xdpj23ResultDto.setCode(EpbEnum.EPB099999.key);
				xdpj23ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ23.key, EsbEnum.TRADE_CODE_XDPJ23.value, e.getMessage());
			xdpj23ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			xdpj23ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		xdpj23ResultDto.setData(xdpj23RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ23.key, EsbEnum.TRADE_CODE_XDPJ23.value, JSON.toJSONString(xdpj23ResultDto));
        return xdpj23ResultDto;
    }
}
