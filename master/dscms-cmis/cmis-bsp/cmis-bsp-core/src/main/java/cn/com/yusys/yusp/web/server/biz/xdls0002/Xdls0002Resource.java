package cn.com.yusys.yusp.web.server.biz.xdls0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdls0002.req.Xdls0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdls0002.resp.Xdls0002RespDto;
import cn.com.yusys.yusp.dto.server.xdls0002.req.Xdls0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0002.resp.Xdls0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizLsClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:市民贷联系人信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDLS0002:市民贷联系人信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdls0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdls0002Resource.class);
    @Autowired
    private DscmsBizLsClientService dscmsBizLsClientService;

    /**
     * 交易码：xdls0002
     * 交易描述：市民贷联系人信息查询
     *
     * @param xdls0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("市民贷联系人信息查询")
    @PostMapping("/xdls0002")
//    @Idempotent({"xdcals0002", "#xdls0002ReqDto.datasq"})
    protected @ResponseBody
    Xdls0002RespDto xdls0002(@Validated @RequestBody Xdls0002ReqDto xdls0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, JSON.toJSONString(xdls0002ReqDto));
        Xdls0002DataReqDto xdls0002DataReqDto = new Xdls0002DataReqDto();// 请求Data： 市民贷联系人信息查询
        Xdls0002DataRespDto xdls0002DataRespDto = new Xdls0002DataRespDto();// 响应Data：市民贷联系人信息查询
        Xdls0002RespDto xdls0002RespDto = new Xdls0002RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdls0002.req.Data reqData = null; // 请求Data：市民贷联系人信息查询
        cn.com.yusys.yusp.dto.server.biz.xdls0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdls0002.resp.Data();// 响应Data：市民贷联系人信息查询
        // 从 xdls0002ReqDto获取 reqData
        reqData = xdls0002ReqDto.getData();
        try {
            // 将 reqData 拷贝到xdls0002DataReqDto
            BeanUtils.copyProperties(reqData, xdls0002DataReqDto);
            // 调用服务：
            ResultDto<Xdls0002DataRespDto> xdls0002DataResultDto = dscmsBizLsClientService.xdls0002(xdls0002DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdls0002RespDto.setErorcd(Optional.ofNullable(xdls0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdls0002RespDto.setErortx(Optional.ofNullable(xdls0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdls0002DataResultDto.getCode())) {
                xdls0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdls0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdls0002DataRespDto = xdls0002DataResultDto.getData();
                // 将 xdls0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdls0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, e.getMessage());
            xdls0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdls0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdls0002RespDto.setDatasq(servsq);

        xdls0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0002.key, DscmsEnum.TRADE_CODE_XDLS0002.value, JSON.toJSONString(xdls0002RespDto));
        return xdls0002RespDto;
    }
}
