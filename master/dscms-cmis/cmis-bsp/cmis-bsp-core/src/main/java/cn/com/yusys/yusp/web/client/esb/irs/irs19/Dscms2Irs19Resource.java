package cn.com.yusys.yusp.web.client.esb.irs.irs19;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.req.Irs19ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.resp.Irs19RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.irs19.req.Irs19ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.irs19.resp.Irs19RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:专业贷款基本信息同步
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Irs19Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Irs19Resource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：irs19
     * 交易描述：专业贷款基本信息同步
     *
     * @param irs19ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/irs19")
    protected @ResponseBody
    ResultDto<Irs19RespDto> irs19(@Validated @RequestBody Irs19ReqDto irs19ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS19.key, EsbEnum.TRADE_CODE_IRS19.value, JSON.toJSONString(irs19ReqDto));
		cn.com.yusys.yusp.online.client.esb.irs.irs19.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.irs19.req.Service();
		cn.com.yusys.yusp.online.client.esb.irs.irs19.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.irs19.resp.Service();
        Irs19ReqService irs19ReqService = new Irs19ReqService();
        Irs19RespService irs19RespService = new Irs19RespService();
        Irs19RespDto irs19RespDto = new Irs19RespDto();
        ResultDto<Irs19RespDto> irs19ResultDto = new ResultDto<>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将irs19ReqDto转换成reqService
			BeanUtils.copyProperties(irs19ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS19.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			irs19ReqService.setService(reqService);
			// 将irs19ReqService转换成irs19ReqServiceMap
			Map irs19ReqServiceMap = beanMapUtil.beanToMap(irs19ReqService);
			context.put("tradeDataMap", irs19ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS19.key, EsbEnum.TRADE_CODE_IRS19.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS19.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS19.key, EsbEnum.TRADE_CODE_IRS19.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			irs19RespService = beanMapUtil.mapToBean(tradeDataMap, Irs19RespService.class, Irs19RespService.class);
			respService = irs19RespService.getService();
			//  将respService转换成Irs19RespDto
			irs19ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			irs19ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Irs21RespDto
				BeanUtils.copyProperties(respService, irs19RespDto);
				irs19ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				irs19ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				irs19ResultDto.setCode(EpbEnum.EPB099999.key);
				irs19ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS19.key, EsbEnum.TRADE_CODE_IRS19.value, e.getMessage());
			irs19ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			irs19ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		irs19ResultDto.setData(irs19RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS19.key, EsbEnum.TRADE_CODE_IRS19.value, JSON.toJSONString(irs19ResultDto));
        return irs19ResultDto;
    }
}
