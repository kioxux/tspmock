package cn.com.yusys.yusp.web.client.http.outerdata.sxbzxr;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.req.SxbzxrReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.SxbzxrRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.outerdata.sxbzxr.req.SxbzxrReqService;
import cn.com.yusys.yusp.web.client.http.outerdata.idcheck.Dscms2IdCheckResource;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;


/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(sxbzxr)")
@RestController
@RequestMapping("/api/dscms2outerdata")
public class Dscms2SxbzxrResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2IdCheckResource.class);
    @Autowired
    private RestTemplate restTemplate;
    @Value("${application.outerdata.url}")
    private String outerdataUrl;

    /**
     * 失信被执行人查询接口
     *
     * @param sxbzxrReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("sxbzxr:失信被执行人查询接口")
    @PostMapping("/sxbzxr")
    protected @ResponseBody
    ResultDto<SxbzxrRespDto> sxbzxr(@Validated @RequestBody SxbzxrReqDto sxbzxrReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value, JSON.toJSONString(sxbzxrReqDto));

        SxbzxrReqService sxbzxrReqService = new SxbzxrReqService();

        SxbzxrRespDto sxbzxrRespDto = new SxbzxrRespDto();
        ResultDto<SxbzxrRespDto> sxbzxrResultDto = new ResultDto<SxbzxrRespDto>();
        try {
            //  将SxbzxrReqDto转换成SxbzxrReqService
            BeanUtils.copyProperties(sxbzxrReqDto, sxbzxrReqService);

            sxbzxrReqService.setPrcscd(EsbEnum.TRADE_CODE_SXBZXR.key);//    交易码
            sxbzxrReqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            sxbzxrReqService.setServsq(servsq);//    渠道流水
            sxbzxrReqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            sxbzxrReqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(sxbzxrReqService), headers);
            //String url = "http://10.28.122.177:7006/edataApi/data";
            String url = outerdataUrl + "/edataApi/data";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value, JSON.toJSONString(sxbzxrReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value, responseEntity.getBody());
            String responseStr = responseEntity.getBody();
            JSONObject jsonObject = JSONObject.parseObject(responseStr);
            String erorcd = jsonObject.get("erorcd").toString();
            String erortx = jsonObject.get("erortx").toString();
            if (Objects.equals(SuccessEnum.SUCCESS.key, erorcd)) {
                sxbzxrResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                sxbzxrResultDto.setMessage(erortx);
                String data = jsonObject.get("data").toString();
                sxbzxrRespDto = JSON.parseObject(data, SxbzxrRespDto.class);
            } else {
                sxbzxrResultDto.setCode(EpbEnum.EPB099999.key);
                sxbzxrResultDto.setMessage(erortx);
            }


        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value, e.getMessage());
            sxbzxrResultDto.setCode(EpbEnum.EPB099999.key);//9999
            sxbzxrResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        sxbzxrResultDto.setData(sxbzxrRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SXBZXR.key, EsbEnum.TRADE_CODE_SXBZXR.value, JSON.toJSONString(sxbzxrResultDto));

        return sxbzxrResultDto;
    }
}
