package cn.com.yusys.yusp.web.client.esb.xwh.xwh001;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.req.Xwh001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.resp.Xwh001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwh.xwh001.req.Xwh001ReqService;
import cn.com.yusys.yusp.online.client.esb.xwh.xwh001.resp.Xwh001RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借据台账信息接收
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装小微公众号平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwh")
public class Dscms2Xwh001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xwh001Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xwh001
     * 交易描述：借据台账信息接收
     *
     * @param xwh001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xwh001:借据台账信息接收")
    @PostMapping("/xwh001")
    protected @ResponseBody
    ResultDto<Xwh001RespDto> xwh001(@Validated @RequestBody Xwh001ReqDto xwh001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value, JSON.toJSONString(xwh001ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwh.xwh001.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwh.xwh001.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwh.xwh001.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwh.xwh001.resp.Service();
        Xwh001ReqService xwh001ReqService = new Xwh001ReqService();
        Xwh001RespService xwh001RespService = new Xwh001RespService();
        Xwh001RespDto xwh001RespDto = new Xwh001RespDto();
        ResultDto<Xwh001RespDto> xwh001ResultDto = new ResultDto<Xwh001RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xwh001ReqDto转换成reqService
            BeanUtils.copyProperties(xwh001ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XWH001.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            xwh001ReqService.setService(reqService);
            // 将xwh001ReqService转换成xwh001ReqServiceMap
            Map xwh001ReqServiceMap = beanMapUtil.beanToMap(xwh001ReqService);
            context.put("tradeDataMap", xwh001ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XWH001.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xwh001RespService = beanMapUtil.mapToBean(tradeDataMap, Xwh001RespService.class, Xwh001RespService.class);
            respService = xwh001RespService.getService();

            xwh001ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xwh001ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, xwh001RespDto);
                xwh001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xwh001ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xwh001ResultDto.setCode(EpbEnum.EPB099999.key);
                xwh001ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value, e.getMessage());
            xwh001ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xwh001ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xwh001ResultDto.setData(xwh001RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWH001.key, EsbEnum.TRADE_CODE_XWH001.value, JSON.toJSONString(xwh001ResultDto));
        return xwh001ResultDto;
    }
}
