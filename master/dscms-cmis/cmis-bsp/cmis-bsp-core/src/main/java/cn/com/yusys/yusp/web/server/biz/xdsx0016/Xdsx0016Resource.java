package cn.com.yusys.yusp.web.server.biz.xdsx0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0016.req.Xdsx0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0016.resp.Xdsx0016RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0016.req.Xdsx0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0016.resp.Xdsx0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵押查封信息同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDSX0016:抵押查封信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0016Resource.class);

    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;
    /**
     * 交易码：xdsx0016
     * 交易描述：抵押查封信息同步
     *
     * @param xdsx0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押查封信息同步")
    @PostMapping("/xdsx0016")
    //@Idempotent({"xdcasx0016", "#xdsx0016ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0016RespDto xdsx0016(@Validated @RequestBody Xdsx0016ReqDto xdsx0016ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, JSON.toJSONString(xdsx0016ReqDto));
        Xdsx0016DataReqDto xdsx0016DataReqDto = new Xdsx0016DataReqDto();// 请求Data： 抵押查封信息同步
        Xdsx0016DataRespDto xdsx0016DataRespDto = new Xdsx0016DataRespDto();// 响应Data：抵押查封信息同步
        Xdsx0016RespDto xdsx0016RespDto = new Xdsx0016RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdsx0016.req.Data reqData = null; // 请求Data：抵押查封信息同步
        cn.com.yusys.yusp.dto.server.biz.xdsx0016.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0016.resp.Data();// 响应Data：抵押查封信息同步

        try {
            // 从 xdsx0016ReqDto获取 reqData
            reqData = xdsx0016ReqDto.getData();
            // 将 reqData 拷贝到xdsx0016DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0016DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, JSON.toJSONString(xdsx0016DataReqDto));
            ResultDto<Xdsx0016DataRespDto> xdsx0016DataResultDto = dscmsBizSxClientService.xdsx0016(xdsx0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, JSON.toJSONString(xdsx0016DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdsx0016RespDto.setErorcd(Optional.ofNullable(xdsx0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0016RespDto.setErortx(Optional.ofNullable(xdsx0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0016DataResultDto.getCode())) {
                xdsx0016DataRespDto = xdsx0016DataResultDto.getData();
                xdsx0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0016DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0016RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, e.getMessage());
            xdsx0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0016RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0016RespDto.setDatasq(servsq);

        xdsx0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0016.key, DscmsEnum.TRADE_CODE_XDSX0016.value, JSON.toJSONString(xdsx0016RespDto));
        return xdsx0016RespDto;
    }
}
