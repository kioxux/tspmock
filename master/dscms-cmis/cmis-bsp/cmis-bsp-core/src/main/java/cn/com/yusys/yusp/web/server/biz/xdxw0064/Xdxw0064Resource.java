package cn.com.yusys.yusp.web.server.biz.xdxw0064;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0064.req.Xdxw0064ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0064.resp.Xdxw0064RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0064.req.Xdxw0064DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0064.resp.Xdxw0064DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷、优农贷授信调查信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0064:优企贷、优农贷授信调查信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0064Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0064Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0064
     * 交易描述：优企贷、优农贷授信调查信息查询
     *
     * @param xdxw0064ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷授信调查信息查询")
    @PostMapping("/xdxw0064")
    //@Idempotent({"xdcaxw0064", "#xdxw0064ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0064RespDto xdxw0064(@Validated @RequestBody Xdxw0064ReqDto xdxw0064ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064ReqDto));
        Xdxw0064DataReqDto xdxw0064DataReqDto = new Xdxw0064DataReqDto();// 请求Data： 优企贷、优农贷授信调查信息查询
        Xdxw0064DataRespDto xdxw0064DataRespDto = new Xdxw0064DataRespDto();// 响应Data：优企贷、优农贷授信调查信息查询
        Xdxw0064RespDto xdxw0064RespDto = new Xdxw0064RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0064.req.Data reqData = null; // 请求Data：优企贷、优农贷授信调查信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0064.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0064.resp.Data();// 响应Data：优企贷、优农贷授信调查信息查询
        try {
            // 从 xdxw0064ReqDto获取 reqData
            reqData = xdxw0064ReqDto.getData();
            // 将 reqData 拷贝到xdxw0064DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0064DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064DataReqDto));
            ResultDto<Xdxw0064DataRespDto> xdxw0064DataResultDto = dscmsBizXwClientService.xdxw0064(xdxw0064DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0064RespDto.setErorcd(Optional.ofNullable(xdxw0064DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0064RespDto.setErortx(Optional.ofNullable(xdxw0064DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0064DataResultDto.getCode())) {
                xdxw0064DataRespDto = xdxw0064DataResultDto.getData();
                xdxw0064RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0064RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0064DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0064DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, e.getMessage());
            xdxw0064RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0064RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0064RespDto.setDatasq(servsq);

        xdxw0064RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0064.key, DscmsEnum.TRADE_CODE_XDXW0064.value, JSON.toJSONString(xdxw0064RespDto));
        return xdxw0064RespDto;
    }
}
