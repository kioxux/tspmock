package cn.com.yusys.yusp.web.client.esb.core.ln3100;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3100.req.Ln3100ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3100.resp.Ln3100RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3100)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3100Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3100Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3100
     * 交易描述：根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等
     * *
     *
     * @param ln3100ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3100:根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等")
    @PostMapping("/ln3100")
    protected @ResponseBody
    ResultDto<Ln3100RespDto> ln3100(@Validated @RequestBody Ln3100ReqDto ln3100ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value, JSON.toJSONString(ln3100ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3100.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3100.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3100.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3100.resp.Service();
        Ln3100ReqService ln3100ReqService = new Ln3100ReqService();
        Ln3100RespService ln3100RespService = new Ln3100RespService();
        Ln3100RespDto ln3100RespDto = new Ln3100RespDto();
        ResultDto<Ln3100RespDto> ln3100ResultDto = new ResultDto<Ln3100RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将ln3100ReqDto转换成reqService
            BeanUtils.copyProperties(ln3100ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3100.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3100ReqService.setService(reqService);
            // 将ln3100ReqService转换成ln3100ReqServiceMap
            Map ln3100ReqServiceMap = beanMapUtil.beanToMap(ln3100ReqService);
            context.put("tradeDataMap", ln3100ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3100.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3100RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3100RespService.class, Ln3100RespService.class);
            respService = ln3100RespService.getService();
            //  将respService转换成Ln3100RespDto
            BeanUtils.copyProperties(respService, ln3100RespDto);
            ln3100ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3100ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, ln3100RespDto);
                ln3100ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3100ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3100ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3100ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value, e.getMessage());
            ln3100ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3100ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3100ResultDto.setData(ln3100RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3100.key, EsbEnum.TRADE_CODE_LN3100.value, JSON.toJSONString(ln3100ResultDto));
        return ln3100ResultDto;
    }
}
