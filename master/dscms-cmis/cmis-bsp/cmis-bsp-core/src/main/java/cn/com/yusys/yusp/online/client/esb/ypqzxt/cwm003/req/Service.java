package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.req;

/**
 * 请求Service：押品权证入库
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:41
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //   全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String userName;//	柜员名称	否	varchar(50)	是	例：2020-10-19
    private String applyTime;//	申请时间	否	date	是	例：2020-10-19
    private String gageId;//	核心担保品编号	否	varchar(32)	是
    private String gageType;//	抵质押类型	否	varchar(6)	是
    private String gageTypeName;//	抵质押类型名称	否	varchar(50)	否
    private String maxAmt;//	权利价值	否	number(16,2)	是
    private String gageUser;//	抵押人	否	varchar(50)	否
    private String acctBrch;//	账务机构	否	varchar(10)	是
    private String acctBrchName;//	账务机构名称	否	varchar(50)	是
    private String applyBrchNo;//	申请支行号	否	varchar(10)	是
    private String applyBrchName;//	申请支行名称	否	varchar(50)	是
    private String applyUserCode;//	申请人操作号	否	varchar(10)	是
    private String applyUserName;//	申请人名称	否	varchar(50)	是
    private String isMortgage;//	是否住房按揭	否	char(1)	是	 1：是          0：否
    private String remark;//	描述，备注	否	varchar(200)	否
    private String gageName;//	抵押物名称	否	varchar(100)	否
    private String isLocal;//	是否张家港地区不动产	否	char(1)	是	 0：不是      1：是
    private String isElectronic;//	押品类型	否	char(1)	是	 1：电子      2：实物

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getGageId() {
        return gageId;
    }

    public void setGageId(String gageId) {
        this.gageId = gageId;
    }

    public String getGageType() {
        return gageType;
    }

    public void setGageType(String gageType) {
        this.gageType = gageType;
    }

    public String getGageTypeName() {
        return gageTypeName;
    }

    public void setGageTypeName(String gageTypeName) {
        this.gageTypeName = gageTypeName;
    }

    public String getMaxAmt() {
        return maxAmt;
    }

    public void setMaxAmt(String maxAmt) {
        this.maxAmt = maxAmt;
    }

    public String getGageUser() {
        return gageUser;
    }

    public void setGageUser(String gageUser) {
        this.gageUser = gageUser;
    }

    public String getAcctBrch() {
        return acctBrch;
    }

    public void setAcctBrch(String acctBrch) {
        this.acctBrch = acctBrch;
    }

    public String getAcctBrchName() {
        return acctBrchName;
    }

    public void setAcctBrchName(String acctBrchName) {
        this.acctBrchName = acctBrchName;
    }

    public String getApplyBrchNo() {
        return applyBrchNo;
    }

    public void setApplyBrchNo(String applyBrchNo) {
        this.applyBrchNo = applyBrchNo;
    }

    public String getApplyBrchName() {
        return applyBrchName;
    }

    public void setApplyBrchName(String applyBrchName) {
        this.applyBrchName = applyBrchName;
    }

    public String getApplyUserCode() {
        return applyUserCode;
    }

    public void setApplyUserCode(String applyUserCode) {
        this.applyUserCode = applyUserCode;
    }

    public String getApplyUserName() {
        return applyUserName;
    }

    public void setApplyUserName(String applyUserName) {
        this.applyUserName = applyUserName;
    }

    public String getIsMortgage() {
        return isMortgage;
    }

    public void setIsMortgage(String isMortgage) {
        this.isMortgage = isMortgage;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getGageName() {
        return gageName;
    }

    public void setGageName(String gageName) {
        this.gageName = gageName;
    }

    public String getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(String isLocal) {
        this.isLocal = isLocal;
    }

    public String getIsElectronic() {
        return isElectronic;
    }

    public void setIsElectronic(String isElectronic) {
        this.isElectronic = isElectronic;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", userName='" + userName + '\'' +
                ", applyTime='" + applyTime + '\'' +
                ", gageId='" + gageId + '\'' +
                ", gageType='" + gageType + '\'' +
                ", gageTypeName='" + gageTypeName + '\'' +
                ", maxAmt='" + maxAmt + '\'' +
                ", gageUser='" + gageUser + '\'' +
                ", acctBrch='" + acctBrch + '\'' +
                ", acctBrchName='" + acctBrchName + '\'' +
                ", applyBrchNo='" + applyBrchNo + '\'' +
                ", applyBrchName='" + applyBrchName + '\'' +
                ", applyUserCode='" + applyUserCode + '\'' +
                ", applyUserName='" + applyUserName + '\'' +
                ", isMortgage='" + isMortgage + '\'' +
                ", remark='" + remark + '\'' +
                ", gageName='" + gageName + '\'' +
                ", isLocal='" + isLocal + '\'' +
                ", isElectronic='" + isElectronic + '\'' +
                '}';
    }
}
