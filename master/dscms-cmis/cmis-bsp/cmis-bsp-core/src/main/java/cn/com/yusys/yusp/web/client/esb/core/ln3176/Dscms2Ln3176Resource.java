package cn.com.yusys.yusp.web.client.esb.core.ln3176;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3176.Ln3176ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3176.Ln3176RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3176.LstLnHuankDZhhObj;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3176.req.Ln3176ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3176.resp.Ln3176RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3176)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3176Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3176Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3176
     * 交易描述：本交易用户贷款多个还款账户进行还款
     *
     * @param ln3176ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3176:本交易用户贷款多个还款账户进行还款")
    @PostMapping("/ln3176")
    protected @ResponseBody
    ResultDto<Ln3176RespDto> ln3176(@Validated @RequestBody Ln3176ReqDto ln3176ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3176.key, EsbEnum.TRADE_CODE_LN3176.value, JSON.toJSONString(ln3176ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3176.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3176.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3176.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3176.resp.Service();
        Ln3176ReqService ln3176ReqService = new Ln3176ReqService();
        Ln3176RespService ln3176RespService = new Ln3176RespService();
        Ln3176RespDto ln3176RespDto = new Ln3176RespDto();
        ResultDto<Ln3176RespDto> ln3176ResultDto = new ResultDto<Ln3176RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3176ReqDto转换成reqService
            BeanUtils.copyProperties(ln3176ReqDto, reqService);
            // 循环相关的判断
            if (CollectionUtils.nonEmpty(ln3176ReqDto.getLstLnHuankDZhhObj())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3176.LstLnHuankDZhhObj> lstLnHuankDZhhObjs = Optional.ofNullable(ln3176ReqDto.getLstLnHuankDZhhObj()).orElseGet(() -> new ArrayList<LstLnHuankDZhhObj>());
                cn.com.yusys.yusp.online.client.esb.core.ln3176.req.LstLnHuankDZhhObj lstLnHuankDZhhObjService = new cn.com.yusys.yusp.online.client.esb.core.ln3176.req.LstLnHuankDZhhObj();
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3176.req.Record> records = lstLnHuankDZhhObjs.parallelStream().map(lstLnHuankDZhhObj -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3176.req.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3176.req.Record();
                    BeanUtils.copyProperties(lstLnHuankDZhhObj, record);
                    return record;
                }).collect(Collectors.toList());
                lstLnHuankDZhhObjService.setRecord(records);
                reqService.setLstLnHuankDZhhObj(lstLnHuankDZhhObjService);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3176.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3176ReqService.setService(reqService);
            // 将ln3176ReqService转换成ln3176ReqServiceMap
            Map ln3176ReqServiceMap = beanMapUtil.beanToMap(ln3176ReqService);
            context.put("tradeDataMap", ln3176ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3176.key, EsbEnum.TRADE_CODE_LN3176.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3176.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3176.key, EsbEnum.TRADE_CODE_LN3176.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3176RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3176RespService.class, Ln3176RespService.class);
            respService = ln3176RespService.getService();

            ln3176ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3176ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3176RespDto
                BeanUtils.copyProperties(respService, ln3176RespDto);
                // 循环相关的判断开始
                ln3176ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3176ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3176ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3176ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3176.key, EsbEnum.TRADE_CODE_LN3176.value, e.getMessage());
            ln3176ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3176ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3176ResultDto.setData(ln3176RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3176.key, EsbEnum.TRADE_CODE_LN3176.value, JSON.toJSONString(ln3176ResultDto));
        return ln3176ResultDto;
    }

}
