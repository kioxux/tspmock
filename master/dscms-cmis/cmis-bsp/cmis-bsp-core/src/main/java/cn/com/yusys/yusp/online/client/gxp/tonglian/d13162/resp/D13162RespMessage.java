package cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp;

/**
 * 响应Service：分期审核
 *
 * @author code-generator
 * @version 1.0
 */
public class D13162RespMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.resp.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D13162RespMessage{" +
                "message=" + message +
                '}';
    }
}
