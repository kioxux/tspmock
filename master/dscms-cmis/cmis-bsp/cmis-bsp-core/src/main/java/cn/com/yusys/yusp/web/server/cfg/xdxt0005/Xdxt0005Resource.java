package cn.com.yusys.yusp.web.server.cfg.xdxt0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0005.req.Xdxt0005ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0005.resp.Xdxt0005RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.req.Xdxt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0005.resp.Xdxt0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户经理所在分部编号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0005:查询客户经理所在分部编号")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0005Resource.class);
    @Autowired
    private DscmsBizXtClientService dscmsBizXtClientService;

    /**
     * 交易码：xdxt0005
     * 交易描述：查询客户经理所在分部编号
     *
     * @param xdxt0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户经理所在分部编号")
    @PostMapping("/xdxt0005")
    //@Idempotent({"xdcaxt0005", "#xdxt0005ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0005RespDto xdxt0005(@Validated @RequestBody Xdxt0005ReqDto xdxt0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005ReqDto));
        Xdxt0005DataReqDto xdxt0005DataReqDto = new Xdxt0005DataReqDto();// 请求Data： 查询客户经理所在分部编号
        Xdxt0005DataRespDto xdxt0005DataRespDto = new Xdxt0005DataRespDto();// 响应Data：查询客户经理所在分部编号
        Xdxt0005RespDto xdxt0005RespDto = new Xdxt0005RespDto();
        cn.com.yusys.yusp.dto.server.cfg.xdxt0005.req.Data reqData = null; // 请求Data：查询客户经理所在分部编号
        cn.com.yusys.yusp.dto.server.cfg.xdxt0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdxt0005.resp.Data();// 响应Data：查询客户经理所在分部编号
        try {
            // 从 xdxt0005ReqDto获取 reqData
            reqData = xdxt0005ReqDto.getData();
            // 将 reqData 拷贝到xdxt0005DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataReqDto));
            ResultDto<Xdxt0005DataRespDto> xdxt0005DataResultDto = dscmsBizXtClientService.xdxt0005(xdxt0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0005RespDto.setErorcd(Optional.ofNullable(xdxt0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0005RespDto.setErortx(Optional.ofNullable(xdxt0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0005DataResultDto.getCode())) {
                xdxt0005DataRespDto = xdxt0005DataResultDto.getData();
                xdxt0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, e.getMessage());
            xdxt0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0005RespDto.setDatasq(servsq);

        xdxt0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0005.key, DscmsEnum.TRADE_CODE_XDXT0005.value, JSON.toJSONString(xdxt0005RespDto));
        return xdxt0005RespDto;
    }
}
