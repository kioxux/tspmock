package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：响应信息域元素:产品层债项评级结果
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class MessageInfo {
    private List<MessageInfoRecord> record; // 产品层债项评级结果

    public List<MessageInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<MessageInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "MessageInfo{" +
                "record=" + record +
                '}';
    }
}
