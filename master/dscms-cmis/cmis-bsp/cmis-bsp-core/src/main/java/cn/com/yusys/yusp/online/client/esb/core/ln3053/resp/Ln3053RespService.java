package cn.com.yusys.yusp.online.client.esb.core.ln3053.resp;

/**
 * 响应Service：还款计划调整
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3053RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

