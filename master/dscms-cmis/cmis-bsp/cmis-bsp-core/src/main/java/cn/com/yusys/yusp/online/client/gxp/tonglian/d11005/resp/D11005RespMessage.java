package cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp;

/**
 * 响应Service：d11005
 *
 * @author chenyong
 * @version 1.0
 */
public class D11005RespMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D11005RespMessage{" +
                "message=" + message +
                '}';
    }
}
