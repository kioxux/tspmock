package cn.com.yusys.yusp.online.client.esb.ypxt.businf.req;

/**
 * 请求Service：信贷业务合同信息同步
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
