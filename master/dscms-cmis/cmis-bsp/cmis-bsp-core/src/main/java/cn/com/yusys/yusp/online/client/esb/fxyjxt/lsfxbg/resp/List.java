package cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.resp;

import java.io.Serializable;

/**
 * 响应Service：查询客户项下历史风险预警报告
 */
public class List implements Serializable {
    private String custId;//客户号
    private String custNm;//客户名称
    private String dataDt;//数据日期
    private String num;//档案号
    private String custType;//客户类型
    private String grtCrcId;//担保圈编号

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustNm() {
        return custNm;
    }

    public void setCustNm(String custNm) {
        this.custNm = custNm;
    }

    public String getDataDt() {
        return dataDt;
    }

    public void setDataDt(String dataDt) {
        this.dataDt = dataDt;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getGrtCrcId() {
        return grtCrcId;
    }

    public void setGrtCrcId(String grtCrcId) {
        this.grtCrcId = grtCrcId;
    }

    @Override
    public String toString() {
        return "List{" +
                "custId='" + custId + '\'' +
                "custNm='" + custNm + '\'' +
                "dataDt='" + dataDt + '\'' +
                "num='" + num + '\'' +
                "custType='" + custType + '\'' +
                "grtCrcId='" + grtCrcId + '\'' +
                '}';
    }
}
