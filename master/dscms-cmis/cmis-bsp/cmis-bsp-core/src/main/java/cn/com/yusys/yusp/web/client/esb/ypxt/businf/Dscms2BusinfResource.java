package cn.com.yusys.yusp.web.client.esb.ypxt.businf;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.BusinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.List;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.resp.BusinfRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.BusinfReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.businf.resp.BusinfRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:信贷业务合同信息同步
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2BusinfResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2BusinfResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：businf
     * 交易描述：信贷业务合同信息同步
     *
     * @param businfReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/businf")
    protected @ResponseBody
    ResultDto<BusinfRespDto> businf(@Validated @RequestBody BusinfReqDto businfReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value, JSON.toJSONString(businfReqDto));
		cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.Service();
		cn.com.yusys.yusp.online.client.esb.ypxt.businf.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.businf.resp.Service();
        BusinfReqService businfReqService = new BusinfReqService();
        BusinfRespService businfRespService = new BusinfRespService();
        BusinfRespDto businfRespDto = new BusinfRespDto();
        ResultDto<BusinfRespDto> businfResultDto = new ResultDto<BusinfRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将businfReqDto转换成reqService
			BeanUtils.copyProperties(businfReqDto, reqService);
			java.util.List<List> reqList = businfReqDto.getList();
			if (CollectionUtils.nonEmpty(reqList)) {
				java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.Record> recordList = reqList.parallelStream().map(req -> {
					cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.Record();
					BeanUtils.copyProperties(req, record);
					return record;
				}).collect(Collectors.toList());
				cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.List serviceList = new cn.com.yusys.yusp.online.client.esb.ypxt.businf.req.List();
				serviceList.setRecord(recordList);
				reqService.setList(serviceList);
			}
			reqService.setPrcscd(EsbEnum.TRADE_CODE_BUSINF.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
			businfReqService.setService(reqService);
			// 将businfReqService转换成businfReqServiceMap
			Map businfReqServiceMap = beanMapUtil.beanToMap(businfReqService);
			context.put("tradeDataMap", businfReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_BUSINF.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			businfRespService = beanMapUtil.mapToBean(tradeDataMap, BusinfRespService.class, BusinfRespService.class);
			respService = businfRespService.getService();
			//  将respService转换成BusinfRespDto
			businfResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			businfResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成CertisRespDto
				BeanUtils.copyProperties(respService, businfRespDto);
				businfResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				businfResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				businfResultDto.setCode(EpbEnum.EPB099999.key);
				businfResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value, e.getMessage());
			businfResultDto.setCode(EpbEnum.EPB099999.key);//9999
			businfResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		businfResultDto.setData(businfRespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSINF.key, EsbEnum.TRADE_CODE_BUSINF.value, JSON.toJSONString(businfResultDto));
        return businfResultDto;
    }
}
