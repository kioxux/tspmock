package cn.com.yusys.yusp.online.client.esb.core.ln3007.resp;

/**
 * 响应Service：资产产品币种查询
 * @author code-generator
 * @version 1.0             
 */      
public class Ln3007RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
