package cn.com.yusys.yusp.web.client.esb.core.da3300;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3300.Da3300ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3300.Da3300RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3300.req.Da3300ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3300.resp.Da3300RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class Dscms2Da3300Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Da3300Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3300
     * 交易描述：交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理
     *
     * @param da3300ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3300")
    protected @ResponseBody
    ResultDto<Da3300RespDto> da3300(@Validated @RequestBody Da3300ReqDto da3300ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3300.key, EsbEnum.TRADE_CODE_DA3300.value, JSON.toJSONString(da3300ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3300.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3300.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3300.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3300.resp.Service();
        Da3300ReqService da3300ReqService = new Da3300ReqService();
        Da3300RespService da3300RespService = new Da3300RespService();
        Da3300RespDto da3300RespDto = new Da3300RespDto();
        ResultDto<Da3300RespDto> da3300ResultDto = new ResultDto<Da3300RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3300ReqDto转换成reqService
            BeanUtils.copyProperties(da3300ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3300.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3300ReqService.setService(reqService);
            // 将da3300ReqService转换成da3300ReqServiceMap
            Map da3300ReqServiceMap = beanMapUtil.beanToMap(da3300ReqService);
            context.put("tradeDataMap", da3300ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3300.key, EsbEnum.TRADE_CODE_DA3300.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3300.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3300.key, EsbEnum.TRADE_CODE_DA3300.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3300RespService = beanMapUtil.mapToBean(tradeDataMap, Da3300RespService.class, Da3300RespService.class);
            respService = da3300RespService.getService();

            da3300ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3300ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3300RespDto
                BeanUtils.copyProperties(respService, da3300RespDto);
                da3300ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3300ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3300ResultDto.setCode(EpbEnum.EPB099999.key);
                da3300ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3300.key, EsbEnum.TRADE_CODE_DA3300.value, e.getMessage());
            da3300ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3300ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3300ResultDto.setData(da3300RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3300.key, EsbEnum.TRADE_CODE_DA3300.value, JSON.toJSONString(da3300ResultDto));
        return da3300ResultDto;
    }
}
