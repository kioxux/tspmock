package cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.resp;

/**
 * 响应Service：获取该笔借据的最新五级分类以及数据日期
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码
    private String erortx; // 响应信息
    private String datasq; // 全局流水

    private String classify_level_new; //分类级别(新)
    private String data_date; //数据日期

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getClassify_level_new() {
        return classify_level_new;
    }

    public void setClassify_level_new(String classify_level_new) {
        this.classify_level_new = classify_level_new;
    }

    public String getData_date() {
        return data_date;
    }

    public void setData_date(String data_date) {
        this.data_date = data_date;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", datasq='" + datasq + '\'' +
                ", classify_level_new='" + classify_level_new + '\'' +
                ", data_date='" + data_date + '\'' +
                '}';
    }
}
