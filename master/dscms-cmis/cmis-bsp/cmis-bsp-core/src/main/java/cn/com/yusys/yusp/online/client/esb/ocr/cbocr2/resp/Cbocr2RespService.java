package cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.resp;

/**
 * 响应Service：识别任务的模板匹配结果列表查询
 * @author code-generator
 * @version 1.0             
 */      
public class Cbocr2RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
