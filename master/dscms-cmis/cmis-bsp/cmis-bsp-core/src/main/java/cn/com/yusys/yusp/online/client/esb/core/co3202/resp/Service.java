package cn.com.yusys.yusp.online.client.esb.core.co3202.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵质押物的开户
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:11
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String dzywbhao; // 抵质押物编号
    private String dzywminc; // 抵质押物名称
    private String ruzjigou; // 入账机构
    private String huobdhao; // 货币代号
    private BigDecimal minyjiaz; // 名义价值
    private BigDecimal shijjiaz; // 实际价值
    private BigDecimal dizybilv; // 抵质押比率
    private BigDecimal keyongje; // 可用金额
    private String shengxrq; // 生效日期
    private String daoqriqi; // 到期日期
    private String dzywztai; // 抵质押物状态
    private String zhaiyoms; // 摘要
    private String kaihguiy; // 开户柜员
    private String kaihriqi; // 开户日期
    private String jiaoyirq; // 交易日期
    private String jiaoyils; // 交易流水
    private String dizyfshi; // 抵质押方式
    private String chrkleix; // 出入库类型
    private String syqrkehh; // 所有权人客户号
    private String syqrkehm; // 所有权人客户名

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDzywminc() {
        return dzywminc;
    }

    public void setDzywminc(String dzywminc) {
        this.dzywminc = dzywminc;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getMinyjiaz() {
        return minyjiaz;
    }

    public void setMinyjiaz(BigDecimal minyjiaz) {
        this.minyjiaz = minyjiaz;
    }

    public BigDecimal getShijjiaz() {
        return shijjiaz;
    }

    public void setShijjiaz(BigDecimal shijjiaz) {
        this.shijjiaz = shijjiaz;
    }

    public BigDecimal getDizybilv() {
        return dizybilv;
    }

    public void setDizybilv(BigDecimal dizybilv) {
        this.dizybilv = dizybilv;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDzywztai() {
        return dzywztai;
    }

    public void setDzywztai(String dzywztai) {
        this.dzywztai = dzywztai;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getDizyfshi() {
        return dizyfshi;
    }

    public void setDizyfshi(String dizyfshi) {
        this.dizyfshi = dizyfshi;
    }

    public String getChrkleix() {
        return chrkleix;
    }

    public void setChrkleix(String chrkleix) {
        this.chrkleix = chrkleix;
    }

    public String getSyqrkehh() {
        return syqrkehh;
    }

    public void setSyqrkehh(String syqrkehh) {
        this.syqrkehh = syqrkehh;
    }

    public String getSyqrkehm() {
        return syqrkehm;
    }

    public void setSyqrkehm(String syqrkehm) {
        this.syqrkehm = syqrkehm;
    }

    @Override
    public String toString() {
        return "Co3202RespService{" +
                "dzywbhao='" + dzywbhao + '\'' +
                ", dzywminc='" + dzywminc + '\'' +
                ", ruzjigou='" + ruzjigou + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", minyjiaz=" + minyjiaz +
                ", shijjiaz=" + shijjiaz +
                ", dizybilv=" + dizybilv +
                ", keyongje=" + keyongje +
                ", shengxrq='" + shengxrq + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", dzywztai='" + dzywztai + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", dizyfshi='" + dizyfshi + '\'' +
                ", chrkleix='" + chrkleix + '\'' +
                ", syqrkehh='" + syqrkehh + '\'' +
                ", syqrkehm='" + syqrkehm + '\'' +
                '}';
    }
}
