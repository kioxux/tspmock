package cn.com.yusys.yusp.web.server.lmt.xded0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0019.req.CmisLmt0019ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0019.req.Xded0019ReqDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0019.resp.Xded0019RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:校验客户是否存在有效综合授信额度
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDED0019:校验客户是否存在有效综合授信额度")
@RestController
@RequestMapping("/api/dscms")
public class Xded0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xded0019Resource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：cmisLmt0019
     * 交易描述：校验客户是否存在有效综合授信额度
     *
     * @param xded0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("校验客户是否存在有效综合授信额度")
    @PostMapping("/xded0019")
    @Idempotent({"xded0019", "#xded0019ReqDto.datasq"})
    protected @ResponseBody
    Xded0019RespDto cmisLmt0019(@Validated @RequestBody Xded0019ReqDto xded0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0019.key, DscmsEnum.TRADE_CODE_CMISLMT0019.value, JSON.toJSONString(xded0019ReqDto));
        CmisLmt0019ReqDto cmisLmt0019DataReqDto = new CmisLmt0019ReqDto();// 请求Data： 校验客户是否存在有效综合授信额度
        CmisLmt0019RespDto cmisLmt0019DataRespDto = new CmisLmt0019RespDto();// 响应Data：校验客户是否存在有效综合授信额度

        Xded0019RespDto xded0019RespDto = new Xded0019RespDto();

        cn.com.yusys.yusp.dto.server.lmt.xded0019.req.Data reqData = null; // 请求Data：校验客户是否存在有效综合授信额度
        cn.com.yusys.yusp.dto.server.lmt.xded0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.xded0019.resp.Data();// 响应Data：校验客户是否存在有效综合授信额度

        try {
            // 从 cmisLmt0019ReqDto获取 reqData
            reqData = xded0019ReqDto.getData();
            // 将 reqData 拷贝到cmisLmt0019DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0019DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0019.key, DscmsEnum.TRADE_CODE_CMISLMT0019.value, JSON.toJSONString(cmisLmt0019DataReqDto));
            ResultDto<CmisLmt0019RespDto> cmisLmt0019DataResultDto = cmisLmtClientService.cmisLmt0019(cmisLmt0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0019.key, DscmsEnum.TRADE_CODE_CMISLMT0019.value, JSON.toJSONString(cmisLmt0019DataRespDto));
            // 从返回值中获取响应码和响应消息
            xded0019RespDto.setErorcd(Optional.ofNullable(cmisLmt0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xded0019RespDto.setErortx(Optional.ofNullable(cmisLmt0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0019DataResultDto.getCode())) {
                cmisLmt0019DataRespDto = cmisLmt0019DataResultDto.getData();
                xded0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xded0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0019DataRespDto, respData);
            } else {
                if (Objects.nonNull(cmisLmt0019DataResultDto.getData())) {
                    cmisLmt0019DataRespDto = cmisLmt0019DataResultDto.getData();
                    xded0019RespDto.setErorcd(cmisLmt0019DataResultDto.getData().getErrorCode());
                    xded0019RespDto.setErortx(cmisLmt0019DataResultDto.getData().getErrorMsg());
                    BeanUtils.copyProperties(cmisLmt0019DataRespDto, respData);
                } else {
                    xded0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                    xded0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
                }
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0019.key, DscmsEnum.TRADE_CODE_CMISLMT0019.value, e.getMessage());
            xded0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xded0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xded0019RespDto.setDatasq(servsq);

        xded0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0019.key, DscmsEnum.TRADE_CODE_CMISLMT0019.value, JSON.toJSONString(xded0019RespDto));
        return xded0019RespDto;
    }
}
