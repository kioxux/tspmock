package cn.com.yusys.yusp.web.server.biz.xddh0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0009.req.Xddh0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0009.resp.Xddh0009RespDto;
import cn.com.yusys.yusp.dto.server.xddh0009.req.Xddh0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0009.resp.Xddh0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:还款日期卡控
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0009:还款日期卡控")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0009Resource.class);
	@Autowired
	private DscmsBizDhClientService dscmsBizDhClientService;
    /**
     * 交易码：xddh0009
     * 交易描述：还款日期卡控
     *
     * @param xddh0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款日期卡控")
    @PostMapping("/xddh0009")
    //@Idempotent({"xddh0009", "#xddh0009ReqDto.datasq"})
    protected @ResponseBody
    Xddh0009RespDto xddh0009(@Validated @RequestBody Xddh0009ReqDto xddh0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, JSON.toJSONString(xddh0009ReqDto));
        Xddh0009DataReqDto xddh0009DataReqDto = new Xddh0009DataReqDto();// 请求Data： 还款日期卡控
        Xddh0009DataRespDto xddh0009DataRespDto = new Xddh0009DataRespDto();// 响应Data：还款日期卡控
        Xddh0009RespDto xddh0009RespDto = new Xddh0009RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0009.req.Data reqData = null; // 请求Data：还款日期卡控
        cn.com.yusys.yusp.dto.server.biz.xddh0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0009.resp.Data();// 响应Data：还款日期卡控
        try {
            // 从 xddh0009ReqDto获取 reqData
            reqData = xddh0009ReqDto.getData();
            // 将 reqData 拷贝到xddh0009DataReqDto
            BeanUtils.copyProperties(reqData, xddh0009DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, JSON.toJSONString(xddh0009DataReqDto));
            ResultDto<Xddh0009DataRespDto> xddh0009DataResultDto = dscmsBizDhClientService.xddh0009(xddh0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, JSON.toJSONString(xddh0009DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0009RespDto.setErorcd(Optional.ofNullable(xddh0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0009RespDto.setErortx(Optional.ofNullable(xddh0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0009DataResultDto.getCode())) {
                xddh0009DataRespDto = xddh0009DataResultDto.getData();
                xddh0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, e.getMessage());
            xddh0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0009RespDto.setDatasq(servsq);

        xddh0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0009.key, DscmsEnum.TRADE_CODE_XDDH0009.value, JSON.toJSONString(xddh0009RespDto));
        return xddh0009RespDto;
    }
}
