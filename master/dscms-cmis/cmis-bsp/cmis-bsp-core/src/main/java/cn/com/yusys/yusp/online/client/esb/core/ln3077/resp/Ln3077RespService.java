package cn.com.yusys.yusp.online.client.esb.core.ln3077.resp;

/**
 * 响应Service：客户账本息调整
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3077RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
