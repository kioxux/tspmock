package cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhlm;

public class Record {
    private String kehugxlx;//客户关系类型
    private String kehuhaoo;//客户号
    private String dkzhangh;//贷款账号
    private String kehmingc;//客户名称

    public String getKehugxlx() {
        return kehugxlx;
    }

    public void setKehugxlx(String kehugxlx) {
        this.kehugxlx = kehugxlx;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    @Override
    public String toString() {
        return "Record{" +
                "kehugxlx='" + kehugxlx + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                '}';
    }
}
