package cn.com.yusys.yusp.web.client.esb.circp.fb1170;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Dblist;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Dylist;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Fb1170ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.resp.Fb1170RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Fb1170ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1170.resp.Fb1170RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1170）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1170Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1170Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1170
     * 交易描述：最高额借款合同信息推送
     *
     * @param fb1170ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1170")
    protected @ResponseBody
    ResultDto<Fb1170RespDto> fb1170(@Validated @RequestBody Fb1170ReqDto fb1170ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1170.key, EsbEnum.TRADE_CODE_FB1170.value, JSON.toJSONString(fb1170ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1170.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1170.resp.Service();
        Fb1170ReqService fb1170ReqService = new Fb1170ReqService();
        Fb1170RespService fb1170RespService = new Fb1170RespService();
        Fb1170RespDto fb1170RespDto = new Fb1170RespDto();
        ResultDto<Fb1170RespDto> fb1170ResultDto = new ResultDto<Fb1170RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1170ReqDto转换成reqService
            BeanUtils.copyProperties(fb1170ReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist.Dblist targetdblist = new cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist.Dblist();
            if (CollectionUtils.nonEmpty(fb1170ReqDto.getDblist())) {
                List<Dblist> dblist = fb1170ReqDto.getDblist();
                java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist.Record> recordList1 = new ArrayList<>();
                for (Dblist d : dblist) {
                    cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist.Record targetRecord1 = new cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist.Record();
                    BeanUtils.copyProperties(d, targetRecord1);
                    recordList1.add(targetRecord1);
                }
                targetdblist.setRecord(recordList1);
                reqService.setList_db(targetdblist);
            }

            cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist.Dylist targetdylist = new cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist.Dylist();
            if (CollectionUtils.nonEmpty(fb1170ReqDto.getDylist())) {
                List<Dylist> dylist = fb1170ReqDto.getDylist();
                java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist.Record> recordList2 = new ArrayList<>();
                for (Dylist dy : dylist) {
                    cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist.Record targetRecord2 = new cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist.Record();
                    BeanUtils.copyProperties(dy, targetRecord2);
                    recordList2.add(targetRecord2);
                }
                targetdylist.setRecord(recordList2);
                reqService.setList_dy(targetdylist);
            }


            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1170.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1170ReqService.setService(reqService);
            // 将fb1170ReqService转换成fb1170ReqServiceMap
            Map fb1170ReqServiceMap = beanMapUtil.beanToMap(fb1170ReqService);
            context.put("tradeDataMap", fb1170ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1170.key, EsbEnum.TRADE_CODE_FB1170.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1170.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1170.key, EsbEnum.TRADE_CODE_FB1170.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1170RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1170RespService.class, Fb1170RespService.class);
            respService = fb1170RespService.getService();

            fb1170ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1170ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1170RespDto
                BeanUtils.copyProperties(respService, fb1170RespDto);

                fb1170ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1170ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1170ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1170ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1170.key, EsbEnum.TRADE_CODE_FB1170.value, e.getMessage());
            fb1170ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1170ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1170ResultDto.setData(fb1170RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1170.key, EsbEnum.TRADE_CODE_FB1170.value, JSON.toJSONString(fb1170ResultDto));
        return fb1170ResultDto;
    }
}
