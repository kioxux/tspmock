package cn.com.yusys.yusp.web.client.esb.ypxt.buscon;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.List;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.resp.BusconRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.BusconReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.buscon.resp.BusconRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(buscon)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2BuconResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2BuconResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：buscon
     * 交易描述：信贷业务与押品关联关系信息同步
     *
     * @param busconReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/buscon")
    protected @ResponseBody
    ResultDto<BusconRespDto> buscon(@Validated @RequestBody BusconReqDto busconReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value, JSON.toJSONString(busconReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.buscon.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.buscon.resp.Service();
        BusconReqService busconReqService = new BusconReqService();
        BusconRespService busconRespService = new BusconRespService();
        BusconRespDto busconRespDto = new BusconRespDto();
        ResultDto<BusconRespDto> busconResultDto = new ResultDto<BusconRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将busconReqDto转换成reqService
            BeanUtils.copyProperties(busconReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.List targetList = new cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.List();
            if (CollectionUtils.nonEmpty(busconReqDto.getList())) {
                java.util.List<List> originList = busconReqDto.getList();
                java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.Record> recordList = new ArrayList<>();
                for (List list : originList) {
                    cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.Record target = new cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req.Record();
                    BeanUtils.copyProperties(list, target);
                    recordList.add(target);
                }
                targetList.setRecord(recordList);
                reqService.setList(targetList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_BUSCON.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            busconReqService.setService(reqService);
            // 将busconReqService转换成busconReqServiceMap
            Map busconReqServiceMap = beanMapUtil.beanToMap(busconReqService);
            context.put("tradeDataMap", busconReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_BUSCON.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            busconRespService = beanMapUtil.mapToBean(tradeDataMap, BusconRespService.class, BusconRespService.class);
            respService = busconRespService.getService();

            busconResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            busconResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成BusconRespDto
                BeanUtils.copyProperties(respService, busconRespDto);
                busconResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                busconResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                busconResultDto.setCode(EpbEnum.EPB099999.key);
                busconResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value, e.getMessage());
            busconResultDto.setCode(EpbEnum.EPB099999.key);//9999
            busconResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        busconResultDto.setData(busconRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUSCON.key, EsbEnum.TRADE_CODE_BUSCON.value, JSON.toJSONString(busconResultDto));
        return busconResultDto;
    }
}
