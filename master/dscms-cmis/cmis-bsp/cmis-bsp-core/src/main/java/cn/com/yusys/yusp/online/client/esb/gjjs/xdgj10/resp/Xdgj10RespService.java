package cn.com.yusys.yusp.online.client.esb.gjjs.xdgj10.resp;

/**
 * 响应Service：信贷获取国结信用证信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdgj10RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdgj10RespService{" +
                "service=" + service +
                '}';
    }
}

