package cn.com.yusys.yusp.web.server.biz.xdcz0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0015.req.Xdcz0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0015.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0015.resp.Xdcz0015RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0015.req.Xdcz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0015.resp.Xdcz0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:银票影像补录同步
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0015:银票影像补录同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0015Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0015
     * 交易描述：银票影像补录同步
     *
     * @param xdcz0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("银票影像补录同步")
    @PostMapping("/xdcz0015")
   //@Idempotent({"xdcz0015", "#xdcz0015ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0015RespDto xdcz0015(@Validated @RequestBody Xdcz0015ReqDto xdcz0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015ReqDto));
        Xdcz0015DataReqDto xdcz0015DataReqDto = new Xdcz0015DataReqDto();// 请求Data： 银票影像补录同步
        Xdcz0015DataRespDto xdcz0015DataRespDto = new Xdcz0015DataRespDto();// 响应Data：银票影像补录同步

        cn.com.yusys.yusp.dto.server.biz.xdcz0015.req.Data reqData = null; // 请求Data：银票影像补录同步
        Data respData = new Data();// 响应Data：银票影像补录同步

        Xdcz0015RespDto xdcz0015RespDto = new Xdcz0015RespDto();
        try {
            // 从 xdcz0015ReqDto获取 reqData
            reqData = xdcz0015ReqDto.getData();
            // 将 reqData 拷贝到xdcz0015DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0015DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015DataReqDto));
            ResultDto<Xdcz0015DataRespDto> xdcz0015DataResultDto = dscmsBizCzClientService.xdcz0015(xdcz0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0015RespDto.setErorcd(Optional.ofNullable(xdcz0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0015RespDto.setErortx(Optional.ofNullable(xdcz0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0015DataResultDto.getCode())) {
                xdcz0015DataRespDto = xdcz0015DataResultDto.getData();
                xdcz0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, e.getMessage());
            xdcz0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0015RespDto.setDatasq(servsq);

        xdcz0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0015.key, DscmsEnum.TRADE_CODE_XDCZ0015.value, JSON.toJSONString(xdcz0015RespDto));
        return xdcz0015RespDto;
    }
}
