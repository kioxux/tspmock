package cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req;

import cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead;

/**
 * 请求Service：账户信息查询
 */
public class Message {
    private cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead head;
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.Body body;

    public GxpReqHead getHead() {
        return head;
    }

    public void setHead(GxpReqHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
