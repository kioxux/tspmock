package cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.req;

/**
 * 请求Service：线下查询接口
 *
 * @author leehuang
 * @version 1.0
 */
public class CredxxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "CredxxReqService{" +
				"service=" + service +
				'}';
	}
}
