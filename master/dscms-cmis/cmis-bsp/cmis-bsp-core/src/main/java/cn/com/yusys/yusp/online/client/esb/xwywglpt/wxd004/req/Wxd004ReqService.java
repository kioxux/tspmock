package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.req;

/**
 * 请求Service：信贷系统获取征信报送监管信息接口
 * @author code-generator
 * @version 1.0
 */
public class Wxd004ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
