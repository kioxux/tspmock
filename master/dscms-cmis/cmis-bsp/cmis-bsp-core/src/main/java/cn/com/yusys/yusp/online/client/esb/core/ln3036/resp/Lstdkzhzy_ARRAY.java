package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkzhzy.Record;

import java.util.List;

// 贷款账户质押信息
public class Lstdkzhzy_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkzhzy.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkzhzy_ARRAY{" +
                "record=" + record +
                '}';
    }
}
