package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req;

/**
 * 请求Service：数据修改接口
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号

    private String suvery_serno;//调查流水号
    private String loan_type;//业务类型
    private String cus_mgr_no;//客户经理工号
    private String spouse_name;//配偶名称
    private String is_whbxd;//是否无还本续贷

    private Guar_list guar_list;
    private Plage_list plage_list;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getSuvery_serno() {
        return suvery_serno;
    }

    public void setSuvery_serno(String suvery_serno) {
        this.suvery_serno = suvery_serno;
    }

    public String getLoan_type() {
        return loan_type;
    }

    public void setLoan_type(String loan_type) {
        this.loan_type = loan_type;
    }

    public String getCus_mgr_no() {
        return cus_mgr_no;
    }

    public void setCus_mgr_no(String cus_mgr_no) {
        this.cus_mgr_no = cus_mgr_no;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getIs_whbxd() {
        return is_whbxd;
    }

    public void setIs_whbxd(String is_whbxd) {
        this.is_whbxd = is_whbxd;
    }

    public Guar_list getGuar_list() {
        return guar_list;
    }

    public void setGuar_list(Guar_list guar_list) {
        this.guar_list = guar_list;
    }

    public Plage_list getPlage_list() {
        return plage_list;
    }

    public void setPlage_list(Plage_list plage_list) {
        this.plage_list = plage_list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", suvery_serno='" + suvery_serno + '\'' +
                ", loan_type='" + loan_type + '\'' +
                ", cus_mgr_no='" + cus_mgr_no + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", is_whbxd='" + is_whbxd + '\'' +
                ", guar_list=" + guar_list +
                ", plage_list=" + plage_list +
                '}';
    }
}
