package cn.com.yusys.yusp.online.client.esb.core.ln3111.resp;

import java.math.BigDecimal;

/**
 * 贷款归还试算
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private String dkjiejuh;//贷款借据号
    private String hetongbh;//合同编号
    private String kehuhaoo;//客户号
    private String kehuzwmc;//客户名
    private BigDecimal guihzcbj;//归还正常本金
    private BigDecimal guihyqbj;//归还逾期本金
    private BigDecimal guihdzbj;//归还呆滞本金
    private BigDecimal ghdzhabj;//归还呆账本金
    private BigDecimal ghysyjlx;//归还应收应计利息
    private BigDecimal ghcsyjlx;//归还催收应计利息
    private BigDecimal ghynshqx;//归还应收欠息
    private BigDecimal ghcushqx;//归还催收欠息
    private BigDecimal ghysyjfx;//归还应收应计罚息
    private BigDecimal ghcsyjfx;//归还催收应计罚息
    private BigDecimal ghynshfx;//归还应收罚息
    private BigDecimal ghcushfx;//归还催收罚息
    private BigDecimal ghyjfuxi;//归还应计复息
    private BigDecimal ghfeiyin;//归还费用
    private BigDecimal ghfxfuxi;//归还复息
    private BigDecimal guihzone;//归还总额
    private BigDecimal ghfajinn;//归还罚金
    private BigDecimal zhchbjin;//正常本金
    private BigDecimal yuqibjin;//逾期本金
    private BigDecimal dzhibjin;//呆滞本金
    private BigDecimal daizbjin;//呆账本金
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal csyjlixi;//催收应计利息
    private BigDecimal ysqianxi;//应收欠息
    private BigDecimal csqianxi;//催收欠息
    private BigDecimal ysyjfaxi;//应收应计罚息
    private BigDecimal csyjfaxi;//催收应计罚息
    private BigDecimal yshofaxi;//应收罚息
    private BigDecimal cshofaxi;//催收罚息
    private BigDecimal yingjifx;//应计复息
    private BigDecimal fuxiiiii;//复息
    private BigDecimal yingshfj;//应收罚金
    private BigDecimal yingshfy;//应收费用
    private String shifyyzq;//是否预约展期
    private String zhanqirq;//展期日期
    private String zhanqdqr;//展期到期日
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String schkriqi;//上次还款日
    private String daikxtai;//贷款形态
    private String huobdhao;//货币代号
    private BigDecimal fkjineee;//放款金额
    private BigDecimal leijyhbj;//累计已还本金
    private BigDecimal daikuyue;//贷款余额
    private BigDecimal leijyhlx;//累计已还利息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public BigDecimal getGuihzcbj() {
        return guihzcbj;
    }

    public void setGuihzcbj(BigDecimal guihzcbj) {
        this.guihzcbj = guihzcbj;
    }

    public BigDecimal getGuihyqbj() {
        return guihyqbj;
    }

    public void setGuihyqbj(BigDecimal guihyqbj) {
        this.guihyqbj = guihyqbj;
    }

    public BigDecimal getGuihdzbj() {
        return guihdzbj;
    }

    public void setGuihdzbj(BigDecimal guihdzbj) {
        this.guihdzbj = guihdzbj;
    }

    public BigDecimal getGhdzhabj() {
        return ghdzhabj;
    }

    public void setGhdzhabj(BigDecimal ghdzhabj) {
        this.ghdzhabj = ghdzhabj;
    }

    public BigDecimal getGhysyjlx() {
        return ghysyjlx;
    }

    public void setGhysyjlx(BigDecimal ghysyjlx) {
        this.ghysyjlx = ghysyjlx;
    }

    public BigDecimal getGhcsyjlx() {
        return ghcsyjlx;
    }

    public void setGhcsyjlx(BigDecimal ghcsyjlx) {
        this.ghcsyjlx = ghcsyjlx;
    }

    public BigDecimal getGhynshqx() {
        return ghynshqx;
    }

    public void setGhynshqx(BigDecimal ghynshqx) {
        this.ghynshqx = ghynshqx;
    }

    public BigDecimal getGhcushqx() {
        return ghcushqx;
    }

    public void setGhcushqx(BigDecimal ghcushqx) {
        this.ghcushqx = ghcushqx;
    }

    public BigDecimal getGhysyjfx() {
        return ghysyjfx;
    }

    public void setGhysyjfx(BigDecimal ghysyjfx) {
        this.ghysyjfx = ghysyjfx;
    }

    public BigDecimal getGhcsyjfx() {
        return ghcsyjfx;
    }

    public void setGhcsyjfx(BigDecimal ghcsyjfx) {
        this.ghcsyjfx = ghcsyjfx;
    }

    public BigDecimal getGhynshfx() {
        return ghynshfx;
    }

    public void setGhynshfx(BigDecimal ghynshfx) {
        this.ghynshfx = ghynshfx;
    }

    public BigDecimal getGhcushfx() {
        return ghcushfx;
    }

    public void setGhcushfx(BigDecimal ghcushfx) {
        this.ghcushfx = ghcushfx;
    }

    public BigDecimal getGhyjfuxi() {
        return ghyjfuxi;
    }

    public void setGhyjfuxi(BigDecimal ghyjfuxi) {
        this.ghyjfuxi = ghyjfuxi;
    }

    public BigDecimal getGhfeiyin() {
        return ghfeiyin;
    }

    public void setGhfeiyin(BigDecimal ghfeiyin) {
        this.ghfeiyin = ghfeiyin;
    }

    public BigDecimal getGhfxfuxi() {
        return ghfxfuxi;
    }

    public void setGhfxfuxi(BigDecimal ghfxfuxi) {
        this.ghfxfuxi = ghfxfuxi;
    }

    public BigDecimal getGuihzone() {
        return guihzone;
    }

    public void setGuihzone(BigDecimal guihzone) {
        this.guihzone = guihzone;
    }

    public BigDecimal getGhfajinn() {
        return ghfajinn;
    }

    public void setGhfajinn(BigDecimal ghfajinn) {
        this.ghfajinn = ghfajinn;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public String getShifyyzq() {
        return shifyyzq;
    }

    public void setShifyyzq(String shifyyzq) {
        this.shifyyzq = shifyyzq;
    }

    public String getZhanqirq() {
        return zhanqirq;
    }

    public void setZhanqirq(String zhanqirq) {
        this.zhanqirq = zhanqirq;
    }

    public String getZhanqdqr() {
        return zhanqdqr;
    }

    public void setZhanqdqr(String zhanqdqr) {
        this.zhanqdqr = zhanqdqr;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getFkjineee() {
        return fkjineee;
    }

    public void setFkjineee(BigDecimal fkjineee) {
        this.fkjineee = fkjineee;
    }

    public BigDecimal getLeijyhbj() {
        return leijyhbj;
    }

    public void setLeijyhbj(BigDecimal leijyhbj) {
        this.leijyhbj = leijyhbj;
    }

    public BigDecimal getDaikuyue() {
        return daikuyue;
    }

    public void setDaikuyue(BigDecimal daikuyue) {
        this.daikuyue = daikuyue;
    }

    public BigDecimal getLeijyhlx() {
        return leijyhlx;
    }

    public void setLeijyhlx(BigDecimal leijyhlx) {
        this.leijyhlx = leijyhlx;
    }

    @Override
    public String toString() {
        return "Ln3111RespDtoService{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "servsq='" + servsq + '\'' +
                "datasq='" + datasq + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehuzwmc='" + kehuzwmc + '\'' +
                "guihzcbj='" + guihzcbj + '\'' +
                "guihyqbj='" + guihyqbj + '\'' +
                "guihdzbj='" + guihdzbj + '\'' +
                "ghdzhabj='" + ghdzhabj + '\'' +
                "ghysyjlx='" + ghysyjlx + '\'' +
                "ghcsyjlx='" + ghcsyjlx + '\'' +
                "ghynshqx='" + ghynshqx + '\'' +
                "ghcushqx='" + ghcushqx + '\'' +
                "ghysyjfx='" + ghysyjfx + '\'' +
                "ghcsyjfx='" + ghcsyjfx + '\'' +
                "ghynshfx='" + ghynshfx + '\'' +
                "ghcushfx='" + ghcushfx + '\'' +
                "ghyjfuxi='" + ghyjfuxi + '\'' +
                "ghfeiyin='" + ghfeiyin + '\'' +
                "ghfxfuxi='" + ghfxfuxi + '\'' +
                "guihzone='" + guihzone + '\'' +
                "ghfajinn='" + ghfajinn + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "yuqibjin='" + yuqibjin + '\'' +
                "dzhibjin='" + dzhibjin + '\'' +
                "daizbjin='" + daizbjin + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "csyjlixi='" + csyjlixi + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "csqianxi='" + csqianxi + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "csyjfaxi='" + csyjfaxi + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "cshofaxi='" + cshofaxi + '\'' +
                "yingjifx='" + yingjifx + '\'' +
                "fuxiiiii='" + fuxiiiii + '\'' +
                "yingshfj='" + yingshfj + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "shifyyzq='" + shifyyzq + '\'' +
                "zhanqirq='" + zhanqirq + '\'' +
                "zhanqdqr='" + zhanqdqr + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "schkriqi='" + schkriqi + '\'' +
                "daikxtai='" + daikxtai + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "fkjineee='" + fkjineee + '\'' +
                "leijyhbj='" + leijyhbj + '\'' +
                "daikuyue='" + daikuyue + '\'' +
                "leijyhlx='" + leijyhlx + '\'' +
                '}';
    }
}  
