package cn.com.yusys.yusp.web.client.esb.core.ln3135;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3135.req.Ln3135ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3135.resp.Ln3135RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3135.req.Ln3135ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3135.resp.Ln3135RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3135)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3135Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3135Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * 交易码：ln3135
     * 交易描述：通过贷款借据号和贷款账号查询贷款期供计划
     *
     * @param ln3135ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3135:通过贷款借据号和贷款账号查询贷款期供计划")
    @PostMapping("/ln3135")
    protected @ResponseBody
    ResultDto<Ln3135RespDto> ln3135(@Validated @RequestBody Ln3135ReqDto ln3135ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3135.key, EsbEnum.TRADE_CODE_LN3135.value, JSON.toJSONString(ln3135ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3135.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3135.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3135.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3135.resp.Service();
        Ln3135ReqService ln3135ReqService = new Ln3135ReqService();
        Ln3135RespService ln3135RespService = new Ln3135RespService();
        Ln3135RespDto ln3135RespDto = new Ln3135RespDto();
        ResultDto<Ln3135RespDto> ln3135ResultDto = new ResultDto<Ln3135RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3135ReqDto转换成reqService
            BeanUtils.copyProperties(ln3135ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3135.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3135ReqService.setService(reqService);
            // 将ln3135ReqService转换成ln3135ReqServiceMap
            Map ln3135ReqServiceMap = beanMapUtil.beanToMap(ln3135ReqService);
            context.put("tradeDataMap", ln3135ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3135.key, EsbEnum.TRADE_CODE_LN3135.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3135.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3135.key, EsbEnum.TRADE_CODE_LN3135.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3135RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3135RespService.class, Ln3135RespService.class);
            respService = ln3135RespService.getService();
            //  将respService转换成Ln3135RespDto
            BeanUtils.copyProperties(respService, ln3135RespDto);
            ln3135ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3135ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3135RespDto
                BeanUtils.copyProperties(respService, ln3135RespDto);
                ln3135ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3135ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3135ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3135ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3135.key, EsbEnum.TRADE_CODE_LN3135.value, e.getMessage());
            ln3135ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3135ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3135ResultDto.setData(ln3135RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3135.key, EsbEnum.TRADE_CODE_LN3135.value, JSON.toJSONString(ln3135ResultDto));
        return ln3135ResultDto;
    }
}
