package cn.com.yusys.yusp.online.client.esb.core.ln3100.resp;

/**
 * 响应Service：对客借据与内部借据关联关系
 *
 * @author chenyong
 * @version 1.0
 */
public class Lstdknbgl {
    private String nbjiejuh;//内部借据号
    private String nbjjxzhi;//内部借据性质

    public String getNbjiejuh() {
        return nbjiejuh;
    }

    public void setNbjiejuh(String nbjiejuh) {
        this.nbjiejuh = nbjiejuh;
    }

    public String getNbjjxzhi() {
        return nbjjxzhi;
    }

    public void setNbjjxzhi(String nbjjxzhi) {
        this.nbjjxzhi = nbjjxzhi;
    }

    @Override
    public String toString() {
        return "Service{" +
                "nbjiejuh='" + nbjiejuh + '\'' +
                "nbjjxzhi='" + nbjjxzhi + '\'' +
                '}';
    }
}

