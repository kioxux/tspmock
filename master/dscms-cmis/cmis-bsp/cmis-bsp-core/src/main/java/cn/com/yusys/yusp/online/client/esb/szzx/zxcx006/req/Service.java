package cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.req;

/**
 * 响应Service：信贷查询地方征信接口
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:24
 */
public class Service {

    private String prcscd;//	处理码	,	是	,	接口交易码区分交易
    private String servtp;//	渠道	,	是	,	交易渠道(暂未确定，只保存值)
    private String servsq;//	渠道流水	,	是	,	由发起渠道生成的唯一标识
    private String userid;//	柜员号	,	是	,
    private String brchno;//	部门号	,	是	,
    private String state; // 请求类型
    private String reportReason; // 查询原因
    private String querystaffNo; // 客户经理工号
    private String querystaff; // 客户经理名称
    private String inqueryorgNo; // 查询机构号
    private String inqueryorg; // 查询机构名称
    private String taskId; // 任务ID
    private String cardCode; // 证件号码
    private String wsUser; // 征信提供的用户名

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public String getQuerystaffNo() {
        return querystaffNo;
    }

    public void setQuerystaffNo(String querystaffNo) {
        this.querystaffNo = querystaffNo;
    }

    public String getQuerystaff() {
        return querystaff;
    }

    public void setQuerystaff(String querystaff) {
        this.querystaff = querystaff;
    }

    public String getInqueryorgNo() {
        return inqueryorgNo;
    }

    public void setInqueryorgNo(String inqueryorgNo) {
        this.inqueryorgNo = inqueryorgNo;
    }

    public String getInqueryorg() {
        return inqueryorg;
    }

    public void setInqueryorg(String inqueryorg) {
        this.inqueryorg = inqueryorg;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getWsUser() {
        return wsUser;
    }

    public void setWsUser(String wsUser) {
        this.wsUser = wsUser;
    }

    @Override
    public String toString() {
        return "Zxcx006ReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", state='" + state + '\'' +
                ", reportReason='" + reportReason + '\'' +
                ", querystaffNo='" + querystaffNo + '\'' +
                ", querystaff='" + querystaff + '\'' +
                ", inqueryorgNo='" + inqueryorgNo + '\'' +
                ", inqueryorg='" + inqueryorg + '\'' +
                ", taskId='" + taskId + '\'' +
                ", cardCode='" + cardCode + '\'' +
                ", wsUser='" + wsUser + '\'' +
                '}';
    }
}
