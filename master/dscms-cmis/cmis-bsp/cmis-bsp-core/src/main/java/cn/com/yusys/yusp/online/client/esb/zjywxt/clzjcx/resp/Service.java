package cn.com.yusys.yusp.online.client.esb.zjywxt.clzjcx.resp;

import java.math.BigDecimal;

/**
 * 响应Service：未备注
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private String savvou;//监管协议号（缴款凭证编号）
    private String contno;//合同编号
    private String acctno;//监管账号
    private String acctna;//监管账户名称
    private BigDecimal totamt;//监管总额
    private BigDecimal payamt;//已交金额
    private String byidna;//买方姓名（买方人名称）
    private String byidno;//买方人证件号码

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getSavvou() {
        return savvou;
    }

    public void setSavvou(String savvou) {
        this.savvou = savvou;
    }

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctna() {
        return acctna;
    }

    public void setAcctna(String acctna) {
        this.acctna = acctna;
    }

    public BigDecimal getTotamt() {
        return totamt;
    }

    public void setTotamt(BigDecimal totamt) {
        this.totamt = totamt;
    }

    public BigDecimal getPayamt() {
        return payamt;
    }

    public void setPayamt(BigDecimal payamt) {
        this.payamt = payamt;
    }

    public String getByidna() {
        return byidna;
    }

    public void setByidna(String byidna) {
        this.byidna = byidna;
    }

    public String getByidno() {
        return byidno;
    }

    public void setByidno(String byidno) {
        this.byidno = byidno;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", savvou='" + savvou + '\'' +
                ", contno='" + contno + '\'' +
                ", acctno='" + acctno + '\'' +
                ", acctna='" + acctna + '\'' +
                ", totamt=" + totamt +
                ", payamt=" + payamt +
                ", byidna='" + byidna + '\'' +
                ", byidno='" + byidno + '\'' +
                '}';
    }
}
