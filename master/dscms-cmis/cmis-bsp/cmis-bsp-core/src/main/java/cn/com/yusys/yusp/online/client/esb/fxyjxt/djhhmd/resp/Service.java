package cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.resp;

/**
 * 响应Service：查询客户风险预警等级与是否黑灰名单
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String custRiskStatus;//风险预警等级
    private String ifBlack;//是否黑名单
    private String ifGrey;//是否灰名单

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCustRiskStatus() {
        return custRiskStatus;
    }

    public void setCustRiskStatus(String custRiskStatus) {
        this.custRiskStatus = custRiskStatus;
    }

    public String getIfBlack() {
        return ifBlack;
    }

    public void setIfBlack(String ifBlack) {
        this.ifBlack = ifBlack;
    }

    public String getIfGrey() {
        return ifGrey;
    }

    public void setIfGrey(String ifGrey) {
        this.ifGrey = ifGrey;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "custRiskStatus='" + custRiskStatus + '\'' +
                "ifBlack='" + ifBlack + '\'' +
                "ifGrey='" + ifGrey + '\'' +
                '}';
    }
}

