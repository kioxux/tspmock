package cn.com.yusys.yusp.web.client.esb.core.ln3074;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3074.req.Ln3074ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3074.resp.Ln3074RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3074.req.Ln3074ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3074.resp.Ln3074RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3074)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3074Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3074Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3074
     * 交易描述：交易说明
     *
     * @param ln3074ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3074:用于贷款预约展期")
    @PostMapping("/ln3074")
    protected @ResponseBody
    ResultDto<Ln3074RespDto> ln3074(@Validated @RequestBody Ln3074ReqDto ln3074ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3074.key, EsbEnum.TRADE_CODE_LN3074.value, JSON.toJSONString(ln3074ReqDto, SerializerFeature.WriteMapNullValue));
        cn.com.yusys.yusp.online.client.esb.core.ln3074.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3074.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3074.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3074.resp.Service();
        Ln3074ReqService ln3074ReqService = new Ln3074ReqService();
        Ln3074RespService ln3074RespService = new Ln3074RespService();
        Ln3074RespDto ln3074RespDto = new Ln3074RespDto();
        ResultDto<Ln3074RespDto> ln3074ResultDto = new ResultDto<Ln3074RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3074ReqDto转换成reqService
            BeanUtils.copyProperties(ln3074ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3074.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(ln3074ReqDto.getBrchno());//    部门号,取账务机构号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3074ReqService.setService(reqService);
            // 将ln3074ReqService转换成ln3074ReqServiceMap
            Map ln3074ReqServiceMap = beanMapUtil.beanToMap(ln3074ReqService);
            Map service = (Map) ln3074ReqServiceMap.get("service");
            // bigdecimal字段转成map之后出现科学计数法问题解决 modify by chenyong 陈勇
            service.forEach((k, v) -> {
                if (v instanceof Double) {
                    BigDecimal bigDecimal = new BigDecimal(((Double) v));
                    service.put(k, bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });
            ln3074ReqServiceMap.put("service", service);

            context.put("tradeDataMap", ln3074ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3074.key, EsbEnum.TRADE_CODE_LN3074.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3074.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3074.key, EsbEnum.TRADE_CODE_LN3074.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3074RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3074RespService.class, Ln3074RespService.class);
            respService = ln3074RespService.getService();

            ln3074ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3074ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3074RespDto
                BeanUtils.copyProperties(respService, ln3074RespDto);
                ln3074ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3074ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3074ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3074ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3074.key, EsbEnum.TRADE_CODE_LN3074.value, e.getMessage());
            ln3074ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3074ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3074ResultDto.setData(ln3074RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3074.key, EsbEnum.TRADE_CODE_LN3074.value, JSON.toJSONString(ln3074ResultDto, SerializerFeature.WriteMapNullValue));

        return ln3074ResultDto;
    }
}
