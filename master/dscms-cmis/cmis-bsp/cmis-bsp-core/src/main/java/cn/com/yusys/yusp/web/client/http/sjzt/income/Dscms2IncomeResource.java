package cn.com.yusys.yusp.web.client.http.sjzt.income;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.sjzt.income.req.IncomeReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.income.resp.IncomeRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.sjzt.income.IncomeReqService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(income)")
@RestController
@RequestMapping("/api/dscms2sjzt")
public class Dscms2IncomeResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2IncomeResource.class);
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 信贷客户核心业绩统计查询列表
     *
     * @param incomeReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("income:资产收益明细查询")
    @PostMapping("/income")
    protected @ResponseBody
    ResultDto<IncomeRespDto> income(@Validated @RequestBody IncomeReqDto incomeReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_INCOME.key, EsbEnum.TRADE_CODE_INCOME.value, JSON.toJSONString(incomeReqDto));

        IncomeReqService incomeReqService = new IncomeReqService();

        IncomeRespDto incomeRespDto = new IncomeRespDto();
        ResultDto<IncomeRespDto> incomeResultDto = new ResultDto<IncomeRespDto>();
        try {
            // 将IncomeReqDto转换成IncomeReqService
            BeanUtils.copyProperties(incomeReqDto, incomeReqService);

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            incomeReqService.setServsq(servsq);//    渠道流水

//            incomeReqService.setUserid(EsbEnum.BRCHNO_XWYWGLPT.key);//    柜员号
//            incomeReqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(incomeReqService), headers);
            String url = "http://sc-gateway.zrcbank.ingress/credit/cus/income";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_INCOME.key, EsbEnum.TRADE_CODE_INCOME.value, JSON.toJSONString(incomeReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_INCOME.key, EsbEnum.TRADE_CODE_INCOME.value, responseEntity);

            if (responseEntity.getStatusCodeValue() == 200) {
                String responseStr = responseEntity.getBody();
                JSONObject jsonObject = JSONObject.parseObject(responseStr);
                String status = jsonObject.get("status").toString();//数据中台成功标识 100成功
                String message = jsonObject.get("message").toString();
                if (status.equals("100")) {
                    String data = jsonObject.get("data").toString();
                    incomeRespDto = JSON.parseObject(data, IncomeRespDto.class);
                    incomeResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                    incomeResultDto.setMessage(message);
                } else {
                    incomeResultDto.setCode(EpbEnum.EPB099999.key);
                    incomeResultDto.setMessage(message);
                }
            } else {
                incomeResultDto.setCode(EpbEnum.EPB099999.key);//9999
                incomeResultDto.setMessage("交易失败");//系统异常
            }


        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_INCOME.key, EsbEnum.TRADE_CODE_INCOME.value, e.getMessage());
            incomeResultDto.setCode(EpbEnum.EPB099999.key);//9999
            incomeResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        incomeResultDto.setData(incomeRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_INCOME.key, EsbEnum.TRADE_CODE_INCOME.value, JSON.toJSONString(incomeResultDto));
        return incomeResultDto;
    }
}
