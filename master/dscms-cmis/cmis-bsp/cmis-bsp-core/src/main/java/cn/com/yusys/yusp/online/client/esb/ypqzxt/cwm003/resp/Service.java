package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.resp;

/**
 * 响应Service：信贷查询地方征信接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:42
 */
public class Service {

    private String erorcd; // 响应码,0000表示成功其它表示失败
    private String erortx; // 响应信息,失败详情
    private String returnCode; // 处理结果
    private String returnInfo; // 处理信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnInfo() {
        return returnInfo;
    }

    public void setReturnInfo(String returnInfo) {
        this.returnInfo = returnInfo;
    }

    @Override
    public String toString() {
        return "Cwm002RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", returnInfo='" + returnInfo + '\'' +
                '}';
    }
}
