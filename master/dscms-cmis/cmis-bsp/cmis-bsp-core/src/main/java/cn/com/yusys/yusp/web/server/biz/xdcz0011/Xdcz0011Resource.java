package cn.com.yusys.yusp.web.server.biz.xdcz0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0011.req.Xdcz0011ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0011.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0011.resp.Xdcz0011RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0011.req.Xdcz0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0011.resp.Xdcz0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:省心E付放款记录列表查询
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0011:省心E付放款记录列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0011Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0011
     * 交易描述：省心E付放款记录列表查询
     *
     * @param xdcz0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("省心E付放款记录列表查询")
    @PostMapping("/xdcz0011")
//   @Idempotent({"xdcz0011", "#xdcz0011ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0011RespDto xdcz0011(@Validated @RequestBody Xdcz0011ReqDto xdcz0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, JSON.toJSONString(xdcz0011ReqDto));
        Xdcz0011DataReqDto xdcz0011DataReqDto = new Xdcz0011DataReqDto();// 请求Data： 电子保函开立
        Xdcz0011DataRespDto xdcz0011DataRespDto = new Xdcz0011DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0011.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0011RespDto xdcz0011RespDto = new Xdcz0011RespDto();
        try {
            // 从 xdcz0011ReqDto获取 reqData
            reqData = xdcz0011ReqDto.getData();
            // 将 reqData 拷贝到xdcz0011DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0011DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, JSON.toJSONString(xdcz0011DataReqDto));
            ResultDto<Xdcz0011DataRespDto> xdcz0011DataResultDto = dscmsBizCzClientService.xdcz0011(xdcz0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, JSON.toJSONString(xdcz0011DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0011RespDto.setErorcd(Optional.ofNullable(xdcz0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0011RespDto.setErortx(Optional.ofNullable(xdcz0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0011DataResultDto.getCode())) {
                xdcz0011DataRespDto = xdcz0011DataResultDto.getData();
                xdcz0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0011DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, e.getMessage());
            xdcz0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0011RespDto.setDatasq(servsq);

        xdcz0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0011.key, DscmsEnum.TRADE_CODE_XDCZ0011.value, JSON.toJSONString(xdcz0011RespDto));
        return xdcz0011RespDto;
    }
}
