package cn.com.yusys.yusp.online.client.esb.core.ln3135.resp;

import java.math.BigDecimal;

/**
 * 响应Service：通过贷款借据号和贷款账号查询贷款期供计划
 *
 * @author chenyong
 * @version 1.0
 */
public class Listnm {
    private String dkjiejuh;//贷款借据号
    private Integer benqqish;//本期期数
    private String qishriqi;//起始日期
    private String daoqriqi;//到期日期
    private BigDecimal huanbjee;//还本金额
    private BigDecimal hxijinee;//还息金额
    private String chulizht;//处理状态
    private String zwhkriqi;//最晚还款日

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getChulizht() {
        return chulizht;
    }

    public void setChulizht(String chulizht) {
        this.chulizht = chulizht;
    }

    public String getZwhkriqi() {
        return zwhkriqi;
    }

    public void setZwhkriqi(String zwhkriqi) {
        this.zwhkriqi = zwhkriqi;
    }

    @Override
    public String toString() {
        return "Service{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "benqqish='" + benqqish + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "huanbjee='" + huanbjee + '\'' +
                "hxijinee='" + hxijinee + '\'' +
                "chulizht='" + chulizht + '\'' +
                "zwhkriqi='" + zwhkriqi + '\'' +
                '}';
    }
}
