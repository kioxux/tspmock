package cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp;

/**
 * 响应Service：查询还款（合约）明细历史表（利翃）中的全量借据一览
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String contract_no;//利翃平台贷款合同号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Record{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}
