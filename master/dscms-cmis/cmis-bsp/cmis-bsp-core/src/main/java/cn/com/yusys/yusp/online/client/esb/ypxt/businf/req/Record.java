package cn.com.yusys.yusp.online.client.esb.ypxt.businf.req;

import java.math.BigDecimal;

/**
 * 请求Service：信贷业务合同信息同步
 */
public class Record {
    private String ywhtbh;//业务合同编号
    private String sxxybh;//授信协议号
    private String jkrkhh;//借款人客户码
    private BigDecimal ywhtje;//业务合同金额
    private String ywhtbz;//业务合同币种
    private BigDecimal htyeyp;//合同余额
    private BigDecimal ywck;//业务敞口
    private String ywpzyp;//业务品种
    private String dbfsyp;//担保方式
    private BigDecimal bzjbly;//保证金比例
    private String ksrqyp;//开始日期
    private String dqrqyp;//到期日期
    private String ywhtzt;//业务合同状态
    private String khghjl;//管户客户经理
    private String khghjg;//管理机构


	public String getYwhtbh() {
        return ywhtbh;
    }

    public void setYwhtbh(String ywhtbh) {
        this.ywhtbh = ywhtbh;
    }

    public String getSxxybh() {
        return sxxybh;
    }

    public void setSxxybh(String sxxybh) {
        this.sxxybh = sxxybh;
    }

    public String getJkrkhh() {
        return jkrkhh;
    }

    public void setJkrkhh(String jkrkhh) {
        this.jkrkhh = jkrkhh;
    }

    public BigDecimal getYwhtje() {
        return ywhtje;
    }

    public void setYwhtje(BigDecimal ywhtje) {
        this.ywhtje = ywhtje;
    }

    public String getYwhtbz() {
        return ywhtbz;
    }

    public void setYwhtbz(String ywhtbz) {
        this.ywhtbz = ywhtbz;
    }

    public BigDecimal getHtyeyp() {
        return htyeyp;
    }

    public void setHtyeyp(BigDecimal htyeyp) {
        this.htyeyp = htyeyp;
    }

    public BigDecimal getYwck() {
        return ywck;
    }

    public void setYwck(BigDecimal ywck) {
        this.ywck = ywck;
    }

    public String getYwpzyp() {
        return ywpzyp;
    }

    public void setYwpzyp(String ywpzyp) {
        this.ywpzyp = ywpzyp;
    }

    public String getDbfsyp() {
        return dbfsyp;
    }

    public void setDbfsyp(String dbfsyp) {
        this.dbfsyp = dbfsyp;
    }

    public BigDecimal getBzjbly() {
        return bzjbly;
    }

    public void setBzjbly(BigDecimal bzjbly) {
        this.bzjbly = bzjbly;
    }

    public String getKsrqyp() {
        return ksrqyp;
    }

    public void setKsrqyp(String ksrqyp) {
        this.ksrqyp = ksrqyp;
    }

    public String getDqrqyp() {
        return dqrqyp;
    }

    public void setDqrqyp(String dqrqyp) {
        this.dqrqyp = dqrqyp;
    }

    public String getYwhtzt() {
        return ywhtzt;
    }

    public void setYwhtzt(String ywhtzt) {
        this.ywhtzt = ywhtzt;
    }

    public String getKhghjl() {
        return khghjl;
    }

    public void setKhghjl(String khghjl) {
        this.khghjl = khghjl;
    }

    public String getKhghjg() {
        return khghjg;
    }

    public void setKhghjg(String khghjg) {
        this.khghjg = khghjg;
    }

	@Override
	public String toString() {
		return "Record{" +
				", ywhtbh='" + ywhtbh + '\'' +
				", sxxybh='" + sxxybh + '\'' +
				", jkrkhh='" + jkrkhh + '\'' +
				", ywhtje=" + ywhtje +
				", ywhtbz='" + ywhtbz + '\'' +
				", htyeyp=" + htyeyp +
				", ywck=" + ywck +
				", ywpzyp='" + ywpzyp + '\'' +
				", dbfsyp='" + dbfsyp + '\'' +
				", bzjbly=" + bzjbly +
				", ksrqyp='" + ksrqyp + '\'' +
				", dqrqyp='" + dqrqyp + '\'' +
				", ywhtzt='" + ywhtzt + '\'' +
				", khghjl='" + khghjl + '\'' +
				", khghjg='" + khghjg + '\'' +
				'}';
	}
}
