package cn.com.yusys.yusp.online.client.esb.core.dp2667.req;

import java.io.Serializable;

/**
 * 请求Dto：组合账户特殊账户查询
 *
 * @author lihh
 * @version 1.0
 */
public class Service implements Serializable {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servdt;//交易日期
    private String servti;//交易时间
    private String kehuzhao;//客户账号
    private String tszhzhlx;//特殊组合账户类型
    private String huobdaih;//货币代号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getTszhzhlx() {
        return tszhzhlx;
    }

    public void setTszhzhlx(String tszhzhlx) {
        this.tszhzhlx = tszhzhlx;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "kehuzhao='" + kehuzhao + '\'' +
                "tszhzhlx='" + tszhzhlx + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                '}';
    }
}  
