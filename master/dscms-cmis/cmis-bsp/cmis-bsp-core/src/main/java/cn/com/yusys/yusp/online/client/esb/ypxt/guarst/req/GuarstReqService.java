package cn.com.yusys.yusp.online.client.esb.ypxt.guarst.req;

/**
 * 请求Service：押品状态变更推送
 *
 * @author lihh
 * @version 1.0
 */
public class GuarstReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
