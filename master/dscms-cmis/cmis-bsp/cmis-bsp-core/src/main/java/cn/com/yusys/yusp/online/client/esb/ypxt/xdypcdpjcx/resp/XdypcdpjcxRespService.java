package cn.com.yusys.yusp.online.client.esb.ypxt.xdypcdpjcx.resp;

/**
 * 响应Service：查询存单票据信息
 * @author zhangpeng
 * @version 1.0             
 */      
public class XdypcdpjcxRespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      

