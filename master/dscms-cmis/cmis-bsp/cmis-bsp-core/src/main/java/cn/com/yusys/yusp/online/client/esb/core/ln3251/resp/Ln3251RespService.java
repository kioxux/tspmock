package cn.com.yusys.yusp.online.client.esb.core.ln3251.resp;

/**
 * 响应Service：贷款自动追缴明细查询
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3251RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
