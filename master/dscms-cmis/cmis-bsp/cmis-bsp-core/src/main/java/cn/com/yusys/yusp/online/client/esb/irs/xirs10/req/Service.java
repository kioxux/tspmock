package cn.com.yusys.yusp.online.client.esb.irs.xirs10.req;

/**
 * 请求Service：公司客户信息
 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String custid;//客户编号
    private String custname;//客户名称
    private String certtype;//证件类型
    private String certid;//证件号码
    private String custtype;//客户类型
    private String inputuserid;//主管客户经理编号
    private String inputorgid;//主管客户经理所属机构编号
    private String industrycode;//所属国标行业
    private String inputqx;//主管客户经理权限
    private String inputusername;//主管客户经理名称

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCerttype() {
        return certtype;
    }

    public void setCerttype(String certtype) {
        this.certtype = certtype;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getCusttype() {
        return custtype;
    }

    public void setCusttype(String custtype) {
        this.custtype = custtype;
    }

    public String getInputuserid() {
        return inputuserid;
    }

    public void setInputuserid(String inputuserid) {
        this.inputuserid = inputuserid;
    }

    public String getInputorgid() {
        return inputorgid;
    }

    public void setInputorgid(String inputorgid) {
        this.inputorgid = inputorgid;
    }

    public String getIndustrycode() {
        return industrycode;
    }

    public void setIndustrycode(String industrycode) {
        this.industrycode = industrycode;
    }

    public String getInputqx() {
        return inputqx;
    }

    public void setInputqx(String inputqx) {
        this.inputqx = inputqx;
    }

    public String getInputusername() {
        return inputusername;
    }

    public void setInputusername(String inputusername) {
        this.inputusername = inputusername;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", custid='" + custid + '\'' +
                ", custname='" + custname + '\'' +
                ", certtype='" + certtype + '\'' +
                ", certid='" + certid + '\'' +
                ", custtype='" + custtype + '\'' +
                ", inputuserid='" + inputuserid + '\'' +
                ", inputorgid='" + inputorgid + '\'' +
                ", industrycode='" + industrycode + '\'' +
                ", inputqx='" + inputqx + '\'' +
                ", inputusername='" + inputusername + '\'' +
                '}';
    }
}
