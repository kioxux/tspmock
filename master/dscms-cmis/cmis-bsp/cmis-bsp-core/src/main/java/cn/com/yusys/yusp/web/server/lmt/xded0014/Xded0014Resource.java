package cn.com.yusys.yusp.web.server.lmt.xded0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.req.CmisLmt0014ReqdealBizListDto;
import cn.com.yusys.yusp.dto.server.cmislmt0014.resp.CmisLmt0014RespDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0014.req.DealBizList;
import cn.com.yusys.yusp.dto.server.lmt.xded0014.req.Xded0014ReqDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0014.resp.Xded0014RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:台账恢复接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDED0014:台账恢复接口")
@RestController
@RequestMapping("/api/dscms")
public class Xded0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xded0014Resource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：cmisLmt0014
     * 交易描述：台账恢复接口
     *
     * @param xded0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账恢复接口")
    @PostMapping("/xded0014")
    //  @Idempotent({"xded0014", "#xded0014ReqDto.datasq"})
    protected @ResponseBody
    Xded0014RespDto cmisLmt0014(@Validated @RequestBody Xded0014ReqDto xded0014ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0014.key, DscmsEnum.TRADE_CODE_XDED0014.value, JSON.toJSONString(xded0014ReqDto));
        CmisLmt0014ReqDto cmisLmt0014ReqDto = new CmisLmt0014ReqDto();// 请求Data： 台账恢复接口
        CmisLmt0014RespDto cmisLmt0014RespDto = new CmisLmt0014RespDto();// 响应Data：台账恢复接口

        Xded0014RespDto xded0014RespDto = new Xded0014RespDto();

        cn.com.yusys.yusp.dto.server.lmt.xded0014.req.Data reqData = null; // 请求Data：台账恢复接口
        cn.com.yusys.yusp.dto.server.lmt.xded0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.xded0014.resp.Data();// 响应Data：台账恢复接口

        try {
            // 从 Xded0014Dto获取 reqData
            reqData = xded0014ReqDto.getData();
            // 将 reqData 拷贝到cmisLmt0014DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0014ReqDto);
            List<DealBizList> dealBizList = reqData.getDealBizList();
            if (CollectionUtils.nonEmpty(dealBizList)) {
                List<CmisLmt0014ReqdealBizListDto> collect = dealBizList.parallelStream().map(dealBiz -> {
                    CmisLmt0014ReqdealBizListDto cmisLmt0014ReqdealBizListDto = new CmisLmt0014ReqdealBizListDto();
                    BeanUtils.copyProperties(dealBiz, cmisLmt0014ReqdealBizListDto);
                    return cmisLmt0014ReqdealBizListDto;
                }).collect(Collectors.toList());
                cmisLmt0014ReqDto.setDealBizList(collect);
            }
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, JSON.toJSONString(cmisLmt0014ReqDto));
            ResultDto<CmisLmt0014RespDto> cmisLmt0014RespDtoResultDto = cmisLmtClientService.cmisLmt0014(cmisLmt0014ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0014.value, JSON.toJSONString(cmisLmt0014RespDtoResultDto));
            // 从返回值中获取响应码和响应消息
            xded0014RespDto.setErorcd(Optional.ofNullable(cmisLmt0014RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xded0014RespDto.setErortx(Optional.ofNullable(cmisLmt0014RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0014RespDtoResultDto.getCode())) {
                cmisLmt0014RespDto = cmisLmt0014RespDtoResultDto.getData();
                xded0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xded0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0014RespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0014.key, DscmsEnum.TRADE_CODE_XDED0014.value, e.getMessage());
            xded0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xded0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xded0014RespDto.setDatasq(servsq);

        xded0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0014.key, DscmsEnum.TRADE_CODE_XDED0014.value, JSON.toJSONString(xded0014RespDto));
        return xded0014RespDto;
    }
}
