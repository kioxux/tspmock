package cn.com.yusys.yusp.web.server.biz.xdtz0020;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0020.req.Xdtz0020ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0020.resp.Xdtz0020RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0020.req.Xdtz0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0020.resp.Xdtz0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:台账入账（保函）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0020:台账入账（保函）")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0020Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0020
     * 交易描述：台账入账（保函）
     *
     * @param xdtz0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账入账（保函）")
    @PostMapping("/xdtz0020")
    //@Idempotent({"xdcatz0020", "#xdtz0020ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0020RespDto xdtz0020(@Validated @RequestBody Xdtz0020ReqDto xdtz0020ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, JSON.toJSONString(xdtz0020ReqDto));
        Xdtz0020DataReqDto xdtz0020DataReqDto = new Xdtz0020DataReqDto();// 请求Data： 台账入账（保函）
        Xdtz0020DataRespDto xdtz0020DataRespDto = new Xdtz0020DataRespDto();// 响应Data：台账入账（保函）
        Xdtz0020RespDto xdtz0020RespDto = new Xdtz0020RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0020.req.Data reqData = null; // 请求Data：台账入账（保函）
        cn.com.yusys.yusp.dto.server.biz.xdtz0020.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0020.resp.Data();// 响应Data：台账入账（保函）
        try {
            // 从 xdtz0020ReqDto获取 reqData
            reqData = xdtz0020ReqDto.getData();
            // 将 reqData 拷贝到xdtz0020DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0020DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, JSON.toJSONString(xdtz0020DataReqDto));
            ResultDto<Xdtz0020DataRespDto> xdtz0020DataResultDto = dscmsBizTzClientService.xdtz0020(xdtz0020DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, JSON.toJSONString(xdtz0020DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0020RespDto.setErorcd(Optional.ofNullable(xdtz0020DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0020RespDto.setErortx(Optional.ofNullable(xdtz0020DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0020DataResultDto.getCode())) {
                xdtz0020DataRespDto = xdtz0020DataResultDto.getData();
                xdtz0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0020DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, e.getMessage());
            xdtz0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0020RespDto.setDatasq(servsq);

        xdtz0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0020.key, DscmsEnum.TRADE_CODE_XDTZ0020.value, JSON.toJSONString(xdtz0020RespDto));
        return xdtz0020RespDto;
    }
}
