package cn.com.yusys.yusp.web.client.esb.rircp.fbxw05;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw05.req.Fbxw05ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw05.resp.Fbxw05RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxw05）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw05Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw05Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB惠享贷批复同步接口（处理码fbxw05）
     *
     * @param fbxw05ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw05:ESB惠享贷批复同步接口")
    @PostMapping("/fbxw05")
    protected @ResponseBody
    ResultDto<Fbxw05RespDto> fbxw05(@Validated @RequestBody Fbxw05ReqDto fbxw05ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value, JSON.toJSONString(fbxw05ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw05.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw05.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw05.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw05.resp.Service();
        Fbxw05ReqService fbxw05ReqService = new Fbxw05ReqService();
        Fbxw05RespService fbxw05RespService = new Fbxw05RespService();
        Fbxw05RespDto fbxw05RespDto = new Fbxw05RespDto();
        ResultDto<Fbxw05RespDto> fbxw05ResultDto = new ResultDto<Fbxw05RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw05ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw05ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW05.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw05ReqService.setService(reqService);
            // 将fbxw05ReqService转换成fbxw05ReqServiceMap
            Map fbxw05ReqServiceMap = beanMapUtil.beanToMap(fbxw05ReqService);
            context.put("tradeDataMap", fbxw05ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW05.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw05RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw05RespService.class, Fbxw05RespService.class);
            respService = fbxw05RespService.getService();

            //  将Fbxw05RespDto封装到ResultDto<Fbxw05RespDto>
            fbxw05ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw05ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw05RespDto
                BeanUtils.copyProperties(respService, fbxw05RespDto);
                fbxw05ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw05ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw05ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw05ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value, e.getMessage());
            fbxw05ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw05ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw05ResultDto.setData(fbxw05RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW05.key, EsbEnum.TRADE_CODE_FBXW05.value, JSON.toJSONString(fbxw05ResultDto));
        return fbxw05ResultDto;
    }
}
