package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.req;

/**
 * 请求Service：查询共有人信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xddb06ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xddb06ReqService{" +
                "service=" + service +
                '}';
    }
}
