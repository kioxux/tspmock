package cn.com.yusys.yusp.online.client.esb.core.ln3107.req;


/**
 * 请求Service：贷款形态转移明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3107ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

