package cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp;

/**
 * 响应Service：获取指定数据日期存在还款记录的借据一览信息
 *
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
