package cn.com.yusys.yusp.online.client.esb.core.co3202.resp;

/**
 * 响应Service：抵质押物的开户
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 17:13
 */
public class Co3202RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
