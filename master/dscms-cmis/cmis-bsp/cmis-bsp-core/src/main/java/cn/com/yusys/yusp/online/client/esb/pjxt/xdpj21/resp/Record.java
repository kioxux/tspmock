package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 21:31
 * @since 2021/5/28 21:31
 */
public class Record {
    private String acctno;//账号
    private String acctseq;//保证金账号子序号
    private String currency;//币种
    private BigDecimal acctAmt;//账户金额
    private String interestMode;//计息方式

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctseq() {
        return acctseq;
    }

    public void setAcctseq(String acctseq) {
        this.acctseq = acctseq;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAcctAmt() {
        return acctAmt;
    }

    public void setAcctAmt(BigDecimal acctAmt) {
        this.acctAmt = acctAmt;
    }

    public String getInterestMode() {
        return interestMode;
    }

    public void setInterestMode(String interestMode) {
        this.interestMode = interestMode;
    }

    @Override
    public String toString() {
        return "Service{" +
                "acctno='" + acctno + '\'' +
                "acctseq='" + acctseq + '\'' +
                "currency='" + currency + '\'' +
                "acctAmt='" + acctAmt + '\'' +
                "interestMode='" + interestMode + '\'' +
                '}';
    }
}
