package cn.com.yusys.yusp.web.client.esb.core.mbt951;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.req.Mbt951ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.resp.Mbt951RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.mbt951.req.Mbt951ReqService;
import cn.com.yusys.yusp.online.client.esb.core.mbt951.resp.Mbt951RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:批量文件处理申请
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(mbt951)")
@RestController
@RequestMapping("/api/dscms2corembt")
public class Mbt951Resource {
    private static final Logger logger = LoggerFactory.getLogger(Mbt951Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：mbt951
     * 交易描述：批量文件处理申请
     *
     * @param mbt951ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("mbt951:批量文件处理申请")
    @PostMapping("/mbt951")
    protected @ResponseBody
    ResultDto<Mbt951RespDto> mbt951(@Validated @RequestBody Mbt951ReqDto mbt951ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT951.key, EsbEnum.TRADE_CODE_MBT951.value, JSON.toJSONString(mbt951ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.mbt951.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.mbt951.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.mbt951.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.mbt951.resp.Service();
        Mbt951ReqService mbt951ReqService = new Mbt951ReqService();
        Mbt951RespService mbt951RespService = new Mbt951RespService();
        Mbt951RespDto mbt951RespDto = new Mbt951RespDto();
        ResultDto<Mbt951RespDto> mbt951ResultDto = new ResultDto<Mbt951RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将mbt951ReqDto转换成reqService
            BeanUtils.copyProperties(mbt951ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_MBT951.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            mbt951ReqService.setService(reqService);
            // 将mbt951ReqService转换成mbt951ReqServiceMap
            Map mbt951ReqServiceMap = beanMapUtil.beanToMap(mbt951ReqService);
            context.put("tradeDataMap", mbt951ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT951.key, EsbEnum.TRADE_CODE_MBT951.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_MBT951.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT951.key, EsbEnum.TRADE_CODE_MBT951.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            mbt951RespService = beanMapUtil.mapToBean(tradeDataMap, Mbt951RespService.class, Mbt951RespService.class);
            respService = mbt951RespService.getService();
            //  将respService转换成Mbt951RespDto
            BeanUtils.copyProperties(respService, mbt951RespDto);
            mbt951ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            mbt951ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {

                BeanUtils.copyProperties(respService, mbt951RespDto);

                mbt951ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                mbt951ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                mbt951ResultDto.setCode(EpbEnum.EPB099999.key);
                mbt951ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT951.key, EsbEnum.TRADE_CODE_MBT951.value, e.getMessage());
            mbt951ResultDto.setCode(EpbEnum.EPB099999.key);//9519
            mbt951ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        mbt951ResultDto.setData(mbt951RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT951.key, EsbEnum.TRADE_CODE_MBT951.value, JSON.toJSONString(mbt951ResultDto));
        return mbt951ResultDto;
    }
}
