package cn.com.yusys.yusp.online.client.esb.core.ln3030.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdkhkzh.Record;

import java.util.List;

public class Lstdkhkzh_ARRAY {

    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhkzh{" +
                "record=" + record +
                '}';
    }
}
