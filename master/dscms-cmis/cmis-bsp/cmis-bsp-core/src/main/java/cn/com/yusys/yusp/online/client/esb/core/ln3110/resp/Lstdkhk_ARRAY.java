package cn.com.yusys.yusp.online.client.esb.core.ln3110.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdkhk.Record;

import java.util.List;

/**
 * 响应Service：还款计划
 *
 * @author code-generator
 * @version 1.0
 */
public class Lstdkhk_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdkhk.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhk_ARRAY{" +
                "record=" + record +
                '}';
    }
}
