package cn.com.yusys.yusp.web.client.gxp.tonglian.d15011;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.resp.D15011RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d15011)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D15011Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D15011Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 永久额度调整（处理码d15011）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d15011:永久额度调整")
    @PostMapping("/d15011")
    protected @ResponseBody
    ResultDto<D15011RespDto> d15011(@Validated @RequestBody D15011ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D15011.key, GxpEnum.TRADE_CODE_D15011.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.D15011ReqMessage d15011ReqMessage = null;//请求Message：永久额度调整
        cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.resp.D15011RespMessage d15011RespMessage = new D15011RespMessage();//响应Message：永久额度调整
        cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.Message reqMessage = null;//请求Message：永久额度调整
        cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.resp.Message();//响应Message：永久额度调整
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：永久额度调整
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：永久额度调整
        cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.Body();//请求Body：永久额度调整
        cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.resp.Body();//响应Body：永久额度调整

        cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011RespDto d15011RespDto = new D15011RespDto();//响应Dto：永久额度调整
        ResultDto<D15011RespDto> d15011ResultDto = new ResultDto<>();//响应ResultDto：永久额度调整

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D15011.key, GxpEnum.TRADE_CODE_D15011.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D15011ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.Message::setBody, reqBody).build();
            // 给d15011ReqMessage赋值
            d15011ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.D15011ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req.D15011ReqMessage::setMessage, reqMessage).build();

            // 将d15011ReqService转换成d15011ReqServiceMap
            Map d15011ReqMessageMap = beanMapUtil.beanToMap(d15011ReqMessage);
            context.put("tradeDataMap", d15011ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D15011.key, GxpEnum.TRADE_CODE_D15011.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D15011.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D15011.key, GxpEnum.TRADE_CODE_D15011.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d15011RespMessage = beanMapUtil.mapToBean(tradeDataMap, D15011RespMessage.class, D15011RespMessage.class);//响应Message：永久额度调整
            respMessage = d15011RespMessage.getMessage();//响应Message：永久额度调整
            gxpRespHead = respMessage.getHead();//响应Head：永久额度调整
            //  将D15011RespDto封装到ResultDto<D15011RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                d15011ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d15011ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d15011ResultDto.setCode(EpbEnum.EPB099999.key);
                d15011ResultDto.setMessage(gxpRespHead.getErortx());
            }
            respBody = respMessage.getBody();
            //  将respBody转换成D15011RespDto
            BeanUtils.copyProperties(respBody, d15011RespDto);
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D15011.key, GxpEnum.TRADE_CODE_D15011.value, e.getMessage());
            d15011ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d15011ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d15011ResultDto.setData(d15011RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D15011.key, GxpEnum.TRADE_CODE_D15011.value, JSON.toJSONString(d15011ResultDto));
        return d15011ResultDto;
    }
}
