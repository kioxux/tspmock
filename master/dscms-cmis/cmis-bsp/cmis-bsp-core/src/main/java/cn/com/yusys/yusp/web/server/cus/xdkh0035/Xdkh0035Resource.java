package cn.com.yusys.yusp.web.server.cus.xdkh0035;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0035.req.Xdkh0035ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0035.resp.Data;
import cn.com.yusys.yusp.dto.server.cus.xdkh0035.resp.Xdkh0035RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0035.req.Xdkh0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0035.resp.Xdkh0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:违约认定、重生认定评级信息同步
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0035:违约认定、重生认定评级信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0035Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0035Resource.class);

	@Autowired
	private DscmsCusClientService dscmsCusClientService;
    /**
     * 交易码：xdkh0035
     * 交易描述：违约认定、重生认定评级信息同步
     *
     * @param xdkh0035ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("违约认定、重生认定评级信息同步")
    @PostMapping("/xdkh0035")
    //@Idempotent({"xdcakh0035", "#xdkh0035ReqDto.datasq"})
    protected @ResponseBody
	Xdkh0035RespDto xdkh0035(@Validated @RequestBody Xdkh0035ReqDto xdkh0035ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035ReqDto));
        Xdkh0035DataReqDto xdkh0035DataReqDto = new Xdkh0035DataReqDto();// 请求Data： 违约认定、重生认定评级信息同步
        Xdkh0035DataRespDto xdkh0035DataRespDto = new Xdkh0035DataRespDto();// 响应Data：违约认定、重生认定评级信息同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0035.req.Data reqData = null; // 请求Data：违约认定、重生认定评级信息同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0035.resp.Data respData = new Data();// 响应Data：违约认定、重生认定评级信息同步
		Xdkh0035RespDto xdkh0035RespDto = new Xdkh0035RespDto();
		try {
            // 从 xdkh0035ReqDto获取 reqData
            reqData = xdkh0035ReqDto.getData();
            // 将 reqData 拷贝到xdkh0035DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0035DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035DataReqDto));
            ResultDto<Xdkh0035DataRespDto> xdkh0035DataResultDto = dscmsCusClientService.xdkh0035(xdkh0035DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0035RespDto.setErorcd(Optional.ofNullable(xdkh0035DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0035RespDto.setErortx(Optional.ofNullable(xdkh0035DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0035DataResultDto.getCode())) {
                xdkh0035DataRespDto = xdkh0035DataResultDto.getData();
                xdkh0035RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0035RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0035DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0035DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, e.getMessage());
            xdkh0035RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0035RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0035RespDto.setDatasq(servsq);

        xdkh0035RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0035.key, DscmsEnum.TRADE_CODE_XDKH0035.value, JSON.toJSONString(xdkh0035RespDto));
        return xdkh0035RespDto;
    }
}
