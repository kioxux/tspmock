package cn.com.yusys.yusp.online.client.esb.doc.doc000.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/17 18:29
 * @since 2021/6/17 18:29
 */
public class List_index implements Serializable {
    private static final long serialVersionUID = 1L;
    private java.util.List<cn.com.yusys.yusp.online.client.esb.doc.doc000.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List_index{" +
                "record=" + record +
                '}';
    }
}
