package cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.resp;

/**
 * 响应Service：惠享贷授信申请件数取得
 * @author leehuang
 * @version 1.0             
 */      
public class Fbxd01RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }

    @Override
    public String toString() {
        return "Fbxd01RespService{" +
                "service=" + service +
                '}';
    }
}
