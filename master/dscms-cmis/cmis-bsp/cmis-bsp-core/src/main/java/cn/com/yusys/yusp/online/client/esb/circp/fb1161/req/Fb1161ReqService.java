package cn.com.yusys.yusp.online.client.esb.circp.fb1161.req;

/**
 * 请求Service：借据信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1161ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1161ReqService{" +
                "service=" + service +
                '}';
    }
}
