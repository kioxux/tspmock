package cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp;

/**
 * 响应Service：取得蚂蚁核销记录表的核销借据号一览
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String contract_no;//贷款合同号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Record{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}
