package cn.com.yusys.yusp.web.client.http.sjzt.zcOverallBusinessIncome;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.req.ZcOverallBusinessIncomeReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.resp.List;
import cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.resp.ZcOverallBusinessIncomeRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.online.client.http.sjzt.zcOverallBusinessIncome.req.ZcOverallBusinessIncomeReqService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(zcOverallBusinessIncome)")
@RestController
@RequestMapping("/api/dscms2sjzt")
public class Dscms2ZcOverallBusinessIncomeResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2ZcOverallBusinessIncomeResource.class);
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 资产池业务总体收益情况详情
     *
     * @param zcOverallBusinessIncomeReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("zcOverallBusinessIncome:资产池业务总体收益情况详情")
    @PostMapping("/zcOverallBusinessIncome")
    protected @ResponseBody
    ResultDto<ZcOverallBusinessIncomeRespDto> zcOverallBusinessIncome(@Validated @RequestBody ZcOverallBusinessIncomeReqDto zcOverallBusinessIncomeReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.key, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.value, JSON.toJSONString(zcOverallBusinessIncomeReqDto));

        ZcOverallBusinessIncomeReqService zcOverallBusinessIncomeReqService = new ZcOverallBusinessIncomeReqService();
        ZcOverallBusinessIncomeRespDto zcOverallBusinessIncomeRespDto = new ZcOverallBusinessIncomeRespDto();
        ResultDto<ZcOverallBusinessIncomeRespDto> zcOverallBusinessIncomeResultDto = new ResultDto<ZcOverallBusinessIncomeRespDto>();
        try {
            // 将ZcOverallBusinessIncomeReqDto转换成ZcOverallBusinessIncomeReqService
            BeanUtils.copyProperties(zcOverallBusinessIncomeReqDto, zcOverallBusinessIncomeReqService);

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            zcOverallBusinessIncomeReqService.setServsq(servsq);//    渠道流水
//            zcOverallBusinessIncomeReqService.setUserid(EsbEnum.BRCHNO_XWYWGLPT.key);//    柜员号
//            zcOverallBusinessIncomeReqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(zcOverallBusinessIncomeReqService), headers);
            String url = "http://sc-gateway.zrcbank.ingress/credit/asset/pool/profit";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.key, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.value, JSON.toJSONString(zcOverallBusinessIncomeReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.key, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.value, responseEntity);

            if (responseEntity.getStatusCodeValue() == 200) {
                String responseStr = responseEntity.getBody();
                JSONObject jsonObject = JSONObject.parseObject(responseStr);
                String data = jsonObject.get("data").toString();
                JSONObject dataObject = JSON.parseObject(data);
                String total = dataObject.get("total").toString();
                Object data1 = dataObject.get("data");
                //Object data = Optional.ofNullable(jsonObject.get("data")).orElse(new Object());
                String message = jsonObject.get("message").toString();
                String status = jsonObject.get("status").toString();
                zcOverallBusinessIncomeRespDto.setTotal(Integer.parseInt(total));
                zcOverallBusinessIncomeRespDto.setList((java.util.List<List>) data1);
                zcOverallBusinessIncomeRespDto.setErrorCode(status);
                zcOverallBusinessIncomeRespDto.setErrorMsg(message);
            } else {
                zcOverallBusinessIncomeResultDto.setCode(EpbEnum.EPB099999.key);//9999
                zcOverallBusinessIncomeResultDto.setMessage("交易失败");//系统异常
            }


        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.key, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.value, e.getMessage());
            zcOverallBusinessIncomeResultDto.setCode(EpbEnum.EPB099999.key);//9999
            zcOverallBusinessIncomeResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        zcOverallBusinessIncomeResultDto.setData(zcOverallBusinessIncomeRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.key, EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.value, JSON.toJSONString(zcOverallBusinessIncomeResultDto));
        return zcOverallBusinessIncomeResultDto;
    }
}
