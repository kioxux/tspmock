package cn.com.yusys.yusp.web.client.esb.core.ln3104;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Lstkhzmx;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3104.req.Ln3104ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3104.resp.Ln3104RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3104)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3104Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3104Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3104
     * 交易描述：客户账交易明细查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3104:客户账交易明细查询")
    @PostMapping("/ln3104")
    protected @ResponseBody
    ResultDto<Ln3104RespDto> ln3104(@Validated @RequestBody Ln3104ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3104.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3104.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3104.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3104.resp.Service();
        Ln3104ReqService ln3104ReqService = new Ln3104ReqService();
        Ln3104RespService ln3104RespService = new Ln3104RespService();
        Ln3104RespDto ln3104RespDto = new Ln3104RespDto();
        ResultDto<Ln3104RespDto> ln3104ResultDto = new ResultDto<Ln3104RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3104ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3104.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3104ReqService.setService(reqService);
            // 将ln3104ReqService转换成ln3104ReqServiceMap
            Map ln3104ReqServiceMap = beanMapUtil.beanToMap(ln3104ReqService);
            context.put("tradeDataMap", ln3104ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3104.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3104RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3104RespService.class, Ln3104RespService.class);
            respService = ln3104RespService.getService();

            ln3104ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3104ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3104RespDto
                BeanUtils.copyProperties(respService, ln3104RespDto);
                List<Lstkhzmx> lstkhzmxs = new ArrayList<Lstkhzmx>();
                if (respService.getLstkhzmx_ARRAY() != null && CollectionUtils.nonEmpty(respService.getLstkhzmx_ARRAY().getRecord()) ) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3104.resp.Record> recordList = respService.getLstkhzmx_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3104.resp.Record record : recordList) {
                        Lstkhzmx lstkhzmx = new Lstkhzmx();
                        BeanUtils.copyProperties(record, lstkhzmx);
                        lstkhzmxs.add(lstkhzmx);
                    }
                }
                ln3104RespDto.setLstkhzmx(lstkhzmxs);
                ln3104ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3104ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3104ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3104ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value, e.getMessage());
            ln3104ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3104ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3104ResultDto.setData(ln3104RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3104.key, EsbEnum.TRADE_CODE_LN3104.value, JSON.toJSONString(ln3104ResultDto));
        return ln3104ResultDto;
    }
}
