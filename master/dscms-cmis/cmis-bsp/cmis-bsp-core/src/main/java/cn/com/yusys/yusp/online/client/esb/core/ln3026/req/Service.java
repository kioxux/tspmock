package cn.com.yusys.yusp.online.client.esb.core.ln3026.req;

import java.math.BigDecimal;

/**
 * 请求Service：银团贷款开户
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servdt;//交易日期
    private String servti;//交易时间
    private String dkkhczbz;//开户操作标志
    private String dkjiejuh;//贷款借据号
    private String zidfkbzh;//自动放款标志
    private String zdkoukbz;//自动扣款标志
    private String hetongbh;//合同编号
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String dkczhzhh;//贷款出账号
    private String huobdhao;//货币代号
    private String qixiriqi;//起息日期
    private String dkqixian;//贷款期限
    private String daoqriqi;//到期日期
    private String yngyjigo;//营业机构
    private BigDecimal hetongje;//合同金额
    private BigDecimal jiejuuje;//借据金额
    private BigDecimal bencfkje;//本次放款金额
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private BigDecimal hetongll;//合同利率
    private String lilvleix;//利率类型
    private String zclilvbh;//正常利率编号
    private String nyuelilv;//年/月利率标识
    private String lilvtzfs;//利率调整方式
    private String lilvtzzq;//利率调整周期
    private String lilvfdfs;//利率浮动方式
    private BigDecimal lilvfdzh;//利率浮动值
    private BigDecimal zhchlilv;//正常利率
    private String yuqillbh;//逾期利率编号
    private String yuqinyll;//逾期年月利率
    private BigDecimal yuqililv;//逾期利率
    private String yuqitzfs;//逾期利率调整方式
    private String yuqitzzq;//逾期利率调整周期
    private String yqfxfdfs;//逾期罚息浮动方式
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    private String fulilvbh;//复利利率编号
    private String fulilvny;//复利利率年月标识
    private BigDecimal fulililv;//复利利率
    private String fulitzfs;//复利利率调整方式
    private String fulitzzq;//复利利率调整周期
    private String fulifdfs;//复利利率浮动方式
    private BigDecimal fulifdzh;//复利利率浮动值
    private String zyjjcpdm;//自营借据产品代码
    private String zyjjcpmc;//自营借据产品名称
    private String fzyjcpdm;//非自营借据产品代码
    private String fzyjcpmc;//非自营借据产品名称
    private String wtckywbm;//内部专户核算码
    private String kshchpdm;//可售产品代码
    private String kshchpmc;//可售产品名称
    private String kuaijilb;//会计类别
    private String dkrzhzhh;//贷款入账账号
    private String dkrzhzxh;//贷款入账账号子序号
    private Lstytczfe_ARRAY lstytczfe_ARRAY;//银团出资份额定价计息复合类型
    private String yintdkbz;//银团贷款
    private String dkdbfshi;//贷款担保方式
    private String huankfsh;//还款方式
    private String hkzhouqi;//还款周期
    private String jixiguiz;//计息规则
    private String yqllcklx;//逾期利率参考类型
    private String jfxibzhi;//计复息标志
    private String flllcklx;//复利利率参考类型
    private String wujiflbz;//五级分类标志
    private String dkyongtu;//贷款用途
    private String beizhuuu;//备注
    private String fuhejgou;//复核机构
    private Lstdkhbjh_ARRAY lstdkhbjh_ARRAY;//贷款还本计划
    private String zhngjigo;//账务机构
    private Lstdzqgjh_ARRAY lstdzqgjh_ARRAY;//贷款定制期供计划表
    private String llqxkdfs;//利率期限靠档方式
    private String lilvqixx;//利率期限
    private String fkjzhfsh;//放款记账方式
    private String fkzjclfs;//放款资金处理方式
    private String zjlyzhao;//资金来源账号
    private String zjlyzzxh;//资金来源账号子序号
    private String dhkzhhbz;//多还款账户标志
    private Lstdkhkzh_ARRAY lstdkhkzh_ARRAY;//贷款多还款账户
    private String sfxmyint;//是否项目类银团标志
    private Lstdkstzf_ARRAY lstdkstzf_ARRAY;//贷款受托支付

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getZidfkbzh() {
        return zidfkbzh;
    }

    public void setZidfkbzh(String zidfkbzh) {
        this.zidfkbzh = zidfkbzh;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getDkczhzhh() {
        return dkczhzhh;
    }

    public void setDkczhzhh(String dkczhzhh) {
        this.dkczhzhh = dkczhzhh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getBencfkje() {
        return bencfkje;
    }

    public void setBencfkje(BigDecimal bencfkje) {
        this.bencfkje = bencfkje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getZyjjcpdm() {
        return zyjjcpdm;
    }

    public void setZyjjcpdm(String zyjjcpdm) {
        this.zyjjcpdm = zyjjcpdm;
    }

    public String getZyjjcpmc() {
        return zyjjcpmc;
    }

    public void setZyjjcpmc(String zyjjcpmc) {
        this.zyjjcpmc = zyjjcpmc;
    }

    public String getFzyjcpdm() {
        return fzyjcpdm;
    }

    public void setFzyjcpdm(String fzyjcpdm) {
        this.fzyjcpdm = fzyjcpdm;
    }

    public String getFzyjcpmc() {
        return fzyjcpmc;
    }

    public void setFzyjcpmc(String fzyjcpmc) {
        this.fzyjcpmc = fzyjcpmc;
    }

    public String getWtckywbm() {
        return wtckywbm;
    }

    public void setWtckywbm(String wtckywbm) {
        this.wtckywbm = wtckywbm;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public Lstytczfe_ARRAY getLstytczfe_ARRAY() {
        return lstytczfe_ARRAY;
    }

    public void setLstytczfe_ARRAY(Lstytczfe_ARRAY lstytczfe_ARRAY) {
        this.lstytczfe_ARRAY = lstytczfe_ARRAY;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public Lstdkhbjh_ARRAY getLstdkhbjh_ARRAY() {
        return lstdkhbjh_ARRAY;
    }

    public void setLstdkhbjh_ARRAY(Lstdkhbjh_ARRAY lstdkhbjh_ARRAY) {
        this.lstdkhbjh_ARRAY = lstdkhbjh_ARRAY;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public Lstdzqgjh_ARRAY getLstdzqgjh_ARRAY() {
        return lstdzqgjh_ARRAY;
    }

    public void setLstdzqgjh_ARRAY(Lstdzqgjh_ARRAY lstdzqgjh_ARRAY) {
        this.lstdzqgjh_ARRAY = lstdzqgjh_ARRAY;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public Lstdkhkzh_ARRAY getLstdkhkzh_ARRAY() {
        return lstdkhkzh_ARRAY;
    }

    public void setLstdkhkzh_ARRAY(Lstdkhkzh_ARRAY lstdkhkzh_ARRAY) {
        this.lstdkhkzh_ARRAY = lstdkhkzh_ARRAY;
    }

    public String getSfxmyint() {
        return sfxmyint;
    }

    public void setSfxmyint(String sfxmyint) {
        this.sfxmyint = sfxmyint;
    }

    public Lstdkstzf_ARRAY getLstdkstzf_ARRAY() {
        return lstdkstzf_ARRAY;
    }

    public void setLstdkstzf_ARRAY(Lstdkstzf_ARRAY lstdkstzf_ARRAY) {
        this.lstdkstzf_ARRAY = lstdkstzf_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "zidfkbzh='" + zidfkbzh + '\'' +
                "zdkoukbz='" + zdkoukbz + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dkczhzhh='" + dkczhzhh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "hetongje='" + hetongje + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "bencfkje='" + bencfkje + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hetongll='" + hetongll + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "yuqillbh='" + yuqillbh + '\'' +
                "yuqinyll='" + yuqinyll + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "yuqitzfs='" + yuqitzfs + '\'' +
                "yuqitzzq='" + yuqitzzq + '\'' +
                "yqfxfdfs='" + yqfxfdfs + '\'' +
                "yqfxfdzh='" + yqfxfdzh + '\'' +
                "fulilvbh='" + fulilvbh + '\'' +
                "fulilvny='" + fulilvny + '\'' +
                "fulililv='" + fulililv + '\'' +
                "fulitzfs='" + fulitzfs + '\'' +
                "fulitzzq='" + fulitzzq + '\'' +
                "fulifdfs='" + fulifdfs + '\'' +
                "fulifdzh='" + fulifdzh + '\'' +
                "zyjjcpdm='" + zyjjcpdm + '\'' +
                "zyjjcpmc='" + zyjjcpmc + '\'' +
                "fzyjcpdm='" + fzyjcpdm + '\'' +
                "fzyjcpmc='" + fzyjcpmc + '\'' +
                "wtckywbm='" + wtckywbm + '\'' +
                "kshchpdm='" + kshchpdm + '\'' +
                "kshchpmc='" + kshchpmc + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "lstytczfe='" + lstytczfe_ARRAY + '\'' +
                "yintdkbz='" + yintdkbz + '\'' +
                "dkdbfshi='" + dkdbfshi + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "jixiguiz='" + jixiguiz + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "jfxibzhi='" + jfxibzhi + '\'' +
                "flllcklx='" + flllcklx + '\'' +
                "wujiflbz='" + wujiflbz + '\'' +
                "dkyongtu='" + dkyongtu + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "lstdkhbjh='" + lstdkhbjh_ARRAY + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "lstdzqgjh='" + lstdzqgjh_ARRAY + '\'' +
                "llqxkdfs='" + llqxkdfs + '\'' +
                "lilvqixx='" + lilvqixx + '\'' +
                "fkjzhfsh='" + fkjzhfsh + '\'' +
                "fkzjclfs='" + fkzjclfs + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "lstdkhkzh='" + lstdkhkzh_ARRAY + '\'' +
                "sfxmyint='" + sfxmyint + '\'' +
                "lstdkstzf='" + lstdkstzf_ARRAY + '\'' +
                '}';
    }
}
