package cn.com.yusys.yusp.web.server.biz.xdcz0029;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0029.req.Xdcz0029ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0029.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0029.resp.Xdcz0029RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0029.req.Xdcz0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0029.resp.Xdcz0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:网银推送省心快贷审核任务至信贷
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0029:网银推送省心快贷审核任务至信贷")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0029Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0029
     * 交易描述：网银推送省心快贷审核任务至信贷
     *
     * @param xdcz0029ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("网银推送省心快贷审核任务至信贷")
    @PostMapping("/xdcz0029")
   //@Idempotent({"xdcz0029", "#xdcz0029ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0029RespDto xdcz0029(@Validated @RequestBody Xdcz0029ReqDto xdcz0029ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value, JSON.toJSONString(xdcz0029ReqDto));
        Xdcz0029DataReqDto xdcz0029DataReqDto = new Xdcz0029DataReqDto();// 请求Data： 电子保函开立
        Xdcz0029DataRespDto xdcz0029DataRespDto = new Xdcz0029DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0029.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0029RespDto xdcz0029RespDto = new Xdcz0029RespDto();
        try {
            // 从 xdcz0029ReqDto获取 reqData
            reqData = xdcz0029ReqDto.getData();
            // 将 reqData 拷贝到xdcz0029DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0029DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value, JSON.toJSONString(xdcz0029DataReqDto));
            ResultDto<Xdcz0029DataRespDto> xdcz0029DataResultDto = dscmsBizCzClientService.xdcz0029(xdcz0029DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value, JSON.toJSONString(xdcz0029DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0029RespDto.setErorcd(Optional.ofNullable(xdcz0029DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0029RespDto.setErortx(Optional.ofNullable(xdcz0029DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0029DataResultDto.getCode())) {
                xdcz0029DataRespDto = xdcz0029DataResultDto.getData();
                xdcz0029RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0029RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0029DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0029DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value, e.getMessage());
            xdcz0029RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0029RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0029RespDto.setDatasq(servsq);

        xdcz0029RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0029.key, DscmsEnum.TRADE_CODE_XDCZ0029.value, JSON.toJSONString(xdcz0029RespDto));
        return xdcz0029RespDto;
    }
}
