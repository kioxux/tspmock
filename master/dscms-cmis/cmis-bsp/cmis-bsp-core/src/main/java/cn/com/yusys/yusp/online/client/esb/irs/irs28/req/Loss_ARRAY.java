package cn.com.yusys.yusp.online.client.esb.irs.irs28.req;

import cn.com.yusys.yusp.online.client.esb.irs.irs28.req.loss.Record;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 16:21
 * @since 2021/5/27 16:21
 */
public class Loss_ARRAY {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.irs.irs28.req.loss.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Loss_ARRAY{" +
                "record=" + record +
                '}';
    }
}
