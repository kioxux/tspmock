package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp;

/**
 * 响应Service：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String busi_seq;//业务流水号
    private String cred_sub_no;//授信分项编号
    private String riskData;//riskData

    public String getBusi_seq() {
        return busi_seq;
    }

    public void setBusi_seq(String busi_seq) {
        this.busi_seq = busi_seq;
    }

    public String getCred_sub_no() {
        return cred_sub_no;
    }

    public void setCred_sub_no(String cred_sub_no) {
        this.cred_sub_no = cred_sub_no;
    }

    public String getRiskData() {
        return riskData;
    }

    public void setRiskData(String riskData) {
        this.riskData = riskData;
    }

    @Override
    public String toString() {
        return "Record{" +
                "busi_seq='" + busi_seq + '\'' +
                ", cred_sub_no='" + cred_sub_no + '\'' +
                ", riskData='" + riskData + '\'' +
                '}';
    }
}
