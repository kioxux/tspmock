package cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkxypc;

import java.math.BigDecimal;

/**
 * 响应Service：资产证券化信息查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String xieybhao;//协议编号
    private String picihaoo;//批次号
    private String xieyimch;//协议名称
    private String jiejuhao;//借据号
    private String zcrfshii;//资产融通方式
    private BigDecimal zcrtbili;//资产融通比例
    private BigDecimal zczrjine;//资产转让金额
    private BigDecimal fbsdlixi;//封包时点利息
    private BigDecimal yhfbsdlx;//已还封包时点利息
    private String zczrcpdm;//资产转让产品代码
    private String zczrcpmc;//资产转让产品名称
    private String zrcpdmzy;//部分转让产品代码(自营)
    private String zrcpmczy;//部分转让产品名称(自营)
    private String zrcpdmtg;//部分转让产品代码(托管)
    private String zrcpmctg;//部分转让产品名称(托管)
    private BigDecimal fbhrcqhb;//封包后入池前还本金额
    private BigDecimal fbhrcqhx;//封包后入池前还息金额
    private BigDecimal fbhrcqfy;//封包后入池前归还费用金额
    private BigDecimal fbhrcqfj;//封包后入池前归还罚金金额
    private String zjjjclzt;//资产借据处理状态
    private String zhngjigo;//账务机构
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private BigDecimal xieyilil;//协议利率
    private String nyuelilv;//年/月利率标识
    private BigDecimal jjzrlilv;//借据转让利率
    private String llqxkdfs;//利率期限靠档方式
    private String lilvqixx;//利率期限
    private String lilvbhao;//利率编号
    private String lilvtzfs;//利率调整方式
    private String lilvtzzq;//利率调整周期
    private String lilvfdfs;//利率浮动方式
    private BigDecimal lilvfdzh;//利率浮动值
    private BigDecimal fbsdfeiy;//封包时点费用
    private BigDecimal yhfbsdfy;//已还封包时点费用

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public String getZcrfshii() {
        return zcrfshii;
    }

    public void setZcrfshii(String zcrfshii) {
        this.zcrfshii = zcrfshii;
    }

    public BigDecimal getZcrtbili() {
        return zcrtbili;
    }

    public void setZcrtbili(BigDecimal zcrtbili) {
        this.zcrtbili = zcrtbili;
    }

    public BigDecimal getZczrjine() {
        return zczrjine;
    }

    public void setZczrjine(BigDecimal zczrjine) {
        this.zczrjine = zczrjine;
    }

    public BigDecimal getFbsdlixi() {
        return fbsdlixi;
    }

    public void setFbsdlixi(BigDecimal fbsdlixi) {
        this.fbsdlixi = fbsdlixi;
    }

    public BigDecimal getYhfbsdlx() {
        return yhfbsdlx;
    }

    public void setYhfbsdlx(BigDecimal yhfbsdlx) {
        this.yhfbsdlx = yhfbsdlx;
    }

    public String getZczrcpdm() {
        return zczrcpdm;
    }

    public void setZczrcpdm(String zczrcpdm) {
        this.zczrcpdm = zczrcpdm;
    }

    public String getZczrcpmc() {
        return zczrcpmc;
    }

    public void setZczrcpmc(String zczrcpmc) {
        this.zczrcpmc = zczrcpmc;
    }

    public String getZrcpdmzy() {
        return zrcpdmzy;
    }

    public void setZrcpdmzy(String zrcpdmzy) {
        this.zrcpdmzy = zrcpdmzy;
    }

    public String getZrcpmczy() {
        return zrcpmczy;
    }

    public void setZrcpmczy(String zrcpmczy) {
        this.zrcpmczy = zrcpmczy;
    }

    public String getZrcpdmtg() {
        return zrcpdmtg;
    }

    public void setZrcpdmtg(String zrcpdmtg) {
        this.zrcpdmtg = zrcpdmtg;
    }

    public String getZrcpmctg() {
        return zrcpmctg;
    }

    public void setZrcpmctg(String zrcpmctg) {
        this.zrcpmctg = zrcpmctg;
    }

    public BigDecimal getFbhrcqhb() {
        return fbhrcqhb;
    }

    public void setFbhrcqhb(BigDecimal fbhrcqhb) {
        this.fbhrcqhb = fbhrcqhb;
    }

    public BigDecimal getFbhrcqhx() {
        return fbhrcqhx;
    }

    public void setFbhrcqhx(BigDecimal fbhrcqhx) {
        this.fbhrcqhx = fbhrcqhx;
    }

    public BigDecimal getFbhrcqfy() {
        return fbhrcqfy;
    }

    public void setFbhrcqfy(BigDecimal fbhrcqfy) {
        this.fbhrcqfy = fbhrcqfy;
    }

    public BigDecimal getFbhrcqfj() {
        return fbhrcqfj;
    }

    public void setFbhrcqfj(BigDecimal fbhrcqfj) {
        this.fbhrcqfj = fbhrcqfj;
    }

    public String getZjjjclzt() {
        return zjjjclzt;
    }

    public void setZjjjclzt(String zjjjclzt) {
        this.zjjjclzt = zjjjclzt;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public BigDecimal getXieyilil() {
        return xieyilil;
    }

    public void setXieyilil(BigDecimal xieyilil) {
        this.xieyilil = xieyilil;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getJjzrlilv() {
        return jjzrlilv;
    }

    public void setJjzrlilv(BigDecimal jjzrlilv) {
        this.jjzrlilv = jjzrlilv;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getLilvbhao() {
        return lilvbhao;
    }

    public void setLilvbhao(String lilvbhao) {
        this.lilvbhao = lilvbhao;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getFbsdfeiy() {
        return fbsdfeiy;
    }

    public void setFbsdfeiy(BigDecimal fbsdfeiy) {
        this.fbsdfeiy = fbsdfeiy;
    }

    public BigDecimal getYhfbsdfy() {
        return yhfbsdfy;
    }

    public void setYhfbsdfy(BigDecimal yhfbsdfy) {
        this.yhfbsdfy = yhfbsdfy;
    }

    @Override
    public String toString() {
        return "Record{" +
                "xieybhao='" + xieybhao + '\'' +
                ", picihaoo='" + picihaoo + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", jiejuhao='" + jiejuhao + '\'' +
                ", zcrfshii='" + zcrfshii + '\'' +
                ", zcrtbili=" + zcrtbili +
                ", zczrjine=" + zczrjine +
                ", fbsdlixi=" + fbsdlixi +
                ", yhfbsdlx=" + yhfbsdlx +
                ", zczrcpdm='" + zczrcpdm + '\'' +
                ", zczrcpmc='" + zczrcpmc + '\'' +
                ", zrcpdmzy='" + zrcpdmzy + '\'' +
                ", zrcpmczy='" + zrcpmczy + '\'' +
                ", zrcpdmtg='" + zrcpdmtg + '\'' +
                ", zrcpmctg='" + zrcpmctg + '\'' +
                ", fbhrcqhb=" + fbhrcqhb +
                ", fbhrcqhx=" + fbhrcqhx +
                ", fbhrcqfy=" + fbhrcqfy +
                ", fbhrcqfj=" + fbhrcqfj +
                ", zjjjclzt='" + zjjjclzt + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", xieyilil=" + xieyilil +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", jjzrlilv=" + jjzrlilv +
                ", llqxkdfs='" + llqxkdfs + '\'' +
                ", lilvqixx='" + lilvqixx + '\'' +
                ", lilvbhao='" + lilvbhao + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", fbsdfeiy=" + fbsdfeiy +
                ", yhfbsdfy=" + yhfbsdfy +
                '}';
    }
}
