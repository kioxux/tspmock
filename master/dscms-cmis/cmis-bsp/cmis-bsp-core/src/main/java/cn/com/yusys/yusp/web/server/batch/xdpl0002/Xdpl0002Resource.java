package cn.com.yusys.yusp.web.server.batch.xdpl0002;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0011.Cmisbatch0011RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.batch.xdpl0002.req.Xdpl0002ReqDto;
import cn.com.yusys.yusp.dto.server.batch.xdpl0002.resp.Xdpl0002RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提供日终调度平台查询批量任务状态
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDPL0002:提供日终调度平台查询批量任务状态")
@RestController
@RequestMapping("/api/dscms")
public class Xdpl0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdpl0002Resource.class);

    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 交易码：xdpl0002
     * 交易描述：提供日终调度平台查询批量任务状态
     *
     * @param xdpl0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提供日终调度平台查询批量任务状态")
    @PostMapping("/xdpl0002")
    @Idempotent({"xdpl0002", "#xdpl0002ReqDto.datasq"})
    protected @ResponseBody
    Xdpl0002RespDto xdpl0002(@Validated @RequestBody Xdpl0002ReqDto xdpl0002ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0002.key, DscmsEnum.TRADE_CODE_XDPL0002.value, JSON.toJSONString(xdpl0002ReqDto));
        Cmisbatch0011ReqDto cmisbatch0011ReqDto = new Cmisbatch0011ReqDto();// 请求Data： 提供日终调度平台查询批量任务状态
        Cmisbatch0011RespDto cmisbatch0011RespDto = new Cmisbatch0011RespDto();// 响应Data：提供日终调度平台查询批量任务状态
        cn.com.yusys.yusp.dto.server.batch.xdpl0002.req.Data reqData = null; // 请求Data：提供日终调度平台查询批量任务状态
        cn.com.yusys.yusp.dto.server.batch.xdpl0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.batch.xdpl0002.resp.Data();// 响应Data：提供日终调度平台查询批量任务状态
        Xdpl0002RespDto xdpl0002RespDto = new Xdpl0002RespDto();
        try {
            // 从 xdpl0002ReqDto获取 reqData
            reqData = xdpl0002ReqDto.getData();
            // 将 reqData 拷贝到cmisbatch0011ReqDto
            BeanUtils.copyProperties(reqData, cmisbatch0011ReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0002.key, DscmsEnum.TRADE_CODE_XDPL0002.value, JSON.toJSONString(cmisbatch0011ReqDto));
            ResultDto<Cmisbatch0011RespDto> cmisbatch0011ResultDto = cmisBatchClientService.cmisbatch0011(cmisbatch0011ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0002.key, DscmsEnum.TRADE_CODE_XDPL0002.value, JSON.toJSONString(cmisbatch0011ResultDto));
            // 从返回值中获取响应码和响应消息
            xdpl0002RespDto.setErorcd(Optional.ofNullable(cmisbatch0011ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdpl0002RespDto.setErortx(Optional.ofNullable(cmisbatch0011ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0011ResultDto.getCode())) {
                cmisbatch0011RespDto = cmisbatch0011ResultDto.getData();
                xdpl0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdpl0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisbatch0011RespDto拷贝到 respData
                BeanUtils.copyProperties(cmisbatch0011RespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0002.key, DscmsEnum.TRADE_CODE_XDPL0002.value, e.getMessage());
            xdpl0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdpl0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdpl0002RespDto.setDatasq(servsq);

        xdpl0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0002.key, DscmsEnum.TRADE_CODE_XDPL0002.value, JSON.toJSONString(xdpl0002RespDto));
        return xdpl0002RespDto;
    }
}
