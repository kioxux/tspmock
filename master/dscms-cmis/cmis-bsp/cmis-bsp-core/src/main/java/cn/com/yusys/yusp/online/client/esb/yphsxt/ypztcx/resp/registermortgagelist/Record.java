package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registermortgagelist;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:17
 * @since 2021/7/9 14:17
 */
public class Record {

    private String mmcenu;//登记簿抵押信息-不动产登记证明号
    private String mormor;//登记簿抵押信息-抵押权人
    private String mormog;//登记簿抵押信息-抵押人
    private String mogusc;//登记簿抵押信息-担保范围
    private String mogord;//登记簿抵押信息-顺位
    private String momome;//登记簿抵押信息-抵押方式
    private String modeam;//登记簿抵押信息-债权数额
    private String mordst;//登记簿抵押信息-债务履行开始时间
    private String mordet;//登记簿抵押信息-债务履行结束时间
    private String morbod;//登记簿抵押信息-登簿日期

    public String getMmcenu() {
        return mmcenu;
    }

    public void setMmcenu(String mmcenu) {
        this.mmcenu = mmcenu;
    }

    public String getMormor() {
        return mormor;
    }

    public void setMormor(String mormor) {
        this.mormor = mormor;
    }

    public String getMormog() {
        return mormog;
    }

    public void setMormog(String mormog) {
        this.mormog = mormog;
    }

    public String getMogusc() {
        return mogusc;
    }

    public void setMogusc(String mogusc) {
        this.mogusc = mogusc;
    }

    public String getMogord() {
        return mogord;
    }

    public void setMogord(String mogord) {
        this.mogord = mogord;
    }

    public String getMomome() {
        return momome;
    }

    public void setMomome(String momome) {
        this.momome = momome;
    }

    public String getModeam() {
        return modeam;
    }

    public void setModeam(String modeam) {
        this.modeam = modeam;
    }

    public String getMordst() {
        return mordst;
    }

    public void setMordst(String mordst) {
        this.mordst = mordst;
    }

    public String getMordet() {
        return mordet;
    }

    public void setMordet(String mordet) {
        this.mordet = mordet;
    }

    public String getMorbod() {
        return morbod;
    }

    public void setMorbod(String morbod) {
        this.morbod = morbod;
    }

    @Override
    public String toString() {
        return "Record{" +
                "mmcenu='" + mmcenu + '\'' +
                ", mormor='" + mormor + '\'' +
                ", mormog='" + mormog + '\'' +
                ", mogusc='" + mogusc + '\'' +
                ", mogord='" + mogord + '\'' +
                ", momome='" + momome + '\'' +
                ", modeam='" + modeam + '\'' +
                ", mordst='" + mordst + '\'' +
                ", mordet='" + mordet + '\'' +
                ", morbod='" + morbod + '\'' +
                '}';
    }
}
