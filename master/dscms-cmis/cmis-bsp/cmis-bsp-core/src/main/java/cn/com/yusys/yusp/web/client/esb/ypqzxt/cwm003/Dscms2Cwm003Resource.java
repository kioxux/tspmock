package cn.com.yusys.yusp.web.client.esb.ypqzxt.cwm003;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.req.Cwm003ReqService;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.resp.Cwm003RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用押品权证系统的接口
 */
@Api(tags = "BSP封装调用押品权证系统的接口(cwm003)")
@RestController
@RequestMapping("/api/dscms2ypqzxt")
public class Dscms2Cwm003Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Cwm003Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 押品权证入库接口（处理码cwm003）
     *
     * @param cwm003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cwm003:押品权证入库接口")
    @PostMapping("/cwm003")
    protected @ResponseBody
    ResultDto<Cwm003RespDto> cwm003(@Validated @RequestBody Cwm003ReqDto cwm003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM003.key, EsbEnum.TRADE_CODE_CWM003.value, JSON.toJSONString(cwm003ReqDto));
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.resp.Service();
        Cwm003ReqService cwm003ReqService = new Cwm003ReqService();
        Cwm003RespService cwm003RespService = new Cwm003RespService();
        Cwm003RespDto cwm003RespDto = new Cwm003RespDto();
        ResultDto<Cwm003RespDto> cwm003ResultDto = new ResultDto<Cwm003RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Cwm003ReqDto转换成reqService
            BeanUtils.copyProperties(cwm003ReqDto, reqService);
            if (StringUtils.isNotBlank(reqService.getAcctBrch())) {
                reqService.setAcctBrch(reqService.getAcctBrch().substring(0, reqService.getAcctBrch().length() - 1) + "1");
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CWM003.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPQZXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPQZXT.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            cwm003ReqService.setService(reqService);
            // 将cwm003ReqService转换成cwm003ReqServiceMap
            Map cwm003ReqServiceMap = beanMapUtil.beanToMap(cwm003ReqService);
            context.put("tradeDataMap", cwm003ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM003.key, EsbEnum.TRADE_CODE_CWM003.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CWM003.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM003.key, EsbEnum.TRADE_CODE_CWM003.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cwm003RespService = beanMapUtil.mapToBean(tradeDataMap, Cwm003RespService.class, Cwm003RespService.class);
            respService = cwm003RespService.getService();

            //  将Cwm003RespDto封装到ResultDto<Cwm003RespDto>
            cwm003ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            cwm003ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Cwm003RespDto
                BeanUtils.copyProperties(respService, cwm003RespDto);
                cwm003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cwm003ResultDto.setMessage(SuccessEnum.SUCCESS.value);

            } else {
                cwm003ResultDto.setCode(EpbEnum.EPB099999.key);
                cwm003ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM003.key, EsbEnum.TRADE_CODE_CWM003.value, e.getMessage());
            cwm003ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cwm003ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cwm003ResultDto.setData(cwm003RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM003.key, EsbEnum.TRADE_CODE_CWM003.value, JSON.toJSONString(cwm003ResultDto));
        return cwm003ResultDto;
    }
}
