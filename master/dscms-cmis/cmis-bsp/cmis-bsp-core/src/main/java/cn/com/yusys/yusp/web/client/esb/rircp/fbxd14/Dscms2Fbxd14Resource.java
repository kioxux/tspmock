package cn.com.yusys.yusp.web.client.esb.rircp.fbxd14;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14.Fbxd14ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14.Fbxd14RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.req.Fbxd14ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.Fbxd14RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd14）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd14Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd14Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd14
     * 交易描述：查询还款（合约）明细历史表（利翃）中的全量借据一览
     *
     * @param fbxd14ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd14")
    protected @ResponseBody
    ResultDto<Fbxd14RespDto> fbxd14(@Validated @RequestBody Fbxd14ReqDto fbxd14ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd14ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.Service();
        Fbxd14ReqService fbxd14ReqService = new Fbxd14ReqService();
        Fbxd14RespService fbxd14RespService = new Fbxd14RespService();
        Fbxd14RespDto fbxd14RespDto = new Fbxd14RespDto();
        ResultDto<Fbxd14RespDto> fbxd14ResultDto = new ResultDto<Fbxd14RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd14ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd14ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD10.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd14ReqService.setService(reqService);
            // 将fbxd14ReqService转换成fbxd14ReqServiceMap
            Map fbxd14ReqServiceMap = beanMapUtil.beanToMap(fbxd14ReqService);
            context.put("tradeDataMap", fbxd14ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD10.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd14RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd14RespService.class, Fbxd14RespService.class);
            respService = fbxd14RespService.getService();

            fbxd14ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd14ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd14RespDto
                BeanUtils.copyProperties(respService, fbxd14RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.List fbxd14RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.List());
                respService.setList(fbxd14RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> fbxd14RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14.List> fbxd14RespDtoLists = fbxd14RespRecordList.parallelStream().map(fbxd14RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14.List fbxd14RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14.List();
                        BeanUtils.copyProperties(fbxd14RespRecord, fbxd14RespDtoList);
                        return fbxd14RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd14RespDto.setList(fbxd14RespDtoLists);
                }
                fbxd14ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd14ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd14ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd14ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, e.getMessage());
            fbxd14ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd14ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd14ResultDto.setData(fbxd14RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd14ResultDto));
        return fbxd14ResultDto;
    }
}
