package cn.com.yusys.yusp.online.client.esb.core.ln3110.req;

import java.math.BigDecimal;

/**
 * 请求Service：用于贷款放款前进行还款计划试算
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间
    private String datasq;//全局流水

    private BigDecimal daikjine;//贷款金额
    private String sftsdkbz;//是否特殊贷款标志
    private Integer tsdkjxqs;//特殊贷款计息总期数
    private String jixiguiz;//计息规则
    private BigDecimal zhchlilv;//正常利率
    private String hkzhouqi;//还款周期
    private String hkqixian;//还款期限(月)
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String huankfsh;//还款方式
    private String qigscfsh;//期供生成方式
    private String qglxleix;//期供利息类型
    private String dechligz;//等额处理规则
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal meiqhbje;//每期还本金额
    private BigDecimal baoliuje;//保留金额
    private Integer kuanxiqi;//宽限期
    private BigDecimal leijinzh;//累进值
    private Integer leijqjsh;//累进区间期数
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数
    private String shifoudy;//是否打印
    private String scihkrbz;//首次还款日模式
    private String mqihkfsh;//末期还款方式
    private String dzhhkjih;//定制还款计划
    private Integer ljsxqish;//累进首段期数
    private Lstdkhbjh_ARRAY lstdkhbjh_ARRAY;//贷款还本计划
    private Lstdzqgjh_ARRAY lstdzqgjh_ARRAY;//贷款定制期供计划表

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public BigDecimal getDaikjine() {
        return daikjine;
    }

    public void setDaikjine(BigDecimal daikjine) {
        this.daikjine = daikjine;
    }

    public String getSftsdkbz() {
        return sftsdkbz;
    }

    public void setSftsdkbz(String sftsdkbz) {
        this.sftsdkbz = sftsdkbz;
    }

    public Integer getTsdkjxqs() {
        return tsdkjxqs;
    }

    public void setTsdkjxqs(Integer tsdkjxqs) {
        this.tsdkjxqs = tsdkjxqs;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getQglxleix() {
        return qglxleix;
    }

    public void setQglxleix(String qglxleix) {
        this.qglxleix = qglxleix;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public Integer getKuanxiqi() {
        return kuanxiqi;
    }

    public void setKuanxiqi(Integer kuanxiqi) {
        this.kuanxiqi = kuanxiqi;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public Integer getLjsxqish() {
        return ljsxqish;
    }

    public void setLjsxqish(Integer ljsxqish) {
        this.ljsxqish = ljsxqish;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public Lstdkhbjh_ARRAY getLstdkhbjh_ARRAY() {
        return lstdkhbjh_ARRAY;
    }

    public void setLstdkhbjh_ARRAY(Lstdkhbjh_ARRAY lstdkhbjh_ARRAY) {
        this.lstdkhbjh_ARRAY = lstdkhbjh_ARRAY;
    }

    public Lstdzqgjh_ARRAY getLstdzqgjh_ARRAY() {
        return lstdzqgjh_ARRAY;
    }

    public void setLstdzqgjh_ARRAY(Lstdzqgjh_ARRAY lstdzqgjh_ARRAY) {
        this.lstdzqgjh_ARRAY = lstdzqgjh_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", daikjine=" + daikjine +
                ", sftsdkbz='" + sftsdkbz + '\'' +
                ", tsdkjxqs=" + tsdkjxqs +
                ", jixiguiz='" + jixiguiz + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", hkqixian='" + hkqixian + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", qigscfsh='" + qigscfsh + '\'' +
                ", qglxleix='" + qglxleix + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", baoliuje=" + baoliuje +
                ", kuanxiqi=" + kuanxiqi +
                ", leijinzh=" + leijinzh +
                ", leijqjsh=" + leijqjsh +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", shifoudy='" + shifoudy + '\'' +
                ", scihkrbz='" + scihkrbz + '\'' +
                ", mqihkfsh='" + mqihkfsh + '\'' +
                ", dzhhkjih='" + dzhhkjih + '\'' +
                ", ljsxqish=" + ljsxqish +
                ", lstdkhbjh_ARRAY=" + lstdkhbjh_ARRAY +
                ", lstdzqgjh_ARRAY=" + lstdzqgjh_ARRAY +
                '}';
    }
}
