package cn.com.yusys.yusp.web.client.esb.rircp.fbxw08;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw08.Fbxw08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw08.Fbxw08RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.req.Fbxw08ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.resp.Fbxw08RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw08Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw08Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 授信审批作废接口（处理码fbxw08）
     *
     * @param fbxw08ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw08:授信审批作废接口")
    @PostMapping("/fbxw08")
    protected @ResponseBody
    ResultDto<Fbxw08RespDto> fbxw08(@Validated @RequestBody Fbxw08ReqDto fbxw08ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW08.key, EsbEnum.TRADE_CODE_FBXW08.value, JSON.toJSONString(fbxw08ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.resp.Service();
        Fbxw08ReqService fbxw08ReqService = new Fbxw08ReqService();
        Fbxw08RespService fbxw08RespService = new Fbxw08RespService();
        Fbxw08RespDto fbxw08RespDto = new Fbxw08RespDto();
        ResultDto<Fbxw08RespDto> fbxw08ResultDto = new ResultDto<Fbxw08RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw08ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw08ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW08.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw08ReqService.setService(reqService);
            // 将fbxw08ReqService转换成fbxw08ReqServiceMap
            Map fbxw08ReqServiceMap = beanMapUtil.beanToMap(fbxw08ReqService);
            context.put("tradeDataMap", fbxw08ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW08.key, EsbEnum.TRADE_CODE_FBXW08.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW08.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW08.key, EsbEnum.TRADE_CODE_FBXW08.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw08RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw08RespService.class, Fbxw08RespService.class);
            respService = fbxw08RespService.getService();

            //  将Fbxw08RespDto封装到ResultDto<Fbxw08RespDto>
            fbxw08ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw08ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw08RespDto
                BeanUtils.copyProperties(respService, fbxw08RespDto);
                fbxw08ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw08ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw08ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw08ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW08.key, EsbEnum.TRADE_CODE_FBXW08.value, e.getMessage());
            fbxw08ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw08ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw08ResultDto.setData(fbxw08RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW08.key, EsbEnum.TRADE_CODE_FBXW08.value, JSON.toJSONString(fbxw08ResultDto));
        return fbxw08ResultDto;
    }

}
