package cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd;

import java.util.List;
/**
 * 响应Service：还款方式转换
 *
 * @author leehuang
 * @version 1.0
 */
public class Lsdkbjfd {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lsdkbjfd{" +
                "record=" + record +
                '}';
    }
}
