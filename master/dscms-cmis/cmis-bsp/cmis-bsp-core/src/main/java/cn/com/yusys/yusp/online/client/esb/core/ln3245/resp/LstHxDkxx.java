package cn.com.yusys.yusp.online.client.esb.core.ln3245.resp;

import java.util.List;

public class LstHxDkxx {

    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstHxDkxx{" +
                "record=" + record +
                '}';
    }
}
