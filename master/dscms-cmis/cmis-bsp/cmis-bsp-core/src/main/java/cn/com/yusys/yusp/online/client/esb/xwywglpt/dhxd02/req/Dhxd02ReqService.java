package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req;

/**
 * 请求Service：信贷系统请求小V平台贷后预警回传定期检查任务结果
 *
 * @author code-generator
 * @version 1.0
 */
public class Dhxd02ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

