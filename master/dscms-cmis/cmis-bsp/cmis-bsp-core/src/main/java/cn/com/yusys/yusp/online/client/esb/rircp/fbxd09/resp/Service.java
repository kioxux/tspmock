package cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.resp;

/**
 * 响应Service：查询日初（合约）信息历史表（利翃）总数据量
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
	private String erorcd; // 响应码
	private String erortx; // 响应信息
    private Integer lh_loan_init_num;//日初（合约）信息历史表（利翃）总数据量

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public Integer getLh_loan_init_num() {
        return lh_loan_init_num;
    }

    public void setLh_loan_init_num(Integer lh_loan_init_num) {
        this.lh_loan_init_num = lh_loan_init_num;
    }

    @Override
    public String toString() {
        return "Service{" +
                "lh_loan_init_num='" + lh_loan_init_num + '\'' +
                '}';
    }
}
