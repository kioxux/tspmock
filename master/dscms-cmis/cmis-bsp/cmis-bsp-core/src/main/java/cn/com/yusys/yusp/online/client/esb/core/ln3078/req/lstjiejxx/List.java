package cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx;

/**
 * 请求Service：贷款机构变更
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
