package cn.com.yusys.yusp.online.client.esb.yk.yky001.req;

/**
 * 请求Service：用印申请
 *
 * @author xuwh
 * @version 1.0
 * @since 2021/8/42下午20:42:07
 */
public class Service {
    private String prcscd; // 处理码
    private String servtp; // 渠道
    private String servsq; // 渠道流水
    private String userid; // 柜员号
    private String brchno; // 部门号
    private String servdt; // 请求方日期
    private String servti; // 请求方时间
    private String oaId; // 流程号
    private String operatorCode; // 操作员号
    private String operatorName; // 操作员名
    private String orgNo; // 机构号
    private String orgName; // 机构名
    private String title; // 标题
    private String accessory; // 文件目录
    private String applyTime; // 交易时间
    private String stampEmpOrg; // 机构号
    private cn.com.yusys.yusp.online.client.esb.yk.yky001.req.List list; // 用印列表

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getOaId() {
        return oaId;
    }

    public void setOaId(String oaId) {
        this.oaId = oaId;
    }

    public String getOperatorCode() {
        return operatorCode;
    }

    public void setOperatorCode(String operatorCode) {
        this.operatorCode = operatorCode;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccessory() {
        return accessory;
    }

    public void setAccessory(String accessory) {
        this.accessory = accessory;
    }

    public String getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(String applyTime) {
        this.applyTime = applyTime;
    }

    public String getStampEmpOrg() {
        return stampEmpOrg;
    }

    public void setStampEmpOrg(String stampEmpOrg) {
        this.stampEmpOrg = stampEmpOrg;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", oaId='" + oaId + '\'' +
                ", operatorCode='" + operatorCode + '\'' +
                ", operatorName='" + operatorName + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", orgName='" + orgName + '\'' +
                ", title='" + title + '\'' +
                ", accessory='" + accessory + '\'' +
                ", applyTime='" + applyTime + '\'' +
                ", stampEmpOrg='" + stampEmpOrg + '\'' +
                ", list=" + list +
                '}';
    }
}