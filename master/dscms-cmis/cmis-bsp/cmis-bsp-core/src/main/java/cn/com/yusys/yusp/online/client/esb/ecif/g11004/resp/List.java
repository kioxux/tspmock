package cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp;
/**
 * 响应Service：客户集团信息查询（new）接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 21:06
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
