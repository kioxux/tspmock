package cn.com.yusys.yusp.online.client.esb.rircp.fbxw01.resp;

/**
 * 响应Service：授信申请提交接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午5:34:07
 */
public class Fbxw01RespService {

        private Service service;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }
}
