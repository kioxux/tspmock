package cn.com.yusys.yusp.web.server.biz.xddh0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0007.req.Xddh0007ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0007.resp.Xddh0007RespDto;
import cn.com.yusys.yusp.dto.server.xddh0007.req.Xddh0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0007.resp.Xddh0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:还款记录列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDDH0007:还款记录列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0007Resource.class);


    @Autowired
	private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0007
     * 交易描述：还款记录列表查询
     *
     * @param xddh0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款记录列表查询")
    @PostMapping("/xddh0007")
    //@Idempotent({"xddh0007", "#xddh0007ReqDto.datasq"})
    protected @ResponseBody
    Xddh0007RespDto xddh0007(@Validated @RequestBody Xddh0007ReqDto xddh0007ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007ReqDto));
        Xddh0007DataReqDto xddh0007DataReqDto = new Xddh0007DataReqDto();// 请求Data： 还款记录列表查询
        Xddh0007DataRespDto xddh0007DataRespDto = new Xddh0007DataRespDto();// 响应Data：还款记录列表查询
        cn.com.yusys.yusp.dto.server.biz.xddh0007.req.Data reqData = null; // 请求Data：还款记录列表查询
        cn.com.yusys.yusp.dto.server.biz.xddh0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0007.resp.Data();// 响应Data：还款记录列表查询
		Xddh0007RespDto xddh0007RespDto = new Xddh0007RespDto();
		try {
            // 从 xddh0007ReqDto获取 reqData
            reqData = xddh0007ReqDto.getData();
            // 将 reqData 拷贝到xddh0007DataReqDto
            BeanUtils.copyProperties(reqData, xddh0007DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007DataReqDto));
            ResultDto<Xddh0007DataRespDto> xddh0007DataResultDto = dscmsBizDhClientService.xddh0007(xddh0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddh0007RespDto.setErorcd(Optional.ofNullable(xddh0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0007RespDto.setErortx(Optional.ofNullable(xddh0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0007DataResultDto.getCode())) {
                xddh0007DataRespDto = xddh0007DataResultDto.getData();
                xddh0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, e.getMessage());
            xddh0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0007RespDto.setDatasq(servsq);

        xddh0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0007.key, DscmsEnum.TRADE_CODE_XDDH0007.value, JSON.toJSONString(xddh0007RespDto));
        return xddh0007RespDto;
    }
}
