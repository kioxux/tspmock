package cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp;

/**
 * 响应Service：获取指定数据日期存在还款记录的借据一览信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String contract_no;//借据号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Record{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}
