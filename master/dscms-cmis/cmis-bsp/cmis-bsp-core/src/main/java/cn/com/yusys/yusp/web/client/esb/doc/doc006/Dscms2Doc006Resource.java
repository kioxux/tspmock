package cn.com.yusys.yusp.web.client.esb.doc.doc006;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.req.Doc006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.resp.Doc006RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.doc.doc006.req.Doc006ReqService;
import cn.com.yusys.yusp.online.client.esb.doc.doc006.resp.Doc006RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(doc006)")
@RestController
@RequestMapping("/api/dscms2doc")
public class Dscms2Doc006Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Doc006Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 出库归还（处理码doc006）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("doc006:出库归还")
    @PostMapping("/doc006")
    protected @ResponseBody
    ResultDto<Doc006RespDto> doc006(@Validated @RequestBody Doc006ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC006.key, EsbEnum.TRADE_CODE_DOC006.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.doc.doc006.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.doc.doc006.req.Service();
        cn.com.yusys.yusp.online.client.esb.doc.doc006.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.doc.doc006.resp.Service();
        Doc006ReqService doc006ReqService = new Doc006ReqService();
        Doc006RespService doc006RespService = new Doc006RespService();
        Doc006RespDto doc006RespDto = new Doc006RespDto();
        ResultDto<Doc006RespDto> doc006ResultDto = new ResultDto<Doc006RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Doc006ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DOC006.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            doc006ReqService.setService(reqService);
            // 将doc006ReqService转换成doc006ReqServiceMap
            Map doc006ReqServiceMap = beanMapUtil.beanToMap(doc006ReqService);
            context.put("tradeDataMap", doc006ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC006.key, EsbEnum.TRADE_CODE_DOC006.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DOC006.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC006.key, EsbEnum.TRADE_CODE_DOC006.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            doc006RespService = beanMapUtil.mapToBean(tradeDataMap, Doc006RespService.class, Doc006RespService.class);
            respService = doc006RespService.getService();

            //  将Doc006RespDto封装到ResultDto<Doc006RespDto>
            doc006ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            doc006ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Doc006RespDto
                BeanUtils.copyProperties(respService, doc006RespDto);
                doc006ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                doc006ResultDto.setMessage(respService.getErortx());
            } else {
                doc006ResultDto.setCode(EpbEnum.EPB099999.key);
                doc006ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC006.key, EsbEnum.TRADE_CODE_DOC006.value, e.getMessage());
            doc006ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            doc006ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        doc006ResultDto.setData(doc006RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC006.key, EsbEnum.TRADE_CODE_DOC006.value, JSON.toJSONString(doc006ResultDto));
        return doc006ResultDto;
    }
}
