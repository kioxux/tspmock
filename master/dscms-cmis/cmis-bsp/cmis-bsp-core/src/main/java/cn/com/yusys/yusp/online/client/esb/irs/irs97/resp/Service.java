package cn.com.yusys.yusp.online.client.esb.irs.irs97.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 响应Service：新增授信时债项评级接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:20
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String prcscd;//交易码
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.AllMessageInfo AllMessageInfo;
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.AppMessageInfo AppMessageInfo;
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.MessageInfo MessageInfo;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.AllMessageInfo getAllMessageInfo() {
        return AllMessageInfo;
    }

    @JsonIgnore
    public void setAllMessageInfo(cn.com.yusys.yusp.online.client.esb.irs.common.AllMessageInfo allMessageInfo) {
        AllMessageInfo = allMessageInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.AppMessageInfo getAppMessageInfo() {
        return AppMessageInfo;
    }

    @JsonIgnore
    public void setAppMessageInfo(cn.com.yusys.yusp.online.client.esb.irs.common.AppMessageInfo appMessageInfo) {
        AppMessageInfo = appMessageInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.MessageInfo getMessageInfo() {
        return MessageInfo;
    }

    @JsonIgnore
    public void setMessageInfo(cn.com.yusys.yusp.online.client.esb.irs.common.MessageInfo messageInfo) {
        MessageInfo = messageInfo;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", prcscd='" + prcscd + '\'' +
                ", AllMessageInfo=" + AllMessageInfo +
                ", AppMessageInfo=" + AppMessageInfo +
                ", MessageInfo=" + MessageInfo +
                '}';
    }
}
