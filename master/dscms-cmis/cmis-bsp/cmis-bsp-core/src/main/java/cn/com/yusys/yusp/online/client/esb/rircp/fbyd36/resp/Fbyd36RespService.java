package cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp;

/**
 * 响应Service：授信列表查询
 * @author code-generator
 * @version 1.0             
 */      
public class Fbyd36RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
