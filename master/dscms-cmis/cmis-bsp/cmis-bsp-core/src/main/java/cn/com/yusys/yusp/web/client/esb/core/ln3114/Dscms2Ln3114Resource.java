package cn.com.yusys.yusp.web.client.esb.core.ln3114;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3114.Ln3114ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3114.Ln3114RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3114.Lstqgtqhk;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3114.req.Ln3114ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3114.resp.Ln3114RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3114.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3114)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3114Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3114Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3114
     * 交易描述：贷款提前还款期供试算
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3114:贷款提前还款期供试算")
    @PostMapping("/ln3114")
    protected @ResponseBody
    ResultDto<Ln3114RespDto> ln3114(@Validated @RequestBody Ln3114ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3114.key, EsbEnum.TRADE_CODE_LN3114.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3114.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3114.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3114.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3114.resp.Service();
        Ln3114ReqService ln3114ReqService = new Ln3114ReqService();
        Ln3114RespService ln3114RespService = new Ln3114RespService();
        Ln3114RespDto ln3114RespDto = new Ln3114RespDto();
        ResultDto<Ln3114RespDto> ln3114ResultDto = new ResultDto<Ln3114RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3114ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3114.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3114ReqService.setService(reqService);
            // 将ln3114ReqService转换成ln3114ReqServiceMap
            Map ln3114ReqServiceMap = beanMapUtil.beanToMap(ln3114ReqService);
            context.put("tradeDataMap", ln3114ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3114.key, EsbEnum.TRADE_CODE_LN3114.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3114.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3114.key, EsbEnum.TRADE_CODE_LN3114.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3114RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3114RespService.class, Ln3114RespService.class);
            respService = ln3114RespService.getService();

            ln3114ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3114ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3114RespDto
                BeanUtils.copyProperties(respService, ln3114RespDto);
                List<Lstqgtqhk> lstqgtqhks = new ArrayList<Lstqgtqhk>();
                if (respService.getLstqgtqhk_ARRAY() != null && CollectionUtils.nonEmpty(respService.getLstqgtqhk_ARRAY().getRecord())) {
                    List<Record> recordList = respService.getLstqgtqhk_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3114.resp.Record record : recordList) {
                        Lstqgtqhk lstqgtqhk = new Lstqgtqhk();
                        BeanUtils.copyProperties(record, lstqgtqhk);
                        lstqgtqhks.add(lstqgtqhk);
                    }
                }
                ln3114RespDto.setLstqgtqhk(lstqgtqhks);
                ln3114ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3114ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3114ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3114ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3114.key, EsbEnum.TRADE_CODE_LN3114.value, e.getMessage());
            ln3114ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3114ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3114ResultDto.setData(ln3114RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3114.key, EsbEnum.TRADE_CODE_LN3114.value, JSON.toJSONString(ln3114ResultDto));
        return ln3114ResultDto;
    }
}
