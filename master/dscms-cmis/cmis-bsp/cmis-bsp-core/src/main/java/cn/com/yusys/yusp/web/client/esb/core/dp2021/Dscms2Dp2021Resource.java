package cn.com.yusys.yusp.web.client.esb.core.dp2021;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.dp2021.req.Dp2021ReqService;
import cn.com.yusys.yusp.online.client.esb.core.dp2021.req.Service;
import cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Dp2021RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:客户账号-子账号互查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(dp2021)")
@RestController
@RequestMapping("/api/dscms2coredp")
public class Dscms2Dp2021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dp2021Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：dp2021
     * 交易描述：客户账号-子账号互查
     *
     * @param dp2021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("dp2021:客户账号-子账号互查")
    @PostMapping("/dp2021")
    protected @ResponseBody
    ResultDto<Dp2021RespDto> dp2021(@Validated @RequestBody Dp2021ReqDto dp2021ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value, JSON.toJSONString(dp2021ReqDto));
        Service reqService = new Service();
        cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Service();
        Dp2021ReqService dp2021ReqService = new Dp2021ReqService();
        Dp2021RespService dp2021RespService = new Dp2021RespService();
        Dp2021RespDto dp2021RespDto = new Dp2021RespDto();
        ResultDto<Dp2021RespDto> dp2021ResultDto = new ResultDto<Dp2021RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将dp2021ReqDto转换成reqService
            BeanUtils.copyProperties(dp2021ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DP2021.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            dp2021ReqService.setService(reqService);
            // 将dp2021ReqService转换成dp2021ReqServiceMap
            Map dp2021ReqServiceMap = beanMapUtil.beanToMap(dp2021ReqService);
            context.put("tradeDataMap", dp2021ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DP2021.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            dp2021RespService = beanMapUtil.mapToBean(tradeDataMap, Dp2021RespService.class, Dp2021RespService.class);
            respService = dp2021RespService.getService();
            //  将respService转换成Dp2021RespDto
            dp2021ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            dp2021ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                BeanUtils.copyProperties(respService, dp2021RespDto);
                cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Lstacctinfo dp2021RespList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Lstacctinfo());
                respService.setList(dp2021RespList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Record> dp2021RespRecords = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo> dp2021RespDtoLists = dp2021RespRecords.parallelStream().map(dp2021RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo dp2021RespDtoList = new cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Lstacctinfo();
                        BeanUtils.copyProperties(dp2021RespRecord, dp2021RespDtoList);
                        return dp2021RespDtoList;
                    }).collect(Collectors.toList());
                    dp2021RespDto.setLstacctinfo(dp2021RespDtoLists);
                }
                dp2021ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                dp2021ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                dp2021ResultDto.setCode(EpbEnum.EPB099999.key);
                dp2021ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value, e.getMessage());
            dp2021ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            dp2021ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }

        dp2021ResultDto.setData(dp2021RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2021.key, EsbEnum.TRADE_CODE_DP2021.value, JSON.toJSONString(dp2021ResultDto));
        return dp2021ResultDto;
    }
}
