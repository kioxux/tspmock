package cn.com.yusys.yusp.online.client.esb.core.mbt999.resp;

/**
 * 响应Service：V5通用记账
 * @author lihh
 * @version 1.0             
 */      
public class Mbt999RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
