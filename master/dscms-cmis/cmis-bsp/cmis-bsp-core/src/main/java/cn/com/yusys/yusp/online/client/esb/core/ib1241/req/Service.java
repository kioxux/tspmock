package cn.com.yusys.yusp.online.client.esb.core.ib1241.req;

/**
 * 请求Service：外围自动冲正(出库撤销)
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String yqzhriqi;//原前置日期
    private String yqzhlshu;//原前置流水
    private String yjiaoyrq;//原交易日期
    private String ygyliush;//原柜员流水
    private String chanpnma;//产品码
    private String qianzhrq;//前置日期
    private String qianzhls;//前置流水

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getYqzhriqi() {
        return yqzhriqi;
    }

    public void setYqzhriqi(String yqzhriqi) {
        this.yqzhriqi = yqzhriqi;
    }

    public String getYqzhlshu() {
        return yqzhlshu;
    }

    public void setYqzhlshu(String yqzhlshu) {
        this.yqzhlshu = yqzhlshu;
    }

    public String getYjiaoyrq() {
        return yjiaoyrq;
    }

    public void setYjiaoyrq(String yjiaoyrq) {
        this.yjiaoyrq = yjiaoyrq;
    }

    public String getYgyliush() {
        return ygyliush;
    }

    public void setYgyliush(String ygyliush) {
        this.ygyliush = ygyliush;
    }

    public String getChanpnma() {
        return chanpnma;
    }

    public void setChanpnma(String chanpnma) {
        this.chanpnma = chanpnma;
    }

    public String getQianzhrq() {
        return qianzhrq;
    }

    public void setQianzhrq(String qianzhrq) {
        this.qianzhrq = qianzhrq;
    }

    public String getQianzhls() {
        return qianzhls;
    }

    public void setQianzhls(String qianzhls) {
        this.qianzhls = qianzhls;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", yqzhriqi='" + yqzhriqi + '\'' +
                ", yqzhlshu='" + yqzhlshu + '\'' +
                ", yjiaoyrq='" + yjiaoyrq + '\'' +
                ", ygyliush='" + ygyliush + '\'' +
                ", chanpnma='" + chanpnma + '\'' +
                ", qianzhrq='" + qianzhrq + '\'' +
                ", qianzhls='" + qianzhls + '\'' +
                '}';
    }
}
