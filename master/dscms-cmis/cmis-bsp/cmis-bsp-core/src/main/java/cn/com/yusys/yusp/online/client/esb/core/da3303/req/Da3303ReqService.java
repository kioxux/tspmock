package cn.com.yusys.yusp.online.client.esb.core.da3303.req;

/**
 * 请求Service：抵债资产信息维护
 *
 * @author leehuang
 * @version 1.0
 */
public class Da3303ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Da3303ReqService{" +
				"service=" + service +
				'}';
	}
}
