package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 保证金信息(AssureAccInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:12
 */
public class AssureAccInfo {

    private List<AssureAccInfoRecord> record; // 保证金信息

    public List<AssureAccInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<AssureAccInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "AssureAccInfo{" +
                "record=" + record +
                '}';
    }
}
