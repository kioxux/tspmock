package cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.req;

/**
 * 请求Service：查询日初（合约）信息历史表（利翃）总数据量
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd09ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd09ReqService{" +
                "service=" + service +
                '}';
    }
}
