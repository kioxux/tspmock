package cn.com.yusys.yusp.online.client.esb.ypxt.cxypbh.req;

/**
 * 请求Service：通过票据号码查询押品编号
 * @author lihh
 * @version 1.0
 */
public class CxypbhReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
