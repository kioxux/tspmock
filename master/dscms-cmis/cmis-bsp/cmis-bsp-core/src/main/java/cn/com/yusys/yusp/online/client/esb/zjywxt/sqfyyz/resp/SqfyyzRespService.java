package cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.resp;

/**
 * 响应Service：行方验证房源信息
 *
 * @author chenyong
 * @version 1.0
 */
public class SqfyyzRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
