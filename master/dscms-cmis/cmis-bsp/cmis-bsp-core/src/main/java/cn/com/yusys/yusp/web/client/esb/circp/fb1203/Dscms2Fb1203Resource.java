package cn.com.yusys.yusp.web.client.esb.circp.fb1203;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1203.req.Fb1203ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1203.resp.Fb1203RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1203.req.Fb1203ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1203.resp.Fb1203RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1203）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1203Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1203Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1203
     * 交易描述：质押物金额覆盖校验
     *
     * @param fb1203ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1203")
    protected @ResponseBody
    ResultDto<Fb1203RespDto> fb1203(@Validated @RequestBody Fb1203ReqDto fb1203ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1203.key, EsbEnum.TRADE_CODE_FB1203.value, JSON.toJSONString(fb1203ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1203.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1203.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1203.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1203.resp.Service();
        Fb1203ReqService fb1203ReqService = new Fb1203ReqService();
        Fb1203RespService fb1203RespService = new Fb1203RespService();
        Fb1203RespDto fb1203RespDto = new Fb1203RespDto();
        ResultDto<Fb1203RespDto> fb1203ResultDto = new ResultDto<Fb1203RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1203ReqDto转换成reqService
            BeanUtils.copyProperties(fb1203ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1203.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1203ReqService.setService(reqService);
            // 将fb1203ReqService转换成fb1203ReqServiceMap
            Map fb1203ReqServiceMap = beanMapUtil.beanToMap(fb1203ReqService);
            context.put("tradeDataMap", fb1203ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1203.key, EsbEnum.TRADE_CODE_FB1203.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1203.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1203.key, EsbEnum.TRADE_CODE_FB1203.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1203RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1203RespService.class, Fb1203RespService.class);
            respService = fb1203RespService.getService();

            fb1203ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1203ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1203RespDto
                BeanUtils.copyProperties(respService, fb1203RespDto);

                fb1203ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1203ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1203ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1203ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1203.key, EsbEnum.TRADE_CODE_FB1203.value, e.getMessage());
            fb1203ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1203ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1203ResultDto.setData(fb1203RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1203.key, EsbEnum.TRADE_CODE_FB1203.value, JSON.toJSONString(fb1203ResultDto));
        return fb1203ResultDto;
    }
}
