package cn.com.yusys.yusp.online.client.esb.core.dp2280.resp;

import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Service：子账户序号查询接口
 *
 * @author
 * @version 1.0
 * @since 2021/4/16上午10:17:34
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private List<ListNm> listnm; // 账户对照表信息
    private String kehuhaoo; // 客户号
    private String kehuzhmc; // 客户账户名称
    private String kehuleix; // 客户类型
    private String doqiriqi; // 客户号证件到期日期
    private String zhfutojn; // 支付条件
    private String zhjnzlei; // 证件种类
    private String zhjhaoma; // 证件号码
    private String pngzzlei; // 凭证种类
    private String pngzphao; // 凭证批号
    private String pngzxhao; // 凭证序号
    private String pngzhhao; // 凭证号
    private String tduifwei; // 通兑范围
    private String glpinzbz; // 关联凭证标志
    private String kehuzhlx; // 客户账号类型
    private String kehumich; // 客户名称
    private String kehuzhwm; // 客户中文名
    private String kehuywmc; // 客户英文名
    private String lminzhhu; // 联名账户标志
    private String gxileixn; // 关系类型
    private String khzhglbz; // 客户账户关联标志
    private String baomibzz; // 保密标志
    private String kzjedjbz; // 客户账户金额冻结标志
    private String kzfbdjbz; // 客户账户封闭冻结标志
    private String kzzsbfbz; // 客户账户只收不付标志
    private String kzzfbsbz; // 客户账户只付不收标志
    private String kzhuztai; // 客户账户状态
    private String dspzsyzt; // 凭证状态
    private String kachapbh; // 卡产品编号
    private String sbkbiaoz; // 社保卡标志
    private String mimaztai; // 密码状态
    private BigDecimal dangqhsh; // 当前行数
    private String kadxiang; // 卡对象
    private String kadengji; // 卡等级

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public List<ListNm> getListnm() {
        return listnm;
    }

    public void setListnm(List<ListNm> listnm) {
        this.listnm = listnm;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getKehuleix() {
        return kehuleix;
    }

    public void setKehuleix(String kehuleix) {
        this.kehuleix = kehuleix;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPngzphao() {
        return pngzphao;
    }

    public void setPngzphao(String pngzphao) {
        this.pngzphao = pngzphao;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getPngzhhao() {
        return pngzhhao;
    }

    public void setPngzhhao(String pngzhhao) {
        this.pngzhhao = pngzhhao;
    }

    public String getTduifwei() {
        return tduifwei;
    }

    public void setTduifwei(String tduifwei) {
        this.tduifwei = tduifwei;
    }

    public String getGlpinzbz() {
        return glpinzbz;
    }

    public void setGlpinzbz(String glpinzbz) {
        this.glpinzbz = glpinzbz;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getKehumich() {
        return kehumich;
    }

    public void setKehumich(String kehumich) {
        this.kehumich = kehumich;
    }

    public String getKehuzhwm() {
        return kehuzhwm;
    }

    public void setKehuzhwm(String kehuzhwm) {
        this.kehuzhwm = kehuzhwm;
    }

    public String getKehuywmc() {
        return kehuywmc;
    }

    public void setKehuywmc(String kehuywmc) {
        this.kehuywmc = kehuywmc;
    }

    public String getLminzhhu() {
        return lminzhhu;
    }

    public void setLminzhhu(String lminzhhu) {
        this.lminzhhu = lminzhhu;
    }

    public String getGxileixn() {
        return gxileixn;
    }

    public void setGxileixn(String gxileixn) {
        this.gxileixn = gxileixn;
    }

    public String getKhzhglbz() {
        return khzhglbz;
    }

    public void setKhzhglbz(String khzhglbz) {
        this.khzhglbz = khzhglbz;
    }

    public String getBaomibzz() {
        return baomibzz;
    }

    public void setBaomibzz(String baomibzz) {
        this.baomibzz = baomibzz;
    }

    public String getKzjedjbz() {
        return kzjedjbz;
    }

    public void setKzjedjbz(String kzjedjbz) {
        this.kzjedjbz = kzjedjbz;
    }

    public String getKzfbdjbz() {
        return kzfbdjbz;
    }

    public void setKzfbdjbz(String kzfbdjbz) {
        this.kzfbdjbz = kzfbdjbz;
    }

    public String getKzzsbfbz() {
        return kzzsbfbz;
    }

    public void setKzzsbfbz(String kzzsbfbz) {
        this.kzzsbfbz = kzzsbfbz;
    }

    public String getKzzfbsbz() {
        return kzzfbsbz;
    }

    public void setKzzfbsbz(String kzzfbsbz) {
        this.kzzfbsbz = kzzfbsbz;
    }

    public String getKzhuztai() {
        return kzhuztai;
    }

    public void setKzhuztai(String kzhuztai) {
        this.kzhuztai = kzhuztai;
    }

    public String getDspzsyzt() {
        return dspzsyzt;
    }

    public void setDspzsyzt(String dspzsyzt) {
        this.dspzsyzt = dspzsyzt;
    }

    public String getKachapbh() {
        return kachapbh;
    }

    public void setKachapbh(String kachapbh) {
        this.kachapbh = kachapbh;
    }

    public String getSbkbiaoz() {
        return sbkbiaoz;
    }

    public void setSbkbiaoz(String sbkbiaoz) {
        this.sbkbiaoz = sbkbiaoz;
    }

    public String getMimaztai() {
        return mimaztai;
    }

    public void setMimaztai(String mimaztai) {
        this.mimaztai = mimaztai;
    }

    public BigDecimal getDangqhsh() {
        return dangqhsh;
    }

    public void setDangqhsh(BigDecimal dangqhsh) {
        this.dangqhsh = dangqhsh;
    }

    public String getKadxiang() {
        return kadxiang;
    }

    public void setKadxiang(String kadxiang) {
        this.kadxiang = kadxiang;
    }

    public String getKadengji() {
        return kadengji;
    }

    public void setKadengji(String kadengji) {
        this.kadengji = kadengji;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", listnm=" + listnm +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", kehuleix='" + kehuleix + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", pngzzlei='" + pngzzlei + '\'' +
                ", pngzphao='" + pngzphao + '\'' +
                ", pngzxhao='" + pngzxhao + '\'' +
                ", pngzhhao='" + pngzhhao + '\'' +
                ", tduifwei='" + tduifwei + '\'' +
                ", glpinzbz='" + glpinzbz + '\'' +
                ", kehuzhlx='" + kehuzhlx + '\'' +
                ", kehumich='" + kehumich + '\'' +
                ", kehuzhwm='" + kehuzhwm + '\'' +
                ", kehuywmc='" + kehuywmc + '\'' +
                ", lminzhhu='" + lminzhhu + '\'' +
                ", gxileixn='" + gxileixn + '\'' +
                ", khzhglbz='" + khzhglbz + '\'' +
                ", baomibzz='" + baomibzz + '\'' +
                ", kzjedjbz='" + kzjedjbz + '\'' +
                ", kzfbdjbz='" + kzfbdjbz + '\'' +
                ", kzzsbfbz='" + kzzsbfbz + '\'' +
                ", kzzfbsbz='" + kzzfbsbz + '\'' +
                ", kzhuztai='" + kzhuztai + '\'' +
                ", dspzsyzt='" + dspzsyzt + '\'' +
                ", kachapbh='" + kachapbh + '\'' +
                ", sbkbiaoz='" + sbkbiaoz + '\'' +
                ", mimaztai='" + mimaztai + '\'' +
                ", dangqhsh=" + dangqhsh +
                ", kadxiang='" + kadxiang + '\'' +
                ", kadengji='" + kadengji + '\'' +
                '}';
    }
}