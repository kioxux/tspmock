package cn.com.yusys.yusp.web.client.esb.core.ln3085;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3085.req.Ln3085ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3085.resp.Ln3085RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3085.req.Ln3085ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3085.resp.Ln3085RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3085)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3085Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3085Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3085
     * 交易描述：贷款本息减免
     *
     * @param ln3085ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3085:贷款本息减免")
    @PostMapping("/ln3085")
    protected @ResponseBody
    ResultDto<Ln3085RespDto> ln3085(@Validated @RequestBody Ln3085ReqDto ln3085ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3085.key, EsbEnum.TRADE_CODE_LN3085.value, JSON.toJSONString(ln3085ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3085.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3085.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3085.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3085.resp.Service();
        Ln3085ReqService ln3085ReqService = new Ln3085ReqService();
        Ln3085RespService ln3085RespService = new Ln3085RespService();
        Ln3085RespDto ln3085RespDto = new Ln3085RespDto();
        ResultDto<Ln3085RespDto> ln3085ResultDto = new ResultDto<Ln3085RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3085ReqDto转换成reqService
            BeanUtils.copyProperties(ln3085ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3085.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3085ReqService.setService(reqService);
            // 将ln3085ReqService转换成ln3085ReqServiceMap
            Map ln3085ReqServiceMap = beanMapUtil.beanToMap(ln3085ReqService);
            context.put("tradeDataMap", ln3085ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3085.key, EsbEnum.TRADE_CODE_LN3085.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3085.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3085.key, EsbEnum.TRADE_CODE_LN3085.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3085RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3085RespService.class, Ln3085RespService.class);
            respService = ln3085RespService.getService();

            ln3085ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3085ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3085RespDto
                BeanUtils.copyProperties(respService, ln3085RespDto);
                ln3085ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3085ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3085ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3085ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3085.key, EsbEnum.TRADE_CODE_LN3085.value, e.getMessage());
            ln3085ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3085ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3085ResultDto.setData(ln3085RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3085.key, EsbEnum.TRADE_CODE_LN3085.value, JSON.toJSONString(ln3085ResultDto));
        return ln3085ResultDto;
    }
}
