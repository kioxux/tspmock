package cn.com.yusys.yusp.web.server.biz.xdqt0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdqt0008.req.Xdqt0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdqt0008.resp.Xdqt0008RespDto;
import cn.com.yusys.yusp.dto.server.xdqt0008.req.Xdqt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0008.resp.Xdqt0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizQtClientService;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:乐悠金卡关联人查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0008:贷款申请预约（企业客户）")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0008Resource.class);

    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdqt0008
     * 交易描述：乐悠金卡关联人查询
     *
     * @param xdqt0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贷款申请预约（企业客户）")
    @PostMapping("/xdqt0008")
    @Idempotent({"xdcaqt0008", "#xdqt0008ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0008RespDto xdqt0008(@Validated @RequestBody Xdqt0008ReqDto xdqt0008ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, JSON.toJSONString(xdqt0008ReqDto));
        Xdqt0008DataReqDto xdqt0008DataReqDto = new Xdqt0008DataReqDto();// 请求Data： 贷款申请预约（企业客户）
        Xdqt0008DataRespDto xdqt0008DataRespDto = new Xdqt0008DataRespDto();// 响应Data：贷款申请预约（企业客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0008.req.Data reqData = null; // 请求Data：贷款申请预约（企业客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdqt0008.resp.Data();// 响应Data：贷款申请预约（企业客户）
		Xdqt0008RespDto xdqt0008RespDto = new Xdqt0008RespDto();
		try {
            // 从 xdqt0008ReqDto获取 reqData
            reqData = xdqt0008ReqDto.getData();
            // 将 reqData 拷贝到xdqt0008DataReqDto
            BeanUtils.copyProperties(reqData, xdqt0008DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, JSON.toJSONString(xdqt0008DataReqDto));
            ResultDto<Xdqt0008DataRespDto> xdqt0008DataResultDto = dscmsCusClientService.xdqt0008(xdqt0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, JSON.toJSONString(xdqt0008DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0008RespDto.setErorcd(Optional.ofNullable(xdqt0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0008RespDto.setErortx(Optional.ofNullable(xdqt0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0008DataResultDto.getCode())) {
                xdqt0008DataRespDto = xdqt0008DataResultDto.getData();
                xdqt0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, e.getMessage());
            xdqt0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0008RespDto.setDatasq(servsq);

        xdqt0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0008.key, DscmsEnum.TRADE_CODE_XDQT0008.value, JSON.toJSONString(xdqt0008RespDto));
        return xdqt0008RespDto;
    }
}
