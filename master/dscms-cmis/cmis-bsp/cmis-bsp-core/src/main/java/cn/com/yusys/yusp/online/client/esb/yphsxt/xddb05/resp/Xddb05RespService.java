package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.resp;

/**
 * 响应Service：查询基本信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xddb05RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xddb05RespService{" +
                "service=" + service +
                '}';
    }
}
