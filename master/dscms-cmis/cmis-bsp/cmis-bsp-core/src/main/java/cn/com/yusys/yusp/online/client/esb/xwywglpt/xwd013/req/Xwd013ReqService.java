package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.req;

/**
 * 请求Service：新信贷获取调查信息接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwd013ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
