package cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/16 20:22
 * @since 2021/6/16 20:22
 */
public class Body {
    private String idtftp;//证件类型
    private String idtfno;//证件号码
    private String cardno;//卡号
    private String opt;//功能码
    private String titlal;//职称
    private String custnm;//姓名
    private String gender;//性别
    private String ocpatn;//职业
    private String bkmrno;//本行员工号
    private String ntnaty;//国籍
    private String mtstat;//婚姻状况
    private String qlftin;//教育状况
    private String hmphon;//家庭电话
    private String cmmobi;//联系人移动电话
    private String email;//电子邮箱
    private String corpnm;//公司名称
    private String embnam;//凸印姓名
    private String nation;//无备注
    private String isdate;//证件起始日
    private String iedate;//证件到期日
    private String idaddr;//发证机关所在地址

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public String getTitlal() {
        return titlal;
    }

    public void setTitlal(String titlal) {
        this.titlal = titlal;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOcpatn() {
        return ocpatn;
    }

    public void setOcpatn(String ocpatn) {
        this.ocpatn = ocpatn;
    }

    public String getBkmrno() {
        return bkmrno;
    }

    public void setBkmrno(String bkmrno) {
        this.bkmrno = bkmrno;
    }

    public String getNtnaty() {
        return ntnaty;
    }

    public void setNtnaty(String ntnaty) {
        this.ntnaty = ntnaty;
    }

    public String getMtstat() {
        return mtstat;
    }

    public void setMtstat(String mtstat) {
        this.mtstat = mtstat;
    }

    public String getQlftin() {
        return qlftin;
    }

    public void setQlftin(String qlftin) {
        this.qlftin = qlftin;
    }

    public String getHmphon() {
        return hmphon;
    }

    public void setHmphon(String hmphon) {
        this.hmphon = hmphon;
    }

    public String getCmmobi() {
        return cmmobi;
    }

    public void setCmmobi(String cmmobi) {
        this.cmmobi = cmmobi;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCorpnm() {
        return corpnm;
    }

    public void setCorpnm(String corpnm) {
        this.corpnm = corpnm;
    }

    public String getEmbnam() {
        return embnam;
    }

    public void setEmbnam(String embnam) {
        this.embnam = embnam;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getIsdate() {
        return isdate;
    }

    public void setIsdate(String isdate) {
        this.isdate = isdate;
    }

    public String getIedate() {
        return iedate;
    }

    public void setIedate(String iedate) {
        this.iedate = iedate;
    }

    public String getIdaddr() {
        return idaddr;
    }

    public void setIdaddr(String idaddr) {
        this.idaddr = idaddr;
    }

    @Override
    public String toString() {
        return "Service{" +
                "idtftp='" + idtftp + '\'' +
                "idtfno='" + idtfno + '\'' +
                "cardno='" + cardno + '\'' +
                "opt='" + opt + '\'' +
                "titlal='" + titlal + '\'' +
                "custnm='" + custnm + '\'' +
                "gender='" + gender + '\'' +
                "ocpatn='" + ocpatn + '\'' +
                "bkmrno='" + bkmrno + '\'' +
                "ntnaty='" + ntnaty + '\'' +
                "mtstat='" + mtstat + '\'' +
                "qlftin='" + qlftin + '\'' +
                "hmphon='" + hmphon + '\'' +
                "cmmobi='" + cmmobi + '\'' +
                "email='" + email + '\'' +
                "corpnm='" + corpnm + '\'' +
                "embnam='" + embnam + '\'' +
                "nation='" + nation + '\'' +
                "isdate='" + isdate + '\'' +
                "iedate='" + iedate + '\'' +
                "idaddr='" + idaddr + '\'' +
                '}';
    }
}
