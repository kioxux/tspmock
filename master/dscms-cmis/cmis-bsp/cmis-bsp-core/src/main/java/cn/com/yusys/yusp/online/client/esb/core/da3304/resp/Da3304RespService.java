package cn.com.yusys.yusp.online.client.esb.core.da3304.resp;

/**
 * 响应Service：资产转让借据筛选
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3304RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3304RespService{" +
                "service=" + service +
                '}';
    }
}
