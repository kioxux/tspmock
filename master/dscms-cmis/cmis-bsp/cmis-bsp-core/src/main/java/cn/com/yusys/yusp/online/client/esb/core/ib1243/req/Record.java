package cn.com.yusys.yusp.online.client.esb.core.ib1243.req;

import java.math.BigDecimal;


/**
 * 请求Service：交易账户信息输入
 */
public class Record {
    private String zhaolaiy;//账号来源
    private String dxzhxhao;//待销账序号
    private String jigolaiy;//机构来源
    private String jigouhao;//机构号
    private String yewubima;//业务编码
    private String zhhaxhao;//账号序号
    private String kehuzhao;//客户账号
    private String zhhuzwmc;//账户名称
    private String huobdaih;//货币代号
    private String chaohubz;//账户钞汇标志
    private String yanmbzhi;//密码校验方式
    private String jiaoymma;//交易密码
    private String jiedaibz;//借贷标志
    private BigDecimal jiaoyije;//交易金额
    private String sffeiybz;//是否费用标志
    private String shoufdma;//收费代码
    private String shfdmamc;//收费代码名称
    private Integer shuliang;//数量
    private BigDecimal yingshfy;//应收费用
    private String chupriqi;//出票日期
    private String zhipleix;//支票种类
    private String zhiphaom;//支票号码
    private String zhaiyodm;//摘要代码
    private String zhaiyoms;//摘要描述
    private String zhfumima;//支付密码
    private String disfyhxx;//第三方用户信息
    private String beizhuxx;//备注信息
    private String sfzdbhzh;//是否指定保护账号标志
    private String baohzhao;//保护客户账号
    private String baohzxho;//保护子账号序号
    private String baohzhmc;//保护账户名称
    private String duifkhlx;//对方客户类型
    private String duifkhzh;//对方客户账号
    private String dfzhhxuh;//对方子账户序号
    private String duifminc;//对方户名
    private String duifjglx;//对方金融机构类型
    private String duifjgdm;//对方金融机构代码
    private String duifjgmc;//对方金融机构名称
    private String kzjkbozh;//控制解控标志
    private String konzhbho;//控制编号
    private String kongzhzl;//控制种类
    private String donjfanw;//冻结范围
    private BigDecimal xkjkjine;//需控制/解控金额
    private String konzhiyy;//控制原因
    private String zdjkriqi;//自动解控日期
    private String dlywhaoo;//代理业务号
    private String xnjnxmdm;//现金项目代码
    private String kehuhaoo;//客户号
    private String xianzzbz;//现转标志
    private String qudaohao;//渠道
    private String sfzdjbsf;//是否登记不收费
    private String shofjely;//收费金额来源
    private String koukfans;//扣款方式
    private String jiejuhao;//借据号
    private Integer xuhaoooo;//序号

    public String getZhaolaiy() {
        return zhaolaiy;
    }

    public void setZhaolaiy(String zhaolaiy) {
        this.zhaolaiy = zhaolaiy;
    }

    public String getDxzhxhao() {
        return dxzhxhao;
    }

    public void setDxzhxhao(String dxzhxhao) {
        this.dxzhxhao = dxzhxhao;
    }

    public String getJigolaiy() {
        return jigolaiy;
    }

    public void setJigolaiy(String jigolaiy) {
        this.jigolaiy = jigolaiy;
    }

    public String getJigouhao() {
        return jigouhao;
    }

    public void setJigouhao(String jigouhao) {
        this.jigouhao = jigouhao;
    }

    public String getYewubima() {
        return yewubima;
    }

    public void setYewubima(String yewubima) {
        this.yewubima = yewubima;
    }

    public String getZhhaxhao() {
        return zhhaxhao;
    }

    public void setZhhaxhao(String zhhaxhao) {
        this.zhhaxhao = zhhaxhao;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getYanmbzhi() {
        return yanmbzhi;
    }

    public void setYanmbzhi(String yanmbzhi) {
        this.yanmbzhi = yanmbzhi;
    }

    public String getJiaoymma() {
        return jiaoymma;
    }

    public void setJiaoymma(String jiaoymma) {
        this.jiaoymma = jiaoymma;
    }

    public String getJiedaibz() {
        return jiedaibz;
    }

    public void setJiedaibz(String jiedaibz) {
        this.jiedaibz = jiedaibz;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getSffeiybz() {
        return sffeiybz;
    }

    public void setSffeiybz(String sffeiybz) {
        this.sffeiybz = sffeiybz;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public Integer getShuliang() {
        return shuliang;
    }

    public void setShuliang(Integer shuliang) {
        this.shuliang = shuliang;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public String getChupriqi() {
        return chupriqi;
    }

    public void setChupriqi(String chupriqi) {
        this.chupriqi = chupriqi;
    }

    public String getZhipleix() {
        return zhipleix;
    }

    public void setZhipleix(String zhipleix) {
        this.zhipleix = zhipleix;
    }

    public String getZhiphaom() {
        return zhiphaom;
    }

    public void setZhiphaom(String zhiphaom) {
        this.zhiphaom = zhiphaom;
    }

    public String getZhaiyodm() {
        return zhaiyodm;
    }

    public void setZhaiyodm(String zhaiyodm) {
        this.zhaiyodm = zhaiyodm;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getZhfumima() {
        return zhfumima;
    }

    public void setZhfumima(String zhfumima) {
        this.zhfumima = zhfumima;
    }

    public String getDisfyhxx() {
        return disfyhxx;
    }

    public void setDisfyhxx(String disfyhxx) {
        this.disfyhxx = disfyhxx;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getSfzdbhzh() {
        return sfzdbhzh;
    }

    public void setSfzdbhzh(String sfzdbhzh) {
        this.sfzdbhzh = sfzdbhzh;
    }

    public String getBaohzhao() {
        return baohzhao;
    }

    public void setBaohzhao(String baohzhao) {
        this.baohzhao = baohzhao;
    }

    public String getBaohzxho() {
        return baohzxho;
    }

    public void setBaohzxho(String baohzxho) {
        this.baohzxho = baohzxho;
    }

    public String getBaohzhmc() {
        return baohzhmc;
    }

    public void setBaohzhmc(String baohzhmc) {
        this.baohzhmc = baohzhmc;
    }

    public String getDuifkhlx() {
        return duifkhlx;
    }

    public void setDuifkhlx(String duifkhlx) {
        this.duifkhlx = duifkhlx;
    }

    public String getDuifkhzh() {
        return duifkhzh;
    }

    public void setDuifkhzh(String duifkhzh) {
        this.duifkhzh = duifkhzh;
    }

    public String getDfzhhxuh() {
        return dfzhhxuh;
    }

    public void setDfzhhxuh(String dfzhhxuh) {
        this.dfzhhxuh = dfzhhxuh;
    }

    public String getDuifminc() {
        return duifminc;
    }

    public void setDuifminc(String duifminc) {
        this.duifminc = duifminc;
    }

    public String getDuifjglx() {
        return duifjglx;
    }

    public void setDuifjglx(String duifjglx) {
        this.duifjglx = duifjglx;
    }

    public String getDuifjgdm() {
        return duifjgdm;
    }

    public void setDuifjgdm(String duifjgdm) {
        this.duifjgdm = duifjgdm;
    }

    public String getDuifjgmc() {
        return duifjgmc;
    }

    public void setDuifjgmc(String duifjgmc) {
        this.duifjgmc = duifjgmc;
    }

    public String getKzjkbozh() {
        return kzjkbozh;
    }

    public void setKzjkbozh(String kzjkbozh) {
        this.kzjkbozh = kzjkbozh;
    }

    public String getKonzhbho() {
        return konzhbho;
    }

    public void setKonzhbho(String konzhbho) {
        this.konzhbho = konzhbho;
    }

    public String getKongzhzl() {
        return kongzhzl;
    }

    public void setKongzhzl(String kongzhzl) {
        this.kongzhzl = kongzhzl;
    }

    public String getDonjfanw() {
        return donjfanw;
    }

    public void setDonjfanw(String donjfanw) {
        this.donjfanw = donjfanw;
    }

    public BigDecimal getXkjkjine() {
        return xkjkjine;
    }

    public void setXkjkjine(BigDecimal xkjkjine) {
        this.xkjkjine = xkjkjine;
    }

    public String getKonzhiyy() {
        return konzhiyy;
    }

    public void setKonzhiyy(String konzhiyy) {
        this.konzhiyy = konzhiyy;
    }

    public String getZdjkriqi() {
        return zdjkriqi;
    }

    public void setZdjkriqi(String zdjkriqi) {
        this.zdjkriqi = zdjkriqi;
    }

    public String getDlywhaoo() {
        return dlywhaoo;
    }

    public void setDlywhaoo(String dlywhaoo) {
        this.dlywhaoo = dlywhaoo;
    }

    public String getXnjnxmdm() {
        return xnjnxmdm;
    }

    public void setXnjnxmdm(String xnjnxmdm) {
        this.xnjnxmdm = xnjnxmdm;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getXianzzbz() {
        return xianzzbz;
    }

    public void setXianzzbz(String xianzzbz) {
        this.xianzzbz = xianzzbz;
    }

    public String getQudaohao() {
        return qudaohao;
    }

    public void setQudaohao(String qudaohao) {
        this.qudaohao = qudaohao;
    }

    public String getSfzdjbsf() {
        return sfzdjbsf;
    }

    public void setSfzdjbsf(String sfzdjbsf) {
        this.sfzdjbsf = sfzdjbsf;
    }

    public String getShofjely() {
        return shofjely;
    }

    public void setShofjely(String shofjely) {
        this.shofjely = shofjely;
    }

    public String getKoukfans() {
        return koukfans;
    }

    public void setKoukfans(String koukfans) {
        this.koukfans = koukfans;
    }

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    @Override
    public String toString() {
        return "Record{" +
                "zhaolaiy='" + zhaolaiy + '\'' +
                "dxzhxhao='" + dxzhxhao + '\'' +
                "jigolaiy='" + jigolaiy + '\'' +
                "jigouhao='" + jigouhao + '\'' +
                "yewubima='" + yewubima + '\'' +
                "zhhaxhao='" + zhhaxhao + '\'' +
                "kehuzhao='" + kehuzhao + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "yanmbzhi='" + yanmbzhi + '\'' +
                "jiaoymma='" + jiaoymma + '\'' +
                "jiedaibz='" + jiedaibz + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "sffeiybz='" + sffeiybz + '\'' +
                "shoufdma='" + shoufdma + '\'' +
                "shfdmamc='" + shfdmamc + '\'' +
                "shuliang='" + shuliang + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "chupriqi='" + chupriqi + '\'' +
                "zhipleix='" + zhipleix + '\'' +
                "zhiphaom='" + zhiphaom + '\'' +
                "zhaiyodm='" + zhaiyodm + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "zhfumima='" + zhfumima + '\'' +
                "disfyhxx='" + disfyhxx + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "sfzdbhzh='" + sfzdbhzh + '\'' +
                "baohzhao='" + baohzhao + '\'' +
                "baohzxho='" + baohzxho + '\'' +
                "baohzhmc='" + baohzhmc + '\'' +
                "duifkhlx='" + duifkhlx + '\'' +
                "duifkhzh='" + duifkhzh + '\'' +
                "dfzhhxuh='" + dfzhhxuh + '\'' +
                "duifminc='" + duifminc + '\'' +
                "duifjglx='" + duifjglx + '\'' +
                "duifjgdm='" + duifjgdm + '\'' +
                "duifjgmc='" + duifjgmc + '\'' +
                "kzjkbozh='" + kzjkbozh + '\'' +
                "konzhbho='" + konzhbho + '\'' +
                "kongzhzl='" + kongzhzl + '\'' +
                "donjfanw='" + donjfanw + '\'' +
                "xkjkjine='" + xkjkjine + '\'' +
                "konzhiyy='" + konzhiyy + '\'' +
                "zdjkriqi='" + zdjkriqi + '\'' +
                "dlywhaoo='" + dlywhaoo + '\'' +
                "xnjnxmdm='" + xnjnxmdm + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "xianzzbz='" + xianzzbz + '\'' +
                "qudaohao='" + qudaohao + '\'' +
                "sfzdjbsf='" + sfzdjbsf + '\'' +
                "shofjely='" + shofjely + '\'' +
                "koukfans='" + koukfans + '\'' +
                "jiejuhao='" + jiejuhao + '\'' +
                "xuhaoooo='" + xuhaoooo + '\'' +
                '}';
    }
}
