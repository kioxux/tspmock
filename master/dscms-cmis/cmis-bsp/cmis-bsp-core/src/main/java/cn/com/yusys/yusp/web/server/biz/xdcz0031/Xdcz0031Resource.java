package cn.com.yusys.yusp.web.server.biz.xdcz0031;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0031.req.Xdcz0031ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0031.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0031.resp.Xdcz0031RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0031.req.Xdcz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0031.resp.Xdcz0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 *
 */
@Api(tags = "XDCZ0031:查询连续出账的次数")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0031Resource {
	private static final Logger logger = LoggerFactory.getLogger(Xdcz0031Resource.class);
	@Autowired
	private DscmsBizCzClientService dscmsBizCzClientService;

	/**
	 * 交易码：xdcz0031
	 * 交易描述：校验额度是否足额，合同是否足额
	 *
	 * @param xdcz0031ReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("查询连续出账的次数")
	@PostMapping("/xdcz0031")
	@Idempotent({"xdcz0031", "#xdcz0031ReqDto.datasq"})
	protected @ResponseBody
	Xdcz0031RespDto xdcz0031(@Validated @RequestBody Xdcz0031ReqDto xdcz0031ReqDto) throws Exception {

		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031ReqDto));
		Xdcz0031DataReqDto xdcz0031DataReqDto = new Xdcz0031DataReqDto();// 请求Data：
		Xdcz0031DataRespDto xdcz0031DataRespDto = new Xdcz0031DataRespDto();// 响应Data

		cn.com.yusys.yusp.dto.server.biz.xdcz0031.req.Data reqData = null; // 请求Data
        Data respData = new Data();
		Xdcz0031RespDto xdcz0031RespDto = new Xdcz0031RespDto();
		try {
			// 从 xdcz0031ReqDto获取 reqData
			reqData = xdcz0031ReqDto.getData();
			// 将 reqData 拷贝到xdcz0030DataReqDto
			BeanUtils.copyProperties(reqData, xdcz0031DataReqDto);

			logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031DataReqDto));
			ResultDto<Xdcz0031DataRespDto> xdcz0031DataResultDto = dscmsBizCzClientService.xdcz0031(xdcz0031DataReqDto);
			logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031DataResultDto));
			// 从返回值中获取响应码和响应消息
			xdcz0031RespDto.setErorcd(Optional.ofNullable(xdcz0031DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
			xdcz0031RespDto.setErortx(Optional.ofNullable(xdcz0031DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

			if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0031DataResultDto.getCode())) {
				xdcz0031DataRespDto = xdcz0031DataResultDto.getData();
				xdcz0031RespDto.setErorcd(SuccessEnum.SUCCESS.key);
				xdcz0031RespDto.setErortx(SuccessEnum.SUCCESS.value);
				// 将 xdcz0031DataRespDto拷贝到 respData
				BeanUtils.copyProperties(xdcz0031DataRespDto, respData);
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, e.getMessage());
			xdcz0031RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
			xdcz0031RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
		}
		logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
		String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
		logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
		xdcz0031RespDto.setDatasq(servsq);

		xdcz0031RespDto.setData(respData);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0031.key, DscmsEnum.TRADE_CODE_XDCZ0031.value, JSON.toJSONString(xdcz0031RespDto));
		return xdcz0031RespDto;
	}
}
