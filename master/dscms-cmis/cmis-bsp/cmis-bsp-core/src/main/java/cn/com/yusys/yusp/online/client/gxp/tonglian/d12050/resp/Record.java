package cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp;

import java.math.BigDecimal;

/**
 * 响应Message：账单列表查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Record {
    private String cardno;//卡号
    private String stdate;//账单年月
    private String currcd;//币种
    private String billdt;//账单日
    private String custnm;//姓名
    private String pmdudt;//到期还款日期
    private BigDecimal cdtlmt;//信用额度
    private BigDecimal cshlmt;//取现额度
    private BigDecimal tplimt;//临时额度
    private String bgdate;//临时额度开始日期
    private String eddate;//临时额度结束日期
    private String ltsmdt;//上个账单日期
    private BigDecimal stbgbl;//上期账单余额
    private BigDecimal stcrbl;//当期账单余额
    private BigDecimal cdcsam;//当期取现金额
    private BigDecimal qlgcbl;//全部应还款额
    private BigDecimal toduam;//最小还款额
    private BigDecimal cdamdb;//当期借记金额
    private Integer cdnbdb;//当期借记交易笔数
    private BigDecimal cdamcr;//当期贷记金额
    private Integer cdnbcr;//当期贷记交易笔数
    private String agescd;//账龄
    private String gcdyfl;//是否已全额还款
    private BigDecimal ptbgbl;//期初积分余额
    private BigDecimal cderpt;//当期新增积分
    private BigDecimal cdajpt;//当期调整积分
    private BigDecimal cddbpt;//当期兑换积分
    private BigDecimal pntbal;//积分余额
    private String dlcrin;//是否存在外币标识
    private String dlcrcd;//外币币种
    private BigDecimal ctdamt;//当期费用金额
    private Integer ctdcnt;//当期费用笔数
    private BigDecimal ctdint;//当期利息金额
    private Integer ctdres;//当期利息笔数

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getStdate() {
        return stdate;
    }

    public void setStdate(String stdate) {
        this.stdate = stdate;
    }

    public String getCurrcd() {
        return currcd;
    }

    public void setCurrcd(String currcd) {
        this.currcd = currcd;
    }

    public String getBilldt() {
        return billdt;
    }

    public void setBilldt(String billdt) {
        this.billdt = billdt;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getPmdudt() {
        return pmdudt;
    }

    public void setPmdudt(String pmdudt) {
        this.pmdudt = pmdudt;
    }

    public BigDecimal getCdtlmt() {
        return cdtlmt;
    }

    public void setCdtlmt(BigDecimal cdtlmt) {
        this.cdtlmt = cdtlmt;
    }

    public BigDecimal getCshlmt() {
        return cshlmt;
    }

    public void setCshlmt(BigDecimal cshlmt) {
        this.cshlmt = cshlmt;
    }

    public BigDecimal getTplimt() {
        return tplimt;
    }

    public void setTplimt(BigDecimal tplimt) {
        this.tplimt = tplimt;
    }

    public String getBgdate() {
        return bgdate;
    }

    public void setBgdate(String bgdate) {
        this.bgdate = bgdate;
    }

    public String getEddate() {
        return eddate;
    }

    public void setEddate(String eddate) {
        this.eddate = eddate;
    }

    public String getLtsmdt() {
        return ltsmdt;
    }

    public void setLtsmdt(String ltsmdt) {
        this.ltsmdt = ltsmdt;
    }

    public BigDecimal getStbgbl() {
        return stbgbl;
    }

    public void setStbgbl(BigDecimal stbgbl) {
        this.stbgbl = stbgbl;
    }

    public BigDecimal getStcrbl() {
        return stcrbl;
    }

    public void setStcrbl(BigDecimal stcrbl) {
        this.stcrbl = stcrbl;
    }

    public BigDecimal getCdcsam() {
        return cdcsam;
    }

    public void setCdcsam(BigDecimal cdcsam) {
        this.cdcsam = cdcsam;
    }

    public BigDecimal getQlgcbl() {
        return qlgcbl;
    }

    public void setQlgcbl(BigDecimal qlgcbl) {
        this.qlgcbl = qlgcbl;
    }

    public BigDecimal getToduam() {
        return toduam;
    }

    public void setToduam(BigDecimal toduam) {
        this.toduam = toduam;
    }

    public BigDecimal getCdamdb() {
        return cdamdb;
    }

    public void setCdamdb(BigDecimal cdamdb) {
        this.cdamdb = cdamdb;
    }

    public Integer getCdnbdb() {
        return cdnbdb;
    }

    public void setCdnbdb(Integer cdnbdb) {
        this.cdnbdb = cdnbdb;
    }

    public BigDecimal getCdamcr() {
        return cdamcr;
    }

    public void setCdamcr(BigDecimal cdamcr) {
        this.cdamcr = cdamcr;
    }

    public Integer getCdnbcr() {
        return cdnbcr;
    }

    public void setCdnbcr(Integer cdnbcr) {
        this.cdnbcr = cdnbcr;
    }

    public String getAgescd() {
        return agescd;
    }

    public void setAgescd(String agescd) {
        this.agescd = agescd;
    }

    public String getGcdyfl() {
        return gcdyfl;
    }

    public void setGcdyfl(String gcdyfl) {
        this.gcdyfl = gcdyfl;
    }

    public BigDecimal getPtbgbl() {
        return ptbgbl;
    }

    public void setPtbgbl(BigDecimal ptbgbl) {
        this.ptbgbl = ptbgbl;
    }

    public BigDecimal getCderpt() {
        return cderpt;
    }

    public void setCderpt(BigDecimal cderpt) {
        this.cderpt = cderpt;
    }

    public BigDecimal getCdajpt() {
        return cdajpt;
    }

    public void setCdajpt(BigDecimal cdajpt) {
        this.cdajpt = cdajpt;
    }

    public BigDecimal getCddbpt() {
        return cddbpt;
    }

    public void setCddbpt(BigDecimal cddbpt) {
        this.cddbpt = cddbpt;
    }

    public BigDecimal getPntbal() {
        return pntbal;
    }

    public void setPntbal(BigDecimal pntbal) {
        this.pntbal = pntbal;
    }

    public String getDlcrin() {
        return dlcrin;
    }

    public void setDlcrin(String dlcrin) {
        this.dlcrin = dlcrin;
    }

    public String getDlcrcd() {
        return dlcrcd;
    }

    public void setDlcrcd(String dlcrcd) {
        this.dlcrcd = dlcrcd;
    }

    public BigDecimal getCtdamt() {
        return ctdamt;
    }

    public void setCtdamt(BigDecimal ctdamt) {
        this.ctdamt = ctdamt;
    }

    public Integer getCtdcnt() {
        return ctdcnt;
    }

    public void setCtdcnt(Integer ctdcnt) {
        this.ctdcnt = ctdcnt;
    }

    public BigDecimal getCtdint() {
        return ctdint;
    }

    public void setCtdint(BigDecimal ctdint) {
        this.ctdint = ctdint;
    }

    public Integer getCtdres() {
        return ctdres;
    }

    public void setCtdres(Integer ctdres) {
        this.ctdres = ctdres;
    }

    @Override
    public String toString() {
        return "Record{" +
                "cardno='" + cardno + '\'' +
                ", stdate='" + stdate + '\'' +
                ", currcd='" + currcd + '\'' +
                ", billdt='" + billdt + '\'' +
                ", custnm='" + custnm + '\'' +
                ", pmdudt='" + pmdudt + '\'' +
                ", cdtlmt=" + cdtlmt +
                ", cshlmt=" + cshlmt +
                ", tplimt=" + tplimt +
                ", bgdate='" + bgdate + '\'' +
                ", eddate='" + eddate + '\'' +
                ", ltsmdt='" + ltsmdt + '\'' +
                ", stbgbl=" + stbgbl +
                ", stcrbl=" + stcrbl +
                ", cdcsam=" + cdcsam +
                ", qlgcbl=" + qlgcbl +
                ", toduam=" + toduam +
                ", cdamdb=" + cdamdb +
                ", cdnbdb=" + cdnbdb +
                ", cdamcr=" + cdamcr +
                ", cdnbcr=" + cdnbcr +
                ", agescd=" + agescd +
                ", gcdyfl='" + gcdyfl + '\'' +
                ", ptbgbl=" + ptbgbl +
                ", cderpt=" + cderpt +
                ", cdajpt=" + cdajpt +
                ", cddbpt=" + cddbpt +
                ", pntbal=" + pntbal +
                ", dlcrin='" + dlcrin + '\'' +
                ", dlcrcd='" + dlcrcd + '\'' +
                ", ctdamt=" + ctdamt +
                ", ctdcnt=" + ctdcnt +
                ", ctdint=" + ctdint +
                ", ctdres=" + ctdres +
                '}';
    }
}
