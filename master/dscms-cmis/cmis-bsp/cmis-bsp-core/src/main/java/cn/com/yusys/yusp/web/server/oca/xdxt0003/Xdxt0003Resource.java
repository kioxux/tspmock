package cn.com.yusys.yusp.web.server.oca.xdxt0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.oca.xdxt0003.req.Xdxt0003ReqDto;
import cn.com.yusys.yusp.dto.server.oca.xdxt0003.resp.Xdxt0003RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.req.Xdxt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0003.resp.Xdxt0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/*
 *
 * 接口处理类:分页查询小微客户经理
 *
 * @author chenyong
 * @version 1.0
 */


@Api(tags = "XDXT0003:分页查询小微客户经理")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0003Resource.class);
    @Autowired
    private DscmsXtClientService dscmsXtClientService;

    /*
     *
     * 交易码：xdxt0003
     * 交易描述：分页查询小微客户经理
     *
     * @param xdxt0003ReqDto
     * @return
     * @throws Exception

     */
    @ApiOperation("分页查询小微客户经理")
    @PostMapping("/xdxt0003")
    //@Idempotent({"xdcaxt0003", "#xdxt0003ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0003RespDto xdxt0003(@Validated @RequestBody Xdxt0003ReqDto xdxt0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0003.key, DscmsEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003ReqDto));
        Xdxt0003DataReqDto xdxt0003DataReqDto = new Xdxt0003DataReqDto();// 请求Data： 分页查询小微客户经理
        Xdxt0003DataRespDto xdxt0003DataRespDto = new Xdxt0003DataRespDto();// 响应Data：分页查询小微客户经理
        Xdxt0003RespDto xdxt0003RespDto = new Xdxt0003RespDto();
        cn.com.yusys.yusp.dto.server.oca.xdxt0003.req.Data reqData = null; // 请求Data：分页查询小微客户经理
        cn.com.yusys.yusp.dto.server.oca.xdxt0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.oca.xdxt0003.resp.Data();// 响应Data：分页查询小微客户经理
        try {
            // 从 xdxt0003ReqDto获取 reqData
            reqData = xdxt0003ReqDto.getData();
            // 将 reqData 拷贝到xdxt0003DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0003DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0003.key, DscmsEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataReqDto));
            ResultDto<Xdxt0003DataRespDto> xdxt0003DataResultDto = dscmsXtClientService.xdxt0003(xdxt0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0003.key, DscmsEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0003RespDto.setErorcd(Optional.ofNullable(xdxt0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0003RespDto.setErortx(Optional.ofNullable(xdxt0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0003DataResultDto.getCode())) {
                xdxt0003DataRespDto = xdxt0003DataResultDto.getData();
                xdxt0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0003.key, DscmsEnum.TRADE_CODE_XDXT0003.value, e.getMessage());
            xdxt0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0003RespDto.setDatasq(servsq);

        xdxt0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0003.key, DscmsEnum.TRADE_CODE_XDXT0003.value, JSON.toJSONString(xdxt0003RespDto));
        return xdxt0003RespDto;
    }
}
