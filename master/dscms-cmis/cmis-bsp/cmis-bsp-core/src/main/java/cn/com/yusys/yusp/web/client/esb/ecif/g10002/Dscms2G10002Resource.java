package cn.com.yusys.yusp.web.client.esb.ecif.g10002;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10002.req.G10002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10002.req.GroupArrayMem;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10002.resp.G10002RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g10002.req.G10002ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g10002.req.Record;
import cn.com.yusys.yusp.online.client.esb.ecif.g10002.resp.G10002RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g10002)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G10002Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G10002Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 客户群组信息维护
     *
     * @param g10002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g10002:客户群组信息维护")
    @PostMapping("/g10002")
    protected @ResponseBody
    ResultDto<G10002RespDto> g10002(@Validated @RequestBody G10002ReqDto g10002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10002.key, EsbEnum.TRADE_CODE_G10002.value, JSON.toJSONString(g10002ReqDto));
        cn.com.yusys.yusp.online.client.esb.ecif.g10002.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g10002.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g10002.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g10002.resp.Service();
        G10002ReqService g10002ReqService = new G10002ReqService();
        G10002RespService g10002RespService = new G10002RespService();
        G10002RespDto g10002RespDto = new G10002RespDto();
        ResultDto<G10002RespDto> g10002ResultDto = new ResultDto<G10002RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将G10002ReqDto转换成reqService
            BeanUtils.copyProperties(g10002ReqDto, reqService);

            if (CollectionUtils.nonEmpty(g10002ReqDto.getList())) {
                cn.com.yusys.yusp.online.client.esb.ecif.g10002.req.List list = new cn.com.yusys.yusp.online.client.esb.ecif.g10002.req.List();
                List<GroupArrayMem> groupArrayMemlist = Optional.ofNullable(g10002ReqDto.getList()).orElse(new ArrayList<>());
                java.util.List<Record> targetList = groupArrayMemlist.stream().map(groupArrayMem -> {
                    Record target = new Record();
                    BeanUtils.copyProperties(groupArrayMem, target);
                    return target;
                }).collect(Collectors.toList());
                list.setRecord(targetList);
                reqService.setList(list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_G10002.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            g10002ReqService.setService(reqService);
            // 将g10002ReqService转换成g10002ReqServiceMap
            Map g10002ReqServiceMap = beanMapUtil.beanToMap(g10002ReqService);
            context.put("tradeDataMap", g10002ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10002.key, EsbEnum.TRADE_CODE_G10002.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G10002.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10002.key, EsbEnum.TRADE_CODE_G10002.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g10002RespService = beanMapUtil.mapToBean(tradeDataMap, G10002RespService.class, G10002RespService.class);
            respService = g10002RespService.getService();
            //  将G10002RespDto封装到ResultDto<G10002RespDto>
            g10002ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g10002ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G10002RespDto
                BeanUtils.copyProperties(respService, g10002RespDto);
                g10002ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g10002ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                BeanUtils.copyProperties(respService, g10002RespDto);
                g10002ResultDto.setCode(EpbEnum.EPB099999.key);
                g10002ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10002.key, EsbEnum.TRADE_CODE_G10002.value, e.getMessage());
            g10002ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g10002ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g10002ResultDto.setData(g10002RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10002.key, EsbEnum.TRADE_CODE_G10002.value, JSON.toJSONString(g10002ResultDto));
        return g10002ResultDto;
    }
}
