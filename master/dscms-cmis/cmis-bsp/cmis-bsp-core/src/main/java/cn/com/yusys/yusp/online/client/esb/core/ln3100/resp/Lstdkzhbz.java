package cn.com.yusys.yusp.online.client.esb.core.ln3100.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款账户保证信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Lstdkzhbz {
    private String bzrkehuh;//保证人客户号
    private String baozhfsh;//保证方式
    private BigDecimal baozjine;//保证金额
    private String beizhuuu;//备注信息
    private String guanlzht;//关联状态
    private String danbzhao;//担保账号
    private String dbzhzxuh;//担保账号子序号

    public String getBzrkehuh() {
        return bzrkehuh;
    }

    public void setBzrkehuh(String bzrkehuh) {
        this.bzrkehuh = bzrkehuh;
    }

    public String getBaozhfsh() {
        return baozhfsh;
    }

    public void setBaozhfsh(String baozhfsh) {
        this.baozhfsh = baozhfsh;
    }

    public BigDecimal getBaozjine() {
        return baozjine;
    }

    public void setBaozjine(BigDecimal baozjine) {
        this.baozjine = baozjine;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getGuanlzht() {
        return guanlzht;
    }

    public void setGuanlzht(String guanlzht) {
        this.guanlzht = guanlzht;
    }

    public String getDanbzhao() {
        return danbzhao;
    }

    public void setDanbzhao(String danbzhao) {
        this.danbzhao = danbzhao;
    }

    public String getDbzhzxuh() {
        return dbzhzxuh;
    }

    public void setDbzhzxuh(String dbzhzxuh) {
        this.dbzhzxuh = dbzhzxuh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "bzrkehuh='" + bzrkehuh + '\'' +
                "baozhfsh='" + baozhfsh + '\'' +
                "baozjine='" + baozjine + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "guanlzht='" + guanlzht + '\'' +
                "danbzhao='" + danbzhao + '\'' +
                "dbzhzxuh='" + dbzhzxuh + '\'' +
                '}';
    }
}
