package cn.com.yusys.yusp.online.client.esb.core.ln3249.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款指定日期利息试算
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String hetongbh;//合同编号
    private String kehuhaoo;//客户号
    private String kehumnch;//客户名称
    private String yngyjigo;//营业机构
    private String zhngjigo;//账务机构
    private String kaihriqi;//开户日期
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String dkqixian;//贷款期限(月)
    private String daikxtai;//贷款形态
    private String yjfyjzht;//应计非应计状态
    private String dkzhhzht;//贷款账户状态
    private String huobdhao;//货币代号
    private BigDecimal jiejuuje;//借据金额
    private BigDecimal benjinnn;//本金
    private BigDecimal qianxiii;//欠息
    private BigDecimal faxiiiii;//罚息
    private BigDecimal fuxiiiii;//复息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehumnch() {
        return kehumnch;
    }

    public void setKehumnch(String kehumnch) {
        this.kehumnch = kehumnch;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getBenjinnn() {
        return benjinnn;
    }

    public void setBenjinnn(BigDecimal benjinnn) {
        this.benjinnn = benjinnn;
    }

    public BigDecimal getQianxiii() {
        return qianxiii;
    }

    public void setQianxiii(BigDecimal qianxiii) {
        this.qianxiii = qianxiii;
    }

    public BigDecimal getFaxiiiii() {
        return faxiiiii;
    }

    public void setFaxiiiii(BigDecimal faxiiiii) {
        this.faxiiiii = faxiiiii;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehumnch='" + kehumnch + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", dkqixian='" + dkqixian + '\'' +
                ", daikxtai='" + daikxtai + '\'' +
                ", yjfyjzht='" + yjfyjzht + '\'' +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", benjinnn=" + benjinnn +
                ", qianxiii=" + qianxiii +
                ", faxiiiii=" + faxiiiii +
                ", fuxiiiii=" + fuxiiiii +
                '}';
    }
}
