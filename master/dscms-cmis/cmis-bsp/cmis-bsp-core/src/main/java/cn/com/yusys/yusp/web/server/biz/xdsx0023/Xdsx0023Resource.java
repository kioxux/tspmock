package cn.com.yusys.yusp.web.server.biz.xdsx0023;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0023.req.Xdsx0023ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0023.resp.Xdsx0023RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0023.req.Xdsx0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0023.resp.Xdsx0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:保函协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0023:保函协议查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0023Resource.class);
    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0023
     * 交易描述：保函协议查询
     *
     * @param xdsx0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保函协议查询")
    @PostMapping("/xdsx0023")
//    @Idempotent({"xdcasx0023", "#xdsx0023ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0023RespDto xdsx0023(@Validated @RequestBody Xdsx0023ReqDto xdsx0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023ReqDto));
        Xdsx0023DataReqDto xdsx0023DataReqDto = new Xdsx0023DataReqDto();// 请求Data： 保函协议查询
        Xdsx0023DataRespDto xdsx0023DataRespDto = new Xdsx0023DataRespDto();// 响应Data：保函协议查询
        Xdsx0023RespDto xdsx0023RespDto = new Xdsx0023RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdsx0023.req.Data reqData = null; // 请求Data：保函协议查询
        cn.com.yusys.yusp.dto.server.biz.xdsx0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0023.resp.Data();// 响应Data：保函协议查询
        try {
            // 从 xdsx0023ReqDto获取 reqData
            reqData = xdsx0023ReqDto.getData();
            // 将 reqData 拷贝到xdsx0023DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0023DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataReqDto));
            ResultDto<Xdsx0023DataRespDto> xdsx0023DataResultDto = dscmsBizSxClientService.xdsx0023(xdsx0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0023RespDto.setErorcd(Optional.ofNullable(xdsx0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0023RespDto.setErortx(Optional.ofNullable(xdsx0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0023DataResultDto.getCode())) {
                xdsx0023DataRespDto = xdsx0023DataResultDto.getData();
                xdsx0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0023DataRespDto, respData);
            }
        } catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, e.getMessage());
            xdsx0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0023RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, e.getMessage());
            xdsx0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0023RespDto.setDatasq(servsq);

        xdsx0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0023.key, DscmsEnum.TRADE_CODE_XDSX0023.value, JSON.toJSONString(xdsx0023RespDto));
        return xdsx0023RespDto;
    }
}
