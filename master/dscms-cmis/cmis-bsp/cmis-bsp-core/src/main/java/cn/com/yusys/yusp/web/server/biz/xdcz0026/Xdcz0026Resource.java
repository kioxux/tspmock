package cn.com.yusys.yusp.web.server.biz.xdcz0026;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0026.req.Xdcz0026ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0026.resp.Xdcz0026RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0026.req.Xdcz0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0026.resp.Xdcz0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:风控调用信贷放款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDCZ0026:风控调用信贷放款")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0026Resource.class);
    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0026
     * 交易描述：风控调用信贷放款
     *
     * @param xdcz0026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控调用信贷放款")
    @PostMapping("/xdcz0026")
   //@Idempotent({"xdcz0026", "#xdcz0026ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0026RespDto xdcz0026(@Validated @RequestBody Xdcz0026ReqDto xdcz0026ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026ReqDto));
        Xdcz0026DataReqDto xdcz0026DataReqDto = new Xdcz0026DataReqDto();// 请求Data： 风控调用信贷放款
        Xdcz0026DataRespDto xdcz0026DataRespDto = new Xdcz0026DataRespDto();// 响应Data：风控调用信贷放款
        Xdcz0026RespDto xdcz0026RespDto = new Xdcz0026RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdcz0026.req.Data reqData = null; // 请求Data：风控调用信贷放款
        cn.com.yusys.yusp.dto.server.biz.xdcz0026.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdcz0026.resp.Data();// 响应Data：风控调用信贷放款

        try {
            // 从 xdcz0026ReqDto获取 reqData
            reqData = xdcz0026ReqDto.getData();
            // 将 reqData 拷贝到xdcz0026DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataReqDto));
            ResultDto<Xdcz0026DataRespDto> xdcz0026DataResultDto = dscmsBizCzClientService.xdcz0026(xdcz0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0026RespDto.setErorcd(Optional.ofNullable(xdcz0026DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0026RespDto.setErortx(Optional.ofNullable(xdcz0026DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0026DataResultDto.getCode())) {
                xdcz0026DataRespDto = xdcz0026DataResultDto.getData();
                xdcz0026RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0026RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0026DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0026DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, e.getMessage());
            xdcz0026RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0026RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0026RespDto.setDatasq(servsq);

        xdcz0026RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0026.key, DscmsEnum.TRADE_CODE_XDCZ0026.value, JSON.toJSONString(xdcz0026RespDto));
        return xdcz0026RespDto;
    }
}
