package cn.com.yusys.yusp.online.client.esb.rircp.fbxw06.resp;

/**
 * 请求Service：利率定价测算提交接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午5:34:07
 */
 public class  Fbxw06RespService {

        private Service service;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }
}
