package cn.com.yusys.yusp.web.server.biz.xdzx0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzx0002.req.Xdzx0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzx0002.resp.Xdzx0002RespDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.req.Xdzx0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0002.resp.Xdzx0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:征信授权查看
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZX0002:征信授权查看")
@RestController
@RequestMapping("/api/dscms")
public class Xdzx0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzx0002Resource.class);
    @Autowired
    private DscmsBizZxClientService dscmsBizZxClientService;

    /**
     * 交易码：xdzx0002
     * 交易描述：征信授权查看
     *
     * @param xdzx0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("征信授权查看")
    @PostMapping("/xdzx0002")
    //@Idempotent({"xdcazx0002", "#xdzx0002ReqDto.datasq"})
    protected @ResponseBody
    Xdzx0002RespDto xdzx0002(@Validated @RequestBody Xdzx0002ReqDto xdzx0002ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002ReqDto));
        Xdzx0002DataReqDto xdzx0002DataReqDto = new Xdzx0002DataReqDto();// 请求Data： 征信授权查看
        Xdzx0002DataRespDto xdzx0002DataRespDto = new Xdzx0002DataRespDto();// 响应Data：征信授权查看
        Xdzx0002RespDto xdzx0002RespDto = new Xdzx0002RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzx0002.req.Data reqData = null; // 请求Data：征信授权查看
        cn.com.yusys.yusp.dto.server.biz.xdzx0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzx0002.resp.Data();// 响应Data：征信授权查看

        try {
            // 从 xdzx0002ReqDto获取 reqData
            reqData = xdzx0002ReqDto.getData();
            // 将 reqData 拷贝到xdzx0002DataReqDto
            BeanUtils.copyProperties(reqData, xdzx0002DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataReqDto));
            ResultDto<Xdzx0002DataRespDto> xdzx0002DataResultDto = dscmsBizZxClientService.xdzx0002(xdzx0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzx0002RespDto.setErorcd(Optional.ofNullable(xdzx0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzx0002RespDto.setErortx(Optional.ofNullable(xdzx0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzx0002DataResultDto.getCode())) {
                xdzx0002DataRespDto = xdzx0002DataResultDto.getData();
                xdzx0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzx0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzx0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzx0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, e.getMessage());
            xdzx0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzx0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzx0002RespDto.setDatasq(servsq);

        xdzx0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0002.key, DscmsEnum.TRADE_CODE_XDZX0002.value, JSON.toJSONString(xdzx0002RespDto));
        return xdzx0002RespDto;
    }
}
