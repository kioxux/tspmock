package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 行政处罚基本信息
 */
@JsonPropertyOrder(alphabetic = true)
public class ENTCASEBASEINFO implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ILLEGACTTYPE")
    private String ILLEGACTTYPE;//违法行为类型
    @JsonProperty(value = "PENAUTH_CN")
    private String PENAUTH_CN;//决定机关名称
    @JsonProperty(value = "PENCONTENT")
    private String PENCONTENT;//行政处罚内容
    @JsonProperty(value = "PENDECISSDATE")
    private String PENDECISSDATE;//处罚决定日期
    @JsonProperty(value = "PENDECNO")
    private String PENDECNO;//决定书文号
    @JsonProperty(value = "PUBLICDATE")
    private String PUBLICDATE;//  公示日期

    @JsonIgnore
    public String getILLEGACTTYPE() {
        return ILLEGACTTYPE;
    }

    @JsonIgnore
    public void setILLEGACTTYPE(String ILLEGACTTYPE) {
        this.ILLEGACTTYPE = ILLEGACTTYPE;
    }

    @JsonIgnore
    public String getPENAUTH_CN() {
        return PENAUTH_CN;
    }

    @JsonIgnore
    public void setPENAUTH_CN(String PENAUTH_CN) {
        this.PENAUTH_CN = PENAUTH_CN;
    }

    @JsonIgnore
    public String getPENCONTENT() {
        return PENCONTENT;
    }

    @JsonIgnore
    public void setPENCONTENT(String PENCONTENT) {
        this.PENCONTENT = PENCONTENT;
    }

    @JsonIgnore
    public String getPENDECISSDATE() {
        return PENDECISSDATE;
    }

    @JsonIgnore
    public void setPENDECISSDATE(String PENDECISSDATE) {
        this.PENDECISSDATE = PENDECISSDATE;
    }

    @JsonIgnore
    public String getPENDECNO() {
        return PENDECNO;
    }

    @JsonIgnore
    public void setPENDECNO(String PENDECNO) {
        this.PENDECNO = PENDECNO;
    }

    @JsonIgnore
    public String getPUBLICDATE() {
        return PUBLICDATE;
    }

    @JsonIgnore
    public void setPUBLICDATE(String PUBLICDATE) {
        this.PUBLICDATE = PUBLICDATE;
    }

    @Override
    public String toString() {
        return "ENTCASEBASEINFO{" +
                "ILLEGACTTYPE='" + ILLEGACTTYPE + '\'' +
                ", PENAUTH_CN='" + PENAUTH_CN + '\'' +
                ", PENCONTENT='" + PENCONTENT + '\'' +
                ", PENDECISSDATE='" + PENDECISSDATE + '\'' +
                ", PENDECNO='" + PENDECNO + '\'' +
                ", PUBLICDATE='" + PUBLICDATE + '\'' +
                '}';
    }
}
