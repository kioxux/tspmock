package cn.com.yusys.yusp.online.client.esb.core.ln3065.req;

import java.math.BigDecimal;

/**
 * 请求Service：资产转让资金划转
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String dkkhczbz;//开户操作标志
    private String xieybhao;//协议编号
    private String xieyimch;//协议名称
    private String huobdhao;//货币代号
    private String dhfkywlx;//业务类型
    private String zrfukzhh;//对外付款账号
    private String zrfukzxh;//对外付款账号子序号
    private BigDecimal bcfkjine;//本次付款金额
    private String fukuanrq;//付款日期
    private String zrfukzht;//转让付款状态
    private String huazhfax;//资金划转方向
    private BigDecimal zhanghye;//账户余额
    private BigDecimal yueeeeee;//余额
    private BigDecimal benjinje;//本金金额
    private BigDecimal lixijine;//利息金额
    private String jydszhmc;//交易对手账户名称
    private String jydsleix;//交易对手类型
    private String jydshmch;//交易对手名称
    private String zhkaihhh;//账户开户行行号
    private String zhkaihhm;//账户开户行行名
    private BigDecimal jiaoyije;//交易金额
    private String sunyrzzh;//损益入账账号
    private String syrzzhxh;//损益入账账号子序号
    private String bjyskzhh;//其他应收款账号(本金)
    private String qyskzhzh;//其他应收款账号子序号(本金)
    private String lxyskzhh;//其他应收款账号(利息)
    private String lxyskzxh;//其他应收款账号子序号(利息)
    private String bjysfzhh;//其他应付款账号(本金)
    private String bjysfzxh;//其他应付款账号子序号(本金)
    private String lxysfzhh;//其他应付款账号(利息)
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    private String jydszhao;//交易对手账号
    private String jydszhzh;//交易对手账号子序号
    private String nbuzhhao;//内部账号
    private String nbuzhzxh;//内部账号子序号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getDhfkywlx() {
        return dhfkywlx;
    }

    public void setDhfkywlx(String dhfkywlx) {
        this.dhfkywlx = dhfkywlx;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public BigDecimal getBcfkjine() {
        return bcfkjine;
    }

    public void setBcfkjine(BigDecimal bcfkjine) {
        this.bcfkjine = bcfkjine;
    }

    public String getFukuanrq() {
        return fukuanrq;
    }

    public void setFukuanrq(String fukuanrq) {
        this.fukuanrq = fukuanrq;
    }

    public String getZrfukzht() {
        return zrfukzht;
    }

    public void setZrfukzht(String zrfukzht) {
        this.zrfukzht = zrfukzht;
    }

    public String getHuazhfax() {
        return huazhfax;
    }

    public void setHuazhfax(String huazhfax) {
        this.huazhfax = huazhfax;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public BigDecimal getYueeeeee() {
        return yueeeeee;
    }

    public void setYueeeeee(BigDecimal yueeeeee) {
        this.yueeeeee = yueeeeee;
    }

    public BigDecimal getBenjinje() {
        return benjinje;
    }

    public void setBenjinje(BigDecimal benjinje) {
        this.benjinje = benjinje;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    public String getJydsleix() {
        return jydsleix;
    }

    public void setJydsleix(String jydsleix) {
        this.jydsleix = jydsleix;
    }

    public String getJydshmch() {
        return jydshmch;
    }

    public void setJydshmch(String jydshmch) {
        this.jydshmch = jydshmch;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getSunyrzzh() {
        return sunyrzzh;
    }

    public void setSunyrzzh(String sunyrzzh) {
        this.sunyrzzh = sunyrzzh;
    }

    public String getSyrzzhxh() {
        return syrzzhxh;
    }

    public void setSyrzzhxh(String syrzzhxh) {
        this.syrzzhxh = syrzzhxh;
    }

    public String getBjyskzhh() {
        return bjyskzhh;
    }

    public void setBjyskzhh(String bjyskzhh) {
        this.bjyskzhh = bjyskzhh;
    }

    public String getQyskzhzh() {
        return qyskzhzh;
    }

    public void setQyskzhzh(String qyskzhzh) {
        this.qyskzhzh = qyskzhzh;
    }

    public String getLxyskzhh() {
        return lxyskzhh;
    }

    public void setLxyskzhh(String lxyskzhh) {
        this.lxyskzhh = lxyskzhh;
    }

    public String getLxyskzxh() {
        return lxyskzxh;
    }

    public void setLxyskzxh(String lxyskzxh) {
        this.lxyskzxh = lxyskzxh;
    }

    public String getBjysfzhh() {
        return bjysfzhh;
    }

    public void setBjysfzhh(String bjysfzhh) {
        this.bjysfzhh = bjysfzhh;
    }

    public String getBjysfzxh() {
        return bjysfzxh;
    }

    public void setBjysfzxh(String bjysfzxh) {
        this.bjysfzxh = bjysfzxh;
    }

    public String getLxysfzhh() {
        return lxysfzhh;
    }

    public void setLxysfzhh(String lxysfzhh) {
        this.lxysfzhh = lxysfzhh;
    }

    public String getQtyfzhzx() {
        return qtyfzhzx;
    }

    public void setQtyfzhzx(String qtyfzhzx) {
        this.qtyfzhzx = qtyfzhzx;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getNbuzhhao() {
        return nbuzhhao;
    }

    public void setNbuzhhao(String nbuzhhao) {
        this.nbuzhhao = nbuzhhao;
    }

    public String getNbuzhzxh() {
        return nbuzhzxh;
    }

    public void setNbuzhzxh(String nbuzhzxh) {
        this.nbuzhzxh = nbuzhzxh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "xieybhao='" + xieybhao + '\'' +
                "xieyimch='" + xieyimch + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "dhfkywlx='" + dhfkywlx + '\'' +
                "zrfukzhh='" + zrfukzhh + '\'' +
                "zrfukzxh='" + zrfukzxh + '\'' +
                "bcfkjine='" + bcfkjine + '\'' +
                "fukuanrq='" + fukuanrq + '\'' +
                "zrfukzht='" + zrfukzht + '\'' +
                "huazhfax='" + huazhfax + '\'' +
                "zhanghye='" + zhanghye + '\'' +
                "yueeeeee='" + yueeeeee + '\'' +
                "benjinje='" + benjinje + '\'' +
                "lixijine='" + lixijine + '\'' +
                "jydszhmc='" + jydszhmc + '\'' +
                "jydsleix='" + jydsleix + '\'' +
                "jydshmch='" + jydshmch + '\'' +
                "zhkaihhh='" + zhkaihhh + '\'' +
                "zhkaihhm='" + zhkaihhm + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "sunyrzzh='" + sunyrzzh + '\'' +
                "syrzzhxh='" + syrzzhxh + '\'' +
                "bjyskzhh='" + bjyskzhh + '\'' +
                "qyskzhzh='" + qyskzhzh + '\'' +
                "lxyskzhh='" + lxyskzhh + '\'' +
                "lxyskzxh='" + lxyskzxh + '\'' +
                "bjysfzhh='" + bjysfzhh + '\'' +
                "bjysfzxh='" + bjysfzxh + '\'' +
                "lxysfzhh='" + lxysfzhh + '\'' +
                "qtyfzhzx='" + qtyfzhzx + '\'' +
                "jydszhao='" + jydszhao + '\'' +
                "jydszhzh='" + jydszhzh + '\'' +
                "nbuzhhao='" + nbuzhhao + '\'' +
                "nbuzhzxh='" + nbuzhzxh + '\'' +
                '}';
    }
}
