package cn.com.yusys.yusp.web.server.biz.xdzc0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0012.req.Xdzc0012ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0012.resp.Xdzc0012RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0012.req.Xdzc0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0012.resp.Xdzc0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:贸易背景资料收集通知接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0012:贸易背景资料收集通知接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0012Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0012
     * 交易描述：贸易背景资料收集通知接口
     *
     * @param xdzc0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贸易背景资料收集通知接口")
    @PostMapping("/xdzc0012")
    //@Idempotent({"xdzc012", "#xdzc0012ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0012RespDto xdzc0012(@Validated @RequestBody Xdzc0012ReqDto xdzc0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, JSON.toJSONString(xdzc0012ReqDto));
        Xdzc0012DataReqDto xdzc0012DataReqDto = new Xdzc0012DataReqDto();// 请求Data： 贸易背景资料收集通知接口
        Xdzc0012DataRespDto xdzc0012DataRespDto = new Xdzc0012DataRespDto();// 响应Data：贸易背景资料收集通知接口
        Xdzc0012RespDto xdzc0012RespDto = new Xdzc0012RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0012.req.Data reqData = null; // 请求Data：贸易背景资料收集通知接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0012.resp.Data();// 响应Data：贸易背景资料收集通知接口

        try {
            // 从 xdzc0012ReqDto获取 reqData
            reqData = xdzc0012ReqDto.getData();
            // 将 reqData 拷贝到xdzc0012DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0012DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, JSON.toJSONString(xdzc0012DataReqDto));
            ResultDto<Xdzc0012DataRespDto> xdzc0012DataResultDto = dscmsBizZcClientService.xdzc0012(xdzc0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, JSON.toJSONString(xdzc0012DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzc0012RespDto.setErorcd(Optional.ofNullable(xdzc0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0012RespDto.setErortx(Optional.ofNullable(xdzc0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0012DataResultDto.getCode())) {
                xdzc0012DataRespDto = xdzc0012DataResultDto.getData();
                xdzc0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, e.getMessage());
            xdzc0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0012RespDto.setDatasq(servsq);

        xdzc0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0012.key, DscmsEnum.TRADE_CODE_XDZC0012.value, JSON.toJSONString(xdzc0012RespDto));
        return xdzc0012RespDto;
    }
}
