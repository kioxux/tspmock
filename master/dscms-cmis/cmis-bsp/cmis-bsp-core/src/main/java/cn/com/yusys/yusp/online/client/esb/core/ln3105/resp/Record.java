package cn.com.yusys.yusp.online.client.esb.core.ln3105.resp;

import java.math.BigDecimal;

/**
 * 贷款期供明细
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class Record {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String kehmingc;//客户名称
    private String jiaoyirq;//交易日期
    private long benqqish;//本期期数
    private long benqizqs;//本期子期数
    private String yinghriq;//应还日期
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String huankzht;//还款状态
    private BigDecimal chushibj;//初始本金
    private BigDecimal chushilx;//初始利息
    private BigDecimal benjinfs;//本金发生额
    private BigDecimal benjinnn;//本金
    private BigDecimal ysyjlxfs;//应收应计利息发生额
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal csyjlxfs;//催收应计利息发生额
    private BigDecimal csyjlixi;//催收应计利息
    private BigDecimal yshqxifs;//应收欠息发生额
    private BigDecimal ysqianxi;//应收欠息
    private BigDecimal cshqxifs;//催收欠息发生额
    private BigDecimal csqianxi;//催收欠息
    private BigDecimal ysyjfxfs;//应收应计罚息发生额
    private BigDecimal ysyjfaxi;//应收应计罚息
    private BigDecimal csyjfxfs;//催收应计罚息发生额
    private BigDecimal csyjfaxi;//催收应计罚息
    private BigDecimal yshfxifs;//应收罚息发生额
    private BigDecimal yshofaxi;//应收罚息
    private BigDecimal cshofxfs;//催收罚息发生额
    private BigDecimal cshofaxi;//催收罚息
    private BigDecimal yjifxifs;//应计复息发生额
    private BigDecimal yingjifx;//应计复息
    private BigDecimal fuxiiifs;//复息发生额
    private BigDecimal fuxiiiii;//复息
    private BigDecimal hexlixfs;//核销利息发生额
    private BigDecimal hexiaolx;//核销利息
    private BigDecimal lxitzhfs;//利息调整发生额
    private BigDecimal lxtiaozh;//利息调整
    private BigDecimal yshfajfs;//应收罚金发生额
    private BigDecimal yingshfj;//应收罚金
    private BigDecimal yshfynfs;//应收费用发生额
    private BigDecimal yingshfy;//应收费用
    private String benqizht;//本期状态
    private String yjfyjzht;//应计非应计状态
    private String hkshxubh;//还款顺序编号
    private long mingxixh;//明细序号
    private String jiaoyijg;//交易机构
    private String jiaoyigy;//交易柜员
    private String jiaoyils;//交易流水
    private String jiaoyisj;//交易事件
    private String shjshuom;//事件说明
    private String jiaoyima;//交易码
    private String zhaiyoms;//摘要
    private String farendma;//法人代码
    private String weihguiy;//维护柜员
    private String weihjigo;//维护机构
    private String weihriqi;//维护日期
    private long shijchuo;//时间戳
    private String jiluztai;//记录状态

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public long getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(long benqqish) {
        this.benqqish = benqqish;
    }

    public long getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(long benqizqs) {
        this.benqizqs = benqizqs;
    }

    public long getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(long mingxixh) {
        this.mingxixh = mingxixh;
    }

    public long getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(long shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getYinghriq() {
        return yinghriq;
    }

    public void setYinghriq(String yinghriq) {
        this.yinghriq = yinghriq;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHuankzht() {
        return huankzht;
    }

    public void setHuankzht(String huankzht) {
        this.huankzht = huankzht;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public BigDecimal getBenjinfs() {
        return benjinfs;
    }

    public void setBenjinfs(BigDecimal benjinfs) {
        this.benjinfs = benjinfs;
    }

    public BigDecimal getBenjinnn() {
        return benjinnn;
    }

    public void setBenjinnn(BigDecimal benjinnn) {
        this.benjinnn = benjinnn;
    }

    public BigDecimal getYsyjlxfs() {
        return ysyjlxfs;
    }

    public void setYsyjlxfs(BigDecimal ysyjlxfs) {
        this.ysyjlxfs = ysyjlxfs;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlxfs() {
        return csyjlxfs;
    }

    public void setCsyjlxfs(BigDecimal csyjlxfs) {
        this.csyjlxfs = csyjlxfs;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYshqxifs() {
        return yshqxifs;
    }

    public void setYshqxifs(BigDecimal yshqxifs) {
        this.yshqxifs = yshqxifs;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCshqxifs() {
        return cshqxifs;
    }

    public void setCshqxifs(BigDecimal cshqxifs) {
        this.cshqxifs = cshqxifs;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfxfs() {
        return ysyjfxfs;
    }

    public void setYsyjfxfs(BigDecimal ysyjfxfs) {
        this.ysyjfxfs = ysyjfxfs;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfxfs() {
        return csyjfxfs;
    }

    public void setCsyjfxfs(BigDecimal csyjfxfs) {
        this.csyjfxfs = csyjfxfs;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshfxifs() {
        return yshfxifs;
    }

    public void setYshfxifs(BigDecimal yshfxifs) {
        this.yshfxifs = yshfxifs;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofxfs() {
        return cshofxfs;
    }

    public void setCshofxfs(BigDecimal cshofxfs) {
        this.cshofxfs = cshofxfs;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYjifxifs() {
        return yjifxifs;
    }

    public void setYjifxifs(BigDecimal yjifxifs) {
        this.yjifxifs = yjifxifs;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiifs() {
        return fuxiiifs;
    }

    public void setFuxiiifs(BigDecimal fuxiiifs) {
        this.fuxiiifs = fuxiiifs;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getHexlixfs() {
        return hexlixfs;
    }

    public void setHexlixfs(BigDecimal hexlixfs) {
        this.hexlixfs = hexlixfs;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getLxitzhfs() {
        return lxitzhfs;
    }

    public void setLxitzhfs(BigDecimal lxitzhfs) {
        this.lxitzhfs = lxitzhfs;
    }

    public BigDecimal getLxtiaozh() {
        return lxtiaozh;
    }

    public void setLxtiaozh(BigDecimal lxtiaozh) {
        this.lxtiaozh = lxtiaozh;
    }

    public BigDecimal getYshfajfs() {
        return yshfajfs;
    }

    public void setYshfajfs(BigDecimal yshfajfs) {
        this.yshfajfs = yshfajfs;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getYshfynfs() {
        return yshfynfs;
    }

    public void setYshfynfs(BigDecimal yshfynfs) {
        this.yshfynfs = yshfynfs;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public String getBenqizht() {
        return benqizht;
    }

    public void setBenqizht(String benqizht) {
        this.benqizht = benqizht;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getShjshuom() {
        return shjshuom;
    }

    public void setShjshuom(String shjshuom) {
        this.shjshuom = shjshuom;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "Lstqgmx{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "servsq='" + servsq + '\'' +
                "datasq='" + datasq + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "benqqish='" + benqqish + '\'' +
                "benqizqs='" + benqizqs + '\'' +
                "yinghriq='" + yinghriq + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "huankzht='" + huankzht + '\'' +
                "chushibj='" + chushibj + '\'' +
                "chushilx='" + chushilx + '\'' +
                "benjinfs='" + benjinfs + '\'' +
                "benjinnn='" + benjinnn + '\'' +
                "ysyjlxfs='" + ysyjlxfs + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "csyjlxfs='" + csyjlxfs + '\'' +
                "csyjlixi='" + csyjlixi + '\'' +
                "yshqxifs='" + yshqxifs + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "cshqxifs='" + cshqxifs + '\'' +
                "csqianxi='" + csqianxi + '\'' +
                "ysyjfxfs='" + ysyjfxfs + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "csyjfxfs='" + csyjfxfs + '\'' +
                "csyjfaxi='" + csyjfaxi + '\'' +
                "yshfxifs='" + yshfxifs + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "cshofxfs='" + cshofxfs + '\'' +
                "cshofaxi='" + cshofaxi + '\'' +
                "yjifxifs='" + yjifxifs + '\'' +
                "yingjifx='" + yingjifx + '\'' +
                "fuxiiifs='" + fuxiiifs + '\'' +
                "fuxiiiii='" + fuxiiiii + '\'' +
                "hexlixfs='" + hexlixfs + '\'' +
                "hexiaolx='" + hexiaolx + '\'' +
                "lxitzhfs='" + lxitzhfs + '\'' +
                "lxtiaozh='" + lxtiaozh + '\'' +
                "yshfajfs='" + yshfajfs + '\'' +
                "yingshfj='" + yingshfj + '\'' +
                "yshfynfs='" + yshfynfs + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "benqizht='" + benqizht + '\'' +
                "yjfyjzht='" + yjfyjzht + '\'' +
                "hkshxubh='" + hkshxubh + '\'' +
                "mingxixh='" + mingxixh + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "jiaoyisj='" + jiaoyisj + '\'' +
                "shjshuom='" + shjshuom + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "farendma='" + farendma + '\'' +
                "weihguiy='" + weihguiy + '\'' +
                "weihjigo='" + weihjigo + '\'' +
                "weihriqi='" + weihriqi + '\'' +
                "shijchuo='" + shijchuo + '\'' +
                "jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
