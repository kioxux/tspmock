package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj15;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.req.Xdpj15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.resp.Xdpj15RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.req.Xdpj15ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.resp.Xdpj15RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:票据承兑签发审批请求
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj15Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj15Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdpj15
     * 交易描述：票据承兑签发审批请求
     *
     * @param xdpj15ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdpj15")
    protected @ResponseBody
    ResultDto<Xdpj15RespDto> xdpj15(@Validated @RequestBody Xdpj15ReqDto xdpj15ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ15.key, EsbEnum.TRADE_CODE_XDPJ15.value, JSON.toJSONString(xdpj15ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.req.Service();
		cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.resp.Service();
        Xdpj15ReqService xdpj15ReqService = new Xdpj15ReqService();
        Xdpj15RespService xdpj15RespService = new Xdpj15RespService();
        Xdpj15RespDto xdpj15RespDto = new Xdpj15RespDto();
        ResultDto<Xdpj15RespDto> xdpj15ResultDto = new ResultDto<Xdpj15RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将xdpj15ReqDto转换成reqService
			BeanUtils.copyProperties(xdpj15ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ15.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
			reqService.setDatasq(servsq);//    全局流水
			xdpj15ReqService.setService(reqService);
			// 将xdpj15ReqService转换成xdpj15ReqServiceMap
			Map xdpj15ReqServiceMap = beanMapUtil.beanToMap(xdpj15ReqService);
			context.put("tradeDataMap", xdpj15ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ15.key, EsbEnum.TRADE_CODE_XDPJ15.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ15.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ15.key, EsbEnum.TRADE_CODE_XDPJ15.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			xdpj15RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj15RespService.class, Xdpj15RespService.class);
			respService = xdpj15RespService.getService();
			//  将respService转换成Xdpj15RespDto
			BeanUtils.copyProperties(respService, xdpj15RespDto);
			xdpj15ResultDto.setCode(Optional.ofNullable(respService.getRetcod()).orElse(SuccessEnum.SUCCESS.key));
			xdpj15ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getRetcod())) {
				//  将respService转换成XdypbdccxRespDto
				BeanUtils.copyProperties(respService,xdpj15RespDto);
				xdpj15ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				xdpj15ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				xdpj15ResultDto.setCode(EpbEnum.EPB099999.key);
				xdpj15ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ15.key, EsbEnum.TRADE_CODE_XDPJ15.value, e.getMessage());
			xdpj15ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			xdpj15ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		xdpj15ResultDto.setData(xdpj15RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ15.key, EsbEnum.TRADE_CODE_XDPJ15.value, JSON.toJSONString(xdpj15ResultDto));
        return xdpj15ResultDto;
    }
}
