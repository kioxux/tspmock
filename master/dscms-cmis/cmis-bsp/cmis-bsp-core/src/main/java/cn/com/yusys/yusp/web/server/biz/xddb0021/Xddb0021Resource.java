package cn.com.yusys.yusp.web.server.biz.xddb0021;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0021.req.Xddb0021ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0021.resp.Xddb0021RespDto;
import cn.com.yusys.yusp.dto.server.xddb0021.req.Xddb0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0021.resp.Xddb0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品共有人信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0021:根据客户名查询抵押物类型")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0021Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0021
     * 交易描述：根据客户名查询抵押物类型
     *
     * @param xddb0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品共有人信息查询")
    @PostMapping("/xddb0021")
    //@Idempotent({"xddb0021", "#xddb0021ReqDto.datasq"})
    protected @ResponseBody
    Xddb0021RespDto xddb0021(@Validated @RequestBody Xddb0021ReqDto xddb0021ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021ReqDto));
        Xddb0021DataReqDto xddb0021DataReqDto = new Xddb0021DataReqDto();// 请求Data： 根据客户名查询抵押物类型
        Xddb0021DataRespDto xddb0021DataRespDto = new Xddb0021DataRespDto();// 响应Data：根据客户名查询抵押物类型
        Xddb0021RespDto xddb0021RespDto = new Xddb0021RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddb0021.req.Data reqData = null; // 请求Data：根据客户名查询抵押物类型
        cn.com.yusys.yusp.dto.server.biz.xddb0021.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0021.resp.Data();// 响应Data：根据客户名查询抵押物类型
        try {
            // 从 xddb0021ReqDto获取 reqData
            reqData = xddb0021ReqDto.getData();
            // 将 reqData 拷贝到xddb0021DataReqDto
            BeanUtils.copyProperties(reqData, xddb0021DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021DataReqDto));
            ResultDto<Xddb0021DataRespDto> xddb0021DataResultDto = dscmsBizDbClientService.xddb0021(xddb0021DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0021RespDto.setErorcd(Optional.ofNullable(xddb0021DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0021RespDto.setErortx(Optional.ofNullable(xddb0021DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0021DataResultDto.getCode())) {
                xddb0021DataRespDto = xddb0021DataResultDto.getData();
                xddb0021RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0021RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0021DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, e.getMessage());
            xddb0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0021RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0021RespDto.setDatasq(servsq);

        xddb0021RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0021.key, DscmsEnum.TRADE_CODE_XDDB0021.value, JSON.toJSONString(xddb0021RespDto));
        return xddb0021RespDto;
    }
}
