package cn.com.yusys.yusp.web.server.biz.xddh0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0015.req.Xddh0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0015.resp.Xddh0015RespDto;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xddh0015.req.Xddh0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0015.resp.Xddh0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优农贷贷后预警通知
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0015:优农贷贷后预警通知")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0015Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0015
     * 交易描述：优农贷贷后预警通知
     *
     * @param xddh0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷贷后预警通知")
    @PostMapping("/xddh0015")
    //@Idempotent({"xddh0015", "#xddh0015ReqDto.datasq"})
    protected @ResponseBody
    Xddh0015RespDto xddh0015(@Validated @RequestBody Xddh0015ReqDto xddh0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, JSON.toJSONString(xddh0015ReqDto));
        Xddh0015DataReqDto xddh0015DataReqDto = new Xddh0015DataReqDto();// 请求Data： 优农贷贷后预警通知
        Xddh0015DataRespDto xddh0015DataRespDto = new Xddh0015DataRespDto();// 响应Data：优农贷贷后预警通知
        Xddh0015RespDto xddh0015RespDto = new Xddh0015RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0015.req.Data reqData = null; // 请求Data：优农贷贷后预警通知
        cn.com.yusys.yusp.dto.server.biz.xddh0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0015.resp.Data();// 响应Data：优农贷贷后预警通知

        try {
            // 从 xddh0015ReqDto获取 reqData
            reqData = xddh0015ReqDto.getData();
            // 将 reqData 拷贝到xddh0015DataReqDto
            BeanUtils.copyProperties(reqData, xddh0015DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, JSON.toJSONString(xddh0015DataReqDto));
            ResultDto<Xddh0015DataRespDto> xddh0015DataResultDto = dscmsBizDhClientService.xddh0015(xddh0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, JSON.toJSONString(xddh0015DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddh0015RespDto.setErorcd(Optional.ofNullable(xddh0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0015RespDto.setErortx(Optional.ofNullable(xddh0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0015DataResultDto.getCode())) {
                xddh0015DataRespDto = xddh0015DataResultDto.getData();
                xddh0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, e.getMessage());
            xddh0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0015RespDto.setDatasq(servsq);

        xddh0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0015.key, DscmsEnum.TRADE_CODE_XDDH0015.value, JSON.toJSONString(xddh0015RespDto));
        return xddh0015RespDto;
    }
}
