package cn.com.yusys.yusp.web.server.cus.xdkh0027;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0027.req.Xdkh0027ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0027.resp.Xdkh0027RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0027.req.Xdkh0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0027.resp.Xdkh0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷、优农贷行内关联自然人基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0027:优企贷、优农贷行内关联自然人基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0027Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0027
     * 交易描述：优企贷、优农贷行内关联自然人基本信息查询
     *
     * @param xdkh0027ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷行内关联自然人基本信息查询")
    @PostMapping("/xdkh0027")
    //@Idempotent({"xdcakh0027", "#xdkh0027ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0027RespDto xdkh0027(@Validated @RequestBody Xdkh0027ReqDto xdkh0027ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027ReqDto));
        Xdkh0027DataReqDto xdkh0027DataReqDto = new Xdkh0027DataReqDto();// 请求Data： 优企贷、优农贷行内关联自然人基本信息查询
        Xdkh0027DataRespDto xdkh0027DataRespDto = new Xdkh0027DataRespDto();// 响应Data：优企贷、优农贷行内关联自然人基本信息查询
        Xdkh0027RespDto xdkh0027RespDto = new Xdkh0027RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0027.req.Data reqData = null; // 请求Data：优企贷、优农贷行内关联自然人基本信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0027.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0027.resp.Data();// 响应Data：优企贷、优农贷行内关联自然人基本信息查询
        try {
            // 从 xdkh0027ReqDto获取 reqData
            reqData = xdkh0027ReqDto.getData();
            // 将 reqData 拷贝到xdkh0027DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0027DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataReqDto));
            ResultDto<Xdkh0027DataRespDto> xdkh0027DataResultDto = dscmsCusClientService.xdkh0027(xdkh0027DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0027RespDto.setErorcd(Optional.ofNullable(xdkh0027DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0027RespDto.setErortx(Optional.ofNullable(xdkh0027DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0027DataResultDto.getCode())) {
                xdkh0027DataRespDto = xdkh0027DataResultDto.getData();
                xdkh0027RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0027RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0027DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0027DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, e.getMessage());
            xdkh0027RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0027RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0027RespDto.setDatasq(servsq);

        xdkh0027RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0027.key, DscmsEnum.TRADE_CODE_XDKH0027.value, JSON.toJSONString(xdkh0027RespDto));
        return xdkh0027RespDto;
    }
}