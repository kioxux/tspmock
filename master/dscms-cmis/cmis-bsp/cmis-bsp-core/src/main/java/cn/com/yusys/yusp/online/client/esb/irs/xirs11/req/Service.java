package cn.com.yusys.yusp.online.client.esb.irs.xirs11.req;

/**
 * 请求Service：同业客户信息
 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String custid;//客户编号
    private String same_org_no;//同业机构（行）号
    private String same_org_name;//同业机构（行）号名称
    private String same_org_type;//同业机构（行）类型
    private String cantoncode;//注册地行政区划代码
    private String inputuserid;//主管客户经理编号
    private String inputorgid;//主管客户经理所属机构编号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getSame_org_no() {
        return same_org_no;
    }

    public void setSame_org_no(String same_org_no) {
        this.same_org_no = same_org_no;
    }

    public String getSame_org_name() {
        return same_org_name;
    }

    public void setSame_org_name(String same_org_name) {
        this.same_org_name = same_org_name;
    }

    public String getSame_org_type() {
        return same_org_type;
    }

    public void setSame_org_type(String same_org_type) {
        this.same_org_type = same_org_type;
    }

    public String getCantoncode() {
        return cantoncode;
    }

    public void setCantoncode(String cantoncode) {
        this.cantoncode = cantoncode;
    }

    public String getInputuserid() {
        return inputuserid;
    }

    public void setInputuserid(String inputuserid) {
        this.inputuserid = inputuserid;
    }

    public String getInputorgid() {
        return inputorgid;
    }

    public void setInputorgid(String inputorgid) {
        this.inputorgid = inputorgid;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", custid='" + custid + '\'' +
                ", same_org_no='" + same_org_no + '\'' +
                ", same_org_name='" + same_org_name + '\'' +
                ", same_org_type='" + same_org_type + '\'' +
                ", cantoncode='" + cantoncode + '\'' +
                ", inputuserid='" + inputuserid + '\'' +
                ", inputorgid='" + inputorgid + '\'' +
                '}';
    }
}
