package cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp;

/**
 * 响应Service：信用证信息同步
 * @author zhugenrong
 * @version 1.0
 */
public class CreditypisRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}