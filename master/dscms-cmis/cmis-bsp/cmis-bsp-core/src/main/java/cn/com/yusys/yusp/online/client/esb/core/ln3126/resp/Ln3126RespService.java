package cn.com.yusys.yusp.online.client.esb.core.ln3126.resp;

/**
 * 响应Service：贷款费用交易明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3126RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
