package cn.com.yusys.yusp.web.server.biz.xdcz0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0012.req.Xdcz0012ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0012.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0012.resp.Xdcz0012RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0012.req.Xdcz0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0012.resp.Xdcz0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:网银推送相关受托支付信息
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0012:网银推送相关受托支付信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0012Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0012
     * 交易描述：网银推送相关受托支付信息
     *
     * @param xdcz0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("网银推送相关受托支付信息")
    @PostMapping("/xdcz0012")
//   @Idempotent({"xdcz0012", "#xdcz0012ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0012RespDto xdcz0012(@Validated @RequestBody Xdcz0012ReqDto xdcz0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value, JSON.toJSONString(xdcz0012ReqDto));
        Xdcz0012DataReqDto xdcz0012DataReqDto = new Xdcz0012DataReqDto();// 请求Data： 电子保函开立
        Xdcz0012DataRespDto xdcz0012DataRespDto = new Xdcz0012DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0012.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0012RespDto xdcz0012RespDto = new Xdcz0012RespDto();
        try {
            // 从 xdcz0012ReqDto获取 reqData
            reqData = xdcz0012ReqDto.getData();
            // 将 reqData 拷贝到xdcz0012DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0012DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value, JSON.toJSONString(xdcz0012DataReqDto));
            ResultDto<Xdcz0012DataRespDto> xdcz0012DataResultDto = dscmsBizCzClientService.xdcz0012(xdcz0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value, JSON.toJSONString(xdcz0012DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0012RespDto.setErorcd(Optional.ofNullable(xdcz0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0012RespDto.setErortx(Optional.ofNullable(xdcz0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0012DataResultDto.getCode())) {
                xdcz0012DataRespDto = xdcz0012DataResultDto.getData();
                xdcz0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value, e.getMessage());
            xdcz0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0012RespDto.setDatasq(servsq);

        xdcz0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0012.key, DscmsEnum.TRADE_CODE_XDCZ0012.value, JSON.toJSONString(xdcz0012RespDto));
        return xdcz0012RespDto;
    }
}
