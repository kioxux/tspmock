package cn.com.yusys.yusp.web.client.esb.gaps.mfzjcr;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.req.MfzjcrReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.resp.MfzjcrRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.req.MfzjcrReqService;
import cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.resp.MfzjcrRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:买方资金存入
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用GAPS系统的接口处理类(mfzjcr)")
@RestController
@RequestMapping("/api/dscms2gaps")
public class DscmsMfzjcrResource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsMfzjcrResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：mfzjcr
     * 交易描述：买方资金存入
     *
     * @param mfzjcrReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("mfzjcr:买方资金存入")
    @PostMapping("/mfzjcr")
    protected @ResponseBody
    ResultDto<MfzjcrRespDto> mfzjcr(@Validated @RequestBody MfzjcrReqDto mfzjcrReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value, JSON.toJSONString(mfzjcrReqDto));
        cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.req.Service();
        cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.resp.Service();
        MfzjcrReqService mfzjcrReqService = new MfzjcrReqService();
        MfzjcrRespService mfzjcrRespService = new MfzjcrRespService();
        MfzjcrRespDto mfzjcrRespDto = new MfzjcrRespDto();
        ResultDto<MfzjcrRespDto> mfzjcrResultDto = new ResultDto<MfzjcrRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将mfzjcrReqDto转换成reqService
            BeanUtils.copyProperties(mfzjcrReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_MFZJCR.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            mfzjcrReqService.setService(reqService);
            // 将mfzjcrReqService转换成mfzjcrReqServiceMap
            Map mfzjcrReqServiceMap = beanMapUtil.beanToMap(mfzjcrReqService);
            context.put("tradeDataMap", mfzjcrReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_MFZJCR.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            mfzjcrRespService = beanMapUtil.mapToBean(tradeDataMap, MfzjcrRespService.class, MfzjcrRespService.class);
            respService = mfzjcrRespService.getService();
            //  将respService转换成MfzjcrRespDto
            BeanUtils.copyProperties(respService, mfzjcrRespDto);
            mfzjcrResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            mfzjcrResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, mfzjcrRespDto);

                mfzjcrResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                mfzjcrResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                mfzjcrResultDto.setCode(EpbEnum.EPB099999.key);
                mfzjcrResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value, e.getMessage());
            mfzjcrResultDto.setCode(EpbEnum.EPB099999.key);//9999
            mfzjcrResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        mfzjcrResultDto.setData(mfzjcrRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MFZJCR.key, EsbEnum.TRADE_CODE_MFZJCR.value, JSON.toJSONString(mfzjcrResultDto));
        return mfzjcrResultDto;
    }
}
