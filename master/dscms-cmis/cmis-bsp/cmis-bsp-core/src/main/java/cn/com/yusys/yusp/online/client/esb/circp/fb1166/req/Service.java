package cn.com.yusys.yusp.online.client.esb.circp.fb1166.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Service：面签邀约结果推送
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "credit_type")
    private String credit_type;//证件类型
    @JsonProperty(value = "credit_id")
    private String credit_id;//证件号码
    @JsonProperty(value = "phone_no")
    private String phone_no;//客户手机号码
    @JsonProperty(value = "manager_id")
    private String manager_id;//管户经理编号
    @JsonProperty(value = "manager_name")
    private String manager_name;//管户经理名称
    @JsonProperty(value = "qd_time")
    private String qd_time;//面签时间
    @JsonProperty(value = "qd_area")
    private String qd_area;//面签地址

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getCus_id() {
        return cus_id;
    }

    @JsonIgnore
    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    @JsonIgnore
    public String getCus_name() {
        return cus_name;
    }

    @JsonIgnore
    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    @JsonIgnore
    public String getCredit_type() {
        return credit_type;
    }

    @JsonIgnore
    public void setCredit_type(String credit_type) {
        this.credit_type = credit_type;
    }

    @JsonIgnore
    public String getCredit_id() {
        return credit_id;
    }

    @JsonIgnore
    public void setCredit_id(String credit_id) {
        this.credit_id = credit_id;
    }

    @JsonIgnore
    public String getPhone_no() {
        return phone_no;
    }

    @JsonIgnore
    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    @JsonIgnore
    public String getManager_id() {
        return manager_id;
    }

    @JsonIgnore
    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    @JsonIgnore
    public String getManager_name() {
        return manager_name;
    }

    @JsonIgnore
    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    @JsonIgnore
    public String getQd_time() {
        return qd_time;
    }

    @JsonIgnore
    public void setQd_time(String qd_time) {
        this.qd_time = qd_time;
    }

    @JsonIgnore
    public String getQd_area() {
        return qd_area;
    }

    @JsonIgnore
    public void setQd_area(String qd_area) {
        this.qd_area = qd_area;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", credit_type='" + credit_type + '\'' +
                ", credit_id='" + credit_id + '\'' +
                ", phone_no='" + phone_no + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", qd_time='" + qd_time + '\'' +
                ", qd_area='" + qd_area + '\'' +
                '}';
    }
}
