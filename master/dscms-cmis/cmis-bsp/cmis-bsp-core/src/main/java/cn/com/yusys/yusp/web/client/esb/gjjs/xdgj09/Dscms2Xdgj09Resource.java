package cn.com.yusys.yusp.web.client.esb.gjjs.xdgj09;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.req.Xdgj09ReqService;
import cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.resp.Xdgj09RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷查询牌价信息
 **/
@Api(tags = "BSP封装调用国际结算系统的接口处理类(xdgj09)")
@RestController
@RequestMapping("/api/dscms2gjjs")
public class Dscms2Xdgj09Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xdgj09Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdgj09
     * 交易描述：信贷查询牌价信息
     *
     * @param xdgj09ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdgj09:信贷查询牌价信息")
    @PostMapping("/xdgj09")
    protected @ResponseBody
    ResultDto<Xdgj09RespDto> xdgj09(@Validated @RequestBody Xdgj09ReqDto xdgj09ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDGJ09.key, EsbEnum.TRADE_CODE_XDGJ09.value, JSON.toJSONString(xdgj09ReqDto));

        cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.req.Service();
        cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.resp.Service();

        Xdgj09ReqService xdgj09ReqService = new Xdgj09ReqService();
        Xdgj09RespService xdgj09RespService = new Xdgj09RespService();
        Xdgj09RespDto xdgj09RespDto = new Xdgj09RespDto();
        ResultDto<Xdgj09RespDto> xdgj09ResultDto = new ResultDto<Xdgj09RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdgj09ReqDto转换成reqService
            BeanUtils.copyProperties(xdgj09ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDGJ09.key);//    交易码
            LocalDateTime now = LocalDateTime.now();
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_GJP.key, EsbEnum.SERVTP_GJP.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_GJP.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_GJP.key, EsbEnum.SERVTP_GJP.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_GJJS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_GJJS.key);//    部门号
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xdgj09ReqService.setService(reqService);
            // 将xdgj09ReqService转换成xdgj09ReqServiceMap
            Map xdgj09ReqServiceMap = beanMapUtil.beanToMap(xdgj09ReqService);
            context.put("tradeDataMap", xdgj09ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDGJ09.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdgj09RespService = beanMapUtil.mapToBean(tradeDataMap, Xdgj09RespService.class, Xdgj09RespService.class);
            respService = xdgj09RespService.getService();

            xdgj09ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdgj09ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xdgj09RespDto
                BeanUtils.copyProperties(respService, xdgj09RespDto);
                xdgj09ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdgj09ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdgj09ResultDto.setCode(EpbEnum.EPB099999.key);
                xdgj09ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDGJ09.key, EsbEnum.TRADE_CODE_XDGJ09.value, e.getMessage());
            xdgj09ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdgj09ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdgj09ResultDto.setData(xdgj09RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDGJ09.key, EsbEnum.TRADE_CODE_XDGJ09.value, JSON.toJSONString(xdgj09RespDto));
        return xdgj09ResultDto;
    }
}
