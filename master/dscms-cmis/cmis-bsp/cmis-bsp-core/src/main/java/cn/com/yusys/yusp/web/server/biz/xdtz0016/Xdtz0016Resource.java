package cn.com.yusys.yusp.web.server.biz.xdtz0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0016.req.Xdtz0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0016.resp.Xdtz0016RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0016.req.Xdtz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0016.resp.Xdtz0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:通知信贷系统更新贴现台账状态
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0016:通知信贷系统更新贴现台账状态")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0016Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0016
     * 交易描述：通知信贷系统更新贴现台账状态
     *
     * @param xdtz0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("通知信贷系统更新贴现台账状态")
    @PostMapping("/xdtz0016")
    //@Idempotent({"xdcatz0016", "#xdtz0016ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0016RespDto xdtz0016(@Validated @RequestBody Xdtz0016ReqDto xdtz0016ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016ReqDto));
        Xdtz0016DataReqDto xdtz0016DataReqDto = new Xdtz0016DataReqDto();// 请求Data： 通知信贷系统更新贴现台账状态
        Xdtz0016DataRespDto xdtz0016DataRespDto = new Xdtz0016DataRespDto();// 响应Data：通知信贷系统更新贴现台账状态
        Xdtz0016RespDto xdtz0016RespDto = new Xdtz0016RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0016.req.Data reqData = null; // 请求Data：通知信贷系统更新贴现台账状态
        cn.com.yusys.yusp.dto.server.biz.xdtz0016.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0016.resp.Data();// 响应Data：通知信贷系统更新贴现台账状态
        try {
            // 从 xdtz0016ReqDto获取 reqData
            reqData = xdtz0016ReqDto.getData();
            // 将 reqData 拷贝到xdtz0016DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0016DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataReqDto));
            ResultDto<Xdtz0016DataRespDto> xdtz0016DataResultDto = dscmsBizTzClientService.xdtz0016(xdtz0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0016RespDto.setErorcd(Optional.ofNullable(xdtz0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0016RespDto.setErortx(Optional.ofNullable(xdtz0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0016DataResultDto.getCode())) {
                xdtz0016DataRespDto = xdtz0016DataResultDto.getData();
                xdtz0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0016DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, e.getMessage());
            xdtz0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0016RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0016RespDto.setDatasq(servsq);

        xdtz0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0016.key, DscmsEnum.TRADE_CODE_XDTZ0016.value, JSON.toJSONString(xdtz0016RespDto));
        return xdtz0016RespDto;
    }
}
