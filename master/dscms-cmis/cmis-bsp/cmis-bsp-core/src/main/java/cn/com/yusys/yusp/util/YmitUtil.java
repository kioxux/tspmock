package cn.com.yusys.yusp.util;

import java.io.BufferedReader;
import java.io.FileInputStream;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import cn.com.yusys.yusp.bsp.component.exception.ComponentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cn.com.yusys.yusp.bsp.dataformat.DataFormatRegistry;
import cn.com.yusys.yusp.bsp.dataformat.exe.AbstractExecutor;
import cn.com.yusys.yusp.bsp.resources.core.ConstDef;
import cn.com.yusys.yusp.bsp.schema.dataformat.def.DataFormat;
import cn.com.yusys.yusp.bsp.toolkit.common.DataFormatTools;
import cn.com.yusys.yusp.bsp.toolkit.common.FileTools;
import cn.com.yusys.yusp.bsp.toolkit.common.StringTools;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;

/**
 * bsp挡板响应处理
 *
 * @author 24100
 */
public class YmitUtil {

    protected static Logger logger = LoggerFactory.getLogger(YmitUtil.class);

    public static final String YMIT_DISK = "ymit/";

    /**
     *
     * @param filename
     *            响应报文路径,eg:CUQ00071A19.xml
     * @param serviceName
     *            通讯接出服务名,eg:TradeClient
     * @param tradeCode
     *            交易码,eg:CUQ00071A19
     * @param respMsgType
     *            响应报文类型,eg:SOAP
     * @param msgEncode
     *            响应报文编码方式,eg:GBK
     * @param rtnClazz
     * @return
     */
    public <T> T echange(String filename, String serviceName, String tradeCode, String respMsgType, String msgEncode,
                         Class<T> rtnClazz) {
        StringBuffer reqMsg = new StringBuffer();;
        BufferedReader br = null;
        T mapToBean = null;
        msgEncode = "GBK";
        try {
//            ClassPathResource res = new ClassPathResource(YMIT_DISK + filename);
//            br = new BufferedReader(new InputStreamReader(res.getInputStream(), msgEncode));
            //String path= "D:/ymit/";
            String path= "/home/nrcmapp/ymit/";
            br = new BufferedReader(new InputStreamReader(new FileInputStream(path+filename),msgEncode));
            while (br.ready()) {
                reqMsg.append(br.readLine());
            }
            //reqMsg = reqMsg.trim();
            int size = reqMsg.toString().getBytes(msgEncode).length;
            logger.info("长度:" + size);
            //logger.info("请求:" + reqMsg);
            String outServicePath = "config/commOut/" + serviceName + "/";
            // 拆报文体
            String respMfd = outServicePath + tradeCode + ConstDef.OUT_RESP_MFD_POST;
            Map<String, Object> autoMap = unpackMsg(reqMsg.toString().getBytes(msgEncode),
                    FileTools.isExist(respMfd) ? respMfd : null, respMsgType, msgEncode);
            BeanMapUtil util = new BeanMapUtil();
            mapToBean = util.mapToBean(autoMap, rtnClazz, rtnClazz);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return mapToBean;
    }

    @SuppressWarnings("unchecked")
    private static Map<String, Object> unpackMsg(byte[] data, String msgpath, String msgtype, String encode)
            throws Exception {
        AbstractExecutor<Object> messageExecutor;
        DataFormat dataFormat = null;
        if (StringTools.isEmpty(msgpath)) {
            if (StringTools.isEmpty(encode)) {
                throw new Exception("报文格式[" + msgtype + "]的报文编码未配置");
            }
            Class<?> clazz = DataFormatRegistry._simpleDataformatRegistry.get(msgtype);
            if (clazz == null) {
                if (msgtype.equals("OTHER")) {
                    throw new Exception("复杂报文必须配置");
                }
                throw new Exception("报文格式[" + msgtype + "]未配置");
            }
            messageExecutor = (AbstractExecutor<Object>) clazz.newInstance();
            messageExecutor.setEncoding(encode);
            dataFormat = DataFormatRegistry.getDataformat(msgtype, encode);
        } else {
            dataFormat = DataFormatTools.loadDataFormat(msgpath);
            if (dataFormat == null) {
                throw new ComponentException("报文定义文件[" + msgpath + "]不存在");
            }
            messageExecutor = (AbstractExecutor<Object>) DataFormatRegistry
                    .getDataFormatExecutor(dataFormat.getImplClass());
            messageExecutor.setEncoding(dataFormat.getEncoding());
        }
        if (logger.isInfoEnabled()) {
            logger.info("{} @ 拆包，报文=[{}]", messageExecutor.getExecutorName(),
                    StringTools.isEmpty(msgpath) ? "未使用报文配置文件" : msgpath);
        }
        Map<String, Object> context = new HashMap<String, Object>();
        messageExecutor.setRootContext(context);
        messageExecutor.setCurrentContext(context);
        messageExecutor.unPack(dataFormat, data);
        int searchIndex = messageExecutor.getSearchIndex();
        context.put(ConstDef.SEARCHINDEX, searchIndex);
        // return messageExecutor.getAutoContext();
        // TODO 此处待测试
        return  messageExecutor.getCurrentContext();
    }

}
