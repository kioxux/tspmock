package cn.com.yusys.yusp.web.server.cus.xdkh0030;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0030.req.Xdkh0030ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0030.resp.Xdkh0030RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0030.req.Xdkh0030DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0030.resp.Xdkh0030DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:公司客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0030:公司客户评级相关信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0030Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0030Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0030
     * 交易描述：公司客户评级相关信息同步
     *
     * @param xdkh0030ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("公司客户评级相关信息同步")
    @PostMapping("/xdkh0030")
    //@Idempotent({"xdcakh0030", "#xdkh0030ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0030RespDto xdkh0030(@Validated @RequestBody Xdkh0030ReqDto xdkh0030ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, JSON.toJSONString(xdkh0030ReqDto));
        Xdkh0030DataReqDto xdkh0030DataReqDto = new Xdkh0030DataReqDto();// 请求Data： 公司客户评级相关信息同步
        Xdkh0030DataRespDto xdkh0030DataRespDto = new Xdkh0030DataRespDto();// 响应Data：公司客户评级相关信息同步
        Xdkh0030RespDto xdkh0030RespDto = new Xdkh0030RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0030.req.Data reqData = null; // 请求Data：公司客户评级相关信息同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0030.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0030.resp.Data();// 响应Data：公司客户评级相关信息同步
        try {
            // 从 xdkh0030ReqDto获取 reqData
            reqData = xdkh0030ReqDto.getData();
            // 将 reqData 拷贝到xdkh0030DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0030DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, JSON.toJSONString(xdkh0030DataReqDto));
            ResultDto<Xdkh0030DataRespDto> xdkh0030DataResultDto = dscmsCusClientService.xdkh0030(xdkh0030DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, JSON.toJSONString(xdkh0030DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0030RespDto.setErorcd(Optional.ofNullable(xdkh0030DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0030RespDto.setErortx(Optional.ofNullable(xdkh0030DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0030DataResultDto.getCode())) {
                xdkh0030DataRespDto = xdkh0030DataResultDto.getData();
                xdkh0030RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0030RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0030DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0030DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, e.getMessage());
            xdkh0030RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0030RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0030RespDto.setDatasq(servsq);

        xdkh0030RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0030.key, DscmsEnum.TRADE_CODE_XDKH0030.value, JSON.toJSONString(xdkh0030RespDto));
        return xdkh0030RespDto;
    }
}