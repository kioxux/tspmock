package cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.req;

/**
 * 请求Service：识别任务的模板匹配结果列表查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Cbocr2ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

