package cn.com.yusys.yusp.web.client.esb.ypxt.xdypzywcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypzywcx.req.XdypzywcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypzywcx.resp.XdypzywcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.req.XdypzywcxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.resp.XdypzywcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(xdypzywcx)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2XdypzywcxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdypzywcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdypzywcx
     * 交易描述：查询质押物信息
     *
     * @param xdypzywcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdypzywcx")
    protected @ResponseBody
    ResultDto<XdypzywcxRespDto> xdypzywcx(@Validated @RequestBody XdypzywcxReqDto xdypzywcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPZYWCX.key, EsbEnum.TRADE_CODE_XDYPZYWCX.value, JSON.toJSONString(xdypzywcxReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.resp.Service();
        XdypzywcxReqService xdypzywcxReqService = new XdypzywcxReqService();
        XdypzywcxRespService xdypzywcxRespService = new XdypzywcxRespService();
        XdypzywcxRespDto xdypzywcxRespDto = new XdypzywcxRespDto();
        ResultDto<XdypzywcxRespDto> xdypzywcxResultDto = new ResultDto<XdypzywcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdypzywcxReqDto转换成reqService
            BeanUtils.copyProperties(xdypzywcxReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDYPZYWCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xdypzywcxReqService.setService(reqService);
            // 将xdypzywcxReqService转换成xdypzywcxReqServiceMap
            Map xdypzywcxReqServiceMap = beanMapUtil.beanToMap(xdypzywcxReqService);
            context.put("tradeDataMap", xdypzywcxReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDYPZYWCX.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdypzywcxRespService = beanMapUtil.mapToBean(tradeDataMap, XdypzywcxRespService.class, XdypzywcxRespService.class);
            respService = xdypzywcxRespService.getService();

            xdypzywcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdypzywcxResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypzywcxRespDto
                BeanUtils.copyProperties(respService, xdypzywcxRespDto);
                xdypzywcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdypzywcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdypzywcxResultDto.setCode(EpbEnum.EPB099999.key);
                xdypzywcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPZYWCX.key, EsbEnum.TRADE_CODE_XDYPZYWCX.value, e.getMessage());
            xdypzywcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdypzywcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        xdypzywcxResultDto.setData(xdypzywcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPZYWCX.key, EsbEnum.TRADE_CODE_XDYPZYWCX.value, JSON.toJSONString(xdypzywcxRespDto));
        return xdypzywcxResultDto;
    }
}
