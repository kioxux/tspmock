package cn.com.yusys.yusp.online.client.esb.irs.xirs28.resp;

/**
 * 响应Service：财务信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class Xirs28RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xirs28RespService{" +
                "service=" + service +
                '}';
    }
}
