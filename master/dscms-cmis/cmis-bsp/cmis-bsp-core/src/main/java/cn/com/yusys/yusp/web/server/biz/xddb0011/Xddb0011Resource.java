package cn.com.yusys.yusp.web.server.biz.xddb0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0011.req.Xddb0011ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0011.resp.Xddb0011RespDto;
import cn.com.yusys.yusp.dto.server.xddb0011.req.Xddb0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0011.resp.Xddb0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提前出库押品信息维护
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDB0011:提前出库押品信息维护")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0011Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0011
     * 交易描述：提前出库押品信息维护
     *
     * @param xddb0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前出库押品信息维护")
    @PostMapping("/xddb0011")
    //@Idempotent({"xddb0011", "#xddb0011ReqDto.datasq"})
    protected @ResponseBody
    Xddb0011RespDto xddb0011(@Validated @RequestBody Xddb0011ReqDto xddb0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011ReqDto));
        Xddb0011DataReqDto xddb0011DataReqDto = new Xddb0011DataReqDto();// 请求Data： 提前出库押品信息维护
        Xddb0011DataRespDto xddb0011DataRespDto = new Xddb0011DataRespDto();// 响应Data：提前出库押品信息维护
        Xddb0011RespDto xddb0011RespDto = new Xddb0011RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0011.req.Data reqData = null; // 请求Data：提前出库押品信息维护
        cn.com.yusys.yusp.dto.server.biz.xddb0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0011.resp.Data();// 响应Data：提前出库押品信息维护

        try {
            // 从 xddb0011ReqDto获取 reqData
            reqData = xddb0011ReqDto.getData();
            // 将 reqData 拷贝到xddb0011DataReqDto
            BeanUtils.copyProperties(reqData, xddb0011DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011DataReqDto));
            ResultDto<Xddb0011DataRespDto> xddb0011DataResultDto = dscmsBizDbClientService.xddb0011(xddb0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0011RespDto.setErorcd(Optional.ofNullable(xddb0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0011RespDto.setErortx(Optional.ofNullable(xddb0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0011DataResultDto.getCode())) {
                xddb0011DataRespDto = xddb0011DataResultDto.getData();
                xddb0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0011DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, e.getMessage());
            xddb0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0011RespDto.setDatasq(servsq);

        xddb0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0011.key, DscmsEnum.TRADE_CODE_XDDB0011.value, JSON.toJSONString(xddb0011RespDto));
        return xddb0011RespDto;
    }
}
