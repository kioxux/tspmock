package cn.com.yusys.yusp.web.server.biz.xddb0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0013.req.CertificateInfo;
import cn.com.yusys.yusp.dto.server.biz.xddb0013.req.UpdateCollateralElectronicCertificateForm;
import cn.com.yusys.yusp.dto.server.biz.xddb0013.req.Xddb0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0013.resp.Xddb0013RespDto;
import cn.com.yusys.yusp.dto.server.xddb0013.req.Xddb0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0013.resp.Xddb0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵押登记不动产登记证明入库
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0013:抵押登记不动产登记证明入库")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0013Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0013
     * 交易描述：抵押登记不动产登记证明入库
     *
     * @param xddb0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记不动产登记证明入库")
    @PostMapping("/xddb0013")
    //@Idempotent({"xddb0013", "#xddb0013ReqDto.datasq"})
    protected @ResponseBody
    Xddb0013RespDto xddb0013(@Validated @RequestBody Xddb0013ReqDto xddb0013ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013ReqDto));
        Xddb0013DataReqDto xddb0013DataReqDto = new Xddb0013DataReqDto();// 请求Data： 抵押登记不动产登记证明入库
        Xddb0013DataRespDto xddb0013DataRespDto = new Xddb0013DataRespDto();// 响应Data：抵押登记不动产登记证明入库
        Xddb0013RespDto xddb0013RespDto = new Xddb0013RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0013.req.Data reqData = null; // 请求Data：抵押登记不动产登记证明入库
        cn.com.yusys.yusp.dto.server.biz.xddb0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0013.resp.Data();// 响应Data：抵押登记不动产登记证明入库

        try {
            // 从 xddb0013ReqDto获取 reqData
            reqData = xddb0013ReqDto.getData();
            // 将 reqData 拷贝到xddb0013DataReqDto
            BeanUtils.copyProperties(reqData, xddb0013DataReqDto);
            UpdateCollateralElectronicCertificateForm updateCollateralElectronicCertificateForm = reqData.getUpdateCollateralElectronicCertificateForm();
            if (Objects.nonNull(updateCollateralElectronicCertificateForm)) {
                cn.com.yusys.yusp.dto.server.hyy.common.UpdateCollateralElectronicCertificateForm targetForm = new cn.com.yusys.yusp.dto.server.hyy.common.UpdateCollateralElectronicCertificateForm();
                BeanUtils.copyProperties(updateCollateralElectronicCertificateForm, targetForm);
                CertificateInfo certificate_info = updateCollateralElectronicCertificateForm.getCertificate_info();
                if (Objects.nonNull(certificate_info)) {
                    cn.com.yusys.yusp.dto.server.hyy.common.CertificateInfo targetInfo = new cn.com.yusys.yusp.dto.server.hyy.common.CertificateInfo();
                    BeanUtils.copyProperties(certificate_info, targetInfo);
                    targetForm.setCertificate_info(targetInfo);
                }
                xddb0013DataReqDto.setUpdateCollateralElectronicCertificateForm(targetForm);

            }
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataReqDto));
            ResultDto<Xddb0013DataRespDto> xddb0013DataResultDto = dscmsBizDbClientService.xddb0013(xddb0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0013RespDto.setErorcd(Optional.ofNullable(xddb0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0013RespDto.setErortx(Optional.ofNullable(xddb0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0013DataResultDto.getCode())) {
                xddb0013DataRespDto = xddb0013DataResultDto.getData();
                xddb0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0013DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, e.getMessage());
            xddb0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0013RespDto.setDatasq(servsq);

        xddb0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013RespDto));
        return xddb0013RespDto;
    }
}
