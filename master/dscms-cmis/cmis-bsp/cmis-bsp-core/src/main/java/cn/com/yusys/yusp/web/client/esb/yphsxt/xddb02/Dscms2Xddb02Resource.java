package cn.com.yusys.yusp.web.client.esb.yphsxt.xddb02;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb02.req.Xddb02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb02.resp.Xddb02RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.req.Xddb02ReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.resp.Xddb02RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询抵押物信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2Xddb02Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xddb02Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xddb02
     * 交易描述：查询抵押物信息
     *
     * @param xddb02ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xddb02")
    protected @ResponseBody
    ResultDto<Xddb02RespDto> xddb02(@Validated @RequestBody Xddb02ReqDto xddb02ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB02.key, EsbEnum.TRADE_CODE_XDDB02.value, JSON.toJSONString(xddb02ReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.resp.Service();
        Xddb02ReqService xddb02ReqService = new Xddb02ReqService();
        Xddb02RespService xddb02RespService = new Xddb02RespService();
        Xddb02RespDto xddb02RespDto = new Xddb02RespDto();
        ResultDto<Xddb02RespDto> xddb02ResultDto = new ResultDto<Xddb02RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xddb02ReqDto转换成reqService
            BeanUtils.copyProperties(xddb02ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDDB02.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xddb02ReqService.setService(reqService);
            // 将xddb02ReqService转换成xddb02ReqServiceMap
            Map xddb02ReqServiceMap = beanMapUtil.beanToMap(xddb02ReqService);
            context.put("tradeDataMap", xddb02ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB02.key, EsbEnum.TRADE_CODE_XDDB02.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDDB02.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB02.key, EsbEnum.TRADE_CODE_XDDB02.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xddb02RespService = beanMapUtil.mapToBean(tradeDataMap, Xddb02RespService.class, Xddb02RespService.class);
            respService = xddb02RespService.getService();
            //  将respService转换成Xddb02RespDto
            xddb02ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xddb02ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xddb02RespDto);
                xddb02ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb02ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xddb02ResultDto.setCode(EpbEnum.EPB099999.key);
                xddb02ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB02.key, EsbEnum.TRADE_CODE_XDDB02.value, e.getMessage());
            xddb02ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xddb02ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xddb02ResultDto.setData(xddb02RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB02.key, EsbEnum.TRADE_CODE_XDDB02.value, JSON.toJSONString(xddb02ResultDto));
        return xddb02ResultDto;
    }
}
