package cn.com.yusys.yusp.web.client.esb.core.ln3047;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3047.Ln3047ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3047.Ln3047RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3047.req.Ln3047ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3047.resp.Ln3047RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3047)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3047Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3047Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3047
     * 交易描述：质押还贷
     *
     * @param ln3047ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3047:质押还贷")
    @PostMapping("/ln3047")
    protected @ResponseBody
    ResultDto<Ln3047RespDto> ln3047(@Validated @RequestBody Ln3047ReqDto ln3047ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3047.key, EsbEnum.TRADE_CODE_LN3047.value, JSON.toJSONString(ln3047ReqDto, SerializerFeature.WriteMapNullValue));
        cn.com.yusys.yusp.online.client.esb.core.ln3047.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3047.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3047.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3047.resp.Service();

        Ln3047ReqService ln3047ReqService = new Ln3047ReqService();
        Ln3047RespService ln3047RespService = new Ln3047RespService();
        Ln3047RespDto ln3047RespDto = new Ln3047RespDto();
        ResultDto<Ln3047RespDto> ln3047ResultDto = new ResultDto<Ln3047RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3047ReqDto转换成reqService
            BeanUtils.copyProperties(ln3047ReqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(ln3047ReqDto.getLstZhzy())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3047.LstZhzy> ln3047ReqLstZhzyDtoList = Optional.ofNullable(ln3047ReqDto.getLstZhzy())
                        .orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.core.ln3047.req.lstZhzy.List ln3047ReqLstZhzyList
                        = Optional.ofNullable(reqService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3047.req.lstZhzy.List());
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3047.req.lstZhzy.Record> ln3047ReqLstZhzyRecords = ln3047ReqLstZhzyDtoList.parallelStream()
                        .map(lstZhzy -> {
                            cn.com.yusys.yusp.online.client.esb.core.ln3047.req.lstZhzy.Record ln3047ReqLstZhzyRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3047.req.lstZhzy.Record();
                            BeanUtils.copyProperties(lstZhzy, ln3047ReqLstZhzyRecord);
                            return ln3047ReqLstZhzyRecord;
                        }).collect(Collectors.toList());
                ln3047ReqLstZhzyList.setRecord(ln3047ReqLstZhzyRecords);
                reqService.setList(ln3047ReqLstZhzyList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3047.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(ln3047ReqDto.getBrchno());//    部门号,取账务机构号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3047ReqService.setService(reqService);
            // 将ln3047ReqService转换成ln3047ReqServiceMap
            Map ln3047ReqServiceMap = beanMapUtil.beanToMap(ln3047ReqService);
            context.put("tradeDataMap", ln3047ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3047.key, EsbEnum.TRADE_CODE_LN3047.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3047.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3047.key, EsbEnum.TRADE_CODE_LN3047.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3047RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3047RespService.class, Ln3047RespService.class);
            respService = ln3047RespService.getService();

            ln3047ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3047ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3047RespDto
                BeanUtils.copyProperties(respService, ln3047RespDto);
                ln3047ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3047ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3047ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3047ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3047.key, EsbEnum.TRADE_CODE_LN3047.value, e.getMessage());
            ln3047ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3047ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3047ResultDto.setData(ln3047RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3047.key, EsbEnum.TRADE_CODE_LN3047.value, JSON.toJSONString(ln3047ResultDto, SerializerFeature.WriteMapNullValue));
        return ln3047ResultDto;
    }
}
