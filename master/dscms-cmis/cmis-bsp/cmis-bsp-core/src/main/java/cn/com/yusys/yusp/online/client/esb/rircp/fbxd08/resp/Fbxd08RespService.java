package cn.com.yusys.yusp.online.client.esb.rircp.fbxd08.resp;

/**
 * 响应Service：查找（利翃）实还正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd08RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd08RespService{" +
                "service=" + service +
                '}';
    }
}
