package cn.com.yusys.yusp.web.server.biz.xdht0032;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0032.req.Xdht0032ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0032.resp.Xdht0032RespDto;
import cn.com.yusys.yusp.dto.server.xdht0032.req.Xdht0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0032.resp.Xdht0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据核心客户号查询我行信用类合同金额汇总
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDHT0032:根据核心客户号查询我行信用类合同金额汇总")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0032Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0032
     * 交易描述：根据核心客户号查询我行信用类合同金额汇总
     *
     * @param xdht0032ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询我行信用类合同金额汇总")
    @PostMapping("/xdht0032")
    //@Idempotent({"xdcaht0032", "#xdht0032ReqDto.datasq"})
    protected @ResponseBody
    Xdht0032RespDto xdht0032(@Validated @RequestBody Xdht0032ReqDto xdht0032ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032ReqDto));
        Xdht0032DataReqDto xdht0032DataReqDto = new Xdht0032DataReqDto();// 请求Data： 根据核心客户号查询我行信用类合同金额汇总
        Xdht0032DataRespDto xdht0032DataRespDto = new Xdht0032DataRespDto();// 响应Data：根据核心客户号查询我行信用类合同金额汇总
        Xdht0032RespDto xdht0032RespDto = new Xdht0032RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0032.req.Data reqData = null; // 请求Data：根据核心客户号查询我行信用类合同金额汇总
        cn.com.yusys.yusp.dto.server.biz.xdht0032.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0032.resp.Data();// 响应Data：根据核心客户号查询我行信用类合同金额汇总
        try {
            // 从 xdht0032ReqDto获取 reqData
            reqData = xdht0032ReqDto.getData();
            // 将 reqData 拷贝到xdht0032DataReqDto
            BeanUtils.copyProperties(reqData, xdht0032DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032DataReqDto));
            ResultDto<Xdht0032DataRespDto> xdht0032DataResultDto = dscmsBizHtClientService.xdht0032(xdht0032DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0032RespDto.setErorcd(Optional.ofNullable(xdht0032DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0032RespDto.setErortx(Optional.ofNullable(xdht0032DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0032DataResultDto.getCode())) {
                xdht0032DataRespDto = xdht0032DataResultDto.getData();
                xdht0032RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0032RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0032DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0032DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, e.getMessage());
            xdht0032RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0032RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0032RespDto.setDatasq(servsq);

        xdht0032RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0032.key, DscmsEnum.TRADE_CODE_XDHT0032.value, JSON.toJSONString(xdht0032RespDto));
        return xdht0032RespDto;
    }
}