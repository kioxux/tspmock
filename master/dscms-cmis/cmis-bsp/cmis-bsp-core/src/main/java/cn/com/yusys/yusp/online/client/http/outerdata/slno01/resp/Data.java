package cn.com.yusys.yusp.online.client.http.outerdata.slno01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/11 14:45
 * @since 2021/6/11 14:45
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarno")
    private String guarno;//抵押物编号
    @JsonProperty(value = "yxzjls")
    private String yxzjls;//影像主键流水号
    @JsonProperty(value = "insert_time")
    private String insert_time;
    @JsonProperty(value = "slyxserno")
    private String slyxserno;

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    public String getYxzjls() {
        return yxzjls;
    }

    public void setYxzjls(String yxzjls) {
        this.yxzjls = yxzjls;
    }

    public String getInsert_time() {
        return insert_time;
    }

    public String getSlyxserno() {
        return slyxserno;
    }

    public void setSlyxserno(String slyxserno) {
        this.slyxserno = slyxserno;
    }

    public void setInsert_time(String insert_time) {
        this.insert_time = insert_time;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guarno='" + guarno + '\'' +
                ", yxzjls='" + yxzjls + '\'' +
                ", insert_time='" + insert_time + '\'' +
                ", slyxserno='" + slyxserno + '\'' +
                '}';
    }
}
