package cn.com.yusys.yusp.online.client.esb.core.ln3249.req;

/**
 * 请求Service：贷款指定日期利息试算
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3249ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3249ReqService{" +
                "service=" + service +
                '}';
    }
}
