package cn.com.yusys.yusp.online.client.esb.doc.doc000.resp;

/**
 * 响应Service：入库接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Doc000RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Doc000RespService{" +
                "service=" + service +
                '}';
    }
}
