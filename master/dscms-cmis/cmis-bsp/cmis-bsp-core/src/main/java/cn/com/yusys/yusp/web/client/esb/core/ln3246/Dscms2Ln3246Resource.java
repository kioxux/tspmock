package cn.com.yusys.yusp.web.client.esb.core.ln3246;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.LstLnDkhkmx;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3246.req.Ln3246ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3246.resp.Ln3246RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3246)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3246Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3246Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();


    /**
     * 根贷款还款计划明细查询（处理码ln3246）
     *
     * @param reqDto Ln3246ReqDto
     * @return ResultDto<Ln3246RespDto>
     * @throws Exception
     */
    @ApiOperation("ln3246:根贷款还款计划明细查询")
    @PostMapping("/ln3246")
    protected @ResponseBody
    ResultDto<Ln3246RespDto> ln3246(@Validated @RequestBody Ln3246ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3246.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3246.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3246.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3246.resp.Service();
        Ln3246ReqService ln3246ReqService = new Ln3246ReqService();
        Ln3246RespService ln3246RespService = new Ln3246RespService();
        Ln3246RespDto ln3246RespDto = new Ln3246RespDto();
        ResultDto<Ln3246RespDto> ln3246ResultDto = new ResultDto<Ln3246RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Ln3246ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3246.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3246ReqService.setService(reqService);
            // 将ln3246ReqService转换成ln3246ReqServiceMap
            Map ln3246ReqServiceMap = beanMapUtil.beanToMap(ln3246ReqService);
            context.put("tradeDataMap", ln3246ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3246.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3246RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3246RespService.class, Ln3246RespService.class);
            respService = ln3246RespService.getService();

            //  将Ln3246RespDto封装到ResultDto<Ln3246RespDto>
            ln3246ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3246ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3246RespDto
                BeanUtils.copyProperties(respService, ln3246RespDto);
                List<LstLnDkhkmx> lstLnDkhkmxs = new ArrayList<LstLnDkhkmx>();
                if (respService.getLstLnDkhkmx_ARRAY() != null && CollectionUtils.nonEmpty(respService.getLstLnDkhkmx_ARRAY().getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3246.resp.Record> recordList = respService.getLstLnDkhkmx_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3246.resp.Record record : recordList) {
                        LstLnDkhkmx lstLnDkhkmx = new LstLnDkhkmx();
                        BeanUtils.copyProperties(record, lstLnDkhkmx);
                        lstLnDkhkmxs.add(lstLnDkhkmx);
                    }
                }
                ln3246RespDto.setLstLnDkhkmx(lstLnDkhkmxs);
                ln3246ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3246ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3246ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3246ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, e.getMessage());
            ln3246ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3246ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3246ResultDto.setData(ln3246RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3246.key, EsbEnum.TRADE_CODE_LN3246.value, JSON.toJSONString(ln3246ResultDto));

        return ln3246ResultDto;
    }
}
