package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsfsj.Record;

import java.util.List;

/**
 * 响应Service：贷款产品收费事件定义属性对象
 * @author lihh
 * @version 1.0
 */
public class Lstcpsfsj_ARRAY {
    private List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsfsj.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstcpsfsj_ARRAY{" +
                "record=" + record +
                '}';
    }
}
