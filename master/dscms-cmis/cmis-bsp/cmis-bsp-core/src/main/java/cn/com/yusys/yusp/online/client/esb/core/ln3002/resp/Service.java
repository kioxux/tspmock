package cn.com.yusys.yusp.online.client.esb.core.ln3002.resp;

/**
 * 响应Service：贷款产品删除
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String jychgbzh;//交易是否成功

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getJychgbzh() {
        return jychgbzh;
    }

    public void setJychgbzh(String jychgbzh) {
        this.jychgbzh = jychgbzh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", jychgbzh='" + jychgbzh + '\'' +
                '}';
    }
}
