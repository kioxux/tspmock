package cn.com.yusys.yusp.web.server.cus.xdkh0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0019.req.Xdkh0019ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0019.resp.Xdkh0019RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.req.Xdkh0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0019.resp.Xdkh0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户查询并开户
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0019:客户查询并开户")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0019Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0019
     * 交易描述：客户查询并开户
     *
     * @param xdkh0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0019:客户查询并开户")
    @PostMapping("/xdkh0019")
    //@Idempotent({"xdcakh0019", "#xdkh0019ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0019RespDto xdkh0019(@Validated @RequestBody Xdkh0019ReqDto xdkh0019ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, JSON.toJSONString(xdkh0019ReqDto));
        Xdkh0019DataReqDto xdkh0019DataReqDto = new Xdkh0019DataReqDto();// 请求Data： 客户查询并开户
        Xdkh0019DataRespDto xdkh0019DataRespDto = new Xdkh0019DataRespDto();// 响应Data：客户查询并开户
        Xdkh0019RespDto xdkh0019RespDto = new Xdkh0019RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0019.req.Data reqData = null; // 请求Data：客户查询并开户
        cn.com.yusys.yusp.dto.server.cus.xdkh0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0019.resp.Data();// 响应Data：客户查询并开户
        try {
            // 从 xdkh0019ReqDto获取 reqData
            reqData = xdkh0019ReqDto.getData();
            // 将 reqData 拷贝到xdkh0019DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0019DataReqDto);
            // 调用服务
            ResultDto<Xdkh0019DataRespDto> xdkh0019DataResultDto = dscmsCusClientService.xdkh0019(xdkh0019DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdkh0019RespDto.setErorcd(Optional.ofNullable(xdkh0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0019RespDto.setErortx(Optional.ofNullable(xdkh0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0019DataResultDto.getCode())) {
                xdkh0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0019DataRespDto = xdkh0019DataResultDto.getData();
                // 将 xdkh0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, e.getMessage());
            xdkh0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0019RespDto.setDatasq(servsq);

        xdkh0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0019.key, DscmsEnum.TRADE_CODE_XDKH0019.value, JSON.toJSONString(xdkh0019RespDto));
        return xdkh0019RespDto;
    }
}
