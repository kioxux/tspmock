package cn.com.yusys.yusp.web.server.biz.xdht0031;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0031.req.Xdht0031ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0031.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdht0031.resp.Xdht0031RespDto;
import cn.com.yusys.yusp.dto.server.xdht0031.req.Xdht0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0031.resp.Xdht0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据核心客户号查询经营性贷款合同额度
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0031:根据核心客户号查询经营性贷款合同额度")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0031Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0031Resource.class);

    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0031
     * 交易描述：根据核心客户号查询经营性贷款合同额度
     *
     * @param xdht0031ReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("根据核心客户号查询经营性贷款合同额度")
    @PostMapping("/xdht0031")
    //@Idempotent({"xdcaht0031", "#xdht0031ReqDto.datasq"})
    protected @ResponseBody
    Xdht0031RespDto xdht0031(@Validated @RequestBody Xdht0031ReqDto xdht0031ReqDto ) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031ReqDto));
        Xdht0031DataReqDto xdht0031DataReqDto = new Xdht0031DataReqDto();// 请求Data： 根据核心客户号查询经营性贷款合同额度
        Xdht0031DataRespDto xdht0031DataRespDto =  new Xdht0031DataRespDto();// 响应Data：根据核心客户号查询经营性贷款合同额度
        Xdht0031RespDto xdht0031RespDto = new Xdht0031RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0031.req.Data reqData = null; // 请求Data：根据核心客户号查询经营性贷款合同额度
        cn.com.yusys.yusp.dto.server.biz.xdht0031.resp.Data respData = new Data();// 响应Data：根据核心客户号查询经营性贷款合同额度

        try {
            // 从 xdht0031ReqDto获取 reqData
            reqData = xdht0031ReqDto.getData();
            // 将 reqData 拷贝到xdht0031DataReqDto
            BeanUtils.copyProperties(reqData, xdht0031DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031DataReqDto));
            ResultDto<Xdht0031DataRespDto> xdht0031DataResultDto= dscmsBizHtClientService.xdht0031(xdht0031DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0031RespDto.setErorcd(Optional.ofNullable(xdht0031DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0031RespDto.setErortx(Optional.ofNullable(xdht0031DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,xdht0031DataResultDto.getCode())) {
                xdht0031DataRespDto = xdht0031DataResultDto.getData();
                xdht0031RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0031RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0031DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0031DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, e.getMessage());
            xdht0031RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0031RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0031RespDto.setDatasq(servsq);

        xdht0031RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0031.key, DscmsEnum.TRADE_CODE_XDHT0031.value, JSON.toJSONString(xdht0031RespDto));
        return xdht0031RespDto;
    }
}
