package cn.com.yusys.yusp.online.client.esb.wx.wxp002.resp;

/**
 * 响应Service：信贷将授信额度推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
public class Wxp002RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
