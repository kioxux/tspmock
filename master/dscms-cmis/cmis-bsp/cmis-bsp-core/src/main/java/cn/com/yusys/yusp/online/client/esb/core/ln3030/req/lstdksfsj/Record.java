package cn.com.yusys.yusp.online.client.esb.core.ln3030.req.lstdksfsj;

import java.math.BigDecimal;

/**
 * 请求Service：贷款收费事件
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String shoufzhl;//收费种类
    private BigDecimal shoufjee;//收费金额/比例
    private String fufeizhh;//付费账号
    private String shoufshj;//收费事件
    private String shoufdma;//收费代码
    private String shfshjmc;//收费事件名称
    private String shfdmamc;//收费代码名称
    private String ffzhhzxh;//付费账号子序号
    private String sfrzhzhh;//收费入账账号
    private String sfrzhzxh;//收费入账账号子序号

    public String getShoufzhl() {
        return shoufzhl;
    }

    public void setShoufzhl(String shoufzhl) {
        this.shoufzhl = shoufzhl;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getShoufshj() {
        return shoufshj;
    }

    public void setShoufshj(String shoufshj) {
        this.shoufshj = shoufshj;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfshjmc() {
        return shfshjmc;
    }

    public void setShfshjmc(String shfshjmc) {
        this.shfshjmc = shfshjmc;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getSfrzhzhh() {
        return sfrzhzhh;
    }

    public void setSfrzhzhh(String sfrzhzhh) {
        this.sfrzhzhh = sfrzhzhh;
    }

    public String getSfrzhzxh() {
        return sfrzhzxh;
    }

    public void setSfrzhzxh(String sfrzhzxh) {
        this.sfrzhzxh = sfrzhzxh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "shoufzhl='" + shoufzhl + '\'' +
                "shoufjee='" + shoufjee + '\'' +
                "fufeizhh='" + fufeizhh + '\'' +
                "shoufshj='" + shoufshj + '\'' +
                "shoufdma='" + shoufdma + '\'' +
                "shfshjmc='" + shfshjmc + '\'' +
                "shfdmamc='" + shfdmamc + '\'' +
                "ffzhhzxh='" + ffzhhzxh + '\'' +
                "sfrzhzhh='" + sfrzhzhh + '\'' +
                "sfrzhzxh='" + sfrzhzxh + '\'' +
                '}';
    }
}
