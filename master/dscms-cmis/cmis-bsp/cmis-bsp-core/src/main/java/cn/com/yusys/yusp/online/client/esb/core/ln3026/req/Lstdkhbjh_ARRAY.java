package cn.com.yusys.yusp.online.client.esb.core.ln3026.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkhbjh.Record;

import java.util.List;

/**
 * 贷款还本计划
 * @author lihh
 * @version 1.0
 */
public class Lstdkhbjh_ARRAY {
    private List<Record> record;

    @Override
    public String toString() {
        return "Lstdkhbjh{" +
                "record=" + record +
                '}';
    }

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }
}
