package cn.com.yusys.yusp.web.client.esb.irs.xirs10;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.xirs10.req.Xirs10ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.xirs10.resp.Xirs10RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(xirs10)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Xirs10Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xirs10Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 同步公司客户信息（处理码IRS10）
     *
     * @param xirs10ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xirs10:同步公司客户信息")
    @PostMapping("/xirs10")
    protected @ResponseBody
    ResultDto<Xirs10RespDto> xirs10(@Validated @RequestBody Xirs10ReqDto xirs10ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS10.key, EsbEnum.TRADE_CODE_IRS10.value, JSON.toJSONString(xirs10ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.xirs10.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.xirs10.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.xirs10.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.xirs10.resp.Service();
        Xirs10ReqService xirs10ReqService = new Xirs10ReqService();
        Xirs10RespService xirs10RespService = new Xirs10RespService();
        Xirs10RespDto xirs10RespDto = new Xirs10RespDto();
        ResultDto<Xirs10RespDto> xirs10ResultDto = new ResultDto<Xirs10RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Xirs10ReqDto转换成reqService
            BeanUtils.copyProperties(xirs10ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS10.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xirs10ReqService.setService(reqService);
            // 将xirs10ReqService转换成xirs10ReqServiceMap
            Map xirs10ReqServiceMap = beanMapUtil.beanToMap(xirs10ReqService);
            context.put("tradeDataMap", xirs10ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS10.key, EsbEnum.TRADE_CODE_IRS10.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS10.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS10.key, EsbEnum.TRADE_CODE_IRS10.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xirs10RespService = beanMapUtil.mapToBean(tradeDataMap, Xirs10RespService.class, Xirs10RespService.class);
            respService = xirs10RespService.getService();

            //  将Xirs10RespDto封装到ResultDto<Xirs10RespDto>
            xirs10ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xirs10ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xirs10RespDto
                BeanUtils.copyProperties(respService, xirs10RespDto);
                xirs10ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xirs10ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xirs10ResultDto.setCode(EpbEnum.EPB099999.key);
                xirs10ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS10.key, EsbEnum.TRADE_CODE_IRS10.value, e.getMessage());
            xirs10ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xirs10ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xirs10ResultDto.setData(xirs10RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS10.key, EsbEnum.TRADE_CODE_IRS10.value, JSON.toJSONString(xirs10ResultDto));
        return xirs10ResultDto;

    }
}
