package cn.com.yusys.yusp.online.client.esb.core.ln3138.resp;

import java.math.BigDecimal;

/**
 * 响应Service：等额等本贷款推算
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private BigDecimal huankjee;//还款金额
    private BigDecimal benjinnn;//本金
    private BigDecimal guihlixi;//归还利息
    private BigDecimal yueeeeee;//余额

    @Override
    public String toString() {
        return "Record{" +
                "huankjee=" + huankjee +
                ", benjinnn=" + benjinnn +
                ", guihlixi=" + guihlixi +
                ", yueeeeee=" + yueeeeee +
                '}';
    }
}
