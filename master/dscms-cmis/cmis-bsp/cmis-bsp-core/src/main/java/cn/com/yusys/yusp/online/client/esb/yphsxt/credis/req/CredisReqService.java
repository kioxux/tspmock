package cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req;

/**
 * 请求Service：信用证信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class CredisReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "CredisReqService{" +
                "service=" + service +
                '}';
    }
}
