package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req;

/**
 * 请求Service：信贷业务零售评级
 */
public class Record {
    private String busi_seq;//业务流水号
    private String cred_sub_no;//授信分项编号
    private String loanData;//业务信息
    private String mortData;//COLL_INFO

    public String getBusi_seq() {
        return busi_seq;
    }

    public void setBusi_seq(String busi_seq) {
        this.busi_seq = busi_seq;
    }

    public String getCred_sub_no() {
        return cred_sub_no;
    }

    public void setCred_sub_no(String cred_sub_no) {
        this.cred_sub_no = cred_sub_no;
    }

    public String getLoanData() {
        return loanData;
    }

    public void setLoanData(String loanData) {
        this.loanData = loanData;
    }


    public String getMortData() {
        return mortData;
    }

    public void setMortData(String mortData) {
        this.mortData = mortData;
    }
}
