package cn.com.yusys.yusp.online.client.esb.gaps.cljctz.resp;

/**
 * 响应Service：连云港存量房列表
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String gapssq;//gaps流水号
    private String hstrsq;//核心流水号（记账流水号）

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGapssq() {
        return gapssq;
    }

    public void setGapssq(String gapssq) {
        this.gapssq = gapssq;
    }

    public String getHstrsq() {
        return hstrsq;
    }

    public void setHstrsq(String hstrsq) {
        this.hstrsq = hstrsq;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", gapssq='" + gapssq + '\'' +
                ", hstrsq='" + hstrsq + '\'' +
                '}';
    }
}
