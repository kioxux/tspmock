package cn.com.yusys.yusp.web.client.esb.core.ln3026;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3026.Ln3026ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3026.Ln3026RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3026.req.*;
import cn.com.yusys.yusp.dto.client.esb.core.ln3026.req.Lstdkstzf;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3026.req.*;
import cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkhbjh.Record;
import cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Ln3026RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:银团贷款开户
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3026)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3026Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Ln3026Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3026
     * 交易描述：银团贷款开户
     *
     * @param ln3026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3026:银团贷款开户")
    @PostMapping("/ln3026")
    protected @ResponseBody
    ResultDto<Ln3026RespDto> ln3026(@Validated @RequestBody Ln3026ReqDto ln3026ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3026.key, EsbEnum.TRADE_CODE_LN3026.value, JSON.toJSONString(ln3026ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3026.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3026.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Service();
        Ln3026ReqService ln3026ReqService = new Ln3026ReqService();
        Ln3026RespService ln3026RespService = new Ln3026RespService();
        Ln3026RespDto ln3026RespDto = new Ln3026RespDto();
        ResultDto<Ln3026RespDto> ln3026ResultDto = new ResultDto<Ln3026RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3026ReqDto转换成reqService
            BeanUtils.copyProperties(ln3026ReqDto, reqService);
            if (CollectionUtils.nonEmpty(ln3026ReqDto.getLstdkhbjh())) {
                List<Lstdkhbjh> lstdkhbjh = Optional.ofNullable(ln3026ReqDto.getLstdkhbjh()).orElse(new ArrayList<>());
                List<Record> dkhbjhRecord = lstdkhbjh.parallelStream().map(dkhbjh -> {
                    Record record = new Record();
                    BeanUtils.copyProperties(dkhbjh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhbjh_ARRAY dkhbjhTemp = new Lstdkhbjh_ARRAY();
                dkhbjhTemp.setRecord(dkhbjhRecord);
                reqService.setLstdkhbjh_ARRAY(dkhbjhTemp);
            }

            if (CollectionUtils.nonEmpty(ln3026ReqDto.getLstdkhkzh())) {
                List<Lstdkhkzh> lstdkhkzh = Optional.ofNullable(ln3026ReqDto.getLstdkhkzh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkhkzh.Record> dkhkzhRecord = lstdkhkzh.parallelStream().map(dkhkzh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkhkzh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkhkzh.Record();
                    BeanUtils.copyProperties(dkhkzh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkhkzh_ARRAY dkhkzhTemp = new Lstdkhkzh_ARRAY();
                dkhkzhTemp.setRecord(dkhkzhRecord);
                reqService.setLstdkhkzh_ARRAY(dkhkzhTemp);
            }

            if (CollectionUtils.nonEmpty(ln3026ReqDto.getLstdkstzf())) {
                List<Lstdkstzf> lstdkstzf = Optional.ofNullable(ln3026ReqDto.getLstdkstzf()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkstzf.Record> dkstzfRecord = lstdkstzf.parallelStream().map(dkstzf -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkstzf.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkstzf.Record();
                    BeanUtils.copyProperties(dkstzf, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdkstzf_ARRAY dkstzfTemp = new Lstdkstzf_ARRAY();
                dkstzfTemp.setRecord(dkstzfRecord);
                reqService.setLstdkstzf_ARRAY(dkstzfTemp);
            }


            if (CollectionUtils.nonEmpty(ln3026ReqDto.getLstdzqgjh())) {
                List<Lstdzqgjh> lstdzqgjh = Optional.ofNullable(ln3026ReqDto.getLstdzqgjh()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdzqgjh.Record> dzqgjhRecord = lstdzqgjh.parallelStream().map(dzqgjh -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdzqgjh.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdzqgjh.Record();
                    BeanUtils.copyProperties(dzqgjh, record);
                    return record;
                }).collect(Collectors.toList());
                Lstdzqgjh_ARRAY dzqgjhTemp = new Lstdzqgjh_ARRAY();
                dzqgjhTemp.setRecord(dzqgjhRecord);
                reqService.setLstdzqgjh_ARRAY(dzqgjhTemp);
            }

            if (CollectionUtils.nonEmpty(ln3026ReqDto.getLstytczfe())) {
                List<Lstytczfe> lstytczfe = Optional.ofNullable(ln3026ReqDto.getLstytczfe()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstytczfe.Record> ytczfeRecord = lstytczfe.parallelStream().map(ytczfe -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstytczfe.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstytczfe.Record();
                    BeanUtils.copyProperties(ytczfe, record);
                    return record;
                }).collect(Collectors.toList());
                Lstytczfe_ARRAY ytczfeTemp = new Lstytczfe_ARRAY();
                ytczfeTemp.setRecord(ytczfeRecord);
                reqService.setLstytczfe_ARRAY(ytczfeTemp);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3026.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3026ReqService.setService(reqService);
            // 将ln3026ReqService转换成ln3026ReqServiceMap
            Map ln3026ReqServiceMap = beanMapUtil.beanToMap(ln3026ReqService);
            context.put("tradeDataMap", ln3026ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3026.key, EsbEnum.TRADE_CODE_LN3026.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3026.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3026.key, EsbEnum.TRADE_CODE_LN3026.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3026RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3026RespService.class, Ln3026RespService.class);
            respService = ln3026RespService.getService();
            //  将respService转换成Ln3026RespDto
            BeanUtils.copyProperties(respService, ln3026RespDto);
            ln3026ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            ln3026ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, ln3026RespDto);
                cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdkhbjh_ARRAY lstdkhbjhRecord = Optional.ofNullable(respService.getLstdkhbjh_ARRAY()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdkhbjh_ARRAY());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.lstdkhbjh.Record> dkhbjhRecords = Optional.ofNullable(lstdkhbjhRecord.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(dkhbjhRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkhbjh> lstdkhbjhList = dkhbjhRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkhbjh temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkhbjh();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3026RespDto.setLstdkhbjh(lstdkhbjhList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdkhkzh_ARRAY lstdkhkzhRecord = Optional.ofNullable(respService.getLstdkhkzh_ARRAY()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdkhkzh_ARRAY());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.lstdkhkzh.Record> dkhkzhRecords = Optional.ofNullable(lstdkhkzhRecord.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(dkhkzhRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkhkzh> lstdkhkzhList = dkhkzhRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkhkzh temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkhkzh();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3026RespDto.setLstdkhkzh(lstdkhkzhList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdkstzf_ARRAY lstdkstzfRecord = Optional.ofNullable(respService.getLstdkstzf_ARRAY()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdkstzf_ARRAY());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.lstdkstzf.Record> dkstzfRecords = Optional.ofNullable(lstdkstzfRecord.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(dkstzfRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkstzf> lstdkstzfList = dkstzfRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkstzf temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdkstzf();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3026RespDto.setLstdkstzf(lstdkstzfList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdzqgjh_ARRAY lstdzqgjhRecord = Optional.ofNullable(respService.getLstdzqgjh_ARRAY()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstdzqgjh_ARRAY());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.lstdzqgjh.Record> dzqgjhRecords = Optional.ofNullable(lstdzqgjhRecord.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(dzqgjhRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdzqgjh> lstdzqgjhList = dzqgjhRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdzqgjh temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstdzqgjh();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3026RespDto.setLstdzqgjh(lstdzqgjhList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstytczfe_ARRAY lstytczfeRecord = Optional.ofNullable(respService.getLstytczfe_ARRAY()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.Lstytczfe_ARRAY());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3026.resp.lstytczfe.Record> ytczfeRecords = Optional.ofNullable(lstytczfeRecord.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(ytczfeRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstytczfe> lstytczfeList = ytczfeRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstytczfe temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp.Lstytczfe();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3026RespDto.setLstytczfe(lstytczfeList);
                }
                ln3026ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3026ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3026ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3026ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3026.key, EsbEnum.TRADE_CODE_LN3026.value, e.getMessage());
            ln3026ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3026ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3026ResultDto.setData(ln3026RespDto);
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3026.key, EsbEnum.TRADE_CODE_LN3026.value, JSON.toJSONString(ln3026ResultDto));
        return ln3026ResultDto;
    }
}
