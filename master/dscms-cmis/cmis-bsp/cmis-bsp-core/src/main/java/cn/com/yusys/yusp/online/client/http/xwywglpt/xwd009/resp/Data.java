package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.resp;

import com.alibaba.fastjson.JSONObject;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/17 14:03
 * @since 2021/7/17 14:03
 */
public class Data {
    private JSONObject jsonObject;

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public void setJsonObject(JSONObject jsonObject) {
        this.jsonObject = jsonObject;
    }

    @Override
    public String toString() {
        return "Data{" +
                "jsonObject=" + jsonObject +
                '}';
    }
}
