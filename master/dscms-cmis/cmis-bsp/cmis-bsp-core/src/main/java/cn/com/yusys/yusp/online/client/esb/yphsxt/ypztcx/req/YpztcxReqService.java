package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.req;

/**
 * 请求Service：信贷押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
public class YpztcxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "YpztcxReqService{" +
                "service=" + service +
                '}';
    }
}

