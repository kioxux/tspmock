package cn.com.yusys.yusp.online.client.http.outerdata.ocr.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：报表核对统计总览列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cbocr3ReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    @JsonProperty(value = "servtp")
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    @JsonProperty(value = "servsq")
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    @JsonProperty(value = "userid")
    private String userid;//	柜员号	否	char(7)	是		userid
    @JsonProperty(value = "brchno")
    private String brchno;//	部门号	否	char(5)	是		brchno
    @JsonProperty(value = "datasq")
    private String datasq; //全局流水
    @JsonProperty(value = "servdt")
    private String servdt;//    交易日期
    @JsonProperty(value = "servti")
    private String servti;//    交易时间

    @JsonProperty(value = "subTaskId")
    private String subTaskId;//子任务id
    @JsonProperty(value = "templateId")
    private String templateId;//子任务对应的模板id

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSubTaskId() {
        return subTaskId;
    }

    public void setSubTaskId(String subTaskId) {
        this.subTaskId = subTaskId;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    @Override
    public String toString() {
        return "Cbocr3ReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", subTaskId='" + subTaskId + '\'' +
                ", templateId='" + templateId + '\'' +
                '}';
    }
}
