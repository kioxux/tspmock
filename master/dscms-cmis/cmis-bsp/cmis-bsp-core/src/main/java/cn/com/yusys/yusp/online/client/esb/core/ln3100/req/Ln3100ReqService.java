package cn.com.yusys.yusp.online.client.esb.core.ln3100.req;

/**
 * 请求Service：根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3100ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

