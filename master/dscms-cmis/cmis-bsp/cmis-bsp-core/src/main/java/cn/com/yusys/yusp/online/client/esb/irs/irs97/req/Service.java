package cn.com.yusys.yusp.online.client.esb.irs.irs97.req;

import cn.com.yusys.yusp.online.client.esb.irs.common.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Service：新增授信时债项评级接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:00
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //   全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    @JsonProperty
    private LimitApplyInfo LimitApplyInfo; // 综合授信申请信息
    @JsonProperty
    private LimitDetailsInfo LimitDetailsInfo; // 分项额度信息
    @JsonProperty
    private LimitProductInfo LimitProductInfo; // 产品额度信息
    @JsonProperty
    private CustomerInfo CustomerInfo; // 客户信息
    @JsonProperty
    private LimitPleMortInfo LimitPleMortInfo; //授信分项额度与抵质押、保证人关系信息
    @JsonProperty
    private PledgeInfo PledgeInfo; // 质押物信息
    @JsonProperty
    private MortgageInfo MortgageInfo; // 抵押物信息
    @JsonProperty
    private AssurePersonInfo AssurePersonInfo; // 保证人信息
    @JsonProperty
    private CurInfo CurInfo; // 汇率信息

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo getLimitApplyInfo() {
        return LimitApplyInfo;
    }
    @JsonIgnore
    public void setLimitApplyInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo limitApplyInfo) {
        LimitApplyInfo = limitApplyInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo getLimitDetailsInfo() {
        return LimitDetailsInfo;
    }
    @JsonIgnore
    public void setLimitDetailsInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo limitDetailsInfo) {
        LimitDetailsInfo = limitDetailsInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo getLimitProductInfo() {
        return LimitProductInfo;
    }
    @JsonIgnore
    public void setLimitProductInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo limitProductInfo) {
        LimitProductInfo = limitProductInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo getCustomerInfo() {
        return CustomerInfo;
    }
    @JsonIgnore
    public void setCustomerInfo(cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo customerInfo) {
        CustomerInfo = customerInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo getLimitPleMortInfo() {
        return LimitPleMortInfo;
    }
    @JsonIgnore
    public void setLimitPleMortInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo limitPleMortInfo) {
        LimitPleMortInfo = limitPleMortInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo getPledgeInfo() {
        return PledgeInfo;
    }
    @JsonIgnore
    public void setPledgeInfo(cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo pledgeInfo) {
        PledgeInfo = pledgeInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo getMortgageInfo() {
        return MortgageInfo;
    }
    @JsonIgnore
    public void setMortgageInfo(cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo mortgageInfo) {
        MortgageInfo = mortgageInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo getAssurePersonInfo() {
        return AssurePersonInfo;
    }
    @JsonIgnore
    public void setAssurePersonInfo(cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo assurePersonInfo) {
        AssurePersonInfo = assurePersonInfo;
    }
    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo getCurInfo() {
        return CurInfo;
    }
    @JsonIgnore
    public void setCurInfo(cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo curInfo) {
        CurInfo = curInfo;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", LimitApplyInfo=" + LimitApplyInfo +
                ", LimitDetailsInfo=" + LimitDetailsInfo +
                ", LimitProductInfo=" + LimitProductInfo +
                ", CustomerInfo=" + CustomerInfo +
                ", LimitPleMortInfo=" + LimitPleMortInfo +
                ", PledgeInfo=" + PledgeInfo +
                ", MortgageInfo=" + MortgageInfo +
                ", AssurePersonInfo=" + AssurePersonInfo +
                ", CurInfo=" + CurInfo +
                '}';
    }
}
