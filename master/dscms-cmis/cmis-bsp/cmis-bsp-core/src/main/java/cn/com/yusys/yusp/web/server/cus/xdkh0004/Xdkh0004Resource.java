package cn.com.yusys.yusp.web.server.cus.xdkh0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0004.req.Xdkh0004ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0004.resp.Xdkh0004RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.req.Xdkh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0004.resp.Xdkh0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户配偶信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0004:查询客户配偶信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0004Resource.class);

    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0004
     * 交易描述：查询客户配偶信息
     *
     * @param xdkh0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0004:对公客户基本信息查询")
    @PostMapping("/xdkh0004")
//    @Idempotent({"xdcakh0004", "#xdkh0004ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0004RespDto xdkh0004(@Validated @RequestBody Xdkh0004ReqDto xdkh0004ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, JSON.toJSONString(xdkh0004ReqDto));
        Xdkh0004DataReqDto xdkh0004DataReqDto = new Xdkh0004DataReqDto();// 请求Data： 查询客户配偶信息
        Xdkh0004DataRespDto xdkh0004DataRespDto = new Xdkh0004DataRespDto();// 响应Data：查询客户配偶信息
        Xdkh0004RespDto xdkh0004RespDto = new Xdkh0004RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0004.req.Data reqData = null; // 请求Data：查询客户配偶信息
        cn.com.yusys.yusp.dto.server.cus.xdkh0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0004.resp.Data();// 响应Data：查询客户配偶信息
        try {
            // 从 xdkh0004ReqDto获取 reqData
            reqData = xdkh0004ReqDto.getData();
            // 将 reqData 拷贝到xdkh0004DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0004DataReqDto);
            // 调用服务
            ResultDto<Xdkh0004DataRespDto> xdkh0004DataResultDto = dscmsCusClientService.xdkh0004(xdkh0004DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdkh0004RespDto.setErorcd(Optional.ofNullable(xdkh0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0004RespDto.setErortx(Optional.ofNullable(xdkh0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0004DataResultDto.getCode())) {
                xdkh0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0004DataRespDto = xdkh0004DataResultDto.getData();
                // 将 xdkh0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, e.getMessage());
            xdkh0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0004RespDto.setDatasq(servsq);
        xdkh0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0004.key, DscmsEnum.TRADE_CODE_XDKH0004.value, JSON.toJSONString(xdkh0004RespDto));
        return xdkh0004RespDto;
    }
}
