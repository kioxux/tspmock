package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp;

/**
 * 响应Service：大额往账列表查询
 *
 * @author chenyong
 * @version 1.0
 */
public class HvpylsRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "HvpylsRespService{" +
                "service=" + service +
                '}';
    }
}
