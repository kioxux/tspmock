package cn.com.yusys.yusp.web.server.biz.xddb0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0010.req.Xddb0010ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0010.resp.Xddb0010RespDto;
import cn.com.yusys.yusp.dto.server.xddb0010.req.Xddb0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0010.resp.Xddb0010DataRespDto;
import cn.com.yusys.yusp.dto.server.xddb0010.resp.YpList;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷押品列表查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0010:信贷押品列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0010Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0010
     * 交易描述：信贷押品列表查询
     *
     * @param xddb0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷押品列表查询")
    @PostMapping("/xddb0010")
    //@Idempotent({"xddb0010", "#xddb0010ReqDto.datasq"})
    protected @ResponseBody
    Xddb0010RespDto xddb0010(@Validated @RequestBody Xddb0010ReqDto xddb0010ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, JSON.toJSONString(xddb0010ReqDto));
        Xddb0010DataReqDto xddb0010DataReqDto = new Xddb0010DataReqDto();// 请求Data： 信贷押品列表查询
        Xddb0010DataRespDto xddb0010DataRespDto = new Xddb0010DataRespDto();// 响应Data：信贷押品列表查询
        Xddb0010RespDto xddb0010RespDto = new Xddb0010RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0010.req.Data reqData = null; // 请求Data：信贷押品列表查询
        cn.com.yusys.yusp.dto.server.biz.xddb0010.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0010.resp.Data();// 响应Data：信贷押品列表查询

        try {
            // 从 xddb0010ReqDto获取 reqData
            reqData = xddb0010ReqDto.getData();
            // 将 reqData 拷贝到xddb0010DataReqDto
            BeanUtils.copyProperties(reqData, xddb0010DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, JSON.toJSONString(xddb0010DataReqDto));
            ResultDto<Xddb0010DataRespDto> xddb0010DataResultDto = dscmsBizDbClientService.xddb0010(xddb0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, JSON.toJSONString(xddb0010DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0010RespDto.setErorcd(Optional.ofNullable(xddb0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0010RespDto.setErortx(Optional.ofNullable(xddb0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0010DataResultDto.getCode())) {
                xddb0010DataRespDto = xddb0010DataResultDto.getData();
                xddb0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0010DataRespDto拷贝到 respData
                List<YpList> source = xddb0010DataRespDto.getYpList();
                List<cn.com.yusys.yusp.dto.server.biz.xddb0010.resp.YpList> target = new ArrayList<>();
                for (YpList ypList : source) {
                    cn.com.yusys.yusp.dto.server.biz.xddb0010.resp.YpList yp = new cn.com.yusys.yusp.dto.server.biz.xddb0010.resp.YpList();
                    BeanUtils.copyProperties(ypList, yp);
                    target.add(yp);
                }
                respData.setYpLists(target);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, e.getMessage());
            xddb0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0010RespDto.setDatasq(servsq);

        xddb0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0010.key, DscmsEnum.TRADE_CODE_XDDB0010.value, JSON.toJSONString(xddb0010RespDto));
        return xddb0010RespDto;
    }
}
