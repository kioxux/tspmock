package cn.com.yusys.yusp.online.client.esb.circp.fb1205.resp;

/**
 * 响应Service：放款信息推送
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1205RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1205RespService{" +
                "service=" + service +
                '}';
    }
}
