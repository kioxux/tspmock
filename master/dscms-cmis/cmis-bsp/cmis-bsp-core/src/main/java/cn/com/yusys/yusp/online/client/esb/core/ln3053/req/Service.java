package cn.com.yusys.yusp.online.client.esb.core.ln3053.req;

/**
 * <br>
 * 0.2ZRC:2021/5/25 20:30:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/25 20:30
 * @since 2021/5/25 20:30
 */

import java.math.BigDecimal;

/**
 * 请求Service：还款计划调整
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水


    private String dkjiejuh;//贷款借据号
    private String daikczbz;//业务操作标志
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private BigDecimal jiejuuje;//借据金额
    private BigDecimal shengybj;//剩余本金
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private Integer zongqish;//总期数
    private Integer benqqish;//本期期数
    private cn.com.yusys.yusp.online.client.esb.core.ln3053.req.List list;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getShengybj() {
        return shengybj;
    }

    public void setShengybj(BigDecimal shengybj) {
        this.shengybj = shengybj;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public Integer getZongqish() {
        return zongqish;
    }

    public void setZongqish(Integer zongqish) {
        this.zongqish = zongqish;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", shengybj=" + shengybj +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", zongqish=" + zongqish +
                ", benqqish=" + benqqish +
                ", list=" + list +
                '}';
    }
}

