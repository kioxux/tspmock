package cn.com.yusys.yusp.online.client.esb.core.ln3114.resp;

import java.math.BigDecimal;

/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class Record {

    private Integer benqqish;//本期期数
    private String huankriq;//还款日期
    private BigDecimal chushibj;//初始本金
    private BigDecimal chushilx;//初始利息
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal bjdshyje;//本阶段剩余本金

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getHuankriq() {
        return huankriq;
    }

    public void setHuankriq(String huankriq) {
        this.huankriq = huankriq;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getBjdshyje() {
        return bjdshyje;
    }

    public void setBjdshyje(BigDecimal bjdshyje) {
        this.bjdshyje = bjdshyje;
    }

    @Override
    public String toString() {
        return "Lstqgtqhk{" +
                "benqqish='" + benqqish + '\'' +
                "huankriq='" + huankriq + '\'' +
                "chushibj='" + chushibj + '\'' +
                "chushilx='" + chushilx + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "bjdshyje='" + bjdshyje + '\'' +
                '}';
    }
}
