package cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp;

/**
 * 响应Service：查找指定数据日期的放款合约明细记录历史表（利翃）一览信息的借据号
 *
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
