package cn.com.yusys.yusp.web.client.esb.zjywxt.clfxcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.req.ClfxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.resp.ClfxcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.req.ClfxcxReqService;
import cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.resp.ClfxcxRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2zjywxt")
public class Dscms2ClfxcxResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2ClfxcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：clfxcx
     * 交易描述：协议信息查询
     *
     * @param clfxcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/clfxcx")
    protected @ResponseBody
    ResultDto<ClfxcxRespDto> clfxcx(@Validated @RequestBody ClfxcxReqDto clfxcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLFXCX.key, EsbEnum.TRADE_CODE_CLFXCX.value, JSON.toJSONString(clfxcxReqDto));
        cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.resp.Service();
        ClfxcxReqService clfxcxReqService = new ClfxcxReqService();
        ClfxcxRespService clfxcxRespService = new ClfxcxRespService();
        ClfxcxRespDto clfxcxRespDto = new ClfxcxRespDto();
        ResultDto<ClfxcxRespDto> clfxcxResultDto = new ResultDto<ClfxcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将clfxcxReqDto转换成reqService
            BeanUtils.copyProperties(clfxcxReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CLFXCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_GAP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_GAP.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            clfxcxReqService.setService(reqService);
            // 将clfxcxReqService转换成clfxcxReqServiceMap
            Map clfxcxReqServiceMap = beanMapUtil.beanToMap(clfxcxReqService);
            context.put("tradeDataMap", clfxcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLFXCX.key, EsbEnum.TRADE_CODE_CLFXCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CLFXCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLFXCX.key, EsbEnum.TRADE_CODE_CLFXCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            clfxcxRespService = beanMapUtil.mapToBean(tradeDataMap, ClfxcxRespService.class, ClfxcxRespService.class);
            respService = clfxcxRespService.getService();

            clfxcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            clfxcxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成ClfxcxRespDto
                BeanUtils.copyProperties(respService, clfxcxRespDto);
                clfxcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                clfxcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                clfxcxResultDto.setCode(EpbEnum.EPB099999.key);
                clfxcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLFXCX.key, EsbEnum.TRADE_CODE_CLFXCX.value, e.getMessage());
            clfxcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            clfxcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        clfxcxResultDto.setData(clfxcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CLFXCX.key, EsbEnum.TRADE_CODE_CLFXCX.value, JSON.toJSONString(clfxcxResultDto));
        return clfxcxResultDto;
    }
}
