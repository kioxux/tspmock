package cn.com.yusys.yusp.online.client.esb.irs.irs19.req;

import java.math.BigDecimal;

/**
 * 请求Service：专业贷款基本信息同步
 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String waibclma;//外部处理码
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String sxserialno;//授信申请流水号
    private String custid;//客户编号
    private String custname;//客户名称
    private String certtype;//证件类型
    private String certid;//证件号码
    private String cantoncode;//注册地行政区划代码
    private BigDecimal creaditsum;//授信申请金额
    private String creadittype;//专业贷款类型
    private String sxbegdate;//授信起始日
    private String sxenddate;//授信到期日
    private String userid1;//管户人编号
    private String username;//管户人名称
    private String orgid;//管户机构编号
    private String orgname;//管户机构名称

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getWaibclma() {
		return waibclma;
	}

	public void setWaibclma(String waibclma) {
		this.waibclma = waibclma;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getSxserialno() {
        return sxserialno;
    }

    public void setSxserialno(String sxserialno) {
        this.sxserialno = sxserialno;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCerttype() {
        return certtype;
    }

    public void setCerttype(String certtype) {
        this.certtype = certtype;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getCantoncode() {
        return cantoncode;
    }

    public void setCantoncode(String cantoncode) {
        this.cantoncode = cantoncode;
    }

    public BigDecimal getCreaditsum() {
        return creaditsum;
    }

    public void setCreaditsum(BigDecimal creaditsum) {
        this.creaditsum = creaditsum;
    }

    public String getCreadittype() {
        return creadittype;
    }

    public void setCreadittype(String creadittype) {
        this.creadittype = creadittype;
    }

    public String getSxbegdate() {
        return sxbegdate;
    }

    public void setSxbegdate(String sxbegdate) {
        this.sxbegdate = sxbegdate;
    }

    public String getSxenddate() {
        return sxenddate;
    }

    public void setSxenddate(String sxenddate) {
        this.sxenddate = sxenddate;
    }

    public String getUserid1() {
        return userid1;
    }

    public void setUserid1(String userid1) {
        this.userid1 = userid1;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getOrgid() {
        return orgid;
    }

    public void setOrgid(String orgid) {
        this.orgid = orgid;
    }

    public String getOrgname() {
        return orgname;
    }

    public void setOrgname(String orgname) {
        this.orgname = orgname;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", datasq='" + datasq + '\'' +
				", servsq='" + servsq + '\'' +
				", waibclma='" + waibclma + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", sxserialno='" + sxserialno + '\'' +
				", custid='" + custid + '\'' +
				", custname='" + custname + '\'' +
				", certtype='" + certtype + '\'' +
				", certid='" + certid + '\'' +
				", cantoncode='" + cantoncode + '\'' +
				", creaditsum=" + creaditsum +
				", creadittype='" + creadittype + '\'' +
				", sxbegdate='" + sxbegdate + '\'' +
				", sxenddate='" + sxenddate + '\'' +
				", userid1='" + userid1 + '\'' +
				", username='" + username + '\'' +
				", orgid='" + orgid + '\'' +
				", orgname='" + orgname + '\'' +
				'}';
	}
}
