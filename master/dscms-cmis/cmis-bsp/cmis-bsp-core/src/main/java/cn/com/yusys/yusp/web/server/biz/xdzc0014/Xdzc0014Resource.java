package cn.com.yusys.yusp.web.server.biz.xdzc0014;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0014.req.Xdzc0014ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0014.resp.Xdzc0014RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0014.req.Xdzc0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0014.resp.Xdzc0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池主动还款接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0014:资产池主动还款接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0014Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0014
     * 交易描述：资产池主动还款接口
     *
     * @param xdzc0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池主动还款接口")
    @PostMapping("/xdzc0014")
    //@Idempotent({"xdzc014", "#xdzc0014ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0014RespDto xdzc0014(@Validated @RequestBody Xdzc0014ReqDto xdzc0014ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, JSON.toJSONString(xdzc0014ReqDto));
        Xdzc0014DataReqDto xdzc0014DataReqDto = new Xdzc0014DataReqDto();// 请求Data： 资产池主动还款接口
        Xdzc0014DataRespDto xdzc0014DataRespDto = new Xdzc0014DataRespDto();// 响应Data：资产池主动还款接口
        Xdzc0014RespDto xdzc0014RespDto = new Xdzc0014RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdzc0014.req.Data reqData = null; // 请求Data：资产池主动还款接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0014.resp.Data();// 响应Data：资产池主动还款接口

        try {
            // 从 xdzc0014ReqDto获取 reqData
            reqData = xdzc0014ReqDto.getData();
            // 将 reqData 拷贝到xdzc0014DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0014DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, JSON.toJSONString(xdzc0014DataReqDto));
            ResultDto<Xdzc0014DataRespDto> xdzc0014DataResultDto = dscmsBizZcClientService.xdzc0014(xdzc0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, JSON.toJSONString(xdzc0014DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0014RespDto.setErorcd(Optional.ofNullable(xdzc0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0014RespDto.setErortx(Optional.ofNullable(xdzc0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0014DataResultDto.getCode())) {
                xdzc0014DataRespDto = xdzc0014DataResultDto.getData();
                xdzc0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0014DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, e.getMessage());
            xdzc0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0014RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0014RespDto.setDatasq(servsq);

        xdzc0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0014.key, DscmsEnum.TRADE_CODE_XDZC0014.value, JSON.toJSONString(xdzc0014RespDto));
        return xdzc0014RespDto;
    }
}
