package cn.com.yusys.yusp.online.client.esb.core.ln3025.req;

import java.math.BigDecimal;

/**
 * 请求Service：垫款开户
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servdt;//交易日期
    private String servti;//交易时间

    private String dkkhczbz;//开户操作标志
    private String dkjiejuh;//贷款借据号
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String khjingli;//客户经理
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String huobdhao;//货币代号
    private BigDecimal zdkuanje;//转垫款金额
    private String yngyjigo;//营业机构
    private String ysywleix;//衍生业务类型
    private String ysywbhao;//衍生业务编号
    private String kehuzhao;//客户账号
    private String khzhhzxh;//客户账号子序号
    private String syzjclfs;//剩余资金处理方式
    private BigDecimal zhrujine;//转入金额
    private String lilvleix;//利率类型
    private String zclilvbh;//正常利率编号
    private String nyuelilv;//年/月利率标识
    private BigDecimal zhchlilv;//正常利率
    private String lilvtzfs;//利率调整方式
    private String lilvtzzq;//利率调整周期
    private String lilvfdfs;//利率浮动方式
    private BigDecimal lilvfdzh;//利率浮动值
    private String yqllcklx;//逾期利率参考类型
    private String yuqillbh;//逾期利率编号
    private String yuqinyll;//逾期年月利率
    private BigDecimal yuqililv;//逾期利率
    private String yuqitzfs;//逾期利率调整方式
    private String yuqitzzq;//逾期利率调整周期
    private String yqfxfdfs;//逾期罚息浮动方式
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    private String flllcklx;//复利利率参考类型
    private String fulilvbh;//复利利率编号
    private String fulilvny;//复利利率年月标识
    private BigDecimal fulililv;//复利利率
    private String fulitzfs;//复利利率调整方式
    private String fulitzzq;//复利利率调整周期
    private String fulifdfs;//复利利率浮动方式
    private BigDecimal fulifdzh;//复利利率浮动值
    private String dkrzhzhh;//贷款入账账号
    private String dkrzhzxh;//贷款入账账号子序号
    private String llqxkdfs;//利率期限靠档方式
    private String lilvqixx;//利率期限
    private Integer benqqish;//本期期数
    private String gljgleib;//管理机构类别
    private String chuzhhao;//出账号
    private String hetongbh;//合同编号
    private String kuaijilb;//会计类别
    private String fuhejgou;//复核机构

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getZdkuanje() {
        return zdkuanje;
    }

    public void setZdkuanje(BigDecimal zdkuanje) {
        this.zdkuanje = zdkuanje;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getYsywleix() {
        return ysywleix;
    }

    public void setYsywleix(String ysywleix) {
        this.ysywleix = ysywleix;
    }

    public String getYsywbhao() {
        return ysywbhao;
    }

    public void setYsywbhao(String ysywbhao) {
        this.ysywbhao = ysywbhao;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getSyzjclfs() {
        return syzjclfs;
    }

    public void setSyzjclfs(String syzjclfs) {
        this.syzjclfs = syzjclfs;
    }

    public BigDecimal getZhrujine() {
        return zhrujine;
    }

    public void setZhrujine(BigDecimal zhrujine) {
        this.zhrujine = zhrujine;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getGljgleib() {
        return gljgleib;
    }

    public void setGljgleib(String gljgleib) {
        this.gljgleib = gljgleib;
    }

    public String getChuzhhao() {
        return chuzhhao;
    }

    public void setChuzhhao(String chuzhhao) {
        this.chuzhhao = chuzhhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "khjingli='" + khjingli + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "zdkuanje='" + zdkuanje + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "ysywleix='" + ysywleix + '\'' +
                "ysywbhao='" + ysywbhao + '\'' +
                "kehuzhao='" + kehuzhao + '\'' +
                "khzhhzxh='" + khzhhzxh + '\'' +
                "syzjclfs='" + syzjclfs + '\'' +
                "zhrujine='" + zhrujine + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "yuqillbh='" + yuqillbh + '\'' +
                "yuqinyll='" + yuqinyll + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "yuqitzfs='" + yuqitzfs + '\'' +
                "yuqitzzq='" + yuqitzzq + '\'' +
                "yqfxfdfs='" + yqfxfdfs + '\'' +
                "yqfxfdzh='" + yqfxfdzh + '\'' +
                "flllcklx='" + flllcklx + '\'' +
                "fulilvbh='" + fulilvbh + '\'' +
                "fulilvny='" + fulilvny + '\'' +
                "fulililv='" + fulililv + '\'' +
                "fulitzfs='" + fulitzfs + '\'' +
                "fulitzzq='" + fulitzzq + '\'' +
                "fulifdfs='" + fulifdfs + '\'' +
                "fulifdzh='" + fulifdzh + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "llqxkdfs='" + llqxkdfs + '\'' +
                "lilvqixx='" + lilvqixx + '\'' +
                "benqqish='" + benqqish + '\'' +
                "gljgleib='" + gljgleib + '\'' +
                "chuzhhao='" + chuzhhao + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                '}';
    }
}
