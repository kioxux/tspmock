package cn.com.yusys.yusp.web.client.esb.rircp.fbyd34;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd34.req.Fbyd34ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd34.resp.Fbyd34RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.req.Fbyd34ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.resp.Fbyd34RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:支用模型校验
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbyd34Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Fbyd34Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbyd34
     * 交易描述：支用模型校验
     *
     * @param fbyd34ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbyd34")
    protected @ResponseBody
    ResultDto<Fbyd34RespDto> fbyd34(@Validated @RequestBody Fbyd34ReqDto fbyd34ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD34.key, EsbEnum.TRADE_CODE_FBYD34.value, JSON.toJSONString(fbyd34ReqDto));
		cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.req.Service();
		cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.resp.Service();
        Fbyd34ReqService fbyd34ReqService = new Fbyd34ReqService();
        Fbyd34RespService fbyd34RespService = new Fbyd34RespService();
        Fbyd34RespDto fbyd34RespDto = new Fbyd34RespDto();
        ResultDto<Fbyd34RespDto> fbyd34ResultDto = new ResultDto<Fbyd34RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将fbyd34ReqDto转换成reqService
			BeanUtils.copyProperties(fbyd34ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_FBYD34.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
			fbyd34ReqService.setService(reqService);
			// 将fbyd34ReqService转换成fbyd34ReqServiceMap
			Map fbyd34ReqServiceMap = beanMapUtil.beanToMap(fbyd34ReqService);
			context.put("tradeDataMap", fbyd34ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD34.key, EsbEnum.TRADE_CODE_FBYD34.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBYD34.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD34.key, EsbEnum.TRADE_CODE_FBYD34.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			fbyd34RespService = beanMapUtil.mapToBean(tradeDataMap, Fbyd34RespService.class, Fbyd34RespService.class);
			respService = fbyd34RespService.getService();
			//  将respService转换成Fbyd34RespDto
			fbyd34ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			fbyd34ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成fbyd02RespDto
				BeanUtils.copyProperties(respService, fbyd34RespDto);
				fbyd34ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				fbyd34ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				fbyd34ResultDto.setCode(EpbEnum.EPB099999.key);
				fbyd34ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD34.key, EsbEnum.TRADE_CODE_FBYD34.value, e.getMessage());
			fbyd34ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			fbyd34ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		fbyd34ResultDto.setData(fbyd34RespDto);
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD34.key, EsbEnum.TRADE_CODE_FBYD34.value, JSON.toJSONString(fbyd34ReqDto));
        return fbyd34ResultDto;
    }
}
