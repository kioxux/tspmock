package cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp;

/**
 * 响应Service：查找指定数据日期的放款合约明细记录历史表（利翃）一览
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码
    private String erortx; // 响应信息

    private cn.com.yusys.yusp.online.client.esb.rircp.fbxd04.resp.List list;//list

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }


}
