package cn.com.yusys.yusp.web.client.esb.core.ln3196;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3196.Ln3196ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3196.Ln3196RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3196.LstioDpCusSysAccListInfo;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3196.req.Ln3196ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.Ln3196RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.LstioDpCusSysAccListInfo_ARRAY;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3196)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3196Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3196Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 账号子序号查询交易，支持查询内部户、负债账户接口（处理码ln3196）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3196:账号子序号查询交易，支持查询内部户、负债账户接口")
    @PostMapping("/ln3196")
    protected @ResponseBody
    ResultDto<Ln3196RespDto> ln3196(@Validated @RequestBody Ln3196ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3196.key, EsbEnum.TRADE_CODE_LN3196.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3196.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3196.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.Service();
        Ln3196ReqService ln3196ReqService = new Ln3196ReqService();
        Ln3196RespService ln3196RespService = new Ln3196RespService();
        Ln3196RespDto ln3196RespDto = new Ln3196RespDto();
        ResultDto<Ln3196RespDto> ln3196ResultDto = new ResultDto<Ln3196RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Ln3196ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3196.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3196ReqService.setService(reqService);
            // 将ln3196ReqService转换成ln3196ReqServiceMap
            Map ln3196ReqServiceMap = beanMapUtil.beanToMap(ln3196ReqService);
            context.put("tradeDataMap", ln3196ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3196.key, EsbEnum.TRADE_CODE_LN3196.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3196.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3196.key, EsbEnum.TRADE_CODE_LN3196.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3196RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3196RespService.class, Ln3196RespService.class);
            respService = ln3196RespService.getService();

            //  将Ln3196RespDto封装到ResultDto<Ln3196RespDto>
            ln3196ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3196ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3196RespDto
                BeanUtils.copyProperties(respService, ln3196RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.LstioDpCusSysAccListInfo_ARRAY lstioDpCusSysAccListInfo_ARRAY = Optional.ofNullable(respService.getLstioDpCusSysAccListInfo_ARRAY()).orElseGet(() -> new LstioDpCusSysAccListInfo_ARRAY());
                respService.setLstioDpCusSysAccListInfo_ARRAY(lstioDpCusSysAccListInfo_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstioDpCusSysAccListInfo_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.lstioDpCusSysAccListInfo.Record> recordList = Optional.ofNullable(respService.getLstioDpCusSysAccListInfo_ARRAY().getRecord())
                            .orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3196.LstioDpCusSysAccListInfo> lstioDpCusSysAccListInfoList = recordList.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3196.LstioDpCusSysAccListInfo lstioDpCusSysAccListInfo = new LstioDpCusSysAccListInfo();
                        BeanUtils.copyProperties(record, lstioDpCusSysAccListInfo);
                        return lstioDpCusSysAccListInfo;
                    }).collect(Collectors.toList());
                    ln3196RespDto.setLstioDpCusSysAccListInfo(lstioDpCusSysAccListInfoList);
                }
                ln3196ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3196ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3196ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3196ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3196.key, EsbEnum.TRADE_CODE_LN3196.value, e.getMessage());
            ln3196ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3196ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3196ResultDto.setData(ln3196RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3196.key, EsbEnum.TRADE_CODE_LN3196.value, JSON.toJSONString(ln3196ResultDto));
        return ln3196ResultDto;
    }
}
