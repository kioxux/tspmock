package cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.resp;

/**
 * 响应Service：贷记入账状态查询申请往帐
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private String ttfnlst;//入账状态
    private String rxcinfo;//退汇原因码
    private String rexchngrslt;//退汇原因
    private String npcprcsts;//NPC处理状态
    private String npcprccd;//NPC处理码
    private String npcrjctinf;//处理信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getTtfnlst() {
        return ttfnlst;
    }

    public void setTtfnlst(String ttfnlst) {
        this.ttfnlst = ttfnlst;
    }

    public String getRxcinfo() {
        return rxcinfo;
    }

    public void setRxcinfo(String rxcinfo) {
        this.rxcinfo = rxcinfo;
    }

    public String getRexchngrslt() {
        return rexchngrslt;
    }

    public void setRexchngrslt(String rexchngrslt) {
        this.rexchngrslt = rexchngrslt;
    }

    public String getNpcprcsts() {
        return npcprcsts;
    }

    public void setNpcprcsts(String npcprcsts) {
        this.npcprcsts = npcprcsts;
    }

    public String getNpcprccd() {
        return npcprccd;
    }

    public void setNpcprccd(String npcprccd) {
        this.npcprccd = npcprccd;
    }

    public String getNpcrjctinf() {
        return npcrjctinf;
    }

    public void setNpcrjctinf(String npcrjctinf) {
        this.npcrjctinf = npcrjctinf;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", ttfnlst='" + ttfnlst + '\'' +
                ", rxcinfo='" + rxcinfo + '\'' +
                ", rexchngrslt='" + rexchngrslt + '\'' +
                ", npcprcsts='" + npcprcsts + '\'' +
                ", npcprccd='" + npcprccd + '\'' +
                ", npcrjctinf='" + npcrjctinf + '\'' +
                '}';
    }
}
