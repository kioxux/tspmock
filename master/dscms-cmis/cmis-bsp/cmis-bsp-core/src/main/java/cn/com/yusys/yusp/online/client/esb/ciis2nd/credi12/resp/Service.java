package cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.resp;

/**
 * 响应Service：ESB通用查询接口（处理码credi12）
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 19:53
 */
public class Service {
    private String erorcd;//	响应码	;	是	;	响应码 0000 查询成功 9999 查询失败 6666白户
    private String erortx;//	响应信息	;	是	;	响应信息
    private String reportId;//	报告编号	;	否	;	报告编号
    private String reqId;//	业务明细ID	;	是	;	本次查询交易流水号，区分查询交易的唯一标识

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", reportId='" + reportId + '\'' +
                ", reqId='" + reqId + '\'' +
                '}';
    }
}
