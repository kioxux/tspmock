package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.req;

import java.math.BigDecimal;

/**
 * 请求Service：大额往账列表查询
 */
public class Service {


    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String hvflag;//大小额交易标志
    private String functp;//业务类型
    private String csbxno;//钱箱号
    private String subprc;//前台交易码
    private String operdt;//交易日期
    private String srflag;//往来账标志
    private String opersq;//业务受理编号
    private String termid;//终端号
    private String systdt;//系统日期
    private String bankid;//行号

    private String xtrqdt;//系统日期
    private BigDecimal tranam;//交易金额
    private String sdcode;//发起行行号
    private String rdcode;//接收行行号
    private String pyerac;//付款人账号
    private String pyeeac;//收款人账号
    private String transt;//交易状态
    private String hstrsq;//业务受理编号
    private String cocain;//来往帐标志
    private String begnum;//起始笔数
    private String countnum;//查询笔数
    private String channelseq;//渠道流水

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getHvflag() {
        return hvflag;
    }

    public void setHvflag(String hvflag) {
        this.hvflag = hvflag;
    }

    public String getFunctp() {
        return functp;
    }

    public void setFunctp(String functp) {
        this.functp = functp;
    }

    public String getCsbxno() {
        return csbxno;
    }

    public void setCsbxno(String csbxno) {
        this.csbxno = csbxno;
    }

    public String getSubprc() {
        return subprc;
    }

    public void setSubprc(String subprc) {
        this.subprc = subprc;
    }

    public String getOperdt() {
        return operdt;
    }

    public void setOperdt(String operdt) {
        this.operdt = operdt;
    }

    public String getSrflag() {
        return srflag;
    }

    public void setSrflag(String srflag) {
        this.srflag = srflag;
    }

    public String getOpersq() {
        return opersq;
    }

    public void setOpersq(String opersq) {
        this.opersq = opersq;
    }

    public String getTermid() {
        return termid;
    }

    public void setTermid(String termid) {
        this.termid = termid;
    }

    public String getSystdt() {
        return systdt;
    }

    public void setSystdt(String systdt) {
        this.systdt = systdt;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getXtrqdt() {
        return xtrqdt;
    }

    public void setXtrqdt(String xtrqdt) {
        this.xtrqdt = xtrqdt;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getSdcode() {
        return sdcode;
    }

    public void setSdcode(String sdcode) {
        this.sdcode = sdcode;
    }

    public String getRdcode() {
        return rdcode;
    }

    public void setRdcode(String rdcode) {
        this.rdcode = rdcode;
    }

    public String getPyerac() {
        return pyerac;
    }

    public void setPyerac(String pyerac) {
        this.pyerac = pyerac;
    }

    public String getPyeeac() {
        return pyeeac;
    }

    public void setPyeeac(String pyeeac) {
        this.pyeeac = pyeeac;
    }

    public String getTranst() {
        return transt;
    }

    public void setTranst(String transt) {
        this.transt = transt;
    }

    public String getHstrsq() {
        return hstrsq;
    }

    public void setHstrsq(String hstrsq) {
        this.hstrsq = hstrsq;
    }

    public String getCocain() {
        return cocain;
    }

    public void setCocain(String cocain) {
        this.cocain = cocain;
    }

    public String getBegnum() {
        return begnum;
    }

    public void setBegnum(String begnum) {
        this.begnum = begnum;
    }

    public String getCountnum() {
        return countnum;
    }

    public void setCountnum(String countnum) {
        this.countnum = countnum;
    }

    public String getChannelseq() {
        return channelseq;
    }

    public void setChannelseq(String channelseq) {
        this.channelseq = channelseq;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", hvflag='" + hvflag + '\'' +
                ", functp='" + functp + '\'' +
                ", csbxno='" + csbxno + '\'' +
                ", subprc='" + subprc + '\'' +
                ", operdt='" + operdt + '\'' +
                ", srflag='" + srflag + '\'' +
                ", opersq='" + opersq + '\'' +
                ", termid='" + termid + '\'' +
                ", systdt='" + systdt + '\'' +
                ", bankid='" + bankid + '\'' +
                ", xtrqdt='" + xtrqdt + '\'' +
                ", tranam=" + tranam +
                ", sdcode='" + sdcode + '\'' +
                ", rdcode='" + rdcode + '\'' +
                ", pyerac='" + pyerac + '\'' +
                ", pyeeac='" + pyeeac + '\'' +
                ", transt='" + transt + '\'' +
                ", hstrsq='" + hstrsq + '\'' +
                ", cocain='" + cocain + '\'' +
                ", begnum='" + begnum + '\'' +
                ", countnum='" + countnum + '\'' +
                ", channelseq='" + channelseq + '\'' +
                '}';
    }
}
