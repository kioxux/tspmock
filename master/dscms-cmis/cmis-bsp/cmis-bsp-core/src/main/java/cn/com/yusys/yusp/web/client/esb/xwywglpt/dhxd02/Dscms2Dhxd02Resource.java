package cn.com.yusys.yusp.web.client.esb.xwywglpt.dhxd02;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.req.Dhxd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.resp.Dhxd02RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.Dhxd02ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.resp.Dhxd02RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 接口处理类:信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装小微业务管理平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Dhxd02Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dhxd02Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：dhxd02
     * 交易描述：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
     *
     * @param dhxd02ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("dhxd02:信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口")
    @PostMapping("/dhxd02")
    protected @ResponseBody
    ResultDto<Dhxd02RespDto> dhxd02(@Validated @RequestBody Dhxd02ReqDto dhxd02ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD02.key, EsbEnum.TRADE_CODE_DHXD02.value, JSON.toJSONString(dhxd02ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.resp.Service();
        Dhxd02ReqService dhxd02ReqService = new Dhxd02ReqService();
        Dhxd02RespService dhxd02RespService = new Dhxd02RespService();
        Dhxd02RespDto dhxd02RespDto = new Dhxd02RespDto();
        ResultDto<Dhxd02RespDto> dhxd02ResultDto = new ResultDto<Dhxd02RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将dhxd02ReqDto转换成reqService
            BeanUtils.copyProperties(dhxd02ReqDto, reqService);

            cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.List targetList = new cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.List();
            if (CollectionUtils.nonEmpty(dhxd02ReqDto.getList())) {
                List<cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.req.List> originList = dhxd02ReqDto.getList();
                List<cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.Record> recordList = new ArrayList<>();
                for (cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.req.List list : originList) {
                    cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.Record target = new cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.Record();
                    BeanUtils.copyProperties(list, target);
					recordList.add(target);
                }
				targetList.setRecord(recordList);
                reqService.setList(targetList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DHXD02.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            dhxd02ReqService.setService(reqService);
            // 将dhxd02ReqService转换成dhxd02ReqServiceMap
            Map dhxd02ReqServiceMap = beanMapUtil.beanToMap(dhxd02ReqService);
            context.put("tradeDataMap", dhxd02ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD02.key, EsbEnum.TRADE_CODE_DHXD02.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DHXD02.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD02.key, EsbEnum.TRADE_CODE_DHXD02.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");

            dhxd02RespService = beanMapUtil.mapToBean(tradeDataMap, Dhxd02RespService.class, Dhxd02RespService.class);
            respService = dhxd02RespService.getService();
            //  将respService转换成Dhxd02RespDto
            dhxd02ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            dhxd02ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, dhxd02RespDto);
                dhxd02ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                dhxd02ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                dhxd02ResultDto.setCode(EpbEnum.EPB099999.key);
                dhxd02ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD02.key, EsbEnum.TRADE_CODE_DHXD02.value, e.getMessage());
            dhxd02ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            dhxd02ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        dhxd02ResultDto.setData(dhxd02RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD02.key, EsbEnum.TRADE_CODE_DHXD02.value, JSON.toJSONString(dhxd02ResultDto));
        return dhxd02ResultDto;
    }
}
