package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp;

import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp.List;

import java.math.BigDecimal;

/**
 * 响应Service：从票据系统获取当日到期票的日出备款金额
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erortx;//交易返回信息
    private String erorcd;//交易返回代码
    private List list;//list

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erortx='" + erortx + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", list=" + list +
                '}';
    }
}
