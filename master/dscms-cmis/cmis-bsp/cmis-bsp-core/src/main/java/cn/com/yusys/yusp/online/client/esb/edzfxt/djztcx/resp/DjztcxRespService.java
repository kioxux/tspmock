package cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.resp;

/**
 * 响应Service：贷记入账状态查询申请往帐
 *
 * @author chenyong
 * @version 1.0
 */
public class DjztcxRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
