package cn.com.yusys.yusp.online.client.esb.irs.xirs11.resp;

/**
 * 响应Service：同业客户信息
 *
 * @author code-generator
 * @version 1.0
 */
public class Xirs11RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
