package cn.com.yusys.yusp.web.client.esb.core.ln3083;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3083.req.Ln3083ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3083.resp.Ln3083RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3083.req.Ln3083ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3083.resp.Ln3083RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3083.resp.Lstytczfe;
import cn.com.yusys.yusp.online.client.esb.core.ln3083.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3083)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3083Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3083Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3083
     * 交易描述：银团协议查询
     *
     * @param ln3083ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3083:银团协议查询")
    @PostMapping("/ln3083")
    protected @ResponseBody
    ResultDto<Ln3083RespDto> ln3083(@Validated @RequestBody Ln3083ReqDto ln3083ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3083.key, EsbEnum.TRADE_CODE_LN3083.value, JSON.toJSONString(ln3083ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3083.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3083.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3083.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3083.resp.Service();
        Ln3083ReqService ln3083ReqService = new Ln3083ReqService();
        Ln3083RespService ln3083RespService = new Ln3083RespService();
        Ln3083RespDto ln3083RespDto = new Ln3083RespDto();
        ResultDto<Ln3083RespDto> ln3083ResultDto = new ResultDto<Ln3083RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3083ReqDto转换成reqService
            BeanUtils.copyProperties(ln3083ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3083.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3083ReqService.setService(reqService);
            // 将ln3083ReqService转换成ln3083ReqServiceMap
            Map ln3083ReqServiceMap = beanMapUtil.beanToMap(ln3083ReqService);
            context.put("tradeDataMap", ln3083ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3083.key, EsbEnum.TRADE_CODE_LN3083.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3083.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3083.key, EsbEnum.TRADE_CODE_LN3083.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3083RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3083RespService.class, Ln3083RespService.class);
            respService = ln3083RespService.getService();

            ln3083ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3083ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3083RespDto
                BeanUtils.copyProperties(respService, ln3083RespDto);
                List<cn.com.yusys.yusp.dto.client.esb.core.ln3083.resp.Lstytczfe> lstytczfeList = new ArrayList<>();
                Lstytczfe lstytczfe = Optional.ofNullable(respService.getLstytczfe()).orElse(new Lstytczfe());
                if (CollectionUtils.nonEmpty(lstytczfe.getRecordList())) {
                    List<Record> recordList = lstytczfe.getRecordList();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3083.resp.Lstytczfe target = new cn.com.yusys.yusp.dto.client.esb.core.ln3083.resp.Lstytczfe();
                        BeanUtils.copyProperties(record, target);
                        lstytczfeList.add(target);
                    }
                    ln3083RespDto.setLstytczfe(lstytczfeList);
                }
                ln3083ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3083ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3083ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3083ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3083.key, EsbEnum.TRADE_CODE_LN3083.value, e.getMessage());
            ln3083ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3083ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3083ResultDto.setData(ln3083RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3083.key, EsbEnum.TRADE_CODE_LN3083.value, JSON.toJSONString(ln3083ResultDto));
        return ln3083ResultDto;
    }
}
