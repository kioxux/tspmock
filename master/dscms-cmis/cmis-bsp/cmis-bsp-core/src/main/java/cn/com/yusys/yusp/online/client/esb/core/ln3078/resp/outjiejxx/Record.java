package cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.outjiejxx;

/**
 * 响应Service：贷款机构变更
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                '}';
    }
}
