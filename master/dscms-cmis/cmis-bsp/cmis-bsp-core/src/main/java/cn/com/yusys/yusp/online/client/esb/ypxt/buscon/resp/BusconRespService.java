package cn.com.yusys.yusp.online.client.esb.ypxt.buscon.resp;

/**
 * 响应Service：信贷业务与押品关联关系信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class BusconRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "BusconRespService{" +
                "service=" + service +
                '}';
    }
}
