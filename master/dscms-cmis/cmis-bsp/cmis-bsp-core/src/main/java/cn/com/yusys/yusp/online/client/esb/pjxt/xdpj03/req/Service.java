package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req;

/**
 * 请求Service：票据承兑签发审批请求
 */
public class Service {
	private String prcscd;//    交易码
	private String servtp;//    渠道
	private String servsq;//    渠道流水
	private String userid;//    柜员号
	private String brchno;//    部门号
	private String txdate;//    交易日期
	private String txtime;//    交易时间
	private String datasq;//    全局流水

    private String txCode;//交易码
    private String batchNo;//批次号
    private String protocolNo;//承兑协议编号
    private String transType;//处理码
    private String isdraftpool;//是否票据池编号
    private String yp_sj;//发票信息补录状态
    private List list;//list

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getTxdate() {
		return txdate;
	}

	public void setTxdate(String txdate) {
		this.txdate = txdate;
	}

	public String getTxtime() {
		return txtime;
	}

	public void setTxtime(String txtime) {
		this.txtime = txtime;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getTxCode() {
		return txCode;
	}

	public void setTxCode(String txCode) {
		this.txCode = txCode;
	}

	public String getBatchNo() {
		return batchNo;
	}

	public void setBatchNo(String batchNo) {
		this.batchNo = batchNo;
	}

	public String getProtocolNo() {
		return protocolNo;
	}

	public void setProtocolNo(String protocolNo) {
		this.protocolNo = protocolNo;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public String getIsdraftpool() {
		return isdraftpool;
	}

	public void setIsdraftpool(String isdraftpool) {
		this.isdraftpool = isdraftpool;
	}

	public String getYp_sj() {
		return yp_sj;
	}

	public void setYp_sj(String yp_sj) {
		this.yp_sj = yp_sj;
	}

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", txdate='" + txdate + '\'' +
				", txtime='" + txtime + '\'' +
				", datasq='" + datasq + '\'' +
				", txCode='" + txCode + '\'' +
				", batchNo='" + batchNo + '\'' +
				", protocolNo='" + protocolNo + '\'' +
				", transType='" + transType + '\'' +
				", isdraftpool='" + isdraftpool + '\'' +
				", yp_sj='" + yp_sj + '\'' +
				", list=" + list +
				'}';
	}
}
