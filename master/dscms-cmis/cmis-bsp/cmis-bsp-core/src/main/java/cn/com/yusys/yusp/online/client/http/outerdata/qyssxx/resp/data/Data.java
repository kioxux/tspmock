package cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Service：涉诉信息查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "civil")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Civil civil;//	民事案件
    @JsonProperty(value = "criminal")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Criminal criminal;//	刑事案件
    @JsonProperty(value = "administrative")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Administrative administrative;//	行政案件
    @JsonProperty(value = "implement")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Implement implement;//执行案件
    @JsonProperty(value = "bankrupt")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Bankrupt bankrupt;//	强制清算与破产案件
    @JsonProperty(value = "count")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Count count;
    /* 接口文档中没有定义但是返回报文存在的字段   开始 */
    @JsonProperty(value = "preservation")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Preservation preservation;
    /* 接口文档中没有定义但是返回报文存在的字段   结束 */

    public Civil getCivil() {
        return civil;
    }

    public void setCivil(Civil civil) {
        this.civil = civil;
    }

    public Criminal getCriminal() {
        return criminal;
    }

    public void setCriminal(Criminal criminal) {
        this.criminal = criminal;
    }

    public Administrative getAdministrative() {
        return administrative;
    }

    public void setAdministrative(Administrative administrative) {
        this.administrative = administrative;
    }

    public Implement getImplement() {
        return implement;
    }

    public void setImplement(Implement implement) {
        this.implement = implement;
    }

    public Bankrupt getBankrupt() {
        return bankrupt;
    }

    public void setBankrupt(Bankrupt bankrupt) {
        this.bankrupt = bankrupt;
    }

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }

    public Preservation getPreservation() {
        return preservation;
    }

    public void setPreservation(Preservation preservation) {
        this.preservation = preservation;
    }

    @Override
    public String toString() {
        return "Data{" +
                "civil=" + civil +
                ", criminal=" + criminal +
                ", administrative=" + administrative +
                ", implement=" + implement +
                ", bankrupt=" + bankrupt +
                ", count=" + count +
                ", preservation=" + preservation +
                '}';
    }
}
