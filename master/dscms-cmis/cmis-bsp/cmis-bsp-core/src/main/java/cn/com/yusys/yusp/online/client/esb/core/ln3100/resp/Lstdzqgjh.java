package cn.com.yusys.yusp.online.client.esb.core.ln3100.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款定制期供计划表
 *
 * @author chenyong
 * @version 1.0
 */
public class Lstdzqgjh {
    private BigDecimal hxijinee;//还息金额
    private String zwhkriqi;//最晚还款日
    private String absbkulx;//贸融ABS备款类型
    private String absgbfsh;//贸融ABS关闭方式

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getZwhkriqi() {
        return zwhkriqi;
    }

    public void setZwhkriqi(String zwhkriqi) {
        this.zwhkriqi = zwhkriqi;
    }

    public String getAbsbkulx() {
        return absbkulx;
    }

    public void setAbsbkulx(String absbkulx) {
        this.absbkulx = absbkulx;
    }

    public String getAbsgbfsh() {
        return absgbfsh;
    }

    public void setAbsgbfsh(String absgbfsh) {
        this.absgbfsh = absgbfsh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "hxijinee='" + hxijinee + '\'' +
                "zwhkriqi='" + zwhkriqi + '\'' +
                "absbkulx='" + absbkulx + '\'' +
                "absgbfsh='" + absgbfsh + '\'' +
                '}';
    }
}
