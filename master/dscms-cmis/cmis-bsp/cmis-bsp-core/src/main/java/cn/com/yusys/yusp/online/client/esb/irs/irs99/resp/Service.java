package cn.com.yusys.yusp.online.client.esb.irs.irs99.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 响应Service：债项评级
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日 下午1:22:06
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String prcscd;//交易码
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.BizMessageInfo BizMessageInfo;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.BizMessageInfo getBizMessageInfo() {
        return BizMessageInfo;
    }

    @JsonIgnore
    public void setBizMessageInfo(cn.com.yusys.yusp.online.client.esb.irs.common.BizMessageInfo bizMessageInfo) {
        BizMessageInfo = bizMessageInfo;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", prcscd='" + prcscd + '\'' +
                '}';
    }
}
