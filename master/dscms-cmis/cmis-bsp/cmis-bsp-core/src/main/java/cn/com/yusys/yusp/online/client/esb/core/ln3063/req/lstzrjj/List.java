package cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj;

/**
 * 请求Service：资产转让处理
 *
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3063.req.lstzrjj.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
