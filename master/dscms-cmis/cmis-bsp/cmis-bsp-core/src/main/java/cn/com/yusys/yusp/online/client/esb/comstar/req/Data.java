package cn.com.yusys.yusp.online.client.esb.comstar.req;

/**
 * 请求Dto：额度同步
 *
 * @author leehuang
 * @version 1.0
 */
public class Data {

    private java.util.List<List> list;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "list=" + list +
                '}';
    }
}
