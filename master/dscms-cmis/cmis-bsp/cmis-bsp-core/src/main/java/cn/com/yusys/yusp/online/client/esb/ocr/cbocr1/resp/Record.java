package cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 10:29
 * @since 2021/6/4 10:29
 */
public class Record {
    private String taskId;//返回结果：任务id

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "Record{" +
                "taskId='" + taskId + '\'' +
                '}';
    }
}
