package cn.com.yusys.yusp.web.server.biz.xdzc0016;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0016.req.Xdzc0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0016.resp.Xdzc0016RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0016.req.Xdzc0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0016.resp.Xdzc0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:购销合同查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0016:购销合同查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0016Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0016
     * 交易描述：购销合同查询
     *
     * @param xdzc0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("购销合同查询")
    @PostMapping("/xdzc0016")
    //@Idempotent({"xdzc016", "#xdzc0016ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0016RespDto xdzc0016(@Validated @RequestBody Xdzc0016ReqDto xdzc0016ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, JSON.toJSONString(xdzc0016ReqDto));
        Xdzc0016DataReqDto xdzc0016DataReqDto = new Xdzc0016DataReqDto();// 请求Data： 购销合同查询
        Xdzc0016DataRespDto xdzc0016DataRespDto = new Xdzc0016DataRespDto();// 响应Data：购销合同查询
        Xdzc0016RespDto xdzc0016RespDto = new Xdzc0016RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0016.req.Data reqData = null; // 请求Data：购销合同查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0016.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0016.resp.Data();// 响应Data：购销合同查询
        try {
            // 从 xdzc0016ReqDto获取 reqData
            reqData = xdzc0016ReqDto.getData();
            // 将 reqData 拷贝到xdzc0016DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0016DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, JSON.toJSONString(xdzc0016DataReqDto));
            ResultDto<Xdzc0016DataRespDto> xdzc0016DataResultDto = dscmsBizZcClientService.xdzc0016(xdzc0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, JSON.toJSONString(xdzc0016DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0016RespDto.setErorcd(Optional.ofNullable(xdzc0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0016RespDto.setErortx(Optional.ofNullable(xdzc0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0016DataResultDto.getCode())) {
                xdzc0016DataRespDto = xdzc0016DataResultDto.getData();
                xdzc0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0016DataRespDto, respData);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, e.getMessage());
            xdzc0016RespDto.setErorcd(e.getErrorCode()); // 9999
            xdzc0016RespDto.setErortx(e.getMessage());// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, e.getMessage());
            xdzc0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0016RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0016RespDto.setDatasq(servsq);

        xdzc0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0016.key, DscmsEnum.TRADE_CODE_XDZC0016.value, JSON.toJSONString(xdzc0016RespDto));
        return xdzc0016RespDto;
    }
}
