package cn.com.yusys.yusp.web.server.cus.xdkh0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0023.req.Xdkh0023ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0023.resp.Xdkh0023RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0023.req.Xdkh0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0023.resp.Xdkh0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:还款试算计划查询日期
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDKH0023:还款试算计划查询日期")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0023Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdkh0023
     * 交易描述：还款试算计划查询日期
     *
     * @param xdkh0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款试算计划查询日期")
    @PostMapping("/xdkh0023")
    //@Idempotent({"xdcakh0023", "#xdkh0023ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0023RespDto xdkh0023(@Validated @RequestBody Xdkh0023ReqDto xdkh0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, JSON.toJSONString(xdkh0023ReqDto));
        Xdkh0023DataReqDto xdkh0023DataReqDto = new Xdkh0023DataReqDto();// 请求Data： 还款试算计划查询日期
        Xdkh0023DataRespDto xdkh0023DataRespDto = new Xdkh0023DataRespDto();// 响应Data：还款试算计划查询日期
        Xdkh0023RespDto xdkh0023RespDto = new Xdkh0023RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0023.req.Data reqData = null; // 请求Data：还款试算计划查询日期
        cn.com.yusys.yusp.dto.server.cus.xdkh0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0023.resp.Data();// 响应Data：还款试算计划查询日期
        try {
            // 从 xdkh0023ReqDto获取 reqData
            reqData = xdkh0023ReqDto.getData();
            // 将 reqData 拷贝到xdkh0023DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0023DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, JSON.toJSONString(xdkh0023DataReqDto));
            ResultDto<Xdkh0023DataRespDto> xdkh0023DataResultDto = dscmsBizXwClientService.xdkh0023(xdkh0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, JSON.toJSONString(xdkh0023DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0023RespDto.setErorcd(Optional.ofNullable(xdkh0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0023RespDto.setErortx(Optional.ofNullable(xdkh0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0023DataResultDto.getCode())) {
                xdkh0023DataRespDto = xdkh0023DataResultDto.getData();
                xdkh0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0023DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, e.getMessage());
            xdkh0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0023RespDto.setDatasq(servsq);

        xdkh0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0023.key, DscmsEnum.TRADE_CODE_XDKH0023.value, JSON.toJSONString(xdkh0023RespDto));
        return xdkh0023RespDto;
    }
}
