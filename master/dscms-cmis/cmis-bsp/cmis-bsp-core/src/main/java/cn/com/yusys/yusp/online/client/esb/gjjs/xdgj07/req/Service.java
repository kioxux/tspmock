package cn.com.yusys.yusp.online.client.esb.gjjs.xdgj07.req;

import java.math.BigDecimal;

/**
 * 请求Service：信贷发送台账信息
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String type;//业务类型
    private String op_flag;//操作类型
    private String bill_no;//借据号
    private String prd_name;//产品名称
    private String biz_type;//产品编号
    private String cont_no;//合同编号
    private String cus_id;//客户代码
    private String cus_name;//客户名称
    private BigDecimal loan_amount;//贷款金额
    private String cur_type;//币种
    private BigDecimal loan_balance;//贷款余额
    private String loan_start_date;//贷款起始日
    private String loan_end_date;//贷款到期日
    private BigDecimal ruling_ir;//基准利率年
    private BigDecimal reality_ir_y;//执行利率年
    private String floating_rate;//利率浮动值
    private String repayment_account;//贷款还款账号
    private String enter_account;//贷款发放账号
    private String account_status;//台账状态
    private String input_id;//登记人
    private String fina_br_id;//账务机构
    private String manager_id;//责任人
    private String manager_br_id;//责任机构
    private String cmis_rate;//信贷牌价

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOp_flag() {
        return op_flag;
    }

    public void setOp_flag(String op_flag) {
        this.op_flag = op_flag;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public BigDecimal getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(BigDecimal loan_amount) {
        this.loan_amount = loan_amount;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getLoan_balance() {
        return loan_balance;
    }

    public void setLoan_balance(BigDecimal loan_balance) {
        this.loan_balance = loan_balance;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public BigDecimal getRuling_ir() {
        return ruling_ir;
    }

    public void setRuling_ir(BigDecimal ruling_ir) {
        this.ruling_ir = ruling_ir;
    }

    public BigDecimal getReality_ir_y() {
        return reality_ir_y;
    }

    public void setReality_ir_y(BigDecimal reality_ir_y) {
        this.reality_ir_y = reality_ir_y;
    }

    public String getFloating_rate() {
        return floating_rate;
    }

    public void setFloating_rate(String floating_rate) {
        this.floating_rate = floating_rate;
    }

    public String getRepayment_account() {
        return repayment_account;
    }

    public void setRepayment_account(String repayment_account) {
        this.repayment_account = repayment_account;
    }

    public String getEnter_account() {
        return enter_account;
    }

    public void setEnter_account(String enter_account) {
        this.enter_account = enter_account;
    }

    public String getAccount_status() {
        return account_status;
    }

    public void setAccount_status(String account_status) {
        this.account_status = account_status;
    }

    public String getInput_id() {
        return input_id;
    }

    public void setInput_id(String input_id) {
        this.input_id = input_id;
    }

    public String getFina_br_id() {
        return fina_br_id;
    }

    public void setFina_br_id(String fina_br_id) {
        this.fina_br_id = fina_br_id;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getCmis_rate() {
        return cmis_rate;
    }

    public void setCmis_rate(String cmis_rate) {
        this.cmis_rate = cmis_rate;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", type='" + type + '\'' +
                ", op_flag='" + op_flag + '\'' +
                ", bill_no='" + bill_no + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", biz_type='" + biz_type + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", loan_amount=" + loan_amount +
                ", cur_type='" + cur_type + '\'' +
                ", loan_balance=" + loan_balance +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", ruling_ir=" + ruling_ir +
                ", reality_ir_y=" + reality_ir_y +
                ", floating_rate='" + floating_rate + '\'' +
                ", repayment_account='" + repayment_account + '\'' +
                ", enter_account='" + enter_account + '\'' +
                ", account_status='" + account_status + '\'' +
                ", input_id='" + input_id + '\'' +
                ", fina_br_id='" + fina_br_id + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", cmis_rate='" + cmis_rate + '\'' +
                '}';
    }
}