package cn.com.yusys.yusp.web.server.biz.xddh0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0016.req.Xddh0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0016.resp.Xddh0016RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.dto.server.xddh0016.req.Xddh0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0016.resp.Xddh0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提前还款申请
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0016:提前还款申请")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0016Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0016
     * 交易描述：提前还款申请
     *
     * @param xddh0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前还款申请")
    @PostMapping("/xddh0016")
    //@Idempotent({"xddh0016", "#xddh0016ReqDto.datasq"})
    protected @ResponseBody
    Xddh0016RespDto xddh0016(@Validated @RequestBody Xddh0016ReqDto xddh0016ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, JSON.toJSONString(xddh0016ReqDto));
        Xddh0016DataReqDto xddh0016DataReqDto = new Xddh0016DataReqDto();// 请求Data： 提前还款申请
        Xddh0016DataRespDto xddh0016DataRespDto = new Xddh0016DataRespDto();// 响应Data：提前还款申请
        Xddh0016RespDto xddh0016RespDto = new Xddh0016RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddh0016.req.Data reqData = null; // 请求Data：提前还款申请
        cn.com.yusys.yusp.dto.server.biz.xddh0016.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0016.resp.Data();// 响应Data：提前还款申请

        try {
            // 从 xddh0016ReqDto获取 reqData
            reqData = xddh0016ReqDto.getData();
            // 将 reqData 拷贝到xddh0016DataReqDto
            BeanUtils.copyProperties(reqData, xddh0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, JSON.toJSONString(xddh0016DataReqDto));
            ResultDto<Xddh0016DataRespDto> xddh0016DataResultDto = dscmsBizDhClientService.xddh0016(xddh0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, JSON.toJSONString(xddh0016DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0016RespDto.setErorcd(Optional.ofNullable(xddh0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0016RespDto.setErortx(Optional.ofNullable(xddh0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0016DataResultDto.getCode())) {
                xddh0016DataRespDto = xddh0016DataResultDto.getData();
                xddh0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0016DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, e.getMessage());
            xddh0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0016RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0016RespDto.setDatasq(servsq);

        xddh0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0016.key, DscmsEnum.TRADE_CODE_XDDH0016.value, JSON.toJSONString(xddh0016RespDto));
        return xddh0016RespDto;
    }
}
