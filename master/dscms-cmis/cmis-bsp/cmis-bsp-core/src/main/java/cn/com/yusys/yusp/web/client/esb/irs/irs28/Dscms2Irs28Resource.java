package cn.com.yusys.yusp.web.client.esb.irs.irs28;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.irs28.req.AssetDebt;
import cn.com.yusys.yusp.dto.client.esb.irs.irs28.req.Irs28ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs28.req.Loss;
import cn.com.yusys.yusp.dto.client.esb.irs.irs28.resp.Irs28RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.irs28.req.AssetDebt_ARRAY;
import cn.com.yusys.yusp.online.client.esb.irs.irs28.req.Irs28ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.irs28.req.Loss_ARRAY;
import cn.com.yusys.yusp.online.client.esb.irs.irs28.resp.Irs28RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(irs28)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Irs28Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Irs28Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：irs28
     * 交易描述：财务信息同步
     *
     * @param irs28ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("irs28:财务信息同步")
    @PostMapping("/irs28")
    protected @ResponseBody
    ResultDto<Irs28RespDto> irs28(@Validated @RequestBody Irs28ReqDto irs28ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value, JSON.toJSONString(irs28ReqDto));

        cn.com.yusys.yusp.online.client.esb.irs.irs28.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.irs28.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.irs28.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.irs28.resp.Service();

        Irs28ReqService irs28ReqService = new Irs28ReqService();
        Irs28RespService irs28RespService = new Irs28RespService();
        Irs28RespDto irs28RespDto = new Irs28RespDto();
        ResultDto<Irs28RespDto> irs28ResultDto = new ResultDto<Irs28RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将irs28ReqDto转换成reqService
            BeanUtils.copyProperties(irs28ReqDto, reqService);

            AssetDebt_ARRAY assetDebt_array = new AssetDebt_ARRAY();
            Loss_ARRAY loss_array = new Loss_ARRAY();
            if (CollectionUtils.nonEmpty(irs28ReqDto.getAssetDebt())) {
                List<AssetDebt> assetDebtList = irs28ReqDto.getAssetDebt();
                List<cn.com.yusys.yusp.online.client.esb.irs.irs28.req.assetdebt.Record> recordList = new ArrayList<>();
                for (AssetDebt assetDebtdto : assetDebtList) {
                    cn.com.yusys.yusp.online.client.esb.irs.irs28.req.assetdebt.Record record = new cn.com.yusys.yusp.online.client.esb.irs.irs28.req.assetdebt.Record();
                    BeanUtils.copyProperties(assetDebtdto, record);
                    recordList.add(record);
                }
                assetDebt_array.setRecord(recordList);
            }
            reqService.setAssetDebt_array(assetDebt_array);

            if (CollectionUtils.nonEmpty(irs28ReqDto.getLoss())) {
                List<Loss> lossList = irs28ReqDto.getLoss();
                List<cn.com.yusys.yusp.online.client.esb.irs.irs28.req.loss.Record> recordList = new ArrayList<>();
                for (Loss lossdto : lossList) {
                    cn.com.yusys.yusp.online.client.esb.irs.irs28.req.loss.Record record = new cn.com.yusys.yusp.online.client.esb.irs.irs28.req.loss.Record();
                    BeanUtils.copyProperties(lossdto, record);
                    recordList.add(record);
                }
                loss_array.setRecord(recordList);
            }
            reqService.setLoss_array(loss_array);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS28.key);//    交易码
            LocalDateTime now = LocalDateTime.now();
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_FLS.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            irs28ReqService.setService(reqService);
            // 将irs28ReqService转换成irs28ReqServiceMap
            Map irs28ReqServiceMap = beanMapUtil.beanToMap(irs28ReqService);
            context.put("tradeDataMap", irs28ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS28.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            irs28RespService = beanMapUtil.mapToBean(tradeDataMap, Irs28RespService.class, Irs28RespService.class);
            respService = irs28RespService.getService();

            irs28ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            irs28ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Irs28RespDto
                BeanUtils.copyProperties(respService, irs28RespDto);
                irs28ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                irs28ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                irs28ResultDto.setCode(EpbEnum.EPB099999.key);
                irs28ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value, e.getMessage());
            irs28ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            irs28ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        irs28ResultDto.setData(irs28RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value, JSON.toJSONString(irs28ResultDto));
        return irs28ResultDto;
    }
}
