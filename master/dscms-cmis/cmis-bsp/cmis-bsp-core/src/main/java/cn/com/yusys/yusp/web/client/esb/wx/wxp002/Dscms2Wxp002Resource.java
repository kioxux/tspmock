package cn.com.yusys.yusp.web.client.esb.wx.wxp002;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp002.req.Wxp002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp002.resp.Wxp002RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.wx.wxp002.req.Wxp002ReqService;
import cn.com.yusys.yusp.online.client.esb.wx.wxp002.resp.Wxp002RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用苏州征信系统的接口
 **/
@Api(tags = "BSP封装调用苏州征信系统的接口(wxp002)")
@RestController
@RequestMapping("/api/dscms2wx")
public class Dscms2Wxp002Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Wxp002Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 信贷将授信额度推送给移动端（处理码wxp002）
     *
     * @param wxp002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("wxp002:信贷将授信额度推送给移动端")
    @PostMapping("/wxp002")
    protected @ResponseBody
    ResultDto<Wxp002RespDto> wxp002(@Validated @RequestBody Wxp002ReqDto wxp002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP002.key, EsbEnum.TRADE_CODE_WXP002.value, JSON.toJSONString(wxp002ReqDto));
        cn.com.yusys.yusp.online.client.esb.wx.wxp002.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.wx.wxp002.req.Service();
        cn.com.yusys.yusp.online.client.esb.wx.wxp002.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.wx.wxp002.resp.Service();
        Wxp002ReqService wxp002ReqService = new Wxp002ReqService();
        Wxp002RespService wxp002RespService = new Wxp002RespService();
        Wxp002RespDto wxp002RespDto = new Wxp002RespDto();
        ResultDto<Wxp002RespDto> wxp002ResultDto = new ResultDto<Wxp002RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Wxp002ReqDto转换成reqService
            BeanUtils.copyProperties(wxp002ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_WXP002.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_SZZX.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_SZZX.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            wxp002ReqService.setService(reqService);
            // 将wxp002ReqService转换成wxp002ReqServiceMap
            Map wxp002ReqServiceMap = beanMapUtil.beanToMap(wxp002ReqService);
            context.put("tradeDataMap", wxp002ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP002.key, EsbEnum.TRADE_CODE_WXP002.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_WXP002.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP002.key, EsbEnum.TRADE_CODE_WXP002.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP002.key, EsbEnum.TRADE_CODE_WXP002.value);
            wxp002RespService = beanMapUtil.mapToBean(tradeDataMap, Wxp002RespService.class, Wxp002RespService.class);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP002.key, EsbEnum.TRADE_CODE_WXP002.value);
            respService = wxp002RespService.getService();
            //  将Wxp002RespDto封装到ResultDto<Wxp002RespDto>
            wxp002ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            wxp002ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Wxp002RespDto
                BeanUtils.copyProperties(respService, wxp002RespDto);
                wxp002ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                wxp002ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                wxp002ResultDto.setCode(EpbEnum.EPB099999.key);
                wxp002ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP002.key, EsbEnum.TRADE_CODE_WXP002.value, e.getMessage());
            wxp002ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            wxp002ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        wxp002ResultDto.setData(wxp002RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP002.key, EsbEnum.TRADE_CODE_WXP002.value, JSON.toJSONString(wxp002ResultDto));
        return wxp002ResultDto;
    }
}
