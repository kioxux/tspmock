package cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.req;

/**
 * 请求Service：获取该笔借据的最新五级分类以及数据日期
 */
public class Service {
	private String prcscd; // 处理码
	private String servtp; // 渠道
	private String servsq; // 渠道流水
	private String userid; // 柜员号
	private String brchno; // 部门号
    private String bill_no;//交易码

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getBill_no() {
		return bill_no;
	}

	public void setBill_no(String bill_no) {
		this.bill_no = bill_no;
	}

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", bill_no='" + bill_no + '\'' +
				'}';
	}
}
