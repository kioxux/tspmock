package cn.com.yusys.yusp.web.server.psp.xdqt0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.psp.xdqt0006.req.Xdqt0006ReqDto;
import cn.com.yusys.yusp.dto.server.psp.xdqt0006.resp.Xdqt0006RespDto;
import cn.com.yusys.yusp.dto.server.xdqt0006.resp.Xdqt0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsPspQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:当天跑P状态查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0006:当天跑P状态查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0006Resource.class);

    @Autowired
    private DscmsPspQtClientService dscmsPspQtClientService;

    /**
     * 交易码：xdqt0006
     * 交易描述：当天跑P状态查询
     *
     * @return
     * @throws Exception
     */
    @ApiOperation("当天跑P状态查询")
    @PostMapping("/xdqt0006")
    //@Idempotent({"xdcaqt0006", "#xdqt0006ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0006RespDto xdqt0006(@Validated @RequestBody Xdqt0006ReqDto xdqt0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, JSON.toJSONString(xdqt0006ReqDto));
        Xdqt0006DataRespDto xdqt0006DataRespDto = new Xdqt0006DataRespDto();// 响应Data：当天跑P状态查询
        cn.com.yusys.yusp.dto.server.psp.xdqt0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.psp.xdqt0006.resp.Data();// 响应Data：当天跑P状态查询
        Xdqt0006RespDto xdqt0006RespDto = new Xdqt0006RespDto();
        try {
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value);
            ResultDto<Xdqt0006DataRespDto> xdqt0006DataResultDto = dscmsPspQtClientService.xdqt0006();
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, JSON.toJSONString(xdqt0006DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0006RespDto.setErorcd(Optional.ofNullable(xdqt0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0006RespDto.setErortx(Optional.ofNullable(xdqt0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0006DataResultDto.getCode())) {
                xdqt0006DataRespDto = xdqt0006DataResultDto.getData();
                xdqt0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, e.getMessage());
            xdqt0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0006RespDto.setDatasq(servsq);

        xdqt0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0006.key, DscmsEnum.TRADE_CODE_XDQT0006.value, JSON.toJSONString(xdqt0006RespDto));
        return xdqt0006RespDto;
    }
}
