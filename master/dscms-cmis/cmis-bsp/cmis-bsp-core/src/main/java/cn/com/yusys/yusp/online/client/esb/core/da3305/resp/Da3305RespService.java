package cn.com.yusys.yusp.online.client.esb.core.da3305.resp;

/**
 * 响应Service：待变现抵债资产销账
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3305RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3305RespService{" +
                "service=" + service +
                '}';
    }
}
