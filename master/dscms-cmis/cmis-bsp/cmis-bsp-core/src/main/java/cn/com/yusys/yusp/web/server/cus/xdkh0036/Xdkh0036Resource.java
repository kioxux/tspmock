package cn.com.yusys.yusp.web.server.cus.xdkh0036;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0036.req.Xdkh0036ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0036.resp.Data;
import cn.com.yusys.yusp.dto.server.cus.xdkh0036.resp.Xdkh0036RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0036.req.Xdkh0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0036.resp.Xdkh0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类: 网银对公客户查询接口
 *
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDKH0036:网银对公客户查询接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0036Resource.class);

	@Autowired
	private DscmsCusClientService dscmsCusClientService;
    /**
     * 交易码：xdkh0036
     * 交易描述：网银对公客户查询接口
     *
     * @param xdkh0036ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("网银对公客户查询接口")
    @PostMapping("/xdkh0036")
    //@Idempotent({"xdcakh0036", "#xdkh0036ReqDto.datasq"})
    protected @ResponseBody
	Xdkh0036RespDto xdkh0036(@Validated @RequestBody Xdkh0036ReqDto xdkh0036ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036ReqDto));
        Xdkh0036DataReqDto xdkh0036DataReqDto = new Xdkh0036DataReqDto();// 请求Data： 网银对公客户查询接口
        Xdkh0036DataRespDto xdkh0036DataRespDto = new Xdkh0036DataRespDto();// 响应Data：网银对公客户查询接口
        cn.com.yusys.yusp.dto.server.cus.xdkh0036.req.Data reqData = null; // 请求Data：网银对公客户查询接口
        Data respData = new Data();// 响应Data：网银对公客户查询接口
		Xdkh0036RespDto xdkh0036RespDto = new Xdkh0036RespDto();
		try {
            // 从 xdkh0036ReqDto获取 reqData
            reqData = xdkh0036ReqDto.getData();
            // 将 reqData 拷贝到xdkh0036DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0036DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036DataReqDto));
            ResultDto<Xdkh0036DataRespDto> xdkh0036DataResultDto = dscmsCusClientService.xdkh0036(xdkh0036DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0036RespDto.setErorcd(Optional.ofNullable(xdkh0036DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0036RespDto.setErortx(Optional.ofNullable(xdkh0036DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0036DataResultDto.getCode())) {
                xdkh0036DataRespDto = xdkh0036DataResultDto.getData();
                xdkh0036RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0036RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0036DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0036DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, e.getMessage());
            xdkh0036RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0036RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0036RespDto.setDatasq(servsq);

        xdkh0036RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0036.key, DscmsEnum.TRADE_CODE_XDKH0036.value, JSON.toJSONString(xdkh0036RespDto));
        return xdkh0036RespDto;
    }
}
