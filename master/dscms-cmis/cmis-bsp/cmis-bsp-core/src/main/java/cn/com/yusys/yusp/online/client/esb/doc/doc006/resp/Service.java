package cn.com.yusys.yusp.online.client.esb.doc.doc006.resp;

/**
 * 响应Service：申请出库
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String rptem1;//应答备用字段1
    private String rptem2;//应答备用字段2
    private String rptem3;//应答备用字段3
    private String rptem4;//应答备用字段4

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getRptem1() {
        return rptem1;
    }

    public void setRptem1(String rptem1) {
        this.rptem1 = rptem1;
    }

    public String getRptem2() {
        return rptem2;
    }

    public void setRptem2(String rptem2) {
        this.rptem2 = rptem2;
    }

    public String getRptem3() {
        return rptem3;
    }

    public void setRptem3(String rptem3) {
        this.rptem3 = rptem3;
    }

    public String getRptem4() {
        return rptem4;
    }

    public void setRptem4(String rptem4) {
        this.rptem4 = rptem4;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", rptem1='" + rptem1 + '\'' +
                ", rptem2='" + rptem2 + '\'' +
                ", rptem3='" + rptem3 + '\'' +
                ", rptem4='" + rptem4 + '\'' +
                '}';
    }
}
