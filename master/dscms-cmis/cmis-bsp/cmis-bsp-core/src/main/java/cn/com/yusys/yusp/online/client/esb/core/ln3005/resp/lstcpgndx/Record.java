package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpgndx;

/**
 * 响应Service：贷款产品功能对象
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String gndxiang;//功能对象
    private String dxmingch;//对象名称
    private String bzhguize;//币种规则
    private String zhidbizh;//指定币种

    public String getGndxiang() {
        return gndxiang;
    }

    public void setGndxiang(String gndxiang) {
        this.gndxiang = gndxiang;
    }

    public String getDxmingch() {
        return dxmingch;
    }

    public void setDxmingch(String dxmingch) {
        this.dxmingch = dxmingch;
    }

    public String getBzhguize() {
        return bzhguize;
    }

    public void setBzhguize(String bzhguize) {
        this.bzhguize = bzhguize;
    }

    public String getZhidbizh() {
        return zhidbizh;
    }

    public void setZhidbizh(String zhidbizh) {
        this.zhidbizh = zhidbizh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "gndxiang='" + gndxiang + '\'' +
                "dxmingch='" + dxmingch + '\'' +
                "bzhguize='" + bzhguize + '\'' +
                "zhidbizh='" + zhidbizh + '\'' +
                '}';
    }
}
