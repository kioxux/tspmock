package cn.com.yusys.yusp.web.client.esb.rircp.fbxw01;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw01.req.Fbxw01ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw01.resp.Fbxw01RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw01Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw01Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB信贷查询地方征信接口（处理码fbxw01）
     *
     * @param fbxw01ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw01:ESB信贷查询地方征信接口")
    @PostMapping("/fbxw01")
    protected @ResponseBody
    ResultDto<Fbxw01RespDto> fbxw01(@Validated @RequestBody Fbxw01ReqDto fbxw01ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value, JSON.toJSONString(fbxw01ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw01.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw01.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw01.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw01.resp.Service();
        Fbxw01ReqService fbxw01ReqService = new Fbxw01ReqService();
        Fbxw01RespService fbxw01RespService = new Fbxw01RespService();
        Fbxw01RespDto fbxw01RespDto = new Fbxw01RespDto();
        ResultDto<Fbxw01RespDto> fbxw01ResultDto = new ResultDto<Fbxw01RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw01ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw01ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW01.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw01ReqService.setService(reqService);
            // 将fbxw01ReqService转换成fbxw01ReqServiceMap
            Map fbxw01ReqServiceMap = beanMapUtil.beanToMap(fbxw01ReqService);
            context.put("tradeDataMap", fbxw01ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW01.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw01RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw01RespService.class, Fbxw01RespService.class);
            respService = fbxw01RespService.getService();

            //  将Fbxw01RespDto封装到ResultDto<Fbxw01RespDto>
            fbxw01ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw01ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw01RespDto
                BeanUtils.copyProperties(respService, fbxw01RespDto);
                fbxw01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw01ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw01ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw01ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value, e.getMessage());
            fbxw01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw01ResultDto.setData(fbxw01RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW01.key, EsbEnum.TRADE_CODE_FBXW01.value, JSON.toJSONString(fbxw01ResultDto));
        return fbxw01ResultDto;
    }
}
