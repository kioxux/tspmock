package cn.com.yusys.yusp.web.client.esb.doc.doc004;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.doc.doc004.req.Doc004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc004.resp.Doc004RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.doc.doc004.req.Doc004ReqService;
import cn.com.yusys.yusp.online.client.esb.doc.doc004.resp.Doc004RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(doc004)")
@RestController
@RequestMapping("/api/dscms2doc")
public class Dscms2Doc004Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Doc004Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 调阅待出库查询（处理码doc004）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("doc004:调阅待出库查询")
    @PostMapping("/doc004")
    protected @ResponseBody
    ResultDto<Doc004RespDto> doc004(@Validated @RequestBody Doc004ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC004.key, EsbEnum.TRADE_CODE_DOC004.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.doc.doc004.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.doc.doc004.req.Service();
        cn.com.yusys.yusp.online.client.esb.doc.doc004.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.doc.doc004.resp.Service();
        Doc004ReqService doc004ReqService = new Doc004ReqService();
        Doc004RespService doc004RespService = new Doc004RespService();
        Doc004RespDto doc004RespDto = new Doc004RespDto();
        ResultDto<Doc004RespDto> doc004ResultDto = new ResultDto<Doc004RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Doc004ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DOC004.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            doc004ReqService.setService(reqService);
            // 将doc004ReqService转换成doc004ReqServiceMap
            Map doc004ReqServiceMap = beanMapUtil.beanToMap(doc004ReqService);
            context.put("tradeDataMap", doc004ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC004.key, EsbEnum.TRADE_CODE_DOC004.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DOC004.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC004.key, EsbEnum.TRADE_CODE_DOC004.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            doc004RespService = beanMapUtil.mapToBean(tradeDataMap, Doc004RespService.class, Doc004RespService.class);
            respService = doc004RespService.getService();

            //  将Doc004RespDto封装到ResultDto<Doc004RespDto>
            doc004ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            doc004ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Doc004RespDto
                BeanUtils.copyProperties(respService, doc004RespDto);
                doc004ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                doc004ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                doc004ResultDto.setCode(EpbEnum.EPB099999.key);
                doc004ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC004.key, EsbEnum.TRADE_CODE_DOC004.value, e.getMessage());
            doc004ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            doc004ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        doc004ResultDto.setData(doc004RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC004.key, EsbEnum.TRADE_CODE_DOC004.value, JSON.toJSONString(doc004ResultDto));
        return doc004ResultDto;
    }
}
