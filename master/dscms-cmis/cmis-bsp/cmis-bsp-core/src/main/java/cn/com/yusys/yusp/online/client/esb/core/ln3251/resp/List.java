package cn.com.yusys.yusp.online.client.esb.core.ln3251.resp;

/**
 * 贷款自动追缴明细查询
 *
 * @author 王玉坤
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
