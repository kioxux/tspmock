package cn.com.yusys.yusp.web.server.lmt.cmislmt0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0001.req.CmisLmt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0001.resp.CmisLmt0001RespDto;
import cn.com.yusys.yusp.dto.server.lmt.cmislmt0001.req.CmisLmt0001DataReqDto;
import cn.com.yusys.yusp.dto.server.lmt.cmislmt0001.resp.CmisLmt0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:单一客户额度同步
 *
 * @author dumingdi
 * @version 1.0
 */
@Api(tags = "CMISLMT0001:单一客户额度同步")
@RestController
@RequestMapping("/api/dscms")
public class CmisLmt0001DataResource {
    private static final Logger logger = LoggerFactory.getLogger(CmisLmt0001DataResource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：cmislmt0001
     * 交易描述：单一客户额度同步
     *
     * @param cmisLmt0001ReqDtoBsp
     * @return
     * @throws Exception
     */
    @ApiOperation("单一客户额度同步")
    @PostMapping("/cmislmt0001")
    //  @Idempotent({"cmislmt0001", "#cmisLmt0001ReqDtoBsp.datasq"})
    protected @ResponseBody
    CmisLmt0001DataRespDto cmisLmt0001(@Validated @RequestBody CmisLmt0001DataReqDto cmisLmt0001ReqDtoBsp) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0001.key, DscmsEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(cmisLmt0001ReqDtoBsp));
        CmisLmt0001ReqDto cmisLmt0001DataReqDto = new CmisLmt0001ReqDto();// 请求Data： 单一客户额度同步
        CmisLmt0001RespDto cmisLmt0001DataRespDto = new CmisLmt0001RespDto();// 响应Data：单一客户额度同步

        CmisLmt0001DataRespDto cmisLmt0001RespDtoBsp = new CmisLmt0001DataRespDto();

        cn.com.yusys.yusp.dto.server.lmt.cmislmt0001.req.Data reqData = null; // 请求Data：单一客户额度同步
        cn.com.yusys.yusp.dto.server.lmt.cmislmt0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.cmislmt0001.resp.Data();// 响应Data：单一客户额度同步

        try {
            // 从 cmisLmt0001ReqDto获取 reqData
            reqData = cmisLmt0001ReqDtoBsp.getData();
            // 将 reqData 拷贝到cmisLmt0001DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0001DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0001.key, DscmsEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(cmisLmt0001DataReqDto));
            ResultDto<CmisLmt0001RespDto> cmisLmt0001DataResultDto = cmisLmtClientService.cmisLmt0001(cmisLmt0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0001.key, DscmsEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(cmisLmt0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            cmisLmt0001RespDtoBsp.setErorcd(Optional.ofNullable(cmisLmt0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            cmisLmt0001RespDtoBsp.setErortx(Optional.ofNullable(cmisLmt0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0001DataResultDto.getCode())) {
                cmisLmt0001DataRespDto = cmisLmt0001DataResultDto.getData();
                cmisLmt0001RespDtoBsp.setErorcd(SuccessEnum.SUCCESS.key);
                cmisLmt0001RespDtoBsp.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0001.key, DscmsEnum.TRADE_CODE_CMISLMT0001.value, e.getMessage());
            cmisLmt0001RespDtoBsp.setErorcd(EpbEnum.EPB099999.key); // 9999
            cmisLmt0001RespDtoBsp.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        cmisLmt0001RespDtoBsp.setDatasq(servsq);

        cmisLmt0001RespDtoBsp.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISLMT0001.key, DscmsEnum.TRADE_CODE_CMISLMT0001.value, JSON.toJSONString(cmisLmt0001RespDtoBsp));
        return cmisLmt0001RespDtoBsp;
    }
}