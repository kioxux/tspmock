package cn.com.yusys.yusp.web.server.lmt.xded0023;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0023.req.CmisLmt0023ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023LmtListRespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0023.resp.CmisLmt0023RespDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0023.req.Xded0023ReqDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0023.resp.LmtList;
import cn.com.yusys.yusp.dto.server.lmt.xded0023.resp.Xded0023RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:个人额度查询（新微贷）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDED0023:个人额度查询（新微贷）")
@RestController
@RequestMapping("/api/dscms")
public class Xded0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xded0023Resource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：cmisLmt0019
     * 交易描述：个人额度查询（新微贷）
     *
     * @param xded0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("个人额度查询（新微贷）")
    @PostMapping("/xded0023")
    //  @Idempotent({"xded0023", "#xded0023ReqDto.datasq"})
    protected @ResponseBody
    Xded0023RespDto cmisLmt0023(@Validated @RequestBody Xded0023ReqDto xded0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0023.key, DscmsEnum.TRADE_CODE_XDED0023.value, JSON.toJSONString(xded0023ReqDto));
        CmisLmt0023ReqDto cmisLmt0023ReqDto = new CmisLmt0023ReqDto();// 请求Data： 个人额度查询（新微贷）
        CmisLmt0023RespDto cmisLmt0023RespDto = new CmisLmt0023RespDto();// 响应Data：个人额度查询（新微贷）

        Xded0023RespDto xded0023RespDto = new Xded0023RespDto();

        cn.com.yusys.yusp.dto.server.lmt.xded0023.req.Data reqData = null; // 请求Data：个人额度查询（新微贷）
        cn.com.yusys.yusp.dto.server.lmt.xded0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.xded0023.resp.Data();// 响应Data：个人额度查询（新微贷）

        try {
            // 从 Xded0023Dto获取 reqData
            reqData = xded0023ReqDto.getData();
            // 将 reqData 拷贝到cmisLmt0023DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0023ReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value, JSON.toJSONString(cmisLmt0023ReqDto));
            ResultDto<CmisLmt0023RespDto> cmisLmt0023RespDtoResultDto = cmisLmtClientService.cmislmt0023(cmisLmt0023ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0023.value, JSON.toJSONString(cmisLmt0023RespDtoResultDto));
            // 从返回值中获取响应码和响应消息
            xded0023RespDto.setErorcd(Optional.ofNullable(cmisLmt0023RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xded0023RespDto.setErortx(Optional.ofNullable(cmisLmt0023RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0023RespDtoResultDto.getCode())) {
                cmisLmt0023RespDto = cmisLmt0023RespDtoResultDto.getData();
                xded0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xded0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0023RespDto, respData);
                List<CmisLmt0023LmtListRespDto> lmtList = cmisLmt0023RespDto.getLmtList();
                if (CollectionUtils.nonEmpty(lmtList)) {
                    List<LmtList> collect = lmtList.parallelStream().map(lmt -> {
                        LmtList temp = new LmtList();
                        BeanUtils.copyProperties(lmt, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    respData.setLmtList(collect);
                }
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0023.key, DscmsEnum.TRADE_CODE_XDED0023.value, e.getMessage());
            xded0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xded0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xded0023RespDto.setDatasq(servsq);

        xded0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0023.key, DscmsEnum.TRADE_CODE_XDED0023.value, JSON.toJSONString(xded0023RespDto));
        return xded0023RespDto;
    }
}
