package cn.com.yusys.yusp.online.client.esb.circp.fb1146.resp;

/**
 * 响应Service：受托信息审核
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1146RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
