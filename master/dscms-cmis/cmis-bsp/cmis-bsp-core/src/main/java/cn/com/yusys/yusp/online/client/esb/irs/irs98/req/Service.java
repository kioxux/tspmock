package cn.com.yusys.yusp.online.client.esb.irs.irs98.req;

import cn.com.yusys.yusp.online.client.esb.irs.common.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Service：授信申请债项评级接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:21
 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间
    /**
     * 1、综合授信申请信息（LimitApplyInfo）
     */
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo LimitApplyInfo;
    /**
     * 2、分项额度信息LimitDetailsInfo
     */
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo LimitDetailsInfo;
    /**
     * 3、产品额度信息LimitProductInfo
     */
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo LimitProductInfo;
    /**
     * 4、合同信息BusinessContractInfo
     */
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfo BusinessContractInfo;
    /**
     * 5、非垫款借据信息	AccLoanInfo
     */
    @JsonProperty
    private cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfo AccLoanInfo;
    /**
     * 6、担保合同信息
     */
    @JsonProperty
    private GuaranteeContrctInfo GuaranteeContrctInfo;
    /**
     * 7、质押物信息	PledgeInfo
     */
    @JsonProperty
    private PledgeInfo PledgeInfo;
    /**
     * 8、抵押物信息	MortgageInfo
     */
    @JsonProperty
    private MortgageInfo MortgageInfo;
    /**
     * 9、保证人信息	AssurePersonInfo
     */
    @JsonProperty
    private AssurePersonInfo AssurePersonInfo;
    /**
     * 10、保证金信息	AssureAccInfo
     */
    @JsonProperty
    private AssureAccInfo AssureAccInfo;
    /**
     * 11、客户信息	CustomerInfo
     */
    @JsonProperty
    private CustomerInfo CustomerInfo;
    /**
     * 12、交易对手信息	DealCustomerInfo
     */
    @JsonProperty
    private DealCustomerInfo DealCustomerInfo;
    /**
     * 13、授信分项额度与抵质押、保证人关联信息	LimitPleMortInfo
     */
    @JsonProperty
    private LimitPleMortInfo LimitPleMortInfo;
    /**
     * 14、担保合同与合同关联信息	BusinessAssureInfo
     */
    @JsonProperty
    private BusinessAssureInfo BusinessAssureInfo;
    /**
     * 15、担保合同与抵质押、保证人关联信息
     */
    @JsonProperty
    private GuaranteePleMortInfo GuaranteePleMortInfo;
    /**
     * 16、最高额授信协议信息	UpApplyInfo
     */
    @JsonProperty
    private UpApplyInfo UpApplyInfo;
    /**
     * 17、汇率信息	CurInfo
     */
    @JsonProperty
    private CurInfo CurInfo;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo getLimitApplyInfo() {
        return LimitApplyInfo;
    }

    @JsonIgnore
    public void setLimitApplyInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo limitApplyInfo) {
        LimitApplyInfo = limitApplyInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo getLimitDetailsInfo() {
        return LimitDetailsInfo;
    }

    @JsonIgnore
    public void setLimitDetailsInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo limitDetailsInfo) {
        LimitDetailsInfo = limitDetailsInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo getLimitProductInfo() {
        return LimitProductInfo;
    }

    @JsonIgnore
    public void setLimitProductInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo limitProductInfo) {
        LimitProductInfo = limitProductInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfo getBusinessContractInfo() {
        return BusinessContractInfo;
    }

    @JsonIgnore
    public void setBusinessContractInfo(cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfo businessContractInfo) {
        BusinessContractInfo = businessContractInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfo getAccLoanInfo() {
        return AccLoanInfo;
    }

    @JsonIgnore
    public void setAccLoanInfo(cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfo accLoanInfo) {
        AccLoanInfo = accLoanInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteeContrctInfo getGuaranteeContrctInfo() {
        return GuaranteeContrctInfo;
    }

    @JsonIgnore
    public void setGuaranteeContrctInfo(cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteeContrctInfo guaranteeContrctInfo) {
        GuaranteeContrctInfo = guaranteeContrctInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo getPledgeInfo() {
        return PledgeInfo;
    }

    @JsonIgnore
    public void setPledgeInfo(cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo pledgeInfo) {
        PledgeInfo = pledgeInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo getMortgageInfo() {
        return MortgageInfo;
    }

    @JsonIgnore
    public void setMortgageInfo(cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo mortgageInfo) {
        MortgageInfo = mortgageInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo getAssurePersonInfo() {
        return AssurePersonInfo;
    }

    @JsonIgnore
    public void setAssurePersonInfo(cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo assurePersonInfo) {
        AssurePersonInfo = assurePersonInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfo getAssureAccInfo() {
        return AssureAccInfo;
    }

    @JsonIgnore
    public void setAssureAccInfo(cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfo assureAccInfo) {
        AssureAccInfo = assureAccInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo getCustomerInfo() {
        return CustomerInfo;
    }

    @JsonIgnore
    public void setCustomerInfo(cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo customerInfo) {
        CustomerInfo = customerInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.DealCustomerInfo getDealCustomerInfo() {
        return DealCustomerInfo;
    }

    @JsonIgnore
    public void setDealCustomerInfo(cn.com.yusys.yusp.online.client.esb.irs.common.DealCustomerInfo dealCustomerInfo) {
        DealCustomerInfo = dealCustomerInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo getLimitPleMortInfo() {
        return LimitPleMortInfo;
    }

    @JsonIgnore
    public void setLimitPleMortInfo(cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo limitPleMortInfo) {
        LimitPleMortInfo = limitPleMortInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.BusinessAssureInfo getBusinessAssureInfo() {
        return BusinessAssureInfo;
    }

    @JsonIgnore
    public void setBusinessAssureInfo(cn.com.yusys.yusp.online.client.esb.irs.common.BusinessAssureInfo businessAssureInfo) {
        BusinessAssureInfo = businessAssureInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteePleMortInfo getGuaranteePleMortInfo() {
        return GuaranteePleMortInfo;
    }

    @JsonIgnore
    public void setGuaranteePleMortInfo(cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteePleMortInfo guaranteePleMortInfo) {
        GuaranteePleMortInfo = guaranteePleMortInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.UpApplyInfo getUpApplyInfo() {
        return UpApplyInfo;
    }

    @JsonIgnore
    public void setUpApplyInfo(cn.com.yusys.yusp.online.client.esb.irs.common.UpApplyInfo upApplyInfo) {
        UpApplyInfo = upApplyInfo;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo getCurInfo() {
        return CurInfo;
    }

    @JsonIgnore
    public void setCurInfo(cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo curInfo) {
        CurInfo = curInfo;
    }

    @Override
    @JsonIgnore
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", LimitApplyInfo=" + LimitApplyInfo +
                ", LimitDetailsInfo=" + LimitDetailsInfo +
                ", LimitProductInfo=" + LimitProductInfo +
                ", BusinessContractInfo=" + BusinessContractInfo +
                ", AccLoanInfo=" + AccLoanInfo +
                ", GuaranteeContrctInfo=" + GuaranteeContrctInfo +
                ", PledgeInfo=" + PledgeInfo +
                ", MortgageInfo=" + MortgageInfo +
                ", AssurePersonInfo=" + AssurePersonInfo +
                ", AssureAccInfo=" + AssureAccInfo +
                ", CustomerInfo=" + CustomerInfo +
                ", DealCustomerInfo=" + DealCustomerInfo +
                ", LimitPleMortInfo=" + LimitPleMortInfo +
                ", BusinessAssureInfo=" + BusinessAssureInfo +
                ", GuaranteePleMortInfo=" + GuaranteePleMortInfo +
                ", UpApplyInfo=" + UpApplyInfo +
                ", CurInfo=" + CurInfo +
                '}';
    }
}
