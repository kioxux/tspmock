package cn.com.yusys.yusp.web.server.biz.xdht0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0005.req.Xdht0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0005.resp.Xdht0005RespDto;
import cn.com.yusys.yusp.dto.server.xdht0005.req.Xdht0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0005.resp.Xdht0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同面签信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0005:合同面签信息列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0005Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0005
     * 交易描述：合同面签信息列表查询
     *
     * @param xdht0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdht0005:合同面签信息列表查询")
    @PostMapping("/xdht0005")
    //@Idempotent({"xdcaht0005", "#xdht0005ReqDto.datasq"})
    protected @ResponseBody
    Xdht0005RespDto xdht0005(@Validated @RequestBody Xdht0005ReqDto xdht0005ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0005ReqDto));
        Xdht0005DataReqDto xdht0005DataReqDto = new Xdht0005DataReqDto();// 请求Data： 合同面签信息列表查询
        Xdht0005DataRespDto xdht0005DataRespDto = new Xdht0005DataRespDto();// 响应Data：合同面签信息列表查询
        Xdht0005RespDto xdht0005RespDto = new Xdht0005RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0005.req.Data reqData = null; // 请求Data：合同面签信息列表查询
        cn.com.yusys.yusp.dto.server.biz.xdht0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0005.resp.Data();// 响应Data：合同面签信息列表查询
        try {
            // 从 xdht0005ReqDto获取 reqData
            reqData = xdht0005ReqDto.getData();
            // 将 reqData 拷贝到xdht0005DataReqDto
            BeanUtils.copyProperties(reqData, xdht0005DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0005ReqDto));
            ResultDto<Xdht0005DataRespDto> xdht0005DataResultDto = dscmsBizHtClientService.xdht0005(xdht0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0005DataResultDto));

            // 从返回值中获取响应码和响应消息
            xdht0005RespDto.setErorcd(Optional.ofNullable(xdht0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0005RespDto.setErortx(Optional.ofNullable(xdht0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdht0005RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0005DataResultDto.getCode())) {
                xdht0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0005DataRespDto = xdht0005DataResultDto.getData();
                // 将 xdht0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, e.getMessage());
            xdht0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0005RespDto.setDatasq(servsq);
        xdht0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0005RespDto));
        return xdht0005RespDto;
    }
}
