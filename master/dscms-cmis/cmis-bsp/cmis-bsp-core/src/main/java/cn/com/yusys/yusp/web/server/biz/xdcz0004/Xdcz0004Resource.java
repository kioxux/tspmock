package cn.com.yusys.yusp.web.server.biz.xdcz0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0004.req.Xdcz0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0004.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0004.resp.Xdcz0004RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0004.req.Xdcz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0004.resp.Xdcz0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:承兑签发审批结果综合服务
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0004:承兑签发审批结果综合服务")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.biz.xdcz0004.Xdcz0004Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0004
     * 交易描述：承兑签发审批结果综合服务
     *
     * @param xdcz0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("承兑签发审批结果综合服务")
    @PostMapping("/xdcz0004")
   //@Idempotent({"xdcz0004", "#xdcz0004ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0004RespDto xdcz0004(@Validated @RequestBody Xdcz0004ReqDto xdcz0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, JSON.toJSONString(xdcz0004ReqDto));
        Xdcz0004DataReqDto xdcz0004DataReqDto = new Xdcz0004DataReqDto();//
        Xdcz0004DataRespDto xdcz0004DataRespDto = new Xdcz0004DataRespDto();//

        cn.com.yusys.yusp.dto.server.biz.xdcz0004.req.Data reqData = null; //
        cn.com.yusys.yusp.dto.server.biz.xdcz0004.resp.Data respData = new Data();//

        Xdcz0004RespDto xdcz0004RespDto = new Xdcz0004RespDto();
        try {
            // 从 xdcz0004ReqDto获取 reqData
            reqData = xdcz0004ReqDto.getData();
            // 将 reqData 拷贝到xdcz0004DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0004DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, JSON.toJSONString(xdcz0004DataReqDto));
            ResultDto<Xdcz0004DataRespDto> xdcz0004DataResultDto = dscmsBizCzClientService.xdcz0004(xdcz0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, JSON.toJSONString(xdcz0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0004RespDto.setErorcd(Optional.ofNullable(xdcz0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0004RespDto.setErortx(Optional.ofNullable(xdcz0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0004DataResultDto.getCode())) {
                xdcz0004DataRespDto = xdcz0004DataResultDto.getData();
                xdcz0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, e.getMessage());
            xdcz0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0004RespDto.setDatasq(servsq);

        xdcz0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0004.key, DscmsEnum.TRADE_CODE_XDCZ0004.value, JSON.toJSONString(xdcz0004RespDto));
        return xdcz0004RespDto;
    }
}
