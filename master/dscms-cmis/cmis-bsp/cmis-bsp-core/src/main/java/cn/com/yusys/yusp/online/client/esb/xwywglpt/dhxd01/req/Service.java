package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.req;

/**
 * 请求Service：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 */
public class Service {

	private String prcscd;//处理码
	private String servtp;//渠道
	private String servsq;//渠道流水
	private String userid;//柜员号
	private String brchno;//部门号

    private String doc_name;//文件名称

    public String getDoc_name() {
        return doc_name;
    }

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", doc_name='" + doc_name + '\'' +
				'}';
	}
}
