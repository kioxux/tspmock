package cn.com.yusys.yusp.online.client.esb.core.da3302.req;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产处置
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //   全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String daikczbz;//业务操作标志
    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String yngyjigo;//营业机构
    private String kehuhaoo;//客户号
    private String kehuzwmc;//客户名
    private BigDecimal dbxdzzic;//待变现抵债资产
    private BigDecimal feiyjine;//费用金额
    private BigDecimal dcldzzic;//待处理抵债资产
    private String dzzcbxkx;//抵债资产变现款项账号
    private String dzzcbzxh;//抵债资产变现账号子序号
    private String fygzzhao;//费用挂账账号
    private String fygzzzxh;//费用挂账账号子序号
    private String dzzcztai;//抵债资产状态
    private String rzczfshi;//入账操作方式
    private BigDecimal buchjine;//补偿金额
    private String buczhhao;//补偿账号
    private String buczhzxh;//补偿账号子序号
    private String dzzcczzl;//抵债资产处置种类
    private String dzzcczwb;//抵债资产是否处置完毕标志

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getFeiyjine() {
        return feiyjine;
    }

    public void setFeiyjine(BigDecimal feiyjine) {
        this.feiyjine = feiyjine;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public String getDzzcbxkx() {
        return dzzcbxkx;
    }

    public void setDzzcbxkx(String dzzcbxkx) {
        this.dzzcbxkx = dzzcbxkx;
    }

    public String getDzzcbzxh() {
        return dzzcbzxh;
    }

    public void setDzzcbzxh(String dzzcbzxh) {
        this.dzzcbzxh = dzzcbzxh;
    }

    public String getFygzzhao() {
        return fygzzhao;
    }

    public void setFygzzhao(String fygzzhao) {
        this.fygzzhao = fygzzhao;
    }

    public String getFygzzzxh() {
        return fygzzzxh;
    }

    public void setFygzzzxh(String fygzzzxh) {
        this.fygzzzxh = fygzzzxh;
    }

    public String getDzzcztai() {
        return dzzcztai;
    }

    public void setDzzcztai(String dzzcztai) {
        this.dzzcztai = dzzcztai;
    }

    public String getRzczfshi() {
        return rzczfshi;
    }

    public void setRzczfshi(String rzczfshi) {
        this.rzczfshi = rzczfshi;
    }

    public BigDecimal getBuchjine() {
        return buchjine;
    }

    public void setBuchjine(BigDecimal buchjine) {
        this.buchjine = buchjine;
    }

    public String getBuczhhao() {
        return buczhhao;
    }

    public void setBuczhhao(String buczhhao) {
        this.buczhhao = buczhhao;
    }

    public String getBuczhzxh() {
        return buczhzxh;
    }

    public void setBuczhzxh(String buczhzxh) {
        this.buczhzxh = buczhzxh;
    }

    public String getDzzcczzl() {
        return dzzcczzl;
    }

    public void setDzzcczzl(String dzzcczzl) {
        this.dzzcczzl = dzzcczzl;
    }

    public String getDzzcczwb() {
        return dzzcczwb;
    }

    public void setDzzcczwb(String dzzcczwb) {
        this.dzzcczwb = dzzcczwb;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", dbxdzzic=" + dbxdzzic +
                ", feiyjine=" + feiyjine +
                ", dcldzzic=" + dcldzzic +
                ", dzzcbxkx='" + dzzcbxkx + '\'' +
                ", dzzcbzxh='" + dzzcbzxh + '\'' +
                ", fygzzhao='" + fygzzhao + '\'' +
                ", fygzzzxh='" + fygzzzxh + '\'' +
                ", dzzcztai='" + dzzcztai + '\'' +
                ", rzczfshi='" + rzczfshi + '\'' +
                ", buchjine=" + buchjine +
                ", buczhhao='" + buczhhao + '\'' +
                ", buczhzxh='" + buczhzxh + '\'' +
                ", dzzcczzl='" + dzzcczzl + '\'' +
                ", dzzcczwb='" + dzzcczwb + '\'' +
                '}';
    }
}
