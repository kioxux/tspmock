package cn.com.yusys.yusp.web.server.biz.xdtz0042;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0042.req.Xdtz0042ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0042.resp.Xdtz0042RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0042.req.Xdtz0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0042.resp.Xdtz0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:申请人在本行当前逾期贷款数量
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0042:申请人在本行当前逾期贷款数量")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0042Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0042Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0042
     * 交易描述：申请人在本行当前逾期贷款数量
     *
     * @param xdtz0042ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("申请人在本行当前逾期贷款数量")
    @PostMapping("/xdtz0042")
    //@Idempotent({"xdcatz0042", "#xdtz0042ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0042RespDto xdtz0042(@Validated @RequestBody Xdtz0042ReqDto xdtz0042ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042ReqDto));
        Xdtz0042DataReqDto xdtz0042DataReqDto = new Xdtz0042DataReqDto();// 请求Data： 申请人在本行当前逾期贷款数量
        Xdtz0042DataRespDto xdtz0042DataRespDto = new Xdtz0042DataRespDto();// 响应Data：申请人在本行当前逾期贷款数量
        Xdtz0042RespDto xdtz0042RespDto = new Xdtz0042RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0042.req.Data reqData = null; // 请求Data：申请人在本行当前逾期贷款数量
        cn.com.yusys.yusp.dto.server.biz.xdtz0042.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0042.resp.Data();// 响应Data：申请人在本行当前逾期贷款数量
        try {
            // 从 xdtz0042ReqDto获取 reqData
            reqData = xdtz0042ReqDto.getData();
            // 将 reqData 拷贝到xdtz0042DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0042DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042DataReqDto));
            ResultDto<Xdtz0042DataRespDto> xdtz0042DataResultDto = dscmsBizTzClientService.xdtz0042(xdtz0042DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042DataResultDto));
            // 从返回值中获取响应码和响应消息

            xdtz0042RespDto.setErorcd(Optional.ofNullable(xdtz0042DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0042RespDto.setErortx(Optional.ofNullable(xdtz0042DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0042DataResultDto.getCode())) {
                xdtz0042DataRespDto = xdtz0042DataResultDto.getData();
                xdtz0042RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0042RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0042DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0042DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, e.getMessage());
            xdtz0042RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0042RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0042RespDto.setDatasq(servsq);

        xdtz0042RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0042.key, DscmsEnum.TRADE_CODE_XDTZ0042.value, JSON.toJSONString(xdtz0042RespDto));
        return xdtz0042RespDto;
    }
}
