package cn.com.yusys.yusp.online.client.esb.core.ln3031.req.lstdkhkfs;

import java.math.BigDecimal;

/**
 * 请求Service：贷款组合还款方式
 *
 * @author lihh
 * @version 1.0
 */
public class Record {

    private String huankfsh;//还款方式
    private String dechligz;//等额处理规则
    private String shengxrq;//生效日期
    private String daoqriqi;//到期日期
    private Integer leijqjsh;//累进区间期数
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal meiqhbje;//每期还本金额
    private BigDecimal leijinzh;//累进值
    private String hkzhouqi;//还款周期
    private String huanbzhq;//还本周期
    private String yuqhkzhq;//逾期还款周期

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    @Override
    public String toString() {
        return "Record{" +
                "huankfsh='" + huankfsh + '\'' +
                "dechligz='" + dechligz + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "leijqjsh='" + leijqjsh + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "meiqhbje='" + meiqhbje + '\'' +
                "leijinzh='" + leijinzh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "huanbzhq='" + huanbzhq + '\'' +
                "yuqhkzhq='" + yuqhkzhq + '\'' +
                '}';
    }
}
