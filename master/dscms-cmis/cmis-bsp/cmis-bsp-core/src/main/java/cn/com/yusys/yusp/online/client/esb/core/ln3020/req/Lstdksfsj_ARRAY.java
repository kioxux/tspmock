package cn.com.yusys.yusp.online.client.esb.core.ln3020.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdksfsj.Record;

import java.util.List;

/**
 * 请求Dto：贷款收费事件
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Lstdksfsj_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdksfsj.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdksfsj_ARRAY{" +
                "record=" + record +
                '}';
    }
}