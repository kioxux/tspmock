package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfkjh;

import java.math.BigDecimal;

public class Record {
    private String fangkzhl;//放款种类

    private String fkriqiii;//放款日期

    private BigDecimal fkjineee;//放款金额

    private String dkrzhzhh;//贷款入账账号

    private String dkrzhzxh;//贷款入账账号子序号

    public String getFangkzhl() {
        return fangkzhl;
    }

    public void setFangkzhl(String fangkzhl) {
        this.fangkzhl = fangkzhl;
    }

    public String getFkriqiii() {
        return fkriqiii;
    }

    public void setFkriqiii(String fkriqiii) {
        this.fkriqiii = fkriqiii;
    }

    public BigDecimal getFkjineee() {
        return fkjineee;
    }

    public void setFkjineee(BigDecimal fkjineee) {
        this.fkjineee = fkjineee;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "fangkzhl='" + fangkzhl + '\'' +
                ", fkriqiii='" + fkriqiii + '\'' +
                ", fkjineee=" + fkjineee +
                ", dkrzhzhh='" + dkrzhzhh + '\'' +
                ", dkrzhzxh='" + dkrzhzxh + '\'' +
                '}';
    }
}
