package cn.com.yusys.yusp.web.server.cus.xdkh0033;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0033.req.Xdkh0033ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0033.resp.Xdkh0033RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0033.req.Xdkh0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0033.resp.Xdkh0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户评级结果同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0033:客户评级结果同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0033Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0033
     * 交易描述：客户评级结果同步
     *
     * @param xdkh0033ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户评级结果同步")
    @PostMapping("/xdkh0033")
    //@Idempotent({"xdcakh0033", "#xdkh0033ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0033RespDto xdkh0033(@Validated @RequestBody Xdkh0033ReqDto xdkh0033ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033ReqDto));
        Xdkh0033DataReqDto xdkh0033DataReqDto = new Xdkh0033DataReqDto();// 请求Data： 客户评级结果同步
        Xdkh0033DataRespDto xdkh0033DataRespDto = new Xdkh0033DataRespDto();// 响应Data：客户评级结果同步
        Xdkh0033RespDto xdkh0033RespDto = new Xdkh0033RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0033.req.Data reqData = null; // 请求Data：客户评级结果同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0033.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0033.resp.Data();// 响应Data：客户评级结果同步
        try {
            // 从 xdkh0033ReqDto获取 reqData
            reqData = xdkh0033ReqDto.getData();
            // 将 reqData 拷贝到xdkh0033DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0033DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033DataReqDto));
            ResultDto<Xdkh0033DataRespDto> xdkh0033DataResultDto = dscmsCusClientService.xdkh0033(xdkh0033DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0033RespDto.setErorcd(Optional.ofNullable(xdkh0033DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0033RespDto.setErortx(Optional.ofNullable(xdkh0033DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0033DataResultDto.getCode())) {
                xdkh0033DataRespDto = xdkh0033DataResultDto.getData();
                xdkh0033RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0033RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0033DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0033DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, e.getMessage());
            xdkh0033RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0033RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0033RespDto.setDatasq(servsq);

        xdkh0033RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0033.key, DscmsEnum.TRADE_CODE_XDKH0033.value, JSON.toJSONString(xdkh0033RespDto));
        return xdkh0033RespDto;
    }
}