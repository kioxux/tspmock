package cn.com.yusys.yusp.web.server.biz.xdtz0031;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0031.req.Xdtz0031ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0031.resp.Xdtz0031RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.req.Xdtz0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0031.resp.Xdtz0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据合同号获取借据信息
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0031:根据合同号获取借据信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0031Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0031Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0031
     * 交易描述：根据合同号获取借据信息
     *
     * @param xdtz0031ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0031:根据合同号获取借据信息")
    @PostMapping("/xdtz0031")
    //@Idempotent({"xdcatz0031", "#xdtz0031ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0031RespDto xdtz0031(@Validated @RequestBody Xdtz0031ReqDto xdtz0031ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(xdtz0031ReqDto));
        Xdtz0031DataReqDto xdtz0031DataReqDto = new Xdtz0031DataReqDto();// 请求Data： 根据合同号获取借据信息
        Xdtz0031DataRespDto xdtz0031DataRespDto = new Xdtz0031DataRespDto();// 响应Data：根据合同号获取借据信息
        Xdtz0031RespDto xdtz0031RespDto = new Xdtz0031RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0031.req.Data reqData = null; // 请求Data：根据合同号获取借据信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0031.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0031.resp.Data();// 响应Data：根据合同号获取借据信息
        try {
            // 从 xdtz0031ReqDto获取 reqData
            reqData = xdtz0031ReqDto.getData();
            // 将 reqData 拷贝到xdtz0031DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0031DataReqDto);
            // 调用服务
            ResultDto<Xdtz0031DataRespDto> xdtz0031DataResultDto = dscmsBizTzClientService.xdtz0031(xdtz0031DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdtz0031RespDto.setErorcd(Optional.ofNullable(xdtz0031DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0031RespDto.setErortx(Optional.ofNullable(xdtz0031DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0031DataResultDto.getCode())) {
                xdtz0031RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0031RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdtz0031DataRespDto = xdtz0031DataResultDto.getData();
                // 将 xdtz0031DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0031DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, e.getMessage());
            xdtz0031RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0031RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0031RespDto.setDatasq(servsq);
        xdtz0031RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0031.key, DscmsEnum.TRADE_CODE_XDTZ0031.value, JSON.toJSONString(xdtz0031RespDto));
        return xdtz0031RespDto;
    }
}
