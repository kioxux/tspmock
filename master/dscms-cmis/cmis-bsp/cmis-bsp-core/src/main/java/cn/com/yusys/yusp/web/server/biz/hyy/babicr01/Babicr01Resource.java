package cn.com.yusys.yusp.web.server.biz.hyy.babicr01;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.hyy.babicr01.Babicr01RespDto;
import cn.com.yusys.yusp.dto.server.xddb0014.req.Xddb0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0014.resp.Xddb0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵押登记注销风控
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "Babicr:抵押登记注销风控")
@RestController
@RequestMapping("/bank/collaterals")
public class Babicr01Resource {
    private static final Logger logger = LoggerFactory.getLogger(Babicr01Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0014
     * 交易描述：抵押登记注销风控
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记注销风控")
    @GetMapping("/{id}/cancellation-registration")
    protected @ResponseBody
    String babicr(@PathVariable("id") String id) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(id));
        Xddb0014DataReqDto xddb0014DataReqDto = new Xddb0014DataReqDto();// 请求Data： 抵押登记注销风控
        Xddb0014DataRespDto xddb0014DataRespDto = new Xddb0014DataRespDto();// 响应Data：抵押登记注销风控
        Babicr01RespDto babicr01RespDto = new Babicr01RespDto();
        cn.com.yusys.yusp.dto.server.biz.hyy.babicr01.Data hyyBabicr01Data = new cn.com.yusys.yusp.dto.server.biz.hyy.babicr01.Data();

        cn.com.yusys.yusp.dto.server.biz.xddb0014.req.Data reqData = null; // 请求Data：抵押登记注销风控
        cn.com.yusys.yusp.dto.server.biz.xddb0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0014.resp.Data();// 响应Data：抵押登记注销风控
        List<Boolean> booleans = new ArrayList<>();

        try {
            // 将 reqData 拷贝到xddb0014DataReqDto
            xddb0014DataReqDto.setGuarid(id);
            xddb0014DataReqDto.setRetype(DscmsBizDbEnum.RETYPE_01.key);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014DataReqDto));
            ResultDto<Xddb0014DataRespDto> xddb0014DataResultDto = dscmsBizDbClientService.xddb0014(xddb0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014DataRespDto));
            // 从返回值中获取响应码和响应消息
            babicr01RespDto.setCode(Optional.ofNullable(xddb0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            babicr01RespDto.setMessage(Optional.ofNullable(xddb0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0014DataResultDto.getCode())) {
                xddb0014DataRespDto = xddb0014DataResultDto.getData();
                babicr01RespDto.setCode(SuccessEnum.CODE_SUCCESS.key);
                babicr01RespDto.setSuccess(true);
                babicr01RespDto.setMessage(SuccessEnum.MESSAGE_SUCCESS.key);
                if (DscmsBizDbEnum.FALG_SUCCESS.key.equals(xddb0014DataRespDto.getOpFlag())) {
                    booleans.add(true);
                    hyyBabicr01Data.setItems(booleans);
                } else {
                    booleans.add(false);
                    hyyBabicr01Data.setItems(booleans);
                }
            } else {
                booleans.add(false);
                hyyBabicr01Data.setItems(booleans);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, e.getMessage());
            babicr01RespDto.setCode("BA-BI-EC01-00"); // 9999
            babicr01RespDto.setMessage(EpbEnum.EPB099999.value);// 系统异常
        }

        babicr01RespDto.setData(hyyBabicr01Data);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(babicr01RespDto));
        return JSON.toJSONString(babicr01RespDto);
    }
}
