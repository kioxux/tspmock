package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.resp;

/**
 * 响应Service：自动审批调查报告
 * @author muxiang
 * @version 1.0
 * @since 2021/4/19 21:58
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    public String  getErorcd() { return erorcd; }
    public void setErorcd(String erorcd ) { this.erorcd = erorcd;}
    public String  getErortx() { return erortx; }
    public void setErortx(String erortx ) { this.erortx = erortx;}
    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd+ '\'' +
                "erortx='" + erortx+ '\'' +
                '}';
    }
}