package cn.com.yusys.yusp.online.client.esb.ypxt.xdypzywcx.req;

/**
 * 请求Service：查询质押物信息
 *
 * @author chenyong
 * @version 1.0
 */
public class XdypzywcxReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}                      
