package cn.com.yusys.yusp.web.client.esb.irs.irs98;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.common.*;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.common.AllMessageInfoRecord;
import cn.com.yusys.yusp.online.client.esb.irs.common.AppMessageInfoRecord;
import cn.com.yusys.yusp.online.client.esb.irs.common.MessageInfoRecord;
import cn.com.yusys.yusp.online.client.esb.irs.irs98.req.Irs98ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.irs98.resp.Irs98RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(irs98)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Irs98Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Irs98Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB授信申请债项评级接口（处理码IRS98）
     *
     * @param irs98ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("irs98:ESB授信申请债项评级接口")
    @PostMapping("/irs98")
    protected @ResponseBody
    ResultDto<Irs98RespDto> irs98(@Validated @RequestBody Irs98ReqDto irs98ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS98.key, EsbEnum.TRADE_CODE_IRS98.value, JSON.toJSONString(irs98ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.irs98.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.irs98.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.irs98.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.irs98.resp.Service();
        Irs98ReqService irs98ReqService = new Irs98ReqService();
        Irs98RespService irs98RespService = new Irs98RespService();
        Irs98RespDto irs98RespDto = new Irs98RespDto();
        ResultDto<Irs98RespDto> irs98ResultDto = new ResultDto<Irs98RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Irs98ReqDto转换成 reqService
            BeanUtils.copyProperties(irs98ReqDto, reqService);
            List<AccLoanInfo> accLoanInfoList = Optional.ofNullable(irs98ReqDto.getAccLoanInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(accLoanInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfo accLoanInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfoRecord> accLoanInfoRecords = accLoanInfoList.parallelStream().map(accLoanInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.AccLoanInfoRecord();
                    BeanUtils.copyProperties(accLoanInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                accLoanInfoService.setRecord(accLoanInfoRecords);
                reqService.setAccLoanInfo(accLoanInfoService);
            }

            List<AssureAccInfo> assureAccInfoList = Optional.ofNullable(irs98ReqDto.getAssureAccInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(assureAccInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfo assureAccInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfoRecord> assureAccInfoRecords = assureAccInfoList.parallelStream().map(assureAccInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.AssureAccInfoRecord();
                    BeanUtils.copyProperties(assureAccInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                assureAccInfoService.setRecord(assureAccInfoRecords);
                reqService.setAssureAccInfo(assureAccInfoService);
            }

            List<AssurePersonInfo> assurePersonInfoList = Optional.ofNullable(irs98ReqDto.getAssurePersonInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(assurePersonInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo assurePersonInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord> assurePersonInfoRecords = assurePersonInfoList.parallelStream().map(assurePersonInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord();
                    BeanUtils.copyProperties(assurePersonInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                assurePersonInfoService.setRecord(assurePersonInfoRecords);
                reqService.setAssurePersonInfo(assurePersonInfoService);
            }

            List<BusinessAssureInfo> businessAssureInfoList = Optional.ofNullable(irs98ReqDto.getBusinessAssureInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(businessAssureInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.BusinessAssureInfo businessAssureInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.BusinessAssureInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.BusinessAssureInfoRecord> businessAssureInfoRecords = businessAssureInfoList.parallelStream().map(businessAssureInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.BusinessAssureInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.BusinessAssureInfoRecord();
                    BeanUtils.copyProperties(businessAssureInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                businessAssureInfoService.setRecord(businessAssureInfoRecords);
                reqService.setBusinessAssureInfo(businessAssureInfoService);
            }

            List<BusinessContractInfo> businessContractInfoList = Optional.ofNullable(irs98ReqDto.getBusinessContractInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(businessContractInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfo businessContractInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfoRecord> businessContractInfoRecords = businessContractInfoList.parallelStream().map(businessContractInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.BusinessContractInfoRecord();
                    BeanUtils.copyProperties(businessContractInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                businessContractInfoService.setRecord(businessContractInfoRecords);
                reqService.setBusinessContractInfo(businessContractInfoService);
            }

            List<CurInfo> curInfoList = Optional.ofNullable(irs98ReqDto.getCurInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(curInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo curInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord> curInfoRecords = curInfoList.parallelStream().map(curInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord();
                    BeanUtils.copyProperties(curInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                curInfoService.setRecord(curInfoRecords);
                reqService.setCurInfo(curInfoService);
            }

            List<CustomerInfo> customerInfoList = Optional.ofNullable(irs98ReqDto.getCustomerInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(customerInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo customerInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord> customerInfoRecords = customerInfoList.parallelStream().map(customerInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord();
                    BeanUtils.copyProperties(customerInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                customerInfoService.setRecord(customerInfoRecords);
                reqService.setCustomerInfo(customerInfoService);
            }

            List<DealCustomerInfo> dealCustomerInfoList = Optional.ofNullable(irs98ReqDto.getDealCustomerInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(dealCustomerInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.DealCustomerInfo dealCustomerInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.DealCustomerInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.DealCustomerInfoRecord> dealCustomerInfoRecords = dealCustomerInfoList.parallelStream().map(dealCustomerInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.DealCustomerInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.DealCustomerInfoRecord();
                    BeanUtils.copyProperties(dealCustomerInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                dealCustomerInfoService.setRecord(dealCustomerInfoRecords);
                reqService.setDealCustomerInfo(dealCustomerInfoService);
            }

            List<GuaranteeContrctInfo> guaranteeContrctInfoList = Optional.ofNullable(irs98ReqDto.getGuaranteeContrctInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(guaranteeContrctInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteeContrctInfo guaranteeContrctInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteeContrctInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteeContrctInfoRecord> guaranteeContrctInfoRecords = guaranteeContrctInfoList.parallelStream().map(guaranteeContrctInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteeContrctInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteeContrctInfoRecord();
                    BeanUtils.copyProperties(guaranteeContrctInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                guaranteeContrctInfoService.setRecord(guaranteeContrctInfoRecords);
                reqService.setGuaranteeContrctInfo(guaranteeContrctInfoService);
            }

            List<GuaranteePleMortInfo> guaranteePleMortInfoList = Optional.ofNullable(irs98ReqDto.getGuaranteePleMortInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(guaranteePleMortInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteePleMortInfo guaranteePleMortInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteePleMortInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteePleMortInfoRecord> guaranteePleMortInfoRecords = guaranteePleMortInfoList.parallelStream().map(guaranteePleMortInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteePleMortInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.GuaranteePleMortInfoRecord();
                    BeanUtils.copyProperties(guaranteePleMortInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                guaranteePleMortInfoService.setRecord(guaranteePleMortInfoRecords);
                reqService.setGuaranteePleMortInfo(guaranteePleMortInfoService);
            }

            List<LimitApplyInfo> limitApplyInfoList = Optional.ofNullable(irs98ReqDto.getLimitApplyInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(limitApplyInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo limitApplyInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfoRecord> limitApplyInfoRecords = limitApplyInfoList.parallelStream().map(limitApplyInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfoRecord();
                    BeanUtils.copyProperties(limitApplyInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                limitApplyInfoService.setRecord(limitApplyInfoRecords);
                reqService.setLimitApplyInfo(limitApplyInfoService);
            }

            List<LimitDetailsInfo> limitDetailsInfoList = Optional.ofNullable(irs98ReqDto.getLimitDetailsInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(limitDetailsInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo limitDetailsInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord> limitDetailsInfoRecords = limitDetailsInfoList.parallelStream().map(limitDetailsInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord();
                    BeanUtils.copyProperties(limitDetailsInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                limitDetailsInfoService.setRecord(limitDetailsInfoRecords);
                reqService.setLimitDetailsInfo(limitDetailsInfoService);
            }

            List<LimitPleMortInfo> limitPleMortInfoList = Optional.ofNullable(irs98ReqDto.getLimitPleMortInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(limitPleMortInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo limitPleMortInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord> limitPleMortInfoRecords = limitPleMortInfoList.parallelStream().map(limitPleMortInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord();
                    BeanUtils.copyProperties(limitPleMortInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                limitPleMortInfoService.setRecord(limitPleMortInfoRecords);
                reqService.setLimitPleMortInfo(limitPleMortInfoService);
            }

            List<LimitProductInfo> limitProductInfoList = Optional.ofNullable(irs98ReqDto.getLimitProductInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(limitProductInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo limitProductInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord> limitProductInfoRecords = limitProductInfoList.parallelStream().map(limitProductInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord();
                    BeanUtils.copyProperties(limitProductInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                limitProductInfoService.setRecord(limitProductInfoRecords);
                reqService.setLimitProductInfo(limitProductInfoService);
            }

            List<MortgageInfo> mortgageInfoList = Optional.ofNullable(irs98ReqDto.getMortgageInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(mortgageInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo mortgageInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord> mortgageInfoRecords = mortgageInfoList.parallelStream().map(mortgageInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord();
                    BeanUtils.copyProperties(mortgageInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                mortgageInfoService.setRecord(mortgageInfoRecords);
                reqService.setMortgageInfo(mortgageInfoService);
            }

            List<PledgeInfo> pledgeInfoList = Optional.ofNullable(irs98ReqDto.getPledgeInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(pledgeInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo pledgeInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord> pledgeInfoRecords = pledgeInfoList.parallelStream().map(pledgeInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord();
                    BeanUtils.copyProperties(pledgeInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                pledgeInfoService.setRecord(pledgeInfoRecords);
                reqService.setPledgeInfo(pledgeInfoService);
            }

            List<UpApplyInfo> upApplyInfoList = Optional.ofNullable(irs98ReqDto.getUpApplyInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(upApplyInfoList)) {
                cn.com.yusys.yusp.online.client.esb.irs.common.UpApplyInfo upApplyInfoService = new cn.com.yusys.yusp.online.client.esb.irs.common.UpApplyInfo();
                List<cn.com.yusys.yusp.online.client.esb.irs.common.UpApplyInfoRecord> upApplyInfoRecords = upApplyInfoList.parallelStream().map(upApplyInfo -> {
                    cn.com.yusys.yusp.online.client.esb.irs.common.UpApplyInfoRecord temp = new cn.com.yusys.yusp.online.client.esb.irs.common.UpApplyInfoRecord();
                    BeanUtils.copyProperties(upApplyInfo, temp);
                    return temp;
                }).collect(Collectors.toList());
                upApplyInfoService.setRecord(upApplyInfoRecords);
                reqService.setUpApplyInfo(upApplyInfoService);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS98.key);// 标准接口代码（交易处理码）
            LocalDateTime now = LocalDateTime.now();
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_FLS.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CIIS2ND.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIIS2ND.key);//    部门号
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            irs98ReqService.setService(reqService);
            // 将irs98ReqService转换成irs98ReqServiceMap
            Map irs98ReqServiceMap = beanMapUtil.beanToMap(irs98ReqService);
            context.put("tradeDataMap", irs98ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS98.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            irs98RespService = beanMapUtil.mapToBean(tradeDataMap, Irs98RespService.class, Irs98RespService.class);
            respService = irs98RespService.getService();
            //  将respService转换成Irs98RespDto
            BeanUtils.copyProperties(respService, irs98RespDto);
            //  将Irs98RespDto封装到ResultDto<Irs98RespDto>
            irs98ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            irs98ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G00101RespDto
                BeanUtils.copyProperties(respService, irs98RespDto);

                cn.com.yusys.yusp.online.client.esb.irs.common.AppMessageInfo appMessageInfo = Optional.ofNullable(respService.getAppMessageInfo()).orElse(new cn.com.yusys.yusp.online.client.esb.irs.common.AppMessageInfo());
                List<AppMessageInfoRecord> appMessageInfoRecords = Optional.ofNullable(appMessageInfo.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(appMessageInfoRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo> appMessageInfoResp = appMessageInfoRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo temp = new cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    irs98RespDto.setAppMessageInfo(appMessageInfoResp);
                }

                cn.com.yusys.yusp.online.client.esb.irs.common.AllMessageInfo allMessageInfo = Optional.ofNullable(respService.getAllMessageInfo()).orElse(new cn.com.yusys.yusp.online.client.esb.irs.common.AllMessageInfo());
                List<AllMessageInfoRecord> allMessageInfoRecords = Optional.ofNullable(allMessageInfo.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(allMessageInfoRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo> allMessageInfoResp = allMessageInfoRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo temp = new cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    irs98RespDto.setAllMessageInfo(allMessageInfoResp);
                }

                cn.com.yusys.yusp.online.client.esb.irs.common.MessageInfo messageInfo = Optional.ofNullable(respService.getMessageInfo()).orElse(new cn.com.yusys.yusp.online.client.esb.irs.common.MessageInfo());
                List<MessageInfoRecord> messageInfoRecords = Optional.ofNullable(messageInfo.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(messageInfoRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo> messageInfoResp = messageInfoRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo temp = new cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    irs98RespDto.setMessageInfo(messageInfoResp);
                }
                irs98ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                irs98ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                irs98ResultDto.setCode(EpbEnum.EPB099999.key);
                irs98ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS98.key, EsbEnum.TRADE_CODE_IRS98.value, e.getMessage());
            irs98ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            irs98ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        irs98ResultDto.setData(irs98RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS98.key, EsbEnum.TRADE_CODE_IRS98.value, JSON.toJSONString(irs98ResultDto));

        return irs98ResultDto;
    }
}
