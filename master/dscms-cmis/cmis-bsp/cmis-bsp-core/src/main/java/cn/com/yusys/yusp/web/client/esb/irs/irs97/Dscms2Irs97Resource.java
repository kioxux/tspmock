package cn.com.yusys.yusp.web.client.esb.irs.irs97;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.common.*;
import cn.com.yusys.yusp.online.client.esb.irs.irs97.req.Irs97ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.irs97.resp.Irs97RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(irs97)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Irs97Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Irs97Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB新增授信时债项评级接口（处理码IRS97）
     *
     * @param irs97ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("irs97:ESB新增授信时债项评级接口")
    @PostMapping("/irs97")
    protected @ResponseBody
    ResultDto<Irs97RespDto> irs97(@Validated @RequestBody Irs97ReqDto irs97ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS97.key, EsbEnum.TRADE_CODE_IRS97.value, JSON.toJSONString(irs97ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.irs97.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.irs97.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.irs97.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.irs97.resp.Service();
        Irs97ReqService irs97ReqService = new Irs97ReqService();
        Irs97RespService irs97RespService = new Irs97RespService();
        Irs97RespDto irs97RespDto = new Irs97RespDto();
        ResultDto<Irs97RespDto> irs97ResultDto = new ResultDto<Irs97RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Irs97ReqDto转换成reqService
            BeanUtils.copyProperties(irs97ReqDto, reqService);
            // 综合授信申请信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo limitApplyInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getLimitApplyInfo())) {
                List<LimitApplyInfoRecord> limitApplyInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo> limitApplyInfoDtos = irs97ReqDto.getLimitApplyInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitApplyInfo limitApplyInfoDto : limitApplyInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfoRecord limitApplyInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitApplyInfoRecord();
                    BeanUtils.copyProperties(limitApplyInfoDto, limitApplyInfoRecord);
                    limitApplyInfoRecords.add(limitApplyInfoRecord);
                }
                limitApplyInfo.setRecord(limitApplyInfoRecords);
            }
            reqService.setLimitApplyInfo(limitApplyInfo);
            // 分项额度信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo limitDetailsInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getLimitDetailsInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord> limitDetailsInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo> limitDetailsInfoDtos = irs97ReqDto.getLimitDetailsInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitDetailsInfo limitDetailsInfoDto : limitDetailsInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord limitDetailsInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitDetailsInfoRecord();
                    BeanUtils.copyProperties(limitDetailsInfoDto, limitDetailsInfoRecord);
                    limitDetailsInfoRecords.add(limitDetailsInfoRecord);
                }
                limitDetailsInfo.setRecord(limitDetailsInfoRecords);
            }
            reqService.setLimitDetailsInfo(limitDetailsInfo);
            // 产品额度信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo limitProductInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getLimitProductInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord> limitProductInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo> limitProductInfoDtos = irs97ReqDto.getLimitProductInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitProductInfo limitProductInfoDto : limitProductInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord limitProductInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitProductInfoRecord();
                    BeanUtils.copyProperties(limitProductInfoDto, limitProductInfoRecord);
                    limitProductInfoRecords.add(limitProductInfoRecord);
                }
                limitProductInfo.setRecord(limitProductInfoRecords);
            }
            reqService.setLimitProductInfo(limitProductInfo);
            // 客户信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo customerInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getCustomerInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord> customerInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo> customerInfoDtos = irs97ReqDto.getCustomerInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.CustomerInfo customerInfoDto : customerInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord customerInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.CustomerInfoRecord();
                    BeanUtils.copyProperties(customerInfoDto, customerInfoRecord);
                    customerInfoRecords.add(customerInfoRecord);
                }
                customerInfo.setRecord(customerInfoRecords);
            }
            reqService.setCustomerInfo(customerInfo);
            // 授信分项额度与抵质押、保证人关系信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo limitPleMortInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getLimitPleMortInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord> limitPleMortInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo> limitPleMortInfoDtos = irs97ReqDto.getLimitPleMortInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.LimitPleMortInfo limitPleMortInfoDto : limitPleMortInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord limitPleMortInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.LimitPleMortInfoRecord();
                    BeanUtils.copyProperties(limitPleMortInfoDto, limitPleMortInfoRecord);
                    limitPleMortInfoRecords.add(limitPleMortInfoRecord);
                }
                limitPleMortInfo.setRecord(limitPleMortInfoRecords);
            }
            reqService.setLimitPleMortInfo(limitPleMortInfo);
            // 质押物信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo pledgeInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getPledgeInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord> pledgeInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo> pledgeInfoDtos = irs97ReqDto.getPledgeInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.PledgeInfo pledgeInfoDto : pledgeInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord pledgeInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.PledgeInfoRecord();
                    BeanUtils.copyProperties(pledgeInfoDto, pledgeInfoRecord);
                    pledgeInfoRecords.add(pledgeInfoRecord);
                }
                pledgeInfo.setRecord(pledgeInfoRecords);
            }
            reqService.setPledgeInfo(pledgeInfo);
            // 抵押物信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo mortgageInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getMortgageInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord> mortgageInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo> mortgageInfoDtos = irs97ReqDto.getMortgageInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.MortgageInfo mortgageInfoDto : mortgageInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord mortgageInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.MortgageInfoRecord();
                    BeanUtils.copyProperties(mortgageInfoDto, mortgageInfoRecord);
                    mortgageInfoRecords.add(mortgageInfoRecord);
                }
                mortgageInfo.setRecord(mortgageInfoRecords);
            }
            reqService.setMortgageInfo(mortgageInfo);
            // 保证人信息list
            cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo assurePersonInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getAssurePersonInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord> assurePersonInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo> assurePersonInfoDtos = irs97ReqDto.getAssurePersonInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.AssurePersonInfo assurePersonInfoDto : assurePersonInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord assurePersonInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.AssurePersonInfoRecord();
                    BeanUtils.copyProperties(assurePersonInfoDto, assurePersonInfoRecord);
                    assurePersonInfoRecords.add(assurePersonInfoRecord);
                }
                assurePersonInfo.setRecord(assurePersonInfoRecords);
            }
            reqService.setAssurePersonInfo(assurePersonInfo);
            // 汇率信息
            cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo curInfo = new cn.com.yusys.yusp.online.client.esb.irs.common.CurInfo();
            if (CollectionUtils.nonEmpty(irs97ReqDto.getCurInfo())) {
                List<cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord> curInfoRecords = new ArrayList<>();
                List<cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo> curInfoDtos = irs97ReqDto.getCurInfo();
                for (cn.com.yusys.yusp.dto.client.esb.irs.common.CurInfo curInfoDto : curInfoDtos) {
                    cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord curInfoRecord = new cn.com.yusys.yusp.online.client.esb.irs.common.CurInfoRecord();
                    BeanUtils.copyProperties(curInfoDto, curInfoRecord);
                    curInfoRecords.add(curInfoRecord);
                }
                curInfo.setRecord(curInfoRecords);
            }
            reqService.setCurInfo(curInfo);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS97.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            irs97ReqService.setService(reqService);
            // 将irs97ReqService转换成irs97ReqServiceMap
            Map irs97ReqServiceMap = beanMapUtil.beanToMap(irs97ReqService);
            context.put("tradeDataMap", irs97ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS97.key, EsbEnum.TRADE_CODE_IRS97.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS97.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS97.key, EsbEnum.TRADE_CODE_IRS97.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            irs97RespService = beanMapUtil.mapToBean(tradeDataMap, Irs97RespService.class, Irs97RespService.class);
            respService = irs97RespService.getService();

            //  将Irs97RespDto封装到ResultDto<Irs97RespDto>
            irs97ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            irs97ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Irs97RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, irs97RespDto);
                AllMessageInfo allMessageInfo = Optional.ofNullable(respService.getAllMessageInfo()).orElse(new AllMessageInfo());
                List<AllMessageInfoRecord> allMessageInfoRecords = Optional.ofNullable(allMessageInfo.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(allMessageInfoRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo> allMessageInfoResp = allMessageInfoRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo temp = new cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    irs97RespDto.setAllMessageInfo(allMessageInfoResp);
                }

                AppMessageInfo appMessageInfo = Optional.ofNullable(respService.getAppMessageInfo()).orElse(new AppMessageInfo());
                List<AppMessageInfoRecord> appMessageInfoRecords = Optional.ofNullable(appMessageInfo.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(appMessageInfoRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo> appMessageInfoResp = appMessageInfoRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo temp = new cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    irs97RespDto.setAppMessageInfo(appMessageInfoResp);
                }

                MessageInfo messageInfo = Optional.ofNullable(respService.getMessageInfo()).orElse(new MessageInfo());
                List<MessageInfoRecord> messageInfoRecords = Optional.ofNullable(messageInfo.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(messageInfoRecords)) {
                    List<cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo> messageInfoResp = messageInfoRecords.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo temp = new cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    irs97RespDto.setMessageInfo(messageInfoResp);
                }
                irs97ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                irs97ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                irs97ResultDto.setCode(EpbEnum.EPB099999.key);
                irs97ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS97.key, EsbEnum.TRADE_CODE_IRS97.value, e.getMessage());
            irs97ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            irs97ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        irs97ResultDto.setData(irs97RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS97.key, EsbEnum.TRADE_CODE_IRS97.value, JSON.toJSONString(irs97ResultDto));
        return irs97ResultDto;

    }
}
