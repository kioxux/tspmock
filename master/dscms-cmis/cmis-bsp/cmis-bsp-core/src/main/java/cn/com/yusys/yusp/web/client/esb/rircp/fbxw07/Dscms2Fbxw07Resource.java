package cn.com.yusys.yusp.web.client.esb.rircp.fbxw07;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw07.Fbxw07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw07.Fbxw07RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw07.req.Fbxw07ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw07.resp.Fbxw07RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxw07）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw07Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw07Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 优惠利率申请接口（处理码fbxw07）
     *
     * @param fbxw07ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw07:优惠利率申请接口")
    @PostMapping("/fbxw07")
    protected @ResponseBody
    ResultDto<Fbxw07RespDto> fbxw07(@Validated @RequestBody Fbxw07ReqDto fbxw07ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW07.key, EsbEnum.TRADE_CODE_FBXW07.value, JSON.toJSONString(fbxw07ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw07.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw07.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw07.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw07.resp.Service();
        Fbxw07ReqService fbxw07ReqService = new Fbxw07ReqService();
        Fbxw07RespService fbxw07RespService = new Fbxw07RespService();
        Fbxw07RespDto fbxw07RespDto = new Fbxw07RespDto();
        ResultDto<Fbxw07RespDto> fbxw07ResultDto = new ResultDto<Fbxw07RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw07ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw07ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW07.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw07ReqService.setService(reqService);
            // 将fbxw07ReqService转换成fbxw07ReqServiceMap
            Map fbxw07ReqServiceMap = beanMapUtil.beanToMap(fbxw07ReqService);
            context.put("tradeDataMap", fbxw07ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW07.key, EsbEnum.TRADE_CODE_FBXW07.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW07.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW07.key, EsbEnum.TRADE_CODE_FBXW07.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw07RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw07RespService.class, Fbxw07RespService.class);
            respService = fbxw07RespService.getService();

            //  将Fbxw07RespDto封装到ResultDto<Fbxw07RespDto>
            fbxw07ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw07ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw07RespDto
                BeanUtils.copyProperties(respService, fbxw07RespDto);
                fbxw07ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw07ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw07ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw07ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW07.key, EsbEnum.TRADE_CODE_FBXW07.value, e.getMessage());
            fbxw07ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw07ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw07ResultDto.setData(fbxw07RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW07.key, EsbEnum.TRADE_CODE_FBXW07.value, JSON.toJSONString(fbxw07ResultDto));
        return fbxw07ResultDto;
    }
}
