package cn.com.yusys.yusp.online.client.esb.core.ln3006.req;

/**
 * 请求Service：贷款产品组合查询
 */
public class Service {
	private String prcscd;//处理码
	private String servtp;//渠道
	private String servsq;//渠道流水
	private String userid;//柜员号
	private String brchno;//部门号
	private String datasq;//全局流水
	private String servdt;//交易日期
	private String servti;//交易时间

    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String chanpzht;//产品状态
    private String daikduix;//贷款对象
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getChanpzht() {
        return chanpzht;
    }

    public void setChanpzht(String chanpzht) {
        this.chanpzht = chanpzht;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", datasq='" + datasq + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", chanpdma='" + chanpdma + '\'' +
				", chanpmch='" + chanpmch + '\'' +
				", chanpzht='" + chanpzht + '\'' +
				", daikduix='" + daikduix + '\'' +
				", qishibis=" + qishibis +
				", chxunbis=" + chxunbis +
				'}';
	}
}
