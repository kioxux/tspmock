package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req;

/**
 * 请求Service：票据承兑签发审批请求
 */
public class Record {
    private String accountType;//钞汇标志
    private String currency;//币种
    private String interestMode;//计结息方式
    private String acctAmt;//金额
    private String firstAcctSeq;//母户
    private String twoAcctSeq;//二级子账户
    private String twoAcctName;//二级账户名称
    private String threeAcctNo;//保证金账户
    private String settleAcctNo;//结算账户
    private String settleAcctSeq;//结算账号子序号
    private String settleAcctName;//结算账户名
    private String settleBankCode;//结算开户行行号
    private String clearAcctSeq;//待清算子序号
    private String clearAcctName;//待清算账号名
    private String clearBankCode;//待清算开户行行号
    private String zhfutojn;//支付条件


	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getInterestMode() {
		return interestMode;
	}

	public void setInterestMode(String interestMode) {
		this.interestMode = interestMode;
	}

	public String getAcctAmt() {
		return acctAmt;
	}

	public void setAcctAmt(String acctAmt) {
		this.acctAmt = acctAmt;
	}

	public String getFirstAcctSeq() {
		return firstAcctSeq;
	}

	public void setFirstAcctSeq(String firstAcctSeq) {
		this.firstAcctSeq = firstAcctSeq;
	}

	public String getTwoAcctSeq() {
		return twoAcctSeq;
	}

	public void setTwoAcctSeq(String twoAcctSeq) {
		this.twoAcctSeq = twoAcctSeq;
	}

	public String getTwoAcctName() {
		return twoAcctName;
	}

	public void setTwoAcctName(String twoAcctName) {
		this.twoAcctName = twoAcctName;
	}

	public String getThreeAcctNo() {
		return threeAcctNo;
	}

	public void setThreeAcctNo(String threeAcctNo) {
		this.threeAcctNo = threeAcctNo;
	}

	public String getSettleAcctNo() {
		return settleAcctNo;
	}

	public void setSettleAcctNo(String settleAcctNo) {
		this.settleAcctNo = settleAcctNo;
	}

	public String getSettleAcctSeq() {
		return settleAcctSeq;
	}

	public void setSettleAcctSeq(String settleAcctSeq) {
		this.settleAcctSeq = settleAcctSeq;
	}

	public String getSettleAcctName() {
		return settleAcctName;
	}

	public void setSettleAcctName(String settleAcctName) {
		this.settleAcctName = settleAcctName;
	}

	public String getSettleBankCode() {
		return settleBankCode;
	}

	public void setSettleBankCode(String settleBankCode) {
		this.settleBankCode = settleBankCode;
	}

	public String getClearAcctSeq() {
		return clearAcctSeq;
	}

	public void setClearAcctSeq(String clearAcctSeq) {
		this.clearAcctSeq = clearAcctSeq;
	}

	public String getClearAcctName() {
		return clearAcctName;
	}

	public void setClearAcctName(String clearAcctName) {
		this.clearAcctName = clearAcctName;
	}

	public String getClearBankCode() {
		return clearBankCode;
	}

	public void setClearBankCode(String clearBankCode) {
		this.clearBankCode = clearBankCode;
	}

	public String getZhfutojn() {
		return zhfutojn;
	}

	public void setZhfutojn(String zhfutojn) {
		this.zhfutojn = zhfutojn;
	}

	@Override
	public String toString() {
		return "Record{" +
				", accountType='" + accountType + '\'' +
				", currency='" + currency + '\'' +
				", interestMode='" + interestMode + '\'' +
				", acctAmt='" + acctAmt + '\'' +
				", firstAcctSeq='" + firstAcctSeq + '\'' +
				", twoAcctSeq='" + twoAcctSeq + '\'' +
				", twoAcctName='" + twoAcctName + '\'' +
				", threeAcctNo='" + threeAcctNo + '\'' +
				", settleAcctNo='" + settleAcctNo + '\'' +
				", settleAcctSeq='" + settleAcctSeq + '\'' +
				", settleAcctName='" + settleAcctName + '\'' +
				", settleBankCode='" + settleBankCode + '\'' +
				", clearAcctSeq='" + clearAcctSeq + '\'' +
				", clearAcctName='" + clearAcctName + '\'' +
				", clearBankCode='" + clearBankCode + '\'' +
				", zhfutojn='" + zhfutojn + '\'' +
				'}';
	}
}
