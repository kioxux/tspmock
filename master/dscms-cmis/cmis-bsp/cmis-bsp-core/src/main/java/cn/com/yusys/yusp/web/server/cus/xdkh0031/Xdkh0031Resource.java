package cn.com.yusys.yusp.web.server.cus.xdkh0031;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0031.req.Xdkh0031ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0031.resp.Xdkh0031RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0031.req.Xdkh0031DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0031.resp.Xdkh0031DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:同业客户评级相关信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0031:同业客户评级相关信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0031Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0031Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0031
     * 交易描述：同业客户评级相关信息同步
     *
     * @param xdkh0031ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("同业客户评级相关信息同步")
    @PostMapping("/xdkh0031")
    //@Idempotent({"xdcakh0031", "#xdkh0031ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0031RespDto xdkh0031(@Validated @RequestBody Xdkh0031ReqDto xdkh0031ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031ReqDto));
        Xdkh0031DataReqDto xdkh0031DataReqDto = new Xdkh0031DataReqDto();// 请求Data： 同业客户评级相关信息同步
        Xdkh0031DataRespDto xdkh0031DataRespDto = new Xdkh0031DataRespDto();// 响应Data：同业客户评级相关信息同步
        Xdkh0031RespDto xdkh0031RespDto = new Xdkh0031RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0031.req.Data reqData = null; // 请求Data：同业客户评级相关信息同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0031.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0031.resp.Data();// 响应Data：同业客户评级相关信息同步
        try {
            // 从 xdkh0031ReqDto获取 reqData
            reqData = xdkh0031ReqDto.getData();
            // 将 reqData 拷贝到xdkh0031DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0031DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataReqDto));
            ResultDto<Xdkh0031DataRespDto> xdkh0031DataResultDto = dscmsCusClientService.xdkh0031(xdkh0031DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0031RespDto.setErorcd(Optional.ofNullable(xdkh0031DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0031RespDto.setErortx(Optional.ofNullable(xdkh0031DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0031DataResultDto.getCode())) {
                xdkh0031DataRespDto = xdkh0031DataResultDto.getData();
                xdkh0031RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0031RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0031DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0031DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, e.getMessage());
            xdkh0031RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0031RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0031RespDto.setDatasq(servsq);

        xdkh0031RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0031.key, DscmsEnum.TRADE_CODE_XDKH0031.value, JSON.toJSONString(xdkh0031RespDto));
        return xdkh0031RespDto;
    }
}