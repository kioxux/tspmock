package cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.resp;

/**
 * 响应Service：用户信息同步接口
 *
 * @author code-generator
 * @version 1.0
 */
public class YhxxtbRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "YhxxtbRespService{" +
                "service=" + service +
                '}';
    }
}
