package cn.com.yusys.yusp.web.server.biz.xdht0025;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0025.req.Xdht0025ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0025.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdht0025.resp.Xdht0025RespDto;
import cn.com.yusys.yusp.dto.server.xdht0025.req.Xdht0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0025.resp.Xdht0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同签订/撤销（优享贷和增享贷）
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0025:合同签订/撤销（优享贷和增享贷）")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0025Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0025
     * 交易描述：合同签订/撤销（优享贷和增享贷）
     *
     * @param xdht0025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订/撤销（优享贷和增享贷）")
    @PostMapping("/xdht0025")
    //@Idempotent({"xdcaht0025", "#xdht0025ReqDto.datasq"})
    protected @ResponseBody
    Xdht0025RespDto xdht0025(@Validated @RequestBody Xdht0025ReqDto xdht0025ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, JSON.toJSONString(xdht0025ReqDto));
        Xdht0025DataReqDto xdht0025DataReqDto = new Xdht0025DataReqDto();// 请求Data： 合同签订/撤销（优享贷和增享贷）
        Xdht0025DataRespDto xdht0025DataRespDto = new Xdht0025DataRespDto();// 响应Data：合同签订/撤销（优享贷和增享贷）
        Xdht0025RespDto xdht0025RespDto = new Xdht0025RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0025.req.Data reqData = null; // 请求Data：合同签订/撤销（优享贷和增享贷）
        cn.com.yusys.yusp.dto.server.biz.xdht0025.resp.Data respData = new Data();// 响应Data：合同签订/撤销（优享贷和增享贷）

        try {
            // 从 xdht0025ReqDto获取 reqData
            reqData = xdht0025ReqDto.getData();
            // 将 reqData 拷贝到xdht0025DataReqDto
            BeanUtils.copyProperties(reqData, xdht0025DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, JSON.toJSONString(xdht0025DataReqDto));
            ResultDto<Xdht0025DataRespDto> xdht0025DataResultDto = dscmsBizHtClientService.xdht0025(xdht0025DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, JSON.toJSONString(xdht0025DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0025RespDto.setErorcd(Optional.ofNullable(xdht0025DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0025RespDto.setErortx(Optional.ofNullable(xdht0025DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0025DataResultDto.getCode())) {
                xdht0025DataRespDto = xdht0025DataResultDto.getData();
                xdht0025RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0025RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0025DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0025DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, e.getMessage());
            xdht0025RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0025RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0025RespDto.setDatasq(servsq);

        xdht0025RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0025.key, DscmsEnum.TRADE_CODE_XDHT0025.value, JSON.toJSONString(xdht0025RespDto));
        return xdht0025RespDto;
    }
}
