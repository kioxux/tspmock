package cn.com.yusys.yusp.web.server.batch.xdpl0001;

import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010ReqDto;
import cn.com.yusys.yusp.batch.dto.server.cmisbatch0010.Cmisbatch0010RespDto;
import cn.com.yusys.yusp.batch.service.CmisBatchClientService;
import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.batch.xdpl0001.req.Xdpl0001ReqDto;
import cn.com.yusys.yusp.dto.server.batch.xdpl0001.resp.Xdpl0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提供日终调度平台调起批量任务
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDPL0001:提供日终调度平台调起批量任务")
@RestController
@RequestMapping("/api/dscms")
public class Xdpl0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdpl0001Resource.class);

    @Autowired
    private CmisBatchClientService cmisBatchClientService;

    /**
     * 交易码：xdpl0001
     * 交易描述：提供日终调度平台调起批量任务
     *
     * @param xdpl0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提供日终调度平台调起批量任务")
    @PostMapping("/xdpl0001")
    @Idempotent({"xdpl0001", "#xdpl0001ReqDto.datasq"})
    protected @ResponseBody
    Xdpl0001RespDto xdpl0001(@Validated @RequestBody Xdpl0001ReqDto xdpl0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0001.key, DscmsEnum.TRADE_CODE_XDPL0001.value, JSON.toJSONString(xdpl0001ReqDto));
        Cmisbatch0010ReqDto cmisbatch0010ReqDto = new Cmisbatch0010ReqDto();// 请求Data： 提供日终调度平台调起批量任务
        Cmisbatch0010RespDto cmisbatch0010RespDto = new Cmisbatch0010RespDto();// 响应Data：提供日终调度平台调起批量任务
        cn.com.yusys.yusp.dto.server.batch.xdpl0001.req.Data reqData = null; // 请求Data：提供日终调度平台调起批量任务
        cn.com.yusys.yusp.dto.server.batch.xdpl0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.batch.xdpl0001.resp.Data();// 响应Data：提供日终调度平台调起批量任务
        Xdpl0001RespDto xdpl0001RespDto = new Xdpl0001RespDto();
        try {
            // 从 xdpl0001ReqDto获取 reqData
            reqData = xdpl0001ReqDto.getData();
            // 将 reqData 拷贝到cmisbatch0010ReqDto
            BeanUtils.copyProperties(reqData, cmisbatch0010ReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0001.key, DscmsEnum.TRADE_CODE_XDPL0001.value, JSON.toJSONString(cmisbatch0010ReqDto));
            ResultDto<Cmisbatch0010RespDto> cmisbatch0010ResultDto = cmisBatchClientService.cmisbatch0010(cmisbatch0010ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0001.key, DscmsEnum.TRADE_CODE_XDPL0001.value, JSON.toJSONString(cmisbatch0010ResultDto));
            // 从返回值中获取响应码和响应消息
            xdpl0001RespDto.setErorcd(Optional.ofNullable(cmisbatch0010ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdpl0001RespDto.setErortx(Optional.ofNullable(cmisbatch0010ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisbatch0010ResultDto.getCode())) {
                cmisbatch0010RespDto = cmisbatch0010ResultDto.getData();
                xdpl0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdpl0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisbatch0010RespDto拷贝到 respData
                BeanUtils.copyProperties(cmisbatch0010RespDto, respData);
                // 此处增加判断，方便根据erorcd判断
                if (Objects.equals(respData.getOpFlag(), DscmsEnum.OP_FLAG_F.key)) {
                    xdpl0001RespDto.setErorcd(EpbEnum.EPB099999.key);
                    xdpl0001RespDto.setErortx(respData.getOpMessage());
                }
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0001.key, DscmsEnum.TRADE_CODE_XDPL0001.value, e.getMessage());
            xdpl0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdpl0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdpl0001RespDto.setDatasq(servsq);

        xdpl0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDPL0001.key, DscmsEnum.TRADE_CODE_XDPL0001.value, JSON.toJSONString(xdpl0001RespDto));
        return xdpl0001RespDto;
    }
}
