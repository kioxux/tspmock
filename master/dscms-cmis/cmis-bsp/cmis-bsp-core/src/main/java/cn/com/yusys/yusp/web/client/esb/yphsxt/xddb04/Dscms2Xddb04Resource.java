package cn.com.yusys.yusp.web.client.esb.yphsxt.xddb04;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb04.req.Xddb04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb04.resp.Xddb04RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.req.Xddb04ReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.resp.Xddb04RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询质押物信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2Xddb04Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xddb04Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xddb04
     * 交易描述：查询质押物信息
     *
     * @param xddb04ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xddb04")
    protected @ResponseBody
    ResultDto<Xddb04RespDto> xddb04(@Validated @RequestBody Xddb04ReqDto xddb04ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB04.key, EsbEnum.TRADE_CODE_XDDB04.value, JSON.toJSONString(xddb04ReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.resp.Service();
        Xddb04ReqService xddb04ReqService = new Xddb04ReqService();
        Xddb04RespService xddb04RespService = new Xddb04RespService();
        Xddb04RespDto xddb04RespDto = new Xddb04RespDto();
        ResultDto<Xddb04RespDto> xddb04ResultDto = new ResultDto<Xddb04RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xddb04ReqDto转换成reqService
            BeanUtils.copyProperties(xddb04ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDDB04.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xddb04ReqService.setService(reqService);
            // 将xddb04ReqService转换成xddb04ReqServiceMap
            Map xddb04ReqServiceMap = beanMapUtil.beanToMap(xddb04ReqService);
            context.put("tradeDataMap", xddb04ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB04.key, EsbEnum.TRADE_CODE_XDDB04.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDDB04.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB04.key, EsbEnum.TRADE_CODE_XDDB04.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xddb04RespService = beanMapUtil.mapToBean(tradeDataMap, Xddb04RespService.class, Xddb04RespService.class);
            respService = xddb04RespService.getService();
            //  将respService转换成Xddb04RespDto
            xddb04ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xddb04ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xddb04RespDto);
                xddb04ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb04ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xddb04ResultDto.setCode(EpbEnum.EPB099999.key);
                xddb04ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB04.key, EsbEnum.TRADE_CODE_XDDB04.value, e.getMessage());
            xddb04ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xddb04ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xddb04ResultDto.setData(xddb04RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB04.key, EsbEnum.TRADE_CODE_XDDB04.value, JSON.toJSONString(xddb04ResultDto));
        return xddb04ResultDto;
    }
}
