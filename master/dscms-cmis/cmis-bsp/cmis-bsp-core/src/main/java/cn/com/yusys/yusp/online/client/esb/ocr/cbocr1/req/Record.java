package cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 10:23
 * @since 2021/6/4 10:23
 */
public class Record {
    private String fileName;//上传文件名称
    private String filePath;//上传文件路径

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "Record{" +
                "fileName='" + fileName + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
