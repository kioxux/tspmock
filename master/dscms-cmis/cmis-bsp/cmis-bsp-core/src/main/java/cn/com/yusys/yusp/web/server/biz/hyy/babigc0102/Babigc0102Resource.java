package cn.com.yusys.yusp.web.server.biz.hyy.babigc0102;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.resp.Babigc0102RespDto;
import cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.resp.Items;
import cn.com.yusys.yusp.dto.server.biz.hyy.common.CollateralBaseInfo;
import cn.com.yusys.yusp.dto.server.xddb0016.req.Xddb0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0016.resp.List;
import cn.com.yusys.yusp.dto.server.xddb0016.resp.Xddb0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品信息查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "BA-BI-GC01:查询抵押信息列表")
@RestController
@RequestMapping("/bank")
public class Babigc0102Resource {
    private static final Logger logger = LoggerFactory.getLogger(Babigc0102Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizdbClientService;

    /**
     * 交易码：Babigc0102Resource
     * 交易描述：押品信息查询
     *
     * @param mortgagor_name
     * @return
     * @throws Exception
     */
    @ApiOperation("查询抵押信息列表")
    @GetMapping("/collaterals")
    protected @ResponseBody
    String babigc0102(@Validated @RequestParam String mortgagor_name, String page, String size) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(mortgagor_name));
        Xddb0016DataReqDto xddb0016DataReqDto = new Xddb0016DataReqDto();// 请求Data： 押品信息查询
        Xddb0016DataRespDto xddb0016DataRespDto = new Xddb0016DataRespDto();// 响应Data：押品信息查询
        Babigc0102RespDto babicr0102RespDto = new Babigc0102RespDto();
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.req.Data reqData = null; // 请求Data：押品信息查询
        java.util.List<Items> hyyBabigc0102Items = new ArrayList<>();
        cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.resp.Data();// 响应Data：押品信息查询
        try {
            //  此处包导入待确定 结束
            // 从 xddb0016ReqDto获取 reqData
            //reqData = babigc0102ReqDto.getData();
            // 将 reqData 拷贝到xddb0016DataReqDto
            xddb0016DataReqDto.setGuarna(mortgagor_name);
            xddb0016DataReqDto.setYppage(page);
            xddb0016DataReqDto.setYpsize(size);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xddb0016DataReqDto));
            ResultDto<Xddb0016DataRespDto> xddb0016DataResultDto = dscmsBizdbClientService.xddb0016(xddb0016DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xddb0016DataResultDto));
            // 从返回值中获取响应码和响应消息
            babicr0102RespDto.setCode(Optional.ofNullable(xddb0016DataResultDto.getCode()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            babicr0102RespDto.setMessage(Optional.ofNullable(xddb0016DataResultDto.getMessage()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            //CollateralBaseInfo collateralBaseInfo = null;//	抵押人

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0016DataResultDto.getCode())) {
                xddb0016DataRespDto = xddb0016DataResultDto.getData();
                // 将 xddb0016DataRespDto拷贝到 respData
                java.util.List<List> lists = xddb0016DataRespDto.getList();
                // 总条数
                Integer count = xddb0016DataRespDto.getList().size();

                if (count > 0) {
                    Items item = new Items();
                    for (int i = 0; i < lists.size(); i++) {
                        String collateral_id = lists.get(i).getCollid();//抵押唯一ID
                        String collateral_no = lists.get(i).getCollno();//抵押物编号
                        String collateral_type = lists.get(i).getCollty();//抵押物类型
                        String mortgagorName = lists.get(i).getMortna();//抵押人名称
                        String mortgagor_client_code = lists.get(i).getMoclco();//抵押人客户码
                        String mortgagor_document_number = lists.get(i).getModonu();//抵押人证件号码
                        BigDecimal collateral_value = lists.get(i).getCollva();//抵押物价值
                        String location = lists.get(i).getLocation();//面积
                        String area = lists.get(i).getArea();

                        //collateralBaseInfo = new CollateralBaseInfo();
                        item.setCollateral_id(collateral_id);
                        item.setCollateral_no(collateral_no);
                        item.setCollateral_type(collateral_type);
                        item.setMortgagor_name(mortgagorName);
                        item.setMortgagor_client_code(mortgagor_client_code);
                        item.setMortgagor_document_number(mortgagor_document_number);
                        item.setCollateral_location(location);
                        if (collateral_value == null) {
                            collateral_value = BigDecimal.ZERO;
                        }
                        item.setCollateral_value(collateral_value.toString());
                        item.setCollateral_area(area);
                        //item.setCollateralBaseInfo(collateralBaseInfo);
                        hyyBabigc0102Items.add(item);
                    }
                    respData.setItems(hyyBabigc0102Items);
                } else {

                }
                babicr0102RespDto.setCode(SuccessEnum.CODE_SUCCESS.key);
                babicr0102RespDto.setSuccess("true");
                babicr0102RespDto.setMessage(SuccessEnum.MESSAGE_SUCCESS.key);
                babicr0102RespDto.setCount(count);
            } else {
                babicr0102RespDto.setCode("BA-BI-GC01-00"); // 9999
                babicr0102RespDto.setSuccess("false");
                babicr0102RespDto.setMessage(EpbEnum.EPB099999.value);// 系统异常
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            babicr0102RespDto.setCode(EpbEnum.EPB099999.key); // 9999
            babicr0102RespDto.setMessage(EpbEnum.EPB099999.value);// 系统异常
        }

        babicr0102RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(babicr0102RespDto));
        return JSON.toJSONString(babicr0102RespDto);
    }
}
