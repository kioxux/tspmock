package cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdent.req;

/**
 * 请求Service：新入职人员信息登记
 */
public class Service {
    private String prcscd;//	处理码	,	是	,	接口交易码区分交易
    private String servtp;//交易渠道
    private String servsq;//由发起渠道生成的唯一标识
    private String userid;//用户ID
    private String brchno;//机构号
    private String pagenm;//第几页
    private String pagecd;//每页多少条
    private String qydate;//查询日期

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getPagenm() {
        return pagenm;
    }

    public void setPagenm(String pagenm) {
        this.pagenm = pagenm;
    }

    public String getPagecd() {
        return pagecd;
    }

    public void setPagecd(String pagecd) {
        this.pagecd = pagecd;
    }

    public String getQydate() {
        return qydate;
    }

    public void setQydate(String qydate) {
        this.qydate = qydate;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", pagenm='" + pagenm + '\'' +
                ", pagecd='" + pagecd + '\'' +
                ", qydate='" + qydate + '\'' +
                '}';
    }
}
