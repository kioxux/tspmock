package cn.com.yusys.yusp.online.client.esb.core.ln3064.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 14:10
 * @since 2021/6/7 14:10
 */
public class LstNbjjInfo {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3064.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstNbjjInfo{" +
                "record=" + record +
                '}';
    }
}
