package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.guar_list;

/**
 * <br>
 * 0.2ZRC:2021/9/9 9:00:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/9/9 9:00
 * @since 2021/9/9 9:00
 */
public class Record {

    private String guar_serno;//担保人流水号
    private String guar_name;//担保人名称
    private String guar_type;//担保人类型

    public String getGuar_serno() {
        return guar_serno;
    }

    public void setGuar_serno(String guar_serno) {
        this.guar_serno = guar_serno;
    }

    public String getGuar_name() {
        return guar_name;
    }

    public void setGuar_name(String guar_name) {
        this.guar_name = guar_name;
    }

    public String getGuar_type() {
        return guar_type;
    }

    public void setGuar_type(String guar_type) {
        this.guar_type = guar_type;
    }

    @Override
    public String toString() {
        return "Record{" +
                "guar_serno='" + guar_serno + '\'' +
                ", guar_name='" + guar_name + '\'' +
                ", guar_type='" + guar_type + '\'' +
                '}';
    }
}
