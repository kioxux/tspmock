package cn.com.yusys.yusp.web.client.esb.core.ln3066;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3066.req.Ln3066ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3066.resp.Ln3066RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3066.req.Ln3066ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3066.resp.Ln3066RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3066.resp.Record;
import cn.com.yusys.yusp.online.client.esb.core.ln3066.resp.Results;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3066)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3066Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3066Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3066
     * 交易描述：资产转让借据筛选
     *
     * @param ln3066ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3066:资产转让借据筛选")
    @PostMapping("/ln3066")
    protected @ResponseBody
    ResultDto<Ln3066RespDto> ln3066(@Validated @RequestBody Ln3066ReqDto ln3066ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3066.key, EsbEnum.TRADE_CODE_LN3066.value, JSON.toJSONString(ln3066ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3066.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3066.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3066.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3066.resp.Service();

        Ln3066ReqService ln3066ReqService = new Ln3066ReqService();
        Ln3066RespService ln3066RespService = new Ln3066RespService();
        Ln3066RespDto ln3066RespDto = new Ln3066RespDto();
        ResultDto<Ln3066RespDto> ln3066ResultDto = new ResultDto<Ln3066RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3066ReqDto转换成reqService
            BeanUtils.copyProperties(ln3066ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3066.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3066ReqService.setService(reqService);
            // 将ln3066ReqService转换成ln3066ReqServiceMap
            Map ln3066ReqServiceMap = beanMapUtil.beanToMap(ln3066ReqService);
            context.put("tradeDataMap", ln3066ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3066.key, EsbEnum.TRADE_CODE_LN3066.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3066.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3066.key, EsbEnum.TRADE_CODE_LN3066.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3066RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3066RespService.class, Ln3066RespService.class);
            respService = ln3066RespService.getService();

            ln3066ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3066ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3066RespDto
                BeanUtils.copyProperties(respService, ln3066RespDto);
                Results results = Optional.ofNullable(respService.getResults()).orElse(new Results());
                if (CollectionUtils.nonEmpty(results.getRecord())) {
                    List<Record> recordList = results.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3066.resp.Results> targetList = new ArrayList<>();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3066.resp.Results target = new cn.com.yusys.yusp.dto.client.esb.core.ln3066.resp.Results();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    ln3066RespDto.setResults(targetList);
                }
                ln3066ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3066ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3066ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3066ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3066.key, EsbEnum.TRADE_CODE_LN3066.value, e.getMessage());
            ln3066ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3066ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3066ResultDto.setData(ln3066RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3066.key, EsbEnum.TRADE_CODE_LN3066.value, JSON.toJSONString(ln3066ResultDto));
        return ln3066ResultDto;
    }
}
