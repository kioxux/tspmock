package cn.com.yusys.yusp.web.client.esb.core.ln3245;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3245.req.Ln3245ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3245.resp.Ln3245RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3245.req.Ln3245ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3245.resp.Ln3245RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3245)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3245Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3245Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3245
     * 交易描述：贷款客户经理批量移交
     *
     * @param ln3245ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3245:贷款客户经理批量移交")
    @PostMapping("/ln3245")
    protected @ResponseBody
    ResultDto<Ln3245RespDto> ln3245(@Validated @RequestBody Ln3245ReqDto ln3245ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3245.key, EsbEnum.TRADE_CODE_LN3245.value, JSON.toJSONString(ln3245ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3245.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3245.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3245.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3245.resp.Service();
        Ln3245ReqService ln3245ReqService = new Ln3245ReqService();
        Ln3245RespService ln3245RespService = new Ln3245RespService();
        Ln3245RespDto ln3245RespDto = new Ln3245RespDto();
        ResultDto<Ln3245RespDto> ln3245ResultDto = new ResultDto<Ln3245RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3245ReqDto转换成reqService
            BeanUtils.copyProperties(ln3245ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3245.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3245ReqService.setService(reqService);
            // 将ln3245ReqService转换成ln3245ReqServiceMap
            Map ln3245ReqServiceMap = beanMapUtil.beanToMap(ln3245ReqService);
            context.put("tradeDataMap", ln3245ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3245.key, EsbEnum.TRADE_CODE_LN3245.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3245.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3245.key, EsbEnum.TRADE_CODE_LN3245.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3245RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3245RespService.class, Ln3245RespService.class);
            respService = ln3245RespService.getService();

            ln3245ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3245ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3245RespDto
                BeanUtils.copyProperties(respService, ln3245RespDto);
                cn.com.yusys.yusp.online.client.esb.core.ln3245.resp.LstHxDkxx lstHxDkxx = Optional.ofNullable(respService.getLstHxDkxx()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3245.resp.LstHxDkxx());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3245.resp.Record> records = lstHxDkxx.getRecord();
                if (CollectionUtils.nonEmpty(records)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3245.resp.LstHxDkxx> respList =  records.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3245.resp.LstHxDkxx temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3245.resp.LstHxDkxx();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3245RespDto.setLstHxDkxx(respList);
                }
                ln3245ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3245ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3245ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3245ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3245.key, EsbEnum.TRADE_CODE_LN3245.value, e.getMessage());
            ln3245ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3245ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3245ResultDto.setData(ln3245RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3245.key, EsbEnum.TRADE_CODE_LN3245.value, JSON.toJSONString(ln3245ResultDto));
        return ln3245ResultDto;
    }

}
