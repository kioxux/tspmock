package cn.com.yusys.yusp.online.client.esb.core.ib1253.resp;


/**
 * 响应Service：根据账号查询帐户信息接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16上午10:43:59
 */
public class Ib1253RespService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
