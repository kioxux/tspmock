package cn.com.yusys.yusp.web.server.biz.xdqt0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdqt0003.req.Xdqt0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdqt0003.resp.Xdqt0003RespDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.req.Xdqt0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0003.resp.Xdqt0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:企业网银推送预约信息
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0003:企业网银推送预约信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0003Resource.class);

    @Autowired
	private DscmsBizQtClientService dscmsBizQtClientService;

    /**
     * 交易码：xdqt0003
     * 交易描述：企业网银推送预约信息
     *
     * @param xdqt0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("企业网银推送预约信息")
    @PostMapping("/xdqt0003")
    //@Idempotent({"xdcaqt0003", "#xdqt0003ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0003RespDto xdqt0003(@Validated @RequestBody Xdqt0003ReqDto xdqt0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, JSON.toJSONString(xdqt0003ReqDto));
        Xdqt0003DataReqDto xdqt0003DataReqDto = new Xdqt0003DataReqDto();// 请求Data： 企业网银推送预约信息
        Xdqt0003DataRespDto xdqt0003DataRespDto = new Xdqt0003DataRespDto();// 响应Data：企业网银推送预约信息
        cn.com.yusys.yusp.dto.server.biz.xdqt0003.req.Data reqData = null; // 请求Data：企业网银推送预约信息
        cn.com.yusys.yusp.dto.server.biz.xdqt0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdqt0003.resp.Data();// 响应Data：企业网银推送预约信息
		Xdqt0003RespDto xdqt0003RespDto = new Xdqt0003RespDto();
		try {
            // 从 xdqt0003ReqDto获取 reqData
            reqData = xdqt0003ReqDto.getData();
            // 将 reqData 拷贝到xdqt0003DataReqDto
            BeanUtils.copyProperties(reqData, xdqt0003DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, JSON.toJSONString(xdqt0003DataReqDto));
            ResultDto<Xdqt0003DataRespDto> xdqt0003DataResultDto = dscmsBizQtClientService.xdqt0003(xdqt0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, JSON.toJSONString(xdqt0003DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0003RespDto.setErorcd(Optional.ofNullable(xdqt0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0003RespDto.setErortx(Optional.ofNullable(xdqt0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0003DataResultDto.getCode())) {
                xdqt0003DataRespDto = xdqt0003DataResultDto.getData();
                xdqt0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, e.getMessage());
            xdqt0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0003RespDto.setDatasq(servsq);

        xdqt0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0003.key, DscmsEnum.TRADE_CODE_XDQT0003.value, JSON.toJSONString(xdqt0003RespDto));
        return xdqt0003RespDto;
    }
}
