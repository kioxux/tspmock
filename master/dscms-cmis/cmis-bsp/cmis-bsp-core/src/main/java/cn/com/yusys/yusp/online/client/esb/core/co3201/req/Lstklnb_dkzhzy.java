package cn.com.yusys.yusp.online.client.esb.core.co3201.req;

import java.util.List;

public class Lstklnb_dkzhzy {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.co3201.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstklnb_dkzhzy{" +
                "record=" + record +
                '}';
    }
}
