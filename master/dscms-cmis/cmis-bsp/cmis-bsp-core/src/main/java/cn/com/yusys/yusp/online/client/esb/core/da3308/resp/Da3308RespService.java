package cn.com.yusys.yusp.online.client.esb.core.da3308.resp;

/**
 * 响应Service：抵债资产费用管理
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3308RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

