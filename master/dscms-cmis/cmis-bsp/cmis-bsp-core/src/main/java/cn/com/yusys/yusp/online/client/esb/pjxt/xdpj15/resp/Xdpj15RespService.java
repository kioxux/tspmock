package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj15.resp;

/**
 * 响应Service：发票补录信息推送（信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
public class Xdpj15RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
