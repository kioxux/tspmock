package cn.com.yusys.yusp.web.client.esb.xwywglpt.wxd003;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.req.Wxd003ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.resp.Wxd003RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷系统请求V平台二级准入接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "BSP封装小微业务管理平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Wxd003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Wxd003Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：wxd003
     * 交易描述：信贷系统请求V平台二级准入接口
     *
     * @param wxd003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("wxd003:信贷系统请求V平台二级准入接口")
    @PostMapping("/wxd003")
    protected @ResponseBody
    ResultDto<Wxd003RespDto> wxd003(@Validated @RequestBody Wxd003ReqDto wxd003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD003.key, EsbEnum.TRADE_CODE_WXD003.value, JSON.toJSONString(wxd003ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.resp.Service();
        Wxd003ReqService wxd003ReqService = new Wxd003ReqService();
        Wxd003RespService wxd003RespService = new Wxd003RespService();
        Wxd003RespDto wxd003RespDto = new Wxd003RespDto();
        ResultDto<Wxd003RespDto> wxd003ResultDto = new ResultDto<Wxd003RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将wxd003ReqDto转换成reqService
            BeanUtils.copyProperties(wxd003ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_WXD003.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            wxd003ReqService.setService(reqService);
            // 将wxd003ReqService转换成wxd003ReqServiceMap
            Map wxd003ReqServiceMap = beanMapUtil.beanToMap(wxd003ReqService);
            context.put("tradeDataMap", wxd003ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD003.key, EsbEnum.TRADE_CODE_WXD003.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_WXD003.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD003.key, EsbEnum.TRADE_CODE_WXD003.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            wxd003RespService = beanMapUtil.mapToBean(tradeDataMap, Wxd003RespService.class, Wxd003RespService.class);
            respService = wxd003RespService.getService();

            wxd003ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            wxd003ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, wxd003RespDto);
                wxd003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                wxd003ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                wxd003ResultDto.setCode(EpbEnum.EPB099999.key);
                wxd003ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD003.key, EsbEnum.TRADE_CODE_WXD003.value, e.getMessage());
            wxd003ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            wxd003ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        wxd003ResultDto.setData(wxd003RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD003.key, EsbEnum.TRADE_CODE_WXD003.value, JSON.toJSONString(wxd003ResultDto));
        return wxd003ResultDto;
    }
}
