package cn.com.yusys.yusp.web.server.biz.xdht0042;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0042.req.Xdht0042ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0042.resp.Xdht0042RespDto;
import cn.com.yusys.yusp.dto.server.xdht0042.req.Xdht0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0042.resp.Xdht0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷、优农贷合同信息查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDHT0042:优企贷、优农贷合同信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0042Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0042Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0042
     * 交易描述：优企贷、优农贷合同信息查询
     *
     * @param xdht0042ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷合同信息查询")
    @PostMapping("/xdht0042")
    //@Idempotent({"xdcaht0042", "#xdht0042ReqDto.datasq"})
    protected @ResponseBody
    Xdht0042RespDto xdht0042(@Validated @RequestBody Xdht0042ReqDto xdht0042ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value, JSON.toJSONString(xdht0042ReqDto));
        Xdht0042DataReqDto xdht0042DataReqDto = new Xdht0042DataReqDto();// 请求Data： 优企贷、优农贷合同信息查询
        Xdht0042DataRespDto xdht0042DataRespDto = new Xdht0042DataRespDto();// 响应Data：优企贷、优农贷合同信息查询
        Xdht0042RespDto xdht0042RespDto = new Xdht0042RespDto();
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdht0042.req.Data reqData = null; // 请求Data：优企贷、优农贷合同信息查询
        cn.com.yusys.yusp.dto.server.biz.xdht0042.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0042.resp.Data();// 响应Data：优企贷、优农贷合同信息查询
        //  此处包导入待确定 结束
        try {
            // 从 xdht0042ReqDto获取 reqData
            reqData = xdht0042ReqDto.getData();
            // 将 reqData 拷贝到xdht0042DataReqDto
            BeanUtils.copyProperties(reqData, xdht0042DataReqDto);
            // 调用服务：
            ResultDto<Xdht0042DataRespDto> xdht0042DataResultDto = dscmsBizHtClientService.xdht0042(xdht0042DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdht0042RespDto.setErorcd(Optional.ofNullable(xdht0042DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0042RespDto.setErortx(Optional.ofNullable(xdht0042DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0042DataResultDto.getCode())) {
                xdht0042DataRespDto = xdht0042DataResultDto.getData();
                // 将 xdht0042DataRespDto拷贝到 respData
                xdht0042RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0042RespDto.setErortx(SuccessEnum.SUCCESS.value);
                BeanUtils.copyProperties(xdht0042DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0041.key, DscmsEnum.TRADE_CODE_XDHT0041.value, e.getMessage());
            xdht0042RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0042RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0042RespDto.setDatasq(servsq);
        xdht0042RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0042.key, DscmsEnum.TRADE_CODE_XDHT0042.value, JSON.toJSONString(xdht0042RespDto));
        return xdht0042RespDto;
    }
}
