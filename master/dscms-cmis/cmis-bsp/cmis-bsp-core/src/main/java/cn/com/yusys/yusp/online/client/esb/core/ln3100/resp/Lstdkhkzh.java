package cn.com.yusys.yusp.online.client.esb.core.ln3100.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款多还款账户
 *
 * @author chenyong
 * @version 1.0
 */
public class Lstdkhkzh {
    private Integer youxianj;//优先级
    private String hkzhhmch;//还款账户名称
    private String hkzhhzhl;//还款账户种类
    private String hkzhhgze;//还款账户规则
    private String hkjshzhl;//还款基数种类
    private BigDecimal huankbli;//还款比例

    public Integer getYouxianj() {
        return youxianj;
    }

    public void setYouxianj(Integer youxianj) {
        this.youxianj = youxianj;
    }

    public String getHkzhhmch() {
        return hkzhhmch;
    }

    public void setHkzhhmch(String hkzhhmch) {
        this.hkzhhmch = hkzhhmch;
    }

    public String getHkzhhzhl() {
        return hkzhhzhl;
    }

    public void setHkzhhzhl(String hkzhhzhl) {
        this.hkzhhzhl = hkzhhzhl;
    }

    public String getHkzhhgze() {
        return hkzhhgze;
    }

    public void setHkzhhgze(String hkzhhgze) {
        this.hkzhhgze = hkzhhgze;
    }

    public String getHkjshzhl() {
        return hkjshzhl;
    }

    public void setHkjshzhl(String hkjshzhl) {
        this.hkjshzhl = hkjshzhl;
    }

    public BigDecimal getHuankbli() {
        return huankbli;
    }

    public void setHuankbli(BigDecimal huankbli) {
        this.huankbli = huankbli;
    }

    @Override
    public String toString() {
        return "Service{" +
                "youxianj='" + youxianj + '\'' +
                "hkzhhmch='" + hkzhhmch + '\'' +
                "hkzhhzhl='" + hkzhhzhl + '\'' +
                "hkzhhgze='" + hkzhhgze + '\'' +
                "hkjshzhl='" + hkjshzhl + '\'' +
                "huankbli='" + huankbli + '\'' +
                '}';
    }
}
