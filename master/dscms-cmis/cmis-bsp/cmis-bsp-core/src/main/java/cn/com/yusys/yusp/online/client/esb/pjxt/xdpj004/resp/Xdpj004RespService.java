package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.resp;

/**
 * 响应Service：承兑签发审批结果综合服务接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xdpj004RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdpj004RespService{" +
                "service=" + service +
                '}';
    }
}
