package cn.com.yusys.yusp.web.server.cus.xdkh0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0005.req.Xdkh0005ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0005.resp.Xdkh0005RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.req.Xdkh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0005.resp.Xdkh0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:同业客户信息查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDKH0005:同业客户信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0005Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0005
     * 交易描述：同业客户信息查询
     *
     * @param xdkh0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("同业客户信息查询")
    @PostMapping("/xdkh0005")
    //@Idempotent({"xdcakh0005", "#xdkh0005ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0005RespDto xdkh0005(@Validated @RequestBody Xdkh0005ReqDto xdkh0005ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005ReqDto));
        Xdkh0005DataReqDto xdkh0005DataReqDto = new Xdkh0005DataReqDto();// 请求Data： 同业客户信息查询
        Xdkh0005DataRespDto xdkh0005DataRespDto = null;// 响应Data： 同业客户信息查询
        Xdkh0005RespDto xdkh0005RespDto = new Xdkh0005RespDto();// 响应DTO：同业客户信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0005.req.Data reqData = null; // 请求Data：同业客户信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0005.resp.Data();// 响应Data：同业客户信息查询
        try {
            // 从 xdkh0005ReqDto获取 reqData
            reqData = xdkh0005ReqDto.getData();
            //将 reqData 拷贝到xdkh0005DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0005DataReqDto);

            // 调用服务：cmis_cus
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005ReqDto));
            ResultDto<Xdkh0005DataRespDto> xdkh0005DataResultDto = dscmsCusClientService.xdkh0005(xdkh0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0005RespDto.setErorcd(Optional.ofNullable(xdkh0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0005RespDto.setErortx(Optional.ofNullable(xdkh0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0005DataResultDto.getCode())) {
                xdkh0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0005DataRespDto = xdkh0005DataResultDto.getData();
                // 将 xdkh0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, e.getMessage());
            xdkh0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0005RespDto.setDatasq(servsq);

        xdkh0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0005.key, DscmsEnum.TRADE_CODE_XDKH0005.value, JSON.toJSONString(xdkh0005RespDto));
        return xdkh0005RespDto;
    }
}
