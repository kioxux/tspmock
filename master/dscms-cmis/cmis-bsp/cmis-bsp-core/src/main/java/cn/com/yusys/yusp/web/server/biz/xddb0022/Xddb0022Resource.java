package cn.com.yusys.yusp.web.server.biz.xddb0022;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0022.req.Xddb0022ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0022.resp.Xddb0022RespDto;
import cn.com.yusys.yusp.dto.server.xddb0022.req.Xddb0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0022.resp.Xddb0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据押品编号查询核心及信贷系统有无押品数据
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDDB0022:根据押品编号查询核心及信贷系统有无押品数据")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0022Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0022
     * 交易描述：根据押品编号查询核心及信贷系统有无押品数据
     *
     * @param xddb0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据押品编号查询核心及信贷系统有无押品数据")
    @PostMapping("/xddb0022")
    //@Idempotent({"xddb0022", "#xddb0022ReqDto.datasq"})
    protected @ResponseBody
    Xddb0022RespDto xddb0022(@Validated @RequestBody Xddb0022ReqDto xddb0022ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022ReqDto));
        Xddb0022DataReqDto xddb0022DataReqDto = new Xddb0022DataReqDto();// 请求Data： 根据押品编号查询核心及信贷系统有无押品数据
        Xddb0022DataRespDto xddb0022DataRespDto = new Xddb0022DataRespDto();// 响应Data：根据押品编号查询核心及信贷系统有无押品数据
        Xddb0022RespDto xddb0022RespDto = new Xddb0022RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddb0022.req.Data reqData = null; // 请求Data：根据客户名查询抵押物类型
        cn.com.yusys.yusp.dto.server.biz.xddb0022.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0022.resp.Data();// 响应Data：根据押品编号查询核心及信贷系统有无押品数据
        try {
            // 从 xddb0022ReqDto获取 reqData
            reqData = xddb0022ReqDto.getData();
            // 将 reqData 拷贝到xddb0022DataReqDto
            BeanUtils.copyProperties(reqData, xddb0022DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022DataReqDto));
            ResultDto<Xddb0022DataRespDto> xddb0022DataResultDto = dscmsBizDbClientService.xddb0022(xddb0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0022RespDto.setErorcd(Optional.ofNullable(xddb0022DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0022RespDto.setErortx(Optional.ofNullable(xddb0022DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0022DataResultDto.getCode())) {
                xddb0022DataRespDto = xddb0022DataResultDto.getData();
                xddb0022RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0022RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0022DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0022DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, e.getMessage());
            xddb0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0022RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0022RespDto.setDatasq(servsq);

        xddb0022RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0022.key, DscmsEnum.TRADE_CODE_XDDB0022.value, JSON.toJSONString(xddb0022RespDto));
        return xddb0022RespDto;
    }
}
