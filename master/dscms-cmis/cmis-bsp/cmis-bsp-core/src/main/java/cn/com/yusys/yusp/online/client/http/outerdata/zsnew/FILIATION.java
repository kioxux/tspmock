package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 分支机构
 */
@JsonPropertyOrder(alphabetic = true)
public class FILIATION implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "BRNAME")
    private String BRNAME;//分支机构名称
    @JsonProperty(value = "BRN_CREDIT_CODE")
    private String BRN_CREDIT_CODE;//分支机构统一社会信用代码
    @JsonProperty(value = "BRN_REG_ORG")
    private String BRN_REG_ORG;//分支机构登记机关
    @JsonProperty(value = "BRREGNO")
    private String BRREGNO;//分支机构企业注册号

    @JsonIgnore
    public String getBRNAME() {
        return BRNAME;
    }

    @JsonIgnore
    public void setBRNAME(String BRNAME) {
        this.BRNAME = BRNAME;
    }

    @JsonIgnore
    public String getBRN_CREDIT_CODE() {
        return BRN_CREDIT_CODE;
    }

    @JsonIgnore
    public void setBRN_CREDIT_CODE(String BRN_CREDIT_CODE) {
        this.BRN_CREDIT_CODE = BRN_CREDIT_CODE;
    }

    @JsonIgnore
    public String getBRN_REG_ORG() {
        return BRN_REG_ORG;
    }

    @JsonIgnore
    public void setBRN_REG_ORG(String BRN_REG_ORG) {
        this.BRN_REG_ORG = BRN_REG_ORG;
    }

    @JsonIgnore
    public String getBRREGNO() {
        return BRREGNO;
    }

    @JsonIgnore
    public void setBRREGNO(String BRREGNO) {
        this.BRREGNO = BRREGNO;
    }

    @Override
    public String toString() {
        return "FILIATION{" +
                "BRNAME='" + BRNAME + '\'' +
                ", BRN_CREDIT_CODE='" + BRN_CREDIT_CODE + '\'' +
                ", BRN_REG_ORG='" + BRN_REG_ORG + '\'' +
                ", BRREGNO='" + BRREGNO + '\'' +
                '}';
    }
}
