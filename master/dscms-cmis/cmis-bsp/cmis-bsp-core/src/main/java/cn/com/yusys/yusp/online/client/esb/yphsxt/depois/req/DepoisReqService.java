package cn.com.yusys.yusp.online.client.esb.yphsxt.depois.req;

/**
 * 请求Service：存单信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class DepoisReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "DepoisReqService{" +
                "service=" + service +
                '}';
    }
}
