package cn.com.yusys.yusp.online.client.esb.core.da3320.req;

/**
 * 请求Service：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Da3320ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3320ReqService{" +
                "service=" + service +
                '}';
    }
}
