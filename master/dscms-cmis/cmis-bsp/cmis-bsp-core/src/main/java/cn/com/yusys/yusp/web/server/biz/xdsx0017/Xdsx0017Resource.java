package cn.com.yusys.yusp.web.server.biz.xdsx0017;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0017.req.Xdsx0017ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0017.resp.Xdsx0017RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0017.req.Xdsx0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0017.resp.Xdsx0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷提供风控查询授信信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0017:信贷提供风控查询授信信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0017Resource.class);
    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0017
     * 交易描述：信贷提供风控查询授信信息
     *
     * @param xdsx0017ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷提供风控查询授信信息")
    @PostMapping("/xdsx0017")
    //@Idempotent({"xdcasx0017", "#xdsx0017ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0017RespDto xdsx0017(@Validated @RequestBody Xdsx0017ReqDto xdsx0017ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017ReqDto));
        Xdsx0017DataReqDto xdsx0017DataReqDto = new Xdsx0017DataReqDto();// 请求Data： 信贷提供风控查询授信信息
        Xdsx0017DataRespDto xdsx0017DataRespDto = new Xdsx0017DataRespDto();// 响应Data：信贷提供风控查询授信信息
        Xdsx0017RespDto xdsx0017RespDto = new Xdsx0017RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdsx0017.req.Data reqData = null; // 请求Data：信贷提供风控查询授信信息
        cn.com.yusys.yusp.dto.server.biz.xdsx0017.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0017.resp.Data();// 响应Data：信贷提供风控查询授信信息
        try {
            // 从 xdsx0017ReqDto获取 reqData
            reqData = xdsx0017ReqDto.getData();
            // 将 reqData 拷贝到xdsx0017DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017DataReqDto));
            ResultDto<Xdsx0017DataRespDto> xdsx0017DataResultDto = dscmsBizSxClientService.xdsx0017(xdsx0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0017RespDto.setErorcd(Optional.ofNullable(xdsx0017DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0017RespDto.setErortx(Optional.ofNullable(xdsx0017DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0017DataResultDto.getCode())) {
                xdsx0017DataRespDto = xdsx0017DataResultDto.getData();
                xdsx0017RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0017RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0017DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0017DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, e.getMessage());
            xdsx0017RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0017RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0017RespDto.setDatasq(servsq);

        xdsx0017RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0017.key, DscmsEnum.TRADE_CODE_XDSX0017.value, JSON.toJSONString(xdsx0017RespDto));
        return xdsx0017RespDto;
    }
}
