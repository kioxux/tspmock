package cn.com.yusys.yusp.web.server.biz.xddh0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0002.req.Xddh0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0002.resp.Xddh0002RespDto;
import cn.com.yusys.yusp.dto.server.xddh0002.req.Xddh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0002.resp.Xddh0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:新增主动还款申请记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0002:新增主动还款申请记录")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0002Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0002
     * 交易描述：新增主动还款申请记录
     *
     * @param xddh0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("新增主动还款申请记录")
    @PostMapping("/xddh0002")
//@Idempotent({"xddh0002", "#xddh0002ReqDto.datasq"})
    protected @ResponseBody
    Xddh0002RespDto xddh0002(@Validated @RequestBody Xddh0002ReqDto xddh0002ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002ReqDto));
        Xddh0002DataReqDto xddh0002DataReqDto = new Xddh0002DataReqDto();// 请求Data： 新增主动还款申请记录
        Xddh0002DataRespDto xddh0002DataRespDto = new Xddh0002DataRespDto();// 响应Data：新增主动还款申请记录
        Xddh0002RespDto xddh0002RespDto = new Xddh0002RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0002.req.Data reqData = null; // 请求Data：新增主动还款申请记录
        cn.com.yusys.yusp.dto.server.biz.xddh0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0002.resp.Data();// 响应Data：新增主动还款申请记录
        try {
            // 从 xddh0002ReqDto获取 reqData
            reqData = xddh0002ReqDto.getData();
            // 将 reqData 拷贝到xddh0002DataReqDto
            BeanUtils.copyProperties(reqData, xddh0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002DataReqDto));
            ResultDto<Xddh0002DataRespDto> xddh0002DataResultDto = dscmsBizDhClientService.xddh0002(xddh0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0002RespDto.setErorcd(Optional.ofNullable(xddh0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0002RespDto.setErortx(Optional.ofNullable(xddh0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0002DataResultDto.getCode())) {
                xddh0002DataRespDto = xddh0002DataResultDto.getData();
                xddh0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, e.getMessage());
            xddh0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0002RespDto.setDatasq(servsq);

        xddh0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0002.key, DscmsEnum.TRADE_CODE_XDDH0002.value, JSON.toJSONString(xddh0002RespDto));
        return xddh0002RespDto;
    }
}
