package cn.com.yusys.yusp.online.client.esb.core.ln3103.req;

/**
 * 请求Service：贷款账户交易明细查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3103ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3103ReqService{" +
                "service=" + service +
                '}';
    }
}
