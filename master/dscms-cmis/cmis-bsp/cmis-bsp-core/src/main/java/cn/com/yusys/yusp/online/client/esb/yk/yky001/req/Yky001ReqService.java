package cn.com.yusys.yusp.online.client.esb.yk.yky001.req;



/**
 * 请求Service：用印申请
 *
 * @author xuwh
 * @version 1.0
 * @since 2021/8/18下午20:41:07
 */
public class Yky001ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
