package cn.com.yusys.yusp.online.client.esb.core.ln3065.resp;

/**
 * 响应Service：资产转让资金划转
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3065RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3065RespService{" +
                "service=" + service +
                '}';
    }
}
