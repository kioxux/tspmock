package cn.com.yusys.yusp.web.client.esb.rircp.fbxd09;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd09.Fbxd09ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd09.Fbxd09RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.req.Fbxd09ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.resp.Fbxd09RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd09）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd09Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd09Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd09
     * 交易描述：查询日初（合约）信息历史表（利翃）总数据量
     *
     * @param fbxd09ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd09")
    protected @ResponseBody
    ResultDto<Fbxd09RespDto> fbxd09(@Validated @RequestBody Fbxd09ReqDto fbxd09ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD09.key, EsbEnum.TRADE_CODE_FBXD09.value, JSON.toJSONString(fbxd09ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd09.resp.Service();
        Fbxd09ReqService fbxd09ReqService = new Fbxd09ReqService();
        Fbxd09RespService fbxd09RespService = new Fbxd09RespService();
        Fbxd09RespDto fbxd09RespDto = new Fbxd09RespDto();
        ResultDto<Fbxd09RespDto> fbxd09ResultDto = new ResultDto<Fbxd09RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd09ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd09ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD09.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd09ReqService.setService(reqService);
            // 将fbxd09ReqService转换成fbxd09ReqServiceMap
            Map fbxd09ReqServiceMap = beanMapUtil.beanToMap(fbxd09ReqService);
            context.put("tradeDataMap", fbxd09ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD09.key, EsbEnum.TRADE_CODE_FBXD09.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD09.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD09.key, EsbEnum.TRADE_CODE_FBXD09.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd09RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd09RespService.class, Fbxd09RespService.class);
            respService = fbxd09RespService.getService();

            fbxd09ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd09ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd09RespDto
                BeanUtils.copyProperties(respService, fbxd09RespDto);
                fbxd09ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd09ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd09ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd09ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD09.key, EsbEnum.TRADE_CODE_FBXD09.value, e.getMessage());
            fbxd09ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd09ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd09ResultDto.setData(fbxd09RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD09.key, EsbEnum.TRADE_CODE_FBXD09.value, JSON.toJSONString(fbxd09ResultDto));
        return fbxd09ResultDto;
    }
}
