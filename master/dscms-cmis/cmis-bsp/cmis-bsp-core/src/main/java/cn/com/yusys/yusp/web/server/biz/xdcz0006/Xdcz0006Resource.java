package cn.com.yusys.yusp.web.server.biz.xdcz0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0006.req.Xdcz0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0006.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0006.resp.Xdcz0006RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0006.req.Xdcz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0006.resp.Xdcz0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池入池接口
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0006:资产池入池接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0006Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0006
     * 交易描述：资产池入池接口
     *
     * @param xdcz0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池入池接口")
    @PostMapping("/xdcz0006")
   //@Idempotent({"xdcz0006", "#xdcz0006ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0006RespDto xdcz0006(@Validated @RequestBody Xdcz0006ReqDto xdcz0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006ReqDto));
        Xdcz0006DataReqDto xdcz0006DataReqDto = new Xdcz0006DataReqDto();// 请求Data： 电子保函开立
        Xdcz0006DataRespDto xdcz0006DataRespDto = new Xdcz0006DataRespDto();// 响应Data：电子保函开立
        cn.com.yusys.yusp.dto.server.biz.xdcz0006.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立
        Xdcz0006RespDto xdcz0006RespDto = new Xdcz0006RespDto();
        try {
            // 从 xdcz0006ReqDto获取 reqData
            reqData = xdcz0006ReqDto.getData();
            // 将 reqData 拷贝到xdcz0006DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0006DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataReqDto));
            ResultDto<Xdcz0006DataRespDto> xdcz0006DataResultDto = dscmsBizCzClientService.xdcz0006(xdcz0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0006RespDto.setErorcd(Optional.ofNullable(xdcz0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0006RespDto.setErortx(Optional.ofNullable(xdcz0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0006DataResultDto.getCode())) {
                xdcz0006DataRespDto = xdcz0006DataResultDto.getData();
                xdcz0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, e.getMessage());
            xdcz0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0006RespDto.setDatasq(servsq);

        xdcz0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0006.key, DscmsEnum.TRADE_CODE_XDCZ0006.value, JSON.toJSONString(xdcz0006RespDto));
        return xdcz0006RespDto;
    }
}
