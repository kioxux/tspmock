package cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp;

/**
 * 响应Service：新增批次报表数据
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应代码
    private String erortx;//响应信息

    private String success;//是否请求成功
    private String taskId;//返回结果：任务id

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((erorcd == null) ? 0 : erorcd.hashCode());
		result = prime * result + ((erortx == null) ? 0 : erortx.hashCode());
		result = prime * result + ((success == null) ? 0 : success.hashCode());
		result = prime * result + ((taskId == null) ? 0 : taskId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (erorcd == null) {
			if (other.erorcd != null)
				return false;
		} else if (!erorcd.equals(other.erorcd))
			return false;
		if (erortx == null) {
			if (other.erortx != null)
				return false;
		} else if (!erortx.equals(other.erortx))
			return false;
		if (success == null) {
			if (other.success != null)
				return false;
		} else if (!success.equals(other.success))
			return false;
		if (taskId == null) {
			if (other.taskId != null)
				return false;
		} else if (!taskId.equals(other.taskId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Service [erorcd=" + erorcd + ", erortx=" + erortx
				+ ", success=" + success + ", taskId=" + taskId + "]";
	}
   
}
