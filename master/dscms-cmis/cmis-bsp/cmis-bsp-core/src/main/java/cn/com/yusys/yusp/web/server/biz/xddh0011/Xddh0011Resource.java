package cn.com.yusys.yusp.web.server.biz.xddh0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0011.req.Xddh0011ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0011.resp.Xddh0011RespDto;
import cn.com.yusys.yusp.dto.server.xddh0011.req.Xddh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0011.resp.Xddh0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:推送优享贷预警信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0011:推送优享贷预警信息")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0011Resource.class);
	@Autowired
	private DscmsBizDhClientService dscmsBizDhClientService;
    /**
     * 交易码：xddh0011
     * 交易描述：推送优享贷预警信息
     *
     * @param xddh0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送优享贷预警信息")
    @PostMapping("/xddh0011")
@Idempotent({"xddh0011", "#xddh0011ReqDto.datasq"})
    protected @ResponseBody
	Xddh0011RespDto xddh0011(@Validated @RequestBody Xddh0011ReqDto xddh0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0011ReqDto));
        Xddh0011DataReqDto xddh0011DataReqDto = new Xddh0011DataReqDto();// 请求Data： 推送优享贷预警信息
        Xddh0011DataRespDto xddh0011DataRespDto = new Xddh0011DataRespDto();// 响应Data：推送优享贷预警信息
		Xddh0011RespDto xddh0011RespDto=new Xddh0011RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0011.req.Data reqData = null; // 请求Data：推送优享贷预警信息
        cn.com.yusys.yusp.dto.server.biz.xddh0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0011.resp.Data();// 响应Data：推送优享贷预警信息
        try {
            // 从 xddh0011ReqDto获取 reqData
            reqData = xddh0011ReqDto.getData();
            // 将 reqData 拷贝到xddh0011DataReqDto
            BeanUtils.copyProperties(reqData, xddh0011DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0011DataReqDto));
            ResultDto<Xddh0011DataRespDto> xddh0011DataResultDto = dscmsBizDhClientService.xddh0011(xddh0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0011DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0011RespDto.setErorcd(Optional.ofNullable(xddh0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0011RespDto.setErortx(Optional.ofNullable(xddh0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0011DataResultDto.getCode())) {
                xddh0011DataRespDto = xddh0011DataResultDto.getData();
                xddh0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0011DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, e.getMessage());
            xddh0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0011RespDto.setDatasq(servsq);

        xddh0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0011.key, DscmsEnum.TRADE_CODE_XDDH0011.value, JSON.toJSONString(xddh0011RespDto));
        return xddh0011RespDto;
    }
}
