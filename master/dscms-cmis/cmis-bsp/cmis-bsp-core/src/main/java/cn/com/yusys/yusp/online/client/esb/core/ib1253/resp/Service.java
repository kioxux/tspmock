package cn.com.yusys.yusp.online.client.esb.core.ib1253.resp;

/**
 * 响应Service：根据账号查询帐户信息接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16上午10:43:59
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String xitozhho; // 系统账号
    private String mokuaiii; // 模块
    private String zhhuzwmc; // 账户名称
    private String kehuhaoo; // 客户号
    private String shjihaom; // 手机号码
    private String zhaoxhao; // 账户序号
    private String yingyjig; // 营业机构
    private String huobdaih; // 货币代号
    private String chaohubz; // 账户钞汇标志
    private String yewudhao; // 业务代号
    private String yewubima; // 业务编码
    private String zhhuywzl; // 账户业务种类
    private String zhkhjigo; // 账户开户机构
    private String kaihriqi; // 开户日期
    private String kaihguiy; // 账户开户柜员
    private String xiohriqi; // 账户销户日期
    private String xiohguiy; // 账户销户柜员
    private String xiohjigo; // 账户销户机构
    private String youxriqi; // 账户有效期
    private String zhhuyuee; // 当前账户余额
    private String shrizhye; // 上日账户余额
    private String dzhqlixi; // 待支取利息
    private String zhlczxye; // 账户留存最小余额
    private String ljdonjje; // 累计冻结金额
    private String konzjine; // 控制金额
    private String zhhuztai; // 账户状态
    private String keyonedu; // 可用额度
    private String touzhibz; // 允许透支标志
    private String yxxjzqbz; // 允许现金支取标志
    private String yxzzzqbz; // 允许转账支取标志
    private String yxxjcrbz; // 允许现金存入标志
    private String yxzzcrbz; // 允许转账存入标志
    private String doqiriqi; // 到期日期
    private String kaihjine; // 开户金额
    private String cunkzlei; // 存款种类
    private String cunqiiii; // 存期
    private String chapbhao; // 产品编号
    private String fzcpleix; // 负债产品类型
    private String suoshudx; // 产品所属对象
    private String lxzffans; // 利息支付方式
    private String jitilixi; // 计提利息
    private String jishuuuu; // 积数
    private String yjyjlixi; // 应加/减利息
    private String yjyjjish; // 应加/减积数
    private String qixifans; // 起息方式
    private String qixiriqi; // 起息日期
    private String jixibioz; // 计息标志
    private String shcijxri; // 上次计息日
    private String shcifxri; // 上次付息日
    private String jixipnlv; // 计息频率
    private String jexipnlv; // 结息频率
    private String zhfutojn; // 支付条件
    private String shijzqcs; // 实际支取次数
    private String shijzqje; // 实际支取金额
    private String dczqzxje; // 单次支取最小金额
    private String dczqzdje; // 单次支取最大金额
    private String shijcrcs; // 实际存入次数
    private String shijcrje; // 实际存入金额
    private String dccrzxje; // 单次存入最小金额
    private String dccrzdje; // 单次存入最大金额
    private String lcuncshu; // 漏存次数
    private String tcunbzhi; // 通存标志
    private String tduibzhi; // 通兑标志
    private String tcunfwei; // 通存范围
    private String tduifwei; // 通兑范围
    private String daoqxcfs; // 到期续存方式
    private String bjzrkhzh; // 本金转入客户账号
    private String bjzrzhao; // 本金转入账号
    private String lxzrkhzh; // 利息转入客户账号
    private String lxzrzhao; // 利息转入账号
    private String zhufldm3; // 账户分类代码3
    private String lancreny; // 揽存人员
    private String zhphbzhi; // 支票户标志
    private String shfojshu; // 是否结算户
    private String dghushux; // 对公活期户属性
    private String tzhizlei; // 通知种类
    private String tonzriqi; // 通知日期
    private String waigzhxz; // 外管账户性质
    private String waihjgbz; // 外汇监管标志
    private String waihhcbz; // 外汇核查标志
    private String jibhhzho; // 基本户开户许可证号
    private String xiedckbz; // 协定存款标志
    private String tzlixibz; // 利率变化调整利息
    private String zdkzbzhi; // 自动扣帐标志
    private String lilvbhlx; // 利率编号类型
    private String lilvbhao; // 利率编号
    private String lilvdmlx; // 利率代码类型
    private String lilvfdbz; // 利率浮动标志
    private String lilvfdlx; // 利率浮动类型
    private String lilvfdsz; // 利率浮动值
    private String jizhunll; // 基准利率
    private String zhxililv; // 当前执行利率
    private String youhuibz; // 优惠标志
    private String youhuilx; // 优惠类型
    private String youhuisz; // 优惠值
    private String yegxriqi; // 余额最近更新日期
    private String scywriqi; // 上次业务日期
    private String kaihuqud; // 开户渠道
    private String zmqzhubz; // 自贸区账户标志
    private String zmswiflx; // 自贸区账户类型
    private String tglzhubz; // 托管类账户标志
    private String tuogzhlx; // 托管账户类型
    private String bfujzhbz; // 备付金账户标志
    private String bfjzhhlx; // 备付金账户类型
    private String czzhhubz; // 财政账户标志
    private String czckzhlx; // 财政存款账户类型
    private String hzbabzhi; // 核准备案标志
    private String weihguiy; // 维护柜员
    private String weihjigo; // 维护机构
    private String weihriqi; // 维护日期
    private String weihshij; // 维护时间
    private String shijchuo; // 时间戳
    private String jiluztai; // 记录状态
    private String beiyye01; // 备用余额01
    private String beiyye02; // 备用余额02
    private String beiyye03; // 备用余额03
    private String beiyzd01; // 备用字段01
    private String beiyzd02; // 备用字段02
    private String beiyzd03; // 备用字段03
    private String beiyrq01; // 备用日期1
    private String yifulixi; // 已付利息
    private String sfsmkehu; // 是否睡眠客户
    private String zhhubumn; // 账户部门
    private String bumenmch; // 部门名称
    private String zhfleidm; // 虚拟帐户类型（二三类账户）
    private String zhcunfsh; // 转存方式
    private String zhjedjbz; // 账户金额冻结标志
    private String zhfbdjbz; // 账户封闭冻结标志
    private String zhzsbfbz; // 账户只收不付标志
    private String zhzfbsbz; // 账户只付不收标志
    private String dspzsyzt; // 凭证状态
    private String mimaztai; // 密码状态
    private String bxzrzhxh; // 本/息入子账序号
    private String shifhnzh; // 是否行内账号
    private String shifznck; // 是否智能存款账户
    private String znckyuee; // 智能存款余额
    private String ilzhkhzh; // I类账户客户账号
    private String pingzhma; // 凭证号码
    private String pngzzlei; // 凭证种类
    private String qyxieyih; // 签约协议号
    private String rengbhao; // 认购编号
    private String decdzhzh; // 大额存单主账号
    private String dfhnhwbz; // 兑付账号行内行外标志
    private String dfruzhzh; // 兑付入账账号
    private String dfrzhhum; // 兑付入账户名
    private String dfruzhhh; // 兑付入账行号
    private String dfruzhhm; // 兑付入账行名
    private String shifdecd; // 是否大额存单
    private String shifszmm; // 是否设置密码
    private String diqumaaa; // 地区码
    private String zhiqjnge; // 支取间隔
    private String zhjnzlei; // 证件种类
    private String zhjhaoma; // 证件号码
    private String kehuleix; // 客户类型
    private String lilvstrs; // 利率字符型
    private String keyongye; // 可用余额
    private String kzjedjbz; // 客户账户金额冻结标志
    private String kzfbdjbz; // 客户账户封闭冻结标志
    private String kzzsbfbz; // 客户账户只收不付标志
    private String kzzfbsbz; // 客户账户只付不收标志
    private String kzhuztai; // 客户账户状态
    private String jnygyuee; // 江南盈余额
    private String shijbhao; // 影像编号
    private String jgzhhubz; // 监管账户标志
    private String meqiqkje; // 每期取款金额
    private String meiqckje; // 每期存款金额
    private String kezhfuje; // 可支付金额
    private String khakjine; // 可扣划金额
    private String yoxqjine; // 优先权金额
    private String sffwjgqy; // 房屋监管签约标志
    private String chanpshm; // 产品说明
    private String jysjzifu; // 交易时间字符串
    private String yxczbzhi; // 允许出账标志
    private String yxrzbzhi; // 允许入账标志
    private String zhufldm1; // 允许出账标志
    private String zhufldm2; // 允许入账标志

    public String getZhufldm1() {
        return zhufldm1;
    }

    public void setZhufldm1(String zhufldm1) {
        this.zhufldm1 = zhufldm1;
    }

    public String getZhufldm2() {
        return zhufldm2;
    }

    public void setZhufldm2(String zhufldm2) {
        this.zhufldm2 = zhufldm2;
    }



    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getXitozhho() {
        return xitozhho;
    }

    public void setXitozhho(String xitozhho) {
        this.xitozhho = xitozhho;
    }

    public String getMokuaiii() {
        return mokuaiii;
    }

    public void setMokuaiii(String mokuaiii) {
        this.mokuaiii = mokuaiii;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getShjihaom() {
        return shjihaom;
    }

    public void setShjihaom(String shjihaom) {
        this.shjihaom = shjihaom;
    }

    public String getZhaoxhao() {
        return zhaoxhao;
    }

    public void setZhaoxhao(String zhaoxhao) {
        this.zhaoxhao = zhaoxhao;
    }

    public String getYingyjig() {
        return yingyjig;
    }

    public void setYingyjig(String yingyjig) {
        this.yingyjig = yingyjig;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getYewudhao() {
        return yewudhao;
    }

    public void setYewudhao(String yewudhao) {
        this.yewudhao = yewudhao;
    }

    public String getYewubima() {
        return yewubima;
    }

    public void setYewubima(String yewubima) {
        this.yewubima = yewubima;
    }

    public String getZhhuywzl() {
        return zhhuywzl;
    }

    public void setZhhuywzl(String zhhuywzl) {
        this.zhhuywzl = zhhuywzl;
    }

    public String getZhkhjigo() {
        return zhkhjigo;
    }

    public void setZhkhjigo(String zhkhjigo) {
        this.zhkhjigo = zhkhjigo;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getXiohriqi() {
        return xiohriqi;
    }

    public void setXiohriqi(String xiohriqi) {
        this.xiohriqi = xiohriqi;
    }

    public String getXiohguiy() {
        return xiohguiy;
    }

    public void setXiohguiy(String xiohguiy) {
        this.xiohguiy = xiohguiy;
    }

    public String getXiohjigo() {
        return xiohjigo;
    }

    public void setXiohjigo(String xiohjigo) {
        this.xiohjigo = xiohjigo;
    }

    public String getYouxriqi() {
        return youxriqi;
    }

    public void setYouxriqi(String youxriqi) {
        this.youxriqi = youxriqi;
    }

    public String getZhhuyuee() {
        return zhhuyuee;
    }

    public void setZhhuyuee(String zhhuyuee) {
        this.zhhuyuee = zhhuyuee;
    }

    public String getShrizhye() {
        return shrizhye;
    }

    public void setShrizhye(String shrizhye) {
        this.shrizhye = shrizhye;
    }

    public String getDzhqlixi() {
        return dzhqlixi;
    }

    public void setDzhqlixi(String dzhqlixi) {
        this.dzhqlixi = dzhqlixi;
    }

    public String getZhlczxye() {
        return zhlczxye;
    }

    public void setZhlczxye(String zhlczxye) {
        this.zhlczxye = zhlczxye;
    }

    public String getLjdonjje() {
        return ljdonjje;
    }

    public void setLjdonjje(String ljdonjje) {
        this.ljdonjje = ljdonjje;
    }

    public String getKonzjine() {
        return konzjine;
    }

    public void setKonzjine(String konzjine) {
        this.konzjine = konzjine;
    }

    public String getZhhuztai() {
        return zhhuztai;
    }

    public void setZhhuztai(String zhhuztai) {
        this.zhhuztai = zhhuztai;
    }

    public String getKeyonedu() {
        return keyonedu;
    }

    public void setKeyonedu(String keyonedu) {
        this.keyonedu = keyonedu;
    }

    public String getTouzhibz() {
        return touzhibz;
    }

    public void setTouzhibz(String touzhibz) {
        this.touzhibz = touzhibz;
    }

    public String getYxxjzqbz() {
        return yxxjzqbz;
    }

    public void setYxxjzqbz(String yxxjzqbz) {
        this.yxxjzqbz = yxxjzqbz;
    }

    public String getYxzzzqbz() {
        return yxzzzqbz;
    }

    public void setYxzzzqbz(String yxzzzqbz) {
        this.yxzzzqbz = yxzzzqbz;
    }

    public String getYxxjcrbz() {
        return yxxjcrbz;
    }

    public void setYxxjcrbz(String yxxjcrbz) {
        this.yxxjcrbz = yxxjcrbz;
    }

    public String getYxzzcrbz() {
        return yxzzcrbz;
    }

    public void setYxzzcrbz(String yxzzcrbz) {
        this.yxzzcrbz = yxzzcrbz;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getKaihjine() {
        return kaihjine;
    }

    public void setKaihjine(String kaihjine) {
        this.kaihjine = kaihjine;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getFzcpleix() {
        return fzcpleix;
    }

    public void setFzcpleix(String fzcpleix) {
        this.fzcpleix = fzcpleix;
    }

    public String getSuoshudx() {
        return suoshudx;
    }

    public void setSuoshudx(String suoshudx) {
        this.suoshudx = suoshudx;
    }

    public String getLxzffans() {
        return lxzffans;
    }

    public void setLxzffans(String lxzffans) {
        this.lxzffans = lxzffans;
    }

    public String getJitilixi() {
        return jitilixi;
    }

    public void setJitilixi(String jitilixi) {
        this.jitilixi = jitilixi;
    }

    public String getJishuuuu() {
        return jishuuuu;
    }

    public void setJishuuuu(String jishuuuu) {
        this.jishuuuu = jishuuuu;
    }

    public String getYjyjlixi() {
        return yjyjlixi;
    }

    public void setYjyjlixi(String yjyjlixi) {
        this.yjyjlixi = yjyjlixi;
    }

    public String getYjyjjish() {
        return yjyjjish;
    }

    public void setYjyjjish(String yjyjjish) {
        this.yjyjjish = yjyjjish;
    }

    public String getQixifans() {
        return qixifans;
    }

    public void setQixifans(String qixifans) {
        this.qixifans = qixifans;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getJixibioz() {
        return jixibioz;
    }

    public void setJixibioz(String jixibioz) {
        this.jixibioz = jixibioz;
    }

    public String getShcijxri() {
        return shcijxri;
    }

    public void setShcijxri(String shcijxri) {
        this.shcijxri = shcijxri;
    }

    public String getShcifxri() {
        return shcifxri;
    }

    public void setShcifxri(String shcifxri) {
        this.shcifxri = shcifxri;
    }

    public String getJixipnlv() {
        return jixipnlv;
    }

    public void setJixipnlv(String jixipnlv) {
        this.jixipnlv = jixipnlv;
    }

    public String getJexipnlv() {
        return jexipnlv;
    }

    public void setJexipnlv(String jexipnlv) {
        this.jexipnlv = jexipnlv;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getShijzqcs() {
        return shijzqcs;
    }

    public void setShijzqcs(String shijzqcs) {
        this.shijzqcs = shijzqcs;
    }

    public String getShijzqje() {
        return shijzqje;
    }

    public void setShijzqje(String shijzqje) {
        this.shijzqje = shijzqje;
    }

    public String getDczqzxje() {
        return dczqzxje;
    }

    public void setDczqzxje(String dczqzxje) {
        this.dczqzxje = dczqzxje;
    }

    public String getDczqzdje() {
        return dczqzdje;
    }

    public void setDczqzdje(String dczqzdje) {
        this.dczqzdje = dczqzdje;
    }

    public String getShijcrcs() {
        return shijcrcs;
    }

    public void setShijcrcs(String shijcrcs) {
        this.shijcrcs = shijcrcs;
    }

    public String getShijcrje() {
        return shijcrje;
    }

    public void setShijcrje(String shijcrje) {
        this.shijcrje = shijcrje;
    }

    public String getDccrzxje() {
        return dccrzxje;
    }

    public void setDccrzxje(String dccrzxje) {
        this.dccrzxje = dccrzxje;
    }

    public String getDccrzdje() {
        return dccrzdje;
    }

    public void setDccrzdje(String dccrzdje) {
        this.dccrzdje = dccrzdje;
    }

    public String getLcuncshu() {
        return lcuncshu;
    }

    public void setLcuncshu(String lcuncshu) {
        this.lcuncshu = lcuncshu;
    }

    public String getTcunbzhi() {
        return tcunbzhi;
    }

    public void setTcunbzhi(String tcunbzhi) {
        this.tcunbzhi = tcunbzhi;
    }

    public String getTduibzhi() {
        return tduibzhi;
    }

    public void setTduibzhi(String tduibzhi) {
        this.tduibzhi = tduibzhi;
    }

    public String getTcunfwei() {
        return tcunfwei;
    }

    public void setTcunfwei(String tcunfwei) {
        this.tcunfwei = tcunfwei;
    }

    public String getTduifwei() {
        return tduifwei;
    }

    public void setTduifwei(String tduifwei) {
        this.tduifwei = tduifwei;
    }

    public String getDaoqxcfs() {
        return daoqxcfs;
    }

    public void setDaoqxcfs(String daoqxcfs) {
        this.daoqxcfs = daoqxcfs;
    }

    public String getBjzrkhzh() {
        return bjzrkhzh;
    }

    public void setBjzrkhzh(String bjzrkhzh) {
        this.bjzrkhzh = bjzrkhzh;
    }

    public String getBjzrzhao() {
        return bjzrzhao;
    }

    public void setBjzrzhao(String bjzrzhao) {
        this.bjzrzhao = bjzrzhao;
    }

    public String getLxzrkhzh() {
        return lxzrkhzh;
    }

    public void setLxzrkhzh(String lxzrkhzh) {
        this.lxzrkhzh = lxzrkhzh;
    }

    public String getLxzrzhao() {
        return lxzrzhao;
    }

    public void setLxzrzhao(String lxzrzhao) {
        this.lxzrzhao = lxzrzhao;
    }

    public String getZhufldm3() {
        return zhufldm3;
    }

    public void setZhufldm3(String zhufldm3) {
        this.zhufldm3 = zhufldm3;
    }

    public String getLancreny() {
        return lancreny;
    }

    public void setLancreny(String lancreny) {
        this.lancreny = lancreny;
    }

    public String getZhphbzhi() {
        return zhphbzhi;
    }

    public void setZhphbzhi(String zhphbzhi) {
        this.zhphbzhi = zhphbzhi;
    }

    public String getShfojshu() {
        return shfojshu;
    }

    public void setShfojshu(String shfojshu) {
        this.shfojshu = shfojshu;
    }

    public String getDghushux() {
        return dghushux;
    }

    public void setDghushux(String dghushux) {
        this.dghushux = dghushux;
    }

    public String getTzhizlei() {
        return tzhizlei;
    }

    public void setTzhizlei(String tzhizlei) {
        this.tzhizlei = tzhizlei;
    }

    public String getTonzriqi() {
        return tonzriqi;
    }

    public void setTonzriqi(String tonzriqi) {
        this.tonzriqi = tonzriqi;
    }

    public String getWaigzhxz() {
        return waigzhxz;
    }

    public void setWaigzhxz(String waigzhxz) {
        this.waigzhxz = waigzhxz;
    }

    public String getWaihjgbz() {
        return waihjgbz;
    }

    public void setWaihjgbz(String waihjgbz) {
        this.waihjgbz = waihjgbz;
    }

    public String getWaihhcbz() {
        return waihhcbz;
    }

    public void setWaihhcbz(String waihhcbz) {
        this.waihhcbz = waihhcbz;
    }

    public String getJibhhzho() {
        return jibhhzho;
    }

    public void setJibhhzho(String jibhhzho) {
        this.jibhhzho = jibhhzho;
    }

    public String getXiedckbz() {
        return xiedckbz;
    }

    public void setXiedckbz(String xiedckbz) {
        this.xiedckbz = xiedckbz;
    }

    public String getTzlixibz() {
        return tzlixibz;
    }

    public void setTzlixibz(String tzlixibz) {
        this.tzlixibz = tzlixibz;
    }

    public String getZdkzbzhi() {
        return zdkzbzhi;
    }

    public void setZdkzbzhi(String zdkzbzhi) {
        this.zdkzbzhi = zdkzbzhi;
    }

    public String getLilvbhlx() {
        return lilvbhlx;
    }

    public void setLilvbhlx(String lilvbhlx) {
        this.lilvbhlx = lilvbhlx;
    }

    public String getLilvbhao() {
        return lilvbhao;
    }

    public void setLilvbhao(String lilvbhao) {
        this.lilvbhao = lilvbhao;
    }

    public String getLilvdmlx() {
        return lilvdmlx;
    }

    public void setLilvdmlx(String lilvdmlx) {
        this.lilvdmlx = lilvdmlx;
    }

    public String getLilvfdbz() {
        return lilvfdbz;
    }

    public void setLilvfdbz(String lilvfdbz) {
        this.lilvfdbz = lilvfdbz;
    }

    public String getLilvfdlx() {
        return lilvfdlx;
    }

    public void setLilvfdlx(String lilvfdlx) {
        this.lilvfdlx = lilvfdlx;
    }

    public String getLilvfdsz() {
        return lilvfdsz;
    }

    public void setLilvfdsz(String lilvfdsz) {
        this.lilvfdsz = lilvfdsz;
    }

    public String getJizhunll() {
        return jizhunll;
    }

    public void setJizhunll(String jizhunll) {
        this.jizhunll = jizhunll;
    }

    public String getZhxililv() {
        return zhxililv;
    }

    public void setZhxililv(String zhxililv) {
        this.zhxililv = zhxililv;
    }

    public String getYouhuibz() {
        return youhuibz;
    }

    public void setYouhuibz(String youhuibz) {
        this.youhuibz = youhuibz;
    }

    public String getYouhuilx() {
        return youhuilx;
    }

    public void setYouhuilx(String youhuilx) {
        this.youhuilx = youhuilx;
    }

    public String getYouhuisz() {
        return youhuisz;
    }

    public void setYouhuisz(String youhuisz) {
        this.youhuisz = youhuisz;
    }

    public String getYegxriqi() {
        return yegxriqi;
    }

    public void setYegxriqi(String yegxriqi) {
        this.yegxriqi = yegxriqi;
    }

    public String getScywriqi() {
        return scywriqi;
    }

    public void setScywriqi(String scywriqi) {
        this.scywriqi = scywriqi;
    }

    public String getKaihuqud() {
        return kaihuqud;
    }

    public void setKaihuqud(String kaihuqud) {
        this.kaihuqud = kaihuqud;
    }

    public String getZmqzhubz() {
        return zmqzhubz;
    }

    public void setZmqzhubz(String zmqzhubz) {
        this.zmqzhubz = zmqzhubz;
    }

    public String getZmswiflx() {
        return zmswiflx;
    }

    public void setZmswiflx(String zmswiflx) {
        this.zmswiflx = zmswiflx;
    }

    public String getTglzhubz() {
        return tglzhubz;
    }

    public void setTglzhubz(String tglzhubz) {
        this.tglzhubz = tglzhubz;
    }

    public String getTuogzhlx() {
        return tuogzhlx;
    }

    public void setTuogzhlx(String tuogzhlx) {
        this.tuogzhlx = tuogzhlx;
    }

    public String getBfujzhbz() {
        return bfujzhbz;
    }

    public void setBfujzhbz(String bfujzhbz) {
        this.bfujzhbz = bfujzhbz;
    }

    public String getBfjzhhlx() {
        return bfjzhhlx;
    }

    public void setBfjzhhlx(String bfjzhhlx) {
        this.bfjzhhlx = bfjzhhlx;
    }

    public String getCzzhhubz() {
        return czzhhubz;
    }

    public void setCzzhhubz(String czzhhubz) {
        this.czzhhubz = czzhhubz;
    }

    public String getCzckzhlx() {
        return czckzhlx;
    }

    public void setCzckzhlx(String czckzhlx) {
        this.czckzhlx = czckzhlx;
    }

    public String getHzbabzhi() {
        return hzbabzhi;
    }

    public void setHzbabzhi(String hzbabzhi) {
        this.hzbabzhi = hzbabzhi;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getWeihshij() {
        return weihshij;
    }

    public void setWeihshij(String weihshij) {
        this.weihshij = weihshij;
    }

    public String getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(String shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    public String getBeiyye01() {
        return beiyye01;
    }

    public void setBeiyye01(String beiyye01) {
        this.beiyye01 = beiyye01;
    }

    public String getBeiyye02() {
        return beiyye02;
    }

    public void setBeiyye02(String beiyye02) {
        this.beiyye02 = beiyye02;
    }

    public String getBeiyye03() {
        return beiyye03;
    }

    public void setBeiyye03(String beiyye03) {
        this.beiyye03 = beiyye03;
    }

    public String getBeiyzd01() {
        return beiyzd01;
    }

    public void setBeiyzd01(String beiyzd01) {
        this.beiyzd01 = beiyzd01;
    }

    public String getBeiyzd02() {
        return beiyzd02;
    }

    public void setBeiyzd02(String beiyzd02) {
        this.beiyzd02 = beiyzd02;
    }

    public String getBeiyzd03() {
        return beiyzd03;
    }

    public void setBeiyzd03(String beiyzd03) {
        this.beiyzd03 = beiyzd03;
    }

    public String getBeiyrq01() {
        return beiyrq01;
    }

    public void setBeiyrq01(String beiyrq01) {
        this.beiyrq01 = beiyrq01;
    }

    public String getYifulixi() {
        return yifulixi;
    }

    public void setYifulixi(String yifulixi) {
        this.yifulixi = yifulixi;
    }

    public String getSfsmkehu() {
        return sfsmkehu;
    }

    public void setSfsmkehu(String sfsmkehu) {
        this.sfsmkehu = sfsmkehu;
    }

    public String getZhhubumn() {
        return zhhubumn;
    }

    public void setZhhubumn(String zhhubumn) {
        this.zhhubumn = zhhubumn;
    }

    public String getBumenmch() {
        return bumenmch;
    }

    public void setBumenmch(String bumenmch) {
        this.bumenmch = bumenmch;
    }

    public String getZhfleidm() {
        return zhfleidm;
    }

    public void setZhfleidm(String zhfleidm) {
        this.zhfleidm = zhfleidm;
    }

    public String getZhcunfsh() {
        return zhcunfsh;
    }

    public void setZhcunfsh(String zhcunfsh) {
        this.zhcunfsh = zhcunfsh;
    }

    public String getZhjedjbz() {
        return zhjedjbz;
    }

    public void setZhjedjbz(String zhjedjbz) {
        this.zhjedjbz = zhjedjbz;
    }

    public String getZhfbdjbz() {
        return zhfbdjbz;
    }

    public void setZhfbdjbz(String zhfbdjbz) {
        this.zhfbdjbz = zhfbdjbz;
    }

    public String getZhzsbfbz() {
        return zhzsbfbz;
    }

    public void setZhzsbfbz(String zhzsbfbz) {
        this.zhzsbfbz = zhzsbfbz;
    }

    public String getZhzfbsbz() {
        return zhzfbsbz;
    }

    public void setZhzfbsbz(String zhzfbsbz) {
        this.zhzfbsbz = zhzfbsbz;
    }

    public String getDspzsyzt() {
        return dspzsyzt;
    }

    public void setDspzsyzt(String dspzsyzt) {
        this.dspzsyzt = dspzsyzt;
    }

    public String getMimaztai() {
        return mimaztai;
    }

    public void setMimaztai(String mimaztai) {
        this.mimaztai = mimaztai;
    }

    public String getBxzrzhxh() {
        return bxzrzhxh;
    }

    public void setBxzrzhxh(String bxzrzhxh) {
        this.bxzrzhxh = bxzrzhxh;
    }

    public String getShifhnzh() {
        return shifhnzh;
    }

    public void setShifhnzh(String shifhnzh) {
        this.shifhnzh = shifhnzh;
    }

    public String getShifznck() {
        return shifznck;
    }

    public void setShifznck(String shifznck) {
        this.shifznck = shifznck;
    }

    public String getZnckyuee() {
        return znckyuee;
    }

    public void setZnckyuee(String znckyuee) {
        this.znckyuee = znckyuee;
    }

    public String getIlzhkhzh() {
        return ilzhkhzh;
    }

    public void setIlzhkhzh(String ilzhkhzh) {
        this.ilzhkhzh = ilzhkhzh;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getQyxieyih() {
        return qyxieyih;
    }

    public void setQyxieyih(String qyxieyih) {
        this.qyxieyih = qyxieyih;
    }

    public String getRengbhao() {
        return rengbhao;
    }

    public void setRengbhao(String rengbhao) {
        this.rengbhao = rengbhao;
    }

    public String getDecdzhzh() {
        return decdzhzh;
    }

    public void setDecdzhzh(String decdzhzh) {
        this.decdzhzh = decdzhzh;
    }

    public String getDfhnhwbz() {
        return dfhnhwbz;
    }

    public void setDfhnhwbz(String dfhnhwbz) {
        this.dfhnhwbz = dfhnhwbz;
    }

    public String getDfruzhzh() {
        return dfruzhzh;
    }

    public void setDfruzhzh(String dfruzhzh) {
        this.dfruzhzh = dfruzhzh;
    }

    public String getDfrzhhum() {
        return dfrzhhum;
    }

    public void setDfrzhhum(String dfrzhhum) {
        this.dfrzhhum = dfrzhhum;
    }

    public String getDfruzhhh() {
        return dfruzhhh;
    }

    public void setDfruzhhh(String dfruzhhh) {
        this.dfruzhhh = dfruzhhh;
    }

    public String getDfruzhhm() {
        return dfruzhhm;
    }

    public void setDfruzhhm(String dfruzhhm) {
        this.dfruzhhm = dfruzhhm;
    }

    public String getShifdecd() {
        return shifdecd;
    }

    public void setShifdecd(String shifdecd) {
        this.shifdecd = shifdecd;
    }

    public String getShifszmm() {
        return shifszmm;
    }

    public void setShifszmm(String shifszmm) {
        this.shifszmm = shifszmm;
    }

    public String getDiqumaaa() {
        return diqumaaa;
    }

    public void setDiqumaaa(String diqumaaa) {
        this.diqumaaa = diqumaaa;
    }

    public String getZhiqjnge() {
        return zhiqjnge;
    }

    public void setZhiqjnge(String zhiqjnge) {
        this.zhiqjnge = zhiqjnge;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getKehuleix() {
        return kehuleix;
    }

    public void setKehuleix(String kehuleix) {
        this.kehuleix = kehuleix;
    }

    public String getLilvstrs() {
        return lilvstrs;
    }

    public void setLilvstrs(String lilvstrs) {
        this.lilvstrs = lilvstrs;
    }

    public String getKeyongye() {
        return keyongye;
    }

    public void setKeyongye(String keyongye) {
        this.keyongye = keyongye;
    }

    public String getKzjedjbz() {
        return kzjedjbz;
    }

    public void setKzjedjbz(String kzjedjbz) {
        this.kzjedjbz = kzjedjbz;
    }

    public String getKzfbdjbz() {
        return kzfbdjbz;
    }

    public void setKzfbdjbz(String kzfbdjbz) {
        this.kzfbdjbz = kzfbdjbz;
    }

    public String getKzzsbfbz() {
        return kzzsbfbz;
    }

    public void setKzzsbfbz(String kzzsbfbz) {
        this.kzzsbfbz = kzzsbfbz;
    }

    public String getKzzfbsbz() {
        return kzzfbsbz;
    }

    public void setKzzfbsbz(String kzzfbsbz) {
        this.kzzfbsbz = kzzfbsbz;
    }

    public String getKzhuztai() {
        return kzhuztai;
    }

    public void setKzhuztai(String kzhuztai) {
        this.kzhuztai = kzhuztai;
    }

    public String getJnygyuee() {
        return jnygyuee;
    }

    public void setJnygyuee(String jnygyuee) {
        this.jnygyuee = jnygyuee;
    }

    public String getShijbhao() {
        return shijbhao;
    }

    public void setShijbhao(String shijbhao) {
        this.shijbhao = shijbhao;
    }

    public String getJgzhhubz() {
        return jgzhhubz;
    }

    public void setJgzhhubz(String jgzhhubz) {
        this.jgzhhubz = jgzhhubz;
    }

    public String getMeqiqkje() {
        return meqiqkje;
    }

    public void setMeqiqkje(String meqiqkje) {
        this.meqiqkje = meqiqkje;
    }

    public String getMeiqckje() {
        return meiqckje;
    }

    public void setMeiqckje(String meiqckje) {
        this.meiqckje = meiqckje;
    }

    public String getKezhfuje() {
        return kezhfuje;
    }

    public void setKezhfuje(String kezhfuje) {
        this.kezhfuje = kezhfuje;
    }

    public String getKhakjine() {
        return khakjine;
    }

    public void setKhakjine(String khakjine) {
        this.khakjine = khakjine;
    }

    public String getYoxqjine() {
        return yoxqjine;
    }

    public void setYoxqjine(String yoxqjine) {
        this.yoxqjine = yoxqjine;
    }

    public String getSffwjgqy() {
        return sffwjgqy;
    }

    public void setSffwjgqy(String sffwjgqy) {
        this.sffwjgqy = sffwjgqy;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getJysjzifu() {
        return jysjzifu;
    }

    public void setJysjzifu(String jysjzifu) {
        this.jysjzifu = jysjzifu;
    }

    public String getYxczbzhi() {
        return yxczbzhi;
    }

    public void setYxczbzhi(String yxczbzhi) {
        this.yxczbzhi = yxczbzhi;
    }

    public String getYxrzbzhi() {
        return yxrzbzhi;
    }

    public void setYxrzbzhi(String yxrzbzhi) {
        this.yxrzbzhi = yxrzbzhi;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", xitozhho='" + xitozhho + '\'' +
                ", mokuaiii='" + mokuaiii + '\'' +
                ", zhhuzwmc='" + zhhuzwmc + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", shjihaom='" + shjihaom + '\'' +
                ", zhaoxhao='" + zhaoxhao + '\'' +
                ", yingyjig='" + yingyjig + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", yewudhao='" + yewudhao + '\'' +
                ", yewubima='" + yewubima + '\'' +
                ", zhhuywzl='" + zhhuywzl + '\'' +
                ", zhkhjigo='" + zhkhjigo + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", xiohriqi='" + xiohriqi + '\'' +
                ", xiohguiy='" + xiohguiy + '\'' +
                ", xiohjigo='" + xiohjigo + '\'' +
                ", youxriqi='" + youxriqi + '\'' +
                ", zhhuyuee='" + zhhuyuee + '\'' +
                ", shrizhye='" + shrizhye + '\'' +
                ", dzhqlixi='" + dzhqlixi + '\'' +
                ", zhlczxye='" + zhlczxye + '\'' +
                ", ljdonjje='" + ljdonjje + '\'' +
                ", konzjine='" + konzjine + '\'' +
                ", zhhuztai='" + zhhuztai + '\'' +
                ", keyonedu='" + keyonedu + '\'' +
                ", touzhibz='" + touzhibz + '\'' +
                ", yxxjzqbz='" + yxxjzqbz + '\'' +
                ", yxzzzqbz='" + yxzzzqbz + '\'' +
                ", yxxjcrbz='" + yxxjcrbz + '\'' +
                ", yxzzcrbz='" + yxzzcrbz + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", kaihjine='" + kaihjine + '\'' +
                ", cunkzlei='" + cunkzlei + '\'' +
                ", cunqiiii='" + cunqiiii + '\'' +
                ", chapbhao='" + chapbhao + '\'' +
                ", fzcpleix='" + fzcpleix + '\'' +
                ", suoshudx='" + suoshudx + '\'' +
                ", lxzffans='" + lxzffans + '\'' +
                ", jitilixi='" + jitilixi + '\'' +
                ", jishuuuu='" + jishuuuu + '\'' +
                ", yjyjlixi='" + yjyjlixi + '\'' +
                ", yjyjjish='" + yjyjjish + '\'' +
                ", qixifans='" + qixifans + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", jixibioz='" + jixibioz + '\'' +
                ", shcijxri='" + shcijxri + '\'' +
                ", shcifxri='" + shcifxri + '\'' +
                ", jixipnlv='" + jixipnlv + '\'' +
                ", jexipnlv='" + jexipnlv + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                ", shijzqcs='" + shijzqcs + '\'' +
                ", shijzqje='" + shijzqje + '\'' +
                ", dczqzxje='" + dczqzxje + '\'' +
                ", dczqzdje='" + dczqzdje + '\'' +
                ", shijcrcs='" + shijcrcs + '\'' +
                ", shijcrje='" + shijcrje + '\'' +
                ", dccrzxje='" + dccrzxje + '\'' +
                ", dccrzdje='" + dccrzdje + '\'' +
                ", lcuncshu='" + lcuncshu + '\'' +
                ", tcunbzhi='" + tcunbzhi + '\'' +
                ", tduibzhi='" + tduibzhi + '\'' +
                ", tcunfwei='" + tcunfwei + '\'' +
                ", tduifwei='" + tduifwei + '\'' +
                ", daoqxcfs='" + daoqxcfs + '\'' +
                ", bjzrkhzh='" + bjzrkhzh + '\'' +
                ", bjzrzhao='" + bjzrzhao + '\'' +
                ", lxzrkhzh='" + lxzrkhzh + '\'' +
                ", lxzrzhao='" + lxzrzhao + '\'' +
                ", zhufldm3='" + zhufldm3 + '\'' +
                ", lancreny='" + lancreny + '\'' +
                ", zhphbzhi='" + zhphbzhi + '\'' +
                ", shfojshu='" + shfojshu + '\'' +
                ", dghushux='" + dghushux + '\'' +
                ", tzhizlei='" + tzhizlei + '\'' +
                ", tonzriqi='" + tonzriqi + '\'' +
                ", waigzhxz='" + waigzhxz + '\'' +
                ", waihjgbz='" + waihjgbz + '\'' +
                ", waihhcbz='" + waihhcbz + '\'' +
                ", jibhhzho='" + jibhhzho + '\'' +
                ", xiedckbz='" + xiedckbz + '\'' +
                ", tzlixibz='" + tzlixibz + '\'' +
                ", zdkzbzhi='" + zdkzbzhi + '\'' +
                ", lilvbhlx='" + lilvbhlx + '\'' +
                ", lilvbhao='" + lilvbhao + '\'' +
                ", lilvdmlx='" + lilvdmlx + '\'' +
                ", lilvfdbz='" + lilvfdbz + '\'' +
                ", lilvfdlx='" + lilvfdlx + '\'' +
                ", lilvfdsz='" + lilvfdsz + '\'' +
                ", jizhunll='" + jizhunll + '\'' +
                ", zhxililv='" + zhxililv + '\'' +
                ", youhuibz='" + youhuibz + '\'' +
                ", youhuilx='" + youhuilx + '\'' +
                ", youhuisz='" + youhuisz + '\'' +
                ", yegxriqi='" + yegxriqi + '\'' +
                ", scywriqi='" + scywriqi + '\'' +
                ", kaihuqud='" + kaihuqud + '\'' +
                ", zmqzhubz='" + zmqzhubz + '\'' +
                ", zmswiflx='" + zmswiflx + '\'' +
                ", tglzhubz='" + tglzhubz + '\'' +
                ", tuogzhlx='" + tuogzhlx + '\'' +
                ", bfujzhbz='" + bfujzhbz + '\'' +
                ", bfjzhhlx='" + bfjzhhlx + '\'' +
                ", czzhhubz='" + czzhhubz + '\'' +
                ", czckzhlx='" + czckzhlx + '\'' +
                ", hzbabzhi='" + hzbabzhi + '\'' +
                ", weihguiy='" + weihguiy + '\'' +
                ", weihjigo='" + weihjigo + '\'' +
                ", weihriqi='" + weihriqi + '\'' +
                ", weihshij='" + weihshij + '\'' +
                ", shijchuo='" + shijchuo + '\'' +
                ", jiluztai='" + jiluztai + '\'' +
                ", beiyye01='" + beiyye01 + '\'' +
                ", beiyye02='" + beiyye02 + '\'' +
                ", beiyye03='" + beiyye03 + '\'' +
                ", beiyzd01='" + beiyzd01 + '\'' +
                ", beiyzd02='" + beiyzd02 + '\'' +
                ", beiyzd03='" + beiyzd03 + '\'' +
                ", beiyrq01='" + beiyrq01 + '\'' +
                ", yifulixi='" + yifulixi + '\'' +
                ", sfsmkehu='" + sfsmkehu + '\'' +
                ", zhhubumn='" + zhhubumn + '\'' +
                ", bumenmch='" + bumenmch + '\'' +
                ", zhfleidm='" + zhfleidm + '\'' +
                ", zhcunfsh='" + zhcunfsh + '\'' +
                ", zhjedjbz='" + zhjedjbz + '\'' +
                ", zhfbdjbz='" + zhfbdjbz + '\'' +
                ", zhzsbfbz='" + zhzsbfbz + '\'' +
                ", zhzfbsbz='" + zhzfbsbz + '\'' +
                ", dspzsyzt='" + dspzsyzt + '\'' +
                ", mimaztai='" + mimaztai + '\'' +
                ", bxzrzhxh='" + bxzrzhxh + '\'' +
                ", shifhnzh='" + shifhnzh + '\'' +
                ", shifznck='" + shifznck + '\'' +
                ", znckyuee='" + znckyuee + '\'' +
                ", ilzhkhzh='" + ilzhkhzh + '\'' +
                ", pingzhma='" + pingzhma + '\'' +
                ", pngzzlei='" + pngzzlei + '\'' +
                ", qyxieyih='" + qyxieyih + '\'' +
                ", rengbhao='" + rengbhao + '\'' +
                ", decdzhzh='" + decdzhzh + '\'' +
                ", dfhnhwbz='" + dfhnhwbz + '\'' +
                ", dfruzhzh='" + dfruzhzh + '\'' +
                ", dfrzhhum='" + dfrzhhum + '\'' +
                ", dfruzhhh='" + dfruzhhh + '\'' +
                ", dfruzhhm='" + dfruzhhm + '\'' +
                ", shifdecd='" + shifdecd + '\'' +
                ", shifszmm='" + shifszmm + '\'' +
                ", diqumaaa='" + diqumaaa + '\'' +
                ", zhiqjnge='" + zhiqjnge + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", kehuleix='" + kehuleix + '\'' +
                ", lilvstrs='" + lilvstrs + '\'' +
                ", keyongye='" + keyongye + '\'' +
                ", kzjedjbz='" + kzjedjbz + '\'' +
                ", kzfbdjbz='" + kzfbdjbz + '\'' +
                ", kzzsbfbz='" + kzzsbfbz + '\'' +
                ", kzzfbsbz='" + kzzfbsbz + '\'' +
                ", kzhuztai='" + kzhuztai + '\'' +
                ", jnygyuee='" + jnygyuee + '\'' +
                ", shijbhao='" + shijbhao + '\'' +
                ", jgzhhubz='" + jgzhhubz + '\'' +
                ", meqiqkje='" + meqiqkje + '\'' +
                ", meiqckje='" + meiqckje + '\'' +
                ", kezhfuje='" + kezhfuje + '\'' +
                ", khakjine='" + khakjine + '\'' +
                ", yoxqjine='" + yoxqjine + '\'' +
                ", sffwjgqy='" + sffwjgqy + '\'' +
                ", chanpshm='" + chanpshm + '\'' +
                ", jysjzifu='" + jysjzifu + '\'' +
                ", yxczbzhi='" + yxczbzhi + '\'' +
                ", yxrzbzhi='" + yxrzbzhi + '\'' +
                '}';
    }
}