package cn.com.yusys.yusp.online.client.esb.ecif.g11004.req;

/**
 * 请求Service：客户集团信息查询（new）接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:30
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String txdate;//    交易日期
    private String txtime;//    交易时间
    private String datasq;//    全局流水

    private String resotp;//识别方式
    private String grouno; // 集团编号
    private String custno;//客户编号


    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getTxdate() {
        return txdate;
    }

    public void setTxdate(String txdate) {
        this.txdate = txdate;
    }

    public String getTxtime() {
        return txtime;
    }

    public void setTxtime(String txtime) {
        this.txtime = txtime;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    public String getResotp() {
        return resotp;
    }

    public void setResotp(String resotp) {
        this.resotp = resotp;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", txdate='" + txdate + '\'' +
                ", txtime='" + txtime + '\'' +
                ", datasq='" + datasq + '\'' +
                ", resotp='" + resotp + '\'' +
                ", grouno='" + grouno + '\'' +
                ", custno='" + custno + '\'' +
                '}';
    }
}
