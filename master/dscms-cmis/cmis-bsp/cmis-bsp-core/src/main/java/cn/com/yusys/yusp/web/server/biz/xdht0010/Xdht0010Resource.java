package cn.com.yusys.yusp.web.server.biz.xdht0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0010.req.Xdht0010ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0010.resp.Xdht0010RespDto;
import cn.com.yusys.yusp.dto.server.xdht0010.req.Xdht0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0010.resp.Xdht0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0010:查询符合条件的省心快贷合同")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0010Resource.class);
	@Autowired
	private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0010
     * 交易描述：查询符合条件的省心快贷合同
     *
     * @param xdht0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询符合条件的省心快贷合同")
    @PostMapping("/xdht0010")
    //@Idempotent({"xdcaht0010", "#xdht0010ReqDto.datasq"})
    protected @ResponseBody
	Xdht0010RespDto xdht0010(@Validated @RequestBody Xdht0010ReqDto xdht0010ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010ReqDto));
        Xdht0010DataReqDto xdht0010DataReqDto = new Xdht0010DataReqDto();// 请求Data： 查询符合条件的省心快贷合同
        Xdht0010DataRespDto xdht0010DataRespDto = new Xdht0010DataRespDto();// 响应Data：查询符合条件的省心快贷合同
		Xdht0010RespDto xdht0010RespDto = new Xdht0010RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdht0010.req.Data reqData = null; // 请求Data：查询符合条件的省心快贷合同
		cn.com.yusys.yusp.dto.server.biz.xdht0010.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0010.resp.Data();// 响应Data：查询符合条件的省心快贷合同

        try {
            // 从 xdht0010ReqDto获取 reqData
            reqData = xdht0010ReqDto.getData();
            // 将 reqData 拷贝到xdht0010DataReqDto
            BeanUtils.copyProperties(reqData, xdht0010DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataReqDto));
            ResultDto<Xdht0010DataRespDto> xdht0010DataResultDto = dscmsBizHtClientService.xdht0010(xdht0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0010RespDto.setErorcd(Optional.ofNullable(xdht0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0010RespDto.setErortx(Optional.ofNullable(xdht0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0010DataResultDto.getCode())) {
                xdht0010DataRespDto = xdht0010DataResultDto.getData();
                xdht0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0010DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0010DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, e.getMessage());
            xdht0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0010RespDto.setDatasq(servsq);

        xdht0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0010.key, DscmsEnum.TRADE_CODE_XDHT0010.value, JSON.toJSONString(xdht0010RespDto));
        return xdht0010RespDto;
    }
}
