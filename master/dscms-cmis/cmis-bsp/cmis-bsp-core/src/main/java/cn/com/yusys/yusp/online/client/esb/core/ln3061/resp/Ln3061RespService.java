package cn.com.yusys.yusp.online.client.esb.core.ln3061.resp;

/**
 * 响应Service：资产转让协议登记
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3061RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3061RespService{" +
                "service=" + service +
                '}';
    }
}
