package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.resp;

/**
 * 响应Service：信贷系统请求小V平台贷后预警回传定期检查任务结果
 *
 * @author chenyong
 * @version 1.0
 */
public class Dhxd02RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
