package cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dblist;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 14:21
 * @since 2021/8/10 14:21
 */
public class Record {
    private String db_cont_no;//担保合同编号
    private String dh_cont_no_cn;//担保合同中文编号
    private String guar_cont_type;//担保合同类型
    private String guar_way;//担保方式
    private String db_cur_type;//担保币种
    private String guar_amt;//担保合同金额
    private String guar_start_date;//担保起始日
    private String guar_end_date;//担保终止日
    private String sign_date;//担保合同签订日期
    private String guar_cont_state;//担保合同状态

    public String getDb_cont_no() {
        return db_cont_no;
    }

    public void setDb_cont_no(String db_cont_no) {
        this.db_cont_no = db_cont_no;
    }

    public String getDh_cont_no_cn() {
        return dh_cont_no_cn;
    }

    public void setDh_cont_no_cn(String dh_cont_no_cn) {
        this.dh_cont_no_cn = dh_cont_no_cn;
    }

    public String getGuar_cont_type() {
        return guar_cont_type;
    }

    public void setGuar_cont_type(String guar_cont_type) {
        this.guar_cont_type = guar_cont_type;
    }

    public String getGuar_way() {
        return guar_way;
    }

    public void setGuar_way(String guar_way) {
        this.guar_way = guar_way;
    }

    public String getDb_cur_type() {
        return db_cur_type;
    }

    public void setDb_cur_type(String db_cur_type) {
        this.db_cur_type = db_cur_type;
    }

    public String getGuar_amt() {
        return guar_amt;
    }

    public void setGuar_amt(String guar_amt) {
        this.guar_amt = guar_amt;
    }

    public String getGuar_start_date() {
        return guar_start_date;
    }

    public void setGuar_start_date(String guar_start_date) {
        this.guar_start_date = guar_start_date;
    }

    public String getGuar_end_date() {
        return guar_end_date;
    }

    public void setGuar_end_date(String guar_end_date) {
        this.guar_end_date = guar_end_date;
    }

    public String getSign_date() {
        return sign_date;
    }

    public void setSign_date(String sign_date) {
        this.sign_date = sign_date;
    }

    public String getGuar_cont_state() {
        return guar_cont_state;
    }

    public void setGuar_cont_state(String guar_cont_state) {
        this.guar_cont_state = guar_cont_state;
    }

    @Override
    public String toString() {
        return "Record{" +
                "db_cont_no='" + db_cont_no + '\'' +
                ", dh_cont_no_cn='" + dh_cont_no_cn + '\'' +
                ", guar_cont_type='" + guar_cont_type + '\'' +
                ", guar_way='" + guar_way + '\'' +
                ", db_cur_type='" + db_cur_type + '\'' +
                ", guar_amt='" + guar_amt + '\'' +
                ", guar_start_date='" + guar_start_date + '\'' +
                ", guar_end_date='" + guar_end_date + '\'' +
                ", sign_date='" + sign_date + '\'' +
                ", guar_cont_state='" + guar_cont_state + '\'' +
                '}';
    }
}
