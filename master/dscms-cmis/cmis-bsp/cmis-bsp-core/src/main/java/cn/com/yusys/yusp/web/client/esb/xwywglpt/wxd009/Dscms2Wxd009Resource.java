package cn.com.yusys.yusp.web.client.esb.xwywglpt.wxd009;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.req.Wxd009ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.resp.Wxd009RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷系统请求V平台二级准入接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "BSP封装小微业务管理平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Wxd009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Wxd009Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：wxd009
     * 交易描述：信贷系统请求小V平台推送合同信息接口
     *
     * @param wxd009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("wxd009:信贷系统请求小V平台推送合同信息接口")
    @PostMapping("/wxd009")
    protected @ResponseBody
    ResultDto<Wxd009RespDto> wxd009(@Validated @RequestBody Wxd009ReqDto wxd009ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD009.key, EsbEnum.TRADE_CODE_WXD009.value, JSON.toJSONString(wxd009ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.resp.Service();
        Wxd009ReqService wxd009ReqService = new Wxd009ReqService();
        Wxd009RespService wxd009RespService = new Wxd009RespService();
        Wxd009RespDto wxd009RespDto = new Wxd009RespDto();
        ResultDto<Wxd009RespDto> wxd009ResultDto = new ResultDto<Wxd009RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将wxd009ReqDto转换成reqService
            BeanUtils.copyProperties(wxd009ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_WXD009.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            wxd009ReqService.setService(reqService);
            // 将wxd009ReqService转换成wxd009ReqServiceMap
            Map wxd009ReqServiceMap = beanMapUtil.beanToMap(wxd009ReqService);
            context.put("tradeDataMap", wxd009ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD009.key, EsbEnum.TRADE_CODE_WXD009.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_WXD009.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD009.key, EsbEnum.TRADE_CODE_WXD009.value, JSON.toJSONString(result));
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            wxd009RespService = beanMapUtil.mapToBean(tradeDataMap, Wxd009RespService.class, Wxd009RespService.class);
            respService = wxd009RespService.getService();

            wxd009ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            wxd009ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, wxd009RespDto);
                wxd009ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                wxd009ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                wxd009ResultDto.setCode(EpbEnum.EPB099999.key);
                wxd009ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD009.key, EsbEnum.TRADE_CODE_WXD009.value, e.getMessage());
            wxd009ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            wxd009ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        wxd009ResultDto.setData(wxd009RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD009.key, EsbEnum.TRADE_CODE_WXD009.value, JSON.toJSONString(wxd009ResultDto));
        return wxd009ResultDto;
    }
}
