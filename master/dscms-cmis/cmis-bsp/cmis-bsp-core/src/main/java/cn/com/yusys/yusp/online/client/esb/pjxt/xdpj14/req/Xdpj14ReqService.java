package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.req;

/**
 * 请求Service：信贷签约通知
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdpj14ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Xdpj14ReqService{" +
				"service=" + service +
				'}';
	}
}
