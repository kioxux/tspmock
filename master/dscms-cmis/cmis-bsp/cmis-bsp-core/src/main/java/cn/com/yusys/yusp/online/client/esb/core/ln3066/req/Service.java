package cn.com.yusys.yusp.online.client.esb.core.ln3066.req;

import java.math.BigDecimal;

/**
 * 请求Service：资产转让借据筛选
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String yewufenl;//业务分类
    private String zhngjigo;//账务机构
    private String chanpdma;//产品代码
    private String kshchpdm;//可售产品代码
    private String huobdhao;//货币代号
    private String qishriqi;//起始日期
    private String zhzhriqi;//终止日期
    private String djqsriqi;//冻结起始日期
    private String djdqriqi;//冻结到期日期
    private BigDecimal meiczxje;//每次最小金额
    private BigDecimal meiczdje;//每次最大金额
    private String dkzhhzht;//贷款账户状态
    private String daikxtai;//贷款形态
    private String jzhdkbzh;//减值贷款标志
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public String getDjqsriqi() {
        return djqsriqi;
    }

    public void setDjqsriqi(String djqsriqi) {
        this.djqsriqi = djqsriqi;
    }

    public String getDjdqriqi() {
        return djdqriqi;
    }

    public void setDjdqriqi(String djdqriqi) {
        this.djdqriqi = djdqriqi;
    }

    public BigDecimal getMeiczxje() {
        return meiczxje;
    }

    public void setMeiczxje(BigDecimal meiczxje) {
        this.meiczxje = meiczxje;
    }

    public BigDecimal getMeiczdje() {
        return meiczdje;
    }

    public void setMeiczdje(BigDecimal meiczdje) {
        this.meiczdje = meiczdje;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getJzhdkbzh() {
        return jzhdkbzh;
    }

    public void setJzhdkbzh(String jzhdkbzh) {
        this.jzhdkbzh = jzhdkbzh;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", yewufenl='" + yewufenl + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", kshchpdm='" + kshchpdm + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", djqsriqi='" + djqsriqi + '\'' +
                ", djdqriqi='" + djdqriqi + '\'' +
                ", meiczxje=" + meiczxje +
                ", meiczdje=" + meiczdje +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", daikxtai='" + daikxtai + '\'' +
                ", jzhdkbzh='" + jzhdkbzh + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                '}';
    }
}
