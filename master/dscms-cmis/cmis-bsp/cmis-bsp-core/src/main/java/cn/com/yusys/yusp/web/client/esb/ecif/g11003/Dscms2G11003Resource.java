package cn.com.yusys.yusp.web.client.esb.ecif.g11003;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.CircleArrayMem;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.G11003ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g11003.resp.G11003RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g11003)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G11003Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G11003Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 客户集团信息维护 (new)接口（处理码g11003）
     *
     * @param g11003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g11003:客户集团信息维护 (new)接口")
    @PostMapping("/g11003")
    protected @ResponseBody
    ResultDto<G11003RespDto> g11003(@Validated @RequestBody G11003ReqDto g11003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value, JSON.toJSONString(g11003ReqDto));

        cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g11003.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g11003.resp.Service();
        G11003ReqService g11003ReqService = new G11003ReqService();
        G11003RespService g11003RespService = new G11003RespService();
        G11003RespDto g11003RespDto = new G11003RespDto();
        ResultDto<G11003RespDto> g11003ResultDto = new ResultDto<G11003RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将G11003ReqDto转换成reqService
            BeanUtils.copyProperties(g11003ReqDto, reqService);
            if (CollectionUtils.nonEmpty(g11003ReqDto.getCircleArrayMem())) {
                List<CircleArrayMem> circleArrayMemList = Optional.ofNullable(g11003ReqDto.getCircleArrayMem()).orElseGet(() -> new ArrayList<CircleArrayMem>());
                cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.List g11003ReqList = Optional.ofNullable(reqService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.List());
                java.util.List<cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.Record> g11003ReqRecords = circleArrayMemList.parallelStream().map(circleArrayMem -> {
                    cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.Record g11003ReqRecord = new cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.Record();
                    BeanUtils.copyProperties(circleArrayMem, g11003ReqRecord);
                    return g11003ReqRecord;
                }).collect(Collectors.toList());
                g11003ReqList.setRecord(g11003ReqRecords);
                reqService.setList(g11003ReqList);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_G11003.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间

            g11003ReqService.setService(reqService);
            // 将g11003ReqService转换成g11003ReqServiceMap
            Map g11003ReqServiceMap = beanMapUtil.beanToMap(g11003ReqService);
            context.put("tradeDataMap", g11003ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G11003.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g11003RespService = beanMapUtil.mapToBean(tradeDataMap, G11003RespService.class, G11003RespService.class);
            respService = g11003RespService.getService();

            //  将G11003RespDto封装到ResultDto<G11003RespDto>
            g11003ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g11003ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G11003RespDto
                BeanUtils.copyProperties(respService, g11003RespDto);
                g11003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g11003ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                g11003ResultDto.setCode(EpbEnum.EPB099999.key);
                g11003ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value, e.getMessage());
            g11003ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g11003ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g11003ResultDto.setData(g11003RespDto);

        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11003.key, EsbEnum.TRADE_CODE_G11003.value, JSON.toJSONString(g11003ResultDto));
        return g11003ResultDto;
    }
}
