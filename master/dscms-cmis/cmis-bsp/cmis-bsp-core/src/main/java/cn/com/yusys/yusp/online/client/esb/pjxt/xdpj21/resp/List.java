package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 21:31
 * @since 2021/5/28 21:31
 */
public class List {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj21.resp.Record> recordList;

    public java.util.List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(java.util.List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        return "List{" +
                "recordList=" + recordList +
                '}';
    }
}
