package cn.com.yusys.yusp.online.client.esb.ypxt.certis.req;


/**
 * 请求Service：权证状态同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:38
 */
public class Record {

    private String sernoy; //核心担保编号
    private String qlpzhm; //权利凭证号
    private String qzlxyp; //权证类型
    private String qzztyp; //权证状态
    private String qzrkrq; //权证入库日期
    private String qzckrq; //权证正常出库日期
    private String qzjyrm; //权证临时借用人名称
    private String qzwjrq; //权证外借日期
    private String yjghrq; //预计归还日期
    private String sjghrq; //权证实际归还日期
    private String qzwjyy; //权证外借原因
    private String qtwbsr; //其他文本输入

    public String getSernoy() {
        return sernoy;
    }

    public void setSernoy(String sernoy) {
        this.sernoy = sernoy;
    }

    public String getQlpzhm() {
        return qlpzhm;
    }

    public void setQlpzhm(String qlpzhm) {
        this.qlpzhm = qlpzhm;
    }

    public String getQzlxyp() {
        return qzlxyp;
    }

    public void setQzlxyp(String qzlxyp) {
        this.qzlxyp = qzlxyp;
    }

    public String getQzztyp() {
        return qzztyp;
    }

    public void setQzztyp(String qzztyp) {
        this.qzztyp = qzztyp;
    }

    public String getQzrkrq() {
        return qzrkrq;
    }

    public void setQzrkrq(String qzrkrq) {
        this.qzrkrq = qzrkrq;
    }

    public String getQzckrq() {
        return qzckrq;
    }

    public void setQzckrq(String qzckrq) {
        this.qzckrq = qzckrq;
    }

    public String getQzjyrm() {
        return qzjyrm;
    }

    public void setQzjyrm(String qzjyrm) {
        this.qzjyrm = qzjyrm;
    }

    public String getQzwjrq() {
        return qzwjrq;
    }

    public void setQzwjrq(String qzwjrq) {
        this.qzwjrq = qzwjrq;
    }

    public String getYjghrq() {
        return yjghrq;
    }

    public void setYjghrq(String yjghrq) {
        this.yjghrq = yjghrq;
    }

    public String getSjghrq() {
        return sjghrq;
    }

    public void setSjghrq(String sjghrq) {
        this.sjghrq = sjghrq;
    }

    public String getQzwjyy() {
        return qzwjyy;
    }

    public void setQzwjyy(String qzwjyy) {
        this.qzwjyy = qzwjyy;
    }

    public String getQtwbsr() {
        return qtwbsr;
    }

    public void setQtwbsr(String qtwbsr) {
        this.qtwbsr = qtwbsr;
    }

    @Override
    public String toString() {
        return "Record{" +
                "sernoy='" + sernoy + '\'' +
                ", qlpzhm='" + qlpzhm + '\'' +
                ", qzlxyp='" + qzlxyp + '\'' +
                ", qzztyp='" + qzztyp + '\'' +
                ", qzrkrq='" + qzrkrq + '\'' +
                ", qzckrq='" + qzckrq + '\'' +
                ", qzjyrm='" + qzjyrm + '\'' +
                ", qzwjrq='" + qzwjrq + '\'' +
                ", yjghrq='" + yjghrq + '\'' +
                ", sjghrq='" + sjghrq + '\'' +
                ", qzwjyy='" + qzwjyy + '\'' +
                ", qtwbsr='" + qtwbsr + '\'' +
                '}';
    }
}
