package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp;

public class Record {
    private String batchNo;// 批次号
    private String totalFAmount;//票面总金额
    private String successAmount;//成功金额

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getTotalFAmount() {
        return totalFAmount;
    }

    public void setTotalFAmount(String totalFAmount) {
        this.totalFAmount = totalFAmount;
    }

    public String getSuccessAmount() {
        return successAmount;
    }

    public void setSuccessAmount(String successAmount) {
        this.successAmount = successAmount;
    }

    @Override
    public String toString() {
        return "Record{" +
                "batchNo='" + batchNo + '\'' +
                ", totalFAmount='" + totalFAmount + '\'' +
                ", successAmount='" + successAmount + '\'' +
                '}';
    }
}