package cn.com.yusys.yusp.web.server.cus.xdkh0018;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0018.req.Xdkh0018ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0018.resp.Xdkh0018RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0018.req.Xdkh0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0018.resp.Xdkh0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:临时客户信息维护
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0018:临时客户信息维护")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0018Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0018
     * 交易描述：临时客户信息维护
     *
     * @param xdkh0018ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0018:临时客户信息维护")
    @PostMapping("/xdkh0018")
    //@Idempotent({"xdcakh0018", "#xdkh0018ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0018RespDto xdkh0018(@Validated @RequestBody Xdkh0018ReqDto xdkh0018ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, JSON.toJSONString(xdkh0018ReqDto));
        Xdkh0018DataReqDto xdkh0018DataReqDto = new Xdkh0018DataReqDto();// 请求Data： 临时客户信息维护
        Xdkh0018DataRespDto xdkh0018DataRespDto = new Xdkh0018DataRespDto();// 响应Data：临时客户信息维护
        Xdkh0018RespDto xdkh0018RespDto = new Xdkh0018RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0018.req.Data reqData = null; // 请求Data：临时客户信息维护
        cn.com.yusys.yusp.dto.server.cus.xdkh0018.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0018.resp.Data();// 响应Data：临时客户信息维护
        try {
            // 从 xdkh0018ReqDto获取 reqData
            reqData = xdkh0018ReqDto.getData();
            // 将 reqData 拷贝到xdkh0018DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0018DataReqDto);
            // 调用服务
            ResultDto<Xdkh0018DataRespDto> xdkh0018DataResultDto = dscmsCusClientService.xdkh0018(xdkh0018DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdkh0018RespDto.setErorcd(Optional.ofNullable(xdkh0018DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0018RespDto.setErortx(Optional.ofNullable(xdkh0018DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0018DataResultDto.getCode())) {
                xdkh0018RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0018RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0018DataRespDto = xdkh0018DataResultDto.getData();
                // 将 xdkh0018DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0018DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, e.getMessage());
            xdkh0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0018RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0018RespDto.setDatasq(servsq);

        xdkh0018RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0018.key, DscmsEnum.TRADE_CODE_XDKH0018.value, JSON.toJSONString(xdkh0018RespDto));
        return xdkh0018RespDto;
    }
}
