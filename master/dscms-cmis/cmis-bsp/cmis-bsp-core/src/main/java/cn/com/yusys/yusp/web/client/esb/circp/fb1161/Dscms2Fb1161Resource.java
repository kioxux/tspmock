package cn.com.yusys.yusp.web.client.esb.circp.fb1161;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1161.req.Fb1161ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1161.resp.Fb1161RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1161.req.Fb1161ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1161.resp.Fb1161RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1161）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1161Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1161Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1161
     * 交易描述：借据信息同步
     *
     * @param fb1161ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1161")
    protected @ResponseBody
    ResultDto<Fb1161RespDto> fb1161(@Validated @RequestBody Fb1161ReqDto fb1161ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1161.key, EsbEnum.TRADE_CODE_FB1161.value, JSON.toJSONString(fb1161ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1161.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1161.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1161.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1161.resp.Service();
        Fb1161ReqService fb1161ReqService = new Fb1161ReqService();
        Fb1161RespService fb1161RespService = new Fb1161RespService();
        Fb1161RespDto fb1161RespDto = new Fb1161RespDto();
        ResultDto<Fb1161RespDto> fb1161ResultDto = new ResultDto<Fb1161RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1161ReqDto转换成reqService
            BeanUtils.copyProperties(fb1161ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1161.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1161ReqService.setService(reqService);
            // 将fb1161ReqService转换成fb1161ReqServiceMap
            Map fb1161ReqServiceMap = beanMapUtil.beanToMap(fb1161ReqService);
            context.put("tradeDataMap", fb1161ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1161.key, EsbEnum.TRADE_CODE_FB1161.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1161.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1161.key, EsbEnum.TRADE_CODE_FB1161.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1161RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1161RespService.class, Fb1161RespService.class);
            respService = fb1161RespService.getService();

            fb1161ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1161ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1161RespDto
                BeanUtils.copyProperties(respService, fb1161RespDto);

                fb1161ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1161ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1161ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1161ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1161.key, EsbEnum.TRADE_CODE_FB1161.value, e.getMessage());
            fb1161ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1161ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1161ResultDto.setData(fb1161RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1161.key, EsbEnum.TRADE_CODE_FB1161.value, JSON.toJSONString(fb1161ResultDto));
        return fb1161ResultDto;
    }
}
