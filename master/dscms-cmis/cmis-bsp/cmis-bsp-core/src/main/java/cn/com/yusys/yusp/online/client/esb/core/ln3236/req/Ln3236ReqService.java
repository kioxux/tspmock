package cn.com.yusys.yusp.online.client.esb.core.ln3236.req;

/**
 * 请求Service：贷款账隔日冲正
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3236ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3236ReqService{" +
				"service=" + service +
				'}';
	}
}

