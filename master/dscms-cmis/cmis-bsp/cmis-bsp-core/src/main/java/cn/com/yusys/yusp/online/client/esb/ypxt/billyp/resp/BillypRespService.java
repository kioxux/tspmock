package cn.com.yusys.yusp.online.client.esb.ypxt.billyp.resp;

/**
 * 响应Service：票据信息同步接口
 * @author zhugenrong
 * @version 1.0
 */
public class BillypRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}