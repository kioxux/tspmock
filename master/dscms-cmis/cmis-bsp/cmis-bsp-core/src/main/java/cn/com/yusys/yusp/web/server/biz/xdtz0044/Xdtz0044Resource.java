package cn.com.yusys.yusp.web.server.biz.xdtz0044;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0044.req.Xdtz0044ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0044.resp.Xdtz0044RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.req.Xdtz0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0044.resp.Xdtz0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号前往信贷查找房贷借据信息
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0044:根据客户号前往信贷查找房贷借据信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0044Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0044Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0044
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param xdtz0044ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号前往信贷查找房贷借据信息")
    @PostMapping("/xdtz0044")
    //@Idempotent({"xdcatz0044", "#xdtz0044ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0044RespDto xdtz0044(@Validated @RequestBody Xdtz0044ReqDto xdtz0044ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044ReqDto));
        Xdtz0044DataReqDto xdtz0044DataReqDto = new Xdtz0044DataReqDto();// 请求Data： 根据客户号前往信贷查找房贷借据信息
        Xdtz0044DataRespDto xdtz0044DataRespDto = new Xdtz0044DataRespDto();// 响应Data：根据客户号前往信贷查找房贷借据信息
        Xdtz0044RespDto xdtz0044RespDto = new Xdtz0044RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0044.req.Data reqData = null; // 请求Data：根据客户号前往信贷查找房贷借据信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0044.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0044.resp.Data();// 响应Data：根据客户号前往信贷查找房贷借据信息
        try {
            // 从 xdtz0044ReqDto获取 reqData
            reqData = xdtz0044ReqDto.getData();
            // 将 reqData 拷贝到xdtz0044DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0044DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044DataReqDto));
            ResultDto<Xdtz0044DataRespDto> xdtz0044DataResultDto = dscmsBizTzClientService.xdtz0044(xdtz0044DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044DataResultDto));
            // 从返回值中获取响应码和响应消息

            xdtz0044RespDto.setErorcd(Optional.ofNullable(xdtz0044DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0044RespDto.setErortx(Optional.ofNullable(xdtz0044DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0044DataResultDto.getCode())) {
                xdtz0044DataRespDto = xdtz0044DataResultDto.getData();
                xdtz0044RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0044RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0044DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0044DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, e.getMessage());
            xdtz0044RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0044RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0044RespDto.setDatasq(servsq);
        xdtz0044RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0044.key, DscmsEnum.TRADE_CODE_XDTZ0044.value, JSON.toJSONString(xdtz0044RespDto));
        return xdtz0044RespDto;
    }
}
