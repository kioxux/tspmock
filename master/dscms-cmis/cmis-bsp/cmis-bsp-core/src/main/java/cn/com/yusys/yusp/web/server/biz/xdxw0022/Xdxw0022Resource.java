package cn.com.yusys.yusp.web.server.biz.xdxw0022;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0022.req.Xdxw0022ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0022.resp.Xdxw0022RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0022.req.Xdxw0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0022.resp.Xdxw0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据证件号查询信贷系统的申请信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0022:根据证件号查询信贷系统的申请信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0022Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0022
     * 交易描述：根据证件号查询信贷系统的申请信息
     *
     * @param xdxw0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据证件号查询信贷系统的申请信息")
    @PostMapping("/xdxw0022")
    //@Idempotent({"xdcaxw0022", "#xdxw0022ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0022RespDto xdxw0022(@Validated @RequestBody Xdxw0022ReqDto xdxw0022ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022ReqDto));
        Xdxw0022DataReqDto xdxw0022DataReqDto = new Xdxw0022DataReqDto();// 请求Data： 根据证件号查询信贷系统的申请信息
        Xdxw0022DataRespDto xdxw0022DataRespDto = new Xdxw0022DataRespDto();// 响应Data：根据证件号查询信贷系统的申请信息
        Xdxw0022RespDto xdxw0022RespDto = new Xdxw0022RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0022.req.Data reqData = null; // 请求Data：根据证件号查询信贷系统的申请信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0022.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0022.resp.Data();// 响应Data：根据证件号查询信贷系统的申请信息
        try {
            // 从 xdxw0022ReqDto获取 reqData
            reqData = xdxw0022ReqDto.getData();
            // 将 reqData 拷贝到xdxw0022DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0022DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022DataReqDto));
            ResultDto<Xdxw0022DataRespDto> xdxw0022DataResultDto = dscmsBizXwClientService.xdxw0022(xdxw0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0022RespDto.setErorcd(Optional.ofNullable(xdxw0022DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0022RespDto.setErortx(Optional.ofNullable(xdxw0022DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0022DataResultDto.getCode())) {
                xdxw0022DataRespDto = xdxw0022DataResultDto.getData();
                xdxw0022RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0022RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0022DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0022DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, e.getMessage());
            xdxw0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0022RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0022RespDto.setDatasq(servsq);

        xdxw0022RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0022.key, DscmsEnum.TRADE_CODE_XDXW0022.value, JSON.toJSONString(xdxw0022RespDto));
        return xdxw0022RespDto;
    }
}
