package cn.com.yusys.yusp.online.client.esb.core.ln3025.resp;

import java.math.BigDecimal;

/**
 * 响应Service：垫款开户
 * @author lihh
 * @version 1.0
 */
public class Service {
	private String erorcd;//错误码
	private String erortx;//错误描述
	private String dkjiejuh;//贷款借据号
	private String hetongbh;//合同编号
	private String daixzhxh;//待销账序号
	private String jiaoyirq;//交易日期
	private String jiaoyils;//交易流水
	private String jiaoyigy;//交易柜员
	private String jiaoyijg;//交易机构
	private String kehuhaoo;//客户号
	private String kehmingc;//客户名称
	private String chanpdma;//产品代码
	private String chanpmch;//产品名称
	private BigDecimal zdkuanje;//转垫款金额
	public String  getErorcd() { return erorcd; }
	public void setErorcd(String erorcd ) { this.erorcd = erorcd;}
	public String  getErortx() { return erortx; }
	public void setErortx(String erortx ) { this.erortx = erortx;}
	public String  getDkjiejuh() { return dkjiejuh; }
	public void setDkjiejuh(String dkjiejuh ) { this.dkjiejuh = dkjiejuh;}
	public String  getHetongbh() { return hetongbh; }
	public void setHetongbh(String hetongbh ) { this.hetongbh = hetongbh;}
	public String  getDaixzhxh() { return daixzhxh; }
	public void setDaixzhxh(String daixzhxh ) { this.daixzhxh = daixzhxh;}
	public String  getJiaoyirq() { return jiaoyirq; }
	public void setJiaoyirq(String jiaoyirq ) { this.jiaoyirq = jiaoyirq;}
	public String  getJiaoyils() { return jiaoyils; }
	public void setJiaoyils(String jiaoyils ) { this.jiaoyils = jiaoyils;}
	public String  getJiaoyigy() { return jiaoyigy; }
	public void setJiaoyigy(String jiaoyigy ) { this.jiaoyigy = jiaoyigy;}
	public String  getJiaoyijg() { return jiaoyijg; }
	public void setJiaoyijg(String jiaoyijg ) { this.jiaoyijg = jiaoyijg;}
	public String  getKehuhaoo() { return kehuhaoo; }
	public void setKehuhaoo(String kehuhaoo ) { this.kehuhaoo = kehuhaoo;}
	public String  getKehmingc() { return kehmingc; }
	public void setKehmingc(String kehmingc ) { this.kehmingc = kehmingc;}
	public String  getChanpdma() { return chanpdma; }
	public void setChanpdma(String chanpdma ) { this.chanpdma = chanpdma;}
	public String  getChanpmch() { return chanpmch; }
	public void setChanpmch(String chanpmch ) { this.chanpmch = chanpmch;}
	public BigDecimal  getZdkuanje() { return zdkuanje; }
	public void setZdkuanje(BigDecimal zdkuanje ) { this.zdkuanje = zdkuanje;}
    @Override
    public String toString() {
	    return "Service{" +
	"erorcd='" + erorcd+ '\'' +
	"erortx='" + erortx+ '\'' +
	"dkjiejuh='" + dkjiejuh+ '\'' +
	"hetongbh='" + hetongbh+ '\'' +
	"daixzhxh='" + daixzhxh+ '\'' +
	"jiaoyirq='" + jiaoyirq+ '\'' +
	"jiaoyils='" + jiaoyils+ '\'' +
	"jiaoyigy='" + jiaoyigy+ '\'' +
	"jiaoyijg='" + jiaoyijg+ '\'' +
	"kehuhaoo='" + kehuhaoo+ '\'' +
	"kehmingc='" + kehmingc+ '\'' +
	"chanpdma='" + chanpdma+ '\'' +
	"chanpmch='" + chanpmch+ '\'' +
	"zdkuanje='" + zdkuanje+ '\'' +
 '}';
    }
}
