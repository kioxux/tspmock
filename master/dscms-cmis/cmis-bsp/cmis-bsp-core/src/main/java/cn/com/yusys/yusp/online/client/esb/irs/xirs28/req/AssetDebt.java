package cn.com.yusys.yusp.online.client.esb.irs.xirs28.req;

import cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.assetDebt.Record;

import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/6/23 20:27:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/23 20:27
 * @since 2021/6/23 20:27
 */
public class AssetDebt {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.assetDebt.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "AssetDebt_ARRAY{" +
                "record=" + record +
                '}';
    }
}
