package cn.com.yusys.yusp.online.client.esb.core.da3301.req;



import cn.com.yusys.yusp.online.client.esb.core.da3301.req.listnm0.Record;

import java.util.List;

/**
 * 请求Service：抵债资产入账
 * @author lihh
 * @version 1.0
 */
public class Listnm0 {

    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Listnm0{" +
                "record=" + record +
                '}';
    }
}
