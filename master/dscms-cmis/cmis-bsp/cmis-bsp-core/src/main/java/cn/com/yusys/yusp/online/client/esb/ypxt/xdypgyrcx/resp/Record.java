package cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp;


public class Record {

    private String common_owner_name;//共有人客户名称
    private String common_cert_code;//共有人证件号码
    private String common_owner_id;//共有人客户编号
    private String common_addr;//共有人地址
    private String common_mail;//共有人电子邮箱
    private String common_qq;//共有人QQ号码
    private String common_wxin;//共有人微信号码
    private String common_tel;//共有人电话
    private String house_no;//不动产（房产证号）
    private String share_owner_type;//共有人客户类型
    private String common_hold_portio;//共有人所占份额

    public String getCommon_owner_name() {
        return common_owner_name;
    }
    public void setCommon_owner_name(String common_owner_name) {
        this.common_owner_name = common_owner_name;
    }

    public String getCommon_cert_code() {
        return common_cert_code;
    }
    public void setCommon_cert_code(String common_cert_code) {
        this.common_cert_code = common_cert_code;
    }

    public String getCommon_owner_id() {
        return common_owner_id;
    }
    public void setCommon_owner_id(String common_owner_id) {
        this.common_owner_id = common_owner_id;
    }

    public String getCommon_addr() {
        return common_addr;
    }
    public void setCommon_addr(String common_addr) {
        this.common_addr = common_addr;
    }

    public String getCommon_mail() {
        return common_mail;
    }
    public void setCommon_mail(String common_mail) {
        this.common_mail = common_mail;
    }

    public String getCommon_qq() {
        return common_qq;
    }
    public void setCommon_qq(String common_qq) {
        this.common_qq = common_qq;
    }

    public String getCommon_wxin() {
        return common_wxin;
    }
    public void setCommon_wxin(String common_wxin) {
        this.common_wxin = common_wxin;
    }

    public String getCommon_tel() {
        return common_tel;
    }
    public void setCommon_tel(String common_tel) {
        this.common_tel = common_tel;
    }

    public String getHouse_no() {
        return house_no;
    }
    public void setHouse_no(String house_no) {
        this.house_no = house_no;
    }

    public String getShare_owner_type() {
        return share_owner_type;
    }
    public void setShare_owner_type(String share_owner_type) {
        this.share_owner_type = share_owner_type;
    }

    public String getCommon_hold_portio() {
        return common_hold_portio;
    }
    public void setCommon_hold_portio(String common_hold_portio) {
        this.common_hold_portio = common_hold_portio;
    }

    @Override
    public String toString() {
        return "Record{" +
                "common_owner_name='" + common_owner_name + '\'' +
                "common_cert_code='" + common_cert_code + '\'' +
                "common_owner_id='" + common_owner_id + '\'' +
                "common_addr='" + common_addr + '\'' +
                "common_mail='" + common_mail + '\'' +
                "common_qq='" + common_qq + '\'' +
                "common_wxin='" + common_wxin + '\'' +
                "common_tel='" + common_tel + '\'' +
                "house_no='" + house_no + '\'' +
                "share_owner_type='" + share_owner_type + '\'' +
                "common_hold_portio='" + common_hold_portio + '\'' +
                '}';
    }
}
