package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

import java.util.List;

/**
 * 响应Service：抵债资产明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否
    private cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Listnm0 listnm0;
    private cn.com.yusys.yusp.online.client.esb.core.da3322.resp.LstDzjtmx lstDzjtmx;
    private cn.com.yusys.yusp.online.client.esb.core.da3322.resp.LstDzsfmx lstDzsfmx;
    private cn.com.yusys.yusp.online.client.esb.core.da3322.resp.LstDztxmx lstDztxmx;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Listnm0 getListnm0() {
        return listnm0;
    }

    public void setListnm0(Listnm0 listnm0) {
        this.listnm0 = listnm0;
    }

    public LstDzjtmx getLstDzjtmx() {
        return lstDzjtmx;
    }

    public void setLstDzjtmx(LstDzjtmx lstDzjtmx) {
        this.lstDzjtmx = lstDzjtmx;
    }

    public LstDzsfmx getLstDzsfmx() {
        return lstDzsfmx;
    }

    public void setLstDzsfmx(LstDzsfmx lstDzsfmx) {
        this.lstDzsfmx = lstDzsfmx;
    }

    public LstDztxmx getLstDztxmx() {
        return lstDztxmx;
    }

    public void setLstDztxmx(LstDztxmx lstDztxmx) {
        this.lstDztxmx = lstDztxmx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", listnm0=" + listnm0 +
                ", lstDzjtmx=" + lstDzjtmx +
                ", lstDzsfmx=" + lstDzsfmx +
                ", lstDztxmx=" + lstDztxmx +
                '}';
    }
}
