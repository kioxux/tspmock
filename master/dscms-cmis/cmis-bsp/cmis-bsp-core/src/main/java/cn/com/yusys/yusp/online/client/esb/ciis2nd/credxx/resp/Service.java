package cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.resp;

/**
 * 响应Service：线下查询接口
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String reqId;//请求业务号
    private String reportId;//报告编号
    private String htmlStr;//html报告
    private String xmlStr;//xml报告
    private String jsonStr;//json报告
    private String reportSource;//报告来源
    private String usetime;//查询报告耗时

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getHtmlStr() {
        return htmlStr;
    }

    public void setHtmlStr(String htmlStr) {
        this.htmlStr = htmlStr;
    }

    public String getXmlStr() {
        return xmlStr;
    }

    public void setXmlStr(String xmlStr) {
        this.xmlStr = xmlStr;
    }

    public String getJsonStr() {
        return jsonStr;
    }

    public void setJsonStr(String jsonStr) {
        this.jsonStr = jsonStr;
    }

    public String getReportSource() {
        return reportSource;
    }

    public void setReportSource(String reportSource) {
        this.reportSource = reportSource;
    }

    public String getUsetime() {
        return usetime;
    }

    public void setUsetime(String usetime) {
        this.usetime = usetime;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "servsq='" + servsq + '\'' +
                "reqId='" + reqId + '\'' +
                "reportId='" + reportId + '\'' +
                "htmlStr='" + htmlStr + '\'' +
                "xmlStr='" + xmlStr + '\'' +
                "jsonStr='" + jsonStr + '\'' +
                "reportSource='" + reportSource + '\'' +
                "usetime='" + usetime + '\'' +
                '}';
    }
}
