package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstydkjjh.Record;

import java.util.List;

/**
 * 响应Dto：借新还旧列表
 *
 * @author code-generator
 * @version 1.0
 */

public class Lstydkjjh_ARRAY {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstydkjjh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstydkjjh_ARRAY{" +
                "record=" + record +
                '}';
    }
}
