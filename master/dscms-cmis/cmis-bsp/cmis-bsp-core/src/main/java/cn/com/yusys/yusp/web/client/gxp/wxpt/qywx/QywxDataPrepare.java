package cn.com.yusys.yusp.web.client.gxp.wxpt.qywx;

import cn.com.yusys.yusp.bsp.resources.BspContextHolder;
import cn.com.yusys.yusp.bsp.resources.BspDataContext;
import cn.com.yusys.yusp.bsp.workflow.BspController;
import cn.com.yusys.yusp.bsp.workflow.TradeMapping;

import java.util.Map;

/**
 * 企业微信报文准备类
 */
@BspController("qywxDataPrepare")
public class QywxDataPrepare {
    @TradeMapping("qywxDataMap")
    public Map<String, Object> qywxDataMap(Map<String, Object> param) throws Exception {
        BspDataContext bspDataContext = BspContextHolder.getInstance().getBspDataContext();
        // 发送的xml数组
        byte[] send = (byte[]) bspDataContext.getData("send");
        //固定字符
        byte[] head = ("SMSTXT").getBytes("GBK");
        byte[] result = new byte[send.length + head.length];
        //拼接
        System.arraycopy(head, 0, result, 0, head.length);
        System.arraycopy(send, 0, result, head.length, send.length);
        bspDataContext.setData("send", result);
        return param;
    }
}
