package cn.com.yusys.yusp.web.client.esb.ypxt.xdypbdccx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypbdccx.req.XdypbdccxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypbdccx.resp.XdypbdccxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypbdccx.req.XdypbdccxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypbdccx.resp.XdypbdccxRespService;
import cn.com.yusys.yusp.web.client.esb.ypxt.xdypcdpjcx.Dscms2XdypcdpjcxResource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.BeanUtils;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(xdypbdccx)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2XdypbdccxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdypcdpjcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * 交易码：xdypbdccx
     * 交易描述：查询不动产信息
     *
     * @param xdypbdccxReqDto
     * @throws Exception
     * @return
     */
    @PostMapping("/xdypbdccx")
    protected @ResponseBody
    ResultDto<XdypbdccxRespDto> xdypbdccx(@Validated @RequestBody XdypbdccxReqDto xdypbdccxReqDto ) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPBDCCX.key, EsbEnum.TRADE_CODE_XDYPBDCCX.value, JSON.toJSONString(xdypbdccxReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypbdccx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypbdccx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypbdccx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypbdccx.resp.Service();
        XdypbdccxReqService xdypbdccxReqService =  new XdypbdccxReqService();
        XdypbdccxRespService xdypbdccxRespService =  new XdypbdccxRespService();
        XdypbdccxRespDto xdypbdccxRespDto =  new XdypbdccxRespDto();
        ResultDto<XdypbdccxRespDto> xdypbdccxResultDto= new ResultDto<XdypbdccxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将xdypbdccxReqDto转换成reqService
            BeanUtils.copyProperties(xdypbdccxReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDYPBDCCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xdypbdccxReqService.setService(reqService);
            // 将xdypbdccxReqService转换成xdypbdccxReqServiceMap
            Map xdypbdccxReqServiceMap = beanMapUtil.beanToMap(xdypbdccxReqService);
            context.put("tradeDataMap", xdypbdccxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPBDCCX.key, EsbEnum.TRADE_CODE_XDYPBDCCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDYPBDCCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPBDCCX.key, EsbEnum.TRADE_CODE_XDYPBDCCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdypbdccxRespService= beanMapUtil.mapToBean(tradeDataMap, XdypbdccxRespService.class,XdypbdccxRespService.class);	 respService = xdypbdccxRespService.getService();
            respService = xdypbdccxRespService.getService();

            xdypbdccxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdypbdccxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService,xdypbdccxRespDto);
                xdypbdccxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdypbdccxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdypbdccxResultDto.setCode(EpbEnum.EPB099999.key);
                xdypbdccxResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPBDCCX.key, EsbEnum.TRADE_CODE_XDYPBDCCX.value, e.getMessage());
            xdypbdccxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdypbdccxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdypbdccxResultDto.setData(xdypbdccxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPBDCCX.key, EsbEnum.TRADE_CODE_XDYPBDCCX.value, JSON.toJSONString(xdypbdccxResultDto));
        return xdypbdccxResultDto;
    }
}
