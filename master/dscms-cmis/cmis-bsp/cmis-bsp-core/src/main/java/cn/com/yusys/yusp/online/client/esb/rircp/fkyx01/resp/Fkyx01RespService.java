package cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.resp;
/**
 * 响应Service：优享贷客户经理分配通知接口
 */      
public class Fkyx01RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      

