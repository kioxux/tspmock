package cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.req;

/**
 * 请求Service：信贷审批结果同步接口
 */
public class Service {
	private String servtp;//渠道码
	private String prcscd;//交易码
	private String userid;//柜员号
	private String brchno;//部门号
	private String datasq;//全局流水
	private String servsq;//渠道流水
	private String servdt;//请求方日期
	private String servti;//请求方时间
	private String ipaddr;//请求方IP
	private String mac;   //请求方MAC

    private String yptybh;//押品统一编号
    private String splxyp;//审批类型
    private String cljgyp;//处理结果
    private String clrghy;//处理人工号
    private String clrjgy;//处理人所属机构ID
    private String remark;//备注

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getIpaddr() {
		return ipaddr;
	}

	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getSplxyp() {
        return splxyp;
    }

    public void setSplxyp(String splxyp) {
        this.splxyp = splxyp;
    }

    public String getCljgyp() {
        return cljgyp;
    }

    public void setCljgyp(String cljgyp) {
        this.cljgyp = cljgyp;
    }

    public String getClrghy() {
        return clrghy;
    }

    public void setClrghy(String clrghy) {
        this.clrghy = clrghy;
    }

    public String getClrjgy() {
        return clrjgy;
    }

    public void setClrjgy(String clrjgy) {
        this.clrjgy = clrjgy;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

	@Override
	public String toString() {
		return "Service{" +
				"servtp='" + servtp + '\'' +
				", prcscd='" + prcscd + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", datasq='" + datasq + '\'' +
				", servsq='" + servsq + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", ipaddr='" + ipaddr + '\'' +
				", mac='" + mac + '\'' +
				", yptybh='" + yptybh + '\'' +
				", splxyp='" + splxyp + '\'' +
				", cljgyp='" + cljgyp + '\'' +
				", clrghy='" + clrghy + '\'' +
				", clrjgy='" + clrjgy + '\'' +
				", remark='" + remark + '\'' +
				'}';
	}
}
