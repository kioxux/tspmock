package cn.com.yusys.yusp.web.client.esb.ecif.g10501;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.ListArrayInfo;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g10501.req.G10501ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.G10501RespService;
import cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g00202)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G10501Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G10501Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 对公及同业客户清单查询
     *
     * @param g10501ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g10501:对公及同业客户清单查询")
    @PostMapping("/g10501")
    protected @ResponseBody
    ResultDto<G10501RespDto> g10501(@Validated @RequestBody G10501ReqDto g10501ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10501.key, EsbEnum.TRADE_CODE_G10501.value, JSON.toJSONString(g10501ReqDto));

        cn.com.yusys.yusp.online.client.esb.ecif.g10501.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g10501.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.Service();
        G10501ReqService g10501ReqService = new G10501ReqService();
        G10501RespService g10501RespService = new G10501RespService();
        G10501RespDto g10501RespDto = new G10501RespDto();
        ResultDto<G10501RespDto> g10501ResultDto = new ResultDto<G10501RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();

        try {
            //  将G10501ReqDto转换成reqService
            BeanUtils.copyProperties(g10501ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_G10501.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            g10501ReqService.setService(reqService);
            // 将g10501ReqService转换成g10501ReqServiceMap
            Map g10501ReqServiceMap = beanMapUtil.beanToMap(g10501ReqService);
            context.put("tradeDataMap", g10501ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10501.key, EsbEnum.TRADE_CODE_G10501.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G10501.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10501.key, EsbEnum.TRADE_CODE_G10501.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g10501RespService = beanMapUtil.mapToBean(tradeDataMap, G10501RespService.class, G10501RespService.class);
            respService = g10501RespService.getService();

            //  将G10501RespDto封装到ResultDto<G10501RespDto>
            g10501ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g10501ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G10501RespDto
                BeanUtils.copyProperties(respService, g10501RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.List g10501List = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.List());
                respService.setList(g10501List);
                cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.List list = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp.List());
                if (CollectionUtils.nonEmpty(list.getRecord())) {
                    List<Record> recordList = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<Record>());
                    List<ListArrayInfo> listArrayInfos = new ArrayList<ListArrayInfo>();
                    // 遍历record传值塞入listArrayInfo
                    for (Record record : recordList) {
                        ListArrayInfo listArrayInfo = new ListArrayInfo();
                        BeanUtils.copyProperties(record, listArrayInfo);
                        listArrayInfos.add(listArrayInfo);
                    }
                    g10501RespDto.setListArrayInfo(listArrayInfos);
                }

                g10501ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g10501ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                g10501ResultDto.setCode(EpbEnum.EPB099999.key);
                g10501ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10501.key, EsbEnum.TRADE_CODE_G10501.value, e.getMessage());
            g10501ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g10501ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g10501ResultDto.setData(g10501RespDto);

        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10501.key, EsbEnum.TRADE_CODE_G10501.value, JSON.toJSONString(g10501ResultDto));
        return g10501ResultDto;
    }
}
