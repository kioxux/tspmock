package cn.com.yusys.yusp.online.client.esb.circp.fb1213.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 16:44
 * @since 2021/8/10 16:44
 */
public class Record {
    @JsonProperty(value = "GUAR_NO")
    private String GUAR_NO;//押品编号

    @JsonIgnore
    public String getGUAR_NO() {
        return GUAR_NO;
    }

    @JsonIgnore
    public void setGUAR_NO(String GUAR_NO) {
        this.GUAR_NO = GUAR_NO;
    }

    @Override
    public String toString() {
        return "Record{" +
                "GUAR_NO='" + GUAR_NO + '\'' +
                '}';
    }
}
