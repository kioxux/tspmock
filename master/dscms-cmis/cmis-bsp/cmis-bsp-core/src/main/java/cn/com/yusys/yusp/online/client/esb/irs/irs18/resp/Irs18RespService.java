package cn.com.yusys.yusp.online.client.esb.irs.irs18.resp;

/**
 * 响应Service：评级主办权更新
 *
 * @author code-generator
 * @version 1.0
 */
public class Irs18RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}


