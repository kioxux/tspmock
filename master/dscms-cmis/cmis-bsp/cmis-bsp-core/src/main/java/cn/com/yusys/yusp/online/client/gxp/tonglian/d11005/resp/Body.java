package cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.resp;

/**
 * 响应Service：d11005
 *
 * @author chenyong
 * @version 1.0
 */
public class Body {
    private String appno;

    public String getAppno() {
        return appno;
    }

    public void setAppno(String appno) {
        this.appno = appno;
    }

    @Override
    public String toString() {
        return "Body{" +
                "appno='" + appno + '\'' +
                '}';
    }
}
