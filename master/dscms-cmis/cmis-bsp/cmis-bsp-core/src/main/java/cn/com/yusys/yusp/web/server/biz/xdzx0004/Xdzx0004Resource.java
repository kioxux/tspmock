package cn.com.yusys.yusp.web.server.biz.xdzx0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzx0004.req.Xdzx0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzx0004.resp.Xdzx0004RespDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.req.Xdzx0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0004.resp.Xdzx0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EcbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:授权结果反馈接口
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZX0004:授权结果反馈接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzx0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzx0004Resource.class);

	@Autowired
	private DscmsBizZxClientService dscmsBizZxClientService;

    /**
     * 交易码：xdzx0004
     * 交易描述：授权结果反馈接口
     *
     * @param xdzx0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授权结果反馈接口")
    @PostMapping("/xdzx0004")
    @Idempotent({"xdzx0004", "#xdzx0004ReqDto.datasq"})
    protected @ResponseBody
    Xdzx0004RespDto xdzx0004(@Validated @RequestBody Xdzx0004ReqDto xdzx0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004ReqDto));
        Xdzx0004DataReqDto xdzx0004DataReqDto = new Xdzx0004DataReqDto();// 请求Data： 授权结果反馈接口
        Xdzx0004DataRespDto xdzx0004DataRespDto = new Xdzx0004DataRespDto();// 响应Data：授权结果反馈接口
        cn.com.yusys.yusp.dto.server.biz.xdzx0004.req.Data reqData = null; // 请求Data：授权结果反馈接口
        cn.com.yusys.yusp.dto.server.biz.xdzx0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzx0004.resp.Data();// 响应Data：授权结果反馈接口
        Xdzx0004RespDto xdzx0004RespDto = new Xdzx0004RespDto();
		try {
            // 从 xdzx0004ReqDto获取 reqData
            reqData = xdzx0004ReqDto.getData();
            // 将 reqData 拷贝到xdzx0004DataReqDto
            BeanUtils.copyProperties(reqData, xdzx0004DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataReqDto));
            ResultDto<Xdzx0004DataRespDto> xdzx0004DataResultDto = dscmsBizZxClientService.xdzx0004(xdzx0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzx0004RespDto.setErorcd(Optional.ofNullable(xdzx0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzx0004RespDto.setErortx(Optional.ofNullable(xdzx0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzx0004DataResultDto.getCode())) {
                xdzx0004DataRespDto = xdzx0004DataResultDto.getData();
                xdzx0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzx0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzx0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzx0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, e.getMessage());
            xdzx0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzx0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzx0004RespDto.setDatasq(servsq);

        xdzx0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0004.key, DscmsEnum.TRADE_CODE_XDZX0004.value, JSON.toJSONString(xdzx0004RespDto));
        return xdzx0004RespDto;
    }
}
