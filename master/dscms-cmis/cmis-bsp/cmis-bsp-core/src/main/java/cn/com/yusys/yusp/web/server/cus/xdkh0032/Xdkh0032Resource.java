package cn.com.yusys.yusp.web.server.cus.xdkh0032;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0032.req.Xdkh0032ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0032.resp.Xdkh0032RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0032.req.Xdkh0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0032.resp.Xdkh0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信息锁定标志同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0032:信息锁定标志同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0032Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0032
     * 交易描述：信息锁定标志同步
     *
     * @param xdkh0032ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信息锁定标志同步")
    @PostMapping("/xdkh0032")
    //@Idempotent({"xdcakh0032", "#xdkh0032ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0032RespDto xdkh0032(@Validated @RequestBody Xdkh0032ReqDto xdkh0032ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032ReqDto));
        Xdkh0032DataReqDto xdkh0032DataReqDto = new Xdkh0032DataReqDto();// 请求Data： 信息锁定标志同步
        Xdkh0032DataRespDto xdkh0032DataRespDto = new Xdkh0032DataRespDto();// 响应Data：信息锁定标志同步
        Xdkh0032RespDto xdkh0032RespDto = new Xdkh0032RespDto();

        cn.com.yusys.yusp.dto.server.cus.xdkh0032.req.Data reqData = null; // 请求Data：信息锁定标志同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0032.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0032.resp.Data();// 响应Data：信息锁定标志同步

        try {
            // 从 xdkh0032ReqDto获取 reqData
            reqData = xdkh0032ReqDto.getData();
            // 将 reqData 拷贝到xdkh0032DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0032DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032DataReqDto));
            ResultDto<Xdkh0032DataRespDto> xdkh0032DataResultDto = dscmsCusClientService.xdkh0032(xdkh0032DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0032RespDto.setErorcd(Optional.ofNullable(xdkh0032DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0032RespDto.setErortx(Optional.ofNullable(xdkh0032DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0032DataResultDto.getCode())) {
                xdkh0032DataRespDto = xdkh0032DataResultDto.getData();
                xdkh0032RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0032RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0032DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0032DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, e.getMessage());
            xdkh0032RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0032RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0032RespDto.setDatasq(servsq);

        xdkh0032RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0032.key, DscmsEnum.TRADE_CODE_XDKH0032.value, JSON.toJSONString(xdkh0032RespDto));
        return xdkh0032RespDto;
    }
}
