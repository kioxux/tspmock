package cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp;

/**
 * 响应Service：贷后任务推送接口
 * @author wrw
 * @version 1.0
 * @since 2021/8/28 11:00
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否
    private String serno;// 流水号 否

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", serno='" + serno + '\'' +
                '}';
    }
}
