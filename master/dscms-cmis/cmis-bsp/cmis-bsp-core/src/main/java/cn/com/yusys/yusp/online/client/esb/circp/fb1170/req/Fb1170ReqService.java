package cn.com.yusys.yusp.online.client.esb.circp.fb1170.req;

/**
 * 请求Service：最高额借款合同信息推送
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1170ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1170ReqService{" +
                "service=" + service +
                '}';
    }
}
