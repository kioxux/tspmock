package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 16:16
 * @since 2021/5/28 16:16
 */
public class Record {
    private String id;//主键id
    private String warnsignal;//预警指标信号
    private String isrelievesignal;//是否解除信号
    private String receivetime;//接收时间

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWarnsignal() {
        return warnsignal;
    }

    public void setWarnsignal(String warnsignal) {
        this.warnsignal = warnsignal;
    }

    public String getIsrelievesignal() {
        return isrelievesignal;
    }

    public void setIsrelievesignal(String isrelievesignal) {
        this.isrelievesignal = isrelievesignal;
    }

    public String getReceivetime() {
        return receivetime;
    }

    public void setReceivetime(String receivetime) {
        this.receivetime = receivetime;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id='" + id + '\'' +
                "warnsignal='" + warnsignal + '\'' +
                "isrelievesignal='" + isrelievesignal + '\'' +
                "receivetime='" + receivetime + '\'' +
                '}';
    }
}
