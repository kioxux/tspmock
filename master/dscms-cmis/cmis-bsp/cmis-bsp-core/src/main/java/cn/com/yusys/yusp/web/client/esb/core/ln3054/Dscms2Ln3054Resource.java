package cn.com.yusys.yusp.web.client.esb.core.ln3054;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.req.Ln3054ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.resp.Ln3054RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Ln3054ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3054.resp.Ln3054RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3054)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3054Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3054Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3054
     * 交易描述：调整贷款还款方式
     *
     * @param ln3054ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3054:调整贷款还款方式")
    @PostMapping("/ln3054")
    protected @ResponseBody
    ResultDto<Ln3054RespDto> ln3054(@Validated @RequestBody Ln3054ReqDto ln3054ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3054.key, EsbEnum.TRADE_CODE_LN3054.value, JSON.toJSONString(ln3054ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3054.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3054.resp.Service();
        Ln3054ReqService ln3054ReqService = new Ln3054ReqService();
        Ln3054RespService ln3054RespService = new Ln3054RespService();
        Ln3054RespDto ln3054RespDto = new Ln3054RespDto();
        ResultDto<Ln3054RespDto> ln3054ResultDto = new ResultDto<Ln3054RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3054ReqDto转换成reqService
            BeanUtils.copyProperties(ln3054ReqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(ln3054ReqDto.getLstdkhkzh())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3054.req.Lstdkhkzh> reqLstdkhkzhs = Optional.ofNullable(ln3054ReqDto.getLstdkhkzh()).orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Lstdkhkzh_ARRAY lstdkhkzh_ARRAY = Optional.ofNullable(reqService.getLstdkhkzh_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Lstdkhkzh_ARRAY());
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Record> recordList = reqLstdkhkzhs.parallelStream().map(reqLstdkhkzh -> {
                            cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Record();
                            BeanUtils.copyProperties(reqLstdkhkzh, record);
                            return record;
                        }
                ).collect(Collectors.toList());
                lstdkhkzh_ARRAY.setRecord(recordList);
                reqService.setLstdkhkzh_ARRAY(lstdkhkzh_ARRAY);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3054.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3054ReqService.setService(reqService);
            // 将ln3054ReqService转换成ln3054ReqServiceMap
            Map ln3054ReqServiceMap = beanMapUtil.beanToMap(ln3054ReqService);
            context.put("tradeDataMap", ln3054ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3054.key, EsbEnum.TRADE_CODE_LN3054.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3054.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3054.key, EsbEnum.TRADE_CODE_LN3054.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3054RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3054RespService.class, Ln3054RespService.class);
            respService = ln3054RespService.getService();
            //  将respService转换成Ln3054RespDto
            BeanUtils.copyProperties(respService, ln3054RespDto);
            ln3054ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3054ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, ln3054RespDto);
                ln3054ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3054ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3054ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3054ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3054.key, EsbEnum.TRADE_CODE_LN3054.value, e.getMessage());
            ln3054ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3054ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3054ResultDto.setData(ln3054RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3054.key, EsbEnum.TRADE_CODE_LN3054.value, JSON.toJSONString(ln3054ResultDto));
        return ln3054ResultDto;
    }
}
