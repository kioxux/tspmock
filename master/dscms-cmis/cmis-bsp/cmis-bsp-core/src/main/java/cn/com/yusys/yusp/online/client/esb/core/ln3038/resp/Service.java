package cn.com.yusys.yusp.online.client.esb.core.ln3038.resp;

/**
 * 响应Service：贷款资料变更明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String zongbish;//总笔数
    private Lstdkbgmx lstdkbgmx;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getZongbish() {
        return zongbish;
    }

    public void setZongbish(String zongbish) {
        this.zongbish = zongbish;
    }

    public Lstdkbgmx getLstdkbgmx() {
        return lstdkbgmx;
    }

    public void setLstdkbgmx(Lstdkbgmx lstdkbgmx) {
        this.lstdkbgmx = lstdkbgmx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zongbish='" + zongbish + '\'' +
                ", lstdkbgmx=" + lstdkbgmx +
                '}';
    }
}
