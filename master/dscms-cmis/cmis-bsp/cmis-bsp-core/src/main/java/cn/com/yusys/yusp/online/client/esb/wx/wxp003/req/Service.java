package cn.com.yusys.yusp.online.client.esb.wx.wxp003.req;

/**
 * 请求Service：信贷将放款标识推送给移动端
 */
public class Service {
	private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
	private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
	private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
	private String userid;//	柜员号	否	char(7)	是		userid
	private String brchno;//	部门号	否	char(5)	是		brchno
	private String datasq; //全局流水
	private String servdt;//    交易日期
	private String servti;//    交易时间

	private String applid;//对接记录ID
	private String entnam;//企业名称
	private String entpid;//企业代码
	private String postim;//放款时间
	private String connum;//贷款合同号
	private String posamt;//放款金额
	private String posrat;//贷款利率
	private String loanno;//贷款借据号
	private String guartp;//担保方式
	private String fivecg;//五级分类
	private String posbeg;//贷款开始时间
	private String posend;//贷款结束时间
	private String posper;//放款期限
	private String posrea;//放款原因

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getApplid() {
		return applid;
	}

	public void setApplid(String applid) {
		this.applid = applid;
	}

	public String getEntnam() {
		return entnam;
	}

	public void setEntnam(String entnam) {
		this.entnam = entnam;
	}

	public String getEntpid() {
		return entpid;
	}

	public void setEntpid(String entpid) {
		this.entpid = entpid;
	}

	public String getPostim() {
		return postim;
	}

	public void setPostim(String postim) {
		this.postim = postim;
	}

	public String getConnum() {
		return connum;
	}

	public void setConnum(String connum) {
		this.connum = connum;
	}

	public String getPosamt() {
		return posamt;
	}

	public void setPosamt(String posamt) {
		this.posamt = posamt;
	}

	public String getPosrat() {
		return posrat;
	}

	public void setPosrat(String posrat) {
		this.posrat = posrat;
	}

	public String getLoanno() {
		return loanno;
	}

	public void setLoanno(String loanno) {
		this.loanno = loanno;
	}

	public String getGuartp() {
		return guartp;
	}

	public void setGuartp(String guartp) {
		this.guartp = guartp;
	}

	public String getFivecg() {
		return fivecg;
	}

	public void setFivecg(String fivecg) {
		this.fivecg = fivecg;
	}

	public String getPosbeg() {
		return posbeg;
	}

	public void setPosbeg(String posbeg) {
		this.posbeg = posbeg;
	}

	public String getPosend() {
		return posend;
	}

	public void setPosend(String posend) {
		this.posend = posend;
	}

	public String getPosper() {
		return posper;
	}

	public void setPosper(String posper) {
		this.posper = posper;
	}

	public String getPosrea() {
		return posrea;
	}

	public void setPosrea(String posrea) {
		this.posrea = posrea;
	}

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", datasq='" + datasq + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", applid='" + applid + '\'' +
				", entnam='" + entnam + '\'' +
				", entpid='" + entpid + '\'' +
				", postim='" + postim + '\'' +
				", connum='" + connum + '\'' +
				", posamt='" + posamt + '\'' +
				", posrat='" + posrat + '\'' +
				", loanno='" + loanno + '\'' +
				", guartp='" + guartp + '\'' +
				", fivecg='" + fivecg + '\'' +
				", posbeg='" + posbeg + '\'' +
				", posend='" + posend + '\'' +
				", posper='" + posper + '\'' +
				", posrea='" + posrea + '\'' +
				'}';
	}
}
