package cn.com.yusys.yusp.web.server.biz.xdtz0023;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0023.req.Xdtz0023ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0023.resp.Xdtz0023RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0023.req.Xdtz0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0023.resp.Xdtz0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:保证金等级入账
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0023:保证金等级入账")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0023Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0023
     * 交易描述：保证金等级入账
     *
     * @param xdtz0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金等级入账")
    @PostMapping("/xdtz0023")
    //@Idempotent({"xdcatz0023", "#xdtz0023ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0023RespDto xdtz0023(@Validated @RequestBody Xdtz0023ReqDto xdtz0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023ReqDto));
        Xdtz0023DataReqDto xdtz0023DataReqDto = new Xdtz0023DataReqDto();// 请求Data： 保证金等级入账
        Xdtz0023DataRespDto xdtz0023DataRespDto = new Xdtz0023DataRespDto();// 响应Data：保证金等级入账
        Xdtz0023RespDto xdtz0023RespDto = new Xdtz0023RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0023.req.Data reqData = null; // 请求Data：保证金等级入账
        cn.com.yusys.yusp.dto.server.biz.xdtz0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0023.resp.Data();// 响应Data：保证金等级入账
        try {
            // 从 xdtz0023ReqDto获取 reqData
            reqData = xdtz0023ReqDto.getData();
            // 将 reqData 拷贝到xdtz0023DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023DataReqDto));
            ResultDto<Xdtz0023DataRespDto> xdtz0023DataResultDto = dscmsBizTzClientService.xdtz0023(xdtz0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0023RespDto.setErorcd(Optional.ofNullable(xdtz0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0023RespDto.setErortx(Optional.ofNullable(xdtz0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0023DataResultDto.getCode())) {
                xdtz0023DataRespDto = xdtz0023DataResultDto.getData();
                xdtz0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0023DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, e.getMessage());
            xdtz0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0023RespDto.setDatasq(servsq);

        xdtz0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0023.key, DscmsEnum.TRADE_CODE_XDTZ0023.value, JSON.toJSONString(xdtz0023RespDto));
        return xdtz0023RespDto;
    }
}
