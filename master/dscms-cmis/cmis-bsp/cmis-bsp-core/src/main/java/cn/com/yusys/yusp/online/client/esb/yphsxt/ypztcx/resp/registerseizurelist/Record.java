package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerseizurelist;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 16:11
 * @since 2021/7/9 16:11
 */
public class Record {

    private String seioff;//登记簿查封信息-查封机关
    private String seimod;//登记簿查封信息-查封类型
    private String seinum;//登记簿查封信息-查封文号
    private String seistd;//登记簿查封信息-开始日期
    private String seiend;//登记簿查封信息-结束日期
    private String seicad;//登记簿查封信息-注销日期
    private String seirem;//登记簿查封信息-附记

    public String getSeioff() {
        return seioff;
    }

    public void setSeioff(String seioff) {
        this.seioff = seioff;
    }

    public String getSeimod() {
        return seimod;
    }

    public void setSeimod(String seimod) {
        this.seimod = seimod;
    }

    public String getSeinum() {
        return seinum;
    }

    public void setSeinum(String seinum) {
        this.seinum = seinum;
    }

    public String getSeistd() {
        return seistd;
    }

    public void setSeistd(String seistd) {
        this.seistd = seistd;
    }

    public String getSeiend() {
        return seiend;
    }

    public void setSeiend(String seiend) {
        this.seiend = seiend;
    }

    public String getSeicad() {
        return seicad;
    }

    public void setSeicad(String seicad) {
        this.seicad = seicad;
    }

    public String getSeirem() {
        return seirem;
    }

    public void setSeirem(String seirem) {
        this.seirem = seirem;
    }

    @Override
    public String toString() {
        return "Record{" +
                "seioff='" + seioff + '\'' +
                ", seimod='" + seimod + '\'' +
                ", seinum='" + seinum + '\'' +
                ", seistd='" + seistd + '\'' +
                ", seiend='" + seiend + '\'' +
                ", seicad='" + seicad + '\'' +
                ", seirem='" + seirem + '\'' +
                '}';
    }
}
