package cn.com.yusys.yusp.online.client.esb.circp.fb1167.resp;

/**
 * 响应Service：企业征信查询通知
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1167RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1167RespService{" +
                "service=" + service +
                '}';
    }
}
