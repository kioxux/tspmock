package cn.com.yusys.yusp.online.client.esb.core.ib1241.resp;

/**
 * 响应Service：外围自动冲正(出库撤销)
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String yqzhriqi;//原前置日期
    private String yqzhlshu;//原前置流水
    private String chzhriqi;//冲账日期
    private String chzhlshu;//冲账流水

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getYqzhriqi() {
        return yqzhriqi;
    }

    public void setYqzhriqi(String yqzhriqi) {
        this.yqzhriqi = yqzhriqi;
    }

    public String getYqzhlshu() {
        return yqzhlshu;
    }

    public void setYqzhlshu(String yqzhlshu) {
        this.yqzhlshu = yqzhlshu;
    }

    public String getChzhriqi() {
        return chzhriqi;
    }

    public void setChzhriqi(String chzhriqi) {
        this.chzhriqi = chzhriqi;
    }

    public String getChzhlshu() {
        return chzhlshu;
    }

    public void setChzhlshu(String chzhlshu) {
        this.chzhlshu = chzhlshu;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", yqzhriqi='" + yqzhriqi + '\'' +
                ", yqzhlshu='" + yqzhlshu + '\'' +
                ", chzhriqi='" + chzhriqi + '\'' +
                ", chzhlshu='" + chzhlshu + '\'' +
                '}';
    }
}
