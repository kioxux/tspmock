package cn.com.yusys.yusp.web.client.esb.core.ln3038;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3038.req.Ln3038ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3038.resp.Ln3038RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3038.req.Ln3038ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3038.resp.Ln3038RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3038.resp.Lstdkbgmx;
import cn.com.yusys.yusp.online.client.esb.core.ln3038.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3038)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3038Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3038Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3038
     * 交易描述：贷款资料变更明细查询
     *
     * @param ln3038ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3038:贷款资料变更明细查询")
    @PostMapping("/ln3038")
    protected @ResponseBody
    ResultDto<Ln3038RespDto> ln3038(@Validated @RequestBody Ln3038ReqDto ln3038ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3038.key, EsbEnum.TRADE_CODE_LN3038.value, JSON.toJSONString(ln3038ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3038.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3038.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3038.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3038.resp.Service();
        Ln3038ReqService ln3038ReqService = new Ln3038ReqService();
        Ln3038RespService ln3038RespService = new Ln3038RespService();
        Ln3038RespDto ln3038RespDto = new Ln3038RespDto();
        ResultDto<Ln3038RespDto> ln3038ResultDto = new ResultDto<Ln3038RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3038ReqDto转换成reqService
            BeanUtils.copyProperties(ln3038ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3038.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3038ReqService.setService(reqService);
            // 将ln3038ReqService转换成ln3038ReqServiceMap
            Map ln3038ReqServiceMap = beanMapUtil.beanToMap(ln3038ReqService);
            context.put("tradeDataMap", ln3038ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3038.key, EsbEnum.TRADE_CODE_LN3038.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3038.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3038.key, EsbEnum.TRADE_CODE_LN3038.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3038RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3038RespService.class, Ln3038RespService.class);
            respService = ln3038RespService.getService();

            ln3038ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3038ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3038RespDto
                BeanUtils.copyProperties(respService, ln3038RespDto);
                List<cn.com.yusys.yusp.dto.client.esb.core.ln3038.resp.Lstdkbgmx> lstdkbgmxList = new ArrayList<>();
                Lstdkbgmx lstdkbgmx = Optional.ofNullable(respService.getLstdkbgmx()).orElse(new Lstdkbgmx());
                if (CollectionUtils.nonEmpty(lstdkbgmx.getRecordList())) {
                    List<Record> recordList = lstdkbgmx.getRecordList();
                    cn.com.yusys.yusp.dto.client.esb.core.ln3038.resp.Lstdkbgmx lstdkbgmxdto = new cn.com.yusys.yusp.dto.client.esb.core.ln3038.resp.Lstdkbgmx();
                    for (Record record : recordList) {
                        BeanUtils.copyProperties(record, lstdkbgmxdto);
                        lstdkbgmxList.add(lstdkbgmxdto);
                    }
                    ln3038RespDto.setLstdkbgmx(lstdkbgmxList);
                }
                ln3038ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3038ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3038ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3038ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3038.key, EsbEnum.TRADE_CODE_LN3038.value, e.getMessage());
            ln3038ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3038ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3038ResultDto.setData(ln3038RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3038.key, EsbEnum.TRADE_CODE_LN3038.value, JSON.toJSONString(ln3038ResultDto));
        return ln3038ResultDto;
    }
}
