package cn.com.yusys.yusp.web.server.biz.xdtz0034;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0034.req.Xdtz0034ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0034.resp.Xdtz0034RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0034.req.Xdtz0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0034.resp.Xdtz0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号查询申请人是否有行内信用记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0034:根据客户号查询申请人是否有行内信用记录")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0034Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0034
     * 交易描述：根据客户号查询申请人是否有行内信用记录
     *
     * @param xdtz0034ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询申请人是否有行内信用记录")
    @PostMapping("/xdtz0034")
    //@Idempotent({"xdcatz0034", "#xdtz0034ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0034RespDto xdtz0034(@Validated @RequestBody Xdtz0034ReqDto xdtz0034ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034ReqDto));
        Xdtz0034DataReqDto xdtz0034DataReqDto = new Xdtz0034DataReqDto();// 请求Data： 根据客户号查询申请人是否有行内信用记录
        Xdtz0034DataRespDto xdtz0034DataRespDto = new Xdtz0034DataRespDto();// 响应Data：根据客户号查询申请人是否有行内信用记录
        Xdtz0034RespDto xdtz0034RespDto = new Xdtz0034RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0034.req.Data reqData = null; // 请求Data：根据客户号查询申请人是否有行内信用记录
        cn.com.yusys.yusp.dto.server.biz.xdtz0034.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0034.resp.Data();// 响应Data：根据客户号查询申请人是否有行内信用记录
        try {
            // 从 xdtz0034ReqDto获取 reqData
            reqData = xdtz0034ReqDto.getData();
            // 将 reqData 拷贝到xdtz0034DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0034DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataReqDto));
            ResultDto<Xdtz0034DataRespDto> xdtz0034DataResultDto = dscmsBizTzClientService.xdtz0034(xdtz0034DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0034RespDto.setErorcd(Optional.ofNullable(xdtz0034DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0034RespDto.setErortx(Optional.ofNullable(xdtz0034DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0034DataResultDto.getCode())) {
                xdtz0034DataRespDto = xdtz0034DataResultDto.getData();
                xdtz0034RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0034RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0034DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0034DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, e.getMessage());
            xdtz0034RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0034RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0034RespDto.setDatasq(servsq);

        xdtz0034RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0034.key, DscmsEnum.TRADE_CODE_XDTZ0034.value, JSON.toJSONString(xdtz0034RespDto));
        return xdtz0034RespDto;
    }
}
