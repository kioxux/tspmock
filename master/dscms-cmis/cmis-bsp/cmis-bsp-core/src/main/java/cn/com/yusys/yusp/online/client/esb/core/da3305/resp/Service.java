package cn.com.yusys.yusp.online.client.esb.core.da3305.resp;

import java.math.BigDecimal;

/**
 * 响应Service：待变现抵债资产销账
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private BigDecimal dbxdzzic;//待变现抵债资产
    private String fygzzhao;//费用挂账账号
    private BigDecimal feiyfase;//费用发生额
    private BigDecimal bnxyijje;//变现溢价金额
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String yngyjigo;//营业机构
    private String fygzzzxh;//费用挂账账号子序号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public String getFygzzhao() {
        return fygzzhao;
    }

    public void setFygzzhao(String fygzzhao) {
        this.fygzzhao = fygzzhao;
    }

    public BigDecimal getFeiyfase() {
        return feiyfase;
    }

    public void setFeiyfase(BigDecimal feiyfase) {
        this.feiyfase = feiyfase;
    }

    public BigDecimal getBnxyijje() {
        return bnxyijje;
    }

    public void setBnxyijje(BigDecimal bnxyijje) {
        this.bnxyijje = bnxyijje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getFygzzzxh() {
        return fygzzzxh;
    }

    public void setFygzzzxh(String fygzzzxh) {
        this.fygzzzxh = fygzzzxh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", dbxdzzic=" + dbxdzzic +
                ", fygzzhao='" + fygzzhao + '\'' +
                ", feiyfase=" + feiyfase +
                ", bnxyijje=" + bnxyijje +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", fygzzzxh='" + fygzzzxh + '\'' +
                '}';
    }
}
