package cn.com.yusys.yusp.web.server.biz.xdht0034;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0034.req.Xdht0034ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0034.resp.Xdht0034RespDto;
import cn.com.yusys.yusp.dto.server.xdht0034.req.Xdht0034DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0034.resp.Xdht0034DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDHT0034:根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0034Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0034Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0034
     * 交易描述：根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
     *
     * @param xdht0034ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额")
    @PostMapping("/xdht0034")
    //@Idempotent({"xdcaht0034", "#xdht0034ReqDto.datasq"})
    protected @ResponseBody
    Xdht0034RespDto xdht0034(@Validated @RequestBody Xdht0034ReqDto xdht0034ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034ReqDto));
        Xdht0034DataReqDto xdht0034DataReqDto = new Xdht0034DataReqDto();// 请求Data： 根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
        Xdht0034DataRespDto xdht0034DataRespDto = new Xdht0034DataRespDto();// 响应Data：根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
        Xdht0034RespDto xdht0034RespDto = new Xdht0034RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0034.req.Data reqData = null; // 请求Data：根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
        cn.com.yusys.yusp.dto.server.biz.xdht0034.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0034.resp.Data();// 响应Data：根据核心客户号查询最近生效的一笔优企贷、优农贷合同金额
        try {
            // 从 xdht0034ReqDto获取 reqData
            reqData = xdht0034ReqDto.getData();
            // 将 reqData 拷贝到xdht0034DataReqDto
            BeanUtils.copyProperties(reqData, xdht0034DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataReqDto));
            ResultDto<Xdht0034DataRespDto> xdht0034DataResultDto = dscmsBizHtClientService.xdht0034(xdht0034DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0034RespDto.setErorcd(Optional.ofNullable(xdht0034DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0034RespDto.setErortx(Optional.ofNullable(xdht0034DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0034DataResultDto.getCode())) {
                xdht0034DataRespDto = xdht0034DataResultDto.getData();
                xdht0034RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0034RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0034DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0034DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, e.getMessage());
            xdht0034RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0034RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0034RespDto.setDatasq(servsq);

        xdht0034RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0034.key, DscmsEnum.TRADE_CODE_XDHT0034.value, JSON.toJSONString(xdht0034RespDto));
        return xdht0034RespDto;
    }
}