package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

import java.math.BigDecimal;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 19:17
 * @since 2021/6/7 19:17
 */
public class Record4 {
    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private BigDecimal jiaoyije;//交易金额
    private String xctanxrq;//下次摊销日期
    private BigDecimal czytanje;//已摊销金额
    private BigDecimal czdtanje;//待摊销金额
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String farendma;//法人代码
    private String weihguiy;//维护柜员
    private String weihjigo;//维护机构
    private String weihriqi;//维护日期
    private String weihshij;//维护时间
    private Integer shijchuo;//时间戳
    private String jiluztai;//记录状态

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getXctanxrq() {
        return xctanxrq;
    }

    public void setXctanxrq(String xctanxrq) {
        this.xctanxrq = xctanxrq;
    }

    public BigDecimal getCzytanje() {
        return czytanje;
    }

    public void setCzytanje(BigDecimal czytanje) {
        this.czytanje = czytanje;
    }

    public BigDecimal getCzdtanje() {
        return czdtanje;
    }

    public void setCzdtanje(BigDecimal czdtanje) {
        this.czdtanje = czdtanje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getWeihshij() {
        return weihshij;
    }

    public void setWeihshij(String weihshij) {
        this.weihshij = weihshij;
    }

    public Integer getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Integer shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "LstDztxmx{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", jiaoyije=" + jiaoyije +
                ", xctanxrq='" + xctanxrq + '\'' +
                ", czytanje=" + czytanje +
                ", czdtanje=" + czdtanje +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", farendma='" + farendma + '\'' +
                ", weihguiy='" + weihguiy + '\'' +
                ", weihjigo='" + weihjigo + '\'' +
                ", weihriqi='" + weihriqi + '\'' +
                ", weihshij='" + weihshij + '\'' +
                ", shijchuo=" + shijchuo +
                ", jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
