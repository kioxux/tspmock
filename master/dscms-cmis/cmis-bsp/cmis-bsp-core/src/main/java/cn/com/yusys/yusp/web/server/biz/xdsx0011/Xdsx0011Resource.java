package cn.com.yusys.yusp.web.server.biz.xdsx0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0011.req.Xdsx0011ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0011.resp.Xdsx0011RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.req.Xdsx0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0011.resp.Xdsx0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询优惠（省心E付）
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0011:查询优惠（省心E付）")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0011Resource.class);
	@Autowired
	private DscmsBizSxClientService dscmsBizSxClientService;
    /**
     * 交易码：xdsx0011
     * 交易描述：查询优惠（省心E付）
     *
     * @param xdsx0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优惠（省心E付）")
    @PostMapping("/xdsx0011")
    //@Idempotent({"xdcasx0011", "#xdsx0011ReqDto.datasq"})
    protected @ResponseBody
	Xdsx0011RespDto xdsx0011(@Validated @RequestBody Xdsx0011ReqDto xdsx0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, JSON.toJSONString(xdsx0011ReqDto));
        Xdsx0011DataReqDto xdsx0011DataReqDto = new Xdsx0011DataReqDto();// 请求Data： 查询优惠（省心E付）
        Xdsx0011DataRespDto xdsx0011DataRespDto = new Xdsx0011DataRespDto();// 响应Data：查询优惠（省心E付）
		Xdsx0011RespDto xdsx0011RespDto = new Xdsx0011RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdsx0011.req.Data reqData = null; // 请求Data：查询优惠（省心E付）
		cn.com.yusys.yusp.dto.server.biz.xdsx0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0011.resp.Data();// 响应Data：查询优惠（省心E付）
        //  此处包导入待确定 结束
        try {
            // 从 xdsx0011ReqDto获取 reqData
            reqData = xdsx0011ReqDto.getData();
            // 将 reqData 拷贝到xdsx0011DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0011DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, JSON.toJSONString(xdsx0011DataReqDto));
            ResultDto<Xdsx0011DataRespDto> xdsx0011DataResultDto = dscmsBizSxClientService.xdsx0011(xdsx0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, JSON.toJSONString(xdsx0011DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdsx0011RespDto.setErorcd(Optional.ofNullable(xdsx0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0011RespDto.setErortx(Optional.ofNullable(xdsx0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0011DataResultDto.getCode())) {
                xdsx0011DataRespDto = xdsx0011DataResultDto.getData();
                xdsx0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0011DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0011RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0011RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0011RespDto.setDatasq(servsq);

        xdsx0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, JSON.toJSONString(xdsx0011RespDto));
        return xdsx0011RespDto;
    }
}
