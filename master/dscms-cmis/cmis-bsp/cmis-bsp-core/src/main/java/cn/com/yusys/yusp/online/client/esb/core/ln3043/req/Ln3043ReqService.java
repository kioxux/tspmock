package cn.com.yusys.yusp.online.client.esb.core.ln3043.req;

/**
 * 请求Service：贷款指定期供归还
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3043ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

