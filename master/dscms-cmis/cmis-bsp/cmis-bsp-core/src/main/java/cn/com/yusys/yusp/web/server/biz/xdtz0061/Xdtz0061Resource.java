package cn.com.yusys.yusp.web.server.biz.xdtz0061;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0061.req.Xdtz0061ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0061.resp.Xdtz0061RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0061.req.Xdtz0061DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0061.resp.Xdtz0061DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小微电子签约借据号生成
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0061:小微电子签约借据号生成")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0061Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0061Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0061
     * 交易描述：小微电子签约借据号生成
     *
     * @param xdtz0061ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微电子签约借据号生成")
    @PostMapping("/xdtz0061")
    //@Idempotent({"xdtz0061", "#xdtz0061ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0061RespDto xdtz0061(@Validated @RequestBody Xdtz0061ReqDto xdtz0061ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061ReqDto));
        Xdtz0061DataReqDto xdtz0061DataReqDto = new Xdtz0061DataReqDto();// 请求Data： 更新信贷台账信息
        Xdtz0061DataRespDto xdtz0061DataRespDto = new Xdtz0061DataRespDto();// 响应Data：更新信贷台账信息
        Xdtz0061RespDto xdtz0061RespDto = new Xdtz0061RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0061.req.Data reqData = null; // 请求Data：更新信贷台账信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0061.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0061.resp.Data();// 响应Data：更新信贷台账信息
        try {
            // 从 xdtz0061ReqDto获取 reqData
            reqData = xdtz0061ReqDto.getData();
            // 将 reqData 拷贝到xdtz0061DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0061DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataReqDto));
            ResultDto<Xdtz0061DataRespDto> xdtz0061DataResultDto = dscmsBizTzClientService.xdtz0061(xdtz0061DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0061RespDto.setErorcd(Optional.ofNullable(xdtz0061DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0061RespDto.setErortx(Optional.ofNullable(xdtz0061DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0061DataResultDto.getCode())) {
                xdtz0061DataRespDto = xdtz0061DataResultDto.getData();
                if(Objects.equals(DscmsBizCzEnum.OP_FLAG_F.value,xdtz0061DataRespDto.getOpFlag())){
                    xdtz0061RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                    xdtz0061RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
                }else{
                    xdtz0061RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                    xdtz0061RespDto.setErortx(SuccessEnum.SUCCESS.value);
                }

                // 将 xdtz0061DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0061DataRespDto, respData);
            }else{
                xdtz0061DataRespDto = xdtz0061DataResultDto.getData();
                xdtz0061RespDto.setErorcd(EpbEnum.EPB099999.key);
                xdtz0061RespDto.setErortx(xdtz0061DataRespDto.getOpMsg());
                // 将 xdtz0061DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0061DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, e.getMessage());
            xdtz0061RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0061RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0061RespDto.setDatasq(servsq);

        xdtz0061RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0061.key, DscmsEnum.TRADE_CODE_XDTZ0061.value, JSON.toJSONString(xdtz0061RespDto));
        return xdtz0061RespDto;
    }
}
