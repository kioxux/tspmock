package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Service：工商数据联网核查
 *
 * @author tangxun
 * @version 1.0.0
 * @date 2021/4/11 下午4:28
 */
@JsonPropertyOrder(alphabetic = true)
public class ZsnewRespService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;// 响应信息
    @JsonProperty(value = "data")
    private Data data;//返回参数

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ZsnewRespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", data=" + data +
                '}';
    }
}
