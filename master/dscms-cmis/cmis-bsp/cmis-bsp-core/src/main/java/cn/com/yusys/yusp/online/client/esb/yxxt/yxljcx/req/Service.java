package cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.req;

/**
 * 请求Service：影像图像路径查询
 */
public class Service {

    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水

    private String docidd;//查询索引值
    private String tpcode;//根节点
    private String sncode;//子节点
    private String imname;//图像名
    private String rqtem1;//请求备用字段1：是否最新影像 [1：最新文件（时间），2：最新文件（相同名称情况下）]
    private String rqtem2;//请求备用字段2：是否显示文件名[1：显示文件名]
    private String rqtem3;//请求备用字段3
    private String rqtem4;//请求备用字段4

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDocidd() {
        return docidd;
    }

    public void setDocidd(String docidd) {
        this.docidd = docidd;
    }

    public String getTpcode() {
        return tpcode;
    }

    public void setTpcode(String tpcode) {
        this.tpcode = tpcode;
    }

    public String getSncode() {
        return sncode;
    }

    public void setSncode(String sncode) {
        this.sncode = sncode;
    }

    public String getImname() {
        return imname;
    }

    public void setImname(String imname) {
        this.imname = imname;
    }

    public String getRqtem1() {
        return rqtem1;
    }

    public void setRqtem1(String rqtem1) {
        this.rqtem1 = rqtem1;
    }

    public String getRqtem2() {
        return rqtem2;
    }

    public void setRqtem2(String rqtem2) {
        this.rqtem2 = rqtem2;
    }

    public String getRqtem3() {
        return rqtem3;
    }

    public void setRqtem3(String rqtem3) {
        this.rqtem3 = rqtem3;
    }

    public String getRqtem4() {
        return rqtem4;
    }

    public void setRqtem4(String rqtem4) {
        this.rqtem4 = rqtem4;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", docidd='" + docidd + '\'' +
                ", tpcode='" + tpcode + '\'' +
                ", sncode='" + sncode + '\'' +
                ", imname='" + imname + '\'' +
                ", rqtem1='" + rqtem1 + '\'' +
                ", rqtem2='" + rqtem2 + '\'' +
                ", rqtem3='" + rqtem3 + '\'' +
                ", rqtem4='" + rqtem4 + '\'' +
                '}';
    }
}
