package cn.com.yusys.yusp.online.client.esb.ecif.g00202.req;

import java.math.BigDecimal;

/**
 * 请求Service：同业客户开户,关联信息_ARRAY
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@Deprecated
public class RelArrayInfo {
    private String relttp;//关系类型
    private String realna;//关系人姓名
    private String rlennm;//关系人英文名称
    private String idtftp;//证件类型
    private String idtfno;//证件号码
    private String efctdt;//证件生效日期
    private String inefdt;//证件失效日期
    private String invswy;//出资方式
    private String currcy;//出资币种
    private BigDecimal invsam;//出资金额
    private BigDecimal invsrt;//出资占比
    private String sthdtg;//是否控股股东
    private String prpsex;//性别
    private String borndt;//出生日期
    private String nation;//国籍
    private String cutycd;//民族
    private String propts;//居民性质
    private String educlv;//教育水平（学历）
    private String wkutna;//工作单位
    private String projob;//职业
    private String poston;//职务
    private BigDecimal income;//月收入
    private BigDecimal tbcome;//年收入
    private String provce;//省/直辖市/自治区
    private String cityna;//州市(地市或城市)
    private String county;//县/区
    private String homead;//联系地址
    private String hometl;//联系电话
    private String emailx;//邮箱
    private String txpstp;//税收居民类型
    private String addttr;//税收现居英文地址
    private String brctry;//税收出生国家或地区
    private String txnion;//税收居民国（地区）
    private String taxnum;//纳税人识别号
    private String notxrs;//不能提供纳税人识别号原因

    public String getRelttp() {
        return relttp;
    }

    public void setRelttp(String relttp) {
        this.relttp = relttp;
    }

    public String getRealna() {
        return realna;
    }

    public void setRealna(String realna) {
        this.realna = realna;
    }

    public String getRlennm() {
        return rlennm;
    }

    public void setRlennm(String rlennm) {
        this.rlennm = rlennm;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getEfctdt() {
        return efctdt;
    }

    public void setEfctdt(String efctdt) {
        this.efctdt = efctdt;
    }

    public String getInefdt() {
        return inefdt;
    }

    public void setInefdt(String inefdt) {
        this.inefdt = inefdt;
    }

    public String getInvswy() {
        return invswy;
    }

    public void setInvswy(String invswy) {
        this.invswy = invswy;
    }

    public String getCurrcy() {
        return currcy;
    }

    public void setCurrcy(String currcy) {
        this.currcy = currcy;
    }

    public BigDecimal getInvsam() {
        return invsam;
    }

    public void setInvsam(BigDecimal invsam) {
        this.invsam = invsam;
    }

    public BigDecimal getInvsrt() {
        return invsrt;
    }

    public void setInvsrt(BigDecimal invsrt) {
        this.invsrt = invsrt;
    }

    public String getSthdtg() {
        return sthdtg;
    }

    public void setSthdtg(String sthdtg) {
        this.sthdtg = sthdtg;
    }

    public String getPrpsex() {
        return prpsex;
    }

    public void setPrpsex(String prpsex) {
        this.prpsex = prpsex;
    }

    public String getBorndt() {
        return borndt;
    }

    public void setBorndt(String borndt) {
        this.borndt = borndt;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getCutycd() {
        return cutycd;
    }

    public void setCutycd(String cutycd) {
        this.cutycd = cutycd;
    }

    public String getPropts() {
        return propts;
    }

    public void setPropts(String propts) {
        this.propts = propts;
    }

    public String getEduclv() {
        return educlv;
    }

    public void setEduclv(String educlv) {
        this.educlv = educlv;
    }

    public String getWkutna() {
        return wkutna;
    }

    public void setWkutna(String wkutna) {
        this.wkutna = wkutna;
    }

    public String getProjob() {
        return projob;
    }

    public void setProjob(String projob) {
        this.projob = projob;
    }

    public String getPoston() {
        return poston;
    }

    public void setPoston(String poston) {
        this.poston = poston;
    }

    public BigDecimal getIncome() {
        return income;
    }

    public void setIncome(BigDecimal income) {
        this.income = income;
    }

    public BigDecimal getTbcome() {
        return tbcome;
    }

    public void setTbcome(BigDecimal tbcome) {
        this.tbcome = tbcome;
    }

    public String getProvce() {
        return provce;
    }

    public void setProvce(String provce) {
        this.provce = provce;
    }

    public String getCityna() {
        return cityna;
    }

    public void setCityna(String cityna) {
        this.cityna = cityna;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getHometl() {
        return hometl;
    }

    public void setHometl(String hometl) {
        this.hometl = hometl;
    }

    public String getEmailx() {
        return emailx;
    }

    public void setEmailx(String emailx) {
        this.emailx = emailx;
    }

    public String getTxpstp() {
        return txpstp;
    }

    public void setTxpstp(String txpstp) {
        this.txpstp = txpstp;
    }

    public String getAddttr() {
        return addttr;
    }

    public void setAddttr(String addttr) {
        this.addttr = addttr;
    }

    public String getBrctry() {
        return brctry;
    }

    public void setBrctry(String brctry) {
        this.brctry = brctry;
    }

    public String getTxnion() {
        return txnion;
    }

    public void setTxnion(String txnion) {
        this.txnion = txnion;
    }

    public String getTaxnum() {
        return taxnum;
    }

    public void setTaxnum(String taxnum) {
        this.taxnum = taxnum;
    }

    public String getNotxrs() {
        return notxrs;
    }

    public void setNotxrs(String notxrs) {
        this.notxrs = notxrs;
    }

    @Override
    public String toString() {
        return "RelArrayInfo{" +
                "relttp='" + relttp + '\'' +
                ", realna='" + realna + '\'' +
                ", rlennm='" + rlennm + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", efctdt='" + efctdt + '\'' +
                ", inefdt='" + inefdt + '\'' +
                ", invswy='" + invswy + '\'' +
                ", currcy='" + currcy + '\'' +
                ", invsam=" + invsam +
                ", invsrt=" + invsrt +
                ", sthdtg='" + sthdtg + '\'' +
                ", prpsex='" + prpsex + '\'' +
                ", borndt='" + borndt + '\'' +
                ", nation='" + nation + '\'' +
                ", cutycd='" + cutycd + '\'' +
                ", propts='" + propts + '\'' +
                ", educlv='" + educlv + '\'' +
                ", wkutna='" + wkutna + '\'' +
                ", projob='" + projob + '\'' +
                ", poston='" + poston + '\'' +
                ", income=" + income +
                ", tbcome=" + tbcome +
                ", provce='" + provce + '\'' +
                ", cityna='" + cityna + '\'' +
                ", county='" + county + '\'' +
                ", homead='" + homead + '\'' +
                ", hometl='" + hometl + '\'' +
                ", emailx='" + emailx + '\'' +
                ", txpstp='" + txpstp + '\'' +
                ", addttr='" + addttr + '\'' +
                ", brctry='" + brctry + '\'' +
                ", txnion='" + txnion + '\'' +
                ", taxnum='" + taxnum + '\'' +
                ", notxrs='" + notxrs + '\'' +
                '}';
    }
}
