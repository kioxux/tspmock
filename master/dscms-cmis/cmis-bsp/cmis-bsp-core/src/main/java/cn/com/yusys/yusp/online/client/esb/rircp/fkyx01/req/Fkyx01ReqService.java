package cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.req;

/**
 * 请求Service：优享贷客户经理分配通知接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Fkyx01ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      

