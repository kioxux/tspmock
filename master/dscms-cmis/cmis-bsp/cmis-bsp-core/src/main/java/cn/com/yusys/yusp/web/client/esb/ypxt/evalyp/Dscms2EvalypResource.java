package cn.com.yusys.yusp.web.client.esb.ypxt.evalyp;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypListInfoReq;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypListInfoResp;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.EvalypReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp.EvalypRespService;
import cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(evalyp)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2EvalypResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2EvalypResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：evalyp
     * 交易描述：押品我行确认价值同步接口
     *
     * @param evalypReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("evalyp:押品我行确认价值同步接口")
    @PostMapping("/evalyp")
    protected @ResponseBody
    ResultDto<EvalypRespDto> evalyp(@Validated @RequestBody EvalypReqDto evalypReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_EVALYP.key, EsbEnum.TRADE_CODE_EVALYP.value, JSON.toJSONString(evalypReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.List list = new cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.List();
        EvalypReqService evalypReqService = new EvalypReqService();
        EvalypRespService evalypRespService = new EvalypRespService();
        EvalypRespDto evalypRespDto = new EvalypRespDto();
        ResultDto<EvalypRespDto> evalypResultDto = new ResultDto<EvalypRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将evalypReqDto转换成reqService
            BeanUtils.copyProperties(evalypReqDto, reqService);
            if (CollectionUtils.nonEmpty(evalypReqDto.getEvalypListInfoReq())) {
                List<EvalypListInfoReq> evalypListInfoReqs = evalypReqDto.getEvalypListInfoReq();
                List<cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.Record> recordList = new ArrayList<cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.Record>();
                for (EvalypListInfoReq evalypListInfoReq : evalypListInfoReqs) {
                    cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req.Record();
                    BeanUtils.copyProperties(evalypListInfoReq, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_EVALYP.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间


            evalypReqService.setService(reqService);
            // 将evalypReqService转换成evalypReqServiceMap
            Map evalypReqServiceMap = beanMapUtil.beanToMap(evalypReqService);
            context.put("tradeDataMap", evalypReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_EVALYP.key, EsbEnum.TRADE_CODE_EVALYP.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_EVALYP.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_EVALYP.key, EsbEnum.TRADE_CODE_EVALYP.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            evalypRespService = beanMapUtil.mapToBean(tradeDataMap, EvalypRespService.class, EvalypRespService.class);
            respService = evalypRespService.getService();

            evalypResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            evalypResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成EvalypRespDto
                BeanUtils.copyProperties(respService, evalypRespDto);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> records = respService.getList().getRecord();
                    List<EvalypListInfoResp> evalypListInfoResps = new ArrayList<EvalypListInfoResp>();
                    for (cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp.Record recordResp : records) {
                        EvalypListInfoResp evalypListInfoResp = new EvalypListInfoResp();
                        BeanUtils.copyProperties(recordResp, evalypListInfoResp);
                        evalypListInfoResps.add(evalypListInfoResp);
                    }
                    evalypRespDto.setEvalypListInfoResp(evalypListInfoResps);
                }
                evalypResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                evalypResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                evalypResultDto.setCode(EpbEnum.EPB099999.key);
                evalypResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_EVALYP.key, EsbEnum.TRADE_CODE_EVALYP.value, e.getMessage());
            evalypResultDto.setCode(EpbEnum.EPB099999.key);//9999
            evalypResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        evalypResultDto.setData(evalypRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_EVALYP.key, EsbEnum.TRADE_CODE_EVALYP.value, JSON.toJSONString(evalypResultDto));
        return evalypResultDto;
    }
}
