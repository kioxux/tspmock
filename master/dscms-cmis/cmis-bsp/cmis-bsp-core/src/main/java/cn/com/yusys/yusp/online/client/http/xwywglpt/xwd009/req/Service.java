package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/9/3 16:07
 * @since 2021/9/3 16:07
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号

    private String businessId;//行内业务流水号
    private String identNo;//申请人身份证号码
    private String applName;//申请人名称
    private String applLimit;//贷款期限
    private String industry;//行业
    private String grade;//客户评级
    private String businessSource;//业务来源
    private String applInterest;//申请利率
    private String customerManagerOpinion;//客户经理意见
    private String applReason;//申请原因
    private String guaranteeType;//担保类型
    private String creator;//申请人工号
    private String collateralType;//抵押物类型
    private String rateBranch;//分部
    private String newCustomers;//新老客户
    private String repayWay; // 还款方式
    private String creditAuthCodeList;//贷款授权码

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getIdentNo() {
        return identNo;
    }

    public void setIdentNo(String identNo) {
        this.identNo = identNo;
    }

    public String getApplName() {
        return applName;
    }

    public void setApplName(String applName) {
        this.applName = applName;
    }

    public String getApplLimit() {
        return applLimit;
    }

    public void setApplLimit(String applLimit) {
        this.applLimit = applLimit;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getBusinessSource() {
        return businessSource;
    }

    public void setBusinessSource(String businessSource) {
        this.businessSource = businessSource;
    }

    public String getApplInterest() {
        return applInterest;
    }

    public void setApplInterest(String applInterest) {
        this.applInterest = applInterest;
    }

    public String getCustomerManagerOpinion() {
        return customerManagerOpinion;
    }

    public void setCustomerManagerOpinion(String customerManagerOpinion) {
        this.customerManagerOpinion = customerManagerOpinion;
    }

    public String getApplReason() {
        return applReason;
    }

    public void setApplReason(String applReason) {
        this.applReason = applReason;
    }

    public String getGuaranteeType() {
        return guaranteeType;
    }

    public void setGuaranteeType(String guaranteeType) {
        this.guaranteeType = guaranteeType;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getCollateralType() {
        return collateralType;
    }

    public void setCollateralType(String collateralType) {
        this.collateralType = collateralType;
    }

    public String getRateBranch() {
        return rateBranch;
    }

    public void setRateBranch(String rateBranch) {
        this.rateBranch = rateBranch;
    }

    public String getNewCustomers() {
        return newCustomers;
    }

    public void setNewCustomers(String newCustomers) {
        this.newCustomers = newCustomers;
    }

    public String getRepayWay() {
        return repayWay;
    }

    public void setRepayWay(String repayWay) {
        this.repayWay = repayWay;
    }

    public String getCreditAuthCodeList() {
        return creditAuthCodeList;
    }

    public void setCreditAuthCodeList(String creditAuthCodeList) {
        this.creditAuthCodeList = creditAuthCodeList;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", businessId='" + businessId + '\'' +
                ", identNo='" + identNo + '\'' +
                ", applName='" + applName + '\'' +
                ", applLimit='" + applLimit + '\'' +
                ", industry='" + industry + '\'' +
                ", grade='" + grade + '\'' +
                ", businessSource='" + businessSource + '\'' +
                ", applInterest='" + applInterest + '\'' +
                ", customerManagerOpinion='" + customerManagerOpinion + '\'' +
                ", applReason='" + applReason + '\'' +
                ", guaranteeType='" + guaranteeType + '\'' +
                ", creator='" + creator + '\'' +
                ", collateralType='" + collateralType + '\'' +
                ", rateBranch='" + rateBranch + '\'' +
                ", newCustomers='" + newCustomers + '\'' +
                ", repayWay='" + repayWay + '\'' +
                ", creditAuthCodeList='" + creditAuthCodeList + '\'' +
                '}';
    }

}
