package cn.com.yusys.yusp.online.client.esb.core.ln3054.req;

import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Service：多还款账户
 */
public class Lstdkhkzh_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3054.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhkzh{" +
                "record=" + record +
                '}';
    }
}
