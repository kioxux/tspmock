package cn.com.yusys.yusp.web.server.biz.xdcz0018;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0018.req.Xdcz0018ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0018.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0018.resp.Xdcz0018RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0018.req.Xdcz0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0018.resp.Xdcz0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:家速贷、极速快贷贷款申请
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0018:家速贷、极速快贷贷款申请")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0018Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0018
     * 交易描述：家速贷、极速快贷贷款申请
     *
     * @param xdcz0018ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("家速贷、极速快贷贷款申请")
    @PostMapping("/xdcz0018")
   //@Idempotent({"xdcz0018", "#xdcz0018ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0018RespDto xdcz0018(@Validated @RequestBody Xdcz0018ReqDto xdcz0018ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018ReqDto));
        Xdcz0018DataReqDto xdcz0018DataReqDto = new Xdcz0018DataReqDto();// 请求Data： 电子保函开立
        Xdcz0018DataRespDto xdcz0018DataRespDto = new Xdcz0018DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0018.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0018RespDto xdcz0018RespDto = new Xdcz0018RespDto();
        try {
            // 从 xdcz0018ReqDto获取 reqData
            reqData = xdcz0018ReqDto.getData();
            // 将 reqData 拷贝到xdcz0018DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0018DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataReqDto));
            ResultDto<Xdcz0018DataRespDto> xdcz0018DataResultDto = dscmsBizCzClientService.xdcz0018(xdcz0018DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0018RespDto.setErorcd(Optional.ofNullable(xdcz0018DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0018RespDto.setErortx(Optional.ofNullable(xdcz0018DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0018DataResultDto.getCode())) {
                xdcz0018DataRespDto = xdcz0018DataResultDto.getData();
                xdcz0018RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0018RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0018DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0018DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, e.getMessage());
            xdcz0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0018RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0018RespDto.setDatasq(servsq);

        xdcz0018RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0018.key, DscmsEnum.TRADE_CODE_XDCZ0018.value, JSON.toJSONString(xdcz0018RespDto));
        return xdcz0018RespDto;
    }
}
