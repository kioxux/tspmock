package cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.req;

/**
 * 请求Service：从风控获取客户详细信息
 * @author zhugenrong
 * @version 1.0
 */
public class Fbxd03ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd03ReqService{" +
                "service=" + service +
                '}';
    }
}