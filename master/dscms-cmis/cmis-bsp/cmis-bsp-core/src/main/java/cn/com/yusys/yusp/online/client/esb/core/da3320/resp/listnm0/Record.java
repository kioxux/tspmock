package cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0;

import java.math.BigDecimal;

/**
 * 响应Service：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String dzzcbhao;//抵债资产编号
    private String fygzzhao;//费用挂账账号
    private String fygzzzxh;//费用挂账账号子序号
    private String zhanghmc;//账户户名
    private BigDecimal feiyfase;//费用发生额
    private String beizhuxx;//备注

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getFygzzhao() {
        return fygzzhao;
    }

    public void setFygzzhao(String fygzzhao) {
        this.fygzzhao = fygzzhao;
    }

    public String getFygzzzxh() {
        return fygzzzxh;
    }

    public void setFygzzzxh(String fygzzzxh) {
        this.fygzzzxh = fygzzzxh;
    }

    public String getZhanghmc() {
        return zhanghmc;
    }

    public void setZhanghmc(String zhanghmc) {
        this.zhanghmc = zhanghmc;
    }

    public BigDecimal getFeiyfase() {
        return feiyfase;
    }

    public void setFeiyfase(BigDecimal feiyfase) {
        this.feiyfase = feiyfase;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", fygzzhao='" + fygzzhao + '\'' +
                ", fygzzzxh='" + fygzzzxh + '\'' +
                ", zhanghmc='" + zhanghmc + '\'' +
                ", feiyfase=" + feiyfase +
                ", beizhuxx='" + beizhuxx + '\'' +
                '}';
    }
}
