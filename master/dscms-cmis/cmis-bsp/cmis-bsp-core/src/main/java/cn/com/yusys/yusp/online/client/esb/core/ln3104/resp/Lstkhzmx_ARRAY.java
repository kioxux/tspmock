package cn.com.yusys.yusp.online.client.esb.core.ln3104.resp;

/**
 * 响应Dto：贷款客户帐明细查询输出
 */
public class Lstkhzmx_ARRAY {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}  
