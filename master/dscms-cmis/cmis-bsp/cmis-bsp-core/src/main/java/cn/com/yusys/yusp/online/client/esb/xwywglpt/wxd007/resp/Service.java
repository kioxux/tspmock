package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.resp;

import java.math.BigDecimal;

/**
 * 响应Service：请求小V平台综合决策管理列表查询
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String code;//返回码
    private String message;//返回信息
    private String productCode;//产品编码
    private String productType;//产品阶段
    private String indName;//借款人姓名
    private String indCertID;//身份证号
    private BigDecimal creditAmount;//审批额度
    private String createTime;//申请时间
    private String statusDetail;//审批状态

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getIndName() {
        return indName;
    }

    public void setIndName(String indName) {
        this.indName = indName;
    }

    public String getIndCertID() {
        return indCertID;
    }

    public void setIndCertID(String indCertID) {
        this.indCertID = indCertID;
    }

    public BigDecimal getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(BigDecimal creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getStatusDetail() {
        return statusDetail;
    }

    public void setStatusDetail(String statusDetail) {
        this.statusDetail = statusDetail;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "code='" + code + '\'' +
                "message='" + message + '\'' +
                "productCode='" + productCode + '\'' +
                "productType='" + productType + '\'' +
                "indName='" + indName + '\'' +
                "indCertID='" + indCertID + '\'' +
                "creditAmount='" + creditAmount + '\'' +
                "createTime='" + createTime + '\'' +
                "statusDetail='" + statusDetail + '\'' +
                '}';
    }
}
