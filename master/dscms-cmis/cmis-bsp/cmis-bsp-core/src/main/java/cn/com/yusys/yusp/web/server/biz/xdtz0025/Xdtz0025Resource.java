package cn.com.yusys.yusp.web.server.biz.xdtz0025;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0025.req.Xdtz0025ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0025.resp.Xdtz0025RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0025.req.Xdtz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0025.resp.Xdtz0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询指定贷款开始日的优企贷客户贷款余额合计
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0025:查询指定贷款开始日的优企贷客户贷款余额合计")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0025Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0025
     * 交易描述：查询指定贷款开始日的优企贷客户贷款余额合计
     *
     * @param xdtz0025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询指定贷款开始日的优企贷客户贷款余额合计")
    @PostMapping("/xdtz0025")
    //@Idempotent({"xdcatz0025", "#xdtz0025ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0025RespDto xdtz0025(@Validated @RequestBody Xdtz0025ReqDto xdtz0025ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025ReqDto));
        Xdtz0025DataReqDto xdtz0025DataReqDto = new Xdtz0025DataReqDto();// 请求Data： 查询指定贷款开始日的优企贷客户贷款余额合计
        Xdtz0025DataRespDto xdtz0025DataRespDto = new Xdtz0025DataRespDto();// 响应Data：查询指定贷款开始日的优企贷客户贷款余额合计'
        Xdtz0025RespDto xdtz0025RespDto = new Xdtz0025RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0025.req.Data reqData = null; // 请求Data：查询指定贷款开始日的优企贷客户贷款余额合计
        cn.com.yusys.yusp.dto.server.biz.xdtz0025.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0025.resp.Data();// 响应Data：查询指定贷款开始日的优企贷客户贷款余额合计
        try {
            // 从 xdtz0025ReqDto获取 reqData
            reqData = xdtz0025ReqDto.getData();
            // 将 reqData 拷贝到xdtz0025DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0025DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025DataReqDto));
            ResultDto<Xdtz0025DataRespDto> xdtz0025DataResultDto = dscmsBizTzClientService.xdtz0025(xdtz0025DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0025RespDto.setErorcd(Optional.ofNullable(xdtz0025DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0025RespDto.setErortx(Optional.ofNullable(xdtz0025DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0025DataResultDto.getCode())) {
                xdtz0025DataRespDto = xdtz0025DataResultDto.getData();
                xdtz0025RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0025RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0025DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0025DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, e.getMessage());
            xdtz0025RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0025RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0025RespDto.setDatasq(servsq);

        xdtz0025RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0025.key, DscmsEnum.TRADE_CODE_XDTZ0025.value, JSON.toJSONString(xdtz0025RespDto));
        return xdtz0025RespDto;
    }
}
