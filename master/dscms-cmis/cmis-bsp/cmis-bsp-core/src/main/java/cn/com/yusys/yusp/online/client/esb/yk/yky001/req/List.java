package cn.com.yusys.yusp.online.client.esb.yk.yky001.req;

/**
 * 请求Service：用印申请,关联信息_用印列表
 *
 * @author xuwh
 * @version 1.0
 * @since 2021年8月18日 下午20:54:06
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
