package cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.resp;

/**
 * 响应Service：授信审批作废接口
 * @author muxiang
 * @version 1.0
 * @since 2021/4/16 16:47
 */
public class Fbxw08RespService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
