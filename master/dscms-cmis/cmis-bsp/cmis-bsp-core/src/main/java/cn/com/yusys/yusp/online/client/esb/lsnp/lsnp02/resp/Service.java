package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.resp;

import java.math.BigDecimal;

/**
 * 响应Service：信用卡业务零售评级
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
	private String prcscd;
	private String erortx;// 响应信息
	private String erorcd;// 响应码
	private String servsq;//

    private String businum;//业务受理编号
    private String trstatus;//交易状态
    private String txsts;//业务状态
    private BigDecimal sxf;//手续费
    private String apply_seq;//申请流水号
    private BigDecimal inpret_val;//数字解读值
    private String inpret_val_risk_lvl;//数字解读值风险等级
    private BigDecimal apply_score;//申请评分
    private String apply_score_risk_lvl;//申请评分风险等级
    private BigDecimal lmt_advice;//额度建议
    private BigDecimal price_advice;//定价建议
    private String rule_risk_lvl;//规则风险等级
    private String fund_deposit_base;//住房公积金缴存基数
    private String aum;//AUM值
    private String day_rate;//乐悠金日利率
    private String limit_flag;//自动化审批标志
    private String complex_risk_lvl;//综合风险等级
    private String cust_lvl;//客户等级

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getBusinum() {
        return businum;
    }

    public void setBusinum(String businum) {
        this.businum = businum;
    }

    public String getTrstatus() {
        return trstatus;
    }

    public void setTrstatus(String trstatus) {
        this.trstatus = trstatus;
    }

    public String getTxsts() {
        return txsts;
    }

    public void setTxsts(String txsts) {
        this.txsts = txsts;
    }

    public BigDecimal getSxf() {
        return sxf;
    }

    public void setSxf(BigDecimal sxf) {
        this.sxf = sxf;
    }

    public String getApply_seq() {
        return apply_seq;
    }

    public void setApply_seq(String apply_seq) {
        this.apply_seq = apply_seq;
    }

    public BigDecimal getInpret_val() {
        return inpret_val;
    }

    public void setInpret_val(BigDecimal inpret_val) {
        this.inpret_val = inpret_val;
    }

    public String getInpret_val_risk_lvl() {
        return inpret_val_risk_lvl;
    }

    public void setInpret_val_risk_lvl(String inpret_val_risk_lvl) {
        this.inpret_val_risk_lvl = inpret_val_risk_lvl;
    }

    public BigDecimal getApply_score() {
        return apply_score;
    }

    public void setApply_score(BigDecimal apply_score) {
        this.apply_score = apply_score;
    }

    public String getApply_score_risk_lvl() {
        return apply_score_risk_lvl;
    }

    public void setApply_score_risk_lvl(String apply_score_risk_lvl) {
        this.apply_score_risk_lvl = apply_score_risk_lvl;
    }

    public BigDecimal getLmt_advice() {
        return lmt_advice;
    }

    public void setLmt_advice(BigDecimal lmt_advice) {
        this.lmt_advice = lmt_advice;
    }

    public BigDecimal getPrice_advice() {
        return price_advice;
    }

    public void setPrice_advice(BigDecimal price_advice) {
        this.price_advice = price_advice;
    }

    public String getRule_risk_lvl() {
        return rule_risk_lvl;
    }

    public void setRule_risk_lvl(String rule_risk_lvl) {
        this.rule_risk_lvl = rule_risk_lvl;
    }

    public String getFund_deposit_base() {
        return fund_deposit_base;
    }

    public void setFund_deposit_base(String fund_deposit_base) {
        this.fund_deposit_base = fund_deposit_base;
    }

    public String getAum() {
        return aum;
    }

    public void setAum(String aum) {
        this.aum = aum;
    }

    public String getDay_rate() {
        return day_rate;
    }

    public void setDay_rate(String day_rate) {
        this.day_rate = day_rate;
    }

    public String getLimit_flag() {
        return limit_flag;
    }

    public void setLimit_flag(String limit_flag) {
        this.limit_flag = limit_flag;
    }

    public String getComplex_risk_lvl() {
        return complex_risk_lvl;
    }

    public void setComplex_risk_lvl(String complex_risk_lvl) {
        this.complex_risk_lvl = complex_risk_lvl;
    }

    public String getCust_lvl() {
        return cust_lvl;
    }

    public void setCust_lvl(String cust_lvl) {
        this.cust_lvl = cust_lvl;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", erortx='" + erortx + '\'' +
				", erorcd='" + erorcd + '\'' +
				", servsq='" + servsq + '\'' +
				", businum='" + businum + '\'' +
				", trstatus='" + trstatus + '\'' +
				", txsts='" + txsts + '\'' +
				", sxf=" + sxf +
				", apply_seq='" + apply_seq + '\'' +
				", inpret_val=" + inpret_val +
				", inpret_val_risk_lvl='" + inpret_val_risk_lvl + '\'' +
				", apply_score=" + apply_score +
				", apply_score_risk_lvl='" + apply_score_risk_lvl + '\'' +
				", lmt_advice=" + lmt_advice +
				", price_advice=" + price_advice +
				", rule_risk_lvl='" + rule_risk_lvl + '\'' +
				", fund_deposit_base='" + fund_deposit_base + '\'' +
				", aum='" + aum + '\'' +
				", day_rate='" + day_rate + '\'' +
				", limit_flag='" + limit_flag + '\'' +
				", complex_risk_lvl='" + complex_risk_lvl + '\'' +
				", cust_lvl='" + cust_lvl + '\'' +
				'}';
	}
}
