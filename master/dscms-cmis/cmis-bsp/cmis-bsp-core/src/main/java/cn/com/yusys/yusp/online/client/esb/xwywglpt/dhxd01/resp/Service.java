package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.resp;

/**
 * 响应Service：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
	private String erorcd;//响应码
	private String erortx;//响应信息

    private String cust_lvl;//客户等级

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getCust_lvl() {
        return cust_lvl;
    }

    public void setCust_lvl(String cust_lvl) {
        this.cust_lvl = cust_lvl;
    }

	@Override
	public String toString() {
		return "Service{" +
				"erorcd='" + erorcd + '\'' +
				", erortx='" + erortx + '\'' +
				", cust_lvl='" + cust_lvl + '\'' +
				'}';
	}
}
