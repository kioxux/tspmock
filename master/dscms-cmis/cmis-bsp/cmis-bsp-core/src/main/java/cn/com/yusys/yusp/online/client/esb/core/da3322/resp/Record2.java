package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

import java.math.BigDecimal;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 19:15
 * @since 2021/6/7 19:15
 */
public class Record2 {
    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String jitiriqi;//计提日期
    private BigDecimal jitijine;//计提金额
    private String scjitirq;//上次计提日
    private String beizhuuu;//备注信息
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String farendma;//法人代码
    private String weihguiy;//维护柜员
    private String weihjigo;//维护机构
    private String weihriqi;//维护日期
    private String weihshij;//维护时间
    private Integer shijchuo;//时间戳
    private String jiluztai;//记录状态

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getJitiriqi() {
        return jitiriqi;
    }

    public void setJitiriqi(String jitiriqi) {
        this.jitiriqi = jitiriqi;
    }

    public BigDecimal getJitijine() {
        return jitijine;
    }

    public void setJitijine(BigDecimal jitijine) {
        this.jitijine = jitijine;
    }

    public String getScjitirq() {
        return scjitirq;
    }

    public void setScjitirq(String scjitirq) {
        this.scjitirq = scjitirq;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getWeihshij() {
        return weihshij;
    }

    public void setWeihshij(String weihshij) {
        this.weihshij = weihshij;
    }

    public Integer getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Integer shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "LstDzjtmx{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", jitiriqi='" + jitiriqi + '\'' +
                ", jitijine=" + jitijine +
                ", scjitirq='" + scjitirq + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", farendma='" + farendma + '\'' +
                ", weihguiy='" + weihguiy + '\'' +
                ", weihjigo='" + weihjigo + '\'' +
                ", weihriqi='" + weihriqi + '\'' +
                ", weihshij='" + weihshij + '\'' +
                ", shijchuo=" + shijchuo +
                ", jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
