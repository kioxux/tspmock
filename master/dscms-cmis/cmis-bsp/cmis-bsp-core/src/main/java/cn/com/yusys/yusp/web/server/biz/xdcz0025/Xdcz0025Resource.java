package cn.com.yusys.yusp.web.server.biz.xdcz0025;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0025.req.Xdcz0025ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0025.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0025.resp.Xdcz0025RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0025.req.Xdcz0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0025.resp.Xdcz0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:未中标业务注销
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0025:未中标业务注销")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0025Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0025
     * 交易描述：未中标业务注销
     *
     * @param xdcz0025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("未中标业务注销")
    @PostMapping("/xdcz0025")
   //@Idempotent({"xdcz0025", "#xdcz0025ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0025RespDto xdcz0025(@Validated @RequestBody Xdcz0025ReqDto xdcz0025ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025ReqDto));
        Xdcz0025DataReqDto xdcz0025DataReqDto = new Xdcz0025DataReqDto();// 请求Data： 未中标业务注销
        Xdcz0025DataRespDto xdcz0025DataRespDto = new Xdcz0025DataRespDto();// 响应Data：未中标业务注销

        cn.com.yusys.yusp.dto.server.biz.xdcz0025.req.Data reqData = null; // 请求Data：未中标业务注销
        Data respData = new Data();// 响应Data：未中标业务注销

        Xdcz0025RespDto xdcz0025RespDto = new Xdcz0025RespDto();
        try {
            // 从 xdcz0025ReqDto获取 reqData
            reqData = xdcz0025ReqDto.getData();
            // 将 reqData 拷贝到xdcz0025DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0025DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataReqDto));
            ResultDto<Xdcz0025DataRespDto> xdcz0025DataResultDto = dscmsBizCzClientService.xdcz0025(xdcz0025DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0025RespDto.setErorcd(Optional.ofNullable(xdcz0025DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0025RespDto.setErortx(Optional.ofNullable(xdcz0025DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0025DataResultDto.getCode())) {
                xdcz0025DataRespDto = xdcz0025DataResultDto.getData();
                xdcz0025RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0025RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0025DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0025DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, e.getMessage());
            xdcz0025RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0025RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0025RespDto.setDatasq(servsq);

        xdcz0025RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0025.key, DscmsEnum.TRADE_CODE_XDCZ0025.value, JSON.toJSONString(xdcz0025RespDto));
        return xdcz0025RespDto;
    }
}
