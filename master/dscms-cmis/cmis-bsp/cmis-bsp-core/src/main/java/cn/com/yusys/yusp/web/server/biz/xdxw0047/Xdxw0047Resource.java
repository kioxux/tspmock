package cn.com.yusys.yusp.web.server.biz.xdxw0047;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0047.req.Xdxw0047ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0047.resp.Xdxw0047RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0047.req.Xdxw0047DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0047.resp.Xdxw0047DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:增享贷2.0风控模型A任务推送
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0047:增享贷2.0风控模型A任务推送")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0047Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0047Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0047
     * 交易描述：增享贷2.0风控模型A任务推送
     *
     * @param xdxw0047ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("增享贷2.0风控模型A任务推送")
    @PostMapping("/xdxw0047")
    //@Idempotent({"xdcaxw0047", "#xdxw0047ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0047RespDto xdxw0047(@Validated @RequestBody Xdxw0047ReqDto xdxw0047ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047ReqDto));
        Xdxw0047DataReqDto xdxw0047DataReqDto = new Xdxw0047DataReqDto();// 请求Data： 增享贷2.0风控模型A任务推送
        Xdxw0047DataRespDto xdxw0047DataRespDto = new Xdxw0047DataRespDto();// 响应Data：增享贷2.0风控模型A任务推送
		Xdxw0047RespDto xdxw0047RespDto = new Xdxw0047RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0047.req.Data reqData = null; // 请求Data：增享贷2.0风控模型A任务推送
		cn.com.yusys.yusp.dto.server.biz.xdxw0047.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0047.resp.Data();// 响应Data：增享贷2.0风控模型A任务推送
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0047ReqDto获取 reqData
            reqData = xdxw0047ReqDto.getData();
            // 将 reqData 拷贝到xdxw0047DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0047DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataReqDto));
            ResultDto<Xdxw0047DataRespDto> xdxw0047DataResultDto = dscmsBizXwClientService.xdxw0047(xdxw0047DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0047RespDto.setErorcd(Optional.ofNullable(xdxw0047DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0047RespDto.setErortx(Optional.ofNullable(xdxw0047DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0047DataResultDto.getCode())) {
                xdxw0047DataRespDto = xdxw0047DataResultDto.getData();
                xdxw0047RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0047RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0047DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0047DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, e.getMessage());
            xdxw0047RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0047RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0047RespDto.setDatasq(servsq);

        xdxw0047RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0047.key, DscmsEnum.TRADE_CODE_XDXW0047.value, JSON.toJSONString(xdxw0047RespDto));
        return xdxw0047RespDto;
    }
}
