package cn.com.yusys.yusp.online.client.esb.core.ln3161.req;

/**
 * 请求Service：资产证券化协议登记
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3161ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3161ReqService{" +
                "service=" + service +
                '}';
    }
}
