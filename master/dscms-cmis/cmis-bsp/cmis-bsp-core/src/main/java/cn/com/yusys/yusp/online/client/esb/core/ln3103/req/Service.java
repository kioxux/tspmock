package cn.com.yusys.yusp.online.client.esb.core.ln3103.req;

/**
 * 请求Service：贷款账户交易明细查询
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String qishriqi;//起始日期
    private String zhzhriqi;//终止日期
    private String jiaoyima;//交易码
    private String jiaoyils;//交易流水
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数
    private String jymxpxfs;//交易明细排序方式
    private String qishishj;//起始时间
    private String jieshshj;//结束时间
    private String chaxfanw;//查询范围标志

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getJymxpxfs() {
        return jymxpxfs;
    }

    public void setJymxpxfs(String jymxpxfs) {
        this.jymxpxfs = jymxpxfs;
    }

    public String getQishishj() {
        return qishishj;
    }

    public void setQishishj(String qishishj) {
        this.qishishj = qishishj;
    }

    public String getJieshshj() {
        return jieshshj;
    }

    public void setJieshshj(String jieshshj) {
        this.jieshshj = jieshshj;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", jiaoyima='" + jiaoyima + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", jymxpxfs='" + jymxpxfs + '\'' +
                ", qishishj='" + qishishj + '\'' +
                ", jieshshj='" + jieshshj + '\'' +
                ", chaxfanw='" + chaxfanw + '\'' +
                '}';
    }
}
