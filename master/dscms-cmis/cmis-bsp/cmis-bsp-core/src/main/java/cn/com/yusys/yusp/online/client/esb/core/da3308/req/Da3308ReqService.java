package cn.com.yusys.yusp.online.client.esb.core.da3308.req;

/**
 * 请求Service：抵债资产费用管理
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3308ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
