package cn.com.yusys.yusp.online.client.esb.core.ln3106.resp;

import java.util.List;

/**
 * 贷款利率变更明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Listllbg {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3106.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Listllbg{" +
                "record=" + record +
                '}';
    }
}
