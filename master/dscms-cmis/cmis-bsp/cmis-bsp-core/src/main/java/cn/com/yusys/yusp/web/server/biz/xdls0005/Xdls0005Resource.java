package cn.com.yusys.yusp.web.server.biz.xdls0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdls0005.req.Xdls0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdls0005.resp.Xdls0005RespDto;
import cn.com.yusys.yusp.dto.server.xdls0005.req.Xdls0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0005.resp.Xdls0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizLsClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:智能风控调用信贷通知
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDLS0005:智能风控调用信贷通知")
@RestController
@RequestMapping("/api/dscms")
public class Xdls0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdls0005Resource.class);
    @Autowired
    private DscmsBizLsClientService dscmsBizLsClientService;
    /**
     * 交易码：xdls0005
     * 交易描述：智能风控调用信贷通知
     *
     * @param xdls0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("智能风控调用信贷通知")
    @PostMapping("/xdls0005")
    //@Idempotent({"xdcals0005", "#xdls0005ReqDto.datasq"})
    protected @ResponseBody
    Xdls0005RespDto xdls0005(@Validated @RequestBody Xdls0005ReqDto xdls0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, JSON.toJSONString(xdls0005ReqDto));
        Xdls0005DataReqDto xdls0005DataReqDto = new Xdls0005DataReqDto();// 请求Data： 智能风控调用信贷通知
        Xdls0005DataRespDto xdls0005DataRespDto = new Xdls0005DataRespDto();// 响应Data：智能风控调用信贷通知
        Xdls0005RespDto xdls0005RespDto = new Xdls0005RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdls0005.req.Data reqData = null; // 请求Data：智能风控调用信贷通知
        cn.com.yusys.yusp.dto.server.biz.xdls0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdls0005.resp.Data();// 响应Data：智能风控调用信贷通知

        try {
            // 从 xdls0005ReqDto获取 reqData
            reqData = xdls0005ReqDto.getData();
            // 将 reqData 拷贝到xdls0005DataReqDto
            BeanUtils.copyProperties(reqData, xdls0005DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, JSON.toJSONString(xdls0005DataReqDto));
            ResultDto<Xdls0005DataRespDto> xdls0005DataResultDto = dscmsBizLsClientService.xdls0005(xdls0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, JSON.toJSONString(xdls0005DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdls0005RespDto.setErorcd(Optional.ofNullable(xdls0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdls0005RespDto.setErortx(Optional.ofNullable(xdls0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdls0005DataResultDto.getCode())) {
                xdls0005DataRespDto = xdls0005DataResultDto.getData();
                xdls0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdls0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdls0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdls0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, e.getMessage());
            xdls0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdls0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdls0005RespDto.setDatasq(servsq);

        xdls0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0005.key, DscmsEnum.TRADE_CODE_XDLS0005.value, JSON.toJSONString(xdls0005RespDto));
        return xdls0005RespDto;
    }
}
