package cn.com.yusys.yusp.web.server.biz.xdtz0058;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0058.req.Xdtz0058ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0058.resp.Xdtz0058RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0058.req.Xdtz0058DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0058.resp.Xdtz0058DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:台账信息通用列表查询
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDTZ0058:台账信息通用列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0058Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0058Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0058
     * 交易描述：台账信息通用列表查询
     *
     * @param xdtz0058ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账信息通用列表查询")
    @PostMapping("/xdtz0058")
    //@Idempotent({"xdcatz0058", "#xdtz0058ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0058RespDto xdtz0058(@Validated @RequestBody Xdtz0058ReqDto xdtz0058ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058ReqDto));
        Xdtz0058DataReqDto xdtz0058DataReqDto = new Xdtz0058DataReqDto();// 请求Data： 台账信息通用列表查询
        Xdtz0058DataRespDto xdtz0058DataRespDto = new Xdtz0058DataRespDto();// 响应Data：台账信息通用列表查询
        Xdtz0058RespDto xdtz0058RespDto = new Xdtz0058RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0058.req.Data reqData = null; // 请求Data：台账信息通用列表查询
        cn.com.yusys.yusp.dto.server.biz.xdtz0058.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0058.resp.Data();// 响应Data：台账信息通用列表查询
        try {
            // 从 xdtz0058ReqDto获取 reqData
            reqData = xdtz0058ReqDto.getData();
            // 将 reqData 拷贝到xdtz0058DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0058DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataReqDto));
            ResultDto<Xdtz0058DataRespDto> xdtz0058DataResultDto = dscmsBizTzClientService.xdtz0058(xdtz0058DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0058RespDto.setErorcd(Optional.ofNullable(xdtz0058DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0058RespDto.setErortx(Optional.ofNullable(xdtz0058DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0058DataResultDto.getCode())) {
                xdtz0058DataRespDto = xdtz0058DataResultDto.getData();
                xdtz0058RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0058RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0058DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0058DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, e.getMessage());
            xdtz0058RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0058RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0058RespDto.setDatasq(servsq);

        xdtz0058RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0058.key, DscmsEnum.TRADE_CODE_XDTZ0058.value, JSON.toJSONString(xdtz0058RespDto));
        return xdtz0058RespDto;
    }
}
