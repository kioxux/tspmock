package cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.req;

/**
 * 请求Service：协议信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class ClfxcxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
