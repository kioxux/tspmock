package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfkjh.Record;

import java.util.List;

/**
 * 响应Dto：贷款放款计划
 *
 * @author code-generator
 * @version 1.0
 */
public class Lstdkfkjh_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfkjh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkfkjh_ARRAY{" +
                "record=" + record +
                '}';
    }
}
