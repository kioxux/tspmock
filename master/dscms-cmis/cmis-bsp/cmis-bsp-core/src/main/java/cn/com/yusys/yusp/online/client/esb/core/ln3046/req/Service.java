package cn.com.yusys.yusp.online.client.esb.core.ln3046.req;

import java.math.BigDecimal;

/**
 * 请求Service：贷款核销归还
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String daikczbz;//业务操作标志
    private String dkjiejuh;//贷款借据号
    private String dkzhangh;//贷款账号
    private String huobdhao;//货币代号
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private BigDecimal hexiaobj;//核销本金
    private BigDecimal hexiaolx;//核销利息
    private BigDecimal yihxbjlx;//已核销本金利息
    private String hetongbh;//合同编号
    private BigDecimal ghbenjin;//归还本金
    private BigDecimal guihlixi;//归还利息
    private String zijnlaiy;//资金来源
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String hexiaozh;//核销来源账号
    private String hexiaozx;//核销来源账号子序号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public BigDecimal getGhbenjin() {
        return ghbenjin;
    }

    public void setGhbenjin(BigDecimal ghbenjin) {
        this.ghbenjin = ghbenjin;
    }

    public BigDecimal getGuihlixi() {
        return guihlixi;
    }

    public void setGuihlixi(BigDecimal guihlixi) {
        this.guihlixi = guihlixi;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHexiaozh() {
        return hexiaozh;
    }

    public void setHexiaozh(String hexiaozh) {
        this.hexiaozh = hexiaozh;
    }

    public String getHexiaozx() {
        return hexiaozx;
    }

    public void setHexiaozx(String hexiaozx) {
        this.hexiaozx = hexiaozx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", hexiaobj=" + hexiaobj +
                ", hexiaolx=" + hexiaolx +
                ", yihxbjlx=" + yihxbjlx +
                ", hetongbh='" + hetongbh + '\'' +
                ", ghbenjin=" + ghbenjin +
                ", guihlixi=" + guihlixi +
                ", zijnlaiy='" + zijnlaiy + '\'' +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", hexiaozh='" + hexiaozh + '\'' +
                ", hexiaozx='" + hexiaozx + '\'' +
                '}';
    }
}
