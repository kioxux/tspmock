package cn.com.yusys.yusp.web.client.esb.ypxt.billyp;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.billyp.req.BillypReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.billyp.resp.BillypRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(billyp)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2BillypResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2BillypResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：billyp
     * 交易描述：票据信息同步接口 0001 代表已经创建押品
     *
     * @param billypReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/billyp")
    protected @ResponseBody
    ResultDto<BillypRespDto> billyp(@Validated @RequestBody BillypReqDto billypReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BILLYP.key, EsbEnum.TRADE_CODE_BILLYP.value, JSON.toJSONString(billypReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.billyp.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.billyp.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.billyp.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.billyp.resp.Service();
        BillypReqService billypReqService = new BillypReqService();
        BillypRespService billypRespService = new BillypRespService();
        BillypRespDto billypRespDto = new BillypRespDto();
        ResultDto<BillypRespDto> billypResultDto = new ResultDto<BillypRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将billypReqDto转换成reqService
            BeanUtils.copyProperties(billypReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_BILLYP.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            billypReqService.setService(reqService);
            // 将billypReqService转换成billypReqServiceMap
            Map billypReqServiceMap = beanMapUtil.beanToMap(billypReqService);
            context.put("tradeDataMap", billypReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BILLYP.key, EsbEnum.TRADE_CODE_BILLYP.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_BILLYP.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BILLYP.key, EsbEnum.TRADE_CODE_BILLYP.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            billypRespService = beanMapUtil.mapToBean(tradeDataMap, BillypRespService.class, BillypRespService.class);
            respService = billypRespService.getService();

            billypResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            billypResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成BillypRespDto
                BeanUtils.copyProperties(respService, billypRespDto);
                billypResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                billypResultDto.setMessage(respService.getErortx());
            } else {
                billypResultDto.setCode(respService.getErorcd());
                billypResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BILLYP.key, EsbEnum.TRADE_CODE_BILLYP.value, e.getMessage());
            billypResultDto.setCode(EpbEnum.EPB099999.key);//9999
            billypResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        billypResultDto.setData(billypRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BILLYP.key, EsbEnum.TRADE_CODE_BILLYP.value, JSON.toJSONString(billypResultDto));
        return billypResultDto;
    }
}
