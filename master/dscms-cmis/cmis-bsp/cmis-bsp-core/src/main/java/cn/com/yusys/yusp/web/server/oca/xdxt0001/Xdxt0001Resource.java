package cn.com.yusys.yusp.web.server.oca.xdxt0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.oca.xdxt0001.req.Xdxt0001ReqDto;
import cn.com.yusys.yusp.dto.server.oca.xdxt0001.resp.Xdxt0001RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.req.Xdxt0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0001.resp.Xdxt0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷同步人力资源
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0001:信贷同步人力资源")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0001Resource.class);

    @Autowired
     private DscmsXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0001
     * 交易描述：信贷同步人力资源
     *
     * @param xdxt0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷同步人力资源")
    @PostMapping("/xdxt0001")
    //@Idempotent({"xdcaxt0001", "#xdxt0001ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0001RespDto xdxt0001(@Validated @RequestBody Xdxt0001ReqDto xdxt0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0001.key, DscmsEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001ReqDto));
        Xdxt0001DataReqDto xdxt0001DataReqDto = new Xdxt0001DataReqDto();// 请求Data： 信贷同步人力资源
        Xdxt0001DataRespDto xdxt0001DataRespDto = new Xdxt0001DataRespDto();// 响应Data：信贷同步人力资源
        Xdxt0001RespDto xdxt0001RespDto = new Xdxt0001RespDto();
        cn.com.yusys.yusp.dto.server.oca.xdxt0001.req.Data reqData = null; // 请求Data：信贷同步人力资源
        cn.com.yusys.yusp.dto.server.oca.xdxt0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.oca.xdxt0001.resp.Data();// 响应Data：信贷同步人力资源
        try {
            // 从 xdxt0001ReqDto获取 reqData
            reqData = xdxt0001ReqDto.getData();
            // 将 reqData 拷贝到xdxt0001DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0001.key, DscmsEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001DataReqDto));
            ResultDto<Xdxt0001DataRespDto> xdxt0001DataResultDto = dscmsXtClientService.xdxt0001(xdxt0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0001.key, DscmsEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0001RespDto.setErorcd(Optional.ofNullable(xdxt0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0001RespDto.setErortx(Optional.ofNullable(xdxt0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0001DataResultDto.getCode())) {
                xdxt0001DataRespDto = xdxt0001DataResultDto.getData();
                xdxt0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0001.key, DscmsEnum.TRADE_CODE_XDXT0001.value, e.getMessage());
            xdxt0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0001RespDto.setDatasq(servsq);

        xdxt0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0001.key, DscmsEnum.TRADE_CODE_XDXT0001.value, JSON.toJSONString(xdxt0001RespDto));
        return xdxt0001RespDto;
    }
}
