package cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.resp;

/**
 * 响应Service：指标通用接口
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String ruleCode;//指标编码
    private String discribe;//指标描述
    private String result;//查询结果

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getDiscribe() {
        return discribe;
    }

    public void setDiscribe(String discribe) {
        this.discribe = discribe;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "servsq='" + servsq + '\'' +
                "ruleCode='" + ruleCode + '\'' +
                "discribe='" + discribe + '\'' +
                "result='" + result + '\'' +
                '}';
    }
}
