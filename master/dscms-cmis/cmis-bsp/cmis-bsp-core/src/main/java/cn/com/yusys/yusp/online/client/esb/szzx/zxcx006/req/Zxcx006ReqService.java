package cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.req;

/**
 * 请求Service：信贷查询地方征信接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Zxcx006ReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
