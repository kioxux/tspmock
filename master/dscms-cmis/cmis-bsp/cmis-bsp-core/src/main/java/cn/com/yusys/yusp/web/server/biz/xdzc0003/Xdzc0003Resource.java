package cn.com.yusys.yusp.web.server.biz.xdzc0003;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0003.req.Xdzc0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0003.resp.Xdzc0003RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0003.req.Xdzc0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0003.resp.Xdzc0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池协议详情查看接口
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZC0003:资产池协议详情查看接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0003Resource.class);

	@Autowired
	private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0003
     * 交易描述：资产池协议详情查看接口
     *
     * @param xdzc0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池协议详情查看接口")
    @PostMapping("/xdzc0003")
    //@Idempotent({"xdzc003", "#xdzc0003ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0003RespDto xdzc0003(@Validated @RequestBody Xdzc0003ReqDto xdzc0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, JSON.toJSONString(xdzc0003ReqDto));
        Xdzc0003DataReqDto xdzc0003DataReqDto = new Xdzc0003DataReqDto();// 请求Data： 客户资产池下资产清单列表查询（出池时查询）
        Xdzc0003DataRespDto xdzc0003DataRespDto = new Xdzc0003DataRespDto();// 响应Data：客户资产池下资产清单列表查询（出池时查询）
        cn.com.yusys.yusp.dto.server.biz.xdzc0003.req.Data reqData = null; // 请求Data：客户资产池下资产清单列表查询（出池时查询）
        cn.com.yusys.yusp.dto.server.biz.xdzc0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0003.resp.Data();// 响应Data：客户资产池下资产清单列表查询（出池时查询）
		Xdzc0003RespDto xdzc0003RespDto = new Xdzc0003RespDto();
		try {
            // 从 xdzc0003ReqDto获取 reqData
            reqData = xdzc0003ReqDto.getData();
            // 将 reqData 拷贝到xdzc0003DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0003DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, JSON.toJSONString(xdzc0003DataReqDto));
            ResultDto<Xdzc0003DataRespDto> xdzc0003DataResultDto = dscmsBizZcClientService.xdzc0003(xdzc0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, JSON.toJSONString(xdzc0003DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzc0003RespDto.setErorcd(Optional.ofNullable(xdzc0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0003RespDto.setErortx(Optional.ofNullable(xdzc0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0003DataResultDto.getCode())) {
                xdzc0003DataRespDto = xdzc0003DataResultDto.getData();
                xdzc0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, e.getMessage());
            xdzc0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0003RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0003RespDto.setDatasq(servsq);

        xdzc0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0003.key, DscmsEnum.TRADE_CODE_XDZC0003.value, JSON.toJSONString(xdzc0003RespDto));
        return xdzc0003RespDto;
    }
}
