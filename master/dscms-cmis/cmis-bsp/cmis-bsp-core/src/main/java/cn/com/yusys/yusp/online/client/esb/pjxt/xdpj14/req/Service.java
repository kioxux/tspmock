package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj14.req;

import java.math.BigDecimal;

/**
 * 请求Service：信贷签约通知
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String txdate;//    交易日期
    private String txtime;//    交易时间
    private String datasq;//    全局流水
    private String manageBrId;//管理机构号
    private String custName;//客户名称
    private String discountNo;//协议编号
    private String phoneNo;//电话
    private BigDecimal secondDisBalance;//
    private BigDecimal totalDisBalance;//总额度
    private String custNo;//客户号
    private BigDecimal totalOnlineBalance;//总线上额度
    private String startDate;//起始日期
    private String endDate;//终止日期
    private String type;//

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getTxdate() {
        return txdate;
    }

    public void setTxdate(String txdate) {
        this.txdate = txdate;
    }

    public String getTxtime() {
        return txtime;
    }

    public void setTxtime(String txtime) {
        this.txtime = txtime;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getManageBrId() {
        return manageBrId;
    }

    public void setManageBrId(String manageBrId) {
        this.manageBrId = manageBrId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getDiscountNo() {
        return discountNo;
    }

    public void setDiscountNo(String discountNo) {
        this.discountNo = discountNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public BigDecimal getSecondDisBalance() {
        return secondDisBalance;
    }

    public void setSecondDisBalance(BigDecimal secondDisBalance) {
        this.secondDisBalance = secondDisBalance;
    }

    public BigDecimal getTotalDisBalance() {
        return totalDisBalance;
    }

    public void setTotalDisBalance(BigDecimal totalDisBalance) {
        this.totalDisBalance = totalDisBalance;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public BigDecimal getTotalOnlineBalance() {
        return totalOnlineBalance;
    }

    public void setTotalOnlineBalance(BigDecimal totalOnlineBalance) {
        this.totalOnlineBalance = totalOnlineBalance;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", txdate='" + txdate + '\'' +
                ", txtime='" + txtime + '\'' +
                ", datasq='" + datasq + '\'' +
                ", manageBrId='" + manageBrId + '\'' +
                ", custName='" + custName + '\'' +
                ", discountNo='" + discountNo + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", secondDisBalance=" + secondDisBalance +
                ", totalDisBalance=" + totalDisBalance +
                ", custNo='" + custNo + '\'' +
                ", totalOnlineBalance=" + totalOnlineBalance +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
