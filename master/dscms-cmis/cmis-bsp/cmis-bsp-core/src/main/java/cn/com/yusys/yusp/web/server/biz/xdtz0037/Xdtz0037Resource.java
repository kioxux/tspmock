package cn.com.yusys.yusp.web.server.biz.xdtz0037;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0037.req.Xdtz0037ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0037.resp.Xdtz0037RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0037.req.Xdtz0037DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0037.resp.Xdtz0037DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:无还本续贷额度状态更新
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0037:无还本续贷额度状态更新")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0037Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0037Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0037
     * 交易描述：无还本续贷额度状态更新
     *
     * @param xdtz0037ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("无还本续贷额度状态更新")
    @PostMapping("/xdtz0037")
    //@Idempotent({"xdcatz0037", "#xdtz0037ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0037RespDto xdtz0037(@Validated @RequestBody Xdtz0037ReqDto xdtz0037ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037ReqDto));
        Xdtz0037DataReqDto xdtz0037DataReqDto = new Xdtz0037DataReqDto();// 请求Data： 无还本续贷额度状态更新
        Xdtz0037DataRespDto xdtz0037DataRespDto = new Xdtz0037DataRespDto();// 响应Data：无还本续贷额度状态更新
        Xdtz0037RespDto xdtz0037RespDto = new Xdtz0037RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0037.req.Data reqData = null; // 请求Data：无还本续贷额度状态更新
        cn.com.yusys.yusp.dto.server.biz.xdtz0037.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0037.resp.Data();// 响应Data：无还本续贷额度状态更新
        try {
            // 从 xdtz0037ReqDto获取 reqData
            reqData = xdtz0037ReqDto.getData();
            // 将 reqData 拷贝到xdtz0037DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0037DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037DataReqDto));
            ResultDto<Xdtz0037DataRespDto> xdtz0037DataResultDto = dscmsBizTzClientService.xdtz0037(xdtz0037DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0037RespDto.setErorcd(Optional.ofNullable(xdtz0037DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0037RespDto.setErortx(Optional.ofNullable(xdtz0037DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0037DataResultDto.getCode())) {
                xdtz0037RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0037RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdtz0037DataRespDto = xdtz0037DataResultDto.getData();
                // 将 xdtz0037DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0037DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, e.getMessage());
            xdtz0037RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0037RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0037RespDto.setDatasq(servsq);

        xdtz0037RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0037.key, DscmsEnum.TRADE_CODE_XDTZ0037.value, JSON.toJSONString(xdtz0037RespDto));
        return xdtz0037RespDto;
    }
}
