package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp;

/**
 * 响应Service：新信贷获取调查信息接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private String code;//返回码
    private String message;//返回信息

    private String totalCurrentAmount;//资产总额 - 本期
    private String currFloatAmount;//流动资产小计 - 本期
    private String bankcashCurAmount;//现金/银行存款/理财 -本期
    private String currentOfReceivables;//应收账款 - 本期
    private String currentOfInventory;//存货 - 本期
    private String prepayRentCurAmount;//待摊租金 - 本期
    private String fixassCurrent;//固定资产小计 - 本期
    private String indebtedCurrentAmount;//负债总额 - 本期
    private String bankCurrentAmount;//银行借款 - 本期
    private String accountsCurrentAmount;//应付账款 - 本期
    private String otherloanCurrentAmount;//其他负债 - 本期
    private String ownersCurrAmount;//所有者权益 - 本期

    private String totalsalFullYear;//销售收入-总额 - 本期
    private String variableMatYear;//可变成本-总额 - 本期
    private String grossYearRecently;//毛利润 - 本期
    private String fixedManagecostFullYear;//固定支出-总额 - 本期
    private String recentlyNetProfit;//净利润 - 本期

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTotalCurrentAmount() {
        return totalCurrentAmount;
    }

    public void setTotalCurrentAmount(String totalCurrentAmount) {
        this.totalCurrentAmount = totalCurrentAmount;
    }

    public String getCurrFloatAmount() {
        return currFloatAmount;
    }

    public void setCurrFloatAmount(String currFloatAmount) {
        this.currFloatAmount = currFloatAmount;
    }

    public String getBankcashCurAmount() {
        return bankcashCurAmount;
    }

    public void setBankcashCurAmount(String bankcashCurAmount) {
        this.bankcashCurAmount = bankcashCurAmount;
    }

    public String getCurrentOfReceivables() {
        return currentOfReceivables;
    }

    public void setCurrentOfReceivables(String currentOfReceivables) {
        this.currentOfReceivables = currentOfReceivables;
    }

    public String getCurrentOfInventory() {
        return currentOfInventory;
    }

    public void setCurrentOfInventory(String currentOfInventory) {
        this.currentOfInventory = currentOfInventory;
    }

    public String getPrepayRentCurAmount() {
        return prepayRentCurAmount;
    }

    public void setPrepayRentCurAmount(String prepayRentCurAmount) {
        this.prepayRentCurAmount = prepayRentCurAmount;
    }

    public String getFixassCurrent() {
        return fixassCurrent;
    }

    public void setFixassCurrent(String fixassCurrent) {
        this.fixassCurrent = fixassCurrent;
    }

    public String getIndebtedCurrentAmount() {
        return indebtedCurrentAmount;
    }

    public void setIndebtedCurrentAmount(String indebtedCurrentAmount) {
        this.indebtedCurrentAmount = indebtedCurrentAmount;
    }

    public String getBankCurrentAmount() {
        return bankCurrentAmount;
    }

    public void setBankCurrentAmount(String bankCurrentAmount) {
        this.bankCurrentAmount = bankCurrentAmount;
    }

    public String getAccountsCurrentAmount() {
        return accountsCurrentAmount;
    }

    public void setAccountsCurrentAmount(String accountsCurrentAmount) {
        this.accountsCurrentAmount = accountsCurrentAmount;
    }

    public String getOtherloanCurrentAmount() {
        return otherloanCurrentAmount;
    }

    public void setOtherloanCurrentAmount(String otherloanCurrentAmount) {
        this.otherloanCurrentAmount = otherloanCurrentAmount;
    }

    public String getOwnersCurrAmount() {
        return ownersCurrAmount;
    }

    public void setOwnersCurrAmount(String ownersCurrAmount) {
        this.ownersCurrAmount = ownersCurrAmount;
    }

    public String getTotalsalFullYear() {
        return totalsalFullYear;
    }

    public void setTotalsalFullYear(String totalsalFullYear) {
        this.totalsalFullYear = totalsalFullYear;
    }

    public String getVariableMatYear() {
        return variableMatYear;
    }

    public void setVariableMatYear(String variableMatYear) {
        this.variableMatYear = variableMatYear;
    }

    public String getGrossYearRecently() {
        return grossYearRecently;
    }

    public void setGrossYearRecently(String grossYearRecently) {
        this.grossYearRecently = grossYearRecently;
    }

    public String getFixedManagecostFullYear() {
        return fixedManagecostFullYear;
    }

    public void setFixedManagecostFullYear(String fixedManagecostFullYear) {
        this.fixedManagecostFullYear = fixedManagecostFullYear;
    }

    public String getRecentlyNetProfit() {
        return recentlyNetProfit;
    }

    public void setRecentlyNetProfit(String recentlyNetProfit) {
        this.recentlyNetProfit = recentlyNetProfit;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", totalCurrentAmount='" + totalCurrentAmount + '\'' +
                ", currFloatAmount='" + currFloatAmount + '\'' +
                ", bankcashCurAmount='" + bankcashCurAmount + '\'' +
                ", currentOfReceivables='" + currentOfReceivables + '\'' +
                ", currentOfInventory='" + currentOfInventory + '\'' +
                ", prepayRentCurAmount='" + prepayRentCurAmount + '\'' +
                ", fixassCurrent='" + fixassCurrent + '\'' +
                ", indebtedCurrentAmount='" + indebtedCurrentAmount + '\'' +
                ", bankCurrentAmount='" + bankCurrentAmount + '\'' +
                ", accountsCurrentAmount='" + accountsCurrentAmount + '\'' +
                ", otherloanCurrentAmount='" + otherloanCurrentAmount + '\'' +
                ", ownersCurrAmount='" + ownersCurrAmount + '\'' +
                ", totalsalFullYear='" + totalsalFullYear + '\'' +
                ", variableMatYear='" + variableMatYear + '\'' +
                ", grossYearRecently='" + grossYearRecently + '\'' +
                ", fixedManagecostFullYear='" + fixedManagecostFullYear + '\'' +
                ", recentlyNetProfit='" + recentlyNetProfit + '\'' +
                '}';
    }
}
