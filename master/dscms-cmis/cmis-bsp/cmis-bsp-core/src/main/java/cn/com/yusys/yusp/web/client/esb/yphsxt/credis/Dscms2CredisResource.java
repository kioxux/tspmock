package cn.com.yusys.yusp.web.client.esb.yphsxt.credis;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.req.CredisReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.req.List;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.resp.CredisRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req.CredisReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req.Record;
import cn.com.yusys.yusp.online.client.esb.yphsxt.credis.resp.CredisRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:信用证信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2CredisResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2CredisResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：credis
     * 交易描述：信用证信息同步
     *
     * @param credisReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/credis")
    protected @ResponseBody
    ResultDto<CredisRespDto> credis(@Validated @RequestBody CredisReqDto credisReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDIS.key, EsbEnum.TRADE_CODE_CREDIS.value, JSON.toJSONString(credisReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.credis.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.credis.resp.Service();
        CredisReqService credisReqService = new CredisReqService();
        CredisRespService credisRespService = new CredisRespService();
        CredisRespDto credisRespDto = new CredisRespDto();
        ResultDto<CredisRespDto> credisResultDto = new ResultDto<CredisRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将credisReqDto转换成reqService
            BeanUtils.copyProperties(credisReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req.List reqList = new cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req.List();
            if (CollectionUtils.nonEmpty(credisReqDto.getList())) {
                java.util.List<List> list = Optional.ofNullable(credisReqDto.getList()).orElse(new ArrayList<>());
                java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.credis.req.Record> recordList = list.parallelStream().map(e -> {
                    Record record = new Record();
                    BeanUtils.copyProperties(e, record);
                    return record;
                }).collect(Collectors.toList());
                reqList.setRecord(recordList);
                reqService.setList(reqList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CREDIS.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            credisReqService.setService(reqService);
            // 将credisReqService转换成credisReqServiceMap
            Map credisReqServiceMap = beanMapUtil.beanToMap(credisReqService);
            context.put("tradeDataMap", credisReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDIS.key, EsbEnum.TRADE_CODE_CREDIS.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CREDIS.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDIS.key, EsbEnum.TRADE_CODE_CREDIS.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            credisRespService = beanMapUtil.mapToBean(tradeDataMap, CredisRespService.class, CredisRespService.class);
            respService = credisRespService.getService();
            //  将respService转换成CredisRespDto
            credisResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            credisResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, credisRespDto);
                cn.com.yusys.yusp.online.client.esb.yphsxt.credis.resp.List respList = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.yphsxt.credis.resp.List());
                if (CollectionUtils.nonEmpty(respList.getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.credis.resp.Record> originList = Optional.ofNullable(respList.getRecord()).orElse(new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.resp.List> targetList = originList.parallelStream().map(e -> {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.resp.List target = new cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.resp.List();
                        BeanUtils.copyProperties(e, target);
                        return target;
                    }).collect(Collectors.toList());
                    credisRespDto.setList(targetList);
                }
                credisResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                credisResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                credisResultDto.setCode(EpbEnum.EPB099999.key);
                credisResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDIS.key, EsbEnum.TRADE_CODE_CREDIS.value, e.getMessage());
            credisResultDto.setCode(EpbEnum.EPB099999.key);//9999
            credisResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        credisResultDto.setData(credisRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDIS.key, EsbEnum.TRADE_CODE_CREDIS.value, JSON.toJSONString(credisResultDto));
        return credisResultDto;
    }
}
