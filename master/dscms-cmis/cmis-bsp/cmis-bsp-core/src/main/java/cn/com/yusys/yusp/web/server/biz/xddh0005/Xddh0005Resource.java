package cn.com.yusys.yusp.web.server.biz.xddh0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0005.req.Xddh0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0005.resp.Xddh0005RespDto;
import cn.com.yusys.yusp.dto.server.xddh0005.req.Xddh0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0005.resp.Xddh0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提前还款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0005:提前还款")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0005Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0005
     * 交易描述：提前还款
     *
     * @param xddh0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前还款")
    @PostMapping("/xddh0005")
//@Idempotent({"xddh0005", "#xddh0005ReqDto.datasq"})
    protected @ResponseBody
    Xddh0005RespDto xddh0005(@Validated @RequestBody Xddh0005ReqDto xddh0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, JSON.toJSONString(xddh0005ReqDto));
        Xddh0005DataReqDto xddh0005DataReqDto = new Xddh0005DataReqDto();// 请求Data： 提前还款
        Xddh0005DataRespDto xddh0005DataRespDto = new Xddh0005DataRespDto();// 响应Data：提前还款
        Xddh0005RespDto xddh0005RespDto = new Xddh0005RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddh0005.req.Data reqData = null; // 请求Data：提前还款
        cn.com.yusys.yusp.dto.server.biz.xddh0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0005.resp.Data();// 响应Data：提前还款
        try {
            // 从 xddh0005ReqDto获取 reqData
            reqData = xddh0005ReqDto.getData();
            // 将 reqData 拷贝到xddh0005DataReqDto
            BeanUtils.copyProperties(reqData, xddh0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, JSON.toJSONString(xddh0005DataReqDto));
            ResultDto<Xddh0005DataRespDto> xddh0005DataResultDto = dscmsBizDhClientService.xddh0005(xddh0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, JSON.toJSONString(xddh0005DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0005RespDto.setErorcd(Optional.ofNullable(xddh0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0005RespDto.setErortx(Optional.ofNullable(xddh0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0005DataResultDto.getCode())) {
                xddh0005DataRespDto = xddh0005DataResultDto.getData();
                xddh0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, e.getMessage());
            xddh0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0005RespDto.setDatasq(servsq);

        xddh0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0005.key, DscmsEnum.TRADE_CODE_XDDH0005.value, JSON.toJSONString(xddh0005RespDto));
        return xddh0005RespDto;
    }
}
