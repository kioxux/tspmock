package cn.com.yusys.yusp.web.client.esb.circp.fb1174;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.req.Fb1174ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.resp.Fb1174RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1174.req.Fb1174ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1174.resp.Fb1174RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1174）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1174Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1174Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1174
     * 交易描述：房抵e点贷尽调结果通知
     *
     * @param fb1174ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1174")
    protected @ResponseBody
    ResultDto<Fb1174RespDto> fb1174(@Validated @RequestBody Fb1174ReqDto fb1174ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1174.key, EsbEnum.TRADE_CODE_FB1174.value, JSON.toJSONString(fb1174ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1174.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1174.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1174.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1174.resp.Service();
        Fb1174ReqService fb1174ReqService = new Fb1174ReqService();
        Fb1174RespService fb1174RespService = new Fb1174RespService();
        Fb1174RespDto fb1174RespDto = new Fb1174RespDto();
        ResultDto<Fb1174RespDto> fb1174ResultDto = new ResultDto<Fb1174RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1174ReqDto转换成reqService
            BeanUtils.copyProperties(fb1174ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1174.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1174ReqService.setService(reqService);
            // 将fb1174ReqService转换成fb1174ReqServiceMap
            Map fb1174ReqServiceMap = beanMapUtil.beanToMap(fb1174ReqService);
            context.put("tradeDataMap", fb1174ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1174.key, EsbEnum.TRADE_CODE_FB1174.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1174.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1174.key, EsbEnum.TRADE_CODE_FB1174.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1174RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1174RespService.class, Fb1174RespService.class);
            respService = fb1174RespService.getService();

            fb1174ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1174ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1174RespDto
                BeanUtils.copyProperties(respService, fb1174RespDto);

                fb1174ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1174ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1174ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1174ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1174.key, EsbEnum.TRADE_CODE_FB1174.value, e.getMessage());
            fb1174ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1174ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1174ResultDto.setData(fb1174RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1174.key, EsbEnum.TRADE_CODE_FB1174.value, JSON.toJSONString(fb1174ResultDto));
        return fb1174ResultDto;
    }
}
