package cn.com.yusys.yusp.online.client.esb.core.ln3083.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 15:04
 * @since 2021/5/28 15:04
 */
public class Record {
    private String dkjiejuh;//贷款借据号
    private String nbjiejuh;//内部借据号
    private String bhchzibz;//本行出资标志
    private String lhdkleix;//联合贷款类型
    private String chzfkehh;//出资方客户号
    private String chzfkhmc;//出资方客户名称
    private String chzfhhao;//出资方行号
    private String czfhming;//出资方行名
    private String chuzfzhh;//出资方账号
    private String czfzhzxh;//出资方账号子序号
    private BigDecimal chuzbili;//出资比例
    private BigDecimal chuzjine;//出资金额
    private String shoukzhh;//收款账号
    private String skzhhzxh;//收款账号子序号
    private String skzhhmch;//收款账户名称
    private String ruzjigou;//入账机构
    private String bjghrzzh;//本金归还入账账号
    private String bjghrzxh;//本金归还入账账号子序号
    private String lxghrzzh;//利息归还入账账号
    private String lxghrzxh;//利息归还入账账号子序号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getNbjiejuh() {
        return nbjiejuh;
    }

    public void setNbjiejuh(String nbjiejuh) {
        this.nbjiejuh = nbjiejuh;
    }

    public String getBhchzibz() {
        return bhchzibz;
    }

    public void setBhchzibz(String bhchzibz) {
        this.bhchzibz = bhchzibz;
    }

    public String getLhdkleix() {
        return lhdkleix;
    }

    public void setLhdkleix(String lhdkleix) {
        this.lhdkleix = lhdkleix;
    }

    public String getChzfkehh() {
        return chzfkehh;
    }

    public void setChzfkehh(String chzfkehh) {
        this.chzfkehh = chzfkehh;
    }

    public String getChzfkhmc() {
        return chzfkhmc;
    }

    public void setChzfkhmc(String chzfkhmc) {
        this.chzfkhmc = chzfkhmc;
    }

    public String getChzfhhao() {
        return chzfhhao;
    }

    public void setChzfhhao(String chzfhhao) {
        this.chzfhhao = chzfhhao;
    }

    public String getCzfhming() {
        return czfhming;
    }

    public void setCzfhming(String czfhming) {
        this.czfhming = czfhming;
    }

    public String getChuzfzhh() {
        return chuzfzhh;
    }

    public void setChuzfzhh(String chuzfzhh) {
        this.chuzfzhh = chuzfzhh;
    }

    public String getCzfzhzxh() {
        return czfzhzxh;
    }

    public void setCzfzhzxh(String czfzhzxh) {
        this.czfzhzxh = czfzhzxh;
    }

    public BigDecimal getChuzbili() {
        return chuzbili;
    }

    public void setChuzbili(BigDecimal chuzbili) {
        this.chuzbili = chuzbili;
    }

    public BigDecimal getChuzjine() {
        return chuzjine;
    }

    public void setChuzjine(BigDecimal chuzjine) {
        this.chuzjine = chuzjine;
    }

    public String getShoukzhh() {
        return shoukzhh;
    }

    public void setShoukzhh(String shoukzhh) {
        this.shoukzhh = shoukzhh;
    }

    public String getSkzhhzxh() {
        return skzhhzxh;
    }

    public void setSkzhhzxh(String skzhhzxh) {
        this.skzhhzxh = skzhhzxh;
    }

    public String getSkzhhmch() {
        return skzhhmch;
    }

    public void setSkzhhmch(String skzhhmch) {
        this.skzhhmch = skzhhmch;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", nbjiejuh='" + nbjiejuh + '\'' +
                ", bhchzibz='" + bhchzibz + '\'' +
                ", lhdkleix='" + lhdkleix + '\'' +
                ", chzfkehh='" + chzfkehh + '\'' +
                ", chzfkhmc='" + chzfkhmc + '\'' +
                ", chzfhhao='" + chzfhhao + '\'' +
                ", czfhming='" + czfhming + '\'' +
                ", chuzfzhh='" + chuzfzhh + '\'' +
                ", czfzhzxh='" + czfzhzxh + '\'' +
                ", chuzbili=" + chuzbili +
                ", chuzjine=" + chuzjine +
                ", shoukzhh='" + shoukzhh + '\'' +
                ", skzhhzxh='" + skzhhzxh + '\'' +
                ", skzhhmch='" + skzhhmch + '\'' +
                ", ruzjigou='" + ruzjigou + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", bjghrzxh='" + bjghrzxh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", lxghrzxh='" + lxghrzxh + '\'' +
                '}';
    }
}
