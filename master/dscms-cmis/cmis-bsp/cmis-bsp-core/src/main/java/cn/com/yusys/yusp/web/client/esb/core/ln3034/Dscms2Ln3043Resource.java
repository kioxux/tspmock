package cn.com.yusys.yusp.web.client.esb.core.ln3034;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3043.req.Ln3043ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3043.req.Tblqgmx;
import cn.com.yusys.yusp.dto.client.esb.core.ln3043.resp.Ln3043RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Ln3043ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3043.resp.Ln3043RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3043)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3043Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3043Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3043
     * 交易描述：贷款指定期供归还
     *
     * @param ln3043ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3043:贷款指定期供归还")
    @PostMapping("/ln3043")
    protected @ResponseBody
    ResultDto<Ln3043RespDto> ln3043(@Validated @RequestBody Ln3043ReqDto ln3043ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3043.key, EsbEnum.TRADE_CODE_LN3043.value, JSON.toJSONString(ln3043ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3043.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3043.resp.Service();
        Ln3043ReqService ln3043ReqService = new Ln3043ReqService();
        Ln3043RespService ln3043RespService = new Ln3043RespService();
        Ln3043RespDto ln3043RespDto = new Ln3043RespDto();
        ResultDto<Ln3043RespDto> ln3043ResultDto = new ResultDto<Ln3043RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3043ReqDto转换成reqService
            BeanUtils.copyProperties(ln3043ReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Tblqgmx tblqgmx = new cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Tblqgmx();
            if (CollectionUtils.nonEmpty(ln3043ReqDto.getTblqgmx())) {
                List<Tblqgmx> tblqgmxList = ln3043ReqDto.getTblqgmx();
                List<cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Record> recordList = new ArrayList<>();
                for (Tblqgmx tblqgmxdto : tblqgmxList) {
                    cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Record();
                    BeanUtils.copyProperties(tblqgmxdto, record);
                    recordList.add(record);
                }
                tblqgmx.setRecordList(recordList);
                reqService.setTblqgmx(tblqgmx);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3043.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3043ReqService.setService(reqService);
            // 将ln3043ReqService转换成ln3043ReqServiceMap
            Map ln3043ReqServiceMap = beanMapUtil.beanToMap(ln3043ReqService);
            context.put("tradeDataMap", ln3043ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3043.key, EsbEnum.TRADE_CODE_LN3043.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3043.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3043.key, EsbEnum.TRADE_CODE_LN3043.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3043RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3043RespService.class, Ln3043RespService.class);
            respService = ln3043RespService.getService();

            ln3043ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3043ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3043RespDto
                BeanUtils.copyProperties(respService, ln3043RespDto);
                ln3043ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3043ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3043ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3043ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3043.key, EsbEnum.TRADE_CODE_LN3043.value, e.getMessage());
            ln3043ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3043ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3043ResultDto.setData(ln3043RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3043.key, EsbEnum.TRADE_CODE_LN3043.value, JSON.toJSONString(ln3043ResultDto));
        return ln3043ResultDto;
    }
}
