package cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/16 20:44
 * @since 2021/6/16 20:44
 */
public class Body {

    private String cardno;//卡号
    private String crcycd;//币种

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    @Override
    public String toString() {
        return "Service{" +
                "cardno='" + cardno + '\'' +
                "crcycd='" + crcycd + '\'' +
                '}';
    }
}
