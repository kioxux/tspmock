package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Record {
    private String batchNo;// 批次号

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    @Override
    public String toString() {
        return "Record{" +
                "batchNo='" + batchNo + '\'' +
                '}';
    }
}
