package cn.com.yusys.yusp.online.client.esb.core.da3302.req;

/**
 * 请求Service：抵债资产处置
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3302ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3302ReqService{" +
                "service=" + service +
                '}';
    }
}

