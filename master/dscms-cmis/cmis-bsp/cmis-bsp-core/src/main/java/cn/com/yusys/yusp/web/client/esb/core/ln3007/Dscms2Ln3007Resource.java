package cn.com.yusys.yusp.web.client.esb.core.ln3007;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3007.req.Ln3007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3007.resp.Ln3007RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3007.req.Ln3007ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3007.resp.Listnm;
import cn.com.yusys.yusp.online.client.esb.core.ln3007.resp.Ln3007RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3007.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3007)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3007Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3007Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3007
     * 交易描述：资产产品币种查询
     *
     * @param ln3007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3007:资产产品币种查询")
    @PostMapping("/ln3007")
    protected @ResponseBody
    ResultDto<Ln3007RespDto> ln3007(@Validated @RequestBody Ln3007ReqDto ln3007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3007.key, EsbEnum.TRADE_CODE_LN3007.value, JSON.toJSONString(ln3007ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3007.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3007.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3007.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3007.resp.Service();
        Ln3007ReqService ln3007ReqService = new Ln3007ReqService();
        Ln3007RespService ln3007RespService = new Ln3007RespService();
        Ln3007RespDto ln3007RespDto = new Ln3007RespDto();
        ResultDto<Ln3007RespDto> ln3007ResultDto = new ResultDto<Ln3007RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3007ReqDto转换成reqService
            BeanUtils.copyProperties(ln3007ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3007.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3007ReqService.setService(reqService);
            // 将ln3007ReqService转换成ln3007ReqServiceMap
            Map ln3007ReqServiceMap = beanMapUtil.beanToMap(ln3007ReqService);
            context.put("tradeDataMap", ln3007ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3007.key, EsbEnum.TRADE_CODE_LN3007.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3007.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3007.key, EsbEnum.TRADE_CODE_LN3007.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3007RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3007RespService.class, Ln3007RespService.class);
            respService = ln3007RespService.getService();

            ln3007ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3007ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3007RespDto
                BeanUtils.copyProperties(respService, ln3007RespDto);
                List<cn.com.yusys.yusp.dto.client.esb.core.ln3007.resp.Listnm> listnmList = new ArrayList<>();
                Listnm serviceList = Optional.ofNullable(respService.getListnm()).orElse(new Listnm());
                if (CollectionUtils.nonEmpty(serviceList.getRecordList())) {
                    List<Record> recordList = respService.getListnm().getRecordList();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3007.resp.Listnm listnm = new cn.com.yusys.yusp.dto.client.esb.core.ln3007.resp.Listnm();
                        BeanUtils.copyProperties(record, listnm);
                        listnmList.add(listnm);
                    }
                }
                ln3007RespDto.setListnm(listnmList);
                ln3007ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3007ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3007ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3007ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3007.key, EsbEnum.TRADE_CODE_LN3007.value, e.getMessage());
            ln3007ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3007ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3007ResultDto.setData(ln3007RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3007.key, EsbEnum.TRADE_CODE_LN3007.value, JSON.toJSONString(ln3007ResultDto));
        return ln3007ResultDto;
    }
}
