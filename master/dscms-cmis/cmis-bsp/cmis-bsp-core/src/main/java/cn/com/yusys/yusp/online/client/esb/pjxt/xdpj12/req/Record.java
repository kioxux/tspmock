package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 20:41
 * @since 2021/5/28 20:41
 */
public class Record {
    private String currency;//币种
    private String acctname;//账户名称
    private String acctseq;//账户序号
    private String acctno;//账号
    private String custname;//客户名称
    private String custno;//客户号

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAcctname() {
        return acctname;
    }

    public void setAcctname(String acctname) {
        this.acctname = acctname;
    }

    public String getAcctseq() {
        return acctseq;
    }

    public void setAcctseq(String acctseq) {
        this.acctseq = acctseq;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    @Override
    public String toString() {
        return "Service{" +
                "currency='" + currency + '\'' +
                "acctname='" + acctname + '\'' +
                "acctseq='" + acctseq + '\'' +
                "acctno='" + acctno + '\'' +
                "custname='" + custname + '\'' +
                "custno='" + custno + '\'' +
                '}';
    }
}
