package cn.com.yusys.yusp.online.client.esb.core.da3300.resp;

/**
 * 响应Service：交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理
 *
 * @author leehuang
 * @version 1.0
 */
public class Da3300RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3300RespService{" +
                "service=" + service +
                '}';
    }
}
