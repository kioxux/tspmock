package cn.com.yusys.yusp.online.client.esb.ecif.g00101.resp;

/**
 * 响应Service：对公客户综合信息查询
 *
 * @author zhangpeng
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class G00101RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
