package cn.com.yusys.yusp.web.client.esb.core.ln3079;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3079.Ln3079ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3079.Ln3079RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3079.req.Ln3079ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3079.resp.Ln3079RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3079)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3079Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3079Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3079
     * 交易描述：贷款产品变更
     *
     * @param ln3079ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3079:贷款产品变更")
    @PostMapping("/ln3079")
    protected @ResponseBody
    ResultDto<Ln3079RespDto> ln3079(@Validated @RequestBody Ln3079ReqDto ln3079ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3079.key, EsbEnum.TRADE_CODE_LN3079.value, JSON.toJSONString(ln3079ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3079.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3079.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3079.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3079.resp.Service();

        Ln3079ReqService ln3079ReqService = new Ln3079ReqService();
        Ln3079RespService ln3079RespService = new Ln3079RespService();
        Ln3079RespDto ln3079RespDto = new Ln3079RespDto();
        ResultDto<Ln3079RespDto> ln3079ResultDto = new ResultDto<Ln3079RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3079ReqDto转换成reqService
            BeanUtils.copyProperties(ln3079ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3079.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3079ReqService.setService(reqService);
            // 将ln3079ReqService转换成ln3079ReqServiceMap
            Map ln3079ReqServiceMap = beanMapUtil.beanToMap(ln3079ReqService);
            context.put("tradeDataMap", ln3079ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3079.key, EsbEnum.TRADE_CODE_LN3079.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3079.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3079.key, EsbEnum.TRADE_CODE_LN3079.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3079RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3079RespService.class, Ln3079RespService.class);
            respService = ln3079RespService.getService();

            ln3079ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3079ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3079RespDto
                BeanUtils.copyProperties(respService, ln3079RespDto);
                ln3079ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3079ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3079ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3079ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3079.key, EsbEnum.TRADE_CODE_LN3079.value, e.getMessage());
            ln3079ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3079ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3079ResultDto.setData(ln3079RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3079.key, EsbEnum.TRADE_CODE_LN3079.value, JSON.toJSONString(ln3079ResultDto));
        return ln3079ResultDto;
    }
}
