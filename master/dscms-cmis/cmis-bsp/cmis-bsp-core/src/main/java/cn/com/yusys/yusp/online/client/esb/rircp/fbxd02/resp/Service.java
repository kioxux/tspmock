package cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.resp;

/**
 * 响应Service：为正式客户更新信贷客户信息手机号码
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码
    private String erortx; // 响应信息

    private String ols_tran_no; // 交易流水号
    private String ols_date; // 交易日期
    private String ifmobl;//手机号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getOls_tran_no() {
        return ols_tran_no;
    }

    public void setOls_tran_no(String ols_tran_no) {
        this.ols_tran_no = ols_tran_no;
    }

    public String getOls_date() {
        return ols_date;
    }

    public void setOls_date(String ols_date) {
        this.ols_date = ols_date;
    }

    public String getIfmobl() {
        return ifmobl;
    }

    public void setIfmobl(String ifmobl) {
        this.ifmobl = ifmobl;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", ols_tran_no='" + ols_tran_no + '\'' +
                ", ols_date='" + ols_date + '\'' +
                ", ifmobl='" + ifmobl + '\'' +
                '}';
    }
}
