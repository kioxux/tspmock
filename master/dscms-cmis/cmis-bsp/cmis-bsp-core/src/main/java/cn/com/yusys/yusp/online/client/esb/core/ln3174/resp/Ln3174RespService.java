package cn.com.yusys.yusp.online.client.esb.core.ln3174.resp;

/**
 * 响应Service：资产证券化欠款借据查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3174RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3174RespService{" +
				"service=" + service +
				'}';
	}
}
