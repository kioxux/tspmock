package cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.req;

/**
 * 请求Service：ESB信贷查询接口（处理码credi1）
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 19:48
 */
public class Service {
    private String prcscd;//	处理码	;	是	;	区分接口的唯一标识，同时也作为路由使用。信贷查询固定传 credi1
    private String servtp;//	渠道码	;	是	;	交易渠道码，ESB总线提供。信贷系统XDG
    private String servsq;//	渠道流水	;	是	;	渠道码+日期+11位不重复序列
    private String userid;//	柜员号	;	是	;	各系统发起该交易登陆用户
    private String brchno;//	部门号	;	是	;	【字典】发起交易用户所在部门
    private String creditType;//	报告类型	;	是	;	区分查询个人报告还是企业报告 0 个人 1 企业
    private String customName;//	报告人	;	是	;	客户名称
    private String certificateType;//	证件类型	;	是	;	【字典】证件类型
    private String certificateNum;//	证件号	;	是	;	证件号
    private String queryReason;//	查询原因	;	是	;	【字典】查询原因
    private String createUserCode;//	客户经理身份证号	;	是	;	客户经理身份证号
    private String synchronizationCode;//	同步异步标识	;	否	;	0同步1异步
    private String createUser;//	客户经理名称	;	是	;	客户经理名称
    private String createUsetId;//	客户经理工号	;	是	;	客户经理工号
    private String createDate;//	创建申请时间	;	否	;	时间格式：yyyy-MM-dd
    private String borrowPersonRelation;//	与主借款人关系	;	是	;	【字典】与主借款人关系
    private String areaName;//	片区名称  	;	否	;	片区名称
    private String areaId;//	片区编号	;	否	;	片区编号
    private String queryStartDate;//	授权书签订日期	;	是	;	授权书签订日期，时间格式：yyyy-MM-dd
    private String queryEndDate;//	授权结束日期	;	否	;	授权结束日期，时间格式：yyyy-MM-dd
    private String creditDocId;//	授权影像编号	;	是	;	授权影像编号
    private String createUserPhone;//	客户经理手机号	;	是	;	客户经理手机号
    private String borrowPersonRelationName;//	主借款人名称	;	否	;	当被查询客户是主借款人时可不传
    private String borrowPersonRelationNumber;//	主借款人证件号	;	否	;	当被查询客户是主借款人时可不传
    private String auditReason;//	授权书内容	;	是	;	【字典】授权书内容，格式: 001;004

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getCertificateNum() {
        return certificateNum;
    }

    public void setCertificateNum(String certificateNum) {
        this.certificateNum = certificateNum;
    }

    public String getQueryReason() {
        return queryReason;
    }

    public void setQueryReason(String queryReason) {
        this.queryReason = queryReason;
    }

    public String getCreateUserCode() {
        return createUserCode;
    }

    public void setCreateUserCode(String createUserCode) {
        this.createUserCode = createUserCode;
    }

    public String getSynchronizationCode() {
        return synchronizationCode;
    }

    public void setSynchronizationCode(String synchronizationCode) {
        this.synchronizationCode = synchronizationCode;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getCreateUsetId() {
        return createUsetId;
    }

    public void setCreateUsetId(String createUsetId) {
        this.createUsetId = createUsetId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getBorrowPersonRelation() {
        return borrowPersonRelation;
    }

    public void setBorrowPersonRelation(String borrowPersonRelation) {
        this.borrowPersonRelation = borrowPersonRelation;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getQueryStartDate() {
        return queryStartDate;
    }

    public void setQueryStartDate(String queryStartDate) {
        this.queryStartDate = queryStartDate;
    }

    public String getQueryEndDate() {
        return queryEndDate;
    }

    public void setQueryEndDate(String queryEndDate) {
        this.queryEndDate = queryEndDate;
    }

    public String getCreditDocId() {
        return creditDocId;
    }

    public void setCreditDocId(String creditDocId) {
        this.creditDocId = creditDocId;
    }

    public String getCreateUserPhone() {
        return createUserPhone;
    }

    public void setCreateUserPhone(String createUserPhone) {
        this.createUserPhone = createUserPhone;
    }

    public String getBorrowPersonRelationName() {
        return borrowPersonRelationName;
    }

    public void setBorrowPersonRelationName(String borrowPersonRelationName) {
        this.borrowPersonRelationName = borrowPersonRelationName;
    }

    public String getBorrowPersonRelationNumber() {
        return borrowPersonRelationNumber;
    }

    public void setBorrowPersonRelationNumber(String borrowPersonRelationNumber) {
        this.borrowPersonRelationNumber = borrowPersonRelationNumber;
    }

    public String getAuditReason() {
        return auditReason;
    }

    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason;
    }

    @Override
    public String toString() {
        return "Credi1ReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", creditType='" + creditType + '\'' +
                ", customName='" + customName + '\'' +
                ", certificateType='" + certificateType + '\'' +
                ", certificateNum='" + certificateNum + '\'' +
                ", queryReason='" + queryReason + '\'' +
                ", createUserCode='" + createUserCode + '\'' +
                ", synchronizationCode='" + synchronizationCode + '\'' +
                ", createUser='" + createUser + '\'' +
                ", createUsetId='" + createUsetId + '\'' +
                ", createDate='" + createDate + '\'' +
                ", borrowPersonRelation='" + borrowPersonRelation + '\'' +
                ", areaName='" + areaName + '\'' +
                ", areaId='" + areaId + '\'' +
                ", queryStartDate='" + queryStartDate + '\'' +
                ", queryEndDate='" + queryEndDate + '\'' +
                ", creditDocId='" + creditDocId + '\'' +
                ", createUserPhone='" + createUserPhone + '\'' +
                ", borrowPersonRelationName='" + borrowPersonRelationName + '\'' +
                ", borrowPersonRelationNumber='" + borrowPersonRelationNumber + '\'' +
                ", auditReason='" + auditReason + '\'' +
                '}';
    }
}
