package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 返回参数
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CODE")
    private String CODE;//		返回码
    @JsonProperty(value = "MSG")
    private String MSG;//			返回信息
    @JsonProperty(value = "ENT_INFO")
    private cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENT_INFO ENT_INFO;

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getMSG() {
        return MSG;
    }

    public void setMSG(String MSG) {
        this.MSG = MSG;
    }

    public cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENT_INFO getENT_INFO() {
        return ENT_INFO;
    }

    public void setENT_INFO(cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENT_INFO ENT_INFO) {
        this.ENT_INFO = ENT_INFO;
    }

    @Override
    public String toString() {
        return "Xdkh0014DataRespDto{" +
                "CODE='" + CODE + '\'' +
                ", MSG='" + MSG + '\'' +
                ", ENT_INFO=" + ENT_INFO +
                '}';
    }
}
