package cn.com.yusys.yusp.online.client.esb.ecif.g00101.req;

public class List {
    private static final long serialVersionUID = 1L;
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
