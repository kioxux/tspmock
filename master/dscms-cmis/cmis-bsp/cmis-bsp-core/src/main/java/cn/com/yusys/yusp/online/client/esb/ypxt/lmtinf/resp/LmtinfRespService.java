package cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.resp;

/**
 * 响应Service：信贷授信协议信息同步
 *
 * @author leehuang
 * @version 1.0
 */
public class LmtinfRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "LmtinfRespService{" +
                "service=" + service +
                '}';
    }
}
