package cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp;

/**
 * 响应Service：客户群组信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String bginnm;//起始笔数
    private String qurynm;//查询笔数
    private String totlnm;//总笔数
    private String grouno;//群组编号
    private String grouna;//群组名称
    private String xdjtbh;//信贷集团编号
    private String xdjtmc;//信贷集团名称
    private String tyjtbh;//同业集团编号
    private String tyjtmc;//同业集团名称
    private String grouds;//群组描述
    private String acctbr;//管户机构
    private String acmang;//管户客户经理
    private String mxmang;//主办客户经理
    private String inptdt;//主办日期
    private String inptbr;//主办机构
    private String jinmlv;//紧密程度
    private String remark;//备注
    private Integer listnm;//循环列表记录数
    private cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.List list;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getBginnm() {
        return bginnm;
    }

    public void setBginnm(String bginnm) {
        this.bginnm = bginnm;
    }

    public String getQurynm() {
        return qurynm;
    }

    public void setQurynm(String qurynm) {
        this.qurynm = qurynm;
    }

    public String getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(String totlnm) {
        this.totlnm = totlnm;
    }

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getXdjtbh() {
        return xdjtbh;
    }

    public void setXdjtbh(String xdjtbh) {
        this.xdjtbh = xdjtbh;
    }

    public String getXdjtmc() {
        return xdjtmc;
    }

    public void setXdjtmc(String xdjtmc) {
        this.xdjtmc = xdjtmc;
    }

    public String getTyjtbh() {
        return tyjtbh;
    }

    public void setTyjtbh(String tyjtbh) {
        this.tyjtbh = tyjtbh;
    }

    public String getTyjtmc() {
        return tyjtmc;
    }

    public void setTyjtmc(String tyjtmc) {
        this.tyjtmc = tyjtmc;
    }

    public String getGrouds() {
        return grouds;
    }

    public void setGrouds(String grouds) {
        this.grouds = grouds;
    }

    public String getAcctbr() {
        return acctbr;
    }

    public void setAcctbr(String acctbr) {
        this.acctbr = acctbr;
    }

    public String getAcmang() {
        return acmang;
    }

    public void setAcmang(String acmang) {
        this.acmang = acmang;
    }

    public String getMxmang() {
        return mxmang;
    }

    public void setMxmang(String mxmang) {
        this.mxmang = mxmang;
    }

    public String getInptdt() {
        return inptdt;
    }

    public void setInptdt(String inptdt) {
        this.inptdt = inptdt;
    }

    public String getInptbr() {
        return inptbr;
    }

    public void setInptbr(String inptbr) {
        this.inptbr = inptbr;
    }

    public String getJinmlv() {
        return jinmlv;
    }

    public void setJinmlv(String jinmlv) {
        this.jinmlv = jinmlv;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getListnm() {
        return listnm;
    }

    public void setListnm(Integer listnm) {
        this.listnm = listnm;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", bginnm='" + bginnm + '\'' +
                ", qurynm='" + qurynm + '\'' +
                ", totlnm='" + totlnm + '\'' +
                ", grouno='" + grouno + '\'' +
                ", grouna='" + grouna + '\'' +
                ", xdjtbh='" + xdjtbh + '\'' +
                ", xdjtmc='" + xdjtmc + '\'' +
                ", tyjtbh='" + tyjtbh + '\'' +
                ", tyjtmc='" + tyjtmc + '\'' +
                ", grouds='" + grouds + '\'' +
                ", acctbr='" + acctbr + '\'' +
                ", acmang='" + acmang + '\'' +
                ", mxmang='" + mxmang + '\'' +
                ", inptdt='" + inptdt + '\'' +
                ", inptbr='" + inptbr + '\'' +
                ", jinmlv='" + jinmlv + '\'' +
                ", remark='" + remark + '\'' +
                ", listnm=" + listnm +
                ", list=" + list +
                '}';
    }
}
