package cn.com.yusys.yusp.web.server.biz.xdht0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0015.req.Xdht0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0015.resp.Xdht0015RespDto;
import cn.com.yusys.yusp.dto.server.xdht0015.req.Xdht0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0015.resp.Xdht0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同房产人员信息查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0015:合同房产人员信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0015Resource.class);

	@Autowired
	private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0015
     * 交易描述：合同房产人员信息查询
     *
     * @param xdht0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同房产人员信息查询")
    @PostMapping("/xdht0015")
    //@Idempotent({"xdcaht0015", "#xdht0015ReqDto.datasq"})
    protected @ResponseBody
	Xdht0015RespDto xdht0015(@Validated @RequestBody Xdht0015ReqDto xdht0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, JSON.toJSONString(xdht0015ReqDto));
        Xdht0015DataReqDto xdht0015DataReqDto = new Xdht0015DataReqDto();// 请求Data： 合同房产人员信息查询
        Xdht0015DataRespDto xdht0015DataRespDto = new Xdht0015DataRespDto();// 响应Data：合同房产人员信息查询
		Xdht0015RespDto xdht0015RespDto = new Xdht0015RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdht0015.req.Data reqData = null; // 请求Data：合同房产人员信息查询
		cn.com.yusys.yusp.dto.server.biz.xdht0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0015.resp.Data();// 响应Data：合同房产人员信息查询
        //  此处包导入待确定 结束
        try {
            // 从 xdht0015ReqDto获取 reqData
            reqData = xdht0015ReqDto.getData();
            // 将 reqData 拷贝到xdht0015DataReqDto
            BeanUtils.copyProperties(reqData, xdht0015DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, JSON.toJSONString(xdht0015DataReqDto));
            ResultDto<Xdht0015DataRespDto> xdht0015DataResultDto = dscmsBizHtClientService.xdht0015(xdht0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, JSON.toJSONString(xdht0015DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0015RespDto.setErorcd(Optional.ofNullable(xdht0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0015RespDto.setErortx(Optional.ofNullable(xdht0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0015DataResultDto.getCode())) {
                xdht0015DataRespDto = xdht0015DataResultDto.getData();
                xdht0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, e.getMessage());
            xdht0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0015RespDto.setDatasq(servsq);

        xdht0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0015.key, DscmsEnum.TRADE_CODE_XDHT0015.value, JSON.toJSONString(xdht0015RespDto));
        return xdht0015RespDto;
    }
}
