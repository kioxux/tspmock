package cn.com.yusys.yusp.web.client.esb.core.ln3163;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3163.Ln3163ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3163.Ln3163RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3163.req.Ln3163ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.Ln3163RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3163)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3163Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3163Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3163
     * 交易描述：资产证券化处理
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3163:资产证券化处理")
    @PostMapping("/ln3163")
    protected @ResponseBody
    ResultDto<Ln3163RespDto> ln3163(@Validated @RequestBody Ln3163ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3163.key, EsbEnum.TRADE_CODE_LN3163.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3163.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3163.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.Service();
        Ln3163ReqService ln3163ReqService = new Ln3163ReqService();
        Ln3163RespService ln3163RespService = new Ln3163RespService();
        Ln3163RespDto ln3163RespDto = new Ln3163RespDto();
        ResultDto<Ln3163RespDto> ln3163ResultDto = new ResultDto<Ln3163RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3163ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3163.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3163ReqService.setService(reqService);
            // 将ln3163ReqService转换成ln3163ReqServiceMap
            Map ln3163ReqServiceMap = beanMapUtil.beanToMap(ln3163ReqService);
            context.put("tradeDataMap", ln3163ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3163.key, EsbEnum.TRADE_CODE_LN3163.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3163.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3163.key, EsbEnum.TRADE_CODE_LN3163.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3163RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3163RespService.class, Ln3163RespService.class);
            respService = ln3163RespService.getService();

            ln3163ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3163ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3163RespDto
                BeanUtils.copyProperties(respService, ln3163RespDto);

                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.List ln3163RespList = Optional.ofNullable(respService.getList())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.List());
                respService.setList(ln3163RespList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3163.resp.Record> recordList =
                            Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3163.Lstzrjj> lstzrjjList =
                            recordList.parallelStream().map(record -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3163.Lstzrjj lstzrjj = new cn.com.yusys.yusp.dto.client.esb.core.ln3163.Lstzrjj();
                                BeanUtils.copyProperties(record, lstzrjj);
                                return lstzrjj;
                            }).collect(Collectors.toList());
                    ln3163RespDto.setLstzrjj(lstzrjjList);
                }
                ln3163ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3163ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3163ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3163ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3163.key, EsbEnum.TRADE_CODE_LN3163.value, e.getMessage());
            ln3163ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3163ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3163ResultDto.setData(ln3163RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3163.key, EsbEnum.TRADE_CODE_LN3163.value, JSON.toJSONString(ln3163ResultDto));
        return ln3163ResultDto;
    }
}
