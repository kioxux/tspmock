package cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.resp;

import java.math.BigDecimal;

/**
 * 响应Service：通过共有人编号查询押品编号
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String guar_no;//押品编号
    private String common_owner_name;//共有人客户名称
    private BigDecimal common_hold_portio;//共有人所占份额

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    public String getCommon_owner_name() {
        return common_owner_name;
    }

    public void setCommon_owner_name(String common_owner_name) {
        this.common_owner_name = common_owner_name;
    }

    public BigDecimal getCommon_hold_portio() {
        return common_hold_portio;
    }

    public void setCommon_hold_portio(BigDecimal common_hold_portio) {
        this.common_hold_portio = common_hold_portio;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", guar_no='" + guar_no + '\'' +
                ", common_owner_name='" + common_owner_name + '\'' +
                ", common_hold_portio=" + common_hold_portio +
                '}';
    }
}
