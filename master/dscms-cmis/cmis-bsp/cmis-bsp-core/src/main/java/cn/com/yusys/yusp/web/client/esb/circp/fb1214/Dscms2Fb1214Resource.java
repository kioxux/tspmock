package cn.com.yusys.yusp.web.client.esb.circp.fb1214;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.ENT_LIST;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.Fb1214ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.HOUSE_LIST;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.Fb1214RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.Fb1214ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.houselist.Record;
import cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.Fb1214RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1214）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1214Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1214Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1214
     * 交易描述：房产信息修改同步
     *
     * @param fb1214ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1214")
    protected @ResponseBody
    ResultDto<Fb1214RespDto> fb1214(@Validated @RequestBody Fb1214ReqDto fb1214ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1214.key, EsbEnum.TRADE_CODE_FB1214.value, JSON.toJSONString(fb1214ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.Service();
        Fb1214ReqService fb1214ReqService = new Fb1214ReqService();
        Fb1214RespService fb1214RespService = new Fb1214RespService();
        Fb1214RespDto fb1214RespDto = new Fb1214RespDto();
        ResultDto<Fb1214RespDto> fb1214ResultDto = new ResultDto<Fb1214RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1214ReqDto转换成reqService
            BeanUtils.copyProperties(fb1214ReqDto, reqService);

            cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.HOUSE_LIST reqHOUSE_LIST = new cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.HOUSE_LIST();
            if (CollectionUtils.nonEmpty(fb1214ReqDto.getHOUSE_LIST())) {
                List<HOUSE_LIST> reqHoseList = fb1214ReqDto.getHOUSE_LIST();
                List<Record> targetList1 = reqHoseList.stream().map(h -> {
                    cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.houselist.Record target1 = new cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.houselist.Record();
                    BeanUtils.copyProperties(h, target1);
                    return target1;
                }).collect(Collectors.toList());
                reqHOUSE_LIST.setRecord(targetList1);
                reqService.setHOUSE_LIST(reqHOUSE_LIST);
            }

            cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.ENT_LIST reqENT_LIST = new cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.ENT_LIST();
            if (CollectionUtils.nonEmpty(fb1214ReqDto.getENT_LIST())) {

                List<ENT_LIST> Reqent_list = fb1214ReqDto.getENT_LIST();
                List<cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.endlist.Record> targetList2 = Reqent_list.stream().map(e -> {
                    cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.endlist.Record target2 = new cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.endlist.Record();
                    BeanUtils.copyProperties(e, target2);
                    return target2;

                }).collect(Collectors.toList());
                reqENT_LIST.setRecord(targetList2);
                reqService.setENT_LIST(reqENT_LIST);
            }


            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1214.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1214ReqService.setService(reqService);
            // 将fb1214ReqService转换成fb1214ReqServiceMap
            Map fb1214ReqServiceMap = beanMapUtil.beanToMap(fb1214ReqService);
            context.put("tradeDataMap", fb1214ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1214.key, EsbEnum.TRADE_CODE_FB1214.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1214.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1214.key, EsbEnum.TRADE_CODE_FB1214.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1214RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1214RespService.class, Fb1214RespService.class);
            respService = fb1214RespService.getService();

            fb1214ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1214ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1214RespDto
                BeanUtils.copyProperties(respService, fb1214RespDto);
                cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.HOUSE_LIST respHouse_list = Optional.ofNullable(respService.getHOUSE_LIST()).orElse(new cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.HOUSE_LIST());
                if (CollectionUtils.nonEmpty(respHouse_list.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.Record> recordList = respHouse_list.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.HOUSE_LIST> targetList3 = recordList.stream().map(h -> {
                        cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.HOUSE_LIST target3 = new cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.HOUSE_LIST();
                        BeanUtils.copyProperties(h, target3);
                        return target3;
                    }).collect(Collectors.toList());
                    fb1214RespDto.setHOSE_LIST(targetList3);
                }


                fb1214ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1214ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1214ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1214ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1214.key, EsbEnum.TRADE_CODE_FB1214.value, e.getMessage());
            fb1214ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1214ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1214ResultDto.setData(fb1214RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1214.key, EsbEnum.TRADE_CODE_FB1214.value, JSON.toJSONString(fb1214ResultDto));
        return fb1214ResultDto;
    }
}
