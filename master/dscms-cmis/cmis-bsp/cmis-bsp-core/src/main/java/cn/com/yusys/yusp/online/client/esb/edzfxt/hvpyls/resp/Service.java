package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp;

import java.math.BigDecimal;

/**
 * 响应Service：大额往账列表查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private Integer icount;//总笔数
    private BigDecimal dcount;//总金额
    private Integer listnm;//当前页面总笔数
    private cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp.List list;


    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Integer getIcount() {
        return icount;
    }

    public void setIcount(Integer icount) {
        this.icount = icount;
    }

    public BigDecimal getDcount() {
        return dcount;
    }

    public void setDcount(BigDecimal dcount) {
        this.dcount = dcount;
    }

    public Integer getListnm() {
        return listnm;
    }

    public void setListnm(Integer listnm) {
        this.listnm = listnm;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", icount=" + icount +
                ", dcount=" + dcount +
                ", listnm=" + listnm +
                ", list=" + list +
                '}';
    }
}
