package cn.com.yusys.yusp.web.client.esb.core.ln3138;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3138.Ln3138ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3138.Ln3138RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3138.req.Ln3138ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3138.resp.Ln3138RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3138.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3138)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3138Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3138Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3138
     * 交易描述：等额等本贷款推算
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3138:等额等本贷款推算")
    @PostMapping("/ln3138")
    protected @ResponseBody
    ResultDto<Ln3138RespDto> ln3138(@Validated @RequestBody Ln3138ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3138.key, EsbEnum.TRADE_CODE_LN3138.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3138.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3138.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3138.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3138.resp.Service();
        Ln3138ReqService ln3138ReqService = new Ln3138ReqService();
        Ln3138RespService ln3138RespService = new Ln3138RespService();
        Ln3138RespDto ln3138RespDto = new Ln3138RespDto();
        ResultDto<Ln3138RespDto> ln3138ResultDto = new ResultDto<Ln3138RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3138ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3138.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3138ReqService.setService(reqService);
            // 将ln3138ReqService转换成ln3138ReqServiceMap
            Map ln3138ReqServiceMap = beanMapUtil.beanToMap(ln3138ReqService);
            context.put("tradeDataMap", ln3138ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3138.key, EsbEnum.TRADE_CODE_LN3138.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3138.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3138.key, EsbEnum.TRADE_CODE_LN3138.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3138RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3138RespService.class, Ln3138RespService.class);
            respService = ln3138RespService.getService();

            ln3138ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3138ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3138RespDto
                BeanUtils.copyProperties(respService, ln3138RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3138.resp.List ln3138RespList = Optional.ofNullable(respService.getList())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3138.resp.List());
                respService.setList(ln3138RespList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3138.resp.Record> recordList =
                            Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3138.Hkmingx> hkmingxList =
                            recordList.parallelStream().map(record -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3138.Hkmingx hkmingx = new cn.com.yusys.yusp.dto.client.esb.core.ln3138.Hkmingx();
                                BeanUtils.copyProperties(record, hkmingx);
                                return hkmingx;
                            }).collect(Collectors.toList());
                    ln3138RespDto.setHkmingx(hkmingxList);
                }
                ln3138ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3138ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3138ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3138ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3138.key, EsbEnum.TRADE_CODE_LN3138.value, e.getMessage());
            ln3138ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3138ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3138ResultDto.setData(ln3138RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3138.key, EsbEnum.TRADE_CODE_LN3138.value, JSON.toJSONString(ln3138ResultDto));
        return ln3138ResultDto;
    }
}
