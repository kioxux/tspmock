package cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 9:37
 * @since 2021/5/27 9:37
 */
public class Body {
    private String cardno;//卡号
    private String crcycd;//币种
    private String rgstid;//分期申请顺序号
    private String lnittm;//分期总期数
    private String lnfemd;//分期手续费收取方式
    private BigDecimal lnitpn;//分期总本金
    private BigDecimal lnfdpt;//分期每期应还本金
    private BigDecimal lnfttm;//分期首期应还本金
    private BigDecimal lnflt2;//分期末期应还本金
    private BigDecimal lnitfi;//分期总手续费
    private BigDecimal lnfdfi;//分期每期手续费
    private BigDecimal lnftfi;//分期首期手续费
    private BigDecimal lnfltm;//分期末期手续费
    private String sendmo;//分期放款方式
    private String dbnknm;//分期放款银行名称
    private String dbkbch;//分期放款开户行号
    private String dbkact;//分期放款账号
    private String bkctnm;//分期放款账户姓名
    private String papose;//资金用途
    private String loanet;//分期放款账户对公/对私标识
    private String salman;//分期营销人员姓名
    private String saleno;//分期营销人员编号
    private String salech;//分期营销人员所属分行
    private String eappno;//外部申请编号
    private String loancd;//分期计划代码
    private String anrate;//分期近似年化利率

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getRgstid() {
        return rgstid;
    }

    public void setRgstid(String rgstid) {
        this.rgstid = rgstid;
    }

    public String getLnittm() {
        return lnittm;
    }

    public void setLnittm(String lnittm) {
        this.lnittm = lnittm;
    }

    public String getLnfemd() {
        return lnfemd;
    }

    public void setLnfemd(String lnfemd) {
        this.lnfemd = lnfemd;
    }

    public BigDecimal getLnitpn() {
        return lnitpn;
    }

    public void setLnitpn(BigDecimal lnitpn) {
        this.lnitpn = lnitpn;
    }

    public BigDecimal getLnfdpt() {
        return lnfdpt;
    }

    public void setLnfdpt(BigDecimal lnfdpt) {
        this.lnfdpt = lnfdpt;
    }

    public BigDecimal getLnfttm() {
        return lnfttm;
    }

    public void setLnfttm(BigDecimal lnfttm) {
        this.lnfttm = lnfttm;
    }

    public BigDecimal getLnflt2() {
        return lnflt2;
    }

    public void setLnflt2(BigDecimal lnflt2) {
        this.lnflt2 = lnflt2;
    }

    public BigDecimal getLnitfi() {
        return lnitfi;
    }

    public void setLnitfi(BigDecimal lnitfi) {
        this.lnitfi = lnitfi;
    }

    public BigDecimal getLnfdfi() {
        return lnfdfi;
    }

    public void setLnfdfi(BigDecimal lnfdfi) {
        this.lnfdfi = lnfdfi;
    }

    public BigDecimal getLnftfi() {
        return lnftfi;
    }

    public void setLnftfi(BigDecimal lnftfi) {
        this.lnftfi = lnftfi;
    }

    public BigDecimal getLnfltm() {
        return lnfltm;
    }

    public void setLnfltm(BigDecimal lnfltm) {
        this.lnfltm = lnfltm;
    }

    public String getSendmo() {
        return sendmo;
    }

    public void setSendmo(String sendmo) {
        this.sendmo = sendmo;
    }

    public String getDbnknm() {
        return dbnknm;
    }

    public void setDbnknm(String dbnknm) {
        this.dbnknm = dbnknm;
    }

    public String getDbkbch() {
        return dbkbch;
    }

    public void setDbkbch(String dbkbch) {
        this.dbkbch = dbkbch;
    }

    public String getDbkact() {
        return dbkact;
    }

    public void setDbkact(String dbkact) {
        this.dbkact = dbkact;
    }

    public String getBkctnm() {
        return bkctnm;
    }

    public void setBkctnm(String bkctnm) {
        this.bkctnm = bkctnm;
    }

    public String getPapose() {
        return papose;
    }

    public void setPapose(String papose) {
        this.papose = papose;
    }

    public String getLoanet() {
        return loanet;
    }

    public void setLoanet(String loanet) {
        this.loanet = loanet;
    }

    public String getSalman() {
        return salman;
    }

    public void setSalman(String salman) {
        this.salman = salman;
    }

    public String getSaleno() {
        return saleno;
    }

    public void setSaleno(String saleno) {
        this.saleno = saleno;
    }

    public String getSalech() {
        return salech;
    }

    public void setSalech(String salech) {
        this.salech = salech;
    }

    public String getEappno() {
        return eappno;
    }

    public void setEappno(String eappno) {
        this.eappno = eappno;
    }

    public String getLoancd() {
        return loancd;
    }

    public void setLoancd(String loancd) {
        this.loancd = loancd;
    }

    public String getAnrate() {
        return anrate;
    }

    public void setAnrate(String anrate) {
        this.anrate = anrate;
    }

    @Override
    public String toString() {
        return "Body{" +
                "cardno='" + cardno + '\'' +
                ", crcycd='" + crcycd + '\'' +
                ", rgstid='" + rgstid + '\'' +
                ", lnittm='" + lnittm + '\'' +
                ", lnfemd='" + lnfemd + '\'' +
                ", lnitpn=" + lnitpn +
                ", lnfdpt=" + lnfdpt +
                ", lnfttm=" + lnfttm +
                ", lnflt2=" + lnflt2 +
                ", lnitfi=" + lnitfi +
                ", lnfdfi=" + lnfdfi +
                ", lnftfi=" + lnftfi +
                ", lnfltm=" + lnfltm +
                ", sendmo='" + sendmo + '\'' +
                ", dbnknm='" + dbnknm + '\'' +
                ", dbkbch='" + dbkbch + '\'' +
                ", dbkact='" + dbkact + '\'' +
                ", bkctnm='" + bkctnm + '\'' +
                ", papose='" + papose + '\'' +
                ", loanet='" + loanet + '\'' +
                ", salman='" + salman + '\'' +
                ", saleno='" + saleno + '\'' +
                ", salech='" + salech + '\'' +
                ", eappno='" + eappno + '\'' +
                ", loancd='" + loancd + '\'' +
                ", anrate='" + anrate + '\'' +
                '}';
    }
}
