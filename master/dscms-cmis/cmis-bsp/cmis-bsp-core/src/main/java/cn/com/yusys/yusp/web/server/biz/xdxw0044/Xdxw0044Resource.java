package cn.com.yusys.yusp.web.server.biz.xdxw0044;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0044.req.Xdxw0044ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0044.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0044.resp.Xdxw0044RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0044.req.Xdxw0044DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0044.resp.Xdxw0044DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优抵贷待办事项接口
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0044:优抵贷待办事项接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0044Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0044Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0044
     * 交易描述：优抵贷待办事项接口
     *
     * @param xdxw0044ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优抵贷待办事项接口")
    @PostMapping("/xdxw0044")
    ////@Idempotent({"xdcaxw0044", "#xdxw0044ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0044RespDto xdxw0044(@Validated @RequestBody Xdxw0044ReqDto xdxw0044ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044ReqDto));
        Xdxw0044DataReqDto xdxw0044DataReqDto = new Xdxw0044DataReqDto();// 请求Data： 优抵贷待办事项接口
        Xdxw0044DataRespDto xdxw0044DataRespDto = new Xdxw0044DataRespDto();// 响应Data：优抵贷待办事项接口
        cn.com.yusys.yusp.dto.server.biz.xdxw0044.req.Data reqData = null; // 请求Data：优抵贷待办事项接口
        cn.com.yusys.yusp.dto.server.biz.xdxw0044.resp.Data respData = new Data();// 响应Data：优抵贷待办事项接口
		Xdxw0044RespDto xdxw0044RespDto = new Xdxw0044RespDto();
		try {
            // 从 xdxw0044ReqDto获取 reqData
            reqData = xdxw0044ReqDto.getData();
            // 将 reqData 拷贝到xdxw0044DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0044DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044DataReqDto));
            ResultDto<Xdxw0044DataRespDto> xdxw0044DataResultDto = dscmsBizXwClientService.xdxw0044(xdxw0044DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0044RespDto.setErorcd(Optional.ofNullable(xdxw0044DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0044RespDto.setErortx(Optional.ofNullable(xdxw0044DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0044DataResultDto.getCode())) {
                xdxw0044DataRespDto = xdxw0044DataResultDto.getData();
                xdxw0044RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0044RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0044DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0044DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, e.getMessage());
            xdxw0044RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0044RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0044RespDto.setDatasq(servsq);

        xdxw0044RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0044.key, DscmsEnum.TRADE_CODE_XDXW0044.value, JSON.toJSONString(xdxw0044RespDto));
        return xdxw0044RespDto;
    }
}
