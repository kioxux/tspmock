package cn.com.yusys.yusp.web.client.esb.xwywglpt.wxd004;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.req.Wxd004ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.resp.Wxd004RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷系统请求V平台二级准入接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "BSP封装小微业务管理平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Xwd004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xwd004Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：wxd004
     * 交易描述：信贷系统获取征信报送监管信息接口
     *
     * @param wxd004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("wxd004:信贷系统获取征信报送监管信息接口")
    @PostMapping("/wxd004")
    protected @ResponseBody
    ResultDto<Wxd004RespDto> wxd004(@Validated @RequestBody Wxd004ReqDto wxd004ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD004.key, EsbEnum.TRADE_CODE_WXD004.value, JSON.toJSONString(wxd004ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.resp.Service();
        Wxd004ReqService wxd004ReqService = new Wxd004ReqService();
        Wxd004RespService wxd004RespService = new Wxd004RespService();
        Wxd004RespDto wxd004RespDto = new Wxd004RespDto();
        ResultDto<Wxd004RespDto> wxd004ResultDto = new ResultDto<Wxd004RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将wxd004ReqDto转换成reqService
            BeanUtils.copyProperties(wxd004ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_WXD004.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            wxd004ReqService.setService(reqService);
            // 将wxd004ReqService转换成wxd004ReqServiceMap
            Map wxd004ReqServiceMap = beanMapUtil.beanToMap(wxd004ReqService);
            context.put("tradeDataMap", wxd004ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD004.key, EsbEnum.TRADE_CODE_WXD004.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_WXD004.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD004.key, EsbEnum.TRADE_CODE_WXD004.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            wxd004RespService = beanMapUtil.mapToBean(tradeDataMap, Wxd004RespService.class, Wxd004RespService.class);
            respService = wxd004RespService.getService();

            wxd004ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            wxd004ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, wxd004RespDto);
                wxd004ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                wxd004ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                wxd004ResultDto.setCode(EpbEnum.EPB099999.key);
                wxd004ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD004.key, EsbEnum.TRADE_CODE_WXD004.value, e.getMessage());
            wxd004ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            wxd004ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        wxd004ResultDto.setData(wxd004RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXD004.key, EsbEnum.TRADE_CODE_WXD004.value, JSON.toJSONString(wxd004ResultDto));
        return wxd004ResultDto;
    }
}
