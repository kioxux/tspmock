package cn.com.yusys.yusp.web.client.esb.rircp.fbxw03;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw03.req.Fbxw03ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw03.resp.Fbxw03RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxw03）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw03Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw03Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB 增享贷风控测算接口（处理码fbxw03）
     *
     * @param fbxw03ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw03:ESB 增享贷风控测算接口")
    @PostMapping("/fbxw03")
    protected @ResponseBody
    ResultDto<Fbxw03RespDto> fbxw03(@Validated @RequestBody Fbxw03ReqDto fbxw03ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value, JSON.toJSONString(fbxw03ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw03.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw03.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw03.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw03.resp.Service();
        Fbxw03ReqService fbxw03ReqService = new Fbxw03ReqService();
        Fbxw03RespService fbxw03RespService = new Fbxw03RespService();
        Fbxw03RespDto fbxw03RespDto = new Fbxw03RespDto();
        ResultDto<Fbxw03RespDto> fbxw03ResultDto = new ResultDto<Fbxw03RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw03ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw03ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW03.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw03ReqService.setService(reqService);
            // 将fbxw03ReqService转换成fbxw03ReqServiceMap
            Map fbxw03ReqServiceMap = beanMapUtil.beanToMap(fbxw03ReqService);
            context.put("tradeDataMap", fbxw03ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW03.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw03RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw03RespService.class, Fbxw03RespService.class);
            respService = fbxw03RespService.getService();

            //  将Fbxw03RespDto封装到ResultDto<Fbxw03RespDto>
            fbxw03ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw03ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw03RespDto
                BeanUtils.copyProperties(respService, fbxw03RespDto);
                fbxw03ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw03ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw03ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw03ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value, e.getMessage());
            fbxw03ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw03ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw03ResultDto.setData(fbxw03RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW03.key, EsbEnum.TRADE_CODE_FBXW03.value, JSON.toJSONString(fbxw03ResultDto));
        return fbxw03ResultDto;
    }
}
