package cn.com.yusys.yusp.web.client.esb.rircp.fbxd10;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10.Fbxd10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10.Fbxd10RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.req.Fbxd10ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp.Fbxd10RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd10）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd10Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd10Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd10
     * 交易描述：查询日初（合约）信息历史表（利翃）的合约状态和结清日期
     *
     * @param fbxd10ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd10")
    protected @ResponseBody
    ResultDto<Fbxd10RespDto> fbxd10(@Validated @RequestBody Fbxd10ReqDto fbxd10ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd10ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp.Service();
        Fbxd10ReqService fbxd10ReqService = new Fbxd10ReqService();
        Fbxd10RespService fbxd10RespService = new Fbxd10RespService();
        Fbxd10RespDto fbxd10RespDto = new Fbxd10RespDto();
        ResultDto<Fbxd10RespDto> fbxd10ResultDto = new ResultDto<Fbxd10RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd10ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd10ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD10.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            fbxd10ReqService.setService(reqService);
            // 将fbxd10ReqService转换成fbxd10ReqServiceMap
            Map fbxd10ReqServiceMap = beanMapUtil.beanToMap(fbxd10ReqService);
            context.put("tradeDataMap", fbxd10ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD10.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd10RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd10RespService.class, Fbxd10RespService.class);
            respService = fbxd10RespService.getService();

            fbxd10ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd10ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd10RespDto
                BeanUtils.copyProperties(respService, fbxd10RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp.List fbxd10RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp.List());
                respService.setList(fbxd10RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> fbxd10RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10.List> fbxd10RespDtoLists = fbxd10RespRecordList.parallelStream().map(fbxd10RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10.List fbxd10RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10.List();
                        BeanUtils.copyProperties(fbxd10RespRecord, fbxd10RespDtoList);
                        return fbxd10RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd10RespDto.setList(fbxd10RespDtoLists);
                }
                fbxd10ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd10ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd10ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd10ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, e.getMessage());
            fbxd10ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd10ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd10ResultDto.setData(fbxd10RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd10ResultDto));
        return fbxd10ResultDto;
    }
}
