package cn.com.yusys.yusp.web.client.esb.yk.yky001;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.List;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.Yky001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.resp.Yky001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yk.yky001.req.Record;
import cn.com.yusys.yusp.online.client.esb.yk.yky001.req.Yky001ReqService;
import cn.com.yusys.yusp.online.client.esb.yk.yky001.resp.Yky001RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用印控系统的接口处理类（yky001）")
@RestController
@RequestMapping("/api/dscms2yk")
public class Dscms2Yky001Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Yky001Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB 用印申请接口（处理码yky001）
     *
     * @param yky001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("yky001:ESB 用印申请接口")
    @PostMapping("/yky001")
    protected @ResponseBody
    ResultDto<Yky001RespDto> yky001(@Validated @RequestBody Yky001ReqDto yky001ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value, JSON.toJSONString(yky001ReqDto));
        cn.com.yusys.yusp.online.client.esb.yk.yky001.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yk.yky001.req.Service();
        cn.com.yusys.yusp.online.client.esb.yk.yky001.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yk.yky001.resp.Service();
        Yky001ReqService yky001ReqService = new Yky001ReqService();
        Yky001RespService yky001RespService = new Yky001RespService();
        Yky001RespDto yky001RespDto = new Yky001RespDto();
        ResultDto<Yky001RespDto> yky001ResultDto = new ResultDto<Yky001RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Yky001ReqDto转换成reqService
            BeanUtils.copyProperties(yky001ReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.yk.yky001.req.List serviceList = new cn.com.yusys.yusp.online.client.esb.yk.yky001.req.List();
            if (CollectionUtils.nonEmpty(yky001ReqDto.getList())) {
                java.util.List<List> list = yky001ReqDto.getList();
                java.util.List<Record> targetList = list.stream().map(l -> {
                    Record record = new Record();
                    BeanUtils.copyProperties(l, record);
                    return record;
                }).collect(Collectors.toList());
                serviceList.setRecord(targetList);
                reqService.setList(serviceList);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_YKY001.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YK.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YK.key);//    部门号
            yky001ReqService.setService(reqService);
            // 将yky001ReqService转换成yky001ReqServiceMap
            Map yky001ReqServiceMap = beanMapUtil.beanToMap(yky001ReqService);
            context.put("tradeDataMap", yky001ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_YKY001.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            yky001RespService = beanMapUtil.mapToBean(tradeDataMap, Yky001RespService.class, Yky001RespService.class);
            respService = yky001RespService.getService();

            //  将Yky001RespDto封装到ResultDto<Yky001RespDto>
            yky001ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            yky001ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.YKY_SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Yky001RespDto
                BeanUtils.copyProperties(respService, yky001RespDto);
                yky001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                yky001ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                yky001ResultDto.setCode(EpbEnum.EPB099999.key);
                yky001ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value, e.getMessage());
            yky001ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            yky001ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        yky001ResultDto.setData(yky001RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YKY001.key, EsbEnum.TRADE_CODE_YKY001.value, JSON.toJSONString(yky001ResultDto));
        return yky001ResultDto;
    }
}
