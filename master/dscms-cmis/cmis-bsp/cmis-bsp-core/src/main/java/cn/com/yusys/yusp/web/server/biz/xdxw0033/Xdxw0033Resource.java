package cn.com.yusys.yusp.web.server.biz.xdxw0033;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0033.req.Xdxw0033ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0033.resp.Xdxw0033RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0033.req.Xdxw0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0033.resp.Xdxw0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据流水号查询征信报告关联信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0033:根据流水号查询征信报告关联信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0033Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0033
     * 交易描述：根据流水号查询征信报告关联信息
     *
     * @param xdxw0033ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询征信报告关联信息")
    @PostMapping("/xdxw0033")
    //@Idempotent({"xdcaxw0033", "#xdxw0033ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0033RespDto xdxw0033(@Validated @RequestBody Xdxw0033ReqDto xdxw0033ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033ReqDto));
        Xdxw0033DataReqDto xdxw0033DataReqDto = new Xdxw0033DataReqDto();// 请求Data： 根据流水号查询征信报告关联信息
        Xdxw0033DataRespDto xdxw0033DataRespDto = new Xdxw0033DataRespDto();// 响应Data：根据流水号查询征信报告关联信息
        Xdxw0033RespDto xdxw0033RespDto = new Xdxw0033RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0033.req.Data reqData = null; // 请求Data：根据流水号查询征信报告关联信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0033.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0033.resp.Data();// 响应Data：根据流水号查询征信报告关联信息
        try {
            // 从 xdxw0033ReqDto获取 reqData
            reqData = xdxw0033ReqDto.getData();
            // 将 reqData 拷贝到xdxw0033DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0033DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataReqDto));
            ResultDto<Xdxw0033DataRespDto> xdxw0033DataResultDto = dscmsBizXwClientService.xdxw0033(xdxw0033DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0033RespDto.setErorcd(Optional.ofNullable(xdxw0033DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0033RespDto.setErortx(Optional.ofNullable(xdxw0033DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0033DataResultDto.getCode())) {
                xdxw0033DataRespDto = xdxw0033DataResultDto.getData();
                xdxw0033RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0033RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0033DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0033DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, e.getMessage());
            xdxw0033RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0033RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0033RespDto.setDatasq(servsq);

        xdxw0033RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0033.key, DscmsEnum.TRADE_CODE_XDXW0033.value, JSON.toJSONString(xdxw0033RespDto));
        return xdxw0033RespDto;
    }
}
