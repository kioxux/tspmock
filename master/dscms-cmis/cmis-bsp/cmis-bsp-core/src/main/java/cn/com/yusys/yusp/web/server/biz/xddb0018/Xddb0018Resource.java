package cn.com.yusys.yusp.web.server.biz.xddb0018;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0018.req.Xddb0018ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0018.resp.Xddb0018RespDto;
import cn.com.yusys.yusp.dto.server.xddb0018.req.Xddb0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0018.resp.Xddb0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品状态同步
 *
 * @author zhugenrong
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDDB0018:押品状态同步")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0018Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0018
     * 交易描述：押品状态同步
     *
     * @param xddb0018ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddb0018:押品状态同步")
    @PostMapping("/xddb0018")
    //@Idempotent({"xddb0018", "#xddb0018ReqDto.datasq"})
    protected @ResponseBody
    Xddb0018RespDto xddb0018(@Validated @RequestBody Xddb0018ReqDto xddb0018ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018ReqDto));
        Xddb0018DataReqDto xddb0018DataReqDto = new Xddb0018DataReqDto();// 请求Data： 押品状态同步
        Xddb0018DataRespDto xddb0018DataRespDto = new Xddb0018DataRespDto();// 响应Data：押品状态同步
        Xddb0018RespDto xddb0018RespDto = new Xddb0018RespDto();// 响应Data：押品状态同步
        cn.com.yusys.yusp.dto.server.biz.xddb0018.req.Data reqData = null; // 请求Data：押品状态同步
        cn.com.yusys.yusp.dto.server.biz.xddb0018.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0018.resp.Data();// 响应Data：押品状态同步
        try {
            // 从 xddb0018ReqDto获取 reqData
            reqData = xddb0018ReqDto.getData();
            // 将 reqData 拷贝到xddb0018DataReqDto
            BeanUtils.copyProperties(reqData, xddb0018DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018DataReqDto));
            ResultDto<Xddb0018DataRespDto> xddb0018DataResultDto = dscmsBizDbClientService.xddb0018(xddb0018DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0018RespDto.setErorcd(Optional.ofNullable(xddb0018DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0018RespDto.setErortx(Optional.ofNullable(xddb0018DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0018DataResultDto.getCode())) {
                xddb0018RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0018RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xddb0018DataRespDto = xddb0018DataResultDto.getData();
                // 将 xddb0018DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0018DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            xddb0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0018RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0018RespDto.setDatasq(servsq);

        xddb0018RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, JSON.toJSONString(xddb0018RespDto));
        return xddb0018RespDto;
    }

}
