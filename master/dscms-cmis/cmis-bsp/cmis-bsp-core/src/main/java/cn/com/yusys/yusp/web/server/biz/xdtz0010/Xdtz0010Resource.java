package cn.com.yusys.yusp.web.server.biz.xdtz0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0010.req.Xdtz0010ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0010.resp.Xdtz0010RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0010.req.Xdtz0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0010.resp.Xdtz0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据身份证号获取借据信息
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDTZ0010:根据身份证号获取借据信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0010Resource.class);
    /**
     * 交易码：xdtz0010
     * 交易描述：根据身份证号获取借据信息
     *
     * @param xdtz0010ReqDto
     * @throws Exception
     * @return
     */
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    @ApiOperation("根据身份证号获取借据信息")
    @PostMapping("/xdtz0010")
    //@Idempotent({"xdcatz0010", "#xdtz0010ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0010RespDto xdtz0010(@Validated @RequestBody Xdtz0010ReqDto xdtz0010ReqDto ) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010ReqDto));
        Xdtz0010DataReqDto  xdtz0010DataReqDto = new Xdtz0010DataReqDto();// 请求Data： 根据身份证号获取借据信息
        Xdtz0010DataRespDto  xdtz0010DataRespDto =  new Xdtz0010DataRespDto();// 响应Data：根据身份证号获取借据信息
        Xdtz0010RespDto xdtz0010RespDto = new Xdtz0010RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0010.req.Data reqData = null; // 请求Data：根据身份证号获取借据信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0010.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0010.resp.Data();// 响应Data：根据身份证号获取借据信息

        try {
            // 从 xdtz0010ReqDto获取 reqData
            reqData = xdtz0010ReqDto.getData();
            // 将 reqData 拷贝到xdtz0010DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0010DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010DataReqDto));
            ResultDto<Xdtz0010DataRespDto> xdtz0010DataResultDto= dscmsBizTzClientService.xdtz0010(xdtz0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0010RespDto.setErorcd(Optional.ofNullable(xdtz0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0010RespDto.setErortx(Optional.ofNullable(xdtz0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,xdtz0010DataResultDto.getCode())) {
                xdtz0010DataRespDto = xdtz0010DataResultDto.getData();
                xdtz0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0010DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0010DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, e.getMessage());
            xdtz0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0010RespDto.setDatasq(servsq);

        xdtz0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0010.key, DscmsEnum.TRADE_CODE_XDTZ0010.value, JSON.toJSONString(xdtz0010RespDto));
        return xdtz0010RespDto;
    }
}

