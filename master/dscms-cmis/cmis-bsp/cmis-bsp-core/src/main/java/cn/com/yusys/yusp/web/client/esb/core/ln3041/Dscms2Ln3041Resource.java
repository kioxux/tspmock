package cn.com.yusys.yusp.web.client.esb.core.ln3041;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.req.Ln3041ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3041.req.Ln3041ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3041.resp.Ln3041RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3041)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3041Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3041Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3041
     * 交易描述：支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还；
     *
     * @param ln3041ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3041:支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还")
    @PostMapping("/ln3041")
    protected @ResponseBody
    ResultDto<Ln3041RespDto> ln3041(@Validated @RequestBody Ln3041ReqDto ln3041ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value, JSON.toJSONString(ln3041ReqDto, SerializerFeature.WriteMapNullValue));
        cn.com.yusys.yusp.online.client.esb.core.ln3041.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3041.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3041.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3041.resp.Service();
        Ln3041ReqService ln3041ReqService = new Ln3041ReqService();
        Ln3041RespService ln3041RespService = new Ln3041RespService();
        Ln3041RespDto ln3041RespDto = new Ln3041RespDto();
        ResultDto<Ln3041RespDto> ln3041ResultDto = new ResultDto<Ln3041RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3041ReqDto转换成reqService
            BeanUtils.copyProperties(ln3041ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3041.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(Optional.ofNullable(ln3041ReqDto.getBrchno()).orElse(EsbEnum.BRCHNO_CORE.key));//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3041ReqService.setService(reqService);
            // 将ln3041ReqService转换成ln3041ReqServiceMap
            Map ln3041ReqServiceMap = beanMapUtil.beanToMap(ln3041ReqService);
            Map service = (Map) ln3041ReqServiceMap.get("service");
            // bigdecimal字段转成map之后出现科学计数法问题解决 modify by chenyong 陈勇
            service.forEach((k, v) -> {
                if (v instanceof Double) {
                    BigDecimal bigDecimal = new BigDecimal(((Double) v));
                    service.put(k, bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });
            ln3041ReqServiceMap.put("service", service);
            context.put("tradeDataMap", ln3041ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3041.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3041RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3041RespService.class, Ln3041RespService.class);
            respService = ln3041RespService.getService();
            context.put("tradeDataMap", ln3041ReqServiceMap);

            //  将respService转换成Ln3041RespDto
            BeanUtils.copyProperties(respService, ln3041RespDto);
            ln3041ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3041ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3041RespDto
                BeanUtils.copyProperties(respService, ln3041RespDto);
                ln3041ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3041ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3041ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3041ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value, e.getMessage());
            ln3041ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3041ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3041ResultDto.setData(ln3041RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3041.key, EsbEnum.TRADE_CODE_LN3041.value, JSON.toJSONString(ln3041ResultDto, SerializerFeature.WriteMapNullValue));
        return ln3041ResultDto;
    }
}
