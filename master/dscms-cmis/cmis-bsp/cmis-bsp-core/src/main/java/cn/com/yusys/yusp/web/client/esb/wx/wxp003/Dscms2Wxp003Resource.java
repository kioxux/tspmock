package cn.com.yusys.yusp.web.client.esb.wx.wxp003;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp003.req.Wxp003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp003.resp.Wxp003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.wx.wxp003.req.Wxp003ReqService;
import cn.com.yusys.yusp.online.client.esb.wx.wxp003.resp.Wxp003RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用苏州征信系统的接口
 **/
@Api(tags = "BSP封装调用苏州征信系统的接口(wxp003)")
@RestController
@RequestMapping("/api/dscms2wx")
public class Dscms2Wxp003Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Wxp003Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 信贷将放款标识推送给移动端（处理码wxp003）
     *
     * @param wxp003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("wxp003:信贷将放款标识推送给移动端")
    @PostMapping("/wxp003")
    protected @ResponseBody
    ResultDto<Wxp003RespDto> wxp003(@Validated @RequestBody Wxp003ReqDto wxp003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP003.key, EsbEnum.TRADE_CODE_WXP003.value, JSON.toJSONString(wxp003ReqDto));
        cn.com.yusys.yusp.online.client.esb.wx.wxp003.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.wx.wxp003.req.Service();
        cn.com.yusys.yusp.online.client.esb.wx.wxp003.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.wx.wxp003.resp.Service();
        Wxp003ReqService wxp003ReqService = new Wxp003ReqService();
        Wxp003RespService wxp003RespService = new Wxp003RespService();
        Wxp003RespDto wxp003RespDto = new Wxp003RespDto();
        ResultDto<Wxp003RespDto> wxp003ResultDto = new ResultDto<Wxp003RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Wxp003ReqDto转换成reqService
            BeanUtils.copyProperties(wxp003ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_WXP003.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_SZZX.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_SZZX.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            wxp003ReqService.setService(reqService);
            // 将wxp003ReqService转换成wxp003ReqServiceMap
            Map wxp003ReqServiceMap = beanMapUtil.beanToMap(wxp003ReqService);
            context.put("tradeDataMap", wxp003ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP003.key, EsbEnum.TRADE_CODE_WXP003.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_WXP003.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP003.key, EsbEnum.TRADE_CODE_WXP003.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP003.key, EsbEnum.TRADE_CODE_WXP003.value);
            wxp003RespService = beanMapUtil.mapToBean(tradeDataMap, Wxp003RespService.class, Wxp003RespService.class);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP003.key, EsbEnum.TRADE_CODE_WXP003.value);
            respService = wxp003RespService.getService();
            //  将Wxp003RespDto封装到ResultDto<Wxp003RespDto>
            wxp003ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            wxp003ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Wxp003RespDto
                BeanUtils.copyProperties(respService, wxp003RespDto);
                wxp003ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                wxp003ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                wxp003ResultDto.setCode(EpbEnum.EPB099999.key);
                wxp003ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP003.key, EsbEnum.TRADE_CODE_WXP003.value, e.getMessage());
            wxp003ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            wxp003ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        wxp003ResultDto.setData(wxp003RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP003.key, EsbEnum.TRADE_CODE_WXP003.value, JSON.toJSONString(wxp003ResultDto));
        return wxp003ResultDto;
    }
}
