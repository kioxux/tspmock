package cn.com.yusys.yusp.online.client.esb.core.ln3128.resp;

/**
 * 响应Service：贷款机构变更查询
 * @author lihh
 * @version 1.0
 */
public class Service {
	private String erorcd;//响应码
	private String erortx;//响应信息
	private String servsq;//渠道流水
	private String datasq;//全局流水

	private Lstdkjgzy_ARRAY lstdkjgzy_ARRAY;//贷款账户机构转移信息

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public Lstdkjgzy_ARRAY getLstdkjgzy_ARRAY() {
		return lstdkjgzy_ARRAY;
	}

	public void setLstdkjgzy_ARRAY(Lstdkjgzy_ARRAY lstdkjgzy_ARRAY) {
		this.lstdkjgzy_ARRAY = lstdkjgzy_ARRAY;
	}

	@Override
	public String toString() {
		return "Service{" +
				"erorcd='" + erorcd + '\'' +
				", erortx='" + erortx + '\'' +
				", servsq='" + servsq + '\'' +
				", datasq='" + datasq + '\'' +
				", lstdkjgzy_ARRAY=" + lstdkjgzy_ARRAY +
				'}';
	}
}
