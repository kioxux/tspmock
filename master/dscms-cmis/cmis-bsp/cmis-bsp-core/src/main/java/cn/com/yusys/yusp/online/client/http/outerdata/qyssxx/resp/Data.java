package cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Service：涉诉信息查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "code")
    private String code;//返回码
    @JsonProperty(value = "msg")
    private String msg;// 返回码说明
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Data data;//返回内容
    @JsonProperty(value = "get_type")
    private String get_type;// 获取类型

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Data getData() {
        return data;
    }

    public void setData(cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.Data data) {
        this.data = data;
    }

    public String getGet_type() {
        return get_type;
    }

    public void setGet_type(String get_type) {
        this.get_type = get_type;
    }

    @Override
    public String toString() {
        return "Data{" +
                "code='" + code + '\'' +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                ", get_type='" + get_type + '\'' +
                '}';
    }
}
