package cn.com.yusys.yusp.web.client.esb.ypqzxt.cwm002;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm002.req.Cwm002ReqService;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm002.resp.Cwm002RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用押品权证系统的接口
 */
@Api(tags = "BSP封装调用押品权证系统的接口(cwm002)")
@RestController
@RequestMapping("/api/dscms2ypqzxt")
public class Dscms2Cwm002Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Cwm002Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 本异地借阅归还接口（处理码cwm002）
     *
     * @param cwm002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cwm002:本异地借阅归还接口")
    @PostMapping("/cwm002")
    protected @ResponseBody
    ResultDto<Cwm002RespDto> cwm002(@Validated @RequestBody Cwm002ReqDto cwm002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM002.key, EsbEnum.TRADE_CODE_CWM002.value, JSON.toJSONString(cwm002ReqDto));
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm002.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm002.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm002.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm002.resp.Service();
        Cwm002ReqService cwm002ReqService = new Cwm002ReqService();
        Cwm002RespService cwm002RespService = new Cwm002RespService();
        Cwm002RespDto cwm002RespDto = new Cwm002RespDto();
        ResultDto<Cwm002RespDto> cwm002ResultDto = new ResultDto<Cwm002RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Cwm002ReqDto转换成reqService
            BeanUtils.copyProperties(cwm002ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CWM002.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPQZXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPQZXT.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            cwm002ReqService.setService(reqService);
            // 将cwm002ReqService转换成cwm002ReqServiceMap
            Map cwm002ReqServiceMap = beanMapUtil.beanToMap(cwm002ReqService);
            context.put("tradeDataMap", cwm002ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM002.key, EsbEnum.TRADE_CODE_CWM002.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CWM002.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM002.key, EsbEnum.TRADE_CODE_CWM002.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cwm002RespService = beanMapUtil.mapToBean(tradeDataMap, Cwm002RespService.class, Cwm002RespService.class);
            respService = cwm002RespService.getService();

            //  将Cwm002RespDto封装到ResultDto<Cwm002RespDto>
            cwm002ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            cwm002ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Cwm002RespDto
                BeanUtils.copyProperties(respService, cwm002RespDto);
                cwm002ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cwm002ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cwm002ResultDto.setCode(EpbEnum.EPB099999.key);
                cwm002ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM002.key, EsbEnum.TRADE_CODE_CWM002.value, e.getMessage());
            cwm002ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cwm002ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cwm002ResultDto.setData(cwm002RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM002.key, EsbEnum.TRADE_CODE_CWM002.value, JSON.toJSONString(cwm002ResultDto));

        return cwm002ResultDto;
    }
}
