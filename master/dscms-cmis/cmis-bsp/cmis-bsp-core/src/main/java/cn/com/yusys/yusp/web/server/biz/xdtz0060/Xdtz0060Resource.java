package cn.com.yusys.yusp.web.server.biz.xdtz0060;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0060.req.Xdtz0060ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0060.resp.Xdtz0060RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0060.req.Xdtz0060DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0060.resp.Xdtz0060DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询我行有未结清贷款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0060:查询我行有未结清贷款")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0060Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0060Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0060
     * 交易描述：查询我行有未结清贷款
     *
     * @param xdtz0060ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询我行有未结清贷款")
    @PostMapping("/xdtz0060")
    //@Idempotent({"xdtz0060", "#xdtz0060ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0060RespDto xdtz0060(@Validated @RequestBody Xdtz0060ReqDto xdtz0060ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060ReqDto));
        Xdtz0060DataReqDto xdtz0060DataReqDto = new Xdtz0060DataReqDto();// 请求Data： 查询我行有未结清贷款
        Xdtz0060DataRespDto xdtz0060DataRespDto = new Xdtz0060DataRespDto();// 响应Data：查询我行有未结清贷款
        Xdtz0060RespDto xdtz0060RespDto = new Xdtz0060RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0060.req.Data reqData = null; // 请求Data：查询我行有未结清贷款
        cn.com.yusys.yusp.dto.server.biz.xdtz0060.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0060.resp.Data();// 响应Data：查询我行有未结清贷款
        try {
            // 从 xdtz0060ReqDto获取 reqData
            reqData = xdtz0060ReqDto.getData();
            // 将 reqData 拷贝到xdtz0060DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0060DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataReqDto));
            ResultDto<Xdtz0060DataRespDto> xdtz0060DataResultDto = dscmsBizTzClientService.xdtz0060(xdtz0060DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0060RespDto.setErorcd(Optional.ofNullable(xdtz0060DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0060RespDto.setErortx(Optional.ofNullable(xdtz0060DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0060DataResultDto.getCode())) {
                xdtz0060DataRespDto = xdtz0060DataResultDto.getData();
                xdtz0060RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0060RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0060DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0060DataRespDto, respData);
            }
        } catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, e.getMessage());
            xdtz0060RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0060RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, e.getMessage());
            xdtz0060RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0060RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0060RespDto.setDatasq(servsq);

        xdtz0060RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0060.key, DscmsEnum.TRADE_CODE_XDTZ0060.value, JSON.toJSONString(xdtz0060RespDto));
        return xdtz0060RespDto;
    }
}
