package cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp;

/**
 * 响应Service：存单信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class DepoisRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "DepoisRespService{" +
                "service=" + service +
                '}';
    }
}
