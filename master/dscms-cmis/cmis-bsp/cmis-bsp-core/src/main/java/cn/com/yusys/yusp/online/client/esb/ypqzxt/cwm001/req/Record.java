package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm001.req;

import java.math.BigDecimal;

/**
 * 请求Service：本异地借阅出库接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class Record {

    private String gageId; // 核心担保品编号
    private String gageType; // 抵质押类型
    private String gageTypeName; // 抵质押类型名称
    private BigDecimal maxAmt; // 权利价值
    private String gageUser; // 抵押人
    private String acctBrch; // 账务机构
    private String acctBrchName; // 账务机构名称
    private String fetchUser; // 取件人
    private String fetchTime; // 取件时间
    private String fetchCardno; // 取件人证件号
    private String backtime; // 归还时间
    private String isMortgage; // 是否住房按揭
    private String locale; // 库位
    private String boxid; // 档案盒号
    private String pawn; // 抵押物名称
    private String isEstate; // 是否张家港地区不动产
    private String isElectronic; // 是否电子类押品
    private String remark; // 备注描述

    public String getGageId() {
        return gageId;
    }

    public void setGageId(String gageId) {
        this.gageId = gageId;
    }

    public String getGageType() {
        return gageType;
    }

    public void setGageType(String gageType) {
        this.gageType = gageType;
    }

    public String getGageTypeName() {
        return gageTypeName;
    }

    public void setGageTypeName(String gageTypeName) {
        this.gageTypeName = gageTypeName;
    }

    public BigDecimal getMaxAmt() {
        return maxAmt;
    }

    public void setMaxAmt(BigDecimal maxAmt) {
        this.maxAmt = maxAmt;
    }

    public String getGageUser() {
        return gageUser;
    }

    public void setGageUser(String gageUser) {
        this.gageUser = gageUser;
    }

    public String getAcctBrch() {
        return acctBrch;
    }

    public void setAcctBrch(String acctBrch) {
        this.acctBrch = acctBrch;
    }

    public String getAcctBrchName() {
        return acctBrchName;
    }

    public void setAcctBrchName(String acctBrchName) {
        this.acctBrchName = acctBrchName;
    }

    public String getFetchUser() {
        return fetchUser;
    }

    public void setFetchUser(String fetchUser) {
        this.fetchUser = fetchUser;
    }

    public String getFetchTime() {
        return fetchTime;
    }

    public void setFetchTime(String fetchTime) {
        this.fetchTime = fetchTime;
    }

    public String getFetchCardno() {
        return fetchCardno;
    }

    public void setFetchCardno(String fetchCardno) {
        this.fetchCardno = fetchCardno;
    }

    public String getBacktime() {
        return backtime;
    }

    public void setBacktime(String backtime) {
        this.backtime = backtime;
    }

    public String getIsMortgage() {
        return isMortgage;
    }

    public void setIsMortgage(String isMortgage) {
        this.isMortgage = isMortgage;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getBoxid() {
        return boxid;
    }

    public void setBoxid(String boxid) {
        this.boxid = boxid;
    }

    public String getPawn() {
        return pawn;
    }

    public void setPawn(String pawn) {
        this.pawn = pawn;
    }

    public String getIsEstate() {
        return isEstate;
    }

    public void setIsEstate(String isEstate) {
        this.isEstate = isEstate;
    }

    public String getIsElectronic() {
        return isElectronic;
    }

    public void setIsElectronic(String isElectronic) {
        this.isElectronic = isElectronic;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "ListArrayInfo{" +
                "gageId='" + gageId + '\'' +
                ", gageType='" + gageType + '\'' +
                ", gageTypeName='" + gageTypeName + '\'' +
                ", maxAmt=" + maxAmt +
                ", gageUser='" + gageUser + '\'' +
                ", acctBrch='" + acctBrch + '\'' +
                ", acctBrchName='" + acctBrchName + '\'' +
                ", fetchUser='" + fetchUser + '\'' +
                ", fetchTime='" + fetchTime + '\'' +
                ", fetchCardno='" + fetchCardno + '\'' +
                ", backtime='" + backtime + '\'' +
                ", isMortgage='" + isMortgage + '\'' +
                ", locale='" + locale + '\'' +
                ", boxid='" + boxid + '\'' +
                ", pawn='" + pawn + '\'' +
                ", isEstate='" + isEstate + '\'' +
                ", isElectronic='" + isElectronic + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
