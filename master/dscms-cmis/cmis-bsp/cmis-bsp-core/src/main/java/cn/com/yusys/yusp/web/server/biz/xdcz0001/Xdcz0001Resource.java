package cn.com.yusys.yusp.web.server.biz.xdcz0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0001.req.Xdcz0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0001.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0001.resp.Xdcz0001RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0001.req.Xdcz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0001.resp.Xdcz0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:电子保函开立
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0001:电子保函开立")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0001Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0001
     * 交易描述：电子保函开立
     *
     * @param xdcz0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("电子保函开立")
    @PostMapping("/xdcz0001")
 //  @Idempotent({"xdcz0001", "#xdcz0001ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0001RespDto xdcz0001(@Validated @RequestBody Xdcz0001ReqDto xdcz0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001ReqDto));
        Xdcz0001DataReqDto xdcz0001DataReqDto = new Xdcz0001DataReqDto();// 请求Data： 电子保函开立
        Xdcz0001DataRespDto xdcz0001DataRespDto = new Xdcz0001DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0001.req.Data reqData = null; // 请求Data：电子保函开立
        cn.com.yusys.yusp.dto.server.biz.xdcz0001.resp.Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0001RespDto xdcz0001RespDto = new Xdcz0001RespDto();
        try {
            // 从 xdcz0001ReqDto获取 reqData
            reqData = xdcz0001ReqDto.getData();
            // 将 reqData 拷贝到xdcz0001DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001DataReqDto));
            ResultDto<Xdcz0001DataRespDto> xdcz0001DataResultDto = dscmsBizCzClientService.xdcz0001(xdcz0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0001RespDto.setErorcd(Optional.ofNullable(xdcz0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0001RespDto.setErortx(Optional.ofNullable(xdcz0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0001DataResultDto.getCode())) {
                xdcz0001DataRespDto = xdcz0001DataResultDto.getData();
                xdcz0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, e.getMessage());
            xdcz0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0001RespDto.setDatasq(servsq);

        xdcz0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0001.key, DscmsEnum.TRADE_CODE_XDCZ0001.value, JSON.toJSONString(xdcz0001RespDto));
        return xdcz0001RespDto;
    }
}

