package cn.com.yusys.yusp.online.client.esb.core.ln3041.resp;

import java.math.BigDecimal;

/**
 * 响应Service：支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还；
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String chanpmch;//产品名称
    private BigDecimal jiejuuje;//借据金额
    private String huankfsh;//还款方式
    private BigDecimal guihzcbj;//归还正常本金
    private BigDecimal guihyqbj;//归还逾期本金
    private BigDecimal guihdzbj;//归还呆滞本金
    private BigDecimal ghdzhabj;//归还呆账本金
    private BigDecimal ghysyjlx;//归还应收应计利息
    private BigDecimal ghcsyjlx;//归还催收应计利息
    private BigDecimal ghynshqx;//归还应收欠息
    private BigDecimal ghcushqx;//归还催收欠息
    private BigDecimal ghysyjfx;//归还应收应计罚息
    private BigDecimal ghcsyjfx;//归还催收应计罚息
    private BigDecimal ghynshfx;//归还应收罚息
    private BigDecimal ghcushfx;//归还催收罚息
    private BigDecimal ghyjfuxi;//归还应计复息
    private BigDecimal ghfxfuxi;//归还复息
    private BigDecimal ghfeiyin;//归还费用
    private BigDecimal ghfajinn;//归还罚金
    private BigDecimal guihzone;//归还总额
    private String xcsdxzxh;//新产生待销账序号
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String wtrmingc;//委托人名称
    private String bjghrzzh;//本金归还入账账号
    private String lxghrzzh;//利息归还入账账号
    private String kaihriqi;//开户日期
    private BigDecimal zhchlilv;//正常利率
    private BigDecimal yuqililv;//逾期利率
    private String dkrzhzhh;//贷款入账账号
    private String zhanghmc;//账户户名

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getGuihzcbj() {
        return guihzcbj;
    }

    public void setGuihzcbj(BigDecimal guihzcbj) {
        this.guihzcbj = guihzcbj;
    }

    public BigDecimal getGuihyqbj() {
        return guihyqbj;
    }

    public void setGuihyqbj(BigDecimal guihyqbj) {
        this.guihyqbj = guihyqbj;
    }

    public BigDecimal getGuihdzbj() {
        return guihdzbj;
    }

    public void setGuihdzbj(BigDecimal guihdzbj) {
        this.guihdzbj = guihdzbj;
    }

    public BigDecimal getGhdzhabj() {
        return ghdzhabj;
    }

    public void setGhdzhabj(BigDecimal ghdzhabj) {
        this.ghdzhabj = ghdzhabj;
    }

    public BigDecimal getGhysyjlx() {
        return ghysyjlx;
    }

    public void setGhysyjlx(BigDecimal ghysyjlx) {
        this.ghysyjlx = ghysyjlx;
    }

    public BigDecimal getGhcsyjlx() {
        return ghcsyjlx;
    }

    public void setGhcsyjlx(BigDecimal ghcsyjlx) {
        this.ghcsyjlx = ghcsyjlx;
    }

    public BigDecimal getGhynshqx() {
        return ghynshqx;
    }

    public void setGhynshqx(BigDecimal ghynshqx) {
        this.ghynshqx = ghynshqx;
    }

    public BigDecimal getGhcushqx() {
        return ghcushqx;
    }

    public void setGhcushqx(BigDecimal ghcushqx) {
        this.ghcushqx = ghcushqx;
    }

    public BigDecimal getGhysyjfx() {
        return ghysyjfx;
    }

    public void setGhysyjfx(BigDecimal ghysyjfx) {
        this.ghysyjfx = ghysyjfx;
    }

    public BigDecimal getGhcsyjfx() {
        return ghcsyjfx;
    }

    public void setGhcsyjfx(BigDecimal ghcsyjfx) {
        this.ghcsyjfx = ghcsyjfx;
    }

    public BigDecimal getGhynshfx() {
        return ghynshfx;
    }

    public void setGhynshfx(BigDecimal ghynshfx) {
        this.ghynshfx = ghynshfx;
    }

    public BigDecimal getGhcushfx() {
        return ghcushfx;
    }

    public void setGhcushfx(BigDecimal ghcushfx) {
        this.ghcushfx = ghcushfx;
    }

    public BigDecimal getGhyjfuxi() {
        return ghyjfuxi;
    }

    public void setGhyjfuxi(BigDecimal ghyjfuxi) {
        this.ghyjfuxi = ghyjfuxi;
    }

    public BigDecimal getGhfxfuxi() {
        return ghfxfuxi;
    }

    public void setGhfxfuxi(BigDecimal ghfxfuxi) {
        this.ghfxfuxi = ghfxfuxi;
    }

    public BigDecimal getGhfeiyin() {
        return ghfeiyin;
    }

    public void setGhfeiyin(BigDecimal ghfeiyin) {
        this.ghfeiyin = ghfeiyin;
    }

    public BigDecimal getGhfajinn() {
        return ghfajinn;
    }

    public void setGhfajinn(BigDecimal ghfajinn) {
        this.ghfajinn = ghfajinn;
    }

    public BigDecimal getGuihzone() {
        return guihzone;
    }

    public void setGuihzone(BigDecimal guihzone) {
        this.guihzone = guihzone;
    }

    public String getXcsdxzxh() {
        return xcsdxzxh;
    }

    public void setXcsdxzxh(String xcsdxzxh) {
        this.xcsdxzxh = xcsdxzxh;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getZhanghmc() {
        return zhanghmc;
    }

    public void setZhanghmc(String zhanghmc) {
        this.zhanghmc = zhanghmc;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", huankfsh='" + huankfsh + '\'' +
                ", guihzcbj=" + guihzcbj +
                ", guihyqbj=" + guihyqbj +
                ", guihdzbj=" + guihdzbj +
                ", ghdzhabj=" + ghdzhabj +
                ", ghysyjlx=" + ghysyjlx +
                ", ghcsyjlx=" + ghcsyjlx +
                ", ghynshqx=" + ghynshqx +
                ", ghcushqx=" + ghcushqx +
                ", ghysyjfx=" + ghysyjfx +
                ", ghcsyjfx=" + ghcsyjfx +
                ", ghynshfx=" + ghynshfx +
                ", ghcushfx=" + ghcushfx +
                ", ghyjfuxi=" + ghyjfuxi +
                ", ghfxfuxi=" + ghfxfuxi +
                ", ghfeiyin=" + ghfeiyin +
                ", ghfajinn=" + ghfajinn +
                ", guihzone=" + guihzone +
                ", xcsdxzxh='" + xcsdxzxh + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", wtrmingc='" + wtrmingc + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", yuqililv=" + yuqililv +
                ", dkrzhzhh='" + dkrzhzhh + '\'' +
                ", zhanghmc='" + zhanghmc + '\'' +
                '}';
    }
}