package cn.com.yusys.yusp.online.client.esb.core.ln3026.resp;

/**
 * 响应Service：银团贷款开户
 * @author lihh
 * @version 1.0             
 */      
public class Ln3026RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
