package cn.com.yusys.yusp.web.client.esb.rircp.fbxd15;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15.Fbxd15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15.Fbxd15RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.req.Fbxd15ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.Fbxd15RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd15）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd15Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd15Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd15
     * 交易描述：还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和
     *
     * @param fbxd15ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd15")
    protected @ResponseBody
    ResultDto<Fbxd15RespDto> fbxd15(@Validated @RequestBody Fbxd15ReqDto fbxd15ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd15ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.Service();
        Fbxd15ReqService fbxd15ReqService = new Fbxd15ReqService();
        Fbxd15RespService fbxd15RespService = new Fbxd15RespService();
        Fbxd15RespDto fbxd15RespDto = new Fbxd15RespDto();
        ResultDto<Fbxd15RespDto> fbxd15ResultDto = new ResultDto<Fbxd15RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd15ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd15ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD15.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            fbxd15ReqService.setService(reqService);
            // 将fbxd15ReqService转换成fbxd15ReqServiceMap
            Map fbxd15ReqServiceMap = beanMapUtil.beanToMap(fbxd15ReqService);
            context.put("tradeDataMap", fbxd15ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD10.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd15RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd15RespService.class, Fbxd15RespService.class);
            respService = fbxd15RespService.getService();

            fbxd15ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd15ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd15RespDto
                BeanUtils.copyProperties(respService, fbxd15RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.List fbxd15RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.List());
                respService.setList(fbxd15RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> fbxd15RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15.List> fbxd15RespDtoLists = fbxd15RespRecordList.parallelStream().map(fbxd15RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15.List fbxd15RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15.List();
                        BeanUtils.copyProperties(fbxd15RespRecord, fbxd15RespDtoList);
                        return fbxd15RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd15RespDto.setList(fbxd15RespDtoLists);
                }
                fbxd15ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd15ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd15ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd15ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, e.getMessage());
            fbxd15ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd15ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd15ResultDto.setData(fbxd15RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd15ResultDto));
        return fbxd15ResultDto;
    }
}
