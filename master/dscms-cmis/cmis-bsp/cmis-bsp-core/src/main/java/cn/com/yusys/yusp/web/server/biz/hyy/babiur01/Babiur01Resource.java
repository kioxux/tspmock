package cn.com.yusys.yusp.web.server.biz.hyy.babiur01;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.CmisCommonConstants;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.hyy.babiur01.Babiur01ReqDto;
import cn.com.yusys.yusp.dto.server.biz.hyy.babiur01.Babiur01RespDto;
import cn.com.yusys.yusp.dto.server.biz.hyy.babiur01.Data;
import cn.com.yusys.yusp.dto.server.xddb0015.req.Xddb0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0015.resp.Xddb0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 接口处理类:押品状态变更推送
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "BA-BI-UR01:押品状态变更推送")
@RestController
@RequestMapping("/bank/collaterals")
public class Babiur01Resource {
    private static final Logger logger = LoggerFactory.getLogger(Babiur01Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：BA-BI-UR01a
     * 交易描述：押品状态变更推送
     *
     * @param district_code,certificate_number,status
     * @return
     * @throws Exception
     */
    @ApiOperation("押品状态变更推送")
    @RequestMapping("/real-estate-info")
    protected @ResponseBody
    String babiur01(String district_code, String certificate_number, String status) throws Exception {

        Babiur01ReqDto babiur01ReqDto = new Babiur01ReqDto();
        babiur01ReqDto.setDistrict_code(district_code);
        babiur01ReqDto.setCertificate_number(certificate_number);
        babiur01ReqDto.setStatus(status);
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIUR.key, DscmsEnum.TRADE_CODE_BABIUR.value, JSON.toJSONString(babiur01ReqDto));
        Xddb0015DataReqDto xddb0015DataReqDto = new Xddb0015DataReqDto();// 请求Data： 押品状态变更推送
        Xddb0015DataRespDto xddb0015DataRespDto = new Xddb0015DataRespDto();// 响应Data：押品状态变更推送
        //Xddb0015RespDto xddb0015RespDto=new Xddb0015RespDto();
        //cn.com.yusys.yusp.dto.server.biz.xddb0015.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xddb0015.req.Data(); // 请求Data：押品状态变更推送
        //cn.com.yusys.yusp.dto.server.biz.xddb0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0015.resp.Data();// 响应Data：押品状态变更推送
        Babiur01RespDto babiur01RespDto = new Babiur01RespDto();
        Data respData = new Data();
        List<Boolean> booleans = new ArrayList<>();
        try {
            // 从 xddb0015ReqDto获取 reqData
            xddb0015DataReqDto.setDistco(babiur01ReqDto.getDistrict_code());
            xddb0015DataReqDto.setCertnu(babiur01ReqDto.getCertificate_number());
            xddb0015DataReqDto.setStatus(babiur01ReqDto.getStatus());
            // 将 reqData 拷贝到xddb0015DataReqDto
            //BeanUtils.copyProperties(reqData, xddb0015DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIUR.key, DscmsEnum.TRADE_CODE_BABIUR.value, JSON.toJSONString(xddb0015DataReqDto));
            ResultDto<Xddb0015DataRespDto> xddb0015DataResultDto = dscmsBizDbClientService.xddb0015(xddb0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIUR.key, DscmsEnum.TRADE_CODE_BABIUR.value, JSON.toJSONString(xddb0015DataRespDto));
            // 从返回值中获取响应码和响应消息
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0015DataResultDto.getCode())) {
                xddb0015DataRespDto = xddb0015DataResultDto.getData();
                if(Objects.equals(xddb0015DataRespDto.getOpFlag(), CmisCommonConstants.OP_FLAG_S)){
                    booleans.add(true);
                    babiur01RespDto.setCode(SuccessEnum.HYY_SUCCESS.key);
                    babiur01RespDto.setMessage(SuccessEnum.HYY_SUCCESS.value);
                    babiur01RespDto.setSuccess(true);
                }else{
                    booleans.add(false);
                    babiur01RespDto.setCode(EpbEnum.EPBHYY9999.key);
                    babiur01RespDto.setMessage(xddb0015DataRespDto.getOpMsg());
                    babiur01RespDto.setSuccess(false);
                }
                respData.setItems(booleans);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIUR.key, DscmsEnum.TRADE_CODE_BABIUR.value, e.getMessage());
            babiur01RespDto.setCode(EpbEnum.EPBHYY9999.key); // 9999
            babiur01RespDto.setCode(EpbEnum.EPBHYY9999.value);// 系统异常
            babiur01RespDto.setSuccess(false);
        }
        babiur01RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIUR.key, DscmsEnum.TRADE_CODE_BABIUR.value, JSON.toJSONString(babiur01RespDto));
        return JSON.toJSONString(babiur01RespDto);
    }
}

