package cn.com.yusys.yusp.online.client.esb.core.da3303.resp;

/**
 * 响应Service：抵债资产信息维护
 * @author leehuang
 * @version 1.0             
 */      
public class Da3303RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }

	@Override
	public String toString() {
		return "Da3303RespService{" +
				"service=" + service +
				'}';
	}
}
