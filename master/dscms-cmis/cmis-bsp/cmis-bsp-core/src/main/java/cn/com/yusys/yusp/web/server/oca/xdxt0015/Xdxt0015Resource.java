package cn.com.yusys.yusp.web.server.oca.xdxt0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.oca.xdxt0015.req.Xdxt0015ReqDto;
import cn.com.yusys.yusp.dto.server.oca.xdxt0015.resp.Xdxt0015RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.req.Xdxt0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0015.resp.Xdxt0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:用户机构角色信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0015:用户机构角色信息列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0015Resource.class);
    @Autowired
    private DscmsXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0015
     * 交易描述：用户机构角色信息列表查询
     *
     * @param xdxt0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("用户机构角色信息列表查询")
    @PostMapping("/xdxt0015")
    //@Idempotent({"xdcaxt0015", "#xdxt0015ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0015RespDto xdxt0015(@Validated @RequestBody Xdxt0015ReqDto xdxt0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0015.key, DscmsEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0015ReqDto));
        Xdxt0015DataReqDto xdxt0015DataReqDto = new Xdxt0015DataReqDto();// 请求Data： 用户机构角色信息列表查询
        Xdxt0015DataRespDto xdxt0015DataRespDto = new Xdxt0015DataRespDto();// 响应Data：用户机构角色信息列表查询
        Xdxt0015RespDto xdxt0015RespDto = new Xdxt0015RespDto();
        cn.com.yusys.yusp.dto.server.oca.xdxt0015.req.Data reqData = null; // 请求Data：用户机构角色信息列表查询
        cn.com.yusys.yusp.dto.server.oca.xdxt0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.oca.xdxt0015.resp.Data();// 响应Data：用户机构角色信息列表查询

        try {
            // 从 xdxt0015ReqDto获取 reqData
            reqData = xdxt0015ReqDto.getData();
            // 将 reqData 拷贝到xdxt0015DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0015DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0015.key, DscmsEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0015DataReqDto));
            ResultDto<Xdxt0015DataRespDto> xdxt0015DataResultDto = dscmsXtClientService.xdxt0015(xdxt0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0015.key, DscmsEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0015DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0015RespDto.setErorcd(Optional.ofNullable(xdxt0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0015RespDto.setErortx(Optional.ofNullable(xdxt0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0015DataResultDto.getCode())) {
                xdxt0015DataRespDto = xdxt0015DataResultDto.getData();
                xdxt0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0015.key, DscmsEnum.TRADE_CODE_XDXT0015.value, e.getMessage());
            xdxt0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0015RespDto.setDatasq(servsq);

        xdxt0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0015.key, DscmsEnum.TRADE_CODE_XDXT0015.value, JSON.toJSONString(xdxt0015RespDto));
        return xdxt0015RespDto;
    }
}
