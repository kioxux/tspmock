package cn.com.yusys.yusp.online.client.esb.core.da3303.req;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产信息维护
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String daikczbz;//业务操作标志
    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String yngyjigo;//营业机构
    private String kehuhaoo;//客户号
    private String kehuzwmc;//客户名
    private String dzwzleib;//抵债物资类别
    private String cqzmzlei;//产权证明种类
    private BigDecimal daizdzzc;//待转抵债资产
    private BigDecimal dcldzzic;//待处理抵债资产
    private BigDecimal dbxdzzic;//待变现抵债资产
    private BigDecimal hfeijine;//还费金额
    private BigDecimal huanbjee;//还本金额
    private BigDecimal hxijinee;//还息金额
    private BigDecimal hqitjine;//还其它金额
    private cn.com.yusys.yusp.online.client.esb.core.da3303.req.Lstweihsr_ARRAY lstweihsr_ARRAY;//抵债资产维护输入

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getDzwzleib() {
        return dzwzleib;
    }

    public void setDzwzleib(String dzwzleib) {
        this.dzwzleib = dzwzleib;
    }

    public String getCqzmzlei() {
        return cqzmzlei;
    }

    public void setCqzmzlei(String cqzmzlei) {
        this.cqzmzlei = cqzmzlei;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public BigDecimal getHqitjine() {
        return hqitjine;
    }

    public void setHqitjine(BigDecimal hqitjine) {
        this.hqitjine = hqitjine;
    }

    public Lstweihsr_ARRAY getLstweihsr_ARRAY() {
        return lstweihsr_ARRAY;
    }

    public void setLstweihsr_ARRAY(Lstweihsr_ARRAY lstweihsr_ARRAY) {
        this.lstweihsr_ARRAY = lstweihsr_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", dzwzleib='" + dzwzleib + '\'' +
                ", cqzmzlei='" + cqzmzlei + '\'' +
                ", daizdzzc=" + daizdzzc +
                ", dcldzzic=" + dcldzzic +
                ", dbxdzzic=" + dbxdzzic +
                ", hfeijine=" + hfeijine +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", hqitjine=" + hqitjine +
                ", lstweihsr_ARRAY=" + lstweihsr_ARRAY +
                '}';
    }
}
