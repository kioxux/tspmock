package cn.com.yusys.yusp.web.client.esb.ecif.g00203;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00203.G00203ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00203.G00203RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00203.RelArrayInfo;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g00203.req.G00203ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g00203.req.Record;
import cn.com.yusys.yusp.online.client.esb.ecif.g00203.resp.G00203RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g00203)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G00203Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G00203Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 同业客户维护
     *
     * @param g00203ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g00203:同业客户维护")
    @PostMapping("/g00203")
    protected @ResponseBody
    ResultDto<G00203RespDto> g00203(@Validated @RequestBody G00203ReqDto g00203ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00203.key, EsbEnum.TRADE_CODE_G00203.value, JSON.toJSONString(g00203ReqDto));

        cn.com.yusys.yusp.online.client.esb.ecif.g00203.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g00203.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g00203.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g00203.resp.Service();
        G00203ReqService g00203ReqService = new G00203ReqService();
        G00203RespService g00203RespService = new G00203RespService();
        G00203RespDto g00203RespDto = new G00203RespDto();
        ResultDto<G00203RespDto> g00203ResultDto = new ResultDto<G00203RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {  //  将G00203ReqDto转换成reqService
            BeanUtils.copyProperties(g00203ReqDto, reqService);
            List<RelArrayInfo> relArrayInfos = Optional.ofNullable(g00203ReqDto.getRelArrayInfo()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(relArrayInfos)) {
                List<Record> collect = relArrayInfos.parallelStream().map(info -> {
                    Record record = new Record();
                    BeanUtils.copyProperties(info, record);
                    return record;
                }).collect(Collectors.toList());
                cn.com.yusys.yusp.online.client.esb.ecif.g00203.req.RelArrayInfo relArrayInfo = new cn.com.yusys.yusp.online.client.esb.ecif.g00203.req.RelArrayInfo();
                relArrayInfo.setRecord(collect);
                reqService.setRelArrayInfo(relArrayInfo);
            }


            reqService.setPrcscd(EsbEnum.TRADE_CODE_G00203.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            g00203ReqService.setService(reqService);
            // 将g00203ReqService转换成g00203ReqServiceMap
            Map g00203ReqServiceMap = beanMapUtil.beanToMap(g00203ReqService);
            context.put("tradeDataMap", g00203ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00203.key, EsbEnum.TRADE_CODE_G00203.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G00203.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00203.key, EsbEnum.TRADE_CODE_G00203.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g00203RespService = beanMapUtil.mapToBean(tradeDataMap, G00203RespService.class, G00203RespService.class);
            respService = g00203RespService.getService();

            //  将G00203RespDto封装到ResultDto<G00203RespDto>
            g00203ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g00203ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G00203RespDto
                BeanUtils.copyProperties(respService, g00203RespDto);
                g00203ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g00203ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                g00203ResultDto.setCode(EpbEnum.EPB099999.key);
                g00203ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00203.key, EsbEnum.TRADE_CODE_G00203.value, e.getMessage());
            g00203ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g00203ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g00203ResultDto.setData(g00203RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00203.key, EsbEnum.TRADE_CODE_G00203.value, JSON.toJSONString(g00203ResultDto));
        return g00203ResultDto;
    }
}
