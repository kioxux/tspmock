package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询基本信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String guar_cus_name;//所有权人名称
    private String ship_status;//权属状况
    private BigDecimal eval_amt;//我行确认价值
    private String eval_date;//我行确认日期
    private String common_assets_ind;//是否共有财产
    private String guar_type_cd;//担保分类代码
    private String guar_sub_no;//押品详细编号
    private BigDecimal eval_in_amt;//评估价值
    private String guar_type_cd_cnname;//担保分类名称

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuar_cus_name() {
        return guar_cus_name;
    }

    public void setGuar_cus_name(String guar_cus_name) {
        this.guar_cus_name = guar_cus_name;
    }

    public String getShip_status() {
        return ship_status;
    }

    public void setShip_status(String ship_status) {
        this.ship_status = ship_status;
    }

    public BigDecimal getEval_amt() {
        return eval_amt;
    }

    public void setEval_amt(BigDecimal eval_amt) {
        this.eval_amt = eval_amt;
    }

    public String getEval_date() {
        return eval_date;
    }

    public void setEval_date(String eval_date) {
        this.eval_date = eval_date;
    }

    public String getCommon_assets_ind() {
        return common_assets_ind;
    }

    public void setCommon_assets_ind(String common_assets_ind) {
        this.common_assets_ind = common_assets_ind;
    }

    public String getGuar_type_cd() {
        return guar_type_cd;
    }

    public void setGuar_type_cd(String guar_type_cd) {
        this.guar_type_cd = guar_type_cd;
    }

    public String getGuar_sub_no() {
        return guar_sub_no;
    }

    public void setGuar_sub_no(String guar_sub_no) {
        this.guar_sub_no = guar_sub_no;
    }

    public BigDecimal getEval_in_amt() {
        return eval_in_amt;
    }

    public void setEval_in_amt(BigDecimal eval_in_amt) {
        this.eval_in_amt = eval_in_amt;
    }

    public String getGuar_type_cd_cnname() {
        return guar_type_cd_cnname;
    }

    public void setGuar_type_cd_cnname(String guar_type_cd_cnname) {
        this.guar_type_cd_cnname = guar_type_cd_cnname;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", guar_cus_name='" + guar_cus_name + '\'' +
                ", ship_status='" + ship_status + '\'' +
                ", eval_amt=" + eval_amt +
                ", eval_date='" + eval_date + '\'' +
                ", common_assets_ind='" + common_assets_ind + '\'' +
                ", guar_type_cd='" + guar_type_cd + '\'' +
                ", guar_sub_no='" + guar_sub_no + '\'' +
                ", eval_in_amt=" + eval_in_amt +
                ", guar_type_cd_cnname='" + guar_type_cd_cnname + '\'' +
                '}';
    }
}
