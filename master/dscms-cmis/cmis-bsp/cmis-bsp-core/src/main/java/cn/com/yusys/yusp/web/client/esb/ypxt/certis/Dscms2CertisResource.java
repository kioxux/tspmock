package cn.com.yusys.yusp.web.client.esb.ypxt.certis;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.CertisReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.Record;
import cn.com.yusys.yusp.online.client.esb.ypxt.certis.resp.CertisRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(certis)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2CertisResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2CertisResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 权证状态同步接口（处理码certis）
     *
     * @param certisReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("certis:权证状态同步接口")
    @PostMapping("/certis")
    protected @ResponseBody
    ResultDto<CertisRespDto> certis(@Validated @RequestBody CertisReqDto certisReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CERTIS.key, EsbEnum.TRADE_CODE_CERTIS.value, JSON.toJSONString(certisReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.certis.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.certis.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.List list = new cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.List();
        CertisReqService certisReqService = new CertisReqService();
        CertisRespService certisRespService = new CertisRespService();
        CertisRespDto certisRespDto = new CertisRespDto();
        ResultDto<CertisRespDto> certisResultDto = new ResultDto<CertisRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            BeanUtils.copyProperties(certisReqDto, reqService);
            if (CollectionUtils.nonEmpty(certisReqDto.getList())) {
                //  将CertisReqDto转换成reqService
                List<CertisListInfo> certisListInfos = certisReqDto.getList();
                List<Record> recordList = new ArrayList<Record>();
                for (CertisListInfo certisListInfo : certisListInfos) {
                    cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.certis.req.Record();
                    BeanUtils.copyProperties(certisListInfo, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CERTIS.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            certisReqService.setService(reqService);
            // 将certisReqService转换成certisReqServiceMap
            Map certisReqServiceMap = beanMapUtil.beanToMap(certisReqService);
            context.put("tradeDataMap", certisReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CERTIS.key, EsbEnum.TRADE_CODE_CERTIS.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CERTIS.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CERTIS.key, EsbEnum.TRADE_CODE_CERTIS.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            certisRespService = beanMapUtil.mapToBean(tradeDataMap, CertisRespService.class, CertisRespService.class);
            respService = certisRespService.getService();

            //  将CertisRespDto封装到ResultDto<CertisRespDto>
            certisResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            certisResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成CertisRespDto
                BeanUtils.copyProperties(respService, certisRespDto);
                certisResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                certisResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                certisResultDto.setCode(EpbEnum.EPB099999.key);
                certisResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CERTIS.key, EsbEnum.TRADE_CODE_CERTIS.value, e.getMessage());
            certisResultDto.setCode(EpbEnum.EPB099999.key);//9999
            certisResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        certisResultDto.setData(certisRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CERTIS.key, EsbEnum.TRADE_CODE_CERTIS.value, JSON.toJSONString(certisResultDto));
        return certisResultDto;
    }
}
