package cn.com.yusys.yusp.web.server.biz.xddh0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0006.req.Xddh0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0006.resp.Xddh0006RespDto;
import cn.com.yusys.yusp.dto.server.xddh0006.req.Xddh0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0006.resp.Xddh0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提前还款试算查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0006:提前还款试算查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0006Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0006
     * 交易描述：提前还款试算查询
     *
     * @param xddh0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提前还款试算查询")
    @PostMapping("/xddh0006")
//@Idempotent({"xddh0006", "#xddh0006ReqDto.datasq"})
    protected @ResponseBody
    Xddh0006RespDto xddh0006(@Validated @RequestBody Xddh0006ReqDto xddh0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, JSON.toJSONString(xddh0006ReqDto));
        Xddh0006DataReqDto xddh0006DataReqDto = new Xddh0006DataReqDto();// 请求Data： 提前还款试算查询
        Xddh0006DataRespDto xddh0006DataRespDto = new Xddh0006DataRespDto();// 响应Data：提前还款试算查询
        Xddh0006RespDto xddh0006RespDto = new Xddh0006RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddh0006.req.Data reqData = null; // 请求Data：提前还款试算查询
        cn.com.yusys.yusp.dto.server.biz.xddh0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0006.resp.Data();// 响应Data：提前还款试算查询

        try {
            // 从 xddh0006ReqDto获取 reqData
            reqData = xddh0006ReqDto.getData();
            // 将 reqData 拷贝到xddh0006DataReqDto
            BeanUtils.copyProperties(reqData, xddh0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, JSON.toJSONString(xddh0006DataReqDto));
            ResultDto<Xddh0006DataRespDto> xddh0006DataResultDto = dscmsBizDhClientService.xddh0006(xddh0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, JSON.toJSONString(xddh0006DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0006RespDto.setErorcd(Optional.ofNullable(xddh0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0006RespDto.setErortx(Optional.ofNullable(xddh0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0006DataResultDto.getCode())) {
                xddh0006DataRespDto = xddh0006DataResultDto.getData();
                xddh0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, e.getMessage());
            xddh0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0006RespDto.setDatasq(servsq);

        xddh0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0006.key, DscmsEnum.TRADE_CODE_XDDH0006.value, JSON.toJSONString(xddh0006RespDto));
        return xddh0006RespDto;
    }
}
