package cn.com.yusys.yusp.web.server.biz.xdxw0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0002.req.Xdxw0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0002.resp.Xdxw0002RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0002.req.Xdxw0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0002.resp.Xdxw0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小微贷前调查信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDXW0002:小微贷前调查信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0002Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0002
     * 交易描述：小微贷前调查信息查询
     *
     * @param xdxw0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微贷前调查信息查询")
    @PostMapping("/xdxw0002")
    //@Idempotent({"xdcaxw0002", "#xdxw0002ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0002RespDto xdxw0002(@Validated @RequestBody Xdxw0002ReqDto xdxw0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0002.key, DscmsEnum.TRADE_CODE_XDXW0002.value, JSON.toJSONString(xdxw0002ReqDto));
        Xdxw0002DataReqDto xdxw0002DataReqDto = new Xdxw0002DataReqDto();// 请求Data： 小微贷前调查信息查询
        Xdxw0002DataRespDto xdxw0002DataRespDto = new Xdxw0002DataRespDto();// 响应Data：小微贷前调查信息查询
        Xdxw0002RespDto xdxw0002RespDto = new Xdxw0002RespDto();// 响应Dto：小微贷前调查信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0002.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdxw0002.req.Data(); // 请求Data：小微贷前调查信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0002.resp.Data();// 响应Data：小微贷前调查信息查询
        try {
            // 从 xdxw0002ReqDto获取 reqData
            reqData = xdxw0002ReqDto.getData();
            // 将 reqData 拷贝到xdxw0002DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0002DataReqDto);
            // 调用服务：
            ResultDto<Xdxw0002DataRespDto> xdxw0002DataResultDto = dscmsBizXwClientService.xdxw0002(xdxw0002DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdxw0002RespDto.setErorcd(Optional.ofNullable(xdxw0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0002RespDto.setErortx(Optional.ofNullable(xdxw0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdxw0002RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0002DataResultDto.getCode())) {
                xdxw0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0002DataRespDto = xdxw0002DataResultDto.getData();
                // 将 xdkh0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0002.key, DscmsEnum.TRADE_CODE_XDXW0002.value, e.getMessage());
            xdxw0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0002RespDto.setDatasq(servsq);
        xdxw0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0002.key, DscmsEnum.TRADE_CODE_XDXW0002.value, JSON.toJSONString(xdxw0002RespDto));
        return xdxw0002RespDto;
    }
}
