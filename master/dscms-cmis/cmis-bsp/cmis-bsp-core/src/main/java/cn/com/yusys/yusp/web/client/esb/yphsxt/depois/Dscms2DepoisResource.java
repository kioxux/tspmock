package cn.com.yusys.yusp.web.client.esb.yphsxt.depois;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.req.DepoisReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.resp.DepoisRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.depois.req.DepoisReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.depois.req.Record;
import cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp.DepoisRespService;
import com.alibaba.fastjson.JSON;
import org.checkerframework.checker.units.qual.C;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:查询抵押物信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2DepoisResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2DepoisResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：depois
     * 交易描述：查询抵押物信息
     *
     * @param depoisReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/depois")
    protected @ResponseBody
    ResultDto<DepoisRespDto> depois(@Validated @RequestBody DepoisReqDto depoisReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEPOIS.key, EsbEnum.TRADE_CODE_DEPOIS.value, JSON.toJSONString(depoisReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.depois.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.depois.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp.Service();
        DepoisReqService depoisReqService = new DepoisReqService();
        DepoisRespService depoisRespService = new DepoisRespService();
        DepoisRespDto depoisRespDto = new DepoisRespDto();
        ResultDto<DepoisRespDto> depoisResultDto = new ResultDto<DepoisRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将depoisReqDto转换成reqService
            BeanUtils.copyProperties(depoisReqDto, reqService);
            if (CollectionUtils.nonEmpty(depoisReqDto.getList())) {
                cn.com.yusys.yusp.online.client.esb.yphsxt.depois.req.List ServiceList = new cn.com.yusys.yusp.online.client.esb.yphsxt.depois.req.List();
                List<cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.req.List> reqList = Optional.ofNullable(depoisReqDto.getList()).orElse(new ArrayList<>());
                List<Record> reqRecordList = reqList.parallelStream().map(e -> {
                    Record reqRecord = new Record();
                    BeanUtils.copyProperties(e, reqRecord);
                    return reqRecord;
                }).collect(Collectors.toList());
                ServiceList.setRecord(reqRecordList);
                reqService.setList(ServiceList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DEPOIS.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            depoisReqService.setService(reqService);
            // 将depoisReqService转换成depoisReqServiceMap
            Map depoisReqServiceMap = beanMapUtil.beanToMap(depoisReqService);
            context.put("tradeDataMap", depoisReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEPOIS.key, EsbEnum.TRADE_CODE_DEPOIS.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DEPOIS.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEPOIS.key, EsbEnum.TRADE_CODE_DEPOIS.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            depoisRespService = beanMapUtil.mapToBean(tradeDataMap, DepoisRespService.class, DepoisRespService.class);
            respService = depoisRespService.getService();
            //  将respService转换成DepoisRespDto
            depoisResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            depoisResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, depoisRespDto);
                cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp.List respList = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp.List());
                if (CollectionUtils.nonEmpty(respList.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.yphsxt.depois.resp.Record> respOriginList = Optional.ofNullable(respList.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.resp.List> respTargetList = respOriginList.parallelStream().map(e -> {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.resp.List respTarget = new cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.resp.List();
                        BeanUtils.copyProperties(e, respTarget);
                        return respTarget;
                    }).collect(Collectors.toList());
                    depoisRespDto.setList(respTargetList);
                }
                depoisResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                depoisResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                depoisResultDto.setCode(EpbEnum.EPB099999.key);
                depoisResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEPOIS.key, EsbEnum.TRADE_CODE_DEPOIS.value, e.getMessage());
            depoisResultDto.setCode(EpbEnum.EPB099999.key);//9999
            depoisResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        depoisResultDto.setData(depoisRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DEPOIS.key, EsbEnum.TRADE_CODE_DEPOIS.value, JSON.toJSONString(depoisResultDto));
        return depoisResultDto;
    }
}
