package cn.com.yusys.yusp.web.server.cfg.xdsx0025;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0025.req.Xdsx0025ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0025.resp.Xdsx0025RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdsx0025.req.Xdsx0025DataReqDto;
import cn.com.yusys.yusp.server.xdsx0025.resp.Xdsx0025DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:审批人资本占用率参数表同步
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0025:审批人资本占用率参数表同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0025Resource.class);
	@Autowired
	private DscmsCfgSxClientService dscmsCfgSxClientService;
    /**
     * 交易码：xdsx0025
     * 交易描述：审批人资本占用率参数表同步
     *
     * @param xdsx0025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("审批人资本占用率参数表同步")
    @PostMapping("/xdsx0025")
    //@Idempotent({"xdcasx0025", "#xdsx0025ReqDto.datasq"})
    protected @ResponseBody
	Xdsx0025RespDto xdsx0025(@Validated @RequestBody Xdsx0025ReqDto xdsx0025ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025ReqDto));
        Xdsx0025DataReqDto xdsx0025DataReqDto = new Xdsx0025DataReqDto();// 请求Data： 审批人资本占用率参数表同步
        Xdsx0025DataRespDto xdsx0025DataRespDto = new Xdsx0025DataRespDto();// 响应Data：审批人资本占用率参数表同步
		Xdsx0025RespDto xdsx0025RespDto = new Xdsx0025RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdsx0025.req.Data reqData = null; // 请求Data：审批人资本占用率参数表同步
		cn.com.yusys.yusp.dto.server.biz.xdsx0025.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0025.resp.Data();// 响应Data：审批人资本占用率参数表同步
        // 此处包导入待确定 结束
        try {
            // 从 xdsx0025ReqDto获取 reqData
            reqData = xdsx0025ReqDto.getData();
            // 将 reqData 拷贝到xdsx0025DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0025DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025DataReqDto));
            ResultDto<Xdsx0025DataRespDto> xdsx0025DataResultDto = dscmsCfgSxClientService.xdsx0025(xdsx0025DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdsx0025RespDto.setErorcd(Optional.ofNullable(xdsx0025DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0025RespDto.setErortx(Optional.ofNullable(xdsx0025DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0025DataResultDto.getCode())) {
                xdsx0025DataRespDto = xdsx0025DataResultDto.getData();
                xdsx0025RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0025RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0025DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0025DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, e.getMessage());
            xdsx0025RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0025RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0025RespDto.setDatasq(servsq);

        xdsx0025RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0025.key, DscmsEnum.TRADE_CODE_XDSX0025.value, JSON.toJSONString(xdsx0025RespDto));
        return xdsx0025RespDto;
    }
}
