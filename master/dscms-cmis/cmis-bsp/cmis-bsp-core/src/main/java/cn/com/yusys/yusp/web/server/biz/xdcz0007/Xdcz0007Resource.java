package cn.com.yusys.yusp.web.server.biz.xdcz0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0007.req.Xdcz0007ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0007.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0007.resp.Xdcz0007RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0007.req.Xdcz0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0007.resp.Xdcz0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询敞口额度及保证金校验
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0007:查询敞口额度及保证金校验")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.biz.xdcz0007.Xdcz0007Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0007
     * 交易描述：查询敞口额度及保证金校验
     *
     * @param xdcz0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询敞口额度及保证金校验")
    @PostMapping("/xdcz0007")
//   @Idempotent({"xdcz0007", "#xdcz0007ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0007RespDto xdcz0007(@Validated @RequestBody Xdcz0007ReqDto xdcz0007ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007ReqDto));
        Xdcz0007DataReqDto xdcz0007DataReqDto = new Xdcz0007DataReqDto();// 请求Data： 电子保函开立
        Xdcz0007DataRespDto xdcz0007DataRespDto = new Xdcz0007DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0007.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0007RespDto xdcz0007RespDto = new Xdcz0007RespDto();
        try {
            // 从 xdcz0007ReqDto获取 reqData
            reqData = xdcz0007ReqDto.getData();
            // 将 reqData 拷贝到xdcz0007DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0007DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007DataReqDto));
            ResultDto<Xdcz0007DataRespDto> xdcz0007DataResultDto = dscmsBizCzClientService.xdcz0007(xdcz0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0007RespDto.setErorcd(Optional.ofNullable(xdcz0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0007RespDto.setErortx(Optional.ofNullable(xdcz0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0007DataResultDto.getCode())) {
                xdcz0007DataRespDto = xdcz0007DataResultDto.getData();
                xdcz0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, e.getMessage());
            xdcz0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0007RespDto.setDatasq(servsq);

        xdcz0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0007.key, DscmsEnum.TRADE_CODE_XDCZ0007.value, JSON.toJSONString(xdcz0007RespDto));
        return xdcz0007RespDto;
    }
}
