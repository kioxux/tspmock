package cn.com.yusys.yusp.online.client.esb.core.ln3020.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdktxzh.Record;

import java.util.List;

/**
 * 请求Dto：贷款多贴息账户
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Lstdktxzh_ARRAY {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdktxzh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdktxzh_ARRAY{" +
                "record=" + record +
                '}';
    }
}