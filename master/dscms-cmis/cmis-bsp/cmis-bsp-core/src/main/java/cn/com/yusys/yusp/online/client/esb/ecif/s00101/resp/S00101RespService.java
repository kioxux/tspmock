package cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp;

/**
 * 请求Service：对私客户综合信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
public class S00101RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
