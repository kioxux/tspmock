package cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.resp;

/**
 * 响应Service：协议信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
	private String erorcd;//响应码
	private String erortx;//响应信息

	private String jgzh;//资金托管账号
	private String zzhxh;//子账户序号
	private String qyrq;//签约日期
	private String zxdm;//房产中心区域代码

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getJgzh() {
		return jgzh;
	}

	public void setJgzh(String jgzh) {
		this.jgzh = jgzh;
	}

	public String getZzhxh() {
		return zzhxh;
	}

	public void setZzhxh(String zzhxh) {
		this.zzhxh = zzhxh;
	}

	public String getQyrq() {
		return qyrq;
	}

	public void setQyrq(String qyrq) {
		this.qyrq = qyrq;
	}

	public String getZxdm() {
		return zxdm;
	}

	public void setZxdm(String zxdm) {
		this.zxdm = zxdm;
	}

	@Override
	public String toString() {
		return "Service{" +
				"erorcd='" + erorcd + '\'' +
				", erortx='" + erortx + '\'' +
				", jgzh='" + jgzh + '\'' +
				", zzhxh='" + zzhxh + '\'' +
				", qyrq='" + qyrq + '\'' +
				", zxdm='" + zxdm + '\'' +
				'}';
	}
}
