package cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.resp;

/**
 * 响应Service：获取该笔借据的最新五级分类以及数据日期
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd06RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd06RespService{" +
                "service=" + service +
                '}';
    }
}
