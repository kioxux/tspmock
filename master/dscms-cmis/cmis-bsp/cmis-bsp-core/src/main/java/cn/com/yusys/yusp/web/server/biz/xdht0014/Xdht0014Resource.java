package cn.com.yusys.yusp.web.server.biz.xdht0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0014.req.Xdht0014ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0014.resp.Xdht0014RespDto;
import cn.com.yusys.yusp.dto.server.xdht0014.req.Xdht0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0014.resp.Xdht0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同签订查询VX
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDHT0014:合同签订查询VX")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0014Resource.class);

    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0014
     * 交易描述：合同签订查询VX
     *
     * @param xdht0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同签订查询VX")
    @PostMapping("/xdht0014")
    //@Idempotent({"xdcaht0014", "#xdht0014ReqDto.datasq"})
    protected @ResponseBody
    Xdht0014RespDto xdht0014(@Validated @RequestBody Xdht0014ReqDto xdht0014ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014ReqDto));
        Xdht0014DataReqDto xdht0014DataReqDto = new Xdht0014DataReqDto();// 请求Data： 合同签订查询VX
        Xdht0014DataRespDto xdht0014DataRespDto = new Xdht0014DataRespDto();// 响应Data：合同签订查询VX
        Xdht0014RespDto xdht0014RespDto = new Xdht0014RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0014.req.Data reqData = null; // 请求Data：合同签订查询VX
        cn.com.yusys.yusp.dto.server.biz.xdht0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0014.resp.Data();// 响应Data：合同签订查询VX
        try {
            // 从 xdht0014ReqDto获取 reqData
            reqData = xdht0014ReqDto.getData();
            // 将 reqData 拷贝到xdht0014DataReqDto
            BeanUtils.copyProperties(reqData, xdht0014DataReqDto);
            // 调用服务：cmis_biz
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014ReqDto));
            ResultDto<Xdht0014DataRespDto> xdht0014DataResultDto = dscmsBizHtClientService.xdht0014(xdht0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014DataResultDto));
            // 从返回值中获取响应码和响应消息

            xdht0014RespDto.setErorcd(Optional.ofNullable(xdht0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0014RespDto.setErortx(Optional.ofNullable(xdht0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.key));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0014DataResultDto.getCode())) {
                xdht0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0014DataRespDto = xdht0014DataResultDto.getData();
                // 将 xdht0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0014DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, e.getMessage());
            xdht0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0014RespDto.setDatasq(servsq);

        xdht0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0014.key, DscmsEnum.TRADE_CODE_XDHT0014.value, JSON.toJSONString(xdht0014RespDto));
        return xdht0014RespDto;
    }
}
