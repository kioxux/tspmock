package cn.com.yusys.yusp.web.server.biz.xdzc0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0008.req.Xdzc0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0008.resp.Xdzc0008RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0008.req.Xdzc0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0008.resp.Xdzc0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池出票校验接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0008:资产池出票校验接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0008Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0008
     * 交易描述：资产池出票校验接口
     *
     * @param xdzc0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池出票校验接口")
    @PostMapping("/xdzc0008")
    //@Idempotent({"xdzc008", "#xdzc0008ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0008RespDto xdzc0008(@Validated @RequestBody Xdzc0008ReqDto xdzc0008ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, JSON.toJSONString(xdzc0008ReqDto));
        Xdzc0008DataReqDto xdzc0008DataReqDto = new Xdzc0008DataReqDto();// 请求Data： 资产池出票校验接口
        Xdzc0008DataRespDto xdzc0008DataRespDto = new Xdzc0008DataRespDto();// 响应Data：资产池出票校验接口
        Xdzc0008RespDto xdzc0008RespDto = new Xdzc0008RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0008.req.Data reqData = null; // 请求Data：资产池出票校验接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0008.resp.Data();// 响应Data：资产池出票校验接口

        try {
            // 从 xdzc0008ReqDto获取 reqData
            reqData = xdzc0008ReqDto.getData();
            // 将 reqData 拷贝到xdzc0008DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0008DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, JSON.toJSONString(xdzc0008DataReqDto));
            ResultDto<Xdzc0008DataRespDto> xdzc0008DataResultDto = dscmsBizZcClientService.xdzc0008(xdzc0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, JSON.toJSONString(xdzc0008DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0008RespDto.setErorcd(Optional.ofNullable(xdzc0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0008RespDto.setErortx(Optional.ofNullable(xdzc0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0008DataResultDto.getCode())) {
                xdzc0008DataRespDto = xdzc0008DataResultDto.getData();
                xdzc0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, e.getMessage());
            xdzc0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0008RespDto.setDatasq(servsq);

        xdzc0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0008.key, DscmsEnum.TRADE_CODE_XDZC0008.value, JSON.toJSONString(xdzc0008RespDto));
        return xdzc0008RespDto;
    }
}
