package cn.com.yusys.yusp.web.server.biz.xdht0036;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0036.req.Xdht0036ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0036.resp.Xdht0036RespDto;
import cn.com.yusys.yusp.dto.server.xdht0036.req.Xdht0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0036.resp.Xdht0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0036:根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0036Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0036
     * 交易描述：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
     *
     * @param xdht0036ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览")
    @PostMapping("/xdht0036")
    //@Idempotent({"xdcaht0036", "#xdht0036ReqDto.datasq"})
    protected @ResponseBody
    Xdht0036RespDto xdht0036(@Validated @RequestBody Xdht0036ReqDto xdht0036ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036ReqDto));
        Xdht0036DataReqDto xdht0036DataReqDto = new Xdht0036DataReqDto();// 请求Data： 根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
        Xdht0036DataRespDto xdht0036DataRespDto = new Xdht0036DataRespDto();// 响应Data：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
        Xdht0036RespDto xdht0036RespDto = new Xdht0036RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0036.req.Data reqData = null; // 请求Data：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
        cn.com.yusys.yusp.dto.server.biz.xdht0036.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0036.resp.Data();// 响应Data：根据客户ID以及管户经理编号，查询优企贷贷款合同信息一览
        try {
            // 从 xdht0036ReqDto获取 reqData
            reqData = xdht0036ReqDto.getData();
            // 将 reqData 拷贝到xdht0036DataReqDto
            BeanUtils.copyProperties(reqData, xdht0036DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036DataReqDto));
            ResultDto<Xdht0036DataRespDto> xdht0036DataResultDto = dscmsBizHtClientService.xdht0036(xdht0036DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0036RespDto.setErorcd(Optional.ofNullable(xdht0036DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0036RespDto.setErortx(Optional.ofNullable(xdht0036DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0036DataResultDto.getCode())) {
                xdht0036DataRespDto = xdht0036DataResultDto.getData();
                xdht0036RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0036RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0036DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0036DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, e.getMessage());
            xdht0036RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0036RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0036RespDto.setDatasq(servsq);

        xdht0036RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0036.key, DscmsEnum.TRADE_CODE_XDHT0036.value, JSON.toJSONString(xdht0036RespDto));
        return xdht0036RespDto;
    }
}
