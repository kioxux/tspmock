package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb02.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 响应Service：查询不动产信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String addr;//地址
    private String reg_no;//数量
    private String num;//编号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", addr='" + addr + '\'' +
                ", reg_no='" + reg_no + '\'' +
                ", num='" + num + '\'' +
                '}';
    }
}
