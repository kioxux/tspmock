package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp06.req;

/**
 * 请求Service：被拒绝的线上产品推送接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Znsp06ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
