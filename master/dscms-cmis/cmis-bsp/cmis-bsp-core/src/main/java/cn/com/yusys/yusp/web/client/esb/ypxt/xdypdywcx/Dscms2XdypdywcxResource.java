package cn.com.yusys.yusp.web.client.esb.ypxt.xdypdywcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypdywcx.req.XdypdywcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypdywcx.resp.XdypdywcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypdywcx.req.XdypdywcxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypdywcx.resp.XdypdywcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(xdypdywcx)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2XdypdywcxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdypdywcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdypdywcx
     * 交易描述：查询抵押物信息
     *
     * @param xdypdywcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdypdywcx")
    protected @ResponseBody
    ResultDto<XdypdywcxRespDto> xdypdywcx(@Validated @RequestBody XdypdywcxReqDto xdypdywcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPDYWCX.key, EsbEnum.TRADE_CODE_XDYPDYWCX.value, JSON.toJSONString(xdypdywcxReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypdywcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypdywcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypdywcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypdywcx.resp.Service();
        XdypdywcxReqService xdypdywcxReqService = new XdypdywcxReqService();
        XdypdywcxRespService xdypdywcxRespService = new XdypdywcxRespService();
        XdypdywcxRespDto xdypdywcxRespDto = new XdypdywcxRespDto();
        ResultDto<XdypdywcxRespDto> xdypdywcxResultDto = new ResultDto<XdypdywcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdypdywcxReqDto转换成reqService
            BeanUtils.copyProperties(xdypdywcxReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDYPDYWCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xdypdywcxReqService.setService(reqService);
            // 将xdypdywcxReqService转换成xdypdywcxReqServiceMap
            Map xdypdywcxReqServiceMap = beanMapUtil.beanToMap(xdypdywcxReqService);
            context.put("tradeDataMap", xdypdywcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUCONT.key, EsbEnum.TRADE_CODE_BUCONT.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_BUCONT.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUCONT.key, EsbEnum.TRADE_CODE_BUCONT.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdypdywcxRespService = beanMapUtil.mapToBean(tradeDataMap, XdypdywcxRespService.class, XdypdywcxRespService.class);
            respService = xdypdywcxRespService.getService();

            xdypdywcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdypdywcxResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypdywcxRespDto
                BeanUtils.copyProperties(respService, xdypdywcxRespDto);
                xdypdywcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdypdywcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdypdywcxResultDto.setCode(EpbEnum.EPB099999.key);
                xdypdywcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPDYWCX.key, EsbEnum.TRADE_CODE_XDYPDYWCX.value, e.getMessage());
            xdypdywcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdypdywcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        xdypdywcxResultDto.setData(xdypdywcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPDYWCX.key, EsbEnum.TRADE_CODE_XDYPDYWCX.value, JSON.toJSONString(xdypdywcxResultDto));
        return xdypdywcxResultDto;
    }
}
