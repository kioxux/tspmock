package cn.com.yusys.yusp.online.client.esb.rircp.fbyd34.req;

/**
 * 请求Service：支用模型校验
 */
public class Service {
	private String prcscd; // 处理码
	private String servtp; // 渠道
	private String servsq; // 渠道流水
	private String userid; // 柜员号
	private String brchno; // 部门号

    private String channel_type;//渠道来源
    private String co_platform;//合作平台
    private String prd_type;//产品类别
    private String prd_code;//产品代码（零售智能风控内部代码）
    private String cust_name;//客户姓名
    private String cert_code;//客户证件号码
    private String app_no;//授信申请业务流水号

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", channel_type='" + channel_type + '\'' +
				", co_platform='" + co_platform + '\'' +
				", prd_type='" + prd_type + '\'' +
				", prd_code='" + prd_code + '\'' +
				", cust_name='" + cust_name + '\'' +
				", cert_code='" + cert_code + '\'' +
				", app_no='" + app_no + '\'' +
				'}';
	}
}
