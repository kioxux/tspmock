package cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp;

public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp.Record> record;

    public java.util.List<cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp.Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
