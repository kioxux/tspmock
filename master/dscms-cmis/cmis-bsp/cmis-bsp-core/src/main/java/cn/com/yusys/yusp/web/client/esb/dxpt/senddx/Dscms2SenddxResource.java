package cn.com.yusys.yusp.web.client.esb.dxpt.senddx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.SenddxReqService;
import cn.com.yusys.yusp.online.client.esb.dxpt.senddx.resp.SenddxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用短信平台的接口
 **/
@Api(tags = "BSP封装调用短信平台的接口处理类")
@RestController
@RequestMapping("/api/dscms2dxpt")
public class Dscms2SenddxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2SenddxResource.class);
    private static DateTimeFormatter tranDateTimestampFormtter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 短信/微信发送批量接口
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("senddx:短信/微信发送批量接口")
    @PostMapping("/senddx")
    protected @ResponseBody
    ResultDto<SenddxRespDto> senddx(@Validated @RequestBody SenddxReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.Service();
        cn.com.yusys.yusp.online.client.esb.dxpt.senddx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.dxpt.senddx.resp.Service();
        SenddxReqService senddxReqService = new SenddxReqService();
        SenddxRespService senddxRespService = new SenddxRespService();
        SenddxRespDto senddxRespDto = new SenddxRespDto();
        ResultDto<SenddxRespDto> senddxResultDto = new ResultDto<SenddxRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将SenddxReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            // 循环相关的判断
            if (CollectionUtils.nonEmpty(reqDto.getSenddxReqList())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqList> senddxReqLists = Optional.ofNullable(reqDto.getSenddxReqList()).orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.List senddxReqListService = new cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.List();
                java.util.List<cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.Record> records = senddxReqLists.parallelStream().map(senddxReqList -> {
                    cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.Record record = new cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req.Record();
                    BeanUtils.copyProperties(senddxReqList, record);
                    return record;
                }).collect(Collectors.toList());
                senddxReqListService.setRecord(records);
                reqService.setList(senddxReqListService);
            }
            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_SENDDX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setSendti(tranDateTimestampFormtter.format(now));//发送时间 ,  格式：YYYYMMDDHHMMSS（只对短信生效）
            senddxReqService.setService(reqService);
            // 将senddxReqService转换成senddxReqServiceMap
            Map senddxReqServiceMap = beanMapUtil.beanToMap(senddxReqService);
            context.put("tradeDataMap", senddxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_SENDDX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            senddxRespService = beanMapUtil.mapToBean(tradeDataMap, SenddxRespService.class, SenddxRespService.class);
            respService = senddxRespService.getService();

            //  将SenddxRespDto封装到ResultDto<SenddxRespDto>
            senddxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            senddxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            senddxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            senddxResultDto.setMessage(SuccessEnum.CMIS_SUCCSESS.value);
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成SenddxRespDto
                BeanUtils.copyProperties(respService, senddxRespDto);
                senddxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                senddxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                senddxResultDto.setCode(EpbEnum.EPB099999.key);
                senddxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, e.getMessage());
            senddxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            senddxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        senddxResultDto.setData(senddxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SENDDX.key, EsbEnum.TRADE_CODE_SENDDX.value, JSON.toJSONString(senddxResultDto));
        return senddxResultDto;
    }


}
