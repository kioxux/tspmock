package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj12;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.req.Xdpj12ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.resp.Xdpj12RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.Xdpj12ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.resp.Xdpj12RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:票据池推送保证金账号
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj12Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj12Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdpj12
     * 交易描述：票据池推送保证金账号
     *
     * @param xdpj12ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdpj12")
    protected @ResponseBody
    ResultDto<Xdpj12RespDto> xdpj12(@Validated @RequestBody Xdpj12ReqDto xdpj12ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ12.key, EsbEnum.TRADE_CODE_XDPJ12.value, JSON.toJSONString(xdpj12ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.Service();
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.resp.Service();
        Xdpj12ReqService xdpj12ReqService = new Xdpj12ReqService();
        Xdpj12RespService xdpj12RespService = new Xdpj12RespService();
        Xdpj12RespDto xdpj12RespDto = new Xdpj12RespDto();
        ResultDto<Xdpj12RespDto> xdpj12ResultDto = new ResultDto<Xdpj12RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdpj12ReqDto转换成reqService
            BeanUtils.copyProperties(xdpj12ReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.List targetList = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.List();
            if (CollectionUtils.nonEmpty(xdpj12ReqDto.getList())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.req.List> originList = xdpj12ReqDto.getList();
                java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.Record> recordList = new ArrayList<>();
                cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.Record target = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.Record();
                for (cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.req.List list : originList) {
                    BeanUtils.copyProperties(list, target);
                    recordList.add(target);
                }
                targetList.setRecord(recordList);
                reqService.setList(targetList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ12.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            xdpj12ReqService.setService(reqService);
            // 将xdpj12ReqService转换成xdpj12ReqServiceMap
            Map xdpj12ReqServiceMap = beanMapUtil.beanToMap(xdpj12ReqService);
            context.put("tradeDataMap", xdpj12ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ12.key, EsbEnum.TRADE_CODE_XDPJ12.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ12.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ12.key, EsbEnum.TRADE_CODE_XDPJ12.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdpj12RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj12RespService.class, Xdpj12RespService.class);
            respService = xdpj12RespService.getService();
            //  将respService转换成Xdpj12RespDto
            BeanUtils.copyProperties(respService, xdpj12RespDto);
            xdpj12ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xdpj12ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xdpj12RespDto);
                xdpj12ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdpj12ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdpj12ResultDto.setCode(EpbEnum.EPB099999.key);
                xdpj12ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ12.key, EsbEnum.TRADE_CODE_XDPJ12.value, e.getMessage());
            xdpj12ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdpj12ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdpj12ResultDto.setData(xdpj12RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ12.key, EsbEnum.TRADE_CODE_XDPJ12.value, JSON.toJSONString(xdpj12ResultDto));
        return xdpj12ResultDto;
    }
}
