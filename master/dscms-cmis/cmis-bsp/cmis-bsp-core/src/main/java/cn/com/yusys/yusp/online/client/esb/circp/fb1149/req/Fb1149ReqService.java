package cn.com.yusys.yusp.online.client.esb.circp.fb1149.req;

/**
 * 请求Service：无还本续贷借据更新
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1149ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
