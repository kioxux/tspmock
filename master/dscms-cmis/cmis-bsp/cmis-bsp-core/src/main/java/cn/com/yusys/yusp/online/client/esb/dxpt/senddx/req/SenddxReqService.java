package cn.com.yusys.yusp.online.client.esb.dxpt.senddx.req;


/**
 * 请求Service：短信/微信发送批量接口
 *
 * @author hjk
 * @version 1.0
 */
public class SenddxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "SenddxService{" +
                "service=" + service +
                '}';
    }
}
