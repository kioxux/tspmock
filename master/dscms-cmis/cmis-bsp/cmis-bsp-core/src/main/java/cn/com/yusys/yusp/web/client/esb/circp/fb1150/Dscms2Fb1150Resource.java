package cn.com.yusys.yusp.web.client.esb.circp.fb1150;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.Fb1150ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.List;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1150.resp.Fb1150RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.Fb1150ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.Record;
import cn.com.yusys.yusp.online.client.esb.circp.fb1150.resp.Fb1150RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1150）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1150Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1150Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1150
     * 交易描述：省心快贷plus授信申请
     *
     * @param fb1150ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1150")
    protected @ResponseBody
    ResultDto<Fb1150RespDto> fb1150(@Validated @RequestBody Fb1150ReqDto fb1150ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1150.key, EsbEnum.TRADE_CODE_FB1150.value, JSON.toJSONString(fb1150ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1150.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1150.resp.Service();
        Fb1150ReqService fb1150ReqService = new Fb1150ReqService();
        Fb1150RespService fb1150RespService = new Fb1150RespService();
        Fb1150RespDto fb1150RespDto = new Fb1150RespDto();
        ResultDto<Fb1150RespDto> fb1150ResultDto = new ResultDto<Fb1150RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1150ReqDto转换成reqService
            BeanUtils.copyProperties(fb1150ReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.List serviceList = new cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.List();
            if (CollectionUtils.nonEmpty(fb1150ReqDto.getList())) {
                java.util.List<List> originList = fb1150ReqDto.getList();
                java.util.List<Record> targetList = originList.stream().map(l -> {
                    Record target = new Record();
                    BeanUtils.copyProperties(l, target);
                    return target;
                }).collect(Collectors.toList());
                serviceList.setRecord(targetList);
                reqService.setList(serviceList);

            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1150.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1150ReqService.setService(reqService);
            // 将fb1150ReqService转换成fb1150ReqServiceMap
            Map fb1150ReqServiceMap = beanMapUtil.beanToMap(fb1150ReqService);
            context.put("tradeDataMap", fb1150ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1150.key, EsbEnum.TRADE_CODE_FB1150.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1150.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1150.key, EsbEnum.TRADE_CODE_FB1150.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1150RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1150RespService.class, Fb1150RespService.class);
            respService = fb1150RespService.getService();

            fb1150ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1150ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1150RespDto
                BeanUtils.copyProperties(respService, fb1150RespDto);

                fb1150ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1150ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1150ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1150ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1150.key, EsbEnum.TRADE_CODE_FB1150.value, e.getMessage());
            fb1150ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1150ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1150ResultDto.setData(fb1150RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1150.key, EsbEnum.TRADE_CODE_FB1150.value, JSON.toJSONString(fb1150ResultDto));
        return fb1150ResultDto;
    }
}
