package cn.com.yusys.yusp.web.server.biz.xdxw0066;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0066.req.Xdxw0066ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0066.resp.Xdxw0066RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0066.req.Xdxw0066DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0066.resp.Xdxw0066DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:调查基本信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0066:调查基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0066Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0066Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0066
     * 交易描述：调查基本信息查询
     *
     * @param xdxw0066ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查基本信息查询")
    @PostMapping("/xdxw0066")
    //@Idempotent({"xdcaxw0066", "#xdxw0066ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0066RespDto xdxw0066(@Validated @RequestBody Xdxw0066ReqDto xdxw0066ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066ReqDto));
        Xdxw0066DataReqDto xdxw0066DataReqDto = new Xdxw0066DataReqDto();// 请求Data： 调查基本信息查询
        Xdxw0066DataRespDto xdxw0066DataRespDto = new Xdxw0066DataRespDto();// 响应Data：调查基本信息查询
        Xdxw0066RespDto xdxw0066RespDto = new Xdxw0066RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0066.req.Data reqData = null; // 请求Data：调查基本信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0066.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0066.resp.Data();// 响应Data：调查基本信息查询
        try {
            // 从 xdxw0066ReqDto获取 reqData
            reqData = xdxw0066ReqDto.getData();
            // 将 reqData 拷贝到xdxw0066DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0066DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066DataReqDto));
            ResultDto<Xdxw0066DataRespDto> xdxw0066DataResultDto = dscmsBizXwClientService.xdxw0066(xdxw0066DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0066RespDto.setErorcd(Optional.ofNullable(xdxw0066DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0066RespDto.setErortx(Optional.ofNullable(xdxw0066DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0066DataResultDto.getCode())) {
                xdxw0066DataRespDto = xdxw0066DataResultDto.getData();
                xdxw0066RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0066RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0066DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0066DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, e.getMessage());
            xdxw0066RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0066RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0066RespDto.setDatasq(servsq);

        xdxw0066RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0066.key, DscmsEnum.TRADE_CODE_XDXW0066.value, JSON.toJSONString(xdxw0066RespDto));
        return xdxw0066RespDto;
    }
}
