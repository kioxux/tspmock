package cn.com.yusys.yusp.web.client.esb.ecif.g10001;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10001.req.G10001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp.G10001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g10001.req.G10001ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.G10001RespService;
import cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g10001)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G10001Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G10001Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 客户群组信息查询
     *
     * @param g10001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g10001:客户群组信息查询")
    @PostMapping("/g10001")
    protected @ResponseBody
    ResultDto<G10001RespDto> g10001(@Validated @RequestBody G10001ReqDto g10001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10001.key, EsbEnum.TRADE_CODE_G10001.value, JSON.toJSONString(g10001ReqDto));
        cn.com.yusys.yusp.online.client.esb.ecif.g10001.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g10001.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.Service();
        G10001ReqService g10001ReqService = new G10001ReqService();
        G10001RespService g10001RespService = new G10001RespService();
        G10001RespDto g10001RespDto = new G10001RespDto();
        ResultDto<G10001RespDto> g10001ResultDto = new ResultDto<G10001RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将G10001ReqDto转换成reqService
            BeanUtils.copyProperties(g10001ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_G10001.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            g10001ReqService.setService(reqService);
            // 将g10001ReqService转换成g10001ReqServiceMap
            Map g10001ReqServiceMap = beanMapUtil.beanToMap(g10001ReqService);
            context.put("tradeDataMap", g10001ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10001.key, EsbEnum.TRADE_CODE_G10001.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G10001.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10001.key, EsbEnum.TRADE_CODE_G10001.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g10001RespService = beanMapUtil.mapToBean(tradeDataMap, G10001RespService.class, G10001RespService.class);
            respService = g10001RespService.getService();
            //  将G10001RespDto封装到ResultDto<G10001RespDto>
            g10001ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g10001ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G10001RespDto
                BeanUtils.copyProperties(respService, g10001RespDto);

                cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.List list = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.List());
                if (CollectionUtils.nonEmpty(list.getRecord())) {
                    java.util.List<Record> recordList = list.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp.GroupArrayMem> targetList = new ArrayList<>();
                    for (cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp.Record r : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp.GroupArrayMem target = new cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp.GroupArrayMem();
                        BeanUtils.copyProperties(r, target);
                        targetList.add(target);
                    }
                    g10001RespDto.setList(targetList);
                }
                g10001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g10001ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                BeanUtils.copyProperties(respService, g10001RespDto);
                g10001ResultDto.setCode(EpbEnum.EPB099999.key);
                g10001ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10001.key, EsbEnum.TRADE_CODE_G10001.value, e.getMessage());
            g10001ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g10001ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g10001ResultDto.setData(g10001RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G10001.key, EsbEnum.TRADE_CODE_G10001.value, JSON.toJSONString(g10001ResultDto));
        return g10001ResultDto;
    }
}
