package cn.com.yusys.yusp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * 挡板测试接口处理
 *
 * @author ksfk
 *
 */
@Service
public class YmitService {

    protected static Logger logger = LoggerFactory.getLogger(YmitService.class);

    /**
     * 是否启用的挡板测试
     */
    @Value("${application.ymit.enabled:false}")
    private boolean ymitEnabled;

    /**
     *
     * @param filename
     *            响应报文文件名,eg:CUQ00071A19.xml，默认在resources/ymit/下
     * @param serviceName
     *            通讯接出服务名,eg:TradeClient
     * @param tradeCode
     *            交易码,eg:CUQ00071A19
     * @param respMsgType
     *            响应报文类型,eg:SOAP
     * @param msgEncode
     *            响应报文编码方式,eg:GBK
     * @param rtnClazz
     * @return
     */
    public <T> T echange(String filename, String serviceName, String tradeCode, String respMsgType, String msgEncode,
                         Class<T> rtnClazz) {
        YmitUtil ymitUtil = new YmitUtil();
        return ymitUtil.echange(filename, serviceName, tradeCode, respMsgType, msgEncode, rtnClazz);
    }

    public boolean getYmitEnabled() {
        return ymitEnabled;
    }

    public void setYmitEnabled(boolean ymitEnabled) {
        this.ymitEnabled = ymitEnabled;
    }

}