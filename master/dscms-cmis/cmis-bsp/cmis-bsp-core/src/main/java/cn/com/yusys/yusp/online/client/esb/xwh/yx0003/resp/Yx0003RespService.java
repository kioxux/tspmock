package cn.com.yusys.yusp.online.client.esb.xwh.yx0003.resp;

/**
 * 响应Service：市民贷优惠券核销锁定
 *
 * @author chenyong
 * @version 1.0
 */
public class Yx0003RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Yx0003RespService{" +
                "service=" + service +
                '}';
    }
}
