package cn.com.yusys.yusp.web.client.gxp.tonglian.d12050;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.D12050RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d12050)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D12050Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D12050Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 账单列表查询（处理码d12050）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d12050:账单列表查询")
    @PostMapping("/d12050")
    protected @ResponseBody
    ResultDto<D12050RespDto> d12050(@Validated @RequestBody D12050ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12050.key, GxpEnum.TRADE_CODE_D12050.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.D12050ReqMessage d12050ReqMessage = null;//请求Message：账单列表查询
        D12050RespMessage d12050RespMessage = new D12050RespMessage();//响应Message：账单列表查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.Message reqMessage = null;//请求Message：账单列表查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.Message();//响应Message：账单列表查询
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：账单列表查询
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：账单列表查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.Body();//请求Body：账单列表查询
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.Body();//响应Body：账单列表查询

        D12050RespDto d12050RespDto = new D12050RespDto();//响应Dto：账单列表查询
        ResultDto<D12050RespDto> d12050ResultDto = new ResultDto<>();//响应ResultDto：账单列表查询

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D12050.key, GxpEnum.TRADE_CODE_D12050.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D12050ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.Message::setBody, reqBody).build();
            // 给d12050ReqMessage赋值
            d12050ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.D12050ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req.D12050ReqMessage::setMessage, reqMessage).build();

            // 将d12050ReqService转换成d12050ReqServiceMap
            Map d12050ReqMessageMap = beanMapUtil.beanToMap(d12050ReqMessage);
            context.put("tradeDataMap", d12050ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12050.key, GxpEnum.TRADE_CODE_D12050.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D12050.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12050.key, GxpEnum.TRADE_CODE_D12050.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d12050RespMessage = beanMapUtil.mapToBean(tradeDataMap, D12050RespMessage.class, D12050RespMessage.class);//响应Message：账单列表查询
            respMessage = d12050RespMessage.getMessage();//响应Message：账单列表查询
            gxpRespHead = respMessage.getHead();//响应Head：账单列表查询
            //  将D12050RespDto封装到ResultDto<D12050RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                respBody = respMessage.getBody();
                //  将respBody转换成D12050RespDto
                BeanUtils.copyProperties(respBody, d12050RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.List d12050RespList = Optional.ofNullable(respBody.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.List());
                respBody.setList(d12050RespList);
                if (CollectionUtils.nonEmpty(d12050RespList.getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.Record> d12050RespRecords = Optional.ofNullable(d12050RespList.getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.List> d12050ListDtos = d12050RespRecords.parallelStream().map(d12050RespRecord -> {
                        cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.List d12050ListDto = new cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.List();
                        BeanUtils.copyProperties(d12050RespRecord, d12050ListDto);
                        return d12050ListDto;
                    }).collect(Collectors.toList());
                    d12050RespDto.setList(d12050ListDtos);
                }
                d12050ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d12050ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d12050ResultDto.setCode(EpbEnum.EPB099999.key);
                d12050ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d12050ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12050.key, GxpEnum.TRADE_CODE_D12050.value, e.getMessage());
            d12050ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d12050ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d12050ResultDto.setData(d12050RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12050.key, GxpEnum.TRADE_CODE_D12050.value, JSON.toJSONString(d12050ResultDto));
        return d12050ResultDto;
    }
}
