package cn.com.yusys.yusp.online.client.esb.rircp.fbxd02.req;

/**
 * 请求Service：为正式客户更新信贷客户信息手机号码
 *
 * @author chenyong
 * @version 1.0
 */
public class Fbxd02ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd02ReqService{" +
                "service=" + service +
                '}';
    }
}
