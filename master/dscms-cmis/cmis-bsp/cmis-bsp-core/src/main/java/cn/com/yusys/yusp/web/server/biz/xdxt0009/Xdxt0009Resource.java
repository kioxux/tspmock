package cn.com.yusys.yusp.web.server.biz.xdxt0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxt0009.req.Xdxt0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxt0009.resp.Xdxt0009RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.req.Xdxt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0009.resp.Xdxt0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:台账信息通用列表查询
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDXT0009:客户经理是否为小微客户经理")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0009Resource.class);
    @Autowired
    private DscmsBizXtClientService dscmsBizXtClientService;

    /**
     * 交易码：xdxt0009
     * 交易描述：客户经理是否为小微客户经理
     *
     * @param xdxt0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户经理是否为小微客户经理")
    @PostMapping("/xdxt0009")
    //@Idempotent({"xdcaxt0009", "#xdxt0009ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0009RespDto xdxt0009(@Validated @RequestBody Xdxt0009ReqDto xdxt0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009ReqDto));
        Xdxt0009DataReqDto xdxt0009DataReqDto = new Xdxt0009DataReqDto();// 请求Data： 台账信息通用列表查询
        Xdxt0009DataRespDto xdxt0009DataRespDto = new Xdxt0009DataRespDto();// 响应Data：台账信息通用列表查询
        Xdxt0009RespDto xdxt0009RespDto = new Xdxt0009RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxt0009.req.Data reqData = null; // 请求Data：台账信息通用列表查询
        cn.com.yusys.yusp.dto.server.biz.xdxt0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxt0009.resp.Data();// 响应Data：台账信息通用列表查询
        try {
            // 从 xdxt0009ReqDto获取 reqData
            reqData = xdxt0009ReqDto.getData();
            // 将 reqData 拷贝到xdxt0009DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0009DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataReqDto));
            ResultDto<Xdxt0009DataRespDto> xdxt0009DataResultDto = dscmsBizXtClientService.xdxt0009(xdxt0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxt0009RespDto.setErorcd(Optional.ofNullable(xdxt0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0009RespDto.setErortx(Optional.ofNullable(xdxt0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0009DataResultDto.getCode())) {
                xdxt0009DataRespDto = xdxt0009DataResultDto.getData();
                xdxt0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, e.getMessage());
            xdxt0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0009RespDto.setDatasq(servsq);

        xdxt0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0009.key, DscmsEnum.TRADE_CODE_XDXT0009.value, JSON.toJSONString(xdxt0009RespDto));
        return xdxt0009RespDto;
    }
}
