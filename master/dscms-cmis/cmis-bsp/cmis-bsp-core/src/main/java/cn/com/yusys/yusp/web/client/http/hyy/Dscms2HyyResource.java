package cn.com.yusys.yusp.web.client.http.hyy;

import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.hyy.bare01.req.Bare01ReqDto;
import cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp.Bare01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.online.client.http.hyy.bare01.req.Bare01ReqService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * BSP封装调用慧押押系统的接口
 * 该接口改为调用 信贷押品状态查询ypztcx 接口，可以不用，暂时预留已备后期需要
 **/
@Api(tags = "BSP封装调用慧押押系统的接口")
@RestController
@RequestMapping("/api/dscms2hyy")
@Deprecated
public class Dscms2HyyResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2HyyResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    @Autowired
    private RestTemplate restTemplate;

    /**
     * 交易码：bare01
     * 交易描述：押品状态查询
     *
     * @param bare01ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/bare01")
    protected @ResponseBody
    ResultDto<Bare01RespDto> bare01(@Validated @RequestBody Bare01ReqDto bare01ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BARE01.key, EsbEnum.TRADE_CODE_BARE01.value, JSON.toJSONString(bare01ReqDto));
        cn.com.yusys.yusp.online.client.http.hyy.bare01.req.Service reqService = new cn.com.yusys.yusp.online.client.http.hyy.bare01.req.Service();
        Bare01ReqService bare01ReqService = new Bare01ReqService();
        Bare01RespDto bare01RespDto = new Bare01RespDto();
        ResultDto<Bare01RespDto> bare01ResultDto = new ResultDto<Bare01RespDto>();
        //  将bare01ReqDto转换成reqService
        BeanUtils.copyProperties(bare01ReqDto, reqService);
        reqService.setPrcscd(EsbEnum.TRADE_CODE_BARE01.key);//    交易码
        reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        reqService.setServsq(servsq);//    渠道流水
        reqService.setUserid(EsbEnum.USERID_FXYJXT.key);//    柜员号
        reqService.setBrchno(EsbEnum.BRCHNO_FXYJXT.key);//    部门号
        bare01ReqService.setService(reqService);

        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            headers.set("Authorization", "3e4fa75f4f96a7da1d1987aeceb83a4a");
            HttpEntity<String> entity = new HttpEntity<>(null, headers);
            String url = "http://10.28.121.56/bank/collaterals/real-estate-info?district_code=" + bare01ReqDto.getDistrict_code() + "&certificate_number=" + bare01ReqDto.getCertificate_number() + "&mortgage_ certificate_number=" + bare01ReqDto.getCertificate_number();
            logger.info("调用慧押押平台的URL为[{}]", url);
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BARE01.key, EsbEnum.TRADE_CODE_BARE01.value, JSON.toJSONString(bare01ReqDto));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BARE01.key, EsbEnum.TRADE_CODE_BARE01.value, responseEntity);
            String responseStr = responseEntity.getBody();
            bare01RespDto = JSONObject.parseObject(responseStr, Bare01RespDto.class);
            bare01ResultDto.setData(bare01RespDto);
        } catch (RestClientException e) {
            logger.error(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BARE01.key, EsbEnum.TRADE_CODE_BARE01.value, e.getMessage());
            bare01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            bare01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BARE01.key, EsbEnum.TRADE_CODE_BARE01.value, JSON.toJSONString(bare01ResultDto));
        return bare01ResultDto;
    }


}
