package cn.com.yusys.yusp.online.client.esb.core.dp2021.resp;

import java.math.BigDecimal;
import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/6/15 14:34:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/15 14:34
 * @since 2021/6/15 14:34
 */
public class Lstacctinfo {


    private String chaxfanw;//查询范围
    private Integer zongbshu;//总笔数
    private String bblujing;//报表路径
    private String kzjedjbz;//客户账户金额冻结标志
    private String kzfbdjbz;//客户账户封闭冻结标志
    private String kzzsbfbz;//客户账户只收不付标志
    private String kzzfbsbz;//客户账户只付不收标志
    private String kzhuztai;//客户账户状态
    private BigDecimal zhhuyuee;//当前账户余额
    private String kehuleix;//客户类型
    private String kehuhaoo;//客户号
    private String doqiriqi;//客户号证件到期日期
    private String kehuzhmc;//客户账户名称
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.dp2021.resp.Record> record;

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    public String getBblujing() {
        return bblujing;
    }

    public void setBblujing(String bblujing) {
        this.bblujing = bblujing;
    }

    public String getKzjedjbz() {
        return kzjedjbz;
    }

    public void setKzjedjbz(String kzjedjbz) {
        this.kzjedjbz = kzjedjbz;
    }

    public String getKzfbdjbz() {
        return kzfbdjbz;
    }

    public void setKzfbdjbz(String kzfbdjbz) {
        this.kzfbdjbz = kzfbdjbz;
    }

    public String getKzzsbfbz() {
        return kzzsbfbz;
    }

    public void setKzzsbfbz(String kzzsbfbz) {
        this.kzzsbfbz = kzzsbfbz;
    }

    public String getKzzfbsbz() {
        return kzzfbsbz;
    }

    public void setKzzfbsbz(String kzzfbsbz) {
        this.kzzfbsbz = kzzfbsbz;
    }

    public String getKzhuztai() {
        return kzhuztai;
    }

    public void setKzhuztai(String kzhuztai) {
        this.kzhuztai = kzhuztai;
    }

    public BigDecimal getZhhuyuee() {
        return zhhuyuee;
    }

    public void setZhhuyuee(BigDecimal zhhuyuee) {
        this.zhhuyuee = zhhuyuee;
    }

    public String getKehuleix() {
        return kehuleix;
    }

    public void setKehuleix(String kehuleix) {
        this.kehuleix = kehuleix;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstacctinfo{" +
                "chaxfanw='" + chaxfanw + '\'' +
                ", zongbshu=" + zongbshu +
                ", bblujing='" + bblujing + '\'' +
                ", kzjedjbz='" + kzjedjbz + '\'' +
                ", kzfbdjbz='" + kzfbdjbz + '\'' +
                ", kzzsbfbz='" + kzzsbfbz + '\'' +
                ", kzzfbsbz='" + kzzfbsbz + '\'' +
                ", kzhuztai='" + kzhuztai + '\'' +
                ", zhhuyuee=" + zhhuyuee +
                ", kehuleix='" + kehuleix + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", record=" + record +
                '}';
    }
}
