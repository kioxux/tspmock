package cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req;

import java.math.BigDecimal;

/**
 * 请求Service：分期审核
 */
public class Body {
    private String rgstid;//分期申请号
    private String cardno;//卡号
    private String remark;//备注信息
    private BigDecimal loanit;//分期总本金
    private String opt;//操作码
    private String sendnd;//是否直接放款

    public String getRgstid() {
        return rgstid;
    }

    public void setRgstid(String rgstid) {
        this.rgstid = rgstid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getLoanit() {
        return loanit;
    }

    public void setLoanit(BigDecimal loanit) {
        this.loanit = loanit;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public String getSendnd() {
        return sendnd;
    }

    public void setSendnd(String sendnd) {
        this.sendnd = sendnd;
    }

    @Override
    public String toString() {
        return "Service{" +
                "rgstid='" + rgstid + '\'' +
                "cardno='" + cardno + '\'' +
                "remark='" + remark + '\'' +
                "loanit='" + loanit + '\'' +
                "opt='" + opt + '\'' +
                "sendnd='" + sendnd + '\'' +
                '}';
    }
}
