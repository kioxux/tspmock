package cn.com.yusys.yusp.online.client.esb.zjywxt.jcxxcx.req;

/**
 * 请求Service：缴存信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class JcxxcxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "JcxxcxReqService{" +
                "service=" + service +
                '}';
    }
}
