package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp;

public class Record {
    private String billNo;//票号
    private String fBillAmount;//票面金额
    private String bailAmt;//保证金金额
    private String chargeFlag;//扣款标志
    private String statusFlag;//承兑状态

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getfBillAmount() {
        return fBillAmount;
    }

    public void setfBillAmount(String fBillAmount) {
        this.fBillAmount = fBillAmount;
    }

    public String getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(String bailAmt) {
        this.bailAmt = bailAmt;
    }

    public String getChargeFlag() {
        return chargeFlag;
    }

    public void setChargeFlag(String chargeFlag) {
        this.chargeFlag = chargeFlag;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    @Override
    public String toString() {
        return "Record{" +
                "billNo='" + billNo + '\'' +
                ", fBillAmount='" + fBillAmount + '\'' +
                ", bailAmt='" + bailAmt + '\'' +
                ", chargeFlag='" + chargeFlag + '\'' +
                ", statusFlag='" + statusFlag + '\'' +
                '}';
    }
}
