package cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.req;

/**
 * 请求Service：用户信息同步接口
 *
 * @author chenyong
 * @version 1.0
 */
public class YhxxtbReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "YhxxtbReqService{" +
                "service=" + service +
                '}';
    }
}
