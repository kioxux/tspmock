package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 17:28
 * @since 2021/6/7 17:28
 */
public class LstDztxmx {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Record4> record;


    public List<Record4> getRecord() {
        return record;
    }

    public void setRecord(List<Record4> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstDztxmx{" +
                "record=" + record +
                '}';
    }
}
