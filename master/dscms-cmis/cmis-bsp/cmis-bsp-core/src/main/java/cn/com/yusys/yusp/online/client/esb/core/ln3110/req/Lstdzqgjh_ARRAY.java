package cn.com.yusys.yusp.online.client.esb.core.ln3110.req;

import java.util.List;

/**
 * 请求Service：贷款定制期供计划表
 *
 * @author code-generator
 * @version 1.0
 */
public class Lstdzqgjh_ARRAY {
    private List<LstdzqgjhRecord> record;//贷款定制期供计划表

    public List<LstdzqgjhRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LstdzqgjhRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdzqgjh{" +
                "record=" + record +
                '}';
    }
}
