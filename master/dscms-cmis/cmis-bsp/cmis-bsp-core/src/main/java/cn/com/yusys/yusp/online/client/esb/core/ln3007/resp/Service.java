package cn.com.yusys.yusp.online.client.esb.core.ln3007.resp;

/**
 * 响应Service：资产产品币种查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否
    private Listnm listnm;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Listnm getListnm() {
        return listnm;
    }

    public void setListnm(Listnm listnm) {
        this.listnm = listnm;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", listnm=" + listnm +
                '}';
    }
}
