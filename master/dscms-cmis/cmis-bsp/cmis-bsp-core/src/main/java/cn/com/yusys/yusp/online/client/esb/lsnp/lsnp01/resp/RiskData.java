package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Service：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class RiskData implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "busi_type")
    private String busi_type;//业务品种
    @JsonProperty(value = "inpret_val")
    private Integer inpret_val;//数字解读值
    @JsonProperty(value = "inpret_val_risk_lvl")
    private String inpret_val_risk_lvl;//数字解读值风险等级
    @JsonProperty(value = "apply_score")
    private Integer apply_score;//申请评分
    @JsonProperty(value = "apply_score_risk_lvl")
    private String apply_score_risk_lvl;//申请评分风险等级
    @JsonProperty(value = "lmt_advice")
    private BigDecimal lmt_advice;//额度建议
    @JsonProperty(value = "limit_flag")
    private String limit_flag;//自动化审批标志
    @JsonProperty(value = "price_advice")
    private BigDecimal price_advice;//定价建议
    @JsonProperty(value = "rule_risk_lvl")
    private String rule_risk_lvl;//规则风险等级
    @JsonProperty(value = "complex_risk_lvl")
    private String complex_risk_lvl;//综合风险等级

    public String getBusi_type() {
        return busi_type;
    }

    public void setBusi_type(String busi_type) {
        this.busi_type = busi_type;
    }

    public Integer getInpret_val() {
        return inpret_val;
    }

    public void setInpret_val(Integer inpret_val) {
        this.inpret_val = inpret_val;
    }

    public String getInpret_val_risk_lvl() {
        return inpret_val_risk_lvl;
    }

    public void setInpret_val_risk_lvl(String inpret_val_risk_lvl) {
        this.inpret_val_risk_lvl = inpret_val_risk_lvl;
    }

    public Integer getApply_score() {
        return apply_score;
    }

    public void setApply_score(Integer apply_score) {
        this.apply_score = apply_score;
    }

    public String getApply_score_risk_lvl() {
        return apply_score_risk_lvl;
    }

    public void setApply_score_risk_lvl(String apply_score_risk_lvl) {
        this.apply_score_risk_lvl = apply_score_risk_lvl;
    }

    public BigDecimal getLmt_advice() {
        return lmt_advice;
    }

    public void setLmt_advice(BigDecimal lmt_advice) {
        this.lmt_advice = lmt_advice;
    }

    public String getLimit_flag() {
        return limit_flag;
    }

    public void setLimit_flag(String limit_flag) {
        this.limit_flag = limit_flag;
    }

    public BigDecimal getPrice_advice() {
        return price_advice;
    }

    public void setPrice_advice(BigDecimal price_advice) {
        this.price_advice = price_advice;
    }

    public String getRule_risk_lvl() {
        return rule_risk_lvl;
    }

    public void setRule_risk_lvl(String rule_risk_lvl) {
        this.rule_risk_lvl = rule_risk_lvl;
    }

    public String getComplex_risk_lvl() {
        return complex_risk_lvl;
    }

    public void setComplex_risk_lvl(String complex_risk_lvl) {
        this.complex_risk_lvl = complex_risk_lvl;
    }

    @Override
    public String toString() {
        return "RiskData{" +
                "busi_type='" + busi_type + '\'' +
                ", inpret_val=" + inpret_val +
                ", inpret_val_risk_lvl='" + inpret_val_risk_lvl + '\'' +
                ", apply_score=" + apply_score +
                ", apply_score_risk_lvl='" + apply_score_risk_lvl + '\'' +
                ", lmt_advice=" + lmt_advice +
                ", limit_flag='" + limit_flag + '\'' +
                ", price_advice=" + price_advice +
                ", rule_risk_lvl='" + rule_risk_lvl + '\'' +
                ", complex_risk_lvl='" + complex_risk_lvl + '\'' +
                '}';
    }
}
