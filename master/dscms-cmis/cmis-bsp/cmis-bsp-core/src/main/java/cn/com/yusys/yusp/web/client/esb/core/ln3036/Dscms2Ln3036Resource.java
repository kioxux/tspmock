package cn.com.yusys.yusp.web.client.esb.core.ln3036;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3036.req.Ln3036ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Ln3036RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3036.req.Ln3036ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Ln3036RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3036)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3036Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3036Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * 交易码：ln3036
     * 交易描述：针对录入、复核模式的交易，用于查询信贷录入信息的查询，复核时使用
     *
     * @param ln3036ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3036:针对录入、复核模式的交易，用于查询信贷录入信息的查询，复核时使用")
    @PostMapping("/ln3036")
    protected @ResponseBody
    ResultDto<Ln3036RespDto> ln3036(@Validated @RequestBody Ln3036ReqDto ln3036ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3036.key, EsbEnum.TRADE_CODE_LN3036.value, JSON.toJSONString(ln3036ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3036.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3036.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Service();
        Ln3036ReqService ln3036ReqService = new Ln3036ReqService();
        Ln3036RespService ln3036RespService = new Ln3036RespService();
        Ln3036RespDto ln3036RespDto = new Ln3036RespDto();
        ResultDto<Ln3036RespDto> ln3036ResultDto = new ResultDto<Ln3036RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3036ReqDto转换成reqService
            BeanUtils.copyProperties(ln3036ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3036.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3036ReqService.setService(reqService);
            // 将ln3036ReqService转换成ln3036ReqServiceMap
            Map ln3036ReqServiceMap = beanMapUtil.beanToMap(ln3036ReqService);
            context.put("tradeDataMap", ln3036ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3036.key, EsbEnum.TRADE_CODE_LN3036.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3036.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3036.key, EsbEnum.TRADE_CODE_LN3036.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3036RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3036RespService.class, Ln3036RespService.class);
            respService = ln3036RespService.getService();

            ln3036ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3036ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3036RespDto
                BeanUtils.copyProperties(respService, ln3036RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkbjfd_ARRAY lstdkbjfd_ARRAY = Optional.ofNullable(respService.getLstdkbjfd_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkbjfd_ARRAY());
                respService.setLstdkbjfd_ARRAY(lstdkbjfd_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkbjfd_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkbjfd.Record> ln3036RespLstdkbjfdRecordList = Optional.ofNullable(respService.getLstdkbjfd_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkbjfd> ln3036RespDtoLstdkbjfdList = ln3036RespLstdkbjfdRecordList.parallelStream().map(
                            ln3036RespLstdkbjfdRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkbjfd ln3036RespDtoLstdkbjfd = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkbjfd();
                                BeanUtils.copyProperties(ln3036RespLstdkbjfdRecord, ln3036RespDtoLstdkbjfd);
                                return ln3036RespDtoLstdkbjfd;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkbjfd(ln3036RespDtoLstdkbjfdList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkfkjh_ARRAY lstdkfkjh_ARRAY = Optional.ofNullable(respService.getLstdkfkjh_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkfkjh_ARRAY());
                respService.setLstdkfkjh_ARRAY(lstdkfkjh_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkfkjh_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfkjh.Record> ln3036RespLstdkfkjhRecordList = Optional.ofNullable(respService.getLstdkfkjh_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkfkjh> ln3036RespDtoLstdkfkjhList = ln3036RespLstdkfkjhRecordList.parallelStream().map(
                            ln3036RespLstdkfkjhRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkfkjh ln3036RespDtoLstdkfkjh = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkfkjh();
                                BeanUtils.copyProperties(ln3036RespLstdkfkjhRecord, ln3036RespDtoLstdkfkjh);
                                return ln3036RespDtoLstdkfkjh;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkfkjh(ln3036RespDtoLstdkfkjhList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkfwdj_ARRAY lstdkfwdj_ARRAY = Optional.ofNullable(respService.getLstdkfwdj_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkfwdj_ARRAY());
                respService.setLstdkfwdj_ARRAY(lstdkfwdj_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkfwdj_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkfwdj.Record> ln3036RespLstdkfwdjRecordList = Optional.ofNullable(respService.getLstdkfwdj_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkfwdj> ln3036RespDtoLstdkfwdjList = ln3036RespLstdkfwdjRecordList.parallelStream().map(
                            ln3036RespLstdkfwdjRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkfwdj ln3036RespDtoLstdkfwdj = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkfwdj();
                                BeanUtils.copyProperties(ln3036RespLstdkfwdjRecord, ln3036RespDtoLstdkfwdj);
                                return ln3036RespDtoLstdkfwdj;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkfwdj(ln3036RespDtoLstdkfwdjList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkhbjh_ARRAY lstdkhbjh_ARRAY = Optional.ofNullable(respService.getLstdkhbjh_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkhbjh_ARRAY());
                respService.setLstdkhbjh_ARRAY(lstdkhbjh_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkhbjh_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhbjh.Record> ln3036RespLstdkhbjhRecordList = Optional.ofNullable(respService.getLstdkhbjh_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhbjh> ln3036RespDtoLstdkhbjhList = ln3036RespLstdkhbjhRecordList.parallelStream().map(
                            ln3036RespLstdkhbjhRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhbjh ln3036RespDtoLstdkhbjh = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhbjh();
                                BeanUtils.copyProperties(ln3036RespLstdkhbjhRecord, ln3036RespDtoLstdkhbjh);
                                return ln3036RespDtoLstdkhbjh;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkhbjh(ln3036RespDtoLstdkhbjhList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkhkfs_ARRAY lstdkhkfs_ARRAY = Optional.ofNullable(respService.getLstdkhkfs_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkhkfs_ARRAY());
                respService.setLstdkhkfs_ARRAY(lstdkhkfs_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkhkfs_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkfs.Record> ln3036RespLstdkhkfsRecordList = Optional.ofNullable(respService.getLstdkhkfs_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhkfs> ln3036RespDtoLstdkhkfsList = ln3036RespLstdkhkfsRecordList.parallelStream().map(
                            ln3036RespLstdkhkfsRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhkfs ln3036RespDtoLstdkhkfs = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhkfs();
                                BeanUtils.copyProperties(ln3036RespLstdkhkfsRecord, ln3036RespDtoLstdkhkfs);
                                return ln3036RespDtoLstdkhkfs;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkhkfs(ln3036RespDtoLstdkhkfsList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkhkzh_ARRAY lstdkhkzh_ARRAY = Optional.ofNullable(respService.getLstdkhkzh_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkhkzh_ARRAY());
                respService.setLstdkhkzh_ARRAY(lstdkhkzh_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkhkzh_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkzh.Record> ln3036RespLstdkhkzhRecordList = Optional.ofNullable(respService.getLstdkhkzh_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhkzh> ln3036RespDtoLstdkhkzhList = ln3036RespLstdkhkzhRecordList.parallelStream().map(
                            ln3036RespLstdkhkzhRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhkzh ln3036RespDtoLstdkhkzh = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkhkzh();
                                BeanUtils.copyProperties(ln3036RespLstdkhkzhRecord, ln3036RespDtoLstdkhkzh);
                                return ln3036RespDtoLstdkhkzh;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkhkzh(ln3036RespDtoLstdkhkzhList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdklhmx_ARRAY lstdklhmx_ARRAY = Optional.ofNullable(respService.getLstdklhmx_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdklhmx_ARRAY());
                respService.setLstdklhmx_ARRAY(lstdklhmx_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdklhmx_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdklhmx.Record> ln3036RespLstdklhmxRecordList = Optional.ofNullable(respService.getLstdklhmx_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdklhmx> ln3036RespDtoLstdklhmxList = ln3036RespLstdklhmxRecordList.parallelStream().map(
                            ln3036RespLstdklhmxRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdklhmx ln3036RespDtoLstdklhmx = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdklhmx();
                                BeanUtils.copyProperties(ln3036RespLstdklhmxRecord, ln3036RespDtoLstdklhmx);
                                return ln3036RespDtoLstdklhmx;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdklhmx(ln3036RespDtoLstdklhmxList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkllfd_ARRAY lstdkllfd_ARRAY = Optional.ofNullable(respService.getLstdkllfd_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkllfd_ARRAY());
                respService.setLstdkllfd_ARRAY(lstdkllfd_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkllfd_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkllfd.Record> ln3036RespLstdkllfdRecordList = Optional.ofNullable(respService.getLstdkllfd_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkllfd> ln3036RespDtoLstdkllfdList = ln3036RespLstdkllfdRecordList.parallelStream().map(
                            ln3036RespLstdkllfdRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkllfd ln3036RespDtoLstdkllfd = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkllfd();
                                BeanUtils.copyProperties(ln3036RespLstdkllfdRecord, ln3036RespDtoLstdkllfd);
                                return ln3036RespDtoLstdkllfd;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkllfd(ln3036RespDtoLstdkllfdList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdksfsj_ARRAY lstdksfsj_ARRAY = Optional.ofNullable(respService.getLstdksfsj_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdksfsj_ARRAY());
                respService.setLstdksfsj_ARRAY(lstdksfsj_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdksfsj_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdksfsj.Record> ln3036RespLstdksfsjRecordList = Optional.ofNullable(respService.getLstdksfsj_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdksfsj> ln3036RespDtoLstdksfsjList = ln3036RespLstdksfsjRecordList.parallelStream().map(
                            ln3036RespLstdksfsjRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdksfsj ln3036RespDtoLstdksfsj = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdksfsj();
                                BeanUtils.copyProperties(ln3036RespLstdksfsjRecord, ln3036RespDtoLstdksfsj);
                                return ln3036RespDtoLstdksfsj;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdksfsj(ln3036RespDtoLstdksfsjList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkstzf_ARRAY lstdkstzf_ARRAY = Optional.ofNullable(respService.getLstdkstzf_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkstzf_ARRAY());
                respService.setLstdkstzf_ARRAY(lstdkstzf_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkstzf_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkstzf.Record> ln3036RespLstdkstzfRecordList = Optional.ofNullable(respService.getLstdkstzf_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkstzf> ln3036RespDtoLstdkstzfList = ln3036RespLstdkstzfRecordList.parallelStream().map(
                            ln3036RespLstdkstzfRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkstzf ln3036RespDtoLstdkstzf = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkstzf();
                                BeanUtils.copyProperties(ln3036RespLstdkstzfRecord, ln3036RespDtoLstdkstzf);
                                return ln3036RespDtoLstdkstzf;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkstzf(ln3036RespDtoLstdkstzfList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdktxzh_ARRAY lstdktxzh_ARRAY = Optional.ofNullable(respService.getLstdktxzh_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdktxzh_ARRAY());
                respService.setLstdktxzh_ARRAY(lstdktxzh_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdktxzh_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdktxzh.Record> ln3036RespLstdktxzhRecordList = Optional.ofNullable(respService.getLstdktxzh_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdktxzh> ln3036RespDtoLstdktxzhList = ln3036RespLstdktxzhRecordList.parallelStream().map(
                            ln3036RespLstdktxzhRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdktxzh ln3036RespDtoLstdktxzh = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdktxzh();
                                BeanUtils.copyProperties(ln3036RespLstdktxzhRecord, ln3036RespDtoLstdktxzh);
                                return ln3036RespDtoLstdktxzh;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdktxzh(ln3036RespDtoLstdktxzhList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkwtxx_ARRAY lstdkwtxx_ARRAY = Optional.ofNullable(respService.getLstdkwtxx_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkwtxx_ARRAY());
                respService.setLstdkwtxx_ARRAY(lstdkwtxx_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkwtxx_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkwtxx.Record> ln3036RespLstdkwtxxRecordList = Optional.ofNullable(respService.getLstdkwtxx_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkwtxx> ln3036RespDtoLstdkwtxxList = ln3036RespLstdkwtxxRecordList.parallelStream().map(
                            ln3036RespLstdkwtxxRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkwtxx ln3036RespDtoLstdkwtxx = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkwtxx();
                                BeanUtils.copyProperties(ln3036RespLstdkwtxxRecord, ln3036RespDtoLstdkwtxx);
                                return ln3036RespDtoLstdkwtxx;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkwtxx(ln3036RespDtoLstdkwtxxList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkzhbz_ARRAY lstdkzhbz_ARRAY = Optional.ofNullable(respService.getLstdkzhbz_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkzhbz_ARRAY());
                respService.setLstdkzhbz_ARRAY(lstdkzhbz_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkzhbz_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkzhbz.Record> ln3036RespLstdkzhbzRecordList = Optional.ofNullable(respService.getLstdkzhbz_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhbz> ln3036RespDtoLstdkzhbzList = ln3036RespLstdkzhbzRecordList.parallelStream().map(
                            ln3036RespLstdkzhbzRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhbz ln3036RespDtoLstdkzhbz = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhbz();
                                BeanUtils.copyProperties(ln3036RespLstdkzhbzRecord, ln3036RespDtoLstdkzhbz);
                                return ln3036RespDtoLstdkzhbz;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkzhbz(ln3036RespDtoLstdkzhbzList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkzhlm_ARRAY lstdkzhlm_ARRAY = Optional.ofNullable(respService.getLstdkzhlm_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkzhlm_ARRAY());
                respService.setLstdkzhlm_ARRAY(lstdkzhlm_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkzhlm_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkzhlm.Record> ln3036RespLstdkzhlmRecordList = Optional.ofNullable(respService.getLstdkzhlm_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhlm> ln3036RespDtoLstdkzhlmList = ln3036RespLstdkzhlmRecordList.parallelStream().map(
                            ln3036RespLstdkzhlmRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhlm ln3036RespDtoLstdkzhlm = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhlm();
                                BeanUtils.copyProperties(ln3036RespLstdkzhlmRecord, ln3036RespDtoLstdkzhlm);
                                return ln3036RespDtoLstdkzhlm;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkzhlm(ln3036RespDtoLstdkzhlmList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkzhzy_ARRAY lstdkzhzy_ARRAY = Optional.ofNullable(respService.getLstdkzhzy_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdkzhzy_ARRAY());
                respService.setLstdkzhzy_ARRAY(lstdkzhzy_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdkzhzy_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkzhzy.Record> ln3036RespLstdkzhzyRecordList = Optional.ofNullable(respService.getLstdkzhzy_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhzy> ln3036RespDtoLstdkzhzyList = ln3036RespLstdkzhzyRecordList.parallelStream().map(
                            ln3036RespLstdkzhzyRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhzy ln3036RespDtoLstdkzhzy = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdkzhzy();
                                BeanUtils.copyProperties(ln3036RespLstdkzhzyRecord, ln3036RespDtoLstdkzhzy);
                                return ln3036RespDtoLstdkzhzy;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdkzhzy(ln3036RespDtoLstdkzhzyList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdzqgjh_ARRAY lstdzqgjh_ARRAY = Optional.ofNullable(respService.getLstdzqgjh_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstdzqgjh_ARRAY());
                respService.setLstdzqgjh_ARRAY(lstdzqgjh_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstdzqgjh_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdzqgjh.Record> ln3036RespLstdzqgjhRecordList = Optional.ofNullable(respService.getLstdzqgjh_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdzqgjh> ln3036RespDtoLstdzqgjhList = ln3036RespLstdzqgjhRecordList.parallelStream().map(
                            ln3036RespLstdzqgjhRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdzqgjh ln3036RespDtoLstdzqgjh = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstdzqgjh();
                                BeanUtils.copyProperties(ln3036RespLstdzqgjhRecord, ln3036RespDtoLstdzqgjh);
                                return ln3036RespDtoLstdzqgjh;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstdzqgjh(ln3036RespDtoLstdzqgjhList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstydkjjh_ARRAY lstydkjjh_ARRAY = Optional.ofNullable(respService.getLstydkjjh_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstydkjjh_ARRAY());
                respService.setLstydkjjh_ARRAY(lstydkjjh_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstydkjjh_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstydkjjh.Record> ln3036RespLstydkjjhRecordList = Optional.ofNullable(respService.getLstydkjjh_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstydkjjh> ln3036RespDtoLstydkjjhList = ln3036RespLstydkjjhRecordList.parallelStream().map(
                            ln3036RespLstydkjjhRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstydkjjh ln3036RespDtoLstydkjjh = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstydkjjh();
                                BeanUtils.copyProperties(ln3036RespLstydkjjhRecord, ln3036RespDtoLstydkjjh);
                                return ln3036RespDtoLstydkjjh;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstydkjjh(ln3036RespDtoLstydkjjhList);
                }
                cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstytczfe_ARRAY lstytczfe_ARRAY = Optional.ofNullable(respService.getLstytczfe_ARRAY())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.Lstytczfe_ARRAY());
                respService.setLstytczfe_ARRAY(lstytczfe_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstytczfe_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstytczfe.Record> ln3036RespLstytczfeRecordList = Optional.ofNullable(respService.getLstytczfe_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstytczfe> ln3036RespDtoLstytczfeList = ln3036RespLstytczfeRecordList.parallelStream().map(
                            ln3036RespLstytczfeRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstytczfe ln3036RespDtoLstytczfe = new cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Lstytczfe();
                                BeanUtils.copyProperties(ln3036RespLstytczfeRecord, ln3036RespDtoLstytczfe);
                                return ln3036RespDtoLstytczfe;
                            }).collect(Collectors.toList());
                    ln3036RespDto.setLstytczfe(ln3036RespDtoLstytczfeList);
                }

                ln3036ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3036ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3036ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3036ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3036.key, EsbEnum.TRADE_CODE_LN3036.value, e.getMessage());
            ln3036ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3036ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3036ResultDto.setData(ln3036RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3036.key, EsbEnum.TRADE_CODE_LN3036.value, JSON.toJSONString(ln3036ResultDto));
        return ln3036ResultDto;
    }
}
