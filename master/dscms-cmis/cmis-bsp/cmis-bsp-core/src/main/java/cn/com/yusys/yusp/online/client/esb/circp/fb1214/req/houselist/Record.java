package cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.houselist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 20:13
 * @since 2021/8/10 20:13
 */
public class Record {

    @JsonProperty(value = "GUAR_NO")
    private String GUAR_NO;//押品统一编号
    @JsonProperty(value = "HOUSE_AREA")
    private BigDecimal HOUSE_AREA;//房产面积
    @JsonProperty(value = "HOUSE_RIGHT_NO")
    private String HOUSE_RIGHT_NO;//产权证号
    @JsonProperty(value = "EVAL_VALUE")
    private BigDecimal EVAL_VALUE;//评估价值
    @JsonProperty(value = "BICYCLE_GARAGE_AREA")
    private BigDecimal BICYCLE_GARAGE_AREA;//自行车库面积
    @JsonProperty(value = "PARK_SPACE_AREA")
    private BigDecimal PARK_SPACE_AREA;//车位面积
    @JsonProperty(value = "LOFT_AREA")
    private BigDecimal LOFT_AREA;//阁楼面积

    @JsonIgnore
    public String getGUAR_NO() {
        return GUAR_NO;
    }

    @JsonIgnore
    public void setGUAR_NO(String GUAR_NO) {
        this.GUAR_NO = GUAR_NO;
    }

    @JsonIgnore
    public BigDecimal getHOUSE_AREA() {
        return HOUSE_AREA;
    }

    @JsonIgnore
    public void setHOUSE_AREA(BigDecimal HOUSE_AREA) {
        this.HOUSE_AREA = HOUSE_AREA;
    }

    @JsonIgnore
    public String getHOUSE_RIGHT_NO() {
        return HOUSE_RIGHT_NO;
    }

    @JsonIgnore
    public void setHOUSE_RIGHT_NO(String HOUSE_RIGHT_NO) {
        this.HOUSE_RIGHT_NO = HOUSE_RIGHT_NO;
    }

    @JsonIgnore
    public BigDecimal getEVAL_VALUE() {
        return EVAL_VALUE;
    }

    @JsonIgnore
    public void setEVAL_VALUE(BigDecimal EVAL_VALUE) {
        this.EVAL_VALUE = EVAL_VALUE;
    }

    @JsonIgnore
    public BigDecimal getBICYCLE_GARAGE_AREA() {
        return BICYCLE_GARAGE_AREA;
    }

    @JsonIgnore
    public void setBICYCLE_GARAGE_AREA(BigDecimal BICYCLE_GARAGE_AREA) {
        this.BICYCLE_GARAGE_AREA = BICYCLE_GARAGE_AREA;
    }

    @JsonIgnore
    public BigDecimal getPARK_SPACE_AREA() {
        return PARK_SPACE_AREA;
    }

    @JsonIgnore
    public void setPARK_SPACE_AREA(BigDecimal PARK_SPACE_AREA) {
        this.PARK_SPACE_AREA = PARK_SPACE_AREA;
    }

    @JsonIgnore
    public BigDecimal getLOFT_AREA() {
        return LOFT_AREA;
    }

    @JsonIgnore
    public void setLOFT_AREA(BigDecimal LOFT_AREA) {
        this.LOFT_AREA = LOFT_AREA;
    }

    @Override
    public String toString() {
        return "HOUSE_LIST{" +
                "GUAR_NO='" + GUAR_NO + '\'' +
                ", HOUSE_AREA=" + HOUSE_AREA +
                ", HOUSE_RIGHT_NO='" + HOUSE_RIGHT_NO + '\'' +
                ", EVAL_VALUE=" + EVAL_VALUE +
                ", BICYCLE_GARAGE_AREA=" + BICYCLE_GARAGE_AREA +
                ", PARK_SPACE_AREA=" + PARK_SPACE_AREA +
                ", LOFT_AREA=" + LOFT_AREA +
                '}';
    }
}
