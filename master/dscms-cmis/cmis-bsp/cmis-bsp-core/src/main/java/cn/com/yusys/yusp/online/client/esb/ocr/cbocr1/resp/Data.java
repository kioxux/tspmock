package cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp;

import java.util.List;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 10:29
 * @since 2021/6/4 10:29
 */
public class Data {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Data{" +
                "record=" + record +
                '}';
    }
}
