package cn.com.yusys.yusp.web.client.esb.doc.doc005;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.req.Doc005ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.req.IndexData;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.resp.Doc005RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.doc.doc005.req.Doc005ReqService;
import cn.com.yusys.yusp.online.client.esb.doc.doc005.resp.Doc005RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(doc005)")
@RestController
@RequestMapping("/api/dscms2doc")
public class Dscms2Doc005Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Doc005Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 申请出库（处理码doc005）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("doc005:申请出库")
    @PostMapping("/doc005")
    protected @ResponseBody
    ResultDto<Doc005RespDto> doc005(@Validated @RequestBody Doc005ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC005.key, EsbEnum.TRADE_CODE_DOC005.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.doc.doc005.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.doc.doc005.req.Service();
        cn.com.yusys.yusp.online.client.esb.doc.doc005.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.doc.doc005.resp.Service();
        cn.com.yusys.yusp.online.client.esb.doc.doc005.req.IndexData indexData = new cn.com.yusys.yusp.online.client.esb.doc.doc005.req.IndexData();
        Doc005ReqService doc005ReqService = new Doc005ReqService();
        Doc005RespService doc005RespService = new Doc005RespService();
        Doc005RespDto doc005RespDto = new Doc005RespDto();
        ResultDto<Doc005RespDto> doc005ResultDto = new ResultDto<Doc005RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Doc005ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            if (CollectionUtils.nonEmpty(reqDto.getIndexData())) {
                List<IndexData> indexDataList = reqDto.getIndexData();
                List<cn.com.yusys.yusp.online.client.esb.doc.doc005.req.Record> recordList = new ArrayList<>();
                for (IndexData i : indexDataList) {
                    cn.com.yusys.yusp.online.client.esb.doc.doc005.req.Record record = new cn.com.yusys.yusp.online.client.esb.doc.doc005.req.Record();
                    BeanUtils.copyProperties(i, record);
                    recordList.add(record);
                }
                indexData.setRecord(recordList);
                reqService.setList_index(indexData);
            }

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DOC005.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            doc005ReqService.setService(reqService);
            // 将doc005ReqService转换成doc005ReqServiceMap
            Map doc005ReqServiceMap = beanMapUtil.beanToMap(doc005ReqService);
            context.put("tradeDataMap", doc005ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC005.key, EsbEnum.TRADE_CODE_DOC005.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DOC005.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC005.key, EsbEnum.TRADE_CODE_DOC005.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            doc005RespService = beanMapUtil.mapToBean(tradeDataMap, Doc005RespService.class, Doc005RespService.class);
            respService = doc005RespService.getService();

            //  将Doc005RespDto封装到ResultDto<Doc005RespDto>
            doc005ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            doc005ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Doc005RespDto
                BeanUtils.copyProperties(respService, doc005RespDto);
                doc005ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                doc005ResultDto.setMessage(respService.getErortx());
            } else {
                doc005ResultDto.setCode(EpbEnum.EPB099999.key);
                doc005ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC005.key, EsbEnum.TRADE_CODE_DOC005.value, e.getMessage());
            doc005ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            doc005ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        doc005ResultDto.setData(doc005RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DOC005.key, EsbEnum.TRADE_CODE_DOC005.value, JSON.toJSONString(doc005ResultDto));
        return doc005ResultDto;
    }
}
