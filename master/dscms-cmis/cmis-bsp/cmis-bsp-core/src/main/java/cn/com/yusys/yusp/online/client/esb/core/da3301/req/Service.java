package cn.com.yusys.yusp.online.client.esb.core.da3301.req;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产入账
 */
public class Service {
    private String prcscd;//交易码
    private String servtp;//渠道
    private String datasq; //全局流水
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间

    private String daikczbz;//业务操作标志
    private String dizruzfs;//抵债入账方式
    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String yngyjigo;//营业机构
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private BigDecimal dcldzcje;//待处理抵债资产金额
    private BigDecimal hfeijine;//还费金额
    private String zijinqux;//资金去向
    private String dzzszhao;//抵债暂收账号
    private String dzzszzxh;//抵债暂收账号子序号
    private String dzzcqdfs;//抵债资产取得方式
    private BigDecimal huankjee;//还款金额
    private Listnm0 listnm0_ARRAY;//抵债资产费用明细
    private Listnm1 listnm1_ARRAY;//抵债资产与贷款关联表

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDizruzfs() {
        return dizruzfs;
    }

    public void setDizruzfs(String dizruzfs) {
        this.dizruzfs = dizruzfs;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getDcldzcje() {
        return dcldzcje;
    }

    public void setDcldzcje(BigDecimal dcldzcje) {
        this.dcldzcje = dcldzcje;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public String getZijinqux() {
        return zijinqux;
    }

    public void setZijinqux(String zijinqux) {
        this.zijinqux = zijinqux;
    }

    public String getDzzszhao() {
        return dzzszhao;
    }

    public void setDzzszhao(String dzzszhao) {
        this.dzzszhao = dzzszhao;
    }

    public String getDzzszzxh() {
        return dzzszzxh;
    }

    public void setDzzszzxh(String dzzszzxh) {
        this.dzzszzxh = dzzszzxh;
    }

    public String getDzzcqdfs() {
        return dzzcqdfs;
    }

    public void setDzzcqdfs(String dzzcqdfs) {
        this.dzzcqdfs = dzzcqdfs;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public Listnm0 getListnm0_ARRAY() {
        return listnm0_ARRAY;
    }

    public void setListnm0_ARRAY(Listnm0 listnm0_ARRAY) {
        this.listnm0_ARRAY = listnm0_ARRAY;
    }

    public Listnm1 getListnm1_ARRAY() {
        return listnm1_ARRAY;
    }

    public void setListnm1_ARRAY(Listnm1 listnm1_ARRAY) {
        this.listnm1_ARRAY = listnm1_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", dizruzfs='" + dizruzfs + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", dcldzcje=" + dcldzcje +
                ", hfeijine=" + hfeijine +
                ", zijinqux='" + zijinqux + '\'' +
                ", dzzszhao='" + dzzszhao + '\'' +
                ", dzzszzxh='" + dzzszzxh + '\'' +
                ", dzzcqdfs='" + dzzcqdfs + '\'' +
                ", huankjee=" + huankjee +
                ", listnm0_ARRAY=" + listnm0_ARRAY +
                ", listnm1_ARRAY=" + listnm1_ARRAY +
                '}';
    }
}
