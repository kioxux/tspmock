package cn.com.yusys.yusp.online.client.esb.ypxt.manage.req;

/**
 * 请求Service：管护权移交信息同步
 *
 * @author lihh
 * @version 1.0
 */
public class ManageReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
