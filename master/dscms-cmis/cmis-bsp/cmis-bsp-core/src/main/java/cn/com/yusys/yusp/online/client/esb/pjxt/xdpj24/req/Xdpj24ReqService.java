package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req;

/**
 * 请求Service：从票据系统获取当日到期票的日出备款金额
 *
 * @author chenyong
 * @version 1.0
 */
public class Xdpj24ReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return "Xdpj24ReqService{" +
				"service=" + service +
				'}';
	}
}
