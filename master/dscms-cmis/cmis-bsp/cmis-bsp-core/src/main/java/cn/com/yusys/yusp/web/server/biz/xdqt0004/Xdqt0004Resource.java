package cn.com.yusys.yusp.web.server.biz.xdqt0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdqt0004.req.Xdqt0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdqt0004.resp.Xdqt0004RespDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.req.Xdqt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0004.resp.Xdqt0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:贷款申请预约（个人客户）
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0004:贷款申请预约（个人客户）")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0004Resource.class);

    @Autowired
	private DscmsBizQtClientService dscmsBizQtClientService;

    /**
     * 交易码：xdqt0004
     * 交易描述：贷款申请预约（个人客户）
     *
     * @param xdqt0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贷款申请预约（个人客户）")
    @PostMapping("/xdqt0004")
    //@Idempotent({"xdcaqt0004", "#xdqt0004ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0004RespDto xdqt0004(@Validated @RequestBody Xdqt0004ReqDto xdqt0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, JSON.toJSONString(xdqt0004ReqDto));
        Xdqt0004DataReqDto xdqt0004DataReqDto = new Xdqt0004DataReqDto();// 请求Data： 贷款申请预约（个人客户）
        Xdqt0004DataRespDto xdqt0004DataRespDto = new Xdqt0004DataRespDto();// 响应Data：贷款申请预约（个人客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0004.req.Data reqData = null; // 请求Data：贷款申请预约（个人客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdqt0004.resp.Data();// 响应Data：贷款申请预约（个人客户）
		Xdqt0004RespDto xdqt0004RespDto = new Xdqt0004RespDto();
		try {
            // 从 xdqt0004ReqDto获取 reqData
            reqData = xdqt0004ReqDto.getData();
            // 将 reqData 拷贝到xdqt0004DataReqDto
            BeanUtils.copyProperties(reqData, xdqt0004DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, JSON.toJSONString(xdqt0004DataReqDto));
            ResultDto<Xdqt0004DataRespDto> xdqt0004DataResultDto = dscmsBizQtClientService.xdqt0004(xdqt0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, JSON.toJSONString(xdqt0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0004RespDto.setErorcd(Optional.ofNullable(xdqt0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0004RespDto.setErortx(Optional.ofNullable(xdqt0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0004DataResultDto.getCode())) {
                xdqt0004DataRespDto = xdqt0004DataResultDto.getData();
                xdqt0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, e.getMessage());
            xdqt0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0004RespDto.setDatasq(servsq);

        xdqt0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0004.key, DscmsEnum.TRADE_CODE_XDQT0004.value, JSON.toJSONString(xdqt0004RespDto));
        return xdqt0004RespDto;
    }
}
