package cn.com.yusys.yusp.web.client.esb.core.da3305;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3305.req.Da3305ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3305.resp.Da3305RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3305.req.Da3305ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3305.resp.Da3305RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:待变现抵债资产销账
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class DscmsDa3305Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsDa3305Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3305
     * 交易描述：待变现抵债资产销账
     *
     * @param da3305ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3305")
    protected @ResponseBody
    ResultDto<Da3305RespDto> da3305(@Validated @RequestBody Da3305ReqDto da3305ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3305.key, EsbEnum.TRADE_CODE_DA3305.value, JSON.toJSONString(da3305ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3305.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3305.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3305.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3305.resp.Service();

        Da3305ReqService da3305ReqService = new Da3305ReqService();
        Da3305RespService da3305RespService = new Da3305RespService();
        Da3305RespDto da3305RespDto = new Da3305RespDto();
        ResultDto<Da3305RespDto> da3305ResultDto = new ResultDto<Da3305RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3305ReqDto转换成reqService
            BeanUtils.copyProperties(da3305ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3305.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3305ReqService.setService(reqService);
            // 将da3305ReqService转换成da3305ReqServiceMap
            Map da3305ReqServiceMap = beanMapUtil.beanToMap(da3305ReqService);
            context.put("tradeDataMap", da3305ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3305.key, EsbEnum.TRADE_CODE_DA3305.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3305.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3305.key, EsbEnum.TRADE_CODE_DA3305.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3305RespService = beanMapUtil.mapToBean(tradeDataMap, Da3305RespService.class, Da3305RespService.class);
            respService = da3305RespService.getService();


            da3305ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3305ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3305RespDto
                BeanUtils.copyProperties(respService, da3305RespDto);
                da3305ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3305ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3305ResultDto.setCode(EpbEnum.EPB099999.key);
                da3305ResultDto.setMessage(respService.getErortx());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3305.key, EsbEnum.TRADE_CODE_DA3305.value, e.getMessage());
            da3305ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3305ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3305ResultDto.setData(da3305RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3305.key, EsbEnum.TRADE_CODE_DA3305.value, JSON.toJSONString(da3305ResultDto));
        return da3305ResultDto;
    }
}
