package cn.com.yusys.yusp.web.client.esb.circp.fb1168;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.req.Fb1168ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.resp.Fb1168RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1168.req.Fb1168ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1168.resp.Fb1168RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1168）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1168Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1168Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1168
     * 交易描述：抵押查封结果推送
     *
     * @param fb1168ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1168")
    protected @ResponseBody
    ResultDto<Fb1168RespDto> fb1168(@Validated @RequestBody Fb1168ReqDto fb1168ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1168.key, EsbEnum.TRADE_CODE_FB1168.value, JSON.toJSONString(fb1168ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1168.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1168.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1168.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1168.resp.Service();
        Fb1168ReqService fb1168ReqService = new Fb1168ReqService();
        Fb1168RespService fb1168RespService = new Fb1168RespService();
        Fb1168RespDto fb1168RespDto = new Fb1168RespDto();
        ResultDto<Fb1168RespDto> fb1168ResultDto = new ResultDto<Fb1168RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1168ReqDto转换成reqService
            BeanUtils.copyProperties(fb1168ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1168.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1168ReqService.setService(reqService);
            // 将fb1168ReqService转换成fb1168ReqServiceMap
            Map fb1168ReqServiceMap = beanMapUtil.beanToMap(fb1168ReqService);
            context.put("tradeDataMap", fb1168ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1168.key, EsbEnum.TRADE_CODE_FB1168.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1168.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1168.key, EsbEnum.TRADE_CODE_FB1168.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1168RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1168RespService.class, Fb1168RespService.class);
            respService = fb1168RespService.getService();

            fb1168ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1168ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1168RespDto
                BeanUtils.copyProperties(respService, fb1168RespDto);

                fb1168ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1168ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1168ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1168ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1168.key, EsbEnum.TRADE_CODE_FB1168.value, e.getMessage());
            fb1168ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1168ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1168ResultDto.setData(fb1168RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1168.key, EsbEnum.TRADE_CODE_FB1168.value, JSON.toJSONString(fb1168ResultDto));
        return fb1168ResultDto;
    }
}
