package cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.resp;

/**
 * 响应Service：信贷审批结果同步接口
 *
 * @author lihh
 * @version 1.0
 */
public class SpsyypRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
