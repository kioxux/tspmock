package cn.com.yusys.yusp.online.client.esb.ecif.g11003.resp;

/**
 * 响应Service：客户集团信息维护 (new)
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class G11003RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
