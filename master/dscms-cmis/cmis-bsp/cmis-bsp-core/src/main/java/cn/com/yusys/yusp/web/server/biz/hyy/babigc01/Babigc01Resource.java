package cn.com.yusys.yusp.web.server.biz.hyy.babigc01;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Babigc01RespDto;
import cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Items;
import cn.com.yusys.yusp.dto.server.biz.hyy.common.*;
import cn.com.yusys.yusp.dto.server.xddb0012.req.Xddb0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.MortgagorsList;
import cn.com.yusys.yusp.dto.server.xddb0012.resp.Xddb0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.Option;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "Babigc:获取抵押信息详情")
@RestController
@RequestMapping("/bank/collaterals")
public class Babigc01Resource {
    private static final Logger logger = LoggerFactory.getLogger(Babigc01Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;


    /**
     * 交易码：babigc
     * 交易描述：获取抵押信息详情
     *
     * @param id 抵押唯一标识（如押品编号#合同编号）
     * @return
     * @throws Exception
     */
    @ApiOperation("获取抵押信息详情")
    @GetMapping("/{id}")
    protected @ResponseBody
    String babigc(@PathVariable("id") String id) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIGC.key, DscmsEnum.TRADE_CODE_BABIGC.value, JSON.toJSONString(id));
        Xddb0012DataReqDto xddb0012DataReqDto = new Xddb0012DataReqDto();// 请求Data： 抵押登记获取押品信息
        Xddb0012DataRespDto xddb0012DataRespDto = new Xddb0012DataRespDto();// 响应Data：抵押登记获取押品信息
        Babigc01RespDto babigc01RespDto = new Babigc01RespDto();
        cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Data hyyBabigc01Data = new cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Data();
        List<Items> hyyBabigc01ItemList = new ArrayList<>();
        cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Items hyyBabigc01Items = new cn.com.yusys.yusp.dto.server.biz.hyy.babigc01.Items();
        // 变量赋值 开始
        String collateral_id = StringUtils.EMPTY;//抵押唯一标识
        Mortgagee mortgagee = new Mortgagee();//抵押权人
        DocumentType mortgageeDocumentType = new DocumentType(); // 抵押权人-证件类型
        MortgageeAgent mortgagee_agent = new MortgageeAgent();//	抵押权人代理人
        DocumentType mortgagee_agentDocumentType = new DocumentType();//抵押权人代理人-证件类型
        List<Mortgagors> mortgagorsList = new ArrayList<>();
        //Mortgagors mortgagors = new Mortgagors();//	抵押人
        MortgagorAgent mortgagorAgent = new MortgagorAgent();
        DocumentType mortgagorAgentDocumentType = new DocumentType(); //抵押人代理人-证件类型
        RealEstateInfo real_estate_info = new RealEstateInfo();//	不动产信息
        MortgageInfo mortgage_info = new MortgageInfo();//抵押信息
        DocumentType mortgage_infoDocumentType = new DocumentType();//抵押信息--证件类型
        DealInfo deal_info = new DealInfo();//交易信息
        //DocumentType documentType = new DocumentType();//证件类型
        District district = new District();//区县代码
        House house = new House();//房屋信息
        //Usage usage = new Usage();//用途
        Usage houseUsage = new Usage();//房屋-用途
        Usage landUsage = new Usage();//土地-用途
        Land land = new Land();//土地信息
        MortgageMethod mortgageMethod = new MortgageMethod();//抵押方式
        Debtor debtor = new Debtor();//债务人
        // 变量赋值 开始
        try {
            xddb0012DataReqDto.setGuarid(id);//押品编号

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataReqDto));
            ResultDto<Xddb0012DataRespDto> xddb0012DataResultDto = dscmsBizDbClientService.xddb0012(xddb0012DataReqDto);
            xddb0012DataResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
            xddb0012DataResultDto.setMessage(SuccessEnum.SUCCESS.value);
            xddb0012DataRespDto.setCollid(id);

            // TODO 增加其他的值
            //xddb0012DataResultDto.setData(xddb0012DataRespDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, JSON.toJSONString(xddb0012DataResultDto));
            // 从返回值中获取响应码和响应消息
            babigc01RespDto.setCode(Optional.ofNullable(xddb0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            babigc01RespDto.setMessage(Optional.ofNullable(xddb0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0012DataResultDto.getCode())) {
                xddb0012DataRespDto = xddb0012DataResultDto.getData();
                babigc01RespDto.setCode(SuccessEnum.CODE_SUCCESS.key);
                babigc01RespDto.setMessage(SuccessEnum.MESSAGE_SUCCESS.key);
                //boolean flag = true;
                babigc01RespDto.setSuccess("true");
                collateral_id = xddb0012DataRespDto.getCollid();
                hyyBabigc01Items.setCollateral_id(collateral_id);//押品编号

                String moname = xddb0012DataRespDto.getMoname();//抵押权人-姓名/名称
                String dotyco = xddb0012DataRespDto.getDotyco();//抵押权人-证件类型-代码
                String dotyna = xddb0012DataRespDto.getDotyna();//抵押权人-证件类型-名称
                String modomu = xddb0012DataRespDto.getModomu();//抵押权人-证件号
                String monalr = xddb0012DataRespDto.getMonalr();//抵押权人-法人/负责人
                String mophon = xddb0012DataRespDto.getMophon();//抵押权人-电话
                mortgagee.setName(moname);
                mortgageeDocumentType.setCode(StringUtils.isNotBlank(dotyco) ? Integer.parseInt(dotyco) : 0);
                mortgageeDocumentType.setName(dotyna);
                mortgagee.setDocument_type(mortgageeDocumentType);
                mortgagee.setDocument_number(modomu);
                mortgagee.setLegal_representative(monalr);
                mortgagee.setPhone(mophon);

                hyyBabigc01Items.setMortgagee(mortgagee);  //抵押权人
                String moagna = xddb0012DataRespDto.getMoagna();//抵押权人代理人-姓名/名称
                String moagtc = xddb0012DataRespDto.getMoagtc();//抵押权人代理人-证件类型-代码
                String moagtn = xddb0012DataRespDto.getMoagtn();//抵押权人代理人-证件类型-名称
                String moagdn = xddb0012DataRespDto.getMoagdn();//抵押权人代理人-证件号
                String moagph = xddb0012DataRespDto.getMoagph();//抵押权人代理人-电话
                mortgagee_agent.setName(moagna);
                mortgagee_agentDocumentType.setCode(StringUtils.isNotBlank(moagtc) ? Integer.parseInt(moagtc) : 0);
                mortgagee_agentDocumentType.setName(moagtn);
                mortgagee_agent.setDocument_type(mortgagee_agentDocumentType);
                mortgagee_agent.setDocument_number(moagdn);
                mortgagee_agent.setPhone(moagph);

                hyyBabigc01Items.setMortgagee_agent(mortgagee_agent);//	抵押权人代理人
                java.util.List<MortgagorsList> mortgagorsLists = xddb0012DataRespDto.getMortgagorsList();
                if (CollectionUtils.nonEmpty(mortgagorsLists)) {
                    for (int i = 0; i < mortgagorsLists.size(); i++) {
                        Mortgagors mortgagors = new Mortgagors();//	抵押人
                        DocumentType mortgagorsDocumentType = new DocumentType();//	抵押人-证件类型
                        String mogana = mortgagorsLists.get(i).getMogana();
                        String modoty = mortgagorsLists.get(i).getModoty();
                        String modonu = mortgagorsLists.get(i).getModonu();
                        String molere = mortgagorsLists.get(i).getMolere();
                        String moleph = mortgagorsLists.get(i).getMoleph();
                        mortgagors.setName(mogana);
                        mortgagorsDocumentType.setCode(StringUtils.isNotBlank(modoty) ? Integer.parseInt(modoty) : 0);
                        modoty = getChangeCertName(modoty);
                        mortgagorsDocumentType.setName(modoty);
                        mortgagors.setDocument_type(mortgagorsDocumentType);
                        mortgagors.setDocument_number(modonu);
                        mortgagors.setLegal_representative(molere);
                        mortgagors.setPhone(moleph);
                        mortgagorsList.add(mortgagors);
                    }
                }
                hyyBabigc01Items.setMortgagors(mortgagorsList);//	抵押人

                String moorna = xddb0012DataRespDto.getMoorna();//抵押人代理人-姓名/名称
                //String moordc = xddb0012DataRespDto.getMoordc();//抵押人代理人-证件类型-代码
                String moordc = "1";
                String moordn = xddb0012DataRespDto.getMoordn();//抵押人代理人-证件类型-名称
                String moordu = xddb0012DataRespDto.getMoordu();//抵押人代理人-证件号
                String moorph = xddb0012DataRespDto.getMoorph();//抵押人代理人-电话
                mortgagorAgent.setName(moorna);
                mortgagorAgentDocumentType.setCode(StringUtils.isNotBlank(moordc) ? Integer.parseInt(moordc) : 0);
                mortgagorAgentDocumentType.setName(moordn);
                mortgagorAgent.setDocument_type(mortgagorAgentDocumentType);
                mortgagorAgent.setDocument_number(moordu);
                mortgagorAgent.setPhone(moorph);

                hyyBabigc01Items.setMortgagor_agent(mortgagorAgent);// 抵押人代理人

                String reeidc = xddb0012DataRespDto.getReeidc();//不动产信息-所属区县-代码
                //String reeidc = "01";
                String reeidn = xddb0012DataRespDto.getReeidn();//不动产信息-所属区县-名称
                String reeilo = xddb0012DataRespDto.getReeilo();//不动产信息-坐落
                String reiheu = xddb0012DataRespDto.getReiheu();//不动产信息-房屋-不动产单元号
                String reihcn = xddb0012DataRespDto.getReihcn();//不动产信息-房屋-产权证书号
                String reihcs = xddb0012DataRespDto.getReihcs();//不动产信息-房屋-产权证书号简称
                BigDecimal reeiha = xddb0012DataRespDto.getReeiha();//不动产信息-房屋-产权面积
                String reihuc = xddb0012DataRespDto.getReihuc();//不动产信息-房屋-用途-代码
                //String reihuc = "01";
                String reihun = xddb0012DataRespDto.getReihun();//不动产信息-房屋-用途-名称
                String reilcn = xddb0012DataRespDto.getReilcn();//不动产信息-土地-证号
                BigDecimal reeila = xddb0012DataRespDto.getReeila();//不动产信息-土地-面积
                String reiluc = xddb0012DataRespDto.getReiluc();//不动产信息-土地-用途-代码
                //String reiluc = "01";
                String reilun = xddb0012DataRespDto.getReilun();//不动产信息-土地-用途-名称
                district.setCode(StringUtils.isNotBlank(reeidc) ? Integer.parseInt(reeidc) : 0);
                district.setName(reeidn);
                real_estate_info.setDistrict(district);
                real_estate_info.setLocation(reeilo);
                house.setReal_estate_unit_number(reiheu);
                house.setCertificate_number(reihcn);
                house.setCertificate_short_number(reihcs);
                house.setArea(reeiha);
                houseUsage.setCode(reihuc);
                houseUsage.setName(reihun);
                house.setUsage(houseUsage);
                real_estate_info.setHouse(house);
                land.setCertificate_number(reilcn);
                land.setArea(Optional.ofNullable(reeila).orElse(BigDecimal.ZERO).toString());
                landUsage.setCode(reiluc);
                landUsage.setName(reilun);
                land.setUsage(landUsage);
                real_estate_info.setLand(land);

                hyyBabigc01Items.setReal_estate_info(real_estate_info);//	不动产信息

                String micenu = xddb0012DataRespDto.getMicenu();//抵押信息-不动产登记证明号
                String micono = xddb0012DataRespDto.getMicono();//抵押信息-抵押合同编号
                BigDecimal micova = xddb0012DataRespDto.getMicova();//抵押信息-抵押物价值(万元)
                BigDecimal mideam = xddb0012DataRespDto.getMideam();//抵押信息-被担保债权数额(万元)
                String mimmco = xddb0012DataRespDto.getMimmco();//抵押信息-抵押方式-代码
                //String mimmco = "01";
                String mimmna = xddb0012DataRespDto.getMimmna();//抵押信息-抵押方式-名称
                String migusc = xddb0012DataRespDto.getMigusc();//抵押信息-担保范围
                String midsti = xddb0012DataRespDto.getMidsti();//抵押信息-债务开始时间
                String mideti = xddb0012DataRespDto.getMideti();//抵押信息-债务结束时间
                String midena = xddb0012DataRespDto.getMidena();//抵押信息-债务人-姓名/名称
                String middtc = xddb0012DataRespDto.getMiddtc();//抵押信息-债务人-证件类型-代码
                //String middtc = "10";
                String middtn = xddb0012DataRespDto.getMiddtn();//抵押信息-债务人-证件类型-名称
                String middnu = xddb0012DataRespDto.getMiddnu();//抵押信息-债务人-证件号
                String miorde = xddb0012DataRespDto.getMiorde();//抵押信息-抵押顺位
                String michco = xddb0012DataRespDto.getMichco();//抵押信息-抵押变更内容
                String mihaga = xddb0012DataRespDto.getMihaga();//抵押信息-是否包含车库
                String mihalo = xddb0012DataRespDto.getMihalo();//抵押信息-是否包含阁楼

                mortgage_info.setMortgage_certificate_number(micenu);
                mortgage_info.setContract_no(micono);
                mortgage_info.setCollateral_value(micova);
                mortgage_info.setDebt_amount(mideam);
                mortgageMethod.setCode(StringUtils.isNotBlank(mimmco) ? Integer.parseInt(mimmco) : 0);
                mortgageMethod.setName(mimmna);
                mortgage_info.setMortgage_method(mortgageMethod);
                mortgage_info.setGuarantee_scope(migusc);
                mortgage_info.setDebt_start_time(midsti);
                mortgage_info.setDebt_end_time(mideti);
                debtor.setName(midena);
                mortgage_infoDocumentType.setCode(StringUtils.isNotBlank(middtc) ? Integer.parseInt(middtc) : 99);
                mortgage_infoDocumentType.setName(middtn);
                debtor.setDocument_type(mortgage_infoDocumentType);
                debtor.setDocument_number(middnu);
                mortgage_info.setDebtor(debtor);
                mortgage_info.setOrder(miorde);
                mortgage_info.setChange_content(michco);
                mortgage_info.setHas_garage(StringUtils.isNotBlank(mihaga) ? Boolean.parseBoolean(mihaga) : Boolean.FALSE);
                mortgage_info.setHas_loft(StringUtils.isNotBlank(mihalo) ? Boolean.parseBoolean(mihalo) : Boolean.FALSE);

                hyyBabigc01Items.setMortgage_info(mortgage_info);//抵押信息
                String distbu = xddb0012DataRespDto.getDistbu();//交易信息-存量业务
                String dinumb = xddb0012DataRespDto.getDinumb();//交易信息-交易编号
                String dicono = xddb0012DataRespDto.getDicono();//交易信息-买卖合同编号
                BigDecimal dicova = xddb0012DataRespDto.getDicova();//交易信息-合同债权金额

                deal_info.setStock_business(StringUtils.isNotBlank(distbu) ? Boolean.getBoolean(distbu) : Boolean.FALSE);
                deal_info.setNumber(dinumb);
                deal_info.setContract_no(dicono);
                deal_info.setContract_value(Optional.ofNullable(dicova).orElse(BigDecimal.ZERO).toString());
                hyyBabigc01Items.setDeal_info(deal_info);//交易信息
            }
            //hyyBabigc01Items.setCollateralInfo(collateralInfo);
            hyyBabigc01ItemList.add(hyyBabigc01Items);
            hyyBabigc01Data.setItems(hyyBabigc01ItemList);

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0012.key, DscmsEnum.TRADE_CODE_XDDB0012.value, e.getMessage());
            babigc01RespDto.setCode(EpbEnum.EPB099999.key); // 9999
            babigc01RespDto.setMessage(EpbEnum.EPB099999.value);// 系统异常
        }
        babigc01RespDto.setData(hyyBabigc01Data);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIGC.key, DscmsEnum.TRADE_CODE_BABIGC.value, JSON.toJSONString(babigc01RespDto));
        return JSON.toJSONString(babigc01RespDto);
    }

    //转换证件号字典项
    private String getChangeCertName(String code){
        if("1".equals(code) || "A".equals(code)){
            code="身份证";//身份证
        }else if("3".equals(code) || "B".equals(code)){
            code="护照";//护照
        }else if("4".equals(code) || "C".equals(code)){
            code="户口簿";//户口簿
        }else if("5".equals(code) || "G".equals(code)){
            code="军官证";//军官证
        }else if("6".equals(code) || "Q".equals(code) || "R".equals(code)){
            code="组织机构代码";//组织机构代码+统一社会信用代码
        }else if("7".equals(code) || "M".equals(code)){
            code="营业执照";//营业执照
        }else{
            code="其他";
        }
        return code;
    }
}
