package cn.com.yusys.yusp.web.client.esb.ypxt.manage;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.manage.req.ManageReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.manage.resp.ManageRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.manage.req.ManageReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.manage.resp.ManageRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:管护权移交信息同步
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2ManageResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2ManageResource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
	private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：manage
     * 交易描述：管护权移交信息同步
     *
     * @param manageReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/manage")
    protected @ResponseBody
    ResultDto<ManageRespDto> manage(@Validated @RequestBody ManageReqDto manageReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MANAGE.key, EsbEnum.TRADE_CODE_MANAGE.value, JSON.toJSONString(manageReqDto));
		cn.com.yusys.yusp.online.client.esb.ypxt.manage.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.manage.req.Service();
		cn.com.yusys.yusp.online.client.esb.ypxt.manage.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.manage.resp.Service();
        ManageReqService manageReqService = new ManageReqService();
        ManageRespService manageRespService = new ManageRespService();
        ManageRespDto manageRespDto = new ManageRespDto();
        ResultDto<ManageRespDto> manageResultDto = new ResultDto<ManageRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将manageReqDto转换成reqService
			BeanUtils.copyProperties(manageReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_MANAGE.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
			reqService.setDatasq(servsq);//    全局流水
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			manageReqService.setService(reqService);
			// 将manageReqService转换成manageReqServiceMap
			Map manageReqServiceMap = beanMapUtil.beanToMap(manageReqService);
			context.put("tradeDataMap", manageReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MANAGE.key, EsbEnum.TRADE_CODE_MANAGE.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_MANAGE.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MANAGE.key, EsbEnum.TRADE_CODE_MANAGE.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			manageRespService = beanMapUtil.mapToBean(tradeDataMap, ManageRespService.class, ManageRespService.class);
			respService = manageRespService.getService();
			//  将respService转换成ManageRespDto
			manageResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			manageResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成XdypbdccxRespDto
				BeanUtils.copyProperties(respService,manageRespDto);
				manageResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				manageResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				manageResultDto.setCode(EpbEnum.EPB099999.key);
				manageResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MANAGE.key, EsbEnum.TRADE_CODE_MANAGE.value, e.getMessage());
			manageResultDto.setCode(EpbEnum.EPB099999.key);//9999
			manageResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		manageResultDto.setData(manageRespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MANAGE.key, EsbEnum.TRADE_CODE_MANAGE.value, JSON.toJSONString(manageResultDto));
        return manageResultDto;
    }
}
