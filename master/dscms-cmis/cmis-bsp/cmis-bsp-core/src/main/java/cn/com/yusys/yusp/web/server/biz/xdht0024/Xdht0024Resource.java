package cn.com.yusys.yusp.web.server.biz.xdht0024;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0024.req.Xdht0024ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0024.resp.Xdht0024RespDto;
import cn.com.yusys.yusp.dto.server.xdht0024.req.Xdht0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0024.resp.Xdht0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同详情查看
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0024:合同详情查看")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0024Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0024
     * 交易描述：合同详情查看
     *
     * @param xdht0024ReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("xdht0024:合同详情查看")
    @PostMapping("/xdht0024")
//    @Idempotent({"xdcaht0024", "#xdht0024ReqDto.datasq"})
    protected @ResponseBody
    Xdht0024RespDto xdht0024(@Validated @RequestBody Xdht0024ReqDto xdht0024ReqDto ) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024ReqDto));
        Xdht0024DataReqDto xdht0024DataReqDto = new Xdht0024DataReqDto();// 请求Data： 合同详情查看
        Xdht0024DataRespDto xdht0024DataRespDto =  new Xdht0024DataRespDto();// 响应Data：合同详情查看
        Xdht0024RespDto xdht0024RespDto = new Xdht0024RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0024.req.Data reqData = null; // 请求Data：合同详情查看
        cn.com.yusys.yusp.dto.server.biz.xdht0024.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0024.resp.Data();// 响应Data：合同详情查看
        try {
            // 从 xdht0024ReqDto获取 reqData
            reqData = xdht0024ReqDto.getData();
            // 将 reqData 拷贝到xdht0024DataReqDto
            BeanUtils.copyProperties(reqData, xdht0024DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataReqDto));
            ResultDto<Xdht0024DataRespDto> xdht0024DataResultDto= dscmsBizHtClientService.xdht0024(xdht0024DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0024RespDto.setErorcd(Optional.ofNullable(xdht0024DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0024RespDto.setErortx(Optional.ofNullable(xdht0024DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,xdht0024DataResultDto.getCode())) {
                xdht0024DataRespDto = xdht0024DataResultDto.getData();
                xdht0024RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0024RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0024DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0024DataRespDto, respData);
            }
        } catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, e.getMessage());
            xdht0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0024RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, e.getMessage());
            xdht0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0024RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0024RespDto.setDatasq(servsq);

        xdht0024RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, JSON.toJSONString(xdht0024RespDto));
        return xdht0024RespDto;
    }
}