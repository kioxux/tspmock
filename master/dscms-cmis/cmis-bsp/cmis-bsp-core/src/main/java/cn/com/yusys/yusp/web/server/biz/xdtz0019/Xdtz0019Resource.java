package cn.com.yusys.yusp.web.server.biz.xdtz0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0019.req.Xdtz0019ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0019.resp.Xdtz0019RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0019.req.Xdtz0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0019.resp.Xdtz0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:台账入账
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0019:台账入账")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0019Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0019
     * 交易描述：台账入账
     *
     * @param xdtz0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账入账")
    @PostMapping("/xdtz0019")
    //@Idempotent({"xdcatz0019", "#xdtz0019ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0019RespDto xdtz0019(@Validated @RequestBody Xdtz0019ReqDto xdtz0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, JSON.toJSONString(xdtz0019ReqDto));
        Xdtz0019DataReqDto xdtz0019DataReqDto = new Xdtz0019DataReqDto();// 请求Data： 台账入账
        Xdtz0019DataRespDto xdtz0019DataRespDto = new Xdtz0019DataRespDto();// 响应Data：台账入账
        Xdtz0019RespDto xdtz0019RespDto = new Xdtz0019RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0019.req.Data reqData = null; // 请求Data：台账入账
        cn.com.yusys.yusp.dto.server.biz.xdtz0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0019.resp.Data();// 响应Data：台账入账
        try {
            // 从 xdtz0019ReqDto获取 reqData
            reqData = xdtz0019ReqDto.getData();
            // 将 reqData 拷贝到xdtz0019DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0019DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, JSON.toJSONString(xdtz0019DataReqDto));
            ResultDto<Xdtz0019DataRespDto> xdtz0019DataResultDto = dscmsBizTzClientService.xdtz0019(xdtz0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, JSON.toJSONString(xdtz0019DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0019RespDto.setErorcd(Optional.ofNullable(xdtz0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0019RespDto.setErortx(Optional.ofNullable(xdtz0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0019DataResultDto.getCode())) {
                xdtz0019DataRespDto = xdtz0019DataResultDto.getData();
                xdtz0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, e.getMessage());
            xdtz0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0019RespDto.setDatasq(servsq);

        xdtz0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0019.key, DscmsEnum.TRADE_CODE_XDTZ0019.value, JSON.toJSONString(xdtz0019RespDto));
        return xdtz0019RespDto;
    }
}
