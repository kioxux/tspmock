package cn.com.yusys.yusp.web.client.esb.rircp.fbxd06;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd06.Fbxd06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd06.Fbxd06RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.req.Fbxd06ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.resp.Fbxd06RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd06）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd06Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd06Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd06
     * 交易描述：获取该笔借据的最新五级分类以及数据日期
     *
     * @param fbxd06ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd06")
    protected @ResponseBody
    ResultDto<Fbxd06RespDto> fbxd06(@Validated @RequestBody Fbxd06ReqDto fbxd06ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD06.key, EsbEnum.TRADE_CODE_FBXD06.value, JSON.toJSONString(fbxd06ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd06.resp.Service();
        Fbxd06ReqService fbxd06ReqService = new Fbxd06ReqService();
        Fbxd06RespService fbxd06RespService = new Fbxd06RespService();
        Fbxd06RespDto fbxd06RespDto = new Fbxd06RespDto();
        ResultDto<Fbxd06RespDto> fbxd06ResultDto = new ResultDto<Fbxd06RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd06ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd06ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD06.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fbxd06ReqService.setService(reqService);
            // 将fbxd06ReqService转换成fbxd06ReqServiceMap
            Map fbxd06ReqServiceMap = beanMapUtil.beanToMap(fbxd06ReqService);
            context.put("tradeDataMap", fbxd06ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD06.key, EsbEnum.TRADE_CODE_FBXD06.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD06.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD06.key, EsbEnum.TRADE_CODE_FBXD06.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd06RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd06RespService.class, Fbxd06RespService.class);
            respService = fbxd06RespService.getService();

            fbxd06ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd06ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd06RespDto
                BeanUtils.copyProperties(respService, fbxd06RespDto);

                fbxd06ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd06ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd06ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd06ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD06.key, EsbEnum.TRADE_CODE_FBXD06.value, e.getMessage());
            fbxd06ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd06ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd06ResultDto.setData(fbxd06RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD06.key, EsbEnum.TRADE_CODE_FBXD06.value, JSON.toJSONString(fbxd06ResultDto));
        return fbxd06ResultDto;
    }
}
