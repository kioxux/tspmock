package cn.com.yusys.yusp.web.server.biz.xddb0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0004.req.Xddb0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0004.resp.Xddb0004RespDto;
import cn.com.yusys.yusp.dto.server.xddb0004.req.Xddb0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0004.resp.Xddb0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:提醒任务处理结果实时同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDB0004:提醒任务处理结果实时同步")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0004Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizdbClientService;
    /**
     * 交易码：xddb0004
     * 交易描述：提醒任务处理结果实时同步
     *
     * @param xddb0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("提醒任务处理结果实时同步")
    @PostMapping("/xddb0004")
    //@Idempotent({"xddb0004", "#xddb0004ReqDto.datasq"})
    protected @ResponseBody
    Xddb0004RespDto xddb0004(@Validated @RequestBody Xddb0004ReqDto xddb0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, JSON.toJSONString(xddb0004ReqDto));
        Xddb0004DataReqDto xddb0004DataReqDto = new Xddb0004DataReqDto();// 请求Data： 提醒任务处理结果实时同步
        Xddb0004DataRespDto xddb0004DataRespDto = new Xddb0004DataRespDto();// 响应Data：提醒任务处理结果实时同步
        Xddb0004RespDto xddb0004RespDto = new Xddb0004RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0004.req.Data reqData = null; // 请求Data：提醒任务处理结果实时同步
        cn.com.yusys.yusp.dto.server.biz.xddb0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0004.resp.Data();// 响应Data：提醒任务处理结果实时同步

        try {
            // 从 xddb0004ReqDto获取 reqData
            reqData = xddb0004ReqDto.getData();
            // 将 reqData 拷贝到xddb0004DataReqDto
            BeanUtils.copyProperties(reqData, xddb0004DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, JSON.toJSONString(xddb0004DataReqDto));
            ResultDto<Xddb0004DataRespDto> xddb0004DataResultDto = dscmsBizdbClientService.xddb0004(xddb0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, JSON.toJSONString(xddb0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0004RespDto.setErorcd(Optional.ofNullable(xddb0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0004RespDto.setErortx(Optional.ofNullable(xddb0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0004DataResultDto.getCode())) {
                xddb0004DataRespDto = xddb0004DataResultDto.getData();
                xddb0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, e.getMessage());
            xddb0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0004RespDto.setDatasq(servsq);

        xddb0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0004.key, DscmsEnum.TRADE_CODE_XDDB0004.value, JSON.toJSONString(xddb0004RespDto));
        return xddb0004RespDto;
    }
}
