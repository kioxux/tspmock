package cn.com.yusys.yusp.online.client.esb.core.ln3126.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款费用交易明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private cn.com.yusys.yusp.online.client.esb.core.ln3126.resp.Listdkfycx listdkfycx;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public Listdkfycx getListdkfycx() {
        return listdkfycx;
    }

    public void setListdkfycx(Listdkfycx listdkfycx) {
        this.listdkfycx = listdkfycx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", listdkfycx=" + listdkfycx +
                '}';
    }
}
