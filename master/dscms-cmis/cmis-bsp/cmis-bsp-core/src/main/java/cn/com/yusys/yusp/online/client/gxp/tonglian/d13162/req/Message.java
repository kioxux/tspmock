package cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req;

import cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead;

/**
 * <br>
 * 0.2ZRC:2021/8/11 15:49:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/8/11 15:49
 * @since 2021/8/11 15:49
 */
public class Message {
    private cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead head;
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13162.req.Body body;

    public GxpReqHead getHead() {
        return head;
    }

    public void setHead(GxpReqHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
