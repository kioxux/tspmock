package cn.com.yusys.yusp.web.server.biz.xdsx0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0014.req.Xdsx0014ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0014.resp.Xdsx0014RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0014.req.Xdsx0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0014.resp.Xdsx0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:风控发信贷进行授信作废接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDSX0014:风控发信贷进行授信作废接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0014Resource.class);
    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0014
     * 交易描述：风控发信贷进行授信作废接口
     *
     * @param xdsx0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发信贷进行授信作废接口")
    @PostMapping("/xdsx0014")
    //@Idempotent({"xdcasx0014", "#xdsx0014ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0014RespDto xdsx0014(@Validated @RequestBody Xdsx0014ReqDto xdsx0014ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, JSON.toJSONString(xdsx0014ReqDto));
        Xdsx0014DataReqDto xdsx0014DataReqDto = new Xdsx0014DataReqDto();// 请求Data： 风控发信贷进行授信作废接口
        Xdsx0014DataRespDto xdsx0014DataRespDto = new Xdsx0014DataRespDto();// 响应Data：风控发信贷进行授信作废接口
        Xdsx0014RespDto xdsx0014RespDto = new Xdsx0014RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdsx0014.req.Data reqData = null; // 请求Data：风控发信贷进行授信作废接口
        cn.com.yusys.yusp.dto.server.biz.xdsx0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0014.resp.Data();// 响应Data：风控发信贷进行授信作废接口

        try {
            // 从 xdsx0014ReqDto获取 reqData
            reqData = xdsx0014ReqDto.getData();
            // 将 reqData 拷贝到xdsx0014DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0014DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, JSON.toJSONString(xdsx0014DataReqDto));
            ResultDto<Xdsx0014DataRespDto> xdsx0014DataResultDto = dscmsBizSxClientService.xdsx0014(xdsx0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, JSON.toJSONString(xdsx0014DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0014RespDto.setErorcd(Optional.ofNullable(xdsx0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0014RespDto.setErortx(Optional.ofNullable(xdsx0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0014DataResultDto.getCode())) {
                xdsx0014DataRespDto = xdsx0014DataResultDto.getData();
                xdsx0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0014DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0014RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, e.getMessage());
            xdsx0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0014RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0014RespDto.setDatasq(servsq);

        xdsx0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0014.key, DscmsEnum.TRADE_CODE_XDSX0014.value, JSON.toJSONString(xdsx0014RespDto));
        return xdsx0014RespDto;
    }
}
