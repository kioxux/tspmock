package cn.com.yusys.yusp.web.server.biz.xdtz0029;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0029.req.Xdtz0029ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0029.resp.Xdtz0029RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0029.req.Xdtz0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0029.resp.Xdtz0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询指定票号在信贷台账中是否已贴现
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0029:查询指定票号在信贷台账中是否已贴现")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0029Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0029
     * 交易描述：查询指定票号在信贷台账中是否已贴现
     *
     * @param xdtz0029ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询指定票号在信贷台账中是否已贴现")
    @PostMapping("/xdtz0029")
    //@Idempotent({"xdcatz0029", "#xdtz0029ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0029RespDto xdtz0029(@Validated @RequestBody Xdtz0029ReqDto xdtz0029ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029ReqDto));
        Xdtz0029DataReqDto xdtz0029DataReqDto = new Xdtz0029DataReqDto();// 请求Data： 查询指定票号在信贷台账中是否已贴现
        Xdtz0029DataRespDto xdtz0029DataRespDto = new Xdtz0029DataRespDto();// 响应Data：查询指定票号在信贷台账中是否已贴现
        Xdtz0029RespDto xdtz0029RespDto = new Xdtz0029RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0029.req.Data reqData = null; // 请求Data：查询指定票号在信贷台账中是否已贴现
        cn.com.yusys.yusp.dto.server.biz.xdtz0029.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0029.resp.Data();// 响应Data：查询指定票号在信贷台账中是否已贴现

        try {
            // 从 xdtz0029ReqDto获取 reqData
            reqData = xdtz0029ReqDto.getData();
            // 将 reqData 拷贝到xdtz0029DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0029DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029DataReqDto));
            ResultDto<Xdtz0029DataRespDto> xdtz0029DataResultDto = dscmsBizTzClientService.xdtz0029(xdtz0029DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0029RespDto.setErorcd(Optional.ofNullable(xdtz0029DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0029RespDto.setErortx(Optional.ofNullable(xdtz0029DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0029DataResultDto.getCode())) {
                xdtz0029DataRespDto = xdtz0029DataResultDto.getData();
                xdtz0029RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0029RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0029DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0029DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, e.getMessage());
            xdtz0029RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0029RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0029RespDto.setDatasq(servsq);

        xdtz0029RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0029.key, DscmsEnum.TRADE_CODE_XDTZ0029.value, JSON.toJSONString(xdtz0029RespDto));
        return xdtz0029RespDto;
    }
}
