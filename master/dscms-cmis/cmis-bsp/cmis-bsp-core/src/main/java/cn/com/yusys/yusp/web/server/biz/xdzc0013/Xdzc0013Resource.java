package cn.com.yusys.yusp.web.server.biz.xdzc0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0013.req.Xdzc0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0013.resp.Xdzc0013RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0013.req.Xdzc0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0013.resp.Xdzc0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:贸易合同资料上传接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0013:贸易新增接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0013Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0013
     * 交易描述：贸易新增接口
     *
     * @param xdzc0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贸易新增接口")
    @PostMapping("/xdzc0013")
    //@Idempotent({"xdzc013", "#xdzc0013ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0013RespDto xdzc0013(@Validated @RequestBody Xdzc0013ReqDto xdzc0013ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, JSON.toJSONString(xdzc0013ReqDto));
        Xdzc0013DataReqDto xdzc0013DataReqDto = new Xdzc0013DataReqDto();// 请求Data： 贸易合同资料上传接口
        Xdzc0013DataRespDto xdzc0013DataRespDto = new Xdzc0013DataRespDto();// 响应Data：贸易合同资料上传接口
        Xdzc0013RespDto xdzc0013RespDto = new Xdzc0013RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0013.req.Data reqData = null; // 请求Data：贸易合同资料上传接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0013.resp.Data();// 响应Data：贸易合同资料上传接口

        try {
            // 从 xdzc0013ReqDto获取 reqData
            reqData = xdzc0013ReqDto.getData();
            // 将 reqData 拷贝到xdzc0013DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, JSON.toJSONString(xdzc0013DataReqDto));
            ResultDto<Xdzc0013DataRespDto> xdzc0013DataResultDto = dscmsBizZcClientService.xdzc0013(xdzc0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, JSON.toJSONString(xdzc0013DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzc0013RespDto.setErorcd(Optional.ofNullable(xdzc0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0013RespDto.setErortx(Optional.ofNullable(xdzc0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0013DataResultDto.getCode())) {
                xdzc0013DataRespDto = xdzc0013DataResultDto.getData();
                xdzc0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0013DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, e.getMessage());
            xdzc0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0013RespDto.setDatasq(servsq);

        xdzc0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0013.key, DscmsEnum.TRADE_CODE_XDZC0013.value, JSON.toJSONString(xdzc0013RespDto));
        return xdzc0013RespDto;
    }
}
