package cn.com.yusys.yusp.online.client.esb.core.dp2352.req;

/**
 * 请求Service：组合账户子账户开立
 * @author lihh
 * @version 1.0             
 */      
public class Dp2352ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
