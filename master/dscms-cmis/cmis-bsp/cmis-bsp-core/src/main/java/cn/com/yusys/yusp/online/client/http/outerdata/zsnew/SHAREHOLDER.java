package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	股东及出资信息
@JsonPropertyOrder(alphabetic = true)
public class SHAREHOLDER implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CONDATE")
    private String CONDATE;//	出资日期
    @JsonProperty(value = "CONFORM")
    private String CONFORM;//	出资方式
    @JsonProperty(value = "CONFORMCODE")
    private String CONFORMCODE;//	出资方式代码
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;//	股东统一社会信用代码
    @JsonProperty(value = "CURRENCYCODE")
    private String CURRENCYCODE;//	认缴出资币种代码
    @JsonProperty(value = "FUNDEDRATIO")
    private String FUNDEDRATIO;//	出资比例
    @JsonProperty(value = "INVTYPE")
    private String INVTYPE;//	股东类型
    @JsonProperty(value = "INVTYPECODE")
    private String INVTYPECODE;//	股东类型代码
    @JsonProperty(value = "REGCAPCUR")
    private String REGCAPCUR;//	认缴出资币种
    @JsonProperty(value = "REGNO")
    private String REGNO;//	股东注册号
    @JsonProperty(value = "SHANAME")
    private String SHANAME;//	股东名称
    @JsonProperty(value = "SUBCONAM")
    private String SUBCONAM;//	认缴出资额（万元）
    @JsonProperty(value = "id")
    private String id;
    @JsonProperty(value = "baseId")
    private String baseId;

    @JsonIgnore
    public String getCONDATE() {
        return CONDATE;
    }

    @JsonIgnore
    public void setCONDATE(String CONDATE) {
        this.CONDATE = CONDATE;
    }

    @JsonIgnore
    public String getCONFORM() {
        return CONFORM;
    }

    @JsonIgnore
    public void setCONFORM(String CONFORM) {
        this.CONFORM = CONFORM;
    }

    @JsonIgnore
    public String getCONFORMCODE() {
        return CONFORMCODE;
    }

    @JsonIgnore
    public void setCONFORMCODE(String CONFORMCODE) {
        this.CONFORMCODE = CONFORMCODE;
    }

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getCURRENCYCODE() {
        return CURRENCYCODE;
    }

    @JsonIgnore
    public void setCURRENCYCODE(String CURRENCYCODE) {
        this.CURRENCYCODE = CURRENCYCODE;
    }

    @JsonIgnore
    public String getFUNDEDRATIO() {
        return FUNDEDRATIO;
    }

    @JsonIgnore
    public void setFUNDEDRATIO(String FUNDEDRATIO) {
        this.FUNDEDRATIO = FUNDEDRATIO;
    }

    @JsonIgnore
    public String getINVTYPE() {
        return INVTYPE;
    }

    @JsonIgnore
    public void setINVTYPE(String INVTYPE) {
        this.INVTYPE = INVTYPE;
    }

    @JsonIgnore
    public String getINVTYPECODE() {
        return INVTYPECODE;
    }

    @JsonIgnore
    public void setINVTYPECODE(String INVTYPECODE) {
        this.INVTYPECODE = INVTYPECODE;
    }

    @JsonIgnore
    public String getREGCAPCUR() {
        return REGCAPCUR;
    }

    @JsonIgnore
    public void setREGCAPCUR(String REGCAPCUR) {
        this.REGCAPCUR = REGCAPCUR;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getSHANAME() {
        return SHANAME;
    }

    @JsonIgnore
    public void setSHANAME(String SHANAME) {
        this.SHANAME = SHANAME;
    }

    @JsonIgnore
    public String getSUBCONAM() {
        return SUBCONAM;
    }

    @JsonIgnore
    public void setSUBCONAM(String SUBCONAM) {
        this.SUBCONAM = SUBCONAM;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBaseId() {
        return baseId;
    }

    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    @Override
    public String toString() {
        return "SHAREHOLDER{" +
                "CONDATE='" + CONDATE + '\'' +
                ", CONFORM='" + CONFORM + '\'' +
                ", CONFORMCODE='" + CONFORMCODE + '\'' +
                ", CREDITCODE='" + CREDITCODE + '\'' +
                ", CURRENCYCODE='" + CURRENCYCODE + '\'' +
                ", FUNDEDRATIO='" + FUNDEDRATIO + '\'' +
                ", INVTYPE='" + INVTYPE + '\'' +
                ", INVTYPECODE='" + INVTYPECODE + '\'' +
                ", REGCAPCUR='" + REGCAPCUR + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", SHANAME='" + SHANAME + '\'' +
                ", SUBCONAM='" + SUBCONAM + '\'' +
                ", id='" + id + '\'' +
                ", baseId='" + baseId + '\'' +
                '}';
    }
}
