package cn.com.yusys.yusp.web.client.esb.core.ln3236;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3236.Ln3236ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3236.Ln3236RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3236.req.Ln3236ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3236.resp.Ln3236RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3236)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3236Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3236Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3236
     * 交易描述：贷款账隔日冲正
     *
     * @param ln3236ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3236:贷款账隔日冲正")
    @PostMapping("/ln3236")
    protected @ResponseBody
    ResultDto<Ln3236RespDto> ln3236(@Validated @RequestBody Ln3236ReqDto ln3236ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3236.key, EsbEnum.TRADE_CODE_LN3236.value, JSON.toJSONString(ln3236ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3236.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3236.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3236.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3236.resp.Service();
        Ln3236ReqService ln3236ReqService = new Ln3236ReqService();
        Ln3236RespService ln3236RespService = new Ln3236RespService();
        Ln3236RespDto ln3236RespDto = new Ln3236RespDto();
        ResultDto<Ln3236RespDto> ln3236ResultDto = new ResultDto<Ln3236RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3236ReqDto转换成reqService
            BeanUtils.copyProperties(ln3236ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3236.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3236ReqService.setService(reqService);
            // 将ln3236ReqService转换成ln3236ReqServiceMap
            Map ln3236ReqServiceMap = beanMapUtil.beanToMap(ln3236ReqService);
            context.put("tradeDataMap", ln3236ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3236.key, EsbEnum.TRADE_CODE_LN3236.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3236.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3236.key, EsbEnum.TRADE_CODE_LN3236.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3236RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3236RespService.class, Ln3236RespService.class);
            respService = ln3236RespService.getService();

            ln3236ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3236ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3236RespDto
                BeanUtils.copyProperties(respService, ln3236RespDto);
                ln3236ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3236ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3236ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3236ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3236.key, EsbEnum.TRADE_CODE_LN3236.value, e.getMessage());
            ln3236ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3236ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3236ResultDto.setData(ln3236RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3236.key, EsbEnum.TRADE_CODE_LN3236.value, JSON.toJSONString(ln3236ResultDto));
        return ln3236ResultDto;
    }

}
