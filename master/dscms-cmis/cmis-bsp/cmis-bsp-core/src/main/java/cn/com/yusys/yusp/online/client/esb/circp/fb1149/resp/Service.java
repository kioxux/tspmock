package cn.com.yusys.yusp.online.client.esb.circp.fb1149.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 响应Service：无还本续贷借据更新
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    @JsonProperty(value = "OLS_TRAN_NO")
    private String OLS_TRAN_NO;
    @JsonProperty(value = "OLS_DATE")
    private String OLS_DATE;

    @JsonIgnore
    public String getOLS_TRAN_NO() {
        return OLS_TRAN_NO;
    }

    @JsonIgnore
    public void setOLS_TRAN_NO(String OLS_TRAN_NO) {
        this.OLS_TRAN_NO = OLS_TRAN_NO;
    }

    @JsonIgnore
    public String getOLS_DATE() {
        return OLS_DATE;
    }

    @JsonIgnore
    public void setOLS_DATE(String OLS_DATE) {
        this.OLS_DATE = OLS_DATE;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", OLS_TRAN_NO='" + OLS_TRAN_NO + '\'' +
                ", OLS_DATE='" + OLS_DATE + '\'' +
                '}';
    }
}
