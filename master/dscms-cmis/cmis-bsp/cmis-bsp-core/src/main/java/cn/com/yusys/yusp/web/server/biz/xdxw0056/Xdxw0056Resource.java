package cn.com.yusys.yusp.web.server.biz.xdxw0056;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0056.req.Xdxw0056ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0056.resp.Xdxw0056RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0056.req.Xdxw0056DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0056.resp.Xdxw0056DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据流水号取得优企贷信息的客户经理ID和客户经理名1
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0056:根据流水号取得优企贷信息的客户经理ID和客户经理名")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0056Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0056Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0056
     * 交易描述：根据流水号取得优企贷信息的客户经理ID和客户经理名
     *
     * @param xdxw0056ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号取得优企贷信息的客户经理ID和客户经理名")
    @PostMapping("/xdxw0056")
    //@Idempotent({"xdcaxw0056", "#xdxw0056ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0056RespDto xdxw0056(@Validated @RequestBody Xdxw0056ReqDto xdxw0056ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056ReqDto));
        Xdxw0056DataReqDto xdxw0056DataReqDto = new Xdxw0056DataReqDto();// 请求Data： 根据流水号取得优企贷信息的客户经理ID和客户经理名
        Xdxw0056DataRespDto xdxw0056DataRespDto = new Xdxw0056DataRespDto();// 响应Data：根据流水号取得优企贷信息的客户经理ID和客户经理名
        Xdxw0056RespDto xdxw0056RespDto = new Xdxw0056RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0056.req.Data reqData = null; // 请求Data：根据流水号取得优企贷信息的客户经理ID和客户经理名
        cn.com.yusys.yusp.dto.server.biz.xdxw0056.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0056.resp.Data();// 响应Data：根据流水号取得优企贷信息的客户经理ID和客户经理名
        try {
            // 从 xdxw0056ReqDto获取 reqData
            reqData = xdxw0056ReqDto.getData();
            // 将 reqData 拷贝到xdxw0056DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0056DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056DataReqDto));
            ResultDto<Xdxw0056DataRespDto> xdxw0056DataResultDto = dscmsBizXwClientService.xdxw0056(xdxw0056DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0056RespDto.setErorcd(Optional.ofNullable(xdxw0056DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0056RespDto.setErortx(Optional.ofNullable(xdxw0056DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0056DataResultDto.getCode())) {
                xdxw0056DataRespDto = xdxw0056DataResultDto.getData();
                xdxw0056RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0056RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0056DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0056DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, e.getMessage());
            xdxw0056RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0056RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0056RespDto.setDatasq(servsq);

        xdxw0056RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0056.key, DscmsEnum.TRADE_CODE_XDXW0056.value, JSON.toJSONString(xdxw0056RespDto));
        return xdxw0056RespDto;
    }
}
