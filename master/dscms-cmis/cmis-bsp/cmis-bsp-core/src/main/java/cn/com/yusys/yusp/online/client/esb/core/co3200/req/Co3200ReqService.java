package cn.com.yusys.yusp.online.client.esb.core.co3200.req;

import java.io.Serializable;

/**
 * 请求Service：抵质押物的开户
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日16:06:30
 */
public class Co3200ReqService implements Serializable {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
