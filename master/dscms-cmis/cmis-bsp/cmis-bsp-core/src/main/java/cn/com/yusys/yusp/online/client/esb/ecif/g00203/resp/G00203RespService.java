package cn.com.yusys.yusp.online.client.esb.ecif.g00203.resp;

/**
 * 响应Service：同业客户维护
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class G00203RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
