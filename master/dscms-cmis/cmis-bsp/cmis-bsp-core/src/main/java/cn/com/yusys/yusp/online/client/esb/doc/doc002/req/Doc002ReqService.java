package cn.com.yusys.yusp.online.client.esb.doc.doc002.req;

/**
 * 请求Service：入库查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Doc002ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Doc002ReqService{" +
                "service=" + service +
                '}';
    }
}
