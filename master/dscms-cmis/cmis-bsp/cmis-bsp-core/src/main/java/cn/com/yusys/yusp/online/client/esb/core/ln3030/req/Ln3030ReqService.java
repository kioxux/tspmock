package cn.com.yusys.yusp.online.client.esb.core.ln3030.req;

/**
 * 请求Service：贷款资料维护
 * @author lihh
 * @version 1.0             
 */      
public class Ln3030ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
