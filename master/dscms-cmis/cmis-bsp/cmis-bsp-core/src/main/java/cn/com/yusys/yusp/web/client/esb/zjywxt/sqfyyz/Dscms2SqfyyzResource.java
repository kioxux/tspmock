package cn.com.yusys.yusp.web.client.esb.zjywxt.sqfyyz;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.req.SqfyyzReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.resp.SqfyyzRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.req.SqfyyzReqService;
import cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.resp.SqfyyzRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类: 中间业务系统
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2zjywxt")
public class Dscms2SqfyyzResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2SqfyyzResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：sqfyyz
     * 交易描述：行方验证房源信息
     *
     * @param sqfyyzReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/sqfyyz")
    protected @ResponseBody
    ResultDto<SqfyyzRespDto> sqfyyz(@Validated @RequestBody SqfyyzReqDto sqfyyzReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SQFYYZ.key, EsbEnum.TRADE_CODE_SQFYYZ.value, JSON.toJSONString(sqfyyzReqDto));
        cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.req.Service();
        cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.resp.Service();
        SqfyyzReqService sqfyyzReqService = new SqfyyzReqService();
        SqfyyzRespService sqfyyzRespService = new SqfyyzRespService();
        SqfyyzRespDto sqfyyzRespDto = new SqfyyzRespDto();
        ResultDto<SqfyyzRespDto> sqfyyzResultDto = new ResultDto<SqfyyzRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将sqfyyzReqDto转换成reqService
            BeanUtils.copyProperties(sqfyyzReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_SQFYYZ.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_GAP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_GAP.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            sqfyyzReqService.setService(reqService);
            // 将sqfyyzReqService转换成sqfyyzReqServiceMap
            Map sqfyyzReqServiceMap = beanMapUtil.beanToMap(sqfyyzReqService);
            context.put("tradeDataMap", sqfyyzReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SQFYYZ.key, EsbEnum.TRADE_CODE_SQFYYZ.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_SQFYYZ.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SQFYYZ.key, EsbEnum.TRADE_CODE_SQFYYZ.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            sqfyyzRespService = beanMapUtil.mapToBean(tradeDataMap, SqfyyzRespService.class, SqfyyzRespService.class);
            respService = sqfyyzRespService.getService();

            sqfyyzResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            sqfyyzResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成SqfyyzRespDto
                BeanUtils.copyProperties(respService, sqfyyzRespDto);
                sqfyyzResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                sqfyyzResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                sqfyyzResultDto.setCode(EpbEnum.EPB099999.key);
                sqfyyzResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SQFYYZ.key, EsbEnum.TRADE_CODE_SQFYYZ.value, e.getMessage());
            sqfyyzResultDto.setCode(EpbEnum.EPB099999.key);//9999
            sqfyyzResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        sqfyyzResultDto.setData(sqfyyzRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_SQFYYZ.key, EsbEnum.TRADE_CODE_SQFYYZ.value, JSON.toJSONString(sqfyyzResultDto));
        return sqfyyzResultDto;
    }
}
