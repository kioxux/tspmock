package cn.com.yusys.yusp.online.client.esb.core.ln3045.resp;

/**
 * 响应Service：贷款核销处理
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3045RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3045RespService{" +
                "service=" + service +
                '}';
    }
}

