package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp;

import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.List;

/**
 * 响应Dto：账单交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Body {
    private String pgeflg;//是否有下一页标志
    private String frtrow;//开始位置
    private String lstrow;//结束位置
    private String cardno;//卡号
    private Integer totrow;//总条数
    private String stdate;//账单年月
    private String currcd;//币种
	private List list;//list


    public String getPgeflg() {
        return pgeflg;
    }

    public void setPgeflg(String pgeflg) {
        this.pgeflg = pgeflg;
    }

    public String getFrtrow() {
        return frtrow;
    }

    public void setFrtrow(String frtrow) {
        this.frtrow = frtrow;
    }

    public String getLstrow() {
        return lstrow;
    }

    public void setLstrow(String lstrow) {
        this.lstrow = lstrow;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public Integer getTotrow() {
        return totrow;
    }

    public void setTotrow(Integer totrow) {
        this.totrow = totrow;
    }

    public String getStdate() {
        return stdate;
    }

    public void setStdate(String stdate) {
        this.stdate = stdate;
    }

    public String getCurrcd() {
        return currcd;
    }

    public void setCurrcd(String currcd) {
        this.currcd = currcd;
    }

	public List getList() {
		return list;
	}

	public void setList(List list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Body{" +
				"pgeflg='" + pgeflg + '\'' +
				", frtrow='" + frtrow + '\'' +
				", lstrow='" + lstrow + '\'' +
				", cardno='" + cardno + '\'' +
				", totrow=" + totrow +
				", stdate='" + stdate + '\'' +
				", currcd='" + currcd + '\'' +
				", list=" + list +
				'}';
	}
}
