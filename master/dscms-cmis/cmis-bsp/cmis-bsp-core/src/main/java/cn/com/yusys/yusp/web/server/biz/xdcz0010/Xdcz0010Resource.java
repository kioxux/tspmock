package cn.com.yusys.yusp.web.server.biz.xdcz0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0010.req.Xdcz0010ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0010.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0010.resp.Xdcz0010RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0010.req.Xdcz0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0010.resp.Xdcz0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:企业网银省心E付放款
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0010:企业网银省心E付放款")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0010Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0010
     * 交易描述：企业网银省心E付放款
     *
     * @param xdcz0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("企业网银省心E付放款")
    @PostMapping("/xdcz0010")
//   @Idempotent({"xdcz0010", "#xdcz0010ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0010RespDto xdcz0010(@Validated @RequestBody Xdcz0010ReqDto xdcz0010ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value, JSON.toJSONString(xdcz0010ReqDto));
        Xdcz0010DataReqDto xdcz0010DataReqDto = new Xdcz0010DataReqDto();// 请求Data： 电子保函开立
        Xdcz0010DataRespDto xdcz0010DataRespDto = new Xdcz0010DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0010.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0010RespDto xdcz0010RespDto = new Xdcz0010RespDto();
        try {
            // 从 xdcz0010ReqDto获取 reqData
            reqData = xdcz0010ReqDto.getData();
            // 将 reqData 拷贝到xdcz0010DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0010DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value, JSON.toJSONString(xdcz0010DataReqDto));
            ResultDto<Xdcz0010DataRespDto> xdcz0010DataResultDto = dscmsBizCzClientService.xdcz0010(xdcz0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value, JSON.toJSONString(xdcz0010DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0010RespDto.setErorcd(Optional.ofNullable(xdcz0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0010RespDto.setErortx(Optional.ofNullable(xdcz0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0010DataResultDto.getCode())) {
                xdcz0010DataRespDto = xdcz0010DataResultDto.getData();
                xdcz0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0010DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0010DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value, e.getMessage());
            xdcz0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0010RespDto.setDatasq(servsq);

        xdcz0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0010.key, DscmsEnum.TRADE_CODE_XDCZ0010.value, JSON.toJSONString(xdcz0010RespDto));
        return xdcz0010RespDto;
    }
}
