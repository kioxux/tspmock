package cn.com.yusys.yusp.web.client.esb.rlzyxt.xxdrel;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.req.XxdrelReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.XxdrelRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.req.XxdrelReqService;
import cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.XxdrelRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:查询人员基本信息岗位信息家庭信息
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2rlzyxt")
public class Dscms2XxdrelResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2XxdrelResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xxdrel
     * 交易描述：查询人员基本信息岗位信息家庭信息
     *
     * @param xxdrelReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xxdrel")
    protected @ResponseBody
    ResultDto<XxdrelRespDto> xxdrel(@Validated @RequestBody XxdrelReqDto xxdrelReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDREL.key, EsbEnum.TRADE_CODE_XXDREL.value, JSON.toJSONString(xxdrelReqDto));
    	cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.req.Service();
		cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.Service();
        XxdrelReqService xxdrelReqService = new XxdrelReqService();
        XxdrelRespService xxdrelRespService = new XxdrelRespService();
        XxdrelRespDto xxdrelRespDto = new XxdrelRespDto();
        ResultDto<XxdrelRespDto> xxdrelResultDto = new ResultDto<XxdrelRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将xxdrelReqDto转换成reqService
			BeanUtils.copyProperties(xxdrelReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_XXDREL.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
			xxdrelReqService.setService(reqService);
			// 将xxdrelReqService转换成xxdrelReqServiceMap
			Map xxdrelReqServiceMap = beanMapUtil.beanToMap(xxdrelReqService);
			context.put("tradeDataMap", xxdrelReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDREL.key, EsbEnum.TRADE_CODE_XXDREL.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XXDREL.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDREL.key, EsbEnum.TRADE_CODE_XXDREL.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			xxdrelRespService = beanMapUtil.mapToBean(tradeDataMap, XxdrelRespService.class, XxdrelRespService.class);
			respService = xxdrelRespService.getService();
			//  将respService转换成XxdrelRespDto
			xxdrelResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			xxdrelResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Fbxd01RespDto
				BeanUtils.copyProperties(respService, xxdrelRespDto);
				cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.List1 list1 = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.List1());
				List<cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.list1.Record> records = Optional.ofNullable(list1.getRecord()).orElse(new ArrayList<>());
				if (CollectionUtils.nonEmpty(records)) {
					List<cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List1> list1s = records.parallelStream().map(record -> {
						cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List1 temp = new cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List1();
						BeanUtils.copyProperties(record, temp);
						return temp;
					}).collect(Collectors.toList());
					xxdrelRespDto.setList1(list1s);
				}

				cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.List2 list2 = Optional.ofNullable(respService.getList2()).orElse(new cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.List2());
				List<cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.list2.Record> records2 = Optional.ofNullable(list2.getRecord()).orElse(new ArrayList<>());
				if (CollectionUtils.nonEmpty(records2)) {
					List<cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List2> list2s = records2.parallelStream().map(record -> {
						cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List2 temp = 	new cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.List2();
						BeanUtils.copyProperties(record, temp);
						return temp;
					}).collect(Collectors.toList());
					xxdrelRespDto.setList2(list2s);
				}
				xxdrelResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				xxdrelResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				xxdrelResultDto.setCode(EpbEnum.EPB099999.key);
				xxdrelResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDREL.key, EsbEnum.TRADE_CODE_XXDREL.value, e.getMessage());
			xxdrelResultDto.setCode(EpbEnum.EPB099999.key);//9999
			xxdrelResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		xxdrelResultDto.setData(xxdrelRespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XXDREL.key, EsbEnum.TRADE_CODE_XXDREL.value, JSON.toJSONString(xxdrelResultDto));
        return xxdrelResultDto;
    }
}
