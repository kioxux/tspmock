package cn.com.yusys.yusp.web.server.biz.xdxw0076;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0076.req.Xdxw0076ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0076.resp.Xdxw0076RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0076.req.Xdxw0076DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0076.resp.Xdxw0076DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:渠道端查询对公客户我的贷款（授信申请流程监控）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0076:渠道端查询对公客户我的贷款（授信申请流程监控）")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0076Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0076Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0076
     * 交易描述：渠道端查询对公客户我的贷款（授信申请流程监控）
     *
     * @param xdxw0076ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("渠道端查询对公客户我的贷款（授信申请流程监控）")
    @PostMapping("/xdxw0076")
    //@Idempotent({"xdcaxw0076", "#xdxw0076ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0076RespDto xdxw0076(@Validated @RequestBody Xdxw0076ReqDto xdxw0076ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076ReqDto));
        Xdxw0076DataReqDto xdxw0076DataReqDto = new Xdxw0076DataReqDto();// 请求Data： 渠道端查询对公客户我的贷款（授信申请流程监控）
        Xdxw0076DataRespDto xdxw0076DataRespDto = new Xdxw0076DataRespDto();// 响应Data：渠道端查询对公客户我的贷款（授信申请流程监控）
        Xdxw0076RespDto xdxw0076RespDto = new Xdxw0076RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0076.req.Data reqData = null; // 请求Data：渠道端查询对公客户我的贷款（授信申请流程监控）
        cn.com.yusys.yusp.dto.server.biz.xdxw0076.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0076.resp.Data();// 响应Data：渠道端查询对公客户我的贷款（授信申请流程监控）

        try {
            // 从 xdxw0076ReqDto获取 reqData
            reqData = xdxw0076ReqDto.getData();
            // 将 reqData 拷贝到xdxw0076DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0076DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataReqDto));
            ResultDto<Xdxw0076DataRespDto> xdxw0076DataResultDto = dscmsBizXwClientService.xdxw0076(xdxw0076DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0076RespDto.setErorcd(Optional.ofNullable(xdxw0076DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0076RespDto.setErortx(Optional.ofNullable(xdxw0076DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0076DataResultDto.getCode())) {
                xdxw0076DataRespDto = xdxw0076DataResultDto.getData();
                xdxw0076RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0076RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0076DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0076DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, e.getMessage());
            xdxw0076RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0076RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0076RespDto.setDatasq(servsq);

        xdxw0076RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0076.key, DscmsEnum.TRADE_CODE_XDXW0076.value, JSON.toJSONString(xdxw0076RespDto));
        return xdxw0076RespDto;
    }
}
