package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	年报-企业对外投资信息
@JsonPropertyOrder(alphabetic = true)
public class YEARREPORTFORINV implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ANCHEID")
    private String ANCHEID;//	年报ID
    @JsonProperty(value = "CREDITNO")
    private String CREDITNO;//	统一信用代码
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//	企业名称
    @JsonProperty(value = "REGNO")
    private String REGNO;//	注册号

    @JsonIgnore
    public String getANCHEID() {
        return ANCHEID;
    }

    @JsonIgnore
    public void setANCHEID(String ANCHEID) {
        this.ANCHEID = ANCHEID;
    }

    @JsonIgnore
    public String getCREDITNO() {
        return CREDITNO;
    }

    @JsonIgnore
    public void setCREDITNO(String CREDITNO) {
        this.CREDITNO = CREDITNO;
    }

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @Override
    public String toString() {
        return "YEARREPORTFORINV{" +
                "ANCHEID='" + ANCHEID + '\'' +
                ", CREDITNO='" + CREDITNO + '\'' +
                ", ENTNAME='" + ENTNAME + '\'' +
                ", REGNO='" + REGNO + '\'' +
                '}';
    }
}
