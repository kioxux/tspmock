package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.req;

/**
 * 请求Service：信贷系统请求V平台二级准入接口
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String businessid;//业务申请编号 (银行业务单号)
    private String grantdate;//授权日期
    private String scenecheckresult;//现场核验结果是否通过
    private String scenechecksuggest;//客户经理勘验意见
    private String latestamount;//调整后最新额度
    private String latestinterest;//调整后最新利率
    private String repaytype;//还款方式
    private String applyterm;//申请期限
    private String turnover;//周转方式

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getBusinessid() {
        return businessid;
    }

    public void setBusinessid(String businessid) {
        this.businessid = businessid;
    }

    public String getGrantdate() {
        return grantdate;
    }

    public void setGrantdate(String grantdate) {
        this.grantdate = grantdate;
    }

    public String getScenecheckresult() {
        return scenecheckresult;
    }

    public void setScenecheckresult(String scenecheckresult) {
        this.scenecheckresult = scenecheckresult;
    }

    public String getScenechecksuggest() {
        return scenechecksuggest;
    }

    public void setScenechecksuggest(String scenechecksuggest) {
        this.scenechecksuggest = scenechecksuggest;
    }

    public String getLatestamount() {
        return latestamount;
    }

    public void setLatestamount(String latestamount) {
        this.latestamount = latestamount;
    }

    public String getLatestinterest() {
        return latestinterest;
    }

    public void setLatestinterest(String latestinterest) {
        this.latestinterest = latestinterest;
    }

    public String getRepaytype() {
        return repaytype;
    }

    public void setRepaytype(String repaytype) {
        this.repaytype = repaytype;
    }

    public String getApplyterm() {
        return applyterm;
    }

    public void setApplyterm(String applyterm) {
        this.applyterm = applyterm;
    }

    public String getTurnover() {
        return turnover;
    }

    public void setTurnover(String turnover) {
        this.turnover = turnover;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", businessid='" + businessid + '\'' +
                ", grantdate='" + grantdate + '\'' +
                ", scenecheckresult='" + scenecheckresult + '\'' +
                ", scenechecksuggest='" + scenechecksuggest + '\'' +
                ", latestamount='" + latestamount + '\'' +
                ", latestinterest='" + latestinterest + '\'' +
                ", repaytype='" + repaytype + '\'' +
                ", applyterm='" + applyterm + '\'' +
                ", turnover='" + turnover + '\'' +
                '}';
    }
}

