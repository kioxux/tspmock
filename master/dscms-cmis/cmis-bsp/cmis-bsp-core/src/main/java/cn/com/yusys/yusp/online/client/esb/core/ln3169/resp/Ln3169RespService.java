package cn.com.yusys.yusp.online.client.esb.core.ln3169.resp;

/**
 * 响应Service：资产证券化处理文件导入
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3169RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3169RespService{" +
				"service=" + service +
				'}';
	}
}
