package cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.req;

import java.math.BigDecimal;

/**
 * 请求Service：买方资金存入
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道码
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servsq;//渠道流水
    private String servdt;//请求方日期
    private String servti;//请求方时间

    private String ipaddr;//请求方IP
    private String mac;//请求方MAC
    private String xybhsq;//协议编号
    private String qydm;//区域代码
    private BigDecimal tranam;//存款金额
    private String zjxz;//资金性质
    private String ckrq;//存款日期
    private String acctno;//买方账号
    private String acctsa;//买方户名
    private String acctba;//资金托管账号
    private String dataid;//资金托管账户名
    private String sfxz;//是否销账标志

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    public String getQydm() {
        return qydm;
    }

    public void setQydm(String qydm) {
        this.qydm = qydm;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getZjxz() {
        return zjxz;
    }

    public void setZjxz(String zjxz) {
        this.zjxz = zjxz;
    }

    public String getCkrq() {
        return ckrq;
    }

    public void setCkrq(String ckrq) {
        this.ckrq = ckrq;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctsa() {
        return acctsa;
    }

    public void setAcctsa(String acctsa) {
        this.acctsa = acctsa;
    }

    public String getAcctba() {
        return acctba;
    }

    public void setAcctba(String acctba) {
        this.acctba = acctba;
    }

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    public String getSfxz() {
        return sfxz;
    }

    public void setSfxz(String sfxz) {
        this.sfxz = sfxz;
    }
}
