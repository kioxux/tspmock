package cn.com.yusys.yusp.online.client.esb.irs.common;

/**
 * 请求Service：交易请求信息域:担保合同与抵质押、保证人关联信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class GuaranteePleMortInfoRecord {
    private String guar_cont_no; // 担保合同流水号
    private String guaranty_id; // 押品编号

    public String getGuar_cont_no() {
        return guar_cont_no;
    }

    public void setGuar_cont_no(String guar_cont_no) {
        this.guar_cont_no = guar_cont_no;
    }

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    @Override
    public String toString() {
        return "GuaranteePleMortInfo{" +
                "guar_cont_no='" + guar_cont_no + '\'' +
                ", guaranty_id='" + guaranty_id + '\'' +
                '}';
    }
}