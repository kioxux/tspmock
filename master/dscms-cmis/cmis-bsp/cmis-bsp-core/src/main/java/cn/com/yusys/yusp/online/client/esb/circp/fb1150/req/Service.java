package cn.com.yusys.yusp.online.client.esb.circp.fb1150.req;

/**
 * 请求Service：省心快贷plus授信申请
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水
    private String lmt_serno;//信贷授信申请流水号
    private String ent_name;//企业名称
    private String ent_cust_id;//企业核心客户号
    private String org_code;//企业统一社会信用代码
    private String cust_name;//企业法人客户名称
    private String cert_code;//企业法人身份证号码
    private String cust_id;//企业法人核心客户号
    private String phone;//企业法人手机号
    private String spouse_name;//企业法人配偶客户名称
    private String spouse_code;//企业法人配偶身份证号码
    private String spouse_cust_id;//企业法人配偶核心客户号
    private String spouse_phone;//企业法人配偶手机号
    private String net_assets;//净资产
    private String sales_indicators;//销售指标
    private String appamt;//申请金额
    private String sxstartdate;//授信起始日期
    private String sxenddate;//授信到期日期
    private String manager_id;//管户经理ID
    private String manager_name;//管户经理名称
    private String manager_org_id;//管户经理所属机构ID
    private String manager_org_name;//管户经理所属机构名称
    private cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.List list;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getEnt_name() {
        return ent_name;
    }

    public void setEnt_name(String ent_name) {
        this.ent_name = ent_name;
    }

    public String getEnt_cust_id() {
        return ent_cust_id;
    }

    public void setEnt_cust_id(String ent_cust_id) {
        this.ent_cust_id = ent_cust_id;
    }

    public String getOrg_code() {
        return org_code;
    }

    public void setOrg_code(String org_code) {
        this.org_code = org_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_code() {
        return spouse_code;
    }

    public void setSpouse_code(String spouse_code) {
        this.spouse_code = spouse_code;
    }

    public String getSpouse_cust_id() {
        return spouse_cust_id;
    }

    public void setSpouse_cust_id(String spouse_cust_id) {
        this.spouse_cust_id = spouse_cust_id;
    }

    public String getSpouse_phone() {
        return spouse_phone;
    }

    public void setSpouse_phone(String spouse_phone) {
        this.spouse_phone = spouse_phone;
    }

    public String getNet_assets() {
        return net_assets;
    }

    public void setNet_assets(String net_assets) {
        this.net_assets = net_assets;
    }

    public String getSales_indicators() {
        return sales_indicators;
    }

    public void setSales_indicators(String sales_indicators) {
        this.sales_indicators = sales_indicators;
    }


    public String getAppamt() {
        return appamt;
    }

    public void setAppamt(String appamt) {
        this.appamt = appamt;
    }

    public String getSxstartdate() {
        return sxstartdate;
    }

    public void setSxstartdate(String sxstartdate) {
        this.sxstartdate = sxstartdate;
    }

    public String getSxenddate() {
        return sxenddate;
    }

    public void setSxenddate(String sxenddate) {
        this.sxenddate = sxenddate;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_org_id() {
        return manager_org_id;
    }

    public void setManager_org_id(String manager_org_id) {
        this.manager_org_id = manager_org_id;
    }

    public String getManager_org_name() {
        return manager_org_name;
    }

    public void setManager_org_name(String manager_org_name) {
        this.manager_org_name = manager_org_name;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", lmt_serno='" + lmt_serno + '\'' +
                ", ent_name='" + ent_name + '\'' +
                ", ent_cust_id='" + ent_cust_id + '\'' +
                ", org_code='" + org_code + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cust_id='" + cust_id + '\'' +
                ", phone='" + phone + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", spouse_code='" + spouse_code + '\'' +
                ", spouse_cust_id='" + spouse_cust_id + '\'' +
                ", spouse_phone='" + spouse_phone + '\'' +
                ", net_assets='" + net_assets + '\'' +
                ", sales_indicators='" + sales_indicators + '\'' +
                ", appamt='" + appamt + '\'' +
                ", sxstartdate='" + sxstartdate + '\'' +
                ", sxenddate='" + sxenddate + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", manager_org_id='" + manager_org_id + '\'' +
                ", manager_org_name='" + manager_org_name + '\'' +
                ", list=" + list +
                '}';
    }
}
