package cn.com.yusys.yusp.web.server.biz.xdht0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0006.req.Xdht0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0006.resp.Xdht0006RespDto;
import cn.com.yusys.yusp.dto.server.xdht0006.req.Xdht0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0006.resp.Xdht0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:双录流水与合同号同步
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0006:双录流水与合同号同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0006Resource.class);
	@Autowired
	private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0006
     * 交易描述：双录流水与合同号同步
     *
     * @param xdht0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("双录流水与合同号同步")
    @PostMapping("/xdht0006")
    //@Idempotent({"xdcaht0006", "#xdht0006ReqDto.datasq"})
    protected @ResponseBody
	Xdht0006RespDto xdht0006(@Validated @RequestBody Xdht0006ReqDto xdht0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, JSON.toJSONString(xdht0006ReqDto));
        Xdht0006DataReqDto xdht0006DataReqDto = new Xdht0006DataReqDto();// 请求Data： 双录流水与合同号同步
        Xdht0006DataRespDto xdht0006DataRespDto = new Xdht0006DataRespDto();// 响应Data：双录流水与合同号同步
		Xdht0006RespDto xdht0006RespDto = new Xdht0006RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdht0006.req.Data reqData = null; // 请求Data：双录流水与合同号同步
		cn.com.yusys.yusp.dto.server.biz.xdht0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0006.resp.Data();// 响应Data：双录流水与合同号同步
        //  此处包导入待确定 结束
        try {
            // 从 xdht0006ReqDto获取 reqData
            reqData = xdht0006ReqDto.getData();
            // 将 reqData 拷贝到xdht0006DataReqDto
            BeanUtils.copyProperties(reqData, xdht0006DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, JSON.toJSONString(xdht0006DataReqDto));
            ResultDto<Xdht0006DataRespDto> xdht0006DataResultDto = dscmsBizHtClientService.xdht0006(xdht0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, JSON.toJSONString(xdht0006DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0006RespDto.setErorcd(Optional.ofNullable(xdht0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0006RespDto.setErortx(Optional.ofNullable(xdht0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0006DataResultDto.getCode())) {
                xdht0006DataRespDto = xdht0006DataResultDto.getData();
                xdht0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, e.getMessage());
            xdht0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0006RespDto.setDatasq(servsq);

        xdht0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0006.key, DscmsEnum.TRADE_CODE_XDHT0006.value, JSON.toJSONString(xdht0006RespDto));
        return xdht0006RespDto;
    }
}
