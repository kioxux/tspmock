package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 交易对手信息( DealCustomerInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
public class DealCustomerInfo {

    private List<DealCustomerInfoRecord> record; // 交易对手信息

    public List<DealCustomerInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<DealCustomerInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "DealCustomerInfo{" +
                "record=" + record +
                '}';
    }
}
