package cn.com.yusys.yusp.web.server.biz.xdzc0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0001.req.Xdzc0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0001.resp.Xdzc0001RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0001.req.Xdzc0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0001.resp.Xdzc0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户资产池协议列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZC0001:客户资产池协议列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0001Resource.class);

    @Autowired
	private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0001
     * 交易描述：客户资产池协议列表查询
     *
     * @param xdzc0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户资产池协议列表查询")
    @PostMapping("/xdzc0001")
    //@Idempotent({"xdzc001", "#xdzc0001ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0001RespDto xdzc0001(@Validated @RequestBody Xdzc0001ReqDto xdzc0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, JSON.toJSONString(xdzc0001ReqDto));
        Xdzc0001DataReqDto xdzc0001DataReqDto = new Xdzc0001DataReqDto();// 请求Data： 客户资产池协议列表查询
        Xdzc0001DataRespDto xdzc0001DataRespDto = new Xdzc0001DataRespDto();// 响应Data：客户资产池协议列表查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0001.req.Data reqData = null; // 请求Data：客户资产池协议列表查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0001.resp.Data();// 响应Data：客户资产池协议列表查询
		Xdzc0001RespDto xdzc0001RespDto = new Xdzc0001RespDto();
		try {
            // 从 xdzc0001ReqDto获取 reqData
            reqData = xdzc0001ReqDto.getData();
            // 将 reqData 拷贝到xdzc0001DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, JSON.toJSONString(xdzc0001DataReqDto));
            ResultDto<Xdzc0001DataRespDto> xdzc0001DataResultDto = dscmsBizZcClientService.xdzc0001(xdzc0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, JSON.toJSONString(xdzc0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzc0001RespDto.setErorcd(Optional.ofNullable(xdzc0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0001RespDto.setErortx(Optional.ofNullable(xdzc0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0001DataResultDto.getCode())) {
                xdzc0001DataRespDto = xdzc0001DataResultDto.getData();
                xdzc0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, e.getMessage());
            xdzc0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0001RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0001RespDto.setDatasq(servsq);

        xdzc0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0001.key, DscmsEnum.TRADE_CODE_XDZC0001.value, JSON.toJSONString(xdzc0001RespDto));
        return xdzc0001RespDto;
    }
}
