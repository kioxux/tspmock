package cn.com.yusys.yusp.web.server.biz.xdsx0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0013.req.Xdsx0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0013.resp.Xdsx0013RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0013.req.Xdsx0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0013.resp.Xdsx0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:风控发信贷根据客户号查询授信信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0013:风控发信贷根据客户号查询授信信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0013Resource.class);
	@Autowired
	private DscmsBizSxClientService dscmsBizSxClientService;
    /**
     * 交易码：xdsx0013
     * 交易描述：风控发信贷根据客户号查询授信信息
     *
     * @param xdsx0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发信贷根据客户号查询授信信息")
    @PostMapping("/xdsx0013")
    //@Idempotent({"xdcasx0013", "#xdsx0013ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0013RespDto xdsx0013(@Validated @RequestBody Xdsx0013ReqDto xdsx0013ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdsx0013ReqDto));
        Xdsx0013DataReqDto xdsx0013DataReqDto = new Xdsx0013DataReqDto();// 请求Data： 风控发信贷根据客户号查询授信信息
        Xdsx0013DataRespDto xdsx0013DataRespDto = new Xdsx0013DataRespDto();// 响应Data：风控发信贷根据客户号查询授信信息
        Xdsx0013RespDto xdsx0013RespDto = new Xdsx0013RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdsx0013.req.Data reqData = null; // 请求Data：风控发信贷根据客户号查询授信信息
        cn.com.yusys.yusp.dto.server.biz.xdsx0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0013.resp.Data();// 响应Data：风控发信贷根据客户号查询授信信息
        try {
            // 从 xdsx0013ReqDto获取 reqData
            reqData = xdsx0013ReqDto.getData();
            // 将 reqData 拷贝到xdsx0013DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0013DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdsx0013DataReqDto));
            ResultDto<Xdsx0013DataRespDto> xdsx0013DataResultDto = dscmsBizSxClientService.xdsx0013(xdsx0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdsx0013DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0013RespDto.setErorcd(Optional.ofNullable(xdsx0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0013RespDto.setErortx(Optional.ofNullable(xdsx0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0013DataResultDto.getCode())) {
                xdsx0013DataRespDto = xdsx0013DataResultDto.getData();
                xdsx0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0013DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0013RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, e.getMessage());
            xdsx0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0013RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0013RespDto.setDatasq(servsq);

        xdsx0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0013.key, DscmsEnum.TRADE_CODE_XDSX0013.value, JSON.toJSONString(xdsx0013RespDto));
        return xdsx0013RespDto;
    }
}
