package cn.com.yusys.yusp.online.client.esb.core.ln3249.resp;

/**
 * 响应Service：贷款指定日期利息试算
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3249RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
