package cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp;

/**
 * 响应Service：押品我行确认价值同步接口
 *
 * @author code-generator
 * @version 1.0
 */
public class EvalypRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
