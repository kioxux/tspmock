package cn.com.yusys.yusp.web.server.biz.xdxw0035;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0035.req.Xdxw0035ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0035.resp.Xdxw0035RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0035.req.Xdxw0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0035.resp.Xdxw0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据流水号查询无还本续贷抵押信息
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDXW0035:根据流水号查询无还本续贷抵押信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0035Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0035Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0035
     * 交易描述：根据流水号查询无还本续贷抵押信息
     *
     * @param xdxw0035ReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("根据流水号查询无还本续贷抵押信息")
    @PostMapping("/xdxw0035")
    //@Idempotent({"xdcaxw0035", "#xdxw0035ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0035RespDto xdxw0035(@Validated @RequestBody Xdxw0035ReqDto xdxw0035ReqDto ) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, JSON.toJSONString(xdxw0035ReqDto));
        Xdxw0035DataReqDto xdxw0035DataReqDto = new Xdxw0035DataReqDto();// 请求Data： 根据流水号查询无还本续贷抵押信息
        Xdxw0035DataRespDto  xdxw0035DataRespDto =  new Xdxw0035DataRespDto();// 响应Data：根据流水号查询无还本续贷抵押信息
        Xdxw0035RespDto xdxw0035RespDto = new Xdxw0035RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0035.req.Data reqData = null; // 请求Data：根据流水号查询无还本续贷抵押信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0035.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0035.resp.Data();// 响应Data：根据流水号查询无还本续贷抵押信息
        try {
            // 从 xdxw0035ReqDto获取 reqData
            reqData = xdxw0035ReqDto.getData();
            // 将 reqData 拷贝到xdxw0035DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0035DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, JSON.toJSONString(xdxw0035DataReqDto));
            ResultDto<Xdxw0035DataRespDto> xdxw0035DataResultDto= dscmsBizXwClientService.xdxw0035(xdxw0035DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, JSON.toJSONString(xdxw0035DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0035RespDto.setErorcd(Optional.ofNullable(xdxw0035DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0035RespDto.setErortx(Optional.ofNullable(xdxw0035DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,xdxw0035DataResultDto.getCode())) {
                xdxw0035DataRespDto = xdxw0035DataResultDto.getData();
                xdxw0035RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0035RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0035DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0035DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, e.getMessage());
            xdxw0035RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0035RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0035RespDto.setDatasq(servsq);

        xdxw0035RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0035.key, DscmsEnum.TRADE_CODE_XDXW0035.value, JSON.toJSONString(xdxw0035RespDto));
        return xdxw0035RespDto;
    }
}