package cn.com.yusys.yusp.web.server.biz.xddb0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0014.req.Xddb0014ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0014.resp.Xddb0014RespDto;
import cn.com.yusys.yusp.dto.server.xddb0014.req.Xddb0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0014.resp.Xddb0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵押登记注销风控
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDDB0014:抵押登记注销风控")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0014Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0014
     * 交易描述：抵押登记注销风控
     *
     * @param xddb0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记注销风控")
    @PostMapping("/xddb0014")
    //@Idempotent({"xddb0014", "#xddb0014ReqDto.datasq"})
    protected @ResponseBody
    Xddb0014RespDto xddb0014(@Validated @RequestBody Xddb0014ReqDto xddb0014ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014ReqDto));
        Xddb0014DataReqDto xddb0014DataReqDto = new Xddb0014DataReqDto();// 请求Data： 抵押登记注销风控
        Xddb0014DataRespDto xddb0014DataRespDto = new Xddb0014DataRespDto();// 响应Data：抵押登记注销风控
        Xddb0014RespDto xddb0014RespDto = new Xddb0014RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0014.req.Data reqData = null; // 请求Data：抵押登记注销风控
        cn.com.yusys.yusp.dto.server.biz.xddb0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0014.resp.Data();// 响应Data：抵押登记注销风控

        try {
            // 从 xddb0014ReqDto获取 reqData
            reqData = xddb0014ReqDto.getData();
            // 将 reqData 拷贝到xddb0014DataReqDto
            BeanUtils.copyProperties(reqData, xddb0014DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014DataReqDto));
            ResultDto<Xddb0014DataRespDto> xddb0014DataResultDto = dscmsBizDbClientService.xddb0014(xddb0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0014RespDto.setErorcd(Optional.ofNullable(xddb0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0014RespDto.setErortx(Optional.ofNullable(xddb0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0014DataResultDto.getCode())) {
                xddb0014DataRespDto = xddb0014DataResultDto.getData();
                xddb0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0014DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, e.getMessage());
            xddb0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0014RespDto.setDatasq(servsq);

        xddb0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0014.key, DscmsEnum.TRADE_CODE_XDDB0014.value, JSON.toJSONString(xddb0014RespDto));
        return xddb0014RespDto;
    }
}
