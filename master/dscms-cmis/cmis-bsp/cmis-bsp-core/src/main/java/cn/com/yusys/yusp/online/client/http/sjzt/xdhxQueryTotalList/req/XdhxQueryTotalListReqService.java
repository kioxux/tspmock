package cn.com.yusys.yusp.online.client.http.sjzt.xdhxQueryTotalList.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Service：信贷客户核心业绩统计查询列表
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdhxQueryTotalListReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//处理码
    @JsonProperty(value = "servtp")
    private String servtp;//渠道
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "userid")
    private String userid;//柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//部门号

    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "loginId")
    private String loginId;//登录用户
    @JsonProperty(value = "loginBrId")
    private String loginBrId;//登录机构

    @JsonProperty(value = "size")
    private Integer size;
    @JsonProperty(value = "from")
    private Integer from;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getLoginBrId() {
        return loginBrId;
    }

    public void setLoginBrId(String loginBrId) {
        this.loginBrId = loginBrId;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "XdhxQueryTotalListReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", queryType='" + queryType + '\'' +
                ", loginId='" + loginId + '\'' +
                ", loginBrId='" + loginBrId + '\'' +
                ", size=" + size +
                ", from=" + from +
                '}';
    }
}
