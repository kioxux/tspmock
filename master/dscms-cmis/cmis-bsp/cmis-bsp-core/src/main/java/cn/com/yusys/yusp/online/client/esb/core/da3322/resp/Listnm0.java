package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 17:27
 * @since 2021/6/7 17:27
 */
public class Listnm0 {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Record1> record;

    public List<Record1> getRecord() {
        return record;
    }

    public void setRecord(List<Record1> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Listnm0{" +
                "record=" + record +
                '}';
    }
}
