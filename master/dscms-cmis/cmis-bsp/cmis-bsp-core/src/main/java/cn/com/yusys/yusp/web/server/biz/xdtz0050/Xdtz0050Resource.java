package cn.com.yusys.yusp.web.server.biz.xdtz0050;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0050.req.Xdtz0050ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0050.resp.Xdtz0050RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0050.req.Xdtz0050DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0050.resp.Xdtz0050DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:对私客户关联业务检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0050:对私客户关联业务检查")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0050Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0050Resource.class);
	@Autowired
	private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0050
     * 交易描述：对私客户关联业务检查
     *
     * @param xdtz0050ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对私客户关联业务检查")
    @PostMapping("/xdtz0050")
    //@Idempotent({"xdcatz0050", "#xdtz0050ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0050RespDto xdtz0050(@Validated @RequestBody Xdtz0050ReqDto xdtz0050ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, JSON.toJSONString(xdtz0050ReqDto));
        Xdtz0050DataReqDto xdtz0050DataReqDto = new Xdtz0050DataReqDto();// 请求Data： 对私客户关联业务检查
        Xdtz0050DataRespDto xdtz0050DataRespDto = new Xdtz0050DataRespDto();// 响应Data：对私客户关联业务检查
        Xdtz0050RespDto xdtz0050RespDto = new Xdtz0050RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0050.req.Data reqData = null; // 请求Data：对私客户关联业务检查
        cn.com.yusys.yusp.dto.server.biz.xdtz0050.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0050.resp.Data();// 响应Data：对私客户关联业务检查
        try {
            // 从 xdtz0050ReqDto获取 reqData
            reqData = xdtz0050ReqDto.getData();
            // 将 reqData 拷贝到xdtz0050DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0050DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, JSON.toJSONString(xdtz0050DataReqDto));
            ResultDto<Xdtz0050DataRespDto> xdtz0050DataResultDto = dscmsBizTzClientService.xdtz0050(xdtz0050DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, JSON.toJSONString(xdtz0050DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0050RespDto.setErorcd(Optional.ofNullable(xdtz0050DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0050RespDto.setErortx(Optional.ofNullable(xdtz0050DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0050DataResultDto.getCode())) {
                xdtz0050DataRespDto = xdtz0050DataResultDto.getData();
                xdtz0050RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0050RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0050DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0050DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, e.getMessage());
            xdtz0050RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0050RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0050RespDto.setDatasq(servsq);

        xdtz0050RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0050.key, DscmsEnum.TRADE_CODE_XDTZ0050.value, JSON.toJSONString(xdtz0050RespDto));
        return xdtz0050RespDto;
    }
}
