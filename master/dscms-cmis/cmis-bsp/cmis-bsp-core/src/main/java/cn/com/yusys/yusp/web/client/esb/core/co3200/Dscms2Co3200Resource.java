package cn.com.yusys.yusp.web.client.esb.core.co3200;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.LstklnbDkzhzy;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.co3200.req.Co3200ReqService;
import cn.com.yusys.yusp.online.client.esb.core.co3200.resp.Co3200RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(co3200)")
@RestController
@RequestMapping("/api/dscms2coreco")
public class Dscms2Co3200Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Co3200Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 抵质押物的开户（处理码co3200）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("co3200:抵质押物的开户")
    @PostMapping("/co3200")
    protected @ResponseBody
    ResultDto<Co3200RespDto> co3200(@Validated @RequestBody Co3200ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3200.key, EsbEnum.TRADE_CODE_CO3200.value, JSON.toJSONString(reqDto, SerializerFeature.WriteMapNullValue));
        cn.com.yusys.yusp.online.client.esb.core.co3200.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.co3200.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.co3200.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.co3200.resp.Service();
        cn.com.yusys.yusp.online.client.esb.core.co3200.req.List list = new cn.com.yusys.yusp.online.client.esb.core.co3200.req.List();
        Co3200ReqService co3200ReqService = new Co3200ReqService();
        Co3200RespService co3200RespService = new Co3200RespService();
        Co3200RespDto co3200RespDto = new Co3200RespDto();
        ResultDto<Co3200RespDto> co3200ResultDto = new ResultDto<Co3200RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Co3200ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            if (CollectionUtils.nonEmpty(reqDto.getLstklnb_dkzhzy())) {
                List<LstklnbDkzhzy> lstklnbDkzhzys = reqDto.getLstklnb_dkzhzy();
                List<cn.com.yusys.yusp.online.client.esb.core.co3200.req.Record> recordList = new ArrayList<cn.com.yusys.yusp.online.client.esb.core.co3200.req.Record>();
                for (LstklnbDkzhzy lstklnbDkzhzy : lstklnbDkzhzys) {
                    cn.com.yusys.yusp.online.client.esb.core.co3200.req.Record record = new cn.com.yusys.yusp.online.client.esb.core.co3200.req.Record();
                    BeanUtils.copyProperties(lstklnbDkzhzy, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CO3200.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(reqDto.getYngyjigo());//    部门号,和许驰确认取营业机构

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            co3200ReqService.setService(reqService);
            // 将co3200ReqService转换成co3200ReqServiceMap
            Map co3200ReqServiceMap = beanMapUtil.beanToMap(co3200ReqService);
            Map service = (Map) co3200ReqServiceMap.get("service");
            // bigdecimal字段转成map之后出现科学计数法问题解决 modify by chenyong 陈勇
            service.forEach((k, v) -> {
                if (v instanceof Double) {
                    BigDecimal bigDecimal = new BigDecimal(((Double) v));
                    service.put(k, bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });
            co3200ReqServiceMap.put("service", service);
            context.put("tradeDataMap", co3200ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3200.key, EsbEnum.TRADE_CODE_CO3200.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CO3200.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3200.key, EsbEnum.TRADE_CODE_CO3200.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            co3200RespService = beanMapUtil.mapToBean(tradeDataMap, Co3200RespService.class, Co3200RespService.class);
            respService = co3200RespService.getService();

            //  将Co3200RespDto封装到ResultDto<Co3200RespDto>
            co3200ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            co3200ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Co3200RespDto
                BeanUtils.copyProperties(respService, co3200RespDto);
                co3200ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                co3200ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                co3200ResultDto.setCode(EpbEnum.EPB099999.key);
                co3200ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3200.key, EsbEnum.TRADE_CODE_CO3200.value, e.getMessage());
            co3200ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            co3200ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        co3200ResultDto.setData(co3200RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3200.key, EsbEnum.TRADE_CODE_CO3200.value, JSON.toJSONString(co3200ResultDto, SerializerFeature.WriteMapNullValue));
        return co3200ResultDto;
    }


}
