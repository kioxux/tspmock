package cn.com.yusys.yusp.online.client.esb.core.dp2200.resp;

/**
 * 响应Service：保证金账户部提
 * @author lihh
 * @version 1.0             
 */      
public class Dp2200RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
