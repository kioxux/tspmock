package cn.com.yusys.yusp.web.server.biz.xdzc0024;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0024.req.Xdzc0024ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0024.resp.Xdzc0024RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0024.req.Xdzc0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0024.resp.Xdzc0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:票据池资料补全查询
 *
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDZC0024:票据池资料补全查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0024Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0024
     * 交易描述：票据池资料补全查询
     *
     * @param xdzc0024ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("票据池资料补全查询")
    @PostMapping("/xdzc0024")
    //@Idempotent({"xdzc024", "#xdzc0024ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0024RespDto xdzc0024(@Validated @RequestBody Xdzc0024ReqDto xdzc0024ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, JSON.toJSONString(xdzc0024ReqDto));
        Xdzc0024DataReqDto xdzc0024DataReqDto = new Xdzc0024DataReqDto();// 请求Data： 票据池资料补全查询
        Xdzc0024DataRespDto xdzc0024DataRespDto = new Xdzc0024DataRespDto();// 响应Data：票据池资料补全查询
        Xdzc0024RespDto xdzc0024RespDto = new Xdzc0024RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0024.req.Data reqData = null; // 请求Data：票据池资料补全查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0024.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0024.resp.Data();// 响应Data：票据池资料补全查询

        try {
            // 从 xdzc0024ReqDto获取 reqData
            reqData = xdzc0024ReqDto.getData();
            // 将 reqData 拷贝到xdzc0024DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0024DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, JSON.toJSONString(xdzc0024DataReqDto));
            ResultDto<Xdzc0024DataRespDto> xdzc0024DataResultDto = dscmsBizZcClientService.xdzc0024(xdzc0024DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, JSON.toJSONString(xdzc0024DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0024RespDto.setErorcd(Optional.ofNullable(xdzc0024DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0024RespDto.setErortx(Optional.ofNullable(xdzc0024DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0024DataResultDto.getCode())) {
                xdzc0024DataRespDto = xdzc0024DataResultDto.getData();
                xdzc0024RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0024RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0024DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0024DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, e.getMessage());
            xdzc0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0024RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0024RespDto.setDatasq(servsq);

        xdzc0024RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0024.key, DscmsEnum.TRADE_CODE_XDZC0024.value, JSON.toJSONString(xdzc0024RespDto));
        return xdzc0024RespDto;
    }
}
