package cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp;

public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
