package cn.com.yusys.yusp.web.server.biz.xddb0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0015.req.Xddb0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0015.resp.Xddb0015RespDto;
import cn.com.yusys.yusp.dto.server.xddb0015.req.Xddb0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0015.resp.Xddb0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品状态变更推送
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0015:押品状态变更推送")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0015Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0015
     * 交易描述：押品状态变更推送
     *
     * @param xddb0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品状态变更推送")
    @PostMapping("/xddb0015")
    //@Idempotent({"xddb0015", "#xddb0015ReqDto.datasq"})
    protected @ResponseBody
    Xddb0015RespDto xddb0015(@Validated @RequestBody Xddb0015ReqDto xddb0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015ReqDto));
        Xddb0015DataReqDto xddb0015DataReqDto = new Xddb0015DataReqDto();// 请求Data： 押品状态变更推送
        Xddb0015DataRespDto xddb0015DataRespDto = new Xddb0015DataRespDto();// 响应Data：押品状态变更推送
        Xddb0015RespDto xddb0015RespDto=new Xddb0015RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0015.req.Data reqData = null; // 请求Data：押品状态变更推送
        cn.com.yusys.yusp.dto.server.biz.xddb0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0015.resp.Data();// 响应Data：押品状态变更推送

        try {
            // 从 xddb0015ReqDto获取 reqData
            reqData = xddb0015ReqDto.getData();
            // 将 reqData 拷贝到xddb0015DataReqDto
            BeanUtils.copyProperties(reqData, xddb0015DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataReqDto));
            ResultDto<Xddb0015DataRespDto> xddb0015DataResultDto = dscmsBizDbClientService.xddb0015(xddb0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0015RespDto.setErorcd(Optional.ofNullable(xddb0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0015RespDto.setErortx(Optional.ofNullable(xddb0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0015DataResultDto.getCode())) {
                xddb0015DataRespDto = xddb0015DataResultDto.getData();
                xddb0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, e.getMessage());
            xddb0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0015RespDto.setDatasq(servsq);

        xddb0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0015.key, DscmsEnum.TRADE_CODE_XDDB0015.value, JSON.toJSONString(xddb0015RespDto));
        return xddb0015RespDto;
    }
}
