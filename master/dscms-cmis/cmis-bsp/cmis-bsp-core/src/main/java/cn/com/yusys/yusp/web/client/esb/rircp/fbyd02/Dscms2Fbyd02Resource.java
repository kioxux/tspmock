package cn.com.yusys.yusp.web.client.esb.rircp.fbyd02;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd02.Fbyd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd02.Fbyd02RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.req.Fbyd02ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.resp.Fbyd02RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbyd02）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbyd02Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbyd02Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbyd02
     * 交易描述：终审申请提交
     *
     * @param fbyd02ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbyd02:终审申请提交")
    @PostMapping("/fbyd02")
    protected @ResponseBody
    ResultDto<Fbyd02RespDto> fbyd02(@Validated @RequestBody Fbyd02ReqDto fbyd02ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD02.key, EsbEnum.TRADE_CODE_FBYD02.value, JSON.toJSONString(fbyd02ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.resp.Service();
        Fbyd02ReqService fbyd02ReqService = new Fbyd02ReqService();
        Fbyd02RespService fbyd02RespService = new Fbyd02RespService();
        Fbyd02RespDto fbyd02RespDto = new Fbyd02RespDto();
        ResultDto<Fbyd02RespDto> fbyd02ResultDto = new ResultDto<Fbyd02RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbyd02ReqDto转换成reqService
            BeanUtils.copyProperties(fbyd02ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBYD02.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbyd02ReqService.setService(reqService);
            // 将fbyd02ReqService转换成fbyd02ReqServiceMap
            Map fbyd02ReqServiceMap = beanMapUtil.beanToMap(fbyd02ReqService);
            context.put("tradeDataMap", fbyd02ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD02.key, EsbEnum.TRADE_CODE_FBYD02.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBYD02.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD02.key, EsbEnum.TRADE_CODE_FBYD02.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbyd02RespService = beanMapUtil.mapToBean(tradeDataMap, Fbyd02RespService.class, Fbyd02RespService.class);
            respService = fbyd02RespService.getService();

            fbyd02ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbyd02ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成fbyd02RespDto
                BeanUtils.copyProperties(respService, fbyd02RespDto);
                fbyd02ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbyd02ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbyd02ResultDto.setCode(EpbEnum.EPB099999.key);
                fbyd02ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD02.key, EsbEnum.TRADE_CODE_FBYD02.value, e.getMessage());
            fbyd02ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbyd02ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbyd02ResultDto.setData(fbyd02RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD02.key, EsbEnum.TRADE_CODE_FBYD02.value, JSON.toJSONString(fbyd02ResultDto));
        return fbyd02ResultDto;
    }
}
