package cn.com.yusys.yusp.online.client.esb.core.ln3046.req;

/**
 * 请求Service：贷款核销归还
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3046ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3046ReqService{" +
                "service=" + service +
                '}';
    }
}
