package cn.com.yusys.yusp.online.client.esb.core.co3225.resp;

/**
 * 响应Service：抵质押物明细查询
 * @author code-generator
 * @version 1.0             
 */      
public class Co3225RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
