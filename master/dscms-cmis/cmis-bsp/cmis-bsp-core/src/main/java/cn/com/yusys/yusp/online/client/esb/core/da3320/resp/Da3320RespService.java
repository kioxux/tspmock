package cn.com.yusys.yusp.online.client.esb.core.da3320.resp;

/**
 * 响应Service：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Da3320RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3320RespService{" +
                "service=" + service +
                '}';
    }
}
