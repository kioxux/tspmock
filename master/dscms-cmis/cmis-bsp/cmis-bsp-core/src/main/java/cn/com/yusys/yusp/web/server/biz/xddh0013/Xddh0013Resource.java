package cn.com.yusys.yusp.web.server.biz.xddh0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0013.req.Xddh0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0013.resp.Xddh0013RespDto;
import cn.com.yusys.yusp.dto.server.xddh0013.req.Xddh0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0013.resp.Xddh0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询提前还款授权信息列表
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDDH0013:查询提前还款授权信息列表")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0013Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;
    /**
     * 交易码：xddh0013
     * 交易描述：查询提前还款授权信息列表
     *
     * @param xddh0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询提前还款授权信息列表")
    @PostMapping("/xddh0013")
//@Idempotent({"xddh0013", "#xddh0013ReqDto.datasq"})
    protected @ResponseBody
    Xddh0013RespDto xddh0013(@Validated @RequestBody Xddh0013ReqDto xddh0013ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, JSON.toJSONString(xddh0013ReqDto));
        Xddh0013DataReqDto xddh0013DataReqDto = new Xddh0013DataReqDto();// 请求Data： 查询提前还款授权信息列表
        Xddh0013DataRespDto xddh0013DataRespDto = new Xddh0013DataRespDto();// 响应Data：查询提前还款授权信息列表
        Xddh0013RespDto xddh0013RespDto = new Xddh0013RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddh0013.req.Data reqData = null; // 请求Data：查询提前还款授权信息列表
        cn.com.yusys.yusp.dto.server.biz.xddh0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0013.resp.Data();// 响应Data：查询提前还款授权信息列表
        try {
        // 从 xddh0013ReqDto获取 reqData
        reqData = xddh0013ReqDto.getData();
        // 将 reqData 拷贝到xddh0013DataReqDto
        BeanUtils.copyProperties(reqData, xddh0013DataReqDto);
        ResultDto<Xddh0013DataRespDto> xddh0013DataResultDto = dscmsBizDhClientService.xddh0013(xddh0013DataReqDto);
        // 从返回值中获取响应码和响应消息
        xddh0013RespDto.setErorcd(Optional.ofNullable(xddh0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
        xddh0013RespDto.setErortx(Optional.ofNullable(xddh0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
        xddh0013RespDto.setDatasq("test");
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0013DataResultDto.getCode())) {
            xddh0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
            xddh0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
            xddh0013DataRespDto = xddh0013DataResultDto.getData();
            // 将 xddh0013DataRespDto拷贝到 respData
            BeanUtils.copyProperties(xddh0013DataRespDto, respData);
        }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, e.getMessage());
            xddh0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0013RespDto.setDatasq(servsq);

        xddh0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0013.key, DscmsEnum.TRADE_CODE_XDDH0013.value, JSON.toJSONString(xddh0013RespDto));
        return xddh0013RespDto;
    }
}
