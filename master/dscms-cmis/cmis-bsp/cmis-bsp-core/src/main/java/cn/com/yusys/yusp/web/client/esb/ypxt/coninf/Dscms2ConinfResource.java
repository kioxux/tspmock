package cn.com.yusys.yusp.web.client.esb.ypxt.coninf;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.ConinfReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.Record;
import cn.com.yusys.yusp.online.client.esb.ypxt.coninf.resp.ConinfRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(coninf)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2ConinfResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2ConinfResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 信贷担保合同信息同步接口（处理码coninf）
     *
     * @param coninfReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("coninf:信贷担保合同信息同步接口")
    @PostMapping("/coninf")
    protected @ResponseBody
    ResultDto<ConinfRespDto> coninf(@Validated @RequestBody ConinfReqDto coninfReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value, JSON.toJSONString(coninfReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.coninf.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.coninf.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.List list = new cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.List();
        ConinfReqService coninfReqService = new ConinfReqService();
        ConinfRespService coninfRespService = new ConinfRespService();
        ConinfRespDto coninfRespDto = new ConinfRespDto();
        ResultDto<ConinfRespDto> coninfResultDto = new ResultDto<ConinfRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            BeanUtils.copyProperties(coninfReqDto, reqService);
            //  将ConinfReqDto转换成reqService
            if (CollectionUtils.nonEmpty(coninfReqDto.getConinfListInfo())) {
                List<ConinfListInfo> coninfListInfos = coninfReqDto.getConinfListInfo();
                List<Record> recordList = new ArrayList<Record>();
                for (ConinfListInfo coninfListInfo : coninfListInfos) {
                    cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req.Record();
                    BeanUtils.copyProperties(coninfListInfo, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CONINF.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            coninfReqService.setService(reqService);
            // 将coninfReqService转换成coninfReqServiceMap
            Map coninfReqServiceMap = beanMapUtil.beanToMap(coninfReqService);
            context.put("tradeDataMap", coninfReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CONINF.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            coninfRespService = beanMapUtil.mapToBean(tradeDataMap, ConinfRespService.class, ConinfRespService.class);
            respService = coninfRespService.getService();
            //  将respService转换成ConinfRespDto
            BeanUtils.copyProperties(respService, coninfRespDto);
            //  将ConinfRespDto封装到ResultDto<ConinfRespDto>
            coninfResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            coninfResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, coninfRespDto);
                coninfResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                coninfResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                coninfResultDto.setCode(EpbEnum.EPB099999.key);
                coninfResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value, e.getMessage());
            coninfResultDto.setCode(EpbEnum.EPB099999.key);//9999
            coninfResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        coninfResultDto.setData(coninfRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONINF.key, EsbEnum.TRADE_CODE_CONINF.value, JSON.toJSONString(coninfResultDto));
        return coninfResultDto;
    }
}
