package cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req;

/**
 * 请求Service：权证信息同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class Record {

    private String yptybh; //押品统一编号

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "yptybh='" + yptybh + '\'' +
                '}';
    }
}
