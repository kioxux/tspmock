package cn.com.yusys.yusp.web.server.biz.xdls0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdls0004.req.Xdls0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdls0004.resp.Xdls0004RespDto;
import cn.com.yusys.yusp.dto.server.xdls0004.req.Xdls0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0004.resp.Xdls0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizLsClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询信贷有无授信历史
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDLS0004:查询信贷有无授信历史")
@RestController
@RequestMapping("/api/dscms")
public class Xdls0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdls0004Resource.class);
    @Autowired
    private DscmsBizLsClientService dscmsBizLsClientService;

    /**
     * 交易码：xdls0004
     * 交易描述：查询信贷有无授信历史
     *
     * @param xdls0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询信贷有无授信历史")
    @PostMapping("/xdls0004")
    //@Idempotent({"xdcals0004", "#xdls0004ReqDto.datasq"})
    protected @ResponseBody
    Xdls0004RespDto xdls0004(@Validated @RequestBody Xdls0004ReqDto xdls0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, JSON.toJSONString(xdls0004ReqDto));
        Xdls0004DataReqDto xdls0004DataReqDto = new Xdls0004DataReqDto();// 请求Data： 查询信贷有无授信历史
        Xdls0004DataRespDto xdls0004DataRespDto = new Xdls0004DataRespDto();// 响应Data：查询信贷有无授信历史
        Xdls0004RespDto xdls0004RespDto = new Xdls0004RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdls0004.req.Data reqData = null; // 请求Data：查询信贷有无授信历史
        cn.com.yusys.yusp.dto.server.biz.xdls0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdls0004.resp.Data();// 响应Data：查询信贷有无授信历史

        try {
            // 从 xdls0004ReqDto获取 reqData
            reqData = xdls0004ReqDto.getData();
            // 将 reqData 拷贝到xdls0004DataReqDto
            BeanUtils.copyProperties(reqData, xdls0004DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, JSON.toJSONString(xdls0004DataReqDto));
            ResultDto<Xdls0004DataRespDto> xdls0004DataResultDto = dscmsBizLsClientService.xdls0004(xdls0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, JSON.toJSONString(xdls0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdls0004RespDto.setErorcd(Optional.ofNullable(xdls0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdls0004RespDto.setErortx(Optional.ofNullable(xdls0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdls0004DataResultDto.getCode())) {
                xdls0004DataRespDto = xdls0004DataResultDto.getData();
                xdls0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdls0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdls0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdls0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, e.getMessage());
            xdls0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdls0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdls0004RespDto.setDatasq(servsq);

        xdls0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0004.key, DscmsEnum.TRADE_CODE_XDLS0004.value, JSON.toJSONString(xdls0004RespDto));
        return xdls0004RespDto;
    }
}
