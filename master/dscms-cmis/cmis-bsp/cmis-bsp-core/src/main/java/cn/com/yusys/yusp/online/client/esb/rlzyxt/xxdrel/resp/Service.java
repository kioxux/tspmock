package cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp;

/**
 * 响应Service：查询人员基本信息岗位信息家庭信息
 *
 * @author lihh
 * @version 1.0
 */
public class Service {

    private String erorcd; // 响应码,是,
    private String erortx; // 响应信息,是,state为new时，值为taskId;为old时，为报告URL
    private String servsq; // 渠道流水,是,
    private String listnm;
    private List1 list;
    private List2 list2;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getListnm() {
        return listnm;
    }

    public void setListnm(String listnm) {
        this.listnm = listnm;
    }

    public List1 getList() {
        return list;
    }

    public void setList(List1 list) {
        this.list = list;
    }

    public List2 getList2() {
        return list2;
    }

    public void setList2(List2 list2) {
        this.list2 = list2;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", listnm='" + listnm + '\'' +
                ", list=" + list +
                ", list2=" + list2 +
                '}';
    }
}
