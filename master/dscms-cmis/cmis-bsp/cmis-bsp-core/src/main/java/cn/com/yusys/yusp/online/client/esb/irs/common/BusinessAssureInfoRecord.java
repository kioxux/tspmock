package cn.com.yusys.yusp.online.client.esb.irs.common;

/**
 * 请求Service：交易请求信息域:担保合同与合同关联信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class BusinessAssureInfoRecord {
    private String serialno; // 流水号
    private String cont_no; // 合同编号
    private String guar_cont_no; // 担保合同流水号

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getGuar_cont_no() {
        return guar_cont_no;
    }

    public void setGuar_cont_no(String guar_cont_no) {
        this.guar_cont_no = guar_cont_no;
    }

    @Override
    public String toString() {
        return "BusinessAssureInfo{" +
                "serialno='" + serialno + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", guar_cont_no='" + guar_cont_no + '\'' +
                '}';
    }
}
