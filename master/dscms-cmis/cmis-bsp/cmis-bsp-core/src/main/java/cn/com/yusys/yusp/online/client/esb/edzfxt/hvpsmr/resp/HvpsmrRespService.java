package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.resp;

/**
 * 响应Service：大额往帐一体化
 *
 * @author code-generator
 * @version 1.0
 */
public class HvpsmrRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
