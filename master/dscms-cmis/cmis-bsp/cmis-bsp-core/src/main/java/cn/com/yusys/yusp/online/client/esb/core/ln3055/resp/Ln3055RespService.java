package cn.com.yusys.yusp.online.client.esb.core.ln3055.resp;

/**
 * 响应Service：贷款利率调整
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3055RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
