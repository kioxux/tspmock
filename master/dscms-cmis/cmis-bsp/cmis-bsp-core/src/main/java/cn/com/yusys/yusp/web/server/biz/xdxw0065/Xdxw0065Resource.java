package cn.com.yusys.yusp.web.server.biz.xdxw0065;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0065.req.Xdxw0065ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0065.resp.Xdxw0065RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0065.req.Xdxw0065DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0065.resp.Xdxw0065DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:调查报告审批结果信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0065:调查报告审批结果信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0065Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0065Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0065
     * 交易描述：调查报告审批结果信息查询
     *
     * @param xdxw0065ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("调查报告审批结果信息查询")
    @PostMapping("/xdxw0065")
    //@Idempotent({"xdcaxw0065", "#xdxw0065ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0065RespDto xdxw0065(@Validated @RequestBody Xdxw0065ReqDto xdxw0065ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065ReqDto));
        Xdxw0065DataReqDto xdxw0065DataReqDto = new Xdxw0065DataReqDto();// 请求Data： 调查报告审批结果信息查询
        Xdxw0065DataRespDto xdxw0065DataRespDto = new Xdxw0065DataRespDto();// 响应Data：调查报告审批结果信息查询
        Xdxw0065RespDto xdxw0065RespDto = new Xdxw0065RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0065.req.Data reqData = null; // 请求Data：调查报告审批结果信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0065.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0065.resp.Data();// 响应Data：调查报告审批结果信息查询
        try {
            // 从 xdxw0065ReqDto获取 reqData
            reqData = xdxw0065ReqDto.getData();
            // 将 reqData 拷贝到xdxw0065DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0065DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065DataReqDto));
            ResultDto<Xdxw0065DataRespDto> xdxw0065DataResultDto = dscmsBizXwClientService.xdxw0065(xdxw0065DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0065RespDto.setErorcd(Optional.ofNullable(xdxw0065DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0065RespDto.setErortx(Optional.ofNullable(xdxw0065DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0065DataResultDto.getCode())) {
                xdxw0065DataRespDto = xdxw0065DataResultDto.getData();
                xdxw0065RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0065RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0065DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0065DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, e.getMessage());
            xdxw0065RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0065RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0065RespDto.setDatasq(servsq);
        xdxw0065RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0065.key, DscmsEnum.TRADE_CODE_XDXW0065.value, JSON.toJSONString(xdxw0065RespDto));
        return xdxw0065RespDto;
    }
}
