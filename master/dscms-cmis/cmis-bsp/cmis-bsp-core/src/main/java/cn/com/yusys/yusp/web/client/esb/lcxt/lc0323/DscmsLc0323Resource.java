package cn.com.yusys.yusp.web.client.esb.lcxt.lc0323;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.req.Lc0323ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp.Lc0323RespDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp.Lc0323RespListDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.req.Lc0323ReqService;
import cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp.Lc0323RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:查询理财是否冻结
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用理财系统系统的接口处理类(lc0323)")
@RestController
@RequestMapping("/api/dscms2lcxt")
public class DscmsLc0323Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsLc0323Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：lc0323
     * 交易描述：查询理财是否冻结
     *
     * @param lc0323ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("lc0323:查询理财是否冻结")
    @PostMapping("/lc0323")
    protected @ResponseBody
    ResultDto<Lc0323RespDto> lc0323(@Validated @RequestBody Lc0323ReqDto lc0323ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LC0323.key, EsbEnum.TRADE_CODE_LC0323.value, JSON.toJSONString(lc0323ReqDto));
        cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.req.Service();
        cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp.Service();
        Lc0323ReqService lc0323ReqService = new Lc0323ReqService();
        Lc0323RespService lc0323RespService = new Lc0323RespService();
        Lc0323RespDto lc0323RespDto = new Lc0323RespDto();
        ResultDto<Lc0323RespDto> lc0323ResultDto = new ResultDto<Lc0323RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将lc0323ReqDto转换成reqService
            BeanUtils.copyProperties(lc0323ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LC0323.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_LC.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_LC.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            lc0323ReqService.setService(reqService);
            // 将lc0323ReqService转换成lc0323ReqServiceMap
            Map lc0323ReqServiceMap = beanMapUtil.beanToMap(lc0323ReqService);
            context.put("tradeDataMap", lc0323ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LC0323.key, EsbEnum.TRADE_CODE_LC0323.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LC0323.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LC0323.key, EsbEnum.TRADE_CODE_LC0323.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            lc0323RespService = beanMapUtil.mapToBean(tradeDataMap, Lc0323RespService.class, Lc0323RespService.class);
            respService = lc0323RespService.getService();
            //  将respService转换成Lc0323RespDto
            BeanUtils.copyProperties(respService, lc0323RespDto);
            lc0323ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            lc0323ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, lc0323RespDto);

                cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp.List list = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp.List());
                if (CollectionUtils.isNotEmpty(list.getRecord())) {
                    java.util.List<Lc0323RespListDto> targetList = list.getRecord().stream().map(l -> {
                        Lc0323RespListDto target = new Lc0323RespListDto();
                        BeanUtils.copyProperties(l, target);
                        return target;
                    }).collect(Collectors.toList());
                    lc0323RespDto.setList(targetList);
                }

                lc0323ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                lc0323ResultDto.setMessage(SuccessEnum.SUCCESS.value);

            } else {
                lc0323ResultDto.setCode(EpbEnum.EPB099999.key);
                lc0323ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LC0323.key, EsbEnum.TRADE_CODE_LC0323.value, e.getMessage());
            lc0323ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            lc0323ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        lc0323ResultDto.setData(lc0323RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LC0323.key, EsbEnum.TRADE_CODE_LC0323.value, JSON.toJSONString(lc0323ResultDto));
        return lc0323ResultDto;
    }
}
