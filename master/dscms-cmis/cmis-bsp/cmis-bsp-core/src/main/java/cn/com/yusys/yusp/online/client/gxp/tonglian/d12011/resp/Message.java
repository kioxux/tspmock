package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp;

import cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead;

/**
 * 响应Dto：账单交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Message {
    private GxpRespHead head;
    private Body body;

    public GxpRespHead getHead() {
        return head;
    }

    public void setHead(GxpRespHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
