package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp;

/**
 * 响应Service：查询共有人信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Xddb06RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Xddb06RespService{" +
				"service=" + service +
				'}';
	}
}
