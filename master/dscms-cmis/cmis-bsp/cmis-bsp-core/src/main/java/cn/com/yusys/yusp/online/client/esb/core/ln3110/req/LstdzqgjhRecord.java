package cn.com.yusys.yusp.online.client.esb.core.ln3110.req;

import java.math.BigDecimal;

/**
 * 请求Service：贷款定制期供计划表
 *
 * @author code-generator
 * @version 1.0
 */
public class LstdzqgjhRecord {
    private Integer benqqish;//本期期数
    private String qishriqi;//起始日期
    private String daoqriqi;//到期日期
    private BigDecimal huanbjee;//还本金额
    private BigDecimal hxijinee;//还息金额
    private String zwhkriqi;//最晚还款日

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getZwhkriqi() {
        return zwhkriqi;
    }

    public void setZwhkriqi(String zwhkriqi) {
        this.zwhkriqi = zwhkriqi;
    }

    @Override
    public String toString() {
        return "Lstdzqgjh{" +
                "benqqish=" + benqqish +
                ", qishriqi='" + qishriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", zwhkriqi='" + zwhkriqi + '\'' +
                '}';
    }
}
