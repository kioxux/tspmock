package cn.com.yusys.yusp.web.client.esb.core.ln3091;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3091.Ln3091ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3091.Ln3091RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3091.req.Ln3091ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.Ln3091RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3091)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3091Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3091Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3091
     * 交易描述：待付款指令查询
     *
     * @param ln3091ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3091:待付款指令查询")
    @PostMapping("/ln3091")
    protected @ResponseBody
    ResultDto<Ln3091RespDto> ln3091(@Validated @RequestBody Ln3091ReqDto ln3091ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3091.key, EsbEnum.TRADE_CODE_LN3091.value, JSON.toJSONString(ln3091ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3091.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3091.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.Service();

        Ln3091ReqService ln3091ReqService = new Ln3091ReqService();
        Ln3091RespService ln3091RespService = new Ln3091RespService();
        Ln3091RespDto ln3091RespDto = new Ln3091RespDto();
        ResultDto<Ln3091RespDto> ln3091ResultDto = new ResultDto<Ln3091RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3091ReqDto转换成reqService
            BeanUtils.copyProperties(ln3091ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3091.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3091ReqService.setService(reqService);
            // 将ln3091ReqService转换成ln3091ReqServiceMap
            Map ln3091ReqServiceMap = beanMapUtil.beanToMap(ln3091ReqService);
            context.put("tradeDataMap", ln3091ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3091.key, EsbEnum.TRADE_CODE_LN3091.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3091.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3091.key, EsbEnum.TRADE_CODE_LN3091.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3091RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3091RespService.class, Ln3091RespService.class);
            respService = ln3091RespService.getService();

            ln3091ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3091ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3091RespDto
                BeanUtils.copyProperties(respService, ln3091RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.lstdkzrfk.List lstdkzrfkList = Optional.ofNullable(respService.getList())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.lstdkzrfk.List());
                respService.setList(lstdkzrfkList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.lstdkzrfk.Record> recordList = Optional
                            .ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3091.Lstdkzrfk> lstdkzrfkDtoList = recordList.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3091.Lstdkzrfk lstdkzrfk = new cn.com.yusys.yusp.dto.client.esb.core.ln3091.Lstdkzrfk();
                        BeanUtils.copyProperties(record, lstdkzrfk);
                        return lstdkzrfk;
                    }).collect(Collectors.toList());
                    ln3091RespDto.setLstdkzrfk(lstdkzrfkDtoList);
                }

                ln3091ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3091ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3091ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3091ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3091.key, EsbEnum.TRADE_CODE_LN3091.value, e.getMessage());
            ln3091ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3091ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3091ResultDto.setData(ln3091RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3091.key, EsbEnum.TRADE_CODE_LN3091.value, JSON.toJSONString(ln3091ResultDto));
        return ln3091ResultDto;
    }
}
