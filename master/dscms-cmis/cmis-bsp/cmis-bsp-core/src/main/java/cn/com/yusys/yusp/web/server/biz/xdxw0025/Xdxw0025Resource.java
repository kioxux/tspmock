package cn.com.yusys.yusp.web.server.biz.xdxw0025;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0025.req.Xdxw0025ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0025.resp.Xdxw0025RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0025.req.Xdxw0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0025.resp.Xdxw0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借据下抵押物信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0025:借据下抵押物信息列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0025Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0025
     * 交易描述：借据下抵押物信息列表查询
     *
     * @param xdxw0025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借据下抵押物信息列表查询")
    @PostMapping("/xdxw0025")
    //@Idempotent({"xdcaxw0025", "#xdxw0025ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0025RespDto xdxw0025(@Validated @RequestBody Xdxw0025ReqDto xdxw0025ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025ReqDto));
        Xdxw0025DataReqDto xdxw0025DataReqDto = new Xdxw0025DataReqDto();// 请求Data： 借据下抵押物信息列表查询
        Xdxw0025DataRespDto xdxw0025DataRespDto = new Xdxw0025DataRespDto();// 响应Data：借据下抵押物信息列表查询
        Xdxw0025RespDto xdxw0025RespDto = new Xdxw0025RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0025.req.Data reqData = null; // 请求Data：借据下抵押物信息列表查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0025.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0025.resp.Data();// 响应Data：借据下抵押物信息列表查询
        try {
            // 从 xdxw0025ReqDto获取 reqData
            reqData = xdxw0025ReqDto.getData();
            // 将 reqData 拷贝到xdxw0025DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0025DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025DataReqDto));
            ResultDto<Xdxw0025DataRespDto> xdxw0025DataResultDto = dscmsBizXwClientService.xdxw0025(xdxw0025DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0025RespDto.setErorcd(Optional.ofNullable(xdxw0025DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0025RespDto.setErortx(Optional.ofNullable(xdxw0025DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0025DataResultDto.getCode())) {
                xdxw0025DataRespDto = xdxw0025DataResultDto.getData();
                xdxw0025RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0025RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0025DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0025DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, e.getMessage());
            xdxw0025RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0025RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0025RespDto.setDatasq(servsq);

        xdxw0025RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0025.key, DscmsEnum.TRADE_CODE_XDXW0025.value, JSON.toJSONString(xdxw0025RespDto));
        return xdxw0025RespDto;
    }
}
