package cn.com.yusys.yusp.web.client.esb.core.dp2200;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2200.Dp2200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2200.Dp2200RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.dp2200.req.Dp2200ReqService;
import cn.com.yusys.yusp.online.client.esb.core.dp2200.resp.Dp2200RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:保证金账户部提
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(dp2200)")
@RestController
@RequestMapping("/api/dscms2coredp")
public class Dscms2Dp2200Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dp2200Resource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：dp2200
     * 交易描述：保证金账户部提
     *
     * @param dp2200ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/dp2200")
    protected @ResponseBody
    ResultDto<Dp2200RespDto> dp2200(@Validated @RequestBody Dp2200ReqDto dp2200ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2200.key, EsbEnum.TRADE_CODE_DP2200.value, JSON.toJSONString(dp2200ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.dp2200.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.dp2200.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.dp2200.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.dp2200.resp.Service();
        Dp2200ReqService dp2200ReqService = new Dp2200ReqService();
        Dp2200RespService dp2200RespService = new Dp2200RespService();
        Dp2200RespDto dp2200RespDto = new Dp2200RespDto();
        ResultDto<Dp2200RespDto> dp2200ResultDto = new ResultDto<Dp2200RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将dp2200ReqDto转换成reqService
			BeanUtils.copyProperties(dp2200ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_DP2200.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水

			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			dp2200ReqService.setService(reqService);
			// 将dp2200ReqService转换成dp2200ReqServiceMap
			Map dp2200ReqServiceMap = beanMapUtil.beanToMap(dp2200ReqService);
			context.put("tradeDataMap", dp2200ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2200.key, EsbEnum.TRADE_CODE_DP2200.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DP2200.key, context);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2200.key, EsbEnum.TRADE_CODE_DP2200.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			dp2200RespService = beanMapUtil.mapToBean(tradeDataMap, Dp2200RespService.class, Dp2200RespService.class);
			respService = dp2200RespService.getService();
			dp2200ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
			dp2200ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));

			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Dp2200RespDto
				BeanUtils.copyProperties(respService, dp2200RespDto);
				dp2200ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				dp2200ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				dp2200ResultDto.setCode(EpbEnum.EPB099999.key);
				dp2200ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2200.key, EsbEnum.TRADE_CODE_DP2200.value);
			dp2200ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			dp2200ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		dp2200ResultDto.setData(dp2200RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2200.key, EsbEnum.TRADE_CODE_DP2200.value, JSON.toJSONString(dp2200ResultDto));
        return dp2200ResultDto;
    }
}
