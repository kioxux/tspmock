package cn.com.yusys.yusp.web.server.biz.xdtz0057;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0057.req.Xdtz0057ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0057.resp.Xdtz0057RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0057.req.Xdtz0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0057.resp.Xdtz0057DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据流水号查询客户调查的放款信息（在途需求）
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDTZ0057:根据流水号查询客户调查的放款信息（在途需求）")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0057Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0057Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0057
     * 交易描述：根据流水号查询客户调查的放款信息（在途需求）
     *
     * @param xdtz0057ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据流水号查询客户调查的放款信息（在途需求）")
    @PostMapping("/xdtz0057")
    //@Idempotent({"xdcatz0057", "#xdtz0057ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0057RespDto xdtz0057(@Validated @RequestBody Xdtz0057ReqDto xdtz0057ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057ReqDto));
        Xdtz0057DataReqDto xdtz0057DataReqDto = new Xdtz0057DataReqDto();// 请求Data： 根据流水号查询客户调查的放款信息（在途需求）
        Xdtz0057DataRespDto xdtz0057DataRespDto = new Xdtz0057DataRespDto();// 响应Data：根据流水号查询客户调查的放款信息（在途需求）
        Xdtz0057RespDto xdtz0057RespDto = new Xdtz0057RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0057.req.Data reqData = null; // 请求Data：根据流水号查询客户调查的放款信息（在途需求）
        cn.com.yusys.yusp.dto.server.biz.xdtz0057.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0057.resp.Data();// 响应Data：根据流水号查询客户调查的放款信息（在途需求）
        try {
            // 从 xdtz0057ReqDto获取 reqData
            reqData = xdtz0057ReqDto.getData();
            // 将 reqData 拷贝到xdtz0057DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0057DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataReqDto));
            ResultDto<Xdtz0057DataRespDto> xdtz0057DataResultDto = dscmsBizTzClientService.xdtz0057(xdtz0057DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0057RespDto.setErorcd(Optional.ofNullable(xdtz0057DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0057RespDto.setErortx(Optional.ofNullable(xdtz0057DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0057DataResultDto.getCode())) {
                xdtz0057DataRespDto = xdtz0057DataResultDto.getData();
                xdtz0057RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0057RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0057DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0057DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, e.getMessage());
            xdtz0057RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0057RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0057RespDto.setDatasq(servsq);

        xdtz0057RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0057.key, DscmsEnum.TRADE_CODE_XDTZ0057.value, JSON.toJSONString(xdtz0057RespDto));
        return xdtz0057RespDto;
    }
}
