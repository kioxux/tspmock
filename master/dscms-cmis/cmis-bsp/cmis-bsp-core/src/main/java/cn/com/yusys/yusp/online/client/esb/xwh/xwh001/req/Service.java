package cn.com.yusys.yusp.online.client.esb.xwh.xwh001.req;

import java.math.BigDecimal;

/**
 * 请求Service：借据台账信息接收
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //   全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String idCard;//客户证件号
    private String cusId;//客户号
    private String cusName;//客户名称
    private String billNo;//借据号
    private BigDecimal loanAmount;//放款金额
    private Integer month;//期限
    private String startDate;//借据开始日
    private String endDate;//借据到期日
    private String recommendId;//推荐客户经理号
    private String productName;//产品名称
    private String phone;//手机号

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public BigDecimal getLoanAmount() {
        return loanAmount;
    }

    public void setLoanAmount(BigDecimal loanAmount) {
        this.loanAmount = loanAmount;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getRecommendId() {
        return recommendId;
    }

    public void setRecommendId(String recommendId) {
        this.recommendId = recommendId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", datasq='" + datasq + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", idCard='" + idCard + '\'' +
				", cusId='" + cusId + '\'' +
				", cusName='" + cusName + '\'' +
				", billNo='" + billNo + '\'' +
				", loanAmount=" + loanAmount +
				", month=" + month +
				", startDate='" + startDate + '\'' +
				", endDate='" + endDate + '\'' +
				", recommendId='" + recommendId + '\'' +
				", productName='" + productName + '\'' +
				", phone='" + phone + '\'' +
				'}';
	}
}
