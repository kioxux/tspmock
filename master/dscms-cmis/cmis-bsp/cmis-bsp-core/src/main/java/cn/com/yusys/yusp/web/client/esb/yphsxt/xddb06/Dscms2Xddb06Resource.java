package cn.com.yusys.yusp.web.client.esb.yphsxt.xddb06;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.req.Xddb06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.Xddb06RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.req.Xddb06ReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp.Record;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp.Xddb06RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:查询共有人信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2Xddb06Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xddb06Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xddb06
     * 交易描述：查询共有人信息
     *
     * @param xddb06ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xddb06")
    protected @ResponseBody
    ResultDto<Xddb06RespDto> xddb06(@Validated @RequestBody Xddb06ReqDto xddb06ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(xddb06ReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp.Service();
        Xddb06ReqService xddb06ReqService = new Xddb06ReqService();
        Xddb06RespService xddb06RespService = new Xddb06RespService();
        Xddb06RespDto xddb06RespDto = new Xddb06RespDto();
        ResultDto<Xddb06RespDto> xddb06ResultDto = new ResultDto<Xddb06RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xddb06ReqDto转换成reqService
            BeanUtils.copyProperties(xddb06ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDDB06.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xddb06ReqService.setService(reqService);
            // 将xddb06ReqService转换成xddb06ReqServiceMap
            Map xddb06ReqServiceMap = beanMapUtil.beanToMap(xddb06ReqService);
            context.put("tradeDataMap", xddb06ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDDB06.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xddb06RespService = beanMapUtil.mapToBean(tradeDataMap, Xddb06RespService.class, Xddb06RespService.class);
            respService = xddb06RespService.getService();
            //  将respService转换成Xddb06RespDto
            xddb06ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xddb06ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xddb06RespDto);
                cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp.List list = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp.List());
                if (CollectionUtils.nonEmpty(list.getRecord())) {
                    List<Record> recordList = list.getRecord();
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List> targetList = new ArrayList<>();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List target = new cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.List();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    xddb06RespDto.setList(targetList);
                }
                xddb06ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb06ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xddb06ResultDto.setCode(EpbEnum.EPB099999.key);
                xddb06ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, e.getMessage());
            xddb06ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xddb06ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xddb06ResultDto.setData(xddb06RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB06.key, EsbEnum.TRADE_CODE_XDDB06.value, JSON.toJSONString(xddb06ResultDto));
        return xddb06ResultDto;
    }
}
