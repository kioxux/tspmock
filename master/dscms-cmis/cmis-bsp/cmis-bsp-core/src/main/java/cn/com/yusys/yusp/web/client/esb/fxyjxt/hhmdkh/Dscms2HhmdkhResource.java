package cn.com.yusys.yusp.web.client.esb.fxyjxt.hhmdkh;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.req.HhmdkhReqService;
import cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.resp.HhmdkhRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:BSP封装调用客户风险预警系统的接口
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "BSP封装调用客户风险预警系统的接口处理类(hhmdkh)")
@RestController
@RequestMapping("/api/dscms2fxyjxt")
public class Dscms2HhmdkhResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2HhmdkhResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * 交易码：hhmdkh
     * 交易描述：查询客户是否为黑灰名单客户
     *
     * @param hhmdkhReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("hhmdkh:查询客户是否为黑灰名单客户")
    @PostMapping("/hhmdkh")
    protected @ResponseBody
    ResultDto<HhmdkhRespDto> hhmdkh(@Validated @RequestBody HhmdkhReqDto hhmdkhReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value, JSON.toJSONString(hhmdkhReqDto));
        cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.req.Service();
        cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.resp.Service();
        HhmdkhReqService hhmdkhReqService = new HhmdkhReqService();
        HhmdkhRespService hhmdkhRespService = new HhmdkhRespService();
        HhmdkhRespDto hhmdkhRespDto = new HhmdkhRespDto();
        ResultDto<HhmdkhRespDto> hhmdkhResultDto = new ResultDto<HhmdkhRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将hhmdkhReqDto转换成reqService
            BeanUtils.copyProperties(hhmdkhReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_HHMDKH.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_FXYJXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_FXYJXT.key);//    部门号
            hhmdkhReqService.setService(reqService);
            // 将hhmdkhReqService转换成hhmdkhReqServiceMap
            Map hhmdkhReqServiceMap = beanMapUtil.beanToMap(hhmdkhReqService);
            context.put("tradeDataMap", hhmdkhReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_HHMDKH.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            hhmdkhRespService = beanMapUtil.mapToBean(tradeDataMap, HhmdkhRespService.class, HhmdkhRespService.class);
            respService = hhmdkhRespService.getService();

            hhmdkhResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            hhmdkhResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成HhmdkhRespDto
                BeanUtils.copyProperties(respService, hhmdkhRespDto);
                hhmdkhResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                hhmdkhResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                hhmdkhResultDto.setCode(EpbEnum.EPB099999.key);
                hhmdkhResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value, e.getMessage());
            hhmdkhResultDto.setCode(EpbEnum.EPB099999.key);//9999
            hhmdkhResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        hhmdkhResultDto.setData(hhmdkhRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_HHMDKH.key, EsbEnum.TRADE_CODE_HHMDKH.value, JSON.toJSONString(hhmdkhResultDto));
        return hhmdkhResultDto;
    }
}
