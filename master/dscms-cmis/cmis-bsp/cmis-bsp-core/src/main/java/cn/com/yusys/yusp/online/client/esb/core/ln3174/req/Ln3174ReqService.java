package cn.com.yusys.yusp.online.client.esb.core.ln3174.req;

/**
 * 请求Service：资产证券化欠款借据查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3174ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3174ReqService{" +
				"service=" + service +
				'}';
	}
}
