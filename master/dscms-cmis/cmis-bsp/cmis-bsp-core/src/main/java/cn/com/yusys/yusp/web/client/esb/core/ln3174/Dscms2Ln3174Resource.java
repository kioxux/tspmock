package cn.com.yusys.yusp.web.client.esb.core.ln3174;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3174.Ln3174ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3174.Ln3174RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3174.LstZhzb;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3174.req.Ln3174ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.Ln3174RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3174)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3174Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3174Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3174
     * 交易描述：资产证券化欠款借据查询
     *
     * @param ln3174ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3174:资产证券化欠款借据查询")
    @PostMapping("/ln3174")
    protected @ResponseBody
    ResultDto<Ln3174RespDto> ln3174(@Validated @RequestBody Ln3174ReqDto ln3174ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3174.key, EsbEnum.TRADE_CODE_LN3174.value, JSON.toJSONString(ln3174ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3174.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3174.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.Service();
        Ln3174ReqService ln3174ReqService = new Ln3174ReqService();
        Ln3174RespService ln3174RespService = new Ln3174RespService();
        Ln3174RespDto ln3174RespDto = new Ln3174RespDto();
        ResultDto<Ln3174RespDto> ln3174ResultDto = new ResultDto<Ln3174RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3174ReqDto转换成reqService
            BeanUtils.copyProperties(ln3174ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3174.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3174ReqService.setService(reqService);
            // 将ln3174ReqService转换成ln3174ReqServiceMap
            Map ln3174ReqServiceMap = beanMapUtil.beanToMap(ln3174ReqService);
            context.put("tradeDataMap", ln3174ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3174.key, EsbEnum.TRADE_CODE_LN3174.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3174.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3174.key, EsbEnum.TRADE_CODE_LN3174.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3174RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3174RespService.class, Ln3174RespService.class);
            respService = ln3174RespService.getService();

            ln3174ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3174ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3174RespDto
                BeanUtils.copyProperties(respService, ln3174RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.List ln3174RespList =
                        Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.List());
                respService.setList(ln3174RespList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> recordList = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3174.resp.Record>());
                    java.util.List<LstZhzb> lstZhzbs = recordList.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3174.LstZhzb lstZhzb = new cn.com.yusys.yusp.dto.client.esb.core.ln3174.LstZhzb();
                        BeanUtils.copyProperties(record, lstZhzb);
                        return lstZhzb;
                    }).collect(Collectors.toList());
                    ln3174RespDto.setLstZhzb(lstZhzbs);
                }
                ln3174ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3174ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3174ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3174ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3174.key, EsbEnum.TRADE_CODE_LN3174.value, e.getMessage());
            ln3174ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3174ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3174ResultDto.setData(ln3174RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3174.key, EsbEnum.TRADE_CODE_LN3174.value, JSON.toJSONString(ln3174ResultDto));
        return ln3174ResultDto;
    }

}
