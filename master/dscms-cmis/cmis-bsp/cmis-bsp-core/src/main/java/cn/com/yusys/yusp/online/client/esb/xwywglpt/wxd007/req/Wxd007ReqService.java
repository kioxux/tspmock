package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd007.req;

/**
 * 请求Service：请求小V平台综合决策管理列表查询
 *
 * @author code-generator
 * @version 1.0
 */
public class Wxd007ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
