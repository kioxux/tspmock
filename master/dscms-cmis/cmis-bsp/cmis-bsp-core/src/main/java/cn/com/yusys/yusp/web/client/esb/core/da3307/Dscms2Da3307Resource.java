package cn.com.yusys.yusp.web.client.esb.core.da3307;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3307.req.Da3307ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3307.resp.Da3307RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3307.req.Da3307ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3307.resp.Da3307RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵债资产出租处理
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class Dscms2Da3307Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Da3307Resource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3307
     * 交易描述：抵债资产出租处理
     *
     * @param da3307ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3307")
    protected @ResponseBody
    ResultDto<Da3307RespDto> da3307(@Validated @RequestBody Da3307ReqDto da3307ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3307.key, EsbEnum.TRADE_CODE_DA3307.value, JSON.toJSONString(da3307ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.da3307.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3307.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.da3307.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3307.resp.Service();
        Da3307ReqService da3307ReqService = new Da3307ReqService();
        Da3307RespService da3307RespService = new Da3307RespService();
        Da3307RespDto da3307RespDto = new Da3307RespDto();
        ResultDto<Da3307RespDto> da3307ResultDto = new ResultDto<Da3307RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将da3307ReqDto转换成reqService
			BeanUtils.copyProperties(da3307ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3307.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
			da3307ReqService.setService(reqService);
			// 将da3307ReqService转换成da3307ReqServiceMap
			Map da3307ReqServiceMap = beanMapUtil.beanToMap(da3307ReqService);
			context.put("tradeDataMap", da3307ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3307.key, EsbEnum.TRADE_CODE_DA3307.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3307.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3307.key, EsbEnum.TRADE_CODE_DA3307.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			da3307RespService = beanMapUtil.mapToBean(tradeDataMap, Da3307RespService.class, Da3307RespService.class);
			respService = da3307RespService.getService();
			//  将respService转换成Da3307RespDto
			da3307ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			da3307ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Ln3246RespDto
				BeanUtils.copyProperties(respService, da3307RespDto);
				da3307ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				da3307ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				da3307ResultDto.setCode(EpbEnum.EPB099999.key);
				da3307ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3307.key, EsbEnum.TRADE_CODE_DA3307.value, e.getMessage());
			da3307ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			da3307ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		da3307ResultDto.setData(da3307RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3307.key, EsbEnum.TRADE_CODE_DA3307.value, JSON.toJSONString(da3307ResultDto));
        return da3307ResultDto;
    }
}
