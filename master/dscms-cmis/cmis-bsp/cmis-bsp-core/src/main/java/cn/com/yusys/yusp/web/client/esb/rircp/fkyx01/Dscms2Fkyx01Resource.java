package cn.com.yusys.yusp.web.client.esb.rircp.fkyx01;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.req.Fkyx01ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.resp.Fkyx01RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fkyx01Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fkyx01Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fkyx01
     * 交易描述：优享贷客户经理分配通知接口
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fkyx01:优享贷客户经理分配通知接口")
    @PostMapping("/fkyx01")
    protected @ResponseBody
    ResultDto<Fkyx01RespDto> fkyx01(@Validated @RequestBody Fkyx01ReqDto reqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fkyx01.resp.Service();
        Fkyx01ReqService fkyx01ReqService = new Fkyx01ReqService();
        Fkyx01RespService fkyx01RespService = new Fkyx01RespService();
        Fkyx01RespDto fkyx01RespDto = new Fkyx01RespDto();
        ResultDto<Fkyx01RespDto> fkyx01ResultDto = new ResultDto<Fkyx01RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fkyx01ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_FKYX01.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fkyx01ReqService.setService(reqService);
            // 将fkyx01ReqService转换成fkyx01ReqServiceMap
            Map fkyx01ReqServiceMap = beanMapUtil.beanToMap(fkyx01ReqService);
            context.put("tradeDataMap", fkyx01ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FKYX01.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fkyx01RespService = beanMapUtil.mapToBean(tradeDataMap, Fkyx01RespService.class, Fkyx01RespService.class);
            respService = fkyx01RespService.getService();

            fkyx01ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fkyx01ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fkyx01RespDto
                BeanUtils.copyProperties(respService, fkyx01RespDto);
                fkyx01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fkyx01ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fkyx01ResultDto.setCode(EpbEnum.EPB099999.key);
                fkyx01ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value, e.getMessage());
            fkyx01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fkyx01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fkyx01ResultDto.setData(fkyx01RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKYX01.key, EsbEnum.TRADE_CODE_FKYX01.value, JSON.toJSONString(fkyx01ResultDto));
        return fkyx01ResultDto;
    }
}
