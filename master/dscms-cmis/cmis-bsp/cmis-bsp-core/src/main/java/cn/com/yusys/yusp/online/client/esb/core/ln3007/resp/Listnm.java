package cn.com.yusys.yusp.online.client.esb.core.ln3007.resp;

import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/5/28 10:36:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/28 10:36
 * @since 2021/5/28 10:36
 */
public class Listnm {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3007.resp.Record> recordList;

    public List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        return "Listnm{" +
                "recordList=" + recordList +
                '}';
    }
}
