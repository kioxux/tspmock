package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp;

import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.incomeStatementList.Record;

import java.util.List;

public class IncomeStatementList {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.incomeStatementList.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "IncomeStatementList{" +
                "record=" + record +
                '}';
    }
}
