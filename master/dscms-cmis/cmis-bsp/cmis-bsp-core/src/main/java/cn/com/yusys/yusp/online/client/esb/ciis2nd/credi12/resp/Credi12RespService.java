package cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.resp;

/**
 * 响应Service：ESB通用查询接口（处理码credi12）
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Credi12RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
