package cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 响应Service：查询理财是否冻结
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    @JsonProperty(value = "FunctionId")
    private String FunctionId;//交易代码
    @JsonProperty(value = "ExSerial")
    private String ExSerial;//发起方流水号
    @JsonProperty(value = "SysDate")
    private String SysDate;//交易发生时的系统日期
    @JsonProperty(value = "SysTime")
    private String SysTime;//交易发生时的系统时间

    private String erorcd;//错误码
    private String erortx;//错误描述

    @JsonProperty(value = "TotNum")
    private String TotNum;//总行数
    @JsonProperty(value = "RetNum")
    private String RetNum;//本次返回行数
    @JsonProperty(value = "OffSet")
    private String OffSet;//定位串

    private cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.resp.List list;

    @JsonIgnore
    public String getFunctionId() {
        return FunctionId;
    }

    @JsonIgnore
    public void setFunctionId(String functionId) {
        FunctionId = functionId;
    }

    @JsonIgnore
    public String getExSerial() {
        return ExSerial;
    }

    @JsonIgnore
    public void setExSerial(String exSerial) {
        ExSerial = exSerial;
    }

    @JsonIgnore
    public String getSysDate() {
        return SysDate;
    }

    @JsonIgnore
    public void setSysDate(String sysDate) {
        SysDate = sysDate;
    }

    @JsonIgnore
    public String getSysTime() {
        return SysTime;
    }

    @JsonIgnore
    public void setSysTime(String sysTime) {
        SysTime = sysTime;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @JsonIgnore
    public String getTotNum() {
        return TotNum;
    }

    @JsonIgnore
    public void setTotNum(String totNum) {
        TotNum = totNum;
    }

    @JsonIgnore
    public String getRetNum() {
        return RetNum;
    }

    @JsonIgnore
    public void setRetNum(String retNum) {
        RetNum = retNum;
    }

    @JsonIgnore
    public String getOffSet() {
        return OffSet;
    }

    @JsonIgnore
    public void setOffSet(String offSet) {
        OffSet = offSet;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "FunctionId='" + FunctionId + '\'' +
                ", ExSerial='" + ExSerial + '\'' +
                ", SysDate='" + SysDate + '\'' +
                ", SysTime='" + SysTime + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", TotNum='" + TotNum + '\'' +
                ", RetNum='" + RetNum + '\'' +
                ", OffSet='" + OffSet + '\'' +
                ", list=" + list +
                '}';
    }
}
