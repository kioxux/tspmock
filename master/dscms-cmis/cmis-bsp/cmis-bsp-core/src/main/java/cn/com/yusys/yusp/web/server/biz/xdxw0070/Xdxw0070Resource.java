package cn.com.yusys.yusp.web.server.biz.xdxw0070;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0070.req.Xdxw0070ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0070.resp.Xdxw0070RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0070.req.Xdxw0070DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0070.resp.Xdxw0070DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:授信侧面调查信息查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0070:授信侧面调查信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0070Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0070Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0070
     * 交易描述：授信侧面调查信息查询
     *
     * @param xdxw0070ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("授信侧面调查信息查询")
    @PostMapping("/xdxw0070")
    //@Idempotent({"xdcaxw0070", "#xdxw0070ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0070RespDto xdxw0070(@Validated @RequestBody Xdxw0070ReqDto xdxw0070ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, JSON.toJSONString(xdxw0070ReqDto));
        Xdxw0070DataReqDto xdxw0070DataReqDto = new Xdxw0070DataReqDto();// 请求Data： 授信侧面调查信息查询
        Xdxw0070DataRespDto xdxw0070DataRespDto = new Xdxw0070DataRespDto();// 响应Data：授信侧面调查信息查询
		Xdxw0070RespDto xdxw0070RespDto = new Xdxw0070RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0070.req.Data reqData = null; // 请求Data：授信侧面调查信息查询
		cn.com.yusys.yusp.dto.server.biz.xdxw0070.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0070.resp.Data();// 响应Data：授信侧面调查信息查询
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0070ReqDto获取 reqData
            reqData = xdxw0070ReqDto.getData();
            // 将 reqData 拷贝到xdxw0070DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0070DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, JSON.toJSONString(xdxw0070DataReqDto));
            ResultDto<Xdxw0070DataRespDto> xdxw0070DataResultDto = dscmsBizXwClientService.xdxw0070(xdxw0070DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, JSON.toJSONString(xdxw0070DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0070RespDto.setErorcd(Optional.ofNullable(xdxw0070DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0070RespDto.setErortx(Optional.ofNullable(xdxw0070DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0070DataResultDto.getCode())) {
                xdxw0070DataRespDto = xdxw0070DataResultDto.getData();
                xdxw0070RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0070RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0070DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0070DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, e.getMessage());
            xdxw0070RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0070RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0070RespDto.setDatasq(servsq);

        xdxw0070RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0070.key, DscmsEnum.TRADE_CODE_XDXW0070.value, JSON.toJSONString(xdxw0070RespDto));
        return xdxw0070RespDto;
    }
}
