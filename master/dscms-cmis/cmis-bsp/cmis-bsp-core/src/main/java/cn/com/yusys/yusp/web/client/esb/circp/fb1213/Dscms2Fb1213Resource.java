package cn.com.yusys.yusp.web.client.esb.circp.fb1213;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1213.req.Fb1213ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1213.req.GUAR_LIST;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1213.resp.Fb1213RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.Fb1213ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1213.resp.Fb1213RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1213）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1213Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1213Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1213
     * 交易描述：押品出库通知
     *
     * @param fb1213ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1213")
    protected @ResponseBody
    ResultDto<Fb1213RespDto> fb1213(@Validated @RequestBody Fb1213ReqDto fb1213ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1213.key, EsbEnum.TRADE_CODE_FB1213.value, JSON.toJSONString(fb1213ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1213.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1213.resp.Service();
        Fb1213ReqService fb1213ReqService = new Fb1213ReqService();
        Fb1213RespService fb1213RespService = new Fb1213RespService();
        Fb1213RespDto fb1213RespDto = new Fb1213RespDto();
        ResultDto<Fb1213RespDto> fb1213ResultDto = new ResultDto<Fb1213RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1213ReqDto转换成reqService
            BeanUtils.copyProperties(fb1213ReqDto, reqService);
            cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.GUAR_LIST glist = new cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.GUAR_LIST();
            if (CollectionUtils.nonEmpty(fb1213ReqDto.getGUAR_LIST())) {
                List<GUAR_LIST> guar_list = fb1213ReqDto.getGUAR_LIST();
                java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.Record> targetList = new ArrayList<>();
                for (GUAR_LIST g : guar_list) {
                    cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.Record target = new cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.Record();
                    BeanUtils.copyProperties(g, target);
                    targetList.add(target);
                }
                glist.setRecord(targetList);
                reqService.setGUAR_LIST(glist);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1213.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1213ReqService.setService(reqService);
            // 将fb1213ReqService转换成fb1213ReqServiceMap
            Map fb1213ReqServiceMap = beanMapUtil.beanToMap(fb1213ReqService);
            context.put("tradeDataMap", fb1213ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1213.key, EsbEnum.TRADE_CODE_FB1213.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1213.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1213.key, EsbEnum.TRADE_CODE_FB1213.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1213RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1213RespService.class, Fb1213RespService.class);
            respService = fb1213RespService.getService();

            fb1213ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1213ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1213RespDto
                BeanUtils.copyProperties(respService, fb1213RespDto);

                fb1213ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1213ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1213ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1213ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1213.key, EsbEnum.TRADE_CODE_FB1213.value, e.getMessage());
            fb1213ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1213ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1213ResultDto.setData(fb1213RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1213.key, EsbEnum.TRADE_CODE_FB1213.value, JSON.toJSONString(fb1213ResultDto));
        return fb1213ResultDto;
    }
}
