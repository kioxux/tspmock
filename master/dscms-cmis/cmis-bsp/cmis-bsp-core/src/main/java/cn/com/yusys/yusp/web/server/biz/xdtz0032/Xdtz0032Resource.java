package cn.com.yusys.yusp.web.server.biz.xdtz0032;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0032.req.Xdtz0032ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0032.resp.Xdtz0032RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0032.req.Xdtz0032DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0032.resp.Xdtz0032DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:询客户所担保的行内当前贷款逾期件数
 *
 * @author zhangpeng
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDTZ0032:询客户所担保的行内当前贷款逾期件数")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0032Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0032Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0032
     * 交易描述：询客户所担保的行内当前贷款逾期件数
     *
     * @param xdtz0032ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("询客户所担保的行内当前贷款逾期件数")
    @PostMapping("/xdtz0032")
    //@Idempotent({"xdcatz0032", "#xdtz0032ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0032RespDto xdtz0032(@Validated @RequestBody Xdtz0032ReqDto xdtz0032ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032ReqDto));
        Xdtz0032DataReqDto xdtz0032DataReqDto = new Xdtz0032DataReqDto();// 请求Data： 询客户所担保的行内当前贷款逾期件数
        Xdtz0032DataRespDto xdtz0032DataRespDto = new Xdtz0032DataRespDto();// 响应Data：询客户所担保的行内当前贷款逾期件数
        Xdtz0032RespDto xdtz0032RespDto = new Xdtz0032RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0032.req.Data reqData = null; // 请求Data：询客户所担保的行内当前贷款逾期件数
        cn.com.yusys.yusp.dto.server.biz.xdtz0032.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0032.resp.Data();// 响应Data：询客户所担保的行内当前贷款逾期件数
        try {
            // 从 xdtz0032ReqDto获取 reqData
            reqData = xdtz0032ReqDto.getData();
            // 将 reqData 拷贝到xdtz0032DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0032DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032ReqDto));
            ResultDto<Xdtz0032DataRespDto> xdtz0032DataResultDto = dscmsBizTzClientService.xdtz0032(xdtz0032DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0032RespDto.setErorcd(Optional.ofNullable(xdtz0032DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0032RespDto.setErortx(Optional.ofNullable(xdtz0032DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0032DataResultDto.getCode())) {
                xdtz0032RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0032RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdtz0032DataRespDto = xdtz0032DataResultDto.getData();
                // 将 xdtz0032DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0032DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, e.getMessage());
            xdtz0032RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0032RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0032RespDto.setDatasq(servsq);

        xdtz0032RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0032.key, DscmsEnum.TRADE_CODE_XDTZ0032.value, JSON.toJSONString(xdtz0032RespDto));
        return xdtz0032RespDto;
    }
}
