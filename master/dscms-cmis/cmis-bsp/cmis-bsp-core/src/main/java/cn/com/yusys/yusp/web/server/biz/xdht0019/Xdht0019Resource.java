package cn.com.yusys.yusp.web.server.biz.xdht0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0019.req.Xdht0019ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0019.resp.Xdht0019RespDto;
import cn.com.yusys.yusp.dto.server.xdht0019.req.Xdht0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0019.resp.Xdht0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同生成
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0019:合同生成")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0019Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0019
     * 交易描述：合同生成
     *
     * @param xdht0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同生成")
    @PostMapping("/xdht0019")
    //@Idempotent({"xdcaht0019", "#xdht0019ReqDto.datasq"})
    protected @ResponseBody
    Xdht0019RespDto xdht0019(@Validated @RequestBody Xdht0019ReqDto xdht0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019ReqDto));
        Xdht0019DataReqDto xdht0019DataReqDto = new Xdht0019DataReqDto();// 请求Data： 合同生成
        Xdht0019DataRespDto xdht0019DataRespDto = new Xdht0019DataRespDto();// 响应Data：合同生成
        Xdht0019RespDto xdht0019RespDto = new Xdht0019RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0019.req.Data reqData = null; // 请求Data：合同生成
        cn.com.yusys.yusp.dto.server.biz.xdht0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0019.resp.Data();// 响应Data：合同生成

        try {
            // 从 xdht0019ReqDto获取 reqData
            reqData = xdht0019ReqDto.getData();
            // 将 reqData 拷贝到xdht0019DataReqDto
            BeanUtils.copyProperties(reqData, xdht0019DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019DataReqDto));
            ResultDto<Xdht0019DataRespDto> xdht0019DataResultDto = dscmsBizHtClientService.xdht0019(xdht0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0019RespDto.setErorcd(Optional.ofNullable(xdht0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0019RespDto.setErortx(Optional.ofNullable(xdht0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0019DataResultDto.getCode())) {
                xdht0019DataRespDto = xdht0019DataResultDto.getData();
                xdht0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, e.getMessage());
            xdht0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0019RespDto.setDatasq(servsq);

        xdht0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0019.key, DscmsEnum.TRADE_CODE_XDHT0019.value, JSON.toJSONString(xdht0019RespDto));
        return xdht0019RespDto;
    }
}
