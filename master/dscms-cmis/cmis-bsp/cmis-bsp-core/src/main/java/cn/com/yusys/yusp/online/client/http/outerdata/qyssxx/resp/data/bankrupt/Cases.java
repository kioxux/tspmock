package cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.bankrupt;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Service：涉诉信息查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cases implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "n_ajlx")
    private String n_ajlx;//案件类型
    @JsonProperty(value = "c_ah")
    private String c_ah;//案号
    @JsonProperty(value = "c_ah_ys")
    private String c_ah_ys;//原审案号
    @JsonProperty(value = "c_ah_hx")
    private String c_ah_hx;//后续案号
    @JsonProperty(value = "n_ajbs")
    private String n_ajbs;//案件标识
    @JsonProperty(value = "n_jbfy")
    private String n_jbfy;//经办法院
    @JsonProperty(value = "n_jbfy_cj")
    private String n_jbfy_cj;//法院所属层级 *
    @JsonProperty(value = "n_ajjzjd")
    private String n_ajjzjd;//案件进展阶段 *
    @JsonProperty(value = "n_slcx")
    private String n_slcx;//审理程序 *
    @JsonProperty(value = "c_ssdy")
    private String c_ssdy;//所属地域
    @JsonProperty(value = "d_larq")
    private String d_larq;//立案时间
    @JsonProperty(value = "n_laay")
    private String n_laay;//立案案由 *
    @JsonProperty(value = "n_qsbdje")
    private BigDecimal n_qsbdje;//立案标的金额 *
    @JsonProperty(value = "n_qsbdje_gj")
    private BigDecimal n_qsbdje_gj;//立案标的金额估计 *
    @JsonProperty(value = "d_jarq")
    private String d_jarq;//结案时间
    @JsonProperty(value = "n_jaay")
    private String n_jaay;//结案案由
    @JsonProperty(value = "n_jabdje")
    private BigDecimal n_jabdje;//结案标的金额 *
    @JsonProperty(value = "n_jabdje_gj")
    private BigDecimal n_jabdje_gj;//结案标的金额估计 *
    @JsonProperty(value = "n_jafs")
    private String n_jafs;//结案方式 *
    @JsonProperty(value = "n_ssdw")
    private String n_ssdw;//诉讼地位 *
    @JsonProperty(value = "n_ssdw_ys")
    private String n_ssdw_ys;//一审诉讼地位 *
    @JsonProperty(value = "c_gkws_glah")
    private String c_gkws_glah;//相关案件号
    @JsonProperty(value = "c_gkws_dsr")
    private String c_gkws_dsr;//当事人
    @JsonProperty(value = "c_gkws_pjjg")
    private String c_gkws_pjjg;//判决结果
    @JsonProperty(value = "n_fzje")
    private String n_fzje;//犯罪金额
    @JsonProperty(value = "n_bqqpcje")
    private String n_bqqpcje;//被请求赔偿金额
    @JsonProperty(value = "n_ccxzxje")
    private String n_ccxzxje;//财产刑执行金额
    @JsonProperty(value = "n_ccxzxje_gj")
    private String n_ccxzxje_gj;//财产刑执行金额估计
    @JsonProperty(value = "n_pcpcje")
    private String n_pcpcje;//判处赔偿金额
    @JsonProperty(value = "n_pcpcje_gj")
    private String n_pcpcje_gj;//判处赔偿金额估计
    /* 接口文档中没有定义但是返回报文存在的字段   开始 */
    @JsonProperty(value = "n_laay_tree")
    private String n_laay_tree;
    @JsonProperty(value = "n_jaay_tree")
    private String n_jaay_tree;
    @JsonProperty(value = "n_pj_victory")
    private String n_pj_victory;
    @JsonProperty(value = "c_gkws_id")
    private String c_gkws_id;
    @JsonProperty(value = "n_jabdje_level")
    private Integer n_jabdje_level;
    @JsonProperty(value = "c_slfsxx")
    private String c_slfsxx;
    @JsonProperty(value = "n_qsbdje_level")
    private Integer n_qsbdje_level;
    @JsonProperty(value = "n_laay_tag")
    private String n_laay_tag;
    /* 接口文档中没有定义但是返回报文存在的字段   结束 */

    public String getN_ajlx() {
        return n_ajlx;
    }

    public void setN_ajlx(String n_ajlx) {
        this.n_ajlx = n_ajlx;
    }

    public String getC_ah() {
        return c_ah;
    }

    public void setC_ah(String c_ah) {
        this.c_ah = c_ah;
    }

    public String getC_ah_ys() {
        return c_ah_ys;
    }

    public void setC_ah_ys(String c_ah_ys) {
        this.c_ah_ys = c_ah_ys;
    }

    public String getC_ah_hx() {
        return c_ah_hx;
    }

    public void setC_ah_hx(String c_ah_hx) {
        this.c_ah_hx = c_ah_hx;
    }

    public String getN_ajbs() {
        return n_ajbs;
    }

    public void setN_ajbs(String n_ajbs) {
        this.n_ajbs = n_ajbs;
    }

    public String getN_jbfy() {
        return n_jbfy;
    }

    public void setN_jbfy(String n_jbfy) {
        this.n_jbfy = n_jbfy;
    }

    public String getN_jbfy_cj() {
        return n_jbfy_cj;
    }

    public void setN_jbfy_cj(String n_jbfy_cj) {
        this.n_jbfy_cj = n_jbfy_cj;
    }

    public String getN_ajjzjd() {
        return n_ajjzjd;
    }

    public void setN_ajjzjd(String n_ajjzjd) {
        this.n_ajjzjd = n_ajjzjd;
    }

    public String getN_slcx() {
        return n_slcx;
    }

    public void setN_slcx(String n_slcx) {
        this.n_slcx = n_slcx;
    }

    public String getC_ssdy() {
        return c_ssdy;
    }

    public void setC_ssdy(String c_ssdy) {
        this.c_ssdy = c_ssdy;
    }

    public String getD_larq() {
        return d_larq;
    }

    public void setD_larq(String d_larq) {
        this.d_larq = d_larq;
    }

    public String getN_laay() {
        return n_laay;
    }

    public void setN_laay(String n_laay) {
        this.n_laay = n_laay;
    }

    public BigDecimal getN_qsbdje() {
        return n_qsbdje;
    }

    public void setN_qsbdje(BigDecimal n_qsbdje) {
        this.n_qsbdje = n_qsbdje;
    }

    public BigDecimal getN_qsbdje_gj() {
        return n_qsbdje_gj;
    }

    public void setN_qsbdje_gj(BigDecimal n_qsbdje_gj) {
        this.n_qsbdje_gj = n_qsbdje_gj;
    }

    public String getD_jarq() {
        return d_jarq;
    }

    public void setD_jarq(String d_jarq) {
        this.d_jarq = d_jarq;
    }

    public String getN_jaay() {
        return n_jaay;
    }

    public void setN_jaay(String n_jaay) {
        this.n_jaay = n_jaay;
    }

    public BigDecimal getN_jabdje() {
        return n_jabdje;
    }

    public void setN_jabdje(BigDecimal n_jabdje) {
        this.n_jabdje = n_jabdje;
    }

    public BigDecimal getN_jabdje_gj() {
        return n_jabdje_gj;
    }

    public void setN_jabdje_gj(BigDecimal n_jabdje_gj) {
        this.n_jabdje_gj = n_jabdje_gj;
    }

    public String getN_jafs() {
        return n_jafs;
    }

    public void setN_jafs(String n_jafs) {
        this.n_jafs = n_jafs;
    }

    public String getN_ssdw() {
        return n_ssdw;
    }

    public void setN_ssdw(String n_ssdw) {
        this.n_ssdw = n_ssdw;
    }

    public String getN_ssdw_ys() {
        return n_ssdw_ys;
    }

    public void setN_ssdw_ys(String n_ssdw_ys) {
        this.n_ssdw_ys = n_ssdw_ys;
    }

    public String getC_gkws_glah() {
        return c_gkws_glah;
    }

    public void setC_gkws_glah(String c_gkws_glah) {
        this.c_gkws_glah = c_gkws_glah;
    }

    public String getC_gkws_dsr() {
        return c_gkws_dsr;
    }

    public void setC_gkws_dsr(String c_gkws_dsr) {
        this.c_gkws_dsr = c_gkws_dsr;
    }

    public String getC_gkws_pjjg() {
        return c_gkws_pjjg;
    }

    public void setC_gkws_pjjg(String c_gkws_pjjg) {
        this.c_gkws_pjjg = c_gkws_pjjg;
    }

    public String getN_fzje() {
        return n_fzje;
    }

    public void setN_fzje(String n_fzje) {
        this.n_fzje = n_fzje;
    }

    public String getN_bqqpcje() {
        return n_bqqpcje;
    }

    public void setN_bqqpcje(String n_bqqpcje) {
        this.n_bqqpcje = n_bqqpcje;
    }

    public String getN_ccxzxje() {
        return n_ccxzxje;
    }

    public void setN_ccxzxje(String n_ccxzxje) {
        this.n_ccxzxje = n_ccxzxje;
    }

    public String getN_ccxzxje_gj() {
        return n_ccxzxje_gj;
    }

    public void setN_ccxzxje_gj(String n_ccxzxje_gj) {
        this.n_ccxzxje_gj = n_ccxzxje_gj;
    }

    public String getN_pcpcje() {
        return n_pcpcje;
    }

    public void setN_pcpcje(String n_pcpcje) {
        this.n_pcpcje = n_pcpcje;
    }

    public String getN_pcpcje_gj() {
        return n_pcpcje_gj;
    }

    public void setN_pcpcje_gj(String n_pcpcje_gj) {
        this.n_pcpcje_gj = n_pcpcje_gj;
    }

    public String getN_laay_tree() {
        return n_laay_tree;
    }

    public void setN_laay_tree(String n_laay_tree) {
        this.n_laay_tree = n_laay_tree;
    }

    public String getN_jaay_tree() {
        return n_jaay_tree;
    }

    public void setN_jaay_tree(String n_jaay_tree) {
        this.n_jaay_tree = n_jaay_tree;
    }

    public String getN_pj_victory() {
        return n_pj_victory;
    }

    public void setN_pj_victory(String n_pj_victory) {
        this.n_pj_victory = n_pj_victory;
    }

    public String getC_gkws_id() {
        return c_gkws_id;
    }

    public void setC_gkws_id(String c_gkws_id) {
        this.c_gkws_id = c_gkws_id;
    }

    public Integer getN_jabdje_level() {
        return n_jabdje_level;
    }

    public void setN_jabdje_level(Integer n_jabdje_level) {
        this.n_jabdje_level = n_jabdje_level;
    }

    public String getC_slfsxx() {
        return c_slfsxx;
    }

    public void setC_slfsxx(String c_slfsxx) {
        this.c_slfsxx = c_slfsxx;
    }

    public Integer getN_qsbdje_level() {
        return n_qsbdje_level;
    }

    public void setN_qsbdje_level(Integer n_qsbdje_level) {
        this.n_qsbdje_level = n_qsbdje_level;
    }

    public String getN_laay_tag() {
        return n_laay_tag;
    }

    public void setN_laay_tag(String n_laay_tag) {
        this.n_laay_tag = n_laay_tag;
    }

    @Override
    public String toString() {
        return "Cases{" +
                "n_ajlx='" + n_ajlx + '\'' +
                ", c_ah='" + c_ah + '\'' +
                ", c_ah_ys='" + c_ah_ys + '\'' +
                ", c_ah_hx='" + c_ah_hx + '\'' +
                ", n_ajbs='" + n_ajbs + '\'' +
                ", n_jbfy='" + n_jbfy + '\'' +
                ", n_jbfy_cj='" + n_jbfy_cj + '\'' +
                ", n_ajjzjd='" + n_ajjzjd + '\'' +
                ", n_slcx='" + n_slcx + '\'' +
                ", c_ssdy='" + c_ssdy + '\'' +
                ", d_larq='" + d_larq + '\'' +
                ", n_laay='" + n_laay + '\'' +
                ", n_qsbdje=" + n_qsbdje +
                ", n_qsbdje_gj=" + n_qsbdje_gj +
                ", d_jarq='" + d_jarq + '\'' +
                ", n_jaay='" + n_jaay + '\'' +
                ", n_jabdje=" + n_jabdje +
                ", n_jabdje_gj=" + n_jabdje_gj +
                ", n_jafs='" + n_jafs + '\'' +
                ", n_ssdw='" + n_ssdw + '\'' +
                ", n_ssdw_ys='" + n_ssdw_ys + '\'' +
                ", c_gkws_glah='" + c_gkws_glah + '\'' +
                ", c_gkws_dsr='" + c_gkws_dsr + '\'' +
                ", c_gkws_pjjg='" + c_gkws_pjjg + '\'' +
                ", n_fzje='" + n_fzje + '\'' +
                ", n_bqqpcje='" + n_bqqpcje + '\'' +
                ", n_ccxzxje='" + n_ccxzxje + '\'' +
                ", n_ccxzxje_gj='" + n_ccxzxje_gj + '\'' +
                ", n_pcpcje='" + n_pcpcje + '\'' +
                ", n_pcpcje_gj='" + n_pcpcje_gj + '\'' +
                ", n_laay_tree='" + n_laay_tree + '\'' +
                ", n_jaay_tree='" + n_jaay_tree + '\'' +
                ", n_pj_victory='" + n_pj_victory + '\'' +
                ", c_gkws_id='" + c_gkws_id + '\'' +
                ", n_jabdje_level=" + n_jabdje_level +
                ", c_slfsxx='" + c_slfsxx + '\'' +
                ", n_qsbdje_level=" + n_qsbdje_level +
                ", n_laay_tag='" + n_laay_tag + '\'' +
                '}';
    }
}
