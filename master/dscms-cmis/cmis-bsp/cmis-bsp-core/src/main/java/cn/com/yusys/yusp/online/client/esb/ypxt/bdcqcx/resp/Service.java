package cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.resp;

import java.math.BigDecimal;

/**
 * 响应Service：不动产权证查询接口
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:43
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否
    private String guarno;// 押品编号 否
    private String bdcnoo;// 不动产权证号 否
    private String bdqxdm;// 所属区县-代码 否
    private String bdqxmc;// 所属区县-名称 否
    private String bdqzll;// 坐落 否
    private String bdcdyh;// 房屋-不动产单元号 否
    private String bdcqzs;// 房屋-产权证书号 否
    private String bdcqjc;// 房屋-产权证书号简称 否
    private BigDecimal bdcqmj;// 房屋-产权面积 否
    private String bdytdm;// 房屋-用途-代码 否
    private String bdytmc;// 房屋-用途-名称 否
    private String tdzhno;// 土地-证号 否
    private BigDecimal tdaera;// 土地-面积 否
    private String tdytdm;// 土地-用途-代码 否
    private String tdytmc;// 土地-用途-名称 否

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    public String getBdcnoo() {
        return bdcnoo;
    }

    public void setBdcnoo(String bdcnoo) {
        this.bdcnoo = bdcnoo;
    }

    public String getBdqxdm() {
        return bdqxdm;
    }

    public void setBdqxdm(String bdqxdm) {
        this.bdqxdm = bdqxdm;
    }

    public String getBdqxmc() {
        return bdqxmc;
    }

    public void setBdqxmc(String bdqxmc) {
        this.bdqxmc = bdqxmc;
    }

    public String getBdqzll() {
        return bdqzll;
    }

    public void setBdqzll(String bdqzll) {
        this.bdqzll = bdqzll;
    }

    public String getBdcdyh() {
        return bdcdyh;
    }

    public void setBdcdyh(String bdcdyh) {
        this.bdcdyh = bdcdyh;
    }

    public String getBdcqzs() {
        return bdcqzs;
    }

    public void setBdcqzs(String bdcqzs) {
        this.bdcqzs = bdcqzs;
    }

    public String getBdcqjc() {
        return bdcqjc;
    }

    public void setBdcqjc(String bdcqjc) {
        this.bdcqjc = bdcqjc;
    }

    public BigDecimal getBdcqmj() {
        return bdcqmj;
    }

    public void setBdcqmj(BigDecimal bdcqmj) {
        this.bdcqmj = bdcqmj;
    }

    public String getBdytdm() {
        return bdytdm;
    }

    public void setBdytdm(String bdytdm) {
        this.bdytdm = bdytdm;
    }

    public String getBdytmc() {
        return bdytmc;
    }

    public void setBdytmc(String bdytmc) {
        this.bdytmc = bdytmc;
    }

    public String getTdzhno() {
        return tdzhno;
    }

    public void setTdzhno(String tdzhno) {
        this.tdzhno = tdzhno;
    }

    public BigDecimal getTdaera() {
        return tdaera;
    }

    public void setTdaera(BigDecimal tdaera) {
        this.tdaera = tdaera;
    }

    public String getTdytdm() {
        return tdytdm;
    }

    public void setTdytdm(String tdytdm) {
        this.tdytdm = tdytdm;
    }

    public String getTdytmc() {
        return tdytmc;
    }

    public void setTdytmc(String tdytmc) {
        this.tdytmc = tdytmc;
    }

    @Override
    public String toString() {
        return "BdcqcxRespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", guarno='" + guarno + '\'' +
                ", bdcnoo='" + bdcnoo + '\'' +
                ", bdqxdm='" + bdqxdm + '\'' +
                ", bdqxmc='" + bdqxmc + '\'' +
                ", bdqzll='" + bdqzll + '\'' +
                ", bdcdyh='" + bdcdyh + '\'' +
                ", bdcqzs='" + bdcqzs + '\'' +
                ", bdcqjc='" + bdcqjc + '\'' +
                ", bdcqmj=" + bdcqmj +
                ", bdytdm='" + bdytdm + '\'' +
                ", bdytmc='" + bdytmc + '\'' +
                ", tdzhno='" + tdzhno + '\'' +
                ", tdaera=" + tdaera +
                ", tdytdm='" + tdytdm + '\'' +
                ", tdytmc='" + tdytmc + '\'' +
                '}';
    }
}
