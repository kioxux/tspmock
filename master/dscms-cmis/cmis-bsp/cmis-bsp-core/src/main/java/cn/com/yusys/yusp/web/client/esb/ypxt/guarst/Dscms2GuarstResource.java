package cn.com.yusys.yusp.web.client.esb.ypxt.guarst;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.req.GuarstReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.resp.GuarstRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.guarst.req.GuarstReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.guarst.resp.GuarstRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品状态变更推送
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用押品系统的接口(guarst)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2GuarstResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2GuarstResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：guarst
     * 交易描述：押品状态变更推送
     *
     * @param guarstReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("guarst:押品状态变更推送")
    @PostMapping("/guarst")
    protected @ResponseBody
    ResultDto<GuarstRespDto> guarst(@Validated @RequestBody GuarstReqDto guarstReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GUARST.key, EsbEnum.TRADE_CODE_GUARST.value, JSON.toJSONString(guarstReqDto));
		cn.com.yusys.yusp.online.client.esb.ypxt.guarst.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.guarst.req.Service();
		cn.com.yusys.yusp.online.client.esb.ypxt.guarst.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.guarst.resp.Service();
        GuarstReqService guarstReqService = new GuarstReqService();
        GuarstRespService guarstRespService = new GuarstRespService();
        GuarstRespDto guarstRespDto = new GuarstRespDto();
        ResultDto<GuarstRespDto> guarstResultDto = new ResultDto<GuarstRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将guarstReqDto转换成reqService
			BeanUtils.copyProperties(guarstReqDto, reqService);

			reqService.setPrcscd(EsbEnum.TRADE_CODE_GUARST.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号

			guarstReqService.setService(reqService);
			// 将guarstReqService转换成guarstReqServiceMap
			Map guarstReqServiceMap = beanMapUtil.beanToMap(guarstReqService);
			context.put("tradeDataMap", guarstReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GUARST.key, EsbEnum.TRADE_CODE_GUARST.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_GUARST.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GUARST.key, EsbEnum.TRADE_CODE_GUARST.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			guarstRespService = beanMapUtil.mapToBean(tradeDataMap, GuarstRespService.class, GuarstRespService.class);
			respService = guarstRespService.getService();
			//  将respService转换成GuarstRespDto

			guarstResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			guarstResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				BeanUtils.copyProperties(respService, guarstRespDto);
				guarstResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				guarstResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				guarstResultDto.setCode(EpbEnum.EPB099999.key);
				guarstResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GUARST.key, EsbEnum.TRADE_CODE_GUARST.value, e.getMessage());
			guarstResultDto.setCode(EpbEnum.EPB099999.key);//9999
			guarstResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		guarstResultDto.setData(guarstRespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GUARST.key, EsbEnum.TRADE_CODE_GUARST.value, JSON.toJSONString(guarstResultDto));
        return guarstResultDto;
    }
}
