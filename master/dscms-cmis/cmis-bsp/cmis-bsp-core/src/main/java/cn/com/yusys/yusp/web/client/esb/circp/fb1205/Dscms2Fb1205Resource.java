package cn.com.yusys.yusp.web.client.esb.circp.fb1205;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1205.req.Fb1205ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1205.resp.Fb1205RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1205.req.Fb1205ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1205.resp.Fb1205RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1205）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1205Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1205Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1205
     * 交易描述：放款信息推送
     *
     * @param fb1205ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1205")
    protected @ResponseBody
    ResultDto<Fb1205RespDto> fb1205(@Validated @RequestBody Fb1205ReqDto fb1205ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1205.key, EsbEnum.TRADE_CODE_FB1205.value, JSON.toJSONString(fb1205ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1205.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1205.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1205.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1205.resp.Service();
        Fb1205ReqService fb1205ReqService = new Fb1205ReqService();
        Fb1205RespService fb1205RespService = new Fb1205RespService();
        Fb1205RespDto fb1205RespDto = new Fb1205RespDto();
        ResultDto<Fb1205RespDto> fb1205ResultDto = new ResultDto<Fb1205RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1205ReqDto转换成reqService
            BeanUtils.copyProperties(fb1205ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1205.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1205ReqService.setService(reqService);
            // 将fb1205ReqService转换成fb1205ReqServiceMap
            Map fb1205ReqServiceMap = beanMapUtil.beanToMap(fb1205ReqService);
            context.put("tradeDataMap", fb1205ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1205.key, EsbEnum.TRADE_CODE_FB1205.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1205.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1205.key, EsbEnum.TRADE_CODE_FB1205.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1205RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1205RespService.class, Fb1205RespService.class);
            respService = fb1205RespService.getService();

            fb1205ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1205ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1205RespDto
                BeanUtils.copyProperties(respService, fb1205RespDto);

                fb1205ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1205ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1205ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1205ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1205.key, EsbEnum.TRADE_CODE_FB1205.value, e.getMessage());
            fb1205ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1205ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1205ResultDto.setData(fb1205RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1205.key, EsbEnum.TRADE_CODE_FB1205.value, JSON.toJSONString(fb1205ResultDto));
        return fb1205ResultDto;
    }
}
