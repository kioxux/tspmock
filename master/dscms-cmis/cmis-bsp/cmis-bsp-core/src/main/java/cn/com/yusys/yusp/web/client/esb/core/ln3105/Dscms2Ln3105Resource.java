package cn.com.yusys.yusp.web.client.esb.core.ln3105;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3105.Ln3105ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3105.Ln3105RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3105.Lstqgmx;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3105.req.Ln3105ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3105.resp.Ln3105RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3105)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3105Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3105Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3105
     * 交易描述：贷款期供交易明细查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3105:贷款期供交易明细查询")
    @PostMapping("/ln3105")
    protected @ResponseBody
    ResultDto<Ln3105RespDto> ln3105(@Validated @RequestBody Ln3105ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3105.key, EsbEnum.TRADE_CODE_LN3105.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3105.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3105.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3105.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3105.resp.Service();
        Ln3105ReqService ln3105ReqService = new Ln3105ReqService();
        Ln3105RespService ln3105RespService = new Ln3105RespService();
        Ln3105RespDto ln3105RespDto = new Ln3105RespDto();
        ResultDto<Ln3105RespDto> ln3105ResultDto = new ResultDto<Ln3105RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3105ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3105.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3105ReqService.setService(reqService);
            // 将ln3105ReqService转换成ln3105ReqServiceMap
            Map ln3105ReqServiceMap = beanMapUtil.beanToMap(ln3105ReqService);
            context.put("tradeDataMap", ln3105ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3105.key, EsbEnum.TRADE_CODE_LN3105.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3105.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3105.key, EsbEnum.TRADE_CODE_LN3105.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3105RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3105RespService.class, Ln3105RespService.class);
            respService = ln3105RespService.getService();

            ln3105ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3105ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3105RespDto
                BeanUtils.copyProperties(respService, ln3105RespDto);
                List<Lstqgmx> lstqgmxs = new ArrayList<Lstqgmx>();
                if (respService.getLstqgmx_ARRAY() != null) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3105.resp.Record> recordList = respService.getLstqgmx_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3105.resp.Record record : recordList) {
                        Lstqgmx lstqgmx = new Lstqgmx();
                        BeanUtils.copyProperties(record, lstqgmx);
                        lstqgmxs.add(lstqgmx);
                    }
                }
                ln3105RespDto.setLstqgmx(lstqgmxs);
                ln3105ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3105ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3105ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3105ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3105.key, EsbEnum.TRADE_CODE_LN3105.value, e.getMessage());
            ln3105ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3105ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3105ResultDto.setData(ln3105RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3105.key, EsbEnum.TRADE_CODE_LN3105.value, JSON.toJSONString(ln3105ResultDto));
        return ln3105ResultDto;
    }
}
