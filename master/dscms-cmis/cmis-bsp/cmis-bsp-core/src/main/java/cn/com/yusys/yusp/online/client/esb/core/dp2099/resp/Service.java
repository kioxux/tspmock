package cn.com.yusys.yusp.online.client.esb.core.dp2099.resp;

/**
 * 响应Service：保证金账户查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String kehuhaoo;//客户号
    private String kehuzhmc;//客户账户名称
    private String lstacctinfo;//查询结果列表展示
    private String chaxfanw;//查询范围
    private Integer zongbshu;//总笔数
    private String bblujing;//报表路径
    private String erorcd;//错误码
    private String erortx;//错误描述
    private List list;

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getLstacctinfo() {
        return lstacctinfo;
    }

    public void setLstacctinfo(String lstacctinfo) {
        this.lstacctinfo = lstacctinfo;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    public String getBblujing() {
        return bblujing;
    }

    public void setBblujing(String bblujing) {
        this.bblujing = bblujing;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", lstacctinfo='" + lstacctinfo + '\'' +
                ", chaxfanw='" + chaxfanw + '\'' +
                ", zongbshu=" + zongbshu +
                ", bblujing='" + bblujing + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", list=" + list +
                '}';
    }
}
