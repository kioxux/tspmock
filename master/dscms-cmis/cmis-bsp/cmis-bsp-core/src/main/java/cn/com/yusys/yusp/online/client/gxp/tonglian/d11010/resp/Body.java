package cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/16 20:23
 * @since 2021/6/16 20:23
 */
public class Body {
    private String idtftp;//证件类型
    private String idtfno;//证件号码
    private String cardno;//卡号
    private String opt;//操作行动码
    private String title;//职务
    private String custnm;//姓名
    private String gender;//性别
    private String ocpatn;//职业
    private String bkmrno;//本行员工号
    private String ntnaty;//国籍
    private String mtstat;//婚姻状况
    private String qlftin;//教育状况
    private String hmphon;//家庭电话
    private String mobile;//移动电话
    private String email;//电子邮箱
    private String corpnm;//公司名称
    private String embnam;//凸印姓名
    private String iedate;//证件到期日
    private String idaddr;//发证机关所在地址
    private String brtday;//生日
    private String hourst;//房屋持有类型
    private String hourpe;//住宅类型
    private String homrom;//现住址居住起始年月
    private String emptus;//就业状态
    private String empity;//工作稳定性
    private String titlal;//职称
    private String income;//年收入
    private String isdate;//证件起始日

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOcpatn() {
        return ocpatn;
    }

    public void setOcpatn(String ocpatn) {
        this.ocpatn = ocpatn;
    }

    public String getBkmrno() {
        return bkmrno;
    }

    public void setBkmrno(String bkmrno) {
        this.bkmrno = bkmrno;
    }

    public String getNtnaty() {
        return ntnaty;
    }

    public void setNtnaty(String ntnaty) {
        this.ntnaty = ntnaty;
    }

    public String getMtstat() {
        return mtstat;
    }

    public void setMtstat(String mtstat) {
        this.mtstat = mtstat;
    }

    public String getQlftin() {
        return qlftin;
    }

    public void setQlftin(String qlftin) {
        this.qlftin = qlftin;
    }

    public String getHmphon() {
        return hmphon;
    }

    public void setHmphon(String hmphon) {
        this.hmphon = hmphon;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCorpnm() {
        return corpnm;
    }

    public void setCorpnm(String corpnm) {
        this.corpnm = corpnm;
    }

    public String getEmbnam() {
        return embnam;
    }

    public void setEmbnam(String embnam) {
        this.embnam = embnam;
    }

    public String getIedate() {
        return iedate;
    }

    public void setIedate(String iedate) {
        this.iedate = iedate;
    }

    public String getIdaddr() {
        return idaddr;
    }

    public void setIdaddr(String idaddr) {
        this.idaddr = idaddr;
    }

    public String getBrtday() {
        return brtday;
    }

    public void setBrtday(String brtday) {
        this.brtday = brtday;
    }

    public String getHourst() {
        return hourst;
    }

    public void setHourst(String hourst) {
        this.hourst = hourst;
    }

    public String getHourpe() {
        return hourpe;
    }

    public void setHourpe(String hourpe) {
        this.hourpe = hourpe;
    }

    public String getHomrom() {
        return homrom;
    }

    public void setHomrom(String homrom) {
        this.homrom = homrom;
    }

    public String getEmptus() {
        return emptus;
    }

    public void setEmptus(String emptus) {
        this.emptus = emptus;
    }

    public String getEmpity() {
        return empity;
    }

    public void setEmpity(String empity) {
        this.empity = empity;
    }

    public String getTitlal() {
        return titlal;
    }

    public void setTitlal(String titlal) {
        this.titlal = titlal;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getIsdate() {
        return isdate;
    }

    public void setIsdate(String isdate) {
        this.isdate = isdate;
    }

    @Override
    public String toString() {
        return "Service{" +
                "idtftp='" + idtftp + '\'' +
                "idtfno='" + idtfno + '\'' +
                "cardno='" + cardno + '\'' +
                "opt='" + opt + '\'' +
                "title='" + title + '\'' +
                "custnm='" + custnm + '\'' +
                "gender='" + gender + '\'' +
                "ocpatn='" + ocpatn + '\'' +
                "bkmrno='" + bkmrno + '\'' +
                "ntnaty='" + ntnaty + '\'' +
                "mtstat='" + mtstat + '\'' +
                "qlftin='" + qlftin + '\'' +
                "hmphon='" + hmphon + '\'' +
                "mobile='" + mobile + '\'' +
                "email='" + email + '\'' +
                "corpnm='" + corpnm + '\'' +
                "embnam='" + embnam + '\'' +
                "iedate='" + iedate + '\'' +
                "idaddr='" + idaddr + '\'' +
                "brtday='" + brtday + '\'' +
                "hourst='" + hourst + '\'' +
                "hourpe='" + hourpe + '\'' +
                "homrom='" + homrom + '\'' +
                "emptus='" + emptus + '\'' +
                "empity='" + empity + '\'' +
                "titlal='" + titlal + '\'' +
                "income='" + income + '\'' +
                "isdate='" + isdate + '\'' +
                '}';
    }
}
