package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

import java.math.BigDecimal;
import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/6/7 17:28:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 17:28
 * @since 2021/6/7 17:28
 */
public class LstDzjtmx {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Record2> record;

    public List<Record2> getRecord() {
        return record;
    }

    public void setRecord(List<Record2> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstDzjtmx{" +
                "record=" + record +
                '}';
    }
}
