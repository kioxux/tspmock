package cn.com.yusys.yusp.web.server.biz.xddb0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0016.req.Xddb0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0016.resp.Xddb0016RespDto;
import cn.com.yusys.yusp.dto.server.xddb0016.req.Xddb0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0016.resp.Xddb0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品信息查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDDB0016:押品信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0016Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizdbClientService;

    /**
     * 交易码：xddb0016
     * 交易描述：押品信息查询
     *
     * @param xddb0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品状态查询")
    @PostMapping("/xddb0016")
    //@Idempotent({"xddb0016", "#xddb0016ReqDto.datasq"})
    protected @ResponseBody
    Xddb0016RespDto xddb0016(@Validated @RequestBody Xddb0016ReqDto xddb0016ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(xddb0016ReqDto));
        Xddb0016DataReqDto xddb0016DataReqDto = new Xddb0016DataReqDto();// 请求Data： 押品信息查询
        Xddb0016DataRespDto xddb0016DataRespDto = new Xddb0016DataRespDto();// 响应Data：押品信息查询
        Xddb0016RespDto xddb0016RespDto = new Xddb0016RespDto();
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xddb0016.req.Data reqData = null; // 请求Data：押品信息查询
        cn.com.yusys.yusp.dto.server.biz.xddb0016.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0016.resp.Data();// 响应Data：押品信息查询
        try {
            //  此处包导入待确定 结束
            // 从 xddb0016ReqDto获取 reqData
            reqData = xddb0016ReqDto.getData();
            // 将 reqData 拷贝到xddb0016DataReqDto
            BeanUtils.copyProperties(reqData, xddb0016DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xddb0016DataReqDto));
            ResultDto<Xddb0016DataRespDto> xddb0016DataResultDto = dscmsBizdbClientService.xddb0016(xddb0016DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xddb0016DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0016RespDto.setErorcd(Optional.ofNullable(xddb0016DataResultDto.getCode()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xddb0016RespDto.setErortx(Optional.ofNullable(xddb0016DataResultDto.getMessage()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0016DataResultDto.getCode())) {
                xddb0016DataRespDto = xddb0016DataResultDto.getData();
                // 将 xddb0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0016DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0018.key, DscmsEnum.TRADE_CODE_XDDB0018.value, e.getMessage());
            xddb0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0016RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0016RespDto.setDatasq(servsq);

        xddb0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0016.key, DscmsEnum.TRADE_CODE_XDDB0016.value, JSON.toJSONString(xddb0016RespDto));
        return xddb0016RespDto;
    }
}
