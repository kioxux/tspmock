package cn.com.yusys.yusp.online.client.esb.core.ln3006.resp;

/**
 * 响应Service：贷款产品组合查询
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String chanpzdm;//产品组代码
    private String chanpzmc;//产品组名称
    private String shengxrq;//生效日期
    private String shxiaorq;//失效日期
    private String chanpzht;//产品状态
    private String daikduix;//贷款对象
    private String yewufenl;//业务分类
    private String ysywleix;//衍生业务类型
    private String chpjlirq;//产品建立日期
    private String chpjligy;//产品建立柜员
    private String farendma;//法人代码
    private String weihguiy;//维护柜员
    private String weihjigo;//维护机构
    private String weihriqi;//维护日期
    private Integer shijchuo;//时间戳
    private String jiluztai;//记录状态


    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getChanpzdm() {
        return chanpzdm;
    }

    public void setChanpzdm(String chanpzdm) {
        this.chanpzdm = chanpzdm;
    }

    public String getChanpzmc() {
        return chanpzmc;
    }

    public void setChanpzmc(String chanpzmc) {
        this.chanpzmc = chanpzmc;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getShxiaorq() {
        return shxiaorq;
    }

    public void setShxiaorq(String shxiaorq) {
        this.shxiaorq = shxiaorq;
    }

    public String getChanpzht() {
        return chanpzht;
    }

    public void setChanpzht(String chanpzht) {
        this.chanpzht = chanpzht;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getYsywleix() {
        return ysywleix;
    }

    public void setYsywleix(String ysywleix) {
        this.ysywleix = ysywleix;
    }

    public String getChpjlirq() {
        return chpjlirq;
    }

    public void setChpjlirq(String chpjlirq) {
        this.chpjlirq = chpjlirq;
    }

    public String getChpjligy() {
        return chpjligy;
    }

    public void setChpjligy(String chpjligy) {
        this.chpjligy = chpjligy;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public Integer getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Integer shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "Record{" +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "chanpzdm='" + chanpzdm + '\'' +
                "chanpzmc='" + chanpzmc + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "shxiaorq='" + shxiaorq + '\'' +
                "chanpzht='" + chanpzht + '\'' +
                "daikduix='" + daikduix + '\'' +
                "yewufenl='" + yewufenl + '\'' +
                "ysywleix='" + ysywleix + '\'' +
                "chpjlirq='" + chpjlirq + '\'' +
                "chpjligy='" + chpjligy + '\'' +
                "farendma='" + farendma + '\'' +
                "weihguiy='" + weihguiy + '\'' +
                "weihjigo='" + weihjigo + '\'' +
                "weihriqi='" + weihriqi + '\'' +
                "shijchuo='" + shijchuo + '\'' +
                "jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
