package cn.com.yusys.yusp.web.server.biz.xdtz0049;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0049.req.Xdtz0049ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0049.resp.Xdtz0049RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0049.req.Xdtz0049DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0049.resp.Xdtz0049DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户贷款信息查询
 *
 * @author chenyog
 * @version 1.0
 */
@Api(tags = "XDTZ0049:客户贷款信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0049Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0049Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0049
     * 交易描述：客户贷款信息查询
     *
     * @param xdtz0049ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户贷款信息查询")
    @PostMapping("/xdtz0049")
    //@Idempotent({"xdcatz0049", "#xdtz0049ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0049RespDto xdtz0049(@Validated @RequestBody Xdtz0049ReqDto xdtz0049ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, JSON.toJSONString(xdtz0049ReqDto));
        Xdtz0049DataReqDto xdtz0049DataReqDto = new Xdtz0049DataReqDto();// 请求Data： 客户贷款信息查询
        Xdtz0049DataRespDto xdtz0049DataRespDto = new Xdtz0049DataRespDto();// 响应Data：客户贷款信息查询
        Xdtz0049RespDto xdtz0049RespDto = new Xdtz0049RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0049.req.Data reqData = null; // 请求Data：客户贷款信息查询
        cn.com.yusys.yusp.dto.server.biz.xdtz0049.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0049.resp.Data();// 响应Data：客户贷款信息查询
        try {
            // 从 xdtz0049ReqDto获取 reqData
            reqData = xdtz0049ReqDto.getData();
            // 将 reqData 拷贝到xdtz0049DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0049DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, JSON.toJSONString(xdtz0049DataReqDto));
            ResultDto<Xdtz0049DataRespDto> xdtz0049DataResultDto = dscmsBizTzClientService.xdtz0049(xdtz0049DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, JSON.toJSONString(xdtz0049DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0049RespDto.setErorcd(Optional.ofNullable(xdtz0049DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0049RespDto.setErortx(Optional.ofNullable(xdtz0049DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0049DataResultDto.getCode())) {
                xdtz0049DataRespDto = xdtz0049DataResultDto.getData();
                xdtz0049RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0049RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0049DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0049DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, e.getMessage());
            xdtz0049RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0049RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0049RespDto.setDatasq(servsq);

        xdtz0049RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0049.key, DscmsEnum.TRADE_CODE_XDTZ0049.value, JSON.toJSONString(xdtz0049RespDto));
        return xdtz0049RespDto;
    }
}
