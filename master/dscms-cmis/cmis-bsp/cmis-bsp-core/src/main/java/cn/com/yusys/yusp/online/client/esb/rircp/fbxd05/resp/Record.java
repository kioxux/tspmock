package cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp;

/**
 * 响应Service：查找指定数据日期的放款合约明细记录历史表（利翃）一览信息的借据号
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String contract_no;//借据编号

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Record{" +
                "contract_no='" + contract_no + '\'' +
                '}';
    }
}
