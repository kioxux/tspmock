package cn.com.yusys.yusp.online.client.esb.circp.fb1174.resp;

/**
 * 响应Service：房抵e点贷尽调结果通知
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1174RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1174RespService{" +
                "service=" + service +
                '}';
    }
}
