package cn.com.yusys.yusp.web.server.biz.xdxw0074;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0074.req.Xdxw0074ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0074.resp.Xdxw0074RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0074.req.Xdxw0074DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0074.resp.Xdxw0074DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户身份证号查询线上产品申请记录
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0074:根据客户身份证号查询线上产品申请记录")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0074Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0074Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0074
     * 交易描述：根据客户身份证号查询线上产品申请记录
     *
     * @param xdxw0074ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户身份证号查询线上产品申请记录")
    @PostMapping("/xdxw0074")
    //@Idempotent({"xdcaxw0074", "#xdxw0074ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0074RespDto xdxw0074(@Validated @RequestBody Xdxw0074ReqDto xdxw0074ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074ReqDto));
        Xdxw0074DataReqDto xdxw0074DataReqDto = new Xdxw0074DataReqDto();// 请求Data： 根据客户身份证号查询线上产品申请记录
        Xdxw0074DataRespDto xdxw0074DataRespDto = new Xdxw0074DataRespDto();// 响应Data：根据客户身份证号查询线上产品申请记录
		Xdxw0074RespDto xdxw0074RespDto = new Xdxw0074RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0074.req.Data reqData = null; // 请求Data：根据客户身份证号查询线上产品申请记录
		cn.com.yusys.yusp.dto.server.biz.xdxw0074.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0074.resp.Data();// 响应Data：根据客户身份证号查询线上产品申请记录

        try {
            // 从 xdxw0074ReqDto获取 reqData
            reqData = xdxw0074ReqDto.getData();
            // 将 reqData 拷贝到xdxw0074DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0074DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074DataReqDto));
            ResultDto<Xdxw0074DataRespDto> xdxw0074DataResultDto = dscmsBizXwClientService.xdxw0074(xdxw0074DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0074RespDto.setErorcd(Optional.ofNullable(xdxw0074DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0074RespDto.setErortx(Optional.ofNullable(xdxw0074DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0074DataResultDto.getCode())) {
                xdxw0074DataRespDto = xdxw0074DataResultDto.getData();
                xdxw0074RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0074RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0074DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0074DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, e.getMessage());
            xdxw0074RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0074RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0074RespDto.setDatasq(servsq);

        xdxw0074RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0074.key, DscmsEnum.TRADE_CODE_XDXW0074.value, JSON.toJSONString(xdxw0074RespDto));
        return xdxw0074RespDto;
    }
}
