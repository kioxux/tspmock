package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpsmr.resp;

import java.math.BigDecimal;

/**
 * 响应Service：大额往帐一体化
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private String chtrtp;//手续费收取方式
    private String hstrsq1;//业务受理编号
    private String payseqno;//支付序号
    private String transq;//核心流水号
    private String bdwybs;//BDWYBS标段号
    private BigDecimal tranam;//交易金额
    private BigDecimal handch;//手续费
    private String guiylius_hzh;//柜员流水号
    private String shoufdma;//收费代码
    private String shfdmamc;//收费代码名称
    private BigDecimal yingshfy;//应收费用
    private BigDecimal shifujne;//实付金额
    private String sffeiybz;//是否费用标志
    private String jiaoyirq;//核心交易日期
    private String jiaoyisj;//核心交易时间

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getChtrtp() {
        return chtrtp;
    }

    public void setChtrtp(String chtrtp) {
        this.chtrtp = chtrtp;
    }

    public String getHstrsq1() {
        return hstrsq1;
    }

    public void setHstrsq1(String hstrsq1) {
        this.hstrsq1 = hstrsq1;
    }

    public String getPayseqno() {
        return payseqno;
    }

    public void setPayseqno(String payseqno) {
        this.payseqno = payseqno;
    }

    public String getTransq() {
        return transq;
    }

    public void setTransq(String transq) {
        this.transq = transq;
    }

    public String getBdwybs() {
        return bdwybs;
    }

    public void setBdwybs(String bdwybs) {
        this.bdwybs = bdwybs;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public BigDecimal getHandch() {
        return handch;
    }

    public void setHandch(BigDecimal handch) {
        this.handch = handch;
    }

    public String getGuiylius_hzh() {
        return guiylius_hzh;
    }

    public void setGuiylius_hzh(String guiylius_hzh) {
        this.guiylius_hzh = guiylius_hzh;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getShifujne() {
        return shifujne;
    }

    public void setShifujne(BigDecimal shifujne) {
        this.shifujne = shifujne;
    }

    public String getSffeiybz() {
        return sffeiybz;
    }

    public void setSffeiybz(String sffeiybz) {
        this.sffeiybz = sffeiybz;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", chtrtp='" + chtrtp + '\'' +
                ", hstrsq1='" + hstrsq1 + '\'' +
                ", payseqno='" + payseqno + '\'' +
                ", transq='" + transq + '\'' +
                ", bdwybs='" + bdwybs + '\'' +
                ", tranam=" + tranam +
                ", handch=" + handch +
                ", guiylius_hzh='" + guiylius_hzh + '\'' +
                ", shoufdma='" + shoufdma + '\'' +
                ", shfdmamc='" + shfdmamc + '\'' +
                ", yingshfy=" + yingshfy +
                ", shifujne=" + shifujne +
                ", sffeiybz='" + sffeiybz + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyisj='" + jiaoyisj + '\'' +
                '}';
    }
}
