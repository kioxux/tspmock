package cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Service：涉诉信息查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class QyssxxReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//    交易码
    @JsonProperty(value = "servtp")
    private String servtp;//    渠道码
    @JsonProperty(value = "servsq")
    private String servsq;//    渠道流水号
    @JsonProperty(value = "userid")
    private String userid;//    柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//    部门号
    @JsonProperty(value = "type")
    private String type;//查询类型 0：查询自然人， 1：查询组织机构
    @JsonProperty(value = "name")
    private String name;//姓名/企业名称
    @JsonProperty(value = "qyid")
    private String qyid;//身份证号/组织机构代码

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQyid() {
        return qyid;
    }

    public void setQyid(String qyid) {
        this.qyid = qyid;
    }

    @Override
    public String toString() {
        return "QyssxxReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", qyid='" + qyid + '\'' +
                '}';
    }
}
