package cn.com.yusys.yusp.online.client.esb.core.co3201.req;

import java.math.BigDecimal;

/**
 * 请求Service：抵质押物信息修改
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String dkkhczbz;//开户操作标志
    private String dzywbhao;//抵质押物编号
    private String dzywminc;//抵质押物名称
    private String zhngjigo;//账务机构
    private String dizyfshi;//抵质押方式
    private String syqrkehh;//所有权人客户号
    private String syqrkehm;//所有权人客户名
    private String huobdhao;//货币代号
    private BigDecimal minyjiaz;//名义价值
    private BigDecimal shijjiaz;//实际价值
    private BigDecimal pingjiaz;//评估价值
    private BigDecimal dizybilv;//抵质押比率
    private BigDecimal keyongje;//可用金额
    private BigDecimal yiyongje;//已用金额
    private String shengxrq;//生效日期
    private String daoqriqi;//到期日期
    private String zhaiyoms;//摘要
    private String glywbhao;//关联业务编号
    private String yewusx01;//记账余额属性
    private String huowuhth;//货物合同号
    private String yewmingc;//业务名称
    private cn.com.yusys.yusp.online.client.esb.core.co3201.req.Lstklnb_dkzhzy lstklnb_dkzhzy;//抵质押物信息

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDzywminc() {
        return dzywminc;
    }

    public void setDzywminc(String dzywminc) {
        this.dzywminc = dzywminc;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getDizyfshi() {
        return dizyfshi;
    }

    public void setDizyfshi(String dizyfshi) {
        this.dizyfshi = dizyfshi;
    }

    public String getSyqrkehh() {
        return syqrkehh;
    }

    public void setSyqrkehh(String syqrkehh) {
        this.syqrkehh = syqrkehh;
    }

    public String getSyqrkehm() {
        return syqrkehm;
    }

    public void setSyqrkehm(String syqrkehm) {
        this.syqrkehm = syqrkehm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getMinyjiaz() {
        return minyjiaz;
    }

    public void setMinyjiaz(BigDecimal minyjiaz) {
        this.minyjiaz = minyjiaz;
    }

    public BigDecimal getShijjiaz() {
        return shijjiaz;
    }

    public void setShijjiaz(BigDecimal shijjiaz) {
        this.shijjiaz = shijjiaz;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDizybilv() {
        return dizybilv;
    }

    public void setDizybilv(BigDecimal dizybilv) {
        this.dizybilv = dizybilv;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public BigDecimal getYiyongje() {
        return yiyongje;
    }

    public void setYiyongje(BigDecimal yiyongje) {
        this.yiyongje = yiyongje;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getGlywbhao() {
        return glywbhao;
    }

    public void setGlywbhao(String glywbhao) {
        this.glywbhao = glywbhao;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getHuowuhth() {
        return huowuhth;
    }

    public void setHuowuhth(String huowuhth) {
        this.huowuhth = huowuhth;
    }

    public String getYewmingc() {
        return yewmingc;
    }

    public void setYewmingc(String yewmingc) {
        this.yewmingc = yewmingc;
    }

    public Lstklnb_dkzhzy getLstklnb_dkzhzy() {
        return lstklnb_dkzhzy;
    }

    public void setLstklnb_dkzhzy(Lstklnb_dkzhzy lstklnb_dkzhzy) {
        this.lstklnb_dkzhzy = lstklnb_dkzhzy;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkkhczbz='" + dkkhczbz + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                ", dzywminc='" + dzywminc + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", dizyfshi='" + dizyfshi + '\'' +
                ", syqrkehh='" + syqrkehh + '\'' +
                ", syqrkehm='" + syqrkehm + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", minyjiaz=" + minyjiaz +
                ", shijjiaz=" + shijjiaz +
                ", pingjiaz=" + pingjiaz +
                ", dizybilv=" + dizybilv +
                ", keyongje=" + keyongje +
                ", yiyongje=" + yiyongje +
                ", shengxrq='" + shengxrq + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", glywbhao='" + glywbhao + '\'' +
                ", yewusx01='" + yewusx01 + '\'' +
                ", huowuhth='" + huowuhth + '\'' +
                ", yewmingc='" + yewmingc + '\'' +
                ", lstklnb_dkzhzy=" + lstklnb_dkzhzy +
                '}';
    }
}
