package cn.com.yusys.yusp.web.server.oca.xdxt0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.oca.xdxt0008.req.Xdxt0008ReqDto;
import cn.com.yusys.yusp.dto.server.oca.xdxt0008.resp.Xdxt0008RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.req.Xdxt0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0008.resp.Xdxt0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户经理号查询账务机构号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0008:根据客户经理号查询账务机构号")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0008Resource.class);
    @Autowired
    private DscmsXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0008
     * 交易描述：根据客户经理号查询账务机构号
     *
     * @param xdxt0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户经理号查询账务机构号")
    @PostMapping("/xdxt0008")
    //@Idempotent({"xdcaxt0008", "#xdxt0008ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0008RespDto xdxt0008(@Validated @RequestBody Xdxt0008ReqDto xdxt0008ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0008.key, DscmsEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008ReqDto));
        Xdxt0008DataReqDto xdxt0008DataReqDto = new Xdxt0008DataReqDto();// 请求Data： 根据客户经理号查询账务机构号
        Xdxt0008DataRespDto xdxt0008DataRespDto = new Xdxt0008DataRespDto();// 响应Data：根据客户经理号查询账务机构号
        Xdxt0008RespDto xdxt0008RespDto = new Xdxt0008RespDto();
        cn.com.yusys.yusp.dto.server.oca.xdxt0008.req.Data reqData = null; // 请求Data：根据客户经理号查询账务机构号
        cn.com.yusys.yusp.dto.server.oca.xdxt0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.oca.xdxt0008.resp.Data();// 响应Data：根据客户经理号查询账务机构号

        try {
            // 从 xdxt0008ReqDto获取 reqData
            reqData = xdxt0008ReqDto.getData();
            // 将 reqData 拷贝到xdxt0008DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0008DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0008.key, DscmsEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008DataReqDto));
            ResultDto<Xdxt0008DataRespDto> xdxt0008DataResultDto = dscmsXtClientService.xdxt0008(xdxt0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0008.key, DscmsEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0008RespDto.setErorcd(Optional.ofNullable(xdxt0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0008RespDto.setErortx(Optional.ofNullable(xdxt0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0008DataResultDto.getCode())) {
                xdxt0008DataRespDto = xdxt0008DataResultDto.getData();
                xdxt0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0008.key, DscmsEnum.TRADE_CODE_XDXT0008.value, e.getMessage());
            xdxt0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0008RespDto.setDatasq(servsq);

        xdxt0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0008.key, DscmsEnum.TRADE_CODE_XDXT0008.value, JSON.toJSONString(xdxt0008RespDto));
        return xdxt0008RespDto;
    }
}
