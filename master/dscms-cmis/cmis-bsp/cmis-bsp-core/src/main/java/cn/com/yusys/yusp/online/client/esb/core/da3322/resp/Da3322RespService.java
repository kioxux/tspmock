package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

/**
 * 响应Service：抵债资产明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Da3322RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3322RespService{" +
                "service=" + service +
                '}';
    }
}

