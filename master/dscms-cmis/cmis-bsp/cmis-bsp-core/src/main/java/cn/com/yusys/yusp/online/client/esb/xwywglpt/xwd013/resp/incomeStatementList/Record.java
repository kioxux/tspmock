package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.incomeStatementList;

public class Record {

    private String totalsalFullYear;//销售收入-总额 - 本期
    private String variableMatYear;//可变成本-总额 - 本期
    private String grossYearRecently;//毛利润 - 本期
    private String fixedManagecostFullYear;//固定支出-总额 - 本期
    private String recentlyNetProfit;//净利润 - 本期

    public String getTotalsalFullYear() {
        return totalsalFullYear;
    }

    public void setTotalsalFullYear(String totalsalFullYear) {
        this.totalsalFullYear = totalsalFullYear;
    }

    public String getVariableMatYear() {
        return variableMatYear;
    }

    public void setVariableMatYear(String variableMatYear) {
        this.variableMatYear = variableMatYear;
    }

    public String getGrossYearRecently() {
        return grossYearRecently;
    }

    public void setGrossYearRecently(String grossYearRecently) {
        this.grossYearRecently = grossYearRecently;
    }

    public String getFixedManagecostFullYear() {
        return fixedManagecostFullYear;
    }

    public void setFixedManagecostFullYear(String fixedManagecostFullYear) {
        this.fixedManagecostFullYear = fixedManagecostFullYear;
    }

    public String getRecentlyNetProfit() {
        return recentlyNetProfit;
    }

    public void setRecentlyNetProfit(String recentlyNetProfit) {
        this.recentlyNetProfit = recentlyNetProfit;
    }

    @Override
    public String toString() {
        return "Record{" +
                "totalsalFullYear='" + totalsalFullYear + '\'' +
                ", variableMatYear='" + variableMatYear + '\'' +
                ", grossYearRecently='" + grossYearRecently + '\'' +
                ", fixedManagecostFullYear='" + fixedManagecostFullYear + '\'' +
                ", recentlyNetProfit='" + recentlyNetProfit + '\'' +
                '}';
    }
}
