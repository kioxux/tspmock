package cn.com.yusys.yusp.online.client.http.comstar.com001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Service：额度同步
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "number")
    private String number;//序号
    @JsonProperty(value = "origiAccNo")
    private String origiAccNo;//授信批复编号
    @JsonProperty(value = "lmtSubNo")
    private String lmtSubNo;//授信分项ID
    @JsonProperty(value = "lmtType")
    private String lmtType;//额度类型
    @JsonProperty(value = "limitSubNo")
    private String limitSubNo;//额度品种编号
    @JsonProperty(value = "limitSubName")
    private String limitSubName;//额度品种名称
    @JsonProperty(value = "cusId")
    private String cusId;//Ecif客户号
    @JsonProperty(value = "certType")
    private String certType;//授信主体证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//授信主体证件号码
    @JsonProperty(value = "cusName")
    private String cusName;//授信主体客户名称
    @JsonProperty(value = "assetNo")
    private String assetNo;//资产编号
    @JsonProperty(value = "lmtAmt")
    private BigDecimal lmtAmt;//授信金额
    @JsonProperty(value = "singleeResaleQuota")
    private BigDecimal singleeResaleQuota;//单个产品户买入返售限额
    @JsonProperty(value = "lmtSingleMfAmt")
    private BigDecimal lmtSingleMfAmt;//单只货币基金授信额度
    @JsonProperty(value = "endDate")
    private String endDate;//额度到期日
    @JsonProperty(value = "optType")
    private String optType;//操作类型

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOrigiAccNo() {
        return origiAccNo;
    }

    public void setOrigiAccNo(String origiAccNo) {
        this.origiAccNo = origiAccNo;
    }

    public String getLmtSubNo() {
        return lmtSubNo;
    }

    public void setLmtSubNo(String lmtSubNo) {
        this.lmtSubNo = lmtSubNo;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public BigDecimal getSingleeResaleQuota() {
        return singleeResaleQuota;
    }

    public void setSingleeResaleQuota(BigDecimal singleeResaleQuota) {
        this.singleeResaleQuota = singleeResaleQuota;
    }

    public BigDecimal getLmtSingleMfAmt() {
        return lmtSingleMfAmt;
    }

    public void setLmtSingleMfAmt(BigDecimal lmtSingleMfAmt) {
        this.lmtSingleMfAmt = lmtSingleMfAmt;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    @Override
    public String toString() {
        return "List{" +
                "number='" + number + '\'' +
                ", origiAccNo='" + origiAccNo + '\'' +
                ", lmtSubNo='" + lmtSubNo + '\'' +
                ", lmtType='" + lmtType + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", cusId='" + cusId + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", cusName='" + cusName + '\'' +
                ", assetNo='" + assetNo + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", singleeResaleQuota=" + singleeResaleQuota +
                ", lmtSingleMfAmt=" + lmtSingleMfAmt +
                ", endDate='" + endDate + '\'' +
                ", optType='" + optType + '\'' +
                '}';
    }
}
