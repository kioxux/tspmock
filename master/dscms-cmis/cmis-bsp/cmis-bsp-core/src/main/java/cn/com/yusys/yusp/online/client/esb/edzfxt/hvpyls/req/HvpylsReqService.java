package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.req;

/**
 * 请求Service：大额往账列表查询
 *
 * @author code-generator
 * @version 1.0
 */
public class HvpylsReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}                      
