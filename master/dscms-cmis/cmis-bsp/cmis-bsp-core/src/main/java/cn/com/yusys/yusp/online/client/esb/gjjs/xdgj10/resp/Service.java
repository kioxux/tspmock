package cn.com.yusys.yusp.online.client.esb.gjjs.xdgj10.resp;

import java.math.BigDecimal;

/**
 * 响应Service：信贷获取国结信用证信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String is_cz;//成功默认1，失败为2
    private String cus_id;//核心客户号
    private String cus_name;//客户名称
    private String lc_no;//信用证编号
    private String apply_cur_type;//开证币种
    private BigDecimal apply_amount;//开证金额
    private String pay_term;//付款期限
    private String loc_due_date;//到期日期
    private String days_of_time;//远期天数

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getIs_cz() {
        return is_cz;
    }

    public void setIs_cz(String is_cz) {
        this.is_cz = is_cz;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getLc_no() {
        return lc_no;
    }

    public void setLc_no(String lc_no) {
        this.lc_no = lc_no;
    }

    public String getApply_cur_type() {
        return apply_cur_type;
    }

    public void setApply_cur_type(String apply_cur_type) {
        this.apply_cur_type = apply_cur_type;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getPay_term() {
        return pay_term;
    }

    public void setPay_term(String pay_term) {
        this.pay_term = pay_term;
    }

    public String getLoc_due_date() {
        return loc_due_date;
    }

    public void setLoc_due_date(String loc_due_date) {
        this.loc_due_date = loc_due_date;
    }

    public String getDays_of_time() {
        return days_of_time;
    }

    public void setDays_of_time(String days_of_time) {
        this.days_of_time = days_of_time;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "is_cz='" + is_cz + '\'' +
                "cus_id='" + cus_id + '\'' +
                "cus_name='" + cus_name + '\'' +
                "lc_no='" + lc_no + '\'' +
                "apply_cur_type='" + apply_cur_type + '\'' +
                "apply_amount='" + apply_amount + '\'' +
                "pay_term='" + pay_term + '\'' +
                "loc_due_date='" + loc_due_date + '\'' +
                "days_of_time='" + days_of_time + '\'' +
                '}';
    }
}  
