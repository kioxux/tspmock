package cn.com.yusys.yusp.web.server.biz.xdht0040;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0040.req.Xdht0040ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0040.resp.Xdht0040RespDto;
import cn.com.yusys.yusp.dto.server.xdht0040.req.Xdht0040DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0040.resp.Xdht0040DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:修改受托信息
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDHT0040:修改受托信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0040Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0040Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0040
     * 交易描述：修改受托信息
     *
     * @param xdht0040ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("修改受托信息")
    @PostMapping("/xdht0040")
    //@Idempotent({"xdcaht0040", "#xdht0040ReqDto.datasq"})
    protected @ResponseBody
    Xdht0040RespDto xdht0040(@Validated @RequestBody Xdht0040ReqDto xdht0040ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040ReqDto));
        Xdht0040DataReqDto xdht0040DataReqDto = new Xdht0040DataReqDto();// 请求Data： 修改受托信息
        Xdht0040DataRespDto xdht0040DataRespDto = new Xdht0040DataRespDto();// 响应Data：修改受托信息
        Xdht0040RespDto xdht0040RespDto = new Xdht0040RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdht0040.req.Data reqData = null; // 请求Data：修改受托信息
        cn.com.yusys.yusp.dto.server.biz.xdht0040.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0040.resp.Data();// 响应Data：修改受托信息

        try {
            // 从 xdht0040ReqDto获取 reqData
            reqData = xdht0040ReqDto.getData();
            // 将 reqData 拷贝到xdht0040DataReqDto
            BeanUtils.copyProperties(reqData, xdht0040DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040DataReqDto));
            ResultDto<Xdht0040DataRespDto> xdht0040DataResultDto = dscmsBizHtClientService.xdht0040(xdht0040DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0040RespDto.setErorcd(Optional.ofNullable(xdht0040DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0040RespDto.setErortx(Optional.ofNullable(xdht0040DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0040DataResultDto.getCode())) {
                xdht0040DataRespDto = xdht0040DataResultDto.getData();
                xdht0040RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0040RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0040DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0040DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, e.getMessage());
            xdht0040RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0040RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0040RespDto.setDatasq(servsq);

        xdht0040RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0040.key, DscmsEnum.TRADE_CODE_XDHT0040.value, JSON.toJSONString(xdht0040RespDto));
        return xdht0040RespDto;
    }
}
