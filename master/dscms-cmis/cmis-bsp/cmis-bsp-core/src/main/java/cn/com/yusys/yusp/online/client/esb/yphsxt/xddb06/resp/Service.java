package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询共有人信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述
    private cn.com.yusys.yusp.online.client.esb.yphsxt.xddb06.resp.List list;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", list=" + list +
                '}';
    }
}
