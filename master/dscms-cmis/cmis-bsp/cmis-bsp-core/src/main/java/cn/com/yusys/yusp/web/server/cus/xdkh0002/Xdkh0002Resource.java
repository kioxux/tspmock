package cn.com.yusys.yusp.web.server.cus.xdkh0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0002.req.Xdkh0002ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0002.resp.Xdkh0002RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.req.Xdkh0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0002.resp.Xdkh0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:对公客户基本信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDKH0002", value = "对公客户基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0002Resource.class);

    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0002
     * 交易描述：对公客户基本信息查询
     *
     * @param xdkh0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对公客户基本信息查询")
    @PostMapping("/xdkh0002")
//    @Idempotent({"xdcakh0002", "#xdkh0002ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0002RespDto xdkh0002(@Validated @RequestBody Xdkh0002ReqDto xdkh0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002ReqDto));
        Xdkh0002DataReqDto xdkh0002DataReqDto = new Xdkh0002DataReqDto();// 请求Data： 对公客户基本信息查询
        Xdkh0002DataRespDto xdkh0002DataRespDto = null;// 响应Data：对公客户基本信息查询
        Xdkh0002RespDto xdkh0002RespDto = new Xdkh0002RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0002.req.Data reqData = null; // 请求Data：对公客户基本信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0002.resp.Data();// 响应Data：对公客户基本信息查询
        try {
            // 从 xdkh0002ReqDto获取 reqData
            reqData = xdkh0002ReqDto.getData();
            // 将 reqData 拷贝到xdkh0002DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0002DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002DataReqDto));
            ResultDto<Xdkh0002DataRespDto> xdkh0002DataResultDto = dscmsCusClientService.xdkh0002(xdkh0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0002RespDto.setErorcd(Optional.ofNullable(xdkh0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0002RespDto.setErortx(Optional.ofNullable(xdkh0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0002DataResultDto.getCode())) {
                xdkh0002DataRespDto = xdkh0002DataResultDto.getData();
                xdkh0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, e.getMessage());
            xdkh0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0002RespDto.setDatasq(servsq);

        xdkh0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0002.key, DscmsEnum.TRADE_CODE_XDKH0002.value, JSON.toJSONString(xdkh0002RespDto));
        return xdkh0002RespDto;
    }
}
