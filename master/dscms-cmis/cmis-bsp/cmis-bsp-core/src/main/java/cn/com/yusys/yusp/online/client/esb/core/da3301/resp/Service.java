package cn.com.yusys.yusp.online.client.esb.core.da3301.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵债资产入账
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String prcscd;//交易码

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String yngyjigo;//营业机构
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private BigDecimal dbgdzcje;//代保管抵债资产金额
    private BigDecimal dcldzcje;//待处理抵债资产金额
    private BigDecimal hfeijine;//还费金额
    private BigDecimal huanbjee;//还本金额
    private BigDecimal hxijinee;//还息金额
    private String dzzszhao;//抵债暂收账号
    private String dzzszzxh;//抵债暂收账号子序号
    private BigDecimal dbxdzcje;//待变现抵债资产金额
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String dizruzfs;//抵债入账方式
    private BigDecimal huankjee;//还款金额
    private Listnm1 listnm1;//借据关联

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getDbgdzcje() {
        return dbgdzcje;
    }

    public void setDbgdzcje(BigDecimal dbgdzcje) {
        this.dbgdzcje = dbgdzcje;
    }

    public BigDecimal getDcldzcje() {
        return dcldzcje;
    }

    public void setDcldzcje(BigDecimal dcldzcje) {
        this.dcldzcje = dcldzcje;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getDzzszhao() {
        return dzzszhao;
    }

    public void setDzzszhao(String dzzszhao) {
        this.dzzszhao = dzzszhao;
    }

    public String getDzzszzxh() {
        return dzzszzxh;
    }

    public void setDzzszzxh(String dzzszzxh) {
        this.dzzszzxh = dzzszzxh;
    }

    public BigDecimal getDbxdzcje() {
        return dbxdzcje;
    }

    public void setDbxdzcje(BigDecimal dbxdzcje) {
        this.dbxdzcje = dbxdzcje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getDizruzfs() {
        return dizruzfs;
    }

    public void setDizruzfs(String dizruzfs) {
        this.dizruzfs = dizruzfs;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public Listnm1 getListnm1() {
        return listnm1;
    }

    public void setListnm1(Listnm1 listnm1) {
        this.listnm1 = listnm1;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", prcscd='" + prcscd + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", dbgdzcje=" + dbgdzcje +
                ", dcldzcje=" + dcldzcje +
                ", hfeijine=" + hfeijine +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", dzzszhao='" + dzzszhao + '\'' +
                ", dzzszzxh='" + dzzszzxh + '\'' +
                ", dbxdzcje=" + dbxdzcje +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", dizruzfs='" + dizruzfs + '\'' +
                ", huankjee=" + huankjee +
                ", listnm1=" + listnm1 +
                '}';
    }
}
