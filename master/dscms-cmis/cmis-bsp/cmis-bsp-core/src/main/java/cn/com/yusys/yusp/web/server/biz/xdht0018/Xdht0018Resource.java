package cn.com.yusys.yusp.web.server.biz.xdht0018;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0018.req.Xdht0018ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0018.resp.Xdht0018RespDto;
import cn.com.yusys.yusp.dto.server.xdht0018.req.Xdht0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0018.resp.Xdht0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:担保合同文本生成pdf
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0018:担保合同文本生成pdf")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0018Resource.class);

	@Resource
	private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0018
     * 交易描述：担保合同文本生成pdf
     *
     * @param xdht0018ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("担保合同文本生成pdf")
    @PostMapping("/xdht0018")
//    @Idempotent({"xdcaht0018", "#xdht0018ReqDto.datasq"})
    protected @ResponseBody
	Xdht0018RespDto xdht0018(@Validated @RequestBody Xdht0018ReqDto xdht0018ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018ReqDto));
        Xdht0018DataReqDto xdht0018DataReqDto = new Xdht0018DataReqDto();// 请求Data： 担保合同文本生成pdf
        Xdht0018DataRespDto xdht0018DataRespDto = new Xdht0018DataRespDto();// 响应Data：担保合同文本生成pdf
		Xdht0018RespDto xdht0018RespDto = new Xdht0018RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdht0018.req.Data reqData = null; // 请求Data：担保合同文本生成pdf
		cn.com.yusys.yusp.dto.server.biz.xdht0018.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0018.resp.Data();// 响应Data：担保合同文本生成pdf
        //  此处包导入待确定 结束
        try {
            // 从 xdht0018ReqDto获取 reqData
            reqData = xdht0018ReqDto.getData();
            // 将 reqData 拷贝到xdht0018DataReqDto
            BeanUtils.copyProperties(reqData, xdht0018DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018DataReqDto));
            ResultDto<Xdht0018DataRespDto> xdht0018DataResultDto = dscmsBizHtClientService.xdht0018(xdht0018DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0018RespDto.setErorcd(Optional.ofNullable(xdht0018DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0018RespDto.setErortx(Optional.ofNullable(xdht0018DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0018DataResultDto.getCode())) {
                xdht0018DataRespDto = xdht0018DataResultDto.getData();
                xdht0018RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0018RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0018DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0018DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, e.getMessage());
            xdht0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0018RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0018RespDto.setDatasq(servsq);

        xdht0018RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0018.key, DscmsEnum.TRADE_CODE_XDHT0018.value, JSON.toJSONString(xdht0018RespDto));
        return xdht0018RespDto;
    }
}
