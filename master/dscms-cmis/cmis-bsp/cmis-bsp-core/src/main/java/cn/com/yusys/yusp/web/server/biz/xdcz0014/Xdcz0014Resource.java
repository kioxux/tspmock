package cn.com.yusys.yusp.web.server.biz.xdcz0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0014.req.Xdcz0014ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0014.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0014.resp.Xdcz0014RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0014.req.Xdcz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0014.resp.Xdcz0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:企业网银查询影像补录批次
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0014:企业网银出票申请额度校验")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0014Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0014
     * 交易描述：企业网银查询影像补录批次
     *
     * @param xdcz0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("企业网银查询影像补录批次")
    @PostMapping("/xdcz0014")
   //@Idempotent({"xdcz0014", "#xdcz0014ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0014RespDto xdcz0014(@Validated @RequestBody Xdcz0014ReqDto xdcz0014ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014ReqDto));
        Xdcz0014DataReqDto xdcz0014DataReqDto = new Xdcz0014DataReqDto();// 请求Data： 电子保函开立
        Xdcz0014DataRespDto xdcz0014DataRespDto = new Xdcz0014DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0014.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0014RespDto xdcz0014RespDto = new Xdcz0014RespDto();
        try {
            // 从 xdcz0014ReqDto获取 reqData
            reqData = xdcz0014ReqDto.getData();
            // 将 reqData 拷贝到xdcz0014DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0014DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014DataReqDto));
            ResultDto<Xdcz0014DataRespDto> xdcz0014DataResultDto = dscmsBizCzClientService.xdcz0014(xdcz0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0014RespDto.setErorcd(Optional.ofNullable(xdcz0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0014RespDto.setErortx(Optional.ofNullable(xdcz0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0014DataResultDto.getCode())) {
                xdcz0014DataRespDto = xdcz0014DataResultDto.getData();
                xdcz0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0014DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, e.getMessage());
            xdcz0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0014RespDto.setDatasq(servsq);

        xdcz0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0014.key, DscmsEnum.TRADE_CODE_XDCZ0014.value, JSON.toJSONString(xdcz0014RespDto));
        return xdcz0014RespDto;
    }
}
