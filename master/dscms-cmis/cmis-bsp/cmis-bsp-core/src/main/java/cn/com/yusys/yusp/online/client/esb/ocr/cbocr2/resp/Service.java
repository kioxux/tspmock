package cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.resp;


/**
 * 响应Service：识别任务的模板匹配结果列表查询
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
	
    private String erorcd;//响应代码
    
    private String erortx;//响应信息

    private String success;//结果消息
    
    private Data list;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

	public Data getList() {
		return list;
	}

	public void setList(Data list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "Service [erorcd=" + erorcd + ", erortx=" + erortx
				+ ", success=" + success + ", list=" + list + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((erorcd == null) ? 0 : erorcd.hashCode());
		result = prime * result + ((erortx == null) ? 0 : erortx.hashCode());
		result = prime * result + ((success == null) ? 0 : success.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Service other = (Service) obj;
		if (erorcd == null) {
			if (other.erorcd != null)
				return false;
		} else if (!erorcd.equals(other.erorcd))
			return false;
		if (erortx == null) {
			if (other.erortx != null)
				return false;
		} else if (!erortx.equals(other.erortx))
			return false;
		if (success == null) {
			if (other.success != null)
				return false;
		} else if (!success.equals(other.success))
			return false;
		return true;
	}
    
}
