package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsfsj;

import java.math.BigDecimal;

/**
 * 响应Service：贷款产品收费事件定义属性对象
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String shoufshj;//收费事件
    private String shfshjmc;//收费事件名称
    private String shoufzhl;//收费种类
    private String shoufdma;//收费代码
    private String shfdmamc;//收费代码名称
    private BigDecimal shoufjee;//收费金额/比例

    public String getShoufshj() {
        return shoufshj;
    }

    public void setShoufshj(String shoufshj) {
        this.shoufshj = shoufshj;
    }

    public String getShfshjmc() {
        return shfshjmc;
    }

    public void setShfshjmc(String shfshjmc) {
        this.shfshjmc = shfshjmc;
    }

    public String getShoufzhl() {
        return shoufzhl;
    }

    public void setShoufzhl(String shoufzhl) {
        this.shoufzhl = shoufzhl;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    @Override
    public String toString() {
        return "Record{" +
                "shoufshj='" + shoufshj + '\'' +
                "shfshjmc='" + shfshjmc + '\'' +
                "shoufzhl='" + shoufzhl + '\'' +
                "shoufdma='" + shoufdma + '\'' +
                "shfdmamc='" + shfdmamc + '\'' +
                "shoufjee='" + shoufjee + '\'' +
                '}';
    }
}
