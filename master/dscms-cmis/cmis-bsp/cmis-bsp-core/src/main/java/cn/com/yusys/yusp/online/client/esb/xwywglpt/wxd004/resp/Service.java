package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd004.resp;

/**
 * 响应Service：信贷系统获取征信报送监管信息接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String reqid;//系统流水号
    private String sexcode;//性别
    private String borndate;//出生日期
    private String marryinfocode;//婚姻状况
    private String educode;//文化程度
    private String conaddress;//通讯地址
    private String postcode;//邮政编码
    private String positionworkcode;//从事职业
    private String comname;//工作单位
    private String positioncode;//职务
    private String houseaddress;//居住地址
    private String housepost;//居住地址邮编
    private String houseinfocode;//居住状况
    private String areacode;//区域编号
    private String sfnhcode;//是否农户
    private String issuccess;//是否加工完成

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReqid() {
        return reqid;
    }

    public void setReqid(String reqid) {
        this.reqid = reqid;
    }

    public String getSexcode() {
        return sexcode;
    }

    public void setSexcode(String sexcode) {
        this.sexcode = sexcode;
    }

    public String getBorndate() {
        return borndate;
    }

    public void setBorndate(String borndate) {
        this.borndate = borndate;
    }

    public String getMarryinfocode() {
        return marryinfocode;
    }

    public void setMarryinfocode(String marryinfocode) {
        this.marryinfocode = marryinfocode;
    }

    public String getEducode() {
        return educode;
    }

    public void setEducode(String educode) {
        this.educode = educode;
    }

    public String getConaddress() {
        return conaddress;
    }

    public void setConaddress(String conaddress) {
        this.conaddress = conaddress;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getPositionworkcode() {
        return positionworkcode;
    }

    public void setPositionworkcode(String positionworkcode) {
        this.positionworkcode = positionworkcode;
    }

    public String getComname() {
        return comname;
    }

    public void setComname(String comname) {
        this.comname = comname;
    }

    public String getPositioncode() {
        return positioncode;
    }

    public void setPositioncode(String positioncode) {
        this.positioncode = positioncode;
    }

    public String getHouseaddress() {
        return houseaddress;
    }

    public void setHouseaddress(String houseaddress) {
        this.houseaddress = houseaddress;
    }

    public String getHousepost() {
        return housepost;
    }

    public void setHousepost(String housepost) {
        this.housepost = housepost;
    }

    public String getHouseinfocode() {
        return houseinfocode;
    }

    public void setHouseinfocode(String houseinfocode) {
        this.houseinfocode = houseinfocode;
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode;
    }

    public String getSfnhcode() {
        return sfnhcode;
    }

    public void setSfnhcode(String sfnhcode) {
        this.sfnhcode = sfnhcode;
    }

    public String getIssuccess() {
        return issuccess;
    }

    public void setIssuccess(String issuccess) {
        this.issuccess = issuccess;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "reqid='" + reqid + '\'' +
                "sexcode ='" + sexcode + '\'' +
                "borndate ='" + borndate + '\'' +
                "marryinfocode='" + marryinfocode + '\'' +
                "educode='" + educode + '\'' +
                "conaddress ='" + conaddress + '\'' +
                "postcode ='" + postcode + '\'' +
                "positionworkcode='" + positionworkcode + '\'' +
                "comname='" + comname + '\'' +
                "positioncode='" + positioncode + '\'' +
                "houseaddress ='" + houseaddress + '\'' +
                "housepost='" + housepost + '\'' +
                "houseinfocode='" + houseinfocode + '\'' +
                "areacode='" + areacode + '\'' +
                "sfnhcode ='" + sfnhcode + '\'' +
                "issuccess ='" + issuccess + '\'' +
                '}';
    }
}
