package cn.com.yusys.yusp.online.client.esb.irs.irs21.req;


/**
 * 请求Service：单一客户限额测算信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Irs21ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
