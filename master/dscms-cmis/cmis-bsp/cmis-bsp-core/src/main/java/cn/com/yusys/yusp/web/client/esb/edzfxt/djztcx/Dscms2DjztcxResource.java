package cn.com.yusys.yusp.web.client.esb.edzfxt.djztcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.req.DjztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.resp.DjztcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.req.DjztcxReqService;
import cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.resp.DjztcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用二代支付系统的接口处理类（djztcx）")
@RestController
@RequestMapping("/api/dscms2edzfxt")
public class Dscms2DjztcxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2DjztcxResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：djztcx
     * 交易描述：贷记入账状态查询申请往帐
     *
     * @param djztcxReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/djztcx")
    protected @ResponseBody
    ResultDto<DjztcxRespDto> djztcx(@Validated @RequestBody DjztcxReqDto djztcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJZTCX.key, EsbEnum.TRADE_CODE_DJZTCX.value, JSON.toJSONString(djztcxReqDto));
        cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.resp.Service();
        DjztcxReqService djztcxReqService = new DjztcxReqService();
        DjztcxRespService djztcxRespService = new DjztcxRespService();
        DjztcxRespDto djztcxRespDto = new DjztcxRespDto();
        ResultDto<DjztcxRespDto> djztcxResultDto = new ResultDto<DjztcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将djztcxReqDto转换成reqService
            BeanUtils.copyProperties(djztcxReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DJZTCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_ZFP.key);//    柜员号
            reqService.setBrchno(djztcxReqDto.getBrchno());//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            djztcxReqService.setService(reqService);
            // 将djztcxReqService转换成djztcxReqServiceMap
            Map djztcxReqServiceMap = beanMapUtil.beanToMap(djztcxReqService);
            context.put("tradeDataMap", djztcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJZTCX.key, EsbEnum.TRADE_CODE_DJZTCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DJZTCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJZTCX.key, EsbEnum.TRADE_CODE_DJZTCX.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            djztcxRespService = beanMapUtil.mapToBean(tradeDataMap, DjztcxRespService.class, DjztcxRespService.class);
            respService = djztcxRespService.getService();

            djztcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            djztcxResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成DjztcxRespDto
                BeanUtils.copyProperties(respService, djztcxRespDto);
                djztcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                djztcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                djztcxResultDto.setCode(EpbEnum.EPB099999.key);
                djztcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJZTCX.key, EsbEnum.TRADE_CODE_DJZTCX.value, e.getMessage());
            djztcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            djztcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        djztcxResultDto.setData(djztcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DJZTCX.key, EsbEnum.TRADE_CODE_DJZTCX.value, JSON.toJSONString(djztcxResultDto));
        return djztcxResultDto;
    }
}
