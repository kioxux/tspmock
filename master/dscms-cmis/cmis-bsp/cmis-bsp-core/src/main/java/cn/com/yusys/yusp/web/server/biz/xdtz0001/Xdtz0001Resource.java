package cn.com.yusys.yusp.web.server.biz.xdtz0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0001.req.Xdtz0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0001.resp.Xdtz0001RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0001.req.Xdtz0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0001.resp.Xdtz0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户信息查询(贷款信息)
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDTZ0001:客户信息查询(贷款信息)")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0001Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0001
     * 交易描述：客户信息查询(贷款信息)
     *
     * @param xdtz0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户信息查询(贷款信息)")
    @PostMapping("/xdtz0001")
    //@Idempotent({"xdcatz0001", "#xdtz0001ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0001RespDto xdtz0001(@Validated @RequestBody Xdtz0001ReqDto xdtz0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001ReqDto));
        Xdtz0001DataReqDto xdtz0001DataReqDto = new Xdtz0001DataReqDto();// 请求Data： 客户信息查询(贷款信息)
        Xdtz0001DataRespDto xdtz0001DataRespDto = new Xdtz0001DataRespDto();// 响应Data：客户信息查询(贷款信息)
        Xdtz0001RespDto xdtz0001RespDto = new Xdtz0001RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0001.req.Data reqData = null; // 请求Data：客户信息查询(贷款信息)
        cn.com.yusys.yusp.dto.server.biz.xdtz0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0001.resp.Data();// 响应Data：客户信息查询(贷款信息)
        try {
            // 从 xdtz0001ReqDto获取 reqData
            reqData = xdtz0001ReqDto.getData();
            // 将 reqData 拷贝到xdtz0001DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataReqDto));
            ResultDto<Xdtz0001DataRespDto> xdtz0001DataResultDto = dscmsBizTzClientService.xdtz0001(xdtz0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001DataResultDto));

            // 从返回值中获取响应码和响应消息
            xdtz0001RespDto.setErorcd(Optional.ofNullable(xdtz0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0001RespDto.setErortx(Optional.ofNullable(xdtz0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0001DataResultDto.getCode())) {
                xdtz0001DataRespDto = xdtz0001DataResultDto.getData();
                xdtz0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, e.getMessage());
            xdtz0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0001RespDto.setDatasq(servsq);

        xdtz0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0001.key, DscmsEnum.TRADE_CODE_XDTZ0001.value, JSON.toJSONString(xdtz0001RespDto));
        return xdtz0001RespDto;
    }
}