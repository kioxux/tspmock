package cn.com.yusys.yusp.web.server.biz.xdxw0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0012.req.Xdxw0012ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0012.resp.Xdxw0012RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0012.req.Xdxw0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0012.resp.Xdxw0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小贷授信申请文本生成pdf
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0012:小贷授信申请文本生成pdf")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0012Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0012
     * 交易描述：小贷授信申请文本生成pdf
     *
     * @param xdxw0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小贷授信申请文本生成pdf")
    @PostMapping("/xdxw0012")
//    @Idempotent({"xdcaxw0012", "#xdxw0012ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0012RespDto xdxw0012(@Validated @RequestBody Xdxw0012ReqDto xdxw0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012ReqDto));
        Xdxw0012DataReqDto xdxw0012DataReqDto = new Xdxw0012DataReqDto();// 请求Data： 小贷授信申请文本生成pdf
        Xdxw0012DataRespDto xdxw0012DataRespDto = new Xdxw0012DataRespDto();// 响应Data：小贷授信申请文本生成pdf
		Xdxw0012RespDto xdxw0012RespDto=new Xdxw0012RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0012.req.Data reqData = null; // 请求Data：小贷授信申请文本生成pdf
        cn.com.yusys.yusp.dto.server.biz.xdxw0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0012.resp.Data();// 响应Data：小贷授信申请文本生成pdf
        try {
            // 从 xdxw0012ReqDto获取 reqData
            reqData = xdxw0012ReqDto.getData();
            // 将 reqData 拷贝到xdxw0012DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0012DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012DataReqDto));
            ResultDto<Xdxw0012DataRespDto> xdxw0012DataResultDto = dscmsBizXwClientService.xdxw0012(xdxw0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0012RespDto.setErorcd(Optional.ofNullable(xdxw0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0012RespDto.setErortx(Optional.ofNullable(xdxw0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0012DataResultDto.getCode())) {
                xdxw0012DataRespDto = xdxw0012DataResultDto.getData();
                xdxw0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, e.getMessage());
            xdxw0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0012RespDto.setDatasq(servsq);

        xdxw0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0012.key, DscmsEnum.TRADE_CODE_XDXW0012.value, JSON.toJSONString(xdxw0012RespDto));
        return xdxw0012RespDto;
    }
}
