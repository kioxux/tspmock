package cn.com.yusys.yusp.online.client.gxp.common.req;

/**
 * GXP请求Head
 *
 * @author leehuang
 * @version 1.0
 * @since 2021/5/25 21:56
 */
public class GxpReqHead {
    private String prcscd;//业务处理码 长度一般为6位，且是字母开头，一般字母为小写
    private String servtp;//渠道
    private String transq;//交易流水号
    private String userid;//柜员号
    private String brchno;//机构号
    private String trandt;//交易日期 YYYYMMDD
    private String tranti;//交易时间 HHmmss

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getTransq() {
        return transq;
    }

    public void setTransq(String transq) {
        this.transq = transq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getTrandt() {
        return trandt;
    }

    public void setTrandt(String trandt) {
        this.trandt = trandt;
    }

    public String getTranti() {
        return tranti;
    }

    public void setTranti(String tranti) {
        this.tranti = tranti;
    }

    @Override
    public String toString() {
        return "GxpReqHead{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", transq='" + transq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", trandt='" + trandt + '\'' +
                ", tranti='" + tranti + '\'' +
                '}';
    }
}
