package cn.com.yusys.yusp.web.client.esb.core.ln3161;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3161.Ln3161ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3161.Ln3161RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3161.req.Ln3161ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3161.resp.Ln3161RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3161)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3161Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3161Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3161
     * 交易描述：资产证券化协议登记
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3161:资产证券化协议登记")
    @PostMapping("/ln3161")
    protected @ResponseBody
    ResultDto<Ln3161RespDto> ln3161(@Validated @RequestBody Ln3161ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3161.key, EsbEnum.TRADE_CODE_LN3161.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3161.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3161.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3161.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3161.resp.Service();
        Ln3161ReqService ln3161ReqService = new Ln3161ReqService();
        Ln3161RespService ln3161RespService = new Ln3161RespService();
        Ln3161RespDto ln3161RespDto = new Ln3161RespDto();
        ResultDto<Ln3161RespDto> ln3161ResultDto = new ResultDto<Ln3161RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3161ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3161.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3161ReqService.setService(reqService);
            // 将ln3161ReqService转换成ln3161ReqServiceMap
            Map ln3161ReqServiceMap = beanMapUtil.beanToMap(ln3161ReqService);
            context.put("tradeDataMap", ln3161ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3161.key, EsbEnum.TRADE_CODE_LN3161.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3161.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3161.key, EsbEnum.TRADE_CODE_LN3161.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3161RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3161RespService.class, Ln3161RespService.class);
            respService = ln3161RespService.getService();

            ln3161ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3161ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3161RespDto
                BeanUtils.copyProperties(respService, ln3161RespDto);
                ln3161ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3161ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3161ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3161ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3161.key, EsbEnum.TRADE_CODE_LN3161.value, e.getMessage());
            ln3161ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3161ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3161ResultDto.setData(ln3161RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3161.key, EsbEnum.TRADE_CODE_LN3161.value, JSON.toJSONString(ln3161ResultDto));
        return ln3161ResultDto;
    }
}
