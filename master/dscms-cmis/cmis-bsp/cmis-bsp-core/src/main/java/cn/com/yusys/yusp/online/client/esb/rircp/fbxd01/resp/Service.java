package cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.resp;

/**
 * 响应Service：惠享贷授信申请件数取得
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码
    private String erortx; // 响应信息

    private String crd_app_count;//授信申请件数

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCrd_app_count() {
        return crd_app_count;
    }

    public void setCrd_app_count(String crd_app_count) {
        this.crd_app_count = crd_app_count;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", crd_app_count='" + crd_app_count + '\'' +
                '}';
    }
}
