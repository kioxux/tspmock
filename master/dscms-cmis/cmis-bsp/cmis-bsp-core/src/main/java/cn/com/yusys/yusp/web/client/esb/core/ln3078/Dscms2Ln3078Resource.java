package cn.com.yusys.yusp.web.client.esb.core.ln3078;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3078.Ln3078ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3078.Ln3078RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3078.req.Ln3078ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.Ln3078RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3078)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3078Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3078Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3078
     * 交易描述：贷款机构变更
     *
     * @param ln3078ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3078:贷款机构变更")
    @PostMapping("/ln3078")
    protected @ResponseBody
    ResultDto<Ln3078RespDto> ln3078(@Validated @RequestBody Ln3078ReqDto ln3078ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3078.key, EsbEnum.TRADE_CODE_LN3078.value, JSON.toJSONString(ln3078ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3078.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3078.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.Service();

        Ln3078ReqService ln3078ReqService = new Ln3078ReqService();
        Ln3078RespService ln3078RespService = new Ln3078RespService();
        Ln3078RespDto ln3078RespDto = new Ln3078RespDto();
        ResultDto<Ln3078RespDto> ln3078ResultDto = new ResultDto<Ln3078RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3078ReqDto转换成reqService
            BeanUtils.copyProperties(ln3078ReqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(ln3078ReqDto.getLstjiejxx())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3078.Lstjiejxx> ln3078ReqLstjiejxxDtoList = Optional.ofNullable(ln3078ReqDto.getLstjiejxx())
                        .orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.List ln3078ReqLstjiejxxList
                        = Optional.ofNullable(reqService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.List());
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.Record> ln3078ReqLstjiejxxRecords = ln3078ReqLstjiejxxDtoList.parallelStream()
                        .map(lstjiejxx -> {
                            cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.Record ln3078ReqLstjiejxxRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3078.req.lstjiejxx.Record();
                            BeanUtils.copyProperties(lstjiejxx, ln3078ReqLstjiejxxRecord);
                            return ln3078ReqLstjiejxxRecord;
                        }).collect(Collectors.toList());
                ln3078ReqLstjiejxxList.setRecord(ln3078ReqLstjiejxxRecords);
                reqService.setList(ln3078ReqLstjiejxxList);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3078.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3078ReqService.setService(reqService);
            // 将ln3078ReqService转换成ln3078ReqServiceMap
            Map ln3078ReqServiceMap = beanMapUtil.beanToMap(ln3078ReqService);
            context.put("tradeDataMap", ln3078ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3078.key, EsbEnum.TRADE_CODE_LN3078.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3078.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3078.key, EsbEnum.TRADE_CODE_LN3078.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3078RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3078RespService.class, Ln3078RespService.class);
            respService = ln3078RespService.getService();

            ln3078ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3078ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3078RespDto
                BeanUtils.copyProperties(respService, ln3078RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.outjiejxx.List outjiejxxList = Optional.ofNullable(respService.getList())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.outjiejxx.List());
                respService.setList(outjiejxxList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.outjiejxx.Record> recordList = Optional
                            .ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3078.Outjiejxx> outjiejxxDtoList = recordList.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3078.Outjiejxx outjiejxx = new cn.com.yusys.yusp.dto.client.esb.core.ln3078.Outjiejxx();
                        BeanUtils.copyProperties(record, outjiejxx);
                        return outjiejxx;
                    }).collect(Collectors.toList());
                    ln3078RespDto.setOutjiejxx(outjiejxxDtoList);
                }
                ln3078ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3078ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3078ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3078ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3078.key, EsbEnum.TRADE_CODE_LN3078.value, e.getMessage());
            ln3078ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3078ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3078ResultDto.setData(ln3078RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3078.key, EsbEnum.TRADE_CODE_LN3078.value, JSON.toJSONString(ln3078ResultDto));
        return ln3078ResultDto;
    }
}
