package cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh;

import java.util.List;

/**
 * 响应Service：还款方式转换
 *
 * @author leehuang
 * @version 1.0
 */
public class Lsdkhbjh {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lsdkhbjh{" +
                "record=" + record +
                '}';
    }
}
