package cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp;

/**
 * 响应Service：大额现金分期申请
 * @author chenyong
 * @version 1.0
 */
public class D13160RespMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D13160RespMessage{" +
                "message=" + message +
                '}';
    }
}

