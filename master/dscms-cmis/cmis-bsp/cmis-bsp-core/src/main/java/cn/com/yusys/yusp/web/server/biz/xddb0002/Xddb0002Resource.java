package cn.com.yusys.yusp.web.server.biz.xddb0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0002.req.Xddb0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0002.resp.Xddb0002RespDto;
import cn.com.yusys.yusp.dto.server.xddb0002.req.Xddb0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0002.resp.Xddb0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品状态查询
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDDB0002:押品状态查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0002Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizdbClientService;

    /**
     * 交易码：xddb0002
     * 交易描述：押品状态查询
     *
     * @param xddb0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品状态查询")
    @PostMapping("/xddb0002")
    //@Idempotent({"xddb0002", "#xddb0002ReqDto.datasq"})
    protected @ResponseBody
    Xddb0002RespDto xddb0002(@Validated @RequestBody Xddb0002ReqDto xddb0002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, JSON.toJSONString(xddb0002ReqDto));
        Xddb0002DataReqDto xddb0002DataReqDto = new Xddb0002DataReqDto();// 请求Data： 押品状态查询
        Xddb0002DataRespDto xddb0002DataRespDto = new Xddb0002DataRespDto();// 响应Data：押品状态查询
        Xddb0002RespDto xddb0002RespDto = new Xddb0002RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddb0002.req.Data reqData = null; // 请求Data：押品状态查询
        cn.com.yusys.yusp.dto.server.biz.xddb0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0002.resp.Data();
        // 响应Data：押品状态查询
        try {
            // 从 xddb0002ReqDto获取 reqData
            reqData = xddb0002ReqDto.getData();
            // 将 reqData 拷贝到xddb0002DataReqDto
            BeanUtils.copyProperties(reqData, xddb0002DataReqDto);
            // 调用服务：
            logger.info("**********押品状态查询调用Biz接口服务开始*************");
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, JSON.toJSONString(xddb0002ReqDto));
            ResultDto<Xddb0002DataRespDto> xddb0002DataResultDto = dscmsBizdbClientService.xddb0002(xddb0002DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, JSON.toJSONString(xddb0002DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0002RespDto.setErorcd(Optional.ofNullable(xddb0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0002RespDto.setErortx(Optional.ofNullable(xddb0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            logger.info("******押品状态查询调用Biz接口服务结束,处理返回信息**********");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0002DataResultDto.getCode())) {
                xddb0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xddb0002DataRespDto = xddb0002DataResultDto.getData();
                // 将 xddb0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, e.getMessage());
            xddb0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0002RespDto.setDatasq(servsq);

        xddb0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0002.key, DscmsEnum.TRADE_CODE_XDDB0002.value, JSON.toJSONString(xddb0002RespDto));
        return xddb0002RespDto;
    }
}
