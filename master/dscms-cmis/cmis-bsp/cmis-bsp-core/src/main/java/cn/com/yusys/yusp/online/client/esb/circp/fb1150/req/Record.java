package cn.com.yusys.yusp.online.client.esb.circp.fb1150.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/11 10:57
 * @since 2021/8/11 10:57
 */
public class Record {
    private String shareholder;//股东名称

    public String getShareholder() {
        return shareholder;
    }

    public void setShareholder(String shareholder) {
        this.shareholder = shareholder;
    }

    @Override
    public String toString() {
        return "Record{" +
                "shareholder='" + shareholder + '\'' +
                '}';
    }
}
