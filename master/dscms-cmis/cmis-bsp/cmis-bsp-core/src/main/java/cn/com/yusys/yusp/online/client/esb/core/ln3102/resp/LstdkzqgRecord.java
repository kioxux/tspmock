package cn.com.yusys.yusp.online.client.esb.core.ln3102.resp;

import java.math.BigDecimal;

/**
 * 贷款账户期供
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class LstdkzqgRecord {
    private Integer benqqish;//本期期数
    private Integer benqizqs;//本期子期数
    private String qishriqi;//起始日期
    private String zhzhriqi;//终止日期
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal chushibj;//初始本金
    private BigDecimal chushilx;//初始利息
    private String kxqdqirq;//宽限期到期日
    private BigDecimal benjinnn;//本金
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal csyjlixi;//催收应计利息
    private BigDecimal ysqianxi;//应收欠息
    private BigDecimal csqianxi;//催收欠息
    private BigDecimal ysyjfaxi;//应收应计罚息
    private BigDecimal csyjfaxi;//催收应计罚息
    private BigDecimal yshofaxi;//应收罚息
    private BigDecimal cshofaxi;//催收罚息
    private BigDecimal yingjifx;//应计复息
    private BigDecimal fuxiiiii;//复息
    private String schkriqi;//上次还款日
    private String qigengzl;//期供种类
    private String benqizht;//本期状态
    private String yjfyjzht;//应计非应计状态
    private BigDecimal zhanghye;//账户余额
    private BigDecimal yingjitx;//应计贴息
    private BigDecimal yingshtx;//应收贴息

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public String getKxqdqirq() {
        return kxqdqirq;
    }

    public void setKxqdqirq(String kxqdqirq) {
        this.kxqdqirq = kxqdqirq;
    }

    public BigDecimal getBenjinnn() {
        return benjinnn;
    }

    public void setBenjinnn(BigDecimal benjinnn) {
        this.benjinnn = benjinnn;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public String getQigengzl() {
        return qigengzl;
    }

    public void setQigengzl(String qigengzl) {
        this.qigengzl = qigengzl;
    }

    public String getBenqizht() {
        return benqizht;
    }

    public void setBenqizht(String benqizht) {
        this.benqizht = benqizht;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public BigDecimal getYingjitx() {
        return yingjitx;
    }

    public void setYingjitx(BigDecimal yingjitx) {
        this.yingjitx = yingjitx;
    }

    public BigDecimal getYingshtx() {
        return yingshtx;
    }

    public void setYingshtx(BigDecimal yingshtx) {
        this.yingshtx = yingshtx;
    }

    @Override
    public String toString() {
        return "Lstdkzqg{" +
                "benqqish='" + benqqish + '\'' +
                "benqizqs='" + benqizqs + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "zhzhriqi='" + zhzhriqi + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "chushibj='" + chushibj + '\'' +
                "chushilx='" + chushilx + '\'' +
                "kxqdqirq='" + kxqdqirq + '\'' +
                "benjinnn='" + benjinnn + '\'' +
                "ysyjlixi='" + ysyjlixi + '\'' +
                "csyjlixi='" + csyjlixi + '\'' +
                "ysqianxi='" + ysqianxi + '\'' +
                "csqianxi='" + csqianxi + '\'' +
                "ysyjfaxi='" + ysyjfaxi + '\'' +
                "csyjfaxi='" + csyjfaxi + '\'' +
                "yshofaxi='" + yshofaxi + '\'' +
                "cshofaxi='" + cshofaxi + '\'' +
                "yingjifx='" + yingjifx + '\'' +
                "fuxiiiii='" + fuxiiiii + '\'' +
                "schkriqi='" + schkriqi + '\'' +
                "qigengzl='" + qigengzl + '\'' +
                "benqizht='" + benqizht + '\'' +
                "yjfyjzht='" + yjfyjzht + '\'' +
                "zhanghye='" + zhanghye + '\'' +
                "yingjitx='" + yingjitx + '\'' +
                "yingshtx='" + yingshtx + '\'' +
                '}';
    }
}
