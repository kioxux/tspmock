package cn.com.yusys.yusp.online.client.esb.circp.fb1205.req;

/**
 * 请求Service：放款信息推送
 *
 * @author code-generator
 * @version 1.0
 */
public class Fb1205ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
