package cn.com.yusys.yusp.web.server.oca.xdxt0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.oca.xdxt0010.req.Xdxt0010ReqDto;
import cn.com.yusys.yusp.dto.server.oca.xdxt0010.resp.Xdxt0010RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.req.Xdxt0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0010.resp.Xdxt0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户经理名单，包括工号、姓名
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0010:查询客户经理名单，包括工号、姓名")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0010Resource.class);
    @Autowired
    private DscmsBizXtClientService dscmsBizXtClientService;

    /**
     * 交易码：xdxt0010
     * 交易描述：查询客户经理名单，包括工号、姓名
     *
     * @param xdxt0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户经理名单，包括工号、姓名")
    @PostMapping("/xdxt0010")
    //@Idempotent({"xdcaxt0010", "#xdxt0010ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0010RespDto xdxt0010(@Validated @RequestBody Xdxt0010ReqDto xdxt0010ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010ReqDto));
        Xdxt0010DataReqDto xdxt0010DataReqDto = new Xdxt0010DataReqDto();// 请求Data： 查询客户经理名单，包括工号、姓名
        Xdxt0010DataRespDto xdxt0010DataRespDto = new Xdxt0010DataRespDto();// 响应Data：查询客户经理名单，包括工号、姓名
        Xdxt0010RespDto xdxt0010RespDto = new Xdxt0010RespDto();
        cn.com.yusys.yusp.dto.server.oca.xdxt0010.req.Data reqData = null; // 请求Data：查询客户经理名单，包括工号、姓名
        cn.com.yusys.yusp.dto.server.oca.xdxt0010.resp.Data respData = new cn.com.yusys.yusp.dto.server.oca.xdxt0010.resp.Data();// 响应Data：查询客户经理名单，包括工号、姓名

        try {
            // 从 xdxt0010ReqDto获取 reqData
            reqData = xdxt0010ReqDto.getData();
            // 将 reqData 拷贝到xdxt0010DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataReqDto));
            ResultDto<Xdxt0010DataRespDto> xdxt0010DataResultDto = dscmsBizXtClientService.xdxt0010(xdxt0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0010RespDto.setErorcd(Optional.ofNullable(xdxt0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0010RespDto.setErortx(Optional.ofNullable(xdxt0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0010DataResultDto.getCode())) {
                xdxt0010DataRespDto = xdxt0010DataResultDto.getData();
                xdxt0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0010DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0010DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, e.getMessage());
            xdxt0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0010RespDto.setDatasq(servsq);

        xdxt0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0010.key, DscmsEnum.TRADE_CODE_XDXT0010.value, JSON.toJSONString(xdxt0010RespDto));
        return xdxt0010RespDto;
    }
}
