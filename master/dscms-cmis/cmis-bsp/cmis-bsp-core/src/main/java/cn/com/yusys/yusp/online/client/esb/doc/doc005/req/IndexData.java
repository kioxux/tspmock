package cn.com.yusys.yusp.online.client.esb.doc.doc005.req;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/23 14:58
 * @since 2021/6/23 14:58
 */
public class IndexData {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.doc.doc005.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "IndexData{" +
                "record=" + record +
                '}';
    }
}
