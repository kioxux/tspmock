package cn.com.yusys.yusp.online.client.esb.core.mbt952.req;

/**
 * 请求Service：批量结果确认处理
 */
public class Service {

    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String jysjzifu;//交易时间字符串
    private String picriqii;//批次日期
    private String picihaoo;//批次号
    private String picileib;//批次类别
    private String fuwudaim;//服务代码
    private String ypiciriq;//原批次日期
    private String ypicihao;//原批次号
    private String weituoha;//委托号
    private String xieyihao;//合同号/协议号
    private String qianyzhh;//签约账号
    private String zhixztai;//执行状态
    private String qishiriq;//起始日期
    private String zhngzhrq;//终止日期
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getJysjzifu() {
        return jysjzifu;
    }

    public void setJysjzifu(String jysjzifu) {
        this.jysjzifu = jysjzifu;
    }

    public String getPicriqii() {
        return picriqii;
    }

    public void setPicriqii(String picriqii) {
        this.picriqii = picriqii;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public String getPicileib() {
        return picileib;
    }

    public void setPicileib(String picileib) {
        this.picileib = picileib;
    }

    public String getFuwudaim() {
        return fuwudaim;
    }

    public void setFuwudaim(String fuwudaim) {
        this.fuwudaim = fuwudaim;
    }

    public String getYpiciriq() {
        return ypiciriq;
    }

    public void setYpiciriq(String ypiciriq) {
        this.ypiciriq = ypiciriq;
    }

    public String getYpicihao() {
        return ypicihao;
    }

    public void setYpicihao(String ypicihao) {
        this.ypicihao = ypicihao;
    }

    public String getWeituoha() {
        return weituoha;
    }

    public void setWeituoha(String weituoha) {
        this.weituoha = weituoha;
    }

    public String getXieyihao() {
        return xieyihao;
    }

    public void setXieyihao(String xieyihao) {
        this.xieyihao = xieyihao;
    }

    public String getQianyzhh() {
        return qianyzhh;
    }

    public void setQianyzhh(String qianyzhh) {
        this.qianyzhh = qianyzhh;
    }

    public String getZhixztai() {
        return zhixztai;
    }

    public void setZhixztai(String zhixztai) {
        this.zhixztai = zhixztai;
    }

    public String getQishiriq() {
        return qishiriq;
    }

    public void setQishiriq(String qishiriq) {
        this.qishiriq = qishiriq;
    }

    public String getZhngzhrq() {
        return zhngzhrq;
    }

    public void setZhngzhrq(String zhngzhrq) {
        this.zhngzhrq = zhngzhrq;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", jysjzifu='" + jysjzifu + '\'' +
                ", picriqii='" + picriqii + '\'' +
                ", picihaoo='" + picihaoo + '\'' +
                ", picileib='" + picileib + '\'' +
                ", fuwudaim='" + fuwudaim + '\'' +
                ", ypiciriq='" + ypiciriq + '\'' +
                ", ypicihao='" + ypicihao + '\'' +
                ", weituoha='" + weituoha + '\'' +
                ", xieyihao='" + xieyihao + '\'' +
                ", qianyzhh='" + qianyzhh + '\'' +
                ", zhixztai='" + zhixztai + '\'' +
                ", qishiriq='" + qishiriq + '\'' +
                ", zhngzhrq='" + zhngzhrq + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                '}';
    }
}
