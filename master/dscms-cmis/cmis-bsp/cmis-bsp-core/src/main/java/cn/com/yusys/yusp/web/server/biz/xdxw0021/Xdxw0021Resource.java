package cn.com.yusys.yusp.web.server.biz.xdxw0021;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0021.req.Xdxw0021ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0021.resp.Xdxw0021RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0021.req.Xdxw0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0021.resp.Xdxw0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0021:根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0021Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0021
     * 交易描述：根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
     *
     * @param xdxw0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）")
    @PostMapping("/xdxw0021")
    //@Idempotent({"xdcaxw0021", "#xdxw0021ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0021RespDto xdxw0021(@Validated @RequestBody Xdxw0021ReqDto xdxw0021ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021ReqDto));
        Xdxw0021DataReqDto xdxw0021DataReqDto = new Xdxw0021DataReqDto();// 请求Data： 根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
        Xdxw0021DataRespDto xdxw0021DataRespDto = new Xdxw0021DataRespDto();// 响应Data：根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
		Xdxw0021RespDto xdxw0021RespDto=new Xdxw0021RespDto();

		cn.com.yusys.yusp.dto.server.biz.xdxw0021.req.Data reqData = null; // 请求Data：根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）
		cn.com.yusys.yusp.dto.server.biz.xdxw0021.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0021.resp.Data();// 响应Data：根据调查流水号查询当前业务状态（合同状态、放款状态、借据状态）

        try {
            // 从 xdxw0021ReqDto获取 reqData
            reqData = xdxw0021ReqDto.getData();
            // 将 reqData 拷贝到xdxw0021DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0021DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021DataReqDto));
            ResultDto<Xdxw0021DataRespDto> xdxw0021DataResultDto = dscmsBizXwClientService.xdxw0021(xdxw0021DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0021RespDto.setErorcd(Optional.ofNullable(xdxw0021DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0021RespDto.setErortx(Optional.ofNullable(xdxw0021DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0021DataResultDto.getCode())) {
                xdxw0021DataRespDto = xdxw0021DataResultDto.getData();
                xdxw0021RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0021RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0021DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, e.getMessage());
            xdxw0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0021RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0021RespDto.setDatasq(servsq);

        xdxw0021RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0021.key, DscmsEnum.TRADE_CODE_XDXW0021.value, JSON.toJSONString(xdxw0021RespDto));
        return xdxw0021RespDto;
    }
}
