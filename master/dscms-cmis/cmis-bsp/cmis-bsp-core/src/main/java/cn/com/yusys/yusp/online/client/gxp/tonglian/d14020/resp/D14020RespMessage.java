package cn.com.yusys.yusp.online.client.gxp.tonglian.d14020.resp;

import cn.com.yusys.yusp.online.client.gxp.tonglian.d14020.resp.Message;

/**
 * 响应Message：永久额度调整
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class D14020RespMessage {
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D14020RespMessage{" +
                "message=" + message +
                '}';
    }
}
