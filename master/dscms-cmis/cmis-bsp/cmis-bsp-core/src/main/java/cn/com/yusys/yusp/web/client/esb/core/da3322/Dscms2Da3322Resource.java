package cn.com.yusys.yusp.web.client.esb.core.da3322;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3322.req.Da3322ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.Da3322RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3322.req.Da3322ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3322.resp.*;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(da3322)")
@RestController
@RequestMapping("/api/dscms2coreda")
public class Dscms2Da3322Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Da3322Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 抵债资产费用管理（处理码da3322）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("da3322:抵债资产费用管理")
    @PostMapping("/da3322")
    protected @ResponseBody
    ResultDto<Da3322RespDto> da3322(@Validated @RequestBody Da3322ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3322.key, EsbEnum.TRADE_CODE_DA3322.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3322.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3322.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Service();
        Da3322ReqService da3322ReqService = new Da3322ReqService();
        Da3322RespService da3322RespService = new Da3322RespService();
        Da3322RespDto da3322RespDto = new Da3322RespDto();
        ResultDto<Da3322RespDto> da3322ResultDto = new ResultDto<Da3322RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Da3322ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3322.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            da3322ReqService.setService(reqService);
            // 将da3322ReqService转换成da3322ReqServiceMap
            Map da3322ReqServiceMap = beanMapUtil.beanToMap(da3322ReqService);
            context.put("tradeDataMap", da3322ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3322.key, EsbEnum.TRADE_CODE_DA3322.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3322.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3322.key, EsbEnum.TRADE_CODE_DA3322.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3322RespService = beanMapUtil.mapToBean(tradeDataMap, Da3322RespService.class, Da3322RespService.class);
            respService = da3322RespService.getService();

            //  将Da3322RespDto封装到ResultDto<Da3322RespDto>
            da3322ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            da3322ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3322RespDto
                BeanUtils.copyProperties(respService, da3322RespDto);
                cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Listnm0 listnm0 = Optional.ofNullable(respService.getListnm0()).orElse(new cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Listnm0());
                List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.Listnm0> targetList1 = new ArrayList<>();
                if (CollectionUtils.nonEmpty(listnm0.getRecord())) {
                    List<Record1> recordList1 = listnm0.getRecord();
                    for (Record1 record1 : recordList1) {
                        cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.Listnm0 targrt1 = new cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.Listnm0();
                        BeanUtils.copyProperties(record1, targrt1);
                        targetList1.add(targrt1);
                    }
                    da3322RespDto.setListnm0(targetList1);
                }

                LstDzjtmx lstDzjtmx = Optional.ofNullable(respService.getLstDzjtmx()).orElse(new LstDzjtmx());
                if (CollectionUtils.nonEmpty(lstDzjtmx.getRecord())) {
                    List<Record2> recordList2 = lstDzjtmx.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzjtmx> targetList2 = new ArrayList<>();
                    for (Record2 record2 : recordList2) {
                        cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzjtmx target2 = new cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzjtmx();
                        BeanUtils.copyProperties(record2, target2);
                        targetList2.add(target2);
                    }
                    da3322RespDto.setLstDzjtmx(targetList2);
                }


                LstDzsfmx lstDzsfmx = Optional.ofNullable(respService.getLstDzsfmx()).orElse(new LstDzsfmx());
                if (CollectionUtils.nonEmpty(lstDzsfmx.getRecord())) {
                    List<Record3> recordList3 = lstDzsfmx.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzsfmx> targertList3 = new ArrayList<>();
                    for (Record3 record3 : recordList3) {
                        cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzsfmx target3 = new cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzsfmx();
                        BeanUtils.copyProperties(record3, target3);
                        targertList3.add(target3);
                    }
                    da3322RespDto.setLstDzsfmx(targertList3);
                }

                LstDztxmx lstDztxmx = Optional.ofNullable(respService.getLstDztxmx()).orElse(new LstDztxmx());
                if (CollectionUtils.nonEmpty(lstDztxmx.getRecord())) {
                    List<Record4> recordList4 = lstDztxmx.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDztxmx> targetList4 = new ArrayList<>();
                    for (Record4 record4 : recordList4) {
                        cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDztxmx target4 = new cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDztxmx();
                        BeanUtils.copyProperties(record4, target4);
                        targetList4.add(target4);
                    }
                    da3322RespDto.setLstDztxmx(targetList4);
                }

                da3322ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3322ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3322ResultDto.setCode(EpbEnum.EPB099999.key);
                da3322ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3322.key, EsbEnum.TRADE_CODE_DA3322.value, e.getMessage());
            da3322ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3322ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3322ResultDto.setData(da3322RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3322.key, EsbEnum.TRADE_CODE_DA3322.value, JSON.toJSONString(da3322ResultDto));
        return da3322ResultDto;
    }
}
