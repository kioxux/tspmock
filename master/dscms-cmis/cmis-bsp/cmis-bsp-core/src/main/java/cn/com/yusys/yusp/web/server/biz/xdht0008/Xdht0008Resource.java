package cn.com.yusys.yusp.web.server.biz.xdht0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0008.req.Xdht0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0008.resp.Xdht0008RespDto;
import cn.com.yusys.yusp.dto.server.xdht0008.req.Xdht0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0008.resp.Xdht0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借款合同双录流水同步
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0008:双录流水与合同号同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0008Resource {
	private static final Logger logger = LoggerFactory.getLogger(Xdht0008Resource.class);
	@Autowired
	private DscmsBizHtClientService dscmsBizHtClientService;
	/**
	 * 交易码：xdht0008
	 * 交易描述：借款合同双录流水同步
	 *
	 * @param xdht0008ReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("双录流水与合同号同步")
	@PostMapping("/xdht0008")
	//@Idempotent({"xdcaht0008", "#xdht0008ReqDto.datasq"})
	protected @ResponseBody
	Xdht0008RespDto xdht0008(@Validated @RequestBody Xdht0008ReqDto xdht0008ReqDto) throws Exception {

		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008ReqDto));
		Xdht0008DataReqDto xdht0008DataReqDto = new Xdht0008DataReqDto();// 请求Data： 双录流水与合同号同步
		Xdht0008DataRespDto xdht0008DataRespDto = new Xdht0008DataRespDto();// 响应Data：双录流水与合同号同步
		Xdht0008RespDto xdht0008RespDto = new Xdht0008RespDto();
		//  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdht0008.req.Data reqData = null; // 请求Data：双录流水与合同号同步
		cn.com.yusys.yusp.dto.server.biz.xdht0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0008.resp.Data();// 响应Data：双录流水与合同号同步
		//  此处包导入待确定 结束
		try {
			// 从 xdht0008ReqDto获取 reqData
			reqData = xdht0008ReqDto.getData();
			// 将 reqData 拷贝到xdht0008DataReqDto
			BeanUtils.copyProperties(reqData, xdht0008DataReqDto);
			// 调用服务：
			logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataReqDto));
			ResultDto<Xdht0008DataRespDto> xdht0008DataResultDto = dscmsBizHtClientService.xdht0008(xdht0008DataReqDto);
			logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008DataRespDto));
			// 从返回值中获取响应码和响应消息
			xdht0008RespDto.setErorcd(Optional.ofNullable(xdht0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
			xdht0008RespDto.setErortx(Optional.ofNullable(xdht0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

			if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0008DataResultDto.getCode())) {
				xdht0008DataRespDto = xdht0008DataResultDto.getData();
				xdht0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
				xdht0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
				// 将 xdht0008DataRespDto拷贝到 respData
				BeanUtils.copyProperties(xdht0008DataRespDto, respData);
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, e.getMessage());
			xdht0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
			xdht0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
		}
		logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
		String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
		logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
		xdht0008RespDto.setDatasq(servsq);

		xdht0008RespDto.setData(respData);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0008.key, DscmsEnum.TRADE_CODE_XDHT0008.value, JSON.toJSONString(xdht0008RespDto));
		return xdht0008RespDto;
	}
}
