package cn.com.yusys.yusp.web.client.esb.core.ln3110;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.*;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3110.req.Ln3110ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3110.req.Lstdkhbjh_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3110.req.Lstdzqgjh_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.Ln3110RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.Lstdkhk_ARRAY;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3110)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3110Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3110Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3110
     * 交易描述：用于贷款放款前进行还款计划试算
     *
     * @param ln3110ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3110:用于贷款放款前进行还款计划试算")
    @PostMapping("/ln3110")
    protected @ResponseBody
    ResultDto<Ln3110RespDto> ln3110(@Validated @RequestBody Ln3110ReqDto ln3110ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value, JSON.toJSONString(ln3110ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3110.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3110.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.Service();
        // 贷款还本计划
        Lstdkhbjh_ARRAY lstdkhbjh = new Lstdkhbjh_ARRAY();
        // 贷款定制期供计划表
        Lstdzqgjh_ARRAY lstdzqgjh = new Lstdzqgjh_ARRAY();
        Ln3110ReqService ln3110ReqService = new Ln3110ReqService();
        Ln3110RespService ln3110RespService = new Ln3110RespService();
        Ln3110RespDto ln3110RespDto = new Ln3110RespDto();
        ResultDto<Ln3110RespDto> ln3110ResultDto = new ResultDto<Ln3110RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3110ReqDto转换成reqService
            BeanUtils.copyProperties(ln3110ReqDto, reqService);
            if (CollectionUtils.nonEmpty(ln3110ReqDto.getLstdkhbjh())) {
                // 贷款还本计划
                List<Lstdkhbjh> lstdkhbjhDtos = ln3110ReqDto.getLstdkhbjh();
                List<cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdkhbjhRecord> lstdkhbjhRecord = new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdkhbjhRecord>();
                for (Lstdkhbjh lstdkhbjhDto : lstdkhbjhDtos) {
                    cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdkhbjhRecord record = new cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdkhbjhRecord();
                    BeanUtils.copyProperties(lstdkhbjhDto, record);
                    lstdkhbjhRecord.add(record);
                }
                lstdkhbjh.setRecord(lstdkhbjhRecord);
                reqService.setLstdkhbjh_ARRAY(lstdkhbjh);
            }
            if (Objects.nonNull(ln3110ReqDto.getLstdzqgjh())) {
                // 贷款定制期供计划表
                List<Lstdzqgjh> lstdzqgjhDtos = ln3110ReqDto.getLstdzqgjh();
                List<cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdzqgjhRecord> lstdzqgjhRecord = new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdzqgjhRecord>();
                if (lstdzqgjhDtos != null) {
                    for (Lstdzqgjh lstdzqgjhDto : lstdzqgjhDtos) {
                        cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdzqgjhRecord record = new cn.com.yusys.yusp.online.client.esb.core.ln3110.req.LstdzqgjhRecord();
                        BeanUtils.copyProperties(lstdzqgjhDto, record);
                        lstdzqgjhRecord.add(record);
                    }
                    lstdzqgjh.setRecord(lstdzqgjhRecord);
                    reqService.setLstdzqgjh_ARRAY(lstdzqgjh);
                }
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3110.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3110ReqService.setService(reqService);
            // 将ln3110ReqService转换成ln3110ReqServiceMap
            Map ln3110ReqServiceMap = beanMapUtil.beanToMap(ln3110ReqService);
            context.put("tradeDataMap", ln3110ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3110.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3110RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3110RespService.class, Ln3110RespService.class);
            respService = ln3110RespService.getService();

            ln3110ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3110ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3110RespDto
                BeanUtils.copyProperties(respService, ln3110RespDto);
                List<Lstdkhk> lstdkhks = new ArrayList<Lstdkhk>();
                Lstdkhk_ARRAY lstdkhk_array = Optional.ofNullable(respService.getLstdkhk_ARRAY()).orElse(new Lstdkhk_ARRAY());
                if (CollectionUtils.nonEmpty(lstdkhk_array.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdkhk.Record> recordLists = respService.getLstdkhk_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdkhk.Record record : recordLists) {
                        Lstdkhk lstdkhk = new Lstdkhk();
                        BeanUtils.copyProperties(record, lstdkhk);
                        lstdkhks.add(lstdkhk);
                    }
                }
                ln3110RespDto.setLstdkhk(lstdkhks);
                ln3110ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3110ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3110ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3110ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
              logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value, e.getMessage());
            ln3110ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3110ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3110ResultDto.setData(ln3110RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3110.key, EsbEnum.TRADE_CODE_LN3110.value, JSON.toJSONString(ln3110ResultDto));
        return ln3110ResultDto;
    }
}
