package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj004.resp;

/**
 * 响应Service：承兑签发审批结果综合服务接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
	private String erortx;//交易返回信息
	private String retcod;//交易返回代码

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getRetcod() {
		return retcod;
	}

	public void setRetcod(String retcod) {
		this.retcod = retcod;
	}

	@Override
	public String toString() {
		return "Service{" +
				"erortx='" + erortx + '\'' +
				"retcod='" + retcod + '\'' +
				'}';
	}
}
