package cn.com.yusys.yusp.web.client.esb.core.ln3045;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3045.req.Ln3045ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3045.resp.Ln3045RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3045)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3045Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3045Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3045
     * 交易描述：贷款核销处理
     *
     * @param ln3045ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3045:贷款核销处理")
    @PostMapping("/ln3045")
    protected @ResponseBody
    ResultDto<Ln3045RespDto> ln3045(@Validated @RequestBody Ln3045ReqDto ln3045ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3045.key, EsbEnum.TRADE_CODE_LN3045.value, JSON.toJSONString(ln3045ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3045.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3045.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3045.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3045.resp.Service();

        Ln3045ReqService ln3045ReqService = new Ln3045ReqService();
        Ln3045RespService ln3045RespService = new Ln3045RespService();
        Ln3045RespDto ln3045RespDto = new Ln3045RespDto();
        ResultDto<Ln3045RespDto> ln3045ResultDto = new ResultDto<Ln3045RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3045ReqDto转换成reqService
            BeanUtils.copyProperties(ln3045ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3045.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3045ReqService.setService(reqService);
            // 将ln3045ReqService转换成ln3045ReqServiceMap
            Map ln3045ReqServiceMap = beanMapUtil.beanToMap(ln3045ReqService);
            context.put("tradeDataMap", ln3045ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3045.key, EsbEnum.TRADE_CODE_LN3045.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3045.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3045.key, EsbEnum.TRADE_CODE_LN3045.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3045RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3045RespService.class, Ln3045RespService.class);
            respService = ln3045RespService.getService();

            ln3045ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3045ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3045RespDto
                BeanUtils.copyProperties(respService, ln3045RespDto);
                ln3045ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3045ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3045ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3045ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3045.key, EsbEnum.TRADE_CODE_LN3045.value, e.getMessage());
            ln3045ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3045ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3045ResultDto.setData(ln3045RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3045.key, EsbEnum.TRADE_CODE_LN3045.value, JSON.toJSONString(ln3045ResultDto));
        return ln3045ResultDto;
    }
}
