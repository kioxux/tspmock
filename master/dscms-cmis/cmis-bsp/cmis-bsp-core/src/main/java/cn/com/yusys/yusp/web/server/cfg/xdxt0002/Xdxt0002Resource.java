package cn.com.yusys.yusp.web.server.cfg.xdxt0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0002.req.Xdxt0002ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0002.resp.Xdxt0002RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.req.Xdxt0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0002.resp.Xdxt0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据直营团队类型查询客户经理工号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0002:根据直营团队类型查询客户经理工号")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0002Resource.class);
    @Autowired
    private DscmsBizXtClientService dscmsBizXtClientService;

    /**
     * 交易码：xdxt0002
     * 交易描述：根据直营团队类型查询客户经理工号
     *
     * @param xdxt0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据直营团队类型查询客户经理工号")
    @PostMapping("/xdxt0002")
    //@Idempotent({"xdcaxt0002", "#xdxt0002ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0002RespDto xdxt0002(@Validated @RequestBody Xdxt0002ReqDto xdxt0002ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002ReqDto));
        Xdxt0002DataReqDto xdxt0002DataReqDto = new Xdxt0002DataReqDto();// 请求Data： 根据直营团队类型查询客户经理工号
        Xdxt0002DataRespDto xdxt0002DataRespDto = new Xdxt0002DataRespDto();// 响应Data：根据直营团队类型查询客户经理工号
        Xdxt0002RespDto xdxt0002RespDto = new Xdxt0002RespDto();
        cn.com.yusys.yusp.dto.server.cfg.xdxt0002.req.Data reqData = null; // 请求Data：根据直营团队类型查询客户经理工号
        cn.com.yusys.yusp.dto.server.cfg.xdxt0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdxt0002.resp.Data();// 响应Data：根据直营团队类型查询客户经理工号

        try {
            // 从 xdxt0002ReqDto获取 reqData
            reqData = xdxt0002ReqDto.getData();
            // 将 reqData 拷贝到xdxt0002DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0002DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002DataReqDto));
            ResultDto<Xdxt0002DataRespDto> xdxt0002DataResultDto = dscmsBizXtClientService.xdxt0002(xdxt0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0002RespDto.setErorcd(Optional.ofNullable(xdxt0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0002RespDto.setErortx(Optional.ofNullable(xdxt0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0002DataResultDto.getCode())) {
                xdxt0002DataRespDto = xdxt0002DataResultDto.getData();
                xdxt0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, e.getMessage());
            xdxt0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0002RespDto.setDatasq(servsq);

        xdxt0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0002.key, DscmsEnum.TRADE_CODE_XDXT0002.value, JSON.toJSONString(xdxt0002RespDto));
        return xdxt0002RespDto;
    }
}
