package cn.com.yusys.yusp.online.client.esb.core.dp2280.resp;

import java.math.BigDecimal;

/**
 * 响应Service：子账户序号查询接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/16 10:22
 */
public class ListNm {

    private String zhhaoxuh; // 子账户序号
    private String kehuzhlx; // 客户账号类型
    private String zhanghao; // 负债账号
    private String zhhuxinz; // 账户性质
    private String huobdaih; // 货币代号
    private String chaohubz; // 账户钞汇标志
    private BigDecimal mingxxuh; // 负债账号明细序号
    private String zhhuztai; // 账户状态
    private String cunkzlei; // 存款种类
    private String cunqiiii; // 存期
    private String doqiriqi; // 到期日期
    private String kaihriqi; // 开户日期
    private String kaihjigo; // 开户机构
    private BigDecimal zhhuyuee; // 当前账户余额
    private String zhhuzwmc; // 账户名称
    private String chapbhao; // 产品编号
    private String chanpshm; // 产品说明
    private String suoshudx; // 产品所属对象
    private String zhufldm1; // 账户分类代码1
    private String zhufldm2; // 账户分类代码2
    private String zhanghfl; // 账户分类
    private String zhufldm3; // 账户分类代码3
    private String beiyzd01; // 备用字段1
    private String beiyzd02; // 备用字段2
    private String beiyzd03; // 备用字段3
    private String lilvyebz; // 利率余额标志
    private String jixiyebz; // 计息余额标志
    private String fzcpleix; // 负债产品类型
    private String yezztbbz; // 余额与总账同步标志
    private String zhcphaoo; // 组合产品号
    private String tjrminc1; // 推荐人名称1
    private String tjrminc2; // 推荐人名称2
    private String tjrminc3; // 推荐人名称3
    private String tjrminc4; // 推荐人名称4
    private String tjrminc5; // 推荐人名称5
    private String tjrgonh1; // 推荐人工号1
    private String tjrgonh2; // 推荐人工号2
    private String tjrgonh3; // 推荐人工号3
    private String tjrgonh4; // 推荐人工号4
    private String tjrgonh5; // 推荐人工号5
    private String pngzzlei; // 凭证种类
    private String pingzhzl; // 凭证种类
    private String pngzphao; // 凭证批号
    private String pngzxhao; // 凭证序号
    private String pngzhhao; // 凭证号
    private String kadxiang; // 卡对象
    private String zhzfbsbz; // 账户只付不收标志
    private String zhzsbfbz; // 账户只收不付标志
    private String zhfbdjbz; // 账户封闭冻结标志
    private String zhjedjbz; // 账户金额冻结标志
    private String aiostype; // 组合账户形态
    private String shifzhbz; // 是否保证金组合产品
    private String xushibzh; // 账户虚实标志
    private String dsdzzhfl; // 个人电子账户分类
    private String youwkbiz; // 有无卡标志
    private String kehuzhao; // 客户账号
    private BigDecimal tjrenbl1; // 推荐人比例1
    private BigDecimal tjrenbl2; // 推荐人比例2
    private BigDecimal tjrenbl3; // 推荐人比例3
    private BigDecimal tjrenbl4; // 推荐人比例4
    private BigDecimal tjrenbl5; // 推荐人比例5
    private long biaozhiz; // 标志值

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getZhhuxinz() {
        return zhhuxinz;
    }

    public void setZhhuxinz(String zhhuxinz) {
        this.zhhuxinz = zhhuxinz;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public BigDecimal getMingxxuh() {
        return mingxxuh;
    }

    public void setMingxxuh(BigDecimal mingxxuh) {
        this.mingxxuh = mingxxuh;
    }

    public String getZhhuztai() {
        return zhhuztai;
    }

    public void setZhhuztai(String zhhuztai) {
        this.zhhuztai = zhhuztai;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getKaihjigo() {
        return kaihjigo;
    }

    public void setKaihjigo(String kaihjigo) {
        this.kaihjigo = kaihjigo;
    }

    public BigDecimal getZhhuyuee() {
        return zhhuyuee;
    }

    public void setZhhuyuee(BigDecimal zhhuyuee) {
        this.zhhuyuee = zhhuyuee;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getSuoshudx() {
        return suoshudx;
    }

    public void setSuoshudx(String suoshudx) {
        this.suoshudx = suoshudx;
    }

    public String getZhufldm1() {
        return zhufldm1;
    }

    public void setZhufldm1(String zhufldm1) {
        this.zhufldm1 = zhufldm1;
    }

    public String getZhufldm2() {
        return zhufldm2;
    }

    public void setZhufldm2(String zhufldm2) {
        this.zhufldm2 = zhufldm2;
    }

    public String getZhanghfl() {
        return zhanghfl;
    }

    public void setZhanghfl(String zhanghfl) {
        this.zhanghfl = zhanghfl;
    }

    public String getZhufldm3() {
        return zhufldm3;
    }

    public void setZhufldm3(String zhufldm3) {
        this.zhufldm3 = zhufldm3;
    }

    public String getBeiyzd01() {
        return beiyzd01;
    }

    public void setBeiyzd01(String beiyzd01) {
        this.beiyzd01 = beiyzd01;
    }

    public String getBeiyzd02() {
        return beiyzd02;
    }

    public void setBeiyzd02(String beiyzd02) {
        this.beiyzd02 = beiyzd02;
    }

    public String getBeiyzd03() {
        return beiyzd03;
    }

    public void setBeiyzd03(String beiyzd03) {
        this.beiyzd03 = beiyzd03;
    }

    public String getLilvyebz() {
        return lilvyebz;
    }

    public void setLilvyebz(String lilvyebz) {
        this.lilvyebz = lilvyebz;
    }

    public String getJixiyebz() {
        return jixiyebz;
    }

    public void setJixiyebz(String jixiyebz) {
        this.jixiyebz = jixiyebz;
    }

    public String getFzcpleix() {
        return fzcpleix;
    }

    public void setFzcpleix(String fzcpleix) {
        this.fzcpleix = fzcpleix;
    }

    public String getYezztbbz() {
        return yezztbbz;
    }

    public void setYezztbbz(String yezztbbz) {
        this.yezztbbz = yezztbbz;
    }

    public String getZhcphaoo() {
        return zhcphaoo;
    }

    public void setZhcphaoo(String zhcphaoo) {
        this.zhcphaoo = zhcphaoo;
    }

    public String getTjrminc1() {
        return tjrminc1;
    }

    public void setTjrminc1(String tjrminc1) {
        this.tjrminc1 = tjrminc1;
    }

    public String getTjrminc2() {
        return tjrminc2;
    }

    public void setTjrminc2(String tjrminc2) {
        this.tjrminc2 = tjrminc2;
    }

    public String getTjrminc3() {
        return tjrminc3;
    }

    public void setTjrminc3(String tjrminc3) {
        this.tjrminc3 = tjrminc3;
    }

    public String getTjrminc4() {
        return tjrminc4;
    }

    public void setTjrminc4(String tjrminc4) {
        this.tjrminc4 = tjrminc4;
    }

    public String getTjrminc5() {
        return tjrminc5;
    }

    public void setTjrminc5(String tjrminc5) {
        this.tjrminc5 = tjrminc5;
    }

    public String getTjrgonh1() {
        return tjrgonh1;
    }

    public void setTjrgonh1(String tjrgonh1) {
        this.tjrgonh1 = tjrgonh1;
    }

    public String getTjrgonh2() {
        return tjrgonh2;
    }

    public void setTjrgonh2(String tjrgonh2) {
        this.tjrgonh2 = tjrgonh2;
    }

    public String getTjrgonh3() {
        return tjrgonh3;
    }

    public void setTjrgonh3(String tjrgonh3) {
        this.tjrgonh3 = tjrgonh3;
    }

    public String getTjrgonh4() {
        return tjrgonh4;
    }

    public void setTjrgonh4(String tjrgonh4) {
        this.tjrgonh4 = tjrgonh4;
    }

    public String getTjrgonh5() {
        return tjrgonh5;
    }

    public void setTjrgonh5(String tjrgonh5) {
        this.tjrgonh5 = tjrgonh5;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getPngzphao() {
        return pngzphao;
    }

    public void setPngzphao(String pngzphao) {
        this.pngzphao = pngzphao;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getPngzhhao() {
        return pngzhhao;
    }

    public void setPngzhhao(String pngzhhao) {
        this.pngzhhao = pngzhhao;
    }

    public String getKadxiang() {
        return kadxiang;
    }

    public void setKadxiang(String kadxiang) {
        this.kadxiang = kadxiang;
    }

    public String getZhzfbsbz() {
        return zhzfbsbz;
    }

    public void setZhzfbsbz(String zhzfbsbz) {
        this.zhzfbsbz = zhzfbsbz;
    }

    public String getZhzsbfbz() {
        return zhzsbfbz;
    }

    public void setZhzsbfbz(String zhzsbfbz) {
        this.zhzsbfbz = zhzsbfbz;
    }

    public String getZhfbdjbz() {
        return zhfbdjbz;
    }

    public void setZhfbdjbz(String zhfbdjbz) {
        this.zhfbdjbz = zhfbdjbz;
    }

    public String getZhjedjbz() {
        return zhjedjbz;
    }

    public void setZhjedjbz(String zhjedjbz) {
        this.zhjedjbz = zhjedjbz;
    }

    public String getAiostype() {
        return aiostype;
    }

    public void setAiostype(String aiostype) {
        this.aiostype = aiostype;
    }

    public String getShifzhbz() {
        return shifzhbz;
    }

    public void setShifzhbz(String shifzhbz) {
        this.shifzhbz = shifzhbz;
    }

    public String getXushibzh() {
        return xushibzh;
    }

    public void setXushibzh(String xushibzh) {
        this.xushibzh = xushibzh;
    }

    public String getDsdzzhfl() {
        return dsdzzhfl;
    }

    public void setDsdzzhfl(String dsdzzhfl) {
        this.dsdzzhfl = dsdzzhfl;
    }

    public String getYouwkbiz() {
        return youwkbiz;
    }

    public void setYouwkbiz(String youwkbiz) {
        this.youwkbiz = youwkbiz;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public BigDecimal getTjrenbl1() {
        return tjrenbl1;
    }

    public void setTjrenbl1(BigDecimal tjrenbl1) {
        this.tjrenbl1 = tjrenbl1;
    }

    public BigDecimal getTjrenbl2() {
        return tjrenbl2;
    }

    public void setTjrenbl2(BigDecimal tjrenbl2) {
        this.tjrenbl2 = tjrenbl2;
    }

    public BigDecimal getTjrenbl3() {
        return tjrenbl3;
    }

    public void setTjrenbl3(BigDecimal tjrenbl3) {
        this.tjrenbl3 = tjrenbl3;
    }

    public BigDecimal getTjrenbl4() {
        return tjrenbl4;
    }

    public void setTjrenbl4(BigDecimal tjrenbl4) {
        this.tjrenbl4 = tjrenbl4;
    }

    public BigDecimal getTjrenbl5() {
        return tjrenbl5;
    }

    public void setTjrenbl5(BigDecimal tjrenbl5) {
        this.tjrenbl5 = tjrenbl5;
    }

    public long getBiaozhiz() {
        return biaozhiz;
    }

    public void setBiaozhiz(long biaozhiz) {
        this.biaozhiz = biaozhiz;
    }

    @Override
    public String toString() {
        return "ListNm{" +
                "zhhaoxuh='" + zhhaoxuh + '\'' +
                ", kehuzhlx='" + kehuzhlx + '\'' +
                ", zhanghao='" + zhanghao + '\'' +
                ", zhhuxinz='" + zhhuxinz + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", mingxxuh=" + mingxxuh +
                ", zhhuztai='" + zhhuztai + '\'' +
                ", cunkzlei='" + cunkzlei + '\'' +
                ", cunqiiii='" + cunqiiii + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", kaihjigo='" + kaihjigo + '\'' +
                ", zhhuyuee=" + zhhuyuee +
                ", zhhuzwmc='" + zhhuzwmc + '\'' +
                ", chapbhao='" + chapbhao + '\'' +
                ", chanpshm='" + chanpshm + '\'' +
                ", suoshudx='" + suoshudx + '\'' +
                ", zhufldm1='" + zhufldm1 + '\'' +
                ", zhufldm2='" + zhufldm2 + '\'' +
                ", zhanghfl='" + zhanghfl + '\'' +
                ", zhufldm3='" + zhufldm3 + '\'' +
                ", beiyzd01='" + beiyzd01 + '\'' +
                ", beiyzd02='" + beiyzd02 + '\'' +
                ", beiyzd03='" + beiyzd03 + '\'' +
                ", lilvyebz='" + lilvyebz + '\'' +
                ", jixiyebz='" + jixiyebz + '\'' +
                ", fzcpleix='" + fzcpleix + '\'' +
                ", yezztbbz='" + yezztbbz + '\'' +
                ", zhcphaoo='" + zhcphaoo + '\'' +
                ", tjrminc1='" + tjrminc1 + '\'' +
                ", tjrminc2='" + tjrminc2 + '\'' +
                ", tjrminc3='" + tjrminc3 + '\'' +
                ", tjrminc4='" + tjrminc4 + '\'' +
                ", tjrminc5='" + tjrminc5 + '\'' +
                ", tjrgonh1='" + tjrgonh1 + '\'' +
                ", tjrgonh2='" + tjrgonh2 + '\'' +
                ", tjrgonh3='" + tjrgonh3 + '\'' +
                ", tjrgonh4='" + tjrgonh4 + '\'' +
                ", tjrgonh5='" + tjrgonh5 + '\'' +
                ", pngzzlei='" + pngzzlei + '\'' +
                ", pingzhzl='" + pingzhzl + '\'' +
                ", pngzphao='" + pngzphao + '\'' +
                ", pngzxhao='" + pngzxhao + '\'' +
                ", pngzhhao='" + pngzhhao + '\'' +
                ", kadxiang='" + kadxiang + '\'' +
                ", zhzfbsbz='" + zhzfbsbz + '\'' +
                ", zhzsbfbz='" + zhzsbfbz + '\'' +
                ", zhfbdjbz='" + zhfbdjbz + '\'' +
                ", zhjedjbz='" + zhjedjbz + '\'' +
                ", aiostype='" + aiostype + '\'' +
                ", shifzhbz='" + shifzhbz + '\'' +
                ", xushibzh='" + xushibzh + '\'' +
                ", dsdzzhfl='" + dsdzzhfl + '\'' +
                ", youwkbiz='" + youwkbiz + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", tjrenbl1=" + tjrenbl1 +
                ", tjrenbl2=" + tjrenbl2 +
                ", tjrenbl3=" + tjrenbl3 +
                ", tjrenbl4=" + tjrenbl4 +
                ", tjrenbl5=" + tjrenbl5 +
                ", biaozhiz=" + biaozhiz +
                '}';
    }
}
