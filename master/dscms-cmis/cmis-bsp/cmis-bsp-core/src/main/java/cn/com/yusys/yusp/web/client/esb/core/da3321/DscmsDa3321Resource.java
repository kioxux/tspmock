package cn.com.yusys.yusp.web.client.esb.core.da3321;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3321.req.Da3321ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3321.resp.Da3321RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3321.req.Da3321ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Da3321RespService;
import cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Record;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 接口处理类:待变现抵债资产销账
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class DscmsDa3321Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsDa3321Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3321
     * 交易描述：待变现抵债资产销账
     *
     * @param da3321ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3321")
    protected @ResponseBody
    ResultDto<Da3321RespDto> da3321(@Validated @RequestBody Da3321ReqDto da3321ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3321.key, EsbEnum.TRADE_CODE_DA3321.value, JSON.toJSONString(da3321ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3321.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3321.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Service();

        Da3321ReqService da3321ReqService = new Da3321ReqService();
        Da3321RespService da3321RespService = new Da3321RespService();
        Da3321RespDto da3321RespDto = new Da3321RespDto();
        ResultDto<Da3321RespDto> da3321ResultDto = new ResultDto<Da3321RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3321ReqDto转换成reqService
            BeanUtils.copyProperties(da3321ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3321.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3321ReqService.setService(reqService);
            // 将da3321ReqService转换成da3321ReqServiceMap
            Map da3321ReqServiceMap = beanMapUtil.beanToMap(da3321ReqService);
            context.put("tradeDataMap", da3321ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3321.key, EsbEnum.TRADE_CODE_DA3321.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3321.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3321.key, EsbEnum.TRADE_CODE_DA3321.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3321RespService = beanMapUtil.mapToBean(tradeDataMap, Da3321RespService.class, Da3321RespService.class);
            respService = da3321RespService.getService();


            da3321ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3321ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3321RespDto
                BeanUtils.copyProperties(respService, da3321RespDto);
                cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Listnm1 listnm1 = Optional.ofNullable(respService.getListnm1()).orElse(new cn.com.yusys.yusp.online.client.esb.core.da3321.resp.Listnm1());
                if (CollectionUtils.nonEmpty(listnm1.getRecord())) {
                    List<Record> recordList = listnm1.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.da3321.resp.Listnm1> targetList = new ArrayList<>();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.da3321.resp.Listnm1 listnm11 = new cn.com.yusys.yusp.dto.client.esb.core.da3321.resp.Listnm1();
                        BeanUtils.copyProperties(record, listnm1);
                        targetList.add(listnm11);
                    }
                    da3321RespDto.setListnm1(targetList);
                }
                da3321ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3321ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3321ResultDto.setCode(EpbEnum.EPB099999.key);
                da3321ResultDto.setMessage(respService.getErortx());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3321.key, EsbEnum.TRADE_CODE_DA3321.value, e.getMessage());
            da3321ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3321ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3321ResultDto.setData(da3321RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3321.key, EsbEnum.TRADE_CODE_DA3321.value, JSON.toJSONString(da3321ResultDto));
        return da3321ResultDto;
    }
}
