package cn.com.yusys.yusp.web.server.biz.xdtz0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0007.req.Xdtz0007ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0007.resp.Xdtz0007RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0007.req.Xdtz0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0007.resp.Xdtz0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号获取非信用方式发放贷款的最长到期日
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0007:根据客户号获取非信用方式发放贷款的最长到期日")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0007Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0007
     * 交易描述：根据客户号获取非信用方式发放贷款的最长到期日
     *
     * @param xdtz0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号获取非信用方式发放贷款的最长到期日")
    @PostMapping("/xdtz0007")
    //@Idempotent({"xdcatz0007", "#xdtz0007ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0007RespDto xdtz0007(@Validated @RequestBody Xdtz0007ReqDto xdtz0007ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(xdtz0007ReqDto));
        Xdtz0007DataReqDto xdtz0007DataReqDto = new Xdtz0007DataReqDto();// 请求Data： 根据客户号获取非信用方式发放贷款的最长到期日
        Xdtz0007DataRespDto xdtz0007DataRespDto = new Xdtz0007DataRespDto();// 响应Data：根据客户号获取非信用方式发放贷款的最长到期日
        Xdtz0007RespDto xdtz0007RespDto = new Xdtz0007RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0007.req.Data reqData = null; // 请求Data：根据客户号获取非信用方式发放贷款的最长到期日
        cn.com.yusys.yusp.dto.server.biz.xdtz0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0007.resp.Data();// 响应Data：根据客户号获取非信用方式发放贷款的最长到期日
        try {
            // 从 xdtz0007ReqDto获取 reqData
            reqData = xdtz0007ReqDto.getData();
            // 将 reqData 拷贝到xdtz0007DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0007DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(xdtz0007DataReqDto));
            ResultDto<Xdtz0007DataRespDto> xdtz0007DataResultDto = dscmsBizTzClientService.xdtz0007(xdtz0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(xdtz0007DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0007RespDto.setErorcd(Optional.ofNullable(xdtz0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0007RespDto.setErortx(Optional.ofNullable(xdtz0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0007DataResultDto.getCode())) {
                xdtz0007DataRespDto = xdtz0007DataResultDto.getData();
                xdtz0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, e.getMessage());
            xdtz0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0007RespDto.setDatasq(servsq);

        xdtz0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0007.key, DscmsEnum.TRADE_CODE_XDTZ0007.value, JSON.toJSONString(xdtz0007RespDto));
        return xdtz0007RespDto;
    }
}
