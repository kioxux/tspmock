package cn.com.yusys.yusp.online.client.esb.irs.irs21.resp;

/**
 * 返回Service：单一客户限额测算信息同步
 *
 * @author zhugenrong
 * @version 1.0
 * @since 2021/4/15 19:55
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String prcscd;//交易码

    private String sxserialno;//授信申请流水号
    private String custid;//客户编号
    private String custame;//客户名称
    private String ratingyear;//评级年度
    private String ratinglevel;//评级结果
    private String modelid;//评级模型
    private String calvalue;//单一客户测算限额
    private String caldate;//单一客户限额测算日期
    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getSxserialno() {
        return sxserialno;
    }

    public void setSxserialno(String sxserialno) {
        this.sxserialno = sxserialno;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustame() {
        return custame;
    }

    public void setCustame(String custame) {
        this.custame = custame;
    }

    public String getRatingyear() {
        return ratingyear;
    }

    public void setRatingyear(String ratingyear) {
        this.ratingyear = ratingyear;
    }

    public String getRatinglevel() {
        return ratinglevel;
    }

    public void setRatinglevel(String ratinglevel) {
        this.ratinglevel = ratinglevel;
    }

    public String getModelid() {
        return modelid;
    }

    public void setModelid(String modelid) {
        this.modelid = modelid;
    }

    public String getCalvalue() {
        return calvalue;
    }

    public void setCalvalue(String calvalue) {
        this.calvalue = calvalue;
    }

    public String getCaldate() {
        return caldate;
    }

    public void setCaldate(String caldate) {
        this.caldate = caldate;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", prcscd='" + prcscd + '\'' +
                ", sxserialno='" + sxserialno + '\'' +
                ", custid='" + custid + '\'' +
                ", custame='" + custame + '\'' +
                ", ratingyear='" + ratingyear + '\'' +
                ", ratinglevel='" + ratinglevel + '\'' +
                ", modelid='" + modelid + '\'' +
                ", calvalue='" + calvalue + '\'' +
                ", caldate='" + caldate + '\'' +
                '}';
    }
}
