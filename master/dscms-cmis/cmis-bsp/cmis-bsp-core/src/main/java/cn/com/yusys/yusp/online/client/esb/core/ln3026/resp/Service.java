package cn.com.yusys.yusp.online.client.esb.core.ln3026.resp;

import java.math.BigDecimal;

/**
 * 响应Service：银团贷款开户
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述
    private String dkjiejuh;//贷款借据号
    private String dkzhangh;//贷款账号
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String dkqixian;//贷款期限
    private BigDecimal jiejuuje;//借据放款金额
    private String jiaoyirq;//交易日期
    private String jiaoyijg;//营业机构
    private String jiaoyigy;//交易柜员
    private String jiaoyils;//交易流水
    private BigDecimal bencfkje;//本次放款金额
    private String huobdhao;//货币代号
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String wtckzhao;//委托存款账号
    private String wtckzixh;//委托存款账号子序号
    private String dkrzhzhh;//贷款入账账号
    private String dkrzhzxh;//贷款入账账号子序号
    private Lstytczfe_ARRAY lstytczfe_ARRAY;//银团出资份额定价计息复合类型
    private String yintdkbz;//银团贷款
    private String dkdbfshi;//贷款担保方式
    private String huankfsh;//还款方式
    private String hkzhouqi;//还款周期
    private String yqllcklx;//逾期利率参考类型
    private String jfxibzhi;//计复息标志
    private String flllcklx;//复利利率参考类型
    private String zjlyzhao;//资金来源账号
    private String zjlyzzxh;//资金来源账号子序号
    private String wujiflbz;//五级分类标志
    private String dkyongtu;//贷款用途
    private String beizhuuu;//备注
    private String fuhejgou;//复核机构
    private Lstdkhbjh_ARRAY lstdkhbjh_ARRAY;//贷款还本计划
    private String zhngjigo;//账务机构
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private Lstdzqgjh_ARRAY lstdzqgjh_ARRAY;//贷款定制期供计划表
    private String llqxkdfs;//利率期限靠档方式
    private String lilvqixx;//利率期限
    private String fkjzhfsh;//放款记账方式
    private BigDecimal hetongll;//合同利率
    private BigDecimal zhchlilv;//正常利率
    private BigDecimal yuqililv;//逾期利率
    private BigDecimal fulililv;//复利利率
    private String dhkzhhbz;//多还款账户标志
    private Lstdkhkzh_ARRAY lstdkhkzh_ARRAY;//贷款多还款账户
    private Lstdkstzf_ARRAY lstdkstzf_ARRAY;//贷款受托支付

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public BigDecimal getBencfkje() {
        return bencfkje;
    }

    public void setBencfkje(BigDecimal bencfkje) {
        this.bencfkje = bencfkje;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public Lstytczfe_ARRAY getLstytczfe_ARRAY() {
        return lstytczfe_ARRAY;
    }

    public void setLstytczfe_ARRAY(Lstytczfe_ARRAY lstytczfe_ARRAY) {
        this.lstytczfe_ARRAY = lstytczfe_ARRAY;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public Lstdkhbjh_ARRAY getLstdkhbjh_ARRAY() {
        return lstdkhbjh_ARRAY;
    }

    public void setLstdkhbjh_ARRAY(Lstdkhbjh_ARRAY lstdkhbjh_ARRAY) {
        this.lstdkhbjh_ARRAY = lstdkhbjh_ARRAY;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public Lstdzqgjh_ARRAY getLstdzqgjh_ARRAY() {
        return lstdzqgjh_ARRAY;
    }

    public void setLstdzqgjh_ARRAY(Lstdzqgjh_ARRAY lstdzqgjh_ARRAY) {
        this.lstdzqgjh_ARRAY = lstdzqgjh_ARRAY;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public Lstdkhkzh_ARRAY getLstdkhkzh_ARRAY() {
        return lstdkhkzh_ARRAY;
    }

    public void setLstdkhkzh_ARRAY(Lstdkhkzh_ARRAY lstdkhkzh_ARRAY) {
        this.lstdkhkzh_ARRAY = lstdkhkzh_ARRAY;
    }

    public Lstdkstzf_ARRAY getLstdkstzf_ARRAY() {
        return lstdkstzf_ARRAY;
    }

    public void setLstdkstzf_ARRAY(Lstdkstzf_ARRAY lstdkstzf_ARRAY) {
        this.lstdkstzf_ARRAY = lstdkstzf_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "bencfkje='" + bencfkje + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "wtckzhao='" + wtckzhao + '\'' +
                "wtckzixh='" + wtckzixh + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "lstytczfe='" + lstytczfe_ARRAY + '\'' +
                "yintdkbz='" + yintdkbz + '\'' +
                "dkdbfshi='" + dkdbfshi + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "jfxibzhi='" + jfxibzhi + '\'' +
                "flllcklx='" + flllcklx + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "wujiflbz='" + wujiflbz + '\'' +
                "dkyongtu='" + dkyongtu + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "lstdkhbjh='" + lstdkhbjh_ARRAY + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "lstdzqgjh='" + lstdzqgjh_ARRAY + '\'' +
                "llqxkdfs='" + llqxkdfs + '\'' +
                "lilvqixx='" + lilvqixx + '\'' +
                "fkjzhfsh='" + fkjzhfsh + '\'' +
                "hetongll='" + hetongll + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "fulililv='" + fulililv + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "lstdkhkzh='" + lstdkhkzh_ARRAY + '\'' +
                "lstdkstzf='" + lstdkstzf_ARRAY + '\'' +
                '}';
    }
}
