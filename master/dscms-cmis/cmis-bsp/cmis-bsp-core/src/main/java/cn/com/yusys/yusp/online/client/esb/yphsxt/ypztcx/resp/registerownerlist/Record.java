package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerownerlist;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:19
 * @since 2021/7/9 14:19
 */
public class Record {

    private String ownnam;//登记簿权利人信息-权利人姓名
    private String owdonu;//登记簿权利人信息-权利人证件号码
    private String owdoty;//登记簿权利人信息-权利人证件类型
    private String owcenu;//登记簿权利人信息-不动产权证号
    private String ooshmo;//登记簿权利人信息-共有方式
    private String ooshin;//登记簿权利人信息-共有情况
    private String ooshpr;//登记簿权利人信息-权利比例
    private String ohcemo;//登记簿权利人信息-持证方式

    public String getOwnnam() {
        return ownnam;
    }

    public void setOwnnam(String ownnam) {
        this.ownnam = ownnam;
    }

    public String getOwdonu() {
        return owdonu;
    }

    public void setOwdonu(String owdonu) {
        this.owdonu = owdonu;
    }

    public String getOwdoty() {
        return owdoty;
    }

    public void setOwdoty(String owdoty) {
        this.owdoty = owdoty;
    }

    public String getOwcenu() {
        return owcenu;
    }

    public void setOwcenu(String owcenu) {
        this.owcenu = owcenu;
    }

    public String getOoshmo() {
        return ooshmo;
    }

    public void setOoshmo(String ooshmo) {
        this.ooshmo = ooshmo;
    }

    public String getOoshin() {
        return ooshin;
    }

    public void setOoshin(String ooshin) {
        this.ooshin = ooshin;
    }

    public String getOoshpr() {
        return ooshpr;
    }

    public void setOoshpr(String ooshpr) {
        this.ooshpr = ooshpr;
    }

    public String getOhcemo() {
        return ohcemo;
    }

    public void setOhcemo(String ohcemo) {
        this.ohcemo = ohcemo;
    }

    @Override
    public String toString() {
        return "Record{" +
                "ownnam='" + ownnam + '\'' +
                ", owdonu='" + owdonu + '\'' +
                ", owdoty='" + owdoty + '\'' +
                ", owcenu='" + owcenu + '\'' +
                ", ooshmo='" + ooshmo + '\'' +
                ", ooshin='" + ooshin + '\'' +
                ", ooshpr='" + ooshpr + '\'' +
                ", ohcemo='" + ohcemo + '\'' +
                '}';
    }
}
