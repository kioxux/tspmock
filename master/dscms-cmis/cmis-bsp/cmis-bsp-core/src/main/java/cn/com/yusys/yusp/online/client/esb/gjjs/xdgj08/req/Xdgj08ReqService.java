package cn.com.yusys.yusp.online.client.esb.gjjs.xdgj08.req;

/**
 * 请求Service：信贷还款信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdgj08ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Xdgj08ReqService{" +
				"service=" + service +
				'}';
	}
}
