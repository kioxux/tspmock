package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerhouselist;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:11
 * @since 2021/7/9 14:11
 */
public class Registerhouselist {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerhouselist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Registerhouselist{" +
                "record=" + record +
                '}';
    }
}
