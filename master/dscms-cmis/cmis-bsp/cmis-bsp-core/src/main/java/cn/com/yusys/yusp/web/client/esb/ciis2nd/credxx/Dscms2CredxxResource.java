package cn.com.yusys.yusp.web.client.esb.ciis2nd.credxx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.req.CredxxReqService;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.resp.CredxxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用二代征信系统的接口处理类
 **/
@Api(tags = "BSP封装调用二代征信系统的接口处理类（credxx）")
@RestController
@RequestMapping("/api/dscms2ciis2nd")
public class Dscms2CredxxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2CredxxResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 线下查询接口（处理码credxx）
     *
     * @param credxxReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("credxx:线下查询接口")
    @PostMapping("/credxx")
    protected @ResponseBody
    ResultDto<CredxxRespDto> credxx(@Validated @RequestBody CredxxReqDto credxxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxReqDto));
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.resp.Service();
        CredxxReqService credxxReqService = new CredxxReqService();
        CredxxRespService credxxRespService = new CredxxRespService();
        CredxxRespDto credxxRespDto = new CredxxRespDto();
        ResultDto<CredxxRespDto> credxxResultDto = new ResultDto<CredxxRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将CredxxReqDto转换成reqService
            BeanUtils.copyProperties(credxxReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CREDXX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setBrchno(credxxReqDto.getBrchno());//    部门号
            LocalDateTime now = LocalDateTime.now();
            credxxReqService.setService(reqService);
            // 将credxxReqService转换成credxxReqServiceMap
            Map credxxReqServiceMap = beanMapUtil.beanToMap(credxxReqService);
            context.put("tradeDataMap", credxxReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CREDXX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            credxxRespService = beanMapUtil.mapToBean(tradeDataMap, CredxxRespService.class, CredxxRespService.class);
            respService = credxxRespService.getService();

            credxxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            credxxResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将CredxxRespDto封装到ResultDto<CredxxRespDto>
                BeanUtils.copyProperties(respService, credxxRespDto);
                credxxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                credxxResultDto.setMessage(Optional.ofNullable(credxxResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            } else if (Objects.equals("6666", respService.getErorcd())) {
                credxxResultDto.setCode("6666");
                credxxResultDto.setMessage("征信中心无信用信息[二代征信新]");
            } else {
                credxxResultDto.setCode(EpbEnum.EPB099999.key);
                credxxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, e.getMessage());
            credxxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            credxxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        credxxResultDto.setData(credxxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDXX.key, EsbEnum.TRADE_CODE_CREDXX.value, JSON.toJSONString(credxxResultDto));
        return credxxResultDto;
    }
}
