package cn.com.yusys.yusp.web.server.biz.xdxw0057;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0057.req.Xdxw0057ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0057.resp.Xdxw0057RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.req.Xdxw0057DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0057.resp.Xdxw0057DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据核心客户号查询经营性贷款批复额度
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0057:根据核心客户号查询经营性贷款批复额度")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0057Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0057Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0057
     * 交易描述：根据核心客户号查询经营性贷款批复额度
     *
     * @param xdxw0057ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询经营性贷款批复额度")
    @PostMapping("/xdxw0057")
    //@Idempotent({"xdcaxw0057", "#xdxw0057ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0057RespDto xdxw0057(@Validated @RequestBody Xdxw0057ReqDto xdxw0057ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057ReqDto));
        Xdxw0057DataReqDto xdxw0057DataReqDto = new Xdxw0057DataReqDto();// 请求Data： 根据核心客户号查询经营性贷款批复额度
        Xdxw0057DataRespDto xdxw0057DataRespDto = new Xdxw0057DataRespDto();// 响应Data：根据核心客户号查询经营性贷款批复额度
        Xdxw0057RespDto xdxw0057RespDto = new Xdxw0057RespDto();// 响应Data：根据核心客户号查询经营性贷款批复额度

        cn.com.yusys.yusp.dto.server.biz.xdxw0057.req.Data reqData = null; // 请求Data：根据核心客户号查询经营性贷款批复额度
        cn.com.yusys.yusp.dto.server.biz.xdxw0057.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0057.resp.Data();// 响应Data：根据核心客户号查询经营性贷款批复额度
        try {
            // 从 xdxw0057ReqDto获取 reqData
            reqData = xdxw0057ReqDto.getData();
            // 将 reqData 拷贝到xdxw0057DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0057DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057DataReqDto));
            ResultDto<Xdxw0057DataRespDto> xdxw0057DataResultDto = dscmsBizXwClientService.xdxw0057(xdxw0057DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0057RespDto.setErorcd(Optional.ofNullable(xdxw0057DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0057RespDto.setErortx(Optional.ofNullable(xdxw0057DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0057DataResultDto.getCode())) {
                xdxw0057RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0057RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0057DataRespDto = xdxw0057DataResultDto.getData();
                // 将 xdxw0057DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0057DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, e.getMessage());
            xdxw0057RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0057RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0057RespDto.setDatasq(servsq);
        xdxw0057RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0057.key, DscmsEnum.TRADE_CODE_XDXW0057.value, JSON.toJSONString(xdxw0057RespDto));
        return xdxw0057RespDto;
    }
}
