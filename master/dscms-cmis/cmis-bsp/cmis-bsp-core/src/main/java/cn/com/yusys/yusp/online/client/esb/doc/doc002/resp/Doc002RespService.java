package cn.com.yusys.yusp.online.client.esb.doc.doc002.resp;

/**
 * 响应Service：入库查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Doc002RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Doc002RespService{" +
                "service=" + service +
                '}';
    }
}
