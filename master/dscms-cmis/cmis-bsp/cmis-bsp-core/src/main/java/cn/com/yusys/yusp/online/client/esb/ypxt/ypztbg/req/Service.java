package cn.com.yusys.yusp.online.client.esb.ypxt.ypztbg.req;

/**
 * 请求Service：押品状态变更
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:51
 */
public class Service {

    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq;//	全局流水	否	char(5)	是		datasq
    private String areano;// 区域编码
    private String cfstat;// 查封状态
    private String bdcnoo;// 不动产权证书号
    private String cftime;// 查封时间
    private String ypstat;// 最高可抵押顺位

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getAreano() {
        return areano;
    }

    public void setAreano(String areano) {
        this.areano = areano;
    }

    public String getCfstat() {
        return cfstat;
    }

    public void setCfstat(String cfstat) {
        this.cfstat = cfstat;
    }

    public String getBdcnoo() {
        return bdcnoo;
    }

    public void setBdcnoo(String bdcnoo) {
        this.bdcnoo = bdcnoo;
    }

    public String getCftime() {
        return cftime;
    }

    public void setCftime(String cftime) {
        this.cftime = cftime;
    }

    public String getYpstat() {
        return ypstat;
    }

    public void setYpstat(String ypstat) {
        this.ypstat = ypstat;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", areano='" + areano + '\'' +
                ", cfstat='" + cfstat + '\'' +
                ", bdcnoo='" + bdcnoo + '\'' +
                ", cftime='" + cftime + '\'' +
                ", ypstat='" + ypstat + '\'' +
                '}';
    }
}
