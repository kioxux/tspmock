package cn.com.yusys.yusp.web.client.http.xwywglpt.xwd009;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req.Xwd009ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp.Xwd009RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.req.Xwd009ReqService;
import cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.resp.Xwd009RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装新微贷平台接口处理类(xwd009)")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Xwd009Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xwd009Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xwd009
     * 交易描述：利率申请接口
     *
     * @param xwd009ReqDto
     * @return
     * @throws Exception
     * @modify 改成走esb交易
     */
    @PostMapping("/xwd009")
    protected @ResponseBody
    ResultDto<Xwd009RespDto> xwd009(@Validated @RequestBody Xwd009ReqDto xwd009ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value, JSON.toJSONString(xwd009ReqDto));
        cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.req.Service reqService = new cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.req.Service();
        cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.resp.Service respService = new cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.resp.Service();
        Xwd009ReqService xwd009ReqService = new Xwd009ReqService();
        Xwd009RespService xwd009RespService = new Xwd009RespService();
        Xwd009RespDto xwd009RespDto = new Xwd009RespDto();
        ResultDto<Xwd009RespDto> xwd009ResultDto = new ResultDto<Xwd009RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xwd009ReqDto转换成reqService
            BeanUtils.copyProperties(xwd009ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_XWD009.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_XWD.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWD.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            xwd009ReqService.setService(reqService);
            // 将xwd009ReqService转换成xwd009ReqServiceMap
            Map xwd009ReqServiceMap = beanMapUtil.beanToMap(xwd009ReqService);
            context.put("tradeDataMap", xwd009ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XWD009.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xwd009RespService = beanMapUtil.mapToBean(tradeDataMap, Xwd009RespService.class, Xwd009RespService.class);
            respService = xwd009RespService.getService();

            xwd009ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xwd009ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xwd009RespDto
                BeanUtils.copyProperties(respService, xwd009RespDto);
                xwd009ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xwd009ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xwd009ResultDto.setCode(EpbEnum.EPB099999.key);
                xwd009ResultDto.setMessage(respService.getMessage());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value, e.getMessage());
            xwd009ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xwd009ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xwd009ResultDto.setData(xwd009RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD009.key, EsbEnum.TRADE_CODE_XWD009.value, JSON.toJSONString(xwd009ResultDto));
        return xwd009ResultDto;
    }
}
