package cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp;
/**
 * 响应Service：信用证信息同步
 * @author zhugenrong
 * @version 1.0
 */
public class Record {
    private String guar_no;//押品统一编号
    public String  getGuar_no() { return guar_no; }
    public void setGuar_no(String guar_no ) { this.guar_no = guar_no;}
    @Override
    public String toString() {
        return "Service{" +
                "guar_no='" + guar_no+ '\'' +
                '}';
    }
}
