package cn.com.yusys.yusp.web.client.esb.core.dp2099;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.dp2099.req.Dp2099ReqService;
import cn.com.yusys.yusp.online.client.esb.core.dp2099.req.Service;
import cn.com.yusys.yusp.online.client.esb.core.dp2099.resp.Dp2099RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:保证金账户查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(dp2099)")
@RestController
@RequestMapping("/api/dscms2coredp")
public class Dscms2Dp2099Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dp2099Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");

    /**
     * 交易码：dp2099
     * 交易描述：保证金账户查询
     *
     * @param dp2099ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("dp2099:保证金账户查询")
    @PostMapping("/dp2099")
    protected @ResponseBody
    ResultDto<Dp2099RespDto> dp2099(@Validated @RequestBody Dp2099ReqDto dp2099ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2099.key, EsbEnum.TRADE_CODE_DP2099.value, JSON.toJSONString(dp2099ReqDto));
        Service reqService = new Service();
        cn.com.yusys.yusp.online.client.esb.core.dp2099.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.dp2099.resp.Service();
        Dp2099ReqService dp2099ReqService = new Dp2099ReqService();
        Dp2099RespService dp2099RespService = new Dp2099RespService();
        Dp2099RespDto dp2099RespDto = new Dp2099RespDto();
        ResultDto<Dp2099RespDto> dp2099ResultDto = new ResultDto<Dp2099RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将dp2099ReqDto转换成reqService
            BeanUtils.copyProperties(dp2099ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DP2099.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            dp2099ReqService.setService(reqService);
            // 将dp2099ReqService转换成dp2099ReqServiceMap
            Map dp2099ReqServiceMap = beanMapUtil.beanToMap(dp2099ReqService);
            context.put("tradeDataMap", dp2099ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DP2099.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            dp2099RespService = beanMapUtil.mapToBean(tradeDataMap, Dp2099RespService.class, Dp2099RespService.class);
            respService = dp2099RespService.getService();
            //  将respService转换成Dp2099RespDto
            dp2099ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            dp2099ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                BeanUtils.copyProperties(respService, dp2099RespDto);
                cn.com.yusys.yusp.online.client.esb.core.dp2099.resp.List dp2099RespList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.dp2099.resp.List());
                respService.setList(dp2099RespList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.dp2099.resp.Record> dp2099RespRecords = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List> dp2099RespDtoLists = dp2099RespRecords.parallelStream().map(dp2099RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List dp2099RespDtoList = new cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.List();
                        BeanUtils.copyProperties(dp2099RespRecord, dp2099RespDtoList);
                        return dp2099RespDtoList;
                    }).collect(Collectors.toList());
                    dp2099RespDto.setList(dp2099RespDtoLists);
                }
                dp2099ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                dp2099ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                dp2099ResultDto.setCode(EpbEnum.EPB099999.key);
                dp2099ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2099.key, EsbEnum.TRADE_CODE_DP2099.value, e.getMessage());
            dp2099ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            dp2099ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }

        dp2099ResultDto.setData(dp2099RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2099.key, EsbEnum.TRADE_CODE_DP2099.value, JSON.toJSONString(dp2099ResultDto));
        return dp2099ResultDto;
    }
}
