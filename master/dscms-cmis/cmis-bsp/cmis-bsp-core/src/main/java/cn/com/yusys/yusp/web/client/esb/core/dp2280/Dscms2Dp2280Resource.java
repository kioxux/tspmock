package cn.com.yusys.yusp.web.client.esb.core.dp2280;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2280.Dp2280ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2280.Dp2280RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.dp2280.req.Dp2280ReqService;
import cn.com.yusys.yusp.online.client.esb.core.dp2280.req.Service;
import cn.com.yusys.yusp.online.client.esb.core.dp2280.resp.Dp2280RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(dp2280)")
@RestController
@RequestMapping("/api/dscms2coredp")
public class Dscms2Dp2280Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Dp2280Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 子账户序号查询（处理码dp2280）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("dp2280:子账户序号查询")
    @PostMapping("/dp2280")
    protected @ResponseBody
    ResultDto<Dp2280RespDto> dp2280(@Validated @RequestBody Dp2280ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2280.key, EsbEnum.TRADE_CODE_DP2280.value, JSON.toJSONString(reqDto));
        Service reqService = new Service();
        cn.com.yusys.yusp.online.client.esb.core.dp2280.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.dp2280.resp.Service();
        Dp2280ReqService dp2280ReqService = new Dp2280ReqService();
        Dp2280RespService dp2280RespService = new Dp2280RespService();
        Dp2280RespDto dp2280RespDto = new Dp2280RespDto();
        ResultDto<Dp2280RespDto> dp2280ResultDto = new ResultDto<Dp2280RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Dp2280ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);


            reqService.setPrcscd(EsbEnum.TRADE_CODE_DP2280.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            dp2280ReqService.setService(reqService);
            // 将dp2280ReqService转换成dp2280ReqServiceMap
            Map dp2280ReqServiceMap = beanMapUtil.beanToMap(dp2280ReqService);
            context.put("tradeDataMap", dp2280ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2280.key, EsbEnum.TRADE_CODE_DP2280.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DP2280.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2280.key, EsbEnum.TRADE_CODE_DP2280.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            dp2280RespService = beanMapUtil.mapToBean(tradeDataMap, Dp2280RespService.class, Dp2280RespService.class);
            respService = dp2280RespService.getService();

            //  将Dp2280RespDto封装到ResultDto<Dp2280RespDto>
            dp2280ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            dp2280ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Dp2280RespDto
                BeanUtils.copyProperties(respService, dp2280RespDto);
                dp2280ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                dp2280ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                dp2280ResultDto.setCode(EpbEnum.EPB099999.key);
                dp2280ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2280.key, EsbEnum.TRADE_CODE_DP2280.value, e.getMessage());
            dp2280ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            dp2280ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }

        dp2280ResultDto.setData(dp2280RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2280.key, EsbEnum.TRADE_CODE_DP2280.value, JSON.toJSONString(dp2280ResultDto));
        return dp2280ResultDto;
    }
}
