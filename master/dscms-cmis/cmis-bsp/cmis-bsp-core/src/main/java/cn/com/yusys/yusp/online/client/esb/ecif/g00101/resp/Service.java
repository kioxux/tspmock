package cn.com.yusys.yusp.online.client.esb.ecif.g00101.resp;

import java.math.BigDecimal;

/**
 * 响应Service：对公客户综合信息查询
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:53
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String custno;//客户编号
    private String custna;//客户名称
    private String csstna;//客户简称
    private String csenna;//客户英文名称
    private String custst;//客户状态
    private String custtp;//客户类型
    private String custsb;//对公客户类型细分
    private String idtftp;//证件类型
    private String idtfno;//证件号码
    private String efctdt;//证件生效日期
    private String inefdt;//证件失效日期
    private String areaad;//发证机关国家或地区
    private String depart;//发证机关
    private String gvrgno;//营业执照号码
    private String gvbgdt;//营业执照生效日期
    private String gveddt;//营业执照失效日期
    private String gvtion;//营业执照发证机关
    private String cropcd;//组织机构代码
    private String oreddt;//组织机构代码失效日期
    private String nttxno;//税务证件号
    private String swbgdt;//税务证件生效日期
    private String sweddt;//税务证件失效日期
    private String swtion;//税务证件发证机关
    private String jgxyno;//机构信用代码证
    private String cropdt;//机构信用代码证失效日期
    private String opcfno;//开户许可证
    private String foundt;//成立日期
    private String corppr;//行业类别
    private String coppbc;//人行行业类别
    private String entetp;//产业分类
    private String corpnu;//企业性质
    private String blogto;//隶属关系
    private String propts;//居民性质
    private String finctp;//控股类型（出资人经济类型）
    private String invesj;//投资主体
    private String regitp;//登记注册类型
    private String regidt;//注册日期
    private String regicd;//注册地国家代码
    private String regchn;//人民银行注册地区代码
    private String rgctry;//注册地行政区划
    private String rgcrcy;//注册资本币别
    private BigDecimal rgcpam;//注册资本金额
    private String cacrcy;//实收资本币别
    private BigDecimal cacpam;//实收资本金额
    private String orgatp;//组织机构类型
    private String spectp;//特殊经济区类型
    private String busstp;//企业经济类型
    private String naticd;//国民经济部门
    private String debttp;//境内主体类型
    private String envirt;//企业环保级别
    private String finacd;//金融机构类型代码
    private String swifcd;//SWIFT编号
    private String spntcd;//该SPV或壳机构所属国家/地区
    private String admnti;//经营期限
    private String admnst;//经营状态
    private String corpsz;//企业规模
    private String busisp;//经营范围
    private String acesbs;//客户兼营业务
    private Integer emplnm;//员工总数
    private String comput;//主管单位
    private String upcrps;//主管单位法人名称
    private String upidtp;//主管单位法人证件类型
    private String upidno;//主管单位法人证件号码
    private String loanno;//贷款卡编码
    private BigDecimal wkflar;//经营场地面积
    private String wkflfe;//经营场地所有权
    private String baseno;//基本账户核准号
    private String acctnm;//基本账户开户行名称
    private String acctbr;//基本账户开户行号
    private String acctno;//基本账户账号
    private String acctdt;//基本账户开户日期
    private String depotp;//存款人类别
    private String rhtype;//人行统计分类
    private BigDecimal sumast;//公司总资产
    private BigDecimal netast;//公司净资产
    private String sthdfg;//是否我行股东
    private BigDecimal sharhd;//拥有我行股份金额
    private String listtg;//是否上市企业
    private String listld;//上市地
    private String stoctp;//股票类别
    private String stoccd;//股票代码
    private BigDecimal onacnm;//一类户数量
    private BigDecimal totlnm;//总户数
    private String grpctg;//是否集团客户
    private String reletg;//是否是我行关联客户
    private String famltg;//是否家族企业
    private String hightg;//是否高新技术企业
    private String isbdcp;//是否是担保公司
    private String spcltg;//是否为特殊经济区内企业
    private String arimtg;//是否地区重点企业
    private String finatg;//是否融资类企业
    private String resutg;//是否国资委所属企业
    private String sctlpr;//是否国家宏观调控限控行业
    private String farmtg;//是否农户
    private String hgegtg;//是否高能耗企业
    private String exegtg;//是否产能过剩企业
    private String elmntg;//是否属于淘汰产能目录
    private String envifg;//是否环保企业
    private String hgplcp;//是否高污染企业
    private String istdcs;//是否是我行贸易融资客户
    private String spclfg;//特种经营标识
    private String steltg;//是否钢贸企业
    private String domitg;//是否优势企业
    private String strutp;//产业结构调整类型
    private String stratp;//战略新兴产业类型
    private String indutg;//工业转型升级标识
    private String leadtg;//是否龙头企业
    private String cncatg;//中资标志
    private String impotg;//进出口权标志
    private String clostg;//企业关停标志
    private String plattg;//是否苏州综合平台企业
    private String fibrtg;//是否为工业园区、经济开发区等行政管理区
    private String gvfntg;//是否政府投融资平台
    private String gvlnlv;//地方政府融资平台隶属关系
    private String gvlwtp;//地方政府融资平台法律性质
    private String gvlntp;//政府融资贷款类型
    private String petytg;//是否小额贷款公司
    private String fscddt;//首次与我行建立信贷关系时间
    private String supptg;//是否列入重点扶持产业
    private String licetg;//是否一照一码
    private String landtg;//是否土地整治机构
    private String pasvfg;//是否消极非金融机构
    private String imagno;//影像编号
    private String taxptp;//纳税人身份
    private String needfp;//是否需要开具增值税专用发票
    private String taxpna;//纳税人全称
    private String taxpad;//纳税人地址
    private String taxptl;//纳税人电话
    private String txbkna;//纳税人行名
    private String taxpac;//纳税人账号
    private String txpstp;//税收居民类型
    private String taxadr;//税收单位英文地址
    private String txnion;//税收居民国（地区）
    private String txennm;//税收英文姓名
    private String txbcty;//税收出生国家或地区
    private String taxnum;//纳税人识别号
    private String notxrs;//不能提供纳税人识别号原因
    private String ctigno;//客户经理号
    private String ctigna;//客户经理姓名
    private String ctigph;//客户经理手机号
    private String corptl;//公司电话
    private String hometl;//联系电话
    private String rgpost;//注册地址邮编
    private String rgcuty;//注册地址所在国家
    private String rgprov;//注册地址所在省
    private String rgcity;//注册地址所在市
    private String rgerea;//注册地址所在区
    private String regadr;//注册地址
    private String bspost;//经营地址邮编
    private String bscuty;//经营地址所在国家
    private String bsprov;//经营地址所在省
    private String bscity;//经营地址所在市
    private String bserea;//经营地址所在区
    private String busiad;//经营地址
    private String enaddr;//英文地址
    private String websit;//公司网址
    private String faxfax;//传真
    private String emailx;//邮箱
    private String sorgno;//同业机构(行)号
    private String sorgtp;//同业机构(行)类型
    private String orgsit;//同业机构(行)网址
    private String bkplic;//同业客户金融业务许可证
    private String reguno;//同业非现场监管统计机构编码
    private BigDecimal pdcpat;//同业客户实际到位资金(万元)
    private String sorglv;//同业授信评估等级
    private String evalda;//同业客户评级到期日期
    private String reldgr;//同业客户与我行合作关系
    private String limind;//同业客户是否授信
    private String mabrid;//同业客户主管机构(同业)
    private String manmgr;//同业客户主管客户经理(同业)
    private String briskc;//同业客户风险大类
    private String mriskc;//同业客户风险种类
    private String friskc;//同业客户风险小类
    private String riskda;//同业客户风险划分日期
    private String chflag;//境内标识
    private String nnjytp;//交易类型
    private String nnjyna;//交易名称
    private String uporgco;//主管单位法人组织机构代码证
    private String upbasano;//主管单位法人基本存款账号编号
    private String upatt;//主管单位法人代表属性
    //    private String sorgfname;//同业机构全称
//    private String sorgtpj;//同业机构类型1（检测表）
//    private String sorgtpg;//同业机构类型2（G24）
    private String twcttp;//城乡类型
    private String corptp;//企业类型
    private String ecegra;//信用等级（外部）
    private String eratdt;//评定日期（外部）
    private String eraorg;//评定机构（外部）
    private String postle;//职称（新信贷）
    private String emptdt;//从业日期
    private String penwrt;//个人净资产
    private String lolvct;//本地居住状况

    private RelArrayInfo relArrayInfo;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCsstna() {
        return csstna;
    }

    public void setCsstna(String csstna) {
        this.csstna = csstna;
    }

    public String getCsenna() {
        return csenna;
    }

    public void setCsenna(String csenna) {
        this.csenna = csenna;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    public String getCusttp() {
        return custtp;
    }

    public void setCusttp(String custtp) {
        this.custtp = custtp;
    }

    public String getCustsb() {
        return custsb;
    }

    public void setCustsb(String custsb) {
        this.custsb = custsb;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getEfctdt() {
        return efctdt;
    }

    public void setEfctdt(String efctdt) {
        this.efctdt = efctdt;
    }

    public String getInefdt() {
        return inefdt;
    }

    public void setInefdt(String inefdt) {
        this.inefdt = inefdt;
    }

    public String getAreaad() {
        return areaad;
    }

    public void setAreaad(String areaad) {
        this.areaad = areaad;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getGvrgno() {
        return gvrgno;
    }

    public void setGvrgno(String gvrgno) {
        this.gvrgno = gvrgno;
    }

    public String getGvbgdt() {
        return gvbgdt;
    }

    public void setGvbgdt(String gvbgdt) {
        this.gvbgdt = gvbgdt;
    }

    public String getGveddt() {
        return gveddt;
    }

    public void setGveddt(String gveddt) {
        this.gveddt = gveddt;
    }

    public String getGvtion() {
        return gvtion;
    }

    public void setGvtion(String gvtion) {
        this.gvtion = gvtion;
    }

    public String getCropcd() {
        return cropcd;
    }

    public void setCropcd(String cropcd) {
        this.cropcd = cropcd;
    }

    public String getOreddt() {
        return oreddt;
    }

    public void setOreddt(String oreddt) {
        this.oreddt = oreddt;
    }

    public String getNttxno() {
        return nttxno;
    }

    public void setNttxno(String nttxno) {
        this.nttxno = nttxno;
    }

    public String getSwbgdt() {
        return swbgdt;
    }

    public void setSwbgdt(String swbgdt) {
        this.swbgdt = swbgdt;
    }

    public String getSweddt() {
        return sweddt;
    }

    public void setSweddt(String sweddt) {
        this.sweddt = sweddt;
    }

    public String getSwtion() {
        return swtion;
    }

    public void setSwtion(String swtion) {
        this.swtion = swtion;
    }

    public String getJgxyno() {
        return jgxyno;
    }

    public void setJgxyno(String jgxyno) {
        this.jgxyno = jgxyno;
    }

    public String getCropdt() {
        return cropdt;
    }

    public void setCropdt(String cropdt) {
        this.cropdt = cropdt;
    }

    public String getOpcfno() {
        return opcfno;
    }

    public void setOpcfno(String opcfno) {
        this.opcfno = opcfno;
    }

    public String getFoundt() {
        return foundt;
    }

    public void setFoundt(String foundt) {
        this.foundt = foundt;
    }

    public String getCorppr() {
        return corppr;
    }

    public void setCorppr(String corppr) {
        this.corppr = corppr;
    }

    public String getCoppbc() {
        return coppbc;
    }

    public void setCoppbc(String coppbc) {
        this.coppbc = coppbc;
    }

    public String getEntetp() {
        return entetp;
    }

    public void setEntetp(String entetp) {
        this.entetp = entetp;
    }

    public String getCorpnu() {
        return corpnu;
    }

    public void setCorpnu(String corpnu) {
        this.corpnu = corpnu;
    }

    public String getBlogto() {
        return blogto;
    }

    public void setBlogto(String blogto) {
        this.blogto = blogto;
    }

    public String getPropts() {
        return propts;
    }

    public void setPropts(String propts) {
        this.propts = propts;
    }

    public String getFinctp() {
        return finctp;
    }

    public void setFinctp(String finctp) {
        this.finctp = finctp;
    }

    public String getInvesj() {
        return invesj;
    }

    public void setInvesj(String invesj) {
        this.invesj = invesj;
    }

    public String getRegitp() {
        return regitp;
    }

    public void setRegitp(String regitp) {
        this.regitp = regitp;
    }

    public String getRegidt() {
        return regidt;
    }

    public void setRegidt(String regidt) {
        this.regidt = regidt;
    }

    public String getRegicd() {
        return regicd;
    }

    public void setRegicd(String regicd) {
        this.regicd = regicd;
    }

    public String getRegchn() {
        return regchn;
    }

    public void setRegchn(String regchn) {
        this.regchn = regchn;
    }

    public String getRgctry() {
        return rgctry;
    }

    public void setRgctry(String rgctry) {
        this.rgctry = rgctry;
    }

    public String getRgcrcy() {
        return rgcrcy;
    }

    public void setRgcrcy(String rgcrcy) {
        this.rgcrcy = rgcrcy;
    }

    public BigDecimal getRgcpam() {
        return rgcpam;
    }

    public void setRgcpam(BigDecimal rgcpam) {
        this.rgcpam = rgcpam;
    }

    public String getCacrcy() {
        return cacrcy;
    }

    public void setCacrcy(String cacrcy) {
        this.cacrcy = cacrcy;
    }

    public BigDecimal getCacpam() {
        return cacpam;
    }

    public void setCacpam(BigDecimal cacpam) {
        this.cacpam = cacpam;
    }

    public String getOrgatp() {
        return orgatp;
    }

    public void setOrgatp(String orgatp) {
        this.orgatp = orgatp;
    }

    public String getSpectp() {
        return spectp;
    }

    public void setSpectp(String spectp) {
        this.spectp = spectp;
    }

    public String getBusstp() {
        return busstp;
    }

    public void setBusstp(String busstp) {
        this.busstp = busstp;
    }

    public String getNaticd() {
        return naticd;
    }

    public void setNaticd(String naticd) {
        this.naticd = naticd;
    }

    public String getDebttp() {
        return debttp;
    }

    public void setDebttp(String debttp) {
        this.debttp = debttp;
    }

    public String getEnvirt() {
        return envirt;
    }

    public void setEnvirt(String envirt) {
        this.envirt = envirt;
    }

    public String getFinacd() {
        return finacd;
    }

    public void setFinacd(String finacd) {
        this.finacd = finacd;
    }

    public String getSwifcd() {
        return swifcd;
    }

    public void setSwifcd(String swifcd) {
        this.swifcd = swifcd;
    }

    public String getSpntcd() {
        return spntcd;
    }

    public void setSpntcd(String spntcd) {
        this.spntcd = spntcd;
    }

    public String getAdmnti() {
        return admnti;
    }

    public void setAdmnti(String admnti) {
        this.admnti = admnti;
    }

    public String getAdmnst() {
        return admnst;
    }

    public void setAdmnst(String admnst) {
        this.admnst = admnst;
    }

    public String getCorpsz() {
        return corpsz;
    }

    public void setCorpsz(String corpsz) {
        this.corpsz = corpsz;
    }

    public String getBusisp() {
        return busisp;
    }

    public void setBusisp(String busisp) {
        this.busisp = busisp;
    }

    public String getAcesbs() {
        return acesbs;
    }

    public void setAcesbs(String acesbs) {
        this.acesbs = acesbs;
    }

    public Integer getEmplnm() {
        return emplnm;
    }

    public void setEmplnm(Integer emplnm) {
        this.emplnm = emplnm;
    }

    public String getComput() {
        return comput;
    }

    public void setComput(String comput) {
        this.comput = comput;
    }

    public String getUpcrps() {
        return upcrps;
    }

    public void setUpcrps(String upcrps) {
        this.upcrps = upcrps;
    }

    public String getUpidtp() {
        return upidtp;
    }

    public void setUpidtp(String upidtp) {
        this.upidtp = upidtp;
    }

    public String getUpidno() {
        return upidno;
    }

    public void setUpidno(String upidno) {
        this.upidno = upidno;
    }

    public String getLoanno() {
        return loanno;
    }

    public void setLoanno(String loanno) {
        this.loanno = loanno;
    }

    public BigDecimal getWkflar() {
        return wkflar;
    }

    public void setWkflar(BigDecimal wkflar) {
        this.wkflar = wkflar;
    }

    public String getWkflfe() {
        return wkflfe;
    }

    public void setWkflfe(String wkflfe) {
        this.wkflfe = wkflfe;
    }

    public String getBaseno() {
        return baseno;
    }

    public void setBaseno(String baseno) {
        this.baseno = baseno;
    }

    public String getAcctnm() {
        return acctnm;
    }

    public void setAcctnm(String acctnm) {
        this.acctnm = acctnm;
    }

    public String getAcctbr() {
        return acctbr;
    }

    public void setAcctbr(String acctbr) {
        this.acctbr = acctbr;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctdt() {
        return acctdt;
    }

    public void setAcctdt(String acctdt) {
        this.acctdt = acctdt;
    }

    public String getDepotp() {
        return depotp;
    }

    public void setDepotp(String depotp) {
        this.depotp = depotp;
    }

    public String getRhtype() {
        return rhtype;
    }

    public void setRhtype(String rhtype) {
        this.rhtype = rhtype;
    }

    public BigDecimal getSumast() {
        return sumast;
    }

    public void setSumast(BigDecimal sumast) {
        this.sumast = sumast;
    }

    public BigDecimal getNetast() {
        return netast;
    }

    public void setNetast(BigDecimal netast) {
        this.netast = netast;
    }

    public String getSthdfg() {
        return sthdfg;
    }

    public void setSthdfg(String sthdfg) {
        this.sthdfg = sthdfg;
    }

    public BigDecimal getSharhd() {
        return sharhd;
    }

    public void setSharhd(BigDecimal sharhd) {
        this.sharhd = sharhd;
    }

    public String getListtg() {
        return listtg;
    }

    public void setListtg(String listtg) {
        this.listtg = listtg;
    }

    public String getListld() {
        return listld;
    }

    public void setListld(String listld) {
        this.listld = listld;
    }

    public String getStoctp() {
        return stoctp;
    }

    public void setStoctp(String stoctp) {
        this.stoctp = stoctp;
    }

    public String getStoccd() {
        return stoccd;
    }

    public void setStoccd(String stoccd) {
        this.stoccd = stoccd;
    }

    public BigDecimal getOnacnm() {
        return onacnm;
    }

    public void setOnacnm(BigDecimal onacnm) {
        this.onacnm = onacnm;
    }

    public BigDecimal getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(BigDecimal totlnm) {
        this.totlnm = totlnm;
    }

    public String getGrpctg() {
        return grpctg;
    }

    public void setGrpctg(String grpctg) {
        this.grpctg = grpctg;
    }

    public String getReletg() {
        return reletg;
    }

    public void setReletg(String reletg) {
        this.reletg = reletg;
    }

    public String getFamltg() {
        return famltg;
    }

    public void setFamltg(String famltg) {
        this.famltg = famltg;
    }

    public String getHightg() {
        return hightg;
    }

    public void setHightg(String hightg) {
        this.hightg = hightg;
    }

    public String getIsbdcp() {
        return isbdcp;
    }

    public void setIsbdcp(String isbdcp) {
        this.isbdcp = isbdcp;
    }

    public String getSpcltg() {
        return spcltg;
    }

    public void setSpcltg(String spcltg) {
        this.spcltg = spcltg;
    }

    public String getArimtg() {
        return arimtg;
    }

    public void setArimtg(String arimtg) {
        this.arimtg = arimtg;
    }

    public String getFinatg() {
        return finatg;
    }

    public void setFinatg(String finatg) {
        this.finatg = finatg;
    }

    public String getResutg() {
        return resutg;
    }

    public void setResutg(String resutg) {
        this.resutg = resutg;
    }

    public String getSctlpr() {
        return sctlpr;
    }

    public void setSctlpr(String sctlpr) {
        this.sctlpr = sctlpr;
    }

    public String getFarmtg() {
        return farmtg;
    }

    public void setFarmtg(String farmtg) {
        this.farmtg = farmtg;
    }

    public String getHgegtg() {
        return hgegtg;
    }

    public void setHgegtg(String hgegtg) {
        this.hgegtg = hgegtg;
    }

    public String getExegtg() {
        return exegtg;
    }

    public void setExegtg(String exegtg) {
        this.exegtg = exegtg;
    }

    public String getElmntg() {
        return elmntg;
    }

    public void setElmntg(String elmntg) {
        this.elmntg = elmntg;
    }

    public String getEnvifg() {
        return envifg;
    }

    public void setEnvifg(String envifg) {
        this.envifg = envifg;
    }

    public String getHgplcp() {
        return hgplcp;
    }

    public void setHgplcp(String hgplcp) {
        this.hgplcp = hgplcp;
    }

    public String getIstdcs() {
        return istdcs;
    }

    public void setIstdcs(String istdcs) {
        this.istdcs = istdcs;
    }

    public String getSpclfg() {
        return spclfg;
    }

    public void setSpclfg(String spclfg) {
        this.spclfg = spclfg;
    }

    public String getSteltg() {
        return steltg;
    }

    public void setSteltg(String steltg) {
        this.steltg = steltg;
    }

    public String getDomitg() {
        return domitg;
    }

    public void setDomitg(String domitg) {
        this.domitg = domitg;
    }

    public String getStrutp() {
        return strutp;
    }

    public void setStrutp(String strutp) {
        this.strutp = strutp;
    }

    public String getStratp() {
        return stratp;
    }

    public void setStratp(String stratp) {
        this.stratp = stratp;
    }

    public String getIndutg() {
        return indutg;
    }

    public void setIndutg(String indutg) {
        this.indutg = indutg;
    }

    public String getLeadtg() {
        return leadtg;
    }

    public void setLeadtg(String leadtg) {
        this.leadtg = leadtg;
    }

    public String getCncatg() {
        return cncatg;
    }

    public void setCncatg(String cncatg) {
        this.cncatg = cncatg;
    }

    public String getImpotg() {
        return impotg;
    }

    public void setImpotg(String impotg) {
        this.impotg = impotg;
    }

    public String getClostg() {
        return clostg;
    }

    public void setClostg(String clostg) {
        this.clostg = clostg;
    }

    public String getPlattg() {
        return plattg;
    }

    public void setPlattg(String plattg) {
        this.plattg = plattg;
    }

    public String getFibrtg() {
        return fibrtg;
    }

    public void setFibrtg(String fibrtg) {
        this.fibrtg = fibrtg;
    }

    public String getGvfntg() {
        return gvfntg;
    }

    public void setGvfntg(String gvfntg) {
        this.gvfntg = gvfntg;
    }

    public String getGvlnlv() {
        return gvlnlv;
    }

    public void setGvlnlv(String gvlnlv) {
        this.gvlnlv = gvlnlv;
    }

    public String getGvlwtp() {
        return gvlwtp;
    }

    public void setGvlwtp(String gvlwtp) {
        this.gvlwtp = gvlwtp;
    }

    public String getGvlntp() {
        return gvlntp;
    }

    public void setGvlntp(String gvlntp) {
        this.gvlntp = gvlntp;
    }

    public String getPetytg() {
        return petytg;
    }

    public void setPetytg(String petytg) {
        this.petytg = petytg;
    }

    public String getFscddt() {
        return fscddt;
    }

    public void setFscddt(String fscddt) {
        this.fscddt = fscddt;
    }

    public String getSupptg() {
        return supptg;
    }

    public void setSupptg(String supptg) {
        this.supptg = supptg;
    }

    public String getLicetg() {
        return licetg;
    }

    public void setLicetg(String licetg) {
        this.licetg = licetg;
    }

    public String getLandtg() {
        return landtg;
    }

    public void setLandtg(String landtg) {
        this.landtg = landtg;
    }

    public String getPasvfg() {
        return pasvfg;
    }

    public void setPasvfg(String pasvfg) {
        this.pasvfg = pasvfg;
    }

    public String getImagno() {
        return imagno;
    }

    public void setImagno(String imagno) {
        this.imagno = imagno;
    }

    public String getTaxptp() {
        return taxptp;
    }

    public void setTaxptp(String taxptp) {
        this.taxptp = taxptp;
    }

    public String getNeedfp() {
        return needfp;
    }

    public void setNeedfp(String needfp) {
        this.needfp = needfp;
    }

    public String getTaxpna() {
        return taxpna;
    }

    public void setTaxpna(String taxpna) {
        this.taxpna = taxpna;
    }

    public String getTaxpad() {
        return taxpad;
    }

    public void setTaxpad(String taxpad) {
        this.taxpad = taxpad;
    }

    public String getTaxptl() {
        return taxptl;
    }

    public void setTaxptl(String taxptl) {
        this.taxptl = taxptl;
    }

    public String getTxbkna() {
        return txbkna;
    }

    public void setTxbkna(String txbkna) {
        this.txbkna = txbkna;
    }

    public String getTaxpac() {
        return taxpac;
    }

    public void setTaxpac(String taxpac) {
        this.taxpac = taxpac;
    }

    public String getTxpstp() {
        return txpstp;
    }

    public void setTxpstp(String txpstp) {
        this.txpstp = txpstp;
    }

    public String getTaxadr() {
        return taxadr;
    }

    public void setTaxadr(String taxadr) {
        this.taxadr = taxadr;
    }

    public String getTxnion() {
        return txnion;
    }

    public void setTxnion(String txnion) {
        this.txnion = txnion;
    }

    public String getTxennm() {
        return txennm;
    }

    public void setTxennm(String txennm) {
        this.txennm = txennm;
    }

    public String getTxbcty() {
        return txbcty;
    }

    public void setTxbcty(String txbcty) {
        this.txbcty = txbcty;
    }

    public String getTaxnum() {
        return taxnum;
    }

    public void setTaxnum(String taxnum) {
        this.taxnum = taxnum;
    }

    public String getNotxrs() {
        return notxrs;
    }

    public void setNotxrs(String notxrs) {
        this.notxrs = notxrs;
    }

    public String getCtigno() {
        return ctigno;
    }

    public void setCtigno(String ctigno) {
        this.ctigno = ctigno;
    }

    public String getCtigna() {
        return ctigna;
    }

    public void setCtigna(String ctigna) {
        this.ctigna = ctigna;
    }

    public String getCtigph() {
        return ctigph;
    }

    public void setCtigph(String ctigph) {
        this.ctigph = ctigph;
    }

    public String getCorptl() {
        return corptl;
    }

    public void setCorptl(String corptl) {
        this.corptl = corptl;
    }

    public String getHometl() {
        return hometl;
    }

    public void setHometl(String hometl) {
        this.hometl = hometl;
    }

    public String getRgpost() {
        return rgpost;
    }

    public void setRgpost(String rgpost) {
        this.rgpost = rgpost;
    }

    public String getRgcuty() {
        return rgcuty;
    }

    public void setRgcuty(String rgcuty) {
        this.rgcuty = rgcuty;
    }

    public String getRgprov() {
        return rgprov;
    }

    public void setRgprov(String rgprov) {
        this.rgprov = rgprov;
    }

    public String getRgcity() {
        return rgcity;
    }

    public void setRgcity(String rgcity) {
        this.rgcity = rgcity;
    }

    public String getRgerea() {
        return rgerea;
    }

    public void setRgerea(String rgerea) {
        this.rgerea = rgerea;
    }

    public String getRegadr() {
        return regadr;
    }

    public void setRegadr(String regadr) {
        this.regadr = regadr;
    }

    public String getBspost() {
        return bspost;
    }

    public void setBspost(String bspost) {
        this.bspost = bspost;
    }

    public String getBscuty() {
        return bscuty;
    }

    public void setBscuty(String bscuty) {
        this.bscuty = bscuty;
    }

    public String getBsprov() {
        return bsprov;
    }

    public void setBsprov(String bsprov) {
        this.bsprov = bsprov;
    }

    public String getBscity() {
        return bscity;
    }

    public void setBscity(String bscity) {
        this.bscity = bscity;
    }

    public String getBserea() {
        return bserea;
    }

    public void setBserea(String bserea) {
        this.bserea = bserea;
    }

    public String getBusiad() {
        return busiad;
    }

    public void setBusiad(String busiad) {
        this.busiad = busiad;
    }

    public String getEnaddr() {
        return enaddr;
    }

    public void setEnaddr(String enaddr) {
        this.enaddr = enaddr;
    }

    public String getWebsit() {
        return websit;
    }

    public void setWebsit(String websit) {
        this.websit = websit;
    }

    public String getFaxfax() {
        return faxfax;
    }

    public void setFaxfax(String faxfax) {
        this.faxfax = faxfax;
    }

    public String getEmailx() {
        return emailx;
    }

    public void setEmailx(String emailx) {
        this.emailx = emailx;
    }

    public String getSorgno() {
        return sorgno;
    }

    public void setSorgno(String sorgno) {
        this.sorgno = sorgno;
    }

    public String getSorgtp() {
        return sorgtp;
    }

    public void setSorgtp(String sorgtp) {
        this.sorgtp = sorgtp;
    }

    public String getOrgsit() {
        return orgsit;
    }

    public void setOrgsit(String orgsit) {
        this.orgsit = orgsit;
    }

    public String getBkplic() {
        return bkplic;
    }

    public void setBkplic(String bkplic) {
        this.bkplic = bkplic;
    }

    public String getReguno() {
        return reguno;
    }

    public void setReguno(String reguno) {
        this.reguno = reguno;
    }

    public BigDecimal getPdcpat() {
        return pdcpat;
    }

    public void setPdcpat(BigDecimal pdcpat) {
        this.pdcpat = pdcpat;
    }

    public String getSorglv() {
        return sorglv;
    }

    public void setSorglv(String sorglv) {
        this.sorglv = sorglv;
    }

    public String getEvalda() {
        return evalda;
    }

    public void setEvalda(String evalda) {
        this.evalda = evalda;
    }

    public String getReldgr() {
        return reldgr;
    }

    public void setReldgr(String reldgr) {
        this.reldgr = reldgr;
    }

    public String getLimind() {
        return limind;
    }

    public void setLimind(String limind) {
        this.limind = limind;
    }

    public String getMabrid() {
        return mabrid;
    }

    public void setMabrid(String mabrid) {
        this.mabrid = mabrid;
    }

    public String getManmgr() {
        return manmgr;
    }

    public void setManmgr(String manmgr) {
        this.manmgr = manmgr;
    }

    public String getBriskc() {
        return briskc;
    }

    public void setBriskc(String briskc) {
        this.briskc = briskc;
    }

    public String getMriskc() {
        return mriskc;
    }

    public void setMriskc(String mriskc) {
        this.mriskc = mriskc;
    }

    public String getFriskc() {
        return friskc;
    }

    public void setFriskc(String friskc) {
        this.friskc = friskc;
    }

    public String getRiskda() {
        return riskda;
    }

    public void setRiskda(String riskda) {
        this.riskda = riskda;
    }

    public String getChflag() {
        return chflag;
    }

    public void setChflag(String chflag) {
        this.chflag = chflag;
    }

    public String getNnjytp() {
        return nnjytp;
    }

    public void setNnjytp(String nnjytp) {
        this.nnjytp = nnjytp;
    }

    public String getNnjyna() {
        return nnjyna;
    }

    public void setNnjyna(String nnjyna) {
        this.nnjyna = nnjyna;
    }

    public String getUporgco() {
        return uporgco;
    }

    public void setUporgco(String uporgco) {
        this.uporgco = uporgco;
    }

    public String getUpbasano() {
        return upbasano;
    }

    public void setUpbasano(String upbasano) {
        this.upbasano = upbasano;
    }

    public String getUpatt() {
        return upatt;
    }

    public void setUpatt(String upatt) {
        this.upatt = upatt;
    }

    public String getTwcttp() {
        return twcttp;
    }

    public void setTwcttp(String twcttp) {
        this.twcttp = twcttp;
    }

    public String getCorptp() {
        return corptp;
    }

    public void setCorptp(String corptp) {
        this.corptp = corptp;
    }

    public String getEcegra() {
        return ecegra;
    }

    public void setEcegra(String ecegra) {
        this.ecegra = ecegra;
    }

    public String getEratdt() {
        return eratdt;
    }

    public void setEratdt(String eratdt) {
        this.eratdt = eratdt;
    }

    public String getEraorg() {
        return eraorg;
    }

    public void setEraorg(String eraorg) {
        this.eraorg = eraorg;
    }

    public String getPostle() {
        return postle;
    }

    public void setPostle(String postle) {
        this.postle = postle;
    }

    public String getEmptdt() {
        return emptdt;
    }

    public void setEmptdt(String emptdt) {
        this.emptdt = emptdt;
    }

    public String getPenwrt() {
        return penwrt;
    }

    public void setPenwrt(String penwrt) {
        this.penwrt = penwrt;
    }

    public String getLolvct() {
        return lolvct;
    }

    public void setLolvct(String lolvct) {
        this.lolvct = lolvct;
    }

    public RelArrayInfo getRelArrayInfo() {
        return relArrayInfo;
    }

    public void setRelArrayInfo(RelArrayInfo relArrayInfo) {
        this.relArrayInfo = relArrayInfo;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", csstna='" + csstna + '\'' +
                ", csenna='" + csenna + '\'' +
                ", custst='" + custst + '\'' +
                ", custtp='" + custtp + '\'' +
                ", custsb='" + custsb + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", efctdt='" + efctdt + '\'' +
                ", inefdt='" + inefdt + '\'' +
                ", areaad='" + areaad + '\'' +
                ", depart='" + depart + '\'' +
                ", gvrgno='" + gvrgno + '\'' +
                ", gvbgdt='" + gvbgdt + '\'' +
                ", gveddt='" + gveddt + '\'' +
                ", gvtion='" + gvtion + '\'' +
                ", cropcd='" + cropcd + '\'' +
                ", oreddt='" + oreddt + '\'' +
                ", nttxno='" + nttxno + '\'' +
                ", swbgdt='" + swbgdt + '\'' +
                ", sweddt='" + sweddt + '\'' +
                ", swtion='" + swtion + '\'' +
                ", jgxyno='" + jgxyno + '\'' +
                ", cropdt='" + cropdt + '\'' +
                ", opcfno='" + opcfno + '\'' +
                ", foundt='" + foundt + '\'' +
                ", corppr='" + corppr + '\'' +
                ", coppbc='" + coppbc + '\'' +
                ", entetp='" + entetp + '\'' +
                ", corpnu='" + corpnu + '\'' +
                ", blogto='" + blogto + '\'' +
                ", propts='" + propts + '\'' +
                ", finctp='" + finctp + '\'' +
                ", invesj='" + invesj + '\'' +
                ", regitp='" + regitp + '\'' +
                ", regidt='" + regidt + '\'' +
                ", regicd='" + regicd + '\'' +
                ", regchn='" + regchn + '\'' +
                ", rgctry='" + rgctry + '\'' +
                ", rgcrcy='" + rgcrcy + '\'' +
                ", rgcpam=" + rgcpam +
                ", cacrcy='" + cacrcy + '\'' +
                ", cacpam=" + cacpam +
                ", orgatp='" + orgatp + '\'' +
                ", spectp='" + spectp + '\'' +
                ", busstp='" + busstp + '\'' +
                ", naticd='" + naticd + '\'' +
                ", debttp='" + debttp + '\'' +
                ", envirt='" + envirt + '\'' +
                ", finacd='" + finacd + '\'' +
                ", swifcd='" + swifcd + '\'' +
                ", spntcd='" + spntcd + '\'' +
                ", admnti='" + admnti + '\'' +
                ", admnst='" + admnst + '\'' +
                ", corpsz='" + corpsz + '\'' +
                ", busisp='" + busisp + '\'' +
                ", acesbs='" + acesbs + '\'' +
                ", emplnm=" + emplnm +
                ", comput='" + comput + '\'' +
                ", upcrps='" + upcrps + '\'' +
                ", upidtp='" + upidtp + '\'' +
                ", upidno='" + upidno + '\'' +
                ", loanno='" + loanno + '\'' +
                ", wkflar=" + wkflar +
                ", wkflfe='" + wkflfe + '\'' +
                ", baseno='" + baseno + '\'' +
                ", acctnm='" + acctnm + '\'' +
                ", acctbr='" + acctbr + '\'' +
                ", acctno='" + acctno + '\'' +
                ", acctdt='" + acctdt + '\'' +
                ", depotp='" + depotp + '\'' +
                ", rhtype='" + rhtype + '\'' +
                ", sumast=" + sumast +
                ", netast=" + netast +
                ", sthdfg='" + sthdfg + '\'' +
                ", sharhd=" + sharhd +
                ", listtg='" + listtg + '\'' +
                ", listld='" + listld + '\'' +
                ", stoctp='" + stoctp + '\'' +
                ", stoccd='" + stoccd + '\'' +
                ", onacnm=" + onacnm +
                ", totlnm=" + totlnm +
                ", grpctg='" + grpctg + '\'' +
                ", reletg='" + reletg + '\'' +
                ", famltg='" + famltg + '\'' +
                ", hightg='" + hightg + '\'' +
                ", isbdcp='" + isbdcp + '\'' +
                ", spcltg='" + spcltg + '\'' +
                ", arimtg='" + arimtg + '\'' +
                ", finatg='" + finatg + '\'' +
                ", resutg='" + resutg + '\'' +
                ", sctlpr='" + sctlpr + '\'' +
                ", farmtg='" + farmtg + '\'' +
                ", hgegtg='" + hgegtg + '\'' +
                ", exegtg='" + exegtg + '\'' +
                ", elmntg='" + elmntg + '\'' +
                ", envifg='" + envifg + '\'' +
                ", hgplcp='" + hgplcp + '\'' +
                ", istdcs='" + istdcs + '\'' +
                ", spclfg='" + spclfg + '\'' +
                ", steltg='" + steltg + '\'' +
                ", domitg='" + domitg + '\'' +
                ", strutp='" + strutp + '\'' +
                ", stratp='" + stratp + '\'' +
                ", indutg='" + indutg + '\'' +
                ", leadtg='" + leadtg + '\'' +
                ", cncatg='" + cncatg + '\'' +
                ", impotg='" + impotg + '\'' +
                ", clostg='" + clostg + '\'' +
                ", plattg='" + plattg + '\'' +
                ", fibrtg='" + fibrtg + '\'' +
                ", gvfntg='" + gvfntg + '\'' +
                ", gvlnlv='" + gvlnlv + '\'' +
                ", gvlwtp='" + gvlwtp + '\'' +
                ", gvlntp='" + gvlntp + '\'' +
                ", petytg='" + petytg + '\'' +
                ", fscddt='" + fscddt + '\'' +
                ", supptg='" + supptg + '\'' +
                ", licetg='" + licetg + '\'' +
                ", landtg='" + landtg + '\'' +
                ", pasvfg='" + pasvfg + '\'' +
                ", imagno='" + imagno + '\'' +
                ", taxptp='" + taxptp + '\'' +
                ", needfp='" + needfp + '\'' +
                ", taxpna='" + taxpna + '\'' +
                ", taxpad='" + taxpad + '\'' +
                ", taxptl='" + taxptl + '\'' +
                ", txbkna='" + txbkna + '\'' +
                ", taxpac='" + taxpac + '\'' +
                ", txpstp='" + txpstp + '\'' +
                ", taxadr='" + taxadr + '\'' +
                ", txnion='" + txnion + '\'' +
                ", txennm='" + txennm + '\'' +
                ", txbcty='" + txbcty + '\'' +
                ", taxnum='" + taxnum + '\'' +
                ", notxrs='" + notxrs + '\'' +
                ", ctigno='" + ctigno + '\'' +
                ", ctigna='" + ctigna + '\'' +
                ", ctigph='" + ctigph + '\'' +
                ", corptl='" + corptl + '\'' +
                ", hometl='" + hometl + '\'' +
                ", rgpost='" + rgpost + '\'' +
                ", rgcuty='" + rgcuty + '\'' +
                ", rgprov='" + rgprov + '\'' +
                ", rgcity='" + rgcity + '\'' +
                ", rgerea='" + rgerea + '\'' +
                ", regadr='" + regadr + '\'' +
                ", bspost='" + bspost + '\'' +
                ", bscuty='" + bscuty + '\'' +
                ", bsprov='" + bsprov + '\'' +
                ", bscity='" + bscity + '\'' +
                ", bserea='" + bserea + '\'' +
                ", busiad='" + busiad + '\'' +
                ", enaddr='" + enaddr + '\'' +
                ", websit='" + websit + '\'' +
                ", faxfax='" + faxfax + '\'' +
                ", emailx='" + emailx + '\'' +
                ", sorgno='" + sorgno + '\'' +
                ", sorgtp='" + sorgtp + '\'' +
                ", orgsit='" + orgsit + '\'' +
                ", bkplic='" + bkplic + '\'' +
                ", reguno='" + reguno + '\'' +
                ", pdcpat=" + pdcpat +
                ", sorglv='" + sorglv + '\'' +
                ", evalda='" + evalda + '\'' +
                ", reldgr='" + reldgr + '\'' +
                ", limind='" + limind + '\'' +
                ", mabrid='" + mabrid + '\'' +
                ", manmgr='" + manmgr + '\'' +
                ", briskc='" + briskc + '\'' +
                ", mriskc='" + mriskc + '\'' +
                ", friskc='" + friskc + '\'' +
                ", riskda='" + riskda + '\'' +
                ", chflag='" + chflag + '\'' +
                ", nnjytp='" + nnjytp + '\'' +
                ", nnjyna='" + nnjyna + '\'' +
                ", uporgco='" + uporgco + '\'' +
                ", upbasano='" + upbasano + '\'' +
                ", upatt='" + upatt + '\'' +
                ", twcttp='" + twcttp + '\'' +
                ", corptp='" + corptp + '\'' +
                ", ecegra='" + ecegra + '\'' +
                ", eratdt='" + eratdt + '\'' +
                ", eraorg='" + eraorg + '\'' +
                ", postle='" + postle + '\'' +
                ", emptdt='" + emptdt + '\'' +
                ", penwrt='" + penwrt + '\'' +
                ", lolvct='" + lolvct + '\'' +
                ", relArrayInfo=" + relArrayInfo +
                '}';
    }
}
