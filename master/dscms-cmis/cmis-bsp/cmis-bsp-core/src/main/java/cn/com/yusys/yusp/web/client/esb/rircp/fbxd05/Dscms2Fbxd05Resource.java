package cn.com.yusys.yusp.web.client.esb.rircp.fbxd05;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd05.Fbxd05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd05.Fbxd05RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.req.Fbxd05ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.Fbxd05RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd05）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd05Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd05Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd05
     * 交易描述：查找指定数据日期的放款合约明细记录历史表（利翃）一览信息的借据号、借据金额、借据余额
     *
     * @param fbxd05ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd05")
    protected @ResponseBody
    ResultDto<Fbxd05RespDto> fbxd05(@Validated @RequestBody Fbxd05ReqDto fbxd05ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD05.key, EsbEnum.TRADE_CODE_FBXD05.value, JSON.toJSONString(fbxd05ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.Service();
        Fbxd05ReqService fbxd05ReqService = new Fbxd05ReqService();
        Fbxd05RespService fbxd05RespService = new Fbxd05RespService();
        Fbxd05RespDto fbxd05RespDto = new Fbxd05RespDto();
        ResultDto<Fbxd05RespDto> fbxd05ResultDto = new ResultDto<Fbxd05RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd05ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd05ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD05.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd05ReqService.setService(reqService);
            // 将fbxd05ReqService转换成fbxd05ReqServiceMap
            Map fbxd05ReqServiceMap = beanMapUtil.beanToMap(fbxd05ReqService);
            context.put("tradeDataMap", fbxd05ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD05.key, EsbEnum.TRADE_CODE_FBXD05.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD05.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD05.key, EsbEnum.TRADE_CODE_FBXD05.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd05RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd05RespService.class, Fbxd05RespService.class);
            respService = fbxd05RespService.getService();

            fbxd05ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd05ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd05RespDto
                BeanUtils.copyProperties(respService, fbxd05RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.List fbxd05RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.List());
                respService.setList(fbxd05RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> fbxd05RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd05.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd05.List> fbxd05RespDtoLists = fbxd05RespRecordList.parallelStream().map(fbxd05RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd05.List fbxd05RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd05.List();
                        BeanUtils.copyProperties(fbxd05RespRecord, fbxd05RespDtoList);
                        return fbxd05RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd05RespDto.setList(fbxd05RespDtoLists);
                }
                fbxd05ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd05ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd05ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd05ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD05.key, EsbEnum.TRADE_CODE_FBXD05.value, e.getMessage());
            fbxd05ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd05ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd05ResultDto.setData(fbxd05RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD05.key, EsbEnum.TRADE_CODE_FBXD05.value, JSON.toJSONString(fbxd05ResultDto));
        return fbxd05ResultDto;
    }
}
