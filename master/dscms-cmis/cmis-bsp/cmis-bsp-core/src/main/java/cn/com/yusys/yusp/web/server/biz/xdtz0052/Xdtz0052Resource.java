package cn.com.yusys.yusp.web.server.biz.xdtz0052;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0052.req.Xdtz0052ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0052.resp.Xdtz0052RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0052.req.Xdtz0052DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0052.resp.Xdtz0052DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据借据号查询申请人行内还款（利息、本金）该笔借据次数
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0052:根据借据号查询申请人行内还款（利息、本金）该笔借据次数")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0052Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0052Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0052
     * 交易描述：根据借据号查询申请人行内还款（利息、本金）该笔借据次数
     *
     * @param xdtz0052ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借据号查询申请人行内还款（利息、本金）该笔借据次数")
    @PostMapping("/xdtz0052")
//@Idempotent({"xdtz0052", "#xdtz0052ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0052RespDto xdtz0052(@Validated @RequestBody Xdtz0052ReqDto xdtz0052ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052ReqDto));
        Xdtz0052DataReqDto xdtz0052DataReqDto = new Xdtz0052DataReqDto();// 请求Data： 根据借据号查询申请人行内还款（利息、本金）该笔借据次数
        Xdtz0052DataRespDto xdtz0052DataRespDto = new Xdtz0052DataRespDto();// 响应Data：根据借据号查询申请人行内还款（利息、本金）该笔借据次数
        Xdtz0052RespDto xdtz0052RespDto = new Xdtz0052RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0052.req.Data reqData = null; // 请求Data：根据借据号查询申请人行内还款（利息、本金）该笔借据次数
        cn.com.yusys.yusp.dto.server.biz.xdtz0052.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0052.resp.Data();// 响应Data：根据借据号查询申请人行内还款（利息、本金）该笔借据次数

        try {
            // 从 xdtz0052ReqDto获取 reqData
            reqData = xdtz0052ReqDto.getData();
            // 将 reqData 拷贝到xdtz0052DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0052DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052DataReqDto));
            ResultDto<Xdtz0052DataRespDto> xdtz0052DataResultDto = dscmsBizTzClientService.xdtz0052(xdtz0052DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0052RespDto.setErorcd(Optional.ofNullable(xdtz0052DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0052RespDto.setErortx(Optional.ofNullable(xdtz0052DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0052DataResultDto.getCode())) {
                xdtz0052DataRespDto = xdtz0052DataResultDto.getData();
                xdtz0052RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0052RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0052DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0052DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, e.getMessage());
            xdtz0052RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0052RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0052RespDto.setDatasq(servsq);

        xdtz0052RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0052.key, DscmsEnum.TRADE_CODE_XDTZ0052.value, JSON.toJSONString(xdtz0052RespDto));
        return xdtz0052RespDto;
    }
}
