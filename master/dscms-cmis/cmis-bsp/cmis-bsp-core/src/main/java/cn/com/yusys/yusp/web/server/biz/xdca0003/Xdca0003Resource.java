package cn.com.yusys.yusp.web.server.biz.xdca0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdca0003.req.Xdca0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdca0003.resp.Xdca0003RespDto;
import cn.com.yusys.yusp.dto.server.xdca0003.req.Xdca0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0003.resp.Xdca0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCaClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:大额分期合同列表查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0003:大额分期合同列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdca0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0003Resource.class);

	@Autowired
	private DscmsBizCaClientService dscmsBizCaClientService;

    /**
     * 交易码：xdca0003
     * 交易描述：大额分期合同列表查询
     *
     * @param xdca0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期合同列表查询")
    @PostMapping("/xdca0003")
//   @Idempotent({"xdcaca0003", "#xdca0003ReqDto.datasq"})
    protected @ResponseBody
	Xdca0003RespDto xdca0003(@Validated @RequestBody Xdca0003ReqDto xdca0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, JSON.toJSONString(xdca0003ReqDto));
        Xdca0003DataReqDto xdca0003DataReqDto = new Xdca0003DataReqDto();// 请求Data： 大额分期合同列表查询
        Xdca0003DataRespDto xdca0003DataRespDto = new Xdca0003DataRespDto();// 响应Data：大额分期合同列表查询
		Xdca0003RespDto xdca0003RespDto = new Xdca0003RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdca0003.req.Data reqData = null; // 请求Data：大额分期合同列表查询
		cn.com.yusys.yusp.dto.server.biz.xdca0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdca0003.resp.Data();// 响应Data：大额分期合同列表查询
        // 此处包导入待确定 结束
        try {
            // 从 xdca0003ReqDto获取 reqData
            reqData = xdca0003ReqDto.getData();
            // 将 reqData 拷贝到xdca0003DataReqDto
            BeanUtils.copyProperties(reqData, xdca0003DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, JSON.toJSONString(xdca0003DataReqDto));
            ResultDto<Xdca0003DataRespDto> xdca0003DataResultDto = dscmsBizCaClientService.xdca0003(xdca0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, JSON.toJSONString(xdca0003DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdca0003RespDto.setErorcd(Optional.ofNullable(xdca0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdca0003RespDto.setErortx(Optional.ofNullable(xdca0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdca0003DataResultDto.getCode())) {
                xdca0003DataRespDto = xdca0003DataResultDto.getData();
                xdca0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdca0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdca0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdca0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, e.getMessage());
            xdca0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdca0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdca0003RespDto.setDatasq(servsq);

        xdca0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0003.key, DscmsEnum.TRADE_CODE_XDCA0003.value, JSON.toJSONString(xdca0003RespDto));
        return xdca0003RespDto;
    }
}
