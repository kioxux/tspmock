package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:业务信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:09
 */
public class BusinessInfo {
    private List<BusinessInfoRecord> record;//	业务信息

    public List<BusinessInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<BusinessInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "BusinessInfo{" +
                "record=" + record +
                '}';
    }
}
