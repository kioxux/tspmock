package cn.com.yusys.yusp.web.server.biz.xdxw0048;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0048.req.Xdxw0048ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0048.resp.Xdxw0048RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0048.req.Xdxw0048DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0048.resp.Xdxw0048DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:增享贷2.0风控模型B生成批复
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0048:增享贷2.0风控模型B生成批复")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0048Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0048Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0048
     * 交易描述：增享贷2.0风控模型B生成批复
     *
     * @param xdxw0048ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("增享贷2.0风控模型B生成批复")
    @PostMapping("/xdxw0048")
    //@Idempotent({"xdcaxw0048", "#xdxw0048ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0048RespDto xdxw0048(@Validated @RequestBody Xdxw0048ReqDto xdxw0048ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048ReqDto));
        Xdxw0048DataReqDto xdxw0048DataReqDto = new Xdxw0048DataReqDto();// 请求Data： 增享贷2.0风控模型B生成批复
        Xdxw0048DataRespDto xdxw0048DataRespDto = new Xdxw0048DataRespDto();// 响应Data：增享贷2.0风控模型B生成批复
		Xdxw0048RespDto xdxw0048RespDto = new Xdxw0048RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0048.req.Data reqData = null; // 请求Data：增享贷2.0风控模型B生成批复
		cn.com.yusys.yusp.dto.server.biz.xdxw0048.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0048.resp.Data();// 响应Data：增享贷2.0风控模型B生成批复
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0048ReqDto获取 reqData
            reqData = xdxw0048ReqDto.getData();
            // 将 reqData 拷贝到xdxw0048DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0048DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048DataReqDto));
            ResultDto<Xdxw0048DataRespDto> xdxw0048DataResultDto = dscmsBizXwClientService.xdxw0048(xdxw0048DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0048RespDto.setErorcd(Optional.ofNullable(xdxw0048DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0048RespDto.setErortx(Optional.ofNullable(xdxw0048DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0048DataResultDto.getCode())) {
                xdxw0048DataRespDto = xdxw0048DataResultDto.getData();
                xdxw0048RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0048RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0048DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0048DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, e.getMessage());
            xdxw0048RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0048RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0048RespDto.setDatasq(servsq);

        xdxw0048RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0048.key, DscmsEnum.TRADE_CODE_XDXW0048.value, JSON.toJSONString(xdxw0048RespDto));
        return xdxw0048RespDto;
    }
}
