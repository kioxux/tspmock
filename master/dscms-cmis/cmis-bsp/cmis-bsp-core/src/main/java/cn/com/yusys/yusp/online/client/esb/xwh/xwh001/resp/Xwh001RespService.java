package cn.com.yusys.yusp.online.client.esb.xwh.xwh001.resp;

/**
 * 响应Service：借据台账信息接收
 * @author lihh
 * @version 1.0             
 */      
public class Xwh001RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
