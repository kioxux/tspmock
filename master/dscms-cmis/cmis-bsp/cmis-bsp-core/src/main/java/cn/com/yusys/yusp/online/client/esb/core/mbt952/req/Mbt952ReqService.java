package cn.com.yusys.yusp.online.client.esb.core.mbt952.req;

/**
 * 请求Service：批量结果确认处理
 *
 * @author chenyong
 * @version 1.0
 */
public class Mbt952ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Mbt952ReqService{" +
                "service=" + service +
                '}';
    }
}
