package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	简易注销-异议信息
@JsonPropertyOrder(alphabetic = true)
public class QUICKCANCELDISSENT implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "DISSENT_DATE")
    private String DISSENT_DATE;//	异议时间
    @JsonProperty(value = "DISSENT_DES")
    private String DISSENT_DES;//	异议内容
    @JsonProperty(value = "DISSENT_ORG")
    private String DISSENT_ORG;//	异议申请人

    @JsonIgnore
    public String getDISSENT_DATE() {
        return DISSENT_DATE;
    }

    @JsonIgnore
    public void setDISSENT_DATE(String DISSENT_DATE) {
        this.DISSENT_DATE = DISSENT_DATE;
    }

    @JsonIgnore
    public String getDISSENT_DES() {
        return DISSENT_DES;
    }

    @JsonIgnore
    public void setDISSENT_DES(String DISSENT_DES) {
        this.DISSENT_DES = DISSENT_DES;
    }

    @JsonIgnore
    public String getDISSENT_ORG() {
        return DISSENT_ORG;
    }

    @JsonIgnore
    public void setDISSENT_ORG(String DISSENT_ORG) {
        this.DISSENT_ORG = DISSENT_ORG;
    }

    @Override
    public String toString() {
        return "QUICKCANCELDISSENT{" +
                "DISSENT_DATE='" + DISSENT_DATE + '\'' +
                ", DISSENT_DES='" + DISSENT_DES + '\'' +
                ", DISSENT_ORG='" + DISSENT_ORG + '\'' +
                '}';
    }
}
