package cn.com.yusys.yusp.online.client.esb.core.ln3100.resp;


import java.math.BigDecimal;

/**
 * 响应Service：贷款账户放款计划
 *
 * @author chenyong
 * @version 1.0
 */
public class Lstdkfkjh {
    private String dkjiejuh;//贷款借据号
    private Integer xuhaoooo;//序号
    private String fangkzhl;//放款种类
    private String shchriqi;//生成日期
    private String fkriqiii;//放款日期
    private BigDecimal fkjineee;//放款金额
    private String dkrzhzhh;//贷款入账账号
    private String dkrzhzxh;//贷款入账账号子序号
    private String chulizht;//处理状态

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getFangkzhl() {
        return fangkzhl;
    }

    public void setFangkzhl(String fangkzhl) {
        this.fangkzhl = fangkzhl;
    }

    public String getShchriqi() {
        return shchriqi;
    }

    public void setShchriqi(String shchriqi) {
        this.shchriqi = shchriqi;
    }

    public String getFkriqiii() {
        return fkriqiii;
    }

    public void setFkriqiii(String fkriqiii) {
        this.fkriqiii = fkriqiii;
    }

    public BigDecimal getFkjineee() {
        return fkjineee;
    }

    public void setFkjineee(BigDecimal fkjineee) {
        this.fkjineee = fkjineee;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public String getChulizht() {
        return chulizht;
    }

    public void setChulizht(String chulizht) {
        this.chulizht = chulizht;
    }

    @Override
    public String toString() {
        return "Service{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "xuhaoooo='" + xuhaoooo + '\'' +
                "fangkzhl='" + fangkzhl + '\'' +
                "shchriqi='" + shchriqi + '\'' +
                "fkriqiii='" + fkriqiii + '\'' +
                "fkjineee='" + fkjineee + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "chulizht='" + chulizht + '\'' +
                '}';
    }
}
