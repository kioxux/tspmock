package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 企业对外投资
 */
@JsonPropertyOrder(alphabetic = true)
public class ENTINV implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "BINVVAMOUNT")
    private String BINVVAMOUNT;//企业总数量
    @JsonProperty(value = "CANDATE")
    private String CANDATE;//注销日期
    @JsonProperty(value = "CONFORM")
    private String CONFORM;//出资方式
    @JsonProperty(value = "CONGROCUR")
    private String CONGROCUR;//认缴出资币种
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;//统一社会信用代码
    @JsonProperty(value = "ENTJGNAME")
    private String ENTJGNAME;//企业机构名称
    @JsonProperty(value = "ENTSTATUS")
    private String ENTSTATUS;//企业状态
    @JsonProperty(value = "ENTTYPE")
    private String ENTTYPE;//企业（机构）类型
    @JsonProperty(value = "ESDATE")
    private String ESDATE;//开业日期
    @JsonProperty(value = "FUNDEDRATIO")
    private String FUNDEDRATIO;//投资比例
    @JsonProperty(value = "NAME")
    private String NAME;//法定代表人姓名
    @JsonProperty(value = "REGCAP")
    private String REGCAP;//注册资本（企业:万元）
    @JsonProperty(value = "REGCAPCUR")
    private String REGCAPCUR;//注册资本币种
    @JsonProperty(value = "REGNO")
    private String REGNO;//注册号
    @JsonProperty(value = "REGORG")
    private String REGORG;//登记机关
    @JsonProperty(value = "REGORGCODE")
    private String REGORGCODE;//注册地址行政区编号
    @JsonProperty(value = "REVDATE")
    private String REVDATE;//吊销日期
    @JsonProperty(value = "SUBCONAM")
    private String SUBCONAM;//认缴出资额（万元）

    @JsonIgnore
    public String getBINVVAMOUNT() {
        return BINVVAMOUNT;
    }

    @JsonIgnore
    public void setBINVVAMOUNT(String BINVVAMOUNT) {
        this.BINVVAMOUNT = BINVVAMOUNT;
    }

    @JsonIgnore
    public String getCANDATE() {
        return CANDATE;
    }

    @JsonIgnore
    public void setCANDATE(String CANDATE) {
        this.CANDATE = CANDATE;
    }

    @JsonIgnore
    public String getCONFORM() {
        return CONFORM;
    }

    @JsonIgnore
    public void setCONFORM(String CONFORM) {
        this.CONFORM = CONFORM;
    }

    @JsonIgnore
    public String getCONGROCUR() {
        return CONGROCUR;
    }

    @JsonIgnore
    public void setCONGROCUR(String CONGROCUR) {
        this.CONGROCUR = CONGROCUR;
    }

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getENTJGNAME() {
        return ENTJGNAME;
    }

    @JsonIgnore
    public void setENTJGNAME(String ENTJGNAME) {
        this.ENTJGNAME = ENTJGNAME;
    }

    @JsonIgnore
    public String getENTSTATUS() {
        return ENTSTATUS;
    }

    @JsonIgnore
    public void setENTSTATUS(String ENTSTATUS) {
        this.ENTSTATUS = ENTSTATUS;
    }

    @JsonIgnore
    public String getENTTYPE() {
        return ENTTYPE;
    }

    @JsonIgnore
    public void setENTTYPE(String ENTTYPE) {
        this.ENTTYPE = ENTTYPE;
    }

    @JsonIgnore
    public String getESDATE() {
        return ESDATE;
    }

    @JsonIgnore
    public void setESDATE(String ESDATE) {
        this.ESDATE = ESDATE;
    }

    @JsonIgnore
    public String getFUNDEDRATIO() {
        return FUNDEDRATIO;
    }

    @JsonIgnore
    public void setFUNDEDRATIO(String FUNDEDRATIO) {
        this.FUNDEDRATIO = FUNDEDRATIO;
    }

    @JsonIgnore
    public String getNAME() {
        return NAME;
    }

    @JsonIgnore
    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    @JsonIgnore
    public String getREGCAP() {
        return REGCAP;
    }

    @JsonIgnore
    public void setREGCAP(String REGCAP) {
        this.REGCAP = REGCAP;
    }

    @JsonIgnore
    public String getREGCAPCUR() {
        return REGCAPCUR;
    }

    @JsonIgnore
    public void setREGCAPCUR(String REGCAPCUR) {
        this.REGCAPCUR = REGCAPCUR;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getREGORG() {
        return REGORG;
    }

    @JsonIgnore
    public void setREGORG(String REGORG) {
        this.REGORG = REGORG;
    }

    @JsonIgnore
    public String getREGORGCODE() {
        return REGORGCODE;
    }

    @JsonIgnore
    public void setREGORGCODE(String REGORGCODE) {
        this.REGORGCODE = REGORGCODE;
    }

    @JsonIgnore
    public String getREVDATE() {
        return REVDATE;
    }

    @JsonIgnore
    public void setREVDATE(String REVDATE) {
        this.REVDATE = REVDATE;
    }

    @JsonIgnore
    public String getSUBCONAM() {
        return SUBCONAM;
    }

    @JsonIgnore
    public void setSUBCONAM(String SUBCONAM) {
        this.SUBCONAM = SUBCONAM;
    }

    @Override
    public String toString() {
        return "ENTINV{" +
                "BINVVAMOUNT='" + BINVVAMOUNT + '\'' +
                ", CANDATE='" + CANDATE + '\'' +
                ", CONFORM='" + CONFORM + '\'' +
                ", CONGROCUR='" + CONGROCUR + '\'' +
                ", CREDITCODE='" + CREDITCODE + '\'' +
                ", ENTJGNAME='" + ENTJGNAME + '\'' +
                ", ENTSTATUS='" + ENTSTATUS + '\'' +
                ", ENTTYPE='" + ENTTYPE + '\'' +
                ", ESDATE='" + ESDATE + '\'' +
                ", FUNDEDRATIO='" + FUNDEDRATIO + '\'' +
                ", NAME='" + NAME + '\'' +
                ", REGCAP='" + REGCAP + '\'' +
                ", REGCAPCUR='" + REGCAPCUR + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", REGORG='" + REGORG + '\'' +
                ", REGORGCODE='" + REGORGCODE + '\'' +
                ", REVDATE='" + REVDATE + '\'' +
                ", SUBCONAM='" + SUBCONAM + '\'' +
                '}';
    }
}
