package cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 20:28
 * @since 2021/6/4 20:28
 */
@JsonPropertyOrder(alphabetic = true)
public class LeftSubject implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "subjectId")
    private String subjectId;//科目id
    @JsonProperty(value = "subjectName")
    private String subjectName;//科目名称
    @JsonProperty(value = "subjectType")
    private Integer subjectType;//科目类型，0：普通科目、1：信贷科目、2：试算平衡项
    @JsonProperty(value = "classCode")
    private String classCode;//科目分类字典编码
    @JsonProperty(value = "className")
    private String className;//科目分类名称
    @JsonProperty(value = "originSubjectName")
    private String originSubjectName;//科目在原始文件中的名称
    @JsonProperty(value = "subjectCode")
    private String subjectCode;//科目编码
    @JsonProperty(value = "fileId")
    private String fileId;//科目所在文件的id
    @JsonProperty(value = "tag")
    private String tag;//科目状态，0负值转换，1试算平衡，2试算不平衡，3比较规则全部通过 4比较规则未全部通过
    @JsonProperty(value = "matchStatus")
    private String matchStatus;//匹配状态（0已匹配、1低匹配、2未匹配）
    @JsonProperty(value = "checkStatus")
    private Integer checkStatus;//科目核对状态，0未核对，1已核对
    @JsonProperty(value = "connectStatus")
    private Integer connectStatus;//科目在模板中的关联等级，0直接关联，1间接关联
    @JsonProperty(value = "hasChildren")
    private Integer hasChildren;//科目子科目状态，0无子科目，1有子科目
    @JsonProperty(value = "statisItem")
    private java.util.List<cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.StatisItem> statisItem;//科目对应的统计项列表
    @JsonProperty(value = "compareResults")
    private java.util.List<cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.compareResults> compareResults;//科目比较结果列表

    public String getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(String subjectId) {
        this.subjectId = subjectId;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public Integer getSubjectType() {
        return subjectType;
    }

    public void setSubjectType(Integer subjectType) {
        this.subjectType = subjectType;
    }

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getOriginSubjectName() {
        return originSubjectName;
    }

    public void setOriginSubjectName(String originSubjectName) {
        this.originSubjectName = originSubjectName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getMatchStatus() {
        return matchStatus;
    }

    public void setMatchStatus(String matchStatus) {
        this.matchStatus = matchStatus;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getConnectStatus() {
        return connectStatus;
    }

    public void setConnectStatus(Integer connectStatus) {
        this.connectStatus = connectStatus;
    }

    public Integer getHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(Integer hasChildren) {
        this.hasChildren = hasChildren;
    }

    public List<StatisItem> getStatisItem() {
        return statisItem;
    }

    public void setStatisItem(List<StatisItem> statisItem) {
        this.statisItem = statisItem;
    }

    public List<cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.compareResults> getCompareResults() {
        return compareResults;
    }

    public void setCompareResults(List<cn.com.yusys.yusp.online.client.http.outerdata.ocr.resp.compareResults> compareResults) {
        this.compareResults = compareResults;
    }

    @Override
    public String toString() {
        return "LeftSubject{" +
                "subjectId='" + subjectId + '\'' +
                ", subjectName='" + subjectName + '\'' +
                ", subjectType=" + subjectType +
                ", classCode='" + classCode + '\'' +
                ", className='" + className + '\'' +
                ", originSubjectName='" + originSubjectName + '\'' +
                ", subjectCode='" + subjectCode + '\'' +
                ", fileId='" + fileId + '\'' +
                ", tag='" + tag + '\'' +
                ", matchStatus='" + matchStatus + '\'' +
                ", checkStatus=" + checkStatus +
                ", connectStatus=" + connectStatus +
                ", hasChildren=" + hasChildren +
                ", statisItem=" + statisItem +
                ", compareResults=" + compareResults +
                '}';
    }
}
