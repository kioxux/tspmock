package cn.com.yusys.yusp.online.client.esb.core.da3303.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵债资产信息维护
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String yngyjigo;//营业机构
    private String kehuhaoo;//客户号
    private String kehuzwmc;//客户名
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private BigDecimal daizdzzc;//待转抵债资产
    private BigDecimal dcldzzic;//待处理抵债资产
    private BigDecimal dbxdzzic;//待变现抵债资产
    private BigDecimal hfeijine;//还费金额
    private BigDecimal huanbjee;//还本金额
    private BigDecimal hxijinee;//还息金额
    private BigDecimal hqitjine;//还其它金额

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public BigDecimal getHqitjine() {
        return hqitjine;
    }

    public void setHqitjine(BigDecimal hqitjine) {
        this.hqitjine = hqitjine;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", daizdzzc=" + daizdzzc +
                ", dcldzzic=" + dcldzzic +
                ", dbxdzzic=" + dbxdzzic +
                ", hfeijine=" + hfeijine +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", hqitjine=" + hqitjine +
                '}';
    }
}
