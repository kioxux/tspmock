package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.resp;

/**
 * 响应Service：数据修改接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Znsp07RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Znsp07RespService{" +
                "service=" + service +
                '}';
    }
}
