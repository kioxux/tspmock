package cn.com.yusys.yusp.web.server.lmt.xded0033;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033DealBizListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033OccRelListReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.req.CmisLmt0033ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0033.resp.CmisLmt0033RespDto;
import cn.com.yusys.yusp.dto.server.cmislmt0019.resp.CmisLmt0019RespDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0033.req.DealBizList;
import cn.com.yusys.yusp.dto.server.lmt.xded0033.req.OccRelList;
import cn.com.yusys.yusp.dto.server.lmt.xded0033.req.Xded0033ReqDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0033.resp.Xded0033RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:国结票据出账额度校验
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDED0033:国结票据出账额度校验")
@RestController
@RequestMapping("/api/dscms")
public class Xded0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xded0033Resource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：cmisLmt0033
     * 交易描述：国结票据出账额度校验
     *
     * @param xded0033ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("国结票据出账额度校验")
    @PostMapping("/xded0033")
    //  @Idempotent({"xded0033", "#xded0033ReqDto.datasq"})
    protected @ResponseBody
    Xded0033RespDto cmisLmt0033(@Validated @RequestBody Xded0033ReqDto xded0033ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0033.key, DscmsEnum.TRADE_CODE_XDED0033.value, JSON.toJSONString(xded0033ReqDto));
        CmisLmt0033ReqDto cmisLmt0033ReqDto = new CmisLmt0033ReqDto();// 请求Data： 国结票据出账额度校验
        CmisLmt0033RespDto cmisLmt0033RespDto = new CmisLmt0033RespDto();// 响应Data：国结票据出账额度校验

        cn.com.yusys.yusp.dto.server.lmt.xded0033.resp.Xded0033RespDto xded0033RespDto = new cn.com.yusys.yusp.dto.server.lmt.xded0033.resp.Xded0033RespDto();

        cn.com.yusys.yusp.dto.server.lmt.xded0033.req.Data reqData = null; // 请求Data：国结票据出账额度校验
        cn.com.yusys.yusp.dto.server.lmt.xded0033.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.xded0033.resp.Data();// 响应Data：国结票据出账额度校验

        try {
            // 从 Xded0033Dto获取 reqData
            reqData = xded0033ReqDto.getData();
            // 将 reqData 拷贝到cmisLmt0033DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0033ReqDto);
            List<DealBizList> dealBizList = reqData.getDealBizList();
            List<OccRelList> occRelList = reqData.getOccRelList();
            if (CollectionUtils.nonEmpty(dealBizList)) {
                List<CmisLmt0033DealBizListReqDto> collect = dealBizList.parallelStream().map(deal -> {
                    CmisLmt0033DealBizListReqDto cmisLmt0033DealBizListReqDto = new CmisLmt0033DealBizListReqDto();
                    BeanUtils.copyProperties(deal, cmisLmt0033DealBizListReqDto);
                    return cmisLmt0033DealBizListReqDto;
                }).collect(Collectors.toList());
                cmisLmt0033ReqDto.setDealBizList(collect);
            }
            if (CollectionUtils.nonEmpty(occRelList)) {
                List<CmisLmt0033OccRelListReqDto> collect = occRelList.parallelStream().map(occ -> {
                    CmisLmt0033OccRelListReqDto cmisLmt0033OccRelListReqDto = new CmisLmt0033OccRelListReqDto();
                    BeanUtils.copyProperties(occ, cmisLmt0033OccRelListReqDto);
                    return cmisLmt0033OccRelListReqDto;
                }).collect(Collectors.toList());
                cmisLmt0033ReqDto.setOccRelList(collect);
            }

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value, JSON.toJSONString(cmisLmt0033ReqDto));
            ResultDto<CmisLmt0033RespDto> cmisLmt0033RespDtoResultDto = cmisLmtClientService.cmislmt0033(cmisLmt0033ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0033.value, JSON.toJSONString(cmisLmt0033RespDtoResultDto));
            // 从返回值中获取响应码和响应消息
            xded0033RespDto.setErorcd(Optional.ofNullable(cmisLmt0033RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xded0033RespDto.setErortx(Optional.ofNullable(cmisLmt0033RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0033RespDtoResultDto.getCode())) {
                cmisLmt0033RespDto = cmisLmt0033RespDtoResultDto.getData();
                xded0033RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xded0033RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0033DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0033RespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0033.key, DscmsEnum.TRADE_CODE_XDED0033.value, e.getMessage());
            xded0033RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xded0033RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xded0033RespDto.setDatasq(servsq);

        xded0033RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0033.key, DscmsEnum.TRADE_CODE_XDED0033.value, JSON.toJSONString(xded0033RespDto));
        return xded0033RespDto;
    }
}
