package cn.com.yusys.yusp.online.client.esb.core.co3222.resp;

/**
 * 响应Service：抵质押物单笔查询
 * @author leehuang
 * @version 1.0             
 */      
public class Co3222RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
