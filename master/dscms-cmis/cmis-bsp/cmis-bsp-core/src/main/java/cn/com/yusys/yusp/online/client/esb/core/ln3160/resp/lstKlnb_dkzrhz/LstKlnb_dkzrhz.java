package cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz;

import java.util.List;

/**
 * 响应Service：资产证券化信息查询
 *
 * @author leehuang
 * @version 1.0
 */
public class LstKlnb_dkzrhz {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstKlnb_dkzrhz{" +
                "record=" + record +
                '}';
    }
}
