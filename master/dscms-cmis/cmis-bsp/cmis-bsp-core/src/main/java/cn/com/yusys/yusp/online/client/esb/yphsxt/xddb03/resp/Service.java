package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询存单票据信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String money_seq_no;//理财平台流水号
    private String money_prod_code;//理财产品代码
    private String acct_no;//账户号码
    private String rate;//利率/收益率
    private BigDecimal origin_amt;//金额
    private String start_date;//起始日
    private String end_date;//到期日
    private String cur_type;//币种
    private String account_num;//子账户序号
    private BigDecimal num;//质押份额
    private String prod_name;//理财产品名称
    private String oth_bank_name;//银行名称
    private String receipt_no;//存单凭证号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getMoney_seq_no() {
        return money_seq_no;
    }

    public void setMoney_seq_no(String money_seq_no) {
        this.money_seq_no = money_seq_no;
    }

    public String getMoney_prod_code() {
        return money_prod_code;
    }

    public void setMoney_prod_code(String money_prod_code) {
        this.money_prod_code = money_prod_code;
    }

    public String getAcct_no() {
        return acct_no;
    }

    public void setAcct_no(String acct_no) {
        this.acct_no = acct_no;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public BigDecimal getOrigin_amt() {
        return origin_amt;
    }

    public void setOrigin_amt(BigDecimal origin_amt) {
        this.origin_amt = origin_amt;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public String getAccount_num() {
        return account_num;
    }

    public void setAccount_num(String account_num) {
        this.account_num = account_num;
    }

    public BigDecimal getNum() {
        return num;
    }

    public void setNum(BigDecimal num) {
        this.num = num;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    public String getOth_bank_name() {
        return oth_bank_name;
    }

    public void setOth_bank_name(String oth_bank_name) {
        this.oth_bank_name = oth_bank_name;
    }

    public String getReceipt_no() {
        return receipt_no;
    }

    public void setReceipt_no(String receipt_no) {
        this.receipt_no = receipt_no;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", money_seq_no='" + money_seq_no + '\'' +
                ", money_prod_code='" + money_prod_code + '\'' +
                ", acct_no='" + acct_no + '\'' +
                ", rate=" + rate +
                ", origin_amt=" + origin_amt +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", account_num='" + account_num + '\'' +
                ", num=" + num +
                ", prod_name='" + prod_name + '\'' +
                ", oth_bank_name='" + oth_bank_name + '\'' +
                ", receipt_no='" + receipt_no + '\'' +
                '}';
    }
}
