package cn.com.yusys.yusp.online.client.esb.ypxt.cxypbh.resp;

import java.math.BigDecimal;

/**
 * 响应Service：通过票据号码查询押品编号
 * @author lihh
 * @version 1.0
 */
public class Service {

    private String erorcd;//响应码
    private String erortx;//响应信息
    private String guar_no;//押品编号
    private String bill_type;//票据类型
    private BigDecimal bill_amt;//票面金额

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    public String getBill_type() {
        return bill_type;
    }

    public void setBill_type(String bill_type) {
        this.bill_type = bill_type;
    }

    public BigDecimal getBill_amt() {
        return bill_amt;
    }

    public void setBill_amt(BigDecimal bill_amt) {
        this.bill_amt = bill_amt;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", guar_no='" + guar_no + '\'' +
                ", bill_type='" + bill_type + '\'' +
                ", bill_amt=" + bill_amt +
                '}';
    }
}
