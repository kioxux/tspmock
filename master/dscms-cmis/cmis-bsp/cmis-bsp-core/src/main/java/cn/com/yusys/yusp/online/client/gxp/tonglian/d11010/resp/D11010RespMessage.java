package cn.com.yusys.yusp.online.client.gxp.tonglian.d11010.resp;

/**
 * 响应Service：客户基本信息查询
 *
 * @author code-generator
 * @version 1.0
 */
public class D11010RespMessage {
    private Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D11010RespService{" +
                "message=" + message +
                '}';
    }
}
