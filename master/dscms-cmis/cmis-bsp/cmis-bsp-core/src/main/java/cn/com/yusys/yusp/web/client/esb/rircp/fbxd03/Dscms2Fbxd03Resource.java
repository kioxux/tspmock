package cn.com.yusys.yusp.web.client.esb.rircp.fbxd03;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd03.Fbxd03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd03.Fbxd03RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.req.Fbxd03ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.resp.Fbxd03RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd03）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd03Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd03Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd03
     * 交易描述：从风控获取客户详细信息
     *
     * @param fbxd03ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd03")
    protected @ResponseBody
    ResultDto<Fbxd03RespDto> fbxd03(@Validated @RequestBody Fbxd03ReqDto fbxd03ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD03.key, EsbEnum.TRADE_CODE_FBXD03.value, JSON.toJSONString(fbxd03ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd03.resp.Service();
        Fbxd03ReqService fbxd03ReqService = new Fbxd03ReqService();
        Fbxd03RespService fbxd03RespService = new Fbxd03RespService();
        Fbxd03RespDto fbxd03RespDto = new Fbxd03RespDto();
        ResultDto<Fbxd03RespDto> fbxd03ResultDto = new ResultDto<Fbxd03RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd03ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd03ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD03.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd03ReqService.setService(reqService);
            // 将fbxd03ReqService转换成fbxd03ReqServiceMap
            Map fbxd03ReqServiceMap = beanMapUtil.beanToMap(fbxd03ReqService);
            context.put("tradeDataMap", fbxd03ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD03.key, EsbEnum.TRADE_CODE_FBXD03.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD03.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD03.key, EsbEnum.TRADE_CODE_FBXD03.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd03RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd03RespService.class, Fbxd03RespService.class);
            respService = fbxd03RespService.getService();

            fbxd03ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd03ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd03RespDto
                BeanUtils.copyProperties(respService, fbxd03RespDto);
                fbxd03ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd03ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd03ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd03ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD03.key, EsbEnum.TRADE_CODE_FBXD03.value, e.getMessage());
            fbxd03ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd03ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd03ResultDto.setData(fbxd03RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD03.key, EsbEnum.TRADE_CODE_FBXD03.value, JSON.toJSONString(fbxd03ResultDto));
        return fbxd03ResultDto;
    }
}
