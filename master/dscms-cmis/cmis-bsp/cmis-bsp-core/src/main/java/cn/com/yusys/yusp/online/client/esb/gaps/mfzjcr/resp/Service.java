package cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.resp;

/**
 * 响应Service：买方资金存入
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String xybhsq;//协议编号
    private String acctto;//托管账号
    private String acctta;//托管账户名称
    private String jkcs;//交款次数

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    public String getAcctto() {
        return acctto;
    }

    public void setAcctto(String acctto) {
        this.acctto = acctto;
    }

    public String getAcctta() {
        return acctta;
    }

    public void setAcctta(String acctta) {
        this.acctta = acctta;
    }

    public String getJkcs() {
        return jkcs;
    }

    public void setJkcs(String jkcs) {
        this.jkcs = jkcs;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", xybhsq='" + xybhsq + '\'' +
                ", acctto='" + acctto + '\'' +
                ", acctta='" + acctta + '\'' +
                ", jkcs='" + jkcs + '\'' +
                '}';
    }
}
