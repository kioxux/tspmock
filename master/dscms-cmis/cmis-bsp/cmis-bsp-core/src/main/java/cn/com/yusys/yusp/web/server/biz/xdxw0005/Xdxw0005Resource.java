package cn.com.yusys.yusp.web.server.biz.xdxw0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0005.req.Xdxw0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0005.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0005.resp.Xdxw0005RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0005.req.Xdxw0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0005.resp.Xdxw0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小微营业额信息维护
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0005:小微营业额信息维护")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0005Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0005
     * 交易描述：小微营业额信息维护
     *
     * @param xdxw0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微营业额信息维护")
    @PostMapping("/xdxw0005")
    //@Idempotent({"xdcaxw0005", "#xdxw0005ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0005RespDto xdxw0005(@Validated @RequestBody Xdxw0005ReqDto xdxw0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, JSON.toJSONString(xdxw0005ReqDto));
        Xdxw0005DataReqDto xdxw0005DataReqDto = new Xdxw0005DataReqDto();// 请求Data： 小微营业额信息维护
        Xdxw0005DataRespDto xdxw0005DataRespDto = new Xdxw0005DataRespDto();// 响应Data：小微营业额信息维护
        cn.com.yusys.yusp.dto.server.biz.xdxw0005.req.Data reqData = null; // 请求Data：小微营业额信息维护
        cn.com.yusys.yusp.dto.server.biz.xdxw0005.resp.Data respData = new Data();// 响应Data：小微营业额信息维护
		Xdxw0005RespDto xdxw0005RespDto = new Xdxw0005RespDto();
		try {
            // 从 xdxw0005ReqDto获取 reqData
            reqData = xdxw0005ReqDto.getData();
            // 将 reqData 拷贝到xdxw0005DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0005DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, JSON.toJSONString(xdxw0005DataReqDto));
            ResultDto<Xdxw0005DataRespDto> xdxw0005DataResultDto = dscmsBizXwClientService.xdxw0005(xdxw0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, JSON.toJSONString(xdxw0005DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0005RespDto.setErorcd(Optional.ofNullable(xdxw0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0005RespDto.setErortx(Optional.ofNullable(xdxw0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0005DataResultDto.getCode())) {
                xdxw0005DataRespDto = xdxw0005DataResultDto.getData();
                xdxw0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, e.getMessage());
            xdxw0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0005RespDto.setDatasq(servsq);

        xdxw0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0005.key, DscmsEnum.TRADE_CODE_XDXW0005.value, JSON.toJSONString(xdxw0005RespDto));
        return xdxw0005RespDto;
    }
}
