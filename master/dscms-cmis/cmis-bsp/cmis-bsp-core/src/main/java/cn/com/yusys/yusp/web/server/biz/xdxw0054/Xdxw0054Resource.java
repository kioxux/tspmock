package cn.com.yusys.yusp.web.server.biz.xdxw0054;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0054.req.Xdxw0054ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0054.resp.Xdxw0054RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0054.req.Xdxw0054DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0054.resp.Xdxw0054DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询优抵贷损益表明细
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDXW0054:查询优抵贷损益表明细")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0054Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0054Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0054
     * 交易描述：查询优抵贷损益表明细
     *
     * @param xdxw0054ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优抵贷损益表明细")
    @PostMapping("/xdxw0054")
    protected @ResponseBody
    Xdxw0054RespDto xdxw0054(@Validated @RequestBody Xdxw0054ReqDto xdxw0054ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054ReqDto));
        Xdxw0054DataReqDto xdxw0054DataReqDto = new Xdxw0054DataReqDto();// 请求Data： 查询优抵贷损益表明细
        Xdxw0054DataRespDto xdxw0054DataRespDto = new Xdxw0054DataRespDto();// 响应Data：查询优抵贷损益表明细
        Xdxw0054RespDto xdxw0054RespDto = new Xdxw0054RespDto();// 响应Dto：查询优抵贷损益表明细

        cn.com.yusys.yusp.dto.server.biz.xdxw0054.req.Data reqData = null; // 请求Data：查询优抵贷损益表明细
        cn.com.yusys.yusp.dto.server.biz.xdxw0054.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0054.resp.Data();// 响应Data：查询优抵贷损益表明细

        try {
            // 从 xdxw0054ReqDto获取 reqData
            reqData = xdxw0054ReqDto.getData();
            // 将 reqData 拷贝到xdxw0054DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0054DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054DataReqDto));
            ResultDto<Xdxw0054DataRespDto> xdxw0054DataResultDto = dscmsBizXwClientService.xdxw0054(xdxw0054DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0054RespDto.setErorcd(Optional.ofNullable(xdxw0054DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0054RespDto.setErortx(Optional.ofNullable(xdxw0054DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0054DataResultDto.getCode())) {
                xdxw0054RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0054RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0054DataRespDto = xdxw0054DataResultDto.getData();
                // 将 xdxw0054DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0054DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, e.getMessage());
            xdxw0054RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0054RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0054RespDto.setDatasq(servsq);
        xdxw0054RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0054.key, DscmsEnum.TRADE_CODE_XDXW0054.value, JSON.toJSONString(xdxw0054RespDto));
        return xdxw0054RespDto;
    }
}
