package cn.com.yusys.yusp.web.client.esb.core.da3301;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Da3301ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.resp.Da3301RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3301.req.Da3301ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3301.req.Listnm0;
import cn.com.yusys.yusp.online.client.esb.core.da3301.req.Listnm1;
import cn.com.yusys.yusp.online.client.esb.core.da3301.req.listnm0.Record;
import cn.com.yusys.yusp.online.client.esb.core.da3301.resp.Da3301RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:抵债资产入账
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class Dscms2Da3301Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Da3301Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3301
     * 交易描述：抵债资产入账
     *
     * @param da3301ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3301")
    protected @ResponseBody
    ResultDto<Da3301RespDto> da3301(@Validated @RequestBody Da3301ReqDto da3301ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3301.key, EsbEnum.TRADE_CODE_DA3301.value, JSON.toJSONString(da3301ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3301.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3301.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3301.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3301.resp.Service();
        Da3301ReqService da3301ReqService = new Da3301ReqService();
        Da3301RespService da3301RespService = new Da3301RespService();
        Da3301RespDto da3301RespDto = new Da3301RespDto();
        ResultDto<Da3301RespDto> da3301ResultDto = new ResultDto<Da3301RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3301ReqDto转换成reqService
            BeanUtils.copyProperties(da3301ReqDto, reqService);
            List<cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Listnm0> listnm0s = Optional.ofNullable(da3301ReqDto.getListnm0()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(listnm0s)) {
                List<Record> collect = listnm0s.parallelStream().map(listnm0 -> {
                    Record record = new Record();
                    BeanUtils.copyProperties(listnm0, record);
                    return record;
                }).collect(Collectors.toList());
                Listnm0 listnm0 = new Listnm0();
                listnm0.setRecord(collect);
                reqService.setListnm0_ARRAY(listnm0);
            }
            List<cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Listnm1> listnm1s = Optional.ofNullable(da3301ReqDto.getListnm1()).orElse(new ArrayList<>());
            if (CollectionUtils.nonEmpty(listnm1s)) {
                List<cn.com.yusys.yusp.online.client.esb.core.da3301.req.listnm1.Record> collect = listnm1s.parallelStream().map(listnm1 -> {
                    cn.com.yusys.yusp.online.client.esb.core.da3301.req.listnm1.Record record = new cn.com.yusys.yusp.online.client.esb.core.da3301.req.listnm1.Record();
                    BeanUtils.copyProperties(listnm1, record);
                    return record;
                }).collect(Collectors.toList());
                Listnm1 listnm1 = new Listnm1();
                listnm1.setRecord(collect);
                reqService.setListnm1_ARRAY(listnm1);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3301.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3301ReqService.setService(reqService);
            // 将da3301ReqService转换成da3301ReqServiceMap
            Map da3301ReqServiceMap = beanMapUtil.beanToMap(da3301ReqService);
            context.put("tradeDataMap", da3301ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3301.key, EsbEnum.TRADE_CODE_DA3301.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3301.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3301.key, EsbEnum.TRADE_CODE_DA3301.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3301RespService = beanMapUtil.mapToBean(tradeDataMap, Da3301RespService.class, Da3301RespService.class);
            respService = da3301RespService.getService();

            da3301ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3301ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3301RespDto
                BeanUtils.copyProperties(respService, da3301RespDto);
                cn.com.yusys.yusp.online.client.esb.core.da3301.resp.Listnm1 listnm1 = Optional.ofNullable(respService.getListnm1()).orElse(new cn.com.yusys.yusp.online.client.esb.core.da3301.resp.Listnm1());
                List<cn.com.yusys.yusp.online.client.esb.core.da3301.resp.Record> records = Optional.ofNullable(listnm1.getRecord()).orElse(new ArrayList<>());
                if (CollectionUtils.nonEmpty(records)) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.da3301.resp.Listnm1> collect = records.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.da3301.resp.Listnm1 temp = new cn.com.yusys.yusp.dto.client.esb.core.da3301.resp.Listnm1();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    da3301RespDto.setListnm1(collect);
                }
                da3301ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3301ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3301ResultDto.setCode(EpbEnum.EPB099999.key);
                da3301ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3301.key, EsbEnum.TRADE_CODE_DA3301.value, e.getMessage());
            da3301ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3301ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3301ResultDto.setData(da3301RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3301.key, EsbEnum.TRADE_CODE_DA3301.value, JSON.toJSONString(da3301ResultDto));
        return da3301ResultDto;
    }
}
