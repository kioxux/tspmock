package cn.com.yusys.yusp.online.client.esb.core.ln3176.req;

/**
 * 请求Service：本交易用户贷款多个还款账户进行还款
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3176ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3176ReqService{" +
				"service=" + service +
				'}';
	}
}
