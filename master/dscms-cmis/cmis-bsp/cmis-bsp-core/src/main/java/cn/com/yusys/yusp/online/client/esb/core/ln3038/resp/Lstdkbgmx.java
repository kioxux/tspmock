package cn.com.yusys.yusp.online.client.esb.core.ln3038.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 14:27
 * @since 2021/5/28 14:27
 */
public class Lstdkbgmx {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3038.resp.Record> recordList;

    public List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        return "Lstdkbgmx{" +
                "recordList=" + recordList +
                '}';
    }
}
