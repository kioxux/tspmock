package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp;

/**
 * 响应Service：新信贷获取调查信息接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwd013RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
