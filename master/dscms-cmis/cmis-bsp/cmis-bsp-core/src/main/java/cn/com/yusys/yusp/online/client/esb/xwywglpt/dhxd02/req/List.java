package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req;

/**
 * <br>
 * 0.2ZRC:2021/5/28 16:17:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/28 16:17
 * @since 2021/5/28 16:17
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd02.req.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
