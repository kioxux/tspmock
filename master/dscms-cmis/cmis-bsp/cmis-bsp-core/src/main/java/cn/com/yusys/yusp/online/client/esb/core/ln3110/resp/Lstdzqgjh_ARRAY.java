package cn.com.yusys.yusp.online.client.esb.core.ln3110.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdzqgjh.Record;

import java.util.List;

public class Lstdzqgjh_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3110.resp.lstdzqgjh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdzqgjh_ARRAY{" +
                "record=" + record +
                '}';
    }
}
