package cn.com.yusys.yusp.online.client.esb.core.ln3020.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkwtxx.Record;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：贷款多委托人账户
 * @author zhugenrong
 * @version 1.0
 */
public class Lstdkwtxx_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkwtxx.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkwtxx_ARRAY{" +
                "record=" + record +
                '}';
    }
}