package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkwtxx;

import java.math.BigDecimal;

public class Record {
    private String bhchzibz;//本行出资标志

    private String bhjiejuh;//本行借据号

    private String bhzhaobz;//本行账号标志

    private String zjingjbz;//资金归集标志

    private String hkzjhzfs;//还款资金划转方式

    private String zhkaihhh;//账户开户行行号

    private String zhkaihhm;//账户开户行行名

    private String wtrkehuh;//委托人客户号

    private String wtrckuzh;//委托人存款账号

    private String wtrckzxh;//委托人存款账号子序号

    private String wtckzhao;//委托存款账号

    private String wtckzixh;//委托存款账号子序号

    private String bjghrzzh;//本金归还入账账号

    private String lxghrzzh;//利息归还入账账号

    private String bjghrzxh;//本金归还入账账号子序号

    private String lxghrzxh;//利息归还入账账号子序号

    private BigDecimal weituoje;//委托金额

    private String wtrmingc;//委托人名称

    public String getBhchzibz() {
        return bhchzibz;
    }

    public void setBhchzibz(String bhchzibz) {
        this.bhchzibz = bhchzibz;
    }

    public String getBhjiejuh() {
        return bhjiejuh;
    }

    public void setBhjiejuh(String bhjiejuh) {
        this.bhjiejuh = bhjiejuh;
    }

    public String getBhzhaobz() {
        return bhzhaobz;
    }

    public void setBhzhaobz(String bhzhaobz) {
        this.bhzhaobz = bhzhaobz;
    }

    public String getZjingjbz() {
        return zjingjbz;
    }

    public void setZjingjbz(String zjingjbz) {
        this.zjingjbz = zjingjbz;
    }

    public String getHkzjhzfs() {
        return hkzjhzfs;
    }

    public void setHkzjhzfs(String hkzjhzfs) {
        this.hkzjhzfs = hkzjhzfs;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getWtrkehuh() {
        return wtrkehuh;
    }

    public void setWtrkehuh(String wtrkehuh) {
        this.wtrkehuh = wtrkehuh;
    }

    public String getWtrckuzh() {
        return wtrckuzh;
    }

    public void setWtrckuzh(String wtrckuzh) {
        this.wtrckuzh = wtrckuzh;
    }

    public String getWtrckzxh() {
        return wtrckzxh;
    }

    public void setWtrckzxh(String wtrckzxh) {
        this.wtrckzxh = wtrckzxh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public BigDecimal getWeituoje() {
        return weituoje;
    }

    public void setWeituoje(BigDecimal weituoje) {
        this.weituoje = weituoje;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    @Override
    public String toString() {
        return "Record{" +
                "bhchzibz='" + bhchzibz + '\'' +
                ", bhjiejuh='" + bhjiejuh + '\'' +
                ", bhzhaobz='" + bhzhaobz + '\'' +
                ", zjingjbz='" + zjingjbz + '\'' +
                ", hkzjhzfs='" + hkzjhzfs + '\'' +
                ", zhkaihhh='" + zhkaihhh + '\'' +
                ", zhkaihhm='" + zhkaihhm + '\'' +
                ", wtrkehuh='" + wtrkehuh + '\'' +
                ", wtrckuzh='" + wtrckuzh + '\'' +
                ", wtrckzxh='" + wtrckzxh + '\'' +
                ", wtckzhao='" + wtckzhao + '\'' +
                ", wtckzixh='" + wtckzixh + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", bjghrzxh='" + bjghrzxh + '\'' +
                ", lxghrzxh='" + lxghrzxh + '\'' +
                ", weituoje=" + weituoje +
                ", wtrmingc='" + wtrmingc + '\'' +
                '}';
    }
}
