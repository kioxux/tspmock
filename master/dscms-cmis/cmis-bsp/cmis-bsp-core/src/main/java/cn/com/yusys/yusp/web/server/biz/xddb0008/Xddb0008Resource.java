package cn.com.yusys.yusp.web.server.biz.xddb0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0008.req.Xddb0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0008.resp.Xddb0008RespDto;
import cn.com.yusys.yusp.dto.server.xddb0008.req.Xddb0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0008.resp.Xddb0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询在线抵押信息
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "xddb0008:查询在线抵押信息")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0008Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0008
     * 交易描述：查询在线抵押信息
     *
     * @param xddb0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xddb0008:查询在线抵押信息")
    @PostMapping("/xddb0008")
    //@Idempotent({"xddb0008", "#xddb0008ReqDto.datasq"})
    protected @ResponseBody
    Xddb0008RespDto xddb0008(@Validated @RequestBody Xddb0008ReqDto xddb0008ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, JSON.toJSONString(xddb0008ReqDto));
        Xddb0008DataReqDto xddb0008DataReqDto = new Xddb0008DataReqDto();// 请求Data： 查询在线抵押信息
        Xddb0008DataRespDto xddb0008DataRespDto = new Xddb0008DataRespDto();// 响应Data：查询在线抵押信息
        Xddb0008RespDto xddb0008RespDto = new Xddb0008RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0008.req.Data reqData = null;  // 请求Data：查询在线抵押信息
        cn.com.yusys.yusp.dto.server.biz.xddb0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0008.resp.Data();// 响应Data：查询在线抵押信息
        try {
            // 从 xddb0008ReqDto获取 reqData
            reqData = xddb0008ReqDto.getData();
            // 将 reqData 拷贝到xddb0008DataReqDto
            BeanUtils.copyProperties(reqData, xddb0008DataReqDto);

            ResultDto<Xddb0008DataRespDto> xddb0008DataResultDto = dscmsBizDbClientService.xddb0008(xddb0008DataReqDto);
            // 从返回值中获取响应码和响应消息
            xddb0008RespDto.setErorcd(Optional.ofNullable(xddb0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0008RespDto.setErortx(Optional.ofNullable(xddb0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0008DataResultDto.getCode())) {
                xddb0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xddb0008DataRespDto = xddb0008DataResultDto.getData();
                // 将 xddb0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, e.getMessage());
            xddb0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0008RespDto.setDatasq(servsq);
        xddb0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0008.key, DscmsEnum.TRADE_CODE_XDDB0008.value, JSON.toJSONString(xddb0008RespDto));
        return xddb0008RespDto;
    }
}
