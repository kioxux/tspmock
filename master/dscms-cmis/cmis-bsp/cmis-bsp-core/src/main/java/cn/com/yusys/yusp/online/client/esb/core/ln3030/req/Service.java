package cn.com.yusys.yusp.online.client.esb.core.ln3030.req;

import java.math.BigDecimal;

/**
 * 请求Service：贷款资料维护
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servdt;//交易日期
    private String servti;//交易时间
    private String dkkhczbz;//开户操作标志
    private String dkjiejuh;//贷款借据号
    private String chuzhhao;//出账号
    private String hetongbh;//合同编号
    private BigDecimal hetongje;//合同金额
    private String fkfangsh;//放款金额方式
    private String hxzdkkbz;//核销自动扣款标志
    private String dkrzhzhh;//贷款入账账号
    private String dkrzhzxh;//贷款入账账号子序号
    private String fkzhouqi;//放款周期
    private String dzhifkjh;//定制放款计划
    private String kxqjjrgz;//宽限期节假日规则
    private String jixibzhi;//计息标志
    private String jfxibzhi;//计复息标志
    private String fxjxbzhi;//复息计息标志
    private String jixiguiz;//计息规则
    private String kxqzyqgz;//宽限期转逾期规则
    private String lilvfdfs;//利率浮动方式
    private String llqxkdfs;//利率期限靠档方式
    private String lilvleix;//利率类型
    private String lilvtzfs;//利率调整方式
    private String ltjixigz;//零头计息规则
    private String meictxfs;//每次摊销方式
    private String nyuelilv;//年/月利率标识
    private String yuqinyll;//逾期年月利率
    private String lilvfend;//利率分段
    private String zaoqixbz;//早起息标志
    private String wanqixbz;//晚起息标志
    private String tiexibzh;//贴息标志
    private String yushxfsh;//预收息方式
    private BigDecimal meictxbl;//每次摊销比例
    private Integer kxqzdcsh;//宽限期最大次数
    private String zclilvbh;//正常利率编号
    private String yuqillbh;//逾期利率编号
    private BigDecimal zhchlilv;//正常利率
    private BigDecimal zhxnlilv;//执行年利率
    private BigDecimal lilvfdzh;//利率浮动值
    private BigDecimal yuqililv;//逾期利率
    private BigDecimal yushxize;//预收息总额
    private String lilvtzzq;//利率调整周期
    private String lixitxzq;//利息摊销周期
    private String huankfsh;//还款方式
    private String dechligz;//等额处理规则
    private String qigscfsh;//期供生成方式
    private String duophkbz;//多频率还款标志
    private String dzhhkjih;//定制还款计划
    private String zdkoukbz;//自动扣款标志
    private String zdplkkbz;//指定文件批量扣款标识
    private String zdjqdkbz;//自动结清贷款标志
    private String dhkzhhbz;//多还款账户标志
    private String qyxhdkbz;//签约循环贷款标志
    private String qxbgtzjh;//期限变更调整计划
    private String llbgtzjh;//利率变更调整计划
    private String dcfktzjh;//多次放款调整计划
    private String tqhktzjh;//提前还款调整计划
    private String yunxtqhk;//允许提前还款
    private String xycihkrq;//下一次还款日
    private Integer leijqjsh;//累进区间期数
    private Integer ljsxqish;//累进首段期数
    private BigDecimal mqhbbili;//每期还本比例
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal meiqhbje;//每期还本金额
    private String huankzhh;//还款账号
    private String xhdkqyzh;//签约账号
    private String hkqixian;//还款期限(月)
    private String hkshxubh;//还款顺序编号
    private BigDecimal leijinzh;//累进值
    private BigDecimal baoliuje;//保留金额
    private String hkzhhzxh;//还款账号子序号
    private String xhdkzhxh;//签约账号子序号
    private String hkzhouqi;//还款周期
    private String huanbzhq;//还本周期
    private String yuqhkzhq;//逾期还款周期
    private String wtrkehuh;//委托人客户号
    private String wtrckuzh;//委托人存款账号
    private String wtckzhao;//委托存款账号
    private String bjghrzzh;//本金归还入账账号
    private String lxghrzzh;//利息归还入账账号
    private String dailimsh;//代理描述
    private String wtrckzxh;//委托人存款账号子序号
    private String wtckzixh;//委托存款账号子序号
    private String bjghrzxh;//本金归还入账账号子序号
    private String lxghrzxh;//利息归还入账账号子序号
    private String baozjzxh;//保证金账号子序号
    private String duowtrbz;//多委托人标志
    private String zbjrsygz;//贷款到期假日规则
    private String zhqxzekk;//展期需足额扣款
    private Integer dbdkkksx;//多笔贷款扣款顺序
    private String yunxdkzq;//允许贷款展期
    private Integer zhqzdcsh;//展期最大次数
    private String zqgzbhao;//展期规则编号
    private String hesuanfs;//核算方式
    private String lixizcgz;//利息转出规则
    private String lixizhgz;//利息转回规则
    private String yjfyjhes;//按应计非应计核算
    private String yiyldhes;//按一逾两呆核算
    private String dkxtkmhs;//形态分科目核算
    private String zidxtzhy;//自动形态转移
    private String shfleixi;//收费类型
    private String tqhkfjbh;//提前还款罚金编号
    private String tqhkfjmc;//提前还款罚金名称
    private BigDecimal tqhkfjfj;//提前还款附加罚金金额
    private String zhchtqtz;//正常提前通知
    private String yqcshtzh;//逾期催收通知
    private String lilvbgtz;//利率变更通知
    private String yuebgtzh;//余额变更通知
    private Integer tzhtqtsh;//通知提前天数
    private Integer tzhjgtsh;//通知间隔天数
    private String pingzhzl;//凭证种类
    private String sydkcnuo;//使用贷款承诺
    private String lmdkbzhi;//联名贷款标志
    private String bzhrdbbz;//保证人担保标志
    private String wujiflbz;//五级分类标志
    private String wujiflrq;//五级分类日期
    private String cndkjjho;//承诺贷款借据号
    private String dkgljgou;//贷款管理机构
    private String gljgleib;//管理机构类别
    private String fuhejgou;//复核机构
    private String khjingli;//客户经理
    private String pingzhma;//凭证批号
    private String pngzxhao;//凭证序号
    private BigDecimal yinhshlv;//印花税率
    private BigDecimal yinhshje;//印花税金额
    private String beizhuuu;//备注信息
    private BigDecimal hetongll;//合同利率
    private String dkzhangh;//贷款账号
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String chanpdma;//产品代码
    private String chanpmch;//产品名称
    private String kuaijilb;//会计类别
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String dkqixian;//贷款期限(月)
    private String huobdhao;//货币代号
    private BigDecimal jiejuuje;//借据金额
    private BigDecimal fafangje;//已发放金额
    private BigDecimal djiekfje;//冻结可放金额
    private BigDecimal kffangje;//可发放金额
    private BigDecimal dkuanjij;//贷款基金
    private String daikduix;//贷款对象
    private String yewufenl;//业务分类
    private String ysywleix;//衍生业务类型
    private String ysywbhao;//衍生业务编号
    private String zhqifkbz;//周期性放款标志
    private BigDecimal mcfkjebl;//每次放款金额或比例
    private String scjixirq;//上次计息日
    private String dlxxzdgz;//代理信息指定规则
    private String dlxxqzgz;//代理信息取值规则
    private String wtckywbm;//委托存款业务编码
    private Integer dailixuh;//代理序号
    private String zhngjigo;//账务机构
    private String edbizhgz;//额度币种规则
    private String edzdbizh;//额度指定币种
    private String yewusx01;//业务属性1
    private String yewusx02;//业务属性2
    private String yewusx03;//业务属性3
    private String yewusx04;//业务属性4
    private String yewusx05;//业务属性5
    private String yewusx06;//业务属性6
    private String yewusx07;//业务属性7
    private String yewusx08;//业务属性8
    private String yewusx09;//业务属性9
    private String yewusx10;//业务属性10
    private String ywsxms01;//业务属性描述1
    private String ywsxms02;//业务属性描述2
    private String ywsxms03;//业务属性描述3
    private String ywsxms04;//业务属性描述4
    private String ywsxms05;//业务属性描述5
    private String ywsxms06;//业务属性描述6
    private String ywsxms07;//业务属性描述7
    private String ywsxms08;//业务属性描述8
    private String ywsxms09;//业务属性描述9
    private String ywsxms10;//业务属性描述10
    private Lstdkfkjh_ARRAY lstdkfkjh_ARRAY;//贷款放款计划
    private Lstdkfwdj_ARRAY lstdkfwdj_ARRAY;//贷款服务登记
    private Lstdkhbjh_ARRAY lstdkhbjh_ARRAY;//贷款还本计划
    private String benjinfd;//本金分段
    private Lstdkbjfd_ARRAY lstdkbjfd_ARRAY;//本金分段登记
    private Lstdkhkfs_ARRAY lstdkhkfs_ARRAY;//贷款组合还款方式
    private Lstdkhkzh_ARRAY lstdkhkzh_ARRAY;//贷款多还款账户
    private Lstdkllfd_ARRAY lstdkllfd_ARRAY;//贷款利率分段
    private Lstdksfsj_ARRAY lstdksfsj_ARRAY;//贷款收费事件
    private Lstdkstzf_ARRAY lstdkstzf_ARRAY;//贷款受托支付
    private Lstdktxzh_ARRAY lstdktxzh_ARRAY;//贷款多贴息账户
    private Lstdkwtxx_ARRAY lstdkwtxx_ARRAY;//贷款多委托人账户
    private Lstdkzhbz_ARRAY lstdkzhbz_ARRAY;//贷款保证人信息
    private Lstdkzhlm_ARRAY lstdkzhlm_ARRAY;//贷款账户联名
    private String chanpmsh;//产品描述
    private String daikdxxf;//贷款对象细分
    private String xunhdaik;//循环贷款
    private String yansdaik;//衍生贷款
    private String chendaik;//承诺贷款
    private String butidaik;//补贴贷款
    private String yintdkbz;//银团贷款标志
    private String yintdkfs;//银团贷款方式
    private String yintleib;//银团类别
    private String yintnbcy;//内部银团成员类型
    private String yintwbcy;//外部银团成员类型
    private String fangkulx;//放款类型
    private String fkzjclfs;//放款资金处理方式
    private String hntmifku;//允许对行内同名账户放款
    private String hnftmfku;//允许对行内非同名账户放款
    private String neibufku;//允许对内部账户放款
    private String jixibjgz;//计息本金规则
    private String lixijsff;//利息计算方法
    private String jixitwgz;//计息头尾规则
    private BigDecimal jixizxje;//计息最小金额
    private String jixisrgz;//计息舍入规则
    private String srzxdanw;//舍入最小单位
    private String lilvqixx;//利率期限
    private String fdjixibz;//分段计息标志
    private String yuqitzfs;//逾期利率调整方式
    private String yuqitzzq;//逾期利率调整周期
    private String yqfxfdfs;//逾期罚息浮动方式
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    private String fulilvbh;//复利利率编号
    private String fulilvny;//复利利率年月标识
    private BigDecimal fulililv;//复利利率
    private String fulitzfs;//复利利率调整方式
    private String fulitzzq;//复利利率调整周期
    private String jitiguiz;//计提规则
    private String butijejs;//补贴金额计算方式
    private String tiaozhkf;//允许调整还款方式
    private String scihkrbz;//首次还款日模式
    private String mqihkfsh;//末期还款方式
    private String yunxsuoq;//允许缩期
    private String bzuekkfs;//不足额扣款方式
    private String yqbzkkfs;//逾期不足额扣款方式
    private String hntmihku;//允许行内同名账户还款
    private String hnftmhku;//允许行内非同名帐户还款
    private String nbuzhhku;//允许内部账户还款
    private Integer tiqhksdq;//提前还款锁定期
    private String sfyxkuxq;//是否有宽限期
    private Integer kuanxqts;//宽限期天数
    private String kxqjixgz;//宽限期计息方式
    private String kxqhjxgz;//宽限期后计息规则
    private String kxqshxgz;//宽限期收息规则
    private String zhqizcqx;//展期最长期限(月)
    private String fulifdfs;//复利利率浮动方式
    private BigDecimal fulifdzh;//复利利率浮动值
    private String bwchapbz;//表外产品
    private String yqllcklx;//逾期利率参考类型
    private String flllcklx;//复利利率参考类型
    private String kshchpdm;//可售产品代码
    private String kshchpmc;//可售产品名称
    private String dkczhzhh;//贷款出账号
    private String dkdbfshi;//贷款担保方式
    private String dkyongtu;//贷款用途
    private String fkjzhfsh;//放款记账方式
    private String shtzfhxm;//受托支付业务编码
    private String bwhesdma;//表外核算码
    private String wtrmingc;//委托人名称
    private String ysxlyzhh;//预收息扣息来源账号
    private String ysxlyzxh;//预收息扣息来源账号子序号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getChuzhhao() {
        return chuzhhao;
    }

    public void setChuzhhao(String chuzhhao) {
        this.chuzhhao = chuzhhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public String getFkfangsh() {
        return fkfangsh;
    }

    public void setFkfangsh(String fkfangsh) {
        this.fkfangsh = fkfangsh;
    }

    public String getHxzdkkbz() {
        return hxzdkkbz;
    }

    public void setHxzdkkbz(String hxzdkkbz) {
        this.hxzdkkbz = hxzdkkbz;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public String getFkzhouqi() {
        return fkzhouqi;
    }

    public void setFkzhouqi(String fkzhouqi) {
        this.fkzhouqi = fkzhouqi;
    }

    public String getDzhifkjh() {
        return dzhifkjh;
    }

    public void setDzhifkjh(String dzhifkjh) {
        this.dzhifkjh = dzhifkjh;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getJixibzhi() {
        return jixibzhi;
    }

    public void setJixibzhi(String jixibzhi) {
        this.jixibzhi = jixibzhi;
    }

    public String getJfxibzhi() {
        return jfxibzhi;
    }

    public void setJfxibzhi(String jfxibzhi) {
        this.jfxibzhi = jfxibzhi;
    }

    public String getFxjxbzhi() {
        return fxjxbzhi;
    }

    public void setFxjxbzhi(String fxjxbzhi) {
        this.fxjxbzhi = fxjxbzhi;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public String getKxqzyqgz() {
        return kxqzyqgz;
    }

    public void setKxqzyqgz(String kxqzyqgz) {
        this.kxqzyqgz = kxqzyqgz;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getLlqxkdfs() {
        return llqxkdfs;
    }

    public void setLlqxkdfs(String llqxkdfs) {
        this.llqxkdfs = llqxkdfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLtjixigz() {
        return ltjixigz;
    }

    public void setLtjixigz(String ltjixigz) {
        this.ltjixigz = ltjixigz;
    }

    public String getMeictxfs() {
        return meictxfs;
    }

    public void setMeictxfs(String meictxfs) {
        this.meictxfs = meictxfs;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public String getLilvfend() {
        return lilvfend;
    }

    public void setLilvfend(String lilvfend) {
        this.lilvfend = lilvfend;
    }

    public String getZaoqixbz() {
        return zaoqixbz;
    }

    public void setZaoqixbz(String zaoqixbz) {
        this.zaoqixbz = zaoqixbz;
    }

    public String getWanqixbz() {
        return wanqixbz;
    }

    public void setWanqixbz(String wanqixbz) {
        this.wanqixbz = wanqixbz;
    }

    public String getTiexibzh() {
        return tiexibzh;
    }

    public void setTiexibzh(String tiexibzh) {
        this.tiexibzh = tiexibzh;
    }

    public String getYushxfsh() {
        return yushxfsh;
    }

    public void setYushxfsh(String yushxfsh) {
        this.yushxfsh = yushxfsh;
    }

    public BigDecimal getMeictxbl() {
        return meictxbl;
    }

    public void setMeictxbl(BigDecimal meictxbl) {
        this.meictxbl = meictxbl;
    }

    public Integer getKxqzdcsh() {
        return kxqzdcsh;
    }

    public void setKxqzdcsh(Integer kxqzdcsh) {
        this.kxqzdcsh = kxqzdcsh;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getZhxnlilv() {
        return zhxnlilv;
    }

    public void setZhxnlilv(BigDecimal zhxnlilv) {
        this.zhxnlilv = zhxnlilv;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public BigDecimal getYushxize() {
        return yushxize;
    }

    public void setYushxize(BigDecimal yushxize) {
        this.yushxize = yushxize;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLixitxzq() {
        return lixitxzq;
    }

    public void setLixitxzq(String lixitxzq) {
        this.lixitxzq = lixitxzq;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getZdplkkbz() {
        return zdplkkbz;
    }

    public void setZdplkkbz(String zdplkkbz) {
        this.zdplkkbz = zdplkkbz;
    }

    public String getZdjqdkbz() {
        return zdjqdkbz;
    }

    public void setZdjqdkbz(String zdjqdkbz) {
        this.zdjqdkbz = zdjqdkbz;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public String getQyxhdkbz() {
        return qyxhdkbz;
    }

    public void setQyxhdkbz(String qyxhdkbz) {
        this.qyxhdkbz = qyxhdkbz;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    public String getXycihkrq() {
        return xycihkrq;
    }

    public void setXycihkrq(String xycihkrq) {
        this.xycihkrq = xycihkrq;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public Integer getLjsxqish() {
        return ljsxqish;
    }

    public void setLjsxqish(Integer ljsxqish) {
        this.ljsxqish = ljsxqish;
    }

    public BigDecimal getMqhbbili() {
        return mqhbbili;
    }

    public void setMqhbbili(BigDecimal mqhbbili) {
        this.mqhbbili = mqhbbili;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getXhdkqyzh() {
        return xhdkqyzh;
    }

    public void setXhdkqyzh(String xhdkqyzh) {
        this.xhdkqyzh = xhdkqyzh;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getXhdkzhxh() {
        return xhdkzhxh;
    }

    public void setXhdkzhxh(String xhdkzhxh) {
        this.xhdkzhxh = xhdkzhxh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public String getWtrkehuh() {
        return wtrkehuh;
    }

    public void setWtrkehuh(String wtrkehuh) {
        this.wtrkehuh = wtrkehuh;
    }

    public String getWtrckuzh() {
        return wtrckuzh;
    }

    public void setWtrckuzh(String wtrckuzh) {
        this.wtrckuzh = wtrckuzh;
    }

    public String getWtckzhao() {
        return wtckzhao;
    }

    public void setWtckzhao(String wtckzhao) {
        this.wtckzhao = wtckzhao;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getDailimsh() {
        return dailimsh;
    }

    public void setDailimsh(String dailimsh) {
        this.dailimsh = dailimsh;
    }

    public String getWtrckzxh() {
        return wtrckzxh;
    }

    public void setWtrckzxh(String wtrckzxh) {
        this.wtrckzxh = wtrckzxh;
    }

    public String getWtckzixh() {
        return wtckzixh;
    }

    public void setWtckzixh(String wtckzixh) {
        this.wtckzixh = wtckzixh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getBaozjzxh() {
        return baozjzxh;
    }

    public void setBaozjzxh(String baozjzxh) {
        this.baozjzxh = baozjzxh;
    }

    public String getDuowtrbz() {
        return duowtrbz;
    }

    public void setDuowtrbz(String duowtrbz) {
        this.duowtrbz = duowtrbz;
    }

    public String getZbjrsygz() {
        return zbjrsygz;
    }

    public void setZbjrsygz(String zbjrsygz) {
        this.zbjrsygz = zbjrsygz;
    }

    public String getZhqxzekk() {
        return zhqxzekk;
    }

    public void setZhqxzekk(String zhqxzekk) {
        this.zhqxzekk = zhqxzekk;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public String getYunxdkzq() {
        return yunxdkzq;
    }

    public void setYunxdkzq(String yunxdkzq) {
        this.yunxdkzq = yunxdkzq;
    }

    public Integer getZhqzdcsh() {
        return zhqzdcsh;
    }

    public void setZhqzdcsh(Integer zhqzdcsh) {
        this.zhqzdcsh = zhqzdcsh;
    }

    public String getZqgzbhao() {
        return zqgzbhao;
    }

    public void setZqgzbhao(String zqgzbhao) {
        this.zqgzbhao = zqgzbhao;
    }

    public String getHesuanfs() {
        return hesuanfs;
    }

    public void setHesuanfs(String hesuanfs) {
        this.hesuanfs = hesuanfs;
    }

    public String getLixizcgz() {
        return lixizcgz;
    }

    public void setLixizcgz(String lixizcgz) {
        this.lixizcgz = lixizcgz;
    }

    public String getLixizhgz() {
        return lixizhgz;
    }

    public void setLixizhgz(String lixizhgz) {
        this.lixizhgz = lixizhgz;
    }

    public String getYjfyjhes() {
        return yjfyjhes;
    }

    public void setYjfyjhes(String yjfyjhes) {
        this.yjfyjhes = yjfyjhes;
    }

    public String getYiyldhes() {
        return yiyldhes;
    }

    public void setYiyldhes(String yiyldhes) {
        this.yiyldhes = yiyldhes;
    }

    public String getDkxtkmhs() {
        return dkxtkmhs;
    }

    public void setDkxtkmhs(String dkxtkmhs) {
        this.dkxtkmhs = dkxtkmhs;
    }

    public String getZidxtzhy() {
        return zidxtzhy;
    }

    public void setZidxtzhy(String zidxtzhy) {
        this.zidxtzhy = zidxtzhy;
    }

    public String getShfleixi() {
        return shfleixi;
    }

    public void setShfleixi(String shfleixi) {
        this.shfleixi = shfleixi;
    }

    public String getTqhkfjbh() {
        return tqhkfjbh;
    }

    public void setTqhkfjbh(String tqhkfjbh) {
        this.tqhkfjbh = tqhkfjbh;
    }

    public String getTqhkfjmc() {
        return tqhkfjmc;
    }

    public void setTqhkfjmc(String tqhkfjmc) {
        this.tqhkfjmc = tqhkfjmc;
    }

    public BigDecimal getTqhkfjfj() {
        return tqhkfjfj;
    }

    public void setTqhkfjfj(BigDecimal tqhkfjfj) {
        this.tqhkfjfj = tqhkfjfj;
    }

    public String getZhchtqtz() {
        return zhchtqtz;
    }

    public void setZhchtqtz(String zhchtqtz) {
        this.zhchtqtz = zhchtqtz;
    }

    public String getYqcshtzh() {
        return yqcshtzh;
    }

    public void setYqcshtzh(String yqcshtzh) {
        this.yqcshtzh = yqcshtzh;
    }

    public String getLilvbgtz() {
        return lilvbgtz;
    }

    public void setLilvbgtz(String lilvbgtz) {
        this.lilvbgtz = lilvbgtz;
    }

    public String getYuebgtzh() {
        return yuebgtzh;
    }

    public void setYuebgtzh(String yuebgtzh) {
        this.yuebgtzh = yuebgtzh;
    }

    public Integer getTzhtqtsh() {
        return tzhtqtsh;
    }

    public void setTzhtqtsh(Integer tzhtqtsh) {
        this.tzhtqtsh = tzhtqtsh;
    }

    public Integer getTzhjgtsh() {
        return tzhjgtsh;
    }

    public void setTzhjgtsh(Integer tzhjgtsh) {
        this.tzhjgtsh = tzhjgtsh;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getSydkcnuo() {
        return sydkcnuo;
    }

    public void setSydkcnuo(String sydkcnuo) {
        this.sydkcnuo = sydkcnuo;
    }

    public String getLmdkbzhi() {
        return lmdkbzhi;
    }

    public void setLmdkbzhi(String lmdkbzhi) {
        this.lmdkbzhi = lmdkbzhi;
    }

    public String getBzhrdbbz() {
        return bzhrdbbz;
    }

    public void setBzhrdbbz(String bzhrdbbz) {
        this.bzhrdbbz = bzhrdbbz;
    }

    public String getWujiflbz() {
        return wujiflbz;
    }

    public void setWujiflbz(String wujiflbz) {
        this.wujiflbz = wujiflbz;
    }

    public String getWujiflrq() {
        return wujiflrq;
    }

    public void setWujiflrq(String wujiflrq) {
        this.wujiflrq = wujiflrq;
    }

    public String getCndkjjho() {
        return cndkjjho;
    }

    public void setCndkjjho(String cndkjjho) {
        this.cndkjjho = cndkjjho;
    }

    public String getDkgljgou() {
        return dkgljgou;
    }

    public void setDkgljgou(String dkgljgou) {
        this.dkgljgou = dkgljgou;
    }

    public String getGljgleib() {
        return gljgleib;
    }

    public void setGljgleib(String gljgleib) {
        this.gljgleib = gljgleib;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public BigDecimal getYinhshlv() {
        return yinhshlv;
    }

    public void setYinhshlv(BigDecimal yinhshlv) {
        this.yinhshlv = yinhshlv;
    }

    public BigDecimal getYinhshje() {
        return yinhshje;
    }

    public void setYinhshje(BigDecimal yinhshje) {
        this.yinhshje = yinhshje;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDkqixian() {
        return dkqixian;
    }

    public void setDkqixian(String dkqixian) {
        this.dkqixian = dkqixian;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getFafangje() {
        return fafangje;
    }

    public void setFafangje(BigDecimal fafangje) {
        this.fafangje = fafangje;
    }

    public BigDecimal getDjiekfje() {
        return djiekfje;
    }

    public void setDjiekfje(BigDecimal djiekfje) {
        this.djiekfje = djiekfje;
    }

    public BigDecimal getKffangje() {
        return kffangje;
    }

    public void setKffangje(BigDecimal kffangje) {
        this.kffangje = kffangje;
    }

    public BigDecimal getDkuanjij() {
        return dkuanjij;
    }

    public void setDkuanjij(BigDecimal dkuanjij) {
        this.dkuanjij = dkuanjij;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public String getYewufenl() {
        return yewufenl;
    }

    public void setYewufenl(String yewufenl) {
        this.yewufenl = yewufenl;
    }

    public String getYsywleix() {
        return ysywleix;
    }

    public void setYsywleix(String ysywleix) {
        this.ysywleix = ysywleix;
    }

    public String getYsywbhao() {
        return ysywbhao;
    }

    public void setYsywbhao(String ysywbhao) {
        this.ysywbhao = ysywbhao;
    }

    public String getZhqifkbz() {
        return zhqifkbz;
    }

    public void setZhqifkbz(String zhqifkbz) {
        this.zhqifkbz = zhqifkbz;
    }

    public BigDecimal getMcfkjebl() {
        return mcfkjebl;
    }

    public void setMcfkjebl(BigDecimal mcfkjebl) {
        this.mcfkjebl = mcfkjebl;
    }

    public String getScjixirq() {
        return scjixirq;
    }

    public void setScjixirq(String scjixirq) {
        this.scjixirq = scjixirq;
    }

    public String getDlxxzdgz() {
        return dlxxzdgz;
    }

    public void setDlxxzdgz(String dlxxzdgz) {
        this.dlxxzdgz = dlxxzdgz;
    }

    public String getDlxxqzgz() {
        return dlxxqzgz;
    }

    public void setDlxxqzgz(String dlxxqzgz) {
        this.dlxxqzgz = dlxxqzgz;
    }

    public String getWtckywbm() {
        return wtckywbm;
    }

    public void setWtckywbm(String wtckywbm) {
        this.wtckywbm = wtckywbm;
    }

    public Integer getDailixuh() {
        return dailixuh;
    }

    public void setDailixuh(Integer dailixuh) {
        this.dailixuh = dailixuh;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getEdbizhgz() {
        return edbizhgz;
    }

    public void setEdbizhgz(String edbizhgz) {
        this.edbizhgz = edbizhgz;
    }

    public String getEdzdbizh() {
        return edzdbizh;
    }

    public void setEdzdbizh(String edzdbizh) {
        this.edzdbizh = edzdbizh;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getYewusx02() {
        return yewusx02;
    }

    public void setYewusx02(String yewusx02) {
        this.yewusx02 = yewusx02;
    }

    public String getYewusx03() {
        return yewusx03;
    }

    public void setYewusx03(String yewusx03) {
        this.yewusx03 = yewusx03;
    }

    public String getYewusx04() {
        return yewusx04;
    }

    public void setYewusx04(String yewusx04) {
        this.yewusx04 = yewusx04;
    }

    public String getYewusx05() {
        return yewusx05;
    }

    public void setYewusx05(String yewusx05) {
        this.yewusx05 = yewusx05;
    }

    public String getYewusx06() {
        return yewusx06;
    }

    public void setYewusx06(String yewusx06) {
        this.yewusx06 = yewusx06;
    }

    public String getYewusx07() {
        return yewusx07;
    }

    public void setYewusx07(String yewusx07) {
        this.yewusx07 = yewusx07;
    }

    public String getYewusx08() {
        return yewusx08;
    }

    public void setYewusx08(String yewusx08) {
        this.yewusx08 = yewusx08;
    }

    public String getYewusx09() {
        return yewusx09;
    }

    public void setYewusx09(String yewusx09) {
        this.yewusx09 = yewusx09;
    }

    public String getYewusx10() {
        return yewusx10;
    }

    public void setYewusx10(String yewusx10) {
        this.yewusx10 = yewusx10;
    }

    public String getYwsxms01() {
        return ywsxms01;
    }

    public void setYwsxms01(String ywsxms01) {
        this.ywsxms01 = ywsxms01;
    }

    public String getYwsxms02() {
        return ywsxms02;
    }

    public void setYwsxms02(String ywsxms02) {
        this.ywsxms02 = ywsxms02;
    }

    public String getYwsxms03() {
        return ywsxms03;
    }

    public void setYwsxms03(String ywsxms03) {
        this.ywsxms03 = ywsxms03;
    }

    public String getYwsxms04() {
        return ywsxms04;
    }

    public void setYwsxms04(String ywsxms04) {
        this.ywsxms04 = ywsxms04;
    }

    public String getYwsxms05() {
        return ywsxms05;
    }

    public void setYwsxms05(String ywsxms05) {
        this.ywsxms05 = ywsxms05;
    }

    public String getYwsxms06() {
        return ywsxms06;
    }

    public void setYwsxms06(String ywsxms06) {
        this.ywsxms06 = ywsxms06;
    }

    public String getYwsxms07() {
        return ywsxms07;
    }

    public void setYwsxms07(String ywsxms07) {
        this.ywsxms07 = ywsxms07;
    }

    public String getYwsxms08() {
        return ywsxms08;
    }

    public void setYwsxms08(String ywsxms08) {
        this.ywsxms08 = ywsxms08;
    }

    public String getYwsxms09() {
        return ywsxms09;
    }

    public void setYwsxms09(String ywsxms09) {
        this.ywsxms09 = ywsxms09;
    }

    public String getYwsxms10() {
        return ywsxms10;
    }

    public void setYwsxms10(String ywsxms10) {
        this.ywsxms10 = ywsxms10;
    }

    public Lstdkfkjh_ARRAY getLstdkfkjh_ARRAY() {
        return lstdkfkjh_ARRAY;
    }

    public void setLstdkfkjh_ARRAY(Lstdkfkjh_ARRAY lstdkfkjh_ARRAY) {
        this.lstdkfkjh_ARRAY = lstdkfkjh_ARRAY;
    }

    public Lstdkfwdj_ARRAY getLstdkfwdj_ARRAY() {
        return lstdkfwdj_ARRAY;
    }

    public void setLstdkfwdj_ARRAY(Lstdkfwdj_ARRAY lstdkfwdj_ARRAY) {
        this.lstdkfwdj_ARRAY = lstdkfwdj_ARRAY;
    }

    public Lstdkhbjh_ARRAY getLstdkhbjh_ARRAY() {
        return lstdkhbjh_ARRAY;
    }

    public void setLstdkhbjh_ARRAY(Lstdkhbjh_ARRAY lstdkhbjh_ARRAY) {
        this.lstdkhbjh_ARRAY = lstdkhbjh_ARRAY;
    }

    public String getBenjinfd() {
        return benjinfd;
    }

    public void setBenjinfd(String benjinfd) {
        this.benjinfd = benjinfd;
    }

    public Lstdkbjfd_ARRAY getLstdkbjfd_ARRAY() {
        return lstdkbjfd_ARRAY;
    }

    public void setLstdkbjfd_ARRAY(Lstdkbjfd_ARRAY lstdkbjfd_ARRAY) {
        this.lstdkbjfd_ARRAY = lstdkbjfd_ARRAY;
    }

    public Lstdkhkfs_ARRAY getLstdkhkfs_ARRAY() {
        return lstdkhkfs_ARRAY;
    }

    public void setLstdkhkfs_ARRAY(Lstdkhkfs_ARRAY lstdkhkfs_ARRAY) {
        this.lstdkhkfs_ARRAY = lstdkhkfs_ARRAY;
    }

    public Lstdkhkzh_ARRAY getLstdkhkzh_ARRAY() {
        return lstdkhkzh_ARRAY;
    }

    public void setLstdkhkzh_ARRAY(Lstdkhkzh_ARRAY lstdkhkzh_ARRAY) {
        this.lstdkhkzh_ARRAY = lstdkhkzh_ARRAY;
    }

    public Lstdkllfd_ARRAY getLstdkllfd_ARRAY() {
        return lstdkllfd_ARRAY;
    }

    public void setLstdkllfd_ARRAY(Lstdkllfd_ARRAY lstdkllfd_ARRAY) {
        this.lstdkllfd_ARRAY = lstdkllfd_ARRAY;
    }

    public Lstdksfsj_ARRAY getLstdksfsj_ARRAY() {
        return lstdksfsj_ARRAY;
    }

    public void setLstdksfsj_ARRAY(Lstdksfsj_ARRAY lstdksfsj_ARRAY) {
        this.lstdksfsj_ARRAY = lstdksfsj_ARRAY;
    }

    public Lstdkstzf_ARRAY getLstdkstzf_ARRAY() {
        return lstdkstzf_ARRAY;
    }

    public void setLstdkstzf_ARRAY(Lstdkstzf_ARRAY lstdkstzf_ARRAY) {
        this.lstdkstzf_ARRAY = lstdkstzf_ARRAY;
    }

    public Lstdktxzh_ARRAY getLstdktxzh_ARRAY() {
        return lstdktxzh_ARRAY;
    }

    public void setLstdktxzh_ARRAY(Lstdktxzh_ARRAY lstdktxzh_ARRAY) {
        this.lstdktxzh_ARRAY = lstdktxzh_ARRAY;
    }

    public Lstdkwtxx_ARRAY getLstdkwtxx_ARRAY() {
        return lstdkwtxx_ARRAY;
    }

    public void setLstdkwtxx_ARRAY(Lstdkwtxx_ARRAY lstdkwtxx_ARRAY) {
        this.lstdkwtxx_ARRAY = lstdkwtxx_ARRAY;
    }

    public Lstdkzhbz_ARRAY getLstdkzhbz_ARRAY() {
        return lstdkzhbz_ARRAY;
    }

    public void setLstdkzhbz_ARRAY(Lstdkzhbz_ARRAY lstdkzhbz_ARRAY) {
        this.lstdkzhbz_ARRAY = lstdkzhbz_ARRAY;
    }

    public Lstdkzhlm_ARRAY getLstdkzhlm_ARRAY() {
        return lstdkzhlm_ARRAY;
    }

    public void setLstdkzhlm_ARRAY(Lstdkzhlm_ARRAY lstdkzhlm_ARRAY) {
        this.lstdkzhlm_ARRAY = lstdkzhlm_ARRAY;
    }

    public String getChanpmsh() {
        return chanpmsh;
    }

    public void setChanpmsh(String chanpmsh) {
        this.chanpmsh = chanpmsh;
    }

    public String getDaikdxxf() {
        return daikdxxf;
    }

    public void setDaikdxxf(String daikdxxf) {
        this.daikdxxf = daikdxxf;
    }

    public String getXunhdaik() {
        return xunhdaik;
    }

    public void setXunhdaik(String xunhdaik) {
        this.xunhdaik = xunhdaik;
    }

    public String getYansdaik() {
        return yansdaik;
    }

    public void setYansdaik(String yansdaik) {
        this.yansdaik = yansdaik;
    }

    public String getChendaik() {
        return chendaik;
    }

    public void setChendaik(String chendaik) {
        this.chendaik = chendaik;
    }

    public String getButidaik() {
        return butidaik;
    }

    public void setButidaik(String butidaik) {
        this.butidaik = butidaik;
    }

    public String getYintdkbz() {
        return yintdkbz;
    }

    public void setYintdkbz(String yintdkbz) {
        this.yintdkbz = yintdkbz;
    }

    public String getYintdkfs() {
        return yintdkfs;
    }

    public void setYintdkfs(String yintdkfs) {
        this.yintdkfs = yintdkfs;
    }

    public String getYintleib() {
        return yintleib;
    }

    public void setYintleib(String yintleib) {
        this.yintleib = yintleib;
    }

    public String getYintnbcy() {
        return yintnbcy;
    }

    public void setYintnbcy(String yintnbcy) {
        this.yintnbcy = yintnbcy;
    }

    public String getYintwbcy() {
        return yintwbcy;
    }

    public void setYintwbcy(String yintwbcy) {
        this.yintwbcy = yintwbcy;
    }

    public String getFangkulx() {
        return fangkulx;
    }

    public void setFangkulx(String fangkulx) {
        this.fangkulx = fangkulx;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getHntmifku() {
        return hntmifku;
    }

    public void setHntmifku(String hntmifku) {
        this.hntmifku = hntmifku;
    }

    public String getHnftmfku() {
        return hnftmfku;
    }

    public void setHnftmfku(String hnftmfku) {
        this.hnftmfku = hnftmfku;
    }

    public String getNeibufku() {
        return neibufku;
    }

    public void setNeibufku(String neibufku) {
        this.neibufku = neibufku;
    }

    public String getJixibjgz() {
        return jixibjgz;
    }

    public void setJixibjgz(String jixibjgz) {
        this.jixibjgz = jixibjgz;
    }

    public String getLixijsff() {
        return lixijsff;
    }

    public void setLixijsff(String lixijsff) {
        this.lixijsff = lixijsff;
    }

    public String getJixitwgz() {
        return jixitwgz;
    }

    public void setJixitwgz(String jixitwgz) {
        this.jixitwgz = jixitwgz;
    }

    public BigDecimal getJixizxje() {
        return jixizxje;
    }

    public void setJixizxje(BigDecimal jixizxje) {
        this.jixizxje = jixizxje;
    }

    public String getJixisrgz() {
        return jixisrgz;
    }

    public void setJixisrgz(String jixisrgz) {
        this.jixisrgz = jixisrgz;
    }

    public String getSrzxdanw() {
        return srzxdanw;
    }

    public void setSrzxdanw(String srzxdanw) {
        this.srzxdanw = srzxdanw;
    }

    public String getLilvqixx() {
        return lilvqixx;
    }

    public void setLilvqixx(String lilvqixx) {
        this.lilvqixx = lilvqixx;
    }

    public String getFdjixibz() {
        return fdjixibz;
    }

    public void setFdjixibz(String fdjixibz) {
        this.fdjixibz = fdjixibz;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getJitiguiz() {
        return jitiguiz;
    }

    public void setJitiguiz(String jitiguiz) {
        this.jitiguiz = jitiguiz;
    }

    public String getButijejs() {
        return butijejs;
    }

    public void setButijejs(String butijejs) {
        this.butijejs = butijejs;
    }

    public String getTiaozhkf() {
        return tiaozhkf;
    }

    public void setTiaozhkf(String tiaozhkf) {
        this.tiaozhkf = tiaozhkf;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public String getYunxsuoq() {
        return yunxsuoq;
    }

    public void setYunxsuoq(String yunxsuoq) {
        this.yunxsuoq = yunxsuoq;
    }

    public String getBzuekkfs() {
        return bzuekkfs;
    }

    public void setBzuekkfs(String bzuekkfs) {
        this.bzuekkfs = bzuekkfs;
    }

    public String getYqbzkkfs() {
        return yqbzkkfs;
    }

    public void setYqbzkkfs(String yqbzkkfs) {
        this.yqbzkkfs = yqbzkkfs;
    }

    public String getHntmihku() {
        return hntmihku;
    }

    public void setHntmihku(String hntmihku) {
        this.hntmihku = hntmihku;
    }

    public String getHnftmhku() {
        return hnftmhku;
    }

    public void setHnftmhku(String hnftmhku) {
        this.hnftmhku = hnftmhku;
    }

    public String getNbuzhhku() {
        return nbuzhhku;
    }

    public void setNbuzhhku(String nbuzhhku) {
        this.nbuzhhku = nbuzhhku;
    }

    public Integer getTiqhksdq() {
        return tiqhksdq;
    }

    public void setTiqhksdq(Integer tiqhksdq) {
        this.tiqhksdq = tiqhksdq;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public String getKxqjixgz() {
        return kxqjixgz;
    }

    public void setKxqjixgz(String kxqjixgz) {
        this.kxqjixgz = kxqjixgz;
    }

    public String getKxqhjxgz() {
        return kxqhjxgz;
    }

    public void setKxqhjxgz(String kxqhjxgz) {
        this.kxqhjxgz = kxqhjxgz;
    }

    public String getKxqshxgz() {
        return kxqshxgz;
    }

    public void setKxqshxgz(String kxqshxgz) {
        this.kxqshxgz = kxqshxgz;
    }

    public String getZhqizcqx() {
        return zhqizcqx;
    }

    public void setZhqizcqx(String zhqizcqx) {
        this.zhqizcqx = zhqizcqx;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getBwchapbz() {
        return bwchapbz;
    }

    public void setBwchapbz(String bwchapbz) {
        this.bwchapbz = bwchapbz;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getKshchpdm() {
        return kshchpdm;
    }

    public void setKshchpdm(String kshchpdm) {
        this.kshchpdm = kshchpdm;
    }

    public String getKshchpmc() {
        return kshchpmc;
    }

    public void setKshchpmc(String kshchpmc) {
        this.kshchpmc = kshchpmc;
    }

    public String getDkczhzhh() {
        return dkczhzhh;
    }

    public void setDkczhzhh(String dkczhzhh) {
        this.dkczhzhh = dkczhzhh;
    }

    public String getDkdbfshi() {
        return dkdbfshi;
    }

    public void setDkdbfshi(String dkdbfshi) {
        this.dkdbfshi = dkdbfshi;
    }

    public String getDkyongtu() {
        return dkyongtu;
    }

    public void setDkyongtu(String dkyongtu) {
        this.dkyongtu = dkyongtu;
    }

    public String getFkjzhfsh() {
        return fkjzhfsh;
    }

    public void setFkjzhfsh(String fkjzhfsh) {
        this.fkjzhfsh = fkjzhfsh;
    }

    public String getShtzfhxm() {
        return shtzfhxm;
    }

    public void setShtzfhxm(String shtzfhxm) {
        this.shtzfhxm = shtzfhxm;
    }

    public String getBwhesdma() {
        return bwhesdma;
    }

    public void setBwhesdma(String bwhesdma) {
        this.bwhesdma = bwhesdma;
    }

    public String getWtrmingc() {
        return wtrmingc;
    }

    public void setWtrmingc(String wtrmingc) {
        this.wtrmingc = wtrmingc;
    }

    public String getYsxlyzhh() {
        return ysxlyzhh;
    }

    public void setYsxlyzhh(String ysxlyzhh) {
        this.ysxlyzhh = ysxlyzhh;
    }

    public String getYsxlyzxh() {
        return ysxlyzxh;
    }

    public void setYsxlyzxh(String ysxlyzxh) {
        this.ysxlyzxh = ysxlyzxh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "chuzhhao='" + chuzhhao + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "hetongje='" + hetongje + '\'' +
                "fkfangsh='" + fkfangsh + '\'' +
                "hxzdkkbz='" + hxzdkkbz + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "fkzhouqi='" + fkzhouqi + '\'' +
                "dzhifkjh='" + dzhifkjh + '\'' +
                "kxqjjrgz='" + kxqjjrgz + '\'' +
                "jixibzhi='" + jixibzhi + '\'' +
                "jfxibzhi='" + jfxibzhi + '\'' +
                "fxjxbzhi='" + fxjxbzhi + '\'' +
                "jixiguiz='" + jixiguiz + '\'' +
                "kxqzyqgz='" + kxqzyqgz + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "llqxkdfs='" + llqxkdfs + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "ltjixigz='" + ltjixigz + '\'' +
                "meictxfs='" + meictxfs + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "yuqinyll='" + yuqinyll + '\'' +
                "lilvfend='" + lilvfend + '\'' +
                "zaoqixbz='" + zaoqixbz + '\'' +
                "wanqixbz='" + wanqixbz + '\'' +
                "tiexibzh='" + tiexibzh + '\'' +
                "yushxfsh='" + yushxfsh + '\'' +
                "meictxbl='" + meictxbl + '\'' +
                "kxqzdcsh='" + kxqzdcsh + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                "yuqillbh='" + yuqillbh + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "zhxnlilv='" + zhxnlilv + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "yushxize='" + yushxize + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lixitxzq='" + lixitxzq + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "dechligz='" + dechligz + '\'' +
                "qigscfsh='" + qigscfsh + '\'' +
                "duophkbz='" + duophkbz + '\'' +
                "dzhhkjih='" + dzhhkjih + '\'' +
                "zdkoukbz='" + zdkoukbz + '\'' +
                "zdplkkbz='" + zdplkkbz + '\'' +
                "zdjqdkbz='" + zdjqdkbz + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "qyxhdkbz='" + qyxhdkbz + '\'' +
                "qxbgtzjh='" + qxbgtzjh + '\'' +
                "llbgtzjh='" + llbgtzjh + '\'' +
                "dcfktzjh='" + dcfktzjh + '\'' +
                "tqhktzjh='" + tqhktzjh + '\'' +
                "yunxtqhk='" + yunxtqhk + '\'' +
                "xycihkrq='" + xycihkrq + '\'' +
                "leijqjsh='" + leijqjsh + '\'' +
                "ljsxqish='" + ljsxqish + '\'' +
                "mqhbbili='" + mqhbbili + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "meiqhbje='" + meiqhbje + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "xhdkqyzh='" + xhdkqyzh + '\'' +
                "hkqixian='" + hkqixian + '\'' +
                "hkshxubh='" + hkshxubh + '\'' +
                "leijinzh='" + leijinzh + '\'' +
                "baoliuje='" + baoliuje + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "xhdkzhxh='" + xhdkzhxh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "huanbzhq='" + huanbzhq + '\'' +
                "yuqhkzhq='" + yuqhkzhq + '\'' +
                "wtrkehuh='" + wtrkehuh + '\'' +
                "wtrckuzh='" + wtrckuzh + '\'' +
                "wtckzhao='" + wtckzhao + '\'' +
                "bjghrzzh='" + bjghrzzh + '\'' +
                "lxghrzzh='" + lxghrzzh + '\'' +
                "dailimsh='" + dailimsh + '\'' +
                "wtrckzxh='" + wtrckzxh + '\'' +
                "wtckzixh='" + wtckzixh + '\'' +
                "bjghrzxh='" + bjghrzxh + '\'' +
                "lxghrzxh='" + lxghrzxh + '\'' +
                "baozjzxh='" + baozjzxh + '\'' +
                "duowtrbz='" + duowtrbz + '\'' +
                "zbjrsygz='" + zbjrsygz + '\'' +
                "zhqxzekk='" + zhqxzekk + '\'' +
                "dbdkkksx='" + dbdkkksx + '\'' +
                "yunxdkzq='" + yunxdkzq + '\'' +
                "zhqzdcsh='" + zhqzdcsh + '\'' +
                "zqgzbhao='" + zqgzbhao + '\'' +
                "hesuanfs='" + hesuanfs + '\'' +
                "lixizcgz='" + lixizcgz + '\'' +
                "lixizhgz='" + lixizhgz + '\'' +
                "yjfyjhes='" + yjfyjhes + '\'' +
                "yiyldhes='" + yiyldhes + '\'' +
                "dkxtkmhs='" + dkxtkmhs + '\'' +
                "zidxtzhy='" + zidxtzhy + '\'' +
                "shfleixi='" + shfleixi + '\'' +
                "tqhkfjbh='" + tqhkfjbh + '\'' +
                "tqhkfjmc='" + tqhkfjmc + '\'' +
                "tqhkfjfj='" + tqhkfjfj + '\'' +
                "zhchtqtz='" + zhchtqtz + '\'' +
                "yqcshtzh='" + yqcshtzh + '\'' +
                "lilvbgtz='" + lilvbgtz + '\'' +
                "yuebgtzh='" + yuebgtzh + '\'' +
                "tzhtqtsh='" + tzhtqtsh + '\'' +
                "tzhjgtsh='" + tzhjgtsh + '\'' +
                "pingzhzl='" + pingzhzl + '\'' +
                "sydkcnuo='" + sydkcnuo + '\'' +
                "lmdkbzhi='" + lmdkbzhi + '\'' +
                "bzhrdbbz='" + bzhrdbbz + '\'' +
                "wujiflbz='" + wujiflbz + '\'' +
                "wujiflrq='" + wujiflrq + '\'' +
                "cndkjjho='" + cndkjjho + '\'' +
                "dkgljgou='" + dkgljgou + '\'' +
                "gljgleib='" + gljgleib + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "khjingli='" + khjingli + '\'' +
                "pingzhma='" + pingzhma + '\'' +
                "pngzxhao='" + pngzxhao + '\'' +
                "yinhshlv='" + yinhshlv + '\'' +
                "yinhshje='" + yinhshje + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "hetongll='" + hetongll + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "kuaijilb='" + kuaijilb + '\'' +
                "qixiriqi='" + qixiriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "dkqixian='" + dkqixian + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "fafangje='" + fafangje + '\'' +
                "djiekfje='" + djiekfje + '\'' +
                "kffangje='" + kffangje + '\'' +
                "dkuanjij='" + dkuanjij + '\'' +
                "daikduix='" + daikduix + '\'' +
                "yewufenl='" + yewufenl + '\'' +
                "ysywleix='" + ysywleix + '\'' +
                "ysywbhao='" + ysywbhao + '\'' +
                "zhqifkbz='" + zhqifkbz + '\'' +
                "mcfkjebl='" + mcfkjebl + '\'' +
                "scjixirq='" + scjixirq + '\'' +
                "dlxxzdgz='" + dlxxzdgz + '\'' +
                "dlxxqzgz='" + dlxxqzgz + '\'' +
                "wtckywbm='" + wtckywbm + '\'' +
                "dailixuh='" + dailixuh + '\'' +
                "zhngjigo='" + zhngjigo + '\'' +
                "edbizhgz='" + edbizhgz + '\'' +
                "edzdbizh='" + edzdbizh + '\'' +
                "yewusx01='" + yewusx01 + '\'' +
                "yewusx02='" + yewusx02 + '\'' +
                "yewusx03='" + yewusx03 + '\'' +
                "yewusx04='" + yewusx04 + '\'' +
                "yewusx05='" + yewusx05 + '\'' +
                "yewusx06='" + yewusx06 + '\'' +
                "yewusx07='" + yewusx07 + '\'' +
                "yewusx08='" + yewusx08 + '\'' +
                "yewusx09='" + yewusx09 + '\'' +
                "yewusx10='" + yewusx10 + '\'' +
                "ywsxms01='" + ywsxms01 + '\'' +
                "ywsxms02='" + ywsxms02 + '\'' +
                "ywsxms03='" + ywsxms03 + '\'' +
                "ywsxms04='" + ywsxms04 + '\'' +
                "ywsxms05='" + ywsxms05 + '\'' +
                "ywsxms06='" + ywsxms06 + '\'' +
                "ywsxms07='" + ywsxms07 + '\'' +
                "ywsxms08='" + ywsxms08 + '\'' +
                "ywsxms09='" + ywsxms09 + '\'' +
                "ywsxms10='" + ywsxms10 + '\'' +
                "lstdkfkjh='" + lstdkfkjh_ARRAY + '\'' +
                "lstdkfwdj='" + lstdkfwdj_ARRAY + '\'' +
                "lstdkhbjh='" + lstdkhbjh_ARRAY + '\'' +
                "benjinfd='" + benjinfd + '\'' +
                "lstdkbjfd='" + lstdkbjfd_ARRAY + '\'' +
                "lstdkhkfs='" + lstdkhkfs_ARRAY + '\'' +
                "lstdkhkzh='" + lstdkhkzh_ARRAY + '\'' +
                "lstdkllfd='" + lstdkllfd_ARRAY + '\'' +
                "lstdksfsj='" + lstdksfsj_ARRAY + '\'' +
                "lstdkstzf='" + lstdkstzf_ARRAY + '\'' +
                "lstdktxzh='" + lstdktxzh_ARRAY + '\'' +
                "lstdkwtxx='" + lstdkwtxx_ARRAY + '\'' +
                "lstdkzhbz='" + lstdkzhbz_ARRAY + '\'' +
                "lstdkzhlm='" + lstdkzhlm_ARRAY + '\'' +
                "chanpmsh='" + chanpmsh + '\'' +
                "daikdxxf='" + daikdxxf + '\'' +
                "xunhdaik='" + xunhdaik + '\'' +
                "yansdaik='" + yansdaik + '\'' +
                "chendaik='" + chendaik + '\'' +
                "butidaik='" + butidaik + '\'' +
                "yintdkbz='" + yintdkbz + '\'' +
                "yintdkfs='" + yintdkfs + '\'' +
                "yintleib='" + yintleib + '\'' +
                "yintnbcy='" + yintnbcy + '\'' +
                "yintwbcy='" + yintwbcy + '\'' +
                "fangkulx='" + fangkulx + '\'' +
                "fkzjclfs='" + fkzjclfs + '\'' +
                "hntmifku='" + hntmifku + '\'' +
                "hnftmfku='" + hnftmfku + '\'' +
                "neibufku='" + neibufku + '\'' +
                "jixibjgz='" + jixibjgz + '\'' +
                "lixijsff='" + lixijsff + '\'' +
                "jixitwgz='" + jixitwgz + '\'' +
                "jixizxje='" + jixizxje + '\'' +
                "jixisrgz='" + jixisrgz + '\'' +
                "srzxdanw='" + srzxdanw + '\'' +
                "lilvqixx='" + lilvqixx + '\'' +
                "fdjixibz='" + fdjixibz + '\'' +
                "yuqitzfs='" + yuqitzfs + '\'' +
                "yuqitzzq='" + yuqitzzq + '\'' +
                "yqfxfdfs='" + yqfxfdfs + '\'' +
                "yqfxfdzh='" + yqfxfdzh + '\'' +
                "fulilvbh='" + fulilvbh + '\'' +
                "fulilvny='" + fulilvny + '\'' +
                "fulililv='" + fulililv + '\'' +
                "fulitzfs='" + fulitzfs + '\'' +
                "fulitzzq='" + fulitzzq + '\'' +
                "jitiguiz='" + jitiguiz + '\'' +
                "butijejs='" + butijejs + '\'' +
                "tiaozhkf='" + tiaozhkf + '\'' +
                "scihkrbz='" + scihkrbz + '\'' +
                "mqihkfsh='" + mqihkfsh + '\'' +
                "yunxsuoq='" + yunxsuoq + '\'' +
                "bzuekkfs='" + bzuekkfs + '\'' +
                "yqbzkkfs='" + yqbzkkfs + '\'' +
                "hntmihku='" + hntmihku + '\'' +
                "hnftmhku='" + hnftmhku + '\'' +
                "nbuzhhku='" + nbuzhhku + '\'' +
                "tiqhksdq='" + tiqhksdq + '\'' +
                "sfyxkuxq='" + sfyxkuxq + '\'' +
                "kuanxqts='" + kuanxqts + '\'' +
                "kxqjixgz='" + kxqjixgz + '\'' +
                "kxqhjxgz='" + kxqhjxgz + '\'' +
                "kxqshxgz='" + kxqshxgz + '\'' +
                "zhqizcqx='" + zhqizcqx + '\'' +
                "fulifdfs='" + fulifdfs + '\'' +
                "fulifdzh='" + fulifdzh + '\'' +
                "bwchapbz='" + bwchapbz + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "flllcklx='" + flllcklx + '\'' +
                "kshchpdm='" + kshchpdm + '\'' +
                "kshchpmc='" + kshchpmc + '\'' +
                "dkczhzhh='" + dkczhzhh + '\'' +
                "dkdbfshi='" + dkdbfshi + '\'' +
                "dkyongtu='" + dkyongtu + '\'' +
                "fkjzhfsh='" + fkjzhfsh + '\'' +
                "shtzfhxm='" + shtzfhxm + '\'' +
                "bwhesdma='" + bwhesdma + '\'' +
                "wtrmingc='" + wtrmingc + '\'' +
                "ysxlyzhh='" + ysxlyzhh + '\'' +
                "ysxlyzxh='" + ysxlyzxh + '\'' +
                '}';
    }
}
