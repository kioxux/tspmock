package cn.com.yusys.yusp.web.client.esb.ecif.g00102;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.RelArrayInfo;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.G00102ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.Record;
import cn.com.yusys.yusp.online.client.esb.ecif.g00102.resp.G00102RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g00102)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G00102Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G00102Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 对私客户清单查询
     *
     * @param g00102ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g00102:对公客户清单查询")
    @PostMapping("/g00102")
    protected @ResponseBody
    ResultDto<G00102RespDto> g00102(@Validated @RequestBody G00102ReqDto g00102ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102ReqDto));
        cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g00102.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g00102.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.List list = new cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.List();
        G00102ReqService g00102ReqService = new G00102ReqService();
        G00102RespService g00102RespService = new G00102RespService();
        G00102RespDto g00102RespDto = new G00102RespDto();
        ResultDto<G00102RespDto> g00102ResultDto = new ResultDto<G00102RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将G00102ReqDto转换成reqService
            BeanUtils.copyProperties(g00102ReqDto, reqService);
            if (CollectionUtils.nonEmpty(g00102ReqDto.getRelArrayInfo())) {
                List<RelArrayInfo> relArrayInfos = Optional.ofNullable(g00102ReqDto.getRelArrayInfo()).orElseGet(() -> new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.Record> recordList = new ArrayList<Record>();
                // 遍历record传值塞入listArrayInfo
                for (RelArrayInfo relArrayInfo : relArrayInfos) {
                    cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.Record record = new cn.com.yusys.yusp.online.client.esb.ecif.g00102.req.Record();
                    BeanUtils.copyProperties(relArrayInfo, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_G00102.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间

            g00102ReqService.setService(reqService);
            // 将g00102ReqService转换成g00102ReqServiceMap
            Map g00102ReqServiceMap = beanMapUtil.beanToMap(g00102ReqService);

            Map service = (Map) g00102ReqServiceMap.get("service");
            // bigdecimal字段转成map之后出现科学计数法问题解决 modify by chenyong 陈勇
            service.forEach((k, v) -> {
                if (v instanceof Double) {
                    BigDecimal bigDecimal = new BigDecimal(((Double) v));
                    service.put(k, bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });

            Map mapList1 = (Map) service.get("list");
            if (CollectionUtils.nonEmpty(mapList1)) {
                List<Map<String, Object>> list1 = (List<Map<String, Object>>) mapList1.get("record");
                list1.forEach(l -> {
                    l.forEach((k, v) -> {
                        if (v instanceof Double) {
                            l.put(k, new BigDecimal(new BigDecimal(String.valueOf(v)).toPlainString()).setScale(2, BigDecimal.ROUND_HALF_UP));
                        }
                    });
                });
            }
            g00102ReqServiceMap.put("service", service);
            context.put("tradeDataMap", g00102ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G00102.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g00102RespService = beanMapUtil.mapToBean(tradeDataMap, G00102RespService.class, G00102RespService.class);
            respService = g00102RespService.getService();

            //  将G00102RespDto封装到ResultDto<G00102RespDto>
            g00102ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g00102ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));

            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G00102RespDto
                BeanUtils.copyProperties(respService, g00102RespDto);
                g00102ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g00102ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                g00102ResultDto.setCode(EpbEnum.EPB099999.key);
                g00102ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, e.getMessage());
            g00102ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g00102ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g00102ResultDto.setData(g00102RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00102.key, EsbEnum.TRADE_CODE_G00102.value, JSON.toJSONString(g00102ResultDto));
        return g00102ResultDto;
    }
}
