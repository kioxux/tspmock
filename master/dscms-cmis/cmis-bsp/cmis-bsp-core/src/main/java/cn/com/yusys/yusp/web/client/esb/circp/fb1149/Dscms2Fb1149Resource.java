package cn.com.yusys.yusp.web.client.esb.circp.fb1149;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1149.req.Fb1149ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1149.resp.Fb1149RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1149.req.Fb1149ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1149.resp.Fb1149RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1149）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1149Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1149Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1149
     * 交易描述：无还本续贷借据更新
     *
     * @param fb1149ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1149")
    protected @ResponseBody
    ResultDto<Fb1149RespDto> fb1149(@Validated @RequestBody Fb1149ReqDto fb1149ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1149.key, EsbEnum.TRADE_CODE_FB1149.value, JSON.toJSONString(fb1149ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1149.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1149.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1149.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1149.resp.Service();
        Fb1149ReqService fb1149ReqService = new Fb1149ReqService();
        Fb1149RespService fb1149RespService = new Fb1149RespService();
        Fb1149RespDto fb1149RespDto = new Fb1149RespDto();
        ResultDto<Fb1149RespDto> fb1149ResultDto = new ResultDto<Fb1149RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1149ReqDto转换成reqService
            BeanUtils.copyProperties(fb1149ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1149.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1149ReqService.setService(reqService);
            // 将fb1149ReqService转换成fb1149ReqServiceMap
            Map fb1149ReqServiceMap = beanMapUtil.beanToMap(fb1149ReqService);
            context.put("tradeDataMap", fb1149ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1149.key, EsbEnum.TRADE_CODE_FB1149.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1149.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1149.key, EsbEnum.TRADE_CODE_FB1149.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1149RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1149RespService.class, Fb1149RespService.class);
            respService = fb1149RespService.getService();

            fb1149ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1149ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1149RespDto
                BeanUtils.copyProperties(respService, fb1149RespDto);

                fb1149ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1149ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1149ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1149ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1149.key, EsbEnum.TRADE_CODE_FB1149.value, e.getMessage());
            fb1149ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1149ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1149ResultDto.setData(fb1149RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1149.key, EsbEnum.TRADE_CODE_FB1149.value, JSON.toJSONString(fb1149ResultDto));
        return fb1149ResultDto;
    }
}
