package cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 20:22
 * @since 2021/8/10 20:22
 */
public class Record {
    @JsonProperty(value = "GUAR_NO")
    private String GUAR_NO;//押品统一编号
    @JsonProperty(value = "HOUSE_UNIT_PRICE")
    private BigDecimal HOUSE_UNIT_PRICE;//房产单价
    @JsonProperty(value = "BICYCLE_GARAGE_EVAL")
    private BigDecimal BICYCLE_GARAGE_EVAL;//自行车库价值
    @JsonProperty(value = "PARK_SPACE_EVAL")
    private BigDecimal PARK_SPACE_EVAL;//车位价值
    @JsonProperty(value = "LOFT_EVAL")
    private BigDecimal LOFT_EVAL;//阁楼价值
    @JsonProperty(value = "EVAL_VALUE")
    private BigDecimal EVAL_VALUE;//房产价值
    @JsonProperty(value = "TOTAL_EVAL")
    private BigDecimal TOTAL_EVAL;//总评估价值

    @JsonIgnore
    public String getGUAR_NO() {
        return GUAR_NO;
    }

    @JsonIgnore
    public void setGUAR_NO(String GUAR_NO) {
        this.GUAR_NO = GUAR_NO;
    }

    @JsonIgnore
    public BigDecimal getHOUSE_UNIT_PRICE() {
        return HOUSE_UNIT_PRICE;
    }

    @JsonIgnore
    public void setHOUSE_UNIT_PRICE(BigDecimal HOUSE_UNIT_PRICE) {
        this.HOUSE_UNIT_PRICE = HOUSE_UNIT_PRICE;
    }

    @JsonIgnore
    public BigDecimal getBICYCLE_GARAGE_EVAL() {
        return BICYCLE_GARAGE_EVAL;
    }

    @JsonIgnore
    public void setBICYCLE_GARAGE_EVAL(BigDecimal BICYCLE_GARAGE_EVAL) {
        this.BICYCLE_GARAGE_EVAL = BICYCLE_GARAGE_EVAL;
    }

    @JsonIgnore
    public BigDecimal getPARK_SPACE_EVAL() {
        return PARK_SPACE_EVAL;
    }

    @JsonIgnore
    public void setPARK_SPACE_EVAL(BigDecimal PARK_SPACE_EVAL) {
        this.PARK_SPACE_EVAL = PARK_SPACE_EVAL;
    }

    @JsonIgnore
    public BigDecimal getLOFT_EVAL() {
        return LOFT_EVAL;
    }

    @JsonIgnore
    public void setLOFT_EVAL(BigDecimal LOFT_EVAL) {
        this.LOFT_EVAL = LOFT_EVAL;
    }

    @JsonIgnore
    public BigDecimal getEVAL_VALUE() {
        return EVAL_VALUE;
    }

    @JsonIgnore
    public void setEVAL_VALUE(BigDecimal EVAL_VALUE) {
        this.EVAL_VALUE = EVAL_VALUE;
    }

    @JsonIgnore
    public BigDecimal getTOTAL_EVAL() {
        return TOTAL_EVAL;
    }

    @JsonIgnore
    public void setTOTAL_EVAL(BigDecimal TOTAL_EVAL) {
        this.TOTAL_EVAL = TOTAL_EVAL;
    }

    @Override
    public String toString() {
        return "HOUSE_LIST{" +
                "GUAR_NO='" + GUAR_NO + '\'' +
                ", HOUSE_UNIT_PRICE=" + HOUSE_UNIT_PRICE +
                ", BICYCLE_GARAGE_EVAL=" + BICYCLE_GARAGE_EVAL +
                ", PARK_SPACE_EVAL=" + PARK_SPACE_EVAL +
                ", LOFT_EVAL=" + LOFT_EVAL +
                ", EVAL_VALUE=" + EVAL_VALUE +
                ", TOTAL_EVAL=" + TOTAL_EVAL +
                '}';
    }

}
