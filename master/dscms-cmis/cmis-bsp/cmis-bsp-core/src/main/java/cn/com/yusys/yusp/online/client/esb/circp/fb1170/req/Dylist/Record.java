package cn.com.yusys.yusp.online.client.esb.circp.fb1170.req.Dylist;

/**
 * @author chenyong
 * @version 0.1
 */
public class Record {

    private String guaranty_id;//抵押物编号
    private String guaranty_type;//抵押物类型
    private String guaranty_name;//抵押物名称
    private String guaranty_amt;//评估金额
    private String guaranty_state;//出入库状态

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getGuaranty_type() {
        return guaranty_type;
    }

    public void setGuaranty_type(String guaranty_type) {
        this.guaranty_type = guaranty_type;
    }

    public String getGuaranty_name() {
        return guaranty_name;
    }

    public void setGuaranty_name(String guaranty_name) {
        this.guaranty_name = guaranty_name;
    }

    public String getGuaranty_amt() {
        return guaranty_amt;
    }

    public void setGuaranty_amt(String guaranty_amt) {
        this.guaranty_amt = guaranty_amt;
    }

    public String getGuaranty_state() {
        return guaranty_state;
    }

    public void setGuaranty_state(String guaranty_state) {
        this.guaranty_state = guaranty_state;
    }

    @Override
    public String toString() {
        return "Record{" +
                "guaranty_id='" + guaranty_id + '\'' +
                ", guaranty_type='" + guaranty_type + '\'' +
                ", guaranty_name='" + guaranty_name + '\'' +
                ", guaranty_amt='" + guaranty_amt + '\'' +
                ", guaranty_state='" + guaranty_state + '\'' +
                '}';
    }
}
