package cn.com.yusys.yusp.web.server.biz.xddb0017;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0017.req.Xddb0017ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0017.resp.AudioInfoList;
import cn.com.yusys.yusp.dto.server.biz.xddb0017.resp.Xddb0017RespDto;
import cn.com.yusys.yusp.dto.server.xddb0017.req.Xddb0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0017.resp.List;
import cn.com.yusys.yusp.dto.server.xddb0017.resp.Xddb0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:获取抵押登记双录音视频信息列表
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0017:获取抵押登记双录音视频信息列表")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0017Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizdbClientService;
    /**
     * 交易码：xddb0017
     * 交易描述：获取抵押登记双录音视频信息列表
     *
     * @param xddb0017ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("获取抵押登记双录音视频信息列表")
    @PostMapping("/xddb0017")
    //@Idempotent({"xddb0017", "#xddb0017ReqDto.datasq"})
    protected @ResponseBody
    Xddb0017RespDto xddb0017(@Validated @RequestBody Xddb0017ReqDto xddb0017ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, JSON.toJSONString(xddb0017ReqDto));
        Xddb0017DataReqDto xddb0017DataReqDto = new Xddb0017DataReqDto();// 请求Data： 获取抵押登记双录音视频信息列表
        Xddb0017DataRespDto xddb0017DataRespDto = new Xddb0017DataRespDto();// 响应Data：获取抵押登记双录音视频信息列表
        Xddb0017RespDto xddb0017RespDto = new Xddb0017RespDto();
		cn.com.yusys.yusp.dto.server.biz.xddb0017.req.Data reqData = null; // 请求Data：获取抵押登记双录音视频信息列表
		cn.com.yusys.yusp.dto.server.biz.xddb0017.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0017.resp.Data();// 响应Data：获取抵押登记双录音视频信息列表
        try {
            // 从 xddb0017ReqDto获取 reqData
            reqData = xddb0017ReqDto.getData();
            // 将 reqData 拷贝到xddb0017DataReqDto
            BeanUtils.copyProperties(reqData, xddb0017DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, JSON.toJSONString(xddb0017DataReqDto));
            ResultDto<Xddb0017DataRespDto> xddb0017DataResultDto = dscmsBizdbClientService.xddb0017(xddb0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, JSON.toJSONString(xddb0017DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0017RespDto.setErorcd(Optional.ofNullable(xddb0017DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0017RespDto.setErortx(Optional.ofNullable(xddb0017DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0017DataResultDto.getCode())) {
                xddb0017DataRespDto = xddb0017DataResultDto.getData();
                xddb0017RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0017RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0017DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0017DataRespDto, respData);
                java.util.List<List> list = Optional.ofNullable(xddb0017DataRespDto.getList()).orElse(new ArrayList<>());
                java.util.List<AudioInfoList> collect = list.parallelStream().map(record -> {
                    AudioInfoList temp = new AudioInfoList();
                    BeanUtils.copyProperties(record, temp);
                    return temp;
                }).collect(Collectors.toList());
                respData.setAudioInfoLists(collect);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, e.getMessage());
            xddb0017RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0017RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0017RespDto.setDatasq(servsq);

        xddb0017RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0017.key, DscmsEnum.TRADE_CODE_XDDB0017.value, JSON.toJSONString(xddb0017RespDto));
        return xddb0017RespDto;
    }
}
