package cn.com.yusys.yusp.web.client.gxp.tonglian.d13087;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.req.D13087ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.resp.D13087RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.resp.D13087RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d13087)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D13087Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D13087Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 现金（大额）放款接口（处理码d13087）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d13087:现金（大额）放款接口")
    @PostMapping("/d13087")
    protected @ResponseBody
    ResultDto<D13087RespDto> d13087(@Validated @RequestBody D13087ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13087.key, GxpEnum.TRADE_CODE_D13087.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.D13087ReqMessage d13087ReqMessage = null;//请求Message：现金（大额）放款接口
        D13087RespMessage d13087RespMessage = new D13087RespMessage();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.Message reqMessage = null;//请求Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.resp.Message();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.Body();//请求Body：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.resp.Body();//响应Body：现金（大额）放款接口

        D13087RespDto d13087RespDto = new D13087RespDto();//响应Dto：现金（大额）放款接口
        ResultDto<D13087RespDto> d13087ResultDto = new ResultDto<>();//响应ResultDto：现金（大额）放款接口

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D13087.key, GxpEnum.TRADE_CODE_D13087.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D13087ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.Message::setBody, reqBody).build();
            // 给d13087ReqMessage赋值
            d13087ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.D13087ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.D13087ReqMessage::setMessage, reqMessage).build();

            // 将d13087ReqService转换成d13087ReqServiceMap
            Map d13087ReqMessageMap = beanMapUtil.beanToMap(d13087ReqMessage);
            context.put("tradeDataMap", d13087ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13087.key, GxpEnum.TRADE_CODE_D13087.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D13087.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13087.key, GxpEnum.TRADE_CODE_D13087.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d13087RespMessage = beanMapUtil.mapToBean(tradeDataMap, D13087RespMessage.class, D13087RespMessage.class);//响应Message：现金（大额）放款接口
            respMessage = d13087RespMessage.getMessage();//响应Message：现金（大额）放款接口
            gxpRespHead = respMessage.getHead();//响应Head：现金（大额）放款接口
            //  将D13087RespDto封装到ResultDto<D13087RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                respBody = respMessage.getBody();
                //  将respBody转换成D13087RespDto
                BeanUtils.copyProperties(respBody, d13087RespDto);
                d13087ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d13087ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d13087ResultDto.setCode(EpbEnum.EPB099999.key);
                d13087ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d13087ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13087.key, GxpEnum.TRADE_CODE_D13087.value, e.getMessage());
            d13087ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d13087ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d13087ResultDto.setData(d13087RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13087.key, GxpEnum.TRADE_CODE_D13087.value, JSON.toJSONString(d13087ResultDto));
        return d13087ResultDto;
    }
}
