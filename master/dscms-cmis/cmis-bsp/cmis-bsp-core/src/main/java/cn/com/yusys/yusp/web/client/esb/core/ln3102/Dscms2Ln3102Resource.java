package cn.com.yusys.yusp.web.client.esb.core.ln3102;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Lstdkhkzh;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Lstdkzqg;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3102.req.Ln3102ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3102.resp.Ln3102RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3102)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3102Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3102Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3102
     * 交易描述：贷款期供查询试算
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3102:贷款期供查询试算")
    @PostMapping("/ln3102")
    protected @ResponseBody
    ResultDto<Ln3102RespDto> ln3102(@Validated @RequestBody Ln3102ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3102.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3102.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3102.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3102.resp.Service();
        Ln3102ReqService ln3102ReqService = new Ln3102ReqService();
        Ln3102RespService ln3102RespService = new Ln3102RespService();
        Ln3102RespDto ln3102RespDto = new Ln3102RespDto();
        ResultDto<Ln3102RespDto> ln3102ResultDto = new ResultDto<Ln3102RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3102ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3102.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3102ReqService.setService(reqService);
            // 将ln3102ReqService转换成ln3102ReqServiceMap
            Map ln3102ReqServiceMap = beanMapUtil.beanToMap(ln3102ReqService);
            context.put("tradeDataMap", ln3102ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3102.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3102RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3102RespService.class, Ln3102RespService.class);
            respService = ln3102RespService.getService();

            ln3102ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3102ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3102RespDto
                BeanUtils.copyProperties(respService, ln3102RespDto);
                List<Lstdkhkzh> lstdkhkzhs = new ArrayList<Lstdkhkzh>();
                List<Lstdkzqg> lstdkzqgs = new ArrayList<Lstdkzqg>();
                // 贷款多还款账户
                if (respService.getLstdkhkzh_ARRAY() != null) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3102.resp.LstdkhkzhRecord> recordList = respService.getLstdkhkzh_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3102.resp.LstdkhkzhRecord record : recordList) {
                        Lstdkhkzh lstdkhkzh = new Lstdkhkzh();
                        BeanUtils.copyProperties(record, lstdkhkzh);
                        lstdkhkzhs.add(lstdkhkzh);
                    }
                    ln3102RespDto.setLstdkhkzh(lstdkhkzhs);
                }
                // 贷款账户期供
                if (respService.getLstdkzqg_ARRAY() != null) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3102.resp.LstdkzqgRecord> recordList = respService.getLstdkzqg_ARRAY().getRecord();
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3102.resp.LstdkzqgRecord record : recordList) {
                        Lstdkzqg lstdkzqg = new Lstdkzqg();
                        BeanUtils.copyProperties(record, lstdkzqg);
                        lstdkzqgs.add(lstdkzqg);
                    }
                    ln3102RespDto.setLstdkzqg(lstdkzqgs);
                }
                ln3102ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3102ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3102ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3102ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value, e.getMessage());
            ln3102ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3102ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3102ResultDto.setData(ln3102RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3102.key, EsbEnum.TRADE_CODE_LN3102.value, JSON.toJSONString(ln3102ResultDto));
        return ln3102ResultDto;
    }
}
