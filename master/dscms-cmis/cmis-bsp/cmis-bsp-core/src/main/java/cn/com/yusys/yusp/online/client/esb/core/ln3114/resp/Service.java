package cn.com.yusys.yusp.online.client.esb.core.ln3114.resp;

import java.math.BigDecimal;

/**
 * 响应Dto：贷款提前还款期供试算
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String kehmingc;//客户名称
    private BigDecimal zhanghye;//账户余额
    private String schkriqi;//上次还款日
    private String huankfsh;//还款方式
    private BigDecimal qiankzee;//欠款总额
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal guihzcbj;//归还正常本金
    private BigDecimal yingshfj;//应收罚金
    private BigDecimal huankjee;//还款金额
    private BigDecimal jiejuuje;//借据金额
    private Integer shyuqish;//剩余期数
    private BigDecimal zhchlilv;//正常利率
    private String nyuelilv;//年/月利率标识
    private String zjlyzhao;//资金来源账号
    private String zjlyzzxh;//资金来源账号子序号
    private String zjlyzhmc;//资金来源账号名称
    private String zjzrzhao;//资金转入账号
    private String zjzrzzxh;//资金转入账号子序号
    private String zjzrzhmc;//资金转入账号名称
    private Lstqgtqhk_ARRAY lstqgtqhk_ARRAY; // 期供提前还款

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public String getSchkriqi() {
        return schkriqi;
    }

    public void setSchkriqi(String schkriqi) {
        this.schkriqi = schkriqi;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getQiankzee() {
        return qiankzee;
    }

    public void setQiankzee(BigDecimal qiankzee) {
        this.qiankzee = qiankzee;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getGuihzcbj() {
        return guihzcbj;
    }

    public void setGuihzcbj(BigDecimal guihzcbj) {
        this.guihzcbj = guihzcbj;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public Integer getShyuqish() {
        return shyuqish;
    }

    public void setShyuqish(Integer shyuqish) {
        this.shyuqish = shyuqish;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZjlyzhmc() {
        return zjlyzhmc;
    }

    public void setZjlyzhmc(String zjlyzhmc) {
        this.zjlyzhmc = zjlyzhmc;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public String getZjzrzhmc() {
        return zjzrzhmc;
    }

    public void setZjzrzhmc(String zjzrzhmc) {
        this.zjzrzhmc = zjzrzhmc;
    }

    public Lstqgtqhk_ARRAY getLstqgtqhk_ARRAY() {
        return lstqgtqhk_ARRAY;
    }

    public void setLstqgtqhk_ARRAY(Lstqgtqhk_ARRAY lstqgtqhk_ARRAY) {
        this.lstqgtqhk_ARRAY = lstqgtqhk_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", zhanghye=" + zhanghye +
                ", schkriqi='" + schkriqi + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", qiankzee=" + qiankzee +
                ", ysyjlixi=" + ysyjlixi +
                ", guihzcbj=" + guihzcbj +
                ", yingshfj=" + yingshfj +
                ", huankjee=" + huankjee +
                ", jiejuuje=" + jiejuuje +
                ", shyuqish=" + shyuqish +
                ", zhchlilv=" + zhchlilv +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", zjlyzhao='" + zjlyzhao + '\'' +
                ", zjlyzzxh='" + zjlyzzxh + '\'' +
                ", zjlyzhmc='" + zjlyzhmc + '\'' +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                ", zjzrzhmc='" + zjzrzhmc + '\'' +
                ", lstqgtqhk_ARRAY=" + lstqgtqhk_ARRAY +
                '}';
    }
}
