package cn.com.yusys.yusp.web.server.biz.xdsx0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0019.req.Xdsx0019ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0019.resp.Xdsx0019RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0019.req.Xdsx0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0019.resp.Xdsx0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:风控发送信贷审核受托信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0019:风控发送信贷审核受托信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0019Resource.class);

    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0019
     * 交易描述：风控发送信贷审核受托信息
     *
     * @param xdsx0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控发送信贷审核受托信息")
    @PostMapping("/xdsx0019")
    //@Idempotent({"xdcasx0019", "#xdsx0019ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0019RespDto xdsx0019(@Validated @RequestBody Xdsx0019ReqDto xdsx0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019ReqDto));
        Xdsx0019DataReqDto xdsx0019DataReqDto = new Xdsx0019DataReqDto();// 请求Data： 风控发送信贷审核受托信息
        Xdsx0019DataRespDto xdsx0019DataRespDto = new Xdsx0019DataRespDto();// 响应Data：风控发送信贷审核受托信息
        Xdsx0019RespDto xdsx0019RespDto = new Xdsx0019RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdsx0019.req.Data reqData = null; // 请求Data：风控发送信贷审核受托信息
        cn.com.yusys.yusp.dto.server.biz.xdsx0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0019.resp.Data();// 响应Data：风控发送信贷审核受托信息

        try {
            // 从 xdsx0019ReqDto获取 reqData
            reqData = xdsx0019ReqDto.getData();
            // 将 reqData 拷贝到xdsx0019DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0019DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataReqDto));
            ResultDto<Xdsx0019DataRespDto> xdsx0019DataResultDto = dscmsBizSxClientService.xdsx0019(xdsx0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0019RespDto.setErorcd(Optional.ofNullable(xdsx0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0019RespDto.setErortx(Optional.ofNullable(xdsx0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0019DataResultDto.getCode())) {
                xdsx0019DataRespDto = xdsx0019DataResultDto.getData();
                xdsx0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, e.getMessage());
            xdsx0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0019RespDto.setDatasq(servsq);

        xdsx0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0019.key, DscmsEnum.TRADE_CODE_XDSX0019.value, JSON.toJSONString(xdsx0019RespDto));
        return xdsx0019RespDto;
    }
}
