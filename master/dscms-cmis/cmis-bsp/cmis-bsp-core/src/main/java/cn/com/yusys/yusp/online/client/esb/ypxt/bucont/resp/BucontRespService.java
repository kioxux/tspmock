package cn.com.yusys.yusp.online.client.esb.ypxt.bucont.resp;

/**
 * 响应Service：ESB信贷查询接口（处理码credi1）
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class BucontRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
