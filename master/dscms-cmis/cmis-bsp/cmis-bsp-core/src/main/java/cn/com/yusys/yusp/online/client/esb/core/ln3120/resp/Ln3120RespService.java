package cn.com.yusys.yusp.online.client.esb.core.ln3120.resp;

/**
 * 响应Service：查询抵质押物录入信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3120RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
