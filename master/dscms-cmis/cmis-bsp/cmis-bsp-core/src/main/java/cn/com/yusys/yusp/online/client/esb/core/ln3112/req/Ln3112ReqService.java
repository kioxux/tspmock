package cn.com.yusys.yusp.online.client.esb.core.ln3112.req;


/**
 * 请求Service：贷款展期查询
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3112ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

