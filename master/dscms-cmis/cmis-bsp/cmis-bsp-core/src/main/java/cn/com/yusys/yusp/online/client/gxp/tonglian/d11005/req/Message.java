package cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req;

import cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/3 13:59
 * @since 2021/6/3 13:59
 */
public class Message {
    private cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead head;
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Body body;

    public GxpReqHead getHead() {
        return head;
    }

    public void setHead(GxpReqHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
