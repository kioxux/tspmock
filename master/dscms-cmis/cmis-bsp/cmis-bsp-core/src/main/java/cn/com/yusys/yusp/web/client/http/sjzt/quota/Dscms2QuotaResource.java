package cn.com.yusys.yusp.web.client.http.sjzt.quota;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.req.QuotaReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp.QuotaRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.sjzt.quota.QuotaReqService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(quota)")
@RestController
@RequestMapping("/api/dscms2sjzt")
public class Dscms2QuotaResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2QuotaResource.class);
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 信贷客户核心业绩统计查询列表
     *
     * @param quotaReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("quota:客户我行授用信分布")
    @PostMapping("/quota")
    protected @ResponseBody
    ResultDto<QuotaRespDto> quota(@Validated @RequestBody QuotaReqDto quotaReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QUOTA.key, EsbEnum.TRADE_CODE_QUOTA.value, JSON.toJSONString(quotaReqDto));

        QuotaReqService quotaReqService = new QuotaReqService();

        QuotaRespDto quotaRespDto = new QuotaRespDto();
        ResultDto<QuotaRespDto> quotaResultDto = new ResultDto<QuotaRespDto>();
        try {
            // 将QuotaReqDto转换成QuotaReqService
            BeanUtils.copyProperties(quotaReqDto, quotaReqService);

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            quotaReqService.setServsq(servsq);//    渠道流水

//            quotaReqService.setUserid(EsbEnum.BRCHNO_XWYWGLPT.key);//    柜员号
//            quotaReqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(quotaReqService), headers);
            String url = "http://sc-gateway.zrcbank.ingress/credit/cus/quota";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QUOTA.key, EsbEnum.TRADE_CODE_QUOTA.value, JSON.toJSONString(quotaReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QUOTA.key, EsbEnum.TRADE_CODE_QUOTA.value, responseEntity);

            if (responseEntity.getStatusCodeValue() == 200) {
                String responseStr = responseEntity.getBody();
                JSONObject jsonObject = JSONObject.parseObject(responseStr);
                String status = jsonObject.get("status").toString();//数据中台成功标识 100成功
                String message = jsonObject.get("message").toString();
                if (status.equals("100")) {
                    //String data = jsonObject.get("data").toString();
                    quotaRespDto = JSON.parseObject(responseStr, QuotaRespDto.class);
                    quotaResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                    quotaResultDto.setMessage(message);
                } else {
                    quotaResultDto.setCode(EpbEnum.EPB099999.key);
                    quotaResultDto.setMessage(message);
                }
            } else {
                quotaResultDto.setCode(EpbEnum.EPB099999.key);//9999
                quotaResultDto.setMessage("交易失败");//系统异常
            }


        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QUOTA.key, EsbEnum.TRADE_CODE_QUOTA.value, e.getMessage());
            quotaResultDto.setCode(EpbEnum.EPB099999.key);//9999
            quotaResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        quotaResultDto.setData(quotaRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_QUOTA.key, EsbEnum.TRADE_CODE_QUOTA.value, JSON.toJSONString(quotaResultDto));
        return quotaResultDto;
    }
}
