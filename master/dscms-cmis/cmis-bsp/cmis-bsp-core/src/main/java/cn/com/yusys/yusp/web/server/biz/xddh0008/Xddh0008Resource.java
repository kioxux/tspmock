package cn.com.yusys.yusp.web.server.biz.xddh0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0008.req.Xddh0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0008.resp.Xddh0008RespDto;
import cn.com.yusys.yusp.dto.server.xddh0008.req.Xddh0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0008.resp.Xddh0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:还款计划列表查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDDH0008:还款计划列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0008Resource.class);

	@Autowired
	private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0008
     * 交易描述：还款计划列表查询
     *
     * @param xddh0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("还款计划列表查询")
    @PostMapping("/xddh0008")
    //@Idempotent({"xddh0008", "#xddh0008ReqDto.datasq"})
    protected @ResponseBody
    Xddh0008RespDto xddh0008(@Validated @RequestBody Xddh0008ReqDto xddh0008ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008ReqDto));
        Xddh0008DataReqDto xddh0008DataReqDto = new Xddh0008DataReqDto();// 请求Data： 还款计划列表查询
        Xddh0008DataRespDto xddh0008DataRespDto = new Xddh0008DataRespDto();// 响应Data：还款计划列表查询
        cn.com.yusys.yusp.dto.server.biz.xddh0008.req.Data reqData = null; // 请求Data：还款计划列表查询
		cn.com.yusys.yusp.dto.server.biz.xddh0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0008.resp.Data();// 响应Data：还款计划列表查询
		Xddh0008RespDto xddh0008RespDto = new Xddh0008RespDto();
		try {
            // 从 xddh0008ReqDto获取 reqData
            reqData = xddh0008ReqDto.getData();
            // 将 reqData 拷贝到xddh0008DataReqDto
            BeanUtils.copyProperties(reqData, xddh0008DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataReqDto));
            ResultDto<Xddh0008DataRespDto> xddh0008DataResultDto = dscmsBizDhClientService.xddh0008(xddh0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddh0008RespDto.setErorcd(Optional.ofNullable(xddh0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0008RespDto.setErortx(Optional.ofNullable(xddh0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0008DataResultDto.getCode())) {
                xddh0008DataRespDto = xddh0008DataResultDto.getData();
                xddh0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, e.getMessage());
            xddh0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0008RespDto.setDatasq(servsq);

        xddh0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0008.key, DscmsEnum.TRADE_CODE_XDDH0008.value, JSON.toJSONString(xddh0008RespDto));
        return xddh0008RespDto;
    }
}
