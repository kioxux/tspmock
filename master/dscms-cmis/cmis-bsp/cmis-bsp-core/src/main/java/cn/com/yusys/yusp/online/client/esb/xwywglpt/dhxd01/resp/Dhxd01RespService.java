package cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.resp;

/**
 * 响应Service：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 * @author lihh
 * @version 1.0             
 */      
public class Dhxd01RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
