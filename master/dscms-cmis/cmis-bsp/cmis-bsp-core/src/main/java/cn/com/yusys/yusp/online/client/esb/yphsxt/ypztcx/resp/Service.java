package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp;

import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerfacilitylist.Registerfacilitylist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerhouselist.Registerhouselist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerlandlist.Registerlandlist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registermortgagelist.Registermortgagelist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerownerlist.Registerownerlist;
import cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerseizurelist.Registerseizurelist;

/**
 * 响应Service：信贷押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {

    private String erorcd;//操作成功标志位
    private String erortx;//描述信息

    private String reocnu;//登记簿概要信息-不动产权证号
    private String reoreu;//登记簿概要信息-不动产单元号
    private String reoona;//登记簿概要信息-权利人
    private String reoodn;//登记簿概要信息-证件号
    private String reobda;//登记簿概要信息-登簿日期
    private String reooss;//登记簿概要信息-权属状态
    private String reorem;//登记簿概要信息-附记
    private Registerfacilitylist registerfacilitylist;
    private Registerhouselist registerhouselist;
    private Registerlandlist registerlandlist;
    private Registermortgagelist registermortgagelist;
    private Registerownerlist registerownerlist;
    private Registerseizurelist registerseizurelist;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReocnu() {
        return reocnu;
    }

    public void setReocnu(String reocnu) {
        this.reocnu = reocnu;
    }

    public String getReoreu() {
        return reoreu;
    }

    public void setReoreu(String reoreu) {
        this.reoreu = reoreu;
    }

    public String getReoona() {
        return reoona;
    }

    public void setReoona(String reoona) {
        this.reoona = reoona;
    }

    public String getReoodn() {
        return reoodn;
    }

    public void setReoodn(String reoodn) {
        this.reoodn = reoodn;
    }

    public String getReobda() {
        return reobda;
    }

    public void setReobda(String reobda) {
        this.reobda = reobda;
    }

    public String getReooss() {
        return reooss;
    }

    public void setReooss(String reooss) {
        this.reooss = reooss;
    }

    public String getReorem() {
        return reorem;
    }

    public void setReorem(String reorem) {
        this.reorem = reorem;
    }

    public Registerfacilitylist getRegisterfacilitylist() {
        return registerfacilitylist;
    }

    public void setRegisterfacilitylist(Registerfacilitylist registerfacilitylist) {
        this.registerfacilitylist = registerfacilitylist;
    }

    public Registerhouselist getRegisterhouselist() {
        return registerhouselist;
    }

    public void setRegisterhouselist(Registerhouselist registerhouselist) {
        this.registerhouselist = registerhouselist;
    }

    public Registerlandlist getRegisterlandlist() {
        return registerlandlist;
    }

    public void setRegisterlandlist(Registerlandlist registerlandlist) {
        this.registerlandlist = registerlandlist;
    }

    public Registermortgagelist getRegistermortgagelist() {
        return registermortgagelist;
    }

    public void setRegistermortgagelist(Registermortgagelist registermortgagelist) {
        this.registermortgagelist = registermortgagelist;
    }

    public Registerownerlist getRegisterownerlist() {
        return registerownerlist;
    }

    public void setRegisterownerlist(Registerownerlist registerownerlist) {
        this.registerownerlist = registerownerlist;
    }

    public Registerseizurelist getRegisterseizurelist() {
        return registerseizurelist;
    }

    public void setRegisterseizurelist(Registerseizurelist registerseizurelist) {
        this.registerseizurelist = registerseizurelist;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", reocnu='" + reocnu + '\'' +
                ", reoreu='" + reoreu + '\'' +
                ", reoona='" + reoona + '\'' +
                ", reoodn='" + reoodn + '\'' +
                ", reobda='" + reobda + '\'' +
                ", reooss='" + reooss + '\'' +
                ", reorem='" + reorem + '\'' +
                ", registerfacilitylist=" + registerfacilitylist +
                ", registerhouselist=" + registerhouselist +
                ", registerlandlist=" + registerlandlist +
                ", registermortgagelist=" + registermortgagelist +
                ", registerownerlist=" + registerownerlist +
                ", registerseizurelist=" + registerseizurelist +
                '}';
    }
}
