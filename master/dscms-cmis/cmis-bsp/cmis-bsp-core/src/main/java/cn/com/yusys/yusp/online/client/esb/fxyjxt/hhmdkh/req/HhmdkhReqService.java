package cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.req;

/**
 * 请求Service：查询客户是否为黑灰名单客户
 *
 * @author code-generator
 * @version 1.0
 */
public class HhmdkhReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
