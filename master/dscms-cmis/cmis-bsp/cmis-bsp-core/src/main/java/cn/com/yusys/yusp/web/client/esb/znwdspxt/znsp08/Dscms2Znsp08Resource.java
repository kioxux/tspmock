package cn.com.yusys.yusp.web.client.esb.znwdspxt.znsp08;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.req.Znsp08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.resp.Znsp08RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp08.req.Znsp08ReqService;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp08.resp.Znsp08RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用智能微贷审批系统的接口处理类
 **/
@Api(tags = "BSP封装调用智能微贷审批系统的接口处理类()")
@RestController
@RequestMapping("/api/dscms2znwdspxt")
public class Dscms2Znsp08Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Znsp08Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：znsp08
     * 交易描述：客户调查撤销接口
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("znsp08:客户调查撤销接口")
    @PostMapping("/znsp08")
    protected @ResponseBody
    ResultDto<Znsp08RespDto> znsp08(@Validated @RequestBody Znsp08ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp08.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp08.req.Service();
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp08.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp08.resp.Service();
        Znsp08ReqService znsp08ReqService = new Znsp08ReqService();
        Znsp08RespService znsp08RespService = new Znsp08RespService();
        Znsp08RespDto znsp08RespDto = new Znsp08RespDto();
        ResultDto<Znsp08RespDto> znsp08ResultDto = new ResultDto<Znsp08RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将znsp08ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_ZNSP08.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_ZNWDSPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ZNWDSPXT.key);//    部门号
            znsp08ReqService.setService(reqService);
            // 将znsp08ReqService转换成znsp08ReqServiceMap
            Map znsp08ReqServiceMap = beanMapUtil.beanToMap(znsp08ReqService);
            context.put("tradeDataMap", znsp08ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_ZNSP08.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            znsp08RespService = beanMapUtil.mapToBean(tradeDataMap, Znsp08RespService.class, Znsp08RespService.class);
            respService = znsp08RespService.getService();
            //  将respService转换成znsp08RespDto
            BeanUtils.copyProperties(respService, znsp08RespDto);
            znsp08ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            znsp08ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成znsp08RespDto
                BeanUtils.copyProperties(respService, znsp08RespDto);
                znsp08ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                znsp08ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                znsp08ResultDto.setCode(EpbEnum.EPB099999.key);
                znsp08ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value, e.getMessage());
            znsp08ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            znsp08ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        znsp08ResultDto.setData(znsp08RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP08.key, EsbEnum.TRADE_CODE_ZNSP08.value, JSON.toJSONString(znsp08ResultDto));
        return znsp08ResultDto;
    }
}
