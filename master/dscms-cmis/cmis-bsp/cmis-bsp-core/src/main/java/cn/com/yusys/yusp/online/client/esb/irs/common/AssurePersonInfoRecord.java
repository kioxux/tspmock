package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.math.BigDecimal;

/**
 * 保证人信息(AssurePersonInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:12
 */
public class AssurePersonInfoRecord {

    private String guaranty_id; // 保证群编号
    private String cus_id; // 保证人客户号
    private String cus_name; // 保证人客户名称
    private String currency; // 币种
    private BigDecimal guarantee_amt; // 担保金额
    private String bus_owner; // 企业所有制
    private String guaranty_type; // 保证类型
    private String law_validity; // 保证法律有效性
    private String isin_major; // 保证人是否专业担保公司

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getGuarantee_amt() {
        return guarantee_amt;
    }

    public void setGuarantee_amt(BigDecimal guarantee_amt) {
        this.guarantee_amt = guarantee_amt;
    }

    public String getBus_owner() {
        return bus_owner;
    }

    public void setBus_owner(String bus_owner) {
        this.bus_owner = bus_owner;
    }

    public String getGuaranty_type() {
        return guaranty_type;
    }

    public void setGuaranty_type(String guaranty_type) {
        this.guaranty_type = guaranty_type;
    }

    public String getLaw_validity() {
        return law_validity;
    }

    public void setLaw_validity(String law_validity) {
        this.law_validity = law_validity;
    }

    public String getIsin_major() {
        return isin_major;
    }

    public void setIsin_major(String isin_major) {
        this.isin_major = isin_major;
    }

    @Override
    public String toString() {
        return "AssurePersonInfo{" +
                "guaranty_id='" + guaranty_id + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", currency='" + currency + '\'' +
                ", guarantee_amt=" + guarantee_amt +
                ", bus_owner='" + bus_owner + '\'' +
                ", guaranty_type='" + guaranty_type + '\'' +
                ", law_validity='" + law_validity + '\'' +
                ", isin_major='" + isin_major + '\'' +
                '}';
    }
}
