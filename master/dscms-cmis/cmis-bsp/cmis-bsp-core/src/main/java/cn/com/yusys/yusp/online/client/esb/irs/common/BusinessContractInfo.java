package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:合同信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日15:10:55
 */
public class BusinessContractInfo {
    private List<BusinessContractInfoRecord> record; // 合同信息

    public List<BusinessContractInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<BusinessContractInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "BusinessContractInfo{" +
                "record=" + record +
                '}';
    }
}