package cn.com.yusys.yusp.web.client.esb.core.ln3062;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3062.req.Ln3062ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3062.resp.Ln3062RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3062.req.Ln3062ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3062.req.Record;
import cn.com.yusys.yusp.online.client.esb.core.ln3062.resp.Ln3062RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3062.resp.LstKlnb_dkzrjj;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3062)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3062Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3062Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3062
     * 交易描述：资产转让借据维护
     *
     * @param ln3062ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3062:主要实现对贷款期限的缩短和延长修改，实时生效")
    @PostMapping("/ln3062")
    protected @ResponseBody
    ResultDto<Ln3062RespDto> ln3062(@Validated @RequestBody Ln3062ReqDto ln3062ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3062.key, EsbEnum.TRADE_CODE_LN3062.value, JSON.toJSONString(ln3062ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3062.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3062.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3062.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3062.resp.Service();
        Ln3062ReqService ln3062ReqService = new Ln3062ReqService();
        Ln3062RespService ln3062RespService = new Ln3062RespService();
        Ln3062RespDto ln3062RespDto = new Ln3062RespDto();
        ResultDto<Ln3062RespDto> ln3062ResultDto = new ResultDto<Ln3062RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3062ReqDto转换成reqService
            BeanUtils.copyProperties(ln3062ReqDto, reqService);
            if (CollectionUtils.nonEmpty(ln3062ReqDto.getLstKlnb_dkzrjj())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3062.req.LstKlnb_dkzrjj> reqList = Optional.ofNullable(ln3062ReqDto.getLstKlnb_dkzrjj()).orElse(new ArrayList<>());
                List<cn.com.yusys.yusp.online.client.esb.core.ln3062.req.Record> reqRecordList = reqList.parallelStream().map(e -> {
                    cn.com.yusys.yusp.online.client.esb.core.ln3062.req.Record reqRecord = new Record();
                    BeanUtils.copyProperties(e, reqRecord);
                    return reqRecord;
                }).collect(Collectors.toList());
                cn.com.yusys.yusp.online.client.esb.core.ln3062.req.LstKlnb_dkzrjj reqLstKlnb_dkzrjj = new cn.com.yusys.yusp.online.client.esb.core.ln3062.req.LstKlnb_dkzrjj();
                reqLstKlnb_dkzrjj.setRecord(reqRecordList);
                reqService.setLstKlnb_dkzrjj(reqLstKlnb_dkzrjj);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3062.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3062ReqService.setService(reqService);
            // 将ln3062ReqService转换成ln3062ReqServiceMap
            Map ln3062ReqServiceMap = beanMapUtil.beanToMap(ln3062ReqService);
            context.put("tradeDataMap", ln3062ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3062.key, EsbEnum.TRADE_CODE_LN3062.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3062.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3062.key, EsbEnum.TRADE_CODE_LN3062.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3062RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3062RespService.class, Ln3062RespService.class);
            respService = ln3062RespService.getService();
            //  将respService转换成Ln3062RespDto
            BeanUtils.copyProperties(respService, ln3062RespDto);
            ln3062ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3062ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, ln3062RespDto);
                LstKlnb_dkzrjj resplstKlnb_dkzrjj = Optional.ofNullable(respService.getLstKlnb_dkzrjj()).orElse(new LstKlnb_dkzrjj());
                if (CollectionUtils.nonEmpty(resplstKlnb_dkzrjj.getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3062.resp.Record> recordList = resplstKlnb_dkzrjj.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3062.resp.LstKlnb_dkzrjj> respLstKlnb_dkzrjj = recordList.parallelStream().map(e -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3062.resp.LstKlnb_dkzrjj target = new cn.com.yusys.yusp.dto.client.esb.core.ln3062.resp.LstKlnb_dkzrjj();
                        BeanUtils.copyProperties(e, target);
                        return target;
                    }).collect(Collectors.toList());
                    ln3062RespDto.setLstKlnb_dkzrjjs(respLstKlnb_dkzrjj);
                }
                ln3062ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3062ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3062ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3062ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3062.key, EsbEnum.TRADE_CODE_LN3062.value, e.getMessage());
            ln3062ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3062ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3062ResultDto.setData(ln3062RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3062.key, EsbEnum.TRADE_CODE_LN3062.value, JSON.toJSONString(ln3062ResultDto));
        return ln3062ResultDto;
    }
}
