package cn.com.yusys.yusp.online.client.esb.ypxt.coninf.req;

/**
 * 请求Service：信贷担保合同信息同步（处理码coninf）
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
