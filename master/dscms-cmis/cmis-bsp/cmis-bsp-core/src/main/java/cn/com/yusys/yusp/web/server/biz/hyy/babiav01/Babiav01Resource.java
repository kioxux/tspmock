package cn.com.yusys.yusp.web.server.biz.hyy.babiav01;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.hyy.babiav01.Babiav01RespDto;
import cn.com.yusys.yusp.dto.server.biz.hyy.babiav01.Items;
import cn.com.yusys.yusp.dto.server.xddb0017.req.Xddb0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0017.resp.Xddb0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:获取抵押登记双录音视频信息列表
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "BA-BI-AV01:获取抵押登记双录音视频信息列表")
@RestController
@RequestMapping("/bank/collaterals")
public class Babiav01Resource {
    private static final Logger logger = LoggerFactory.getLogger(Babiav01Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：BA-BI-AV01
     * 交易描述：获取抵押登记双录音视频信息列表
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation("获取抵押登记双录音视频信息列表")
    @GetMapping("/{id}/av")
    protected @ResponseBody
    String babiav01(@PathVariable("id") String id) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIAV.key, DscmsEnum.TRADE_CODE_BABIAV.value, JSON.toJSONString(id));
        Xddb0017DataReqDto xddb0017DataReqDto = new Xddb0017DataReqDto();// 请求Data： 获取抵押登记双录音视频信息列表
        Xddb0017DataRespDto xddb0017DataRespDto = new Xddb0017DataRespDto();// 响应Data：获取抵押登记双录音视频信息列表
        Babiav01RespDto babiav01RespDto = new Babiav01RespDto();
        cn.com.yusys.yusp.dto.server.biz.hyy.babiav01.Data respData = new cn.com.yusys.yusp.dto.server.biz.hyy.babiav01.Data();
        try {
            xddb0017DataReqDto.setGuarno(id);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIAV.key, DscmsEnum.TRADE_CODE_BABIAV.value, JSON.toJSONString(xddb0017DataReqDto));
            ResultDto<Xddb0017DataRespDto> xddb0017DataResultDto = dscmsBizDbClientService.xddb0017(xddb0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIAV.key, DscmsEnum.TRADE_CODE_BABIAV.value, JSON.toJSONString(xddb0017DataRespDto));

            babiav01RespDto.setCode(Optional.ofNullable(xddb0017DataResultDto.getCode()).orElse(SuccessEnum.HYY_SUCCESS.key));
            babiav01RespDto.setMessage(Optional.ofNullable(xddb0017DataResultDto.getMessage()).orElse(SuccessEnum.HYY_SUCCESS.value));
            babiav01RespDto.setSuccess(false);
            // 从返回值中获取响应码和响应消息
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0017DataResultDto.getCode())) {
                xddb0017DataRespDto = xddb0017DataResultDto.getData();
                babiav01RespDto.setSuccess(true);
                babiav01RespDto.setCode(SuccessEnum.HYY_SUCCESS.key);
                babiav01RespDto.setMessage(SuccessEnum.HYY_SUCCESS.value);
                java.util.List<cn.com.yusys.yusp.dto.server.xddb0017.resp.List> lists = xddb0017DataRespDto.getList();
                java.util.List<Items> list = lists.stream().map(list2->{
                    Items list1 = new Items();
                    list1.setFile_id(Optional.ofNullable(list2.getFile_id()).orElse(Strings.EMPTY));//文件ID
                    list1.setMortgagor(Optional.ofNullable(list2.getMortgagor()).orElse(Strings.EMPTY));//抵押人名称
                    list1.setMortgage_no(Optional.ofNullable(list2.getMortgage_no()).orElse(Strings.EMPTY));//抵押物编号
                    list1.setContract_no(Optional.ofNullable(list2.getContract_no()).orElse(Strings.EMPTY));//抵押合同编号
                    list1.setMortgage_location(Optional.ofNullable(list2.getMortgage_location()).orElse(Strings.EMPTY));//抵押物坐落
                    list1.setCreate_time(list2.getCreate_time());//录制时间
                    return list1;
                }).collect(Collectors.toList());
                respData.setItems(list);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIAV.key, DscmsEnum.TRADE_CODE_BABIAV.value, e.getMessage());
            babiav01RespDto.setCode(EpbEnum.EPBHYY9999.key); // 9999
            babiav01RespDto.setCode(EpbEnum.EPBHYY9999.value);// 系统异常
            babiav01RespDto.setSuccess(false);
        }
        babiav01RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_BABIAV.key, DscmsEnum.TRADE_CODE_BABIAV.value, JSON.toJSONString(babiav01RespDto));
        return JSON.toJSONString(babiav01RespDto);
    }
}


