package cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.req;

/**
 * 请求Service：查询还款（合约）明细历史表（利翃）中的全量借据一览
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd14ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd14ReqService{" +
                "service=" + service +
                '}';
    }
}
