package cn.com.yusys.yusp.online.client.http.sjzt.zcOverallBusinessIncome.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Service：资产池业务总体收益情况详情
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ZcOverallBusinessIncomeReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//处理码
    @JsonProperty(value = "servtp")
    private String servtp;//渠道
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "userid")
    private String userid;//柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//部门号

    @JsonProperty(value = "agrNo")
    private String agrNo;//协议编号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户姓名

    @JsonProperty(value = "size")
    private Integer size;
    @JsonProperty(value = "from")
    private Integer from;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getAgrNo() {
        return agrNo;
    }

    public void setAgrNo(String agrNo) {
        this.agrNo = agrNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getFrom() {
        return from;
    }

    public void setFrom(Integer from) {
        this.from = from;
    }

    @Override
    public String toString() {
        return "ZcOverallBusinessIncomeReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", agrNo='" + agrNo + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", size=" + size +
                ", from=" + from +
                '}';
    }
}


