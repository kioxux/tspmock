package cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.req;


/**
 * 请求Service：权证信息同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:40
 */
public class Service {

    private String prcscd; //处理码
    private String servtp; //渠道
    private String servsq; //渠道流水
    private String userid; //柜员号
    private String brchno; //部门号
    private String datasq; //全局流水

    private String dbhtbh; //担保合同编号
    private String sernoy; //核心担保编号
    private String dyswbs; //抵押顺位标识
    private String qzlxyp; //权证类型
    private String qlpzhm; //权利凭证号
    private String qzfzjg; //权证发证机关名称
    private String qzffrq; //权证发证日期
    private String qzdqrq; //权证到期日期
    private String qljeyp; //权利金额
    private String qzztyp; //权证状态
    private String qzbzxx; //权证备注信息
    private String djrmyp; //登记人
    private String djjgyp; //登记机构
    private String djrqyp; //登记日期
    private String operat; //操作
    private List list;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getDbhtbh() {
        return dbhtbh;
    }

    public void setDbhtbh(String dbhtbh) {
        this.dbhtbh = dbhtbh;
    }

    public String getSernoy() {
        return sernoy;
    }

    public void setSernoy(String sernoy) {
        this.sernoy = sernoy;
    }

    public String getDyswbs() {
        return dyswbs;
    }

    public void setDyswbs(String dyswbs) {
        this.dyswbs = dyswbs;
    }

    public String getQzlxyp() {
        return qzlxyp;
    }

    public void setQzlxyp(String qzlxyp) {
        this.qzlxyp = qzlxyp;
    }

    public String getQlpzhm() {
        return qlpzhm;
    }

    public void setQlpzhm(String qlpzhm) {
        this.qlpzhm = qlpzhm;
    }

    public String getQzfzjg() {
        return qzfzjg;
    }

    public void setQzfzjg(String qzfzjg) {
        this.qzfzjg = qzfzjg;
    }

    public String getQzffrq() {
        return qzffrq;
    }

    public void setQzffrq(String qzffrq) {
        this.qzffrq = qzffrq;
    }

    public String getQzdqrq() {
        return qzdqrq;
    }

    public void setQzdqrq(String qzdqrq) {
        this.qzdqrq = qzdqrq;
    }

    public String getQljeyp() {
        return qljeyp;
    }

    public void setQljeyp(String qljeyp) {
        this.qljeyp = qljeyp;
    }

    public String getQzztyp() {
        return qzztyp;
    }

    public void setQzztyp(String qzztyp) {
        this.qzztyp = qzztyp;
    }

    public String getQzbzxx() {
        return qzbzxx;
    }

    public void setQzbzxx(String qzbzxx) {
        this.qzbzxx = qzbzxx;
    }

    public String getDjrmyp() {
        return djrmyp;
    }

    public void setDjrmyp(String djrmyp) {
        this.djrmyp = djrmyp;
    }

    public String getDjjgyp() {
        return djjgyp;
    }

    public void setDjjgyp(String djjgyp) {
        this.djjgyp = djjgyp;
    }

    public String getDjrqyp() {
        return djrqyp;
    }

    public void setDjrqyp(String djrqyp) {
        this.djrqyp = djrqyp;
    }

    public String getOperat() {
        return operat;
    }

    public void setOperat(String operat) {
        this.operat = operat;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dbhtbh='" + dbhtbh + '\'' +
                ", sernoy='" + sernoy + '\'' +
                ", dyswbs='" + dyswbs + '\'' +
                ", qzlxyp='" + qzlxyp + '\'' +
                ", qlpzhm='" + qlpzhm + '\'' +
                ", qzfzjg='" + qzfzjg + '\'' +
                ", qzffrq='" + qzffrq + '\'' +
                ", qzdqrq='" + qzdqrq + '\'' +
                ", qljeyp='" + qljeyp + '\'' +
                ", qzztyp='" + qzztyp + '\'' +
                ", qzbzxx='" + qzbzxx + '\'' +
                ", djrmyp='" + djrmyp + '\'' +
                ", djjgyp='" + djjgyp + '\'' +
                ", djrqyp='" + djrqyp + '\'' +
                ", operat='" + operat + '\'' +
                ", list=" + list +
                '}';
    }
}
