package cn.com.yusys.yusp.web.server.biz.hyy.babiec01;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.hyy.babiec01.Babiec01RespDto;
import cn.com.yusys.yusp.dto.server.hyy.common.UpdateCollateralElectronicCertificateForm;
import cn.com.yusys.yusp.dto.server.xddb0013.req.Xddb0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0013.resp.Xddb0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import cn.com.yusys.yusp.web.server.biz.hyy.babicr01.Babicr01Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "Babiec:抵押登记不动产登记证明入库")
@RestController
@RequestMapping("/bank/collaterals")
public class Babiec01Resource {

    private static final Logger logger = LoggerFactory.getLogger(Babiec01Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0013
     * 交易描述：抵押登记不动产登记证明入库
     *
     * @param id
     * @return
     * @throws Exception
     */
    @ApiOperation("抵押登记不动产登记证明入库")
    @RequestMapping("/{id}/electronic-certificate")
    protected @ResponseBody
    String babiec(@PathVariable("id") String id, UpdateCollateralElectronicCertificateForm form) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(id));
        Xddb0013DataReqDto xddb0013DataReqDto = new Xddb0013DataReqDto();// 请求Data： 抵押登记不动产登记证明入库
        Xddb0013DataRespDto xddb0013DataRespDto = new Xddb0013DataRespDto();// 响应Data：抵押登记不动产登记证明入库
        Babiec01RespDto babiec01RespDto = new Babiec01RespDto();
        cn.com.yusys.yusp.dto.server.biz.hyy.babiec01.Data hyyBabiec01Data = new cn.com.yusys.yusp.dto.server.biz.hyy.babiec01.Data();

        cn.com.yusys.yusp.dto.server.biz.xddb0013.req.Data reqData = null; // 请求Data：抵押登记不动产登记证明入库
        cn.com.yusys.yusp.dto.server.biz.xddb0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0013.resp.Data();// 响应Data：抵押登记不动产登记证明入库
        List<Boolean> booleans = new ArrayList<>();
        try {
            // 将 reqData 拷贝到xddb0013DataReqDto
            xddb0013DataReqDto.setGuarid(id);
            //xddb0013DataReqDto.setRetype(DscmsBizDbEnum.RETYPE_01.key);
            xddb0013DataReqDto.setUpdateCollateralElectronicCertificateForm(form);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataReqDto));
            ResultDto<Xddb0013DataRespDto> xddb0013DataResultDto = dscmsBizDbClientService.xddb0013(xddb0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(xddb0013DataRespDto));
            // 从返回值中获取响应码和响应消息
            //babiec01RespDto.setCode(Optional.ofNullable(xddb0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            //babiec01RespDto.setMessage(Optional.ofNullable(xddb0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0013DataResultDto.getCode())) {
                xddb0013DataRespDto = xddb0013DataResultDto.getData();
                babiec01RespDto.setCode(SuccessEnum.CODE_SUCCESS.key);
                babiec01RespDto.setSuccess(true);
                babiec01RespDto.setMessage(SuccessEnum.MESSAGE_SUCCESS.key);
                if(DscmsBizDbEnum.FALG_SUCCESS.key.equals(xddb0013DataRespDto.getOpFlag())){
                    booleans.add(true);
                    hyyBabiec01Data.setItems(booleans);
                }else{
                    booleans.add(false);
                    hyyBabiec01Data.setItems(booleans);
                }
            } else {
                babiec01RespDto.setCode(EpbEnum.EPB099999.key);
                babiec01RespDto.setSuccess(false);
                babiec01RespDto.setMessage(xddb0013DataResultDto.getMessage());
                booleans.add(false);
                hyyBabiec01Data.setItems(booleans);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, e.getMessage());
            babiec01RespDto.setCode("BA-BI-EC01-00"); // 9999
            babiec01RespDto.setMessage(EpbEnum.EPB099999.value);// 系统异常
        }

        babiec01RespDto.setData(hyyBabiec01Data);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0013.key, DscmsEnum.TRADE_CODE_XDDB0013.value, JSON.toJSONString(babiec01RespDto));
        return JSON.toJSONString(babiec01RespDto);
    }
}
