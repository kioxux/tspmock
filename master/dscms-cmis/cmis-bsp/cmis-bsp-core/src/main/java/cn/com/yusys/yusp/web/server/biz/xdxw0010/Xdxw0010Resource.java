package cn.com.yusys.yusp.web.server.biz.xdxw0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0010.req.Xdxw0010ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0010.resp.Xdxw0010RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0010.req.Xdxw0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0010.resp.Xdxw0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:勘验列表信息查询
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms")
@Api(tags = "XDXW001:勘验列表信息查询")
public class Xdxw0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0010Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0010
     * 交易描述：勘验列表信息查询
     *
     * @param xdxw0010ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdxw0010")
    @ApiOperation("勘验列表信息查询")
    //@Idempotent({"xdcaxw0010", "#xdxw0010ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0010RespDto xdxw0010(@Validated @RequestBody Xdxw0010ReqDto xdxw0010ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(xdxw0010ReqDto));
        Xdxw0010DataReqDto xdxw0010DataReqDto = new Xdxw0010DataReqDto();// 请求Data： 勘验列表信息查询
        Xdxw0010DataRespDto xdxw0010DataRespDto = new Xdxw0010DataRespDto();// 响应Data：勘验列表信息查询
        Xdxw0010RespDto xdxw0010RespDto = new Xdxw0010RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0010.req.Data reqData = null; // 请求Data：勘验列表信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0010.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0010.resp.Data();// 响应Data：勘验列表信息查询
        try {
            // 从 xdxw0010ReqDto获取 reqData
            reqData = xdxw0010ReqDto.getData();
            // 将 reqData 拷贝到xdxw0010DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0010DataReqDto);
            // 调用服务
            ResultDto<Xdxw0010DataRespDto> xdxw0010DataResultDto = dscmsBizXwClientService.xdxw0010(xdxw0010DataReqDto);
            // 从返回值中获取响应码和响应消息

            xdxw0010RespDto.setErorcd(Optional.ofNullable(xdxw0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0010RespDto.setErortx(Optional.ofNullable(xdxw0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0010DataResultDto.getCode())) {
                xdxw0010DataRespDto = xdxw0010DataResultDto.getData();
                xdxw0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0010DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0010DataRespDto, respData);
            }
        } catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, e.getMessage());
            xdxw0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0010RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, e.getMessage());
            xdxw0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0010RespDto.setDatasq(servsq);
        xdxw0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0010.key, DscmsEnum.TRADE_CODE_XDXW0010.value, JSON.toJSONString(xdxw0010RespDto));
        return xdxw0010RespDto;
    }
}
