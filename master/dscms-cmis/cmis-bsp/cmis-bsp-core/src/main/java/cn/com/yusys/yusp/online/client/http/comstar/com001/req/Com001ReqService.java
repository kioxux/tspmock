package cn.com.yusys.yusp.online.client.http.comstar.com001.req;

import java.io.Serializable;

/**
 * 请求Service：额度同步
 *
 * @author leehuang
 * @version 1.0
 */
public class Com001ReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    private String serno;//交易流水号
    private Data data;

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Com001ReqService{" +
                "serno='" + serno + '\'' +
                ", data=" + data +
                '}';
    }
}