package cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.req;


/**
 * 请求Service：惠享贷规则审批申请接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午7:53:32
 */
public class Service {

    private String prcscd; // 处理码
    private String servtp; // 渠道
    private String servsq; // 渠道流水
    private String userid; // 柜员号
    private String brchno; // 部门号
    private String channel_type; // 渠道来源
    private String co_platform; // 合作平台
    private String prd_type; // 产品类别
    private String prd_code; // 产品代码（零售智能风控内部代码）
    private String cert_type; // 证件类型
    private String cert_code; // 证件号码
    private String cust_name; // 客户姓名
    private String phone; // 移动电话
    private String cust_id_core; // 核心客户号
    private String company_name; // 企业名称
    private String company_cert_code; // 企业证件号码
    private String company_cert_type; // 企业证件类型
    private String company_type; // 企业类型
    private String biz_manager_name; // 客户经理姓名
    private String biz_manager_id; // 客户经理号
    private String biz_org_id; // 归属机构
    private String survey_serno; // 调查表编号
    private String prd_sub_code; // 产品代码（零售智能风控内部代码）

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCust_id_core() {
        return cust_id_core;
    }

    public void setCust_id_core(String cust_id_core) {
        this.cust_id_core = cust_id_core;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_cert_code() {
        return company_cert_code;
    }

    public void setCompany_cert_code(String company_cert_code) {
        this.company_cert_code = company_cert_code;
    }

    public String getCompany_cert_type() {
        return company_cert_type;
    }

    public void setCompany_cert_type(String company_cert_type) {
        this.company_cert_type = company_cert_type;
    }

    public String getCompany_type() {
        return company_type;
    }

    public void setCompany_type(String company_type) {
        this.company_type = company_type;
    }

    public String getBiz_manager_name() {
        return biz_manager_name;
    }

    public void setBiz_manager_name(String biz_manager_name) {
        this.biz_manager_name = biz_manager_name;
    }

    public String getBiz_manager_id() {
        return biz_manager_id;
    }

    public void setBiz_manager_id(String biz_manager_id) {
        this.biz_manager_id = biz_manager_id;
    }

    public String getBiz_org_id() {
        return biz_org_id;
    }

    public void setBiz_org_id(String biz_org_id) {
        this.biz_org_id = biz_org_id;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getPrd_sub_code() {
        return prd_sub_code;
    }

    public void setPrd_sub_code(String prd_sub_code) {
        this.prd_sub_code = prd_sub_code;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", channel_type='" + channel_type + '\'' +
                ", co_platform='" + co_platform + '\'' +
                ", prd_type='" + prd_type + '\'' +
                ", prd_code='" + prd_code + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", phone='" + phone + '\'' +
                ", cust_id_core='" + cust_id_core + '\'' +
                ", company_name='" + company_name + '\'' +
                ", company_cert_code='" + company_cert_code + '\'' +
                ", company_cert_type='" + company_cert_type + '\'' +
                ", company_type='" + company_type + '\'' +
                ", biz_manager_name='" + biz_manager_name + '\'' +
                ", biz_manager_id='" + biz_manager_id + '\'' +
                ", biz_org_id='" + biz_org_id + '\'' +
                ", survey_serno='" + survey_serno + '\'' +
                ", prd_sub_code='" + prd_sub_code + '\'' +
                '}';
    }
}
