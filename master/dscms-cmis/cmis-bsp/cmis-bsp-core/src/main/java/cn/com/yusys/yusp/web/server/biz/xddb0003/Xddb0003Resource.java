package cn.com.yusys.yusp.web.server.biz.xddb0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0003.req.Xddb0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0003.resp.Xddb0003RespDto;
import cn.com.yusys.yusp.dto.server.xddb0003.req.Xddb0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0003.resp.Xddb0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:押品信息同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "xddb0003:押品信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0003Resource.class);
	@Autowired
	private DscmsBizDbClientService dscmsBizdbClientService;
    /**
     * 交易码：xddb0003
     * 交易描述：押品信息同步
     *
     * @param xddb0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("押品信息同步")
    @PostMapping("/xddb0003")
    //@Idempotent({"xddb0003", "#xddb0003ReqDto.datasq"})
    protected @ResponseBody
    Xddb0003RespDto xddb0003(@Validated @RequestBody Xddb0003ReqDto xddb0003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003ReqDto));
        Xddb0003DataReqDto xddb0003DataReqDto = new Xddb0003DataReqDto();// 请求Data： 押品信息同步
        Xddb0003DataRespDto xddb0003DataRespDto = new Xddb0003DataRespDto();// 响应Data：押品信息同步
        Xddb0003RespDto xddb0003RespDto = new Xddb0003RespDto();

		cn.com.yusys.yusp.dto.server.biz.xddb0003.req.Data reqData = null; // 请求Data：押品信息同步
		cn.com.yusys.yusp.dto.server.biz.xddb0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0003.resp.Data();// 响应Data：押品信息同步
        try {
        // 从 xddb0003ReqDto获取 reqData
        reqData = xddb0003ReqDto.getData();
        // 将 reqData 拷贝到xddb0003DataReqDto
        BeanUtils.copyProperties(reqData, xddb0003DataReqDto);

        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataReqDto));
        ResultDto<Xddb0003DataRespDto> xddb0003DataResultDto = dscmsBizdbClientService.xddb0003(xddb0003DataReqDto);
        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003DataResultDto));
        // 从返回值中获取响应码和响应消息
        xddb0003RespDto.setErorcd(Optional.ofNullable(xddb0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
        xddb0003RespDto.setErortx(Optional.ofNullable(xddb0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0003DataResultDto.getCode())) {
            xddb0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
            xddb0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
            xddb0003DataRespDto = xddb0003DataResultDto.getData();
            // 将 xddb0003DataRespDto拷贝到 respData
            BeanUtils.copyProperties(xddb0003DataRespDto, respData);
        }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, e.getMessage());
            xddb0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0003RespDto.setDatasq(servsq);
        xddb0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0003.key, DscmsEnum.TRADE_CODE_XDDB0003.value, JSON.toJSONString(xddb0003RespDto));
        return xddb0003RespDto;
    }
}
