package cn.com.yusys.yusp.online.client.esb.core.ln3075.resp;

/**
 * 响应Service：逾期贷款展期
 * @author code-generator
 * @version 1.0             
 */      
public class Ln3075RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
