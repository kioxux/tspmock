package cn.com.yusys.yusp.online.client.esb.core.ln3038.req;

/**
 * 请求Service：贷款资料变更明细查询
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3038ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

