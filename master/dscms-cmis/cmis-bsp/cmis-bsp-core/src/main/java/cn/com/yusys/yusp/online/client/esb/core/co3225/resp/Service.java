package cn.com.yusys.yusp.online.client.esb.core.co3225.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵质押物明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private lstdzywmx_ARRAY lstdzywmx_ARRAY;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public cn.com.yusys.yusp.online.client.esb.core.co3225.resp.lstdzywmx_ARRAY getLstdzywmx_ARRAY() {
        return lstdzywmx_ARRAY;
    }

    public void setLstdzywmx_ARRAY(cn.com.yusys.yusp.online.client.esb.core.co3225.resp.lstdzywmx_ARRAY lstdzywmx_ARRAY) {
        this.lstdzywmx_ARRAY = lstdzywmx_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", lstdzywmx_ARRAY=" + lstdzywmx_ARRAY +
                '}';
    }
}
