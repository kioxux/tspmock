package cn.com.yusys.yusp.online.client.esb.core.ln3103.resp;
/**
 * 响应Service：贷款账户交易明细查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Listnm_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
