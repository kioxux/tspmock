package cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.req;

/**
 * 请求Service：终审申请提交
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String channel_type;//渠道来源
    private String co_platform;//合作平台
    private String prd_type;//产品类别
    private String prd_code;//产品代码（零售智能风控内部代码）
    private String cust_name;//客户姓名
    private String cert_code;//客户证件号码
    private String spouse_name;//配偶姓名
    private String spouse_cert_code;//配偶证件号码
    private String qy_grant_no;//企业征信查询授权书编号
    private String report_type;//调查报告类型
    private String survey_serno;//信贷调查表编号
    private String app_no;//业务流水号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getQy_grant_no() {
        return qy_grant_no;
    }

    public void setQy_grant_no(String qy_grant_no) {
        this.qy_grant_no = qy_grant_no;
    }

    public String getReport_type() {
        return report_type;
    }

    public void setReport_type(String report_type) {
        this.report_type = report_type;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "channel_type='" + channel_type + '\'' +
                "co_platform='" + co_platform + '\'' +
                "prd_type='" + prd_type + '\'' +
                "prd_code='" + prd_code + '\'' +
                "cust_name='" + cust_name + '\'' +
                "cert_code='" + cert_code + '\'' +
                "spouse_name='" + spouse_name + '\'' +
                "spouse_cert_code='" + spouse_cert_code + '\'' +
                "qy_grant_no='" + qy_grant_no + '\'' +
                "report_type='" + report_type + '\'' +
                "survey_serno='" + survey_serno + '\'' +
                "app_no='" + app_no + '\'' +
                '}';
    }
}