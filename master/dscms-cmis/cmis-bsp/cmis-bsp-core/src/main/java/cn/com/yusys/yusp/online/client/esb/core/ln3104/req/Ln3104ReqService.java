package cn.com.yusys.yusp.online.client.esb.core.ln3104.req;


/**
 * 贷款利率变更明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3104ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

