package cn.com.yusys.yusp.online.client.esb.circp.fb1150.req;

/**
 * <br>
 * 0.2ZRC:2021/8/11 10:58:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/8/11 10:58
 * @since 2021/8/11 10:58
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1150.req.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
