package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstytczfe.Record;

import java.util.List;

public class Lstytczfe_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstytczfe.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstytczfe_ARRAY{" +
                "record=" + record +
                '}';
    }
}
