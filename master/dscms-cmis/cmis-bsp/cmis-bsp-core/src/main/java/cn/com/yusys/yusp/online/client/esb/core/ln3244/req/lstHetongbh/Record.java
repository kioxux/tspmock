package cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh;
/**
 * 请求Service：贷款客户经理批量移交
 */
public class Record {
    private String hetongbh;//合同编号

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "hetongbh='" + hetongbh + '\'' +
                '}';
    }
}
