package cn.com.yusys.yusp.online.client.esb.core.ln3102.req;

/**
 * 请求Dto：贷款期供查询试算
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间
    private String datasq;//全局流水

    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private Integer qishiqsh;//起始期数
    private Integer zhzhiqsh;//终止期数
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数
    private String qishriqi;//起始日期
    private String zhzhriqi;//终止日期
    private Integer jixiqish;//计息期数
    private String benqizht;//本期状态
    private String shisriqi;//试算日期
    private String chaxunzl;//查询种类
    private String shifoudy;//是否打印

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getQishiqsh() {
        return qishiqsh;
    }

    public void setQishiqsh(Integer qishiqsh) {
        this.qishiqsh = qishiqsh;
    }

    public Integer getZhzhiqsh() {
        return zhzhiqsh;
    }

    public void setZhzhiqsh(Integer zhzhiqsh) {
        this.zhzhiqsh = zhzhiqsh;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public Integer getJixiqish() {
        return jixiqish;
    }

    public void setJixiqish(Integer jixiqish) {
        this.jixiqish = jixiqish;
    }

    public String getBenqizht() {
        return benqizht;
    }

    public void setBenqizht(String benqizht) {
        this.benqizht = benqizht;
    }

    public String getShisriqi() {
        return shisriqi;
    }

    public void setShisriqi(String shisriqi) {
        this.shisriqi = shisriqi;
    }

    public String getChaxunzl() {
        return chaxunzl;
    }

    public void setChaxunzl(String chaxunzl) {
        this.chaxunzl = chaxunzl;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", qishiqsh=" + qishiqsh +
                ", zhzhiqsh=" + zhzhiqsh +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", jixiqish=" + jixiqish +
                ", benqizht='" + benqizht + '\'' +
                ", shisriqi='" + shisriqi + '\'' +
                ", chaxunzl='" + chaxunzl + '\'' +
                ", shifoudy='" + shifoudy + '\'' +
                '}';
    }
}  
