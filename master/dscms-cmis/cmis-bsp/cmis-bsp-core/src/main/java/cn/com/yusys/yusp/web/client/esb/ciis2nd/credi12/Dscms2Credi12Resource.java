package cn.com.yusys.yusp.web.client.esb.ciis2nd.credi12;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi12.Credi12ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi12.Credi12RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.req.Credi12ReqService;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.resp.Credi12RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用二代征信系统的接口处理类
 **/
@Api(tags = "BSP封装调用二代征信系统的接口处理类（credi12）")
@RestController
@RequestMapping("/api/dscms2ciis2nd")
public class Dscms2Credi12Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Credi12Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB通用查询接口（处理码credi12）
     *
     * @param credi12ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("credi12:ESB通用查询接口")
    @PostMapping("/credi12")
    protected @ResponseBody
    ResultDto<Credi12RespDto> credi12(@Validated @RequestBody Credi12ReqDto credi12ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CRED12.key, EsbEnum.TRADE_CODE_CRED12.value, JSON.toJSONString(credi12ReqDto));
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.req.Service();
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.resp.Service();
        Credi12ReqService credi12ReqService = new Credi12ReqService();
        Credi12RespService credi12RespService = new Credi12RespService();
        Credi12RespDto credi12RespDto = new Credi12RespDto();
        ResultDto<Credi12RespDto> credi12ResultDto = new ResultDto<Credi12RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Credi12ReqDto转换成reqService
            BeanUtils.copyProperties(credi12ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CRED12.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_CIIS2ND.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIIS2ND.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            credi12ReqService.setService(reqService);
            // 将credi12ReqService转换成credi12ReqServiceMap
            Map credi12ReqServiceMap = beanMapUtil.beanToMap(credi12ReqService);
            context.put("tradeDataMap", credi12ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CRED12.key, EsbEnum.TRADE_CODE_CRED12.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CRED12.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CRED12.key, EsbEnum.TRADE_CODE_CRED12.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            credi12RespService = beanMapUtil.mapToBean(tradeDataMap, Credi12RespService.class, Credi12RespService.class);
            respService = credi12RespService.getService();

            credi12ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            credi12ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将Credi12RespDto封装到ResultDto<Credi12RespDto>
                BeanUtils.copyProperties(respService, credi12RespDto);
                credi12ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                credi12ResultDto.setMessage(Optional.ofNullable(credi12ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            } else {
                credi12ResultDto.setCode(EpbEnum.EPB099999.key);
                credi12ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CRED12.key, EsbEnum.TRADE_CODE_CRED12.value, e.getMessage());
            credi12ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            credi12ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        credi12ResultDto.setData(credi12RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CRED12.key, EsbEnum.TRADE_CODE_CRED12.value, JSON.toJSONString(credi12ResultDto));
        return credi12ResultDto;
    }
}
