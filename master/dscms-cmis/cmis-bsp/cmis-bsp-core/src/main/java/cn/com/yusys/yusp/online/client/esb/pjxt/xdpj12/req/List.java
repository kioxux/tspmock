package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 20:41
 * @since 2021/5/28 20:41
 */
public class List {

    java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
