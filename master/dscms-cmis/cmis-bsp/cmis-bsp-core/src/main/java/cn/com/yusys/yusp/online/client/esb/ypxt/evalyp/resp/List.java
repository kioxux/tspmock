package cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp;

/**
 * 响应Service：押品我行确认价值同步接口
 *
 * @author chenyong
 * @version 1.0
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
