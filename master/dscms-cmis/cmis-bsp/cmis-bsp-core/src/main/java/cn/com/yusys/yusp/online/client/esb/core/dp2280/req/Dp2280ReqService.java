package cn.com.yusys.yusp.online.client.esb.core.dp2280.req;

/**
 * 请求Service：子账户序号查询接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/16上午9:54:28
 */
public class Dp2280ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
