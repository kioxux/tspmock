package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd003.req;

/**
 * 请求Service：信贷系统请求V平台二级准入接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Wxd003ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Wxd003ReqService{" +
                "service=" + service +
                '}';
    }
}