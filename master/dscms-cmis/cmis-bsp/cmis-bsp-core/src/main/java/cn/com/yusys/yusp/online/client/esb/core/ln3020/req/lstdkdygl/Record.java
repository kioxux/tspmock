package cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkdygl;

import java.math.BigDecimal;

public class Record {
    private String dkjiejuh;//贷款借据号
    private String glyewuzl;//关联业务种类
    private String dbaofshi;//担保方式
    private String dzywbhao;//抵质押物编号
    private String dbaokzzl;//担保控制种类
    private BigDecimal dbaobili;//担保比例
    private String lddbkzhi;//联动担保控制
    private String dbaobizh;//担保币种
    private BigDecimal dbaojine;//担保金额
    private String jijubizh;//借据币种
    private BigDecimal jiejuuje;//借据金额
    private BigDecimal zhshuilv;//折算汇率
    private String guanlzht;//关联状态
    private String mingxixh;//明细序号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getGlyewuzl() {
        return glyewuzl;
    }

    public void setGlyewuzl(String glyewuzl) {
        this.glyewuzl = glyewuzl;
    }

    public String getDbaofshi() {
        return dbaofshi;
    }

    public void setDbaofshi(String dbaofshi) {
        this.dbaofshi = dbaofshi;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDbaokzzl() {
        return dbaokzzl;
    }

    public void setDbaokzzl(String dbaokzzl) {
        this.dbaokzzl = dbaokzzl;
    }

    public BigDecimal getDbaobili() {
        return dbaobili;
    }

    public void setDbaobili(BigDecimal dbaobili) {
        this.dbaobili = dbaobili;
    }

    public String getLddbkzhi() {
        return lddbkzhi;
    }

    public void setLddbkzhi(String lddbkzhi) {
        this.lddbkzhi = lddbkzhi;
    }

    public String getDbaobizh() {
        return dbaobizh;
    }

    public void setDbaobizh(String dbaobizh) {
        this.dbaobizh = dbaobizh;
    }

    public BigDecimal getDbaojine() {
        return dbaojine;
    }

    public void setDbaojine(BigDecimal dbaojine) {
        this.dbaojine = dbaojine;
    }

    public String getJijubizh() {
        return jijubizh;
    }

    public void setJijubizh(String jijubizh) {
        this.jijubizh = jijubizh;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getZhshuilv() {
        return zhshuilv;
    }

    public void setZhshuilv(BigDecimal zhshuilv) {
        this.zhshuilv = zhshuilv;
    }

    public String getGuanlzht() {
        return guanlzht;
    }

    public void setGuanlzht(String guanlzht) {
        this.guanlzht = guanlzht;
    }

    public String getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(String mingxixh) {
        this.mingxixh = mingxixh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", glyewuzl='" + glyewuzl + '\'' +
                ", dbaofshi='" + dbaofshi + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                ", dbaokzzl='" + dbaokzzl + '\'' +
                ", dbaobili=" + dbaobili +
                ", lddbkzhi='" + lddbkzhi + '\'' +
                ", dbaobizh='" + dbaobizh + '\'' +
                ", dbaojine=" + dbaojine +
                ", jijubizh='" + jijubizh + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", zhshuilv=" + zhshuilv +
                ", guanlzht='" + guanlzht + '\'' +
                ", mingxixh='" + mingxixh + '\'' +
                '}';
    }
}
