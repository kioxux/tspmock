package cn.com.yusys.yusp.web.server.biz.xdxw0045;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0045.req.Xdxw0045ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0045.resp.Xdxw0045RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0045.req.Xdxw0045DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0045.resp.Xdxw0045DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优抵贷客户调查撤销
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0045:优抵贷客户调查撤销")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0045Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0045Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0045
     * 交易描述：优抵贷客户调查撤销
     *
     * @param xdxw0045ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优抵贷客户调查撤销")
    @PostMapping("/xdxw0045")
    //@Idempotent({"xdcaxw0045", "#xdxw0045ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0045RespDto xdxw0045(@Validated @RequestBody Xdxw0045ReqDto xdxw0045ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045ReqDto));
        Xdxw0045DataReqDto xdxw0045DataReqDto = new Xdxw0045DataReqDto();// 请求Data： 优抵贷客户调查撤销
        Xdxw0045DataRespDto xdxw0045DataRespDto = new Xdxw0045DataRespDto();// 响应Data：优抵贷客户调查撤销
		Xdxw0045RespDto xdxw0045RespDto=new Xdxw0045RespDto();
		cn.com.yusys.yusp.dto.server.biz.xdxw0045.req.Data reqData = null; // 请求Data：优抵贷客户调查撤销
		cn.com.yusys.yusp.dto.server.biz.xdxw0045.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0045.resp.Data ();// 响应Data：优抵贷客户调查撤销

        try {
            // 从 xdxw0045ReqDto获取 reqData
            reqData = xdxw0045ReqDto.getData();
            // 将 reqData 拷贝到xdxw0045DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0045DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045DataReqDto));
            ResultDto<Xdxw0045DataRespDto> xdxw0045DataResultDto = dscmsBizXwClientService.xdxw0045(xdxw0045DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0045RespDto.setErorcd(Optional.ofNullable(xdxw0045DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0045RespDto.setErortx(Optional.ofNullable(xdxw0045DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0045DataResultDto.getCode())) {
                xdxw0045DataRespDto = xdxw0045DataResultDto.getData();
                xdxw0045RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0045RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0045DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0045DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, e.getMessage());
            xdxw0045RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0045RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0045RespDto.setDatasq(servsq);

        xdxw0045RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0045.key, DscmsEnum.TRADE_CODE_XDXW0045.value, JSON.toJSONString(xdxw0045RespDto));
        return xdxw0045RespDto;
    }
}
