package cn.com.yusys.yusp.online.client.esb.core.ln3235.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd.Lsdkbjfd;
import cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh.Lsdkhbjh;

/**
 * 响应Service：还款方式转换
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh.Lsdkhbjh lsdkhbjh;//贷款还本计划[LIST]
    private cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd.Lsdkbjfd lsdkbjfd;//本金分段登记[LIST]
    private String qxbgtzjh;//期限变更调整计划
    private String benjinfd;//本金分段
    private String dzhhkjih;//定制还款计划
    private String llbgtzjh;//利率变更调整计划
    private String dcfktzjh;//多次放款调整计划
    private String tqhktzjh;//提前还款调整计划
    private String huankfsh;//还款方式
    private String scihkrbz;//首次还款日模式
    private String yunxtqhk;//允许提前还款

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public Lsdkhbjh getLsdkhbjh() {
        return lsdkhbjh;
    }

    public void setLsdkhbjh(Lsdkhbjh lsdkhbjh) {
        this.lsdkhbjh = lsdkhbjh;
    }

    public Lsdkbjfd getLsdkbjfd() {
        return lsdkbjfd;
    }

    public void setLsdkbjfd(Lsdkbjfd lsdkbjfd) {
        this.lsdkbjfd = lsdkbjfd;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getBenjinfd() {
        return benjinfd;
    }

    public void setBenjinfd(String benjinfd) {
        this.benjinfd = benjinfd;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", lsdkhbjh=" + lsdkhbjh +
                ", lsdkbjfd=" + lsdkbjfd +
                ", qxbgtzjh='" + qxbgtzjh + '\'' +
                ", benjinfd='" + benjinfd + '\'' +
                ", dzhhkjih='" + dzhhkjih + '\'' +
                ", llbgtzjh='" + llbgtzjh + '\'' +
                ", dcfktzjh='" + dcfktzjh + '\'' +
                ", tqhktzjh='" + tqhktzjh + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", scihkrbz='" + scihkrbz + '\'' +
                ", yunxtqhk='" + yunxtqhk + '\'' +
                '}';
    }
}
