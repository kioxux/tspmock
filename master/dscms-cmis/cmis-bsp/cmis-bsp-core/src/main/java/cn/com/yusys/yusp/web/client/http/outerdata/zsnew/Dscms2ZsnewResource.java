package cn.com.yusys.yusp.web.client.http.outerdata.zsnew;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.outerdata.zsnew.Data;
import cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ZsnewReqService;
import cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ZsnewRespService;
import cn.com.yusys.yusp.web.client.http.outerdata.idcheck.Dscms2IdCheckResource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(zsnew)")
@RestController
@RequestMapping("/api/dscms2outerdata")
public class Dscms2ZsnewResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2IdCheckResource.class);
    @Autowired
    private RestTemplate restTemplate;

    @Value("${application.outerdata.url}")
    private String outerdataUrl;

    /**
     * 企业工商信息查询
     *
     * @param zsnewReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("zsnew:企业工商信息查询")
    @PostMapping("/zsnew")
    protected @ResponseBody
    ResultDto<ZsnewRespDto> zsnew(@Validated @RequestBody ZsnewReqDto zsnewReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewReqDto));

        ZsnewReqService zsnewReqService = new ZsnewReqService();
        ZsnewRespService zsnewRespService = new ZsnewRespService();
        ZsnewRespDto zsnewRespDto = new ZsnewRespDto();
        ResultDto<ZsnewRespDto> zsnewResultDto = new ResultDto<ZsnewRespDto>();
        try {
            //  将ZsnewReqDto转换成ZsnewReqService
            BeanUtils.copyProperties(zsnewReqDto, zsnewReqService);

            zsnewReqService.setPrcscd(EsbEnum.TRADE_CODE_ZSNEW.key);//    交易码
            zsnewReqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            zsnewReqService.setServsq(servsq);//    渠道流水
            zsnewReqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            zsnewReqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(zsnewReqService), headers);
            //String url = "http://10.28.122.177:7006/edataApi/data";
            String url = outerdataUrl + "/edataApi/data";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, responseEntity.getBody());

            String responseStr = responseEntity.getBody();
            zsnewRespService = (ZsnewRespService) JSON.parseObject(responseStr, ZsnewRespService.class);
            Data zsnewRespData = new Data();
            cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.Data zsnewRespDtoData = new cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.Data();
            //  将ZsnewRespDto封装到ResultDto<ZsnewRespDto>
            zsnewResultDto.setCode(Optional.ofNullable(zsnewRespService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            zsnewResultDto.setMessage(Optional.ofNullable(zsnewRespService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, zsnewRespService.getErorcd())) {
                zsnewResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                zsnewResultDto.setMessage(SuccessEnum.SUCCESS.value);
                zsnewRespData = zsnewRespService.getData();
                String code = Optional.ofNullable(zsnewRespData.getCODE()).orElseGet(zsnewRespData::getCODE);
                if (Objects.equals("200", code)) {// 先校验
                    //  将ZsnewRespService转换成ZsnewRespDto
                    BeanUtils.copyProperties(zsnewRespData, zsnewRespDtoData);
                    cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENT_INFO entInfoRespService = zsnewRespData.getENT_INFO();
                    cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ENT_INFO entInfoRespDto = Optional.ofNullable(zsnewRespDtoData.getENT_INFO()).orElseGet(() -> new cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ENT_INFO());
                    BeanUtils.copyProperties(entInfoRespService, entInfoRespDto);
                    // 此处对BASIC单独处理 开始
                    cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BASIC basicRespService = Optional.ofNullable(entInfoRespService.getBASIC()).orElseGet(() -> new cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BASIC());
                    cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.BASIC basicRespDto = Optional.ofNullable(entInfoRespDto.getBASIC()).orElseGet(() -> new cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.BASIC());
                    BeanUtils.copyProperties(basicRespService, basicRespDto);
                    entInfoRespDto.setBASIC(basicRespDto);
                    // 此处对BASIC单独处理 结束
                    zsnewRespDtoData.setENT_INFO(entInfoRespDto);
                    zsnewRespDto.setData(zsnewRespDtoData);
                } else {
                    String msg = Optional.ofNullable(zsnewRespData.getMSG()).orElseGet(zsnewRespData::getMSG);
                    //  将IdCheckRespService转换成IdCheckRespDto
                    BeanUtils.copyProperties(zsnewRespData, zsnewRespDtoData);
                    zsnewResultDto.setCode(code);
                    zsnewResultDto.setMessage(msg);
                }
            } else {
                zsnewResultDto.setCode(EpbEnum.EPB099999.key);
                zsnewResultDto.setMessage(zsnewRespService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, e.getMessage());
            zsnewResultDto.setCode(EpbEnum.EPB099999.key);//9999
            zsnewResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        zsnewResultDto.setData(zsnewRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZSNEW.key, EsbEnum.TRADE_CODE_ZSNEW.value, JSON.toJSONString(zsnewResultDto));

        return zsnewResultDto;
    }
}
