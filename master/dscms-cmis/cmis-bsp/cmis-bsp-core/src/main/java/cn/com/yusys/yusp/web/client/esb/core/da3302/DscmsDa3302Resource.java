package cn.com.yusys.yusp.web.client.esb.core.da3302;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3302.req.Da3302ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3302.resp.Da3302RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3302.req.Da3302ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3302.resp.Da3302RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵债资产处置
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class DscmsDa3302Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsDa3302Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3302
     * 交易描述：抵债资产处置
     *
     * @param da3302ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3302")
    protected @ResponseBody
    ResultDto<Da3302RespDto> da3302(@Validated @RequestBody Da3302ReqDto da3302ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3302.key, EsbEnum.TRADE_CODE_DA3302.value, JSON.toJSONString(da3302ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3302.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3302.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3302.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3302.resp.Service();

        Da3302ReqService da3302ReqService = new Da3302ReqService();
        Da3302RespService da3302RespService = new Da3302RespService();
        Da3302RespDto da3302RespDto = new Da3302RespDto();
        ResultDto<Da3302RespDto> da3302ResultDto = new ResultDto<Da3302RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3302ReqDto转换成reqService
            BeanUtils.copyProperties(da3302ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3302.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3302ReqService.setService(reqService);
            // 将da3302ReqService转换成da3302ReqServiceMap
            Map da3302ReqServiceMap = beanMapUtil.beanToMap(da3302ReqService);
            context.put("tradeDataMap", da3302ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3302.key, EsbEnum.TRADE_CODE_DA3302.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3302.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3302.key, EsbEnum.TRADE_CODE_DA3302.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3302RespService = beanMapUtil.mapToBean(tradeDataMap, Da3302RespService.class, Da3302RespService.class);
            respService = da3302RespService.getService();


            da3302ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3302ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3302RespDto
                BeanUtils.copyProperties(respService, da3302RespDto);
                da3302ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3302ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3302ResultDto.setCode(EpbEnum.EPB099999.key);
                da3302ResultDto.setMessage(respService.getErortx());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3302.key, EsbEnum.TRADE_CODE_DA3302.value, e.getMessage());
            da3302ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3302ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3302ResultDto.setData(da3302RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3302.key, EsbEnum.TRADE_CODE_DA3302.value, JSON.toJSONString(da3302ResultDto));
        return da3302ResultDto;
    }
}
