package cn.com.yusys.yusp.online.client.esb.core.ln3248.req;

/**
 * 请求Service：委托清收变更
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3248ReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}

	@Override
	public String toString() {
		return "Ln3248ReqService{" +
				"service=" + service +
				'}';
	}
}

