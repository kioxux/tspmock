package cn.com.yusys.yusp.web.server.biz.xdtz0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0009.req.Xdtz0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0009.resp.Xdtz0009RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0009.req.Xdtz0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0009.resp.Xdtz0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户经理不良率
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDTZ0009:查询客户经理不良率")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0009Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0009
     * 交易描述：查询客户经理不良率
     *
     * @param xdtz0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0009:查询客户经理不良率")
    @PostMapping("/xdtz0009")
    //@Idempotent({"xdcatz0009", "#xdtz0009ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0009RespDto xdtz0009(@Validated @RequestBody Xdtz0009ReqDto xdtz0009ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009ReqDto));

        Xdtz0009DataReqDto xdtz0009DataReqDto = new Xdtz0009DataReqDto();// 请求Data： 查询客户经理不良率
        Xdtz0009DataRespDto xdtz0009DataRespDto = new Xdtz0009DataRespDto();// 响应Data：查询客户经理不良率
        Xdtz0009RespDto xdtz0009RespDto = new Xdtz0009RespDto();// 响应Dto：客户准入级别同步
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdtz0009.req.Data reqData = null; // 请求Data：查询客户经理不良率
        cn.com.yusys.yusp.dto.server.biz.xdtz0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0009.resp.Data();
        // 响应Data：查询客户经理不良率
        //  此处包导入待确定 结束
        // 从 xdtz0009ReqDto获取 reqData
        try {
            reqData = xdtz0009ReqDto.getData();
            // 将 reqData 拷贝到xdtz0009DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0009DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009DataReqDto));
            ResultDto<Xdtz0009DataRespDto> xdtz0009DataResultDto = dscmsBizTzClientService.xdtz0009(xdtz0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0009RespDto.setErorcd(Optional.ofNullable(xdtz0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0009RespDto.setErortx(Optional.ofNullable(xdtz0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0009DataResultDto.getCode())) {
                xdtz0009DataRespDto = xdtz0009DataResultDto.getData();
                xdtz0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, e.getMessage());
            xdtz0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0009RespDto.setDatasq(servsq);

        xdtz0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0009.key, DscmsEnum.TRADE_CODE_XDTZ0009.value, JSON.toJSONString(xdtz0009RespDto));
        return xdtz0009RespDto;
    }
}
