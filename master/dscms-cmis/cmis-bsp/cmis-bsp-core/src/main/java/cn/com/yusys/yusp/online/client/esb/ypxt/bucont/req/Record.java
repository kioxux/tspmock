package cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req;


/**
 * 请求Service：业务与担保合同关系接口（处理码credi1）
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Record {


    private String ywbhyp;//	业务编号	是	varchar(32)	是		ywbhyp
    private String ywlxyp;//	业务类型	是	varchar(5)	是	01-授信；02-业务	ywlxyp
    private String dbhtbh;//	担保合同编号	是	varchar(32)	是		dbhtbh
    private String zjdbbs;//	是否追加的担保合同号	是	varchar(32)	是	1-是；0-否（）	zjdbbs
    private String isflag;//	是否有效	是	varchar(5)	是	1-有效；0-无效	isflag

    public String getYwbhyp() {
        return ywbhyp;
    }

    public void setYwbhyp(String ywbhyp) {
        this.ywbhyp = ywbhyp;
    }

    public String getYwlxyp() {
        return ywlxyp;
    }

    public void setYwlxyp(String ywlxyp) {
        this.ywlxyp = ywlxyp;
    }

    public String getDbhtbh() {
        return dbhtbh;
    }

    public void setDbhtbh(String dbhtbh) {
        this.dbhtbh = dbhtbh;
    }

    public String getZjdbbs() {
        return zjdbbs;
    }

    public void setZjdbbs(String zjdbbs) {
        this.zjdbbs = zjdbbs;
    }

    public String getIsflag() {
        return isflag;
    }

    public void setIsflag(String isflag) {
        this.isflag = isflag;
    }

    @Override
    public String toString() {
        return "Record{" +
                "ywbhyp='" + ywbhyp + '\'' +
                ", ywlxyp='" + ywlxyp + '\'' +
                ", dbhtbh='" + dbhtbh + '\'' +
                ", zjdbbs='" + zjdbbs + '\'' +
                ", isflag='" + isflag + '\'' +
                '}';
    }
}
