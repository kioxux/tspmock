package cn.com.yusys.yusp.web.server.cfg.xdxt0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0013.req.Xdxt0013ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0013.resp.Xdxt0013RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdxt0013.req.Xdxt0013DataReqDto;
import cn.com.yusys.yusp.server.xdxt0013.resp.Xdxt0013DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:树形字典通用列表查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDXT0013:树形字典通用列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0013Resource.class);
    @Autowired
    private DscmsCfgXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0013
     * 交易描述：树形字典通用列表查询
     *
     * @param xdxt0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("树形字典通用列表查询")
    @PostMapping("/xdxt0013")
    //@Idempotent({"xdcaxt0013", "#xdxt0013ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0013RespDto xdxt0013(@Validated @RequestBody Xdxt0013ReqDto xdxt0013ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013ReqDto));
        Xdxt0013DataReqDto xdxt0013DataReqDto = new Xdxt0013DataReqDto();// 请求Data： 树形字典通用列表查询
        Xdxt0013DataRespDto xdxt0013DataRespDto = new Xdxt0013DataRespDto();// 响应Data：树形字典通用列表查询
        Xdxt0013RespDto xdxt0013RespDto = new Xdxt0013RespDto();
        cn.com.yusys.yusp.dto.server.cfg.xdxt0013.req.Data reqData = null; // 请求Data：树形字典通用列表查询
        cn.com.yusys.yusp.dto.server.cfg.xdxt0013.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdxt0013.resp.Data();// 响应Data：树形字典通用列表查询

        try {
            // 从 xdxt0013ReqDto获取 reqData
            reqData = xdxt0013ReqDto.getData();
            // 将 reqData 拷贝到xdxt0013DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0013DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013DataReqDto));
            ResultDto<Xdxt0013DataRespDto> xdxt0013DataResultDto = dscmsXtClientService.xdxt0013(xdxt0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0013RespDto.setErorcd(Optional.ofNullable(xdxt0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0013RespDto.setErortx(Optional.ofNullable(xdxt0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0013DataResultDto.getCode())) {
                xdxt0013DataRespDto = xdxt0013DataResultDto.getData();
                xdxt0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0013DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, e.getMessage());
            xdxt0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0013RespDto.setDatasq(servsq);

        xdxt0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0013.key, DscmsEnum.TRADE_CODE_XDXT0013.value, JSON.toJSONString(xdxt0013RespDto));
        return xdxt0013RespDto;
    }
}
