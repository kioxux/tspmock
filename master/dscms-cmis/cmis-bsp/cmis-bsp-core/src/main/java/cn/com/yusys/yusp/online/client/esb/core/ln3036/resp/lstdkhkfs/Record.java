package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkfs;

import java.math.BigDecimal;

public class Record {
    private String huankfsh;//还款方式

    private String shengxrq;//生效日期

    private BigDecimal meiqhkze;//每期还款总额

    private BigDecimal meiqhbje;//每期还本金额

    private String huanbzhq;//还本周期

    private String yuqhkzhq;//逾期还款周期

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    @Override
    public String toString() {
        return "Record{" +
                "huankfsh='" + huankfsh + '\'' +
                ", shengxrq='" + shengxrq + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", huanbzhq='" + huanbzhq + '\'' +
                ", yuqhkzhq='" + yuqhkzhq + '\'' +
                '}';
    }
}
