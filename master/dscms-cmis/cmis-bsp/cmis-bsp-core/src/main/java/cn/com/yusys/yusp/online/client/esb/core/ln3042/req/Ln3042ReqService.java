package cn.com.yusys.yusp.online.client.esb.core.ln3042.req;

/**
 * 请求Service：手工指定归还某项利息金额，不按既定顺序归还
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3042ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}


