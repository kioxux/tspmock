package cn.com.yusys.yusp.online.client.esb.core.ln3196.resp;

/**
 * 响应Service：账号子序号查询交易，支持查询内部户、负债账户
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:13
 */
public class Service {

    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String kehuzhao;//	客户账号		BaseType.U_KEHUZHAO	string	(35)		否
    private String kehuhaoo;//	客户号		BaseType.U_KEHUHAOO	string	(20)		否
    private String kehmingc;//	客户名称		BaseType.U_ZHONGWMC	string	(750)		否
    private LstioDpCusSysAccListInfo_ARRAY lstioDpCusSysAccListInfo_ARRAY;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public LstioDpCusSysAccListInfo_ARRAY getLstioDpCusSysAccListInfo_ARRAY() {
        return lstioDpCusSysAccListInfo_ARRAY;
    }

    public void setLstioDpCusSysAccListInfo_ARRAY(LstioDpCusSysAccListInfo_ARRAY lstioDpCusSysAccListInfo_ARRAY) {
        this.lstioDpCusSysAccListInfo_ARRAY = lstioDpCusSysAccListInfo_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", lstioDpCusSysAccListInfo_ARRAY=" + lstioDpCusSysAccListInfo_ARRAY +
                '}';
    }
}
