package cn.com.yusys.yusp.web.server.cfg.xdsx0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdsx0007.req.Xdsx0007ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdsx0007.resp.Xdsx0007RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdsx0007.req.Xdsx0007DataReqDto;
import cn.com.yusys.yusp.server.xdsx0007.resp.Xdsx0007DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:授信业务授权同步、资本占用率参数表同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDSX0007:授信业务授权同步、资本占用率参数表同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0007Resource.class);
    @Autowired
    private DscmsCfgSxClientService dscmsCfgSxClientService;

    /**
     * 交易码：xdsx0007
     * 交易描述：授信业务授权同步、资本占用率参数表同步
     *
     * @param xdsx0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdsx0007:授信业务授权同步、资本占用率参数表同步")
    @PostMapping("/xdsx0007")
    //@Idempotent({"xdcasx0007", "#xdsx0007ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0007RespDto xdsx0007(@Validated @RequestBody Xdsx0007ReqDto xdsx0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007ReqDto));
        Xdsx0007DataReqDto xdsx0007DataReqDto = new Xdsx0007DataReqDto();// 请求Data： 授信业务授权同步、资本占用率参数表同步
        Xdsx0007DataRespDto xdsx0007DataRespDto = new Xdsx0007DataRespDto();// 响应Data：授信业务授权同步、资本占用率参数表同步
        Xdsx0007RespDto xdsx0007RespDto = new Xdsx0007RespDto();// 响应Dto：专业贷款评级结果同步
        cn.com.yusys.yusp.dto.server.cfg.xdsx0007.req.Data reqData = null; // 请求Data：授信业务授权同步、资本占用率参数表同步
        cn.com.yusys.yusp.dto.server.cfg.xdsx0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdsx0007.resp.Data();// 响应Data：授信业务授权同步、资本占用率参数表同步
        try {
            // 从 xdsx0007ReqDto获取 reqData
            reqData = xdsx0007ReqDto.getData();
            // 将 reqData 拷贝到xdsx0007DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0007DataReqDto);
            // 调用服务
            ResultDto<Xdsx0007DataRespDto> xdsx0007DataResultDto = dscmsCfgSxClientService.xdsx0007(xdsx0007DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdsx0007RespDto.setErorcd(Optional.ofNullable(xdsx0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0007RespDto.setErortx(Optional.ofNullable(xdsx0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdsx0007RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0007DataResultDto.getCode())) {
                xdsx0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdsx0007DataRespDto = xdsx0007DataResultDto.getData();
                // 将 xdsx0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0007DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, e.getMessage());
            xdsx0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0007RespDto.setDatasq(servsq);
        xdsx0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0007.key, DscmsEnum.TRADE_CODE_XDSX0007.value, JSON.toJSONString(xdsx0007RespDto));
        return xdsx0007RespDto;
    }
}

