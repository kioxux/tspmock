package cn.com.yusys.yusp.web.client.esb.core.ib1241;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ib1241.req.Ib1241ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ib1241.resp.Ib1241RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(Ib1241)")
@RestController
@RequestMapping("/api/dscms2coreib")
public class Dscms2Ib1241Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ib1241Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ib1241
     * 交易描述：外围自动冲正(出库撤销)
     *
     * @param ib1241ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ib1241:外围自动冲正(出库撤销)")
    @PostMapping("/ib1241")
    protected @ResponseBody
    ResultDto<Ib1241RespDto> ib1241(@Validated @RequestBody Ib1241ReqDto ib1241ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value, JSON.toJSONString(ib1241ReqDto));

        cn.com.yusys.yusp.online.client.esb.core.ib1241.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ib1241.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ib1241.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ib1241.resp.Service();

        Ib1241ReqService ib1241ReqService = new Ib1241ReqService();
        Ib1241RespService ib1241RespService = new Ib1241RespService();
        Ib1241RespDto ib1241RespDto = new Ib1241RespDto();
        ResultDto<Ib1241RespDto> ib1241ResultDto = new ResultDto<Ib1241RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ib1241ReqDto转换成reqService
            BeanUtils.copyProperties(ib1241ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IB1241.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(Optional.ofNullable(ib1241ReqDto.getBrchno()).orElse(EsbEnum.BRCHNO_CORE.key));//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ib1241ReqService.setService(reqService);
            // 将ib1241ReqService转换成ib1241ReqServiceMap
            Map ib1241ReqServiceMap = beanMapUtil.beanToMap(ib1241ReqService);
            context.put("tradeDataMap", ib1241ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IB1241.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ib1241RespService = beanMapUtil.mapToBean(tradeDataMap, Ib1241RespService.class, Ib1241RespService.class);
            respService = ib1241RespService.getService();
            //  将respService转换成Ib1241RespDto
            BeanUtils.copyProperties(respService, ib1241RespDto);
            ib1241ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ib1241ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ib1241RespDto
                BeanUtils.copyProperties(respService, ib1241RespDto);
                ib1241ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ib1241ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ib1241ResultDto.setCode(EpbEnum.EPB099999.key);
                ib1241ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value, e.getMessage());
            ib1241ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ib1241ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ib1241ResultDto.setData(ib1241RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1241.key, EsbEnum.TRADE_CODE_IB1241.value, JSON.toJSONString(ib1241ResultDto));

        return ib1241ResultDto;
    }
}

