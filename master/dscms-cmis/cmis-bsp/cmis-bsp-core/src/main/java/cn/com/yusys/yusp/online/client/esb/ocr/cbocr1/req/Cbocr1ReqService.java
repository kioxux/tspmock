package cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.req;

/**
 * 请求Service：新增批次报表数据
 * @author chenyong
 * @version 1.0             
 */      
public class Cbocr1ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
