package cn.com.yusys.yusp.online.client.esb.core.ln3196.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.lstioDpCusSysAccListInfo.Record;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Service：账号子序号查询交易，支持查询内部户、负债账户
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class LstioDpCusSysAccListInfo_ARRAY  {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.lstioDpCusSysAccListInfo.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstioDpCusSysAccListInfo_ARRAY{" +
                "record=" + record +
                '}';
    }
}
