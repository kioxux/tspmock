package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.plage_list;

/**
 * <br>
 * 0.2ZRC:2021/9/9 9:00:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/9/9 9:00
 * @since 2021/9/9 9:00
 */
public class Record {
    private String plage_serno;//抵质押品流水号
    private String plage_addr;//抵质押品地址
    private String plage_owner;//抵质押品所有权人
    private String reason;//修改原因


    public String getPlage_serno() {
        return plage_serno;
    }

    public void setPlage_serno(String plage_serno) {
        this.plage_serno = plage_serno;
    }

    public String getPlage_addr() {
        return plage_addr;
    }

    public void setPlage_addr(String plage_addr) {
        this.plage_addr = plage_addr;
    }

    public String getPlage_owner() {
        return plage_owner;
    }

    public void setPlage_owner(String plage_owner) {
        this.plage_owner = plage_owner;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "Record{" +
                "plage_serno='" + plage_serno + '\'' +
                ", plage_addr='" + plage_addr + '\'' +
                ", plage_owner='" + plage_owner + '\'' +
                ", reason='" + reason + '\'' +
                '}';
    }
}
