package cn.com.yusys.yusp.web.server.biz.xdxw0008;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0008.req.Xdxw0008ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0008.resp.Xdxw0008RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0008.req.Xdxw0008DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0008.resp.Xdxw0008DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优农贷黑白名单校验
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0008:优农贷黑白名单校验")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0008Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0008Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0008
     * 交易描述：优农贷黑白名单校验
     *
     * @param xdxw0008ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷黑白名单校验")
    @PostMapping("/xdxw0008")
    //@Idempotent({"xdcaxw0008", "#xdxw0008ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0008RespDto xdxw0008(@Validated @RequestBody Xdxw0008ReqDto xdxw0008ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008ReqDto));
        Xdxw0008DataReqDto xdxw0008DataReqDto = new Xdxw0008DataReqDto();// 请求Data： 优农贷黑白名单校验
        Xdxw0008DataRespDto xdxw0008DataRespDto = new Xdxw0008DataRespDto();// 响应Data：优农贷黑白名单校验
        Xdxw0008RespDto xdxw0008RespDto = new Xdxw0008RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0008.req.Data reqData = null; // 请求Data：优农贷黑白名单校验
        cn.com.yusys.yusp.dto.server.biz.xdxw0008.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0008.resp.Data();// 响应Data：优农贷黑白名单校验
        try {
            // 从 xdxw0008ReqDto获取 reqData
            reqData = xdxw0008ReqDto.getData();
            // 将 reqData 拷贝到xdxw0008DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0008DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataReqDto));
            ResultDto<Xdxw0008DataRespDto> xdxw0008DataResultDto = dscmsBizXwClientService.xdxw0008(xdxw0008DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0008RespDto.setErorcd(Optional.ofNullable(xdxw0008DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0008RespDto.setErortx(Optional.ofNullable(xdxw0008DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0008DataResultDto.getCode())) {
                xdxw0008DataRespDto = xdxw0008DataResultDto.getData();
                xdxw0008RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0008RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0008DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0008DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, e.getMessage());
            xdxw0008RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0008RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0008RespDto.setDatasq(servsq);

        xdxw0008RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0008.key, DscmsEnum.TRADE_CODE_XDXW0008.value, JSON.toJSONString(xdxw0008RespDto));
        return xdxw0008RespDto;
    }
}
