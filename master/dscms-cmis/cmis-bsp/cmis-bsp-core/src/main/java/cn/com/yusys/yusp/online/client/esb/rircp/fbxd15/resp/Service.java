package cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp;

import java.math.BigDecimal;

/**
 * 响应Service：还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码
    private String erortx; // 响应信息
    private cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp.List list;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", list=" + list +
                '}';
    }
}
