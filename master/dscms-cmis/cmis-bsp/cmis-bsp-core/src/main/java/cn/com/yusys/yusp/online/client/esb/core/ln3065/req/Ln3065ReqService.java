package cn.com.yusys.yusp.online.client.esb.core.ln3065.req;

/**
 * 请求Service：资产转让资金划转
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3065ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3065ReqService{" +
                "service=" + service +
                '}';
    }
}
