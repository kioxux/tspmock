package cn.com.yusys.yusp.online.client.esb.doc.doc006.resp;

/**
 * 响应Service：申请出库
 *
 * @author chenyong
 * @version 1.0
 */
public class Doc006RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Doc006RespService{" +
                "service=" + service +
                '}';
    }
}
