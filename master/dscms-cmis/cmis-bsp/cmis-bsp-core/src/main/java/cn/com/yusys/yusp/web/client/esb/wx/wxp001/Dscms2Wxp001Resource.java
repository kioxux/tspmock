package cn.com.yusys.yusp.web.client.esb.wx.wxp001;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;

import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.req.Wxp001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.resp.Wxp001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;

import cn.com.yusys.yusp.online.client.esb.wx.wxp001.req.Wxp001ReqService;
import cn.com.yusys.yusp.online.client.esb.wx.wxp001.resp.Wxp001RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用苏州征信系统的接口
 **/
@Api(tags = "BSP封装调用苏州征信系统的接口(wxp001)")
@RestController
@RequestMapping("/api/dscms2wx")
public class Dscms2Wxp001Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Wxp001Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 信贷将审批结果推送给移动端（处理码wxp001）
     *
     * @param wxp001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("wxp001:信贷将审批结果推送给移动端")
    @PostMapping("/wxp001")
    protected @ResponseBody
    ResultDto<Wxp001RespDto> wxp001(@Validated @RequestBody Wxp001ReqDto wxp001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP001.key, EsbEnum.TRADE_CODE_WXP001.value, JSON.toJSONString(wxp001ReqDto));
        cn.com.yusys.yusp.online.client.esb.wx.wxp001.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.wx.wxp001.req.Service();
        cn.com.yusys.yusp.online.client.esb.wx.wxp001.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.wx.wxp001.resp.Service();
        Wxp001ReqService wxp001ReqService = new Wxp001ReqService();
        Wxp001RespService wxp001RespService = new Wxp001RespService();
        Wxp001RespDto wxp001RespDto = new Wxp001RespDto();
        ResultDto<Wxp001RespDto> wxp001ResultDto = new ResultDto<Wxp001RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Wxp001ReqDto转换成reqService
            BeanUtils.copyProperties(wxp001ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_WXP001.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_SZZX.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_SZZX.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            wxp001ReqService.setService(reqService);
            // 将wxp001ReqService转换成wxp001ReqServiceMap
            Map wxp001ReqServiceMap = beanMapUtil.beanToMap(wxp001ReqService);
            context.put("tradeDataMap", wxp001ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP001.key, EsbEnum.TRADE_CODE_WXP001.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_WXP001.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP001.key, EsbEnum.TRADE_CODE_WXP001.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP001.key, EsbEnum.TRADE_CODE_WXP001.value);
            wxp001RespService = beanMapUtil.mapToBean(tradeDataMap, Wxp001RespService.class, Wxp001RespService.class);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP001.key, EsbEnum.TRADE_CODE_WXP001.value);
            respService = wxp001RespService.getService();
            //  将Wxp001RespDto封装到ResultDto<Wxp001RespDto>
            wxp001ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            wxp001ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Wxp001RespDto
                BeanUtils.copyProperties(respService, wxp001RespDto);
                wxp001ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                wxp001ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                wxp001ResultDto.setCode(EpbEnum.EPB099999.key);
                wxp001ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP001.key, EsbEnum.TRADE_CODE_WXP001.value, e.getMessage());
            wxp001ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            wxp001ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        wxp001ResultDto.setData(wxp001RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_WXP001.key, EsbEnum.TRADE_CODE_WXP001.value, JSON.toJSONString(wxp001ResultDto));
        return wxp001ResultDto;
    }
}
