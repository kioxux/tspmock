package cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrhz;
/**
 * 响应Service：资产证券化信息查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String xieybhao;//协议编号
    private String xieyimch;//协议名称
    private String zhngjigo;//账务机构
    private String bjyskzhh;//其他应收款账号(本金)
    private String lxyskzhh;//其他应收款账号(利息)
    private String fyyskzhh;//其他应收款账号(费用)
    private String fjyskzhh;//其他应收款账号(罚金)
    private String bjysfzhh;//其他应付款账号(本金)
    private String lxysfzhh;//其他应付款账号(利息)
    private String fyyfkzhh;//其他应付款账号(费用)
    private String fjyfkzhh;//其他应付款账号(罚金)
    private String qyskzhzh;//其他应收款账号子序号(本金)
    private String lxyskzxh;//其他应收款账号子序号(利息)
    private String fyyskzxh;//其他应收款账号子序号(费用)
    private String fjyskzxh;//其他应收款账号子序号(罚金)
    private String bjysfzxh;//其他应付款账号子序号(本金)
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    private String fyyfkzxh;//其他应付款账号子序号(费用)
    private String fjyfkzxh;//其他应付款账号子序号(罚金)
    private String sunyrzzh;//损益入账账号
    private String syrzzhxh;//损益入账账号子序号
    private String syrzzhmc;//损益入账账户名称
    private String dfkbjzhh;//待付款本金账号
    private String dfkbjzxh;//待付款本金账号子序号
    private String dfklxzhh;//待付款利息账号
    private String dfklxzxh;//待付款利息账号子序号
    private String bjghrzzh;//本金归还入账账号
    private String bjghrzxh;//本金归还入账账号子序号
    private String lxghrzzh;//利息归还入账账号
    private String lxghrzxh;//利息归还入账账号子序号
    private String yywsrzhh;//营业外收入账号
    private String yywsrzxh;//营业外收入账号子序号
    private String yywzczhh;//营业外支出账号
    private String yywzczxh;//营业外支出账号子序号
    private String dailihbz;//代理行标志

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getBjyskzhh() {
        return bjyskzhh;
    }

    public void setBjyskzhh(String bjyskzhh) {
        this.bjyskzhh = bjyskzhh;
    }

    public String getLxyskzhh() {
        return lxyskzhh;
    }

    public void setLxyskzhh(String lxyskzhh) {
        this.lxyskzhh = lxyskzhh;
    }

    public String getFyyskzhh() {
        return fyyskzhh;
    }

    public void setFyyskzhh(String fyyskzhh) {
        this.fyyskzhh = fyyskzhh;
    }

    public String getFjyskzhh() {
        return fjyskzhh;
    }

    public void setFjyskzhh(String fjyskzhh) {
        this.fjyskzhh = fjyskzhh;
    }

    public String getBjysfzhh() {
        return bjysfzhh;
    }

    public void setBjysfzhh(String bjysfzhh) {
        this.bjysfzhh = bjysfzhh;
    }

    public String getLxysfzhh() {
        return lxysfzhh;
    }

    public void setLxysfzhh(String lxysfzhh) {
        this.lxysfzhh = lxysfzhh;
    }

    public String getFyyfkzhh() {
        return fyyfkzhh;
    }

    public void setFyyfkzhh(String fyyfkzhh) {
        this.fyyfkzhh = fyyfkzhh;
    }

    public String getFjyfkzhh() {
        return fjyfkzhh;
    }

    public void setFjyfkzhh(String fjyfkzhh) {
        this.fjyfkzhh = fjyfkzhh;
    }

    public String getQyskzhzh() {
        return qyskzhzh;
    }

    public void setQyskzhzh(String qyskzhzh) {
        this.qyskzhzh = qyskzhzh;
    }

    public String getLxyskzxh() {
        return lxyskzxh;
    }

    public void setLxyskzxh(String lxyskzxh) {
        this.lxyskzxh = lxyskzxh;
    }

    public String getFyyskzxh() {
        return fyyskzxh;
    }

    public void setFyyskzxh(String fyyskzxh) {
        this.fyyskzxh = fyyskzxh;
    }

    public String getFjyskzxh() {
        return fjyskzxh;
    }

    public void setFjyskzxh(String fjyskzxh) {
        this.fjyskzxh = fjyskzxh;
    }

    public String getBjysfzxh() {
        return bjysfzxh;
    }

    public void setBjysfzxh(String bjysfzxh) {
        this.bjysfzxh = bjysfzxh;
    }

    public String getQtyfzhzx() {
        return qtyfzhzx;
    }

    public void setQtyfzhzx(String qtyfzhzx) {
        this.qtyfzhzx = qtyfzhzx;
    }

    public String getFyyfkzxh() {
        return fyyfkzxh;
    }

    public void setFyyfkzxh(String fyyfkzxh) {
        this.fyyfkzxh = fyyfkzxh;
    }

    public String getFjyfkzxh() {
        return fjyfkzxh;
    }

    public void setFjyfkzxh(String fjyfkzxh) {
        this.fjyfkzxh = fjyfkzxh;
    }

    public String getSunyrzzh() {
        return sunyrzzh;
    }

    public void setSunyrzzh(String sunyrzzh) {
        this.sunyrzzh = sunyrzzh;
    }

    public String getSyrzzhxh() {
        return syrzzhxh;
    }

    public void setSyrzzhxh(String syrzzhxh) {
        this.syrzzhxh = syrzzhxh;
    }

    public String getSyrzzhmc() {
        return syrzzhmc;
    }

    public void setSyrzzhmc(String syrzzhmc) {
        this.syrzzhmc = syrzzhmc;
    }

    public String getDfkbjzhh() {
        return dfkbjzhh;
    }

    public void setDfkbjzhh(String dfkbjzhh) {
        this.dfkbjzhh = dfkbjzhh;
    }

    public String getDfkbjzxh() {
        return dfkbjzxh;
    }

    public void setDfkbjzxh(String dfkbjzxh) {
        this.dfkbjzxh = dfkbjzxh;
    }

    public String getDfklxzhh() {
        return dfklxzhh;
    }

    public void setDfklxzhh(String dfklxzhh) {
        this.dfklxzhh = dfklxzhh;
    }

    public String getDfklxzxh() {
        return dfklxzxh;
    }

    public void setDfklxzxh(String dfklxzxh) {
        this.dfklxzxh = dfklxzxh;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getYywsrzhh() {
        return yywsrzhh;
    }

    public void setYywsrzhh(String yywsrzhh) {
        this.yywsrzhh = yywsrzhh;
    }

    public String getYywsrzxh() {
        return yywsrzxh;
    }

    public void setYywsrzxh(String yywsrzxh) {
        this.yywsrzxh = yywsrzxh;
    }

    public String getYywzczhh() {
        return yywzczhh;
    }

    public void setYywzczhh(String yywzczhh) {
        this.yywzczhh = yywzczhh;
    }

    public String getYywzczxh() {
        return yywzczxh;
    }

    public void setYywzczxh(String yywzczxh) {
        this.yywzczxh = yywzczxh;
    }

    public String getDailihbz() {
        return dailihbz;
    }

    public void setDailihbz(String dailihbz) {
        this.dailihbz = dailihbz;
    }

    @Override
    public String toString() {
        return "Record{" +
                "xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", bjyskzhh='" + bjyskzhh + '\'' +
                ", lxyskzhh='" + lxyskzhh + '\'' +
                ", fyyskzhh='" + fyyskzhh + '\'' +
                ", fjyskzhh='" + fjyskzhh + '\'' +
                ", bjysfzhh='" + bjysfzhh + '\'' +
                ", lxysfzhh='" + lxysfzhh + '\'' +
                ", fyyfkzhh='" + fyyfkzhh + '\'' +
                ", fjyfkzhh='" + fjyfkzhh + '\'' +
                ", qyskzhzh='" + qyskzhzh + '\'' +
                ", lxyskzxh='" + lxyskzxh + '\'' +
                ", fyyskzxh='" + fyyskzxh + '\'' +
                ", fjyskzxh='" + fjyskzxh + '\'' +
                ", bjysfzxh='" + bjysfzxh + '\'' +
                ", qtyfzhzx='" + qtyfzhzx + '\'' +
                ", fyyfkzxh='" + fyyfkzxh + '\'' +
                ", fjyfkzxh='" + fjyfkzxh + '\'' +
                ", sunyrzzh='" + sunyrzzh + '\'' +
                ", syrzzhxh='" + syrzzhxh + '\'' +
                ", syrzzhmc='" + syrzzhmc + '\'' +
                ", dfkbjzhh='" + dfkbjzhh + '\'' +
                ", dfkbjzxh='" + dfkbjzxh + '\'' +
                ", dfklxzhh='" + dfklxzhh + '\'' +
                ", dfklxzxh='" + dfklxzxh + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", bjghrzxh='" + bjghrzxh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", lxghrzxh='" + lxghrzxh + '\'' +
                ", yywsrzhh='" + yywsrzhh + '\'' +
                ", yywsrzxh='" + yywsrzxh + '\'' +
                ", yywzczhh='" + yywzczhh + '\'' +
                ", yywzczxh='" + yywzczxh + '\'' +
                ", dailihbz='" + dailihbz + '\'' +
                '}';
    }
}
