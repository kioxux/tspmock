package cn.com.yusys.yusp.web.client.esb.core.co3222;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.co3222.Co3222ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3222.Co3222RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.co3222.req.Co3222ReqService;
import cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Co3222RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(co3222)")
@RestController
@RequestMapping("/api/dscms2coreco")
public class Dscms2Co3222Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Co3222Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 抵质押物单笔查询（处理码co3222）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("co3222:抵质押物单笔查询")
    @PostMapping("/co3222")
    protected @ResponseBody
    ResultDto<Co3222RespDto> co3222(@Validated @RequestBody Co3222ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3222.key, EsbEnum.TRADE_CODE_CO3222.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.co3222.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.co3222.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Service();
        Co3222ReqService co3222ReqService = new Co3222ReqService();
        Co3222RespService co3222RespService = new Co3222RespService();
        Co3222RespDto co3222RespDto = new Co3222RespDto();
        ResultDto<Co3222RespDto> co3222ResultDto = new ResultDto<Co3222RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Co3222ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CO3222.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            co3222ReqService.setService(reqService);
            // 将co3222ReqService转换成co3222ReqServiceMap
            Map co3222ReqServiceMap = beanMapUtil.beanToMap(co3222ReqService);
            context.put("tradeDataMap", co3222ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3222.key, EsbEnum.TRADE_CODE_CO3222.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CO3222.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3222.key, EsbEnum.TRADE_CODE_CO3222.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            co3222RespService = beanMapUtil.mapToBean(tradeDataMap, Co3222RespService.class, Co3222RespService.class);
            respService = co3222RespService.getService();

            //  将Co3222RespDto封装到ResultDto<Co3222RespDto>
            co3222ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            co3222ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Co3222RespDto
                BeanUtils.copyProperties(respService, co3222RespDto);
                // 循环相关的判断
                cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum1 co3222RespLstnum1Service = Optional.ofNullable(respService.getLstnum1()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum1());
                cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum2 co3222RespLstnum2Service = Optional.ofNullable(respService.getLstnum2()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum2());
                respService.setLstnum1(co3222RespLstnum1Service);
                respService.setLstnum2(co3222RespLstnum2Service);
                if (CollectionUtils.nonEmpty(co3222RespLstnum1Service.getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum1Record> lstnum1Records = Optional.ofNullable(co3222RespLstnum1Service.getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum1> lstnum1s = lstnum1Records.parallelStream().map(lstnum1Record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum1 lstnum1 = new cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum1();
                        BeanUtils.copyProperties(lstnum1Record, lstnum1);
                        return lstnum1;
                    }).collect(Collectors.toList());
                    co3222RespDto.setLstnum1(lstnum1s);
                }
                if (CollectionUtils.nonEmpty(co3222RespLstnum2Service.getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum2Record> lstnum2Records = Optional.ofNullable(co3222RespLstnum2Service.getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum2> lstnum2s = lstnum2Records.parallelStream().map(lstnum2Record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum2 lstnum2 = new cn.com.yusys.yusp.dto.client.esb.core.co3222.Lstnum2();
                        BeanUtils.copyProperties(lstnum2Record, lstnum2);
                        return lstnum2;
                    }).collect(Collectors.toList());
                    co3222RespDto.setLstnum2(lstnum2s);
                }
                co3222ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                co3222ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                co3222ResultDto.setCode(EpbEnum.EPB099999.key);
                co3222ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3222.key, EsbEnum.TRADE_CODE_CO3222.value, e.getMessage());
            co3222ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            co3222ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        co3222ResultDto.setData(co3222RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3222.key, EsbEnum.TRADE_CODE_CO3222.value, JSON.toJSONString(co3222ResultDto));
        return co3222ResultDto;
    }


}
