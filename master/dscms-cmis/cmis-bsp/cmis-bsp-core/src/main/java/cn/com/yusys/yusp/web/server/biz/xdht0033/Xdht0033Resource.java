package cn.com.yusys.yusp.web.server.biz.xdht0033;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0033.req.Xdht0033ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0033.resp.Xdht0033RespDto;
import cn.com.yusys.yusp.dto.server.xdht0033.req.Xdht0033DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0033.resp.Xdht0033DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
 *
 * @author leehuang
 * @version 1.0
 */
@Api(tags = "XDHT0033:根据核心客户号查询小贷是否具有客户调查合同并返回合同金额")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0033Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0033Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0033
     * 交易描述：根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
     *
     * @param xdht0033ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据核心客户号查询小贷是否具有客户调查合同并返回合同金额")
    @PostMapping("/xdht0033")
    //@Idempotent({"xdcaht0033", "#xdht0033ReqDto.datasq"})
    protected @ResponseBody
    Xdht0033RespDto xdht0033(@Validated @RequestBody Xdht0033ReqDto xdht0033ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033ReqDto));
        Xdht0033DataReqDto xdht0033DataReqDto = new Xdht0033DataReqDto();// 请求Data： 根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
        Xdht0033DataRespDto xdht0033DataRespDto = new Xdht0033DataRespDto();// 响应Data：根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
        Xdht0033RespDto xdht0033RespDto = new Xdht0033RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0033.req.Data reqData = null; // 请求Data：根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
        cn.com.yusys.yusp.dto.server.biz.xdht0033.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0033.resp.Data();// 响应Data：根据核心客户号查询小贷是否具有客户调查合同并返回合同金额
        try {
            // 从 xdht0033ReqDto获取 reqData
            reqData = xdht0033ReqDto.getData();
            // 将 reqData 拷贝到xdht0033DataReqDto
            BeanUtils.copyProperties(reqData, xdht0033DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033DataReqDto));
            ResultDto<Xdht0033DataRespDto> xdht0033DataResultDto = dscmsBizHtClientService.xdht0033(xdht0033DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0033RespDto.setErorcd(Optional.ofNullable(xdht0033DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0033RespDto.setErortx(Optional.ofNullable(xdht0033DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0033DataResultDto.getCode())) {
                xdht0033DataRespDto = xdht0033DataResultDto.getData();
                xdht0033RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0033RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0033DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0033DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, e.getMessage());
            xdht0033RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0033RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0033RespDto.setDatasq(servsq);

        xdht0033RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0033.key, DscmsEnum.TRADE_CODE_XDHT0033.value, JSON.toJSONString(xdht0033RespDto));
        return xdht0033RespDto;
    }
}