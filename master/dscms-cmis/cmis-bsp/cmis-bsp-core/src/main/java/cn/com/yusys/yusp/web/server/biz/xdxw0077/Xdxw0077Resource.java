package cn.com.yusys.yusp.web.server.biz.xdxw0077;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0077.req.Xdxw0077ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0077.resp.Xdxw0077RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0077.req.Xdxw0077DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0077.resp.Xdxw0077DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:渠道端查询我的贷款（流程监控）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0077:渠道端查询我的贷款（流程监控）")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0077Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0077Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0077
     * 交易描述：渠道端查询我的贷款（流程监控）
     *
     * @param xdxw0077ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("渠道端查询我的贷款（流程监控）")
    @PostMapping("/xdxw0077")
    @Idempotent({"xdcaxw0077", "#xdxw0077ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0077RespDto xdxw0077(@Validated @RequestBody Xdxw0077ReqDto xdxw0077ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077ReqDto));
        Xdxw0077DataReqDto xdxw0077DataReqDto = new Xdxw0077DataReqDto();// 请求Data： 渠道端查询我的贷款（流程监控）
        Xdxw0077DataRespDto xdxw0077DataRespDto = new Xdxw0077DataRespDto();// 响应Data：渠道端查询我的贷款（流程监控）
        Xdxw0077RespDto xdxw0077RespDto = new Xdxw0077RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0077.req.Data reqData = null; // 请求Data：渠道端查询我的贷款（流程监控）
        cn.com.yusys.yusp.dto.server.biz.xdxw0077.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0077.resp.Data();// 响应Data：渠道端查询我的贷款（流程监控）

        try {
            // 从 xdxw0077ReqDto获取 reqData
            reqData = xdxw0077ReqDto.getData();
            // 将 reqData 拷贝到xdxw0077DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0077DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataReqDto));
            ResultDto<Xdxw0077DataRespDto> xdxw0077DataResultDto = dscmsBizXwClientService.xdxw0077(xdxw0077DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0077RespDto.setErorcd(Optional.ofNullable(xdxw0077DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0077RespDto.setErortx(Optional.ofNullable(xdxw0077DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0077DataResultDto.getCode())) {
                xdxw0077DataRespDto = xdxw0077DataResultDto.getData();
                xdxw0077RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0077RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0077DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0077DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, e.getMessage());
            xdxw0077RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0077RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0077RespDto.setDatasq(servsq);

        xdxw0077RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0077.key, DscmsEnum.TRADE_CODE_XDXW0077.value, JSON.toJSONString(xdxw0077RespDto));
        return xdxw0077RespDto;
    }
}
