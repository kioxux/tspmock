package cn.com.yusys.yusp.web.client.esb.core.ln3005;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3005.Ln3005ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3005.Ln3005RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3005.req.Ln3005ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.*;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:贷款产品查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3005)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Ln3005Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3005
     * 交易描述：贷款产品查询
     *
     * @param ln3005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3005:贷款产品查询")
    @PostMapping("/ln3005")
    protected @ResponseBody
    ResultDto<Ln3005RespDto> ln3005(@Validated @RequestBody Ln3005ReqDto ln3005ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3005.key, EsbEnum.TRADE_CODE_LN3005.value, JSON.toJSONString(ln3005ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3005.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3005.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Service();
        Ln3005ReqService ln3005ReqService = new Ln3005ReqService();
        Ln3005RespService ln3005RespService = new Ln3005RespService();
        Ln3005RespDto ln3005RespDto = new Ln3005RespDto();
        ResultDto<Ln3005RespDto> ln3005ResultDto = new ResultDto<Ln3005RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3005ReqDto转换成reqService
            BeanUtils.copyProperties(ln3005ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3005.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ln3005ReqService.setService(reqService);
            // 将ln3005ReqService转换成ln3005ReqServiceMap
            Map ln3005ReqServiceMap = beanMapUtil.beanToMap(ln3005ReqService);
            context.put("tradeDataMap", ln3005ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3005.key, EsbEnum.TRADE_CODE_LN3005.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3005.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3005.key, EsbEnum.TRADE_CODE_LN3005.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3005RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3005RespService.class, Ln3005RespService.class);
            respService = ln3005RespService.getService();

            ln3005ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            ln3005ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3005RespDto
                BeanUtils.copyProperties(respService, ln3005RespDto);
                cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpfwdj_ARRAY ln3005RespLstcpfwdj_ARRAY = Optional.ofNullable(respService.getLstcpfwdj_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpfwdj_ARRAY());
                respService.setLstcpfwdj_ARRAY(ln3005RespLstcpfwdj_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstcpfwdj_ARRAY().getRecord())) {
                    Lstcpfwdj_ARRAY lstcpfwdj = Optional.ofNullable(respService.getLstcpfwdj_ARRAY()).orElse(new Lstcpfwdj_ARRAY());
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpfwdj.Record> lstcpfwdjRecord = Optional.ofNullable(lstcpfwdj.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpfwdj> lstcpfwdjList = lstcpfwdjRecord.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpfwdj temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpfwdj();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3005RespDto.setLstcpfwdj(lstcpfwdjList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpgndx_ARRAY ln3005RespLstcpgndx_ARRAY =
                        Optional.ofNullable(respService.getLstcpgndx_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpgndx_ARRAY());
                respService.setLstcpgndx_ARRAY(ln3005RespLstcpgndx_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstcpgndx_ARRAY().getRecord())) {
                    Lstcpgndx_ARRAY lstcpgndx = Optional.ofNullable(respService.getLstcpgndx_ARRAY()).orElse(new Lstcpgndx_ARRAY());
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpgndx.Record> lstcpgndxRecord = Optional.ofNullable(lstcpgndx.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpgndx> lstcpgndxList = lstcpgndxRecord.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpgndx temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpgndx();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3005RespDto.setLstcpgndx(lstcpgndxList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcphkfs_ARRAY ln3005RespLstcphkfs_ARRAY =
                        Optional.ofNullable(respService.getLstcphkfs_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcphkfs_ARRAY());
                respService.setLstcphkfs_ARRAY(ln3005RespLstcphkfs_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstcphkfs_ARRAY().getRecord())) {
                    Lstcphkfs_ARRAY lstcphkfs = Optional.ofNullable(respService.getLstcphkfs_ARRAY()).orElse(new Lstcphkfs_ARRAY());
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcphkfs.Record> lstcphkfsRecord = Optional.ofNullable(lstcphkfs.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcphkfs> lstcphkfsList = lstcphkfsRecord.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcphkfs temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcphkfs();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3005RespDto.setLstcphkfs(lstcphkfsList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpkjlb_ARRAY ln3005RespLstcpkjlb_ARRAY =
                        Optional.ofNullable(respService.getLstcpkjlb_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpkjlb_ARRAY());
                respService.setLstcpkjlb_ARRAY(ln3005RespLstcpkjlb_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstcpkjlb_ARRAY().getRecord())) {
                    Lstcpkjlb_ARRAY lstcpkjlb = Optional.ofNullable(respService.getLstcpkjlb_ARRAY()).orElse(new Lstcpkjlb_ARRAY());
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpkjlb.Record> lstcpkjlbRecord = Optional.ofNullable(lstcpkjlb.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpkjlb> lstcpkjlbList = lstcpkjlbRecord.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpkjlb temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpkjlb();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3005RespDto.setLstcpkjlb(lstcpkjlbList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpqskz_ARRAY ln3005RespLstcpqskz_ARRAY =
                        Optional.ofNullable(respService.getLstcpqskz_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpqskz_ARRAY());
                respService.setLstcpqskz_ARRAY(ln3005RespLstcpqskz_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstcpqskz_ARRAY().getRecord())) {
                    Lstcpqskz_ARRAY lstcpqskz = Optional.ofNullable(respService.getLstcpqskz_ARRAY()).orElse(new Lstcpqskz_ARRAY());
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpqskz.Record> lstcpqskzRecord = Optional.ofNullable(lstcpqskz.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpqskz> lstcpqskzList = lstcpqskzRecord.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpqskz temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpqskz();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3005RespDto.setLstcpqskz(lstcpqskzList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpsfsj_ARRAY ln3005RespLstcpsfsj_ARRAY =
                        Optional.ofNullable(respService.getLstcpsfsj_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpsfsj_ARRAY());
                respService.setLstcpsfsj_ARRAY(ln3005RespLstcpsfsj_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstcpsfsj_ARRAY().getRecord())) {
                    Lstcpsfsj_ARRAY lstcpsfsj = Optional.ofNullable(respService.getLstcpsfsj_ARRAY()).orElse(new Lstcpsfsj_ARRAY());
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsfsj.Record> lstcpsfsjRecord = Optional.ofNullable(lstcpsfsj.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpsfsj> lstcpsfsjList = lstcpsfsjRecord.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpsfsj temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpsfsj();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3005RespDto.setLstcpsfsj(lstcpsfsjList);
                }

                cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpsyfw_ARRAY ln3005RespLstcpsyfw_ARRAY =
                        Optional.ofNullable(respService.getLstcpsyfw_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.Lstcpsyfw_ARRAY());
                respService.setLstcpsyfw_ARRAY(ln3005RespLstcpsyfw_ARRAY);
                if (CollectionUtils.nonEmpty(respService.getLstcpsyfw_ARRAY().getRecord())) {
                    Lstcpsyfw_ARRAY lstcpsyfw = Optional.ofNullable(respService.getLstcpsyfw_ARRAY()).orElse(new Lstcpsyfw_ARRAY());
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsyfw.Record> lstcpsyfwRecord = Optional.ofNullable(lstcpsyfw.getRecord()).orElse(new ArrayList<>());
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpsyfw> lstcpsyfwList = lstcpsyfwRecord.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpsyfw temp = new cn.com.yusys.yusp.dto.client.esb.core.ln3005.Lstcpsyfw();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    ln3005RespDto.setLstcpsyfw(lstcpsyfwList);
                }

                ln3005ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3005ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3005ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3005ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3005.key, EsbEnum.TRADE_CODE_LN3005.value, e.getMessage());
            ln3005ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3005ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3005ResultDto.setData(ln3005RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3005.key, EsbEnum.TRADE_CODE_LN3005.value, JSON.toJSONString(ln3005ResultDto));
        return ln3005ResultDto;
    }
}
