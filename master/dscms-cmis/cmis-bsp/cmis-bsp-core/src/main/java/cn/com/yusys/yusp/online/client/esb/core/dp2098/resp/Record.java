package cn.com.yusys.yusp.online.client.esb.core.dp2098.resp;

public class Record {
    private String kehuzhao;//客户账号
    private String muhuxuho;//母户序号
    private String muhuminc;//母户名称
    private String zhbuxuho;//账簿层子序号
    private String zhbuminc;//账簿层名称
    private String daiqsxuh;//待清算子序号
    private String daiqsmin;//待清算账户名
    private String chaohubz;//账户钞汇标志
    private String huobdaih;//货币代号
    private String kaihjigo;//开户机构
    private String zhfutojn;//支付条件

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getMuhuxuho() {
        return muhuxuho;
    }

    public void setMuhuxuho(String muhuxuho) {
        this.muhuxuho = muhuxuho;
    }

    public String getMuhuminc() {
        return muhuminc;
    }

    public void setMuhuminc(String muhuminc) {
        this.muhuminc = muhuminc;
    }

    public String getZhbuxuho() {
        return zhbuxuho;
    }

    public void setZhbuxuho(String zhbuxuho) {
        this.zhbuxuho = zhbuxuho;
    }

    public String getZhbuminc() {
        return zhbuminc;
    }

    public void setZhbuminc(String zhbuminc) {
        this.zhbuminc = zhbuminc;
    }

    public String getDaiqsxuh() {
        return daiqsxuh;
    }

    public void setDaiqsxuh(String daiqsxuh) {
        this.daiqsxuh = daiqsxuh;
    }

    public String getDaiqsmin() {
        return daiqsmin;
    }

    public void setDaiqsmin(String daiqsmin) {
        this.daiqsmin = daiqsmin;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getKaihjigo() {
        return kaihjigo;
    }

    public void setKaihjigo(String kaihjigo) {
        this.kaihjigo = kaihjigo;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    @Override
    public String toString() {
        return "Record{" +
                "kehuzhao='" + kehuzhao + '\'' +
                ", muhuxuho='" + muhuxuho + '\'' +
                ", muhuminc='" + muhuminc + '\'' +
                ", zhbuxuho='" + zhbuxuho + '\'' +
                ", zhbuminc='" + zhbuminc + '\'' +
                ", daiqsxuh='" + daiqsxuh + '\'' +
                ", daiqsmin='" + daiqsmin + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", kaihjigo='" + kaihjigo + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                '}';
    }
}
