package cn.com.yusys.yusp.online.client.esb.core.mbt951.resp;

/**
 * 响应Service：批量文件处理申请
 *
 * @author chenyong
 * @version 1.0
 */
public class Mbt951RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Mbt951RespService{" +
                "service=" + service +
                '}';
    }
}

