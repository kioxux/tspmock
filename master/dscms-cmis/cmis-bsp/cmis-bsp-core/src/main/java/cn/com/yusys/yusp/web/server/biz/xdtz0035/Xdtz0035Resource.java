package cn.com.yusys.yusp.web.server.biz.xdtz0035;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0035.req.Xdtz0035ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0035.resp.Xdtz0035RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0035.req.Xdtz0035DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0035.resp.Xdtz0035DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0035:根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0035Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0035Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0035
     * 交易描述：根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
     *
     * @param xdtz0035ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录")
    @PostMapping("/xdtz0035")
    //@Idempotent({"xdcatz0035", "#xdtz0035ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0035RespDto xdtz0035(@Validated @RequestBody Xdtz0035ReqDto xdtz0035ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035ReqDto));
        Xdtz0035DataReqDto xdtz0035DataReqDto = new Xdtz0035DataReqDto();// 请求Data： 根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
        Xdtz0035DataRespDto xdtz0035DataRespDto = new Xdtz0035DataRespDto();// 响应Data：根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
        Xdtz0035RespDto xdtz0035RespDto = new Xdtz0035RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0035.req.Data reqData = null; // 请求Data：根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
        cn.com.yusys.yusp.dto.server.biz.xdtz0035.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0035.resp.Data();// 响应Data：根据借据编号前往信贷查询当房贷未结清时，近24个月内有无逾期记录
        try {
            // 从 xdtz0035ReqDto获取 reqData
            reqData = xdtz0035ReqDto.getData();
            // 将 reqData 拷贝到xdtz0035DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0035DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataReqDto));
            ResultDto<Xdtz0035DataRespDto> xdtz0035DataResultDto = dscmsBizTzClientService.xdtz0035(xdtz0035DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0035RespDto.setErorcd(Optional.ofNullable(xdtz0035DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0035RespDto.setErortx(Optional.ofNullable(xdtz0035DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0035DataResultDto.getCode())) {
                xdtz0035DataRespDto = xdtz0035DataResultDto.getData();
                xdtz0035RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0035RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0035DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0035DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, e.getMessage());
            xdtz0035RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0035RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0035RespDto.setDatasq(servsq);

        xdtz0035RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0035.key, DscmsEnum.TRADE_CODE_XDTZ0035.value, JSON.toJSONString(xdtz0035RespDto));
        return xdtz0035RespDto;
    }
}
