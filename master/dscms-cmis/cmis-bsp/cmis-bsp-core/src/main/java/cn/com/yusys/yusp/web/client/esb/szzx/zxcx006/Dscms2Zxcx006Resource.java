package cn.com.yusys.yusp.web.client.esb.szzx.zxcx006;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.req.Zxcx006ReqService;
import cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.resp.Zxcx006RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用苏州征信系统的接口
 **/
@Api(tags = "BSP封装调用苏州征信系统的接口(zxcx006)")
@RestController
@RequestMapping("/api/dscms2szzx")
public class Dscms2Zxcx006Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Zxcx006Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * ESB信贷查询地方征信接口（处理码zxcx006）
     *
     * @param zxcx006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("zxcx006:ESB信贷查询地方征信接口")
    @PostMapping("/zxcx006")
    protected @ResponseBody
    ResultDto<Zxcx006RespDto> zxcx006(@Validated @RequestBody Zxcx006ReqDto zxcx006ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZXCX006.key, EsbEnum.TRADE_CODE_ZXCX006.value, JSON.toJSONString(zxcx006ReqDto));
        cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.req.Service();
        cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.resp.Service();
        Zxcx006ReqService zxcx006ReqService = new Zxcx006ReqService();
        Zxcx006RespService zxcx006RespService = new Zxcx006RespService();
        Zxcx006RespDto zxcx006RespDto = new Zxcx006RespDto();
        ResultDto<Zxcx006RespDto> zxcx006ResultDto = new ResultDto<Zxcx006RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Zxcx006ReqDto转换成reqService
            BeanUtils.copyProperties(zxcx006ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_ZXCX006.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_SZZX.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_SZZX.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            zxcx006ReqService.setService(reqService);
            // 将zxcx006ReqService转换成zxcx006ReqServiceMap
            Map zxcx006ReqServiceMap = beanMapUtil.beanToMap(zxcx006ReqService);
            context.put("tradeDataMap", zxcx006ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_ZXCX006.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZXCX006.key, EsbEnum.TRADE_CODE_ZXCX006.value);
            zxcx006RespService = beanMapUtil.mapToBean(tradeDataMap, Zxcx006RespService.class, Zxcx006RespService.class);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZXCX006.key, EsbEnum.TRADE_CODE_ZXCX006.value);
            respService = zxcx006RespService.getService();
            //  将respService转换成Zxcx006RespDto
            BeanUtils.copyProperties(respService, zxcx006RespDto);
            //  将Zxcx006RespDto封装到ResultDto<Zxcx006RespDto>
            zxcx006ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            zxcx006ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G10501RespDto
                BeanUtils.copyProperties(respService, zxcx006RespDto);
                zxcx006ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                zxcx006ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                zxcx006ResultDto.setCode(EpbEnum.EPB099999.key);
                zxcx006ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZXCX006.key, EsbEnum.TRADE_CODE_ZXCX006.value, e.getMessage());
            zxcx006ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            zxcx006ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        zxcx006ResultDto.setData(zxcx006RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZXCX006.key, EsbEnum.TRADE_CODE_ZXCX006.value, JSON.toJSONString(zxcx006ResultDto));
        return zxcx006ResultDto;
    }
}
