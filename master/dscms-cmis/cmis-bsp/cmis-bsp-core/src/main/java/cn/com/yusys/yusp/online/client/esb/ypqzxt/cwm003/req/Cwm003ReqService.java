package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.req;

/**
 * 请求Service：押品权证入库
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月12日 下午1:22:06
 */
public class Cwm003ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
