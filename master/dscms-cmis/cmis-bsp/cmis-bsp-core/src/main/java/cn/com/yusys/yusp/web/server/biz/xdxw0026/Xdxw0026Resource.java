package cn.com.yusys.yusp.web.server.biz.xdxw0026;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0026.req.Xdxw0026ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0026.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0026.resp.Xdxw0026RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0026.req.Xdxw0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0026.resp.Xdxw0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:学区信息列表查询（分页）
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0026:学区信息列表查询（分页）")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0026Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0026
     * 交易描述：学区信息列表查询（分页）
     *
     * @param xdxw0026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("学区信息列表查询（分页）")
    @PostMapping("/xdxw0026")
    //@Idempotent({"xdcaxw0026", "#xdxw0026ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0026RespDto xdxw0026(@Validated @RequestBody Xdxw0026ReqDto xdxw0026ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, JSON.toJSONString(xdxw0026ReqDto));
        Xdxw0026DataReqDto xdxw0026DataReqDto = new Xdxw0026DataReqDto();// 请求Data： 学区信息列表查询（分页）
        Xdxw0026DataRespDto xdxw0026DataRespDto = new Xdxw0026DataRespDto();// 响应Data：学区信息列表查询（分页）
        cn.com.yusys.yusp.dto.server.biz.xdxw0026.req.Data reqData = null; // 请求Data：学区信息列表查询（分页）
        cn.com.yusys.yusp.dto.server.biz.xdxw0026.resp.Data respData = new Data();// 响应Data：学区信息列表查询（分页）
		Xdxw0026RespDto xdxw0026RespDto = new Xdxw0026RespDto();
		try {
            // 从 xdxw0026ReqDto获取 reqData
            reqData = xdxw0026ReqDto.getData();
            // 将 reqData 拷贝到xdxw0026DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0026DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, JSON.toJSONString(xdxw0026DataReqDto));
            ResultDto<Xdxw0026DataRespDto> xdxw0026DataResultDto = dscmsBizXwClientService.xdxw0026(xdxw0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, JSON.toJSONString(xdxw0026DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0026RespDto.setErorcd(Optional.ofNullable(xdxw0026DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0026RespDto.setErortx(Optional.ofNullable(xdxw0026DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0026DataResultDto.getCode())) {
                xdxw0026DataRespDto = xdxw0026DataResultDto.getData();
                xdxw0026RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0026RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0026DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0026DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, e.getMessage());
            xdxw0026RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0026RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0026RespDto.setDatasq(servsq);

        xdxw0026RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0026.key, DscmsEnum.TRADE_CODE_XDXW0026.value, JSON.toJSONString(xdxw0026RespDto));
        return xdxw0026RespDto;
    }
}
