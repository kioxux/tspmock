package cn.com.yusys.yusp.web.server.biz.xdqt0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdqt0005.req.Xdqt0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdqt0005.resp.Xdqt0005RespDto;
import cn.com.yusys.yusp.dto.server.xdqt0005.req.Xdqt0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0005.resp.Xdqt0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:贷款申请预约（企业客户）
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0005:贷款申请预约（企业客户）")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0005Resource.class);

    @Autowired
	private DscmsBizQtClientService dscmsBizQtClientService;

    /**
     * 交易码：xdqt0005
     * 交易描述：贷款申请预约（企业客户）
     *
     * @param xdqt0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贷款申请预约（企业客户）")
    @PostMapping("/xdqt0005")
    @Idempotent({"xdcaqt0005", "#xdqt0005ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0005RespDto xdqt0005(@Validated @RequestBody Xdqt0005ReqDto xdqt0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, JSON.toJSONString(xdqt0005ReqDto));
        Xdqt0005DataReqDto xdqt0005DataReqDto = new Xdqt0005DataReqDto();// 请求Data： 贷款申请预约（企业客户）
        Xdqt0005DataRespDto xdqt0005DataRespDto = new Xdqt0005DataRespDto();// 响应Data：贷款申请预约（企业客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0005.req.Data reqData = null; // 请求Data：贷款申请预约（企业客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdqt0005.resp.Data();// 响应Data：贷款申请预约（企业客户）
		Xdqt0005RespDto xdqt0005RespDto = new Xdqt0005RespDto();
		try {
            // 从 xdqt0005ReqDto获取 reqData
            reqData = xdqt0005ReqDto.getData();
            // 将 reqData 拷贝到xdqt0005DataReqDto
            BeanUtils.copyProperties(reqData, xdqt0005DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, JSON.toJSONString(xdqt0005DataReqDto));
            ResultDto<Xdqt0005DataRespDto> xdqt0005DataResultDto = dscmsBizQtClientService.xdqt0005(xdqt0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, JSON.toJSONString(xdqt0005DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0005RespDto.setErorcd(Optional.ofNullable(xdqt0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0005RespDto.setErortx(Optional.ofNullable(xdqt0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0005DataResultDto.getCode())) {
                xdqt0005DataRespDto = xdqt0005DataResultDto.getData();
                xdqt0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, e.getMessage());
            xdqt0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0005RespDto.setDatasq(servsq);

        xdqt0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0005.key, DscmsEnum.TRADE_CODE_XDQT0005.value, JSON.toJSONString(xdqt0005RespDto));
        return xdqt0005RespDto;
    }
}
