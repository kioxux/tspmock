package cn.com.yusys.yusp.web.client.esb.core.ln3106;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3106.Ln3106ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3106.Ln3106RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3106.req.Ln3106ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3106.resp.Ln3106RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3106)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3106Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3106Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3106
     * 交易描述：贷款利率变更明细查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3106:贷款利率变更明细查询")
    @PostMapping("/ln3106")
    protected @ResponseBody
    ResultDto<Ln3106RespDto> ln3106(@Validated @RequestBody Ln3106ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3106.key, EsbEnum.TRADE_CODE_LN3106.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3106.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3106.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3106.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3106.resp.Service();
        Ln3106ReqService ln3106ReqService = new Ln3106ReqService();
        Ln3106RespService ln3106RespService = new Ln3106RespService();
        Ln3106RespDto ln3106RespDto = new Ln3106RespDto();
        ResultDto<Ln3106RespDto> ln3106ResultDto = new ResultDto<Ln3106RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3106ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3106.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3106ReqService.setService(reqService);
            // 将ln3106ReqService转换成ln3106ReqServiceMap
            Map ln3106ReqServiceMap = beanMapUtil.beanToMap(ln3106ReqService);
            context.put("tradeDataMap", ln3106ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3106.key, EsbEnum.TRADE_CODE_LN3106.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3106.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3106.key, EsbEnum.TRADE_CODE_LN3106.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3106RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3106RespService.class, Ln3106RespService.class);
            respService = ln3106RespService.getService();
            //  将respService转换成Ln3106RespDto
            BeanUtils.copyProperties(respService, ln3106RespDto);
            ln3106ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3106ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, ln3106RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3106.resp.Listllbg listllbgRespService = Optional.ofNullable(respService.getListllbg()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3106.resp.Listllbg());
                respService.setListllbg(listllbgRespService);
                if (CollectionUtils.nonEmpty(respService.getListllbg().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3106.resp.Record> listllbgRespRecords = Optional.ofNullable(respService.getListllbg().getRecord()).orElseGet(() -> new ArrayList<>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3106.Listllbg> listllbgs = listllbgRespRecords.parallelStream().map(listllbgRespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3106.Listllbg listllbgDto = new cn.com.yusys.yusp.dto.client.esb.core.ln3106.Listllbg();
                        BeanUtils.copyProperties(listllbgRespRecord, listllbgDto);
                        return listllbgDto;
                    }).collect(Collectors.toList());
                    ln3106RespDto.setListllbg(listllbgs);
                }
                ln3106ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3106ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3106ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3106ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3106.key, EsbEnum.TRADE_CODE_LN3106.value, e.getMessage());
            ln3106ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3106ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3106ResultDto.setData(ln3106RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3106.key, EsbEnum.TRADE_CODE_LN3106.value, JSON.toJSONString(ln3106ResultDto));
        return ln3106ResultDto;
    }
}
