package cn.com.yusys.yusp.online.client.esb.circp.fb1214.req;

/**
 * 请求Service：房产信息修改同步
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1214ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1214ReqService{" +
                "service=" + service +
                '}';
    }
}
