package cn.com.yusys.yusp.web.server.biz.xddb0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0001.req.Data;
import cn.com.yusys.yusp.dto.server.biz.xddb0001.req.Xddb0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0001.resp.Xddb0001RespDto;
import cn.com.yusys.yusp.dto.server.xddb0001.req.Xddb0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0001.resp.Xddb0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询押品是否按揭
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDDB0001:查询押品是否按揭")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0001Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizdbClientService;

    /**
     * 交易码：xddb0001
     * 交易描述：查询押品是否按揭
     *
     * @param xddb0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询押品是否按揭")
    @PostMapping("/xddb0001")
    //@Idempotent({"xddb0001", "#xddb0001ReqDto.datasq"})
    protected @ResponseBody
    Xddb0001RespDto xddb0001(@Validated @RequestBody Xddb0001ReqDto xddb0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001ReqDto));
        Xddb0001DataReqDto xddb0001DataReqDto = new Xddb0001DataReqDto();// 请求Data： 查询押品是否按揭
        Xddb0001DataRespDto xddb0001DataRespDto = new Xddb0001DataRespDto();// 响应Data：查询押品是否按揭
        Xddb0001RespDto xddb0001RespDto = new Xddb0001RespDto();
        Data reqData = null; // 请求Data：查询押品是否按揭
        cn.com.yusys.yusp.dto.server.biz.xddb0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0001.resp.Data();// 响应Data：查询押品是否按揭
        try {
            // 从 xddb0001ReqDto获取 reqData
            reqData = xddb0001ReqDto.getData();
            // 将 reqData 拷贝到xddb0001DataReqDto
            BeanUtils.copyProperties(reqData, xddb0001DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001ReqDto));
            ResultDto<Xddb0001DataRespDto> xddb0001DataResultDto = dscmsBizdbClientService.xddb0001(xddb0001DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0001RespDto.setErorcd(Optional.ofNullable(xddb0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0001RespDto.setErortx(Optional.ofNullable(xddb0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0001DataResultDto.getCode())) {
                xddb0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xddb0001DataRespDto = xddb0001DataResultDto.getData();
                // 将 xddb0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, e.getMessage());
            xddb0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0001RespDto.setDatasq(servsq);

        xddb0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0001.key, DscmsEnum.TRADE_CODE_XDDB0001.value, JSON.toJSONString(xddb0001RespDto));
        return xddb0001RespDto;

    }
}
