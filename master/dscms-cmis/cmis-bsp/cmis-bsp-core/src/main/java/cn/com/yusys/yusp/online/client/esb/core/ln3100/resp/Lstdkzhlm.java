package cn.com.yusys.yusp.online.client.esb.core.ln3100.resp;

/**
 * 响应Service：贷款账户联名信息
 *
 * @author chenyo g
 * @version 1.0
 */
public class Lstdkzhlm {
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String kehugxlx;//客户关系类型

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getKehugxlx() {
        return kehugxlx;
    }

    public void setKehugxlx(String kehugxlx) {
        this.kehugxlx = kehugxlx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "kehugxlx='" + kehugxlx + '\'' +
                '}';
    }
}
