package cn.com.yusys.yusp.online.client.esb.ecif.g11003.req;

/**
 * 请求Service：客户集团信息维护 (new)
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:28
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String txdate;//    交易日期
    private String txtime;//    交易时间
    private String datasq;//    全局流水

    private String grouna;//	varchar2  	30	集团客户名称
    private String custno;//	VARCHAR2	30	客户编号			Y
    private String custln;//	varchar2  	50	客户中证码
    private String custna;//	VARCHAR2	240	客户名称
    private String idtfno;//	VARCHAR2	40	证件号码
    private String oadate;//	varchar2  	10	更新办公地址日期					yyyymmdd
    private String homead;//	VARCHAR2  	100	地址
    private String socino;//	varchar2  	30	社会信用代码
    private String busino;//	varchar2  	50	工商登记注册号
    private String ofaddr;//	varchar2  	255	办公地址行政区划
    private String crpdeg;//	varchar2  	40	集团紧密程度
    private String crpcno;//	varchar2  	250	集团客户情况说明
    private String ctigno;//	varchar2  	20	管户客户经理
    private String citorg;//	varchar2  	20	所属机构
    private String grpsta;//	varchar2  	5	集团客户状态	"0：无 1：有效"
    private String oprtye;//	varchar2  	5	操作类型
    private String inptno;//	varchar2  	30	登记人
    private String inporg;//	varchar2  	30	登记机构
    private cn.com.yusys.yusp.online.client.esb.ecif.g11003.req.List list;// 集团成员信息_ARRAY

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getTxdate() {
        return txdate;
    }

    public void setTxdate(String txdate) {
        this.txdate = txdate;
    }

    public String getTxtime() {
        return txtime;
    }

    public void setTxtime(String txtime) {
        this.txtime = txtime;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustln() {
        return custln;
    }

    public void setCustln(String custln) {
        this.custln = custln;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getOadate() {
        return oadate;
    }

    public void setOadate(String oadate) {
        this.oadate = oadate;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getBusino() {
        return busino;
    }

    public void setBusino(String busino) {
        this.busino = busino;
    }

    public String getOfaddr() {
        return ofaddr;
    }

    public void setOfaddr(String ofaddr) {
        this.ofaddr = ofaddr;
    }

    public String getCrpdeg() {
        return crpdeg;
    }

    public void setCrpdeg(String crpdeg) {
        this.crpdeg = crpdeg;
    }

    public String getCrpcno() {
        return crpcno;
    }

    public void setCrpcno(String crpcno) {
        this.crpcno = crpcno;
    }

    public String getCtigno() {
        return ctigno;
    }

    public void setCtigno(String ctigno) {
        this.ctigno = ctigno;
    }

    public String getCitorg() {
        return citorg;
    }

    public void setCitorg(String citorg) {
        this.citorg = citorg;
    }

    public String getGrpsta() {
        return grpsta;
    }

    public void setGrpsta(String grpsta) {
        this.grpsta = grpsta;
    }

    public String getOprtye() {
        return oprtye;
    }

    public void setOprtye(String oprtye) {
        this.oprtye = oprtye;
    }

    public String getInptno() {
        return inptno;
    }

    public void setInptno(String inptno) {
        this.inptno = inptno;
    }

    public String getInporg() {
        return inporg;
    }

    public void setInporg(String inporg) {
        this.inporg = inporg;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", txdate='" + txdate + '\'' +
                ", txtime='" + txtime + '\'' +
                ", datasq='" + datasq + '\'' +
                ", grouna='" + grouna + '\'' +
                ", custno='" + custno + '\'' +
                ", custln='" + custln + '\'' +
                ", custna='" + custna + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", oadate='" + oadate + '\'' +
                ", homead='" + homead + '\'' +
                ", socino='" + socino + '\'' +
                ", busino='" + busino + '\'' +
                ", ofaddr='" + ofaddr + '\'' +
                ", crpdeg='" + crpdeg + '\'' +
                ", crpcno='" + crpcno + '\'' +
                ", ctigno='" + ctigno + '\'' +
                ", citorg='" + citorg + '\'' +
                ", grpsta='" + grpsta + '\'' +
                ", oprtye='" + oprtye + '\'' +
                ", inptno='" + inptno + '\'' +
                ", inporg='" + inporg + '\'' +
                ", list=" + list +
                '}';
    }
}
