package cn.com.yusys.yusp.online.client.esb.core.ln3111.req;


/**
 * 请求Service：用于贷款放款前进行还款计划试算
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3111ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

