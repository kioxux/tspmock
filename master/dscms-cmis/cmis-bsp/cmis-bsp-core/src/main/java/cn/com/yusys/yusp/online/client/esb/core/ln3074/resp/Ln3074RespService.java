package cn.com.yusys.yusp.online.client.esb.core.ln3074.resp;

/**
 * 响应Service：用于贷款预约展期
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3074RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      

