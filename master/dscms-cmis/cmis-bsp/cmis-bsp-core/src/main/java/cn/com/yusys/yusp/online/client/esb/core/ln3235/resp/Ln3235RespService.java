package cn.com.yusys.yusp.online.client.esb.core.ln3235.resp;

/**
 * 响应Service：还款方式转换
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3235RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3235RespService{" +
                "service=" + service +
                '}';
    }
}

