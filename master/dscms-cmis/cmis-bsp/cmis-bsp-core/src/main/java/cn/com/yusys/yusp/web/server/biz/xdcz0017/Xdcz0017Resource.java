package cn.com.yusys.yusp.web.server.biz.xdcz0017;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0017.req.Xdcz0017ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0017.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0017.resp.Xdcz0017RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0017.req.Xdcz0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0017.resp.Xdcz0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:支用(微信小程序)
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0017:支用(微信小程序)")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0017Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0017
     * 交易描述：支用(微信小程序)
     *
     * @param xdcz0017ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("支用(微信小程序)")
    @PostMapping("/xdcz0017")
//   @Idempotent({"xdcz0017", "#xdcz0017ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0017RespDto xdcz0017(@Validated @RequestBody Xdcz0017ReqDto xdcz0017ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017ReqDto));
        Xdcz0017DataReqDto xdcz0017DataReqDto = new Xdcz0017DataReqDto();// 请求Data： 电子保函开立
        Xdcz0017DataRespDto xdcz0017DataRespDto = new Xdcz0017DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0017.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0017RespDto xdcz0017RespDto = new Xdcz0017RespDto();
        try {
            // 从 xdcz0017ReqDto获取 reqData
            reqData = xdcz0017ReqDto.getData();
            // 将 reqData 拷贝到xdcz0017DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0017DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017DataReqDto));
            ResultDto<Xdcz0017DataRespDto> xdcz0017DataResultDto = dscmsBizCzClientService.xdcz0017(xdcz0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0017RespDto.setErorcd(Optional.ofNullable(xdcz0017DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0017RespDto.setErortx(Optional.ofNullable(xdcz0017DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0017DataResultDto.getCode())) {
                xdcz0017DataRespDto = xdcz0017DataResultDto.getData();
                xdcz0017RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0017RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0017DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0017DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, e.getMessage());
            xdcz0017RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0017RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0017RespDto.setDatasq(servsq);

        xdcz0017RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0017.key, DscmsEnum.TRADE_CODE_XDCZ0017.value, JSON.toJSONString(xdcz0017RespDto));
        return xdcz0017RespDto;
    }
}
