package cn.com.yusys.yusp.online.client.esb.core.ln3036.req;

/**
 * 请求Service：针对录入、复核模式的交易，用于查询信贷录入信息的查询，复核时使用
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3036ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

