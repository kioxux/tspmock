package cn.com.yusys.yusp.online.client.esb.core.da3320.resp;

import cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0.Listnm0_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm1.Listnm1_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.da3320.resp.lstdzzccz.Lstdzzccz_ARRAY;

import java.math.BigDecimal;

/**
 * 响应Service：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;// 响应码 否
    private String erortx;// 响应信息 否

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private String dzwzleib;//抵债物资类别
    private String cqzmzlei;//产权证明种类
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String yngyjigo;//营业机构
    private String zhngjigo;//账务机构
    private String huobdhao;//货币代号
    private String dzzcztai;//抵债资产状态
    private BigDecimal daizdzzc;//待转抵债资产
    private BigDecimal dcldzzic;//待处理抵债资产
    private BigDecimal dbxdzzic;//待变现抵债资产
    private BigDecimal hfeijine;//还费金额
    private BigDecimal huanbjee;//还本金额
    private BigDecimal hxijinee;//还息金额
    private BigDecimal yicldzzc;//已处置抵债资产
    private String dzzcdanw;//抵债资产单位
    private Long dzzcshul;//抵债资产数量
    private BigDecimal pingjiaz;//评估价值
    private BigDecimal dzzcrzjz;//抵债资产入账价值
    private String beizhuuu;//备注信息
    private String jitiriqi;//计提日期
    private BigDecimal jitijine;//计提金额
    private BigDecimal jianzzhb;//减值准备
    private BigDecimal jianzssh;//减值损失
    private BigDecimal dzczywsr;//抵债资产处置营业外收入
    private BigDecimal dzczywzc;//抵债资产处置营业外支出
    private cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm0.Listnm0_ARRAY listnm0_ARRAY;//费用明细
    private cn.com.yusys.yusp.online.client.esb.core.da3320.resp.listnm1.Listnm1_ARRAY listnm1_ARRAY;//抵债资产与贷款关联信息
    private cn.com.yusys.yusp.online.client.esb.core.da3320.resp.lstdzzccz.Lstdzzccz_ARRAY lstdzzccz_ARRAY;//抵债资产出租信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getDzwzleib() {
        return dzwzleib;
    }

    public void setDzwzleib(String dzwzleib) {
        this.dzwzleib = dzwzleib;
    }

    public String getCqzmzlei() {
        return cqzmzlei;
    }

    public void setCqzmzlei(String cqzmzlei) {
        this.cqzmzlei = cqzmzlei;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getDzzcztai() {
        return dzzcztai;
    }

    public void setDzzcztai(String dzzcztai) {
        this.dzzcztai = dzzcztai;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public BigDecimal getYicldzzc() {
        return yicldzzc;
    }

    public void setYicldzzc(BigDecimal yicldzzc) {
        this.yicldzzc = yicldzzc;
    }

    public String getDzzcdanw() {
        return dzzcdanw;
    }

    public void setDzzcdanw(String dzzcdanw) {
        this.dzzcdanw = dzzcdanw;
    }

    public Long getDzzcshul() {
        return dzzcshul;
    }

    public void setDzzcshul(Long dzzcshul) {
        this.dzzcshul = dzzcshul;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDzzcrzjz() {
        return dzzcrzjz;
    }

    public void setDzzcrzjz(BigDecimal dzzcrzjz) {
        this.dzzcrzjz = dzzcrzjz;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getJitiriqi() {
        return jitiriqi;
    }

    public void setJitiriqi(String jitiriqi) {
        this.jitiriqi = jitiriqi;
    }

    public BigDecimal getJitijine() {
        return jitijine;
    }

    public void setJitijine(BigDecimal jitijine) {
        this.jitijine = jitijine;
    }

    public BigDecimal getJianzzhb() {
        return jianzzhb;
    }

    public void setJianzzhb(BigDecimal jianzzhb) {
        this.jianzzhb = jianzzhb;
    }

    public BigDecimal getJianzssh() {
        return jianzssh;
    }

    public void setJianzssh(BigDecimal jianzssh) {
        this.jianzssh = jianzssh;
    }

    public BigDecimal getDzczywsr() {
        return dzczywsr;
    }

    public void setDzczywsr(BigDecimal dzczywsr) {
        this.dzczywsr = dzczywsr;
    }

    public BigDecimal getDzczywzc() {
        return dzczywzc;
    }

    public void setDzczywzc(BigDecimal dzczywzc) {
        this.dzczywzc = dzczywzc;
    }

    public Listnm0_ARRAY getListnm0_ARRAY() {
        return listnm0_ARRAY;
    }

    public void setListnm0_ARRAY(Listnm0_ARRAY listnm0_ARRAY) {
        this.listnm0_ARRAY = listnm0_ARRAY;
    }

    public Listnm1_ARRAY getListnm1_ARRAY() {
        return listnm1_ARRAY;
    }

    public void setListnm1_ARRAY(Listnm1_ARRAY listnm1_ARRAY) {
        this.listnm1_ARRAY = listnm1_ARRAY;
    }

    public Lstdzzccz_ARRAY getLstdzzccz_ARRAY() {
        return lstdzzccz_ARRAY;
    }

    public void setLstdzzccz_ARRAY(Lstdzzccz_ARRAY lstdzzccz_ARRAY) {
        this.lstdzzccz_ARRAY = lstdzzccz_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", dzwzleib='" + dzwzleib + '\'' +
                ", cqzmzlei='" + cqzmzlei + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", dzzcztai='" + dzzcztai + '\'' +
                ", daizdzzc=" + daizdzzc +
                ", dcldzzic=" + dcldzzic +
                ", dbxdzzic=" + dbxdzzic +
                ", hfeijine=" + hfeijine +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", yicldzzc=" + yicldzzc +
                ", dzzcdanw='" + dzzcdanw + '\'' +
                ", dzzcshul=" + dzzcshul +
                ", pingjiaz=" + pingjiaz +
                ", dzzcrzjz=" + dzzcrzjz +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", jitiriqi='" + jitiriqi + '\'' +
                ", jitijine=" + jitijine +
                ", jianzzhb=" + jianzzhb +
                ", jianzssh=" + jianzssh +
                ", dzczywsr=" + dzczywsr +
                ", dzczywzc=" + dzczywzc +
                ", listnm0_ARRAY=" + listnm0_ARRAY +
                ", listnm1_ARRAY=" + listnm1_ARRAY +
                ", lstdzzccz_ARRAY=" + lstdzzccz_ARRAY +
                '}';
    }
}
