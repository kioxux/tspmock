package cn.com.yusys.yusp.online.client.esb.core.ln3120.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/31 15:06
 * @since 2021/5/31 15:06
 */
public class Record {

    private String dkjiejuh;//贷款借据号
    private String jiaoyirq;//交易日期
    private String jiaoyijg;//交易机构
    private String jiaoyigy;//交易柜员
    private String jiaoyils;//交易流水
    private String jiaoyima;//交易码
    private String wjmingch;//文件名
    private String jyshju01;//交易数据1
    private String jyshju02;//交易数据2
    private String jyshju03;//交易数据3
    private String jyshju04;//交易数据4
    private String jychlizt;//交易处理状态
    private String fuheriqi;//复核日期
    private String fuheguiy;//复核柜员
    private String fuhelius;//复核流水
    private String fuhejgou;//复核机构
    private String farendma;//法人代码
    private String weihguiy;//维护柜员
    private String weihjigo;//维护机构
    private String weihriqi;//维护日期
    private Integer shijchuo;//时间戳
    private String jiluztai;//记录状态

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getWjmingch() {
        return wjmingch;
    }

    public void setWjmingch(String wjmingch) {
        this.wjmingch = wjmingch;
    }

    public String getJyshju01() {
        return jyshju01;
    }

    public void setJyshju01(String jyshju01) {
        this.jyshju01 = jyshju01;
    }

    public String getJyshju02() {
        return jyshju02;
    }

    public void setJyshju02(String jyshju02) {
        this.jyshju02 = jyshju02;
    }

    public String getJyshju03() {
        return jyshju03;
    }

    public void setJyshju03(String jyshju03) {
        this.jyshju03 = jyshju03;
    }

    public String getJyshju04() {
        return jyshju04;
    }

    public void setJyshju04(String jyshju04) {
        this.jyshju04 = jyshju04;
    }

    public String getJychlizt() {
        return jychlizt;
    }

    public void setJychlizt(String jychlizt) {
        this.jychlizt = jychlizt;
    }

    public String getFuheriqi() {
        return fuheriqi;
    }

    public void setFuheriqi(String fuheriqi) {
        this.fuheriqi = fuheriqi;
    }

    public String getFuheguiy() {
        return fuheguiy;
    }

    public void setFuheguiy(String fuheguiy) {
        this.fuheguiy = fuheguiy;
    }

    public String getFuhelius() {
        return fuhelius;
    }

    public void setFuhelius(String fuhelius) {
        this.fuhelius = fuhelius;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public Integer getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Integer shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "Service{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "wjmingch='" + wjmingch + '\'' +
                "jyshju01='" + jyshju01 + '\'' +
                "jyshju02='" + jyshju02 + '\'' +
                "jyshju03='" + jyshju03 + '\'' +
                "jyshju04='" + jyshju04 + '\'' +
                "jychlizt='" + jychlizt + '\'' +
                "fuheriqi='" + fuheriqi + '\'' +
                "fuheguiy='" + fuheguiy + '\'' +
                "fuhelius='" + fuhelius + '\'' +
                "fuhejgou='" + fuhejgou + '\'' +
                "farendma='" + farendma + '\'' +
                "weihguiy='" + weihguiy + '\'' +
                "weihjigo='" + weihjigo + '\'' +
                "weihriqi='" + weihriqi + '\'' +
                "shijchuo='" + shijchuo + '\'' +
                "jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
