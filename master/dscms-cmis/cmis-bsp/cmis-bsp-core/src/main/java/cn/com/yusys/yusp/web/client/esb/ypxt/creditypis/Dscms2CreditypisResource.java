package cn.com.yusys.yusp.web.client.esb.ypxt.creditypis;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.req.CreditypisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.resp.CreditypisRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.CreditypisReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp.CreditypisRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(creditypis)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2CreditypisResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2CreditypisResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：creditypis
     * 交易描述：信用证信息同步
     *
     * @param creditypisReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/creditypis")
    protected @ResponseBody
    ResultDto<CreditypisRespDto> creditypis(@Validated @RequestBody CreditypisReqDto creditypisReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDITYPIS.key, EsbEnum.TRADE_CODE_CREDITYPIS.value, JSON.toJSONString(creditypisReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp.Service();
        CreditypisReqService creditypisReqService = new CreditypisReqService();
        CreditypisRespService creditypisRespService = new CreditypisRespService();
        CreditypisRespDto creditypisRespDto = new CreditypisRespDto();
        ResultDto<CreditypisRespDto> creditypisResultDto = new ResultDto<CreditypisRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将creditypisReqDto转换成reqService
            BeanUtils.copyProperties(creditypisReqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(creditypisReqDto.getList())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.req.List> creditypisReqDtoLists = Optional.ofNullable(creditypisReqDto.getList()).orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.List creditypisReqServiceList = Optional.ofNullable(reqService.getList())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.List());
                java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.Record> creditypisReqRecords = creditypisReqDtoLists.parallelStream().map(creditypisReqDtoList -> {
                    cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.Record creditypisReqRecord = new cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.req.Record();
                    BeanUtils.copyProperties(creditypisReqDtoList, creditypisReqRecord);
                    return creditypisReqRecord;
                }).collect(Collectors.toList());
                creditypisReqServiceList.setRecord(creditypisReqRecords);
                reqService.setList(creditypisReqServiceList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CREDITYPIS.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            creditypisReqService.setService(reqService);
            // 将creditypisReqService转换成creditypisReqServiceMap
            Map creditypisReqServiceMap = beanMapUtil.beanToMap(creditypisReqService);
            context.put("tradeDataMap", creditypisReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDITYPIS.key, EsbEnum.TRADE_CODE_CREDITYPIS.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CREDITYPIS.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDITYPIS.key, EsbEnum.TRADE_CODE_CREDITYPIS.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            creditypisRespService = beanMapUtil.mapToBean(tradeDataMap, CreditypisRespService.class, CreditypisRespService.class);
            respService = creditypisRespService.getService();

            creditypisResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            creditypisResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, creditypisRespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp.List creditypisRespServiceList = Optional.ofNullable(respService.getList())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp.List());
                respService.setList(creditypisRespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp.Record> creditypisRespRecordList = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.ypxt.creditypis.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.resp.List> creditypisRespLists = creditypisRespRecordList.parallelStream().map(creditypisRespRecord -> {
                                cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.resp.List creditypisRespList = new cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.resp.List();
                                BeanUtils.copyProperties(creditypisRespRecord, creditypisRespList);
                                return creditypisRespList;
                            }
                    ).collect(Collectors.toList());
                    creditypisRespDto.setList(creditypisRespLists);
                }
                creditypisResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                creditypisResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                creditypisResultDto.setCode(EpbEnum.EPB099999.key);
                creditypisResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDITYPIS.key, EsbEnum.TRADE_CODE_CREDITYPIS.value, e.getMessage());
            creditypisResultDto.setCode(EpbEnum.EPB099999.key);//9999
            creditypisResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        creditypisResultDto.setData(creditypisRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDITYPIS.key, EsbEnum.TRADE_CODE_CREDITYPIS.value, JSON.toJSONString(creditypisResultDto));
        return creditypisResultDto;
    }
}
