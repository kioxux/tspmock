package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpqskz.Record;

import java.util.List;

/**
 * 响应Service：产品缺省控制对象
 *
 * @author lihh
 * @version 1.0
 */
public class Lstcpqskz_ARRAY {

    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstcpqskz{" +
                "record=" + record +
                '}';
    }
}
