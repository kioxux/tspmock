package cn.com.yusys.yusp.online.client.esb.circp.fb1150.req;

/**
 * 请求Service：省心快贷plus授信申请
 *
 * @author code-generator
 * @version 1.0
 */
public class Fb1150ReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}                      
