package cn.com.yusys.yusp.online.client.esb.core.ln3032.resp;

/**
 * 响应Service：受托支付信息维护
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:00
 */
public class LstStzf_ARRAY {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstStzf_ARRAY{" +
                "record=" + record +
                '}';
    }
}
