package cn.com.yusys.yusp.online.client.esb.ecif.g11004.req;

/**
 * 请求Service：客户集团信息查询（new）接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 21:06
 */
public class G11004ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
