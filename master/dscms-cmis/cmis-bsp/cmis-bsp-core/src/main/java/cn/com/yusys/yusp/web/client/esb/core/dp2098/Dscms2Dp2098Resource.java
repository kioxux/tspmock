package cn.com.yusys.yusp.web.client.esb.core.dp2098;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.req.Dp2098ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Dp2098RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.dp2098.req.Dp2098ReqService;
import cn.com.yusys.yusp.online.client.esb.core.dp2098.req.Service;
import cn.com.yusys.yusp.online.client.esb.core.dp2098.resp.Dp2098RespService;
import cn.com.yusys.yusp.online.client.esb.core.dp2098.resp.Lstacctinfo;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:待清算账户查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(dp2098)")
@RestController
@RequestMapping("/api/dscms2coredp")
public class Dscms2Dp2098Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dp2098Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：dp2098
     * 交易描述：待清算账户查询
     *
     * @param dp2098ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("dp2098:待清算账户查询")
    @PostMapping("/dp2098")
    protected @ResponseBody
    ResultDto<Dp2098RespDto> dp2098(@Validated @RequestBody Dp2098ReqDto dp2098ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2098.key, EsbEnum.TRADE_CODE_DP2098.value, JSON.toJSONString(dp2098ReqDto));
        Service reqService = new Service();
        cn.com.yusys.yusp.online.client.esb.core.dp2098.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.dp2098.resp.Service();
        Dp2098ReqService dp2098ReqService = new Dp2098ReqService();
        Dp2098RespService dp2098RespService = new Dp2098RespService();
        Dp2098RespDto dp2098RespDto = new Dp2098RespDto();
        ResultDto<Dp2098RespDto> dp2098ResultDto = new ResultDto<Dp2098RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将dp2098ReqDto转换成reqService
            BeanUtils.copyProperties(dp2098ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DP2098.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            dp2098ReqService.setService(reqService);
            // 将dp2098ReqService转换成dp2098ReqServiceMap
            Map dp2098ReqServiceMap = beanMapUtil.beanToMap(dp2098ReqService);
            context.put("tradeDataMap", dp2098ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DP2098.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            dp2098RespService = beanMapUtil.mapToBean(tradeDataMap, Dp2098RespService.class, Dp2098RespService.class);
            respService = dp2098RespService.getService();
            //  将respService转换成Dp2098RespDto
            dp2098ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            dp2098ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                BeanUtils.copyProperties(respService, dp2098RespDto);
                Lstacctinfo lstacctinfo = Optional.ofNullable(respService.getList()).orElse(new Lstacctinfo());
                if (CollectionUtils.nonEmpty(lstacctinfo.getRecord())) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Lstacctinfo> targetList = lstacctinfo.getRecord().stream().map(l -> {
                        cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Lstacctinfo target = new cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Lstacctinfo();
                        BeanUtils.copyProperties(l, target);
                        return target;
                    }).collect(Collectors.toList());
                    dp2098RespDto.setLstacctinfo(targetList);
                }

                dp2098ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                dp2098ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                dp2098ResultDto.setCode(EpbEnum.EPB099999.key);
                dp2098ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2098.key, EsbEnum.TRADE_CODE_DP2098.value, e.getMessage());
            dp2098ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            dp2098ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }

        dp2098ResultDto.setData(dp2098RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DP2098.key, EsbEnum.TRADE_CODE_DP2098.value, JSON.toJSONString(dp2098ResultDto));
        return dp2098ResultDto;
    }
}
