package cn.com.yusys.yusp.web.client.esb.yphsxt.gyypbh;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.req.GyypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.resp.GyypbhRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.req.GyypbhReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.resp.GyypbhRespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:通过共有人编号查询押品编号
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2GyypbhResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2GyypbhResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：gyypbh
     * 交易描述：通过共有人编号查询押品编号
     *
     * @param gyypbhReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/gyypbh")
    protected @ResponseBody
    ResultDto<GyypbhRespDto> gyypbh(@Validated @RequestBody GyypbhReqDto gyypbhReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GYYPBH.key, EsbEnum.TRADE_CODE_GYYPBH.value, JSON.toJSONString(gyypbhReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.gyypbh.resp.Service();
        GyypbhReqService gyypbhReqService = new GyypbhReqService();
        GyypbhRespService gyypbhRespService = new GyypbhRespService();
        GyypbhRespDto gyypbhRespDto = new GyypbhRespDto();
        ResultDto<GyypbhRespDto> gyypbhResultDto = new ResultDto<GyypbhRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将gyypbhReqDto转换成reqService
            BeanUtils.copyProperties(gyypbhReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_GYYPBH.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            gyypbhReqService.setService(reqService);
            // 将gyypbhReqService转换成gyypbhReqServiceMap
            Map gyypbhReqServiceMap = beanMapUtil.beanToMap(gyypbhReqService);
            context.put("tradeDataMap", gyypbhReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GYYPBH.key, EsbEnum.TRADE_CODE_GYYPBH.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_GYYPBH.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GYYPBH.key, EsbEnum.TRADE_CODE_GYYPBH.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            gyypbhRespService = beanMapUtil.mapToBean(tradeDataMap, GyypbhRespService.class, GyypbhRespService.class);
            respService = gyypbhRespService.getService();
            //  将respService转换成GyypbhRespDto
            gyypbhResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            gyypbhResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, gyypbhRespDto);
                gyypbhResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                gyypbhResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                gyypbhResultDto.setCode(EpbEnum.EPB099999.key);
                gyypbhResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GYYPBH.key, EsbEnum.TRADE_CODE_GYYPBH.value, e.getMessage());
            gyypbhResultDto.setCode(EpbEnum.EPB099999.key);//9999
            gyypbhResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        gyypbhResultDto.setData(gyypbhRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_GYYPBH.key, EsbEnum.TRADE_CODE_GYYPBH.value, JSON.toJSONString(gyypbhResultDto));
        return gyypbhResultDto;
    }
}
