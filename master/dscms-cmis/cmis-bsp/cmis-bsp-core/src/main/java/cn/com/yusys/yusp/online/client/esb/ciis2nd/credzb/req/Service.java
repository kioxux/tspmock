package cn.com.yusys.yusp.online.client.esb.ciis2nd.credzb.req;

/**
 * 请求Service：指标通用接口
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String ruleCode;//指标编码
    private String reqId;//请求业务号
    private String reportId;//报告编号
    private String brchno;//机构号
    private String customName;//客户名称
    private String certificateNum;//客户证件号
    private String businessLine;//产品编号
    private String borrowPersonRelation;//与主借款人关系
    private String auditReason;//授权书条款
    private String startDate;//开始日期
    private String endDate;//结束日期

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getCertificateNum() {
        return certificateNum;
    }

    public void setCertificateNum(String certificateNum) {
        this.certificateNum = certificateNum;
    }

    public String getBusinessLine() {
        return businessLine;
    }

    public void setBusinessLine(String businessLine) {
        this.businessLine = businessLine;
    }

    public String getBorrowPersonRelation() {
        return borrowPersonRelation;
    }

    public void setBorrowPersonRelation(String borrowPersonRelation) {
        this.borrowPersonRelation = borrowPersonRelation;
    }

    public String getAuditReason() {
        return auditReason;
    }

    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "ruleCode='" + ruleCode + '\'' +
                "reqId='" + reqId + '\'' +
                "reportId='" + reportId + '\'' +
                "brchno='" + brchno + '\'' +
                "customName='" + customName + '\'' +
                "certificateNum='" + certificateNum + '\'' +
                "businessLine='" + businessLine + '\'' +
                "borrowPersonRelation='" + borrowPersonRelation + '\'' +
                "auditReason='" + auditReason + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                '}';
    }
}
