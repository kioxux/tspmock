package cn.com.yusys.yusp.online.client.esb.irs.xirs28.req;

/**
 * 请求Service：财务信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class Xirs28ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xirs28ReqService{" +
                "service=" + service +
                '}';
    }
}
