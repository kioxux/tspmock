package cn.com.yusys.yusp.online.client.esb.core.ln3006.req;

/**
 * 请求Service：贷款产品组合查询
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3006ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
