package cn.com.yusys.yusp.online.client.esb.ecif.g10002.req;

/**
 * 请求Service：客户群组信息维护
 *
 * @author code-generator
 * @version 1.0
 */
public class G10002ReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}                      
