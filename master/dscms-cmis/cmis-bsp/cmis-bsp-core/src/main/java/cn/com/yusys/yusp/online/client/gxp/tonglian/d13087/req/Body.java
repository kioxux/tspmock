package cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 10:14
 * @since 2021/5/27 10:14
 */
public class Body {
    private String rgstid;//分期申请号
    private String cardno;//卡号
    private String remark;//备注信息

    public String getRgstid() {
        return rgstid;
    }

    public void setRgstid(String rgstid) {
        this.rgstid = rgstid;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "Service{" +
                "rgstid='" + rgstid + '\'' +
                "cardno='" + cardno + '\'' +
                "remark='" + remark + '\'' +
                '}';
    }
}
