package cn.com.yusys.yusp.web.client.esb.core.da3304;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3304.req.Da3304ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3304.resp.Da3304RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3304.req.Da3304ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3304.resp.Da3304RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产转让借据筛选
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class DscmsDa3304Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsDa3304Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3304
     * 交易描述：资产转让借据筛选
     *
     * @param da3304ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3304")
    protected @ResponseBody
    ResultDto<Da3304RespDto> da3304(@Validated @RequestBody Da3304ReqDto da3304ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3304.key, EsbEnum.TRADE_CODE_DA3304.value, JSON.toJSONString(da3304ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3304.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3304.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3304.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3304.resp.Service();

        Da3304ReqService da3304ReqService = new Da3304ReqService();
        Da3304RespService da3304RespService = new Da3304RespService();
        Da3304RespDto da3304RespDto = new Da3304RespDto();
        ResultDto<Da3304RespDto> da3304ResultDto = new ResultDto<Da3304RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3304ReqDto转换成reqService
            BeanUtils.copyProperties(da3304ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3304.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3304ReqService.setService(reqService);
            // 将da3304ReqService转换成da3304ReqServiceMap
            Map da3304ReqServiceMap = beanMapUtil.beanToMap(da3304ReqService);
            context.put("tradeDataMap", da3304ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3304.key, EsbEnum.TRADE_CODE_DA3304.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3304.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3304.key, EsbEnum.TRADE_CODE_DA3304.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3304RespService = beanMapUtil.mapToBean(tradeDataMap, Da3304RespService.class, Da3304RespService.class);
            respService = da3304RespService.getService();


            da3304ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3304ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3304RespDto
                BeanUtils.copyProperties(respService, da3304RespDto);
                da3304ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3304ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3304ResultDto.setCode(EpbEnum.EPB099999.key);
                da3304ResultDto.setMessage(respService.getErortx());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3304.key, EsbEnum.TRADE_CODE_DA3304.value, e.getMessage());
            da3304ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3304ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3304ResultDto.setData(da3304RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3304.key, EsbEnum.TRADE_CODE_DA3304.value, JSON.toJSONString(da3304ResultDto));
        return da3304ResultDto;
    }
}
