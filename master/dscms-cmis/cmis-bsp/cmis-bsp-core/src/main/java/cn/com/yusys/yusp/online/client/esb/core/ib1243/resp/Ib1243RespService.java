package cn.com.yusys.yusp.online.client.esb.core.ib1243.resp;

/**
 * 响应Service：通用电子记帐交易（多借多贷-多币种）
 * @author lihh
 * @version 1.0             
 */      
public class Ib1243RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
