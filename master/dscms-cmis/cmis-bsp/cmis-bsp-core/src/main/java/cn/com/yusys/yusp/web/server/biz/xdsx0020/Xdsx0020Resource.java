package cn.com.yusys.yusp.web.server.biz.xdsx0020;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0020.req.Xdsx0020ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0020.resp.Xdsx0020RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0020.req.Xdsx0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0020.resp.Xdsx0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:担保公司合作协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0020:担保公司合作协议查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0020Resource.class);

    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0020
     * 交易描述：担保公司合作协议查询
     *
     * @param xdsx0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("担保公司合作协议查询")
    @PostMapping("/xdsx0020")
    //@Idempotent({"xdcasx0020", "#xdsx0020ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0020RespDto xdsx0020(@Validated @RequestBody Xdsx0020ReqDto xdsx0020ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, JSON.toJSONString(xdsx0020ReqDto));
        Xdsx0020DataReqDto xdsx0020DataReqDto = new Xdsx0020DataReqDto();// 请求Data： 担保公司合作协议查询
        Xdsx0020DataRespDto xdsx0020DataRespDto = new Xdsx0020DataRespDto();// 响应Data：担保公司合作协议查询
        Xdsx0020RespDto xdsx0020RespDto = new Xdsx0020RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdsx0020.req.Data reqData = null; // 请求Data：担保公司合作协议查询
        cn.com.yusys.yusp.dto.server.biz.xdsx0020.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0020.resp.Data();// 响应Data：担保公司合作协议查询
        try {
            // 从 xdsx0020ReqDto获取 reqData
            reqData = xdsx0020ReqDto.getData();
            // 将 reqData 拷贝到xdsx0020DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0020DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, JSON.toJSONString(xdsx0020DataReqDto));
            ResultDto<Xdsx0020DataRespDto> xdsx0020DataResultDto = dscmsBizSxClientService.xdsx0020(xdsx0020DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, JSON.toJSONString(xdsx0020DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0020RespDto.setErorcd(Optional.ofNullable(xdsx0020DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0020RespDto.setErortx(Optional.ofNullable(xdsx0020DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0020DataResultDto.getCode())) {
                xdsx0020DataRespDto = xdsx0020DataResultDto.getData();
                xdsx0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0020DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, e.getMessage());
            xdsx0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0020RespDto.setDatasq(servsq);

        xdsx0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0020.key, DscmsEnum.TRADE_CODE_XDSX0020.value, JSON.toJSONString(xdsx0020RespDto));
        return xdsx0020RespDto;
    }
}
