package cn.com.yusys.yusp.online.client.esb.core.ln3070.resp;



    /**
     * 响应Service：贷款录入信息的取消，即取消出账指令
     *
     * @author chenyong
     * @version 1.0
     */
    public class Service {
        private String erorcd;//响应码
        private String erortx;// 响应信息
        private String servsq;// 渠道流水
        private String datasq;//全局流水

        private String dkjiejuh;//贷款借据号
        private String jiaoyijg;//交易机构
        private String jiaoyigy;//交易柜员
        private String jiaoyima;//交易码
        private String wjmingch;//文件名
        private String jychlizt;//交易处理状态
        private String jiaoyirq;//交易日期
        private String jiaoyils;//交易流水

        public String getErorcd() {
            return erorcd;
        }

        public void setErorcd(String erorcd) {
            this.erorcd = erorcd;
        }

        public String getErortx() {
            return erortx;
        }

        public void setErortx(String erortx) {
            this.erortx = erortx;
        }

        public String getServsq() {
            return servsq;
        }

        public void setServsq(String servsq) {
            this.servsq = servsq;
        }

        public String getDatasq() {
            return datasq;
        }

        public void setDatasq(String datasq) {
            this.datasq = datasq;
        }

        public String getDkjiejuh() {
            return dkjiejuh;
        }

        public void setDkjiejuh(String dkjiejuh) {
            this.dkjiejuh = dkjiejuh;
        }

        public String getJiaoyijg() {
            return jiaoyijg;
        }

        public void setJiaoyijg(String jiaoyijg) {
            this.jiaoyijg = jiaoyijg;
        }

        public String getJiaoyigy() {
            return jiaoyigy;
        }

        public void setJiaoyigy(String jiaoyigy) {
            this.jiaoyigy = jiaoyigy;
        }

        public String getJiaoyima() {
            return jiaoyima;
        }

        public void setJiaoyima(String jiaoyima) {
            this.jiaoyima = jiaoyima;
        }

        public String getWjmingch() {
            return wjmingch;
        }

        public void setWjmingch(String wjmingch) {
            this.wjmingch = wjmingch;
        }

        public String getJychlizt() {
            return jychlizt;
        }

        public void setJychlizt(String jychlizt) {
            this.jychlizt = jychlizt;
        }

        public String getJiaoyirq() {
            return jiaoyirq;
        }

        public void setJiaoyirq(String jiaoyirq) {
            this.jiaoyirq = jiaoyirq;
        }

        public String getJiaoyils() {
            return jiaoyils;
        }

        public void setJiaoyils(String jiaoyils) {
            this.jiaoyils = jiaoyils;
        }

        @Override
        public String toString() {
            return "Service{" +
                    "erorcd='" + erorcd + '\'' +
                    ", erortx='" + erortx + '\'' +
                    ", servsq='" + servsq + '\'' +
                    ", datasq='" + datasq + '\'' +
                    ", dkjiejuh='" + dkjiejuh + '\'' +
                    ", jiaoyijg='" + jiaoyijg + '\'' +
                    ", jiaoyigy='" + jiaoyigy + '\'' +
                    ", jiaoyima='" + jiaoyima + '\'' +
                    ", wjmingch='" + wjmingch + '\'' +
                    ", jychlizt='" + jychlizt + '\'' +
                    ", jiaoyirq='" + jiaoyirq + '\'' +
                    ", jiaoyils='" + jiaoyils + '\'' +
                    '}';
        }
    }

