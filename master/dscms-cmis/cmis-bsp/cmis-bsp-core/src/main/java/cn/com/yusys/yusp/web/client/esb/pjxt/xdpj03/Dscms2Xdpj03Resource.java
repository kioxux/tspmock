package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj03;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.Xdpj03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.resp.Xdpj03RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.Xdpj03ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.resp.Xdpj03RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:票据承兑签发审批请求
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj03Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj03Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdpj03
     * 交易描述：票据承兑签发审批请求
     *
     * @param xdpj03ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdpj03")
    protected @ResponseBody
    ResultDto<Xdpj03RespDto> xdpj03(@Validated @RequestBody Xdpj03ReqDto xdpj03ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ03.key, EsbEnum.TRADE_CODE_XDPJ03.value, JSON.toJSONString(xdpj03ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.Service();
        cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.resp.Service();
        Xdpj03ReqService xdpj03ReqService = new Xdpj03ReqService();
        Xdpj03RespService xdpj03RespService = new Xdpj03RespService();
        Xdpj03RespDto xdpj03RespDto = new Xdpj03RespDto();
        ResultDto<Xdpj03RespDto> xdpj03ResultDto = new ResultDto<Xdpj03RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdpj03ReqDto转换成reqService
            BeanUtils.copyProperties(xdpj03ReqDto, reqService);
            java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.List> reqList = xdpj03ReqDto.getList();
            if (CollectionUtils.nonEmpty(reqList)) {
                java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.Record> recordList = reqList.parallelStream().map(req -> {
                    cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.Record record = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.Record();
                    BeanUtils.copyProperties(req, record);
                    return record;
                }).collect(Collectors.toList());
                cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.List serviceList = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj03.req.List();
                serviceList.setRecord(recordList);
                reqService.setList(serviceList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ03.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            xdpj03ReqService.setService(reqService);
            // 将xdpj03ReqService转换成xdpj03ReqServiceMap
            Map xdpj03ReqServiceMap = beanMapUtil.beanToMap(xdpj03ReqService);
            context.put("tradeDataMap", xdpj03ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ03.key, EsbEnum.TRADE_CODE_XDPJ03.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ03.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ03.key, EsbEnum.TRADE_CODE_XDPJ03.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdpj03RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj03RespService.class, Xdpj03RespService.class);
            respService = xdpj03RespService.getService();
            //  将respService转换成Xdpj03RespDto
            BeanUtils.copyProperties(respService, xdpj03RespDto);
            xdpj03ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xdpj03ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xdpj03RespDto);
                xdpj03ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdpj03ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdpj03ResultDto.setCode(EpbEnum.EPB099999.key);
                xdpj03ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ03.key, EsbEnum.TRADE_CODE_XDPJ03.value, e.getMessage());
            xdpj03ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdpj03ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdpj03ResultDto.setData(xdpj03RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ03.key, EsbEnum.TRADE_CODE_XDPJ03.value, JSON.toJSONString(xdpj03ResultDto));
        return xdpj03ResultDto;
    }
}
