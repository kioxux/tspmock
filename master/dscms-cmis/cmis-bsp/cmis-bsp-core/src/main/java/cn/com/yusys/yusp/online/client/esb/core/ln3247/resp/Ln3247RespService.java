package cn.com.yusys.yusp.online.client.esb.core.ln3247.resp;

/**
 * 响应Service：贷款还款周期预定变更
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3247RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3247RespService{" +
                "service=" + service +
                '}';
    }
}
