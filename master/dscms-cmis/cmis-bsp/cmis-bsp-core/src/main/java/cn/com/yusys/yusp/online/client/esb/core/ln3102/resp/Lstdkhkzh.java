package cn.com.yusys.yusp.online.client.esb.core.ln3102.resp;


import java.util.List;

/**
 * 响应Service：贷款多还款账户
 *
 * @author lihh
 * @version 1.0
 */
public class Lstdkhkzh {
    java.util.List<LstdkhkzhRecord> record;

    public List<LstdkhkzhRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LstdkhkzhRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhkzh{" +
                "record=" + record +
                '}';
    }
}
