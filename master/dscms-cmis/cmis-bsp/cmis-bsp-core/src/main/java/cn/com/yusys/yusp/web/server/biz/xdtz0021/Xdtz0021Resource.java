package cn.com.yusys.yusp.web.server.biz.xdtz0021;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0021.req.Xdtz0021ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0021.resp.Xdtz0021RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0021.req.Xdtz0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0021.resp.Xdtz0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:台账入账（贸易融资\福费廷）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0021:台账入账（贸易融资\\福费廷）")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0021Resource.class);
	@Autowired
	private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0021
     * 交易描述：台账入账（贸易融资\福费廷）
     *
     * @param xdtz0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("台账入账（贸易融资\\福费廷）")
    @PostMapping("/xdtz0021")
    //@Idempotent({"xdcatz0021", "#xdtz0021ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0021RespDto xdtz0021(@Validated @RequestBody Xdtz0021ReqDto xdtz0021ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, JSON.toJSONString(xdtz0021ReqDto));
        Xdtz0021DataReqDto xdtz0021DataReqDto = new Xdtz0021DataReqDto();// 请求Data： 台账入账（贸易融资\福费廷）
        Xdtz0021DataRespDto xdtz0021DataRespDto = new Xdtz0021DataRespDto();// 响应Data：台账入账（贸易融资\福费廷）
        Xdtz0021RespDto xdtz0021RespDto = new Xdtz0021RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0021.req.Data reqData = null; // 请求Data：台账入账（贸易融资\福费廷）
        cn.com.yusys.yusp.dto.server.biz.xdtz0021.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0021.resp.Data();// 响应Data：台账入账（贸易融资\福费廷）
        try {
            // 从 xdtz0021ReqDto获取 reqData
            reqData = xdtz0021ReqDto.getData();
            // 将 reqData 拷贝到xdtz0021DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0021DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, JSON.toJSONString(xdtz0021DataReqDto));
            ResultDto<Xdtz0021DataRespDto> xdtz0021DataResultDto = dscmsBizTzClientService.xdtz0021(xdtz0021DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, JSON.toJSONString(xdtz0021DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0021RespDto.setErorcd(Optional.ofNullable(xdtz0021DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0021RespDto.setErortx(Optional.ofNullable(xdtz0021DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0021DataResultDto.getCode())) {
                xdtz0021DataRespDto = xdtz0021DataResultDto.getData();
                xdtz0021RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0021RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0021DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, e.getMessage());
            xdtz0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0021RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0021RespDto.setDatasq(servsq);

        xdtz0021RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0021.key, DscmsEnum.TRADE_CODE_XDTZ0021.value, JSON.toJSONString(xdtz0021RespDto));
        return xdtz0021RespDto;
    }
}
