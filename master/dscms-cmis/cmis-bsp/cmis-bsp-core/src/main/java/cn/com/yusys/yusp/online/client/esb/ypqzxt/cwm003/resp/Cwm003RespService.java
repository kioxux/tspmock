package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm003.resp;

/**
 * 响应Service：信贷查询地方征信接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Cwm003RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
