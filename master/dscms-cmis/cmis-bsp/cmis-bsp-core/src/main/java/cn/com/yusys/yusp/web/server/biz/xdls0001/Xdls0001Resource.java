package cn.com.yusys.yusp.web.server.biz.xdls0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdls0001.req.Xdls0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdls0001.resp.Xdls0001RespDto;
import cn.com.yusys.yusp.dto.server.xdls0001.req.Xdls0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdls0001.resp.Xdls0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizLsClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:房贷要素查询
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDLS0001:房贷要素查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdls0001Resource {
	private static final Logger logger = LoggerFactory.getLogger(Xdls0001Resource.class);
	@Autowired
	private DscmsBizLsClientService dscmsBizLsClientService;

	/**
	 * 交易码：xdls0001
	 * 交易描述：房贷要素查询
	 *
	 * @param xdls0001ReqDto
	 * @return
	 * @throws Exception
	 */
	@ApiOperation("房贷要素查询")
	@PostMapping("/xdls0001")
	//@Idempotent({"xdcals0001", "#xdls0001ReqDto.datasq"})
	protected @ResponseBody
	Xdls0001RespDto xdls0001(@Validated @RequestBody Xdls0001ReqDto xdls0001ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, JSON.toJSONString(xdls0001ReqDto));
		Xdls0001DataReqDto xdls0001DataReqDto = new Xdls0001DataReqDto();// 请求Data： 房贷要素查询
		Xdls0001DataRespDto xdls0001DataRespDto = new Xdls0001DataRespDto();// 响应Data：房贷要素查询
		Xdls0001RespDto xdls0001RespDto = new Xdls0001RespDto();
		cn.com.yusys.yusp.dto.server.biz.xdls0001.req.Data reqData = null; // 请求Data：房贷要素查询
		cn.com.yusys.yusp.dto.server.biz.xdls0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdls0001.resp.Data();// 响应Data：房贷要素查询
		try {
			// 从 xdls0001ReqDto获取 reqData
			reqData = xdls0001ReqDto.getData();
			// 将 reqData 拷贝到xdls0001DataReqDto
			BeanUtils.copyProperties(reqData, xdls0001DataReqDto);
			// 调用服务：
			ResultDto<Xdls0001DataRespDto> xdls0001DataResultDto = dscmsBizLsClientService.xdls0001(xdls0001DataReqDto);
			// 从返回值中获取响应码和响应消息
			xdls0001RespDto.setErorcd(Optional.ofNullable(xdls0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
			xdls0001RespDto.setErortx(Optional.ofNullable(xdls0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdls0001DataResultDto.getCode())) {
				// 定义返回值
				xdls0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
				xdls0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
				xdls0001DataRespDto = xdls0001DataResultDto.getData();
				// 将 xdls0001DataRespDto拷贝到 respData
				BeanUtils.copyProperties(xdls0001DataRespDto, respData);
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, e.getMessage());
			xdls0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
			xdls0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
		}
		logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
		String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
		logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
		xdls0001RespDto.setDatasq(servsq);
		xdls0001RespDto.setData(respData);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDLS0001.key, DscmsEnum.TRADE_CODE_XDLS0001.value, JSON.toJSONString(xdls0001RespDto));
		return xdls0001RespDto;
	}
}
