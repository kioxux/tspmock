package cn.com.yusys.yusp.web.server.cus.xdkh0025;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0025.req.Xdkh0025ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0025.resp.Xdkh0025RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0025.req.Xdkh0025DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0025.resp.Xdkh0025DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷、优农贷客户信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0025:优企贷、优农贷客户信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0025Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0025Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0025
     * 交易描述：优企贷、优农贷客户信息查询
     *
     * @param xdkh0025ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷客户信息查询")
    @PostMapping("/xdkh0025")
    //@Idempotent({"xdcakh0025", "#xdkh0025ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0025RespDto xdkh0025(@Validated @RequestBody Xdkh0025ReqDto xdkh0025ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025ReqDto));
        Xdkh0025DataReqDto xdkh0025DataReqDto = new Xdkh0025DataReqDto();// 请求Data： 优企贷、优农贷客户信息查询
        Xdkh0025DataRespDto xdkh0025DataRespDto = new Xdkh0025DataRespDto();// 响应Data：优企贷、优农贷客户信息查询
        Xdkh0025RespDto xdkh0025RespDto = new Xdkh0025RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0025.req.Data reqData = null; // 请求Data：优企贷、优农贷客户信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0025.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0025.resp.Data();// 响应Data：优企贷、优农贷客户信息查询
        try {
            // 从 xdkh0025ReqDto获取 reqData
            reqData = xdkh0025ReqDto.getData();
            // 将 reqData 拷贝到xdkh0025DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0025DataReqDto);
            // 调用服务：cus
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataReqDto));
            ResultDto<Xdkh0025DataRespDto> xdkh0025DataResultDto = dscmsCusClientService.xdkh0025(xdkh0025DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0025RespDto.setErorcd(Optional.ofNullable(xdkh0025DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0025RespDto.setErortx(Optional.ofNullable(xdkh0025DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0025DataResultDto.getCode())) {
                xdkh0025DataRespDto = xdkh0025DataResultDto.getData();
                xdkh0025RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0025RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0025DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0025DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, e.getMessage());
            xdkh0025RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0025RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0025RespDto.setDatasq(servsq);

        xdkh0025RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0025.key, DscmsEnum.TRADE_CODE_XDKH0025.value, JSON.toJSONString(xdkh0025RespDto));
        return xdkh0025RespDto;
    }
}