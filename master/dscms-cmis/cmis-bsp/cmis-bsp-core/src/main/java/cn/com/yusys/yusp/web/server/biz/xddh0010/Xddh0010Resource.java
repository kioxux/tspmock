package cn.com.yusys.yusp.web.server.biz.xddh0010;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0010.req.Xddh0010ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0010.resp.Xddh0010RespDto;
import cn.com.yusys.yusp.dto.server.xddh0010.req.Xddh0010DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0010.resp.Xddh0010DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:推送优享贷预警信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0010:推送优享贷预警信息")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0010Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0010Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0010
     * 交易描述：推送优享贷预警信息
     *
     * @param xddh0010ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送优享贷预警信息")
    @PostMapping("/xddh0010")
    //@Idempotent({"xddh0010", "#xddh0010ReqDto.datasq"})
    protected @ResponseBody
    Xddh0010RespDto xddh0010(@Validated @RequestBody Xddh0010ReqDto xddh0010ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, JSON.toJSONString(xddh0010ReqDto));
        Xddh0010DataReqDto xddh0010DataReqDto = new Xddh0010DataReqDto();// 请求Data： 推送优享贷预警信息
        Xddh0010DataRespDto xddh0010DataRespDto = new Xddh0010DataRespDto();// 响应Data：推送优享贷预警信息
        Xddh0010RespDto xddh0010RespDto = new Xddh0010RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0010.req.Data reqData = null; // 请求Data：推送优享贷预警信息
        cn.com.yusys.yusp.dto.server.biz.xddh0010.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0010.resp.Data();// 响应Data：推送优享贷预警信息
        try {
            // 从 xddh0010ReqDto获取 reqData
            reqData = xddh0010ReqDto.getData();
            // 将 reqData 拷贝到xddh0010DataReqDto
            BeanUtils.copyProperties(reqData, xddh0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, JSON.toJSONString(xddh0010DataReqDto));
            ResultDto<Xddh0010DataRespDto> xddh0010DataResultDto = dscmsBizDhClientService.xddh0010(xddh0010DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, JSON.toJSONString(xddh0010DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0010RespDto.setErorcd(Optional.ofNullable(xddh0010DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0010RespDto.setErortx(Optional.ofNullable(xddh0010DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0010DataResultDto.getCode())) {
                xddh0010DataRespDto = xddh0010DataResultDto.getData();
                xddh0010RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0010RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0010DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0010DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, e.getMessage());
            xddh0010RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0010RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0010RespDto.setDatasq(servsq);

        xddh0010RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0010.key, DscmsEnum.TRADE_CODE_XDDH0010.value, JSON.toJSONString(xddh0010RespDto));
        return xddh0010RespDto;
    }
}
