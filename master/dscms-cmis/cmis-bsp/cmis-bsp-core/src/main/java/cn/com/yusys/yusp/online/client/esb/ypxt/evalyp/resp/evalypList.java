package cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.resp;

/**
 * 响应Service：押品我行确认价值同步接口
 *
 * @author chenyong
 * @version 1.0
 */
public class evalypList {
    private String yptybh;//押品统一编号
    private String gzdqry;//下次估值到期日

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getGzdqry() {
        return gzdqry;
    }

    public void setGzdqry(String gzdqry) {
        this.gzdqry = gzdqry;
    }

    @Override
    public String toString() {
        return "Service{" +
                "yptybh='" + yptybh + '\'' +
                "gzdqry='" + gzdqry + '\'' +
                '}';
    }
}
