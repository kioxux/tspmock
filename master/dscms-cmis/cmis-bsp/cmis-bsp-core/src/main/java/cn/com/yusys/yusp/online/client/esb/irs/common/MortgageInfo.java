package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 抵押物信息(MortgageInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:10
 */
public class MortgageInfo {

    private List<MortgageInfoRecord> record; // 抵押物信息

    public List<MortgageInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<MortgageInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "MortgageInfo{" +
                "record=" + record +
                '}';
    }
}
