package cn.com.yusys.yusp.online.client.esb.core.ln3065.resp;

import java.math.BigDecimal;

/**
 * 响应Service：资产转让资金划转
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String xieybhao;//协议编号
    private String xieyimch;//协议名称
    private String dhfkywlx;//业务类型
    private String zrfukzhh;//对外付款账号
    private String zrfukzxh;//对外付款账号子序号
    private BigDecimal bcfkjine;//本次付款金额
    private String fukuanrq;//付款日期
    private String zrfukzht;//转让付款状态
    private String bjyskzhh;//其他应收款账号(本金)
    private String qyskzhzh;//其他应收款账号子序号(本金)
    private BigDecimal zhanghye;//账户余额
    private String lxyskzhh;//其他应收款账号(利息)
    private String lxyskzxh;//其他应收款账号子序号(利息)
    private BigDecimal yueeeeee;//余额
    private String bjysfzhh;//其他应付款账号(本金)
    private String bjysfzxh;//其他应付款账号子序号(本金)
    private BigDecimal benjinje;//本金金额
    private String lxysfzhh;//其他应付款账号(利息)
    private String qtyfzhzx;//其他应付款账号子序号(利息)
    private BigDecimal lixijine;//利息金额
    private String ruzjigou;//入账机构
    private String jydszhmc;//交易对手账户名称
    private String jydsleix;//交易对手类型
    private String jydshmch;//交易对手名称
    private String zhkaihhh;//账户开户行行号
    private String zhkaihhm;//账户开户行行名
    private BigDecimal jiaoyije;//交易金额
    private String jiaoyirq;//交易日期
    private String jiaoyijg;//交易机构
    private String jiaoyigy;//交易柜员
    private String jiaoyils;//交易流水
    private String jydszhao;//交易对手账号
    private String jydszhzh;//交易对手账号子序号
    private String sunyrzzh;//损益入账账号
    private String syrzzhxh;//损益入账账号子序号
    private String nbuzhhao;//内部账号
    private String nbuzhzxh;//内部账号子序号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getDhfkywlx() {
        return dhfkywlx;
    }

    public void setDhfkywlx(String dhfkywlx) {
        this.dhfkywlx = dhfkywlx;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public BigDecimal getBcfkjine() {
        return bcfkjine;
    }

    public void setBcfkjine(BigDecimal bcfkjine) {
        this.bcfkjine = bcfkjine;
    }

    public String getFukuanrq() {
        return fukuanrq;
    }

    public void setFukuanrq(String fukuanrq) {
        this.fukuanrq = fukuanrq;
    }

    public String getZrfukzht() {
        return zrfukzht;
    }

    public void setZrfukzht(String zrfukzht) {
        this.zrfukzht = zrfukzht;
    }

    public String getBjyskzhh() {
        return bjyskzhh;
    }

    public void setBjyskzhh(String bjyskzhh) {
        this.bjyskzhh = bjyskzhh;
    }

    public String getQyskzhzh() {
        return qyskzhzh;
    }

    public void setQyskzhzh(String qyskzhzh) {
        this.qyskzhzh = qyskzhzh;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public String getLxyskzhh() {
        return lxyskzhh;
    }

    public void setLxyskzhh(String lxyskzhh) {
        this.lxyskzhh = lxyskzhh;
    }

    public String getLxyskzxh() {
        return lxyskzxh;
    }

    public void setLxyskzxh(String lxyskzxh) {
        this.lxyskzxh = lxyskzxh;
    }

    public BigDecimal getYueeeeee() {
        return yueeeeee;
    }

    public void setYueeeeee(BigDecimal yueeeeee) {
        this.yueeeeee = yueeeeee;
    }

    public String getBjysfzhh() {
        return bjysfzhh;
    }

    public void setBjysfzhh(String bjysfzhh) {
        this.bjysfzhh = bjysfzhh;
    }

    public String getBjysfzxh() {
        return bjysfzxh;
    }

    public void setBjysfzxh(String bjysfzxh) {
        this.bjysfzxh = bjysfzxh;
    }

    public BigDecimal getBenjinje() {
        return benjinje;
    }

    public void setBenjinje(BigDecimal benjinje) {
        this.benjinje = benjinje;
    }

    public String getLxysfzhh() {
        return lxysfzhh;
    }

    public void setLxysfzhh(String lxysfzhh) {
        this.lxysfzhh = lxysfzhh;
    }

    public String getQtyfzhzx() {
        return qtyfzhzx;
    }

    public void setQtyfzhzx(String qtyfzhzx) {
        this.qtyfzhzx = qtyfzhzx;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    public String getJydsleix() {
        return jydsleix;
    }

    public void setJydsleix(String jydsleix) {
        this.jydsleix = jydsleix;
    }

    public String getJydshmch() {
        return jydshmch;
    }

    public void setJydshmch(String jydshmch) {
        this.jydshmch = jydshmch;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getSunyrzzh() {
        return sunyrzzh;
    }

    public void setSunyrzzh(String sunyrzzh) {
        this.sunyrzzh = sunyrzzh;
    }

    public String getSyrzzhxh() {
        return syrzzhxh;
    }

    public void setSyrzzhxh(String syrzzhxh) {
        this.syrzzhxh = syrzzhxh;
    }

    public String getNbuzhhao() {
        return nbuzhhao;
    }

    public void setNbuzhhao(String nbuzhhao) {
        this.nbuzhhao = nbuzhhao;
    }

    public String getNbuzhzxh() {
        return nbuzhzxh;
    }

    public void setNbuzhzxh(String nbuzhzxh) {
        this.nbuzhzxh = nbuzhzxh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", xieyimch='" + xieyimch + '\'' +
                ", dhfkywlx='" + dhfkywlx + '\'' +
                ", zrfukzhh='" + zrfukzhh + '\'' +
                ", zrfukzxh='" + zrfukzxh + '\'' +
                ", bcfkjine=" + bcfkjine +
                ", fukuanrq='" + fukuanrq + '\'' +
                ", zrfukzht='" + zrfukzht + '\'' +
                ", bjyskzhh='" + bjyskzhh + '\'' +
                ", qyskzhzh='" + qyskzhzh + '\'' +
                ", zhanghye=" + zhanghye +
                ", lxyskzhh='" + lxyskzhh + '\'' +
                ", lxyskzxh='" + lxyskzxh + '\'' +
                ", yueeeeee=" + yueeeeee +
                ", bjysfzhh='" + bjysfzhh + '\'' +
                ", bjysfzxh='" + bjysfzxh + '\'' +
                ", benjinje=" + benjinje +
                ", lxysfzhh='" + lxysfzhh + '\'' +
                ", qtyfzhzx='" + qtyfzhzx + '\'' +
                ", lixijine=" + lixijine +
                ", ruzjigou='" + ruzjigou + '\'' +
                ", jydszhmc='" + jydszhmc + '\'' +
                ", jydsleix='" + jydsleix + '\'' +
                ", jydshmch='" + jydshmch + '\'' +
                ", zhkaihhh='" + zhkaihhh + '\'' +
                ", zhkaihhm='" + zhkaihhm + '\'' +
                ", jiaoyije=" + jiaoyije +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jydszhao='" + jydszhao + '\'' +
                ", jydszhzh='" + jydszhzh + '\'' +
                ", sunyrzzh='" + sunyrzzh + '\'' +
                ", syrzzhxh='" + syrzzhxh + '\'' +
                ", nbuzhhao='" + nbuzhhao + '\'' +
                ", nbuzhzxh='" + nbuzhzxh + '\'' +
                '}';
    }
}
