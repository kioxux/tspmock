package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:担保合同信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:10
 */
public class GuaranteeContrctInfo {
    private List<GuaranteeContrctInfoRecord> record; // 担保合同信息

    public List<GuaranteeContrctInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<GuaranteeContrctInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "GuaranteeContrctInfo{" +
                "record=" + record +
                '}';
    }
}
