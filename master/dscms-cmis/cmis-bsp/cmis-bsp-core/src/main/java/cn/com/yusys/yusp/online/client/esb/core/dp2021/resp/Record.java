package cn.com.yusys.yusp.online.client.esb.core.dp2021.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/15 14:34
 * @since 2021/6/15 14:34
 */
public class Record {
    private String kehuzhlx;//客户账号类型
    private String kehuhaoo;//客户号
    private String kehuzhao;//客户账号
    private String zhanghao;//负债账号
    private String zhhuzwmc;//账户名称
    private String yezztbbz;//余额与总账同步标志
    private String chanpshm;//产品说明
    private String zhhaoxuh;//子账户序号
    private String huobdaih;//货币代号
    private String chaohubz;//账户钞汇标志
    private BigDecimal zhanghye;//账户余额
    private BigDecimal keyongye;//可用余额
    private BigDecimal keyonedu;//可用额度
    private String dmhsbioz;//当面核实标志
    private String zhhuztai;//账户状态
    private String cunqiiii;//存期
    private String qixiriqi;//起息日期
    private String doqiriqi;//到期日期
    private String kaihjigo;//账户开户机构
    private String kaihriqi;//开户日期
    private BigDecimal zhxililv;//当前执行利率
    private String yegxriqi;//余额最近更新日期
    private String chapbhao;//产品编号
    private String cunkzlei;//存款种类
    private String glpinzbz;//关联凭证标志
    private String kaihguiy;//账户开户柜员
    private String suoshudx;//产品所属对象
    private String kehuzhwm;//客户中文名
    private String kehuywmc;//客户英文名
    private String dinhuobz;//产品定活标志
    private String beiyzd01;//备用字段01
    private String beiyzd02;//备用字段02
    private String beiyzd03;//备用字段03
    private BigDecimal beiyye01;//备用余额01
    private BigDecimal beiyye02;//备用余额02
    private BigDecimal beiyye03;//备用余额03
    private String kongzhzt;//账户控制状态
    private String pngzzlei;//凭证种类
    private String pingzhma;//凭证号码
    private String zhcunfsh;//转存方式
    private String zhfutojn;//支付条件
    private String zhhufenl;//账户分类
    private String zhshuxin;//账户属性
    private String zhujigoh;//账户所属机构
    private String tduibzhi;//通兑标志
    private Integer pnzqiyrq;//凭证启用日期
    private String weidzhes;//未登折数
    private String dgdsbzhi;//对公对私标志
    private String xiohriqi;//销户日期
    private String kzjedjbz;//客户账户金额冻结标志
    private String kzfbdjbz;//客户账户封闭冻结标志
    private String kzzsbfbz;//客户账户只收不付标志
    private String kzzfbsbz;//客户账户只付不收标志
    private String kzhuztai;//客户账户状态
    private String zhfbdjbz;//账户封闭冻结标志
    private String zhjedjbz;//账户金额冻结标志
    private String zhzfbsbz;//账户只付不收标志
    private String zhzsbfbz;//账户只收不付标志
    private String dspzsyzt;//凭证状态
    private String kachapbh;//卡产品编号
    private String aiostype;//组合账户形态
    private String shifzhbz;//是否保证金组合产品
    private String xushibzh;//账户虚实标志
    private String dsdzzhfl;//个人电子账户分类
    private String youwkbiz;//有无卡标志
    private String sbkbiaoz;//社保卡标志
    private String cenjleix;//层级类型
    private String tonzbhao;//通知编号
    private BigDecimal tonzjine;//通知金额
    private String tonzriqi;//通知日期
    private String qukuanrq;//取款日期
    private BigDecimal khakjine;//可扣划金额
    private BigDecimal yoxqjine;//优先权金额
    private String ztmiaosu;//状态描述
    private String querenbz;//确认标志
    private String kaihuqud;//开户渠道
    private String shfojshu;//是否结算户

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getYezztbbz() {
        return yezztbbz;
    }

    public void setYezztbbz(String yezztbbz) {
        this.yezztbbz = yezztbbz;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public BigDecimal getKeyongye() {
        return keyongye;
    }

    public void setKeyongye(BigDecimal keyongye) {
        this.keyongye = keyongye;
    }

    public BigDecimal getKeyonedu() {
        return keyonedu;
    }

    public void setKeyonedu(BigDecimal keyonedu) {
        this.keyonedu = keyonedu;
    }

    public String getDmhsbioz() {
        return dmhsbioz;
    }

    public void setDmhsbioz(String dmhsbioz) {
        this.dmhsbioz = dmhsbioz;
    }

    public String getZhhuztai() {
        return zhhuztai;
    }

    public void setZhhuztai(String zhhuztai) {
        this.zhhuztai = zhhuztai;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDoqiriqi() {
        return doqiriqi;
    }

    public void setDoqiriqi(String doqiriqi) {
        this.doqiriqi = doqiriqi;
    }

    public String getKaihjigo() {
        return kaihjigo;
    }

    public void setKaihjigo(String kaihjigo) {
        this.kaihjigo = kaihjigo;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public BigDecimal getZhxililv() {
        return zhxililv;
    }

    public void setZhxililv(BigDecimal zhxililv) {
        this.zhxililv = zhxililv;
    }

    public String getYegxriqi() {
        return yegxriqi;
    }

    public void setYegxriqi(String yegxriqi) {
        this.yegxriqi = yegxriqi;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getGlpinzbz() {
        return glpinzbz;
    }

    public void setGlpinzbz(String glpinzbz) {
        this.glpinzbz = glpinzbz;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getSuoshudx() {
        return suoshudx;
    }

    public void setSuoshudx(String suoshudx) {
        this.suoshudx = suoshudx;
    }

    public String getKehuzhwm() {
        return kehuzhwm;
    }

    public void setKehuzhwm(String kehuzhwm) {
        this.kehuzhwm = kehuzhwm;
    }

    public String getKehuywmc() {
        return kehuywmc;
    }

    public void setKehuywmc(String kehuywmc) {
        this.kehuywmc = kehuywmc;
    }

    public String getDinhuobz() {
        return dinhuobz;
    }

    public void setDinhuobz(String dinhuobz) {
        this.dinhuobz = dinhuobz;
    }

    public String getBeiyzd01() {
        return beiyzd01;
    }

    public void setBeiyzd01(String beiyzd01) {
        this.beiyzd01 = beiyzd01;
    }

    public String getBeiyzd02() {
        return beiyzd02;
    }

    public void setBeiyzd02(String beiyzd02) {
        this.beiyzd02 = beiyzd02;
    }

    public String getBeiyzd03() {
        return beiyzd03;
    }

    public void setBeiyzd03(String beiyzd03) {
        this.beiyzd03 = beiyzd03;
    }

    public BigDecimal getBeiyye01() {
        return beiyye01;
    }

    public void setBeiyye01(BigDecimal beiyye01) {
        this.beiyye01 = beiyye01;
    }

    public BigDecimal getBeiyye02() {
        return beiyye02;
    }

    public void setBeiyye02(BigDecimal beiyye02) {
        this.beiyye02 = beiyye02;
    }

    public BigDecimal getBeiyye03() {
        return beiyye03;
    }

    public void setBeiyye03(BigDecimal beiyye03) {
        this.beiyye03 = beiyye03;
    }

    public String getKongzhzt() {
        return kongzhzt;
    }

    public void setKongzhzt(String kongzhzt) {
        this.kongzhzt = kongzhzt;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getZhcunfsh() {
        return zhcunfsh;
    }

    public void setZhcunfsh(String zhcunfsh) {
        this.zhcunfsh = zhcunfsh;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }

    public String getZhujigoh() {
        return zhujigoh;
    }

    public void setZhujigoh(String zhujigoh) {
        this.zhujigoh = zhujigoh;
    }

    public String getTduibzhi() {
        return tduibzhi;
    }

    public void setTduibzhi(String tduibzhi) {
        this.tduibzhi = tduibzhi;
    }

    public Integer getPnzqiyrq() {
        return pnzqiyrq;
    }

    public void setPnzqiyrq(Integer pnzqiyrq) {
        this.pnzqiyrq = pnzqiyrq;
    }

    public String getWeidzhes() {
        return weidzhes;
    }

    public void setWeidzhes(String weidzhes) {
        this.weidzhes = weidzhes;
    }

    public String getDgdsbzhi() {
        return dgdsbzhi;
    }

    public void setDgdsbzhi(String dgdsbzhi) {
        this.dgdsbzhi = dgdsbzhi;
    }

    public String getXiohriqi() {
        return xiohriqi;
    }

    public void setXiohriqi(String xiohriqi) {
        this.xiohriqi = xiohriqi;
    }

    public String getKzjedjbz() {
        return kzjedjbz;
    }

    public void setKzjedjbz(String kzjedjbz) {
        this.kzjedjbz = kzjedjbz;
    }

    public String getKzfbdjbz() {
        return kzfbdjbz;
    }

    public void setKzfbdjbz(String kzfbdjbz) {
        this.kzfbdjbz = kzfbdjbz;
    }

    public String getKzzsbfbz() {
        return kzzsbfbz;
    }

    public void setKzzsbfbz(String kzzsbfbz) {
        this.kzzsbfbz = kzzsbfbz;
    }

    public String getKzzfbsbz() {
        return kzzfbsbz;
    }

    public void setKzzfbsbz(String kzzfbsbz) {
        this.kzzfbsbz = kzzfbsbz;
    }

    public String getKzhuztai() {
        return kzhuztai;
    }

    public void setKzhuztai(String kzhuztai) {
        this.kzhuztai = kzhuztai;
    }

    public String getZhfbdjbz() {
        return zhfbdjbz;
    }

    public void setZhfbdjbz(String zhfbdjbz) {
        this.zhfbdjbz = zhfbdjbz;
    }

    public String getZhjedjbz() {
        return zhjedjbz;
    }

    public void setZhjedjbz(String zhjedjbz) {
        this.zhjedjbz = zhjedjbz;
    }

    public String getZhzfbsbz() {
        return zhzfbsbz;
    }

    public void setZhzfbsbz(String zhzfbsbz) {
        this.zhzfbsbz = zhzfbsbz;
    }

    public String getZhzsbfbz() {
        return zhzsbfbz;
    }

    public void setZhzsbfbz(String zhzsbfbz) {
        this.zhzsbfbz = zhzsbfbz;
    }

    public String getDspzsyzt() {
        return dspzsyzt;
    }

    public void setDspzsyzt(String dspzsyzt) {
        this.dspzsyzt = dspzsyzt;
    }

    public String getKachapbh() {
        return kachapbh;
    }

    public void setKachapbh(String kachapbh) {
        this.kachapbh = kachapbh;
    }

    public String getAiostype() {
        return aiostype;
    }

    public void setAiostype(String aiostype) {
        this.aiostype = aiostype;
    }

    public String getShifzhbz() {
        return shifzhbz;
    }

    public void setShifzhbz(String shifzhbz) {
        this.shifzhbz = shifzhbz;
    }

    public String getXushibzh() {
        return xushibzh;
    }

    public void setXushibzh(String xushibzh) {
        this.xushibzh = xushibzh;
    }

    public String getDsdzzhfl() {
        return dsdzzhfl;
    }

    public void setDsdzzhfl(String dsdzzhfl) {
        this.dsdzzhfl = dsdzzhfl;
    }

    public String getYouwkbiz() {
        return youwkbiz;
    }

    public void setYouwkbiz(String youwkbiz) {
        this.youwkbiz = youwkbiz;
    }

    public String getSbkbiaoz() {
        return sbkbiaoz;
    }

    public void setSbkbiaoz(String sbkbiaoz) {
        this.sbkbiaoz = sbkbiaoz;
    }

    public String getCenjleix() {
        return cenjleix;
    }

    public void setCenjleix(String cenjleix) {
        this.cenjleix = cenjleix;
    }

    public String getTonzbhao() {
        return tonzbhao;
    }

    public void setTonzbhao(String tonzbhao) {
        this.tonzbhao = tonzbhao;
    }

    public BigDecimal getTonzjine() {
        return tonzjine;
    }

    public void setTonzjine(BigDecimal tonzjine) {
        this.tonzjine = tonzjine;
    }

    public String getTonzriqi() {
        return tonzriqi;
    }

    public void setTonzriqi(String tonzriqi) {
        this.tonzriqi = tonzriqi;
    }

    public String getQukuanrq() {
        return qukuanrq;
    }

    public void setQukuanrq(String qukuanrq) {
        this.qukuanrq = qukuanrq;
    }

    public BigDecimal getKhakjine() {
        return khakjine;
    }

    public void setKhakjine(BigDecimal khakjine) {
        this.khakjine = khakjine;
    }

    public BigDecimal getYoxqjine() {
        return yoxqjine;
    }

    public void setYoxqjine(BigDecimal yoxqjine) {
        this.yoxqjine = yoxqjine;
    }

    public String getZtmiaosu() {
        return ztmiaosu;
    }

    public void setZtmiaosu(String ztmiaosu) {
        this.ztmiaosu = ztmiaosu;
    }

    public String getQuerenbz() {
        return querenbz;
    }

    public void setQuerenbz(String querenbz) {
        this.querenbz = querenbz;
    }

    public String getKaihuqud() {
        return kaihuqud;
    }

    public void setKaihuqud(String kaihuqud) {
        this.kaihuqud = kaihuqud;
    }

    public String getShfojshu() {
        return shfojshu;
    }

    public void setShfojshu(String shfojshu) {
        this.shfojshu = shfojshu;
    }

    @Override
    public String toString() {
        return "Record{" +
                "kehuzhlx='" + kehuzhlx + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", zhanghao='" + zhanghao + '\'' +
                ", zhhuzwmc='" + zhhuzwmc + '\'' +
                ", yezztbbz='" + yezztbbz + '\'' +
                ", chanpshm='" + chanpshm + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", zhanghye=" + zhanghye +
                ", keyongye=" + keyongye +
                ", keyonedu=" + keyonedu +
                ", dmhsbioz='" + dmhsbioz + '\'' +
                ", zhhuztai='" + zhhuztai + '\'' +
                ", cunqiiii='" + cunqiiii + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", doqiriqi='" + doqiriqi + '\'' +
                ", kaihjigo='" + kaihjigo + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", zhxililv=" + zhxililv +
                ", yegxriqi='" + yegxriqi + '\'' +
                ", chapbhao='" + chapbhao + '\'' +
                ", cunkzlei='" + cunkzlei + '\'' +
                ", glpinzbz='" + glpinzbz + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", suoshudx='" + suoshudx + '\'' +
                ", kehuzhwm='" + kehuzhwm + '\'' +
                ", kehuywmc='" + kehuywmc + '\'' +
                ", dinhuobz='" + dinhuobz + '\'' +
                ", beiyzd01='" + beiyzd01 + '\'' +
                ", beiyzd02='" + beiyzd02 + '\'' +
                ", beiyzd03='" + beiyzd03 + '\'' +
                ", beiyye01=" + beiyye01 +
                ", beiyye02=" + beiyye02 +
                ", beiyye03=" + beiyye03 +
                ", kongzhzt='" + kongzhzt + '\'' +
                ", pngzzlei='" + pngzzlei + '\'' +
                ", pingzhma='" + pingzhma + '\'' +
                ", zhcunfsh='" + zhcunfsh + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                ", zhhufenl='" + zhhufenl + '\'' +
                ", zhshuxin='" + zhshuxin + '\'' +
                ", zhujigoh='" + zhujigoh + '\'' +
                ", tduibzhi='" + tduibzhi + '\'' +
                ", pnzqiyrq=" + pnzqiyrq +
                ", weidzhes='" + weidzhes + '\'' +
                ", dgdsbzhi='" + dgdsbzhi + '\'' +
                ", xiohriqi='" + xiohriqi + '\'' +
                ", kzjedjbz='" + kzjedjbz + '\'' +
                ", kzfbdjbz='" + kzfbdjbz + '\'' +
                ", kzzsbfbz='" + kzzsbfbz + '\'' +
                ", kzzfbsbz='" + kzzfbsbz + '\'' +
                ", kzhuztai='" + kzhuztai + '\'' +
                ", zhfbdjbz='" + zhfbdjbz + '\'' +
                ", zhjedjbz='" + zhjedjbz + '\'' +
                ", zhzfbsbz='" + zhzfbsbz + '\'' +
                ", zhzsbfbz='" + zhzsbfbz + '\'' +
                ", dspzsyzt='" + dspzsyzt + '\'' +
                ", kachapbh='" + kachapbh + '\'' +
                ", aiostype='" + aiostype + '\'' +
                ", shifzhbz='" + shifzhbz + '\'' +
                ", xushibzh='" + xushibzh + '\'' +
                ", dsdzzhfl='" + dsdzzhfl + '\'' +
                ", youwkbiz='" + youwkbiz + '\'' +
                ", sbkbiaoz='" + sbkbiaoz + '\'' +
                ", cenjleix='" + cenjleix + '\'' +
                ", tonzbhao='" + tonzbhao + '\'' +
                ", tonzjine=" + tonzjine +
                ", tonzriqi='" + tonzriqi + '\'' +
                ", qukuanrq='" + qukuanrq + '\'' +
                ", khakjine=" + khakjine +
                ", yoxqjine=" + yoxqjine +
                ", ztmiaosu='" + ztmiaosu + '\'' +
                ", querenbz='" + querenbz + '\'' +
                ", kaihuqud='" + kaihuqud + '\'' +
                ", shfojshu='" + shfojshu + '\'' +
                '}';
    }
}
