package cn.com.yusys.yusp.web.server.biz.xdcz0013;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0013.req.Xdcz0013ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0013.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0013.resp.Xdcz0013RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0013.req.Xdcz0013DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0013.resp.Xdcz0013DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:企业网银出票申请额度校验
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0013:企业网银出票申请额度校验")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0013Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0013
     * 交易描述：企业网银出票申请额度校验
     *
     * @param xdcz0013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("企业网银出票申请额度校验")
    @PostMapping("/xdcz0013")
   //@Idempotent({"xdcz0013", "#xdcz0013ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0013RespDto xdcz0013(@Validated @RequestBody Xdcz0013ReqDto xdcz0013ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013ReqDto));
        Xdcz0013DataReqDto xdcz0013DataReqDto = new Xdcz0013DataReqDto();// 请求Data： 电子保函开立
        Xdcz0013DataRespDto xdcz0013DataRespDto = new Xdcz0013DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0013.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0013RespDto xdcz0013RespDto = new Xdcz0013RespDto();
        try {
            // 从 xdcz0013ReqDto获取 reqData
            reqData = xdcz0013ReqDto.getData();
            // 将 reqData 拷贝到xdcz0013DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0013DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataReqDto));
            ResultDto<Xdcz0013DataRespDto> xdcz0013DataResultDto = dscmsBizCzClientService.xdcz0013(xdcz0013DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0013RespDto.setErorcd(Optional.ofNullable(xdcz0013DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0013RespDto.setErortx(Optional.ofNullable(xdcz0013DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0013DataResultDto.getCode())) {
                xdcz0013DataRespDto = xdcz0013DataResultDto.getData();
                xdcz0013RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0013RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0013DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0013DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, e.getMessage());
            xdcz0013RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0013RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0013RespDto.setDatasq(servsq);

        xdcz0013RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0013.key, DscmsEnum.TRADE_CODE_XDCZ0013.value, JSON.toJSONString(xdcz0013RespDto));
        return xdcz0013RespDto;
    }
}
