package cn.com.yusys.yusp.online.client.esb.core.ln3005.resp.lstcpsyfw;

/**
 * 响应Service：机构控制信息对象
 * @author lihh
 * @version 1.0
 */
public class Record {

    private String xzhileix;//限制类型
    private String shiyleix;//适用类型
    private String jigshybz;//机构适用标志
    private String shiyfanw;//适用范围

    public String getXzhileix() {
        return xzhileix;
    }

    public void setXzhileix(String xzhileix) {
        this.xzhileix = xzhileix;
    }

    public String getShiyleix() {
        return shiyleix;
    }

    public void setShiyleix(String shiyleix) {
        this.shiyleix = shiyleix;
    }

    public String getJigshybz() {
        return jigshybz;
    }

    public void setJigshybz(String jigshybz) {
        this.jigshybz = jigshybz;
    }

    public String getShiyfanw() {
        return shiyfanw;
    }

    public void setShiyfanw(String shiyfanw) {
        this.shiyfanw = shiyfanw;
    }

    @Override
    public String toString() {
        return "Record{" +
                "xzhileix='" + xzhileix + '\'' +
                "shiyleix='" + shiyleix + '\'' +
                "jigshybz='" + jigshybz + '\'' +
                "shiyfanw='" + shiyfanw + '\'' +
                '}';
    }
}
