package cn.com.yusys.yusp.web.server.biz.xddb0019;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0019.req.Xddb0019ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0019.resp.Xddb0019RespDto;
import cn.com.yusys.yusp.dto.server.xddb0019.req.Xddb0019DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0019.resp.Xddb0019DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:解质押押品校验
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0019:解质押押品校验")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0019Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0019Resource.class);
	@Autowired
	private DscmsBizDbClientService dscmsBizDbClientService;
    /**
     * 交易码：xddb0019
     * 交易描述：解质押押品校验
     *
     * @param xddb0019ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("解质押押品校验")
    @PostMapping("/xddb0019")
    //@Idempotent({"xddb0019", "#xddb0019ReqDto.datasq"})
    protected @ResponseBody
	Xddb0019RespDto xddb0019(@Validated @RequestBody Xddb0019ReqDto xddb0019ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, JSON.toJSONString(xddb0019ReqDto));
        Xddb0019DataReqDto xddb0019DataReqDto = new Xddb0019DataReqDto();// 请求Data： 解质押押品校验
        Xddb0019DataRespDto xddb0019DataRespDto = new Xddb0019DataRespDto();// 响应Data：解质押押品校验
		Xddb0019RespDto xddb0019RespDto=new Xddb0019RespDto();
		cn.com.yusys.yusp.dto.server.biz.xddb0019.req.Data reqData = null; // 请求Data：解质押押品校验
		cn.com.yusys.yusp.dto.server.biz.xddb0019.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0019.resp.Data();// 响应Data：解质押押品校验
        try {
            // 从 xddb0019ReqDto获取 reqData
            reqData = xddb0019ReqDto.getData();
            // 将 reqData 拷贝到xddb0019DataReqDto
            BeanUtils.copyProperties(reqData, xddb0019DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, JSON.toJSONString(xddb0019DataReqDto));
            ResultDto<Xddb0019DataRespDto> xddb0019DataResultDto = dscmsBizDbClientService.xddb0019(xddb0019DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, JSON.toJSONString(xddb0019DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0019RespDto.setErorcd(Optional.ofNullable(xddb0019DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0019RespDto.setErortx(Optional.ofNullable(xddb0019DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0019DataResultDto.getCode())) {
                xddb0019DataRespDto = xddb0019DataResultDto.getData();
                xddb0019RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0019RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0019DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0019DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, e.getMessage());
            xddb0019RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0019RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0019RespDto.setDatasq(servsq);

        xddb0019RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0019.key, DscmsEnum.TRADE_CODE_XDDB0019.value, JSON.toJSONString(xddb0019RespDto));
        return xddb0019RespDto;
    }
}
