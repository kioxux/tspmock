package cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req;

/**
 * 请求Service：票据承兑签发审批请求
 */
public class Service {
	private String prcscd;//    交易码
	private String servtp;//    渠道
	private String servsq;//    渠道流水
	private String userid;//    柜员号
	private String brchno;//    部门号
	private String txdate;//    交易日期
	private String txtime;//    交易时间
	private String datasq;//    全局流水

    private String txCode;//交易码
    private String sBatchNo;// 出池批次号
    private String billno;//票据编号
    private String cusid;//客户编号

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getTxdate() {
		return txdate;
	}

	public void setTxdate(String txdate) {
		this.txdate = txdate;
	}

	public String getTxtime() {
		return txtime;
	}

	public void setTxtime(String txtime) {
		this.txtime = txtime;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getTxCode() {
		return txCode;
	}

	public void setTxCode(String txCode) {
		this.txCode = txCode;
	}

	public String getsBatchNo() {
		return sBatchNo;
	}

	public void setsBatchNo(String sBatchNo) {
		this.sBatchNo = sBatchNo;
	}

	public String getBillno() {
		return billno;
	}

	public void setBillno(String billno) {
		this.billno = billno;
	}

	public String getCusid() {
		return cusid;
	}

	public void setCusid(String cusid) {
		this.cusid = cusid;
	}

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", txdate='" + txdate + '\'' +
				", txtime='" + txtime + '\'' +
				", datasq='" + datasq + '\'' +
				", txCode='" + txCode + '\'' +
				", sBatchNo='" + sBatchNo + '\'' +
				", billno='" + billno + '\'' +
				", cusid='" + cusid + '\'' +
				'}';
	}
}
