package cn.com.yusys.yusp.online.client.esb.core.da3306.req;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产拨备计提
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间


    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private BigDecimal jitijine;//计提金额
    private BigDecimal dzzcrzjz;//抵债资产入账价值
    private BigDecimal pingjiaz;//评估价值
    private String scbbjtrq;//上次拨备计提日期
    private BigDecimal scjzzbje;//上次减值准备金额

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getJitijine() {
        return jitijine;
    }

    public void setJitijine(BigDecimal jitijine) {
        this.jitijine = jitijine;
    }

    public BigDecimal getDzzcrzjz() {
        return dzzcrzjz;
    }

    public void setDzzcrzjz(BigDecimal dzzcrzjz) {
        this.dzzcrzjz = dzzcrzjz;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public String getScbbjtrq() {
        return scbbjtrq;
    }

    public void setScbbjtrq(String scbbjtrq) {
        this.scbbjtrq = scbbjtrq;
    }

    public BigDecimal getScjzzbje() {
        return scjzzbje;
    }

    public void setScjzzbje(BigDecimal scjzzbje) {
        this.scjzzbje = scjzzbje;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", jitijine=" + jitijine +
                ", dzzcrzjz=" + dzzcrzjz +
                ", pingjiaz=" + pingjiaz +
                ", scbbjtrq='" + scbbjtrq + '\'' +
                ", scjzzbje=" + scjzzbje +
                '}';
    }
}
