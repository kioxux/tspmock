package cn.com.yusys.yusp.web.server.biz.xdcz0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0016.req.Xdcz0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0016.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0016.resp.Xdcz0016RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0016.req.Xdcz0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0016.resp.Xdcz0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:企业网银查询影像补录批次
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0016:企业网银出票申请额度校验")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0016Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0016
     * 交易描述：企业网银查询影像补录批次
     *
     * @param xdcz0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("企业网银查询影像补录批次")
    @PostMapping("/xdcz0016")
   //@Idempotent({"xdcz0016", "#xdcz0016ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0016RespDto xdcz0016(@Validated @RequestBody Xdcz0016ReqDto xdcz0016ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016ReqDto));
        Xdcz0016DataReqDto xdcz0016DataReqDto = new Xdcz0016DataReqDto();// 请求Data： 电子保函开立
        Xdcz0016DataRespDto xdcz0016DataRespDto = new Xdcz0016DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0016.req.Data reqData = null; // 请求Data：电子保函开立
        Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0016RespDto xdcz0016RespDto = new Xdcz0016RespDto();
        try {
            // 从 xdcz0016ReqDto获取 reqData
            reqData = xdcz0016ReqDto.getData();
            // 将 reqData 拷贝到xdcz0016DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0016DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016DataReqDto));
            ResultDto<Xdcz0016DataRespDto> xdcz0016DataResultDto = dscmsBizCzClientService.xdcz0016(xdcz0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0016RespDto.setErorcd(Optional.ofNullable(xdcz0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0016RespDto.setErortx(Optional.ofNullable(xdcz0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0016DataResultDto.getCode())) {
                xdcz0016DataRespDto = xdcz0016DataResultDto.getData();
                xdcz0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0016DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, e.getMessage());
            xdcz0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0016RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0016RespDto.setDatasq(servsq);

        xdcz0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0016.key, DscmsEnum.TRADE_CODE_XDCZ0016.value, JSON.toJSONString(xdcz0016RespDto));
        return xdcz0016RespDto;
    }
}
