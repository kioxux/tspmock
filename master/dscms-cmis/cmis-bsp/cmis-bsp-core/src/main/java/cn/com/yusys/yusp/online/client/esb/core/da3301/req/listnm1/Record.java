package cn.com.yusys.yusp.online.client.esb.core.da3301.req.listnm1;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产入账
 */
public class Record {
    private String dkjiejuh;//贷款借据号
    private BigDecimal ysyjlixi;//应收应计利息
    private BigDecimal ysqianxi;//应收欠息
    private BigDecimal ysyjfaxi;//应收应计罚息
    private BigDecimal yshofaxi;//应收罚息
    private BigDecimal yingjifx;//应计复息
    private BigDecimal fuxiiiii;//复息
    private BigDecimal yingshfj;//应收罚金
    private BigDecimal yingshfy;//应收费用
    private BigDecimal yihxbjlx;//已核销本金利息
    private BigDecimal hexiaolx;//核销利息
    private BigDecimal hexiaobj;//核销本金
    private BigDecimal zhchbjin;//正常本金
    private BigDecimal yuqibjin;//逾期本金
    private BigDecimal dzhibjin;//呆滞本金
    private BigDecimal daizbjin;//呆账本金
    private BigDecimal csyjlixi;//催收应计利息
    private BigDecimal csqianxi;//催收欠息
    private BigDecimal csyjfaxi;//催收应计罚息
    private BigDecimal cshofaxi;//催收罚息

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

	@Override
	public String toString() {
		return "Record{" +
				"dkjiejuh='" + dkjiejuh + '\'' +
				", ysyjlixi=" + ysyjlixi +
				", ysqianxi=" + ysqianxi +
				", ysyjfaxi=" + ysyjfaxi +
				", yshofaxi=" + yshofaxi +
				", yingjifx=" + yingjifx +
				", fuxiiiii=" + fuxiiiii +
				", yingshfj=" + yingshfj +
				", yingshfy=" + yingshfy +
				", yihxbjlx=" + yihxbjlx +
				", hexiaolx=" + hexiaolx +
				", hexiaobj=" + hexiaobj +
				", zhchbjin=" + zhchbjin +
				", yuqibjin=" + yuqibjin +
				", dzhibjin=" + dzhibjin +
				", daizbjin=" + daizbjin +
				", csyjlixi=" + csyjlixi +
				", csqianxi=" + csqianxi +
				", csyjfaxi=" + csyjfaxi +
				", cshofaxi=" + cshofaxi +
				'}';
	}
}
