package cn.com.yusys.yusp.web.client.esb.ypxt.lmtinf;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.LmtinfReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.Service;
import cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.resp.LmtinfRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:信贷授信协议信息同步
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用押品系统的接口(lmtinf)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2LmtinfResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2LmtinfResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：lmtinf
     * 交易描述：信贷授信协议信息同步
     *
     * @param lmtinfReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("lmtinf:信贷授信协议信息同步")
    @PostMapping("/lmtinf")
    protected @ResponseBody
    ResultDto<LmtinfRespDto> lmtinf(@Validated @RequestBody LmtinfReqDto lmtinfReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value, JSON.toJSONString(lmtinfReqDto));
        Service reqService = new Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.resp.Service();
        LmtinfReqService lmtinfReqService = new LmtinfReqService();
        LmtinfRespService lmtinfRespService = new LmtinfRespService();
        LmtinfRespDto lmtinfRespDto = new LmtinfRespDto();
        ResultDto<LmtinfRespDto> lmtinfResultDto = new ResultDto<LmtinfRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将lmtinfReqDto转换成reqService
            BeanUtils.copyProperties(lmtinfReqDto, reqService);
            // 循环相关的判断开始
            java.util.List<cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.List> lmtinfReqListDtos = Optional.ofNullable(lmtinfReqDto.getList()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.List>());
            if (CollectionUtils.nonEmpty(lmtinfReqListDtos)) {
                cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.List lmtinfReqListService = new cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.List();
                java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.Record> lmtinfReqRecords = lmtinfReqListDtos.parallelStream().map(lmtinfReqListDto -> {
                    cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.Record();
                    BeanUtils.copyProperties(lmtinfReqListDto, record);
                    return record;
                }).collect(Collectors.toList());
                lmtinfReqListService.setRecord(lmtinfReqRecords);
                reqService.setList(lmtinfReqListService);
            }
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LMTINF.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            lmtinfReqService.setService(reqService);
            // 将lmtinfReqService转换成lmtinfReqServiceMap
            Map lmtinfReqServiceMap = beanMapUtil.beanToMap(lmtinfReqService);
            context.put("tradeDataMap", lmtinfReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value, JSON.toJSONString(lmtinfReqDto));
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_EVALYP.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value, JSON.toJSONString(result));
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            lmtinfRespService = beanMapUtil.mapToBean(tradeDataMap, LmtinfRespService.class, LmtinfRespService.class);
            respService = lmtinfRespService.getService();
            //  将respService转换成LmtinfRespDto
            lmtinfResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            lmtinfResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                BeanUtils.copyProperties(respService, lmtinfRespDto);
                lmtinfResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                lmtinfResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                lmtinfResultDto.setCode(EpbEnum.EPB099999.key);
                lmtinfResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value, e.getMessage());
            lmtinfResultDto.setCode(EpbEnum.EPB099999.key);//9999
            lmtinfResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        lmtinfResultDto.setData(lmtinfRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LMTINF.key, EsbEnum.TRADE_CODE_LMTINF.value, JSON.toJSONString(lmtinfReqDto));
        return lmtinfResultDto;
    }
}
