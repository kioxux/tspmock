package cn.com.yusys.yusp.online.client.esb.core.da3303.req;

import java.util.List;

/**
 * 请求Service：抵债资产信息维护
 */
public class Lstweihsr_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3303.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstweihsr_ARRAY{" +
                "record=" + record +
                '}';
    }
}
