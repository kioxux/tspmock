package cn.com.yusys.yusp.online.client.esb.core.da3320.resp.lstdzzccz;

import java.math.BigDecimal;
/**
 * 响应Service：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String dzzcbhao;//抵债资产编号
    private String shiftanx;//是否摊销
    private String txrzzhqi;//摊销入账周期
    private BigDecimal mctxjine;//每次摊销金额
    private BigDecimal tanxzjie;//摊销总额
    private BigDecimal czytanje;//已摊销金额
    private BigDecimal czdtanje;//待摊销金额
    private BigDecimal chuzshru;//出租收入金额
    private BigDecimal chuzzhch;//出租支出金额
    private String qishriqi;//起始日期
    private String daoqriqi;//到期日期
    private String xctanxrq;//下次摊销日期
    private String fukzhhao;//付款账号
    private String zhanghxh;//账号子序号

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getShiftanx() {
        return shiftanx;
    }

    public void setShiftanx(String shiftanx) {
        this.shiftanx = shiftanx;
    }

    public String getTxrzzhqi() {
        return txrzzhqi;
    }

    public void setTxrzzhqi(String txrzzhqi) {
        this.txrzzhqi = txrzzhqi;
    }

    public BigDecimal getMctxjine() {
        return mctxjine;
    }

    public void setMctxjine(BigDecimal mctxjine) {
        this.mctxjine = mctxjine;
    }

    public BigDecimal getTanxzjie() {
        return tanxzjie;
    }

    public void setTanxzjie(BigDecimal tanxzjie) {
        this.tanxzjie = tanxzjie;
    }

    public BigDecimal getCzytanje() {
        return czytanje;
    }

    public void setCzytanje(BigDecimal czytanje) {
        this.czytanje = czytanje;
    }

    public BigDecimal getCzdtanje() {
        return czdtanje;
    }

    public void setCzdtanje(BigDecimal czdtanje) {
        this.czdtanje = czdtanje;
    }

    public BigDecimal getChuzshru() {
        return chuzshru;
    }

    public void setChuzshru(BigDecimal chuzshru) {
        this.chuzshru = chuzshru;
    }

    public BigDecimal getChuzzhch() {
        return chuzzhch;
    }

    public void setChuzzhch(BigDecimal chuzzhch) {
        this.chuzzhch = chuzzhch;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getXctanxrq() {
        return xctanxrq;
    }

    public void setXctanxrq(String xctanxrq) {
        this.xctanxrq = xctanxrq;
    }

    public String getFukzhhao() {
        return fukzhhao;
    }

    public void setFukzhhao(String fukzhhao) {
        this.fukzhhao = fukzhhao;
    }

    public String getZhanghxh() {
        return zhanghxh;
    }

    public void setZhanghxh(String zhanghxh) {
        this.zhanghxh = zhanghxh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", shiftanx='" + shiftanx + '\'' +
                ", txrzzhqi='" + txrzzhqi + '\'' +
                ", mctxjine=" + mctxjine +
                ", tanxzjie=" + tanxzjie +
                ", czytanje=" + czytanje +
                ", czdtanje=" + czdtanje +
                ", chuzshru=" + chuzshru +
                ", chuzzhch=" + chuzzhch +
                ", qishriqi='" + qishriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", xctanxrq='" + xctanxrq + '\'' +
                ", fukzhhao='" + fukzhhao + '\'' +
                ", zhanghxh='" + zhanghxh + '\'' +
                '}';
    }
}
