package cn.com.yusys.yusp.web.server.biz.xdht0026;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0026.req.Xdht0026ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0026.resp.Xdht0026RespDto;
import cn.com.yusys.yusp.dto.server.xdht0026.req.Xdht0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0026.resp.Xdht0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据zheng获取信贷合同信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0026:根据zheng获取信贷合同信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0026Resource.class);
    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0026
     * 交易描述：根据zheng获取信贷合同信息
     *
     * @param xdht0026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据zheng获取信贷合同信息")
    @PostMapping("/xdht0026")
    //@Idempotent({"xdcaht0026", "#xdht0026ReqDto.datasq"})
    protected @ResponseBody
    Xdht0026RespDto xdht0026(@Validated @RequestBody Xdht0026ReqDto xdht0026ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026ReqDto));
        Xdht0026DataReqDto xdht0026DataReqDto = new Xdht0026DataReqDto(); // 请求Data： 根据zheng获取信贷合同信息
        Xdht0026DataRespDto xdht0026DataRespDto = new Xdht0026DataRespDto();// 响应Data：根据zheng获取信贷合同信息
        Xdht0026RespDto xdht0026RespDto = new Xdht0026RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0026.req.Data reqData = null; // 请求Data：根据zheng获取信贷合同信息
        cn.com.yusys.yusp.dto.server.biz.xdht0026.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0026.resp.Data();// 响应Data：根据zheng获取信贷合同信息
        try {
            // 从 xdht0026ReqDto获取 reqData
            reqData = xdht0026ReqDto.getData();
            // 将 reqData 拷贝到xdht0026DataReqDto
            BeanUtils.copyProperties(reqData, xdht0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026DataReqDto));
            ResultDto<Xdht0026DataRespDto> xdht0026DataResultDto = dscmsBizHtClientService.xdht0026(xdht0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0026RespDto.setErorcd(Optional.ofNullable(xdht0026DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0026RespDto.setErortx(Optional.ofNullable(xdht0026DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0026DataResultDto.getCode())) {
                xdht0026DataRespDto = xdht0026DataResultDto.getData();
                xdht0026RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0026RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0026DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0026DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, e.getMessage());
            xdht0026RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0026RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0026RespDto.setDatasq(servsq);

        xdht0026RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0026.key, DscmsEnum.TRADE_CODE_XDHT0026.value, JSON.toJSONString(xdht0026RespDto));
        return xdht0026RespDto;
    }
}
