package cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.resp;

/**
 * 响应Service：ESB信贷查询接口（处理码credi1）
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Credi1RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
