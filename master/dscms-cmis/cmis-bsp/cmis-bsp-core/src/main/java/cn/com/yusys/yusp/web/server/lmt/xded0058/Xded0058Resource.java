package cn.com.yusys.yusp.web.server.lmt.xded0058;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmislmt0058.req.CmisLmt0058ReqDto;
import cn.com.yusys.yusp.dto.server.cmislmt0058.resp.CmisLmt0058RespDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0058.req.RecoverListDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0058.req.Xded0058ReqDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0058.resp.Xded0058RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:转贴现业务额度恢复
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDED0058:转贴现业务额度恢复")
@RestController
@RequestMapping("/api/dscms")
public class Xded0058Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xded0058Resource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：XDED0058
     * 交易描述：转贴现业务额度恢复
     *
     * @param xded0058ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("转贴现业务额度恢复")
    @PostMapping("/xded0058")
    //  @Idempotent({"xded0058", "#xded0058ReqDto.datasq"})
    protected @ResponseBody
    Xded0058RespDto cmisLmt0058(@Validated @RequestBody Xded0058ReqDto xded0058ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0058.key, DscmsEnum.TRADE_CODE_XDED0058.value, JSON.toJSONString(xded0058ReqDto));
        CmisLmt0058ReqDto cmisLmt0058ReqDto = new CmisLmt0058ReqDto();// 请求Data： 转贴现业务额度恢复
        CmisLmt0058RespDto cmisLmt0058RespDto = new CmisLmt0058RespDto();// 响应Data：转贴现业务额度恢复

        Xded0058RespDto xded0058RespDto = new Xded0058RespDto();

        cn.com.yusys.yusp.dto.server.lmt.xded0058.req.Data reqData = null; // 请求Data：转贴现业务额度恢复
        cn.com.yusys.yusp.dto.server.lmt.xded0058.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.xded0058.resp.Data();// 响应Data：转贴现业务额度恢复

        try {
            // 从 Xded0058Dto获取 reqData
            reqData = xded0058ReqDto.getData();
            // 将 reqData 拷贝到cmisLmt0058DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0058ReqDto);
            List<RecoverListDto> recoverListDtos = reqData.getRecoverListDtoList();
            if (CollectionUtils.nonEmpty(recoverListDtos)) {
                List<cn.com.yusys.yusp.dto.server.cmislmt0058.req.RecoverListDto> collect = recoverListDtos.parallelStream().map(deal -> {
                    cn.com.yusys.yusp.dto.server.cmislmt0058.req.RecoverListDto cmisLmt0058DealBizListReqDto = new cn.com.yusys.yusp.dto.server.cmislmt0058.req.RecoverListDto();
                    BeanUtils.copyProperties(deal, cmisLmt0058DealBizListReqDto);
                    return cmisLmt0058DealBizListReqDto;
                }).collect(Collectors.toList());
                cmisLmt0058ReqDto.setRecoverListDtoList(collect);
            }

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value, JSON.toJSONString(cmisLmt0058ReqDto));
            ResultDto<CmisLmt0058RespDto> cmisLmt0058RespDtoResultDto = cmisLmtClientService.cmislmt0058(cmisLmt0058ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0058.value, JSON.toJSONString(cmisLmt0058RespDtoResultDto));
            // 从返回值中获取响应码和响应消息
            xded0058RespDto.setErorcd(Optional.ofNullable(cmisLmt0058RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xded0058RespDto.setErortx(Optional.ofNullable(cmisLmt0058RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0058RespDtoResultDto.getCode())) {
                cmisLmt0058RespDto = cmisLmt0058RespDtoResultDto.getData();
                xded0058RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xded0058RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0058DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0058RespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0058.key, DscmsEnum.TRADE_CODE_XDED0058.value, e.getMessage());
            xded0058RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xded0058RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xded0058RespDto.setDatasq(servsq);

        xded0058RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0058.key, DscmsEnum.TRADE_CODE_XDED0058.value, JSON.toJSONString(xded0058RespDto));
        return xded0058RespDto;
    }
}
