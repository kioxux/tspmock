package cn.com.yusys.yusp.web.server.biz.xdzc0015;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0015.req.Xdzc0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0015.resp.Xdzc0015RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0015.req.Xdzc0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0015.resp.Xdzc0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:保证金查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0015:保证金查询接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0015Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0015
     * 交易描述：保证金查询接口
     *
     * @param xdzc0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金查询接口")
    @PostMapping("/xdzc0015")
    //@Idempotent({"xdzc015", "#xdzc0015ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0015RespDto xdzc0015(@Validated @RequestBody Xdzc0015ReqDto xdzc0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, JSON.toJSONString(xdzc0015ReqDto));
        Xdzc0015DataReqDto xdzc0015DataReqDto = new Xdzc0015DataReqDto();// 请求Data： 保证金查询接口
        Xdzc0015DataRespDto xdzc0015DataRespDto = new Xdzc0015DataRespDto();// 响应Data：保证金查询接口
        Xdzc0015RespDto xdzc0015RespDto = new Xdzc0015RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0015.req.Data reqData = null; // 请求Data：保证金查询接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0015.resp.Data();// 响应Data：保证金查询接口
        try {
            // 从 xdzc0015ReqDto获取 reqData
            reqData = xdzc0015ReqDto.getData();
            // 将 reqData 拷贝到xdzc0015DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0015DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, JSON.toJSONString(xdzc0015DataReqDto));
            ResultDto<Xdzc0015DataRespDto> xdzc0015DataResultDto = dscmsBizZcClientService.xdzc0015(xdzc0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, JSON.toJSONString(xdzc0015DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0015RespDto.setErorcd(Optional.ofNullable(xdzc0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0015RespDto.setErortx(Optional.ofNullable(xdzc0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0015DataResultDto.getCode())) {
                xdzc0015DataRespDto = xdzc0015DataResultDto.getData();
                xdzc0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, e.getMessage());
            xdzc0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0015RespDto.setDatasq(servsq);

        xdzc0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0015.key, DscmsEnum.TRADE_CODE_XDZC0015.value, JSON.toJSONString(xdzc0015RespDto));
        return xdzc0015RespDto;
    }
}
