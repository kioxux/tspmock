package cn.com.yusys.yusp.online.client.esb.lcxt.lc0323.req;

/**
 * 请求Service：查询理财是否冻结
 * @author code-generator
 * @version 1.0             
 */      
public class Lc0323ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
