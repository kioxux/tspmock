package cn.com.yusys.yusp.web.client.esb.core.ln3064;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3064.req.Ln3064ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3064.resp.Ln3064RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3064.req.Ln3064ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3064.resp.Ln3064RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3064.resp.LstNbjjInfo;
import cn.com.yusys.yusp.online.client.esb.core.ln3064.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3064)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3064Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3064Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3064
     * 交易描述：资产转让资金划转
     *
     * @param ln3064ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3064:资产转让资金划转")
    @PostMapping("/ln3064")
    protected @ResponseBody
    ResultDto<Ln3064RespDto> ln3064(@Validated @RequestBody Ln3064ReqDto ln3064ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3064.key, EsbEnum.TRADE_CODE_LN3064.value, JSON.toJSONString(ln3064ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3064.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3064.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3064.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3064.resp.Service();

        Ln3064ReqService ln3064ReqService = new Ln3064ReqService();
        Ln3064RespService ln3064RespService = new Ln3064RespService();
        Ln3064RespDto ln3064RespDto = new Ln3064RespDto();
        ResultDto<Ln3064RespDto> ln3064ResultDto = new ResultDto<Ln3064RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3064ReqDto转换成reqService
            BeanUtils.copyProperties(ln3064ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3064.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3064ReqService.setService(reqService);
            // 将ln3064ReqService转换成ln3064ReqServiceMap
            Map ln3064ReqServiceMap = beanMapUtil.beanToMap(ln3064ReqService);
            context.put("tradeDataMap", ln3064ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3064.key, EsbEnum.TRADE_CODE_LN3064.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3064.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3064.key, EsbEnum.TRADE_CODE_LN3064.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3064RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3064RespService.class, Ln3064RespService.class);
            respService = ln3064RespService.getService();

            ln3064ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3064ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3064RespDto
                BeanUtils.copyProperties(respService, ln3064RespDto);
                LstNbjjInfo lstNbjjInfo = Optional.ofNullable(respService.getLstNbjjInfo()).orElse(new LstNbjjInfo());
                if (CollectionUtils.nonEmpty(lstNbjjInfo.getRecord())) {
                    List<Record> recordList = lstNbjjInfo.getRecord();
                    List<cn.com.yusys.yusp.dto.client.esb.core.ln3064.resp.LstNbjjInfo> targetList = new ArrayList<>();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3064.resp.LstNbjjInfo target = new cn.com.yusys.yusp.dto.client.esb.core.ln3064.resp.LstNbjjInfo();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    ln3064RespDto.setLstNbjjInfo(targetList);
                }
                ln3064ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3064ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3064ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3064ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3064.key, EsbEnum.TRADE_CODE_LN3064.value, e.getMessage());
            ln3064ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3064ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3064ResultDto.setData(ln3064RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3064.key, EsbEnum.TRADE_CODE_LN3064.value, JSON.toJSONString(ln3064ResultDto));
        return ln3064ResultDto;
    }
}
