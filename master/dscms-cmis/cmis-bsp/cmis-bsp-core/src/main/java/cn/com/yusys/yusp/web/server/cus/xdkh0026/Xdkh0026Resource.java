package cn.com.yusys.yusp.web.server.cus.xdkh0026;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0026.req.Xdkh0026ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0026.resp.Xdkh0026RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0026.req.Xdkh0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0026.resp.Xdkh0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优企贷、优农贷行内关联人基本信息查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0026:优企贷、优农贷行内关联人基本信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0026Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0026
     * 交易描述：优企贷、优农贷行内关联人基本信息查询
     *
     * @param xdkh0026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优企贷、优农贷行内关联人基本信息查询")
    @PostMapping("/xdkh0026")
    //@Idempotent({"xdcakh0026", "#xdkh0026ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0026RespDto xdkh0026(@Validated @RequestBody Xdkh0026ReqDto xdkh0026ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026ReqDto));
        Xdkh0026DataReqDto xdkh0026DataReqDto = new Xdkh0026DataReqDto();// 请求Data： 优企贷、优农贷行内关联人基本信息查询
        Xdkh0026DataRespDto xdkh0026DataRespDto = new Xdkh0026DataRespDto();// 响应Data：优企贷、优农贷行内关联人基本信息查询
        Xdkh0026RespDto xdkh0026RespDto = new Xdkh0026RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0026.req.Data reqData = null; // 请求Data：优企贷、优农贷行内关联人基本信息查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0026.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0026.resp.Data();// 响应Data：优企贷、优农贷行内关联人基本信息查询
        try {
            // 从 xdkh0026ReqDto获取 reqData
            reqData = xdkh0026ReqDto.getData();
            // 将 reqData 拷贝到xdkh0026DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0026DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataReqDto));
            ResultDto<Xdkh0026DataRespDto> xdkh0026DataResultDto = dscmsCusClientService.xdkh0026(xdkh0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0026RespDto.setErorcd(Optional.ofNullable(xdkh0026DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0026RespDto.setErortx(Optional.ofNullable(xdkh0026DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0026DataResultDto.getCode())) {
                xdkh0026DataRespDto = xdkh0026DataResultDto.getData();
                xdkh0026RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0026RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0026DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0026DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, e.getMessage());
            xdkh0026RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0026RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0026RespDto.setDatasq(servsq);

        xdkh0026RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0026.key, DscmsEnum.TRADE_CODE_XDKH0026.value, JSON.toJSONString(xdkh0026RespDto));
        return xdkh0026RespDto;
    }
}