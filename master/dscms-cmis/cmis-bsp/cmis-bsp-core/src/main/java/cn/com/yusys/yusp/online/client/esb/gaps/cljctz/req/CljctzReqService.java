package cn.com.yusys.yusp.online.client.esb.gaps.cljctz.req;

/**
 * 请求Service：连云港存量房列表
 *
 * @author chenyong
 * @version 1.0
 */
public class CljctzReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
