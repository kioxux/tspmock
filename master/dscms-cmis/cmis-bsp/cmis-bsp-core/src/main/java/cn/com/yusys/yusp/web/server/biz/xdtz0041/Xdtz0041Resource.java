package cn.com.yusys.yusp.web.server.biz.xdtz0041;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0041.req.Xdtz0041ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0041.resp.Xdtz0041RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0041.req.Xdtz0041DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0041.resp.Xdtz0041DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号前往信贷查找房贷借据信息
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0041:根据客户号前往信贷查找房贷借据信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0041Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0041Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0041
     * 交易描述：根据客户号前往信贷查找房贷借据信息
     *
     * @param xdtz0041ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号前往信贷查找房贷借据信息")
    @PostMapping("/xdtz0041")
    //@Idempotent({"xdcatz0041", "#xdtz0041ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0041RespDto xdtz0041(@Validated @RequestBody Xdtz0041ReqDto xdtz0041ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041ReqDto));
        Xdtz0041DataReqDto xdtz0041DataReqDto = new Xdtz0041DataReqDto();// 请求Data： 根据客户号前往信贷查找房贷借据信息
        Xdtz0041DataRespDto xdtz0041DataRespDto = new Xdtz0041DataRespDto();// 响应Data：根据客户号前往信贷查找房贷借据信息
        Xdtz0041RespDto xdtz0041RespDto = new Xdtz0041RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0041.req.Data reqData = null; // 请求Data：根据客户号前往信贷查找房贷借据信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0041.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0041.resp.Data();// 响应Data：根据客户号前往信贷查找房贷借据信息
        try {
            // 从 xdtz0041ReqDto获取 reqData
            reqData = xdtz0041ReqDto.getData();
            // 将 reqData 拷贝到xdtz0041DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0041DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041DataReqDto));
            ResultDto<Xdtz0041DataRespDto> xdtz0041DataResultDto = dscmsBizTzClientService.xdtz0041(xdtz0041DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041DataResultDto));
            // 从返回值中获取响应码和响应消息

            xdtz0041RespDto.setErorcd(Optional.ofNullable(xdtz0041DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0041RespDto.setErortx(Optional.ofNullable(xdtz0041DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0041DataResultDto.getCode())) {
                xdtz0041DataRespDto = xdtz0041DataResultDto.getData();
                xdtz0041RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0041RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0041DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0041DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, e.getMessage());
            xdtz0041RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0041RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0041RespDto.setDatasq(servsq);
        xdtz0041RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0041.key, DscmsEnum.TRADE_CODE_XDTZ0041.value, JSON.toJSONString(xdtz0041RespDto));
        return xdtz0041RespDto;
    }
}
