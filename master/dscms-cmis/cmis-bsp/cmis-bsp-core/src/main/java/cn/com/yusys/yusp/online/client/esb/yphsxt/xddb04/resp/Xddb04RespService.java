package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.resp;

/**
 * 响应Service：查询质押物信息
 * @author code-generator
 * @version 1.0             
 */      
public class Xddb04RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
