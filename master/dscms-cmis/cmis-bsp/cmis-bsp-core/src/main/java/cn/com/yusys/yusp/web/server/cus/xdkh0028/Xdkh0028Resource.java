package cn.com.yusys.yusp.web.server.cus.xdkh0028;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0028.req.Xdkh0028ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0028.resp.Xdkh0028RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0028.req.Xdkh0028DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0028.resp.Xdkh0028DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:优农贷黑名单查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0028:优农贷黑名单查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0028Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0028Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0028
     * 交易描述：优农贷黑名单查询
     *
     * @param xdkh0028ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("优农贷黑名单查询")
    @PostMapping("/xdkh0028")
    //@Idempotent({"xdcakh0028", "#xdkh0028ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0028RespDto xdkh0028(@Validated @RequestBody Xdkh0028ReqDto xdkh0028ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, JSON.toJSONString(xdkh0028ReqDto));
        Xdkh0028DataReqDto xdkh0028DataReqDto = new Xdkh0028DataReqDto();// 请求Data： 优农贷黑名单查询
        Xdkh0028DataRespDto xdkh0028DataRespDto = new Xdkh0028DataRespDto();// 响应Data：优农贷黑名单查询
        Xdkh0028RespDto xdkh0028RespDto = new Xdkh0028RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0028.req.Data reqData = null; // 请求Data：优农贷黑名单查询
        cn.com.yusys.yusp.dto.server.cus.xdkh0028.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0028.resp.Data();// 响应Data：优农贷黑名单查询
        try {
            // 从 xdkh0028ReqDto获取 reqData
            reqData = xdkh0028ReqDto.getData();
            // 将 reqData 拷贝到xdkh0028DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0028DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, JSON.toJSONString(xdkh0028DataReqDto));
            ResultDto<Xdkh0028DataRespDto> xdkh0028DataResultDto = dscmsCusClientService.xdkh0028(xdkh0028DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, JSON.toJSONString(xdkh0028DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0028RespDto.setErorcd(Optional.ofNullable(xdkh0028DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0028RespDto.setErortx(Optional.ofNullable(xdkh0028DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0028DataResultDto.getCode())) {
                xdkh0028DataRespDto = xdkh0028DataResultDto.getData();
                xdkh0028RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0028RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0028DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0028DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, e.getMessage());
            xdkh0028RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0028RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0028RespDto.setDatasq(servsq);

        xdkh0028RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0028.key, DscmsEnum.TRADE_CODE_XDKH0028.value, JSON.toJSONString(xdkh0028RespDto));
        return xdkh0028RespDto;
    }
}