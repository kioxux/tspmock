package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp;

import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj22.resp.List;

import java.math.BigDecimal;

/**
 * 响应Service：根据批次号查询票号和票面金额
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erortx;//交易返回信息
    private String erorcd;//交易返回代码
    private List list;//list

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erortx='" + erortx + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", list=" + list +
                '}';
    }
}
