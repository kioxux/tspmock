package cn.com.yusys.yusp.online.client.esb.irs.common;

/**
 * 授信分项额度与抵质押、保证人关系信息(LimitPleMortInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
public class LimitPleMortInfoRecord {
    private String detail_serno; // 授信分项流水号
    private String item_id; // 授信分项台账编号
    private String guaranty_id; // 抵质押物编号/保证群编号

    public String getDetail_serno() {
        return detail_serno;
    }

    public void setDetail_serno(String detail_serno) {
        this.detail_serno = detail_serno;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    @Override
    public String toString() {
        return "LimitPleMortInfo{" +
                "detail_serno='" + detail_serno + '\'' +
                ", item_id='" + item_id + '\'' +
                ", guaranty_id='" + guaranty_id + '\'' +
                '}';
    }
}


