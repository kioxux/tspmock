package cn.com.yusys.yusp.web.client.esb.core.co3202;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.co3202.req.Co3202ReqService;
import cn.com.yusys.yusp.online.client.esb.core.co3202.resp.Co3202RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(co3202)")
@RestController
@RequestMapping("/api/dscms2coreco")
public class Dscms2Co3202Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Co3202Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 抵质押物出入库处理（处理码co3202）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("co3202:抵质押物出入库处理")
    @PostMapping("/co3202")
    protected @ResponseBody
    ResultDto<Co3202RespDto> co3202(@Validated @RequestBody Co3202ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3202.key, EsbEnum.TRADE_CODE_CO3202.value, JSON.toJSONString(reqDto, SerializerFeature.WriteMapNullValue));
        cn.com.yusys.yusp.online.client.esb.core.co3202.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.co3202.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.co3202.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.co3202.resp.Service();
        Co3202ReqService co3202ReqService = new Co3202ReqService();
        Co3202RespService co3202RespService = new Co3202RespService();
        Co3202RespDto co3202RespDto = new Co3202RespDto();
        ResultDto<Co3202RespDto> co3202ResultDto = new ResultDto<Co3202RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Co3202ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CO3202.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(reqDto.getRuzjigou());//    部门号,和许驰确认取入账机构

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            co3202ReqService.setService(reqService);
            // 将co3202ReqService转换成co3202ReqServiceMap
            Map co3202ReqServiceMap = beanMapUtil.beanToMap(co3202ReqService);
            Map service = (Map) co3202ReqServiceMap.get("service");
            // bigdecimal字段转成map之后出现科学计数法问题解决 modify by chenyong 陈勇
            service.forEach((k, v) -> {
                if (v instanceof Double) {
                    BigDecimal bigDecimal = new BigDecimal(((Double) v));
                    service.put(k, bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });
            co3202ReqServiceMap.put("service", service);
            context.put("tradeDataMap", co3202ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3202.key, EsbEnum.TRADE_CODE_CO3202.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CO3202.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3202.key, EsbEnum.TRADE_CODE_CO3202.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            co3202RespService = beanMapUtil.mapToBean(tradeDataMap, Co3202RespService.class, Co3202RespService.class);
            respService = co3202RespService.getService();

            //  将Co3202RespDto封装到ResultDto<Co3202RespDto>
            co3202ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            co3202ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Co3202RespDto
                BeanUtils.copyProperties(respService, co3202RespDto);
                co3202ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                co3202ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                co3202ResultDto.setCode(EpbEnum.EPB099999.key);
                co3202ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3202.key, EsbEnum.TRADE_CODE_CO3202.value, e.getMessage());
            co3202ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            co3202ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        co3202ResultDto.setData(co3202RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3202.key, EsbEnum.TRADE_CODE_CO3202.value, JSON.toJSONString(co3202ResultDto, SerializerFeature.WriteMapNullValue));
        return co3202ResultDto;
    }
}
