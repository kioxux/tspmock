package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb01.resp;

/**
 * 响应Service：查询不动产信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述

    private String guar_no;//押品编号
    private String poc_addr;//地址
    private String build_area;//建筑面积
    private String county_cd;//所在区县
    private String house_land_no;//房产证号
    private String land_no;//土地证号
    private String is_carport;//是否包含车库
    private String is_attic;//是否包含阁楼

    private String land_use_qual;
    private String land_use_way;
    private String land_up_explain;
    private String property;
    private String house_remainder_year;
    private String area_location;
    private String located_position;
    private String land_up;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    public String getPoc_addr() {
        return poc_addr;
    }

    public void setPoc_addr(String poc_addr) {
        this.poc_addr = poc_addr;
    }

    public String getBuild_area() {
        return build_area;
    }

    public void setBuild_area(String build_area) {
        this.build_area = build_area;
    }

    public String getCounty_cd() {
        return county_cd;
    }

    public void setCounty_cd(String county_cd) {
        this.county_cd = county_cd;
    }

    public String getHouse_land_no() {
        return house_land_no;
    }

    public void setHouse_land_no(String house_land_no) {
        this.house_land_no = house_land_no;
    }

    public String getLand_no() {
        return land_no;
    }

    public void setLand_no(String land_no) {
        this.land_no = land_no;
    }

    public String getIs_carport() {
        return is_carport;
    }

    public void setIs_carport(String is_carport) {
        this.is_carport = is_carport;
    }

    public String getIs_attic() {
        return is_attic;
    }

    public void setIs_attic(String is_attic) {
        this.is_attic = is_attic;
    }

    public String getLand_use_qual() {
        return land_use_qual;
    }

    public void setLand_use_qual(String land_use_qual) {
        this.land_use_qual = land_use_qual;
    }

    public String getLand_use_way() {
        return land_use_way;
    }

    public void setLand_use_way(String land_use_way) {
        this.land_use_way = land_use_way;
    }

    public String getLand_up_explain() {
        return land_up_explain;
    }

    public void setLand_up_explain(String land_up_explain) {
        this.land_up_explain = land_up_explain;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getHouse_remainder_year() {
        return house_remainder_year;
    }

    public void setHouse_remainder_year(String house_remainder_year) {
        this.house_remainder_year = house_remainder_year;
    }

    public String getArea_location() {
        return area_location;
    }

    public void setArea_location(String area_location) {
        this.area_location = area_location;
    }

    public String getLocated_position() {
        return located_position;
    }

    public void setLocated_position(String located_position) {
        this.located_position = located_position;
    }

    public String getLand_up() {
        return land_up;
    }

    public void setLand_up(String land_up) {
        this.land_up = land_up;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", guar_no='" + guar_no + '\'' +
                ", poc_addr='" + poc_addr + '\'' +
                ", build_area='" + build_area + '\'' +
                ", county_cd='" + county_cd + '\'' +
                ", house_land_no='" + house_land_no + '\'' +
                ", land_no='" + land_no + '\'' +
                ", is_carport='" + is_carport + '\'' +
                ", is_attic='" + is_attic + '\'' +
                ", land_use_qual='" + land_use_qual + '\'' +
                ", land_use_way='" + land_use_way + '\'' +
                ", land_up_explain='" + land_up_explain + '\'' +
                ", property='" + property + '\'' +
                ", house_remainder_year='" + house_remainder_year + '\'' +
                ", area_location='" + area_location + '\'' +
                ", located_position='" + located_position + '\'' +
                '}';
    }
}
