package cn.com.yusys.yusp.online.client.esb.core.ln3007.req;

/**
 * 请求Service：资产产品币种查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3007ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

