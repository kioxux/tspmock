package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.req;

/**
 * 请求Service：查询基本信息
 * @author code-generator
 * @version 1.0             
 */      
public class Xddb05ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
