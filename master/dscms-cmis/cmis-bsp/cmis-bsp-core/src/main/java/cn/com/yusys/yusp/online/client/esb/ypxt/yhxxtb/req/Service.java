package cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.req;

/**
 * 请求Service：用户信息同步接口
 */
public class Service {
    private String servti;//交易时间
    private String datasq;//    全局流水
    private String servdt;//交易日期
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String usrnid;//用户编号
    private String usrnam;//用户名称
    private String urcert;//证件类型
    private String urcern;//证件号码
    private String uorgid;//所属机构码
    private String sursta;//用户状态

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getUsrnid() {
        return usrnid;
    }

    public void setUsrnid(String usrnid) {
        this.usrnid = usrnid;
    }

    public String getUsrnam() {
        return usrnam;
    }

    public void setUsrnam(String usrnam) {
        this.usrnam = usrnam;
    }

    public String getUrcert() {
        return urcert;
    }

    public void setUrcert(String urcert) {
        this.urcert = urcert;
    }

    public String getUrcern() {
        return urcern;
    }

    public void setUrcern(String urcern) {
        this.urcern = urcern;
    }

    public String getUorgid() {
        return uorgid;
    }

    public void setUorgid(String uorgid) {
        this.uorgid = uorgid;
    }

    public String getSursta() {
        return sursta;
    }

    public void setSursta(String sursta) {
        this.sursta = sursta;
    }

    @Override
    public String toString() {
        return "Service{" +
                "servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", usrnid='" + usrnid + '\'' +
                ", usrnam='" + usrnam + '\'' +
                ", urcert='" + urcert + '\'' +
                ", urcern='" + urcern + '\'' +
                ", uorgid='" + uorgid + '\'' +
                ", sursta='" + sursta + '\'' +
                '}';
    }
}
