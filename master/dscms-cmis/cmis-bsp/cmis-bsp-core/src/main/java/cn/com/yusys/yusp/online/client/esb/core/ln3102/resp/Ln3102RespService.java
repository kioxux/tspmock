package cn.com.yusys.yusp.online.client.esb.core.ln3102.resp;

/**
 * 响应Service：贷款期供查询试算
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3102RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
