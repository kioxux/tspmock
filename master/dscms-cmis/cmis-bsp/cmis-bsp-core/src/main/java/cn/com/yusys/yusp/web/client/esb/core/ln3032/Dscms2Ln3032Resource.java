package cn.com.yusys.yusp.web.client.esb.core.ln3032;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.Ln3032ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.Ln3032RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.LstStzf;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3032.req.Ln3032ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3032.req.LstStzf_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3032.resp.Ln3032RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3032)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3032Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3032Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3032
     * 交易描述：受托支付信息维护
     *
     * @param ln3032ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3032:受托支付信息维护")
    @PostMapping("/ln3032")
    protected @ResponseBody
    ResultDto<Ln3032RespDto> ln3032(@Validated @RequestBody Ln3032ReqDto ln3032ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3032.key, EsbEnum.TRADE_CODE_LN3032.value, JSON.toJSONString(ln3032ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3032.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3032.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3032.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3032.resp.Service();

        Ln3032ReqService ln3032ReqService = new Ln3032ReqService();
        Ln3032RespService ln3032RespService = new Ln3032RespService();
        Ln3032RespDto ln3032RespDto = new Ln3032RespDto();
        ResultDto<Ln3032RespDto> ln3032ResultDto = new ResultDto<Ln3032RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3032ReqDto转换成reqService
            BeanUtils.copyProperties(ln3032ReqDto, reqService);
            List<cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.LstStzf> lstStzfs = ln3032ReqDto.getLstStzf();
            LstStzf_ARRAY list = new LstStzf_ARRAY();
            List<cn.com.yusys.yusp.online.client.esb.core.ln3032.req.Record> recordList = new ArrayList<>();
            if (CollectionUtils.nonEmpty(ln3032ReqDto.getLstStzf())) {
                for (cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.LstStzf lstStzf : lstStzfs) {
                    cn.com.yusys.yusp.online.client.esb.core.ln3032.req.Record record = new cn.com.yusys.yusp.online.client.esb.core.ln3032.req.Record();
                    BeanUtils.copyProperties(lstStzf, record);
                    recordList.add(record);
                }
            }
            list.setRecord(recordList);
            reqService.setLstStzf_ARRAY(list);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3032.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(ln3032ReqDto.getBrchno());//    部门号,取账务机构号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3032ReqService.setService(reqService);
            // 将ln3032ReqService转换成ln3032ReqServiceMap
            Map ln3032ReqServiceMap = beanMapUtil.beanToMap(ln3032ReqService);
            context.put("tradeDataMap", ln3032ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3032.key, EsbEnum.TRADE_CODE_LN3032.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3032.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3032.key, EsbEnum.TRADE_CODE_LN3032.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3032RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3032RespService.class, Ln3032RespService.class);
            respService = ln3032RespService.getService();

            ln3032ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3032ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3032RespDto
                BeanUtils.copyProperties(respService, ln3032RespDto);
                List<LstStzf> respLstStzfs = new ArrayList<LstStzf>();
                cn.com.yusys.yusp.online.client.esb.core.ln3032.resp.LstStzf_ARRAY ln3032RespList = Optional.ofNullable(respService.getLstStzf_ARRAY()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3032.resp.LstStzf_ARRAY());
                respService.setLstStzf_ARRAY(ln3032RespList);
                if (CollectionUtils.nonEmpty(respService.getLstStzf_ARRAY().getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3032.resp.Record> recordLists = Optional.ofNullable(respService.getLstStzf_ARRAY().getRecord()).orElseGet(() -> new ArrayList<>());
                    // 遍历record传值塞入listArrayInfo
                    for (cn.com.yusys.yusp.online.client.esb.core.ln3032.resp.Record record : recordLists) {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.LstStzf lstStzf = new cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.LstStzf();
                        BeanUtils.copyProperties(record, lstStzf);
                        respLstStzfs.add(lstStzf);
                    }
                }
                ln3032RespDto.setLstStzf(respLstStzfs);
                ln3032ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3032ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3032ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3032ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3032.key, EsbEnum.TRADE_CODE_LN3032.value, e.getMessage());
            ln3032ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3032ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3032ResultDto.setData(ln3032RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3032.key, EsbEnum.TRADE_CODE_LN3032.value, JSON.toJSONString(ln3032ResultDto));
        return ln3032ResultDto;
    }
}
