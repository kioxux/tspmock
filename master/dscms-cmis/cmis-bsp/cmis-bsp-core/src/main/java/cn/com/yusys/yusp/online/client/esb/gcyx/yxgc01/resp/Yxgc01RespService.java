package cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp;

import cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp.Service;

/**
 * 响应Service：不动产权证查询接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:11:25
 */
public class Yxgc01RespService {

    private cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp.Service service;

    public cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.resp.Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
