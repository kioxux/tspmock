package cn.com.yusys.yusp.web.client.esb.ypxt.contra;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.ContraReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.Record;
import cn.com.yusys.yusp.online.client.esb.ypxt.contra.resp.ContraRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(contra)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2ContraResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2ContraResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 押品与担保合同关系同步接口（处理码contra）
     *
     * @param contraReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("contra:押品与担保合同关系同步接口")
    @PostMapping("/contra")
    protected @ResponseBody
    ResultDto<ContraRespDto> contra(@Validated @RequestBody ContraReqDto contraReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value, JSON.toJSONString(contraReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.contra.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.contra.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.List list = new cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.List();
        ContraReqService contraReqService = new ContraReqService();
        ContraRespService contraRespService = new ContraRespService();
        ContraRespDto contraRespDto = new ContraRespDto();
        ResultDto<ContraRespDto> contraResultDto = new ResultDto<ContraRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ContraReqDto转换成reqService
            BeanUtils.copyProperties(contraReqDto, reqService);
            if (CollectionUtils.nonEmpty(contraReqDto.getContraListInfo())) {
                List<ContraListInfo> contraListInfos = Optional.ofNullable(contraReqDto.getContraListInfo()).orElseGet(() -> new ArrayList<>());
                List<Record> recordList = new ArrayList<Record>();
                for (ContraListInfo contraListInfo : contraListInfos) {
                    cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.contra.req.Record();
                    BeanUtils.copyProperties(contraListInfo, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CONTRA.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(EsbEnum.BRCHNO_YPXT.key);//    全局流水
            contraReqService.setService(reqService);
            // 将contraReqService转换成contraReqServiceMap
            Map contraReqServiceMap = beanMapUtil.beanToMap(contraReqService);
            context.put("tradeDataMap", contraReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CONTRA.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            contraRespService = beanMapUtil.mapToBean(tradeDataMap, ContraRespService.class, ContraRespService.class);
            respService = contraRespService.getService();

            //  将ContraRespDto封装到ResultDto<ContraRespDto>
            contraResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            contraResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G10501RespDto
                BeanUtils.copyProperties(respService, contraRespDto);
                contraResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                contraResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                contraResultDto.setCode(EpbEnum.EPB099999.key);
                contraResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value, e.getMessage());
            contraResultDto.setCode(EpbEnum.EPB099999.key);//9999
            contraResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        contraResultDto.setData(contraRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CONTRA.key, EsbEnum.TRADE_CODE_CONTRA.value, JSON.toJSONString(contraResultDto));
        return contraResultDto;
    }
}
