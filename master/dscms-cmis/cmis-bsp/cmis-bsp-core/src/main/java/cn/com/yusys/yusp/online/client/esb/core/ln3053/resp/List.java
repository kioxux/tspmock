package cn.com.yusys.yusp.online.client.esb.core.ln3053.resp;

/**
 * <br>
 * 0.2ZRC:2021/5/25 20:25:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 20:25
 * @since 2021/5/25 20:25
 */
public class List {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3053.resp.Record> recordList;

    public java.util.List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(java.util.List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        return "List{" +
                "recordList=" + recordList +
                '}';
    }
}
