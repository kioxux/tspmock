package cn.com.yusys.yusp.web.server.cfg.xdxt0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0004.req.Xdxt0004ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0004.resp.Xdxt0004RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.req.Xdxt0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0004.resp.Xdxt0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据工号获取所辖区域
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0004:根据工号获取所辖区域")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0004Resource.class);
    @Autowired
    private DscmsBizXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0004
     * 交易描述：根据工号获取所辖区域
     *
     * @param xdxt0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据工号获取所辖区域")
    @PostMapping("/xdxt0004")
    //@Idempotent({"xdcaxt0004", "#xdxt0004ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0004RespDto xdxt0004(@Validated @RequestBody Xdxt0004ReqDto xdxt0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004ReqDto));
        Xdxt0004DataReqDto xdxt0004DataReqDto = new Xdxt0004DataReqDto();// 请求Data： 根据工号获取所辖区域
        Xdxt0004DataRespDto xdxt0004DataRespDto = new Xdxt0004DataRespDto();// 响应Data：根据工号获取所辖区域
        Xdxt0004RespDto xdxt0004RespDto = new Xdxt0004RespDto();
        cn.com.yusys.yusp.dto.server.cfg.xdxt0004.req.Data reqData = null; // 请求Data：根据工号获取所辖区域
        cn.com.yusys.yusp.dto.server.cfg.xdxt0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdxt0004.resp.Data();// 响应Data：根据工号获取所辖区域
        try {
            // 从 xdxt0004ReqDto获取 reqData
            reqData = xdxt0004ReqDto.getData();
            // 将 reqData 拷贝到xdxt0004DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataReqDto));
            ResultDto<Xdxt0004DataRespDto> xdxt0004DataResultDto = dscmsXtClientService.xdxt0004(xdxt0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0004RespDto.setErorcd(Optional.ofNullable(xdxt0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0004RespDto.setErortx(Optional.ofNullable(xdxt0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0004DataResultDto.getCode())) {
                xdxt0004DataRespDto = xdxt0004DataResultDto.getData();
                xdxt0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, e.getMessage());
            xdxt0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0004RespDto.setDatasq(servsq);

        xdxt0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0004.key, DscmsEnum.TRADE_CODE_XDXT0004.value, JSON.toJSONString(xdxt0004RespDto));
        return xdxt0004RespDto;
    }
}
