package cn.com.yusys.yusp.web.client.esb.ypxt.yhxxtb;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.yhxxtb.req.YhxxtbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.yhxxtb.resp.YhxxtbRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.req.YhxxtbReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.resp.YhxxtbRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(yhxxtb)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2YhxxtbResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2YhxxtbResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：yhxxtb
     * 交易描述：用户信息同步接口
     *
     * @param yhxxtbReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/yhxxtb")
    protected @ResponseBody
    ResultDto<YhxxtbRespDto> yhxxtb(@Validated @RequestBody YhxxtbReqDto yhxxtbReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YHXXTB.key, EsbEnum.TRADE_CODE_YHXXTB.value, JSON.toJSONString(yhxxtbReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.yhxxtb.resp.Service();
        YhxxtbReqService yhxxtbReqService = new YhxxtbReqService();
        YhxxtbRespService yhxxtbRespService = new YhxxtbRespService();
        YhxxtbRespDto yhxxtbRespDto = new YhxxtbRespDto();
        ResultDto<YhxxtbRespDto> yhxxtbResultDto = new ResultDto<YhxxtbRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将yhxxtbReqDto转换成reqService
            BeanUtils.copyProperties(yhxxtbReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_YHXXTB.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            yhxxtbReqService.setService(reqService);
            // 将yhxxtbReqService转换成yhxxtbReqServiceMap
            Map yhxxtbReqServiceMap = beanMapUtil.beanToMap(yhxxtbReqService);
            context.put("tradeDataMap", yhxxtbReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_YHXXTB.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            yhxxtbRespService = beanMapUtil.mapToBean(tradeDataMap, YhxxtbRespService.class, YhxxtbRespService.class);
            respService = yhxxtbRespService.getService();

            yhxxtbResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            yhxxtbResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成YhxxtbRespDto
                BeanUtils.copyProperties(respService, yhxxtbRespDto);
                yhxxtbResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                yhxxtbResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                yhxxtbResultDto.setCode(EpbEnum.EPB099999.key);
                yhxxtbResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YHXXTB.key, EsbEnum.TRADE_CODE_YHXXTB.value, e.getMessage());
            yhxxtbResultDto.setCode(EpbEnum.EPB099999.key);//9999
            yhxxtbResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        yhxxtbResultDto.setData(yhxxtbRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_YHXXTB.key, EsbEnum.TRADE_CODE_YHXXTB.value, JSON.toJSONString(yhxxtbRespDto));
        return yhxxtbResultDto;
    }
}
