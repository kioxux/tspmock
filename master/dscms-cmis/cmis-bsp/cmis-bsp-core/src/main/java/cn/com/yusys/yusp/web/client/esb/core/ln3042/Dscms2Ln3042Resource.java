package cn.com.yusys.yusp.web.client.esb.core.ln3042;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3042.req.Ln3042ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3042.resp.Ln3042RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3042.req.Ln3042ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3042.resp.Ln3042RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3042)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3042Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3042Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3042
     * 交易描述：手工指定归还某项利息金额，不按既定顺序归还
     *
     * @param ln3042ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3042:手工指定归还某项利息金额，不按既定顺序归还")
    @PostMapping("/ln3042")
    protected @ResponseBody
    ResultDto<Ln3042RespDto> ln3042(@Validated @RequestBody Ln3042ReqDto ln3042ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3042.key, EsbEnum.TRADE_CODE_LN3042.value, JSON.toJSONString(ln3042ReqDto, SerializerFeature.WriteMapNullValue));
        cn.com.yusys.yusp.online.client.esb.core.ln3042.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3042.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3042.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3042.resp.Service();
        Ln3042ReqService ln3042ReqService = new Ln3042ReqService();
        Ln3042RespService ln3042RespService = new Ln3042RespService();
        Ln3042RespDto ln3042RespDto = new Ln3042RespDto();
        ResultDto<Ln3042RespDto> ln3042ResultDto = new ResultDto<Ln3042RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3042ReqDto转换成reqService
            BeanUtils.copyProperties(ln3042ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3042.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(ln3042ReqDto.getBrchno());//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3042ReqService.setService(reqService);
            // 将ln3042ReqService转换成ln3042ReqServiceMap
            Map ln3042ReqServiceMap = beanMapUtil.beanToMap(ln3042ReqService);
            Map service = (Map) ln3042ReqServiceMap.get("service");
            // bigdecimal字段转成map之后出现科学计数法问题解决 modify by chenyong 陈勇
            service.forEach((k, v) -> {
                if (v instanceof Double) {
                    BigDecimal bigDecimal = new BigDecimal(((Double) v));
                    service.put(k, bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP));
                }
            });
            ln3042ReqServiceMap.put("service", service);
            context.put("tradeDataMap", ln3042ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3042.key, EsbEnum.TRADE_CODE_LN3042.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3042.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3042.key, EsbEnum.TRADE_CODE_LN3042.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3042RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3042RespService.class, Ln3042RespService.class);
            respService = ln3042RespService.getService();

            ln3042ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3042ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3042RespDto
                BeanUtils.copyProperties(respService, ln3042RespDto);
                ln3042ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3042ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3042ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3042ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3042.key, EsbEnum.TRADE_CODE_LN3042.value, e.getMessage());
            ln3042ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3042ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3042ResultDto.setData(ln3042RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3042.key, EsbEnum.TRADE_CODE_LN3042.value, JSON.toJSONString(ln3042ResultDto, SerializerFeature.WriteMapNullValue));
        return ln3042ResultDto;
    }
}
