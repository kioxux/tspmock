package cn.com.yusys.yusp.web.server.biz.xdxw0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0014.req.Xdxw0014ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0014.resp.Xdxw0014RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0014.req.Xdxw0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0014.resp.Xdxw0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 接口处理类:优企贷共借人签订
 *
 * @author sunzhen
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms")
@Api(tags = "XDXW0014:优企贷共借人签订")
public class Xdxw0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0014Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0014
     * 交易描述：优企贷共借人签订
     *
     * @param xdxw0014ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdxw0014")
    //@Idempotent({"xdcaxw0014", "#xdxw0014ReqDto.datasq"})
    protected @ResponseBody
    @ApiOperation("优企贷共借人签订")
    Xdxw0014RespDto xdxw0014(@Validated @RequestBody Xdxw0014ReqDto xdxw0014ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014ReqDto));
        Xdxw0014DataReqDto xdxw0014DataReqDto = new Xdxw0014DataReqDto();// 请求Data： 优企贷共借人签订
        Xdxw0014RespDto xdxw0014RespDto = new Xdxw0014RespDto();//响应Dto：优企贷共借人签订
        Xdxw0014DataRespDto xdxw0014DataRespDto = new Xdxw0014DataRespDto();// 响应Data：优企贷共借人签订
        cn.com.yusys.yusp.dto.server.biz.xdxw0014.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdxw0014.req.Data(); // 请求Data：优企贷共借人签订
        cn.com.yusys.yusp.dto.server.biz.xdxw0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0014.resp.Data();// 响应Data：优企贷共借人签订
        try {
            // 从 xdxw0014ReqDto获取 reqData
            reqData = xdxw0014ReqDto.getData();
            // 将 reqData 拷贝到xdxw0014DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0014DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014DataReqDto));
            ResultDto<Xdxw0014DataRespDto> xdxw0014DataResultDto = dscmsBizXwClientService.xdxw0014(xdxw0014DataReqDto);
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0014DataResultDto.getCode())) {
                xdxw0014DataRespDto = xdxw0014DataResultDto.getData();
                xdxw0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0014DataRespDto, respData);
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, e.getMessage());
            xdxw0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0014RespDto.setDatasq(servsq);
        xdxw0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0014.key, DscmsEnum.TRADE_CODE_XDXW0014.value, JSON.toJSONString(xdxw0014RespDto));
        return xdxw0014RespDto;
    }

}
