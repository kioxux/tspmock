package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstytczfe;

import java.math.BigDecimal;

public class Record {
    private String ruzjigou;//入账机构

    private String jigoumch;//机构名称

    private String lhdkleix;//联合贷款类型

    private String chzfkehh;//出资方客户号

    private String chzfkhmc;//出资方客户名称

    private String chzfhhao;//出资方行号

    private String czfhming;//出资方行名

    private String czfzhzxh;//出资方账号子序号

    private String chuzfzhh;//出资方账号

    private String chzfzhmc;//出资方账户名称

    private BigDecimal chuzbili;//出资比例

    private BigDecimal chuzjine;//出资金额

    private String shoukzhh;//收款账号

    private String skzhhzxh;//收款账号子序号

    private String skzhhmch;//收款账户名称

    private String nyuelilv;//年/月利率标识

    private BigDecimal hetongll;//合同利率

    private String yqllcklx;//逾期利率参考类型

    private String yuqillbh;//逾期利率编号

    private String yuqinyll;//逾期年月利率

    private BigDecimal yuqililv;//逾期利率

    private String yuqitzfs;//逾期利率调整方式

    private String yuqitzzq;//逾期利率调整周期

    private String yqfxfdfs;//逾期罚息浮动方式

    private BigDecimal yqfxfdzh;//逾期罚息浮动值

    private String flllcklx;//复利利率参考类型

    private String fulilvbh;//复利利率编号

    private String fulilvny;//复利利率年月标识

    private BigDecimal fulililv;//复利利率

    private String fulitzfs;//复利利率调整方式

    private String fulitzzq;//复利利率调整周期

    private String fulifdfs;//复利利率浮动方式

    private BigDecimal fulifdzh;//复利利率浮动值

    private String dfkbjzhh;//待付款本金账号

    private String dfkbjzxh;//待付款本金账号子序号

    private String dfklxzhh;//待付款利息账号

    private String dfklxzxh;//待付款利息账号子序号

    private String dkjiejuh;//贷款借据号

    private String nbjiejuh;//内部借据号

    private BigDecimal xieyjine;//协议金额

    private BigDecimal xieybili;//协议比例

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getJigoumch() {
        return jigoumch;
    }

    public void setJigoumch(String jigoumch) {
        this.jigoumch = jigoumch;
    }

    public String getLhdkleix() {
        return lhdkleix;
    }

    public void setLhdkleix(String lhdkleix) {
        this.lhdkleix = lhdkleix;
    }

    public String getChzfkehh() {
        return chzfkehh;
    }

    public void setChzfkehh(String chzfkehh) {
        this.chzfkehh = chzfkehh;
    }

    public String getChzfkhmc() {
        return chzfkhmc;
    }

    public void setChzfkhmc(String chzfkhmc) {
        this.chzfkhmc = chzfkhmc;
    }

    public String getChzfhhao() {
        return chzfhhao;
    }

    public void setChzfhhao(String chzfhhao) {
        this.chzfhhao = chzfhhao;
    }

    public String getCzfhming() {
        return czfhming;
    }

    public void setCzfhming(String czfhming) {
        this.czfhming = czfhming;
    }

    public String getCzfzhzxh() {
        return czfzhzxh;
    }

    public void setCzfzhzxh(String czfzhzxh) {
        this.czfzhzxh = czfzhzxh;
    }

    public String getChuzfzhh() {
        return chuzfzhh;
    }

    public void setChuzfzhh(String chuzfzhh) {
        this.chuzfzhh = chuzfzhh;
    }

    public String getChzfzhmc() {
        return chzfzhmc;
    }

    public void setChzfzhmc(String chzfzhmc) {
        this.chzfzhmc = chzfzhmc;
    }

    public BigDecimal getChuzbili() {
        return chuzbili;
    }

    public void setChuzbili(BigDecimal chuzbili) {
        this.chuzbili = chuzbili;
    }

    public BigDecimal getChuzjine() {
        return chuzjine;
    }

    public void setChuzjine(BigDecimal chuzjine) {
        this.chuzjine = chuzjine;
    }

    public String getShoukzhh() {
        return shoukzhh;
    }

    public void setShoukzhh(String shoukzhh) {
        this.shoukzhh = shoukzhh;
    }

    public String getSkzhhzxh() {
        return skzhhzxh;
    }

    public void setSkzhhzxh(String skzhhzxh) {
        this.skzhhzxh = skzhhzxh;
    }

    public String getSkzhhmch() {
        return skzhhmch;
    }

    public void setSkzhhmch(String skzhhmch) {
        this.skzhhmch = skzhhmch;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getDfkbjzhh() {
        return dfkbjzhh;
    }

    public void setDfkbjzhh(String dfkbjzhh) {
        this.dfkbjzhh = dfkbjzhh;
    }

    public String getDfkbjzxh() {
        return dfkbjzxh;
    }

    public void setDfkbjzxh(String dfkbjzxh) {
        this.dfkbjzxh = dfkbjzxh;
    }

    public String getDfklxzhh() {
        return dfklxzhh;
    }

    public void setDfklxzhh(String dfklxzhh) {
        this.dfklxzhh = dfklxzhh;
    }

    public String getDfklxzxh() {
        return dfklxzxh;
    }

    public void setDfklxzxh(String dfklxzxh) {
        this.dfklxzxh = dfklxzxh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getNbjiejuh() {
        return nbjiejuh;
    }

    public void setNbjiejuh(String nbjiejuh) {
        this.nbjiejuh = nbjiejuh;
    }

    public BigDecimal getXieyjine() {
        return xieyjine;
    }

    public void setXieyjine(BigDecimal xieyjine) {
        this.xieyjine = xieyjine;
    }

    public BigDecimal getXieybili() {
        return xieybili;
    }

    public void setXieybili(BigDecimal xieybili) {
        this.xieybili = xieybili;
    }

    @Override
    public String toString() {
        return "Record{" +
                "ruzjigou='" + ruzjigou + '\'' +
                ", jigoumch='" + jigoumch + '\'' +
                ", lhdkleix='" + lhdkleix + '\'' +
                ", chzfkehh='" + chzfkehh + '\'' +
                ", chzfkhmc='" + chzfkhmc + '\'' +
                ", chzfhhao='" + chzfhhao + '\'' +
                ", czfhming='" + czfhming + '\'' +
                ", czfzhzxh='" + czfzhzxh + '\'' +
                ", chuzfzhh='" + chuzfzhh + '\'' +
                ", chzfzhmc='" + chzfzhmc + '\'' +
                ", chuzbili=" + chuzbili +
                ", chuzjine=" + chuzjine +
                ", shoukzhh='" + shoukzhh + '\'' +
                ", skzhhzxh='" + skzhhzxh + '\'' +
                ", skzhhmch='" + skzhhmch + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", hetongll=" + hetongll +
                ", yqllcklx='" + yqllcklx + '\'' +
                ", yuqillbh='" + yuqillbh + '\'' +
                ", yuqinyll='" + yuqinyll + '\'' +
                ", yuqililv=" + yuqililv +
                ", yuqitzfs='" + yuqitzfs + '\'' +
                ", yuqitzzq='" + yuqitzzq + '\'' +
                ", yqfxfdfs='" + yqfxfdfs + '\'' +
                ", yqfxfdzh=" + yqfxfdzh +
                ", flllcklx='" + flllcklx + '\'' +
                ", fulilvbh='" + fulilvbh + '\'' +
                ", fulilvny='" + fulilvny + '\'' +
                ", fulililv=" + fulililv +
                ", fulitzfs='" + fulitzfs + '\'' +
                ", fulitzzq='" + fulitzzq + '\'' +
                ", fulifdfs='" + fulifdfs + '\'' +
                ", fulifdzh=" + fulifdzh +
                ", dfkbjzhh='" + dfkbjzhh + '\'' +
                ", dfkbjzxh='" + dfkbjzxh + '\'' +
                ", dfklxzhh='" + dfklxzhh + '\'' +
                ", dfklxzxh='" + dfklxzxh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", nbjiejuh='" + nbjiejuh + '\'' +
                ", xieyjine=" + xieyjine +
                ", xieybili=" + xieybili +
                '}';
    }
}
