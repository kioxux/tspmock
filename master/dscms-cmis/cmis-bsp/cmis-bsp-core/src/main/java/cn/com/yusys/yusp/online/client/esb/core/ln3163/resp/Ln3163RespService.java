package cn.com.yusys.yusp.online.client.esb.core.ln3163.resp;

/**
 * 响应Service：资产证券化处理
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3163RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

	@Override
	public String toString() {
		return "Ln3163RespService{" +
				"service=" + service +
				'}';
	}
}
