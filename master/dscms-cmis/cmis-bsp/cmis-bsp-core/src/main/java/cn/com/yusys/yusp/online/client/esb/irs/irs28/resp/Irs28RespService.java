package cn.com.yusys.yusp.online.client.esb.irs.irs28.resp;

/**
 * 响应Service：财务信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class Irs28RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

