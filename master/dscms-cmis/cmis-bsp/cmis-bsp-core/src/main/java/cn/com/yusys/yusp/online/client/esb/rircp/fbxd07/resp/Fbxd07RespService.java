package cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp;

/**
 * 响应Service：获取指定数据日期存在还款记录的借据一览信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd07RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd07RespService{" +
                "service=" + service +
                '}';
    }
}
