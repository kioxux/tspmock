package cn.com.yusys.yusp.online.client.esb.core.dp2098.resp;

/**
 * 响应Service：待清算账户查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Dp2098RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
