package cn.com.yusys.yusp.web.server.biz.xdht0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0003.req.Xdht0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0003.resp.Xdht0003RespDto;
import cn.com.yusys.yusp.dto.server.xdht0003.req.Xdht0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0003.resp.Xdht0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷贴现合同号查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0003:信贷贴现合同号查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0003Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0003
     * 交易描述：信贷贴现合同号查询
     *
     * @param xdht0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信贷贴现合同号查询")
    @PostMapping("/xdht0003")
    @Idempotent({"xdcaht0003", "#xdht0003ReqDto.datasq"})
    protected @ResponseBody
    Xdht0003RespDto xdht0003(@Validated @RequestBody Xdht0003ReqDto xdht0003ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003ReqDto));
        Xdht0003DataReqDto xdht0003DataReqDto = new Xdht0003DataReqDto();// 请求Data： 信贷贴现合同号查询
        Xdht0003DataRespDto xdht0003DataRespDto = new Xdht0003DataRespDto();// 响应Data：信贷贴现合同号查询
        Xdht0003RespDto xdht0003RespDto = new Xdht0003RespDto();// 响应Data：信贷贴现合同号查询
        cn.com.yusys.yusp.dto.server.biz.xdht0003.req.Data reqData = null; // 请求Data：信贷贴现合同号查询
        cn.com.yusys.yusp.dto.server.biz.xdht0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0003.resp.Data();// 响应Data：信贷贴现合同号查询
        try {
            // 从 xdht0003ReqDto获取 reqData
            reqData = xdht0003ReqDto.getData();
            // 将 reqData 拷贝到xdht0003DataReqDto
            BeanUtils.copyProperties(reqData, xdht0003DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_SERVICE_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003ReqDto));
            ResultDto<Xdht0003DataRespDto> xdht0003DataResultDto = dscmsBizHtClientService.xdht0003(xdht0003DataReqDto);
            logger.info(TradeLogConstants.CALL_SERVICE_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0003RespDto.setErorcd(Optional.ofNullable(xdht0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0003RespDto.setErortx(Optional.ofNullable(xdht0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0003DataResultDto.getCode())) {
                xdht0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0003DataRespDto = xdht0003DataResultDto.getData();
                // 将 xdht0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, e.getMessage());
            xdht0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0003RespDto.setDatasq(servsq);

        xdht0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0003.key, DscmsEnum.TRADE_CODE_XDHT0003.value, JSON.toJSONString(xdht0003RespDto));
        return xdht0003RespDto;
    }

}
