package cn.com.yusys.yusp.web.server.biz.xdht0022;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0022.req.Xdht0022ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0022.resp.Xdht0022RespDto;
import cn.com.yusys.yusp.dto.server.xdht0022.req.Xdht0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0022.resp.Xdht0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDHT0022:合同信息列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0022Resource.class);

    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0022
     * 交易描述：合同信息列表查询
     *
     * @param xdht0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息列表查询")
    @PostMapping("/xdht0022")
    //@Idempotent({"xdcaht0022", "#xdht0022ReqDto.datasq"})
    protected @ResponseBody
    Xdht0022RespDto xdht0022(@Validated @RequestBody Xdht0022ReqDto xdht0022ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022ReqDto));
        Xdht0022DataReqDto xdht0022DataReqDto = new Xdht0022DataReqDto();// 请求Data： 合同信息列表查询
        Xdht0022DataRespDto xdht0022DataRespDto = new Xdht0022DataRespDto();// 响应Data：合同信息列表查询
        Xdht0022RespDto xdht0022RespDto = new Xdht0022RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0022.req.Data reqData = null; // 请求Data：合同信息列表查询
        cn.com.yusys.yusp.dto.server.biz.xdht0022.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0022.resp.Data();// 响应Data：合同信息列表查询
        try {
            // 从 xdht0022ReqDto获取 reqData
            reqData = xdht0022ReqDto.getData();
            // 将 reqData 拷贝到xdht0022DataReqDto
            BeanUtils.copyProperties(reqData, xdht0022DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022DataReqDto));
            ResultDto<Xdht0022DataRespDto> xdht0022DataResultDto = dscmsBizHtClientService.xdht0022(xdht0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0022RespDto.setErorcd(Optional.ofNullable(xdht0022DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0022RespDto.setErortx(Optional.ofNullable(xdht0022DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0022DataResultDto.getCode())) {
                xdht0022DataRespDto = xdht0022DataResultDto.getData();
                xdht0022RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0022RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0022DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0022DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, e.getMessage());
            xdht0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0022RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0022RespDto.setDatasq(servsq);

        xdht0022RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0022.key, DscmsEnum.TRADE_CODE_XDHT0022.value, JSON.toJSONString(xdht0022RespDto));
        return xdht0022RespDto;
    }
}
