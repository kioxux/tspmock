package cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data;

import cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.criminal.Cases;
import cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.criminal.Count;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Service：涉诉信息查询接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Criminal implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cases")
    private java.util.List<cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.criminal.Cases> cases;
    @JsonProperty(value = "count")
    private cn.com.yusys.yusp.online.client.http.outerdata.qyssxx.resp.data.criminal.Count count;

    public List<Cases> getCases() {
        return cases;
    }

    public void setCases(List<Cases> cases) {
        this.cases = cases;
    }

    public Count getCount() {
        return count;
    }

    public void setCount(Count count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Criminal{" +
                "cases=" + cases +
                ", count=" + count +
                '}';
    }
}
