package cn.com.yusys.yusp.web.server.biz.xdcz0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0003.req.Xdcz0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0003.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0003.resp.Xdcz0003RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0003.req.Xdcz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0003.resp.Xdcz0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:电子银行承兑汇票出账申请
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0003:电子银行承兑汇票出账申请")
@RestController
// TODO /api/tradesystem 待确认
@RequestMapping("/api/dscms")
public class Xdcz0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.biz.xdcz0003.Xdcz0003Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0003
     * 交易描述：电子银行承兑汇票出账申请
     *
     * @param xdcz0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("电子银行承兑汇票出账申请")
    @PostMapping("/xdcz0003")
//   @Idempotent({"xdcz0003", "#xdcz0003ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0003RespDto xdcz0003(@Validated @RequestBody Xdcz0003ReqDto xdcz0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003ReqDto));
        Xdcz0003DataReqDto xdcz0003DataReqDto = new Xdcz0003DataReqDto();// 请求Data： 电子保函开立
        Xdcz0003DataRespDto xdcz0003DataRespDto = new Xdcz0003DataRespDto();// 响应Data：电子保函开立

        cn.com.yusys.yusp.dto.server.biz.xdcz0003.req.Data reqData = null; // 请求Data：电子保函开立
        cn.com.yusys.yusp.dto.server.biz.xdcz0003.resp.Data respData = new Data();// 响应Data：电子保函开立

        Xdcz0003RespDto xdcz0003RespDto = new Xdcz0003RespDto();
        try {
            // 从 xdcz0003ReqDto获取 reqData
            reqData = xdcz0003ReqDto.getData();
            // 将 reqData 拷贝到xdcz0003DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0003DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003DataReqDto));
            ResultDto<Xdcz0003DataRespDto> xdcz0003DataResultDto = dscmsBizCzClientService.xdcz0003(xdcz0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0003RespDto.setErorcd(Optional.ofNullable(xdcz0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0003RespDto.setErortx(Optional.ofNullable(xdcz0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0003DataResultDto.getCode())) {
                xdcz0003DataRespDto = xdcz0003DataResultDto.getData();
                xdcz0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, e.getMessage());
            xdcz0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0003RespDto.setDatasq(servsq);

        xdcz0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0003.key, DscmsEnum.TRADE_CODE_XDCZ0003.value, JSON.toJSONString(xdcz0003RespDto));
        return xdcz0003RespDto;
    }
}
