package cn.com.yusys.yusp.web.server.biz.xdsx0022;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0022.req.Xdsx0022ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0022.resp.Xdsx0022RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0022.req.Xdsx0022DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0022.resp.Xdsx0022DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:推送苏州地方征信给信贷
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDSX0022:推送苏州地方征信给信贷")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0022Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0022Resource.class);
	@Autowired
	private DscmsBizSxClientService dscmsBizSxClientService;
    /**
     * 交易码：xdsx0022
     * 交易描述：推送苏州地方征信给信贷
     *
     * @param xdsx0022ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("推送苏州地方征信给信贷")
    @PostMapping("/xdsx0022")
    //@Idempotent({"xdcasx0022", "#xdsx0022ReqDto.datasq"})
    protected @ResponseBody
	Xdsx0022RespDto xdsx0022(@Validated @RequestBody Xdsx0022ReqDto xdsx0022ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, JSON.toJSONString(xdsx0022ReqDto));
        Xdsx0022DataReqDto xdsx0022DataReqDto = new Xdsx0022DataReqDto();// 请求Data： 推送苏州地方征信给信贷
        Xdsx0022DataRespDto xdsx0022DataRespDto = new Xdsx0022DataRespDto();// 响应Data：推送苏州地方征信给信贷
		Xdsx0022RespDto xdsx0022RespDto =new Xdsx0022RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdsx0022.req.Data reqData = null; // 请求Data：推送苏州地方征信给信贷
		cn.com.yusys.yusp.dto.server.biz.xdsx0022.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0022.resp.Data();// 响应Data：推送苏州地方征信给信贷
        //  此处包导入待确定 结束
        try {
            // 从 xdsx0022ReqDto获取 reqData
            reqData = xdsx0022ReqDto.getData();
            // 将 reqData 拷贝到xdsx0022DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, JSON.toJSONString(xdsx0022DataReqDto));
            ResultDto<Xdsx0022DataRespDto> xdsx0022DataResultDto = dscmsBizSxClientService.xdsx0022(xdsx0022DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, JSON.toJSONString(xdsx0022DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdsx0022RespDto.setErorcd(Optional.ofNullable(xdsx0022DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0022RespDto.setErortx(Optional.ofNullable(xdsx0022DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0022DataResultDto.getCode())) {
                xdsx0022DataRespDto = xdsx0022DataResultDto.getData();
                xdsx0022RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0022RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0022DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0022DataRespDto, respData);
            }
        }catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0011.key, DscmsEnum.TRADE_CODE_XDSX0011.value, e.getMessage());
            xdsx0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0022RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, e.getMessage());
            xdsx0022RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0022RespDto.setErortx(e.getMessage());// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0022RespDto.setDatasq(servsq);

        xdsx0022RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0022.key, DscmsEnum.TRADE_CODE_XDSX0022.value, JSON.toJSONString(xdsx0022RespDto));
        return xdsx0022RespDto;
    }
}
