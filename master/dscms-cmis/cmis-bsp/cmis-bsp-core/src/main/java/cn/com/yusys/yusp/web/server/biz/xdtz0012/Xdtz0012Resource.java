package cn.com.yusys.yusp.web.server.biz.xdtz0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0012.req.Xdtz0012ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0012.resp.Xdtz0012RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0012.req.Xdtz0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0012.resp.Xdtz0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据借款人证件号，判断配偶经营性贷款是否存在余额
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0012:根据借款人证件号，判断配偶经营性贷款是否存在余额")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0012Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0012
     * 交易描述：根据借款人证件号，判断配偶经营性贷款是否存在余额
     *
     * @param xdtz0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借款人证件号，判断配偶经营性贷款是否存在余额")
    @PostMapping("/xdtz0012")
    //@Idempotent({"xdcatz0012", "#xdtz0012ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0012RespDto xdtz0012(@Validated @RequestBody Xdtz0012ReqDto xdtz0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012ReqDto));
        Xdtz0012DataReqDto xdtz0012DataReqDto = new Xdtz0012DataReqDto();// 请求Data： 根据借款人证件号，判断配偶经营性贷款是否存在余额
        Xdtz0012DataRespDto xdtz0012DataRespDto = new Xdtz0012DataRespDto();// 响应Data：根据借款人证件号，判断配偶经营性贷款是否存在余额
        Xdtz0012RespDto xdtz0012RespDto = new Xdtz0012RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0012.req.Data reqData = null; // 请求Data：根据借款人证件号，判断配偶经营性贷款是否存在余额
        cn.com.yusys.yusp.dto.server.biz.xdtz0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0012.resp.Data();// 响应Data：根据借款人证件号，判断配偶经营性贷款是否存在余额

        try {
            // 从 xdtz0012ReqDto获取 reqData
            reqData = xdtz0012ReqDto.getData();
            // 将 reqData 拷贝到xdtz0012DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0012DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataReqDto));
            ResultDto<Xdtz0012DataRespDto> xdtz0012DataResultDto = dscmsBizTzClientService.xdtz0012(xdtz0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0012RespDto.setErorcd(Optional.ofNullable(xdtz0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0012RespDto.setErortx(Optional.ofNullable(xdtz0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0012DataResultDto.getCode())) {
                xdtz0012DataRespDto = xdtz0012DataResultDto.getData();
                xdtz0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, e.getMessage());
            xdtz0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0012RespDto.setDatasq(servsq);

        xdtz0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0012.key, DscmsEnum.TRADE_CODE_XDTZ0012.value, JSON.toJSONString(xdtz0012RespDto));
        return xdtz0012RespDto;
    }
}
