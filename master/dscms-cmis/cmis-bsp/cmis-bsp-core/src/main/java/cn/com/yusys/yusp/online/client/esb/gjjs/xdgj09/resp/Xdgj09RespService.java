package cn.com.yusys.yusp.online.client.esb.gjjs.xdgj09.resp;

/**
 * 响应Service：信贷查询牌价信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Xdgj09RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdgj09RespService{" +
                "service=" + service +
                '}';
    }
}
