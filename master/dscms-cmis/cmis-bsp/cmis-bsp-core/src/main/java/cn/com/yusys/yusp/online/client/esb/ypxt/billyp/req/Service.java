package cn.com.yusys.yusp.online.client.esb.ypxt.billyp.req;

import java.math.BigDecimal;

/**
 * 请求Service：票据信息同步接口
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String custno;//押品所有人编号
    private String custnm;//押品所有人名称
    private String custlx;//押品所有人类型
    private String zjlxyp;//押品所有人证件类型
    private String zjhmyp;//押品所有人证件号码
    private String dzypnm;//质押品名称
    private String cjxtyp;//创建系统
    private String ghridy;//管户人
    private String zhxgry;//最后修改人
    private String zhxgsj;//最后更新时间
    private String zhxgjg;//最后修改人机构
    private String pjhmyp;//票据号码
    private String cprorg;//出票人组织机构代码
    private String cprnmy;//出票人名称
    private String cprlxy;//出票人类型
    private String cprhnm;//出票人开户行名称
    private String cprhno;//出票人开户行行号
    private String cprzhy;//出票人账号
    private String cdrorg;//承兑人组织机构代码
    private String cdrhno;//承兑行行号
    private String cdrhnm;//承兑人名称
    private String cdrlxy;//承兑人类型
    private String skrorg;//收款人组织机构代码
    private String skrhnm;//收款人开户行行名
    private String skrhno;//收款人开户行行号
    private String skrzhy;//收款人账号
    private String skrnmy;//收款人名称
    private String skrlxy;//收款人类型
    private String ispjqs;//是否有票据前手
    private String pjqsjg;//票据前手组织机构代码
    private String pjqsnm;//票据前手名称
    private String pjqslx;//票据前手类型
    private BigDecimal pmjeyp;//票面金额
    private String bzypxt;//币种
    private BigDecimal llypxt;//利率
    private String cprqyp;//出票日期
    private String pjdqrq;//票据到期日期
    private String cxcfqk;//查询查复情况
    private String cxcfrq;//查询查复日期
    private String assetNo;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustnm() {
        return custnm;
    }

    public void setCustnm(String custnm) {
        this.custnm = custnm;
    }

    public String getCustlx() {
        return custlx;
    }

    public void setCustlx(String custlx) {
        this.custlx = custlx;
    }

    public String getZjlxyp() {
        return zjlxyp;
    }

    public void setZjlxyp(String zjlxyp) {
        this.zjlxyp = zjlxyp;
    }

    public String getZjhmyp() {
        return zjhmyp;
    }

    public void setZjhmyp(String zjhmyp) {
        this.zjhmyp = zjhmyp;
    }

    public String getDzypnm() {
        return dzypnm;
    }

    public void setDzypnm(String dzypnm) {
        this.dzypnm = dzypnm;
    }

    public String getCjxtyp() {
        return cjxtyp;
    }

    public void setCjxtyp(String cjxtyp) {
        this.cjxtyp = cjxtyp;
    }

    public String getGhridy() {
        return ghridy;
    }

    public void setGhridy(String ghridy) {
        this.ghridy = ghridy;
    }

    public String getZhxgry() {
        return zhxgry;
    }

    public void setZhxgry(String zhxgry) {
        this.zhxgry = zhxgry;
    }

    public String getZhxgsj() {
        return zhxgsj;
    }

    public void setZhxgsj(String zhxgsj) {
        this.zhxgsj = zhxgsj;
    }

    public String getZhxgjg() {
        return zhxgjg;
    }

    public void setZhxgjg(String zhxgjg) {
        this.zhxgjg = zhxgjg;
    }

    public String getPjhmyp() {
        return pjhmyp;
    }

    public void setPjhmyp(String pjhmyp) {
        this.pjhmyp = pjhmyp;
    }

    public String getCprorg() {
        return cprorg;
    }

    public void setCprorg(String cprorg) {
        this.cprorg = cprorg;
    }

    public String getCprnmy() {
        return cprnmy;
    }

    public void setCprnmy(String cprnmy) {
        this.cprnmy = cprnmy;
    }

    public String getCprlxy() {
        return cprlxy;
    }

    public void setCprlxy(String cprlxy) {
        this.cprlxy = cprlxy;
    }

    public String getCprhnm() {
        return cprhnm;
    }

    public void setCprhnm(String cprhnm) {
        this.cprhnm = cprhnm;
    }

    public String getCprhno() {
        return cprhno;
    }

    public void setCprhno(String cprhno) {
        this.cprhno = cprhno;
    }

    public String getCprzhy() {
        return cprzhy;
    }

    public void setCprzhy(String cprzhy) {
        this.cprzhy = cprzhy;
    }

    public String getCdrorg() {
        return cdrorg;
    }

    public void setCdrorg(String cdrorg) {
        this.cdrorg = cdrorg;
    }

    public String getCdrhno() {
        return cdrhno;
    }

    public void setCdrhno(String cdrhno) {
        this.cdrhno = cdrhno;
    }

    public String getCdrhnm() {
        return cdrhnm;
    }

    public void setCdrhnm(String cdrhnm) {
        this.cdrhnm = cdrhnm;
    }

    public String getCdrlxy() {
        return cdrlxy;
    }

    public void setCdrlxy(String cdrlxy) {
        this.cdrlxy = cdrlxy;
    }

    public String getSkrorg() {
        return skrorg;
    }

    public void setSkrorg(String skrorg) {
        this.skrorg = skrorg;
    }

    public String getSkrhnm() {
        return skrhnm;
    }

    public void setSkrhnm(String skrhnm) {
        this.skrhnm = skrhnm;
    }

    public String getSkrhno() {
        return skrhno;
    }

    public void setSkrhno(String skrhno) {
        this.skrhno = skrhno;
    }

    public String getSkrzhy() {
        return skrzhy;
    }

    public void setSkrzhy(String skrzhy) {
        this.skrzhy = skrzhy;
    }

    public String getSkrnmy() {
        return skrnmy;
    }

    public void setSkrnmy(String skrnmy) {
        this.skrnmy = skrnmy;
    }

    public String getSkrlxy() {
        return skrlxy;
    }

    public void setSkrlxy(String skrlxy) {
        this.skrlxy = skrlxy;
    }

    public String getIspjqs() {
        return ispjqs;
    }

    public void setIspjqs(String ispjqs) {
        this.ispjqs = ispjqs;
    }

    public String getPjqsjg() {
        return pjqsjg;
    }

    public void setPjqsjg(String pjqsjg) {
        this.pjqsjg = pjqsjg;
    }

    public String getPjqsnm() {
        return pjqsnm;
    }

    public void setPjqsnm(String pjqsnm) {
        this.pjqsnm = pjqsnm;
    }

    public String getPjqslx() {
        return pjqslx;
    }

    public void setPjqslx(String pjqslx) {
        this.pjqslx = pjqslx;
    }

    public BigDecimal getPmjeyp() {
        return pmjeyp;
    }

    public void setPmjeyp(BigDecimal pmjeyp) {
        this.pmjeyp = pmjeyp;
    }

    public String getBzypxt() {
        return bzypxt;
    }

    public void setBzypxt(String bzypxt) {
        this.bzypxt = bzypxt;
    }

    public BigDecimal getLlypxt() {
        return llypxt;
    }

    public void setLlypxt(BigDecimal llypxt) {
        this.llypxt = llypxt;
    }

    public String getCprqyp() {
        return cprqyp;
    }

    public void setCprqyp(String cprqyp) {
        this.cprqyp = cprqyp;
    }

    public String getPjdqrq() {
        return pjdqrq;
    }

    public void setPjdqrq(String pjdqrq) {
        this.pjdqrq = pjdqrq;
    }

    public String getCxcfqk() {
        return cxcfqk;
    }

    public void setCxcfqk(String cxcfqk) {
        this.cxcfqk = cxcfqk;
    }

    public String getCxcfrq() {
        return cxcfrq;
    }

    public void setCxcfrq(String cxcfrq) {
        this.cxcfrq = cxcfrq;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", custno='" + custno + '\'' +
                ", custnm='" + custnm + '\'' +
                ", custlx='" + custlx + '\'' +
                ", zjlxyp='" + zjlxyp + '\'' +
                ", zjhmyp='" + zjhmyp + '\'' +
                ", dzypnm='" + dzypnm + '\'' +
                ", cjxtyp='" + cjxtyp + '\'' +
                ", ghridy='" + ghridy + '\'' +
                ", zhxgry='" + zhxgry + '\'' +
                ", zhxgsj='" + zhxgsj + '\'' +
                ", zhxgjg='" + zhxgjg + '\'' +
                ", pjhmyp='" + pjhmyp + '\'' +
                ", cprorg='" + cprorg + '\'' +
                ", cprnmy='" + cprnmy + '\'' +
                ", cprlxy='" + cprlxy + '\'' +
                ", cprhnm='" + cprhnm + '\'' +
                ", cprhno='" + cprhno + '\'' +
                ", cprzhy='" + cprzhy + '\'' +
                ", cdrorg='" + cdrorg + '\'' +
                ", cdrhno='" + cdrhno + '\'' +
                ", cdrhnm='" + cdrhnm + '\'' +
                ", cdrlxy='" + cdrlxy + '\'' +
                ", skrorg='" + skrorg + '\'' +
                ", skrhnm='" + skrhnm + '\'' +
                ", skrhno='" + skrhno + '\'' +
                ", skrzhy='" + skrzhy + '\'' +
                ", skrnmy='" + skrnmy + '\'' +
                ", skrlxy='" + skrlxy + '\'' +
                ", ispjqs='" + ispjqs + '\'' +
                ", pjqsjg='" + pjqsjg + '\'' +
                ", pjqsnm='" + pjqsnm + '\'' +
                ", pjqslx='" + pjqslx + '\'' +
                ", pmjeyp=" + pmjeyp +
                ", bzypxt='" + bzypxt + '\'' +
                ", llypxt=" + llypxt +
                ", cprqyp='" + cprqyp + '\'' +
                ", pjdqrq='" + pjdqrq + '\'' +
                ", cxcfqk='" + cxcfqk + '\'' +
                ", cxcfrq='" + cxcfrq + '\'' +
                ", assetNo='" + assetNo + '\'' +
                '}';
    }
}