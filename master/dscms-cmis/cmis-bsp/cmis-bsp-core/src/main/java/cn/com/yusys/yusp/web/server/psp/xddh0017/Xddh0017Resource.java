package cn.com.yusys.yusp.web.server.psp.xddh0017;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.psp.xddh0017.req.Xddh0017ReqDto;
import cn.com.yusys.yusp.dto.server.psp.xddh0017.resp.Xddh0017RespDto;
import cn.com.yusys.yusp.dto.server.xddh0017.req.Xddh0017DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0017.resp.Xddh0017DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsPspQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:贷后任务完成接收接口
 *
 * @author wrw
 * @version 1.0
 */
@Api(tags = "XDDH0017:贷后任务完成接收接口")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0017Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0017Resource.class);

    @Autowired
    private DscmsPspQtClientService dscmsPspQtClientService;

    /**
     * 交易码：xddh0017
     * 交易描述：贷后任务完成接收接口
     *
     * @return
     * @throws Exception
     */
    @ApiOperation("贷后任务完成接收接口")
    @PostMapping("/xddh0017")
    protected @ResponseBody
    Xddh0017RespDto Xddh0017(@Validated @RequestBody Xddh0017ReqDto xddh0017ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, JSON.toJSONString(xddh0017ReqDto));
        Xddh0017DataRespDto xddh0017DataRespDto = new Xddh0017DataRespDto();// 响应Data：贷后任务完成接收
        Xddh0017DataReqDto xddh0017DataReqDto = new Xddh0017DataReqDto();// 响应Data：贷后任务完成接收
        Xddh0017RespDto xddh0017RespDto = new Xddh0017RespDto();
        cn.com.yusys.yusp.dto.server.psp.xddh0017.req.Data reqData = new cn.com.yusys.yusp.dto.server.psp.xddh0017.req.Data(); // 请求Data：贷后任务完成接收
        cn.com.yusys.yusp.dto.server.psp.xddh0017.resp.Data respData = new cn.com.yusys.yusp.dto.server.psp.xddh0017.resp.Data();// 响应Data：贷后任务完成接收

        try {
            // 从 xddh0017ReqDto reqData
            reqData = xddh0017ReqDto.getData();
            /*reqData.setSerno("24ebf5a3c3fe44f5adce024579c90a65");
            reqData.setCusId("1000000002");
            reqData.setChkState("5");
            reqData.setChkDate("2021-08-29");
            reqData.setChkOpt("1");
            reqData.setManagerId("21340136");
            reqData.setManagerName("zz");*/
            // 将 reqData 拷贝到xddh0017DataReqDto
            BeanUtils.copyProperties(reqData, xddh0017DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value);
            ResultDto<Xddh0017DataRespDto> xddh0017DataResultDto = dscmsPspQtClientService.xddh0017(xddh0017DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, JSON.toJSONString(xddh0017DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddh0017RespDto.setErorcd(Optional.ofNullable(xddh0017DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0017RespDto.setErortx(Optional.ofNullable(xddh0017DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0017DataResultDto.getCode())) {
                xddh0017DataRespDto = xddh0017DataResultDto.getData();
                xddh0017RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0017RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0017DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0017DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, e.getMessage());
            xddh0017RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0017RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0017RespDto.setDatasq(servsq);

        xddh0017RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0017.key, DscmsEnum.TRADE_CODE_XDDH0017.value, JSON.toJSONString(xddh0017RespDto));
        return xddh0017RespDto;
    }
}

