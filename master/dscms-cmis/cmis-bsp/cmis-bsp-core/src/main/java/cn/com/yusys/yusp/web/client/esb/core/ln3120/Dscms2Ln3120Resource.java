package cn.com.yusys.yusp.web.client.esb.core.ln3120;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3120.req.Ln3120ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3120.resp.Ln3120RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3120.req.Ln3120ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3120.resp.Listdkjyzl;
import cn.com.yusys.yusp.online.client.esb.core.ln3120.resp.Ln3120RespService;
import cn.com.yusys.yusp.online.client.esb.core.ln3120.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3120)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3120Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3120Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3120
     * 交易描述：查询抵质押物录入信息
     *
     * @param ln3120ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3120:查询抵质押物录入信息")
    @PostMapping("/ln3120")
    protected @ResponseBody
    ResultDto<Ln3120RespDto> ln3120(@Validated @RequestBody Ln3120ReqDto ln3120ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3120.key, EsbEnum.TRADE_CODE_LN3120.value, JSON.toJSONString(ln3120ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3120.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3120.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3120.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3120.resp.Service();
        Ln3120ReqService ln3120ReqService = new Ln3120ReqService();
        Ln3120RespService ln3120RespService = new Ln3120RespService();
        Ln3120RespDto ln3120RespDto = new Ln3120RespDto();
        ResultDto<Ln3120RespDto> ln3120ResultDto = new ResultDto<Ln3120RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3120ReqDto转换成reqService
            BeanUtils.copyProperties(ln3120ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3120.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3120ReqService.setService(reqService);
            // 将ln3120ReqService转换成ln3120ReqServiceMap
            Map ln3120ReqServiceMap = beanMapUtil.beanToMap(ln3120ReqService);
            context.put("tradeDataMap", ln3120ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3120.key, EsbEnum.TRADE_CODE_LN3120.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3120.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3120.key, EsbEnum.TRADE_CODE_LN3120.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3120RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3120RespService.class, Ln3120RespService.class);
            respService = ln3120RespService.getService();

            ln3120ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3120ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3120RespDto
                BeanUtils.copyProperties(respService, ln3120RespDto);

                Listdkjyzl listdkjyzl = Optional.ofNullable(respService.getListdkjyzll()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ln3120.resp.Listdkjyzl());
                if (CollectionUtils.nonEmpty(listdkjyzl.getRecord())) {
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3120.resp.Listdkjyzl> listdkjyzlList = new ArrayList<>();
                    List<Record> recordList = respService.getListdkjyzll().getRecord();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3120.resp.Listdkjyzl target = new cn.com.yusys.yusp.dto.client.esb.core.ln3120.resp.Listdkjyzl();
                        BeanUtils.copyProperties(record, target);
                        listdkjyzlList.add(target);
                    }
                    ln3120RespDto.setListdkjyzl(listdkjyzlList);
                }

                ln3120ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3120ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3120ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3120ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3120.key, EsbEnum.TRADE_CODE_LN3120.value, e.getMessage());
            ln3120ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3120ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3120ResultDto.setData(ln3120RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3120.key, EsbEnum.TRADE_CODE_LN3120.value, JSON.toJSONString(ln3120ResultDto));
        return ln3120ResultDto;
    }

}
