package cn.com.yusys.yusp.online.client.esb.core.ib1243.req;

/**
 * 请求Service：通用电子记帐交易（多借多贷-多币种）
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servdt;//交易日期
    private String servti;//交易时间
    private String qudaofwm;//渠道服务码
    private String xitongbs;//系统标识号
    private String qianzhrq;//前置日期
    private String qianzhls;//前置流水
    private String laiwzhlx;//来往账类型
    private String fukrzhao;//付款人账号
    private String fukrzwmc;//付款人名称
    private String fukhzfho;//付款行行号
    private String fukhzwmc;//付款行名称
    private String shkrzhao;//收款人账号
    private String shkrzwmc;//收款人名称
    private String shkhzfho;//收款行行号
    private String shkhzwmc;//收款行名称
    private String jykonzbz;//交易控制标志
    private String piaojulx;//票据类型
    private String pjuhaoma;//票据号码
    private String pingzhxh;//凭证序号
    private String pngzzlei;//凭证种类
    private String pingzhma;//凭证号码
    private String beizhuxx;//备注信息
    private String yewubhao;//业务编号
    private List list;//交易账户信息输入
    private String licaibho;//理财编号
    private String jypiciho;//交易批次号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getQudaofwm() {
        return qudaofwm;
    }

    public void setQudaofwm(String qudaofwm) {
        this.qudaofwm = qudaofwm;
    }

    public String getXitongbs() {
        return xitongbs;
    }

    public void setXitongbs(String xitongbs) {
        this.xitongbs = xitongbs;
    }

    public String getQianzhrq() {
        return qianzhrq;
    }

    public void setQianzhrq(String qianzhrq) {
        this.qianzhrq = qianzhrq;
    }

    public String getQianzhls() {
        return qianzhls;
    }

    public void setQianzhls(String qianzhls) {
        this.qianzhls = qianzhls;
    }

    public String getLaiwzhlx() {
        return laiwzhlx;
    }

    public void setLaiwzhlx(String laiwzhlx) {
        this.laiwzhlx = laiwzhlx;
    }

    public String getFukrzhao() {
        return fukrzhao;
    }

    public void setFukrzhao(String fukrzhao) {
        this.fukrzhao = fukrzhao;
    }

    public String getFukrzwmc() {
        return fukrzwmc;
    }

    public void setFukrzwmc(String fukrzwmc) {
        this.fukrzwmc = fukrzwmc;
    }

    public String getFukhzfho() {
        return fukhzfho;
    }

    public void setFukhzfho(String fukhzfho) {
        this.fukhzfho = fukhzfho;
    }

    public String getFukhzwmc() {
        return fukhzwmc;
    }

    public void setFukhzwmc(String fukhzwmc) {
        this.fukhzwmc = fukhzwmc;
    }

    public String getShkrzhao() {
        return shkrzhao;
    }

    public void setShkrzhao(String shkrzhao) {
        this.shkrzhao = shkrzhao;
    }

    public String getShkrzwmc() {
        return shkrzwmc;
    }

    public void setShkrzwmc(String shkrzwmc) {
        this.shkrzwmc = shkrzwmc;
    }

    public String getShkhzfho() {
        return shkhzfho;
    }

    public void setShkhzfho(String shkhzfho) {
        this.shkhzfho = shkhzfho;
    }

    public String getShkhzwmc() {
        return shkhzwmc;
    }

    public void setShkhzwmc(String shkhzwmc) {
        this.shkhzwmc = shkhzwmc;
    }

    public String getJykonzbz() {
        return jykonzbz;
    }

    public void setJykonzbz(String jykonzbz) {
        this.jykonzbz = jykonzbz;
    }

    public String getPiaojulx() {
        return piaojulx;
    }

    public void setPiaojulx(String piaojulx) {
        this.piaojulx = piaojulx;
    }

    public String getPjuhaoma() {
        return pjuhaoma;
    }

    public void setPjuhaoma(String pjuhaoma) {
        this.pjuhaoma = pjuhaoma;
    }

    public String getPingzhxh() {
        return pingzhxh;
    }

    public void setPingzhxh(String pingzhxh) {
        this.pingzhxh = pingzhxh;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getLicaibho() {
        return licaibho;
    }

    public void setLicaibho(String licaibho) {
        this.licaibho = licaibho;
    }

    public String getJypiciho() {
        return jypiciho;
    }

    public void setJypiciho(String jypiciho) {
        this.jypiciho = jypiciho;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "qudaofwm='" + qudaofwm + '\'' +
                "xitongbs='" + xitongbs + '\'' +
                "qianzhrq='" + qianzhrq + '\'' +
                "qianzhls='" + qianzhls + '\'' +
                "laiwzhlx='" + laiwzhlx + '\'' +
                "fukrzhao='" + fukrzhao + '\'' +
                "fukrzwmc='" + fukrzwmc + '\'' +
                "fukhzfho='" + fukhzfho + '\'' +
                "fukhzwmc='" + fukhzwmc + '\'' +
                "shkrzhao='" + shkrzhao + '\'' +
                "shkrzwmc='" + shkrzwmc + '\'' +
                "shkhzfho='" + shkhzfho + '\'' +
                "shkhzwmc='" + shkhzwmc + '\'' +
                "jykonzbz='" + jykonzbz + '\'' +
                "piaojulx='" + piaojulx + '\'' +
                "pjuhaoma='" + pjuhaoma + '\'' +
                "pingzhxh='" + pingzhxh + '\'' +
                "pngzzlei='" + pngzzlei + '\'' +
                "pingzhma='" + pingzhma + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "yewubhao='" + yewubhao + '\'' +
                "list='" + list + '\'' +
                "licaibho='" + licaibho + '\'' +
                "jypiciho='" + jypiciho + '\'' +
                '}';
    }
}
