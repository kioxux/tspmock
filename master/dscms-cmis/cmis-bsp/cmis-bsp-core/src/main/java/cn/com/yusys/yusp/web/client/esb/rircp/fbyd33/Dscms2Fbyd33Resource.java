package cn.com.yusys.yusp.web.client.esb.rircp.fbyd33;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd33.Fbyd33ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd33.Fbyd33RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd33.req.Fbyd33ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbyd33.resp.Fbyd33RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:预授信申请提交
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbyd33Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Fbyd33Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbyd33
     * 交易描述：预授信申请提交
     *
     * @param fbyd33ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbyd33")
    protected @ResponseBody
    ResultDto<Fbyd33RespDto> fbyd33(@Validated @RequestBody Fbyd33ReqDto fbyd33ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD33.key, EsbEnum.TRADE_CODE_FBYD33.value, JSON.toJSONString(fbyd33ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbyd33.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd33.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbyd33.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbyd33.resp.Service();
        Fbyd33ReqService fbyd33ReqService = new Fbyd33ReqService();
        Fbyd33RespService fbyd33RespService = new Fbyd33RespService();
        Fbyd33RespDto fbyd33RespDto = new Fbyd33RespDto();
        ResultDto<Fbyd33RespDto> fbyd33ResultDto = new ResultDto<Fbyd33RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbyd33ReqDto转换成reqService
            BeanUtils.copyProperties(fbyd33ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBYD33.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbyd33ReqService.setService(reqService);
            // 将fbyd33ReqService转换成fbyd33ReqServiceMap
            Map fbyd33ReqServiceMap = beanMapUtil.beanToMap(fbyd33ReqService);
            context.put("tradeDataMap", fbyd33ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD33.key, EsbEnum.TRADE_CODE_FBYD33.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBYD33.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD33.key, EsbEnum.TRADE_CODE_FBYD33.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbyd33RespService = beanMapUtil.mapToBean(tradeDataMap, Fbyd33RespService.class, Fbyd33RespService.class);
            respService = fbyd33RespService.getService();
            //  将respService转换成Fbyd33RespDto
            fbyd33ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            fbyd33ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成fbyd02RespDto
                BeanUtils.copyProperties(respService, fbyd33RespDto);
                fbyd33ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbyd33ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbyd33ResultDto.setCode(EpbEnum.EPB099999.key);
                fbyd33ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD33.key, EsbEnum.TRADE_CODE_FBYD33.value, e.getMessage());
            fbyd33ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbyd33ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbyd33ResultDto.setData(fbyd33RespDto);
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBYD33.key, EsbEnum.TRADE_CODE_FBYD33.value, JSON.toJSONString(fbyd33ReqDto));
        return fbyd33ResultDto;
    }
}
