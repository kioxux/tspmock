package cn.com.yusys.yusp.web.client.esb.ciis2nd.credi1;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi1.Credi1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi1.Credi1RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.req.Credi1ReqService;
import cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.resp.Credi1RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用二代征信系统的接口处理类
 **/
@Api(tags = "BSP封装调用二代征信系统的接口处理类(credi1)")
@RestController
@RequestMapping("/api/dscms2ciis2nd")
public class Dscms2Credi1Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Credi1Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB信贷查询接口（处理码credi1）
     *
     * @param credi1ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("credi1:ESB信贷查询接口")
    @PostMapping("/credi1")
    protected @ResponseBody
    ResultDto<Credi1RespDto> credi1(@Validated @RequestBody Credi1ReqDto credi1ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDI1.key, EsbEnum.TRADE_CODE_CREDI1.value, JSON.toJSONString(credi1ReqDto));
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.req.Service();
        cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ciis2nd.credi1.resp.Service();
        Credi1ReqService credi1ReqService = new Credi1ReqService();
        Credi1RespService credi1RespService = new Credi1RespService();
        Credi1RespDto credi1RespDto = new Credi1RespDto();
        ResultDto<Credi1RespDto> credi1ResultDto = new ResultDto<Credi1RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 将Credi1ReqDto转换成reqService
            BeanUtils.copyProperties(credi1ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CREDI1.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_CIIS2ND.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIIS2ND.key);//    部门号
            credi1ReqService.setService(reqService);
            // 将credi1ReqService转换成credi1ReqServiceMap
            Map credi1ReqServiceMap = beanMapUtil.beanToMap(credi1ReqService);
            context.put("tradeDataMap", credi1ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDI1.key, EsbEnum.TRADE_CODE_CREDI1.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CREDI1.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDI1.key, EsbEnum.TRADE_CODE_CREDI1.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            credi1RespService = beanMapUtil.mapToBean(tradeDataMap, Credi1RespService.class, Credi1RespService.class);
            respService = credi1RespService.getService();
            //  将Credi1RespDto封装到ResultDto<Credi1RespDto>
            credi1ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            credi1ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Credi1RespDto
                BeanUtils.copyProperties(respService, credi1RespDto);
                credi1ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                credi1ResultDto.setMessage(Optional.ofNullable(credi1ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            } else {
                credi1ResultDto.setCode(EpbEnum.EPB099999.key);
                credi1ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDI1.key, EsbEnum.TRADE_CODE_CREDI1.value, e.getMessage());
            credi1ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            credi1ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        credi1ResultDto.setData(credi1RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CREDI1.key, EsbEnum.TRADE_CODE_CREDI1.value, JSON.toJSONString(credi1ResultDto));
        return credi1ResultDto;
    }
}
