package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

/**
 * 响应Service：针对录入、复核模式的交易，用于查询信贷录入信息的查询，复核时使用
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3036RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3036RespService{" +
                "service=" + service +
                '}';
    }
}
