package cn.com.yusys.yusp.web.client.esb.ecif.s00102;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.s00102.req.S00102ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.s00102.resp.S00102RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(s00102)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2S00102Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2S00102Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 对私客户创建及维护
     *
     * @param s00102ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("s00102:对私客户创建及维护")
    @PostMapping("/s00102")
    protected @ResponseBody
    ResultDto<S00102RespDto> s00102(@Validated @RequestBody S00102ReqDto s00102ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value, JSON.toJSONString(s00102ReqDto));
        cn.com.yusys.yusp.online.client.esb.ecif.s00102.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.s00102.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.s00102.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.s00102.resp.Service();
        S00102ReqService s00102ReqService = new S00102ReqService();
        S00102RespService s00102RespService = new S00102RespService();
        S00102RespDto s00102RespDto = new S00102RespDto();
        ResultDto<S00102RespDto> s00102ResultDto = new ResultDto<S00102RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将S00102ReqDto转换成reqService
            BeanUtils.copyProperties(s00102ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_S00102.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            s00102ReqService.setService(reqService);
            // 将s00102ReqService转换成s00102ReqServiceMap
            Map s00102ReqServiceMap = beanMapUtil.beanToMap(s00102ReqService);
            context.put("tradeDataMap", s00102ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_S00102.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            s00102RespService = beanMapUtil.mapToBean(tradeDataMap, S00102RespService.class, S00102RespService.class);
            respService = s00102RespService.getService();
     
            //  将S00102RespDto封装到ResultDto<S00102RespDto>
            s00102ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            s00102ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00102RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, s00102RespDto);
                s00102ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                s00102ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                s00102ResultDto.setCode(EpbEnum.EPB099999.key);
                s00102ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value, e.getMessage());
            s00102ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            s00102ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        s00102ResultDto.setData(s00102RespDto);

        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00102.key, EsbEnum.TRADE_CODE_S00102.value, JSON.toJSONString(s00102ResultDto));
        return s00102ResultDto;
    }
}