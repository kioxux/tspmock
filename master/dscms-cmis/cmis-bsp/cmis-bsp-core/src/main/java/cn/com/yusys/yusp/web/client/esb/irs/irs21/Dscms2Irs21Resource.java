package cn.com.yusys.yusp.web.client.esb.irs.irs21;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.irs21.req.Irs21ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.irs21.resp.Irs21RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(irs21)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Irs21Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Irs21Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 单一客户限额测算信息同步接口（处理码irs21）
     *
     * @param irs21ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("irs21:单一客户限额测算信息同步接口")
    @PostMapping("/irs21")
    protected @ResponseBody
    ResultDto<Irs21RespDto> irs21(@Validated @RequestBody Irs21ReqDto irs21ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value, JSON.toJSONString(irs21ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.irs21.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.irs21.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.irs21.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.irs21.resp.Service();
        Irs21ReqService irs21ReqService = new Irs21ReqService();
        Irs21RespService irs21RespService = new Irs21RespService();
        Irs21RespDto irs21RespDto = new Irs21RespDto();
        ResultDto<Irs21RespDto> irs21ResultDto = new ResultDto<Irs21RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Irs21ReqDto转换成reqService
            BeanUtils.copyProperties(irs21ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS21.key);//交易码
            LocalDateTime now = LocalDateTime.now();
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_FLS.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            irs21ReqService.setService(reqService);
            // 将irs21ReqService转换成irs21ReqServiceMap
            Map irs21ReqServiceMap = beanMapUtil.beanToMap(irs21ReqService);
            context.put("tradeDataMap", irs21ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS21.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            irs21RespService = beanMapUtil.mapToBean(tradeDataMap, Irs21RespService.class, Irs21RespService.class);
            respService = irs21RespService.getService();

            //  将Irs21RespDto封装到ResultDto<Irs21RespDto>
            irs21ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            irs21ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Irs21RespDto
                BeanUtils.copyProperties(respService, irs21RespDto);
                irs21ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                irs21ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                irs21ResultDto.setCode(EpbEnum.EPB099999.key);
                irs21ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value, e.getMessage());
            irs21ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            irs21ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        irs21ResultDto.setData(irs21RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS21.key, EsbEnum.TRADE_CODE_IRS21.value, JSON.toJSONString(irs21ResultDto));

        return irs21ResultDto;
    }
}
