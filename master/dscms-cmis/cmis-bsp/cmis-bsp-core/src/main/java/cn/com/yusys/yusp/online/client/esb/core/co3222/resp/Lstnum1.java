package cn.com.yusys.yusp.online.client.esb.core.co3222.resp;

import java.util.List;

/**
 * 响应Service：抵质押物单笔查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Lstnum1 {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum1Record> record;

    public List<Lstnum1Record> getRecord() {
        return record;
    }

    public void setRecord(List<Lstnum1Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstnum1{" +
                "record=" + record +
                '}';
    }
}
