package cn.com.yusys.yusp.online.client.esb.core.ln3169.req;

import java.math.BigDecimal;

/**
 * 请求Service：资产证券化处理文件导入
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String zichculi;//资产处理类型
    private String xieybhao;//协议编号
    private String jiaoyirq;//交易日期
    private String shuhuirq;//赎回日期
    private String fengbriq;//封包日期
    private String huigriqi;//回购日期
    private BigDecimal jiaoyije;//支付金额
    private Integer zongbish;//总笔数
    private Integer qishibis;//删除笔数
    private Integer chxunbis;//新增笔数
    private String plclmich;//批量处理文件名

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getZichculi() {
        return zichculi;
    }

    public void setZichculi(String zichculi) {
        this.zichculi = zichculi;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getShuhuirq() {
        return shuhuirq;
    }

    public void setShuhuirq(String shuhuirq) {
        this.shuhuirq = shuhuirq;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getPlclmich() {
        return plclmich;
    }

    public void setPlclmich(String plclmich) {
        this.plclmich = plclmich;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zichculi='" + zichculi + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", shuhuirq='" + shuhuirq + '\'' +
                ", fengbriq='" + fengbriq + '\'' +
                ", huigriqi='" + huigriqi + '\'' +
                ", jiaoyije=" + jiaoyije +
                ", zongbish=" + zongbish +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", plclmich='" + plclmich + '\'' +
                '}';
    }
}
