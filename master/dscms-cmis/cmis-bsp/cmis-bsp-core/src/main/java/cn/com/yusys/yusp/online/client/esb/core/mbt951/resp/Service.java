package cn.com.yusys.yusp.online.client.esb.core.mbt951.resp;

/**
 * 响应Service：批量文件处理申请
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {

    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String filename;//报表文件名
    private String picihaoo;//批次号
    private String picriqii;//批次日期
    private String weituoha;//委托号
    private Integer zongbshu;//总笔数

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public String getPicriqii() {
        return picriqii;
    }

    public void setPicriqii(String picriqii) {
        this.picriqii = picriqii;
    }

    public String getWeituoha() {
        return weituoha;
    }

    public void setWeituoha(String weituoha) {
        this.weituoha = weituoha;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", filename='" + filename + '\'' +
                ", picihaoo='" + picihaoo + '\'' +
                ", picriqii='" + picriqii + '\'' +
                ", weituoha='" + weituoha + '\'' +
                ", zongbshu=" + zongbshu +
                '}';
    }
}
