package cn.com.yusys.yusp.online.client.esb.wx.wxp003.resp;

/**
 * 响应Service：信贷将放款标识推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {

    private String erorcd;//响应码
    private String erortx;//响应信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                '}';
    }
}
