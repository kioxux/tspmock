package cn.com.yusys.yusp.online.client.esb.core.ln3032.req;

/**
 * 请求Service：受托支付信息维护
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//交易日期
    private String servti;//交易时间
    private String datasq;//    全局流水

    private String dkkhczbz;//业务操作方式
    private String dkjiejuh;//贷款借据号
    private String chuzhhao;//出账号
    private String dkzhangh;//贷款账号
    private String fuhejgou;//复核机构
    private LstStzf_ARRAY lstStzf_ARRAY;//受托支付信息

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getChuzhhao() {
        return chuzhhao;
    }

    public void setChuzhhao(String chuzhhao) {
        this.chuzhhao = chuzhhao;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getFuhejgou() {
        return fuhejgou;
    }

    public void setFuhejgou(String fuhejgou) {
        this.fuhejgou = fuhejgou;
    }

    public LstStzf_ARRAY getLstStzf_ARRAY() {
        return lstStzf_ARRAY;
    }

    public void setLstStzf_ARRAY(LstStzf_ARRAY lstStzf_ARRAY) {
        this.lstStzf_ARRAY = lstStzf_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkkhczbz='" + dkkhczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", chuzhhao='" + chuzhhao + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", fuhejgou='" + fuhejgou + '\'' +
                ", lstStzf_ARRAY=" + lstStzf_ARRAY +
                '}';
    }
}

