package cn.com.yusys.yusp.online.client.esb.irs.common;


import java.util.List;

/**
 * 请求Service：响应信息域元素:授信层债项评级结果
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class AppMessageInfo {
    private List<AppMessageInfoRecord> record; // 授信层债项评级结果

    public List<AppMessageInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<AppMessageInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "AppMessageInfo{" +
                "record=" + record +
                '}';
    }
}
