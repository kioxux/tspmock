package cn.com.yusys.yusp.web.client.esb.core.da3306;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3306.req.Da3306ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3306.resp.Da3306RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3306.req.Da3306ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3306.resp.Da3306RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:抵债资产拨备计提
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class DscmsDa3306Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsDa3306Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3306
     * 交易描述：抵债资产拨备计提
     *
     * @param da3306ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3306")
    protected @ResponseBody
    ResultDto<Da3306RespDto> da3306(@Validated @RequestBody Da3306ReqDto da3306ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3306.key, EsbEnum.TRADE_CODE_DA3306.value, JSON.toJSONString(da3306ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3306.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3306.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3306.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3306.resp.Service();

        Da3306ReqService da3306ReqService = new Da3306ReqService();
        Da3306RespService da3306RespService = new Da3306RespService();
        Da3306RespDto da3306RespDto = new Da3306RespDto();
        ResultDto<Da3306RespDto> da3306ResultDto = new ResultDto<Da3306RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3306ReqDto转换成reqService
            BeanUtils.copyProperties(da3306ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3306.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3306ReqService.setService(reqService);
            // 将da3306ReqService转换成da3306ReqServiceMap
            Map da3306ReqServiceMap = beanMapUtil.beanToMap(da3306ReqService);
            context.put("tradeDataMap", da3306ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3306.key, EsbEnum.TRADE_CODE_DA3306.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3306.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3306.key, EsbEnum.TRADE_CODE_DA3306.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3306RespService = beanMapUtil.mapToBean(tradeDataMap, Da3306RespService.class, Da3306RespService.class);
            respService = da3306RespService.getService();


            da3306ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3306ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3306RespDto
                BeanUtils.copyProperties(respService, da3306RespDto);
                da3306ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3306ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3306ResultDto.setCode(EpbEnum.EPB099999.key);
                da3306ResultDto.setMessage(respService.getErortx());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3306.key, EsbEnum.TRADE_CODE_DA3306.value, e.getMessage());
            da3306ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3306ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3306ResultDto.setData(da3306RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3306.key, EsbEnum.TRADE_CODE_DA3306.value, JSON.toJSONString(da3306ResultDto));
        return da3306ResultDto;
    }
}
