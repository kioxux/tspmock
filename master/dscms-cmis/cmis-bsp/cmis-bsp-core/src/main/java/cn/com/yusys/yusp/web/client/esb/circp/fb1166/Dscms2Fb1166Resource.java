package cn.com.yusys.yusp.web.client.esb.circp.fb1166;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1166.req.Fb1166ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1166.resp.Fb1166RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1166.req.Fb1166ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1166.resp.Fb1166RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1166）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1166Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1166Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1166
     * 交易描述：面签邀约结果推送
     *
     * @param fb1166ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1166")
    protected @ResponseBody
    ResultDto<Fb1166RespDto> fb1166(@Validated @RequestBody Fb1166ReqDto fb1166ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1166.key, EsbEnum.TRADE_CODE_FB1166.value, JSON.toJSONString(fb1166ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1166.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1166.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1166.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1166.resp.Service();
        Fb1166ReqService fb1166ReqService = new Fb1166ReqService();
        Fb1166RespService fb1166RespService = new Fb1166RespService();
        Fb1166RespDto fb1166RespDto = new Fb1166RespDto();
        ResultDto<Fb1166RespDto> fb1166ResultDto = new ResultDto<Fb1166RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1166ReqDto转换成reqService
            BeanUtils.copyProperties(fb1166ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1166.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1166ReqService.setService(reqService);
            // 将fb1166ReqService转换成fb1166ReqServiceMap
            Map fb1166ReqServiceMap = beanMapUtil.beanToMap(fb1166ReqService);
            context.put("tradeDataMap", fb1166ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1166.key, EsbEnum.TRADE_CODE_FB1166.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1166.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1166.key, EsbEnum.TRADE_CODE_FB1166.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1166RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1166RespService.class, Fb1166RespService.class);
            respService = fb1166RespService.getService();

            fb1166ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1166ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1166RespDto
                BeanUtils.copyProperties(respService, fb1166RespDto);

                fb1166ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1166ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1166ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1166ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1166.key, EsbEnum.TRADE_CODE_FB1166.value, e.getMessage());
            fb1166ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1166ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1166ResultDto.setData(fb1166RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1166.key, EsbEnum.TRADE_CODE_FB1166.value, JSON.toJSONString(fb1166ResultDto));
        return fb1166ResultDto;
    }
}
