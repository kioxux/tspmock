package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.math.BigDecimal;

/**
 * 保证金信息(AssureAccInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:12
 */
public class AssureAccInfoRecord {

    private String serialno; // 流水号
    private String bill_no; // 借据编号
    private String cur_type; // 保证金币种
    private BigDecimal security_money_amt; // 保证金金额
    private String cont_state; // 状态

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getSecurity_money_amt() {
        return security_money_amt;
    }

    public void setSecurity_money_amt(BigDecimal security_money_amt) {
        this.security_money_amt = security_money_amt;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    @Override
    public String toString() {
        return "AssureAccInfo{" +
                "serialno='" + serialno + '\'' +
                ", bill_no='" + bill_no + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", security_money_amt=" + security_money_amt +
                ", cont_state='" + cont_state + '\'' +
                '}';
    }
}
