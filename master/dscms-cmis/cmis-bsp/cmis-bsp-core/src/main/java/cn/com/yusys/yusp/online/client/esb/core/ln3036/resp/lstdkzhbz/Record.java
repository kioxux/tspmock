package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkzhbz;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Record {
    @JsonProperty(value = "baozhfsh")
    private String baozhfsh;//保证方式
    @JsonProperty(value = "bzrkehuh")
    private String bzrkehuh;//保证人客户号
    @JsonProperty(value = "baozjine")
    private BigDecimal baozjine;//保证金额
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "danbzhao")
    private String danbzhao;//担保账号
    @JsonProperty(value = "dbzhzxuh")
    private String dbzhzxuh;//担保账号子序号
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息

    public String getBaozhfsh() {
        return baozhfsh;
    }

    public void setBaozhfsh(String baozhfsh) {
        this.baozhfsh = baozhfsh;
    }

    public String getBzrkehuh() {
        return bzrkehuh;
    }

    public void setBzrkehuh(String bzrkehuh) {
        this.bzrkehuh = bzrkehuh;
    }

    public BigDecimal getBaozjine() {
        return baozjine;
    }

    public void setBaozjine(BigDecimal baozjine) {
        this.baozjine = baozjine;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getDanbzhao() {
        return danbzhao;
    }

    public void setDanbzhao(String danbzhao) {
        this.danbzhao = danbzhao;
    }

    public String getDbzhzxuh() {
        return dbzhzxuh;
    }

    public void setDbzhzxuh(String dbzhzxuh) {
        this.dbzhzxuh = dbzhzxuh;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    @Override
    public String toString() {
        return "Record{" +
                "baozhfsh='" + baozhfsh + '\'' +
                ", bzrkehuh='" + bzrkehuh + '\'' +
                ", baozjine=" + baozjine +
                ", kehmingc='" + kehmingc + '\'' +
                ", danbzhao='" + danbzhao + '\'' +
                ", dbzhzxuh='" + dbzhzxuh + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                '}';
    }
}
