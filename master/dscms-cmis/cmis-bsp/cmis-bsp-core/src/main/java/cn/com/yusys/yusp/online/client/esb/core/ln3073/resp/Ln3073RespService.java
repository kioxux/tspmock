package cn.com.yusys.yusp.online.client.esb.core.ln3073.resp;

/**
 * 响应Service：用于贷款定制期供计划的修改调整
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3073RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

