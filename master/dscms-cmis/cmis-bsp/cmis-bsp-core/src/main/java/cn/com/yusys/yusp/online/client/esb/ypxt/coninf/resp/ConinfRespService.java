package cn.com.yusys.yusp.online.client.esb.ypxt.coninf.resp;

/**
 * 响应Service：信贷担保合同信息同步（处理码coninf）
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class ConinfRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
