package cn.com.yusys.yusp.online.client.esb.core.da3321.resp;

/**
 * 响应Service：抵债资产模糊查询
 *
 * @author code-generator
 * @version 1.0
 */
public class Da3321RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Da3321RespService{" +
                "service=" + service +
                '}';
    }
}

