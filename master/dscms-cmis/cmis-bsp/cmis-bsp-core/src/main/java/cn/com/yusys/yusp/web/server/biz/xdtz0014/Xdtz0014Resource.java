package cn.com.yusys.yusp.web.server.biz.xdtz0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0014.req.Xdtz0014ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0014.resp.Xdtz0014RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0014.req.Xdtz0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0014.resp.Xdtz0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:保证金补交/冲补交结果通知服务
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0014:保证金补交/冲补交结果通知服务")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0014Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0014
     * 交易描述：保证金补交/冲补交结果通知服务
     *
     * @param xdtz0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("保证金补交/冲补交结果通知服务")
    @PostMapping("/xdtz0014")
    //@Idempotent({"xdcatz0014", "#xdtz0014ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0014RespDto xdtz0014(@Validated @RequestBody Xdtz0014ReqDto xdtz0014ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, JSON.toJSONString(xdtz0014ReqDto));
        Xdtz0014DataReqDto xdtz0014DataReqDto = new Xdtz0014DataReqDto();// 请求Data： 保证金补交/冲补交结果通知服务
        Xdtz0014DataRespDto xdtz0014DataRespDto = new Xdtz0014DataRespDto();// 响应Data：保证金补交/冲补交结果通知服务
        Xdtz0014RespDto xdtz0014RespDto = new Xdtz0014RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0014.req.Data reqData = null; // 请求Data：保证金补交/冲补交结果通知服务
        cn.com.yusys.yusp.dto.server.biz.xdtz0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0014.resp.Data();// 响应Data：保证金补交/冲补交结果通知服务
        try {
            // 从 xdtz0014ReqDto获取 reqData
            reqData = xdtz0014ReqDto.getData();
            // 将 reqData 拷贝到xdtz0014DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, JSON.toJSONString(xdtz0014DataReqDto));
            ResultDto<Xdtz0014DataRespDto> xdtz0014DataResultDto = dscmsBizTzClientService.xdtz0014(xdtz0014DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, JSON.toJSONString(xdtz0014DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0014RespDto.setErorcd(Optional.ofNullable(xdtz0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0014RespDto.setErortx(Optional.ofNullable(xdtz0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0014DataResultDto.getCode())) {
                xdtz0014DataRespDto = xdtz0014DataResultDto.getData();
                xdtz0014RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0014RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0014DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, e.getMessage());
            xdtz0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0014RespDto.setDatasq(servsq);

        xdtz0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0014.key, DscmsEnum.TRADE_CODE_XDTZ0014.value, JSON.toJSONString(xdtz0014RespDto));
        return xdtz0014RespDto;
    }
}
