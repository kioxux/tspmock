package cn.com.yusys.yusp.online.client.esb.core.ib1243.req;


/**
 * 请求Service：通用电子记帐交易（多借多贷-多币种）
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/15 20:00
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
