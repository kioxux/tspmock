package cn.com.yusys.yusp.web.server.biz.xdxw0042;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0042.req.Xdxw0042ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0042.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0042.resp.Xdxw0042RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0042.req.Xdxw0042DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0042.resp.Xdxw0042DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:惠享贷模型结果推送给信贷
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0042:惠享贷模型结果推送给信贷")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0042Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0042Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0042
     * 交易描述：惠享贷模型结果推送给信贷
     *
     * @param xdxw0042ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("惠享贷模型结果推送给信贷")
    @PostMapping("/xdxw0042")
    //@Idempotent({"xdcaxw0042", "#xdxw0042ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0042RespDto xdxw0042(@Validated @RequestBody Xdxw0042ReqDto xdxw0042ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, JSON.toJSONString(xdxw0042ReqDto));
        Xdxw0042DataReqDto xdxw0042DataReqDto = new Xdxw0042DataReqDto();// 请求Data： 惠享贷模型结果推送给信贷
        Xdxw0042DataRespDto xdxw0042DataRespDto = new Xdxw0042DataRespDto();// 响应Data：惠享贷模型结果推送给信贷
        cn.com.yusys.yusp.dto.server.biz.xdxw0042.req.Data reqData = null; // 请求Data：惠享贷模型结果推送给信贷
        cn.com.yusys.yusp.dto.server.biz.xdxw0042.resp.Data respData = new Data();// 响应Data：惠享贷模型结果推送给信贷
		Xdxw0042RespDto xdxw0042RespDto = new Xdxw0042RespDto();
		try {
            // 从 xdxw0042ReqDto获取 reqData
            reqData = xdxw0042ReqDto.getData();
            // 将 reqData 拷贝到xdxw0042DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0042DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, JSON.toJSONString(xdxw0042DataReqDto));
            ResultDto<Xdxw0042DataRespDto> xdxw0042DataResultDto = dscmsBizXwClientService.xdxw0042(xdxw0042DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, JSON.toJSONString(xdxw0042DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0042RespDto.setErorcd(Optional.ofNullable(xdxw0042DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0042RespDto.setErortx(Optional.ofNullable(xdxw0042DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0042DataResultDto.getCode())) {
                xdxw0042DataRespDto = xdxw0042DataResultDto.getData();
                xdxw0042RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0042RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0042DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0042DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, e.getMessage());
            xdxw0042RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0042RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0042RespDto.setDatasq(servsq);

        xdxw0042RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0042.key, DscmsEnum.TRADE_CODE_XDXW0042.value, JSON.toJSONString(xdxw0042RespDto));
        return xdxw0042RespDto;
    }
}
