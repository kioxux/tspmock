package cn.com.yusys.yusp.web.server.biz.xdzc0002;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0002.req.Xdzc0002ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0002.resp.Xdzc0002RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0002.req.Xdzc0002DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0002.resp.Xdzc0002DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户资产池协议维护（协议激活）
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDZC0002:客户资产池协议维护（协议激活）")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0002Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0002Resource.class);

	@Autowired
	private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0002
     * 交易描述：客户资产池协议维护（协议激活）
     *
     * @param xdzc0002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户资产池协议维护（协议激活）")
    @PostMapping("/xdzc0002")
    //@Idempotent({"xdzc002", "#xdzc0002ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0002RespDto xdzc0002(@Validated @RequestBody Xdzc0002ReqDto xdzc0002ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, JSON.toJSONString(xdzc0002ReqDto));
        Xdzc0002DataReqDto xdzc0002DataReqDto = new Xdzc0002DataReqDto();// 请求Data： 客户资产池协议维护（协议激活）
        Xdzc0002DataRespDto xdzc0002DataRespDto = new Xdzc0002DataRespDto();// 响应Data：客户资产池协议维护（协议激活）
        cn.com.yusys.yusp.dto.server.biz.xdzc0002.req.Data reqData = null; // 请求Data：客户资产池协议维护（协议激活）
        cn.com.yusys.yusp.dto.server.biz.xdzc0002.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0002.resp.Data();// 响应Data：客户资产池协议维护（协议激活）
		Xdzc0002RespDto xdzc0002RespDto = new Xdzc0002RespDto();
		try {
            // 从 xdzc0002ReqDto获取 reqData
            reqData = xdzc0002ReqDto.getData();
            // 将 reqData 拷贝到xdzc0002DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0002DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, JSON.toJSONString(xdzc0002DataReqDto));
            ResultDto<Xdzc0002DataRespDto> xdzc0002DataResultDto = dscmsBizZcClientService.xdzc0002(xdzc0002DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, JSON.toJSONString(xdzc0002DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzc0002RespDto.setErorcd(Optional.ofNullable(xdzc0002DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0002RespDto.setErortx(Optional.ofNullable(xdzc0002DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0002DataResultDto.getCode())) {
                xdzc0002DataRespDto = xdzc0002DataResultDto.getData();
                xdzc0002RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0002RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0002DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0002DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, e.getMessage());
            xdzc0002RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0002RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0002RespDto.setDatasq(servsq);

        xdzc0002RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0002.key, DscmsEnum.TRADE_CODE_XDZC0002.value, JSON.toJSONString(xdzc0002RespDto));
        return xdzc0002RespDto;
    }
}
