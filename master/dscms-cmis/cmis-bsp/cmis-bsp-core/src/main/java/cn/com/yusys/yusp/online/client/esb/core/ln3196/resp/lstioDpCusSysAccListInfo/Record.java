package cn.com.yusys.yusp.online.client.esb.core.ln3196.resp.lstioDpCusSysAccListInfo;

public class Record {
    //0--对公账户	1--卡	2--活期一本通	3--定期一本通	4--活期存折	5--存单	7--股金账户	8--贷款账户	9--内部账	A--组合账户	B--一号通	F--一证通存折	G--国债	N--电子储蓄国债	X--待销账	K--会计账户	D--电子账户	J--快捷账户
    private String kehuzhlx;
    private String kehuzhao;    //;	客户账号		BaseType.U_KEHUZHAO	string	(35)		否
    private String zhanghao;    //;	负债账号 		BaseType.U_ZHANGHAO	string	(40)		否
    private String chanpshm;    //;	产品说明		BaseType.U_DNMIAOSH	string	(300)		否
    private String zhhaoxuh;    //;	子账户序号		BaseType.U_ZHHAOXUH	string	(8)		否
    // 货币代号		BaseEnumType.E_HUOBDAIH
    //01--人民币	12--英镑	13--香港元	14--美元	15--瑞士法郎	18--新加坡元	21--瑞典克郎	22--丹麦克郎	23--挪威克郎	27--日元	28--加元	29--澳大利亚元	38--欧元	70--南非兰特	81--澳门元	82--菲律宾比索	84--泰铢	87--新西兰元	88--韩元	90--俄罗斯卢布	36--银	34--黄金	SB--所有币种	WB--所有外币
    private String huobdaih;
    private String chaohubz;    //;	账户钞汇标志  0--现钞	1--现汇	x--无
    private String zhanghye;    //;	账户余额		BaseType.U_ZHANGHYE	decimal	(21,2)		否
    private String keyongye;    //;	可用余额		BaseType.U_ZHANGHYE	decimal	(21,2)		否
    //A--正常	J--控制	C--销户	D--久悬户	I--转营业外收入	H--待启用	G--未启用	Y--预销户
    private String zhhuztai;//		账户状态		BaseEnumType.E_ZHHUZTAI	string	(1)	A--正常
    private String cunqiiii;    //;	存期	//;	BaseType.U_CUNQIIII	string	(6)		否
    private String dinhuobz;    //;	产品定活标志	//;	BaseEnumType.E_DINHUOBZ	string	(1)	0--活期产品	 1--定期产品

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getChanpshm() {
        return chanpshm;
    }

    public void setChanpshm(String chanpshm) {
        this.chanpshm = chanpshm;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(String zhanghye) {
        this.zhanghye = zhanghye;
    }

    public String getKeyongye() {
        return keyongye;
    }

    public void setKeyongye(String keyongye) {
        this.keyongye = keyongye;
    }

    public String getZhhuztai() {
        return zhhuztai;
    }

    public void setZhhuztai(String zhhuztai) {
        this.zhhuztai = zhhuztai;
    }

    public String getCunqiiii() {
        return cunqiiii;
    }

    public void setCunqiiii(String cunqiiii) {
        this.cunqiiii = cunqiiii;
    }

    public String getDinhuobz() {
        return dinhuobz;
    }

    public void setDinhuobz(String dinhuobz) {
        this.dinhuobz = dinhuobz;
    }

    @Override
    public String toString() {
        return "Record{" +
                "kehuzhlx='" + kehuzhlx + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", zhanghao='" + zhanghao + '\'' +
                ", chanpshm='" + chanpshm + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", zhanghye='" + zhanghye + '\'' +
                ", keyongye='" + keyongye + '\'' +
                ", zhhuztai='" + zhhuztai + '\'' +
                ", cunqiiii='" + cunqiiii + '\'' +
                ", dinhuobz='" + dinhuobz + '\'' +
                '}';
    }
}
