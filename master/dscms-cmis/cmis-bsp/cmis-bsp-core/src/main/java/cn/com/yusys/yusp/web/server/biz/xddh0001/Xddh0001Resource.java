package cn.com.yusys.yusp.web.server.biz.xddh0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0001.req.Xddh0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0001.resp.Xddh0001RespDto;
import cn.com.yusys.yusp.dto.server.xddh0001.req.Xddh0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0001.resp.Xddh0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询贷后任务是否完成现场检查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0001:查询贷后任务是否完成现场检查")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0001Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0001
     * 交易描述：查询贷后任务是否完成现场检查
     *
     * @param xddh0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询贷后任务是否完成现场检查")
    @PostMapping("/xddh0001")
//@Idempotent({"xddh0001", "#xddh0001ReqDto.datasq"})
    protected @ResponseBody
    Xddh0001RespDto xddh0001(@Validated @RequestBody Xddh0001ReqDto xddh0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, JSON.toJSONString(xddh0001ReqDto));
        Xddh0001DataReqDto xddh0001DataReqDto = new Xddh0001DataReqDto();// 请求Data： 查询贷后任务是否完成现场检查
        Xddh0001DataRespDto xddh0001DataRespDto = new Xddh0001DataRespDto();// 响应Data：查询贷后任务是否完成现场检查
        Xddh0001RespDto xddh0001RespDto = new Xddh0001RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0001.req.Data reqData = null; // 请求Data：查询贷后任务是否完成现场检查
        cn.com.yusys.yusp.dto.server.biz.xddh0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0001.resp.Data();// 响应Data：查询贷后任务是否完成现场检查
        try {
            // 从 xddh0001ReqDto获取 reqData
            reqData = xddh0001ReqDto.getData();
            // 将 reqData 拷贝到xddh0001DataReqDto
            BeanUtils.copyProperties(reqData, xddh0001DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, JSON.toJSONString(xddh0001DataReqDto));
            ResultDto<Xddh0001DataRespDto> xddh0001DataResultDto = dscmsBizDhClientService.xddh0001(xddh0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, JSON.toJSONString(xddh0001DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0001RespDto.setErorcd(Optional.ofNullable(xddh0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0001RespDto.setErortx(Optional.ofNullable(xddh0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0001DataResultDto.getCode())) {
                xddh0001DataRespDto = xddh0001DataResultDto.getData();
                xddh0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, e.getMessage());
            xddh0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0001RespDto.setDatasq(servsq);

        xddh0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0001.key, DscmsEnum.TRADE_CODE_XDDH0001.value, JSON.toJSONString(xddh0001RespDto));
        return xddh0001RespDto;
    }
}
