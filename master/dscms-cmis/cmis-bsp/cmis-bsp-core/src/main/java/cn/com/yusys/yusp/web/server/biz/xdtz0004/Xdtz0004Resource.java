package cn.com.yusys.yusp.web.server.biz.xdtz0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0004.req.Xdtz0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0004.resp.Xdtz0004RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.req.Xdtz0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0004.resp.Xdtz0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:在查询经营性贷款借据信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0004:在查询经营性贷款借据信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0004Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0004
     * 交易描述：在查询经营性贷款借据信息
     *
     * @param xdtz0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("在查询经营性贷款借据信息")
    @PostMapping("/xdtz0004")
    //@Idempotent({"xdcatz0004", "#xdtz0004ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0004RespDto xdtz0004(@Validated @RequestBody Xdtz0004ReqDto xdtz0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004ReqDto));
        Xdtz0004DataReqDto xdtz0004DataReqDto = new Xdtz0004DataReqDto();// 请求Data： 在查询经营性贷款借据信息
        Xdtz0004DataRespDto xdtz0004DataRespDto = new Xdtz0004DataRespDto();// 响应Data：在查询经营性贷款借据信息
        Xdtz0004RespDto xdtz0004RespDto = new Xdtz0004RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0004.req.Data reqData = null; // 请求Data：在查询经营性贷款借据信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0004.resp.Data();// 响应Data：在查询经营性贷款借据信息
        try {
            // 从 xdtz0004ReqDto获取 reqData
            reqData = xdtz0004ReqDto.getData();
            // 将 reqData 拷贝到xdtz0004DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0004DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataReqDto));
            ResultDto<Xdtz0004DataRespDto> xdtz0004DataResultDto = dscmsBizTzClientService.xdtz0004(xdtz0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0004RespDto.setErorcd(Optional.ofNullable(xdtz0004DataResultDto.getCode()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdtz0004RespDto.setErortx(Optional.ofNullable(xdtz0004DataResultDto.getMessage()).orElse(SuccessEnum.CMIS_SUCCSESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0004DataResultDto.getCode())) {
                xdtz0004DataRespDto = xdtz0004DataResultDto.getData();
                xdtz0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, e.getMessage());
            xdtz0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0004RespDto.setDatasq(servsq);

        xdtz0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0004.key, DscmsEnum.TRADE_CODE_XDTZ0004.value, JSON.toJSONString(xdtz0004RespDto));
        return xdtz0004RespDto;
    }
}
