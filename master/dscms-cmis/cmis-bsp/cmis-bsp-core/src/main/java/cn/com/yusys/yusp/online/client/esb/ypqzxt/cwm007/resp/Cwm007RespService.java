package cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.resp;

import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.resp.Service;

/**
 * 响应Service：押品状态查询接口
 *
 * @author dumd
 * @version 1.0
 * @since 2021年10月18日 下午4:22:06
 */
public class Cwm007RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
