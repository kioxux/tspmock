package cn.com.yusys.yusp.web.client.esb.rircp.fbxd16;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16.Fbxd16ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16.Fbxd16RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.req.Fbxd16ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Fbxd16RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd16）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd16Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd16Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd16
     * 交易描述：取得蚂蚁核销记录表的核销借据号一览
     *
     * @param fbxd16ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd16")
    protected @ResponseBody
    ResultDto<Fbxd16RespDto> fbxd16(@Validated @RequestBody Fbxd16ReqDto fbxd16ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd16ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Service();
        Fbxd16ReqService fbxd16ReqService = new Fbxd16ReqService();
        Fbxd16RespService fbxd16RespService = new Fbxd16RespService();
        Fbxd16RespDto fbxd16RespDto = new Fbxd16RespDto();
        ResultDto<Fbxd16RespDto> fbxd16ResultDto = new ResultDto<Fbxd16RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd16ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd16ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD10.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd16ReqService.setService(reqService);
            // 将fbxd16ReqService转换成fbxd16ReqServiceMap
            Map fbxd16ReqServiceMap = beanMapUtil.beanToMap(fbxd16ReqService);
            context.put("tradeDataMap", fbxd16ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD10.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd16RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd16RespService.class, Fbxd16RespService.class);
            respService = fbxd16RespService.getService();

            fbxd16ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd16ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd16RespDto
                BeanUtils.copyProperties(respService, fbxd16RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.List fbxd16RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.List());
                respService.setList(fbxd16RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> fbxd16RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd16.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16.List> fbxd16RespDtoLists = fbxd16RespRecordList.parallelStream().map(fbxd16RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16.List fbxd16RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16.List();
                        BeanUtils.copyProperties(fbxd16RespRecord, fbxd16RespDtoList);
                        return fbxd16RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd16RespDto.setList(fbxd16RespDtoLists);
                }
                fbxd16ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd16ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd16ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd16ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, e.getMessage());
            fbxd16ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd16ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd16ResultDto.setData(fbxd16RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd16ResultDto));
        return fbxd16ResultDto;
    }
}
