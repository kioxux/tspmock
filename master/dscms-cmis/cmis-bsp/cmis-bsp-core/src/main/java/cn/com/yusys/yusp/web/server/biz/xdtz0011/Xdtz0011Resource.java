package cn.com.yusys.yusp.web.server.biz.xdtz0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0011.req.Xdtz0011ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0011.resp.Xdtz0011RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0011.req.Xdtz0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0011.resp.Xdtz0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借据明细查询
 *
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDTZ0011:借据明细查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0011Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：XDTZ0011
     * 交易描述：借据明细查询
     *
     * @param xdtz0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借据明细查询")
    @PostMapping("/xdtz0011")
    //@Idempotent({"xdcatz0011", "#xdtz0011ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0011RespDto xdtz0011(@Validated @RequestBody Xdtz0011ReqDto xdtz0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011ReqDto));
        Xdtz0011DataReqDto xdtz0011DataReqDto = new Xdtz0011DataReqDto();// 请求Data： 借据明细查询
        Xdtz0011DataRespDto xdtz0011DataRespDto = new Xdtz0011DataRespDto();// 响应Data：借据明细查询
        Xdtz0011RespDto xdtz0011RespDto = new Xdtz0011RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0011.req.Data reqData = null; // 请求Data：借据明细查询
        cn.com.yusys.yusp.dto.server.biz.xdtz0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0011.resp.Data();// 响应Data：借据明细查询

        try {
            // 从 XDTZ0011ReqDto获取 reqData
            reqData = xdtz0011ReqDto.getData();
            // 将 reqData 拷贝到XDTZ0011DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0011DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011DataReqDto));
            ResultDto<Xdtz0011DataRespDto> xdtz0011DataResultDto = dscmsBizTzClientService.xdtz0011(xdtz0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0011RespDto.setErorcd(Optional.ofNullable(xdtz0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0011RespDto.setErortx(Optional.ofNullable(xdtz0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0011DataResultDto.getCode())) {
                xdtz0011DataRespDto = xdtz0011DataResultDto.getData();
                xdtz0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 XDTZ0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0011DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, e.getMessage());
            xdtz0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0011RespDto.setDatasq(servsq);

        xdtz0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0011.key, DscmsEnum.TRADE_CODE_XDTZ0011.value, JSON.toJSONString(xdtz0011RespDto));
        return xdtz0011RespDto;
    }
}
