package cn.com.yusys.yusp.online.client.esb.core.ln3163.resp;

import java.math.BigDecimal;
/**
 * 响应Service：资产证券化处理
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String jiejuhao;//借据号
    private BigDecimal zczrjine;//资产转让金额
    private BigDecimal fbsdlixi;//封包时点利息
    private BigDecimal yhfbsdlx;//已还封包时点利息
    private BigDecimal fbhrcqhb;//封包后入池前还本金额
    private BigDecimal fbhrcqhx;//封包后入池前还息金额
    private BigDecimal jjzrlilv;//借据转让利率
    private String zjjjclzt;//资产借据处理状态

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public BigDecimal getZczrjine() {
        return zczrjine;
    }

    public void setZczrjine(BigDecimal zczrjine) {
        this.zczrjine = zczrjine;
    }

    public BigDecimal getFbsdlixi() {
        return fbsdlixi;
    }

    public void setFbsdlixi(BigDecimal fbsdlixi) {
        this.fbsdlixi = fbsdlixi;
    }

    public BigDecimal getYhfbsdlx() {
        return yhfbsdlx;
    }

    public void setYhfbsdlx(BigDecimal yhfbsdlx) {
        this.yhfbsdlx = yhfbsdlx;
    }

    public BigDecimal getFbhrcqhb() {
        return fbhrcqhb;
    }

    public void setFbhrcqhb(BigDecimal fbhrcqhb) {
        this.fbhrcqhb = fbhrcqhb;
    }

    public BigDecimal getFbhrcqhx() {
        return fbhrcqhx;
    }

    public void setFbhrcqhx(BigDecimal fbhrcqhx) {
        this.fbhrcqhx = fbhrcqhx;
    }

    public BigDecimal getJjzrlilv() {
        return jjzrlilv;
    }

    public void setJjzrlilv(BigDecimal jjzrlilv) {
        this.jjzrlilv = jjzrlilv;
    }

    public String getZjjjclzt() {
        return zjjjclzt;
    }

    public void setZjjjclzt(String zjjjclzt) {
        this.zjjjclzt = zjjjclzt;
    }

    @Override
    public String toString() {
        return "Record{" +
                "jiejuhao='" + jiejuhao + '\'' +
                ", zczrjine=" + zczrjine +
                ", fbsdlixi=" + fbsdlixi +
                ", yhfbsdlx=" + yhfbsdlx +
                ", fbhrcqhb=" + fbhrcqhb +
                ", fbhrcqhx=" + fbhrcqhx +
                ", jjzrlilv=" + jjzrlilv +
                ", zjjjclzt='" + zjjjclzt + '\'' +
                '}';
    }
}
