package cn.com.yusys.yusp.online.client.esb.ypxt.certis.req;

/**
 * 请求Service：权证状态同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class CertisReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
