package cn.com.yusys.yusp.online.client.esb.rircp.fbyd02.req;

/**
 * 请求Service：终审申请提交
 *
 * @author code-generator
 * @version 1.0
 */
public class Fbyd02ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

