package cn.com.yusys.yusp.online.client.esb.core.ln3007.resp;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 10:35
 * @since 2021/5/28 10:35
 */
public class Record {
    private String beizhuxx;//备注
    private String huobdhao;//货币代号

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    @Override
    public String toString() {
        return "Service{" +
                "beizhuxx='" + beizhuxx + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                '}';
    }
}
