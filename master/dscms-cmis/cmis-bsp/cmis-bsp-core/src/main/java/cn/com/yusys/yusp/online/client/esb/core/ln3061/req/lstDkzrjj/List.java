package cn.com.yusys.yusp.online.client.esb.core.ln3061.req.lstDkzrjj;

/**
 * 请求Service：资产转让协议登记
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3061.req.lstDkzrjj.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
