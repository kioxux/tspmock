package cn.com.yusys.yusp.online.client.esb.core.ln3110.req;

import java.util.List;

/**
 * 请求Service：贷款还本计划
 */
public class Lstdkhbjh_ARRAY {
    private List<LstdkhbjhRecord> record;//贷款还本计划

    public List<LstdkhbjhRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LstdkhbjhRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhbjh{" +
                "record=" + record +
                '}';
    }
}
