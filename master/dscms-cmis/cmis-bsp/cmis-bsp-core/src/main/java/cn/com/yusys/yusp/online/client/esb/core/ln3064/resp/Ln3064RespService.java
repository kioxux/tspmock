package cn.com.yusys.yusp.online.client.esb.core.ln3064.resp;

/**
 * 响应Service：资产转让内部借据信息查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3064RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3064RespService{" +
                "service=" + service +
                '}';
    }
}
