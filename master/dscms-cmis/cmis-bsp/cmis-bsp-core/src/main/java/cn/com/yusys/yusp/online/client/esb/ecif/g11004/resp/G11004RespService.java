package cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp;

/**
 * 响应Service：客户集团信息查询（new）接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 21:06
 */
public class G11004RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
