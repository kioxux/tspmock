package cn.com.yusys.yusp.online.client.esb.core.da3307.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵债资产出租处理
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private BigDecimal jiaoyije;//交易金额
    private String daoqriqi;//到期日期
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String jiaoyigy;//交易柜员

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

	@Override
	public String toString() {
		return "Service{" +
				"erorcd='" + erorcd + '\'' +
				", erortx='" + erortx + '\'' +
				", servsq='" + servsq + '\'' +
				", datasq='" + datasq + '\'' +
				", dzzcbhao='" + dzzcbhao + '\'' +
				", dzzcminc='" + dzzcminc + '\'' +
				", jiaoyije=" + jiaoyije +
				", daoqriqi='" + daoqriqi + '\'' +
				", jiaoyirq='" + jiaoyirq + '\'' +
				", jiaoyils='" + jiaoyils + '\'' +
				", jiaoyigy='" + jiaoyigy + '\'' +
				'}';
	}
}
