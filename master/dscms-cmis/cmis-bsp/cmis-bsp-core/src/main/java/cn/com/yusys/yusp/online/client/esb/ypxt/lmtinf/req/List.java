package cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req;

/**
 * 请求Service：信贷授信协议信息同步
 *
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.lmtinf.req.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
