package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.resp;

/**
 * 响应Service：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String prcscd;
    private String erortx;// 响应信息
    private String apply_seq;//申请流水号
    private String erorcd;// 响应码
    private String servsq;//
    private String trstatus;
    private List list;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getApply_seq() {
        return apply_seq;
    }

    public void setApply_seq(String apply_seq) {
        this.apply_seq = apply_seq;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getTrstatus() {
        return trstatus;
    }

    public void setTrstatus(String trstatus) {
        this.trstatus = trstatus;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", apply_seq='" + apply_seq + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", servsq='" + servsq + '\'' +
                ", trstatus='" + trstatus + '\'' +
                ", list=" + list +
                '}';
    }
}
