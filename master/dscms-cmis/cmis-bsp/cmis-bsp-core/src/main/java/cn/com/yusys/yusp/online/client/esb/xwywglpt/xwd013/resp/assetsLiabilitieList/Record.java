package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.assetsLiabilitieList;

public class Record {

    private String totalCurrentAmount;//资产总额 - 本期
    private String currFloatAmount;//流动资产小计 - 本期
    private String bankcashCurAmount;//现金/银行存款/理财 -本期
    private String currentOfReceivables;//应收账款 - 本期
    private String currentOfInventory;//存货 - 本期
    private String prepayRentCurAmount;//待摊租金 - 本期
    private String fixassCurrent;//固定资产小计 - 本期
    private String indebtedCurrentAmount;//负债总额 - 本期
    private String bankCurrentAmount;//银行借款 - 本期
    private String accountsCurrentAmount;//应付账款 - 本期
    private String otherloanCurrentAmount;//其他负债 - 本期
    private String ownersCurrAmount;//所有者权益 - 本期

    public String getTotalCurrentAmount() {
        return totalCurrentAmount;
    }

    public void setTotalCurrentAmount(String totalCurrentAmount) {
        this.totalCurrentAmount = totalCurrentAmount;
    }

    public String getCurrFloatAmount() {
        return currFloatAmount;
    }

    public void setCurrFloatAmount(String currFloatAmount) {
        this.currFloatAmount = currFloatAmount;
    }

    public String getBankcashCurAmount() {
        return bankcashCurAmount;
    }

    public void setBankcashCurAmount(String bankcashCurAmount) {
        this.bankcashCurAmount = bankcashCurAmount;
    }

    public String getCurrentOfReceivables() {
        return currentOfReceivables;
    }

    public void setCurrentOfReceivables(String currentOfReceivables) {
        this.currentOfReceivables = currentOfReceivables;
    }

    public String getCurrentOfInventory() {
        return currentOfInventory;
    }

    public void setCurrentOfInventory(String currentOfInventory) {
        this.currentOfInventory = currentOfInventory;
    }

    public String getPrepayRentCurAmount() {
        return prepayRentCurAmount;
    }

    public void setPrepayRentCurAmount(String prepayRentCurAmount) {
        this.prepayRentCurAmount = prepayRentCurAmount;
    }

    public String getFixassCurrent() {
        return fixassCurrent;
    }

    public void setFixassCurrent(String fixassCurrent) {
        this.fixassCurrent = fixassCurrent;
    }

    public String getIndebtedCurrentAmount() {
        return indebtedCurrentAmount;
    }

    public void setIndebtedCurrentAmount(String indebtedCurrentAmount) {
        this.indebtedCurrentAmount = indebtedCurrentAmount;
    }

    public String getBankCurrentAmount() {
        return bankCurrentAmount;
    }

    public void setBankCurrentAmount(String bankCurrentAmount) {
        this.bankCurrentAmount = bankCurrentAmount;
    }

    public String getAccountsCurrentAmount() {
        return accountsCurrentAmount;
    }

    public void setAccountsCurrentAmount(String accountsCurrentAmount) {
        this.accountsCurrentAmount = accountsCurrentAmount;
    }

    public String getOtherloanCurrentAmount() {
        return otherloanCurrentAmount;
    }

    public void setOtherloanCurrentAmount(String otherloanCurrentAmount) {
        this.otherloanCurrentAmount = otherloanCurrentAmount;
    }

    public String getOwnersCurrAmount() {
        return ownersCurrAmount;
    }

    public void setOwnersCurrAmount(String ownersCurrAmount) {
        this.ownersCurrAmount = ownersCurrAmount;
    }

    @Override
    public String toString() {
        return "Record{" +
                "totalCurrentAmount='" + totalCurrentAmount + '\'' +
                ", currFloatAmount='" + currFloatAmount + '\'' +
                ", bankcashCurAmount='" + bankcashCurAmount + '\'' +
                ", currentOfReceivables='" + currentOfReceivables + '\'' +
                ", currentOfInventory='" + currentOfInventory + '\'' +
                ", prepayRentCurAmount='" + prepayRentCurAmount + '\'' +
                ", fixassCurrent='" + fixassCurrent + '\'' +
                ", indebtedCurrentAmount='" + indebtedCurrentAmount + '\'' +
                ", bankCurrentAmount='" + bankCurrentAmount + '\'' +
                ", accountsCurrentAmount='" + accountsCurrentAmount + '\'' +
                ", otherloanCurrentAmount='" + otherloanCurrentAmount + '\'' +
                ", ownersCurrAmount='" + ownersCurrAmount + '\'' +
                '}';
    }
}
