package cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.resp;

import java.math.BigDecimal;

/**
 * 响应Service：不动产权证查询接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:11:25
 */
public class BdcqcxRespService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
