package cn.com.yusys.yusp.online.client.esb.core.ln3108.req;

import java.math.BigDecimal;

/**
 * 请求Dto：贷款组合查询
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String servdt;//交易日期
    private String servti;//交易时间
    private String datasq;//全局流水

    private String dkjiejuh;//贷款借据号
    private String dkzhangh;//贷款账号
    private BigDecimal jiejuuje;//借据金额
    private String hetongbh;//合同编号
    private String kehuhaoo;//客户名
    private String chaxfanw;//查询范围标志
    private String yngyjigo;//营业机构
    private String chanpdma;//产品代码
    private String kaihriqi;//开户日期
    private String zhzhriqi;//终止日期
    private String qishriqi;//起始日期
    private String daoqriqi;//到期日期
    private String xiaohurq;//销户日期
    private String djdqriqi;//冻结到期日期
    private String zhcwjyrq;//最后财务交易日
    private String scjyriqi;//上次交易日期
    private String daikxtai;//贷款形态
    private String yjfyjzht;//应计非应计状态
    private String dkzhhzht;//贷款账户状态
    private String huobdhao;//货币代号
    private String huankzhh;//还款账号
    private String kaihujig;//开户机构
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数
    private String daikduix;//贷款对象
    private String xhdkqyzh;//循环贷款签约账号
    private String dkrzhzhh;//贷款入账账号
    private String cndkjjho;//承诺贷款借据号
    private String kehmingc;//客户名称
    private String huankfsh;//还款方式
    private String dzhhkjih;//定制还款计划
    private String cxqkbizi;//查询欠款标志
    private String khjingli;//客户经理

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getXiaohurq() {
        return xiaohurq;
    }

    public void setXiaohurq(String xiaohurq) {
        this.xiaohurq = xiaohurq;
    }

    public String getDjdqriqi() {
        return djdqriqi;
    }

    public void setDjdqriqi(String djdqriqi) {
        this.djdqriqi = djdqriqi;
    }

    public String getZhcwjyrq() {
        return zhcwjyrq;
    }

    public void setZhcwjyrq(String zhcwjyrq) {
        this.zhcwjyrq = zhcwjyrq;
    }

    public String getScjyriqi() {
        return scjyriqi;
    }

    public void setScjyriqi(String scjyriqi) {
        this.scjyriqi = scjyriqi;
    }

    public String getDaikxtai() {
        return daikxtai;
    }

    public void setDaikxtai(String daikxtai) {
        this.daikxtai = daikxtai;
    }

    public String getYjfyjzht() {
        return yjfyjzht;
    }

    public void setYjfyjzht(String yjfyjzht) {
        this.yjfyjzht = yjfyjzht;
    }

    public String getDkzhhzht() {
        return dkzhhzht;
    }

    public void setDkzhhzht(String dkzhhzht) {
        this.dkzhhzht = dkzhhzht;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getKaihujig() {
        return kaihujig;
    }

    public void setKaihujig(String kaihujig) {
        this.kaihujig = kaihujig;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getDaikduix() {
        return daikduix;
    }

    public void setDaikduix(String daikduix) {
        this.daikduix = daikduix;
    }

    public String getXhdkqyzh() {
        return xhdkqyzh;
    }

    public void setXhdkqyzh(String xhdkqyzh) {
        this.xhdkqyzh = xhdkqyzh;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getCndkjjho() {
        return cndkjjho;
    }

    public void setCndkjjho(String cndkjjho) {
        this.cndkjjho = cndkjjho;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getCxqkbizi() {
        return cxqkbizi;
    }

    public void setCxqkbizi(String cxqkbizi) {
        this.cxqkbizi = cxqkbizi;
    }

    public String getKhjingli() {
        return khjingli;
    }

    public void setKhjingli(String khjingli) {
        this.khjingli = khjingli;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", hetongbh='" + hetongbh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", chaxfanw='" + chaxfanw + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", qishriqi='" + qishriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", xiaohurq='" + xiaohurq + '\'' +
                ", djdqriqi='" + djdqriqi + '\'' +
                ", zhcwjyrq='" + zhcwjyrq + '\'' +
                ", scjyriqi='" + scjyriqi + '\'' +
                ", daikxtai='" + daikxtai + '\'' +
                ", yjfyjzht='" + yjfyjzht + '\'' +
                ", dkzhhzht='" + dkzhhzht + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", huankzhh='" + huankzhh + '\'' +
                ", kaihujig='" + kaihujig + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", daikduix='" + daikduix + '\'' +
                ", xhdkqyzh='" + xhdkqyzh + '\'' +
                ", dkrzhzhh='" + dkrzhzhh + '\'' +
                ", cndkjjho='" + cndkjjho + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", dzhhkjih='" + dzhhkjih + '\'' +
                ", cxqkbizi='" + cxqkbizi + '\'' +
                ", khjingli='" + khjingli + '\'' +
                '}';
    }
}
