package cn.com.yusys.yusp.online.client.esb.core.ln3026.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdzqgjh.Record;

import java.util.List;

/**
 * 贷款定制期供计划表
 *
 * @author lihh
 * @version 1.0
 */
public class Lstdzqgjh_ARRAY {

    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdzqgjh{" +
                "record=" + record +
                '}';
    }
}
