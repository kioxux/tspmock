package cn.com.yusys.yusp.online.client.esb.ypxt.evalyp.req;

import java.math.BigDecimal;

/**
 * 请求Service：押品我行确认价值同步接口
 */
public class evalypList {
    private String yptybh;//押品统一编号
    private BigDecimal whrdjz;//我行认定价值
    private String rdbzyp;//认定币种
    private String rdrqyp;//认定日期

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public BigDecimal getWhrdjz() {
        return whrdjz;
    }

    public void setWhrdjz(BigDecimal whrdjz) {
        this.whrdjz = whrdjz;
    }

    public String getRdbzyp() {
        return rdbzyp;
    }

    public void setRdbzyp(String rdbzyp) {
        this.rdbzyp = rdbzyp;
    }

    public String getRdrqyp() {
        return rdrqyp;
    }

    public void setRdrqyp(String rdrqyp) {
        this.rdrqyp = rdrqyp;
    }

    @Override
    public String toString() {
        return "Service{" +
                "yptybh='" + yptybh + '\'' +
                "whrdjz='" + whrdjz + '\'' +
                "rdbzyp='" + rdbzyp + '\'' +
                "rdrqyp='" + rdrqyp + '\'' +
                '}';
    }
}
