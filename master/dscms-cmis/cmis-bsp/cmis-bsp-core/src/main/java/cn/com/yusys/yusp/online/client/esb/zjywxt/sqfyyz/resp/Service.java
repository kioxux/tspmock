package cn.com.yusys.yusp.online.client.esb.zjywxt.sqfyyz.resp;

/**
 * 响应Service：行方验证房源信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息

    private String hlocation;//房屋坐落
    private String spaccount;//监管账号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getHlocation() {
        return hlocation;
    }

    public void setHlocation(String hlocation) {
        this.hlocation = hlocation;
    }

    public String getSpaccount() {
        return spaccount;
    }

    public void setSpaccount(String spaccount) {
        this.spaccount = spaccount;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", hlocation='" + hlocation + '\'' +
                ", spaccount='" + spaccount + '\'' +
                '}';
    }
}
