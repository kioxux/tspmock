package cn.com.yusys.yusp.web.client.esb.ecif.g11004;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g11004.req.G11004ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp.G11004RespService;
import cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g11004)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G11004Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G11004Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 客户集团信息查询（new）接口（处理码g11004）
     *
     * @param g11004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g11004:客户集团信息查询（new）接口")
    @PostMapping("/g11004")
    protected @ResponseBody
    ResultDto<G11004RespDto> g11004(@Validated @RequestBody G11004ReqDto g11004ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11004.key, EsbEnum.TRADE_CODE_G11004.value, JSON.toJSONString(g11004ReqDto));

        cn.com.yusys.yusp.online.client.esb.ecif.g11004.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g11004.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp.Service();
        G11004ReqService g11004ReqService = new G11004ReqService();
        G11004RespService g11004RespService = new G11004RespService();
        G11004RespDto g11004RespDto = new G11004RespDto();
        ResultDto<G11004RespDto> g11004ResultDto = new ResultDto<G11004RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将G11004ReqDto转换成reqService
            BeanUtils.copyProperties(g11004ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_G11004.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            g11004ReqService.setService(reqService);
            // 将g11004ReqService转换成g11004ReqServiceMap
            Map g11004ReqServiceMap = beanMapUtil.beanToMap(g11004ReqService);
            context.put("tradeDataMap", g11004ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11004.key, EsbEnum.TRADE_CODE_G11004.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G11004.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11004.key, EsbEnum.TRADE_CODE_G11004.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g11004RespService = beanMapUtil.mapToBean(tradeDataMap, G11004RespService.class, G11004RespService.class);
            respService = g11004RespService.getService();

            //  将G11004RespDto封装到ResultDto<G11004RespDto>
            g11004ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g11004ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G11004RespDto
                BeanUtils.copyProperties(respService, g11004RespDto);

                cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp.List g11004List = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp.List());
                respService.setList(g11004List);
                java.util.List<Record> recordList = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<Record>());
                java.util.List<cn.com.yusys.yusp.dto.client.esb.ecif.g11004.CircleArrayMem> circleArrayMems = new ArrayList<>();
                // 遍历record传值塞入
                if (CollectionUtils.nonEmpty(recordList)) {
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.ecif.g11004.CircleArrayMem circleArrayMem = new cn.com.yusys.yusp.dto.client.esb.ecif.g11004.CircleArrayMem();
                        BeanUtils.copyProperties(record, circleArrayMem);
                        circleArrayMems.add(circleArrayMem);
                    }
                }
                g11004RespDto.setCircleArrayMem(circleArrayMems);
                g11004ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g11004ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                g11004ResultDto.setCode(EpbEnum.EPB099999.key);
                g11004ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11004.key, EsbEnum.TRADE_CODE_G11004.value, e.getMessage());
            g11004ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g11004ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g11004ResultDto.setData(g11004RespDto);

        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G11004.key, EsbEnum.TRADE_CODE_G11004.value, JSON.toJSONString(g11004ResultDto));
        return g11004ResultDto;
    }
}
