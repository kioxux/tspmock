package cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.req;

/**
 * 请求Service：查询共有人信息
 *
 * @author chenyong
 * @version 1.0
 */
public class XdypgyrcxReqService {
	private Service service;

	public Service getService() {
		return service;
	}

	public void setService(Service service) {
		this.service = service;
	}
}                      

