package cn.com.yusys.yusp.online.client.http.outerdata.idcheck.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Service：身份核验接口
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class IdCheckReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//    交易码
    @JsonProperty(value = "servtp")
    private String servtp;//    渠道码
    @JsonProperty(value = "servsq")
    private String servsq;//    渠道流水号
    @JsonProperty(value = "userid")
    private String userid;//    柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//    部门号
    @JsonProperty(value = "cert_type")
    private String cert_type;//    证件类型
    @JsonProperty(value = "user_name")
    private String user_name;//    姓名
    @JsonProperty(value = "id_number")
    private String id_number;//    身份证号
    @JsonProperty(value = "source_type")
    private String source_type;//    机构类型
    @JsonProperty(value = "ip")
    private String ip;//    网上营业厅申请人IP地址
    @JsonProperty(value = "authority")
    private String authority;//    发证单位
    @JsonProperty(value = "expiryBegin")
    private String expiryBegin;//    有效期始
    @JsonProperty(value = "expiryEnd")
    private String expiryEnd;//    有效期止
    @JsonProperty(value = "phone_number")
    private String phone_number;
    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getId_number() {
        return id_number;
    }

    public void setId_number(String id_number) {
        this.id_number = id_number;
    }

    public String getSource_type() {
        return source_type;
    }

    public void setSource_type(String source_type) {
        this.source_type = source_type;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getExpiryBegin() {
        return expiryBegin;
    }

    public void setExpiryBegin(String expiryBegin) {
        this.expiryBegin = expiryBegin;
    }

    public String getExpiryEnd() {
        return expiryEnd;
    }

    public void setExpiryEnd(String expiryEnd) {
        this.expiryEnd = expiryEnd;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    @Override
    public String toString() {
        return "IdCheckReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", user_name='" + user_name + '\'' +
                ", id_number='" + id_number + '\'' +
                ", source_type='" + source_type + '\'' +
                ", ip='" + ip + '\'' +
                ", authority='" + authority + '\'' +
                ", expiryBegin='" + expiryBegin + '\'' +
                ", expiryEnd='" + expiryEnd + '\'' +
                ", phone_number='" + phone_number + '\'' +
                '}';
    }
}
