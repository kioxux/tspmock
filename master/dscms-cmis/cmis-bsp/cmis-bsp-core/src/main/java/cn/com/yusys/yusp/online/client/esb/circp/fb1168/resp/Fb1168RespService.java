package cn.com.yusys.yusp.online.client.esb.circp.fb1168.resp;

/**
 * 响应Service：抵押查封结果推送
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1168RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1168RespService{" +
                "service=" + service +
                '}';
    }
}
