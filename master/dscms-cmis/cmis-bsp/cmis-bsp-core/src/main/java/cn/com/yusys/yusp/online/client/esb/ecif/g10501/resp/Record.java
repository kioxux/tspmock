package cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp;
/**
 * 响应Service：对公及同业客户清单查询
 */
public class Record {

    private String custno      ;//       客户编号
    private String custna      ;//       客户名称
    private String custst      ;//       客户状态
    private String cridno      ;//       社会统一信用代码
    private String cpopcd      ;//       组织机构代码
    private String gvrgno      ;//       营业执照号码
    private String creabr      ;//       开户机构
    private String userid      ;//       开户柜员
    private String opendt      ;//       开户时间
    private String homead      ;//       地址

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    public String getCridno() {
        return cridno;
    }

    public void setCridno(String cridno) {
        this.cridno = cridno;
    }

    public String getCpopcd() {
        return cpopcd;
    }

    public void setCpopcd(String cpopcd) {
        this.cpopcd = cpopcd;
    }

    public String getGvrgno() {
        return gvrgno;
    }

    public void setGvrgno(String gvrgno) {
        this.gvrgno = gvrgno;
    }

    public String getCreabr() {
        return creabr;
    }

    public void setCreabr(String creabr) {
        this.creabr = creabr;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getOpendt() {
        return opendt;
    }

    public void setOpendt(String opendt) {
        this.opendt = opendt;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    @Override
    public String toString() {
        return "Record{" +
                "custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", custst='" + custst + '\'' +
                ", cridno='" + cridno + '\'' +
                ", cpopcd='" + cpopcd + '\'' +
                ", gvrgno='" + gvrgno + '\'' +
                ", creabr='" + creabr + '\'' +
                ", userid='" + userid + '\'' +
                ", opendt='" + opendt + '\'' +
                ", homead='" + homead + '\'' +
                '}';
    }
}
