package cn.com.yusys.yusp.web.client.http.outerdata.ztod01;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.req.Ztod01ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.resp.Ztod01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.outerdata.ztod01.req.Ztod01ReqService;
import cn.com.yusys.yusp.online.client.http.outerdata.ztod01.resp.Ztod01RespService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(ztod01)")
@RestController
@RequestMapping("/api/dscms2outerdata")
public class Dscms2Ztod01Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ztod01Resource.class);
    @Autowired
    private RestTemplate restTemplate;

    @Value("${application.outerdata.url}")
    private String outerdataUrl;

    /**
     * 融E开-工商数据查询
     *
     * @param ztod01ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ztod01:融E开-工商数据查询")
    @PostMapping("/ztod01")
    protected @ResponseBody
    ResultDto<cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.resp.Data> ztod01(@Validated @RequestBody Ztod01ReqDto ztod01ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZTOD01.key, EsbEnum.TRADE_CODE_ZTOD01.value, JSON.toJSONString(ztod01ReqDto));

        Ztod01ReqService ztod01ReqService = new Ztod01ReqService();
        Ztod01RespService ztod01RespService = new Ztod01RespService();
        Ztod01RespDto ztod01RespDto = new Ztod01RespDto();
        ResultDto<Ztod01RespDto> ztod01ResultDto = new ResultDto<Ztod01RespDto>();
        ResultDto<cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.resp.Data> ztod01ResultData = new ResultDto<>();
        cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.resp.Data respDtoData = new cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.resp.Data();
        try {
            // 将Ztod01ReqDto转换成Ztod01ReqService
            BeanUtils.copyProperties(ztod01ReqDto, ztod01ReqService);
            ztod01ReqService.setPrcscd(EsbEnum.TRADE_CODE_IDCHECK.key);//    交易码
            ztod01ReqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            ztod01ReqService.setServsq(servsq);//    渠道流水
            ztod01ReqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            ztod01ReqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(ztod01ReqService), headers);
            //String url = "http://10.28.122.177:7006/zhongshunew/api";// TODO SIT环境的测试URL
            String url = outerdataUrl + "/zhongshunew/api";// TODO SIT环境的测试URL
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, JSON.toJSONString(ztod01ReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, responseEntity);

            String responseStr = responseEntity.getBody();
            // 此处获得的data是一个字符串不是json对象 将它取出来转换之后再放进去 最后转pojo实体对象
            JSONObject jsonObject = JSON.parseObject(responseStr);
            String data = jsonObject.get("data").toString();
            JSONObject dataObject = JSON.parseObject(data);
            jsonObject.put("data", dataObject);
            ztod01RespService = JSON.toJavaObject(jsonObject, Ztod01RespService.class);
            ztod01ResultDto.setCode(Optional.ofNullable(ztod01RespService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ztod01ResultDto.setMessage(Optional.ofNullable(ztod01RespService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));

            if (Objects.equals(SuccessEnum.SUCCESS.key, ztod01RespService.getErorcd())) {
                ztod01RespDto = JSON.toJavaObject(jsonObject, Ztod01RespDto.class);
                respDtoData = ztod01RespDto.getData();
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, e.getMessage());
            ztod01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ztod01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        //ztod01ResultDto.setData(ztod01RespDto);
        ztod01ResultData.setData(respDtoData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZTOD01.key, EsbEnum.TRADE_CODE_ZTOD01.value, JSON.toJSONString(ztod01ResultDto));
        // return ztod01ResultDto;
        return ztod01ResultData;
    }
}
