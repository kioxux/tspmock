package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:综合授信申请信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日15:10:55
 */
public class LimitApplyInfo {
    private List<LimitApplyInfoRecord> record; // 综合授信申请信息

    public List<LimitApplyInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LimitApplyInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LimitApplyInfo{" +
                "record=" + record +
                '}';
    }
}
