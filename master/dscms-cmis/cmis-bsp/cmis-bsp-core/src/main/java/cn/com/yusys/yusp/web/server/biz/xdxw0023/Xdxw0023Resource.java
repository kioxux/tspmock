package cn.com.yusys.yusp.web.server.biz.xdxw0023;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0023.req.Xdxw0023ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0023.resp.Xdxw0023RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0023.req.Xdxw0023DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0023.resp.Xdxw0023DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:在小贷是否有调查申请记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0023:在小贷是否有调查申请记录")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0023Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0023Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0023
     * 交易描述：在小贷是否有调查申请记录
     *
     * @param xdxw0023ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("在小贷是否有调查申请记录")
    @PostMapping("/xdxw0023")
    //@Idempotent({"xdcaxw0023", "#xdxw0023ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0023RespDto xdxw0023(@Validated @RequestBody Xdxw0023ReqDto xdxw0023ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023ReqDto));
        Xdxw0023DataReqDto xdxw0023DataReqDto = new Xdxw0023DataReqDto();// 请求Data： 在小贷是否有调查申请记录
        Xdxw0023DataRespDto xdxw0023DataRespDto = new Xdxw0023DataRespDto();// 响应Data：在小贷是否有调查申请记录
        Xdxw0023RespDto xdxw0023RespDto = new Xdxw0023RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0023.req.Data reqData = null; // 请求Data：在小贷是否有调查申请记录
        cn.com.yusys.yusp.dto.server.biz.xdxw0023.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0023.resp.Data();// 响应Data：在小贷是否有调查申请记录
        try {
            // 从 xdxw0023ReqDto获取 reqData
            reqData = xdxw0023ReqDto.getData();
            // 将 reqData 拷贝到xdxw0023DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0023DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023DataReqDto));
            ResultDto<Xdxw0023DataRespDto> xdxw0023DataResultDto = dscmsBizXwClientService.xdxw0023(xdxw0023DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0023RespDto.setErorcd(Optional.ofNullable(xdxw0023DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0023RespDto.setErortx(Optional.ofNullable(xdxw0023DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0023DataResultDto.getCode())) {
                xdxw0023DataRespDto = xdxw0023DataResultDto.getData();
                xdxw0023RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0023RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0023DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0023DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, e.getMessage());
            xdxw0023RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0023RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0023RespDto.setDatasq(servsq);

        xdxw0023RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0023.key, DscmsEnum.TRADE_CODE_XDXW0023.value, JSON.toJSONString(xdxw0023RespDto));
        return xdxw0023RespDto;
    }
}
