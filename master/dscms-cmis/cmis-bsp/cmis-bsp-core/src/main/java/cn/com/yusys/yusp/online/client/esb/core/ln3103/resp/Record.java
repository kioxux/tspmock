package cn.com.yusys.yusp.online.client.esb.core.ln3103.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款账户交易明细查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private Integer mingxixh;//明细序号
    private String yngyjigo;//营业机构
    private String chanpdma;//产品代码
    private String kuaijilb;//会计类别
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String huobdhao;//货币代号
    private String jiaoyirq;//交易日期
    private String jiaoyifx;//交易方向
    private BigDecimal jiaoyije;//交易金额
    private BigDecimal yueeeeee;//余额
    private String yeziduan;//贷款余额字段
    private String yuezdshm;//余额字段说明
    private String jiaoyijg;//交易机构
    private String jiaoyigy;//交易柜员
    private String jiaoyils;//交易流水
    private String jiaoyisj;//交易事件
    private String shjshuom;//事件说明
    private String jiaoyima;//交易码
    private String zhaiyoms;//摘要
    private String chzhbzhi;//冲正标志
    private String bchzhbzh;//被冲正标志
    private String czhyriqi;//错账原日期
    private String czhygyls;//错账原柜员流水号
    private String beizhuuu;//备注信息
    private String farendma;//法人代码
    private String weihguiy;//维护柜员
    private String weihjigo;//维护机构
    private String weihriqi;//维护日期
    private Long shijchuo;//时间戳
    private String jiluztai;//记录状态

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Integer mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getKuaijilb() {
        return kuaijilb;
    }

    public void setKuaijilb(String kuaijilb) {
        this.kuaijilb = kuaijilb;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyifx() {
        return jiaoyifx;
    }

    public void setJiaoyifx(String jiaoyifx) {
        this.jiaoyifx = jiaoyifx;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public BigDecimal getYueeeeee() {
        return yueeeeee;
    }

    public void setYueeeeee(BigDecimal yueeeeee) {
        this.yueeeeee = yueeeeee;
    }

    public String getYeziduan() {
        return yeziduan;
    }

    public void setYeziduan(String yeziduan) {
        this.yeziduan = yeziduan;
    }

    public String getYuezdshm() {
        return yuezdshm;
    }

    public void setYuezdshm(String yuezdshm) {
        this.yuezdshm = yuezdshm;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getShjshuom() {
        return shjshuom;
    }

    public void setShjshuom(String shjshuom) {
        this.shjshuom = shjshuom;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getChzhbzhi() {
        return chzhbzhi;
    }

    public void setChzhbzhi(String chzhbzhi) {
        this.chzhbzhi = chzhbzhi;
    }

    public String getBchzhbzh() {
        return bchzhbzh;
    }

    public void setBchzhbzh(String bchzhbzh) {
        this.bchzhbzh = bchzhbzh;
    }

    public String getCzhyriqi() {
        return czhyriqi;
    }

    public void setCzhyriqi(String czhyriqi) {
        this.czhyriqi = czhyriqi;
    }

    public String getCzhygyls() {
        return czhygyls;
    }

    public void setCzhygyls(String czhygyls) {
        this.czhygyls = czhygyls;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public Long getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Long shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", mingxixh=" + mingxixh +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", chanpdma='" + chanpdma + '\'' +
                ", kuaijilb='" + kuaijilb + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyifx='" + jiaoyifx + '\'' +
                ", jiaoyije=" + jiaoyije +
                ", yueeeeee=" + yueeeeee +
                ", yeziduan='" + yeziduan + '\'' +
                ", yuezdshm='" + yuezdshm + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jiaoyisj='" + jiaoyisj + '\'' +
                ", shjshuom='" + shjshuom + '\'' +
                ", jiaoyima='" + jiaoyima + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", chzhbzhi='" + chzhbzhi + '\'' +
                ", bchzhbzh='" + bchzhbzh + '\'' +
                ", czhyriqi='" + czhyriqi + '\'' +
                ", czhygyls='" + czhygyls + '\'' +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", farendma='" + farendma + '\'' +
                ", weihguiy='" + weihguiy + '\'' +
                ", weihjigo='" + weihjigo + '\'' +
                ", weihriqi='" + weihriqi + '\'' +
                ", shijchuo=" + shijchuo +
                ", jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
