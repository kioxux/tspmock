package cn.com.yusys.yusp.online.client.esb.irs.xirs27.resp;

/**
 * 响应Service：工作台提示条数
 *
 * @author chenyong
 * @version 1.0
 */
public class Xirs27RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xirs27RespService{" +
                "service=" + service +
                '}';
    }
}
