package cn.com.yusys.yusp.web.client.esb.core.ln3111;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3111.req.Ln3111ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3111.resp.Ln3111RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3111)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3111Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3111Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3111
     * 交易描述：贷款归还试算
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3111:贷款归还试算")
    @PostMapping("/ln3111")
    protected @ResponseBody
    ResultDto<Ln3111RespDto> ln3111(@Validated @RequestBody Ln3111ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3111.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3111.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3111.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3111.resp.Service();
        Ln3111ReqService ln3111ReqService = new Ln3111ReqService();
        Ln3111RespService ln3111RespService = new Ln3111RespService();
        Ln3111RespDto ln3111RespDto = new Ln3111RespDto();
        ResultDto<Ln3111RespDto> ln3111ResultDto = new ResultDto<Ln3111RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3111ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3111.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3111ReqService.setService(reqService);
            // 将ln3111ReqService转换成ln3111ReqServiceMap
            Map ln3111ReqServiceMap = beanMapUtil.beanToMap(ln3111ReqService);
            context.put("tradeDataMap", ln3111ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3111.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3111RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3111RespService.class, Ln3111RespService.class);
            respService = ln3111RespService.getService();

            ln3111ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3111ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3111RespDto
                BeanUtils.copyProperties(respService, ln3111RespDto);
                ln3111ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3111ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3111ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3111ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value, e.getMessage());
            ln3111ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3111ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3111ResultDto.setData(ln3111RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3111.key, EsbEnum.TRADE_CODE_LN3111.value, JSON.toJSONString(ln3111ResultDto));
        return ln3111ResultDto;
    }
}
