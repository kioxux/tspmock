package cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.req;

import java.math.BigDecimal;

/**
 * 请求Body：永久额度调整
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Body {

    private String cardno;//  卡号
    private String crcycd;//  币种
    private String opt;//  功能码
    private BigDecimal cdtlmt;//  信用额度

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getOpt() {
        return opt;
    }

    public void setOpt(String opt) {
        this.opt = opt;
    }

    public BigDecimal getCdtlmt() {
        return cdtlmt;
    }

    public void setCdtlmt(BigDecimal cdtlmt) {
        this.cdtlmt = cdtlmt;
    }

    @Override
    public String toString() {
        return "Body{" +
                "cardno='" + cardno + '\'' +
                ", crcycd='" + crcycd + '\'' +
                ", opt='" + opt + '\'' +
                ", cdtlmt=" + cdtlmt +
                '}';
    }
}
