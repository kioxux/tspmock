package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdzqgjh;

import java.math.BigDecimal;

public class Record {
    private Integer benqqish;//本期期数

    private String qishriqi;//起始日期

    private String daoqriqi;//到期日期

    private BigDecimal hxijinee;//还息金额

    private String zwhkriqi;//最晚还款日

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public String getZwhkriqi() {
        return zwhkriqi;
    }

    public void setZwhkriqi(String zwhkriqi) {
        this.zwhkriqi = zwhkriqi;
    }

    @Override
    public String toString() {
        return "Record{" +
                "benqqish=" + benqqish +
                ", qishriqi='" + qishriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", hxijinee=" + hxijinee +
                ", zwhkriqi='" + zwhkriqi + '\'' +
                '}';
    }
}
