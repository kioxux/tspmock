package cn.com.yusys.yusp.online.client.esb.circp.fb1205.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 请求Service：放款信息推送
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "CUST_ID")
    private String CUST_ID;//客户号
    @JsonProperty(value = "CONT_NO")
    private String CONT_NO;//借款合同号
    @JsonProperty(value = "GUAR_CONT_NO")
    private String GUAR_CONT_NO;//担保合同号
    @JsonProperty(value = "LOAN_ACC")
    private String LOAN_ACC;//放款账号
    @JsonProperty(value = "BILL_NO")
    private BigDecimal BILL_NO;//借据号
    @JsonProperty(value = "LOAN_AMT")
    private BigDecimal LOAN_AMT;//借款金额
    @JsonProperty(value = "LOAN_BAL")
    private String LOAN_BAL;//贷款余额
    @JsonProperty(value = "CONT_STATUS")
    private String CONT_STATUS;//贷款状态
    @JsonProperty(value = "CUR_TYPE")
    private String CUR_TYPE;//币种
    @JsonProperty(value = "LOAN_START_DATE")
    private String LOAN_START_DATE;//放款日期
    @JsonProperty(value = "LOAN_END_DATE")
    private String LOAN_END_DATE;//到期日期
    @JsonProperty(value = "LOAN_TERM")
    private String LOAN_TERM;//贷款期限（月）
    @JsonProperty(value = "LOAN_RATE")
    private String LOAN_RATE;//贷款利率%
    @JsonProperty(value = "REPAY_TYPE")
    private String REPAY_TYPE;//还款方式
    @JsonProperty(value = "REPAY_ACCOUNT")
    private String REPAY_ACCOUNT;//还款账号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }


    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getCUST_ID() {
        return CUST_ID;
    }

    @JsonIgnore
    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    @JsonIgnore
    public String getCONT_NO() {
        return CONT_NO;
    }

    @JsonIgnore
    public void setCONT_NO(String CONT_NO) {
        this.CONT_NO = CONT_NO;
    }

    @JsonIgnore
    public String getGUAR_CONT_NO() {
        return GUAR_CONT_NO;
    }

    @JsonIgnore
    public void setGUAR_CONT_NO(String GUAR_CONT_NO) {
        this.GUAR_CONT_NO = GUAR_CONT_NO;
    }

    @JsonIgnore
    public String getLOAN_ACC() {
        return LOAN_ACC;
    }

    @JsonIgnore
    public void setLOAN_ACC(String LOAN_ACC) {
        this.LOAN_ACC = LOAN_ACC;
    }

    @JsonIgnore
    public BigDecimal getBILL_NO() {
        return BILL_NO;
    }

    @JsonIgnore
    public void setBILL_NO(BigDecimal BILL_NO) {
        this.BILL_NO = BILL_NO;
    }

    @JsonIgnore
    public BigDecimal getLOAN_AMT() {
        return LOAN_AMT;
    }

    @JsonIgnore
    public void setLOAN_AMT(BigDecimal LOAN_AMT) {
        this.LOAN_AMT = LOAN_AMT;
    }

    @JsonIgnore
    public String getLOAN_BAL() {
        return LOAN_BAL;
    }

    @JsonIgnore
    public void setLOAN_BAL(String LOAN_BAL) {
        this.LOAN_BAL = LOAN_BAL;
    }

    @JsonIgnore
    public String getCONT_STATUS() {
        return CONT_STATUS;
    }

    @JsonIgnore
    public void setCONT_STATUS(String CONT_STATUS) {
        this.CONT_STATUS = CONT_STATUS;
    }

    @JsonIgnore
    public String getCUR_TYPE() {
        return CUR_TYPE;
    }

    @JsonIgnore
    public void setCUR_TYPE(String CUR_TYPE) {
        this.CUR_TYPE = CUR_TYPE;
    }

    @JsonIgnore
    public String getLOAN_START_DATE() {
        return LOAN_START_DATE;
    }

    public void setLOAN_START_DATE(String LOAN_START_DATE) {
        this.LOAN_START_DATE = LOAN_START_DATE;
    }

    @JsonIgnore
    public String getLOAN_END_DATE() {
        return LOAN_END_DATE;
    }

    @JsonIgnore
    public void setLOAN_END_DATE(String LOAN_END_DATE) {
        this.LOAN_END_DATE = LOAN_END_DATE;
    }

    @JsonIgnore
    public String getLOAN_TERM() {
        return LOAN_TERM;
    }

    @JsonIgnore
    public void setLOAN_TERM(String LOAN_TERM) {
        this.LOAN_TERM = LOAN_TERM;
    }

    @JsonIgnore
    public String getLOAN_RATE() {
        return LOAN_RATE;
    }

    @JsonIgnore
    public void setLOAN_RATE(String LOAN_RATE) {
        this.LOAN_RATE = LOAN_RATE;
    }

    @JsonIgnore
    public String getREPAY_TYPE() {
        return REPAY_TYPE;
    }

    @JsonIgnore
    public void setREPAY_TYPE(String REPAY_TYPE) {
        this.REPAY_TYPE = REPAY_TYPE;
    }

    @JsonIgnore
    public String getREPAY_ACCOUNT() {
        return REPAY_ACCOUNT;
    }

    @JsonIgnore
    public void setREPAY_ACCOUNT(String REPAY_ACCOUNT) {
        this.REPAY_ACCOUNT = REPAY_ACCOUNT;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", CUST_ID='" + CUST_ID + '\'' +
                ", CONT_NO='" + CONT_NO + '\'' +
                ", GUAR_CONT_NO='" + GUAR_CONT_NO + '\'' +
                ", LOAN_ACC='" + LOAN_ACC + '\'' +
                ", BILL_NO=" + BILL_NO +
                ", LOAN_AMT=" + LOAN_AMT +
                ", LOAN_BAL='" + LOAN_BAL + '\'' +
                ", CONT_STATUS='" + CONT_STATUS + '\'' +
                ", CUR_TYPE='" + CUR_TYPE + '\'' +
                ", LOAN_START_DATE='" + LOAN_START_DATE + '\'' +
                ", LOAN_END_DATE='" + LOAN_END_DATE + '\'' +
                ", LOAN_TERM='" + LOAN_TERM + '\'' +
                ", LOAN_RATE='" + LOAN_RATE + '\'' +
                ", REPAY_TYPE='" + REPAY_TYPE + '\'' +
                ", REPAY_ACCOUNT='" + REPAY_ACCOUNT + '\'' +
                '}';
    }
}
