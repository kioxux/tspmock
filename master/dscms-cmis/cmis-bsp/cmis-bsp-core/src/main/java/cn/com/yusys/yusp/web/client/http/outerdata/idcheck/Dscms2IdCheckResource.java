package cn.com.yusys.yusp.web.client.http.outerdata.idcheck;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.http.outerdata.idcheck.req.IdCheckReqService;
import cn.com.yusys.yusp.online.client.http.outerdata.idcheck.resp.Data;
import cn.com.yusys.yusp.online.client.http.outerdata.idcheck.resp.IdCheckRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(idCheck)")
@RestController
@RequestMapping("/api/dscms2outerdata")
public class Dscms2IdCheckResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2IdCheckResource.class);
    @Autowired
    private RestTemplate restTemplate;
    @Value("${application.outerdata.url}")
    private String outerdataUrl;

    /**
     * 身份核查
     *
     * @param idCheckReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("idCheck:身份核查")
    @PostMapping("/idCheck")
    protected @ResponseBody
    ResultDto<IdCheckRespDto> idCheck(@Validated @RequestBody IdCheckReqDto idCheckReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, JSON.toJSONString(idCheckReqDto));

        IdCheckReqService idCheckReqService = new IdCheckReqService();
        IdCheckRespService idCheckRespService = new IdCheckRespService();
        IdCheckRespDto idCheckRespDto = new IdCheckRespDto();
        ResultDto<IdCheckRespDto> idCheckResultDto = new ResultDto<IdCheckRespDto>();
        try {
            // 将IdCheckReqDto转换成IdCheckReqService
            BeanUtils.copyProperties(idCheckReqDto, idCheckReqService);
            idCheckReqService.setPrcscd(EsbEnum.TRADE_CODE_IDCHECK.key);//    交易码
            idCheckReqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            idCheckReqService.setServsq(servsq);//    渠道流水
            idCheckReqService.setUserid(EsbEnum.USERID_OUTERDATA.key);//    柜员号
            idCheckReqService.setBrchno(EsbEnum.BRCHNO_OUTERDATA.key);//    部门号

            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(idCheckReqService), headers);
            //String url = "http://10.28.122.177:7006/edataApi/data";// TODO SIT环境的测试URL
            String url = outerdataUrl + "/edataApi/data";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, JSON.toJSONString(idCheckReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, responseEntity);

            String responseStr = responseEntity.getBody();
            idCheckRespService = (IdCheckRespService) JSON.parseObject(responseStr, IdCheckRespService.class);
            Data idCheckRespData = new Data();

            //  将IdCheckRespDto封装到ResultDto<IdCheckRespDto>
            idCheckResultDto.setCode(Optional.ofNullable(idCheckRespService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            idCheckResultDto.setMessage(Optional.ofNullable(idCheckRespService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));

            if (Objects.equals(SuccessEnum.SUCCESS.key, idCheckRespService.getErorcd())) {
                idCheckRespData = idCheckRespService.getData();
                String code = Optional.ofNullable(idCheckRespData.getCode()).orElseGet(idCheckRespData::getCode);
                if (Objects.equals("00", code)) {// 先校验
                    //  将IdCheckRespService转换成IdCheckRespDto
                    BeanUtils.copyProperties(idCheckRespData, idCheckRespDto);
                    idCheckResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                    idCheckResultDto.setMessage(SuccessEnum.SUCCESS.value);
                } else {
                    String msg = Optional.ofNullable(idCheckRespData.getMsg()).orElseGet(idCheckRespData::getMsg);
                    //  将IdCheckRespService转换成IdCheckRespDto
                    BeanUtils.copyProperties(idCheckRespData, idCheckRespDto);
                    idCheckResultDto.setCode(code);
                    idCheckResultDto.setMessage(msg);
                }
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, e.getMessage());
            idCheckResultDto.setCode(EpbEnum.EPB099999.key);//9999
            idCheckResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        idCheckResultDto.setData(idCheckRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IDCHECK.key, EsbEnum.TRADE_CODE_IDCHECK.value, JSON.toJSONString(idCheckResultDto));
        return idCheckResultDto;
    }
}
