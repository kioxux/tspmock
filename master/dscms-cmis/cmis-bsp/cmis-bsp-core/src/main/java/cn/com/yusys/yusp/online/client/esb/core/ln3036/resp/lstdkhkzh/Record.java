package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkhkzh;

import java.math.BigDecimal;

public class Record {
    private String hkjshzhl;//还款基数种类
    private String hkzhhgze;//还款账户规则
    private String hkzhhzhl;//还款账户种类
    private BigDecimal huankbli;//还款比例
    private String shengxrq;//生效日期
    private String daoqriqi;//到期日期
    private Integer youxianj;//优先级
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String hkzhhmch;//还款账户名称

    public String getHkjshzhl() {
        return hkjshzhl;
    }

    public void setHkjshzhl(String hkjshzhl) {
        this.hkjshzhl = hkjshzhl;
    }

    public String getHkzhhgze() {
        return hkzhhgze;
    }

    public void setHkzhhgze(String hkzhhgze) {
        this.hkzhhgze = hkzhhgze;
    }

    public String getHkzhhzhl() {
        return hkzhhzhl;
    }

    public void setHkzhhzhl(String hkzhhzhl) {
        this.hkzhhzhl = hkzhhzhl;
    }

    public BigDecimal getHuankbli() {
        return huankbli;
    }

    public void setHuankbli(BigDecimal huankbli) {
        this.huankbli = huankbli;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public Integer getYouxianj() {
        return youxianj;
    }

    public void setYouxianj(Integer youxianj) {
        this.youxianj = youxianj;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHkzhhmch() {
        return hkzhhmch;
    }

    public void setHkzhhmch(String hkzhhmch) {
        this.hkzhhmch = hkzhhmch;
    }

    @Override
    public String toString() {
        return "Record{" +
                "hkjshzhl='" + hkjshzhl + '\'' +
                "hkzhhgze='" + hkzhhgze + '\'' +
                "hkzhhzhl='" + hkzhhzhl + '\'' +
                "huankbli='" + huankbli + '\'' +
                "shengxrq='" + shengxrq + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "youxianj='" + youxianj + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hkzhhmch='" + hkzhhmch + '\'' +
                '}';
    }
}
