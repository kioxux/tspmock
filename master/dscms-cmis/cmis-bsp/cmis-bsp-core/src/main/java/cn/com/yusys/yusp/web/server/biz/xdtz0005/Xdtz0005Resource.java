package cn.com.yusys.yusp.web.server.biz.xdtz0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0005.req.Xdtz0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0005.resp.Xdtz0005RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0005.req.Xdtz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0005.resp.Xdtz0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据流水号查询是否放款标记
 *
 * @author xull
 * @version 1.0
 */
@Api(tags = "XDTZ0005:根据流水号查询是否放款标记")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0005Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0005
     * 交易描述：根据流水号查询是否放款标记
     *
     * @param xdtz0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdtz0005:根据流水号查询是否放款标记")
    @PostMapping("/xdtz0005")
    //@Idempotent({"xdcatz0005", "#xdtz0005ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0005RespDto xdtz0005(@Validated @RequestBody Xdtz0005ReqDto xdtz0005ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005ReqDto));

        Xdtz0005DataReqDto xdtz0005DataReqDto = new Xdtz0005DataReqDto();// 请求Data： 根据流水号查询是否放款标记
        Xdtz0005DataRespDto xdtz0005DataRespDto = new Xdtz0005DataRespDto();// 响应Data：根据流水号查询是否放款标记
        Xdtz0005RespDto xdtz0005RespDto = new Xdtz0005RespDto();// 响应Dto：客户准入级别同步
        //  此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdtz0005.req.Data reqData = null; // 请求Data：根据流水号查询是否放款标记
        cn.com.yusys.yusp.dto.server.biz.xdtz0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0005.resp.Data();
        // 响应Data：根据流水号查询是否放款标记
        //  此处包导入待确定 结束
        // 从 xdtz0005ReqDto获取 reqData
        try {
            reqData = xdtz0005ReqDto.getData();
            // 将 reqData 拷贝到xdtz0005DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0005DataReqDto);
            // 调用服务
			logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataReqDto));
            ResultDto<Xdtz0005DataRespDto> xdtz0005DataResultDto = dscmsBizTzClientService.xdtz0005(xdtz0005DataReqDto);
			logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0005RespDto.setErorcd(Optional.ofNullable(xdtz0005DataResultDto.getCode()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdtz0005RespDto.setErortx(Optional.ofNullable(xdtz0005DataResultDto.getMessage()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0005DataResultDto.getCode())) {
                xdtz0005DataRespDto = xdtz0005DataResultDto.getData();
                xdtz0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, e.getMessage());
            xdtz0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0005RespDto.setDatasq(servsq);

        xdtz0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0005.key, DscmsEnum.TRADE_CODE_XDTZ0005.value, JSON.toJSONString(xdtz0005RespDto));
        return xdtz0005RespDto;
    }
}
