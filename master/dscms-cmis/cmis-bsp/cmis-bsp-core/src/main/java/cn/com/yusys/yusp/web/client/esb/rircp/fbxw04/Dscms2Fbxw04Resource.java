package cn.com.yusys.yusp.web.client.esb.rircp.fbxw04;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.req.Fbxw04ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.resp.Fbxw04RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw04Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw04Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB惠享贷规则审批申请接口（处理码fbxw04）
     *
     * @param fbxw04ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw04:ESB惠享贷规则审批申请接口")
    @PostMapping("/fbxw04")
    protected @ResponseBody
    ResultDto<Fbxw04RespDto> fbxw04(@Validated @RequestBody Fbxw04ReqDto fbxw04ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value, JSON.toJSONString(fbxw04ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.resp.Service();
        Fbxw04ReqService fbxw04ReqService = new Fbxw04ReqService();
        Fbxw04RespService fbxw04RespService = new Fbxw04RespService();
        Fbxw04RespDto fbxw04RespDto = new Fbxw04RespDto();
        ResultDto<Fbxw04RespDto> fbxw04ResultDto = new ResultDto<Fbxw04RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw04ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw04ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW04.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw04ReqService.setService(reqService);
            // 将fbxw04ReqService转换成fbxw04ReqServiceMap
            Map fbxw04ReqServiceMap = beanMapUtil.beanToMap(fbxw04ReqService);
            context.put("tradeDataMap", fbxw04ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW04.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw04RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw04RespService.class, Fbxw04RespService.class);
            respService = fbxw04RespService.getService();

            //  将Fbxw04RespDto封装到ResultDto<Fbxw04RespDto>
            fbxw04ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw04ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw04RespDto
                BeanUtils.copyProperties(respService, fbxw04RespDto);
                fbxw04ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw04ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw04ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw04ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value, e.getMessage());
            fbxw04ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw04ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw04ResultDto.setData(fbxw04RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW04.key, EsbEnum.TRADE_CODE_FBXW04.value, JSON.toJSONString(fbxw04ResultDto));
        return fbxw04ResultDto;
    }
}
