package cn.com.yusys.yusp.online.client.esb.xwh.xwh003.req;

/**
 * 请求Service：核销锁定接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwh003ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwh003ReqService{" +
                "service=" + service +
                '}';
    }
}
