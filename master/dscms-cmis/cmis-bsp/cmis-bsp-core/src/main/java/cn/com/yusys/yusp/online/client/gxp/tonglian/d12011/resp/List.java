package cn.com.yusys.yusp.online.client.gxp.tonglian.d12011.resp;

/**
 * 响应Dto：账单交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
