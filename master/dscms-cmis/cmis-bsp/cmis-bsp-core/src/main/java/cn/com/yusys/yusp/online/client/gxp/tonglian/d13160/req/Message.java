package cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req;

import cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead;

import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/27 9:32:<br>
 * 请求Message：大额现金分期申请
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 9:32
 * @since 2021/5/27 9:32
 */
public class Message {
    private cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead head;
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Body body;

    public GxpReqHead getHead() {
        return head;
    }

    public void setHead(GxpReqHead head) {
        this.head = head;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Message{" +
                "head=" + head +
                ", body=" + body +
                '}';
    }
}
