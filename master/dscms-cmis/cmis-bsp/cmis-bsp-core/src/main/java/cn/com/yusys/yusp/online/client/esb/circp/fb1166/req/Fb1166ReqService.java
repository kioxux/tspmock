package cn.com.yusys.yusp.online.client.esb.circp.fb1166.req;

/**
 * 请求Service：面签邀约结果推送
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1166ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1166ReqService{" +
                "service=" + service +
                '}';
    }
}
