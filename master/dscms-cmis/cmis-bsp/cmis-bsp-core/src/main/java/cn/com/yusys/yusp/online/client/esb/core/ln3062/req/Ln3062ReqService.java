package cn.com.yusys.yusp.online.client.esb.core.ln3062.req;

/**
 * 请求Service：资产转让借据维护
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3062ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3062ReqService{" +
                "service=" + service +
                '}';
    }
}
