package cn.com.yusys.yusp.web.client.esb.core.ln3103;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3103.req.Ln3103ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Listnm_ARRAY;
import cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Ln3103RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3103)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3103Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3103Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3103
     * 交易描述：贷款账户交易明细查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3103:贷款账户交易明细查询")
    @PostMapping("/ln3103")
    protected @ResponseBody
    ResultDto<Ln3103RespDto> ln3103(@Validated @RequestBody Ln3103ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3103.key, EsbEnum.TRADE_CODE_LN3103.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3103.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3103.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Service();
        Ln3103ReqService ln3103ReqService = new Ln3103ReqService();
        Ln3103RespService ln3103RespService = new Ln3103RespService();
        Ln3103RespDto ln3103RespDto = new Ln3103RespDto();
        ResultDto<Ln3103RespDto> ln3103ResultDto = new ResultDto<Ln3103RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3103ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3103.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3103ReqService.setService(reqService);
            // 将ln3103ReqService转换成ln3103ReqServiceMap
            Map ln3103ReqServiceMap = beanMapUtil.beanToMap(ln3103ReqService);
            context.put("tradeDataMap", ln3103ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3103.key, EsbEnum.TRADE_CODE_LN3103.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3103.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3103.key, EsbEnum.TRADE_CODE_LN3103.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3103RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3103RespService.class, Ln3103RespService.class);
            respService = ln3103RespService.getService();

            ln3103ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3103ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3103RespDto
                BeanUtils.copyProperties(respService, ln3103RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Listnm_ARRAY ln3103RespList = Optional.ofNullable(respService.getListnm_ARRAY())
                        .orElseGet(() -> new Listnm_ARRAY());
                respService.setListnm_ARRAY(ln3103RespList);
                if (CollectionUtils.nonEmpty(respService.getListnm_ARRAY().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Record> recordList =
                            Optional.ofNullable(respService.getListnm_ARRAY().getRecord()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3103.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3103.Listnm> listnmList =
                            recordList.parallelStream().map(record -> {
                                cn.com.yusys.yusp.dto.client.esb.core.ln3103.Listnm listnm = new cn.com.yusys.yusp.dto.client.esb.core.ln3103.Listnm();
                                BeanUtils.copyProperties(record, listnm);
                                return listnm;
                            }).collect(Collectors.toList());
                    ln3103RespDto.setListnm(listnmList);
                }

                ln3103ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3103ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3103ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3103ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3103.key, EsbEnum.TRADE_CODE_LN3103.value, e.getMessage());
            ln3103ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3103ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3103ResultDto.setData(ln3103RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3103.key, EsbEnum.TRADE_CODE_LN3103.value, JSON.toJSONString(ln3103ResultDto));
        return ln3103ResultDto;
    }
}
