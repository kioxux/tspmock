package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	失信被执行人信息
@JsonPropertyOrder(alphabetic = true)
public class PUNISHBREAK implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "AGECLEAN")
    private String AGECLEAN;//	年龄
    @JsonProperty(value = "AREANAMECLEAN")
    private String AREANAMECLEAN;//	省份
    @JsonProperty(value = "BUSINESSENTITY")
    private String BUSINESSENTITY;//	法定代表人或者负责人姓名
    @JsonProperty(value = "CARDNUM")
    private String CARDNUM;//	身份证号码/工商注册号
    @JsonProperty(value = "CASECODE")
    private String CASECODE;//	案号
    @JsonProperty(value = "COURTNAME")
    private String COURTNAME;//	执行法院
    @JsonProperty(value = "DISRUPTTYPENAME")
    private String DISRUPTTYPENAME;//	失信被执行人行为具体情形
    @JsonProperty(value = "DUTY")
    private String DUTY;//	生效法律文书确定的义务
    @JsonProperty(value = "GISTID")
    private String GISTID;//	执行依据文号
    @JsonProperty(value = "GISTUNIT")
    private String GISTUNIT;//	做出执行依据单位
    @JsonProperty(value = "INAMECLEAN")
    private String INAMECLEAN;//	被执行人姓名/名称
    @JsonProperty(value = "PERFORMANCE")
    private String PERFORMANCE;//	被执行人履行情况
    @JsonProperty(value = "PERFORMEDPART")
    private String PERFORMEDPART;//	已履行（元）
    @JsonProperty(value = "PUBLISHDATECLEAN")
    private String PUBLISHDATECLEAN;//	发布时间
    @JsonProperty(value = "REGDATECLEAN")
    private String REGDATECLEAN;//	立案时间
    @JsonProperty(value = "SEXYCLEAN")
    private String SEXYCLEAN;//	性别
    @JsonProperty(value = "TYPE")
    private String TYPE;//	失信人类型:自然人，法人或其他组织
    @JsonProperty(value = "UNPERFORMPART")
    private String UNPERFORMPART;//	未履行（元）
    @JsonProperty(value = "YSFZD")
    private String YSFZD;//	身份证原始发证地

    @JsonIgnore
    public String getAGECLEAN() {
        return AGECLEAN;
    }

    @JsonIgnore
    public void setAGECLEAN(String AGECLEAN) {
        this.AGECLEAN = AGECLEAN;
    }

    @JsonIgnore
    public String getAREANAMECLEAN() {
        return AREANAMECLEAN;
    }

    @JsonIgnore
    public void setAREANAMECLEAN(String AREANAMECLEAN) {
        this.AREANAMECLEAN = AREANAMECLEAN;
    }

    @JsonIgnore
    public String getBUSINESSENTITY() {
        return BUSINESSENTITY;
    }

    @JsonIgnore
    public void setBUSINESSENTITY(String BUSINESSENTITY) {
        this.BUSINESSENTITY = BUSINESSENTITY;
    }

    @JsonIgnore
    public String getCARDNUM() {
        return CARDNUM;
    }

    @JsonIgnore
    public void setCARDNUM(String CARDNUM) {
        this.CARDNUM = CARDNUM;
    }

    @JsonIgnore
    public String getCASECODE() {
        return CASECODE;
    }

    @JsonIgnore
    public void setCASECODE(String CASECODE) {
        this.CASECODE = CASECODE;
    }

    @JsonIgnore
    public String getCOURTNAME() {
        return COURTNAME;
    }

    @JsonIgnore
    public void setCOURTNAME(String COURTNAME) {
        this.COURTNAME = COURTNAME;
    }

    @JsonIgnore
    public String getDISRUPTTYPENAME() {
        return DISRUPTTYPENAME;
    }

    @JsonIgnore
    public void setDISRUPTTYPENAME(String DISRUPTTYPENAME) {
        this.DISRUPTTYPENAME = DISRUPTTYPENAME;
    }

    @JsonIgnore
    public String getDUTY() {
        return DUTY;
    }

    @JsonIgnore
    public void setDUTY(String DUTY) {
        this.DUTY = DUTY;
    }

    @JsonIgnore
    public String getGISTID() {
        return GISTID;
    }

    @JsonIgnore
    public void setGISTID(String GISTID) {
        this.GISTID = GISTID;
    }

    @JsonIgnore
    public String getGISTUNIT() {
        return GISTUNIT;
    }

    @JsonIgnore
    public void setGISTUNIT(String GISTUNIT) {
        this.GISTUNIT = GISTUNIT;
    }

    @JsonIgnore
    public String getINAMECLEAN() {
        return INAMECLEAN;
    }

    @JsonIgnore
    public void setINAMECLEAN(String INAMECLEAN) {
        this.INAMECLEAN = INAMECLEAN;
    }

    @JsonIgnore
    public String getPERFORMANCE() {
        return PERFORMANCE;
    }

    @JsonIgnore
    public void setPERFORMANCE(String PERFORMANCE) {
        this.PERFORMANCE = PERFORMANCE;
    }

    @JsonIgnore
    public String getPERFORMEDPART() {
        return PERFORMEDPART;
    }

    @JsonIgnore
    public void setPERFORMEDPART(String PERFORMEDPART) {
        this.PERFORMEDPART = PERFORMEDPART;
    }

    @JsonIgnore
    public String getPUBLISHDATECLEAN() {
        return PUBLISHDATECLEAN;
    }

    @JsonIgnore
    public void setPUBLISHDATECLEAN(String PUBLISHDATECLEAN) {
        this.PUBLISHDATECLEAN = PUBLISHDATECLEAN;
    }

    @JsonIgnore
    public String getREGDATECLEAN() {
        return REGDATECLEAN;
    }

    @JsonIgnore
    public void setREGDATECLEAN(String REGDATECLEAN) {
        this.REGDATECLEAN = REGDATECLEAN;
    }

    @JsonIgnore
    public String getSEXYCLEAN() {
        return SEXYCLEAN;
    }

    @JsonIgnore
    public void setSEXYCLEAN(String SEXYCLEAN) {
        this.SEXYCLEAN = SEXYCLEAN;
    }

    @JsonIgnore
    public String getTYPE() {
        return TYPE;
    }

    @JsonIgnore
    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    @JsonIgnore
    public String getUNPERFORMPART() {
        return UNPERFORMPART;
    }

    @JsonIgnore
    public void setUNPERFORMPART(String UNPERFORMPART) {
        this.UNPERFORMPART = UNPERFORMPART;
    }

    @JsonIgnore
    public String getYSFZD() {
        return YSFZD;
    }

    @JsonIgnore
    public void setYSFZD(String YSFZD) {
        this.YSFZD = YSFZD;
    }

    @Override
    public String toString() {
        return "PUNISHBREAK{" +
                "AGECLEAN='" + AGECLEAN + '\'' +
                ", AREANAMECLEAN='" + AREANAMECLEAN + '\'' +
                ", BUSINESSENTITY='" + BUSINESSENTITY + '\'' +
                ", CARDNUM='" + CARDNUM + '\'' +
                ", CASECODE='" + CASECODE + '\'' +
                ", COURTNAME='" + COURTNAME + '\'' +
                ", DISRUPTTYPENAME='" + DISRUPTTYPENAME + '\'' +
                ", DUTY='" + DUTY + '\'' +
                ", GISTID='" + GISTID + '\'' +
                ", GISTUNIT='" + GISTUNIT + '\'' +
                ", INAMECLEAN='" + INAMECLEAN + '\'' +
                ", PERFORMANCE='" + PERFORMANCE + '\'' +
                ", PERFORMEDPART='" + PERFORMEDPART + '\'' +
                ", PUBLISHDATECLEAN='" + PUBLISHDATECLEAN + '\'' +
                ", REGDATECLEAN='" + REGDATECLEAN + '\'' +
                ", SEXYCLEAN='" + SEXYCLEAN + '\'' +
                ", TYPE='" + TYPE + '\'' +
                ", UNPERFORMPART='" + UNPERFORMPART + '\'' +
                ", YSFZD='" + YSFZD + '\'' +
                '}';
    }
}
