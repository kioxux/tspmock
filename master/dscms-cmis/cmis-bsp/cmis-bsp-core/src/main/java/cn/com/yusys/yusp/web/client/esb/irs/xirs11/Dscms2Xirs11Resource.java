package cn.com.yusys.yusp.web.client.esb.irs.xirs11;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs11.req.Xirs11ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs11.resp.Xirs11RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.xirs11.req.Xirs11ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.xirs11.resp.Xirs11RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(xirs11)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Xirs11Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xirs11Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 同业客户信息（处理码IRS11）
     *
     * @param xirs11ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xirs11:同业客户信息")
    @PostMapping("/xirs11")
    protected @ResponseBody
    ResultDto<Xirs11RespDto> xirs11(@Validated @RequestBody Xirs11ReqDto xirs11ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS11.key, EsbEnum.TRADE_CODE_IRS11.value, JSON.toJSONString(xirs11ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.xirs11.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.xirs11.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.xirs11.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.xirs11.resp.Service();
        Xirs11ReqService xirs11ReqService = new Xirs11ReqService();
        Xirs11RespService xirs11RespService = new Xirs11RespService();
        Xirs11RespDto xirs11RespDto = new Xirs11RespDto();
        ResultDto<Xirs11RespDto> xirs11ResultDto = new ResultDto<Xirs11RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Xirs11ReqDto转换成reqService
            BeanUtils.copyProperties(xirs11ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS11.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xirs11ReqService.setService(reqService);
            // 将xirs11ReqService转换成xirs11ReqServiceMap
            Map xirs11ReqServiceMap = beanMapUtil.beanToMap(xirs11ReqService);
            context.put("tradeDataMap", xirs11ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS11.key, EsbEnum.TRADE_CODE_IRS11.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS11.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS11.key, EsbEnum.TRADE_CODE_IRS11.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xirs11RespService = beanMapUtil.mapToBean(tradeDataMap, Xirs11RespService.class, Xirs11RespService.class);
            respService = xirs11RespService.getService();

            //  将Xirs11RespDto封装到ResultDto<Xirs11RespDto>
            xirs11ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xirs11ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xirs11RespDto
                BeanUtils.copyProperties(respService, xirs11RespDto);
                xirs11ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xirs11ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xirs11ResultDto.setCode(EpbEnum.EPB099999.key);
                xirs11ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS11.key, EsbEnum.TRADE_CODE_IRS11.value, e.getMessage());
            xirs11ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xirs11ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xirs11ResultDto.setData(xirs11RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS11.key, EsbEnum.TRADE_CODE_IRS11.value, JSON.toJSONString(xirs11ResultDto));
        return xirs11ResultDto;

    }
}
