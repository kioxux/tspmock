package cn.com.yusys.yusp.online.client.esb.circp.fb1213.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Service：押品出库通知
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "CUST_ID")
    private String CUST_ID;//客户号
    @JsonProperty(value = "CUST_NAME")
    private String CUST_NAME;//客户名称
    @JsonProperty(value = "CERT_TYPE")
    private String CERT_TYPE;//证件类型
    @JsonProperty(value = "CERT_NO")
    private String CERT_NO;//证件号码
    @JsonProperty(value = "GUAR_LIST")
    private GUAR_LIST GUAR_LIST;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getCUST_ID() {
        return CUST_ID;
    }

    @JsonIgnore
    public void setCUST_ID(String CUST_ID) {
        this.CUST_ID = CUST_ID;
    }

    @JsonIgnore
    public String getCUST_NAME() {
        return CUST_NAME;
    }

    @JsonIgnore
    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    @JsonIgnore
    public String getCERT_TYPE() {
        return CERT_TYPE;
    }

    @JsonIgnore
    public void setCERT_TYPE(String CERT_TYPE) {
        this.CERT_TYPE = CERT_TYPE;
    }

    @JsonIgnore
    public String getCERT_NO() {
        return CERT_NO;
    }

    @JsonIgnore
    public void setCERT_NO(String CERT_NO) {
        this.CERT_NO = CERT_NO;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.GUAR_LIST getGUAR_LIST() {
        return GUAR_LIST;
    }

    @JsonIgnore
    public void setGUAR_LIST(cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.GUAR_LIST GUAR_LIST) {
        this.GUAR_LIST = GUAR_LIST;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", CUST_ID='" + CUST_ID + '\'' +
                ", CUST_NAME='" + CUST_NAME + '\'' +
                ", CERT_TYPE='" + CERT_TYPE + '\'' +
                ", CERT_NO='" + CERT_NO + '\'' +
                ", GUAR_LIST=" + GUAR_LIST +
                '}';
    }
}
