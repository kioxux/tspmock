package cn.com.yusys.yusp.online.client.esb.circp.fb1146.req;

import java.math.BigDecimal;

/**
 * 请求Service：受托信息审核
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String app_no;//申请流水号
    private String vrify_task;//受托审核状态
    private BigDecimal exec_rate;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getVrify_task() {
        return vrify_task;
    }

    public void setVrify_task(String vrify_task) {
        this.vrify_task = vrify_task;
    }

    public BigDecimal getExec_rate() {
        return exec_rate;
    }

    public void setExec_rate(BigDecimal exec_rate) {
        this.exec_rate = exec_rate;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", app_no='" + app_no + '\'' +
                ", vrify_task='" + vrify_task + '\'' +
                ", exec_rate=" + exec_rate +
                '}';
    }
}
