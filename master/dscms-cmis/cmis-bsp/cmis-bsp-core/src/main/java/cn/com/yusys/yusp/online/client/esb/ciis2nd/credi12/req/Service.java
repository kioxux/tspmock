package cn.com.yusys.yusp.online.client.esb.ciis2nd.credi12.req;

/**
 * 请求Service：ESB通用查询接口（处理码credi12）
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 19:52
 */
public class Service {
    private String prcscd;//	处理码	;	是	;	区分接口的唯一标识，同时也作为路由使用。移动信贷查询接口固定传 credi12
    private String servtp;//	渠道	;	是	;	交易渠道码，ESB总线提供。移动信贷系统YDY
    private String servsq;//	渠道流水	;	是	;	渠道码+日期+11位不重复序列
    private String userid;//	柜员号	;	是	;	各系统发起该交易登陆用户
    private String brchno;//	部门号	;	是	;	【字典】发起交易用户所在部门
    private String creditType;//	报告类型	;	是	;	"区分查询个人报告还是企业报告 0 个人 1 企业"
    private String customName;//	客户名称	;	是	;	客户名称
    private String certificateNum;//	证件号码	;	是	;	证件号码
    private String certificateType;//	证件类型	;	是	;	【字典】证件类型
    private String createUserCode;//	客户经理证件号 	;	是	;	客户经理证件号
    private String createUser;//	客户经理名称	;	是	;	客户经理名称
    private String applySrc;//	申请来源	;	否	;	系统编号
    private String archiveCreateDate;//	授权书签订日期	;	是	;	授权书签订日期 格式：yyyy-MM-dd
    private String auditReason;//	授权书条款	;	是	;	【字典】授权书内容，格式: 001;004
    private String systemCode;//	系统编号	;	是	;	【字典】系统编号
    private String createUserId;//	客户经理工号	;	是	;	客户经理工号
    private String archiveExpireDate;//	授权书到期日期	;	否	;	授权书到期日期预留
    private String creditDocId;//	影像编号	;	是	;	授权影像编号
    private String business;//	产品业务线	;	是	;	【字典】预留
    private String syncFlag;//	是否需要审批	;	是	;	该笔查询的审批策略 0 无需审批 1 需要审批
    private String areaId;//	片区号	;	是	;	片区号
    private String areaName;//	片区名称	;	是	;	片区名称
    private String queryReason;//	查询原因	;	是	;	【字典】查询原因
    private String borrowPersonRelation;//	与主借款人关系	;	是	;	【字典】与主借款人关系
    private String borrowPersonRelationName;//	主借款人名称	;	否	;	主借款人名称
    private String borrowPersonRelationNumber;//	主借款人证件号	;	否	;	主借款人证件号
    private String isAutoCheckPass;//	是否自动审批通过	;	是	;	"是否自动审批通过 true 后台自动审批通过false 需要人工审批"
    private String createUserPhone;//	客户经理手机号	;	是	;	客户经理手机号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getCertificateNum() {
        return certificateNum;
    }

    public void setCertificateNum(String certificateNum) {
        this.certificateNum = certificateNum;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getCreateUserCode() {
        return createUserCode;
    }

    public void setCreateUserCode(String createUserCode) {
        this.createUserCode = createUserCode;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getApplySrc() {
        return applySrc;
    }

    public void setApplySrc(String applySrc) {
        this.applySrc = applySrc;
    }

    public String getArchiveCreateDate() {
        return archiveCreateDate;
    }

    public void setArchiveCreateDate(String archiveCreateDate) {
        this.archiveCreateDate = archiveCreateDate;
    }

    public String getAuditReason() {
        return auditReason;
    }

    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getArchiveExpireDate() {
        return archiveExpireDate;
    }

    public void setArchiveExpireDate(String archiveExpireDate) {
        this.archiveExpireDate = archiveExpireDate;
    }

    public String getCreditDocId() {
        return creditDocId;
    }

    public void setCreditDocId(String creditDocId) {
        this.creditDocId = creditDocId;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getSyncFlag() {
        return syncFlag;
    }

    public void setSyncFlag(String syncFlag) {
        this.syncFlag = syncFlag;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getQueryReason() {
        return queryReason;
    }

    public void setQueryReason(String queryReason) {
        this.queryReason = queryReason;
    }

    public String getBorrowPersonRelation() {
        return borrowPersonRelation;
    }

    public void setBorrowPersonRelation(String borrowPersonRelation) {
        this.borrowPersonRelation = borrowPersonRelation;
    }

    public String getBorrowPersonRelationName() {
        return borrowPersonRelationName;
    }

    public void setBorrowPersonRelationName(String borrowPersonRelationName) {
        this.borrowPersonRelationName = borrowPersonRelationName;
    }

    public String getBorrowPersonRelationNumber() {
        return borrowPersonRelationNumber;
    }

    public void setBorrowPersonRelationNumber(String borrowPersonRelationNumber) {
        this.borrowPersonRelationNumber = borrowPersonRelationNumber;
    }

    public String getIsAutoCheckPass() {
        return isAutoCheckPass;
    }

    public void setIsAutoCheckPass(String isAutoCheckPass) {
        this.isAutoCheckPass = isAutoCheckPass;
    }

    public String getCreateUserPhone() {
        return createUserPhone;
    }

    public void setCreateUserPhone(String createUserPhone) {
        this.createUserPhone = createUserPhone;
    }

    @Override
    public String toString() {
        return "Credi12ReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", creditType='" + creditType + '\'' +
                ", customName='" + customName + '\'' +
                ", certificateNum='" + certificateNum + '\'' +
                ", certificateType='" + certificateType + '\'' +
                ", createUserCode='" + createUserCode + '\'' +
                ", createUser='" + createUser + '\'' +
                ", applySrc='" + applySrc + '\'' +
                ", archiveCreateDate='" + archiveCreateDate + '\'' +
                ", auditReason='" + auditReason + '\'' +
                ", systemCode='" + systemCode + '\'' +
                ", createUserId='" + createUserId + '\'' +
                ", archiveExpireDate='" + archiveExpireDate + '\'' +
                ", creditDocId='" + creditDocId + '\'' +
                ", business='" + business + '\'' +
                ", syncFlag='" + syncFlag + '\'' +
                ", areaId='" + areaId + '\'' +
                ", areaName='" + areaName + '\'' +
                ", queryReason='" + queryReason + '\'' +
                ", borrowPersonRelation='" + borrowPersonRelation + '\'' +
                ", borrowPersonRelationName='" + borrowPersonRelationName + '\'' +
                ", borrowPersonRelationNumber='" + borrowPersonRelationNumber + '\'' +
                ", isAutoCheckPass='" + isAutoCheckPass + '\'' +
                ", createUserPhone='" + createUserPhone + '\'' +
                '}';
    }
}
