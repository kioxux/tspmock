package cn.com.yusys.yusp.web.server.biz.xdht0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0009.req.Xdht0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0009.resp.Xdht0009RespDto;
import cn.com.yusys.yusp.dto.server.xdht0009.req.Xdht0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0009.resp.Xdht0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:银承合同信息列表查询
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDHT0009:银承合同信息列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0009Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0009
     * 交易描述：银承合同信息列表查询
     *
     * @param xdht0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("银承合同信息列表查询")
    @PostMapping("/xdht0009")
    //@Idempotent({"xdcaht0009", "#xdht0009ReqDto.datasq"})
    protected @ResponseBody
    Xdht0009RespDto xdht0009(@Validated @RequestBody Xdht0009ReqDto xdht0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009ReqDto));
        Xdht0009DataReqDto xdht0009DataReqDto = new Xdht0009DataReqDto();// 请求Data： 银承合同信息列表查询
        Xdht0009DataRespDto xdht0009DataRespDto = new Xdht0009DataRespDto();// 响应Data：银承合同信息列表查询
        Xdht0009RespDto xdht0009RespDto = new Xdht0009RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0009.req.Data reqData = null; // 请求Data：银承合同信息列表查询
        cn.com.yusys.yusp.dto.server.biz.xdht0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0009.resp.Data();// 响应Data：银承合同信息列表查询
        try {
            // 从 xdht0009ReqDto获取 reqData
            reqData = xdht0009ReqDto.getData();
            // 将 reqData 拷贝到xdht0009DataReqDto
            BeanUtils.copyProperties(reqData, xdht0009DataReqDto);
            // 调用服务：cmis-biz
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataReqDto));
            ResultDto<Xdht0009DataRespDto> xdht0009DataResultDto = dscmsBizHtClientService.xdht0009(xdht0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0009RespDto.setErorcd(Optional.ofNullable(xdht0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0009RespDto.setErortx(Optional.ofNullable(xdht0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0009DataResultDto.getCode())) {
                xdht0009DataRespDto = xdht0009DataResultDto.getData();
                xdht0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, e.getMessage());
            xdht0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0009RespDto.setDatasq(servsq);

        xdht0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0009.key, DscmsEnum.TRADE_CODE_XDHT0009.value, JSON.toJSONString(xdht0009RespDto));
        return xdht0009RespDto;
    }
}
