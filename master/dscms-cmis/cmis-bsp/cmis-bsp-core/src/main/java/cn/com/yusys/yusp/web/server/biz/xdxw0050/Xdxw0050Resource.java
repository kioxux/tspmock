package cn.com.yusys.yusp.web.server.biz.xdxw0050;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0050.req.Xdxw0050ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0050.resp.Xdxw0050RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0050.req.Xdxw0050DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0050.resp.Xdxw0050DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产负债表信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0050:资产负债表信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0050Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0050Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0050
     * 交易描述：资产负债表信息查询
     *
     * @param xdxw0050ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产负债表信息查询")
    @PostMapping("/xdxw0050")
    //@Idempotent({"xdcaxw0050", "#xdxw0050ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0050RespDto xdxw0050(@Validated @RequestBody Xdxw0050ReqDto xdxw0050ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050ReqDto));
        Xdxw0050DataReqDto xdxw0050DataReqDto = new Xdxw0050DataReqDto();// 请求Data： 资产负债表信息查询
        Xdxw0050DataRespDto xdxw0050DataRespDto = new Xdxw0050DataRespDto();// 响应Data：资产负债表信息查询
        Xdxw0050RespDto xdxw0050RespDto = new Xdxw0050RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0050.req.Data reqData = null; // 请求Data：资产负债表信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0050.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0050.resp.Data();// 响应Data：资产负债表信息查询

        try {
            // 从 xdxw0050ReqDto获取 reqData
            reqData = xdxw0050ReqDto.getData();
            // 将 reqData 拷贝到xdxw0050DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0050DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050DataReqDto));
            ResultDto<Xdxw0050DataRespDto> xdxw0050DataResultDto = dscmsBizXwClientService.xdxw0050(xdxw0050DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0050RespDto.setErorcd(Optional.ofNullable(xdxw0050DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0050RespDto.setErortx(Optional.ofNullable(xdxw0050DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0050DataResultDto.getCode())) {
                xdxw0050DataRespDto = xdxw0050DataResultDto.getData();
                xdxw0050RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0050RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0050DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0050DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, e.getMessage());
            xdxw0050RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0050RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0050RespDto.setDatasq(servsq);

        xdxw0050RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0050.key, DscmsEnum.TRADE_CODE_XDXW0050.value, JSON.toJSONString(xdxw0050RespDto));
        return xdxw0050RespDto;
    }
}
