package cn.com.yusys.yusp.online.client.esb.rircp.fbyd36.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 15:45
 * @since 2021/5/28 15:45
 */
public class Record {
    private String app_type;//申请类型
    private String app_date;//申请时间
    private String appr_pass_time;//审批通过时间
    private String app_no;//申请流水号
    private BigDecimal appr_amt;//审批通过金额
    private String manage_status;//处理状态
    private String Khssqy;//地区（渠道端使用）
    private BigDecimal interest;//利率

    public String getApp_type() {
        return app_type;
    }

    public void setApp_type(String app_type) {
        this.app_type = app_type;
    }

    public String getApp_date() {
        return app_date;
    }

    public void setApp_date(String app_date) {
        this.app_date = app_date;
    }

    public String getAppr_pass_time() {
        return appr_pass_time;
    }

    public void setAppr_pass_time(String appr_pass_time) {
        this.appr_pass_time = appr_pass_time;
    }

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public BigDecimal getAppr_amt() {
        return appr_amt;
    }

    public void setAppr_amt(BigDecimal appr_amt) {
        this.appr_amt = appr_amt;
    }

    public String getManage_status() {
        return manage_status;
    }

    public void setManage_status(String manage_status) {
        this.manage_status = manage_status;
    }

    public String getKhssqy() {
        return Khssqy;
    }

    public void setKhssqy(String khssqy) {
        Khssqy = khssqy;
    }

    public BigDecimal getInterest() {
        return interest;
    }

    public void setInterest(BigDecimal interest) {
        this.interest = interest;
    }

    @Override
    public String toString() {
        return "Record{" +
                "app_type='" + app_type + '\'' +
                ", app_date='" + app_date + '\'' +
                ", appr_pass_time='" + appr_pass_time + '\'' +
                ", app_no='" + app_no + '\'' +
                ", appr_amt=" + appr_amt +
                ", manage_status='" + manage_status + '\'' +
                ", Khssqy='" + Khssqy + '\'' +
                ", interest=" + interest +
                '}';
    }
}
