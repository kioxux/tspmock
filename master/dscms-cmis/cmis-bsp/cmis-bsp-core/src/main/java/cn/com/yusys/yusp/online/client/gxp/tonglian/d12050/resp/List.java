package cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp;
/**
 * 响应Message：账单列表查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
