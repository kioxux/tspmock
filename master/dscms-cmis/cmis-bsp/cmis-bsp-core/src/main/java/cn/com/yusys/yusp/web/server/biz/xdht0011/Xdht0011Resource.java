package cn.com.yusys.yusp.web.server.biz.xdht0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0011.req.Xdht0011ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.Xdht0011RespDto;
import cn.com.yusys.yusp.dto.server.xdht0011.req.Xdht0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0011.resp.*;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0011:查询符合条件的省心快贷合同")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0011Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0011
     * 交易描述：查询符合条件的省心快贷合同
     *
     * @param xdht0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询符合条件的省心快贷合同")
    @PostMapping("/xdht0011")
    //@Idempotent({"xdcaht0011", "#xdht0011ReqDto.datasq"})
    protected @ResponseBody
    Xdht0011RespDto xdht0011(@Validated @RequestBody Xdht0011ReqDto xdht0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011ReqDto));
        Xdht0011DataReqDto xdht0011DataReqDto = new Xdht0011DataReqDto();// 请求Data： 查询符合条件的省心快贷合同
        Xdht0011DataRespDto xdht0011DataRespDto = new Xdht0011DataRespDto();// 响应Data：查询符合条件的省心快贷合同
        Xdht0011RespDto xdht0011RespDto = new Xdht0011RespDto();
        // 此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdht0011.req.Data reqData = null; // 请求Data：查询符合条件的省心快贷合同
        cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.Data();// 响应Data：查询符合条件的省心快贷合同
        //  此处包导入待确定 结束
        try {
            // 从 xdht0011ReqDto获取 reqData
            reqData = xdht0011ReqDto.getData();
            // 将 reqData 拷贝到xdht0011DataReqDto
            BeanUtils.copyProperties(reqData, xdht0011DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataReqDto));
            ResultDto<Xdht0011DataRespDto> xdht0011DataResultDto = dscmsBizHtClientService.xdht0011(xdht0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0011RespDto.setErorcd(Optional.ofNullable(xdht0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0011RespDto.setErortx(Optional.ofNullable(xdht0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0011DataResultDto.getCode())) {
                xdht0011DataRespDto = xdht0011DataResultDto.getData();
                xdht0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0011DataRespDto, respData);
                if (CollectionUtils.nonEmpty(xdht0011DataRespDto.getLoanContList())) {
                    List<cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.LoanContList> targetList1 = new ArrayList<>();
                    List<LoanContList> loanContList = xdht0011DataRespDto.getLoanContList();
                    for (LoanContList l : loanContList) {
                        cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.LoanContList targetLoanContList = new cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.LoanContList();
                        BeanUtils.copyProperties(l, targetLoanContList);
                        targetList1.add(targetLoanContList);
                    }
                    respData.setLoanContList(targetList1);
                }

                if (CollectionUtils.nonEmpty(xdht0011DataRespDto.getAccountList())) {
                    List<cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.AccountList> targetList2 = new ArrayList<>();
                    List<AccountList> accountList = xdht0011DataRespDto.getAccountList();
                    for (AccountList a : accountList) {
                        cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.AccountList targetAccountList = new cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.AccountList();
                        BeanUtils.copyProperties(a, targetAccountList);
                        targetList2.add(targetAccountList);
                    }
                    respData.setAccountList(targetList2);
                }

                if (CollectionUtils.nonEmpty(xdht0011DataRespDto.getGuarContList())) {
                    List<cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.GuarContList> targetList3 = new ArrayList<>();
                    List<GuarContList> guarContList = xdht0011DataRespDto.getGuarContList();
                    for (GuarContList g : guarContList) {
                        cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.GuarContList targetGuarContList = new cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.GuarContList();
                        BeanUtils.copyProperties(g, targetGuarContList);
                        targetList3.add(targetGuarContList);
                    }
                    respData.setGuarContList(targetList3);
                }

                if (CollectionUtils.nonEmpty(xdht0011DataRespDto.getHxdLoanContList())) {
                    List<cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.HxdLoanContList> targetList4 = new ArrayList<>();
                    List<HxdLoanContList> hxdLoanContList = xdht0011DataRespDto.getHxdLoanContList();
                    for (HxdLoanContList h : hxdLoanContList) {
                        cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.HxdLoanContList targetHxdLoanContList = new cn.com.yusys.yusp.dto.server.biz.xdht0011.resp.HxdLoanContList();
                        BeanUtils.copyProperties(h, targetHxdLoanContList);
                        targetList4.add(targetHxdLoanContList);
                    }
                    respData.setHxdLoanContList(targetList4);
                }


            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, e.getMessage());
            xdht0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0011RespDto.setDatasq(servsq);

        xdht0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0011.key, DscmsEnum.TRADE_CODE_XDHT0011.value, JSON.toJSONString(xdht0011RespDto));
        return xdht0011RespDto;
    }
}
