package cn.com.yusys.yusp.online.client.http.outerdata.slno01.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：双录流水号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Slno01ReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//    交易码
    @JsonProperty(value = "servtp")
    private String servtp;//    渠道码
    @JsonProperty(value = "servsq")
    private String servsq;//    渠道流水号
    @JsonProperty(value = "userid")
    private String userid;//    柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//    部门号
    @JsonProperty(value = "guarid")
    private String guarid;//抵押物编号
    @JsonProperty(value = "yxlsno")
    private String yxlsno;//影像主键流水号
    @JsonProperty(value = "jytype")
    private String jytype;//交易类型


    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public String getYxlsno() {
        return yxlsno;
    }

    public void setYxlsno(String yxlsno) {
        this.yxlsno = yxlsno;
    }

    public String getJytype() {
        return jytype;
    }

    public void setJytype(String jytype) {
        this.jytype = jytype;
    }

    @Override
    public String toString() {
        return "Slno01ReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", guarid='" + guarid + '\'' +
                ", yxlsno='" + yxlsno + '\'' +
                ", jytype='" + jytype + '\'' +
                '}';
    }
}
