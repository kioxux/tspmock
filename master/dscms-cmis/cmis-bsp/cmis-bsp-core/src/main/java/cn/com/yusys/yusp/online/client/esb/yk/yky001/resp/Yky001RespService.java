package cn.com.yusys.yusp.online.client.esb.yk.yky001.resp;

/**
 * 响应Service：对公客户综合信息维护
 *
 * @author xuwh
 * @version 1.0
 * @since 2021年8月18日 下午21:03:06
 */
public class Yky001RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
