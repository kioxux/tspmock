package cn.com.yusys.yusp.online.client.esb.ecif.g11003.req;

/**
 * 请求Service：客户集团信息维护 (new)
 *
 * @author jijian
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
@Deprecated
public class CircleArrayMem {

    private String custno;//	varchar2  	30	集团成员客户号
    private String cortype;//	varchar2  	5	集团关系类型
    private String grpdesc;//	varchar2  	250	集团关系描述
    private String iscust;//	varchar2  	5	是否集团主客户	"0：否 1：是"
    private String mainno;//	varchar2  	30	主管人
    private String maiorg;//	varchar2  	30	主管机构
    private String socino;//	varchar2  	18	统一社会信用代码
    private String custst;//	varchar2  	5	成员状态	"0：有效  1：无效"

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCortype() {
        return cortype;
    }

    public void setCortype(String cortype) {
        this.cortype = cortype;
    }

    public String getGrpdesc() {
        return grpdesc;
    }

    public void setGrpdesc(String grpdesc) {
        this.grpdesc = grpdesc;
    }

    public String getIscust() {
        return iscust;
    }

    public void setIscust(String iscust) {
        this.iscust = iscust;
    }

    public String getMainno() {
        return mainno;
    }

    public void setMainno(String mainno) {
        this.mainno = mainno;
    }

    public String getMaiorg() {
        return maiorg;
    }

    public void setMaiorg(String maiorg) {
        this.maiorg = maiorg;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }


    @Override
    public String toString() {
        return "RelArrayInfo{" +
                "custno='" + custno + '\'' +
                ", cortype='" + cortype + '\'' +
                ", grpdesc='" + grpdesc + '\'' +
                ", iscust='" + iscust + '\'' +
                ", mainno='" + mainno + '\'' +
                ", maiorg='" + maiorg + '\'' +
                ", socino='" + socino + '\'' +
                ", custst='" + custst + '\'' +
                '}';
    }
}
