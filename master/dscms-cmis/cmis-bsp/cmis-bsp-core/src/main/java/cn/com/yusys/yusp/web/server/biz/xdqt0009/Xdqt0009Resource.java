package cn.com.yusys.yusp.web.server.biz.xdqt0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdqt0009.req.Xdqt0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdqt0009.resp.Xdqt0009RespDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.req.Xdqt0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdqt0009.resp.Xdqt0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizQtClientService;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@Api(tags = "XDQT0009:微业贷信贷文件处理通知")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0009Resource.class);

    @Autowired
    private DscmsBizQtClientService dscmsBizQtClientService;

    /**
     * 交易码：xdqt0009
     * 交易描述：微业贷信贷文件处理通知
     *
     * @param xdqt0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("微业贷信贷文件处理通知")
    @PostMapping("/xdqt0009")
    @Idempotent({"xdcaqt0009", "#xdqt0009ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0009RespDto xdqt0009(@Validated @RequestBody Xdqt0009ReqDto xdqt0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, JSON.toJSONString(xdqt0009ReqDto));
        Xdqt0009DataReqDto xdqt0009DataReqDto = new Xdqt0009DataReqDto();// 请求Data： 贷款申请预约（企业客户）
        Xdqt0009DataRespDto xdqt0009DataRespDto = new Xdqt0009DataRespDto();// 响应Data：贷款申请预约（企业客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0009.req.Data reqData = null; // 请求Data：贷款申请预约（企业客户）
        cn.com.yusys.yusp.dto.server.biz.xdqt0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdqt0009.resp.Data();// 响应Data：贷款申请预约（企业客户）
		Xdqt0009RespDto xdqt0009RespDto = new Xdqt0009RespDto();
		try {
            // 从 xdqt0009ReqDto获取 reqData
            reqData = xdqt0009ReqDto.getData();
            // 将 reqData 拷贝到xdqt0009DataReqDto
            BeanUtils.copyProperties(reqData, xdqt0009DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, JSON.toJSONString(xdqt0009DataReqDto));
            ResultDto<Xdqt0009DataRespDto> xdqt0009DataResultDto = dscmsBizQtClientService.xdqt0009(xdqt0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, JSON.toJSONString(xdqt0009DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0009RespDto.setErorcd(Optional.ofNullable(xdqt0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0009RespDto.setErortx(Optional.ofNullable(xdqt0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0009DataResultDto.getCode())) {
                xdqt0009DataRespDto = xdqt0009DataResultDto.getData();
                xdqt0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, e.getMessage());
            xdqt0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0009RespDto.setDatasq(servsq);

        xdqt0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0009.key, DscmsEnum.TRADE_CODE_XDQT0009.value, JSON.toJSONString(xdqt0009RespDto));
        return xdqt0009RespDto;
    }
}
