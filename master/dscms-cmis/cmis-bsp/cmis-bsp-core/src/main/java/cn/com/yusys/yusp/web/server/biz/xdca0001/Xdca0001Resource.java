package cn.com.yusys.yusp.web.server.biz.xdca0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdca0001.req.Xdca0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdca0001.resp.Xdca0001RespDto;
import cn.com.yusys.yusp.dto.server.xdca0001.req.Xdca0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0001.resp.Xdca0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCaClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信用卡调额申请
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDCA0001:信用卡调额申请")
@RestController
@RequestMapping("/api/dscms")
public class Xdca0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0001Resource.class);

    @Autowired
	private DscmsBizCaClientService dscmsBizCaClientService;

    /**
     * 交易码：xdca0001
     * 交易描述：信用卡调额申请
     *
     * @param xdca0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信用卡调额申请")
    @PostMapping("/xdca0001")
   //@Idempotent({"xdcaca0001", "#xdca0001ReqDto.datasq"})
    protected @ResponseBody
    Xdca0001RespDto xdca0001(@Validated @RequestBody Xdca0001ReqDto xdca0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001ReqDto));
        Xdca0001DataReqDto xdca0001DataReqDto = new Xdca0001DataReqDto();// 请求Data： 信用卡调额申请
        Xdca0001DataRespDto xdca0001DataRespDto = new Xdca0001DataRespDto();// 响应Data：信用卡调额申请
        cn.com.yusys.yusp.dto.server.biz.xdca0001.req.Data reqData = null; // 请求Data：信用卡调额申请
        cn.com.yusys.yusp.dto.server.biz.xdca0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdca0001.resp.Data();// 响应Data：信用卡调额申请
		Xdca0001RespDto xdca0001RespDto = new Xdca0001RespDto();
		try {
            // 从 xdca0001ReqDto获取 reqData
            reqData = xdca0001ReqDto.getData();
            // 将 reqData 拷贝到xdca0001DataReqDto
            BeanUtils.copyProperties(reqData, xdca0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001DataReqDto));
            ResultDto<Xdca0001DataRespDto> xdca0001DataResultDto = dscmsBizCaClientService.xdca0001(xdca0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdca0001RespDto.setErorcd(Optional.ofNullable(xdca0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdca0001RespDto.setErortx(Optional.ofNullable(xdca0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdca0001DataResultDto.getCode())) {
                xdca0001DataRespDto = xdca0001DataResultDto.getData();
                xdca0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdca0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdca0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdca0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, e.getMessage());
            xdca0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdca0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdca0001RespDto.setDatasq(servsq);

        xdca0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0001.key, DscmsEnum.TRADE_CODE_XDCA0001.value, JSON.toJSONString(xdca0001RespDto));
        return xdca0001RespDto;
    }
}
