package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.req;

/**
 * @类名 Service
 * @描述 请求Service：新微贷贷款申请
 * @作者 黄勃
 * @时间 2021/10/26 19:33
 **/
public class Service {
    private String prcscd;//处理码

    private String servtp;//渠道

    private String servsq;//渠道流水

    private String userid;//柜员号

    private String brchno;//部门号

    private String businessId;//行内申请流水号

    private String applName;//申请人姓名

    private String applCertificateType;//申请人证件类型

    private String applCertificateNumber;//申请人证件号码

    private String appluserType;//申请人类型

    private String companyName;//企业名称

    private String uscc;//统一社会信用代码

    private String taxCode;//纳税人识别号

    private String tel;//联系方式

    private String applAmount;//贷款申请金额

    private String applTimeLimit;//贷款期限

    private String loanPurposeCode;//资金用途

    private String productCode;//产品代码

    private String marketingNumber;//营销人员编号

    private String marketingName;//营销人员姓名

    private String applArea;//客户区域

    private String detailAddress;//详细地址

    private String creditAuthCodeList;//贷款授权或声明信息

    private String fraudGID;//百融全局设备标识

    private String channelCode;//渠道标记

    private String existsHouse;//有无房产

    private String houseValue;//房屋估值

    private String taxAuthorizationCode;//税务授权码

    private String marketPathCode;//营销路径

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getApplName() {
        return applName;
    }

    public void setApplName(String applName) {
        this.applName = applName;
    }

    public String getApplCertificateType() {
        return applCertificateType;
    }

    public void setApplCertificateType(String applCertificateType) {
        this.applCertificateType = applCertificateType;
    }

    public String getApplCertificateNumber() {
        return applCertificateNumber;
    }

    public void setApplCertificateNumber(String applCertificateNumber) {
        this.applCertificateNumber = applCertificateNumber;
    }

    public String getAppluserType() {
        return appluserType;
    }

    public void setAppluserType(String appluserType) {
        this.appluserType = appluserType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getUscc() {
        return uscc;
    }

    public void setUscc(String uscc) {
        this.uscc = uscc;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public void setTaxCode(String taxCode) {
        this.taxCode = taxCode;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getApplAmount() {
        return applAmount;
    }

    public void setApplAmount(String applAmount) {
        this.applAmount = applAmount;
    }

    public String getApplTimeLimit() {
        return applTimeLimit;
    }

    public void setApplTimeLimit(String applTimeLimit) {
        this.applTimeLimit = applTimeLimit;
    }

    public String getLoanPurposeCode() {
        return loanPurposeCode;
    }

    public void setLoanPurposeCode(String loanPurposeCode) {
        this.loanPurposeCode = loanPurposeCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getMarketingNumber() {
        return marketingNumber;
    }

    public void setMarketingNumber(String marketingNumber) {
        this.marketingNumber = marketingNumber;
    }

    public String getMarketingName() {
        return marketingName;
    }

    public void setMarketingName(String marketingName) {
        this.marketingName = marketingName;
    }

    public String getApplArea() {
        return applArea;
    }

    public void setApplArea(String applArea) {
        this.applArea = applArea;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getCreditAuthCodeList() {
        return creditAuthCodeList;
    }

    public void setCreditAuthCodeList(String creditAuthCodeList) {
        this.creditAuthCodeList = creditAuthCodeList;
    }

    public String getFraudGID() {
        return fraudGID;
    }

    public void setFraudGID(String fraudGID) {
        this.fraudGID = fraudGID;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getExistsHouse() {
        return existsHouse;
    }

    public void setExistsHouse(String existsHouse) {
        this.existsHouse = existsHouse;
    }

    public String getHouseValue() {
        return houseValue;
    }

    public void setHouseValue(String houseValue) {
        this.houseValue = houseValue;
    }

    public String getTaxAuthorizationCode() {
        return taxAuthorizationCode;
    }

    public void setTaxAuthorizationCode(String taxAuthorizationCode) {
        this.taxAuthorizationCode = taxAuthorizationCode;
    }

    public String getMarketPathCode() {
        return marketPathCode;
    }

    public void setMarketPathCode(String marketPathCode) {
        this.marketPathCode = marketPathCode;
    }

    @Override
    public String toString() {
        return "Xwd001ReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", businessId='" + businessId + '\'' +
                ", applName='" + applName + '\'' +
                ", applCertificateType='" + applCertificateType + '\'' +
                ", applCertificateNumber='" + applCertificateNumber + '\'' +
                ", appluserType='" + appluserType + '\'' +
                ", companyName='" + companyName + '\'' +
                ", uscc='" + uscc + '\'' +
                ", taxCode='" + taxCode + '\'' +
                ", tel='" + tel + '\'' +
                ", applAmount='" + applAmount + '\'' +
                ", applTimeLimit='" + applTimeLimit + '\'' +
                ", loanPurposeCode='" + loanPurposeCode + '\'' +
                ", productCode='" + productCode + '\'' +
                ", marketingNumber='" + marketingNumber + '\'' +
                ", marketingName='" + marketingName + '\'' +
                ", applArea='" + applArea + '\'' +
                ", detailAddress='" + detailAddress + '\'' +
                ", creditAuthCodeList='" + creditAuthCodeList + '\'' +
                ", fraudGID='" + fraudGID + '\'' +
                ", channelCode='" + channelCode + '\'' +
                ", existsHouse='" + existsHouse + '\'' +
                ", houseValue='" + houseValue + '\'' +
                ", taxAuthorizationCode='" + taxAuthorizationCode + '\'' +
                ", marketPathCode='" + marketPathCode + '\'' +
                '}';
    }
}
