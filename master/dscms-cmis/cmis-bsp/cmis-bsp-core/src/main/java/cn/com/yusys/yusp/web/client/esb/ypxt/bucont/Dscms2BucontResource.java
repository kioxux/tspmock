package cn.com.yusys.yusp.web.client.esb.ypxt.bucont;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bucont.BucontListInfo;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bucont.BucontReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bucont.BucontRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.BucontReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.Record;
import cn.com.yusys.yusp.online.client.esb.ypxt.bucont.resp.BucontRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(bucont)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2BucontResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2BucontResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 业务与担保合同关系接口（处理码bucont）
     *
     * @param bucontReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("bucont:业务与担保合同关系接口")
    @PostMapping("/bucont")
    protected @ResponseBody
    ResultDto<BucontRespDto> bucont(@Validated @RequestBody BucontReqDto bucontReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUCONT.key, EsbEnum.TRADE_CODE_BUCONT.value, JSON.toJSONString(bucontReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.bucont.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.bucont.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.List list = new cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.List();
        BucontReqService bucontReqService = new BucontReqService();
        BucontRespService bucontRespService = new BucontRespService();
        BucontRespDto bucontRespDto = new BucontRespDto();
        ResultDto<BucontRespDto> bucontResultDto = new ResultDto<BucontRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            BeanUtils.copyProperties(bucontReqDto, reqService);
            //  将BucontReqDto转换成reqService
            if (CollectionUtils.nonEmpty(bucontReqDto.getBucontListInfo())) {
                List<BucontListInfo> bucontListInfos = Optional.ofNullable(bucontReqDto.getBucontListInfo()).orElseGet(() -> new ArrayList<>());
                List<Record> recordList = new ArrayList<Record>();
                for (BucontListInfo bucontListInfo : bucontListInfos) {
                    cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.bucont.req.Record();
                    BeanUtils.copyProperties(bucontListInfo, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_BUCONT.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(EsbEnum.BRCHNO_YPXT.key);//    全局流水
            bucontReqService.setService(reqService);
            // 将bucontReqService转换成bucontReqServiceMap
            Map bucontReqServiceMap = beanMapUtil.beanToMap(bucontReqService);
            context.put("tradeDataMap", bucontReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUCONT.key, EsbEnum.TRADE_CODE_BUCONT.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_BUCONT.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUCONT.key, EsbEnum.TRADE_CODE_BUCONT.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            bucontRespService = beanMapUtil.mapToBean(tradeDataMap, BucontRespService.class, BucontRespService.class);
            respService = bucontRespService.getService();
            //  将BucontRespDto封装到ResultDto<BucontRespDto>
            bucontResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            bucontResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                BeanUtils.copyProperties(respService, bucontRespDto);
                bucontResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                bucontResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                bucontResultDto.setCode(EpbEnum.EPB099999.key);
                bucontResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUCONT.key, EsbEnum.TRADE_CODE_BUCONT.value, e.getMessage());
            bucontResultDto.setCode(EpbEnum.EPB099999.key);//9999
            bucontResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        bucontResultDto.setData(bucontRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BUCONT.key, EsbEnum.TRADE_CODE_BUCONT.value, JSON.toJSONString(bucontResultDto));
        return bucontResultDto;
    }
}
