package cn.com.yusys.yusp.web.server.biz.xdxw0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0003.req.Xdxw0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0003.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0003.resp.Xdxw0003RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0003.req.Xdxw0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0003.resp.Xdxw0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小微贷前调查信息维护
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0003:小微贷前调查信息维护")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0003Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0003
     * 交易描述：小微贷前调查信息维护
     *
     * @param xdxw0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微贷前调查信息维护")
    @PostMapping("/xdxw0003")
    //@Idempotent({"xdcaxw0003", "#xdxw0003ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0003RespDto xdxw0003(@Validated @RequestBody Xdxw0003ReqDto xdxw0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, JSON.toJSONString(xdxw0003ReqDto));
        Xdxw0003DataReqDto xdxw0003DataReqDto = new Xdxw0003DataReqDto();// 请求Data： 小微贷前调查信息维护
        Xdxw0003DataRespDto xdxw0003DataRespDto = new Xdxw0003DataRespDto();// 响应Data：小微贷前调查信息维护
        cn.com.yusys.yusp.dto.server.biz.xdxw0003.req.Data reqData = null; // 请求Data：小微贷前调查信息维护
        cn.com.yusys.yusp.dto.server.biz.xdxw0003.resp.Data respData = new Data();// 响应Data：小微贷前调查信息维护
		Xdxw0003RespDto xdxw0003RespDto = new Xdxw0003RespDto();
		try {
            // 从 xdxw0003ReqDto获取 reqData
            reqData = xdxw0003ReqDto.getData();
            // 将 reqData 拷贝到xdxw0003DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0003DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, JSON.toJSONString(xdxw0003DataReqDto));
            ResultDto<Xdxw0003DataRespDto> xdxw0003DataResultDto = dscmsBizXwClientService.xdxw0003(xdxw0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, JSON.toJSONString(xdxw0003DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0003RespDto.setErorcd(Optional.ofNullable(xdxw0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0003RespDto.setErortx(Optional.ofNullable(xdxw0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0003DataResultDto.getCode())) {
                xdxw0003DataRespDto = xdxw0003DataResultDto.getData();
                xdxw0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, e.getMessage());
            xdxw0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0003RespDto.setDatasq(servsq);

        xdxw0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0003.key, DscmsEnum.TRADE_CODE_XDXW0003.value, JSON.toJSONString(xdxw0003RespDto));
        return xdxw0003RespDto;
    }
}
