package cn.com.yusys.yusp.web.server.biz.xdzc0004;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0004.req.Xdzc0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0004.resp.Xdzc0004RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0004.req.Xdzc0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0004.resp.Xdzc0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户资产清单查询接口
 *
 * @author xs
 * @version 1.0
 */
@Api(tags = "XDZC0004:客户资产清单查询接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0004Resource.class);

	@Autowired
	private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0004
     * 交易描述：客户资产清单查询接口
     *
     * @param xdzc0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户资产清单查询接口")
    @PostMapping("/xdzc0004")
    //@Idempotent({"xdzc004", "#xdzc0004ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0004RespDto xdzc0004(@Validated @RequestBody Xdzc0004ReqDto xdzc0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, JSON.toJSONString(xdzc0004ReqDto));
        Xdzc0004DataReqDto xdzc0004DataReqDto = new Xdzc0004DataReqDto();// 请求Data： 客户资产池下资产清单列表查询（出池时查询）
        Xdzc0004DataRespDto xdzc0004DataRespDto = new Xdzc0004DataRespDto();// 响应Data：客户资产池下资产清单列表查询（出池时查询）
        cn.com.yusys.yusp.dto.server.biz.xdzc0004.req.Data reqData = null; // 请求Data：客户资产池下资产清单列表查询（出池时查询）
        cn.com.yusys.yusp.dto.server.biz.xdzc0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0004.resp.Data();// 响应Data：客户资产池下资产清单列表查询（出池时查询）
		Xdzc0004RespDto xdzc0004RespDto = new Xdzc0004RespDto();
		try {
            // 从 xdzc0004ReqDto获取 reqData
            reqData = xdzc0004ReqDto.getData();
            // 将 reqData 拷贝到xdzc0004DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0004DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, JSON.toJSONString(xdzc0004DataReqDto));
            ResultDto<Xdzc0004DataRespDto> xdzc0004DataResultDto = dscmsBizZcClientService.xdzc0004(xdzc0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, JSON.toJSONString(xdzc0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdzc0004RespDto.setErorcd(Optional.ofNullable(xdzc0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0004RespDto.setErortx(Optional.ofNullable(xdzc0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0004DataResultDto.getCode())) {
                xdzc0004DataRespDto = xdzc0004DataResultDto.getData();
                xdzc0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, e.getMessage());
            xdzc0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0004RespDto.setDatasq(servsq);

        xdzc0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0004.key, DscmsEnum.TRADE_CODE_XDZC0004.value, JSON.toJSONString(xdzc0004RespDto));
        return xdzc0004RespDto;
    }
}
