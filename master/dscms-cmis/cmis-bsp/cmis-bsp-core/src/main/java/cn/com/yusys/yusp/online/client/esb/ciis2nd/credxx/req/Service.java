package cn.com.yusys.yusp.online.client.esb.ciis2nd.credxx.req;

/**
 * 请求Service：线下查询接口
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String brchno;//部门号
    private String creditType;//报告类型
    private String customName;//客户名称
    private String certificateType;//证件类型
    private String certificateNum;//证件号
    private String queryReason;//查询原因
    private String reportType;//信用报告返回格式
    private String queryType;//信用报告复用策略
    private String businessLine;//产品业务线
    private String sysCode;//系统标识
    private String creditDocId;//影像编号
    private String archiveCreateDate;//授权书签订日期 时间格式：2016-01-04
    private String archiveExpireDate;//授权书到期日期
    private String auditReason;//授权条款
    private String borrowPersonRelation;//与主借款人关系
    private String borrowPersonRelationName;//主借款人名称
    private String borrowPersonRelationNumber;//主借款人证件号
    private String createUserId;//客户经理工号
    private String createUserName;//客户经理名称
    private String createUserIdCard;//客户经理身份证号
    private String approvalName;//审批人姓名
    private String approvalIdCard;//审批人身份证号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getCertificateType() {
        return certificateType;
    }

    public void setCertificateType(String certificateType) {
        this.certificateType = certificateType;
    }

    public String getCertificateNum() {
        return certificateNum;
    }

    public void setCertificateNum(String certificateNum) {
        this.certificateNum = certificateNum;
    }

    public String getQueryReason() {
        return queryReason;
    }

    public void setQueryReason(String queryReason) {
        this.queryReason = queryReason;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getBusinessLine() {
        return businessLine;
    }

    public void setBusinessLine(String businessLine) {
        this.businessLine = businessLine;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getCreditDocId() {
        return creditDocId;
    }

    public void setCreditDocId(String creditDocId) {
        this.creditDocId = creditDocId;
    }

    public String getArchiveCreateDate() {
        return archiveCreateDate;
    }

    public void setArchiveCreateDate(String archiveCreateDate) {
        this.archiveCreateDate = archiveCreateDate;
    }

    public String getArchiveExpireDate() {
        return archiveExpireDate;
    }

    public void setArchiveExpireDate(String archiveExpireDate) {
        this.archiveExpireDate = archiveExpireDate;
    }

    public String getAuditReason() {
        return auditReason;
    }

    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason;
    }

    public String getBorrowPersonRelation() {
        return borrowPersonRelation;
    }

    public void setBorrowPersonRelation(String borrowPersonRelation) {
        this.borrowPersonRelation = borrowPersonRelation;
    }

    public String getBorrowPersonRelationName() {
        return borrowPersonRelationName;
    }

    public void setBorrowPersonRelationName(String borrowPersonRelationName) {
        this.borrowPersonRelationName = borrowPersonRelationName;
    }

    public String getBorrowPersonRelationNumber() {
        return borrowPersonRelationNumber;
    }

    public void setBorrowPersonRelationNumber(String borrowPersonRelationNumber) {
        this.borrowPersonRelationNumber = borrowPersonRelationNumber;
    }

    public String getCreateUserId() {
        return createUserId;
    }

    public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateUserIdCard() {
        return createUserIdCard;
    }

    public void setCreateUserIdCard(String createUserIdCard) {
        this.createUserIdCard = createUserIdCard;
    }

    public String getApprovalName() {
        return approvalName;
    }

    public void setApprovalName(String approvalName) {
        this.approvalName = approvalName;
    }

    public String getApprovalIdCard() {
        return approvalIdCard;
    }

    public void setApprovalIdCard(String approvalIdCard) {
        this.approvalIdCard = approvalIdCard;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "brchno='" + brchno + '\'' +
                "creditType='" + creditType + '\'' +
                "customName='" + customName + '\'' +
                "certificateType='" + certificateType + '\'' +
                "certificateNum='" + certificateNum + '\'' +
                "queryReason='" + queryReason + '\'' +
                "reportType='" + reportType + '\'' +
                "queryType='" + queryType + '\'' +
                "businessLine='" + businessLine + '\'' +
                "sysCode='" + sysCode + '\'' +
                "creditDocId='" + creditDocId + '\'' +
                "archiveCreateDate='" + archiveCreateDate + '\'' +
                "archiveExpireDate='" + archiveExpireDate + '\'' +
                "auditReason='" + auditReason + '\'' +
                "borrowPersonRelation='" + borrowPersonRelation + '\'' +
                "borrowPersonRelationName='" + borrowPersonRelationName + '\'' +
                "borrowPersonRelationNumber='" + borrowPersonRelationNumber + '\'' +
                "createUserId='" + createUserId + '\'' +
                "createUserName='" + createUserName + '\'' +
                "createUserIdCard='" + createUserIdCard + '\'' +
                "approvalName='" + approvalName + '\'' +
                "approvalIdCard='" + approvalIdCard + '\'' +
                '}';
    }
}
