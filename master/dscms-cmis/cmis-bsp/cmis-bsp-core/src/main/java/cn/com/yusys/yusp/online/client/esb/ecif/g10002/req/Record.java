package cn.com.yusys.yusp.online.client.esb.ecif.g10002.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/6 14:01
 * @since 2021/7/6 14:01
 */
public class Record {
    private String gpmbna;//群组成员客户号
    private String membtp;//成员类型
    private String membst;//成员状态

    public String getGpmbna() {
        return gpmbna;
    }

    public void setGpmbna(String gpmbna) {
        this.gpmbna = gpmbna;
    }

    public String getMembtp() {
        return membtp;
    }

    public void setMembtp(String membtp) {
        this.membtp = membtp;
    }

    public String getMembst() {
        return membst;
    }

    public void setMembst(String membst) {
        this.membst = membst;
    }

    @Override
    public String toString() {
        return "Record{" +
                "gpmbna='" + gpmbna + '\'' +
                ", membtp='" + membtp + '\'' +
                ", membst='" + membst + '\'' +
                '}';
    }
}
