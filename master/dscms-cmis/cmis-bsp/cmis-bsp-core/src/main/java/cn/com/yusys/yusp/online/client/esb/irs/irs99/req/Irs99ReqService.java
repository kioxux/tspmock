package cn.com.yusys.yusp.online.client.esb.irs.irs99.req;

/**
 * 请求Service：债项评级
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日 下午1:22:06
 */
public class Irs99ReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
