package cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req;

/**
 * 请求Message：大额现金分期申请
 *
 * @author chenyong
 * @version 1.0
 */
public class D13160ReqMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D13160ReqService{" +
                "message=" + message +
                '}';
    }
}

