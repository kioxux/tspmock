package cn.com.yusys.yusp.online.client.esb.circp.fb1214.req;

import cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.houselist.Record;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 20:16
 * @since 2021/8/10 20:16
 */
public class HOUSE_LIST {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.houselist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "HOUSE_LIST{" +
                "record=" + record +
                '}';
    }
}
