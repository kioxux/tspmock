package cn.com.yusys.yusp.web.client.gxp.tonglian.d13160;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.req.D13160ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.resp.D13160RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp.D13160RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d13160)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D13160Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D13160Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 大额现金分期申请（处理码d13160）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d13160:大额现金分期申请")
    @PostMapping("/d13160")
    protected @ResponseBody
    ResultDto<D13160RespDto> d13160(@Validated @RequestBody D13160ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13160.key, GxpEnum.TRADE_CODE_D13160.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.D13160ReqMessage d13160ReqMessage = null;//请求Message：大额现金分期申请
        D13160RespMessage d13160RespMessage = new D13160RespMessage();//响应Message：大额现金分期申请
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Message reqMessage = null;//请求Message：大额现金分期申请
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp.Message();//响应Message：大额现金分期申请
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：大额现金分期申请
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：大额现金分期申请
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Body();//请求Body：大额现金分期申请
        cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.resp.Body();//响应Body：大额现金分期申请

        D13160RespDto d13160RespDto = new D13160RespDto();//响应Dto：大额现金分期申请
        ResultDto<D13160RespDto> d13160ResultDto = new ResultDto<>();//响应ResultDto：大额现金分期申请

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D13160.key, GxpEnum.TRADE_CODE_D13160.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D13160ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.Message::setBody, reqBody).build();
            // 给d13160ReqMessage赋值
            d13160ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.D13160ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d13160.req.D13160ReqMessage::setMessage, reqMessage).build();

            // 将d13160ReqService转换成d13160ReqServiceMap
            Map d13160ReqMessageMap = beanMapUtil.beanToMap(d13160ReqMessage);
            context.put("tradeDataMap", d13160ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13160.key, GxpEnum.TRADE_CODE_D13160.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D13160.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13160.key, GxpEnum.TRADE_CODE_D13160.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d13160RespMessage = beanMapUtil.mapToBean(tradeDataMap, D13160RespMessage.class, D13160RespMessage.class);//响应Message：大额现金分期申请
            respMessage = d13160RespMessage.getMessage();//响应Message：大额现金分期申请
            gxpRespHead = respMessage.getHead();//响应Head：大额现金分期申请
            //  将D13160RespDto封装到ResultDto<D13160RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                respBody = respMessage.getBody();
                //  将respBody转换成D13160RespDto
                BeanUtils.copyProperties(respBody, d13160RespDto);
                d13160ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d13160ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d13160ResultDto.setCode(EpbEnum.EPB099999.key);
                d13160ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d13160ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13160.key, GxpEnum.TRADE_CODE_D13160.value, e.getMessage());
            d13160ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d13160ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d13160ResultDto.setData(d13160RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D13160.key, GxpEnum.TRADE_CODE_D13160.value, JSON.toJSONString(d13160ResultDto));
        return d13160ResultDto;
    }
}
