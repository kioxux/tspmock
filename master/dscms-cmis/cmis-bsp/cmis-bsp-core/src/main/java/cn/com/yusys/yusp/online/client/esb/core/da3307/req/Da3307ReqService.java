package cn.com.yusys.yusp.online.client.esb.core.da3307.req;

/**
 * 请求Service：抵债资产出租处理
 * @author lihh
 * @version 1.0             
 */      
public class Da3307ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
