package cn.com.yusys.yusp.online.client.esb.core.ln3177.req;

/**
 * 请求Service：资产转让付款状态维护
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3177ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3177ReqService{" +
                "service=" + service +
                '}';
    }
}
