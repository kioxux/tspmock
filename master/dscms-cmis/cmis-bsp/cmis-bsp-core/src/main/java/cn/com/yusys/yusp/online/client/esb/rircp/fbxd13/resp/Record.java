package cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.resp;

import java.math.BigDecimal;

/**
 * 响应Service：查询客户核心编号，客户名称对应的放款和还款信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String erorcd; // 响应码
    private String erortx; // 响应信息
    private String datasq; //全局流水号
    private String cust_id_core;//核心客户编号
    private String cust_name;//客户名称
    private String contract_no;//融资平台贷款合同号，等于借据号，唯一标识一笔借据
    private BigDecimal encash_amt;//放款金额单位（元）
    private BigDecimal day_rate;//贷款日利率，保留6位小数
    private String start_date;//货款起息日
    private String end_date;//货款到期日
    private String apply_date;//申请支用时间，格式：yyyy-MM-dd HH:mm:ss
    private String encash_date;//放款日期，格式：yyyy-MM-dd HH:mm:ss
    private String repay_mode;//还款方式，1：等额本息2：等额本金3：按期付息到期还本6：到期一次还本付息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getCust_id_core() {
        return cust_id_core;
    }

    public void setCust_id_core(String cust_id_core) {
        this.cust_id_core = cust_id_core;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public BigDecimal getEncash_amt() {
        return encash_amt;
    }

    public void setEncash_amt(BigDecimal encash_amt) {
        this.encash_amt = encash_amt;
    }

    public BigDecimal getDay_rate() {
        return day_rate;
    }

    public void setDay_rate(BigDecimal day_rate) {
        this.day_rate = day_rate;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getApply_date() {
        return apply_date;
    }

    public void setApply_date(String apply_date) {
        this.apply_date = apply_date;
    }

    public String getEncash_date() {
        return encash_date;
    }

    public void setEncash_date(String encash_date) {
        this.encash_date = encash_date;
    }

    public String getRepay_mode() {
        return repay_mode;
    }

    public void setRepay_mode(String repay_mode) {
        this.repay_mode = repay_mode;
    }

    @Override
    public String toString() {
        return "Record{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", datasq='" + datasq + '\'' +
                ", cust_id_core='" + cust_id_core + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", contract_no='" + contract_no + '\'' +
                ", encash_amt=" + encash_amt +
                ", day_rate=" + day_rate +
                ", start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                ", apply_date='" + apply_date + '\'' +
                ", encash_date='" + encash_date + '\'' +
                ", repay_mode='" + repay_mode + '\'' +
                '}';
    }
}
