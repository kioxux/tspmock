package cn.com.yusys.yusp.web.client.esb.yphsxt.xddb05;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb05.req.Xddb05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb05.resp.Xddb05RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.req.Xddb05ReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.resp.Xddb05RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询基本信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2Xddb05Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xddb05Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xddb05
     * 交易描述：查询基本信息
     *
     * @param xddb05ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xddb05")
    protected @ResponseBody
    ResultDto<Xddb05RespDto> xddb05(@Validated @RequestBody Xddb05ReqDto xddb05ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB05.key, EsbEnum.TRADE_CODE_XDDB05.value, JSON.toJSONString(xddb05ReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb05.resp.Service();
        Xddb05ReqService xddb05ReqService = new Xddb05ReqService();
        Xddb05RespService xddb05RespService = new Xddb05RespService();
        Xddb05RespDto xddb05RespDto = new Xddb05RespDto();
        ResultDto<Xddb05RespDto> xddb05ResultDto = new ResultDto<Xddb05RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xddb05ReqDto转换成reqService
            BeanUtils.copyProperties(xddb05ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDDB05.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xddb05ReqService.setService(reqService);
            // 将xddb05ReqService转换成xddb05ReqServiceMap
            Map xddb05ReqServiceMap = beanMapUtil.beanToMap(xddb05ReqService);
            context.put("tradeDataMap", xddb05ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB05.key, EsbEnum.TRADE_CODE_XDDB05.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDDB05.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB05.key, EsbEnum.TRADE_CODE_XDDB05.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xddb05RespService = beanMapUtil.mapToBean(tradeDataMap, Xddb05RespService.class, Xddb05RespService.class);
            respService = xddb05RespService.getService();
            //  将respService转换成Xddb05RespDto
            xddb05ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xddb05ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xddb05RespDto);
                xddb05ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb05ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xddb05ResultDto.setCode(EpbEnum.EPB099999.key);
                xddb05ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB05.key, EsbEnum.TRADE_CODE_XDDB05.value, e.getMessage());
            xddb05ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xddb05ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xddb05ResultDto.setData(xddb05RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB05.key, EsbEnum.TRADE_CODE_XDDB05.value, JSON.toJSONString(xddb05ResultDto));
        return xddb05ResultDto;
    }
}
