package cn.com.yusys.yusp.online.client.esb.core.ln3244.resp;

/**
 * 响应Service：贷款客户经理批量移交
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3244RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3244RespService{" +
                "service=" + service +
                '}';
    }
}

