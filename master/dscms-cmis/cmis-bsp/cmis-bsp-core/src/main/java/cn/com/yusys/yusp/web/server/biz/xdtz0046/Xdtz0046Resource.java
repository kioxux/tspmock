package cn.com.yusys.yusp.web.server.biz.xdtz0046;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0046.req.Xdtz0046ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0046.resp.Xdtz0046RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0046.req.Xdtz0046DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0046.resp.Xdtz0046DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据借据号获取共同借款人信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0046:根据借据号获取共同借款人信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0046Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0046Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0046
     * 交易描述：根据借据号获取共同借款人信息
     *
     * @param xdtz0046ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据借据号获取共同借款人信息")
    @PostMapping("/xdtz0046")
    //@Idempotent({"xdcatz0046", "#xdtz0046ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0046RespDto xdtz0046(@Validated @RequestBody Xdtz0046ReqDto xdtz0046ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, JSON.toJSONString(xdtz0046ReqDto));
        Xdtz0046DataReqDto xdtz0046DataReqDto = new Xdtz0046DataReqDto();// 请求Data： 根据借据号获取共同借款人信息
        Xdtz0046DataRespDto xdtz0046DataRespDto = new Xdtz0046DataRespDto();// 响应Data：根据借据号获取共同借款人信息
        Xdtz0046RespDto xdtz0046RespDto = new Xdtz0046RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0046.req.Data reqData = null; // 请求Data：根据借据号获取共同借款人信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0046.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0046.resp.Data();// 响应Data：根据借据号获取共同借款人信息
        try {
            // 从 xdtz0046ReqDto获取 reqData
            reqData = xdtz0046ReqDto.getData();
            // 将 reqData 拷贝到xdtz0046DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0046DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, JSON.toJSONString(xdtz0046DataReqDto));
            ResultDto<Xdtz0046DataRespDto> xdtz0046DataResultDto = dscmsBizTzClientService.xdtz0046(xdtz0046DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, JSON.toJSONString(xdtz0046DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0046RespDto.setErorcd(Optional.ofNullable(xdtz0046DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0046RespDto.setErortx(Optional.ofNullable(xdtz0046DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0046DataResultDto.getCode())) {
                xdtz0046DataRespDto = xdtz0046DataResultDto.getData();
                xdtz0046RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0046RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0046DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0046DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, e.getMessage());
            xdtz0046RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0046RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0046RespDto.setDatasq(servsq);

        xdtz0046RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0046.key, DscmsEnum.TRADE_CODE_XDTZ0046.value, JSON.toJSONString(xdtz0046RespDto));
        return xdtz0046RespDto;
    }
}
