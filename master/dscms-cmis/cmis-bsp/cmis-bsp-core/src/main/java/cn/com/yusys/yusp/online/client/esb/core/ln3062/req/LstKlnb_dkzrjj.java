package cn.com.yusys.yusp.online.client.esb.core.ln3062.req;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 13:29
 * @since 2021/6/7 13:29
 */
public class LstKlnb_dkzrjj {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3062.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstKlnb_dkzrjj{" +
                "record=" + record +
                '}';
    }
}
