package cn.com.yusys.yusp.online.client.esb.fxyjxt.lsfxbg.req;

/**
 * 请求Service：查询客户项下历史风险预警报告
 *
 * @author code-generator
 * @version 1.0
 */
public class LsfxbgReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
