package cn.com.yusys.yusp.web.server.biz.xdht0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0004.req.Xdht0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0004.resp.Xdht0004RespDto;
import cn.com.yusys.yusp.dto.server.xdht0004.req.Xdht0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0004.resp.Xdht0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import cn.com.yusys.yusp.web.server.biz.xdht0003.Xdht0003Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:合同信息列表查询
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDHT0004:合同信息列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0003Resource.class);
    @Autowired
    private DscmsBizHtClientService dscmsBizHtClientService;

    /**
     * 交易码：xdht0004
     * 交易描述：合同信息列表查询
     *
     * @param xdht0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("合同信息列表查询")
    @PostMapping("/xdht0004")
    //@Idempotent({"xdcaht0004", "#xdht0004ReqDto.datasq"})
    protected @ResponseBody
    Xdht0004RespDto xdht0004(@Validated @RequestBody Xdht0004ReqDto xdht0004ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004ReqDto));
        Xdht0004DataReqDto xdht0004DataReqDto = new Xdht0004DataReqDto();// 请求Data： 合同信息列表查询
        Xdht0004DataRespDto xdht0004DataRespDto = new Xdht0004DataRespDto();// 响应Data：合同信息列表查询
        Xdht0004RespDto xdht0004RespDto = new Xdht0004RespDto();//响应Data：合同信息列表查询
        cn.com.yusys.yusp.dto.server.biz.xdht0004.req.Data reqData = null; // 请求Data：合同信息列表查询
        cn.com.yusys.yusp.dto.server.biz.xdht0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0004.resp.Data();// 响应Data：合同信息列表查询
        // 从 xdht0004ReqDto获取 reqData
        try {
            reqData = xdht0004ReqDto.getData();
            // 将 reqData 拷贝到xdht0004DataReqDto
            BeanUtils.copyProperties(reqData, xdht0004DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004ReqDto));
            ResultDto<Xdht0004DataRespDto> xdht0004DataResultDto = dscmsBizHtClientService.xdht0004(xdht0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdht0004RespDto.setErorcd(Optional.ofNullable(xdht0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0004RespDto.setErortx(Optional.ofNullable(xdht0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            xdht0004RespDto.setDatasq("test");
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0004DataResultDto.getCode())) {
                xdht0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdht0004DataRespDto = xdht0004DataResultDto.getData();
                // 将 xdht0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, e.getMessage());
            xdht0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0004RespDto.setDatasq(servsq);
        xdht0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0004.key, DscmsEnum.TRADE_CODE_XDHT0004.value, JSON.toJSONString(xdht0004RespDto));
        return xdht0004RespDto;
    }
}
