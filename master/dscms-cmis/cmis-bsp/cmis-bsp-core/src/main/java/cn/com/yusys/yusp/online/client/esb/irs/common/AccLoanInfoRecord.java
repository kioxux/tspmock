package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.math.BigDecimal;

/**
 * 请求Service：交易请求信息域:非垫款借据信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 10:10
 */
public class AccLoanInfoRecord {

    private String bill_no; // 借据编号
    private String cont_no; // 合同编号
    private String item_id; // 授信台帐编号
    private String lmt_serno; // 授信协议编号
    private String pro_no; // 产品编号
    private String prd_name; // 产品名称
    private String cus_id; // 客户代码
    private String cus_name; // 客户名称
    private String loan_form; // 贷款形式
    private String assure_means_main; // 主担保方式
    private String cur_type; // 币种
    private BigDecimal margin_ratio; // 保证金比例
    private BigDecimal margin_balance; // 保证金金额
    private BigDecimal balance; // 借据金额(元)
    private BigDecimal loan_balance; // 借据余额(元)
    private BigDecimal pad_balance; // 逾期利息（元）
    private BigDecimal interest_balance; // 应收未收利息余额（元）
    private BigDecimal hand_balance; // 手续费金额(元)
    private String loan_start_date; // 贷款起始日
    private String loan_end_date; // 贷款到期日
    private String loan_direction; // 贷款投向
    private String guarantee_no; // 保函种类
    private String guarantee_name; // 保函名称
    private BigDecimal loancard_due; // 信用证付款期限
    private BigDecimal forward_days; // 远期天数
    private String pro_details; // 具体产品
    private String loan_paym_mtd; // 付款方式
    private String isin_card; // 是否单证相符
    private String isin_revocation; // 是否可无条件撤销
    private BigDecimal loan_due; // 贷款期限
    private String status; // 台账状态
    private String bankstatus; // 银承台账状态
    private String clear_status; // 清收状态
    private String low_cost; // 法律费用
    private String input_id; // 登记人
    private String input_br_id; // 登记机构
    private String manager_id; // 责任人
    private String manager_br_id; // 责任人机构
    private String fina_br_id; // 账务机构

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getPro_no() {
        return pro_no;
    }

    public void setPro_no(String pro_no) {
        this.pro_no = pro_no;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getLoan_form() {
        return loan_form;
    }

    public void setLoan_form(String loan_form) {
        this.loan_form = loan_form;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public BigDecimal getMargin_ratio() {
        return margin_ratio;
    }

    public void setMargin_ratio(BigDecimal margin_ratio) {
        this.margin_ratio = margin_ratio;
    }

    public BigDecimal getMargin_balance() {
        return margin_balance;
    }

    public void setMargin_balance(BigDecimal margin_balance) {
        this.margin_balance = margin_balance;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getLoan_balance() {
        return loan_balance;
    }

    public void setLoan_balance(BigDecimal loan_balance) {
        this.loan_balance = loan_balance;
    }

    public BigDecimal getPad_balance() {
        return pad_balance;
    }

    public void setPad_balance(BigDecimal pad_balance) {
        this.pad_balance = pad_balance;
    }

    public BigDecimal getInterest_balance() {
        return interest_balance;
    }

    public void setInterest_balance(BigDecimal interest_balance) {
        this.interest_balance = interest_balance;
    }

    public BigDecimal getHand_balance() {
        return hand_balance;
    }

    public void setHand_balance(BigDecimal hand_balance) {
        this.hand_balance = hand_balance;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getLoan_direction() {
        return loan_direction;
    }

    public void setLoan_direction(String loan_direction) {
        this.loan_direction = loan_direction;
    }

    public String getGuarantee_no() {
        return guarantee_no;
    }

    public void setGuarantee_no(String guarantee_no) {
        this.guarantee_no = guarantee_no;
    }

    public String getGuarantee_name() {
        return guarantee_name;
    }

    public void setGuarantee_name(String guarantee_name) {
        this.guarantee_name = guarantee_name;
    }

    public BigDecimal getLoancard_due() {
        return loancard_due;
    }

    public void setLoancard_due(BigDecimal loancard_due) {
        this.loancard_due = loancard_due;
    }

    public BigDecimal getForward_days() {
        return forward_days;
    }

    public void setForward_days(BigDecimal forward_days) {
        this.forward_days = forward_days;
    }

    public String getPro_details() {
        return pro_details;
    }

    public void setPro_details(String pro_details) {
        this.pro_details = pro_details;
    }

    public String getLoan_paym_mtd() {
        return loan_paym_mtd;
    }

    public void setLoan_paym_mtd(String loan_paym_mtd) {
        this.loan_paym_mtd = loan_paym_mtd;
    }

    public String getIsin_card() {
        return isin_card;
    }

    public void setIsin_card(String isin_card) {
        this.isin_card = isin_card;
    }

    public String getIsin_revocation() {
        return isin_revocation;
    }

    public void setIsin_revocation(String isin_revocation) {
        this.isin_revocation = isin_revocation;
    }

    public BigDecimal getLoan_due() {
        return loan_due;
    }

    public void setLoan_due(BigDecimal loan_due) {
        this.loan_due = loan_due;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBankstatus() {
        return bankstatus;
    }

    public void setBankstatus(String bankstatus) {
        this.bankstatus = bankstatus;
    }

    public String getClear_status() {
        return clear_status;
    }

    public void setClear_status(String clear_status) {
        this.clear_status = clear_status;
    }

    public String getLow_cost() {
        return low_cost;
    }

    public void setLow_cost(String low_cost) {
        this.low_cost = low_cost;
    }

    public String getInput_id() {
        return input_id;
    }

    public void setInput_id(String input_id) {
        this.input_id = input_id;
    }

    public String getInput_br_id() {
        return input_br_id;
    }

    public void setInput_br_id(String input_br_id) {
        this.input_br_id = input_br_id;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getFina_br_id() {
        return fina_br_id;
    }

    public void setFina_br_id(String fina_br_id) {
        this.fina_br_id = fina_br_id;
    }

    @Override
    public String toString() {
        return "GuaranteeContrctInfo{" +
                "bill_no='" + bill_no + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", item_id='" + item_id + '\'' +
                ", lmt_serno='" + lmt_serno + '\'' +
                ", pro_no='" + pro_no + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", loan_form='" + loan_form + '\'' +
                ", assure_means_main='" + assure_means_main + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", margin_ratio=" + margin_ratio +
                ", margin_balance=" + margin_balance +
                ", balance=" + balance +
                ", loan_balance=" + loan_balance +
                ", pad_balance=" + pad_balance +
                ", interest_balance=" + interest_balance +
                ", hand_balance=" + hand_balance +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", loan_direction='" + loan_direction + '\'' +
                ", guarantee_no='" + guarantee_no + '\'' +
                ", guarantee_name='" + guarantee_name + '\'' +
                ", loancard_due=" + loancard_due +
                ", forward_days=" + forward_days +
                ", pro_details='" + pro_details + '\'' +
                ", loan_paym_mtd='" + loan_paym_mtd + '\'' +
                ", isin_card='" + isin_card + '\'' +
                ", isin_revocation='" + isin_revocation + '\'' +
                ", loan_due=" + loan_due +
                ", status='" + status + '\'' +
                ", bankstatus='" + bankstatus + '\'' +
                ", clear_status='" + clear_status + '\'' +
                ", low_cost='" + low_cost + '\'' +
                ", input_id='" + input_id + '\'' +
                ", input_br_id='" + input_br_id + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", fina_br_id='" + fina_br_id + '\'' +
                '}';
    }
}
