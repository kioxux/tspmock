package cn.com.yusys.yusp.web.client.esb.core.ln3070;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3070.req.Ln3070ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3070.resp.Ln3070RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3070.req.Ln3070ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3070.resp.Ln3070RespService;
import cn.com.yusys.yusp.web.client.esb.core.ln3055.Dscms2Ln3055Resource;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3070)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3070Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3055Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3070
     * 交易描述：交易说明
     *
     * @param ln3070ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3070:贷款录入信息的取消，即取消出账指令")
    @PostMapping("/ln3070")
    protected @ResponseBody
    ResultDto<Ln3070RespDto> ln3070(@Validated @RequestBody Ln3070ReqDto ln3070ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3070.key, EsbEnum.TRADE_CODE_LN3070.value, JSON.toJSONString(ln3070ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3070.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3070.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3070.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3070.resp.Service();
        Ln3070ReqService ln3070ReqService = new Ln3070ReqService();
        Ln3070RespService ln3070RespService = new Ln3070RespService();
        Ln3070RespDto ln3070RespDto = new Ln3070RespDto();
        ResultDto<Ln3070RespDto> ln3070ResultDto = new ResultDto<Ln3070RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3070ReqDto转换成reqService
            BeanUtils.copyProperties(ln3070ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3070.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3070ReqService.setService(reqService);
            // 将ln3070ReqService转换成ln3070ReqServiceMap
            Map ln3070ReqServiceMap = beanMapUtil.beanToMap(ln3070ReqService);
            context.put("tradeDataMap", ln3070ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3070.key, EsbEnum.TRADE_CODE_LN3070.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3070.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3070.key, EsbEnum.TRADE_CODE_LN3070.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3070RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3070RespService.class, Ln3070RespService.class);
            respService = ln3070RespService.getService();

            ln3070ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3070ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3070RespDto
                BeanUtils.copyProperties(respService, ln3070RespDto);
                ln3070ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3070ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3070ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3070ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3070.key, EsbEnum.TRADE_CODE_LN3070.value, e.getMessage());
            ln3070ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3070ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3070ResultDto.setData(ln3070RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3070.key, EsbEnum.TRADE_CODE_LN3070.value, JSON.toJSONString(ln3070ResultDto));
        return ln3070ResultDto;
    }
}
