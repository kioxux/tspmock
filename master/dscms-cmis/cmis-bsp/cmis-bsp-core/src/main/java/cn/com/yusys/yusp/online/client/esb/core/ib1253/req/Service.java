package cn.com.yusys.yusp.online.client.esb.core.ib1253.req;

/**
 * 请求Service：根据账号查询帐户信息接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16上午10:43:45
 */
public class Service {

    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String kehuzhao; // 客户账号
    private String zhhaoxuh; // 子账户序号
    private String yanmbzhi; // 密码校验方式
    private String mimammmm; // 密码
    private String kehzhao2; // 客户账号2
    private String shifoubz; // 是否标志
    private String zhufldm1; // 账户分类代码1
    private String zhufldm2; // 账户分类代码2

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getYanmbzhi() {
        return yanmbzhi;
    }

    public void setYanmbzhi(String yanmbzhi) {
        this.yanmbzhi = yanmbzhi;
    }

    public String getMimammmm() {
        return mimammmm;
    }

    public void setMimammmm(String mimammmm) {
        this.mimammmm = mimammmm;
    }

    public String getKehzhao2() {
        return kehzhao2;
    }

    public void setKehzhao2(String kehzhao2) {
        this.kehzhao2 = kehzhao2;
    }

    public String getShifoubz() {
        return shifoubz;
    }

    public void setShifoubz(String shifoubz) {
        this.shifoubz = shifoubz;
    }

    public String getZhufldm1() {
        return zhufldm1;
    }

    public void setZhufldm1(String zhufldm1) {
        this.zhufldm1 = zhufldm1;
    }

    public String getZhufldm2() {
        return zhufldm2;
    }

    public void setZhufldm2(String zhufldm2) {
        this.zhufldm2 = zhufldm2;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", yanmbzhi='" + yanmbzhi + '\'' +
                ", mimammmm='" + mimammmm + '\'' +
                ", kehzhao2='" + kehzhao2 + '\'' +
                ", shifoubz='" + shifoubz + '\'' +
                ", zhufldm1='" + zhufldm1 + '\'' +
                ", zhufldm2='" + zhufldm2 + '\'' +
                '}';
    }
}