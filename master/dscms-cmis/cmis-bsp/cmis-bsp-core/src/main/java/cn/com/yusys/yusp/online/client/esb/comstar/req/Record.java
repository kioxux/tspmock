package cn.com.yusys.yusp.online.client.esb.comstar.req;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/22 16:52
 * @since 2021/6/22 16:52
 */
public class Record {
    private String number;//序号
    private String origiAccNo;//授信批复编号
    private String lmtSubNo;//授信分项ID
    private String lmtType;//额度类型
    private String limitSubNo;//额度品种编号
    private String limitSubName;//额度品种名称
    private String cusId;//Ecif客户号
    private String certType;//授信主体证件类型
    private String certCode;//授信主体证件号码
    private String cusName;//授信主体客户名称
    private String assetNo;//资产编号
    private BigDecimal lmtAmt;//授信金额
    private BigDecimal singleeResaleQuota;//单个产品户买入返售限额
    private BigDecimal lmtSingleMfAmt;//单只货币基金授信额度
    private String endDate;//额度到期日
    private String optType;//操作类型
    private String deleteFlag;//删除类型

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getOrigiAccNo() {
        return origiAccNo;
    }

    public void setOrigiAccNo(String origiAccNo) {
        this.origiAccNo = origiAccNo;
    }

    public String getLmtSubNo() {
        return lmtSubNo;
    }

    public void setLmtSubNo(String lmtSubNo) {
        this.lmtSubNo = lmtSubNo;
    }

    public String getLmtType() {
        return lmtType;
    }

    public void setLmtType(String lmtType) {
        this.lmtType = lmtType;
    }

    public String getLimitSubNo() {
        return limitSubNo;
    }

    public void setLimitSubNo(String limitSubNo) {
        this.limitSubNo = limitSubNo;
    }

    public String getLimitSubName() {
        return limitSubName;
    }

    public void setLimitSubName(String limitSubName) {
        this.limitSubName = limitSubName;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public BigDecimal getLmtAmt() {
        return lmtAmt;
    }

    public void setLmtAmt(BigDecimal lmtAmt) {
        this.lmtAmt = lmtAmt;
    }

    public BigDecimal getSingleeResaleQuota() {
        return singleeResaleQuota;
    }

    public void setSingleeResaleQuota(BigDecimal singleeResaleQuota) {
        this.singleeResaleQuota = singleeResaleQuota;
    }

    public BigDecimal getLmtSingleMfAmt() {
        return lmtSingleMfAmt;
    }

    public void setLmtSingleMfAmt(BigDecimal lmtSingleMfAmt) {
        this.lmtSingleMfAmt = lmtSingleMfAmt;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getOptType() {
        return optType;
    }

    public void setOptType(String optType) {
        this.optType = optType;
    }

    public String getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    @Override
    public String toString() {
        return "Record{" +
                "number='" + number + '\'' +
                ", origiAccNo='" + origiAccNo + '\'' +
                ", lmtSubNo='" + lmtSubNo + '\'' +
                ", lmtType='" + lmtType + '\'' +
                ", limitSubNo='" + limitSubNo + '\'' +
                ", limitSubName='" + limitSubName + '\'' +
                ", cusId='" + cusId + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", cusName='" + cusName + '\'' +
                ", assetNo='" + assetNo + '\'' +
                ", lmtAmt=" + lmtAmt +
                ", singleeResaleQuota=" + singleeResaleQuota +
                ", lmtSingleMfAmt=" + lmtSingleMfAmt +
                ", endDate='" + endDate + '\'' +
                ", optType='" + optType + '\'' +
                ", deleteFlag='" + deleteFlag + '\'' +
                '}';
    }
}
