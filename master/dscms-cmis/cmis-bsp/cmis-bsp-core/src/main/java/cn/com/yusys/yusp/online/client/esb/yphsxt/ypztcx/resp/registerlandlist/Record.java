package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerlandlist;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:15
 * @since 2021/7/9 14:15
 */
public class Record {
    private String laarea;//登记簿土地信息-土地面积
    private String lanuse;//登记簿土地信息-土地用途
    private String landna;//登记簿土地信息-土地性质
    private String lanrsd;//登记簿土地信息-土地使用权开始
    private String lanred;//登记簿土地信息-土地使用权结束
    private String lanloc;//登记簿土地信息-坐落

    public String getLaarea() {
        return laarea;
    }

    public void setLaarea(String laarea) {
        this.laarea = laarea;
    }

    public String getLanuse() {
        return lanuse;
    }

    public void setLanuse(String lanuse) {
        this.lanuse = lanuse;
    }

    public String getLandna() {
        return landna;
    }

    public void setLandna(String landna) {
        this.landna = landna;
    }

    public String getLanrsd() {
        return lanrsd;
    }

    public void setLanrsd(String lanrsd) {
        this.lanrsd = lanrsd;
    }

    public String getLanred() {
        return lanred;
    }

    public void setLanred(String lanred) {
        this.lanred = lanred;
    }

    public String getLanloc() {
        return lanloc;
    }

    public void setLanloc(String lanloc) {
        this.lanloc = lanloc;
    }

    @Override
    public String toString() {
        return "Record{" +
                "laarea='" + laarea + '\'' +
                ", lanuse='" + lanuse + '\'' +
                ", landna='" + landna + '\'' +
                ", lanrsd='" + lanrsd + '\'' +
                ", lanred='" + lanred + '\'' +
                ", lanloc='" + lanloc + '\'' +
                '}';
    }
}
