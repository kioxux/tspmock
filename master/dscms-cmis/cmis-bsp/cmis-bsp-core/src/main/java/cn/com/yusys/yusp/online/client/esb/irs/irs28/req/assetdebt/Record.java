package cn.com.yusys.yusp.online.client.esb.irs.irs28.req.assetdebt;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 16:18
 * @since 2021/5/27 16:18
 */
public class Record {
    private String subjectno;//科目编码
    private BigDecimal colvalue1;//期初值
    private BigDecimal colvalue2;//期末值

    public String getSubjectno() {
        return subjectno;
    }

    public void setSubjectno(String subjectno) {
        this.subjectno = subjectno;
    }

    public BigDecimal getColvalue1() {
        return colvalue1;
    }

    public void setColvalue1(BigDecimal colvalue1) {
        this.colvalue1 = colvalue1;
    }

    public BigDecimal getColvalue2() {
        return colvalue2;
    }

    public void setColvalue2(BigDecimal colvalue2) {
        this.colvalue2 = colvalue2;
    }

    @Override
    public String toString() {
        return "Record{" +
                "subjectno='" + subjectno + '\'' +
                ", colvalue1=" + colvalue1 +
                ", colvalue2=" + colvalue2 +
                '}';
    }
}
