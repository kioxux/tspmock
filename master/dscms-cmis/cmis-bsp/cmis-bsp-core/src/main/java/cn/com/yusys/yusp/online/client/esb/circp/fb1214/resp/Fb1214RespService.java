package cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp;

/**
 * 响应Service：房产信息修改同步
 *
 * @author code-generator
 * @version 1.0
 */
public class Fb1214RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

