package cn.com.yusys.yusp.web.server.biz.xdtz0063;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0063.req.Xdtz0063ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0063.resp.Xdtz0063RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0063.req.Xdtz0063DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0063.resp.Xdtz0063DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:他行退汇冻结编号更新
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0063:他行退汇冻结编号更新")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0063Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0063Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0063
     * 交易描述：他行退汇冻结编号更新
     *
     * @param xdtz0063ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("他行退汇冻结编号更新")
    @PostMapping("/xdtz0063")
    //@Idempotent({"xdtz0063", "#xdtz0063ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0063RespDto xdtz0063(@Validated @RequestBody Xdtz0063ReqDto xdtz0063ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063ReqDto));
        Xdtz0063DataReqDto xdtz0063DataReqDto = new Xdtz0063DataReqDto();// 请求Data： 更新信贷台账信息
        Xdtz0063DataRespDto xdtz0063DataRespDto = new Xdtz0063DataRespDto();// 响应Data：更新信贷台账信息
        Xdtz0063RespDto xdtz0063RespDto = new Xdtz0063RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0063.req.Data reqData = null; // 请求Data：更新信贷台账信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0063.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0063.resp.Data();// 响应Data：更新信贷台账信息
        try {
            // 从 xdtz0063ReqDto获取 reqData
            reqData = xdtz0063ReqDto.getData();
            // 将 reqData 拷贝到xdtz0063DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0063DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataReqDto));
            ResultDto<Xdtz0063DataRespDto> xdtz0063DataResultDto = dscmsBizTzClientService.xdtz0063(xdtz0063DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0063RespDto.setErorcd(Optional.ofNullable(xdtz0063DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0063RespDto.setErortx(Optional.ofNullable(xdtz0063DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0063DataResultDto.getCode())) {
                xdtz0063DataRespDto = xdtz0063DataResultDto.getData();
                xdtz0063RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0063RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0063DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0063DataRespDto, respData);
            }else{
                xdtz0063RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                xdtz0063RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, e.getMessage());
            xdtz0063RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0063RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0063RespDto.setDatasq(servsq);

        xdtz0063RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0063.key, DscmsEnum.TRADE_CODE_XDTZ0063.value, JSON.toJSONString(xdtz0063RespDto));
        return xdtz0063RespDto;
    }
}
