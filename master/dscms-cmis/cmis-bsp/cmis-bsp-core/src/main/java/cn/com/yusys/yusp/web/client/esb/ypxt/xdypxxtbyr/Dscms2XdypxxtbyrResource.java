package cn.com.yusys.yusp.web.client.esb.ypxt.xdypxxtbyr;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.req.XdypxxtbyrList;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.req.XdypxxtbyrReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.resp.XdypxxtbyrRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.Record;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.XdypxxtbyrReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.resp.XdypxxtbyrRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(xdypxxtbyr)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2XdypxxtbyrResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdypxxtbyrResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdypxxtbyr
     * 交易描述：押品信息同步及引入
     *
     * @param xdypxxtbyrReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xdypxxtbyr")
    protected @ResponseBody
    ResultDto<XdypxxtbyrRespDto> xdypxxtbyr(@Validated @RequestBody XdypxxtbyrReqDto xdypxxtbyrReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPXXTBYR.key, EsbEnum.TRADE_CODE_XDYPXXTBYR.value, JSON.toJSONString(xdypxxtbyrReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.resp.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.List list = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.List();
        XdypxxtbyrReqService xdypxxtbyrReqService = new XdypxxtbyrReqService();
        XdypxxtbyrRespService xdypxxtbyrRespService = new XdypxxtbyrRespService();
        XdypxxtbyrRespDto xdypxxtbyrRespDto = new XdypxxtbyrRespDto();
        ResultDto<XdypxxtbyrRespDto> xdypxxtbyrResultDto = new ResultDto<XdypxxtbyrRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdypxxtbyrReqDto转换成reqService
            BeanUtils.copyProperties(xdypxxtbyrReqDto, reqService);
            if(CollectionUtils.nonEmpty(xdypxxtbyrReqDto.getXdypxxtbyrList())){
                java.util.List<XdypxxtbyrList> xdypxxtbyrLists=Optional.ofNullable(xdypxxtbyrReqDto.getXdypxxtbyrList()).orElseGet(() -> new ArrayList<>());
                List<Record> recordList = new ArrayList<>();
                for(XdypxxtbyrList xdypxxtbyrList : xdypxxtbyrLists){
                    cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.Record record = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypxxtbyr.req.Record();
                    BeanUtils.copyProperties(xdypxxtbyrList, record);
                    recordList.add(record);
                }
                list.setRecord(recordList);
                reqService.setList(list);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDYPXXTBYR.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xdypxxtbyrReqService.setService(reqService);
            // 将xdypxxtbyrReqService转换成xdypxxtbyrReqServiceMap
            Map xdypxxtbyrReqServiceMap = beanMapUtil.beanToMap(xdypxxtbyrReqService);
            context.put("tradeDataMap", xdypxxtbyrReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPXXTBYR.key, EsbEnum.TRADE_CODE_XDYPXXTBYR.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDYPXXTBYR.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPXXTBYR.key, EsbEnum.TRADE_CODE_XDYPXXTBYR.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdypxxtbyrRespService = beanMapUtil.mapToBean(tradeDataMap, XdypxxtbyrRespService.class, XdypxxtbyrRespService.class);
            respService = xdypxxtbyrRespService.getService();

            xdypxxtbyrResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdypxxtbyrResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G00101RespDto
                BeanUtils.copyProperties(respService, xdypxxtbyrRespDto);
                xdypxxtbyrResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdypxxtbyrResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdypxxtbyrResultDto.setCode(EpbEnum.EPB099999.key);
                xdypxxtbyrResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPXXTBYR.key, EsbEnum.TRADE_CODE_XDYPXXTBYR.value, e.getMessage());
            xdypxxtbyrResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdypxxtbyrResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdypxxtbyrResultDto.setData(xdypxxtbyrRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPXXTBYR.key, EsbEnum.TRADE_CODE_XDYPXXTBYR.value, JSON.toJSONString(xdypxxtbyrResultDto));
        return xdypxxtbyrResultDto;
    }
}
