package cn.com.yusys.yusp.web.server.cfg.xdxt0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0011.req.Xdxt0011ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdxt0011.resp.Xdxt0011RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.req.Xdxt0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0011.resp.Xdxt0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户经理信息通用查
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0011:客户经理信息通用查")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0011Resource.class);
    @Autowired
    private DscmsBizXtClientService dscmsXtClientService;

    /**
     * 交易码：xdxt0011
     * 交易描述：客户经理信息通用查
     *
     * @param xdxt0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户经理信息通用查")
    @PostMapping("/xdxt0011")
    //@Idempotent({"xdcaxt0011", "#xdxt0011ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0011RespDto xdxt0011(@Validated @RequestBody Xdxt0011ReqDto xdxt0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011ReqDto));
        Xdxt0011DataReqDto xdxt0011DataReqDto = new Xdxt0011DataReqDto();// 请求Data： 客户经理信息通用查
        Xdxt0011DataRespDto xdxt0011DataRespDto = new Xdxt0011DataRespDto();// 响应Data：客户经理信息通用查
        Xdxt0011RespDto xdxt0011RespDto = new Xdxt0011RespDto();
        cn.com.yusys.yusp.dto.server.cfg.xdxt0011.req.Data reqData = null; // 请求Data：客户经理信息通用查
        cn.com.yusys.yusp.dto.server.cfg.xdxt0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdxt0011.resp.Data();// 响应Data：客户经理信息通用查

        try {
            // 从 xdxt0011ReqDto获取 reqData
            reqData = xdxt0011ReqDto.getData();
            // 将 reqData 拷贝到xdxt0011DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataReqDto));
            ResultDto<Xdxt0011DataRespDto> xdxt0011DataResultDto = dscmsXtClientService.xdxt0011(xdxt0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxt0011RespDto.setErorcd(Optional.ofNullable(xdxt0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0011RespDto.setErortx(Optional.ofNullable(xdxt0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0011DataResultDto.getCode())) {
                xdxt0011DataRespDto = xdxt0011DataResultDto.getData();
                xdxt0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0011DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, e.getMessage());
            xdxt0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0011RespDto.setDatasq(servsq);

        xdxt0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0011.key, DscmsEnum.TRADE_CODE_XDXT0011.value, JSON.toJSONString(xdxt0011RespDto));
        return xdxt0011RespDto;
    }
}
