package cn.com.yusys.yusp.web.server.biz.xdxw0024;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0024.req.Xdxw0024ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0024.resp.Xdxw0024RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0024.req.Xdxw0024DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0024.resp.Xdxw0024DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询是否有信贷记录
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0024:查询是否有信贷记录")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0024Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0024Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0024
     * 交易描述：查询是否有信贷记录
     *
     * @param xdxw0024ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询是否有信贷记录")
    @PostMapping("/xdxw0024")
    //@Idempotent({"xdcaxw0024", "#xdxw0024ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0024RespDto xdxw0024(@Validated @RequestBody Xdxw0024ReqDto xdxw0024ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024ReqDto));
        Xdxw0024DataReqDto xdxw0024DataReqDto = new Xdxw0024DataReqDto();// 请求Data： 查询是否有信贷记录
        Xdxw0024DataRespDto xdxw0024DataRespDto = new Xdxw0024DataRespDto();// 响应Data：查询是否有信贷记录
        Xdxw0024RespDto xdxw0024RespDto = new Xdxw0024RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0024.req.Data reqData = null; // 请求Data：查询是否有信贷记录
        cn.com.yusys.yusp.dto.server.biz.xdxw0024.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0024.resp.Data();// 响应Data：查询是否有信贷记录
        try {
            // 从 xdxw0024ReqDto获取 reqData
            reqData = xdxw0024ReqDto.getData();
            // 将 reqData 拷贝到xdxw0024DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0024DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024DataReqDto));
            ResultDto<Xdxw0024DataRespDto> xdxw0024DataResultDto = dscmsBizXwClientService.xdxw0024(xdxw0024DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0024RespDto.setErorcd(Optional.ofNullable(xdxw0024DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0024RespDto.setErortx(Optional.ofNullable(xdxw0024DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0024DataResultDto.getCode())) {
                xdxw0024DataRespDto = xdxw0024DataResultDto.getData();
                xdxw0024RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0024RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0024DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0024DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, e.getMessage());
            xdxw0024RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0024RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0024RespDto.setDatasq(servsq);

        xdxw0024RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0024.key, DscmsEnum.TRADE_CODE_XDXW0024.value, JSON.toJSONString(xdxw0024RespDto));
        return xdxw0024RespDto;
    }
}
