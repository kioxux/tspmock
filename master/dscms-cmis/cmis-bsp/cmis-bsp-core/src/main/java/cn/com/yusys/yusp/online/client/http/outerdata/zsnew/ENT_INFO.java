package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonPropertyOrder(alphabetic = true)
public class ENT_INFO implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ALTER")
    private List<ALTER> ALTER;//企业历史变更信息
    @JsonProperty(value = "BASIC")
    private BASIC BASIC;//企业照面信息
    @JsonProperty(value = "BASICLIST")
    private List<BASICLIST> BASICLIST;//企业基本信息列表
    @JsonProperty(value = "BREAKLAW")
    private List<BREAKLAW> BREAKLAW;//严重违法
    @JsonProperty(value = "ENTCASEBASEINFO")
    private List<ENTCASEBASEINFO> ENTCASEBASEINFO;//行政处罚基本信息
    @JsonProperty(value = "ENTINV")
    private List<ENTINV> ENTINV;//企业对外投资
    @JsonProperty(value = "EXCEPTIONLIST")
    private List<EXCEPTIONLIST> EXCEPTIONLIST;//企业经营异常名录
    @JsonProperty(value = "FILIATION")
    private List<FILIATION> FILIATION;//分支机构
    @JsonProperty(value = "FINALCASE")
    private List<FINALCASE> FINALCASE;//终本案件信息
    @JsonProperty(value = "FRINV")
    private List<FRINV> FRINV;//法定代表人对外投资
    @JsonProperty(value = "FRPOSITION")
    private List<FRPOSITION> FRPOSITION;//法定代表人其他公司任职
    @JsonProperty(value = "INSPECT")
    private List<INSPECT> INSPECT;//抽查检查
    @JsonProperty(value = "JUDICIALAID")
    private List<JUDICIALAID> JUDICIALAID;//司法协助基本信息
    @JsonProperty(value = "JUDICIALAIDALTER")
    private List<JUDICIALAIDALTER> JUDICIALAIDALTER;//司法协助变更信息
    @JsonProperty(value = "JUDICIALAIDDETAIL")
    private List<JUDICIALAIDDETAIL> JUDICIALAIDDETAIL;//司法协助详情
    @JsonProperty(value = "LIQUIDATION")
    private List<LIQUIDATION> LIQUIDATION;// 清算信息
    @JsonProperty(value = "LISTEDCOMPINFO")
    private List<LISTEDCOMPINFO> LISTEDCOMPINFO;//上市公司基本信息
    @JsonProperty(value = "LISTEDINFO")
    private List<LISTEDINFO> LISTEDINFO;//	上市股票基本信息
    @JsonProperty(value = "LISTEDMANAGER")
    private List<LISTEDMANAGER> LISTEDMANAGER;//	上市公司高管信息
    @JsonProperty(value = "LISTEDSHAREHOLDER")
    private List<LISTEDSHAREHOLDER> LISTEDSHAREHOLDER;//	上市公司十大股东
    @JsonProperty(value = "MORTGAGEALT")
    private List<MORTGAGEALT> MORTGAGEALT;//	动产抵押-变更信息
    @JsonProperty(value = "MORTGAGEBASIC")
    private List<MORTGAGEBASIC> MORTGAGEBASIC;//	动产抵押-基本信息
    @JsonProperty(value = "MORTGAGECAN")
    private List<MORTGAGECAN> MORTGAGECAN;//	动产抵押-注销信息
    @JsonProperty(value = "MORTGAGEDEBT")
    private List<MORTGAGEDEBT> MORTGAGEDEBT;//	动产抵押-被担保主债权信息
    @JsonProperty(value = "MORTGAGEPAWN")
    private List<MORTGAGEPAWN> MORTGAGEPAWN;//	动产抵押-抵押物信息
    @JsonProperty(value = "MORTGAGEPER")
    private List<MORTGAGEPER> MORTGAGEPER;//	动产抵押-抵押权人信息
    @JsonProperty(value = "MORTGAGEREG")
    private List<MORTGAGEREG> MORTGAGEREG;//	动产抵押-登记信息
    @JsonProperty(value = "PERSON")
    private List<PERSON> PERSON;//主要管理人员
    @JsonProperty(value = "PUNISHBREAK")
    private List<PUNISHBREAK> PUNISHBREAK;//	失信被执行人信息
    @JsonProperty(value = "PUNISHED")
    private List<PUNISHED> PUNISHED;//	被执行人
    @JsonProperty(value = "QUICKCANCELBASIC")
    private List<QUICKCANCELBASIC> QUICKCANCELBASIC;//	简易注销-基本信息
    @JsonProperty(value = "QUICKCANCELDISSENT")
    private List<QUICKCANCELDISSENT> QUICKCANCELDISSENT;//	简易注销-异议信息
    @JsonProperty(value = "RELATEDPUNISHBREAK")
    private List<RELATEDPUNISHBREAK> RELATEDPUNISHBREAK;//	关联失信被执行人信息
    @JsonProperty(value = "RELATEDPUNISHED")
    private List<RELATEDPUNISHED> RELATEDPUNISHED;//	关联被执行人信息
    @JsonProperty(value = "SHAREHOLDER")
    private List<SHAREHOLDER> SHAREHOLDER;//	股东及出资信息
    @JsonProperty(value = "STOCKPAWN")
    private List<STOCKPAWN> STOCKPAWN;//	股权出质信息（新）
    @JsonProperty(value = "STOCKPAWNALT")
    private List<STOCKPAWNALT> STOCKPAWNALT;//	股权出质信息（新）-变更信息
    @JsonProperty(value = "STOCKPAWNREV")
    private List<STOCKPAWNREV> STOCKPAWNREV;//	股权出质信息（新）-注销信息
    @JsonProperty(value = "YEARREPORTALTER")
    private List<YEARREPORTALTER> YEARREPORTALTER;//	年报-修改信息
    @JsonProperty(value = "YEARREPORTALTERSTOCK")
    private List<YEARREPORTALTERSTOCK> YEARREPORTALTERSTOCK;// 年报-股权变更信息
    @JsonProperty(value = "YEARREPORTBASIC")
    private List<YEARREPORTBASIC> YEARREPORTBASIC;//	年报-企业年报基础信息
    @JsonProperty(value = "YEARREPORTFORINV")
    private List<YEARREPORTFORINV> YEARREPORTFORINV;//	年报-企业对外投资信息
    @JsonProperty(value = "YEARREPORTPAIDUPCAPITAL")
    private List<YEARREPORTPAIDUPCAPITAL> YEARREPORTPAIDUPCAPITAL;//	年报-企业实缴出资信息
    @JsonProperty(value = "YEARREPORTSOCSEC")
    private List<YEARREPORTSOCSEC> YEARREPORTSOCSEC;//	年报-社会保险信息
    @JsonProperty(value = "YEARREPORTSUBCAPITAL")
    private List<YEARREPORTSUBCAPITAL> YEARREPORTSUBCAPITAL;//	年报-企业实缴出资信息
    @JsonProperty(value = "YEARREPORTWEBSITEINFO")
    private List<YEARREPORTWEBSITEINFO> YEARREPORTWEBSITEINFO;//	年报-网站信息

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ALTER> getALTER() {
        return ALTER;
    }

    @JsonIgnore
    public void setALTER(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ALTER> ALTER) {
        this.ALTER = ALTER;
    }

    @JsonIgnore
    public cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BASIC getBASIC() {
        return BASIC;
    }

    @JsonIgnore
    public void setBASIC(cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BASIC BASIC) {
        this.BASIC = BASIC;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BASICLIST> getBASICLIST() {
        return BASICLIST;
    }

    @JsonIgnore
    public void setBASICLIST(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BASICLIST> BASICLIST) {
        this.BASICLIST = BASICLIST;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BREAKLAW> getBREAKLAW() {
        return BREAKLAW;
    }

    @JsonIgnore
    public void setBREAKLAW(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.BREAKLAW> BREAKLAW) {
        this.BREAKLAW = BREAKLAW;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENTCASEBASEINFO> getENTCASEBASEINFO() {
        return ENTCASEBASEINFO;
    }

    @JsonIgnore
    public void setENTCASEBASEINFO(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENTCASEBASEINFO> ENTCASEBASEINFO) {
        this.ENTCASEBASEINFO = ENTCASEBASEINFO;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENTINV> getENTINV() {
        return ENTINV;
    }

    @JsonIgnore
    public void setENTINV(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.ENTINV> ENTINV) {
        this.ENTINV = ENTINV;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.EXCEPTIONLIST> getEXCEPTIONLIST() {
        return EXCEPTIONLIST;
    }

    @JsonIgnore
    public void setEXCEPTIONLIST(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.EXCEPTIONLIST> EXCEPTIONLIST) {
        this.EXCEPTIONLIST = EXCEPTIONLIST;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FILIATION> getFILIATION() {
        return FILIATION;
    }

    @JsonIgnore
    public void setFILIATION(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FILIATION> FILIATION) {
        this.FILIATION = FILIATION;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FINALCASE> getFINALCASE() {
        return FINALCASE;
    }

    @JsonIgnore
    public void setFINALCASE(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FINALCASE> FINALCASE) {
        this.FINALCASE = FINALCASE;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FRINV> getFRINV() {
        return FRINV;
    }

    @JsonIgnore
    public void setFRINV(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FRINV> FRINV) {
        this.FRINV = FRINV;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FRPOSITION> getFRPOSITION() {
        return FRPOSITION;
    }

    @JsonIgnore
    public void setFRPOSITION(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.FRPOSITION> FRPOSITION) {
        this.FRPOSITION = FRPOSITION;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.INSPECT> getINSPECT() {
        return INSPECT;
    }

    @JsonIgnore
    public void setINSPECT(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.INSPECT> INSPECT) {
        this.INSPECT = INSPECT;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.JUDICIALAID> getJUDICIALAID() {
        return JUDICIALAID;
    }

    @JsonIgnore
    public void setJUDICIALAID(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.JUDICIALAID> JUDICIALAID) {
        this.JUDICIALAID = JUDICIALAID;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.JUDICIALAIDALTER> getJUDICIALAIDALTER() {
        return JUDICIALAIDALTER;
    }

    @JsonIgnore
    public void setJUDICIALAIDALTER(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.JUDICIALAIDALTER> JUDICIALAIDALTER) {
        this.JUDICIALAIDALTER = JUDICIALAIDALTER;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.JUDICIALAIDDETAIL> getJUDICIALAIDDETAIL() {
        return JUDICIALAIDDETAIL;
    }

    @JsonIgnore
    public void setJUDICIALAIDDETAIL(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.JUDICIALAIDDETAIL> JUDICIALAIDDETAIL) {
        this.JUDICIALAIDDETAIL = JUDICIALAIDDETAIL;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LIQUIDATION> getLIQUIDATION() {
        return LIQUIDATION;
    }

    @JsonIgnore
    public void setLIQUIDATION(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LIQUIDATION> LIQUIDATION) {
        this.LIQUIDATION = LIQUIDATION;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDCOMPINFO> getLISTEDCOMPINFO() {
        return LISTEDCOMPINFO;
    }

    @JsonIgnore
    public void setLISTEDCOMPINFO(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDCOMPINFO> LISTEDCOMPINFO) {
        this.LISTEDCOMPINFO = LISTEDCOMPINFO;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDINFO> getLISTEDINFO() {
        return LISTEDINFO;
    }

    @JsonIgnore
    public void setLISTEDINFO(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDINFO> LISTEDINFO) {
        this.LISTEDINFO = LISTEDINFO;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDMANAGER> getLISTEDMANAGER() {
        return LISTEDMANAGER;
    }

    @JsonIgnore
    public void setLISTEDMANAGER(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDMANAGER> LISTEDMANAGER) {
        this.LISTEDMANAGER = LISTEDMANAGER;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDSHAREHOLDER> getLISTEDSHAREHOLDER() {
        return LISTEDSHAREHOLDER;
    }

    @JsonIgnore
    public void setLISTEDSHAREHOLDER(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.LISTEDSHAREHOLDER> LISTEDSHAREHOLDER) {
        this.LISTEDSHAREHOLDER = LISTEDSHAREHOLDER;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEALT> getMORTGAGEALT() {
        return MORTGAGEALT;
    }

    @JsonIgnore
    public void setMORTGAGEALT(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEALT> MORTGAGEALT) {
        this.MORTGAGEALT = MORTGAGEALT;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEBASIC> getMORTGAGEBASIC() {
        return MORTGAGEBASIC;
    }

    @JsonIgnore
    public void setMORTGAGEBASIC(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEBASIC> MORTGAGEBASIC) {
        this.MORTGAGEBASIC = MORTGAGEBASIC;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGECAN> getMORTGAGECAN() {
        return MORTGAGECAN;
    }

    @JsonIgnore
    public void setMORTGAGECAN(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGECAN> MORTGAGECAN) {
        this.MORTGAGECAN = MORTGAGECAN;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEDEBT> getMORTGAGEDEBT() {
        return MORTGAGEDEBT;
    }

    @JsonIgnore
    public void setMORTGAGEDEBT(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEDEBT> MORTGAGEDEBT) {
        this.MORTGAGEDEBT = MORTGAGEDEBT;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEPAWN> getMORTGAGEPAWN() {
        return MORTGAGEPAWN;
    }

    @JsonIgnore
    public void setMORTGAGEPAWN(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEPAWN> MORTGAGEPAWN) {
        this.MORTGAGEPAWN = MORTGAGEPAWN;
    }

    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEPER> getMORTGAGEPER() {
        return MORTGAGEPER;
    }

    @JsonIgnore
    public void setMORTGAGEPER(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEPER> MORTGAGEPER) {
        this.MORTGAGEPER = MORTGAGEPER;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEREG> getMORTGAGEREG() {
        return MORTGAGEREG;
    }

    @JsonIgnore
    public void setMORTGAGEREG(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.MORTGAGEREG> MORTGAGEREG) {
        this.MORTGAGEREG = MORTGAGEREG;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.PERSON> getPERSON() {
        return PERSON;
    }

    @JsonIgnore
    public void setPERSON(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.PERSON> PERSON) {
        this.PERSON = PERSON;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.PUNISHBREAK> getPUNISHBREAK() {
        return PUNISHBREAK;
    }

    @JsonIgnore
    public void setPUNISHBREAK(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.PUNISHBREAK> PUNISHBREAK) {
        this.PUNISHBREAK = PUNISHBREAK;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.PUNISHED> getPUNISHED() {
        return PUNISHED;
    }

    @JsonIgnore
    public void setPUNISHED(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.PUNISHED> PUNISHED) {
        this.PUNISHED = PUNISHED;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.QUICKCANCELBASIC> getQUICKCANCELBASIC() {
        return QUICKCANCELBASIC;
    }

    @JsonIgnore
    public void setQUICKCANCELBASIC(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.QUICKCANCELBASIC> QUICKCANCELBASIC) {
        this.QUICKCANCELBASIC = QUICKCANCELBASIC;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.QUICKCANCELDISSENT> getQUICKCANCELDISSENT() {
        return QUICKCANCELDISSENT;
    }

    @JsonIgnore
    public void setQUICKCANCELDISSENT(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.QUICKCANCELDISSENT> QUICKCANCELDISSENT) {
        this.QUICKCANCELDISSENT = QUICKCANCELDISSENT;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.RELATEDPUNISHBREAK> getRELATEDPUNISHBREAK() {
        return RELATEDPUNISHBREAK;
    }

    @JsonIgnore
    public void setRELATEDPUNISHBREAK(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.RELATEDPUNISHBREAK> RELATEDPUNISHBREAK) {
        this.RELATEDPUNISHBREAK = RELATEDPUNISHBREAK;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.RELATEDPUNISHED> getRELATEDPUNISHED() {
        return RELATEDPUNISHED;
    }

    @JsonIgnore
    public void setRELATEDPUNISHED(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.RELATEDPUNISHED> RELATEDPUNISHED) {
        this.RELATEDPUNISHED = RELATEDPUNISHED;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.SHAREHOLDER> getSHAREHOLDER() {
        return SHAREHOLDER;
    }

    @JsonIgnore
    public void setSHAREHOLDER(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.SHAREHOLDER> SHAREHOLDER) {
        this.SHAREHOLDER = SHAREHOLDER;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.STOCKPAWN> getSTOCKPAWN() {
        return STOCKPAWN;
    }

    @JsonIgnore
    public void setSTOCKPAWN(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.STOCKPAWN> STOCKPAWN) {
        this.STOCKPAWN = STOCKPAWN;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.STOCKPAWNALT> getSTOCKPAWNALT() {
        return STOCKPAWNALT;
    }

    @JsonIgnore
    public void setSTOCKPAWNALT(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.STOCKPAWNALT> STOCKPAWNALT) {
        this.STOCKPAWNALT = STOCKPAWNALT;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.STOCKPAWNREV> getSTOCKPAWNREV() {
        return STOCKPAWNREV;
    }

    @JsonIgnore
    public void setSTOCKPAWNREV(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.STOCKPAWNREV> STOCKPAWNREV) {
        this.STOCKPAWNREV = STOCKPAWNREV;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTALTER> getYEARREPORTALTER() {
        return YEARREPORTALTER;
    }

    @JsonIgnore
    public void setYEARREPORTALTER(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTALTER> YEARREPORTALTER) {
        this.YEARREPORTALTER = YEARREPORTALTER;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTALTERSTOCK> getYEARREPORTALTERSTOCK() {
        return YEARREPORTALTERSTOCK;
    }

    @JsonIgnore
    public void setYEARREPORTALTERSTOCK(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTALTERSTOCK> YEARREPORTALTERSTOCK) {
        this.YEARREPORTALTERSTOCK = YEARREPORTALTERSTOCK;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTBASIC> getYEARREPORTBASIC() {
        return YEARREPORTBASIC;
    }

    @JsonIgnore
    public void setYEARREPORTBASIC(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTBASIC> YEARREPORTBASIC) {
        this.YEARREPORTBASIC = YEARREPORTBASIC;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTFORINV> getYEARREPORTFORINV() {
        return YEARREPORTFORINV;
    }

    @JsonIgnore
    public void setYEARREPORTFORINV(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTFORINV> YEARREPORTFORINV) {
        this.YEARREPORTFORINV = YEARREPORTFORINV;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTPAIDUPCAPITAL> getYEARREPORTPAIDUPCAPITAL() {
        return YEARREPORTPAIDUPCAPITAL;
    }

    @JsonIgnore
    public void setYEARREPORTPAIDUPCAPITAL(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTPAIDUPCAPITAL> YEARREPORTPAIDUPCAPITAL) {
        this.YEARREPORTPAIDUPCAPITAL = YEARREPORTPAIDUPCAPITAL;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTSOCSEC> getYEARREPORTSOCSEC() {
        return YEARREPORTSOCSEC;
    }

    @JsonIgnore
    public void setYEARREPORTSOCSEC(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTSOCSEC> YEARREPORTSOCSEC) {
        this.YEARREPORTSOCSEC = YEARREPORTSOCSEC;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTSUBCAPITAL> getYEARREPORTSUBCAPITAL() {
        return YEARREPORTSUBCAPITAL;
    }

    @JsonIgnore
    public void setYEARREPORTSUBCAPITAL(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTSUBCAPITAL> YEARREPORTSUBCAPITAL) {
        this.YEARREPORTSUBCAPITAL = YEARREPORTSUBCAPITAL;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTWEBSITEINFO> getYEARREPORTWEBSITEINFO() {
        return YEARREPORTWEBSITEINFO;
    }

    @JsonIgnore
    public void setYEARREPORTWEBSITEINFO(List<cn.com.yusys.yusp.online.client.http.outerdata.zsnew.YEARREPORTWEBSITEINFO> YEARREPORTWEBSITEINFO) {
        this.YEARREPORTWEBSITEINFO = YEARREPORTWEBSITEINFO;
    }

    @Override
    public String toString() {
        return "ENT_INFO{" +
                "ALTER=" + ALTER +
                ", BASIC=" + BASIC +
                ", BASICLIST=" + BASICLIST +
                ", BREAKLAW=" + BREAKLAW +
                ", ENTCASEBASEINFO=" + ENTCASEBASEINFO +
                ", ENTINV=" + ENTINV +
                ", EXCEPTIONLIST=" + EXCEPTIONLIST +
                ", FILIATION=" + FILIATION +
                ", FINALCASE=" + FINALCASE +
                ", FRINV=" + FRINV +
                ", FRPOSITION=" + FRPOSITION +
                ", INSPECT=" + INSPECT +
                ", JUDICIALAID=" + JUDICIALAID +
                ", JUDICIALAIDALTER=" + JUDICIALAIDALTER +
                ", JUDICIALAIDDETAIL=" + JUDICIALAIDDETAIL +
                ", LIQUIDATION=" + LIQUIDATION +
                ", LISTEDCOMPINFO=" + LISTEDCOMPINFO +
                ", LISTEDINFO=" + LISTEDINFO +
                ", LISTEDMANAGER=" + LISTEDMANAGER +
                ", LISTEDSHAREHOLDER=" + LISTEDSHAREHOLDER +
                ", MORTGAGEALT=" + MORTGAGEALT +
                ", MORTGAGEBASIC=" + MORTGAGEBASIC +
                ", MORTGAGECAN=" + MORTGAGECAN +
                ", MORTGAGEDEBT=" + MORTGAGEDEBT +
                ", MORTGAGEPAWN=" + MORTGAGEPAWN +
                ", MORTGAGEPER=" + MORTGAGEPER +
                ", MORTGAGEREG=" + MORTGAGEREG +
                ", PERSON=" + PERSON +
                ", PUNISHBREAK=" + PUNISHBREAK +
                ", PUNISHED=" + PUNISHED +
                ", QUICKCANCELBASIC=" + QUICKCANCELBASIC +
                ", QUICKCANCELDISSENT=" + QUICKCANCELDISSENT +
                ", RELATEDPUNISHBREAK=" + RELATEDPUNISHBREAK +
                ", RELATEDPUNISHED=" + RELATEDPUNISHED +
                ", SHAREHOLDER=" + SHAREHOLDER +
                ", STOCKPAWN=" + STOCKPAWN +
                ", STOCKPAWNALT=" + STOCKPAWNALT +
                ", STOCKPAWNREV=" + STOCKPAWNREV +
                ", YEARREPORTALTER=" + YEARREPORTALTER +
                ", YEARREPORTALTERSTOCK=" + YEARREPORTALTERSTOCK +
                ", YEARREPORTBASIC=" + YEARREPORTBASIC +
                ", YEARREPORTFORINV=" + YEARREPORTFORINV +
                ", YEARREPORTPAIDUPCAPITAL=" + YEARREPORTPAIDUPCAPITAL +
                ", YEARREPORTSOCSEC=" + YEARREPORTSOCSEC +
                ", YEARREPORTSUBCAPITAL=" + YEARREPORTSUBCAPITAL +
                ", YEARREPORTWEBSITEINFO=" + YEARREPORTWEBSITEINFO +
                '}';
    }
}
