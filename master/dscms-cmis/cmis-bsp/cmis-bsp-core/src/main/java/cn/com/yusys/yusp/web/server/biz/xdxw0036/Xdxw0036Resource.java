package cn.com.yusys.yusp.web.server.biz.xdxw0036;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0036.req.Xdxw0036ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0036.resp.Xdxw0036RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0036.req.Xdxw0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0036.resp.Xdxw0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询优抵贷调查结论
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0036:查询优抵贷调查结论")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0036Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0036
     * 交易描述：查询优抵贷调查结论
     *
     * @param xdxw0036ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优抵贷调查结论")
    @PostMapping("/xdxw0036")
    //@Idempotent({"xdcaxw0036", "#xdxw0036ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0036RespDto xdxw0036(@Validated @RequestBody Xdxw0036ReqDto xdxw0036ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, JSON.toJSONString(xdxw0036ReqDto));
        Xdxw0036DataReqDto xdxw0036DataReqDto = new Xdxw0036DataReqDto();// 请求Data： 查询优抵贷调查结论
        Xdxw0036DataRespDto xdxw0036DataRespDto = new Xdxw0036DataRespDto();// 响应Data：查询优抵贷调查结论
        Xdxw0036RespDto xdxw0036RespDto = new Xdxw0036RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0036.req.Data reqData = null; // 请求Data：查询优抵贷调查结论
        cn.com.yusys.yusp.dto.server.biz.xdxw0036.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0036.resp.Data();// 响应Data：查询优抵贷调查结论
        try {
            // 从 xdxw0036ReqDto获取 reqData
            reqData = xdxw0036ReqDto.getData();
            // 将 reqData 拷贝到xdxw0036DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0036DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, JSON.toJSONString(xdxw0036DataReqDto));
            ResultDto<Xdxw0036DataRespDto> xdxw0036DataResultDto = dscmsBizXwClientService.xdxw0036(xdxw0036DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, JSON.toJSONString(xdxw0036DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0036RespDto.setErorcd(Optional.ofNullable(xdxw0036DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0036RespDto.setErortx(Optional.ofNullable(xdxw0036DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0036DataResultDto.getCode())) {
                xdxw0036DataRespDto = xdxw0036DataResultDto.getData();
                xdxw0036RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0036RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0036DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0036DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, e.getMessage());
            xdxw0036RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0036RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0036RespDto.setDatasq(servsq);

        xdxw0036RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0036.key, DscmsEnum.TRADE_CODE_XDXW0036.value, JSON.toJSONString(xdxw0036RespDto));
        return xdxw0036RespDto;
    }
}
