package cn.com.yusys.yusp.web.server.biz.xdca0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdca0004.req.Xdca0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdca0004.resp.Xdca0004RespDto;
import cn.com.yusys.yusp.dto.server.xdca0004.req.Xdca0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0004.resp.Xdca0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCaClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:大额分期合同详情查询
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDCA0004:大额分期合同详情查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdca0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0004Resource.class);

	@Autowired
	private DscmsBizCaClientService dscmsBizCaClientService;

    /**
     * 交易码：xdca0004
     * 交易描述：大额分期合同详情查询
     *
     * @param xdca0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("大额分期合同详情查询")
    @PostMapping("/xdca0004")
//   @Idempotent({"xdcaca0004", "#xdca0004ReqDto.datasq"})
    protected @ResponseBody
	Xdca0004RespDto xdca0004(@Validated @RequestBody Xdca0004ReqDto xdca0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004ReqDto));
        Xdca0004DataReqDto xdca0004DataReqDto = new Xdca0004DataReqDto();// 请求Data： 大额分期合同详情查询
        Xdca0004DataRespDto xdca0004DataRespDto = new Xdca0004DataRespDto();// 响应Data：大额分期合同详情查询
		Xdca0004RespDto xdca0004RespDto = new Xdca0004RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdca0004.req.Data reqData = null; // 请求Data：大额分期合同详情查询
		cn.com.yusys.yusp.dto.server.biz.xdca0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdca0004.resp.Data();// 响应Data：大额分期合同详情查询
        //  此处包导入待确定 结束
        try {
            // 从 xdca0004ReqDto获取 reqData
            reqData = xdca0004ReqDto.getData();
            // 将 reqData 拷贝到xdca0004DataReqDto
            BeanUtils.copyProperties(reqData, xdca0004DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004DataReqDto));
            ResultDto<Xdca0004DataRespDto> xdca0004DataResultDto = dscmsBizCaClientService.xdca0004(xdca0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdca0004RespDto.setErorcd(Optional.ofNullable(xdca0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdca0004RespDto.setErortx(Optional.ofNullable(xdca0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdca0004DataResultDto.getCode())) {
                xdca0004DataRespDto = xdca0004DataResultDto.getData();
                xdca0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdca0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdca0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdca0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, e.getMessage());
            xdca0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdca0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdca0004RespDto.setDatasq(servsq);

        xdca0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0004.key, DscmsEnum.TRADE_CODE_XDCA0004.value, JSON.toJSONString(xdca0004RespDto));
        return xdca0004RespDto;
    }
}
