package cn.com.yusys.yusp.web.server.biz.xdxw0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0016.req.Xdxw0016ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0016.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0016.resp.Xdxw0016RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0016.req.Xdxw0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0016.resp.Xdxw0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号查询查询统一管控额度接口（总额度）
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0016:根据客户号查询查询统一管控额度接口（总额度）")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0016Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0016
     * 交易描述：根据客户号查询查询统一管控额度接口（总额度）
     *
     * @param xdxw0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询查询统一管控额度接口（总额度）")
    @PostMapping("/xdxw0016")
    @Idempotent({"xdcaxw0016", "#xdxw0016ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0016RespDto xdxw0016(@Validated @RequestBody Xdxw0016ReqDto xdxw0016ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016ReqDto));
        Xdxw0016DataReqDto xdxw0016DataReqDto = new Xdxw0016DataReqDto();// 请求Data： 根据客户号查询查询统一管控额度接口（总额度）
        Xdxw0016DataRespDto xdxw0016DataRespDto = new Xdxw0016DataRespDto();// 响应Data：根据客户号查询查询统一管控额度接口（总额度）
        cn.com.yusys.yusp.dto.server.biz.xdxw0016.req.Data reqData = null; // 请求Data：根据客户号查询查询统一管控额度接口（总额度）
        cn.com.yusys.yusp.dto.server.biz.xdxw0016.resp.Data respData = new Data();// 响应Data：根据客户号查询查询统一管控额度接口（总额度）
		Xdxw0016RespDto xdxw0016RespDto = new Xdxw0016RespDto();
		try {
            // 从 xdxw0016ReqDto获取 reqData
            reqData = xdxw0016ReqDto.getData();
            // 将 reqData 拷贝到xdxw0016DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0016DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016DataReqDto));
            ResultDto<Xdxw0016DataRespDto> xdxw0016DataResultDto = dscmsBizXwClientService.xdxw0016(xdxw0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0016RespDto.setErorcd(Optional.ofNullable(xdxw0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0016RespDto.setErortx(Optional.ofNullable(xdxw0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0016DataResultDto.getCode())) {
                xdxw0016DataRespDto = xdxw0016DataResultDto.getData();
                xdxw0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0016DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, e.getMessage());
            xdxw0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0016RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0016RespDto.setDatasq(servsq);

        xdxw0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0016.key, DscmsEnum.TRADE_CODE_XDXW0016.value, JSON.toJSONString(xdxw0016RespDto));
        return xdxw0016RespDto;
    }
}
