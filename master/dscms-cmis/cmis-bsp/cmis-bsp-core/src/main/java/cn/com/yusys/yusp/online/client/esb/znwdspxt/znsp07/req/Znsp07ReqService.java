package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req;

/**
 * 请求Service：数据修改接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Znsp07ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

