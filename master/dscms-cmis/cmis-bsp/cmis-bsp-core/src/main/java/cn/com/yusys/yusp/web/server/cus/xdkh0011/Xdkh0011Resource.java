package cn.com.yusys.yusp.web.server.cus.xdkh0011;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0011.req.Xdkh0011ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0011.resp.Xdkh0011RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0011.req.Xdkh0011DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0011.resp.Xdkh0011DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:对私客户信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0011:对私客户信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0011Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0011Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0011
     * 交易描述：对私客户信息同步
     *
     * @param xdkh0011ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对私客户信息同步")
    @PostMapping("/xdkh0011")
    //@Idempotent({"xdcakh0011", "#xdkh0011ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0011RespDto xdkh0011(@Validated @RequestBody Xdkh0011ReqDto xdkh0011ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011ReqDto));
        Xdkh0011DataReqDto xdkh0011DataReqDto = new Xdkh0011DataReqDto();// 请求Data： 对私客户信息同步
        Xdkh0011DataRespDto xdkh0011DataRespDto = new Xdkh0011DataRespDto();// 响应Data：对私客户信息同步
        Xdkh0011RespDto xdkh0011RespDto = new Xdkh0011RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0011.req.Data reqData = null; // 请求Data：对私客户信息同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0011.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0011.resp.Data();// 响应Data：对私客户信息同步

        try {
            // 从 xdkh0011ReqDto获取 reqData
            reqData = xdkh0011ReqDto.getData();
            // 将 reqData 拷贝到xdkh0011DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0011DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataReqDto));
            ResultDto<Xdkh0011DataRespDto> xdkh0011DataResultDto = dscmsCusClientService.xdkh0011(xdkh0011DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0011RespDto.setErorcd(Optional.ofNullable(xdkh0011DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0011RespDto.setErortx(Optional.ofNullable(xdkh0011DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0011DataResultDto.getCode())) {
                xdkh0011DataRespDto = xdkh0011DataResultDto.getData();
                xdkh0011RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0011RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0011DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0011DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, e.getMessage());
            xdkh0011RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0011RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0011RespDto.setDatasq(servsq);

        xdkh0011RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0011.key, DscmsEnum.TRADE_CODE_XDKH0011.value, JSON.toJSONString(xdkh0011RespDto));
        return xdkh0011RespDto;
    }
}