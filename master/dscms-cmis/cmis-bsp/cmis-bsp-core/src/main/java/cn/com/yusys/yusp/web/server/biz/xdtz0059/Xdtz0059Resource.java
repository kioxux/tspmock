package cn.com.yusys.yusp.web.server.biz.xdtz0059;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0059.req.Xdtz0059ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0059.resp.Xdtz0059RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0059.req.Xdtz0059DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0059.resp.Xdtz0059DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsBizCzEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:更新信贷台账信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0059:更新信贷台账信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0059Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0059Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0059
     * 交易描述：更新信贷台账信息
     *
     * @param xdtz0059ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("更新信贷台账信息")
    @PostMapping("/xdtz0059")
    //@Idempotent({"xdtz0059", "#xdtz0059ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0059RespDto xdtz0059(@Validated @RequestBody Xdtz0059ReqDto xdtz0059ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0054.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059ReqDto));
        Xdtz0059DataReqDto xdtz0059DataReqDto = new Xdtz0059DataReqDto();// 请求Data： 更新信贷台账信息
        Xdtz0059DataRespDto xdtz0059DataRespDto = new Xdtz0059DataRespDto();// 响应Data：更新信贷台账信息
        Xdtz0059RespDto xdtz0059RespDto = new Xdtz0059RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0059.req.Data reqData = null; // 请求Data：更新信贷台账信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0059.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0059.resp.Data();// 响应Data：更新信贷台账信息
        try {
            // 从 xdtz0059ReqDto获取 reqData
            reqData = xdtz0059ReqDto.getData();
            // 将 reqData 拷贝到xdtz0059DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0059DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataReqDto));
            ResultDto<Xdtz0059DataRespDto> xdtz0059DataResultDto = dscmsBizTzClientService.xdtz0059(xdtz0059DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0059RespDto.setErorcd(Optional.ofNullable(xdtz0059DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0059RespDto.setErortx(Optional.ofNullable(xdtz0059DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0059DataResultDto.getCode())) {
                xdtz0059DataRespDto = xdtz0059DataResultDto.getData();
                if(Objects.equals(DscmsBizCzEnum.OP_FLAG_F.value,xdtz0059DataRespDto.getOpFlag())){
                    xdtz0059RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                    xdtz0059RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
                }else{
                    xdtz0059RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                    xdtz0059RespDto.setErortx(SuccessEnum.SUCCESS.value);
                }

                // 将 xdtz0059DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0059DataRespDto, respData);
            }else{
                xdtz0059DataRespDto = xdtz0059DataResultDto.getData();
                xdtz0059RespDto.setErorcd(EpbEnum.EPB099999.key);
                xdtz0059RespDto.setErortx(xdtz0059DataRespDto.getOpMsg());
                // 将 xdtz0059DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0059DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, e.getMessage());
            xdtz0059RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0059RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0059RespDto.setDatasq(servsq);

        xdtz0059RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0059.key, DscmsEnum.TRADE_CODE_XDTZ0059.value, JSON.toJSONString(xdtz0059RespDto));
        return xdtz0059RespDto;
    }
}
