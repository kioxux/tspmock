package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:分项额度信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日15:10:55
 */
public class LimitDetailsInfo {
    private List<LimitDetailsInfoRecord> record; // 分项额度信息

    public List<LimitDetailsInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LimitDetailsInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LimitDetailsInfo{" +
                "record=" + record +
                '}';
    }
}