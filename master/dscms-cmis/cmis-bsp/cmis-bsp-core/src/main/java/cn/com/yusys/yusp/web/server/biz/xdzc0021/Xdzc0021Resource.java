package cn.com.yusys.yusp.web.server.biz.xdzc0021;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0021.req.Xdzc0021ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0021.resp.Xdzc0021RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0021.req.Xdzc0021DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0021.resp.Xdzc0021DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:融资汇总查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0021:融资汇总查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0021Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0021Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0021
     * 交易描述：融资汇总查询
     *
     * @param xdzc0021ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("融资汇总查询")
    @PostMapping("/xdzc0021")
    //@Idempotent({"xdzc021", "#xdzc0021ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0021RespDto xdzc0021(@Validated @RequestBody Xdzc0021ReqDto xdzc0021ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, JSON.toJSONString(xdzc0021ReqDto));
        Xdzc0021DataReqDto xdzc0021DataReqDto = new Xdzc0021DataReqDto();// 请求Data： 融资汇总查询
        Xdzc0021DataRespDto xdzc0021DataRespDto = new Xdzc0021DataRespDto();// 响应Data：融资汇总查询
        Xdzc0021RespDto xdzc0021RespDto = new Xdzc0021RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0021.req.Data reqData = null; // 请求Data：融资汇总查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0021.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0021.resp.Data();// 响应Data：融资汇总查询

        try {
            // 从 xdzc0021ReqDto获取 reqData
            reqData = xdzc0021ReqDto.getData();
            // 将 reqData 拷贝到xdzc0021DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0021DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, JSON.toJSONString(xdzc0021DataReqDto));
            ResultDto<Xdzc0021DataRespDto> xdzc0021DataResultDto = dscmsBizZcClientService.xdzc0021(xdzc0021DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, JSON.toJSONString(xdzc0021DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0021RespDto.setErorcd(Optional.ofNullable(xdzc0021DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0021RespDto.setErortx(Optional.ofNullable(xdzc0021DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0021DataResultDto.getCode())) {
                xdzc0021DataRespDto = xdzc0021DataResultDto.getData();
                xdzc0021RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0021RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0021DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0021DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, e.getMessage());
            xdzc0021RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0021RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0021RespDto.setDatasq(servsq);

        xdzc0021RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0021.key, DscmsEnum.TRADE_CODE_XDZC0021.value, JSON.toJSONString(xdzc0021RespDto));
        return xdzc0021RespDto;
    }
}
