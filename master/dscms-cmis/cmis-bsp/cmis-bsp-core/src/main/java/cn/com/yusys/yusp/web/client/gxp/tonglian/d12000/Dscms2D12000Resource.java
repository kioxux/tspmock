package cn.com.yusys.yusp.web.client.gxp.tonglian.d12000;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.req.D12000ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.resp.D12000RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.resp.D12000RespMessage;
import cn.com.yusys.yusp.util.GenericBuilder;
import cn.com.yusys.yusp.util.GxpBuilder;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用通联系统的接口
 **/
@Api(tags = "BSP封装调用通联系统的接口处理类(d12000)")
@RestController
@RequestMapping("/api/dscms2tonglian")
public class Dscms2D12000Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2D12000Resource.class);

    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 账户信息查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("d12000:账户信息查询")
    @PostMapping("/d12000")
    protected @ResponseBody
    ResultDto<D12000RespDto> d12000(@Validated @RequestBody D12000ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12000.key, GxpEnum.TRADE_CODE_D12000.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.D12000ReqMessage d12000ReqMessage = null;//请求Message：现金（大额）放款接口
        D12000RespMessage d12000RespMessage = new D12000RespMessage();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.Message reqMessage = null;//请求Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.resp.Message respMessage = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.resp.Message();//响应Message：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.req.GxpReqHead gxpReqHead = null;//请求Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead gxpRespHead = new cn.com.yusys.yusp.online.client.gxp.common.resp.GxpRespHead();//响应Head：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.Body reqBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.Body();//请求Body：现金（大额）放款接口
        cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.resp.Body respBody = new cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.resp.Body();//响应Body：现金（大额）放款接口

        D12000RespDto d12000RespDto = new D12000RespDto();//响应Dto：现金（大额）放款接口
        ResultDto<D12000RespDto> d12000ResultDto = new ResultDto<>();//响应ResultDto：现金（大额）放款接口

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            // 组装reqHead
            gxpReqHead = GxpBuilder.gxpReqHeadBuilder(GxpEnum.TRADE_CODE_D12000.key, GxpEnum.TRADE_CODE_D12000.value, GxpEnum.SERVTP_XDG.key, GxpEnum.SERVTP_XDG.value, GxpEnum.USERID_TONGLIAN.key, GxpEnum.BRCHNO_TONGLIAN.key);

            //  将D12000ReqDto转换成reqBody
            BeanUtils.copyProperties(reqDto, reqBody);
            // 给reqMessage赋值
            reqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.Message::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.Message::setHead, gxpReqHead).with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.Message::setBody, reqBody).build();
            // 给d12000ReqMessage赋值
            d12000ReqMessage = GenericBuilder.of(cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.D12000ReqMessage::new)
                    .with(cn.com.yusys.yusp.online.client.gxp.tonglian.d12000.req.D12000ReqMessage::setMessage, reqMessage).build();

            // 将d12000ReqService转换成d12000ReqServiceMap
            Map d12000ReqMessageMap = beanMapUtil.beanToMap(d12000ReqMessage);
            context.put("tradeDataMap", d12000ReqMessageMap);
            logger.info(TradeLogConstants.CALL_GXP_BEGIN_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12000.key, GxpEnum.TRADE_CODE_D12000.value);
            result = BspTemplate.exchange(GxpEnum.SERVICE_NAME_GXP_TRADE_CLIENT.key, GxpEnum.TRADE_CODE_D12000.key, context);
            logger.info(TradeLogConstants.CALL_GXP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12000.key, GxpEnum.TRADE_CODE_D12000.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            d12000RespMessage = beanMapUtil.mapToBean(tradeDataMap, D12000RespMessage.class, D12000RespMessage.class);//响应Message：现金（大额）放款接口
            respMessage = d12000RespMessage.getMessage();//响应Message：现金（大额）放款接口
            gxpRespHead = respMessage.getHead();//响应Head：现金（大额）放款接口
            //  将D12000RespDto封装到ResultDto<D12000RespDto>
            //  && Objects.equals(GxpEnum.TRANSTATE_S.key, gxpRespHead.getTranstate()) 先不设置
            if (Objects.equals(SuccessEnum.SUCCESS.key, gxpRespHead.getRetrcd())) {
                respBody = respMessage.getBody();
                //  将respBody转换成D12000RespDto
                BeanUtils.copyProperties(respBody, d12000RespDto);
                d12000ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                d12000ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                d12000ResultDto.setCode(EpbEnum.EPB099999.key);
                d12000ResultDto.setMessage(gxpRespHead.getErortx());
            }
            d12000ResultDto.setMessage(Optional.ofNullable(gxpRespHead.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12000.key, GxpEnum.TRADE_CODE_D12000.value, e.getMessage());
            d12000ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            d12000ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        d12000ResultDto.setData(d12000RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, GxpEnum.TRADE_CODE_D12000.key, GxpEnum.TRADE_CODE_D12000.value, JSON.toJSONString(d12000ResultDto));
        return d12000ResultDto;
    }
}
