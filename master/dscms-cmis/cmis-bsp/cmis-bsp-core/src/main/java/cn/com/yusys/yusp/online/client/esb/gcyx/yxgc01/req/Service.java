package cn.com.yusys.yusp.online.client.esb.gcyx.yxgc01.req;

/**
 * 请求Service：贷后任务推送接口
 *
 * @author wrw
 * @version 1.0
 * @since 2021/8/2 10:50
 */
public class Service {

    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String serno;// 流水号 否
    private String cus_id;// 客户号 否
    private String cus_name;// 客户名称 否
    private String phone;// 电话号码 否
    private String address;// 地址 否
    private String cus_mng_id;
    private String cus_mng_name;
    private String mng_br_id;
    private String mng_br_name;
    private String crt_date;
    private String rqr_fin_date;
    private String psp_state;// 贷后任务操作标记 否  1-信贷发起不定期  2-校验>30天重新发送 3-流程结束 4-任务延期，5-贷后任务移交 6-信贷发起微业贷  7-信贷退回

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCus_mng_id() {
        return cus_mng_id;
    }

    public void setCus_mng_id(String cus_mng_id) {
        this.cus_mng_id = cus_mng_id;
    }

    public String getCus_mng_name() {
        return cus_mng_name;
    }

    public void setCus_mng_name(String cus_mng_name) {
        this.cus_mng_name = cus_mng_name;
    }

    public String getMng_br_id() {
        return mng_br_id;
    }

    public void setMng_br_id(String mng_br_id) {
        this.mng_br_id = mng_br_id;
    }

    public String getMng_br_name() {
        return mng_br_name;
    }

    public void setMng_br_name(String mng_br_name) {
        this.mng_br_name = mng_br_name;
    }

    public String getCrt_date() {
        return crt_date;
    }

    public void setCrt_date(String crt_date) {
        this.crt_date = crt_date;
    }

    public String getRqr_fin_date() {
        return rqr_fin_date;
    }

    public void setRqr_fin_date(String rqr_fin_date) {
        this.rqr_fin_date = rqr_fin_date;
    }

    public String getPsp_state() {
        return psp_state;
    }

    public void setPsp_state(String psp_state) {
        this.psp_state = psp_state;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", serno='" + serno + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", cus_mng_id='" + cus_mng_id + '\'' +
                ", cus_mng_name='" + cus_mng_name + '\'' +
                ", mng_br_id='" + mng_br_id + '\'' +
                ", mng_br_name='" + mng_br_name + '\'' +
                ", crt_date='" + crt_date + '\'' +
                ", rqr_fin_date='" + rqr_fin_date + '\'' +
                ", psp_state='" + psp_state + '\'' +
                '}';
    }
}
