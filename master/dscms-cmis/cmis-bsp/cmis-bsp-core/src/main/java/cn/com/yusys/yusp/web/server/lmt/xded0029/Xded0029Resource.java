package cn.com.yusys.yusp.web.server.lmt.xded0029;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.req.CmisLmt0029ReqDto;
import cn.com.yusys.yusp.dto.server.cmisLmt0029.resp.CmisLmt0029RespDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0029.req.Xded0029ReqDto;
import cn.com.yusys.yusp.dto.server.lmt.xded0029.resp.Xded0029RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.DscmsLmtEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.CmisLmtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:更新台账编号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDED0029:更新台账编号")
@RestController
@RequestMapping("/api/dscms")
public class Xded0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xded0029Resource.class);
    @Autowired
    private CmisLmtClientService cmisLmtClientService;

    /**
     * 交易码：cmisLmt0029
     * 交易描述：更新台账编号
     *
     * @param xded0029ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("更新台账编号")
    @PostMapping("/xded0029")
    //  @Idempotent({"xded0029", "#xded0029ReqDto.datasq"})
    protected @ResponseBody
    Xded0029RespDto cmisLmt0029(@Validated @RequestBody Xded0029ReqDto xded0029ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0029.key, DscmsEnum.TRADE_CODE_XDED0029.value, JSON.toJSONString(xded0029ReqDto));
        CmisLmt0029ReqDto cmisLmt0029ReqDto = new CmisLmt0029ReqDto();// 请求Data： 更新台账编号
        CmisLmt0029RespDto cmisLmt0029RespDto = new CmisLmt0029RespDto();// 响应Data：更新台账编号

        Xded0029RespDto xded0029RespDto = new Xded0029RespDto();

        cn.com.yusys.yusp.dto.server.lmt.xded0029.req.Data reqData = null; // 请求Data：更新台账编号
        cn.com.yusys.yusp.dto.server.lmt.xded0029.resp.Data respData = new cn.com.yusys.yusp.dto.server.lmt.xded0029.resp.Data();// 响应Data：更新台账编号

        try {
            // 从 Xded0029Dto获取 reqData
            reqData = xded0029ReqDto.getData();
            // 将 reqData 拷贝到cmisLmt0029DataReqDto
            BeanUtils.copyProperties(reqData, cmisLmt0029ReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, JSON.toJSONString(cmisLmt0029ReqDto));
            ResultDto<CmisLmt0029RespDto> cmisLmt0029RespDtoResultDto = cmisLmtClientService.cmislmt0029(cmisLmt0029ReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.key, DscmsLmtEnum.TRADE_CODE_CMISLMT0029.value, JSON.toJSONString(cmisLmt0029RespDtoResultDto));
            // 从返回值中获取响应码和响应消息
            xded0029RespDto.setErorcd(Optional.ofNullable(cmisLmt0029RespDtoResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xded0029RespDto.setErortx(Optional.ofNullable(cmisLmt0029RespDtoResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisLmt0029RespDtoResultDto.getCode())) {
                cmisLmt0029RespDto = cmisLmt0029RespDtoResultDto.getData();
                xded0029RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xded0029RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 cmisLmt0029DataRespDto拷贝到 respData
                BeanUtils.copyProperties(cmisLmt0029RespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0029.key, DscmsEnum.TRADE_CODE_XDED0029.value, e.getMessage());
            xded0029RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xded0029RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xded0029RespDto.setDatasq(servsq);

        xded0029RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDED0029.key, DscmsEnum.TRADE_CODE_XDED0029.value, JSON.toJSONString(xded0029RespDto));
        return xded0029RespDto;
    }
}
