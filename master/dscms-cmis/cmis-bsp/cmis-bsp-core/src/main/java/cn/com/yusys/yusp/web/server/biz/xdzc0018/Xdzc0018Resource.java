package cn.com.yusys.yusp.web.server.biz.xdzc0018;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0018.req.Xdzc0018ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0018.resp.Xdzc0018RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0018.req.Xdzc0018DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0018.resp.Xdzc0018DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:我的资产统计查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0018:我的资产统计查询接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0018Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0018Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0018
     * 交易描述：我的资产统计查询接口
     *
     * @param xdzc0018ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("我的资产统计查询接口")
    @PostMapping("/xdzc0018")
    //@Idempotent({"xdzc018", "#xdzc0018ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0018RespDto xdzc0018(@Validated @RequestBody Xdzc0018ReqDto xdzc0018ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, JSON.toJSONString(xdzc0018ReqDto));
        Xdzc0018DataReqDto xdzc0018DataReqDto = new Xdzc0018DataReqDto();// 请求Data： 我的资产统计查询接口
        Xdzc0018DataRespDto xdzc0018DataRespDto = new Xdzc0018DataRespDto();// 响应Data：我的资产统计查询接口
        Xdzc0018RespDto xdzc0018RespDto = new Xdzc0018RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0018.req.Data reqData = null; // 请求Data：我的资产统计查询接口
        cn.com.yusys.yusp.dto.server.biz.xdzc0018.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0018.resp.Data();// 响应Data：我的资产统计查询接口

        try {
            // 从 xdzc0018ReqDto获取 reqData
            reqData = xdzc0018ReqDto.getData();
            // 将 reqData 拷贝到xdzc0018DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0018DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, JSON.toJSONString(xdzc0018DataReqDto));
            ResultDto<Xdzc0018DataRespDto> xdzc0018DataResultDto = dscmsBizZcClientService.xdzc0018(xdzc0018DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, JSON.toJSONString(xdzc0018DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0018RespDto.setErorcd(Optional.ofNullable(xdzc0018DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0018RespDto.setErortx(Optional.ofNullable(xdzc0018DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0018DataResultDto.getCode())) {
                xdzc0018DataRespDto = xdzc0018DataResultDto.getData();
                xdzc0018RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0018RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0018DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0018DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, e.getMessage());
            xdzc0018RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0018RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0018RespDto.setDatasq(servsq);

        xdzc0018RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0018.key, DscmsEnum.TRADE_CODE_XDZC0018.value, JSON.toJSONString(xdzc0018RespDto));
        return xdzc0018RespDto;
    }
}
