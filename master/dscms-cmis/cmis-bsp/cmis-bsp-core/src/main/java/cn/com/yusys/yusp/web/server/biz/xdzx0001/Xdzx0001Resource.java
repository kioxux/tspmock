package cn.com.yusys.yusp.web.server.biz.xdzx0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzx0001.req.Xdzx0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzx0001.resp.Xdzx0001RespDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.req.Xdzx0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzx0001.resp.Xdzx0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询征信业务流水号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZX0001:查询征信业务流水号")
@RestController
@RequestMapping("/api/dscms")
public class Xdzx0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzx0001Resource.class);
	@Autowired
	private DscmsBizZxClientService dscmsBizZxClientService;

    /**
     * 交易码：xdzx0001
     * 交易描述：查询征信业务流水号
     *
     * @param xdzx0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询征信业务流水号")
    @PostMapping("/xdzx0001")
    //@Idempotent({"xdcazx0001", "#xdzx0001ReqDto.datasq"})
    protected @ResponseBody
	Xdzx0001RespDto xdzx0001(@Validated @RequestBody Xdzx0001ReqDto xdzx0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001ReqDto));
        Xdzx0001DataReqDto xdzx0001DataReqDto = new Xdzx0001DataReqDto();// 请求Data： 查询征信业务流水号
        Xdzx0001DataRespDto xdzx0001DataRespDto = new Xdzx0001DataRespDto();// 响应Data：查询征信业务流水号
		cn.com.yusys.yusp.dto.server.biz.xdzx0001.req.Data reqData = null; // 请求Data：查询征信业务流水号
		Xdzx0001RespDto xdzx0001RespDto=new Xdzx0001RespDto();
		cn.com.yusys.yusp.dto.server.biz.xdzx0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzx0001.resp.Data ();// 响应Data：查询征信业务流水号
        try {
            // 从 xdzx0001ReqDto获取 reqData
            reqData = xdzx0001ReqDto.getData();
            // 将 reqData 拷贝到xdzx0001DataReqDto
            BeanUtils.copyProperties(reqData, xdzx0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001DataReqDto));
            ResultDto<Xdzx0001DataRespDto> xdzx0001DataResultDto = dscmsBizZxClientService.xdzx0001(xdzx0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzx0001RespDto.setErorcd(Optional.ofNullable(xdzx0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzx0001RespDto.setErortx(Optional.ofNullable(xdzx0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzx0001DataResultDto.getCode())) {
                xdzx0001DataRespDto = xdzx0001DataResultDto.getData();
                xdzx0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzx0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzx0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzx0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, e.getMessage());
            xdzx0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzx0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzx0001RespDto.setDatasq(servsq);

        xdzx0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZX0001.key, DscmsEnum.TRADE_CODE_XDZX0001.value, JSON.toJSONString(xdzx0001RespDto));
        return xdzx0001RespDto;
    }
}
