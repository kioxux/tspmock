package cn.com.yusys.yusp.web.server.biz.xdxw0086;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0086.req.Xdxw0086ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0086.resp.Xdxw0086RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0086.req.Xdxw0086DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0086.resp.Xdxw0086DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:涉税保密信息查询委托授权书文本生产PDF
 *
 * @author zr
 * @version 1.0
 */
@Api(tags = "XDXW0086:涉税保密信息查询委托授权书文本生产PDF")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0086Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0086Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0086
     * 交易描述：共借人声明文本生产PDF接口
     *
     * @param xdxw0086ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("涉税保密信息查询委托授权书文本生产PDF")
    @PostMapping("/xdxw0086")
    @Idempotent({"xdxw0086", "#xdxw0086ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0086RespDto xdxw0086(@Validated @RequestBody Xdxw0086ReqDto xdxw0086ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086ReqDto));
        Xdxw0086DataReqDto xdxw0086DataReqDto = new Xdxw0086DataReqDto();// 请求Data： 勘验任务状态同步
        Xdxw0086DataRespDto xdxw0086DataRespDto = new Xdxw0086DataRespDto();// 响应Data：勘验任务状态同步
		Xdxw0086RespDto xdxw0086RespDto = new Xdxw0086RespDto();
        // 此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0086.req.Data reqData = null; // 请求Data：勘验任务状态同步
		cn.com.yusys.yusp.dto.server.biz.xdxw0086.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0086.resp.Data();// 响应Data：勘验任务状态同步
        // 此处包导入待确定 结束
        try {
            // 从 xdxw0086ReqDto获取 reqData
            reqData = xdxw0086ReqDto.getData();
            // 将 reqData 拷贝到xdxw0086DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0086DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086DataReqDto));
            ResultDto<Xdxw0086DataRespDto> xdxw0086DataResultDto = dscmsBizXwClientService.xdxw0086(xdxw0086DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0086RespDto.setErorcd(Optional.ofNullable(xdxw0086DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0086RespDto.setErortx(Optional.ofNullable(xdxw0086DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0086DataResultDto.getCode())) {
                xdxw0086DataRespDto = xdxw0086DataResultDto.getData();
                xdxw0086RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0086RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0086DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0086DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, e.getMessage());
            xdxw0086RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0086RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0086RespDto.setDatasq(servsq);

        xdxw0086RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0086.key, DscmsEnum.TRADE_CODE_XDXW0086.value, JSON.toJSONString(xdxw0086RespDto));
        return xdxw0086RespDto;
    }
}
