package cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req;

/**
 * 请求Message：现金（大额）放款接口
 *
 * @author chenyong
 * @version 1.0
 */
public class D13087ReqMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d13087.req.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D13087ReqMessage{" +
                "message=" + message +
                '}';
    }
}
