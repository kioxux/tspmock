package cn.com.yusys.yusp.online.client.esb.core.dp2099.resp;

/**
 * 响应Service：保证金账户查询
 * @author code-generator
 * @version 1.0             
 */      
public class Dp2099RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
