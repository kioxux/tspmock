package cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.req;

/**
 * 请求Service：获取还款记录的借据一览信息，包括借据号、借据金额、借据余额
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd11ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd11ReqService{" +
                "service=" + service +
                '}';
    }
}
