package cn.com.yusys.yusp.online.client.esb.core.ln3057.resp;

/**
 * 响应Service：主要实现对贷款期限的缩短和延长修改，实时生效。
 * @author chenyong
 * @version 1.0             
 */      
public class Ln3057RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      


