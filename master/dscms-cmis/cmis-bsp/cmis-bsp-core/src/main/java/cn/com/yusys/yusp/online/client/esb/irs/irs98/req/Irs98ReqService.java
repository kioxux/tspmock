package cn.com.yusys.yusp.online.client.esb.irs.irs98.req;

/**
 * 请求Service：授信申请债项评级接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日 下午1:22:06
 */
public class Irs98ReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
