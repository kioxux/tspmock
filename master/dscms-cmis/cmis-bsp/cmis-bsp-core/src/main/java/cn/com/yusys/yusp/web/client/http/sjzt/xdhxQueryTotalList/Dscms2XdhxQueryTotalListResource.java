package cn.com.yusys.yusp.web.client.http.sjzt.xdhxQueryTotalList;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.req.XdhxQueryTotalListReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.List;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.XdhxQueryTotalListRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.online.client.http.sjzt.xdhxQueryTotalList.req.XdhxQueryTotalListReqService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * BSP封装调用外部数据平台的接口
 */
@Api(tags = "BSP封装调用外部数据平台的接口处理类(xdhxQueryTotalList)")
@RestController
@RequestMapping("/api/dscms2sjzt")
public class Dscms2XdhxQueryTotalListResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdhxQueryTotalListResource.class);
    @Autowired
    private RestTemplate restTemplate;

    /**
     * 信贷客户核心业绩统计查询列表
     *
     * @param xdhxQueryTotalListReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdhxQueryTotalList:信贷客户核心业绩统计查询列表")
    @PostMapping("/xdhxQueryTotalList")
    protected @ResponseBody
    ResultDto<XdhxQueryTotalListRespDto> xdhxQueryTotalList(@Validated @RequestBody XdhxQueryTotalListReqDto xdhxQueryTotalListReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.key, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.value, JSON.toJSONString(xdhxQueryTotalListReqDto));

        XdhxQueryTotalListReqService xdhxQueryTotalListReqService = new XdhxQueryTotalListReqService();

        XdhxQueryTotalListRespDto xdhxQueryTotalListRespDto = new XdhxQueryTotalListRespDto();
        ResultDto<XdhxQueryTotalListRespDto> xdhxQueryTotalListResultDto = new ResultDto<XdhxQueryTotalListRespDto>();
        try {
            // 将XdhxQueryTotalListReqDto转换成XdhxQueryTotalListReqService
            BeanUtils.copyProperties(xdhxQueryTotalListReqDto, xdhxQueryTotalListReqService);

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            xdhxQueryTotalListReqService.setServsq(servsq);//    渠道流水
            xdhxQueryTotalListReqService.setFrom(1);
            if(0 ==xdhxQueryTotalListReqService.getSize() || null == xdhxQueryTotalListReqService.getSize() ){
                xdhxQueryTotalListReqService.setSize(10);
            }
//            xdhxQueryTotalListReqService.setUserid(EsbEnum.BRCHNO_XWYWGLPT.key);//    柜员号
//            xdhxQueryTotalListReqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(JSON.toJSONString(xdhxQueryTotalListReqService), headers);
            String url = "http://sc-gateway.zrcbank.ingress/credit/cus/performance/stat";
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.key, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.value, JSON.toJSONString(xdhxQueryTotalListReqService));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.key, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.value, responseEntity);

            if (responseEntity.getStatusCodeValue() == 200) {
                String responseStr = responseEntity.getBody();
                JSONObject jsonObject = JSONObject.parseObject(responseStr);
                String data = jsonObject.get("data").toString();
                JSONObject dataObject = JSON.parseObject(data);
                String total = dataObject.get("total").toString();
                Object data1 = dataObject.get("data");
                //Object data = Optional.ofNullable(jsonObject.get("data")).orElse(new Object());
                String message = jsonObject.get("message").toString();
                String status = jsonObject.get("status").toString();
                xdhxQueryTotalListRespDto.setList((java.util.List<List>) data1);
                xdhxQueryTotalListRespDto.setErrorCode(status);
                xdhxQueryTotalListRespDto.setErrorMsg(message);
                xdhxQueryTotalListRespDto.setTotal(Integer.parseInt(total));
            } else {
                xdhxQueryTotalListResultDto.setCode(EpbEnum.EPB099999.key);//9999
                xdhxQueryTotalListResultDto.setMessage("交易失败");//系统异常
            }


        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.key, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.value, e.getMessage());
            xdhxQueryTotalListResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdhxQueryTotalListResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdhxQueryTotalListResultDto.setData(xdhxQueryTotalListRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.key, EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.value, JSON.toJSONString(xdhxQueryTotalListResultDto));
        return xdhxQueryTotalListResultDto;
    }
}
