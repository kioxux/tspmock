package cn.com.yusys.yusp.web.server.biz.xdzc0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdzc0005.req.Xdzc0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdzc0005.resp.Xdzc0005RespDto;
import cn.com.yusys.yusp.dto.server.xdzc0005.req.Xdzc0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdzc0005.resp.Xdzc0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizZcClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池入池接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDZC0005:资产池入池接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdzc0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdzc0005Resource.class);
    @Autowired
    private DscmsBizZcClientService dscmsBizZcClientService;

    /**
     * 交易码：xdzc0005
     * 交易描述：资产池入池接口
     *
     * @param xdzc0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池入池接口")
    @PostMapping("/xdzc0005")
    //@Idempotent({"xdzc005", "#xdzc0005ReqDto.datasq"})
    protected @ResponseBody
    Xdzc0005RespDto xdzc0005(@Validated @RequestBody Xdzc0005ReqDto xdzc0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, JSON.toJSONString(xdzc0005ReqDto));
        Xdzc0005DataReqDto xdzc0005DataReqDto = new Xdzc0005DataReqDto();// 请求Data： 抵质押物明细查询
        Xdzc0005DataRespDto xdzc0005DataRespDto = new Xdzc0005DataRespDto();// 响应Data：抵质押物明细查询
        Xdzc0005RespDto xdzc0005RespDto = new Xdzc0005RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdzc0005.req.Data reqData = null; // 请求Data：抵质押物明细查询
        cn.com.yusys.yusp.dto.server.biz.xdzc0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdzc0005.resp.Data();// 响应Data：抵质押物明细查询

        try {
            // 从 xdzc0005ReqDto获取 reqData
            reqData = xdzc0005ReqDto.getData();
            // 将 reqData 拷贝到xdzc0005DataReqDto
            BeanUtils.copyProperties(reqData, xdzc0005DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, JSON.toJSONString(xdzc0005DataReqDto));
            ResultDto<Xdzc0005DataRespDto> xdzc0005DataResultDto = dscmsBizZcClientService.xdzc0005(xdzc0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, JSON.toJSONString(xdzc0005DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdzc0005RespDto.setErorcd(Optional.ofNullable(xdzc0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdzc0005RespDto.setErortx(Optional.ofNullable(xdzc0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdzc0005DataResultDto.getCode())) {
                xdzc0005DataRespDto = xdzc0005DataResultDto.getData();
                xdzc0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdzc0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdzc0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdzc0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, e.getMessage());
            xdzc0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdzc0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdzc0005RespDto.setDatasq(servsq);

        xdzc0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDZC0005.key, DscmsEnum.TRADE_CODE_XDZC0005.value, JSON.toJSONString(xdzc0005RespDto));
        return xdzc0005RespDto;
    }
}
