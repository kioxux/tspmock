package cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp;

import java.math.BigDecimal;

/**
 * 响应Service：还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String repay_date;//还款日期
    private BigDecimal hkqbj;//本次还款前的应收未收正常本金余额和本次还款前应收未收逾期本金金额之和
    private BigDecimal shbj;//本次实还正常本金金额和本次实还逾期本金金额之和

    public String getRepay_date() {
        return repay_date;
    }

    public void setRepay_date(String repay_date) {
        this.repay_date = repay_date;
    }

    public BigDecimal getHkqbj() {
        return hkqbj;
    }

    public void setHkqbj(BigDecimal hkqbj) {
        this.hkqbj = hkqbj;
    }

    public BigDecimal getShbj() {
        return shbj;
    }

    public void setShbj(BigDecimal shbj) {
        this.shbj = shbj;
    }

    @Override
    public String toString() {
        return "Record{" +
                "repay_date='" + repay_date + '\'' +
                ", hkqbj=" + hkqbj +
                ", shbj=" + shbj +
                '}';
    }
}
