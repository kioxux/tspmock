package cn.com.yusys.yusp.web.server.biz.xdht0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0001.req.Xdht0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0001.resp.Xdht0001RespDto;
import cn.com.yusys.yusp.dto.server.xdht0001.req.Xdht0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0001.resp.Xdht0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询省心E付合同信息
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDHT0001:查询省心E付合同信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0001Resource.class);
	@Autowired
	private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0001
     * 交易描述：查询省心E付合同信息
     *
     * @param xdht0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询省心E付合同信息")
    @PostMapping("/xdht0001")
    @Idempotent({"xdcaht0001", "#xdht0001ReqDto.datasq"})
    protected @ResponseBody
	Xdht0001RespDto xdht0001(@Validated @RequestBody Xdht0001ReqDto xdht0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, JSON.toJSONString(xdht0001ReqDto));
        Xdht0001DataReqDto xdht0001DataReqDto = new Xdht0001DataReqDto();// 请求Data： 查询省心E付合同信息
        Xdht0001DataRespDto xdht0001DataRespDto = new Xdht0001DataRespDto();// 响应Data：查询省心E付合同信息
		Xdht0001RespDto xdht0001RespDto = new Xdht0001RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdht0001.req.Data reqData = null; // 请求Data：查询省心E付合同信息
		cn.com.yusys.yusp.dto.server.biz.xdht0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdht0001.resp.Data();// 响应Data：查询省心E付合同信息
        // 此处包导入待确定 结束
        try {
            // 从 xdht0001ReqDto获取 reqData
            reqData = xdht0001ReqDto.getData();
            // 将 reqData 拷贝到xdht0001DataReqDto
            BeanUtils.copyProperties(reqData, xdht0001DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, JSON.toJSONString(xdht0001DataReqDto));
            ResultDto<Xdht0001DataRespDto> xdht0001DataResultDto = dscmsBizHtClientService.xdht0001(xdht0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, JSON.toJSONString(xdht0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0001RespDto.setErorcd(Optional.ofNullable(xdht0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0001RespDto.setErortx(Optional.ofNullable(xdht0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdht0001DataResultDto.getCode())) {
                xdht0001DataRespDto = xdht0001DataResultDto.getData();
                xdht0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, e.getMessage());
            xdht0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0001RespDto.setDatasq(servsq);

        xdht0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0001.key, DscmsEnum.TRADE_CODE_XDHT0001.value, JSON.toJSONString(xdht0001RespDto));
        return xdht0001RespDto;
    }
}
