package cn.com.yusys.yusp.web.client.esb.ypxt.bdcqcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.req.BdcqcxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.resp.BdcqcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(bdcqcx)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2BdcqcxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2BdcqcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 不动产权证查询（处理码bdcqcx）
     *
     * @return
     * @throws Exception
     */
    @ApiOperation("bdcqcx:不动产权证查询")
    @PostMapping("/bdcqcx")
    protected @ResponseBody
    ResultDto<BdcqcxRespDto> bdcqcx(@Validated @RequestBody BdcqcxReqDto bdcqcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BDCQCX.key, EsbEnum.TRADE_CODE_BDCQCX.value, JSON.toJSONString(bdcqcxReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.resp.Service();
        BdcqcxReqService bdcqcxReqService = new BdcqcxReqService();
        BdcqcxRespService bdcqcxRespService = new BdcqcxRespService();
        BdcqcxRespDto bdcqcxRespDto = new BdcqcxRespDto();
        ResultDto<BdcqcxRespDto> bdcqcxResultDto = new ResultDto<BdcqcxRespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将BdcqcxReqDto转换成reqService
            BeanUtils.copyProperties(bdcqcxReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_BDCQCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            bdcqcxReqService.setService(reqService);
            // 将bdcqcxReqService转换成bdcqcxReqServiceMap
            Map bdcqcxReqServiceMap = beanMapUtil.beanToMap(bdcqcxReqService);
            context.put("tradeDataMap", bdcqcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BDCQCX.key, EsbEnum.TRADE_CODE_BDCQCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_BDCQCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BDCQCX.key, EsbEnum.TRADE_CODE_BDCQCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            bdcqcxRespService = beanMapUtil.mapToBean(tradeDataMap, BdcqcxRespService.class, BdcqcxRespService.class);
            respService = bdcqcxRespService.getService();

            //  将BdcqcxRespDto封装到ResultDto<BdcqcxRespDto>
            bdcqcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            bdcqcxResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成BdcqcxRespDto
                BeanUtils.copyProperties(respService, bdcqcxRespDto);
                bdcqcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                bdcqcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                bdcqcxResultDto.setCode(EpbEnum.EPB099999.key);
                bdcqcxResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BDCQCX.key, EsbEnum.TRADE_CODE_BDCQCX.value, e.getMessage());
            bdcqcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            bdcqcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        bdcqcxResultDto.setData(bdcqcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_BDCQCX.key, EsbEnum.TRADE_CODE_BDCQCX.value, JSON.toJSONString(bdcqcxResultDto));
        return bdcqcxResultDto;
    }
}
