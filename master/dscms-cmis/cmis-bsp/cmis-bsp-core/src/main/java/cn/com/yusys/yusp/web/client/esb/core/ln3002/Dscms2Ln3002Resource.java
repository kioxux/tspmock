package cn.com.yusys.yusp.web.client.esb.core.ln3002;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3002.req.Ln3002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3002.resp.Ln3002RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3002.req.Ln3002ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3002.resp.Ln3002RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3002)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3002Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3002Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();
    /**
     * 交易码：ln3002
     * 交易描述：贷款产品删除
     *
     * @param ln3002ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3002:贷款产品删除")
    @PostMapping("/ln3002")
    protected @ResponseBody
    ResultDto<Ln3002RespDto> ln3002(@Validated @RequestBody Ln3002ReqDto ln3002ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3002.key, EsbEnum.TRADE_CODE_LN3002.value, JSON.toJSONString(ln3002ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3002.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3002.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3002.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3002.resp.Service();
        Ln3002ReqService ln3002ReqService = new Ln3002ReqService();
        Ln3002RespService ln3002RespService = new Ln3002RespService();
        Ln3002RespDto ln3002RespDto = new Ln3002RespDto();
        ResultDto<Ln3002RespDto> ln3002ResultDto = new ResultDto<Ln3002RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3002ReqDto转换成reqService
            BeanUtils.copyProperties(ln3002ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3002.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3002ReqService.setService(reqService);
            // 将ln3002ReqService转换成ln3002ReqServiceMap
            Map ln3002ReqServiceMap = beanMapUtil.beanToMap(ln3002ReqService);
            context.put("tradeDataMap", ln3002ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3002.key, EsbEnum.TRADE_CODE_LN3002.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3002.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3002.key, EsbEnum.TRADE_CODE_LN3002.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3002RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3002RespService.class, Ln3002RespService.class);
            respService = ln3002RespService.getService();

            ln3002ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3002ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3002RespDto
                BeanUtils.copyProperties(respService, ln3002RespDto);
                ln3002ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3002ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3002ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3002ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3002.key, EsbEnum.TRADE_CODE_LN3002.value, e.getMessage());
            ln3002ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3002ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3002ResultDto.setData(ln3002RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3002.key, EsbEnum.TRADE_CODE_LN3002.value, JSON.toJSONString(ln3002ResultDto));
        return ln3002ResultDto;
    }
}
