package cn.com.yusys.yusp.online.client.esb.circp.fb1213.resp;

/**
 * 响应Service：押品出库通知
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1213RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1213RespService{" +
                "service=" + service +
                '}';
    }
}
