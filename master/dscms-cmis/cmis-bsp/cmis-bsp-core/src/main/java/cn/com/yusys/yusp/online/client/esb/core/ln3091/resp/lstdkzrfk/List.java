package cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.lstdkzrfk;

/**
 * 响应Service：待付款指令查询
 *
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3091.resp.lstdkzrfk.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
