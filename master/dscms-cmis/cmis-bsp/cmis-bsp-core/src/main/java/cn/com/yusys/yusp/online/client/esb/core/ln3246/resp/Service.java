package cn.com.yusys.yusp.online.client.esb.core.ln3246.resp;

/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class Service {

    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水


    private Integer zongbish; // 总笔数 BaseType.U_CHANZXLX

    private List lstLnDkhkmx_ARRAY; // 还款明细列表

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List getLstLnDkhkmx_ARRAY() {
        return lstLnDkhkmx_ARRAY;
    }

    public void setLstLnDkhkmx_ARRAY(List lstLnDkhkmx_ARRAY) {
        this.lstLnDkhkmx_ARRAY = lstLnDkhkmx_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zongbish=" + zongbish +
                ", lstLnDkhkmx_ARRAY=" + lstLnDkhkmx_ARRAY +
                '}';
    }
}
