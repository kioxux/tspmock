package cn.com.yusys.yusp.online.client.esb.core.ln3083.resp;

/**
 * 响应Service：银团协议查询
 * @author code-generator
 * @version 1.0             
 */      
public class Ln3083RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
