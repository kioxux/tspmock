package cn.com.yusys.yusp.online.client.esb.edzfxt.djztcx.req;

/**
 * 请求Service：贷记入账状态查询申请往帐
 *
 * @author code-generator
 * @version 1.0
 */
public class DjztcxReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
