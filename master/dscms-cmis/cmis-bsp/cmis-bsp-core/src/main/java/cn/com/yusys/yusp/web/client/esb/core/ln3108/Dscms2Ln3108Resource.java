package cn.com.yusys.yusp.web.client.esb.core.ln3108;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Lstdkzhzb;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3108.req.Ln3108ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3108.resp.Ln3108RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3108)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3108Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3108Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3108
     * 交易描述：贷款组合查询
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3108:贷款组合查询")
    @PostMapping("/ln3108")
    protected @ResponseBody
    ResultDto<Ln3108RespDto> ln3108(@Validated @RequestBody Ln3108ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3108.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3108.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3108.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3108.resp.Service();
        Ln3108ReqService ln3108ReqService = new Ln3108ReqService();
        Ln3108RespService ln3108RespService = new Ln3108RespService();
        Ln3108RespDto ln3108RespDto = new Ln3108RespDto();
        ResultDto<Ln3108RespDto> ln3108ResultDto = new ResultDto<Ln3108RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3108ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3108.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3108ReqService.setService(reqService);
            // 将ln3108ReqService转换成ln3108ReqServiceMap
            Map ln3108ReqServiceMap = beanMapUtil.beanToMap(ln3108ReqService);
            context.put("tradeDataMap", ln3108ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3108.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3108RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3108RespService.class, Ln3108RespService.class);
            respService = ln3108RespService.getService();
            //  将respService转换成Ln3108RespDto
            BeanUtils.copyProperties(respService, ln3108RespDto);
            List<Lstdkzhzb> lstdkzhzbs = new ArrayList<Lstdkzhzb>();
            if (respService.getLstdkzhzb_ARRAY() != null) {
                List<cn.com.yusys.yusp.online.client.esb.core.ln3108.resp.Record> recordList = respService.getLstdkzhzb_ARRAY().getRecord();
                // 遍历record传值塞入listArrayInfo
                for (cn.com.yusys.yusp.online.client.esb.core.ln3108.resp.Record record : recordList) {
                    Lstdkzhzb lstdkzhzb = new Lstdkzhzb();
                    BeanUtils.copyProperties(record, lstdkzhzb);
                    lstdkzhzbs.add(lstdkzhzb);
                }
            }
            ln3108RespDto.setLstdkzhzb(lstdkzhzbs);
            ln3108ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3108ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, ln3108RespDto);
                ln3108ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3108ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3108ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3108ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value, e.getMessage());
            ln3108ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3108ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ln3108ResultDto.setData(ln3108RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3108.key, EsbEnum.TRADE_CODE_LN3108.value, JSON.toJSONString(ln3108ResultDto));
        return ln3108ResultDto;
    }
}
