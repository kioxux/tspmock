package cn.com.yusys.yusp.online.client.esb.ecif.g10002.resp;

/**
 * 响应Service：客户群组信息维护
 *
 * @author chenyong
 * @version 1.0
 */
public class G10002RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "G10002RespService{" +
                "service=" + service +
                '}';
    }
}

