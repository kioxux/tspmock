package cn.com.yusys.yusp.online.client.esb.core.ln3112.resp;

import java.math.BigDecimal;

/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
public class Record {

    private Integer zhanqixh;//展期序号
    private String zhanqirq;//展期日期
    private String zhanqdqr;//展期到期日
    private BigDecimal zhanqije;//展期金额
    private String lilvleix;//利率类型
    private BigDecimal zhchlilv;//正常利率
    private String lilvtzfs;//利率调整方式
    private String lilvtzzq;//利率调整周期
    private String lilvfdfs;//利率浮动方式
    private BigDecimal lilvfdzh;//利率浮动值
    private String dkzhqizt;//贷款展期状态


    public Integer getZhanqixh() {
        return zhanqixh;
    }

    public void setZhanqixh(Integer zhanqixh) {
        this.zhanqixh = zhanqixh;
    }

    public String getZhanqirq() {
        return zhanqirq;
    }

    public void setZhanqirq(String zhanqirq) {
        this.zhanqirq = zhanqirq;
    }

    public String getZhanqdqr() {
        return zhanqdqr;
    }

    public void setZhanqdqr(String zhanqdqr) {
        this.zhanqdqr = zhanqdqr;
    }

    public BigDecimal getZhanqije() {
        return zhanqije;
    }

    public void setZhanqije(BigDecimal zhanqije) {
        this.zhanqije = zhanqije;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getDkzhqizt() {
        return dkzhqizt;
    }

    public void setDkzhqizt(String dkzhqizt) {
        this.dkzhqizt = dkzhqizt;
    }

    @Override
    public String toString() {
        return "Lstdkzqdj{" +
                "zhanqixh='" + zhanqixh + '\'' +
                "zhanqirq='" + zhanqirq + '\'' +
                "zhanqdqr='" + zhanqdqr + '\'' +
                "zhanqije='" + zhanqije + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "dkzhqizt='" + dkzhqizt + '\'' +
                '}';
    }
}
