package cn.com.yusys.yusp.online.client.esb.yphsxt.credis.resp;

/**
 * 响应Service：信用证信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class CredisRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "CredisRespService{" +
                "service=" + service +
                '}';
    }
}
