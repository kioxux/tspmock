package cn.com.yusys.yusp.online.client.esb.core.ln3020.req;

import cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhkzh.Record;

import java.util.List;

/**
 * 请求Dto：贷款多还款账户
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Lstdkhkzh_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkhkzh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkhkzh_ARRAY{" +
                "record=" + record +
                '}';
    }
}