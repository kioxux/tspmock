package cn.com.yusys.yusp.web.server.biz.xdxw0007;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0007.req.Xdxw0007ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0007.resp.Xdxw0007RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0007.req.Xdxw0007DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0007.resp.Xdxw0007DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import feign.FeignException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:批复信息查询
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDXW0007:批复信息查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0007Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0007Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0007
     * 交易描述：通过合同号，查询小贷调查报告批复结果表，得到批复起始日、批复到期日、批复金额等，返回结果为单条数据
     *
     * @param xdxw0007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("批复信息查询")
    @PostMapping("/xdxw0007")
//    @Idempotent({"xdcaxw0007", "#xdxw0007ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0007RespDto xdxw0007(@Validated @RequestBody Xdxw0007ReqDto xdxw0007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, JSON.toJSONString(xdxw0007ReqDto));
        Xdxw0007DataReqDto xdxw0007DataReqDto = new Xdxw0007DataReqDto();// 请求Data： 批复信息查询
        Xdxw0007DataRespDto xdxw0007DataRespDto = null;// 响应Data：批复信息查询
        Xdxw0007RespDto xdxw0007RespDto = new Xdxw0007RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0007.req.Data reqData = null; // 请求Data：批复信息查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0007.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0007.resp.Data();// 响应Data：批复信息查询
        try {
            // 从 xdxw0007ReqDto获取 reqData
            reqData = xdxw0007ReqDto.getData();
            // 将 reqData 拷贝到xdxw0007DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0007DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, JSON.toJSONString(xdxw0007DataReqDto));
            ResultDto<Xdxw0007DataRespDto> xdxw0007DataResultDto = dscmsBizXwClientService.xdxw0007(xdxw0007DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, JSON.toJSONString(xdxw0007DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0007RespDto.setErorcd(Optional.ofNullable(xdxw0007DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0007RespDto.setErortx(Optional.ofNullable(xdxw0007DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0007DataResultDto.getCode())) {
                xdxw0007DataRespDto = xdxw0007DataResultDto.getData();
                xdxw0007RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0007RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0007DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0007DataRespDto, respData);
            }
        } catch (FeignException e) {
            JSONObject j = JSONObject.parseObject(e.contentUTF8());
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0024.key, DscmsEnum.TRADE_CODE_XDHT0024.value, e.getMessage());
            xdxw0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0007RespDto.setErortx(String.valueOf(j.get("message")));// 系统异常
        }  catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, e.getMessage());
            xdxw0007RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0007RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0007RespDto.setDatasq(servsq);

        xdxw0007RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0007.key, DscmsEnum.TRADE_CODE_XDXW0007.value, JSON.toJSONString(xdxw0007RespDto));
        return xdxw0007RespDto;
    }
}
