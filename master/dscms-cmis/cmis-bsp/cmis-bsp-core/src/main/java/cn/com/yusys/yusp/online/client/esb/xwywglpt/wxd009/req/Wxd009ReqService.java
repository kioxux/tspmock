package cn.com.yusys.yusp.online.client.esb.xwywglpt.wxd009.req;

/**
 * 请求Service：信贷系统请求小V平台推送合同信息接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Wxd009ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Wxd009ReqService{" +
                "service=" + service +
                '}';
    }
}
