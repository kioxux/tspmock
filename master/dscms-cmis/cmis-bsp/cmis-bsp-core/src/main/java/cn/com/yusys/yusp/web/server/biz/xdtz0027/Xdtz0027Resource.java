package cn.com.yusys.yusp.web.server.biz.xdtz0027;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0027.req.Xdtz0027ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0027.resp.Xdtz0027RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.req.Xdtz0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0027.resp.Xdtz0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0027:根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0027Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0027
     * 交易描述：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
     *
     * @param xdtz0027ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款")
    @PostMapping("/xdtz0027")
    //@Idempotent({"xdcatz0027", "#xdtz0027ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0027RespDto xdtz0027(@Validated @RequestBody Xdtz0027ReqDto xdtz0027ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027ReqDto));
        Xdtz0027DataReqDto xdtz0027DataReqDto = new Xdtz0027DataReqDto();// 请求Data： 根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
        Xdtz0027DataRespDto xdtz0027DataRespDto = new Xdtz0027DataRespDto();// 响应Data：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
        Xdtz0027RespDto xdtz0027RespDto = new Xdtz0027RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0027.req.Data reqData = null; // 请求Data：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
        cn.com.yusys.yusp.dto.server.biz.xdtz0027.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0027.resp.Data();// 响应Data：根据客户ID，查询是否有优企贷贷款期限半年以上，过去一年内结清或者到期的贷款
        try {
            // 从 xdtz0027ReqDto获取 reqData
            reqData = xdtz0027ReqDto.getData();
            // 将 reqData 拷贝到xdtz0027DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0027DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataReqDto));
            ResultDto<Xdtz0027DataRespDto> xdtz0027DataResultDto = dscmsBizTzClientService.xdtz0027(xdtz0027DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0027RespDto.setErorcd(Optional.ofNullable(xdtz0027DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0027RespDto.setErortx(Optional.ofNullable(xdtz0027DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0027DataResultDto.getCode())) {
                xdtz0027DataRespDto = xdtz0027DataResultDto.getData();
                xdtz0027RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0027RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0027DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0027DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, e.getMessage());
            xdtz0027RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0027RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0027RespDto.setDatasq(servsq);

        xdtz0027RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0027.key, DscmsEnum.TRADE_CODE_XDTZ0027.value, JSON.toJSONString(xdtz0027RespDto));
        return xdtz0027RespDto;
    }
}
