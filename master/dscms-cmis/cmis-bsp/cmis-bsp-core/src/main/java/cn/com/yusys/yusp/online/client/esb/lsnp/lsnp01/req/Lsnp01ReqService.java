package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp01.req;

/**
 * 请求Service：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
public class Lsnp01ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}