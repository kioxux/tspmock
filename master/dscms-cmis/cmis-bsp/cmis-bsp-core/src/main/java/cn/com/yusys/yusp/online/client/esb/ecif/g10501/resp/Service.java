package cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp;

/**
 * 响应Service：对公及同业客户清单查询
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:26
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String bginnm;//起始笔数
    private String qurynm;// 查询笔数
    private String totlnm;//  总笔数
    private Integer listnm;//    循环列表记录数
    private List list;//清单客户信息_ARRAY

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getBginnm() {
        return bginnm;
    }

    public void setBginnm(String bginnm) {
        this.bginnm = bginnm;
    }

    public String getQurynm() {
        return qurynm;
    }

    public void setQurynm(String qurynm) {
        this.qurynm = qurynm;
    }

    public String getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(String totlnm) {
        this.totlnm = totlnm;
    }

    public Integer getListnm() {
        return listnm;
    }

    public void setListnm(Integer listnm) {
        this.listnm = listnm;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", bginnm='" + bginnm + '\'' +
                ", qurynm='" + qurynm + '\'' +
                ", totlnm='" + totlnm + '\'' +
                ", listnm=" + listnm +
                ", list=" + list +
                '}';
    }
}
