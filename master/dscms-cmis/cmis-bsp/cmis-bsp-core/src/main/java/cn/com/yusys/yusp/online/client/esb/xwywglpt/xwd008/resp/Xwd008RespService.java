package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.resp;

/**
 * 响应Service：新信贷同步用户账号
 *
 * @author chenyong
 * @version 1.0
 */
public class Xwd008RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xwd008RespService{" +
                "service=" + service +
                '}';
    }
}
