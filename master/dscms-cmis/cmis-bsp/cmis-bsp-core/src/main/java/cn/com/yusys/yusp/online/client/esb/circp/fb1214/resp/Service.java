package cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * 响应Service：房产信息修改同步
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    @JsonProperty(value = "CRD_ITEM_NUM")
    private String CRD_ITEM_NUM;//授信分项编号
    @JsonProperty(value = "NEW_PRE_CRD_AMT")
    private BigDecimal NEW_PRE_CRD_AMT;//重新计算后的授信额度
    @JsonProperty(value = "OLS_TRAN_NO")
    private String OLS_TRAN_NO;//系统交易流水
    @JsonProperty(value = "OLS_DATE")
    private String OLS_DATE;//系统交易日期
    @JsonProperty(value = "HOUSE_LIST")
    private cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.HOUSE_LIST HOUSE_LIST;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCRD_ITEM_NUM() {
        return CRD_ITEM_NUM;
    }

    public void setCRD_ITEM_NUM(String CRD_ITEM_NUM) {
        this.CRD_ITEM_NUM = CRD_ITEM_NUM;
    }

    public BigDecimal getNEW_PRE_CRD_AMT() {
        return NEW_PRE_CRD_AMT;
    }

    public void setNEW_PRE_CRD_AMT(BigDecimal NEW_PRE_CRD_AMT) {
        this.NEW_PRE_CRD_AMT = NEW_PRE_CRD_AMT;
    }

    public String getOLS_TRAN_NO() {
        return OLS_TRAN_NO;
    }

    public void setOLS_TRAN_NO(String OLS_TRAN_NO) {
        this.OLS_TRAN_NO = OLS_TRAN_NO;
    }

    public String getOLS_DATE() {
        return OLS_DATE;
    }

    public void setOLS_DATE(String OLS_DATE) {
        this.OLS_DATE = OLS_DATE;
    }

    public cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.HOUSE_LIST getHOUSE_LIST() {
        return HOUSE_LIST;
    }

    public void setHOUSE_LIST(cn.com.yusys.yusp.online.client.esb.circp.fb1214.resp.HOUSE_LIST HOUSE_LIST) {
        this.HOUSE_LIST = HOUSE_LIST;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", CRD_ITEM_NUM='" + CRD_ITEM_NUM + '\'' +
                ", NEW_PRE_CRD_AMT=" + NEW_PRE_CRD_AMT +
                ", OLS_TRAN_NO='" + OLS_TRAN_NO + '\'' +
                ", OLS_DATE='" + OLS_DATE + '\'' +
                ", HOUSE_LIST=" + HOUSE_LIST +
                '}';
    }
}
