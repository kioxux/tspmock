package cn.com.yusys.yusp.online.client.esb.circp.fb1161.resp;

/**
 * 响应Service：借据信息同步
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1161RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1161RespService{" +
                "service=" + service +
                '}';
    }
}
