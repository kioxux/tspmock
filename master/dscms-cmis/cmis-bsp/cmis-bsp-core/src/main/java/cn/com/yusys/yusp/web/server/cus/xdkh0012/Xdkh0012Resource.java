package cn.com.yusys.yusp.web.server.cus.xdkh0012;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0012.req.Xdkh0012ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0012.resp.Xdkh0012RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0012.req.Xdkh0012DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0012.resp.Xdkh0012DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:对公客户信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0012:对公客户信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0012Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0012Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0012
     * 交易描述：对公客户信息同步
     *
     * @param xdkh0012ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("对公客户信息同步")
    @PostMapping("/xdkh0012")
    //@Idempotent({"xdcakh0012", "#xdkh0012ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0012RespDto xdkh0012(@Validated @RequestBody Xdkh0012ReqDto xdkh0012ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012ReqDto));
        Xdkh0012DataReqDto xdkh0012DataReqDto = new Xdkh0012DataReqDto();// 请求Data： 对公客户信息同步
        Xdkh0012DataRespDto xdkh0012DataRespDto = new Xdkh0012DataRespDto();// 响应Data：对公客户信息同步
        Xdkh0012RespDto xdkh0012RespDto = new Xdkh0012RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0012.req.Data reqData = null; // 请求Data：对公客户信息同步
        cn.com.yusys.yusp.dto.server.cus.xdkh0012.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0012.resp.Data();// 响应Data：对公客户信息同步
        try {
            // 从 xdkh0012ReqDto获取 reqData
            reqData = xdkh0012ReqDto.getData();
            // 将 reqData 拷贝到xdkh0012DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0012DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataReqDto));
            ResultDto<Xdkh0012DataRespDto> xdkh0012DataResultDto = dscmsCusClientService.xdkh0012(xdkh0012DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdkh0012RespDto.setErorcd(Optional.ofNullable(xdkh0012DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0012RespDto.setErortx(Optional.ofNullable(xdkh0012DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0012DataResultDto.getCode())) {
                xdkh0012DataRespDto = xdkh0012DataResultDto.getData();
                xdkh0012RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0012RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdkh0012DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0012DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, e.getMessage());
            xdkh0012RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0012RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0012RespDto.setDatasq(servsq);

        xdkh0012RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0012.key, DscmsEnum.TRADE_CODE_XDKH0012.value, JSON.toJSONString(xdkh0012RespDto));
        return xdkh0012RespDto;
    }
}