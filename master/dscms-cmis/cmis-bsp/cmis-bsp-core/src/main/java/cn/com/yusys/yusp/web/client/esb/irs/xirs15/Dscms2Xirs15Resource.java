package cn.com.yusys.yusp.web.client.esb.irs.xirs15;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs15.req.Xirs15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs15.resp.Xirs15RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.xirs15.req.Xirs15ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.xirs15.resp.Xirs15RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(xirs15)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Xirs15Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xirs15Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 评级在途申请标识（处理码IRS15）
     *
     * @param xirs15ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xirs15:评级在途申请标识")
    @PostMapping("/xirs15")
    protected @ResponseBody
    ResultDto<Xirs15RespDto> xirs15(@Validated @RequestBody Xirs15ReqDto xirs15ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS15.key, EsbEnum.TRADE_CODE_IRS15.value, JSON.toJSONString(xirs15ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.xirs15.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.xirs15.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.xirs15.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.xirs15.resp.Service();
        Xirs15ReqService xirs15ReqService = new Xirs15ReqService();
        Xirs15RespService xirs15RespService = new Xirs15RespService();
        Xirs15RespDto xirs15RespDto = new Xirs15RespDto();
        ResultDto<Xirs15RespDto> xirs15ResultDto = new ResultDto<Xirs15RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Xirs15ReqDto转换成reqService
            BeanUtils.copyProperties(xirs15ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS15.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xirs15ReqService.setService(reqService);
            // 将xirs15ReqService转换成xirs15ReqServiceMap
            Map xirs15ReqServiceMap = beanMapUtil.beanToMap(xirs15ReqService);
            context.put("tradeDataMap", xirs15ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS15.key, EsbEnum.TRADE_CODE_IRS15.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS15.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS15.key, EsbEnum.TRADE_CODE_IRS15.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xirs15RespService = beanMapUtil.mapToBean(tradeDataMap, Xirs15RespService.class, Xirs15RespService.class);
            respService = xirs15RespService.getService();

            //  将Xirs15RespDto封装到ResultDto<Xirs15RespDto>
            xirs15ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xirs15ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xirs15RespDto
                BeanUtils.copyProperties(respService, xirs15RespDto);
                xirs15ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xirs15ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xirs15ResultDto.setCode(EpbEnum.EPB099999.key);
                xirs15ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS15.key, EsbEnum.TRADE_CODE_IRS15.value, e.getMessage());
            xirs15ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xirs15ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xirs15ResultDto.setData(xirs15RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS15.key, EsbEnum.TRADE_CODE_IRS15.value, JSON.toJSONString(xirs15ResultDto));
        return xirs15ResultDto;

    }
}
