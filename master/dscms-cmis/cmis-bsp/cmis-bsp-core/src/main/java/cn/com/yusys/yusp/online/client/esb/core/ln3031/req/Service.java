package cn.com.yusys.yusp.online.client.esb.core.ln3031.req;

import java.math.BigDecimal;

/**
 * 请求Service：贷款资料维护（关于还款）
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servdt;//交易日期
    private String servti;//交易时间
    private String dkkhczbz;//开户操作标志
    private String dkjiejuh;//贷款借据号
    private String huobdhao;//货币代号
    private String chanpdma;//产品代码
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private Integer dbdkkksx;//多笔贷款扣款顺序
    private String beizhuuu;//备注信息
    private String hetongbh;//合同编号
    private BigDecimal hetongje;//合同金额
    private BigDecimal jiejuuje;//借据金额
    private String huankfsh;//还款方式
    private String dechligz;//等额处理规则
    private String qigscfsh;//期供生成方式
    private String duophkbz;//多频率还款标志
    private String dzhhkjih;//定制还款计划
    private String rchukkbz;//日初扣款标志
    private String zdkoukbz;//自动扣款标志
    private String zdjqdkbz;//自动结清贷款标志
    private String dhkzhhbz;//多还款账户标志
    private String qyxhdkbz;//签约循环贷款标志
    private String llbgtzjh;//利率变更调整计划
    private String dcfktzjh;//多次放款调整计划
    private String tqhktzjh;//提前还款调整计划
    private String yunxtqhk;//允许提前还款
    private String xycihkrq;//下一次还款日
    private Integer leijqjsh;//累进区间期数
    private BigDecimal meiqhkze;//每期还款总额
    private BigDecimal meiqhbje;//每期还本金额
    private String huankzhh;//还款账号
    private String xhdkqyzh;//签约账号
    private String hkqixian;//还款期限(月)
    private String hkshxubh;//还款顺序编号
    private BigDecimal leijinzh;//累进值
    private BigDecimal baoliuje;//保留金额
    private String hkzhhzxh;//还款账号子序号
    private String xhdkzhxh;//签约账号子序号
    private String hkzhouqi;//还款周期
    private String huanbzhq;//还本周期
    private String yuqhkzhq;//逾期还款周期
    private Lstdkhbjh_ARRAY lstdkhbjh_ARRAY;//贷款还本计划
    private Lstdkhkfs_ARRAY lstdkhkfs_ARRAY;//贷款组合还款方式
    private Lstdkhkzh_ARRAY lstdkhkzh_ARRAY;//贷款多还款账户
    private String qxbgtzjh;//期限变更调整计划
    private String tiaozhkf;//允许调整还款方式
    private String scihkrbz;//首次还款日模式
    private String mqihkfsh;//末期还款方式
    private Integer suoqcish;//缩期次数
    private String bzuekkfs;//不足额扣款方式
    private String yqbzkkfs;//逾期不足额扣款方式
    private String hntmihku;//允许行内同名账户还款
    private String hnftmhku;//允许行内非同名帐户还款
    private String nbuzhhku;//允许内部账户还款
    private Integer tiqhksdq;//提前还款锁定期
    private String sfyxkuxq;//是否有宽限期
    private Integer kuanxqts;//宽限期天数
    private Integer kxqzdcsh;//宽限期最大次数
    private String kxqjixgz;//宽限期计息方式
    private String kxqhjxgz;//宽限期后计息规则
    private String kxqzyqgz;//宽限期转逾期规则
    private String kxqjjrgz;//宽限期节假日规则
    private String yunxsuoq;//允许缩期
    private String kxqshxgz;//宽限期收息规则

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public Integer getDbdkkksx() {
        return dbdkkksx;
    }

    public void setDbdkkksx(Integer dbdkkksx) {
        this.dbdkkksx = dbdkkksx;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public BigDecimal getHetongje() {
        return hetongje;
    }

    public void setHetongje(BigDecimal hetongje) {
        this.hetongje = hetongje;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getDuophkbz() {
        return duophkbz;
    }

    public void setDuophkbz(String duophkbz) {
        this.duophkbz = duophkbz;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public String getRchukkbz() {
        return rchukkbz;
    }

    public void setRchukkbz(String rchukkbz) {
        this.rchukkbz = rchukkbz;
    }

    public String getZdkoukbz() {
        return zdkoukbz;
    }

    public void setZdkoukbz(String zdkoukbz) {
        this.zdkoukbz = zdkoukbz;
    }

    public String getZdjqdkbz() {
        return zdjqdkbz;
    }

    public void setZdjqdkbz(String zdjqdkbz) {
        this.zdjqdkbz = zdjqdkbz;
    }

    public String getDhkzhhbz() {
        return dhkzhhbz;
    }

    public void setDhkzhhbz(String dhkzhhbz) {
        this.dhkzhhbz = dhkzhhbz;
    }

    public String getQyxhdkbz() {
        return qyxhdkbz;
    }

    public void setQyxhdkbz(String qyxhdkbz) {
        this.qyxhdkbz = qyxhdkbz;
    }

    public String getLlbgtzjh() {
        return llbgtzjh;
    }

    public void setLlbgtzjh(String llbgtzjh) {
        this.llbgtzjh = llbgtzjh;
    }

    public String getDcfktzjh() {
        return dcfktzjh;
    }

    public void setDcfktzjh(String dcfktzjh) {
        this.dcfktzjh = dcfktzjh;
    }

    public String getTqhktzjh() {
        return tqhktzjh;
    }

    public void setTqhktzjh(String tqhktzjh) {
        this.tqhktzjh = tqhktzjh;
    }

    public String getYunxtqhk() {
        return yunxtqhk;
    }

    public void setYunxtqhk(String yunxtqhk) {
        this.yunxtqhk = yunxtqhk;
    }

    public String getXycihkrq() {
        return xycihkrq;
    }

    public void setXycihkrq(String xycihkrq) {
        this.xycihkrq = xycihkrq;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getXhdkqyzh() {
        return xhdkqyzh;
    }

    public void setXhdkqyzh(String xhdkqyzh) {
        this.xhdkqyzh = xhdkqyzh;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public String getHkshxubh() {
        return hkshxubh;
    }

    public void setHkshxubh(String hkshxubh) {
        this.hkshxubh = hkshxubh;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getXhdkzhxh() {
        return xhdkzhxh;
    }

    public void setXhdkzhxh(String xhdkzhxh) {
        this.xhdkzhxh = xhdkzhxh;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHuanbzhq() {
        return huanbzhq;
    }

    public void setHuanbzhq(String huanbzhq) {
        this.huanbzhq = huanbzhq;
    }

    public String getYuqhkzhq() {
        return yuqhkzhq;
    }

    public void setYuqhkzhq(String yuqhkzhq) {
        this.yuqhkzhq = yuqhkzhq;
    }

    public Lstdkhbjh_ARRAY getLstdkhbjh_ARRAY() {
        return lstdkhbjh_ARRAY;
    }

    public void setLstdkhbjh_ARRAY(Lstdkhbjh_ARRAY lstdkhbjh_ARRAY) {
        this.lstdkhbjh_ARRAY = lstdkhbjh_ARRAY;
    }

    public Lstdkhkfs_ARRAY getLstdkhkfs_ARRAY() {
        return lstdkhkfs_ARRAY;
    }

    public void setLstdkhkfs_ARRAY(Lstdkhkfs_ARRAY lstdkhkfs_ARRAY) {
        this.lstdkhkfs_ARRAY = lstdkhkfs_ARRAY;
    }

    public Lstdkhkzh_ARRAY getLstdkhkzh_ARRAY() {
        return lstdkhkzh_ARRAY;
    }

    public void setLstdkhkzh_ARRAY(Lstdkhkzh_ARRAY lstdkhkzh_ARRAY) {
        this.lstdkhkzh_ARRAY = lstdkhkzh_ARRAY;
    }

    public String getQxbgtzjh() {
        return qxbgtzjh;
    }

    public void setQxbgtzjh(String qxbgtzjh) {
        this.qxbgtzjh = qxbgtzjh;
    }

    public String getTiaozhkf() {
        return tiaozhkf;
    }

    public void setTiaozhkf(String tiaozhkf) {
        this.tiaozhkf = tiaozhkf;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public Integer getSuoqcish() {
        return suoqcish;
    }

    public void setSuoqcish(Integer suoqcish) {
        this.suoqcish = suoqcish;
    }

    public String getBzuekkfs() {
        return bzuekkfs;
    }

    public void setBzuekkfs(String bzuekkfs) {
        this.bzuekkfs = bzuekkfs;
    }

    public String getYqbzkkfs() {
        return yqbzkkfs;
    }

    public void setYqbzkkfs(String yqbzkkfs) {
        this.yqbzkkfs = yqbzkkfs;
    }

    public String getHntmihku() {
        return hntmihku;
    }

    public void setHntmihku(String hntmihku) {
        this.hntmihku = hntmihku;
    }

    public String getHnftmhku() {
        return hnftmhku;
    }

    public void setHnftmhku(String hnftmhku) {
        this.hnftmhku = hnftmhku;
    }

    public String getNbuzhhku() {
        return nbuzhhku;
    }

    public void setNbuzhhku(String nbuzhhku) {
        this.nbuzhhku = nbuzhhku;
    }

    public Integer getTiqhksdq() {
        return tiqhksdq;
    }

    public void setTiqhksdq(Integer tiqhksdq) {
        this.tiqhksdq = tiqhksdq;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public Integer getKxqzdcsh() {
        return kxqzdcsh;
    }

    public void setKxqzdcsh(Integer kxqzdcsh) {
        this.kxqzdcsh = kxqzdcsh;
    }

    public String getKxqjixgz() {
        return kxqjixgz;
    }

    public void setKxqjixgz(String kxqjixgz) {
        this.kxqjixgz = kxqjixgz;
    }

    public String getKxqhjxgz() {
        return kxqhjxgz;
    }

    public void setKxqhjxgz(String kxqhjxgz) {
        this.kxqhjxgz = kxqhjxgz;
    }

    public String getKxqzyqgz() {
        return kxqzyqgz;
    }

    public void setKxqzyqgz(String kxqzyqgz) {
        this.kxqzyqgz = kxqzyqgz;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getYunxsuoq() {
        return yunxsuoq;
    }

    public void setYunxsuoq(String yunxsuoq) {
        this.yunxsuoq = yunxsuoq;
    }

    public String getKxqshxgz() {
        return kxqshxgz;
    }

    public void setKxqshxgz(String kxqshxgz) {
        this.kxqshxgz = kxqshxgz;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "dkkhczbz='" + dkkhczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "dbdkkksx='" + dbdkkksx + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "hetongje='" + hetongje + '\'' +
                "jiejuuje='" + jiejuuje + '\'' +
                "huankfsh='" + huankfsh + '\'' +
                "dechligz='" + dechligz + '\'' +
                "qigscfsh='" + qigscfsh + '\'' +
                "duophkbz='" + duophkbz + '\'' +
                "dzhhkjih='" + dzhhkjih + '\'' +
                "rchukkbz='" + rchukkbz + '\'' +
                "zdkoukbz='" + zdkoukbz + '\'' +
                "zdjqdkbz='" + zdjqdkbz + '\'' +
                "dhkzhhbz='" + dhkzhhbz + '\'' +
                "qyxhdkbz='" + qyxhdkbz + '\'' +
                "llbgtzjh='" + llbgtzjh + '\'' +
                "dcfktzjh='" + dcfktzjh + '\'' +
                "tqhktzjh='" + tqhktzjh + '\'' +
                "yunxtqhk='" + yunxtqhk + '\'' +
                "xycihkrq='" + xycihkrq + '\'' +
                "leijqjsh='" + leijqjsh + '\'' +
                "meiqhkze='" + meiqhkze + '\'' +
                "meiqhbje='" + meiqhbje + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "xhdkqyzh='" + xhdkqyzh + '\'' +
                "hkqixian='" + hkqixian + '\'' +
                "hkshxubh='" + hkshxubh + '\'' +
                "leijinzh='" + leijinzh + '\'' +
                "baoliuje='" + baoliuje + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "xhdkzhxh='" + xhdkzhxh + '\'' +
                "hkzhouqi='" + hkzhouqi + '\'' +
                "huanbzhq='" + huanbzhq + '\'' +
                "yuqhkzhq='" + yuqhkzhq + '\'' +
                "lstdkhbjh='" + lstdkhbjh_ARRAY + '\'' +
                "lstdkhkfs='" + lstdkhkfs_ARRAY + '\'' +
                "lstdkhkzh='" + lstdkhkzh_ARRAY + '\'' +
                "qxbgtzjh='" + qxbgtzjh + '\'' +
                "tiaozhkf='" + tiaozhkf + '\'' +
                "scihkrbz='" + scihkrbz + '\'' +
                "mqihkfsh='" + mqihkfsh + '\'' +
                "suoqcish='" + suoqcish + '\'' +
                "bzuekkfs='" + bzuekkfs + '\'' +
                "yqbzkkfs='" + yqbzkkfs + '\'' +
                "hntmihku='" + hntmihku + '\'' +
                "hnftmhku='" + hnftmhku + '\'' +
                "nbuzhhku='" + nbuzhhku + '\'' +
                "tiqhksdq='" + tiqhksdq + '\'' +
                "sfyxkuxq='" + sfyxkuxq + '\'' +
                "kuanxqts='" + kuanxqts + '\'' +
                "kxqzdcsh='" + kxqzdcsh + '\'' +
                "kxqjixgz='" + kxqjixgz + '\'' +
                "kxqhjxgz='" + kxqhjxgz + '\'' +
                "kxqzyqgz='" + kxqzyqgz + '\'' +
                "kxqjjrgz='" + kxqjjrgz + '\'' +
                "yunxsuoq='" + yunxsuoq + '\'' +
                "kxqshxgz='" + kxqshxgz + '\'' +
                '}';
    }
}
