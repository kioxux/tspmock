package cn.com.yusys.yusp.web.server.cfg.xdqt0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cfg.xdqt0001.req.Xdqt0001ReqDto;
import cn.com.yusys.yusp.dto.server.cfg.xdqt0001.resp.Xdqt0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.xdqt0001.req.Xdqt0001DataReqDto;
import cn.com.yusys.yusp.server.xdqt0001.resp.Xdqt0001DataRespDto;
import cn.com.yusys.yusp.service.DscmsCfgQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据产品号查询产品名称
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDQT0001:根据产品号查询产品名称")
@RestController
@RequestMapping("/api/dscms")
public class Xdqt0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdqt0001Resource.class);

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    /**
     * 交易码：xdqt0001
     * 交易描述：根据产品号查询产品名称
     *
     * @param xdqt0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据产品号查询产品名称")
    @PostMapping("/xdqt0001")
    //@Idempotent({"xdcaqt0001", "#xdqt0001ReqDto.datasq"})
    protected @ResponseBody
    Xdqt0001RespDto xdqt0001(@Validated @RequestBody Xdqt0001ReqDto xdqt0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, JSON.toJSONString(xdqt0001ReqDto));
        Xdqt0001DataReqDto xdqt0001DataReqDto = new Xdqt0001DataReqDto();// 请求Data： 根据产品号查询产品名称
        Xdqt0001DataRespDto xdqt0001DataRespDto = new Xdqt0001DataRespDto();// 响应Data：根据产品号查询产品名称
        cn.com.yusys.yusp.dto.server.cfg.xdqt0001.req.Data reqData = null; // 请求Data：根据产品号查询产品名称
        cn.com.yusys.yusp.dto.server.cfg.xdqt0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.cfg.xdqt0001.resp.Data();// 响应Data：根据产品号查询产品名称
        Xdqt0001RespDto xdqt0001RespDto = new Xdqt0001RespDto();
        try {
            // 从 xdqt0001ReqDto获取 reqData
            reqData = xdqt0001ReqDto.getData();
            // 将 reqData 拷贝到xdqt0001DataReqDto
            BeanUtils.copyProperties(reqData, xdqt0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, JSON.toJSONString(xdqt0001DataReqDto));
            ResultDto<Xdqt0001DataRespDto> xdqt0001DataResultDto = dscmsCfgQtClientService.xdqt0001(xdqt0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, JSON.toJSONString(xdqt0001DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdqt0001RespDto.setErorcd(Optional.ofNullable(xdqt0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdqt0001RespDto.setErortx(Optional.ofNullable(xdqt0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdqt0001DataResultDto.getCode())) {
                xdqt0001DataRespDto = xdqt0001DataResultDto.getData();
                xdqt0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdqt0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdqt0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdqt0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, e.getMessage());
            xdqt0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdqt0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdqt0001RespDto.setDatasq(servsq);

        xdqt0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDQT0001.key, DscmsEnum.TRADE_CODE_XDQT0001.value, JSON.toJSONString(xdqt0001RespDto));
        return xdqt0001RespDto;
    }
}
