package cn.com.yusys.yusp.web.server.biz.xdxw0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0001.req.Xdxw0001ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0001.resp.Xdxw0001RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0001.req.Xdxw0001DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0001.resp.Xdxw0001DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:房屋估价信息同步
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDXW0001:房屋估价信息同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0001Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0001
     * 交易描述：房屋估价信息同步
     *
     * @param xdxw0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("房屋估价信息同步")
    @PostMapping("/xdxw0001")
    //@Idempotent({"xdcaxw0001", "#xdxw0001ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0001RespDto xdxw0001(@Validated @RequestBody Xdxw0001ReqDto xdxw0001ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001ReqDto));
        Xdxw0001DataReqDto xdxw0001DataReqDto = new Xdxw0001DataReqDto();// 请求Data： 房屋估价信息同步
        Xdxw0001DataRespDto xdxw0001DataRespDto = new Xdxw0001DataRespDto();// 响应Data：房屋估价信息同步
        Xdxw0001RespDto xdxw0001RespDto = new Xdxw0001RespDto();// 响应Data：房屋估价信息同步
        cn.com.yusys.yusp.dto.server.biz.xdxw0001.req.Data reqData = null; // 请求Data：房屋估价信息同步
        cn.com.yusys.yusp.dto.server.biz.xdxw0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0001.resp.Data();// 响应Data：房屋估价信息同步
        try {
            // 从 xdxw0001ReqDto获取 reqData
            reqData = xdxw0001ReqDto.getData();
            // 将 reqData 拷贝到xdxw0001DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0001DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001ReqDto));
            ResultDto<Xdxw0001DataRespDto> xdxw0001DataResultDto = dscmsBizXwClientService.xdxw0001(xdxw0001DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001DataResultDto));

            // 从返回值中获取响应码和响应消息
            xdxw0001RespDto.setErorcd(Optional.ofNullable(xdxw0001DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0001RespDto.setErortx(Optional.ofNullable(xdxw0001DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0001DataResultDto.getCode())) {
                xdxw0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdxw0001DataRespDto = xdxw0001DataResultDto.getData();
                // 将 xdxw0001DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0001DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, e.getMessage());
            xdxw0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0001RespDto.setDatasq(servsq);

        xdxw0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0001.key, DscmsEnum.TRADE_CODE_XDXW0001.value, JSON.toJSONString(xdxw0001RespDto));
        return xdxw0001RespDto;
    }
}
