package cn.com.yusys.yusp.web.client.esb.gjjs.xdgj08;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj08.Xdgj08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj08.Xdgj08RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.gjjs.xdgj08.req.Xdgj08ReqService;
import cn.com.yusys.yusp.online.client.esb.gjjs.xdgj08.resp.Xdgj08RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷还款信息
 **/
@Api(tags = "BSP封装调用国际结算系统的接口处理类(xdgj08)")
@RestController
@RequestMapping("/api/dscms2gjjs")
public class Dscms2Xdgj08Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xdgj08Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdgj08
     * 交易描述：信贷还款信息
     *
     * @param xdgj08ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdgj08:信贷还款信息")
    @PostMapping("/xdgj08")
    protected @ResponseBody
    ResultDto<Xdgj08RespDto> xdgj08(@Validated @RequestBody Xdgj08ReqDto xdgj08ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDGJ08.key, EsbEnum.TRADE_CODE_XDGJ08.value, JSON.toJSONString(xdgj08ReqDto));

        cn.com.yusys.yusp.online.client.esb.gjjs.xdgj08.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.gjjs.xdgj08.req.Service();
        cn.com.yusys.yusp.online.client.esb.gjjs.xdgj08.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.gjjs.xdgj08.resp.Service();

        Xdgj08ReqService xdgj08ReqService = new Xdgj08ReqService();
        Xdgj08RespService xdgj08RespService = new Xdgj08RespService();
        Xdgj08RespDto xdgj08RespDto = new Xdgj08RespDto();
        ResultDto<Xdgj08RespDto> xdgj08ResultDto = new ResultDto<Xdgj08RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdgj08ReqDto转换成reqService
            BeanUtils.copyProperties(xdgj08ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDGJ08.key);//    交易码
            LocalDateTime now = LocalDateTime.now();
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_GJP.key, EsbEnum.SERVTP_GJP.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_GJP.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_GJP.key, EsbEnum.SERVTP_GJP.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_GJJS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_GJJS.key);//    部门号
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xdgj08ReqService.setService(reqService);
            // 将xdgj08ReqService转换成xdgj08ReqServiceMap
            Map xdgj08ReqServiceMap = beanMapUtil.beanToMap(xdgj08ReqService);
            context.put("tradeDataMap", xdgj08ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDGJ08.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdgj08RespService = beanMapUtil.mapToBean(tradeDataMap, Xdgj08RespService.class, Xdgj08RespService.class);
            respService = xdgj08RespService.getService();

            xdgj08ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdgj08ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xdgj08RespDto
                BeanUtils.copyProperties(respService, xdgj08RespDto);
                xdgj08ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdgj08ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdgj08ResultDto.setCode(EpbEnum.EPB099999.key);
                xdgj08ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDGJ08.key, EsbEnum.TRADE_CODE_XDGJ08.value, e.getMessage());
            xdgj08ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdgj08ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdgj08ResultDto.setData(xdgj08RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDGJ08.key, EsbEnum.TRADE_CODE_XDGJ08.value, JSON.toJSONString(xdgj08RespDto));
        return xdgj08ResultDto;
    }
}
