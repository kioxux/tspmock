package cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp;

/**
 * 响应Service：对公及同业客户清单查询
 */
public class G10501RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
