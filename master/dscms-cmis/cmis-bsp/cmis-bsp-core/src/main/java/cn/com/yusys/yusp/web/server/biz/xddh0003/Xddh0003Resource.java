package cn.com.yusys.yusp.web.server.biz.xddh0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0003.req.Xddh0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0003.resp.Xddh0003RespDto;
import cn.com.yusys.yusp.dto.server.xddh0003.req.Xddh0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0003.resp.Xddh0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.alibaba.fastjson.JSON;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:主动还款申请记录列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0003:主动还款申请记录列表查询")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0003Resource.class);
	@Autowired
	private DscmsBizDhClientService dscmsBizDhClientService;
    /**
     * 交易码：xddh0003
     * 交易描述：主动还款申请记录列表查询
     *
     * @param xddh0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("主动还款申请记录列表查询")
    @PostMapping("/xddh0003")
    //@Idempotent({"xddh0003", "#xddh0003ReqDto.datasq"})
    protected @ResponseBody
    Xddh0003RespDto xddh0003(@Validated @RequestBody Xddh0003ReqDto xddh0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, JSON.toJSONString(xddh0003ReqDto));
        Xddh0003DataReqDto xddh0003DataReqDto = new Xddh0003DataReqDto();// 请求Data： 主动还款申请记录列表查询
        Xddh0003DataRespDto xddh0003DataRespDto = new Xddh0003DataRespDto();// 响应Data：主动还款申请记录列表查询
        Xddh0003RespDto xddh0003RespDto = new Xddh0003RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0003.req.Data reqData = null; // 请求Data：主动还款申请记录列表查询
        cn.com.yusys.yusp.dto.server.biz.xddh0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0003.resp.Data();// 响应Data：主动还款申请记录列表查询
        try {
            // 从 xddh0003ReqDto获取 reqData
            reqData = xddh0003ReqDto.getData();
            // 将 reqData 拷贝到xddh0003DataReqDto
            BeanUtils.copyProperties(reqData, xddh0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, JSON.toJSONString(xddh0003DataReqDto));
            ResultDto<Xddh0003DataRespDto> xddh0003DataResultDto = dscmsBizDhClientService.xddh0003(xddh0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, JSON.toJSONString(xddh0003DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0003RespDto.setErorcd(Optional.ofNullable(xddh0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0003RespDto.setErortx(Optional.ofNullable(xddh0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0003DataResultDto.getCode())) {
                xddh0003DataRespDto = xddh0003DataResultDto.getData();
                xddh0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, e.getMessage());
            xddh0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0003RespDto.setDatasq(servsq);

        xddh0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0003.key, DscmsEnum.TRADE_CODE_XDDH0003.value, JSON.toJSONString(xddh0003RespDto));
        return xddh0003RespDto;
    }
}
