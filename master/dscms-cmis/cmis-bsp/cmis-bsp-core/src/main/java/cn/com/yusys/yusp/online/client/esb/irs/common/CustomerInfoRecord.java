package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.math.BigDecimal;

/**
 * 客户信息(CustomerInfo)
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 10:13
 */
public class CustomerInfoRecord {

    private String cus_id; // 客户号
    private String cus_name; // 客户名称
    private String cus_type; // 客户类型
    private String cert_type; // 证件类型
    private String cert_code; // 证件号码
    private String bus_owner; // 企业所有制
    private String new_industry_type; // 所属国标行业
    private String bas_acc_flg; // 基本户是否在本行
    private BigDecimal assets; // 资产负债率
    private String grade; // 本行即期信用等级
    private String reg_area_code; // 注册地行政区划代码
    private String reg_area_name; // 注册地行政区划名称
    private String cust_mgr; // 主管客户经理
    private String main_br_id; // 主管机构

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getBus_owner() {
        return bus_owner;
    }

    public void setBus_owner(String bus_owner) {
        this.bus_owner = bus_owner;
    }

    public String getNew_industry_type() {
        return new_industry_type;
    }

    public void setNew_industry_type(String new_industry_type) {
        this.new_industry_type = new_industry_type;
    }

    public String getBas_acc_flg() {
        return bas_acc_flg;
    }

    public void setBas_acc_flg(String bas_acc_flg) {
        this.bas_acc_flg = bas_acc_flg;
    }

    public BigDecimal getAssets() {
        return assets;
    }

    public void setAssets(BigDecimal assets) {
        this.assets = assets;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getReg_area_code() {
        return reg_area_code;
    }

    public void setReg_area_code(String reg_area_code) {
        this.reg_area_code = reg_area_code;
    }

    public String getReg_area_name() {
        return reg_area_name;
    }

    public void setReg_area_name(String reg_area_name) {
        this.reg_area_name = reg_area_name;
    }

    public String getCust_mgr() {
        return cust_mgr;
    }

    public void setCust_mgr(String cust_mgr) {
        this.cust_mgr = cust_mgr;
    }

    public String getMain_br_id() {
        return main_br_id;
    }

    public void setMain_br_id(String main_br_id) {
        this.main_br_id = main_br_id;
    }

    @Override
    public String toString() {
        return "CustomerInfo{" +
                "cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cus_type='" + cus_type + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", bus_owner='" + bus_owner + '\'' +
                ", new_industry_type='" + new_industry_type + '\'' +
                ", bas_acc_flg='" + bas_acc_flg + '\'' +
                ", assets=" + assets +
                ", grade='" + grade + '\'' +
                ", reg_area_code='" + reg_area_code + '\'' +
                ", reg_area_name='" + reg_area_name + '\'' +
                ", cust_mgr='" + cust_mgr + '\'' +
                ", main_br_id='" + main_br_id + '\'' +
                '}';
    }
}
