package cn.com.yusys.yusp.online.client.esb.circp.fb1167.req;

/**
 * 请求Service：企业征信查询通知
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1167ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1167ReqService{" +
                "service=" + service +
                '}';
    }
}

