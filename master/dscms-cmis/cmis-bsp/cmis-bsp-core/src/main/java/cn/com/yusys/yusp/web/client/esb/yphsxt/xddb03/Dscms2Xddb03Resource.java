package cn.com.yusys.yusp.web.client.esb.yphsxt.xddb03;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb03.req.Xddb03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb03.resp.Xddb03RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.req.Xddb03ReqService;
import cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.resp.Xddb03RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询存单票据信息
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2yphsxt")
public class Dscms2Xddb03Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xddb03Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xddb03
     * 交易描述：查询存单票据信息
     *
     * @param xddb03ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/xddb03")
    protected @ResponseBody
    ResultDto<Xddb03RespDto> xddb03(@Validated @RequestBody Xddb03ReqDto xddb03ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB03.key, EsbEnum.TRADE_CODE_XDDB03.value, JSON.toJSONString(xddb03ReqDto));
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.req.Service();
        cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.yphsxt.xddb03.resp.Service();
        Xddb03ReqService xddb03ReqService = new Xddb03ReqService();
        Xddb03RespService xddb03RespService = new Xddb03RespService();
        Xddb03RespDto xddb03RespDto = new Xddb03RespDto();
        ResultDto<Xddb03RespDto> xddb03ResultDto = new ResultDto<Xddb03RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xddb03ReqDto转换成reqService
            BeanUtils.copyProperties(xddb03ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDDB03.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xddb03ReqService.setService(reqService);
            // 将xddb03ReqService转换成xddb03ReqServiceMap
            Map xddb03ReqServiceMap = beanMapUtil.beanToMap(xddb03ReqService);
            context.put("tradeDataMap", xddb03ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB03.key, EsbEnum.TRADE_CODE_XDDB03.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDDB03.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB03.key, EsbEnum.TRADE_CODE_XDDB03.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xddb03RespService = beanMapUtil.mapToBean(tradeDataMap, Xddb03RespService.class, Xddb03RespService.class);
            respService = xddb03RespService.getService();
            //  将respService转换成Xddb03RespDto
            xddb03ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            xddb03ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, xddb03RespDto);
                xddb03ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xddb03ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xddb03ResultDto.setCode(EpbEnum.EPB099999.key);
                xddb03ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB03.key, EsbEnum.TRADE_CODE_XDDB03.value, e.getMessage());
            xddb03ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xddb03ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xddb03ResultDto.setData(xddb03RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDDB03.key, EsbEnum.TRADE_CODE_XDDB03.value, JSON.toJSONString(xddb03ResultDto));
        return xddb03ResultDto;
    }
}
