package cn.com.yusys.yusp.web.server.cus.xdkh0014;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0014.req.Xdkh0014ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0014.resp.Xdkh0014RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0014.req.Xdkh0014DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0014.resp.Xdkh0014DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:省心E付白名单信息维护
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDKH0014:省心E付白名单信息维护")
@RestController
@RequestMapping("/api/dscms")
@RefreshScope
public class Xdkh0014Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0014Resource.class);

    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0014
     * 交易描述：省心E付白名单信息维护
     *
     * @param xdkh0014ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdkh0013:省心E付白名单信息维护")
    @PostMapping("/xdkh0014")
    //@Idempotent({"xdcakh0014", "#xdkh0014ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0014RespDto xdkh0014(@Validated @RequestBody Xdkh0014ReqDto xdkh0014ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0014.key, DscmsEnum.TRADE_CODE_XDKH0014.value, JSON.toJSONString(xdkh0014ReqDto));
        Xdkh0014DataReqDto xdkh0014DataReqDto = new Xdkh0014DataReqDto();// 请求Data： 省心E付白名单信息维护
        Xdkh0014DataRespDto xdkh0014DataRespDto = new Xdkh0014DataRespDto();// 响应Data：省心E付白名单信息维护
        Xdkh0014RespDto xdkh0014RespDto = new Xdkh0014RespDto();// 响应DTO：省心E付白名单信息维护
        cn.com.yusys.yusp.dto.server.cus.xdkh0014.req.Data reqData = null; // 请求Data：省心E付白名单信息维护
        cn.com.yusys.yusp.dto.server.cus.xdkh0014.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0014.resp.Data();// 响应Data：省心E付白名单信息维护
        try {
            // 从 xdkh0014ReqDto获取 reqData
            reqData = xdkh0014ReqDto.getData();
            // 将 reqData 拷贝到xdkh0014DataReqDto
            org.springframework.beans.BeanUtils.copyProperties(reqData, xdkh0014DataReqDto);
            // 调用服务：
            ResultDto<Xdkh0014DataRespDto> xdkh0014DataResultDto = dscmsCusClientService.xdkh0014(xdkh0014DataReqDto);
            // 从返回值中获取响应码和响应消息
            xdkh0014RespDto.setErorcd(Optional.ofNullable(xdkh0014DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0014RespDto.setErortx(Optional.ofNullable(xdkh0014DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0014DataResultDto.getCode())) {
                xdkh0014DataRespDto = xdkh0014DataResultDto.getData();
                // 将 xdkh0014DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0014DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0014.key, DscmsEnum.TRADE_CODE_XDKH0014.value, e.getMessage());
            xdkh0014RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0014RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0014RespDto.setDatasq(servsq);
        xdkh0014RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0014.key, DscmsEnum.TRADE_CODE_XDKH0014.value, JSON.toJSONString(xdkh0014RespDto));
        return xdkh0014RespDto;
    }
}
