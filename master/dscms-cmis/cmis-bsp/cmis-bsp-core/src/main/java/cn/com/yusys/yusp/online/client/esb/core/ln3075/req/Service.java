package cn.com.yusys.yusp.online.client.esb.core.ln3075.req;

import java.math.BigDecimal;

/**
 * 请求Service：逾期贷款展期
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private BigDecimal zhanqixh;//展期序号
    private String zhanqirq;//展期日期
    private BigDecimal zhanqije;//展期金额
    private String zhanqhth;//展期合同号
    private String zhanqdqr;//展期到期日
    private String lilvtzzq;//利率调整周期
    private String lilvtzfs;//利率调整方式
    private String lilvleix;//利率类型
    private BigDecimal lilvfdzh;//利率浮动值
    private String lilvfdfs;//利率浮动方式
    private String kehuzwmc;//客户名
    private String kehuhaoo;//客户号
    private String huobdhao;//货币代号
    private String hetongbh;//合同编号
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private String daikczbz;//业务操作标志
    private BigDecimal zhchlilv;//正常利率

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public BigDecimal getZhanqixh() {
        return zhanqixh;
    }

    public void setZhanqixh(BigDecimal zhanqixh) {
        this.zhanqixh = zhanqixh;
    }

    public String getZhanqirq() {
        return zhanqirq;
    }

    public void setZhanqirq(String zhanqirq) {
        this.zhanqirq = zhanqirq;
    }

    public BigDecimal getZhanqije() {
        return zhanqije;
    }

    public void setZhanqije(BigDecimal zhanqije) {
        this.zhanqije = zhanqije;
    }

    public String getZhanqhth() {
        return zhanqhth;
    }

    public void setZhanqhth(String zhanqhth) {
        this.zhanqhth = zhanqhth;
    }

    public String getZhanqdqr() {
        return zhanqdqr;
    }

    public void setZhanqdqr(String zhanqdqr) {
        this.zhanqdqr = zhanqdqr;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", zhanqixh=" + zhanqixh +
                ", zhanqirq='" + zhanqirq + '\'' +
                ", zhanqije=" + zhanqije +
                ", zhanqhth='" + zhanqhth + '\'' +
                ", zhanqdqr='" + zhanqdqr + '\'' +
                ", lilvtzzq='" + lilvtzzq + '\'' +
                ", lilvtzfs='" + lilvtzfs + '\'' +
                ", lilvleix='" + lilvleix + '\'' +
                ", lilvfdzh=" + lilvfdzh +
                ", lilvfdfs='" + lilvfdfs + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", daikczbz='" + daikczbz + '\'' +
                ", zhchlilv=" + zhchlilv +
                '}';
    }
}
