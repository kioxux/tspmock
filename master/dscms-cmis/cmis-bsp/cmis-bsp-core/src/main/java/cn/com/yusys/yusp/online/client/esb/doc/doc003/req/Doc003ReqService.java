package cn.com.yusys.yusp.online.client.esb.doc.doc003.req;

/**
 * 请求Service：出库
 *
 * @author chenyong
 * @version 1.0
 */
public class Doc003ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Doc003ReqService{" +
                "service=" + service +
                '}';
    }
}
