package cn.com.yusys.yusp.web.server.biz.xddb0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0005.req.Xddb0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0005.resp.Xddb0005RespDto;
import cn.com.yusys.yusp.dto.server.xddb0005.req.Xddb0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0005.resp.Xddb0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:重估任务分配同步
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDDB0005:重估任务分配同步")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0005Resource.class);

    @Autowired
    private DscmsBizDbClientService dscmsBizdbClientService;
    /**
     * 交易码：xddb0005
     * 交易描述：重估任务分配同步
     *
     * @param xddb0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("重估任务分配同步")
    @PostMapping("/xddb0005")
    //@Idempotent({"xddb0005", "#xddb0005ReqDto.datasq"})
    protected @ResponseBody
    Xddb0005RespDto xddb0005(@Validated @RequestBody Xddb0005ReqDto xddb0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddb0005ReqDto));
        Xddb0005DataReqDto xddb0005DataReqDto = new Xddb0005DataReqDto();// 请求Data： 重估任务分配同步
        Xddb0005DataRespDto xddb0005DataRespDto = new Xddb0005DataRespDto();// 响应Data：重估任务分配同步
        Xddb0005RespDto xddb0005RespDto = new Xddb0005RespDto();

        cn.com.yusys.yusp.dto.server.biz.xddb0005.req.Data reqData = null; // 请求Data：重估任务分配同步
        cn.com.yusys.yusp.dto.server.biz.xddb0005.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0005.resp.Data();// 响应Data：重估任务分配同步

        try {
            // 从 xddb0005ReqDto获取 reqData
            reqData = xddb0005ReqDto.getData();
            // 将 reqData 拷贝到xddb0005DataReqDto
            BeanUtils.copyProperties(reqData, xddb0005DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddb0005DataReqDto));
            ResultDto<Xddb0005DataRespDto> xddb0005DataResultDto = dscmsBizdbClientService.xddb0005(xddb0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddb0005DataRespDto));
            // 从返回值中获取响应码和响应消息
            xddb0005RespDto.setErorcd(Optional.ofNullable(xddb0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0005RespDto.setErortx(Optional.ofNullable(xddb0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0005DataResultDto.getCode())) {
                xddb0005DataRespDto = xddb0005DataResultDto.getData();
                xddb0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, e.getMessage());
            xddb0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0005RespDto.setDatasq(servsq);

        xddb0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0005.key, DscmsEnum.TRADE_CODE_XDDB0005.value, JSON.toJSONString(xddb0005RespDto));
        return xddb0005RespDto;
    }
}
