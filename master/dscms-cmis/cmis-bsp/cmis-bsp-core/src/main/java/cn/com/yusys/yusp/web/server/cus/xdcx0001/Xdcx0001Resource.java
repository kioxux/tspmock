package cn.com.yusys.yusp.web.server.cus.xdcx0001;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cmiscus0024.req.CmisCus0024ReqDto;
import cn.com.yusys.yusp.dto.server.cmiscus0024.resp.CmisCus0024RespDto;
import cn.com.yusys.yusp.dto.server.cus.xdcx0001.req.Xdcx0001ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdcx0001.resp.Xdcx0001RespDto;
import cn.com.yusys.yusp.enums.online.DscmsCfgEnum;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.server.cmiscfg0004.req.CmisCfg0004ReqDto;
import cn.com.yusys.yusp.server.cmiscfg0004.resp.CmisCfg0004RespDto;
import cn.com.yusys.yusp.service.CmisCusClientService;
import cn.com.yusys.yusp.service.DscmsCfgQtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

/**
 * 接口处理类:根据行号查询同业客户号
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDCX0001:根据行号查询同业客户号")
@RestController
@RequestMapping("/api/dscms")
public class Xdcx0001Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcx0001Resource.class);

    @Autowired
    private DscmsCfgQtClientService dscmsCfgQtClientService;

    @Autowired
    private CmisCusClientService cmisCusClientService;

    /**
     * 交易码：xdcx0001
     * 交易描述：根据行号查询同业客户号
     *
     * @param xdcx0001ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据行号查询同业客户号")
    @PostMapping("/xdcx0001")
    @Idempotent({"xdcx0001", "#xdcx0001ReqDto.datasq"})
    protected @ResponseBody
    Xdcx0001RespDto xdcx0001(@Validated @RequestBody Xdcx0001ReqDto xdcx0001ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCX0001.key, DscmsEnum.TRADE_CODE_XDCX0001.value, JSON.toJSONString(xdcx0001ReqDto));
        Xdcx0001RespDto xdcx0001RespDto = new Xdcx0001RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdcx0001.req.Data reqData = null; // 请求Data：根据行号查询同业客户号
        cn.com.yusys.yusp.dto.server.cus.xdcx0001.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdcx0001.resp.Data();// 响应Data：根据行号查询同业客户号
        try {
            if (StringUtils.isBlank(xdcx0001ReqDto.getData().getBankNo()) && StringUtils.isBlank(xdcx0001ReqDto.getData().getBicCode())) {
                xdcx0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                xdcx0001RespDto.setErortx("行号和分支行号至少有一个不为空！");// 系统异常
                return xdcx0001RespDto;
            }
            if (StringUtils.isNotBlank(xdcx0001ReqDto.getData().getBankNo())) {//如果行号不为空，则以行号为准，调用cmiscfg0004获取对应总行行号
                CmisCfg0004ReqDto cmisCfg0004ReqDto = new CmisCfg0004ReqDto();
                cmisCfg0004ReqDto.setBankNo(xdcx0001ReqDto.getData().getBankNo());
                logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(cmisCfg0004ReqDto));
                ResultDto<CmisCfg0004RespDto> cmisCfg0004RespDtoResultDto = dscmsCfgQtClientService.cmisCfg0004(cmisCfg0004ReqDto);
                logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(cmisCfg0004RespDtoResultDto));
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisCfg0004RespDtoResultDto.getCode())) {
                    CmisCfg0004RespDto data = cmisCfg0004RespDtoResultDto.getData();
                    String bankNo = data.getSuperBankNo();
                    if (StringUtils.isNotBlank(bankNo)) {
                        CmisCus0024ReqDto cmisCus0024ReqDto = new CmisCus0024ReqDto();
                        cmisCus0024ReqDto.setBankNo(bankNo);
                        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(cmisCus0024ReqDto));
                        ResultDto<CmisCus0024RespDto> cmisCus0024RespDtoResultDto = cmisCusClientService.cmiscus0024(cmisCus0024ReqDto);
                        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(cmisCus0024RespDtoResultDto));
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisCus0024RespDtoResultDto.getCode())) {
                            CmisCus0024RespDto data1 = cmisCus0024RespDtoResultDto.getData();
                            String cusId = data1.getCusId();
                            respData.setCusId(cusId);
                            xdcx0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                            xdcx0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                        }
                    } else {
                        xdcx0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                        xdcx0001RespDto.setErortx("未查询到该行信息，请通知新信贷维护");// 系统异常
                        return xdcx0001RespDto;
                    }

                }
            } else {
                CmisCfg0004ReqDto cmisCfg0004ReqDto = new CmisCfg0004ReqDto();
                cmisCfg0004ReqDto.setBicCode(xdcx0001ReqDto.getData().getBicCode());
                logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(cmisCfg0004ReqDto));
                ResultDto<CmisCfg0004RespDto> cmisCfg0004RespDtoResultDto = dscmsCfgQtClientService.cmisCfg0004(cmisCfg0004ReqDto);
                logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.key, DscmsCfgEnum.TRADE_CODE_CMISCFG0004.value, JSON.toJSONString(cmisCfg0004RespDtoResultDto));
                if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisCfg0004RespDtoResultDto.getCode())) {
                    CmisCfg0004RespDto data = cmisCfg0004RespDtoResultDto.getData();
                    String superBicCode = data.getSuperBicCode();
                    if (Objects.nonNull(superBicCode)) {
                        CmisCus0024ReqDto cmisCus0024ReqDto = new CmisCus0024ReqDto();
                        cmisCus0024ReqDto.setBicCode(superBicCode);
                        logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0024.key, DscmsEnum.TRADE_CODE_CMISCUS0024.value, JSON.toJSONString(cmisCus0024ReqDto));
                        ResultDto<CmisCus0024RespDto> cmisCus0024RespDtoResultDto = cmisCusClientService.cmiscus0024(cmisCus0024ReqDto);
                        logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_CMISCUS0024.key, DscmsEnum.TRADE_CODE_CMISCUS0024.value, JSON.toJSONString(cmisCus0024RespDtoResultDto));
                        if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, cmisCus0024RespDtoResultDto.getCode())) {
                            CmisCus0024RespDto data1 = cmisCus0024RespDtoResultDto.getData();
                            String cusId = data1.getCusId();
                            respData.setCusId(cusId);
                            xdcx0001RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                            xdcx0001RespDto.setErortx(SuccessEnum.SUCCESS.value);
                        } else {
                            xdcx0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                            xdcx0001RespDto.setErortx("未查询到该行信息，请通知新信贷维护");// 系统异常
                            return xdcx0001RespDto;
                        }
                    }

                }
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCX0001.key, DscmsEnum.TRADE_CODE_XDCX0001.value, e.getMessage());
            xdcx0001RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcx0001RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcx0001RespDto.setDatasq(servsq);

        xdcx0001RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCX0001.key, DscmsEnum.TRADE_CODE_XDCX0001.value, JSON.toJSONString(xdcx0001RespDto));
        return xdcx0001RespDto;
    }
}
