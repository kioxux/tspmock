package cn.com.yusys.yusp.online.client.esb.core.ln3105.resp;


/**
 * 响应Dto：贷款期供交易明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private String dkzhangh;//贷款账号
    private String dkjiejuh;//贷款借据号
    private Integer zongbish;//总笔数
    private List lstqgmx_ARRAY;//贷款期供明细

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getZongbish() {
        return zongbish;
    }

    public void setZongbish(Integer zongbish) {
        this.zongbish = zongbish;
    }

    public List getLstqgmx_ARRAY() {
        return lstqgmx_ARRAY;
    }

    public void setLstqgmx_ARRAY(List lstqgmx_ARRAY) {
        this.lstqgmx_ARRAY = lstqgmx_ARRAY;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkzhangh='" + dkzhangh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", zongbish=" + zongbish +
                ", lstqgmx_ARRAY=" + lstqgmx_ARRAY +
                '}';
    }
}
