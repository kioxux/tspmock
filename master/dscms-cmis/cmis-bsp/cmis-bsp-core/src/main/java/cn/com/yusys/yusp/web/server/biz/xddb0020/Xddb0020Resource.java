package cn.com.yusys.yusp.web.server.biz.xddb0020;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddb0020.req.Xddb0020ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddb0020.resp.Xddb0020RespDto;
import cn.com.yusys.yusp.dto.server.xddb0020.req.Xddb0020DataReqDto;
import cn.com.yusys.yusp.dto.server.xddb0020.resp.Xddb0020DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDbClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户名查询抵押物类型
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDB0020:根据客户名查询抵押物类型")
@RestController
@RequestMapping("/api/dscms")
public class Xddb0020Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddb0020Resource.class);
    @Autowired
    private DscmsBizDbClientService dscmsBizDbClientService;

    /**
     * 交易码：xddb0020
     * 交易描述：根据客户名查询抵押物类型
     *
     * @param xddb0020ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户名查询抵押物类型")
    @PostMapping("/xddb0020")
    //@Idempotent({"xddb0020", "#xddb0020ReqDto.datasq"})
    protected @ResponseBody
    Xddb0020RespDto xddb0020(@Validated @RequestBody Xddb0020ReqDto xddb0020ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020ReqDto));
        Xddb0020DataReqDto xddb0020DataReqDto = new Xddb0020DataReqDto();// 请求Data： 根据客户名查询抵押物类型
        Xddb0020DataRespDto xddb0020DataRespDto = new Xddb0020DataRespDto();// 响应Data：根据客户名查询抵押物类型
        Xddb0020RespDto xddb0020RespDto = new Xddb0020RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddb0020.req.Data reqData = null; // 请求Data：根据客户名查询抵押物类型
        cn.com.yusys.yusp.dto.server.biz.xddb0020.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddb0020.resp.Data();// 响应Data：根据客户名查询抵押物类型

        try {
            // 从 xddb0020ReqDto获取 reqData
            reqData = xddb0020ReqDto.getData();
            // 将 reqData 拷贝到xddb0020DataReqDto
            BeanUtils.copyProperties(reqData, xddb0020DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020DataReqDto));
            ResultDto<Xddb0020DataRespDto> xddb0020DataResultDto = dscmsBizDbClientService.xddb0020(xddb0020DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddb0020RespDto.setErorcd(Optional.ofNullable(xddb0020DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddb0020RespDto.setErortx(Optional.ofNullable(xddb0020DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddb0020DataResultDto.getCode())) {
                xddb0020DataRespDto = xddb0020DataResultDto.getData();
                xddb0020RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddb0020RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddb0020DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddb0020DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, e.getMessage());
            xddb0020RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddb0020RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddb0020RespDto.setDatasq(servsq);

        xddb0020RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDB0020.key, DscmsEnum.TRADE_CODE_XDDB0020.value, JSON.toJSONString(xddb0020RespDto));
        return xddb0020RespDto;
    }
}
