package cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.resp;

/**
 * 响应Service：信贷审批结果同步接口
 * @author lihh
 * @version 1.0
 */
public class Service {
	private String erorcd;//错误码
	private String erortx;//错误描述

	public String getErorcd() {
		return erorcd;
	}

	public void setErorcd(String erorcd) {
		this.erorcd = erorcd;
	}

	public String getErortx() {
		return erortx;
	}

	public void setErortx(String erortx) {
		this.erortx = erortx;
	}

	@Override
	public String toString() {
		return "Service{" +
				"erorcd='" + erorcd + '\'' +
				", erortx='" + erortx + '\'' +
				'}';
	}
}
