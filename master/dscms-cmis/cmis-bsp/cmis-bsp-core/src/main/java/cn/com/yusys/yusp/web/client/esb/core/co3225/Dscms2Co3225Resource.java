package cn.com.yusys.yusp.web.client.esb.core.co3225;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.co3225.req.Co3225ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3225.resp.Co3225RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.co3225.req.Co3225ReqService;
import cn.com.yusys.yusp.online.client.esb.core.co3225.resp.Co3225RespService;

import cn.com.yusys.yusp.online.client.esb.core.co3225.resp.Record;
import cn.com.yusys.yusp.online.client.esb.core.co3225.resp.lstdzywmx_ARRAY;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(co3225)")
@RestController
@RequestMapping("/api/dscms2coreco")
public class Dscms2Co3225Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Co3225Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 抵质押物明细查询（处理码co3225）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("co3225:抵质押物明细查询")
    @PostMapping("/co3225")
    protected @ResponseBody
    ResultDto<Co3225RespDto> co3225(@Validated @RequestBody Co3225ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3225.key, EsbEnum.TRADE_CODE_CO3225.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.co3225.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.co3225.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.co3225.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.co3225.resp.Service();
        Co3225ReqService co3225ReqService = new Co3225ReqService();
        Co3225RespService co3225RespService = new Co3225RespService();
        Co3225RespDto co3225RespDto = new Co3225RespDto();
        ResultDto<Co3225RespDto> co3225ResultDto = new ResultDto<Co3225RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Co3225ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            // 塞入报文头固定字段
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CO3225.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            co3225ReqService.setService(reqService);
            // 将co3225ReqService转换成co3225ReqServiceMap
            Map co3225ReqServiceMap = beanMapUtil.beanToMap(co3225ReqService);
            context.put("tradeDataMap", co3225ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3225.key, EsbEnum.TRADE_CODE_CO3225.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CO3225.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3225.key, EsbEnum.TRADE_CODE_CO3225.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            co3225RespService = beanMapUtil.mapToBean(tradeDataMap, Co3225RespService.class, Co3225RespService.class);
            respService = co3225RespService.getService();

            //  将Co3225RespDto封装到ResultDto<Co3225RespDto>
            co3225ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            co3225ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Co3225RespDto
                BeanUtils.copyProperties(respService, co3225RespDto);
                lstdzywmx_ARRAY lstdzywmx = Optional.ofNullable(respService.getLstdzywmx_ARRAY()).orElse(new lstdzywmx_ARRAY());
                if (CollectionUtils.nonEmpty(lstdzywmx.getRecord())) {
                    List<cn.com.yusys.yusp.dto.client.esb.core.co3225.resp.Lstdzywmx> targetList = new ArrayList<>();
                    List<Record> recordList = respService.getLstdzywmx_ARRAY().getRecord();
                    for (Record record : recordList) {
                        cn.com.yusys.yusp.dto.client.esb.core.co3225.resp.Lstdzywmx target = new cn.com.yusys.yusp.dto.client.esb.core.co3225.resp.Lstdzywmx();
                        BeanUtils.copyProperties(record, target);
                        targetList.add(target);
                    }
                    co3225RespDto.setLstdzywmx(targetList);
                }
                co3225ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                co3225ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                co3225ResultDto.setCode(EpbEnum.EPB099999.key);
                co3225ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3225.key, EsbEnum.TRADE_CODE_CO3225.value, e.getMessage());
            co3225ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            co3225ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        co3225ResultDto.setData(co3225RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3225.key, EsbEnum.TRADE_CODE_CO3225.value, JSON.toJSONString(co3225ResultDto));
        return co3225ResultDto;
    }
}
