package cn.com.yusys.yusp.web.client.esb.ypqzxt.cwm004;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm004.req.Cwm004ReqService;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm004.resp.Cwm004RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用押品权证系统的接口
 */
@Api(tags = "BSP封装调用押品权证系统的接口(cwm004)")
@RestController
@RequestMapping("/api/dscms2ypqzxt")
public class Dscms2Cwm004Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Cwm004Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 押品权证出库接口（处理码cwm004）
     *
     * @param cwm004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cwm004:押品权证出库接口")
    @PostMapping("/cwm004")
    protected @ResponseBody
    ResultDto<Cwm004RespDto> cwm004(@Validated @RequestBody Cwm004ReqDto cwm004ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM004.key, EsbEnum.TRADE_CODE_CWM004.value, JSON.toJSONString(cwm004ReqDto));
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm004.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm004.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm004.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm004.resp.Service();
        Cwm004ReqService cwm004ReqService = new Cwm004ReqService();
        Cwm004RespService cwm004RespService = new Cwm004RespService();
        Cwm004RespDto cwm004RespDto = new Cwm004RespDto();
        ResultDto<Cwm004RespDto> cwm004ResultDto = new ResultDto<Cwm004RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Cwm004qReqDto转换成reqService
            BeanUtils.copyProperties(cwm004ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CWM004.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPQZXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPQZXT.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            cwm004ReqService.setService(reqService);
            // 将cwm004ReqService转换成cwm004ReqServiceMap
            Map cwm004ReqServiceMap = beanMapUtil.beanToMap(cwm004ReqService);
            context.put("tradeDataMap", cwm004ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM004.key, EsbEnum.TRADE_CODE_CWM004.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CWM004.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM004.key, EsbEnum.TRADE_CODE_CWM004.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cwm004RespService = beanMapUtil.mapToBean(tradeDataMap, Cwm004RespService.class, Cwm004RespService.class);
            respService = cwm004RespService.getService();

            //  将Cwm004qRespDto封装到ResultDto<Cwm004qRespDto>
            cwm004ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            cwm004ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Cwm004qRespDto
                BeanUtils.copyProperties(respService, cwm004RespDto);
                cwm004ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cwm004ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cwm004ResultDto.setCode(EpbEnum.EPB099999.key);
                cwm004ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM004.key, EsbEnum.TRADE_CODE_CWM004.value, e.getMessage());
            cwm004ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cwm004ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cwm004ResultDto.setData(cwm004RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM004.key, EsbEnum.TRADE_CODE_CWM004.value, JSON.toJSONString(cwm004ResultDto));
        return cwm004ResultDto;
    }
}
