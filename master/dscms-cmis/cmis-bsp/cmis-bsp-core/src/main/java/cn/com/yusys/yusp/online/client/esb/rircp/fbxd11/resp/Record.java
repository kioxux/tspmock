package cn.com.yusys.yusp.online.client.esb.rircp.fbxd11.resp;

import java.math.BigDecimal;

public class Record {
    private String bill_no;//借据号
    private BigDecimal loan_amount;//借据金额
    private BigDecimal loan_balance;//借据余额

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public BigDecimal getLoan_amount() {
        return loan_amount;
    }

    public void setLoan_amount(BigDecimal loan_amount) {
        this.loan_amount = loan_amount;
    }

    public BigDecimal getLoan_balance() {
        return loan_balance;
    }

    public void setLoan_balance(BigDecimal loan_balance) {
        this.loan_balance = loan_balance;
    }

    @Override
    public String toString() {
        return "Record{" +
                "bill_no='" + bill_no + '\'' +
                ", loan_amount=" + loan_amount +
                ", loan_balance=" + loan_balance +
                '}';
    }
}
