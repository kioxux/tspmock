package cn.com.yusys.yusp.online.client.esb.core.ln3002.resp;

/**
 * 响应Service：贷款产品删除
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3002RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

