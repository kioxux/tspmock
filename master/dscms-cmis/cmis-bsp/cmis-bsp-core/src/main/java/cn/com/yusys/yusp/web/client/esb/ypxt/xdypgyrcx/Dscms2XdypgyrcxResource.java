package cn.com.yusys.yusp.web.client.esb.ypxt.xdypgyrcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.Record;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(xdypgyrcx)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2XdypgyrcxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdypgyrcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdypgyrcx
     * 交易描述：查询共有人信息
     *
     * @param xdypgyrcxReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xdypgyrcx:查询共有人信息")
    @PostMapping("/xdypgyrcx")
    protected @ResponseBody
    ResultDto<XdypgyrcxRespDto> xdypgyrcx(@Validated @RequestBody XdypgyrcxReqDto xdypgyrcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcxReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.Service();
        XdypgyrcxReqService xdypgyrcxReqService = new XdypgyrcxReqService();
        XdypgyrcxRespService xdypgyrcxRespService = new XdypgyrcxRespService();
        XdypgyrcxRespDto xdypgyrcxRespDto = new XdypgyrcxRespDto();
        ResultDto<XdypgyrcxRespDto> xdypgyrcxResultDto = new ResultDto<XdypgyrcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdypgyrcxReqDto转换成reqService
            BeanUtils.copyProperties(xdypgyrcxReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDYPGYRCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_YPP.key, EsbEnum.SERVTP_YPP.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_YPP.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_YPP.key, EsbEnum.SERVTP_YPP.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xdypgyrcxReqService.setService(reqService);
            // 将xdypgyrcxReqService转换成xdypgyrcxReqServiceMap
            Map xdypgyrcxReqServiceMap = beanMapUtil.beanToMap(xdypgyrcxReqService);
            context.put("tradeDataMap", xdypgyrcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDYPGYRCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdypgyrcxRespService = beanMapUtil.mapToBean(tradeDataMap, XdypgyrcxRespService.class, XdypgyrcxRespService.class);
            respService = xdypgyrcxRespService.getService();

            xdypgyrcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdypgyrcxResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypgyrcxRespDto
                BeanUtils.copyProperties(respService, xdypgyrcxRespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.List xdypgyrcxRespServiceList = Optional.ofNullable(respService.getList())
                        .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.List());
                respService.setList(xdypgyrcxRespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    java.util.List<cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.Record> xdypgyrcxRespRecordList = Optional.ofNullable(respService.getList().getRecord()).orElseGet(() -> new ArrayList<Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.List> xdypgyrcxRespLists = xdypgyrcxRespRecordList.parallelStream().map(xdypgyrcxRespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.List xdypgyrcxRespList = new cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.List();
                                BeanUtils.copyProperties(xdypgyrcxRespRecord, xdypgyrcxRespList);
                                return xdypgyrcxRespList;
                            }
                    ).collect(Collectors.toList());
                    xdypgyrcxRespDto.setList(xdypgyrcxRespLists);
                }

                xdypgyrcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdypgyrcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdypgyrcxResultDto.setCode(EpbEnum.EPB099999.key);
                xdypgyrcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, e.getMessage());
            xdypgyrcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdypgyrcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdypgyrcxResultDto.setData(xdypgyrcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPGYRCX.key, EsbEnum.TRADE_CODE_XDYPGYRCX.value, JSON.toJSONString(xdypgyrcxResultDto));
        return xdypgyrcxResultDto;
    }
}
