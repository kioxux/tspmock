package cn.com.yusys.yusp.online.client.esb.core.ib1253.req;



/**
 * 请求Service：根据账号查询帐户信息接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16上午10:43:45
 */
public class Ib1253ReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}