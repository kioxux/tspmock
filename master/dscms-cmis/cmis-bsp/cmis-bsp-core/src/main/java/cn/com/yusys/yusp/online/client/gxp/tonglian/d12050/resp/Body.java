package cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp;

/**
 * 响应Body：账单列表查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Body {

    private String pgeflg;//是否有下一页标志
    private String frtrow;//开始位置
    private String lstrow;//结束位置
    private String cardno;//卡号
    private Integer totrow;//总条数
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.resp.List list;

    public String getPgeflg() {
        return pgeflg;
    }

    public void setPgeflg(String pgeflg) {
        this.pgeflg = pgeflg;
    }

    public String getFrtrow() {
        return frtrow;
    }

    public void setFrtrow(String frtrow) {
        this.frtrow = frtrow;
    }

    public String getLstrow() {
        return lstrow;
    }

    public void setLstrow(String lstrow) {
        this.lstrow = lstrow;
    }

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public Integer getTotrow() {
        return totrow;
    }

    public void setTotrow(Integer totrow) {
        this.totrow = totrow;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }
}
