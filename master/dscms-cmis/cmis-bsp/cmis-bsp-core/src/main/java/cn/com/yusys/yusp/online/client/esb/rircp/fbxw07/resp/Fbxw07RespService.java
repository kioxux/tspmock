package cn.com.yusys.yusp.online.client.esb.rircp.fbxw07.resp;

/**
 * 响应Service：优惠利率申请接口接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/16 16:50
 */
public class Fbxw07RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
