package cn.com.yusys.yusp.online.client.esb.ecif.g10001.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/6 11:33
 * @since 2021/7/6 11:33
 */
public class Record {
    private String gpmbna;//群组成员客户号
    private String gpmbmc;//群组成员客户名称
    private String gpmbxt;//客户类型
    private String gpmbgs;//成员归属
    private String membtp;//成员类型

    public String getGpmbna() {
        return gpmbna;
    }

    public void setGpmbna(String gpmbna) {
        this.gpmbna = gpmbna;
    }

    public String getGpmbmc() {
        return gpmbmc;
    }

    public void setGpmbmc(String gpmbmc) {
        this.gpmbmc = gpmbmc;
    }

    public String getGpmbxt() {
        return gpmbxt;
    }

    public void setGpmbxt(String gpmbxt) {
        this.gpmbxt = gpmbxt;
    }

    public String getGpmbgs() {
        return gpmbgs;
    }

    public void setGpmbgs(String gpmbgs) {
        this.gpmbgs = gpmbgs;
    }

    public String getMembtp() {
        return membtp;
    }

    public void setMembtp(String membtp) {
        this.membtp = membtp;
    }

    @Override
    public String toString() {
        return "Record{" +
                "gpmbna='" + gpmbna + '\'' +
                ", gpmbmc='" + gpmbmc + '\'' +
                ", gpmbxt='" + gpmbxt + '\'' +
                ", gpmbgs='" + gpmbgs + '\'' +
                ", membtp='" + membtp + '\'' +
                '}';
    }
}
