package cn.com.yusys.yusp.online.client.http.outerdata.slno01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：双录流水号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Slno01RespService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "data")
    private Data data;
    @JsonProperty(value = "status")
    private String status;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Slno01RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", data=" + data +
                ", status='" + status + '\'' +
                '}';
    }
}
