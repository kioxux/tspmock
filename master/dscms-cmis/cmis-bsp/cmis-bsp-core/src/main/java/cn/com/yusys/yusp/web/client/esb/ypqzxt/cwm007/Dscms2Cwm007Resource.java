package cn.com.yusys.yusp.web.client.esb.ypqzxt.cwm007;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.req.Cwm007ReqService;
import cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.resp.Cwm007RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用押品权证系统的接口
 */
@Api(tags = "BSP封装调用押品权证系统的接口(cwm007)")
@RestController
@RequestMapping("/api/dscms2ypqzxt")
public class Dscms2Cwm007Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Cwm007Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 押品状态查询接口（处理码cwm007）
     *
     * @param cwm007ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("cwm007:押品权证入库接口")
    @PostMapping("/cwm007")
    protected @ResponseBody
    ResultDto<Cwm007RespDto> cwm007(@Validated @RequestBody Cwm007ReqDto cwm007ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM007.key, EsbEnum.TRADE_CODE_CWM007.value, JSON.toJSONString(cwm007ReqDto));
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypqzxt.cwm007.resp.Service();
        Cwm007ReqService cwm007ReqService = new Cwm007ReqService();
        Cwm007RespService cwm007RespService = new Cwm007RespService();
        Cwm007RespDto cwm007RespDto = new Cwm007RespDto();
        ResultDto<Cwm007RespDto> cwm007ResultDto = new ResultDto<Cwm007RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Cwm007ReqDto转换成reqService
            BeanUtils.copyProperties(cwm007ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CWM007.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPQZXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPQZXT.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            cwm007ReqService.setService(reqService);
            // 将cwm007ReqService转换成cwm007ReqServiceMap
            Map cwm007ReqServiceMap = beanMapUtil.beanToMap(cwm007ReqService);
            context.put("tradeDataMap", cwm007ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM007.key, EsbEnum.TRADE_CODE_CWM007.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CWM007.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM007.key, EsbEnum.TRADE_CODE_CWM007.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cwm007RespService = beanMapUtil.mapToBean(tradeDataMap, Cwm007RespService.class, Cwm007RespService.class);
            respService = cwm007RespService.getService();

            //  将Cwm007RespDto封装到ResultDto<Cwm007RespDto>
            cwm007ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            cwm007ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Cwm007RespDto
                BeanUtils.copyProperties(respService, cwm007RespDto);
                cwm007ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cwm007ResultDto.setMessage(SuccessEnum.SUCCESS.value);

            } else {
                cwm007ResultDto.setCode(EpbEnum.EPB099999.key);
                cwm007ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM007.key, EsbEnum.TRADE_CODE_CWM007.value, e.getMessage());
            cwm007ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cwm007ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cwm007ResultDto.setData(cwm007RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CWM007.key, EsbEnum.TRADE_CODE_CWM007.value, JSON.toJSONString(cwm007ResultDto));
        return cwm007ResultDto;
    }
}