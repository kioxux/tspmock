package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	年报-修改信息
@JsonPropertyOrder(alphabetic = true)
public class YEARREPORTALTER implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ALITEM")
    private String ALITEM;//	修改事项
    @JsonProperty(value = "ALTAF")
    private String ALTAF;//	修改后
    @JsonProperty(value = "ALTBE")
    private String ALTBE;//	修改前
    @JsonProperty(value = "ALTDATE")
    private String ALTDATE;//	修改日期
    @JsonProperty(value = "ANCHEID")
    private String ANCHEID;//	年报ID

    @JsonIgnore
    public String getALITEM() {
        return ALITEM;
    }

    @JsonIgnore
    public void setALITEM(String ALITEM) {
        this.ALITEM = ALITEM;
    }

    @JsonIgnore
    public String getALTAF() {
        return ALTAF;
    }

    @JsonIgnore
    public void setALTAF(String ALTAF) {
        this.ALTAF = ALTAF;
    }

    @JsonIgnore
    public String getALTBE() {
        return ALTBE;
    }

    @JsonIgnore
    public void setALTBE(String ALTBE) {
        this.ALTBE = ALTBE;
    }

    @JsonIgnore
    public String getALTDATE() {
        return ALTDATE;
    }

    @JsonIgnore
    public void setALTDATE(String ALTDATE) {
        this.ALTDATE = ALTDATE;
    }

    @JsonIgnore
    public String getANCHEID() {
        return ANCHEID;
    }

    @JsonIgnore
    public void setANCHEID(String ANCHEID) {
        this.ANCHEID = ANCHEID;
    }

    @Override
    public String toString() {
        return "YEARREPORTALTER{" +
                "ALITEM='" + ALITEM + '\'' +
                ", ALTAF='" + ALTAF + '\'' +
                ", ALTBE='" + ALTBE + '\'' +
                ", ALTDATE='" + ALTDATE + '\'' +
                ", ANCHEID='" + ANCHEID + '\'' +
                '}';
    }
}

