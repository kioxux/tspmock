package cn.com.yusys.yusp.web.client.esb.ocr.cbocr2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.req.Cbocr2ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp.Cbocr2RespDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp.Data;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.req.Cbocr2ReqService;
import cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.resp.Cbocr2RespService;
import cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.resp.Record;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * 接口处理类:识别任务的模板匹配结果列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2ocr")
public class DscmsCbocr2Resource {
    private static final Logger logger = LoggerFactory.getLogger(DscmsCbocr2Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：cbocr2
     * 交易描述：识别任务的模板匹配结果列表查询
     *
     * @param cbocr2ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/cbocr2")
    protected @ResponseBody
    ResultDto<Cbocr2RespDto> cbocr2(@Validated @RequestBody Cbocr2ReqDto cbocr2ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR2.key, EsbEnum.TRADE_CODE_CBOCR2.value, JSON.toJSONString(cbocr2ReqDto));
        cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.req.Service();
        cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ocr.cbocr2.resp.Service();

        Cbocr2ReqService cbocr2ReqService = new Cbocr2ReqService();
        Cbocr2RespService cbocr2RespService = new Cbocr2RespService();
        Cbocr2RespDto cbocr2RespDto = new Cbocr2RespDto();
        ResultDto<Cbocr2RespDto> cbocr2ResultDto = new ResultDto<Cbocr2RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将cbocr2ReqDto转换成reqService
            BeanUtils.copyProperties(cbocr2ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CBOCR2.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            cbocr2ReqService.setService(reqService);
            // 将cbocr2ReqService转换成cbocr2ReqServiceMap
            Map cbocr2ReqServiceMap = beanMapUtil.beanToMap(cbocr2ReqService);
            context.put("tradeDataMap", cbocr2ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CBOCR1.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cbocr2RespService = beanMapUtil.mapToBean(tradeDataMap, Cbocr2RespService.class, Cbocr2RespService.class);
            respService = cbocr2RespService.getService();

            if (Objects.equals(SuccessEnum.OCR_SUCCESS_CODE.key, respService.getErorcd())) {
                //  将respService转换成Cbocr2RespDto
                BeanUtils.copyProperties(respService, cbocr2RespDto);
                List<Record> recordList = respService.getList().getRecord();
                List<Data> dataList = createDataList(recordList);
                cbocr2RespDto.setList(dataList);
                cbocr2ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cbocr2ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cbocr2ResultDto.setCode(EpbEnum.EPB099999.key);
                cbocr2ResultDto.setMessage(respService.getErortx());
            }

        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value, e.getMessage());
            cbocr2ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cbocr2ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cbocr2ResultDto.setData(cbocr2RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CBOCR1.key, EsbEnum.TRADE_CODE_CBOCR1.value, JSON.toJSONString(cbocr2ResultDto));
        return cbocr2ResultDto;
    }
    
    private List<Data> createDataList(List<Record> recordList) {
    	if (CollectionUtils.isEmpty(recordList)) {
    		return null;
    	}
    	List<Data> dataList = new ArrayList<Data>();
    	recordList.forEach(record -> {
    		Data data = new Data();
    		BeanUtils.copyProperties(record, data);
    		dataList.add(data);
    	});
    	return dataList;
    }
}
