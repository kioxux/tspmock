package cn.com.yusys.yusp.web.client.esb.core.ib1253;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ib1253.req.Ib1253ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ib1253.resp.Ib1253RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(Ib1253)")
@RestController
@RequestMapping("/api/dscms2coreib")
public class Dscms2Ib1253Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ib1253Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 根据账号查询帐户信息（处理码ib1253）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ib1253:根据账号查询帐户信息")
    @PostMapping("/ib1253")
    protected @ResponseBody
    ResultDto<Ib1253RespDto> ib1253(@Validated @RequestBody Ib1253ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.ib1253.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ib1253.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ib1253.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ib1253.resp.Service();
        Ib1253ReqService ib1253ReqService = new Ib1253ReqService();
        Ib1253RespService ib1253RespService = new Ib1253RespService();
        Ib1253RespDto ib1253RespDto = new Ib1253RespDto();
        ResultDto<Ib1253RespDto> ib1253ResultDto = new ResultDto<Ib1253RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try{
            //  将Ib1253ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IB1253.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            ib1253ReqService.setService(reqService);
            // 将ib1253ReqService转换成ib1253ReqServiceMap
            Map ib1253ReqServiceMap = beanMapUtil.beanToMap(ib1253ReqService);
            context.put("tradeDataMap", ib1253ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IB1253.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ib1253RespService = beanMapUtil.mapToBean(tradeDataMap, Ib1253RespService.class, Ib1253RespService.class);
            respService = ib1253RespService.getService();
            //  将respService转换成Ib1253RespDto
            BeanUtils.copyProperties(respService, ib1253RespDto);
            //  将Ib1253RespDto封装到ResultDto<Ib1253RespDto>
            ib1253ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ib1253ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                org.springframework.beans.BeanUtils.copyProperties(respService, ib1253RespDto);
                ib1253ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ib1253ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ib1253ResultDto.setCode(EpbEnum.EPB099999.key);
                ib1253ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e){
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value, e.getMessage());
            ib1253ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ib1253ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        ib1253ResultDto.setData(ib1253RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1253.key, EsbEnum.TRADE_CODE_IB1253.value, JSON.toJSONString(ib1253ResultDto));
        return ib1253ResultDto;
    }

}
