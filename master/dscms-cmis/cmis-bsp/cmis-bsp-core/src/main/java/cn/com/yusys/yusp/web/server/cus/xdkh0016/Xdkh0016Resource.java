package cn.com.yusys.yusp.web.server.cus.xdkh0016;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.cus.xdkh0016.req.Xdkh0016ReqDto;
import cn.com.yusys.yusp.dto.server.cus.xdkh0016.resp.Xdkh0016RespDto;
import cn.com.yusys.yusp.dto.server.xdkh0016.req.Xdkh0016DataReqDto;
import cn.com.yusys.yusp.dto.server.xdkh0016.resp.Xdkh0016DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsCusClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询是否我行关系人
 *
 * @author zhugenrong
 * @version 1.0
 */
@Api(tags = "XDKH0016:查询是否我行关系人")
@RestController
@RequestMapping("/api/dscms")
public class Xdkh0016Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdkh0016Resource.class);
    @Autowired
    private DscmsCusClientService dscmsCusClientService;

    /**
     * 交易码：xdkh0016
     * 交易描述：查询是否我行关系人
     *
     * @param xdkh0016ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询是否我行关系人")
    @PostMapping("/xdkh0016")
    //@Idempotent({"xdcakh0016", "#xdkh0016ReqDto.datasq"})
    protected @ResponseBody
    Xdkh0016RespDto xdkh0016(@Validated @RequestBody Xdkh0016ReqDto xdkh0016ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, JSON.toJSONString(xdkh0016ReqDto));
        Xdkh0016DataReqDto xdkh0016DataReqDto = new Xdkh0016DataReqDto();// 请求Data： 查询是否我行关系人
        Xdkh0016DataRespDto xdkh0016DataRespDto = new Xdkh0016DataRespDto();// 响应Data：查询是否我行关系人
        Xdkh0016RespDto xdkh0016RespDto = new Xdkh0016RespDto();
        cn.com.yusys.yusp.dto.server.cus.xdkh0016.req.Data reqData = null; // 请求Data：查询是否我行关系人
        cn.com.yusys.yusp.dto.server.cus.xdkh0016.resp.Data respData = new cn.com.yusys.yusp.dto.server.cus.xdkh0016.resp.Data();// 响应Data：查询是否我行关系人
        try {
            // 从 xdkh0016ReqDto获取 reqData
            reqData = xdkh0016ReqDto.getData();
            // 将 reqData 拷贝到xdkh0016DataReqDto
            BeanUtils.copyProperties(reqData, xdkh0016DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, JSON.toJSONString(xdkh0016DataReqDto));
            ResultDto<Xdkh0016DataRespDto> xdkh0016DataResultDto = dscmsCusClientService.xdkh0016(xdkh0016DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, JSON.toJSONString(xdkh0016DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdkh0016RespDto.setErorcd(Optional.ofNullable(xdkh0016DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdkh0016RespDto.setErortx(Optional.ofNullable(xdkh0016DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdkh0016DataResultDto.getCode())) {
                xdkh0016RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdkh0016RespDto.setErortx(SuccessEnum.SUCCESS.value);
                xdkh0016DataRespDto = xdkh0016DataResultDto.getData();
                // 将 xdkh0016DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdkh0016DataRespDto, respData);
            }
        } catch (BeansException e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, e.getMessage());
            xdkh0016RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdkh0016RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdkh0016RespDto.setDatasq(servsq);

        xdkh0016RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDKH0016.key, DscmsEnum.TRADE_CODE_XDKH0016.value, JSON.toJSONString(xdkh0016RespDto));
        return xdkh0016RespDto;
    }
}