package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd001.resp;

/**
 * @类名 Service
 * @描述 响应Service：新微贷贷款申请
 * @作者 黄勃
 * @时间 2021/10/26 19:33
 **/
public class Service {
    private String erorcd;//响应码

    private String erortx;//响应信息

    private String code;//响应码

    private String data;//新微贷流水号

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xwd001RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", code='" + code + '\'' +
                ", data=" + data +
                '}';
    }
}
