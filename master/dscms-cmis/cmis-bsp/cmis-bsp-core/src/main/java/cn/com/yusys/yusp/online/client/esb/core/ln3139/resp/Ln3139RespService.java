package cn.com.yusys.yusp.online.client.esb.core.ln3139.resp;

/**
 * 响应Service：通过贷款借据号和贷款账号的交易日期试算贷款欠息明细。
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3139RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
