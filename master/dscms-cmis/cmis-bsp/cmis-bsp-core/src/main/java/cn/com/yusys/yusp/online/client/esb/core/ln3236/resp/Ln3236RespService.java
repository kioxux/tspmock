package cn.com.yusys.yusp.online.client.esb.core.ln3236.resp;

import java.math.BigDecimal;

/**
 * 响应Service：贷款账隔日冲正
 * @author leehuang
 * @version 1.0             
 */      
public class Ln3236RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
