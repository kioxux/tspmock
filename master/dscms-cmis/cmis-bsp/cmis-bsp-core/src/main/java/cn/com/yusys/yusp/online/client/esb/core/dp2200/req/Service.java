package cn.com.yusys.yusp.online.client.esb.core.dp2200.req;

import java.math.BigDecimal;

/**
 * 请求Service：保证金账户部提
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号
    private String datasq;//全局流水
    private String servdt;//交易日期
    private String servti;//交易时间
    private String kehuzhao;//客户账号
    private String zhhaoxuh;//子账户序号
    private String kehuzhlx;//客户账号类型
    private String zhhuzwmc;//账户名称
    private String huobdaih;//货币代号
    private String chaohubz;//账户钞汇标志
    private String chapbhao;//产品编号
    private String zhanghao;//负债账号
    private String tonzbhao;//通知编号
    private String zhdaoqir;//账户到期日
    private BigDecimal jiaoyije;//交易金额
    private String pngzzlei;//凭证种类
    private String pngzphao;//凭证批号
    private String pngzxhao;//凭证序号
    private String xpizleix;//新凭证种类
    private String xpnzphao;//新凭证批号
    private String xpnzxhao;//新凭证序号
    private String zhfutojn;//支付条件
    private String jiaoymma;//交易密码
    private String zijinqux;//资金去向
    private String skrkhuzh;//收款人客户账号
    private String skrzhalx;//收款人账号类型
    private String skrzhamc;//收款人账户名称
    private String skzhxuho;//收款人子账户序号
    private String skhobidh;//收款人币种
    private String skchhubz;//收款人钞汇
    private String sfsfbzhi;//是否收费标志
    private String zhaiyodm;//摘要代码
    private String zhaiyoms;//摘要描述
    private String beizhuuu;//备注
    private String xianzzbz;//现转标志
    private String mimazlei;//密码种类
    private String dfzhhxuh;//对方子账户序号
    private String duifjgdm;//对方金融机构代码
    private String duifjgmc;//对方金融机构名称
    private BigDecimal daokjine;//倒扣金额
    private String lxzrkhzh;//利息转入客户账号
    private String zrzzhhxh;//转入子账户序号
    private String lixizjqx;//利息资金去向

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public String getZhhuzwmc() {
        return zhhuzwmc;
    }

    public void setZhhuzwmc(String zhhuzwmc) {
        this.zhhuzwmc = zhhuzwmc;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getChapbhao() {
        return chapbhao;
    }

    public void setChapbhao(String chapbhao) {
        this.chapbhao = chapbhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getTonzbhao() {
        return tonzbhao;
    }

    public void setTonzbhao(String tonzbhao) {
        this.tonzbhao = tonzbhao;
    }

    public String getZhdaoqir() {
        return zhdaoqir;
    }

    public void setZhdaoqir(String zhdaoqir) {
        this.zhdaoqir = zhdaoqir;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPngzphao() {
        return pngzphao;
    }

    public void setPngzphao(String pngzphao) {
        this.pngzphao = pngzphao;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getXpizleix() {
        return xpizleix;
    }

    public void setXpizleix(String xpizleix) {
        this.xpizleix = xpizleix;
    }

    public String getXpnzphao() {
        return xpnzphao;
    }

    public void setXpnzphao(String xpnzphao) {
        this.xpnzphao = xpnzphao;
    }

    public String getXpnzxhao() {
        return xpnzxhao;
    }

    public void setXpnzxhao(String xpnzxhao) {
        this.xpnzxhao = xpnzxhao;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getJiaoymma() {
        return jiaoymma;
    }

    public void setJiaoymma(String jiaoymma) {
        this.jiaoymma = jiaoymma;
    }

    public String getZijinqux() {
        return zijinqux;
    }

    public void setZijinqux(String zijinqux) {
        this.zijinqux = zijinqux;
    }

    public String getSkrkhuzh() {
        return skrkhuzh;
    }

    public void setSkrkhuzh(String skrkhuzh) {
        this.skrkhuzh = skrkhuzh;
    }

    public String getSkrzhalx() {
        return skrzhalx;
    }

    public void setSkrzhalx(String skrzhalx) {
        this.skrzhalx = skrzhalx;
    }

    public String getSkrzhamc() {
        return skrzhamc;
    }

    public void setSkrzhamc(String skrzhamc) {
        this.skrzhamc = skrzhamc;
    }

    public String getSkzhxuho() {
        return skzhxuho;
    }

    public void setSkzhxuho(String skzhxuho) {
        this.skzhxuho = skzhxuho;
    }

    public String getSkhobidh() {
        return skhobidh;
    }

    public void setSkhobidh(String skhobidh) {
        this.skhobidh = skhobidh;
    }

    public String getSkchhubz() {
        return skchhubz;
    }

    public void setSkchhubz(String skchhubz) {
        this.skchhubz = skchhubz;
    }

    public String getSfsfbzhi() {
        return sfsfbzhi;
    }

    public void setSfsfbzhi(String sfsfbzhi) {
        this.sfsfbzhi = sfsfbzhi;
    }

    public String getZhaiyodm() {
        return zhaiyodm;
    }

    public void setZhaiyodm(String zhaiyodm) {
        this.zhaiyodm = zhaiyodm;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getXianzzbz() {
        return xianzzbz;
    }

    public void setXianzzbz(String xianzzbz) {
        this.xianzzbz = xianzzbz;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getDfzhhxuh() {
        return dfzhhxuh;
    }

    public void setDfzhhxuh(String dfzhhxuh) {
        this.dfzhhxuh = dfzhhxuh;
    }

    public String getDuifjgdm() {
        return duifjgdm;
    }

    public void setDuifjgdm(String duifjgdm) {
        this.duifjgdm = duifjgdm;
    }

    public String getDuifjgmc() {
        return duifjgmc;
    }

    public void setDuifjgmc(String duifjgmc) {
        this.duifjgmc = duifjgmc;
    }

    public BigDecimal getDaokjine() {
        return daokjine;
    }

    public void setDaokjine(BigDecimal daokjine) {
        this.daokjine = daokjine;
    }

    public String getLxzrkhzh() {
        return lxzrkhzh;
    }

    public void setLxzrkhzh(String lxzrkhzh) {
        this.lxzrkhzh = lxzrkhzh;
    }

    public String getZrzzhhxh() {
        return zrzzhhxh;
    }

    public void setZrzzhhxh(String zrzzhhxh) {
        this.zrzzhhxh = zrzzhhxh;
    }

    public String getLixizjqx() {
        return lixizjqx;
    }

    public void setLixizjqx(String lixizjqx) {
        this.lixizjqx = lixizjqx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "datasq='" + datasq + '\'' +
                "servdt='" + servdt + '\'' +
                "servti='" + servti + '\'' +
                "kehuzhao='" + kehuzhao + '\'' +
                "zhhaoxuh='" + zhhaoxuh + '\'' +
                "kehuzhlx='" + kehuzhlx + '\'' +
                "zhhuzwmc='" + zhhuzwmc + '\'' +
                "huobdaih='" + huobdaih + '\'' +
                "chaohubz='" + chaohubz + '\'' +
                "chapbhao='" + chapbhao + '\'' +
                "zhanghao='" + zhanghao + '\'' +
                "tonzbhao='" + tonzbhao + '\'' +
                "zhdaoqir='" + zhdaoqir + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                "pngzzlei='" + pngzzlei + '\'' +
                "pngzphao='" + pngzphao + '\'' +
                "pngzxhao='" + pngzxhao + '\'' +
                "xpizleix='" + xpizleix + '\'' +
                "xpnzphao='" + xpnzphao + '\'' +
                "xpnzxhao='" + xpnzxhao + '\'' +
                "zhfutojn='" + zhfutojn + '\'' +
                "jiaoymma='" + jiaoymma + '\'' +
                "zijinqux='" + zijinqux + '\'' +
                "skrkhuzh='" + skrkhuzh + '\'' +
                "skrzhalx='" + skrzhalx + '\'' +
                "skrzhamc='" + skrzhamc + '\'' +
                "skzhxuho='" + skzhxuho + '\'' +
                "skhobidh='" + skhobidh + '\'' +
                "skchhubz='" + skchhubz + '\'' +
                "sfsfbzhi='" + sfsfbzhi + '\'' +
                "zhaiyodm='" + zhaiyodm + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "beizhuuu='" + beizhuuu + '\'' +
                "xianzzbz='" + xianzzbz + '\'' +
                "mimazlei='" + mimazlei + '\'' +
                "dfzhhxuh='" + dfzhhxuh + '\'' +
                "duifjgdm='" + duifjgdm + '\'' +
                "duifjgmc='" + duifjgmc + '\'' +
                "daokjine='" + daokjine + '\'' +
                "lxzrkhzh='" + lxzrkhzh + '\'' +
                "zrzzhhxh='" + zrzzhhxh + '\'' +
                "lixizjqx='" + lixizjqx + '\'' +
                '}';
    }
}
