package cn.com.yusys.yusp.web.client.esb.ypxt.cxypbh;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.cxypbh.req.CxypbhReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.cxypbh.req.Service;
import cn.com.yusys.yusp.online.client.esb.ypxt.cxypbh.resp.CxypbhRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:通过票据号码查询押品编号
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用押品系统的接口(cxypbh)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2CxypbhResource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2CxypbhResource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：cxypbh
     * 交易描述：通过票据号码查询押品编号
     *
     * @param cxypbhReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("cxypbh:通过票据号码查询押品编号")
    @PostMapping("/cxypbh")
    protected @ResponseBody
    ResultDto<CxypbhRespDto> cxypbh(@Validated @RequestBody CxypbhReqDto cxypbhReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CXYPBH.key, EsbEnum.TRADE_CODE_CXYPBH.value, JSON.toJSONString(cxypbhReqDto));
        Service reqService = new Service();
		cn.com.yusys.yusp.online.client.esb.ypxt.cxypbh.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.cxypbh.resp.Service();
        CxypbhReqService cxypbhReqService = new CxypbhReqService();
        CxypbhRespService cxypbhRespService = new CxypbhRespService();
        CxypbhRespDto cxypbhRespDto = new CxypbhRespDto();
        ResultDto<CxypbhRespDto> cxypbhResultDto = new ResultDto<CxypbhRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将cxypbhReqDto转换成reqService
            BeanUtils.copyProperties(cxypbhReqDto, reqService);
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            reqService.setPrcscd(EsbEnum.TRADE_CODE_CXYPBH.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            cxypbhReqService.setService(reqService);
            // 将cxypbhReqService转换成cxypbhReqServiceMap
            Map cxypbhReqServiceMap = beanMapUtil.beanToMap(cxypbhReqService);
            context.put("tradeDataMap", cxypbhReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CXYPBH.key, EsbEnum.TRADE_CODE_CXYPBH.value, JSON.toJSONString(cxypbhReqDto));
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_EVALYP.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CXYPBH.key, EsbEnum.TRADE_CODE_CXYPBH.value, JSON.toJSONString(result));
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            cxypbhRespService = beanMapUtil.mapToBean(tradeDataMap, CxypbhRespService.class, CxypbhRespService.class);
            respService = cxypbhRespService.getService();
            //  将respService转换成CxypbhRespDto
            cxypbhResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            cxypbhResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                BeanUtils.copyProperties(respService, cxypbhRespDto);
                cxypbhResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                cxypbhResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                cxypbhResultDto.setCode(EpbEnum.EPB099999.key);
                cxypbhResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CXYPBH.key, EsbEnum.TRADE_CODE_CXYPBH.value, e.getMessage());
            cxypbhResultDto.setCode(EpbEnum.EPB099999.key);//9999
            cxypbhResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        cxypbhResultDto.setData(cxypbhRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CXYPBH.key, EsbEnum.TRADE_CODE_CXYPBH.value, JSON.toJSONString(cxypbhResultDto));
        return cxypbhResultDto;
    }
}
