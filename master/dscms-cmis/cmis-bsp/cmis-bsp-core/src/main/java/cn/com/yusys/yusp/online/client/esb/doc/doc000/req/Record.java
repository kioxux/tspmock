package cn.com.yusys.yusp.online.client.esb.doc.doc000.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/22 15:15
 * @since 2021/6/22 15:15
 */
public class Record {
    private String contid;
    private String contdate;
    private String custname;
    private String outsystem_code;
    private String custid;
    private String billno;
    private String custconduct;
    private String custtype;

    public String getContid() {
        return contid;
    }

    public void setContid(String contid) {
        this.contid = contid;
    }

    public String getContdate() {
        return contdate;
    }

    public void setContdate(String contdate) {
        this.contdate = contdate;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getOutsystem_code() {
        return outsystem_code;
    }

    public void setOutsystem_code(String outsystem_code) {
        this.outsystem_code = outsystem_code;
    }

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getCustconduct() {
        return custconduct;
    }

    public void setCustconduct(String custconduct) {
        this.custconduct = custconduct;
    }

    public String getCusttype() {
        return custtype;
    }

    public void setCusttype(String custtype) {
        this.custtype = custtype;
    }

    @Override
    public String toString() {
        return "Record{" +
                "contid='" + contid + '\'' +
                ", contdate='" + contdate + '\'' +
                ", custname='" + custname + '\'' +
                ", outsystem_code='" + outsystem_code + '\'' +
                ", custid='" + custid + '\'' +
                ", billno='" + billno + '\'' +
                ", custconduct='" + custconduct + '\'' +
                ", custtype='" + custtype + '\'' +
                '}';
    }
}
