package cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.resp;

/**
 * 响应Service：还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd15RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd15RespService{" +
                "service=" + service +
                '}';
    }
}
