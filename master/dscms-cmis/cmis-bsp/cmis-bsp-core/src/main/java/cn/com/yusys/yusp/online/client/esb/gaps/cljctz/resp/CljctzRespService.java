package cn.com.yusys.yusp.online.client.esb.gaps.cljctz.resp;

/**
 * 响应Service：连云港存量房列表
 * @author code-generator
 * @version 1.0             
 */      
public class CljctzRespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
