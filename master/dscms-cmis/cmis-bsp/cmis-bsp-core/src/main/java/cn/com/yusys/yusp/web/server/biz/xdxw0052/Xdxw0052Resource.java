package cn.com.yusys.yusp.web.server.biz.xdxw0052;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0052.req.Xdxw0052ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0052.resp.Xdxw0052RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0052.req.Xdxw0052DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0052.resp.Xdxw0052DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询优抵贷客户调查表
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0052:查询优抵贷客户调查表")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0052Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0052Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0052
     * 交易描述：查询优抵贷客户调查表
     *
     * @param xdxw0052ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询优抵贷客户调查表")
    @PostMapping("/xdxw0052")
    //@Idempotent({"xdcaxw0052", "#xdxw0052ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0052RespDto xdxw0052(@Validated @RequestBody Xdxw0052ReqDto xdxw0052ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052ReqDto));
        Xdxw0052DataReqDto xdxw0052DataReqDto = new Xdxw0052DataReqDto();// 请求Data： 查询优抵贷客户调查表
        Xdxw0052DataRespDto xdxw0052DataRespDto = new Xdxw0052DataRespDto();// 响应Data：查询优抵贷客户调查表
        Xdxw0052RespDto xdxw0052RespDto = new Xdxw0052RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0052.req.Data reqData = null; // 请求Data：查询优抵贷客户调查表
        cn.com.yusys.yusp.dto.server.biz.xdxw0052.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0052.resp.Data();// 响应Data：查询优抵贷客户调查表
        try {
            // 从 xdxw0052ReqDto获取 reqData
            reqData = xdxw0052ReqDto.getData();
            // 将 reqData 拷贝到xdxw0052DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0052DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052DataReqDto));
            ResultDto<Xdxw0052DataRespDto> xdxw0052DataResultDto = dscmsBizXwClientService.xdxw0052(xdxw0052DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0052RespDto.setErorcd(Optional.ofNullable(xdxw0052DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0052RespDto.setErortx(Optional.ofNullable(xdxw0052DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0052DataResultDto.getCode())) {
                xdxw0052DataRespDto = xdxw0052DataResultDto.getData();
                xdxw0052RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0052RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0052DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0052DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, e.getMessage());
            xdxw0052RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0052RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0052RespDto.setDatasq(servsq);

        xdxw0052RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0052.key, DscmsEnum.TRADE_CODE_XDXW0052.value, JSON.toJSONString(xdxw0052RespDto));
        return xdxw0052RespDto;
    }
}
