package cn.com.yusys.yusp.online.client.http.xwywglpt.xwd009.resp;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/9/3 16:07
 * @since 2021/9/3 16:07
 */
public class Service {

    private String erorcd;//响应码
    private String erortx;//响应信息

    private String code;//响应码
    private String message;//响应信息
    private String businessId;//行内业务流水号
    private String reqId;//利率申请业务流水号
    private String isApproval;//是否需要审批

    private String data;//JSONOBJECT

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getIsApproval() {
        return isApproval;
    }

    public void setIsApproval(String isApproval) {
        this.isApproval = isApproval;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xwd009RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", businessId='" + businessId + '\'' +
                ", reqId='" + reqId + '\'' +
                ", isApproval='" + isApproval + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
