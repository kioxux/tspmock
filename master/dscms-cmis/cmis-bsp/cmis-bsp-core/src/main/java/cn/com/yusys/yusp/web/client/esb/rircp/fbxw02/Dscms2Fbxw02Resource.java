package cn.com.yusys.yusp.web.client.esb.rircp.fbxw02;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw02.Fbxw02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw02.Fbxw02RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw02.req.Fbxw02ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxw02.resp.Fbxw02RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxw02Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxw02Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * ESB信贷查询地方征信接口（处理码fbxw02）
     *
     * @param fbxw02ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("fbxw02:ESB信贷查询地方征信接口")
    @PostMapping("/fbxw02")
    protected @ResponseBody
    ResultDto<Fbxw02RespDto> fbxw02(@Validated @RequestBody Fbxw02ReqDto fbxw02ReqDto) throws Exception {
        // BSP中处理[{}|{}]逻辑开始,请求参数为:[{}]
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW02.key, EsbEnum.TRADE_CODE_FBXW02.value, JSON.toJSONString(fbxw02ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw02.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw02.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxw02.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxw02.resp.Service();
        Fbxw02ReqService fbxw02ReqService = new Fbxw02ReqService();
        Fbxw02RespService fbxw02RespService = new Fbxw02RespService();
        Fbxw02RespDto fbxw02RespDto = new Fbxw02RespDto();
        ResultDto<Fbxw02RespDto> fbxw02ResultDto = new ResultDto<Fbxw02RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Fbxw02ReqDto转换成reqService
            BeanUtils.copyProperties(fbxw02ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXW02.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            fbxw02ReqService.setService(reqService);
            // 将fbxw02ReqService转换成fbxw02ReqServiceMap
            Map fbxw02ReqServiceMap = beanMapUtil.beanToMap(fbxw02ReqService);
            context.put("tradeDataMap", fbxw02ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW02.key, EsbEnum.TRADE_CODE_FBXW02.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXW02.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW02.key, EsbEnum.TRADE_CODE_FBXW02.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxw02RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxw02RespService.class, Fbxw02RespService.class);
            respService = fbxw02RespService.getService();

            //  将Fbxw02RespDto封装到ResultDto<Fbxw02RespDto>
            fbxw02ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxw02ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxw02RespDto
                BeanUtils.copyProperties(respService, fbxw02RespDto);
                fbxw02ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxw02ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxw02ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxw02ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW02.key, EsbEnum.TRADE_CODE_FBXW02.value, e.getMessage());
            fbxw02ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxw02ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxw02ResultDto.setData(fbxw02RespDto);
        // BSP中处理[{}|{}]逻辑结束,响应参数为:[{}]
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXW02.key, EsbEnum.TRADE_CODE_FBXW02.value, JSON.toJSONString(fbxw02ResultDto));
        return fbxw02ResultDto;
    }
}
