package cn.com.yusys.yusp.web.client.esb.core.ib1243;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.Ib1243ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.Ib1243RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.LstAcctIn;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.LstAcctOut;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ib1243.req.Ib1243ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ib1243.req.Record;
import cn.com.yusys.yusp.online.client.esb.core.ib1243.resp.Ib1243RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:通用电子记帐交易（多借多贷-多币种）
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(Ib1243)")
@RestController
@RequestMapping("/api/dscms2coreib")
public class Dscms2Ib1243Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Ib1243Resource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
	private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ib1243
     * 交易描述：通用电子记帐交易（多借多贷-多币种）
     *
     * @param ib1243ReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("ib1243:通用电子记帐交易（多借多贷-多币种）")
    @PostMapping("/ib1243")
    protected @ResponseBody
    ResultDto<Ib1243RespDto> ib1243(@Validated @RequestBody Ib1243ReqDto ib1243ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1243.key, EsbEnum.TRADE_CODE_IB1243.value, JSON.toJSONString(ib1243ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.ib1243.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ib1243.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.ib1243.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ib1243.resp.Service();
        Ib1243ReqService ib1243ReqService = new Ib1243ReqService();
        Ib1243RespService ib1243RespService = new Ib1243RespService();
        Ib1243RespDto ib1243RespDto = new Ib1243RespDto();
        ResultDto<Ib1243RespDto> ib1243ResultDto = new ResultDto<Ib1243RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将ib1243ReqDto转换成reqService
			BeanUtils.copyProperties(ib1243ReqDto, reqService);
			List<LstAcctIn> lstAcctIn = ib1243ReqDto.getLstAcctIn();
			List<Record> records = lstAcctIn.parallelStream().map(lst -> {
				Record record = new Record();
				BeanUtils.copyProperties(lst, record);
				return record;
			}).collect(Collectors.toList());
			cn.com.yusys.yusp.online.client.esb.core.ib1243.req.List reqList = new cn.com.yusys.yusp.online.client.esb.core.ib1243.req.List();
			reqList.setRecord(records);
			reqService.setList(reqList);

			reqService.setPrcscd(EsbEnum.TRADE_CODE_IB1243.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

			ib1243ReqService.setService(reqService);
			// 将ib1243ReqService转换成ib1243ReqServiceMap
			Map ib1243ReqServiceMap = beanMapUtil.beanToMap(ib1243ReqService);
			context.put("tradeDataMap", ib1243ReqServiceMap);

			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1243.key, EsbEnum.TRADE_CODE_IB1243.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IB1243.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1243.key, EsbEnum.TRADE_CODE_IB1243.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			ib1243RespService = beanMapUtil.mapToBean(tradeDataMap, Ib1243RespService.class, Ib1243RespService.class);
			respService = ib1243RespService.getService();
			//  将respService转换成Ib1243RespDto
			BeanUtils.copyProperties(respService, ib1243RespDto);
			ib1243ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			ib1243ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Ib1241RespDto
				BeanUtils.copyProperties(respService, ib1243RespDto);
				cn.com.yusys.yusp.online.client.esb.core.ib1243.resp.List respList = Optional.ofNullable(respService.getList()).orElse(new cn.com.yusys.yusp.online.client.esb.core.ib1243.resp.List());
				List<cn.com.yusys.yusp.online.client.esb.core.ib1243.resp.Record> respRecords = Optional.ofNullable(respList.getRecord()).orElse(new ArrayList<>());
				List<LstAcctOut> lstAcctOuts = respRecords.parallelStream().map(record -> {
					LstAcctOut lstAcctOut = new LstAcctOut();
					BeanUtils.copyProperties(record, lstAcctOut);
					return lstAcctOut;
				}).collect(Collectors.toList());
				ib1243RespDto.setLstAcctOut(lstAcctOuts);

				ib1243ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				ib1243ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				ib1243ResultDto.setCode(EpbEnum.EPB099999.key);
				ib1243ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1243.key, EsbEnum.TRADE_CODE_IB1243.value, e.getMessage());
			ib1243ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			ib1243ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		ib1243ResultDto.setData(ib1243RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IB1243.key, EsbEnum.TRADE_CODE_IB1243.value, JSON.toJSONString(ib1243ResultDto));
        return ib1243ResultDto;
    }
}
