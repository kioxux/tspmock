package cn.com.yusys.yusp.web.client.esb.core.ln3065;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3065.Ln3065ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3065.Ln3065RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3065.req.Ln3065ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3065.resp.Ln3065RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3065)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3065Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3065Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3065
     * 交易描述：资产转让资金划转
     *
     * @param ln3065ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3065:资产转让资金划转")
    @PostMapping("/ln3065")
    protected @ResponseBody
    ResultDto<Ln3065RespDto> ln3065(@Validated @RequestBody Ln3065ReqDto ln3065ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3065.key, EsbEnum.TRADE_CODE_LN3065.value, JSON.toJSONString(ln3065ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3065.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3065.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3065.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3065.resp.Service();

        Ln3065ReqService ln3065ReqService = new Ln3065ReqService();
        Ln3065RespService ln3065RespService = new Ln3065RespService();
        Ln3065RespDto ln3065RespDto = new Ln3065RespDto();
        ResultDto<Ln3065RespDto> ln3065ResultDto = new ResultDto<Ln3065RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3065ReqDto转换成reqService
            BeanUtils.copyProperties(ln3065ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3065.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3065ReqService.setService(reqService);
            // 将ln3065ReqService转换成ln3065ReqServiceMap
            Map ln3065ReqServiceMap = beanMapUtil.beanToMap(ln3065ReqService);
            context.put("tradeDataMap", ln3065ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3065.key, EsbEnum.TRADE_CODE_LN3065.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3065.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3065.key, EsbEnum.TRADE_CODE_LN3065.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3065RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3065RespService.class, Ln3065RespService.class);
            respService = ln3065RespService.getService();

            ln3065ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3065ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3065RespDto
                BeanUtils.copyProperties(respService, ln3065RespDto);
                ln3065ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3065ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3065ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3065ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3065.key, EsbEnum.TRADE_CODE_LN3065.value, e.getMessage());
            ln3065ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3065ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3065ResultDto.setData(ln3065RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3065.key, EsbEnum.TRADE_CODE_LN3065.value, JSON.toJSONString(ln3065ResultDto));
        return ln3065ResultDto;
    }
}
