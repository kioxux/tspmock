package cn.com.yusys.yusp.online.client.esb.fxyjxt.hhmdkh.resp;

/**
 * 响应Service：查询客户是否为黑灰名单客户
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String ifBlack;//是否黑名单
    private String ifGrey;//是否灰名单
    private String erorcd;//响应码
    private String erortx;//响应信息

    public String getIfBlack() {
        return ifBlack;
    }

    public void setIfBlack(String ifBlack) {
        this.ifBlack = ifBlack;
    }

    public String getIfGrey() {
        return ifGrey;
    }

    public void setIfGrey(String ifGrey) {
        this.ifGrey = ifGrey;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Service{" +
                "ifBlack='" + ifBlack + '\'' +
                "ifGrey='" + ifGrey + '\'' +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                '}';
    }
}
