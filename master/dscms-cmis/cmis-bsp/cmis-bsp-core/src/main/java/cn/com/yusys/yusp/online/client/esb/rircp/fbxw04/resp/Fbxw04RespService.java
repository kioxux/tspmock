package cn.com.yusys.yusp.online.client.esb.rircp.fbxw04.resp;

/**
 * 响应Service：惠享贷规则审批申请接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午5:34:07
 */
public class Fbxw04RespService {

        private Service service;

        public Service getService() {
            return service;
        }

        public void setService(Service service) {
            this.service = service;
        }
}
