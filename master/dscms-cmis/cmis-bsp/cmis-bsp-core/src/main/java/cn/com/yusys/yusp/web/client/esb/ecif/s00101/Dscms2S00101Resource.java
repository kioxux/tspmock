package cn.com.yusys.yusp.web.client.esb.ecif.s00101;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.s00101.req.S00101ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp.S00101RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(s00101)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2S00101Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2S00101Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：s00101
     * 交易描述：对私客户综合信息查询
     *
     * @param s00101ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/s00101")
    protected @ResponseBody
    ResultDto<S00101RespDto> s00101(@Validated @RequestBody S00101ReqDto s00101ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value, JSON.toJSONString(s00101ReqDto));

        cn.com.yusys.yusp.online.client.esb.ecif.s00101.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.s00101.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.s00101.resp.Service();
        S00101ReqService s00101ReqService = new S00101ReqService();
        S00101RespService s00101RespService = new S00101RespService();
        S00101RespDto s00101RespDto = new S00101RespDto();
        ResultDto<S00101RespDto> s00101ResultDto = new ResultDto<S00101RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try { //  将s00101ReqDto转换成reqService
            BeanUtils.copyProperties(s00101ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_S00101.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            s00101ReqService.setService(reqService);
            // 将s00101ReqService转换成s00101ReqServiceMap
            Map s00101ReqServiceMap = beanMapUtil.beanToMap(s00101ReqService);
            context.put("tradeDataMap", s00101ReqServiceMap);

            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_S00101.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            s00101RespService = beanMapUtil.mapToBean(tradeDataMap, S00101RespService.class, S00101RespService.class);
            respService = s00101RespService.getService();

            s00101ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            s00101ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成S00101RespDto
                BeanUtils.copyProperties(respService, s00101RespDto);
                s00101ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                s00101ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                s00101ResultDto.setCode(EpbEnum.EPB099999.key);
                s00101ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value, e.getMessage());
            s00101ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            s00101ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        s00101ResultDto.setData(s00101RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_S00101.key, EsbEnum.TRADE_CODE_S00101.value, JSON.toJSONString(s00101ResultDto));
        return s00101ResultDto;
    }
}
