package cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp;

/**
 * 响应Service：客户集团信息查询（new）接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:31
 */
public class Service {

    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String grouno; // 集团编号
    private String grouna; // 集团客户名称
    private String custno; // 客户编号
    private String custln; // 客户中证码
    private String custna; // 客户名称
    private String idtfno; // 证件号码
    private String oadate; // 更新办公地址日期
    private String homead; // 地址
    private String socino; // 社会信用代码
    private String busino; // 工商登记注册号
    private String ofaddr; // 办公地址行政区划
    private String crpdeg; // 集团紧密程度
    private String crpcno; // 集团客户情况说明
    private String ctigno; // 管户客户经理
    private String citorg; // 所属机构
    private String grpsta; // 集团客户状态
    private String oprtye; // 操作类型
    private String inptno; // 登记人
    private String inporg; // 登记机构

    private cn.com.yusys.yusp.online.client.esb.ecif.g11004.resp.List list; // 集团成员信息_ARRAY

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getGrouno() {
        return grouno;
    }

    public void setGrouno(String grouno) {
        this.grouno = grouno;
    }

    public String getGrouna() {
        return grouna;
    }

    public void setGrouna(String grouna) {
        this.grouna = grouna;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustln() {
        return custln;
    }

    public void setCustln(String custln) {
        this.custln = custln;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getOadate() {
        return oadate;
    }

    public void setOadate(String oadate) {
        this.oadate = oadate;
    }

    public String getHomead() {
        return homead;
    }

    public void setHomead(String homead) {
        this.homead = homead;
    }

    public String getSocino() {
        return socino;
    }

    public void setSocino(String socino) {
        this.socino = socino;
    }

    public String getBusino() {
        return busino;
    }

    public void setBusino(String busino) {
        this.busino = busino;
    }

    public String getOfaddr() {
        return ofaddr;
    }

    public void setOfaddr(String ofaddr) {
        this.ofaddr = ofaddr;
    }

    public String getCrpdeg() {
        return crpdeg;
    }

    public void setCrpdeg(String crpdeg) {
        this.crpdeg = crpdeg;
    }

    public String getCrpcno() {
        return crpcno;
    }

    public void setCrpcno(String crpcno) {
        this.crpcno = crpcno;
    }

    public String getCtigno() {
        return ctigno;
    }

    public void setCtigno(String ctigno) {
        this.ctigno = ctigno;
    }

    public String getCitorg() {
        return citorg;
    }

    public void setCitorg(String citorg) {
        this.citorg = citorg;
    }

    public String getGrpsta() {
        return grpsta;
    }

    public void setGrpsta(String grpsta) {
        this.grpsta = grpsta;
    }

    public String getOprtye() {
        return oprtye;
    }

    public void setOprtye(String oprtye) {
        this.oprtye = oprtye;
    }

    public String getInptno() {
        return inptno;
    }

    public void setInptno(String inptno) {
        this.inptno = inptno;
    }

    public String getInporg() {
        return inporg;
    }

    public void setInporg(String inporg) {
        this.inporg = inporg;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", grouno='" + grouno + '\'' +
                ", grouna='" + grouna + '\'' +
                ", custno='" + custno + '\'' +
                ", custln='" + custln + '\'' +
                ", custna='" + custna + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", oadate='" + oadate + '\'' +
                ", homead='" + homead + '\'' +
                ", socino='" + socino + '\'' +
                ", busino='" + busino + '\'' +
                ", ofaddr='" + ofaddr + '\'' +
                ", crpdeg='" + crpdeg + '\'' +
                ", crpcno='" + crpcno + '\'' +
                ", ctigno='" + ctigno + '\'' +
                ", citorg='" + citorg + '\'' +
                ", grpsta='" + grpsta + '\'' +
                ", oprtye='" + oprtye + '\'' +
                ", inptno='" + inptno + '\'' +
                ", inporg='" + inporg + '\'' +
                ", list=" + list +
                '}';
    }
}
