package cn.com.yusys.yusp.web.server.oca.xdxt0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.oca.xdxt0006.req.Xdxt0006ReqDto;
import cn.com.yusys.yusp.dto.server.oca.xdxt0006.resp.Xdxt0006RespDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.req.Xdxt0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxt0006.resp.Xdxt0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询信贷用户的特定岗位信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXT0006:查询信贷用户的特定岗位信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxt0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxt0006Resource.class);
    @Autowired
    private DscmsBizXtClientService dscmsBizXtClientService;

    /**
     * 交易码：xdxt0006
     * 交易描述：查询信贷用户的特定岗位信息
     *
     * @param xdxt0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询信贷用户的特定岗位信息")
    @PostMapping("/xdxt0006")
    //@Idempotent({"xdcaxt0006", "#xdxt0006ReqDto.datasq"})
    protected @ResponseBody
    Xdxt0006RespDto xdxt0006(@Validated @RequestBody Xdxt0006ReqDto xdxt0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006ReqDto));
        Xdxt0006DataReqDto xdxt0006DataReqDto = new Xdxt0006DataReqDto();// 请求Data： 查询信贷用户的特定岗位信息
        Xdxt0006DataRespDto xdxt0006DataRespDto = new Xdxt0006DataRespDto();// 响应Data：查询信贷用户的特定岗位信息
        Xdxt0006RespDto xdxt0006RespDto = new Xdxt0006RespDto();
        cn.com.yusys.yusp.dto.server.oca.xdxt0006.req.Data reqData = null; // 请求Data：查询信贷用户的特定岗位信息
        cn.com.yusys.yusp.dto.server.oca.xdxt0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.oca.xdxt0006.resp.Data();// 响应Data：查询信贷用户的特定岗位信息

        try {
            // 从 xdxt0006ReqDto获取 reqData
            reqData = xdxt0006ReqDto.getData();
            // 将 reqData 拷贝到xdxt0006DataReqDto
            BeanUtils.copyProperties(reqData, xdxt0006DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataReqDto));
            ResultDto<Xdxt0006DataRespDto> xdxt0006DataResultDto = dscmsBizXtClientService.xdxt0006(xdxt0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxt0006RespDto.setErorcd(Optional.ofNullable(xdxt0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxt0006RespDto.setErortx(Optional.ofNullable(xdxt0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxt0006DataResultDto.getCode())) {
                xdxt0006DataRespDto = xdxt0006DataResultDto.getData();
                xdxt0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxt0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxt0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxt0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, e.getMessage());
            xdxt0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxt0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxt0006RespDto.setDatasq(servsq);

        xdxt0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXT0006.key, DscmsEnum.TRADE_CODE_XDXT0006.value, JSON.toJSONString(xdxt0006RespDto));
        return xdxt0006RespDto;
    }
}
