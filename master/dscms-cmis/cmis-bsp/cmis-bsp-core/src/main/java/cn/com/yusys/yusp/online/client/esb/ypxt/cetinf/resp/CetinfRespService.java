package cn.com.yusys.yusp.online.client.esb.ypxt.cetinf.resp;

/**
 * 响应Service：权证信息同步接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月14日 下午1:22:06
 */
public class CetinfRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
