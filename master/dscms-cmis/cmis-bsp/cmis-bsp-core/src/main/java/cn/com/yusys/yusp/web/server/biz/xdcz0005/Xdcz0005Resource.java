package cn.com.yusys.yusp.web.server.biz.xdcz0005;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0005.req.Xdcz0005ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0005.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0005.resp.Xdcz0005RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0005.req.Xdcz0005DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0005.resp.Xdcz0005DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:资产池入池接口
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0005:资产池入池接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0005Resource {
    private static final Logger logger = LoggerFactory.getLogger(cn.com.yusys.yusp.web.server.biz.xdcz0005.Xdcz0005Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0005
     * 交易描述：资产池入池接口
     *
     * @param xdcz0005ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("资产池入池接口")
    @PostMapping("/xdcz0005")
   //@Idempotent({"xdcz0005", "#xdcz0005ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0005RespDto xdcz0005(@Validated @RequestBody Xdcz0005ReqDto xdcz0005ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005ReqDto));
        Xdcz0005DataReqDto xdcz0005DataReqDto = new Xdcz0005DataReqDto();// 请求Data： 电子保函开立
        Xdcz0005DataRespDto xdcz0005DataRespDto = new Xdcz0005DataRespDto();// 响应Data：电子保函开立
        cn.com.yusys.yusp.dto.server.biz.xdcz0005.req.Data reqData = null; // 请求Data：电子保函开立
        cn.com.yusys.yusp.dto.server.biz.xdcz0005.resp.Data respData = new Data();// 响应Data：电子保函开立
        Xdcz0005RespDto xdcz0005RespDto = new Xdcz0005RespDto();
        try {
            // 从 xdcz0005ReqDto获取 reqData
            reqData = xdcz0005ReqDto.getData();
            // 将 reqData 拷贝到xdcz0005DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0005DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005DataReqDto));
            ResultDto<Xdcz0005DataRespDto> xdcz0005DataResultDto = dscmsBizCzClientService.xdcz0005(xdcz0005DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0005RespDto.setErorcd(Optional.ofNullable(xdcz0005DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0005RespDto.setErortx(Optional.ofNullable(xdcz0005DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0005DataResultDto.getCode())) {
                xdcz0005DataRespDto = xdcz0005DataResultDto.getData();
                xdcz0005RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0005RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0005DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0005DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, e.getMessage());
            xdcz0005RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0005RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0005RespDto.setDatasq(servsq);

        xdcz0005RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0005.key, DscmsEnum.TRADE_CODE_XDCZ0005.value, JSON.toJSONString(xdcz0005RespDto));
        return xdcz0005RespDto;
    }
}
