package cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp;
/**
 * 响应Service：查询还款（合约）明细历史表（利翃）中的全量借据一览
 * @author leehuang
 * @version 1.0
 */
public class List {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.rircp.fbxd14.resp.Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
