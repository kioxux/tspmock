package cn.com.yusys.yusp.online.client.esb.core.dp2280.req;

/**
 * 请求Service：子账户序号查询接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/16上午9:54:28
 */
public class Service {

    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //   全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String kehuzhao; // 客户账号
    private String zhhaoxuh; // 子账户序号
    private String yxzhuhbz; // 允许账户序号为空标志
    private String sfcxxhbz; // 是否查询虚账户标志
    private long qishibis; // 起始笔数
    private long chxunbis; // 查询笔数
    private String sfcxxhzh; // 是否查询销户账户标志
    private String sfcxglzh; // 是否查询关联账户

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getYxzhuhbz() {
        return yxzhuhbz;
    }

    public void setYxzhuhbz(String yxzhuhbz) {
        this.yxzhuhbz = yxzhuhbz;
    }

    public String getSfcxxhbz() {
        return sfcxxhbz;
    }

    public void setSfcxxhbz(String sfcxxhbz) {
        this.sfcxxhbz = sfcxxhbz;
    }

    public long getQishibis() {
        return qishibis;
    }

    public void setQishibis(long qishibis) {
        this.qishibis = qishibis;
    }

    public long getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(long chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getSfcxxhzh() {
        return sfcxxhzh;
    }

    public void setSfcxxhzh(String sfcxxhzh) {
        this.sfcxxhzh = sfcxxhzh;
    }

    public String getSfcxglzh() {
        return sfcxglzh;
    }

    public void setSfcxglzh(String sfcxglzh) {
        this.sfcxglzh = sfcxglzh;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", yxzhuhbz='" + yxzhuhbz + '\'' +
                ", sfcxxhbz='" + sfcxxhbz + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", sfcxxhzh='" + sfcxxhzh + '\'' +
                ", sfcxglzh='" + sfcxglzh + '\'' +
                '}';
    }
}