package cn.com.yusys.yusp.web.server.biz.xdtz0006;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0006.req.Xdtz0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0006.resp.Xdtz0006RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0006.req.Xdtz0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0006.resp.Xdtz0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据证件号查询借据信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0006:根据证件号查询借据信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0006Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0006
     * 交易描述：根据证件号查询借据信息
     *
     * @param xdtz0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据证件号查询借据信息")
    @PostMapping("/xdtz0006")
    //@Idempotent({"xdcatz0006", "#xdtz0006ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0006RespDto xdtz0006(@Validated @RequestBody Xdtz0006ReqDto xdtz0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(xdtz0006ReqDto));
        Xdtz0006DataReqDto xdtz0006DataReqDto = new Xdtz0006DataReqDto();// 请求Data： 根据证件号查询借据信息
        Xdtz0006DataRespDto xdtz0006DataRespDto = new Xdtz0006DataRespDto();// 响应Data：根据证件号查询借据信息
        Xdtz0006RespDto xdtz0006RespDto = new Xdtz0006RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0006.req.Data reqData = null; // 请求Data：根据证件号查询借据信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0006.resp.Data();// 响应Data：根据证件号查询借据信息
        try {
            // 从 xdtz0006ReqDto获取 reqData
            reqData = xdtz0006ReqDto.getData();
            // 将 reqData 拷贝到xdtz0006DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0006DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(xdtz0006DataReqDto));
            ResultDto<Xdtz0006DataRespDto> xdtz0006DataResultDto = dscmsBizTzClientService.xdtz0006(xdtz0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(xdtz0006DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0006RespDto.setErorcd(Optional.ofNullable(xdtz0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0006RespDto.setErortx(Optional.ofNullable(xdtz0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0006DataResultDto.getCode())) {
                xdtz0006DataRespDto = xdtz0006DataResultDto.getData();
                xdtz0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0006DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, e.getMessage());
            xdtz0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0006RespDto.setDatasq(servsq);

        xdtz0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0006.key, DscmsEnum.TRADE_CODE_XDTZ0006.value, JSON.toJSONString(xdtz0006RespDto));
        return xdtz0006RespDto;
    }
}
