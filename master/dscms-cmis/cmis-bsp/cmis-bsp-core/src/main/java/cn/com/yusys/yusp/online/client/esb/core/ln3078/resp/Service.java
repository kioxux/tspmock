package cn.com.yusys.yusp.online.client.esb.core.ln3078.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.outjiejxx.List;

/**
 * 响应Service：贷款机构变更
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String yewubhao;//业务编号
    private String jgzhyibz;//机构转移标志
    private String zhchjgou;//转出机构
    private String zhrujgou;//转入机构
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String huobdhao;//货币代号
    private String hetongbh;//合同编号
    private String jiaoyigy;//交易柜员
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private cn.com.yusys.yusp.online.client.esb.core.ln3078.resp.outjiejxx.List list;//借据输出信息[LIST]

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public String getJgzhyibz() {
        return jgzhyibz;
    }

    public void setJgzhyibz(String jgzhyibz) {
        this.jgzhyibz = jgzhyibz;
    }

    public String getZhchjgou() {
        return zhchjgou;
    }

    public void setZhchjgou(String zhchjgou) {
        this.zhchjgou = zhchjgou;
    }

    public String getZhrujgou() {
        return zhrujgou;
    }

    public void setZhrujgou(String zhrujgou) {
        this.zhrujgou = zhrujgou;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", yewubhao='" + yewubhao + '\'' +
                ", jgzhyibz='" + jgzhyibz + '\'' +
                ", zhchjgou='" + zhchjgou + '\'' +
                ", zhrujgou='" + zhrujgou + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", list=" + list +
                '}';
    }
}
