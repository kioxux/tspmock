package cn.com.yusys.yusp.web.client.esb.pjxt.xdpj24;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.req.List;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.req.Xdpj24ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp.Xdpj24RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req.Record;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req.Xdpj24ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp.Xdpj24RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类:从票据系统获取当日到期票的日出备款金额
 *
 * @author chenyong
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj24Resource {
	private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj24Resource.class);
	private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
	private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
	private final BeanMapUtil beanMapUtil = new BeanMapUtil();

	/**
	 * 交易码：xdpj24
	 * 交易描述：从票据系统获取当日到期票的日出备款金额
	 *
	 * @param xdpj24ReqDto
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/xdpj24")
	protected @ResponseBody
	ResultDto<Xdpj24RespDto> xdpj24(@Validated @RequestBody Xdpj24ReqDto xdpj24ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ24.key, EsbEnum.TRADE_CODE_XDPJ24.value, JSON.toJSONString(xdpj24ReqDto));
		cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req.Service();
		cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp.Service();
		Xdpj24ReqService xdpj24ReqService = new Xdpj24ReqService();
		Xdpj24RespService xdpj24RespService = new Xdpj24RespService();
		Xdpj24RespDto xdpj24RespDto = new Xdpj24RespDto();
		ResultDto<Xdpj24RespDto> xdpj24ResultDto = new ResultDto<Xdpj24RespDto>();
		Map<String, Object> context = new HashMap<>();
		Map<String, Object> result = new HashMap<>();
		try {
			//  将xdpj24ReqDto转换成reqService
			BeanUtils.copyProperties(xdpj24ReqDto, reqService);
			java.util.List<List> reqList = xdpj24ReqDto.getList();
			if (CollectionUtils.nonEmpty(reqList)) {
				java.util.List<Record> recordList = reqList.parallelStream().map(req -> {
					Record record = new Record();
					BeanUtils.copyProperties(req, record);
					return record;
				}).collect(Collectors.toList());
				cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req.List serviceList = new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.req.List();
				serviceList.setRecord(recordList);
				reqService.setList(serviceList);
			}
			reqService.setPrcscd(EsbEnum.TRADE_CODE_XDPJ24.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
			reqService.setDatasq(servsq);//    全局流水
			xdpj24ReqService.setService(reqService);
			// 将xdpj24ReqService转换成xdpj24ReqServiceMap
			Map xdpj24ReqServiceMap = beanMapUtil.beanToMap(xdpj24ReqService);
			context.put("tradeDataMap", xdpj24ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ24.key, EsbEnum.TRADE_CODE_XDPJ24.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDPJ24.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ24.key, EsbEnum.TRADE_CODE_XDPJ24.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			xdpj24RespService = beanMapUtil.mapToBean(tradeDataMap, Xdpj24RespService.class, Xdpj24RespService.class);
			respService = xdpj24RespService.getService();

			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成XdypbdccxRespDto

				BeanUtils.copyProperties(respService, xdpj24RespDto);
				cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp.List serviceList = Optional.ofNullable(respService.getList())
						.orElse(new cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp.List());
				java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.xdpj24.resp.Record> records = serviceList.getRecord();
				if (CollectionUtils.nonEmpty(records)) {
					java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp.List> respList = records.parallelStream().map(record -> {
						cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp.List temp = new cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp.List();
						BeanUtils.copyProperties(record, temp);
						return temp;
					}).collect(Collectors.toList());
					xdpj24RespDto.setList(respList);
				}
				xdpj24ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				xdpj24ResultDto.setMessage(respService.getErortx());
			} else {
				xdpj24ResultDto.setCode(EpbEnum.EPB099999.key);
				xdpj24ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ24.key, EsbEnum.TRADE_CODE_XDPJ24.value, e.getMessage());
			xdpj24ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			xdpj24ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		xdpj24ResultDto.setData(xdpj24RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDPJ24.key, EsbEnum.TRADE_CODE_XDPJ24.value, JSON.toJSONString(xdpj24ResultDto));
		return xdpj24ResultDto;
	}
}
