package cn.com.yusys.yusp.online.client.esb.edzfxt.hvpyls.resp;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/28 12:34
 * @since 2021/8/28 12:34
 */
public class Record {
    private String hstrdt;//业务受理日期
    private String hstrsq;//业务受理编号
    private String opertp;//业务类型
    private BigDecimal tranam;//交易金额
    private String jysts;//新交易状态（未翻译）
    private String transt;//交易状态

    public String getHstrdt() {
        return hstrdt;
    }

    public void setHstrdt(String hstrdt) {
        this.hstrdt = hstrdt;
    }

    public String getHstrsq() {
        return hstrsq;
    }

    public void setHstrsq(String hstrsq) {
        this.hstrsq = hstrsq;
    }

    public String getOpertp() {
        return opertp;
    }

    public void setOpertp(String opertp) {
        this.opertp = opertp;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getJysts() {
        return jysts;
    }

    public void setJysts(String jysts) {
        this.jysts = jysts;
    }

    public String getTranst() {
        return transt;
    }

    public void setTranst(String transt) {
        this.transt = transt;
    }

    @Override
    public String toString() {
        return "Record{" +
                "hstrdt='" + hstrdt + '\'' +
                ", hstrsq='" + hstrsq + '\'' +
                ", opertp='" + opertp + '\'' +
                ", tranam=" + tranam +
                ", jysts='" + jysts + '\'' +
                ", transt='" + transt + '\'' +
                '}';
    }
}
