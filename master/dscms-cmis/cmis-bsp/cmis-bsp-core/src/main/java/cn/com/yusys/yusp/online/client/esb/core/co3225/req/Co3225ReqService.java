package cn.com.yusys.yusp.online.client.esb.core.co3225.req;

/**
 * 请求Service：抵质押物明细查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Co3225ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
