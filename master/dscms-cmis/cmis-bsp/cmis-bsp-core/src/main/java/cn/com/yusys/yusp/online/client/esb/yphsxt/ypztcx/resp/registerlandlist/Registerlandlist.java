package cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerlandlist;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/9 14:15
 * @since 2021/7/9 14:15
 */
public class Registerlandlist {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.yphsxt.ypztcx.resp.registerlandlist.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Registerlandlist{" +
                "record=" + record +
                '}';
    }
}
