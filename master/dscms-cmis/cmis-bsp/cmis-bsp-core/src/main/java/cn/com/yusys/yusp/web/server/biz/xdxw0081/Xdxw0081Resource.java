package cn.com.yusys.yusp.web.server.biz.xdxw0081;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0081.req.Xdxw0081ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0081.resp.Xdxw0081RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0081.req.Xdxw0081DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0081.resp.Xdxw0081DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:检查是否有优农贷、优企贷、惠享贷合同
 *
 * @author zdl
 * @version 1.0
 */
@Api(tags = "XDXW0081:检查是否有优农贷、优企贷、惠享贷合同")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0081Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0081Resource.class);

    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0081
     * 交易描述：检查是否有优农贷、优企贷、惠享贷合同
     *
     * @param xdxw0081ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("检查是否有优农贷、优企贷、惠享贷合同")
    @PostMapping("/xdxw0081")
    @Idempotent({"xdxw0081", "#xdxw0081ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0081RespDto xdxw0081(@Validated @RequestBody Xdxw0081ReqDto xdxw0081ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0081ReqDto));
        Xdxw0081DataReqDto xdxw0081DataReqDto = new Xdxw0081DataReqDto();// 请求Data： 检查是否有优农贷、优企贷、惠享贷合同
        Xdxw0081DataRespDto xdxw0081DataRespDto = new Xdxw0081DataRespDto();// 响应Data：检查是否有优农贷、优企贷、惠享贷合同
        Xdxw0081RespDto xdxw0081RespDto = new Xdxw0081RespDto();
        // 此处包导入待确定 开始
        cn.com.yusys.yusp.dto.server.biz.xdxw0081.req.Data reqData = null; // 请求Data：检查是否有优农贷、优企贷、惠享贷合同
        cn.com.yusys.yusp.dto.server.biz.xdxw0081.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0081.resp.Data();// 响应Data：检查是否有优农贷、优企贷、惠享贷合同
        // 此处包导入待确定 结束
        try {
            // 从 xdxw0081ReqDto获取 reqData
            reqData = xdxw0081ReqDto.getData();
            // 将 reqData 拷贝到xdxw0081DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0081DataReqDto);
            // 调用服务：TODO
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0081DataReqDto));
            ResultDto<Xdxw0081DataRespDto> xdxw0081DataResultDto = dscmsBizXwClientService.xdxw0081(xdxw0081DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0081DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0081RespDto.setErorcd(Optional.ofNullable(xdxw0081DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0081RespDto.setErortx(Optional.ofNullable(xdxw0081DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0081DataResultDto.getCode())) {
                xdxw0081DataRespDto = xdxw0081DataResultDto.getData();
                xdxw0081RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0081RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0081DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0081DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, e.getMessage());
            xdxw0081RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0081RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0081RespDto.setDatasq(servsq);

        xdxw0081RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0081RespDto));
        return xdxw0081RespDto;
    }
}
