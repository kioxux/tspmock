package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdksfsj.Record;

import java.util.List;

//贷款收费事件

public class Lstdksfsj_ARRAY {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdksfsj.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdksfsj_ARRAY{" +
                "record=" + record +
                '}';
    }
}
