package cn.com.yusys.yusp.web.client.esb.irs.irs18;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.irs18.Irs18ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs18.Irs18RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.irs18.req.Irs18ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.irs18.resp.Irs18RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(irs18)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Irs18Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Irs18Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：irs18
     * 交易描述：评级主办权更新
     *
     * @param irs18ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("irs18:评级主办权更新")
    @PostMapping("/irs18")
    protected @ResponseBody
    ResultDto<Irs18RespDto> irs18(@Validated @RequestBody Irs18ReqDto irs18ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS18.key, EsbEnum.TRADE_CODE_IRS18.value, JSON.toJSONString(irs18ReqDto));

        cn.com.yusys.yusp.online.client.esb.irs.irs18.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.irs18.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.irs18.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.irs18.resp.Service();

        Irs18ReqService irs18ReqService = new Irs18ReqService();
        Irs18RespService irs18RespService = new Irs18RespService();
        Irs18RespDto irs18RespDto = new Irs18RespDto();
        ResultDto<Irs18RespDto> irs18ResultDto = new ResultDto<Irs18RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将irs18ReqDto转换成reqService
            BeanUtils.copyProperties(irs18ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS18.key);//    交易码
            LocalDateTime now = LocalDateTime.now();
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_FLS.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_FLS.key, EsbEnum.SERVTP_FLS.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            irs18ReqService.setService(reqService);
            // 将irs18ReqService转换成irs18ReqServiceMap
            Map irs18ReqServiceMap = beanMapUtil.beanToMap(irs18ReqService);
            context.put("tradeDataMap", irs18ReqServiceMap);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS18.key, context);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            irs18RespService = beanMapUtil.mapToBean(tradeDataMap, Irs18RespService.class, Irs18RespService.class);
            respService = irs18RespService.getService();

            irs18ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            irs18ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Irs18RespDto
                BeanUtils.copyProperties(respService, irs18RespDto);
                irs18ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                irs18ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                irs18ResultDto.setCode(EpbEnum.EPB099999.key);
                irs18ResultDto.setMessage(respService.getErortx());
            }
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS18.key, EsbEnum.TRADE_CODE_IRS18.value, e.getMessage());
            irs18ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            irs18ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        irs18ResultDto.setData(irs18RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS18.key, EsbEnum.TRADE_CODE_IRS18.value, JSON.toJSONString(irs18ResultDto));
        return irs18ResultDto;
    }
}
