package cn.com.yusys.yusp.web.server.biz.xdtz0003;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0003.req.Xdtz0003ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0003.resp.Xdtz0003RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0003.req.Xdtz0003DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0003.resp.Xdtz0003DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询小微借据余额
 *
 * @author zhangpeng
 * @version 1.0
 */
@Api(tags = "XDTZ0003:查询小微借据余额")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0003Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0003Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0003
     * 交易描述：查询小微借据余额
     *
     * @param xdtz0003ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询小微借据余额")
    @PostMapping("/xdtz0003")
    //@Idempotent({"xdcatz0003", "#xdtz0003ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0003RespDto xdtz0003(@Validated @RequestBody Xdtz0003ReqDto xdtz0003ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003ReqDto));
        Xdtz0003DataReqDto xdtz0003DataReqDto = new Xdtz0003DataReqDto();// 请求Data： 查询小微借据余额
        Xdtz0003DataRespDto xdtz0003DataRespDto = new Xdtz0003DataRespDto();// 响应Data：查询小微借据余额
        Xdtz0003RespDto xdtz0003RespDto = new Xdtz0003RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0003.req.Data reqData = null; // 请求Data：查询小微借据余额
        cn.com.yusys.yusp.dto.server.biz.xdtz0003.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0003.resp.Data();// 响应Data：查询小微借据余额

        try {
            // 从 xdtz0003ReqDto获取 reqData
            reqData = xdtz0003ReqDto.getData();
            // 将 reqData 拷贝到xdtz0003DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0003DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataReqDto));
            ResultDto<Xdtz0003DataRespDto> xdtz0003DataResultDto = dscmsBizTzClientService.xdtz0003(xdtz0003DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0003RespDto.setErorcd(Optional.ofNullable(xdtz0003DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0003RespDto.setErortx(Optional.ofNullable(xdtz0003DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0003DataResultDto.getCode())) {
                xdtz0003DataRespDto = xdtz0003DataResultDto.getData();
                xdtz0003RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0003RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0003DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0003DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, e.getMessage());
            xdtz0003RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0003RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0003RespDto.setDatasq(servsq);

        xdtz0003RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0003.key, DscmsEnum.TRADE_CODE_XDTZ0003.value, JSON.toJSONString(xdtz0003RespDto));
        return xdtz0003RespDto;
    }
}
