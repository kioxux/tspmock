package cn.com.yusys.yusp.online.client.esb.rircp.fbxd15.req;

/**
 * 请求Service：还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和
 */
public class Service {
	private String prcscd; // 处理码
	private String servtp; // 渠道
	private String servsq; // 渠道流水
	private String userid; // 柜员号
	private String brchno; // 部门号
    private String contract_no;//利翃平台贷款合同号

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", contract_no='" + contract_no + '\'' +
				'}';
	}
}
