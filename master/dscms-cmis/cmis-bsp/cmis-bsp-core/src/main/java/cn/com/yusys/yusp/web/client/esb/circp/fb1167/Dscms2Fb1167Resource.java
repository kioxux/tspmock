package cn.com.yusys.yusp.web.client.esb.circp.fb1167;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1167.req.Fb1167ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1167.resp.Fb1167RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.circp.fb1167.req.Fb1167ReqService;
import cn.com.yusys.yusp.online.client.esb.circp.fb1167.resp.Fb1167RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用对公智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用对公智能风控系统的接口处理类（fb1167）")
@RestController
@RequestMapping("/api/dscms2circp")
public class Dscms2Fb1167Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fb1167Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fb1167
     * 交易描述：企业征信查询通知
     *
     * @param fb1167ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fb1167")
    protected @ResponseBody
    ResultDto<Fb1167RespDto> fb1167(@Validated @RequestBody Fb1167ReqDto fb1167ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1167.key, EsbEnum.TRADE_CODE_FB1167.value, JSON.toJSONString(fb1167ReqDto));
        cn.com.yusys.yusp.online.client.esb.circp.fb1167.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.circp.fb1167.req.Service();
        cn.com.yusys.yusp.online.client.esb.circp.fb1167.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.circp.fb1167.resp.Service();
        Fb1167ReqService fb1167ReqService = new Fb1167ReqService();
        Fb1167RespService fb1167RespService = new Fb1167RespService();
        Fb1167RespDto fb1167RespDto = new Fb1167RespDto();
        ResultDto<Fb1167RespDto> fb1167ResultDto = new ResultDto<Fb1167RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fb1167ReqDto转换成reqService
            BeanUtils.copyProperties(fb1167ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FB1167.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_CIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            fb1167ReqService.setService(reqService);
            // 将fb1167ReqService转换成fb1167ReqServiceMap
            Map fb1167ReqServiceMap = beanMapUtil.beanToMap(fb1167ReqService);
            context.put("tradeDataMap", fb1167ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1167.key, EsbEnum.TRADE_CODE_FB1167.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FB1167.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1167.key, EsbEnum.TRADE_CODE_FB1167.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fb1167RespService = beanMapUtil.mapToBean(tradeDataMap, Fb1167RespService.class, Fb1167RespService.class);
            respService = fb1167RespService.getService();

            fb1167ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fb1167ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fb1167RespDto
                BeanUtils.copyProperties(respService, fb1167RespDto);

                fb1167ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fb1167ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fb1167ResultDto.setCode(EpbEnum.EPB099999.key);
                fb1167ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1167.key, EsbEnum.TRADE_CODE_FB1167.value, e.getMessage());
            fb1167ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fb1167ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fb1167ResultDto.setData(fb1167RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FB1167.key, EsbEnum.TRADE_CODE_FB1167.value, JSON.toJSONString(fb1167ResultDto));
        return fb1167ResultDto;
    }
}
