package cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req;

/**
 * 请求Service：d11005
 *
 * @author chenyong
 * @version 1.0
 */
public class D11005ReqMessage {
    private cn.com.yusys.yusp.online.client.gxp.tonglian.d11005.req.Message message;

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "D11005ReqMessage{" +
                "message=" + message +
                '}';
    }
}
