package cn.com.yusys.yusp.online.client.esb.irs.irs28.req;

/**
 * 请求Service：财务信息同步
 * @author code-generator
 * @version 1.0             
 */      
public class Irs28ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
