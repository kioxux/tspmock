package cn.com.yusys.yusp.online.client.esb.core.dp2352.req;

import java.math.BigDecimal;

/**
 * 推荐人信息列表
 * @author lihh
 * @version 1.0
 */
public class Record {


    private String tjrminc1;//推荐人名称1
    private String tjrgonh1;//推荐人工号1
    private BigDecimal tjrenbl1;//推荐人比例1

    public String getTjrminc1() {
        return tjrminc1;
    }

    public void setTjrminc1(String tjrminc1) {
        this.tjrminc1 = tjrminc1;
    }

    public String getTjrgonh1() {
        return tjrgonh1;
    }

    public void setTjrgonh1(String tjrgonh1) {
        this.tjrgonh1 = tjrgonh1;
    }

    public BigDecimal getTjrenbl1() {
        return tjrenbl1;
    }

    public void setTjrenbl1(BigDecimal tjrenbl1) {
        this.tjrenbl1 = tjrenbl1;
    }

    @Override
    public String toString() {
        return "Record{" +
                "tjrminc1='" + tjrminc1 + '\'' +
                ", tjrgonh1='" + tjrgonh1 + '\'' +
                ", tjrenbl1=" + tjrenbl1 +
                '}';
    }
}
