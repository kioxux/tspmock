package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 动产抵押-抵押权人信息
 */
@JsonPropertyOrder(alphabetic = true)
public class MORTGAGEPER implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "MAB_PER_CERNO")
    private String MAB_PER_CERNO;//	抵押权人证照/证件号码
    @JsonProperty(value = "MAB_PER_CERTYPE")
    private String MAB_PER_CERTYPE;//	抵押权人证照/证件类型
    @JsonProperty(value = "MAB_PER_DOM")
    private String MAB_PER_DOM;//	所在地
    @JsonProperty(value = "MAB_PER_NAME")
    private String MAB_PER_NAME;//	抵押权人名称
    @JsonProperty(value = "MAB_REGNO")
    private String MAB_REGNO;//	登记编号

    @JsonIgnore
    public String getMAB_PER_CERNO() {
        return MAB_PER_CERNO;
    }

    @JsonIgnore
    public void setMAB_PER_CERNO(String MAB_PER_CERNO) {
        this.MAB_PER_CERNO = MAB_PER_CERNO;
    }

    @JsonIgnore
    public String getMAB_PER_CERTYPE() {
        return MAB_PER_CERTYPE;
    }

    @JsonIgnore
    public void setMAB_PER_CERTYPE(String MAB_PER_CERTYPE) {
        this.MAB_PER_CERTYPE = MAB_PER_CERTYPE;
    }

    @JsonIgnore
    public String getMAB_PER_DOM() {
        return MAB_PER_DOM;
    }

    @JsonIgnore
    public void setMAB_PER_DOM(String MAB_PER_DOM) {
        this.MAB_PER_DOM = MAB_PER_DOM;
    }

    @JsonIgnore
    public String getMAB_PER_NAME() {
        return MAB_PER_NAME;
    }

    @JsonIgnore
    public void setMAB_PER_NAME(String MAB_PER_NAME) {
        this.MAB_PER_NAME = MAB_PER_NAME;
    }

    @JsonIgnore
    public String getMAB_REGNO() {
        return MAB_REGNO;
    }

    @JsonIgnore
    public void setMAB_REGNO(String MAB_REGNO) {
        this.MAB_REGNO = MAB_REGNO;
    }

    @Override
    public String toString() {
        return "MORTGAGEPER{" +
                "MAB_PER_CERNO='" + MAB_PER_CERNO + '\'' +
                ", MAB_PER_CERTYPE='" + MAB_PER_CERTYPE + '\'' +
                ", MAB_PER_DOM='" + MAB_PER_DOM + '\'' +
                ", MAB_PER_NAME='" + MAB_PER_NAME + '\'' +
                ", MAB_REGNO='" + MAB_REGNO + '\'' +
                '}';
    }
}
