package cn.com.yusys.yusp.web.server.biz.xdtz0062;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0062.req.Xdtz0062ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0062.resp.Xdtz0062RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0062.req.Xdtz0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0062.resp.Xdtz0062DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询客户的个人消费贷款
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0062:查询客户的个人消费贷款")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0062Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0062Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0062
     * 交易描述：查询客户的个人消费贷款
     *
     * @param xdtz0062ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询客户的个人消费贷款")
    @PostMapping("/xdtz0062")
    //@Idempotent({"xdtz0062", "#xdtz0062ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0062RespDto xdtz0062(@Validated @RequestBody Xdtz0062ReqDto xdtz0062ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062ReqDto));
        Xdtz0062DataReqDto xdtz0062DataReqDto = new Xdtz0062DataReqDto();// 请求Data： 更新信贷台账信息
        Xdtz0062DataRespDto xdtz0062DataRespDto = new Xdtz0062DataRespDto();// 响应Data：更新信贷台账信息
        Xdtz0062RespDto xdtz0062RespDto = new Xdtz0062RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0062.req.Data reqData = null; // 请求Data：更新信贷台账信息
        cn.com.yusys.yusp.dto.server.biz.xdtz0062.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0062.resp.Data();// 响应Data：更新信贷台账信息
        try {
            // 从 xdtz0062ReqDto获取 reqData
            reqData = xdtz0062ReqDto.getData();
            // 将 reqData 拷贝到xdtz0062DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0062DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataReqDto));
            ResultDto<Xdtz0062DataRespDto> xdtz0062DataResultDto = dscmsBizTzClientService.xdtz0062(xdtz0062DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0062RespDto.setErorcd(Optional.ofNullable(xdtz0062DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0062RespDto.setErortx(Optional.ofNullable(xdtz0062DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0062DataResultDto.getCode())) {
                xdtz0062DataRespDto = xdtz0062DataResultDto.getData();
                xdtz0062RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0062RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0062DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0062DataRespDto, respData);
            }else{
                xdtz0062RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
                xdtz0062RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, e.getMessage());
            xdtz0062RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0062RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0062RespDto.setDatasq(servsq);

        xdtz0062RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0062.key, DscmsEnum.TRADE_CODE_XDTZ0062.value, JSON.toJSONString(xdtz0062RespDto));
        return xdtz0062RespDto;
    }
}
