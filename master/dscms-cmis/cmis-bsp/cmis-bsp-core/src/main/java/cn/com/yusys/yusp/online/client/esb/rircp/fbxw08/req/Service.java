package cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.req;

/**
 * 请求Service：授信审批作废接口
 * @author muxiang
 * @version 1.0
 * @since 2021/4/16 16:37
 */
public class Service {

    private String prcscd; // 处理码
    private String servtp; // 渠道
    private String servsq; // 渠道流水
    private String userid; // 柜员号
    private String brchno; // 部门号
    private String channel_type; // 渠道来源
    private String co_platform; // 合作平台
    private String prd_code; // 产品代码（零售智能风控内部代码）
    private String cust_name; // 客户姓名
    private String cert_code; // 客户证件号码
    private String crd_cont_no; // 授信合同编号
    private String cancel_flag; // 作废标识

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCrd_cont_no() {
        return crd_cont_no;
    }

    public void setCrd_cont_no(String crd_cont_no) {
        this.crd_cont_no = crd_cont_no;
    }

    public String getCancel_flag() {
        return cancel_flag;
    }

    public void setCancel_flag(String cancel_flag) {
        this.cancel_flag = cancel_flag;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", channel_type='" + channel_type + '\'' +
                ", co_platform='" + co_platform + '\'' +
                ", prd_code='" + prd_code + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", crd_cont_no='" + crd_cont_no + '\'' +
                ", cancel_flag='" + cancel_flag + '\'' +
                '}';
    }
}
