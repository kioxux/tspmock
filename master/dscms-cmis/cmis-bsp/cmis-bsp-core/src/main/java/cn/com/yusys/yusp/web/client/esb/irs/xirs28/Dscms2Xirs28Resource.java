package cn.com.yusys.yusp.web.client.esb.irs.xirs28;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Xirs28ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.resp.Xirs28RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.AssetDebt;
import cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.Loss;
import cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.Xirs28ReqService;
import cn.com.yusys.yusp.online.client.esb.irs.xirs28.resp.Xirs28RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 调用非零内评系统的接口处理类
 **/
@Api(tags = "BSP封装调用非零内评系统的接口处理类(xirs28)")
@RestController
@RequestMapping("/api/dscms2irs")
public class Dscms2Xirs28Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Xirs28Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 财务信息同步（处理码IRS28）
     *
     * @param xirs28ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xirs28:财务信息同步")
    @PostMapping("/xirs28")
    protected @ResponseBody
    ResultDto<Xirs28RespDto> xirs28(@Validated @RequestBody Xirs28ReqDto xirs28ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value, JSON.toJSONString(xirs28ReqDto));
        cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.Service();
        cn.com.yusys.yusp.online.client.esb.irs.xirs28.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.irs.xirs28.resp.Service();
        AssetDebt assetDebt = new AssetDebt();
        Loss loss = new Loss();
        Xirs28ReqService xirs28ReqService = new Xirs28ReqService();
        Xirs28RespService xirs28RespService = new Xirs28RespService();
        Xirs28RespDto xirs28RespDto = new Xirs28RespDto();
        ResultDto<Xirs28RespDto> xirs28ResultDto = new ResultDto<Xirs28RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Xirs28ReqDto转换成reqService
            BeanUtils.copyProperties(xirs28ReqDto, reqService);
            if (CollectionUtils.nonEmpty(xirs28ReqDto.getAssetDebt())) {
                List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.AssetDebt> assetDebtList = xirs28ReqDto.getAssetDebt();
                List<cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.assetDebt.Record> recordList1 = new ArrayList<>();
                for (cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.AssetDebt a : assetDebtList) {
                    cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.assetDebt.Record target1 = new cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.assetDebt.Record();
                    BeanUtils.copyProperties(a, target1);
                    recordList1.add(target1);
                }
                assetDebt.setRecord(recordList1);
                reqService.setAssetDebt(assetDebt);
            }

            if (CollectionUtils.nonEmpty(xirs28ReqDto.getLoss())) {
                List<cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Loss> lossList = xirs28ReqDto.getLoss();
                List<cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.loss.Record> recordList2 = new ArrayList<>();
                for (cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Loss s : lossList) {
                    cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.loss.Record target2 = new cn.com.yusys.yusp.online.client.esb.irs.xirs28.req.loss.Record();
                    BeanUtils.copyProperties(s, target2);
                    recordList2.add(target2);
                }
                loss.setRecord(recordList2);
                reqService.setLoss(loss);
            }


            reqService.setPrcscd(EsbEnum.TRADE_CODE_IRS28.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_IRS.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_IRS.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            xirs28ReqService.setService(reqService);
            // 将xirs28ReqService转换成xirs28ReqServiceMap
            Map xirs28ReqServiceMap = beanMapUtil.beanToMap(xirs28ReqService);
            context.put("tradeDataMap", xirs28ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_IRS28.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xirs28RespService = beanMapUtil.mapToBean(tradeDataMap, Xirs28RespService.class, Xirs28RespService.class);
            respService = xirs28RespService.getService();

            //  将Xirs28RespDto封装到ResultDto<Xirs28RespDto>
            xirs28ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xirs28ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Xirs28RespDto
                BeanUtils.copyProperties(respService, xirs28RespDto);
                xirs28ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xirs28ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xirs28ResultDto.setCode(EpbEnum.EPB099999.key);
                xirs28ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value, e.getMessage());
            xirs28ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xirs28ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xirs28ResultDto.setData(xirs28RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IRS28.key, EsbEnum.TRADE_CODE_IRS28.value, JSON.toJSONString(xirs28ResultDto));
        return xirs28ResultDto;

    }
}
