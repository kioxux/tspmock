package cn.com.yusys.yusp.web.server.biz.xdtz0015;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0015.req.Xdtz0015ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0015.resp.Xdtz0015RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0015.req.Xdtz0015DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0015.resp.Xdtz0015DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:贴现记账结果通知
 *
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDTZ0015:贴现记账结果通知")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0015Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0015Resource.class);

	@Autowired
	private DscmsBizTzClientService dscmsBizTzClientService;
    /**
     * 交易码：xdtz0015
     * 交易描述：贴现记账结果通知
     *
     * @param xdtz0015ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("贴现记账结果通知")
    @PostMapping("/xdtz0015")
    //@Idempotent({"xdcatz0015", "#xdtz0015ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0015RespDto xdtz0015(@Validated @RequestBody Xdtz0015ReqDto xdtz0015ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015ReqDto));
        Xdtz0015DataReqDto xdtz0015DataReqDto = new Xdtz0015DataReqDto();// 请求Data： 贴现记账结果通知
        Xdtz0015DataRespDto xdtz0015DataRespDto = new Xdtz0015DataRespDto();// 响应Data：贴现记账结果通知
		Xdtz0015RespDto xdtz0015RespDto = new Xdtz0015RespDto();

		cn.com.yusys.yusp.dto.server.biz.xdtz0015.req.Data reqData = null; // 请求Data：贴现记账结果通知
		cn.com.yusys.yusp.dto.server.biz.xdtz0015.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0015.resp.Data();// 响应Data：贴现记账结果通知

        try {
            // 从 xdtz0015ReqDto获取 reqData
            reqData = xdtz0015ReqDto.getData();
            // 将 reqData 拷贝到xdtz0015DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0015DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataReqDto));
            ResultDto<Xdtz0015DataRespDto> xdtz0015DataResultDto = dscmsBizTzClientService.xdtz0015(xdtz0015DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0015RespDto.setErorcd(Optional.ofNullable(xdtz0015DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0015RespDto.setErortx(Optional.ofNullable(xdtz0015DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0015DataResultDto.getCode())) {
                xdtz0015DataRespDto = xdtz0015DataResultDto.getData();
                xdtz0015RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0015RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0015DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0015DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, e.getMessage());
            xdtz0015RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0015RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0015RespDto.setDatasq(servsq);

        xdtz0015RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0015.key, DscmsEnum.TRADE_CODE_XDTZ0015.value, JSON.toJSONString(xdtz0015RespDto));
        return xdtz0015RespDto;
    }
}
