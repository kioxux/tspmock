package cn.com.yusys.yusp.online.client.esb.core.co3200.resp;

/**
 * 响应Service：抵质押物的开户
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日16:06:35
 */
public class Co3200RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
