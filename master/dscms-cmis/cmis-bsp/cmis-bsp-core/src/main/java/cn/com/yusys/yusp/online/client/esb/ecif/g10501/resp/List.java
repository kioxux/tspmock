package cn.com.yusys.yusp.online.client.esb.ecif.g10501.resp;

/**
 * 响应Service：对公及同业客户清单查询
 */
public class List {
    private java.util.List<Record> record;

    public java.util.List<Record> getRecord() {
        return record;
    }

    public void setRecord(java.util.List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List{" +
                "record=" + record +
                '}';
    }
}
