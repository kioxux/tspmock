package cn.com.yusys.yusp.web.client.esb.core.co3201;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.co3201.Co3201ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3201.Co3201RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.co3201.req.Co3201ReqService;
import cn.com.yusys.yusp.online.client.esb.core.co3201.req.Record;
import cn.com.yusys.yusp.online.client.esb.core.co3201.resp.Co3201RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(co3201)")
@RestController
@RequestMapping("/api/dscms2coreco")
public class Dscms2Co3201Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Co3201Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 此交易用于抵质押物入库之后，可以增加、修改、抵质押物信息以及关联关系（处理码co3201）
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("co3201:此交易用于抵质押物入库之后，可以增加、修改、抵质押物信息以及关联关系")
    @PostMapping("/co3201")
    protected @ResponseBody
    ResultDto<Co3201RespDto> co3201(@Validated @RequestBody Co3201ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3201.key, EsbEnum.TRADE_CODE_CO3201.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.core.co3201.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.co3201.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.co3201.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.co3201.resp.Service();
        Co3201ReqService co3201ReqService = new Co3201ReqService();
        Co3201RespService co3201RespService = new Co3201RespService();
        Co3201RespDto co3201RespDto = new Co3201RespDto();
        ResultDto<Co3201RespDto> co3201ResultDto = new ResultDto<Co3201RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将Co3201ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(reqDto.getLstklnb_dkzhzy())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.co3201.Lstklnb_dkzhzy> lstklnb_dkzhzys = Optional.ofNullable(reqDto.getLstklnb_dkzhzy()).orElseGet(() -> new ArrayList<cn.com.yusys.yusp.dto.client.esb.core.co3201.Lstklnb_dkzhzy>());
                cn.com.yusys.yusp.online.client.esb.core.co3201.req.Lstklnb_dkzhzy lstklnb_dkzhzyService = new cn.com.yusys.yusp.online.client.esb.core.co3201.req.Lstklnb_dkzhzy();
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.co3201.req.Record> records = lstklnb_dkzhzys.parallelStream().map(lstklnb_dkzhzy -> {
                    cn.com.yusys.yusp.online.client.esb.core.co3201.req.Record record = new Record();
                    BeanUtils.copyProperties(lstklnb_dkzhzy, record);
                    return record;
                }).collect(Collectors.toList());
                lstklnb_dkzhzyService.setRecord(records);
                reqService.setLstklnb_dkzhzy(lstklnb_dkzhzyService);
            }

            // 塞入报文头固定字段0
            reqService.setPrcscd(EsbEnum.TRADE_CODE_CO3201.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号

            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            co3201ReqService.setService(reqService);
            // 将co3201ReqService转换成co3201ReqServiceMap
            Map co3201ReqServiceMap = beanMapUtil.beanToMap(co3201ReqService);
            context.put("tradeDataMap", co3201ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3201.key, EsbEnum.TRADE_CODE_CO3201.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_CO3201.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3201.key, EsbEnum.TRADE_CODE_CO3201.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            co3201RespService = beanMapUtil.mapToBean(tradeDataMap, Co3201RespService.class, Co3201RespService.class);
            respService = co3201RespService.getService();

            //  将Co3201RespDto封装到ResultDto<Co3201RespDto>
            co3201ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            co3201ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Co3201RespDto
                BeanUtils.copyProperties(respService, co3201RespDto);
                co3201ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                co3201ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                co3201ResultDto.setCode(EpbEnum.EPB099999.key);
                co3201ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3201.key, EsbEnum.TRADE_CODE_CO3201.value, e.getMessage());
            co3201ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            co3201ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常

        }
        co3201ResultDto.setData(co3201RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_CO3201.key, EsbEnum.TRADE_CODE_CO3201.value, JSON.toJSONString(co3201ResultDto));
        return co3201ResultDto;
    }


}
