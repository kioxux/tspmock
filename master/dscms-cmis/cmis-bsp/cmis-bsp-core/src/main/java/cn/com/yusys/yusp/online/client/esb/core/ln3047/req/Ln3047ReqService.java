package cn.com.yusys.yusp.online.client.esb.core.ln3047.req;

/**
 * 请求Service：质押还贷
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3047ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3047ReqService{" +
                "service=" + service +
                '}';
    }
}
