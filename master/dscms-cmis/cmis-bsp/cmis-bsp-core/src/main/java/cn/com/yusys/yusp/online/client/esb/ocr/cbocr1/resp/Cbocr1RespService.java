package cn.com.yusys.yusp.online.client.esb.ocr.cbocr1.resp;

/**
 * 响应Service：新增批次报表数据
 * @author code-generator
 * @version 1.0             
 */      
public class Cbocr1RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
