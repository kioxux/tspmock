package cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp;

import cn.com.yusys.yusp.online.client.esb.rlzyxt.xxdrel.resp.list2.Record;

import java.util.List;
/**
 * 响应Service：查询人员基本信息岗位信息家庭信息
 *
 * @author lihh
 * @version 1.0
 */
public class List2 {
    private List<Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "List2{" +
                "record=" + record +
                '}';
    }
}
