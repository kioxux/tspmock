package cn.com.yusys.yusp.online.client.esb.core.ln3043.req;

import java.math.BigDecimal;
import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/5/28 11:20:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/28 11:20
 * @since 2021/5/28 11:20
 */
public class Tblqgmx {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3043.req.Record> recordList;

    public List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        return "Tblqgmx{" +
                "recordList=" + recordList +
                '}';
    }
}
