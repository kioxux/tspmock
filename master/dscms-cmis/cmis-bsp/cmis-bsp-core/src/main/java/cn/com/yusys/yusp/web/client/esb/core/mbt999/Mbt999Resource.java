package cn.com.yusys.yusp.web.client.esb.core.mbt999;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.mbt999.IbmingxOut;
import cn.com.yusys.yusp.dto.client.esb.core.mbt999.Mbt999ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt999.Mbt999RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.mbt999.req.Mbt999ReqService;
import cn.com.yusys.yusp.online.client.esb.core.mbt999.resp.List;
import cn.com.yusys.yusp.online.client.esb.core.mbt999.resp.Mbt999RespService;
import cn.com.yusys.yusp.online.client.esb.core.mbt999.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:V5通用记账
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装调用核心系统的接口处理类(mbt999)")
@RestController
@RequestMapping("/api/dscms2corembt")
public class Mbt999Resource {
    private static final Logger logger = LoggerFactory.getLogger(Mbt999Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：mbt999
     * 交易描述：V5通用记账
     *
     * @param mbt999ReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("mbt999:V5通用记账")
    @PostMapping("/mbt999")
    protected @ResponseBody
    ResultDto<Mbt999RespDto> mbt999(@Validated @RequestBody Mbt999ReqDto mbt999ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT999.key, EsbEnum.TRADE_CODE_MBT999.value, JSON.toJSONString(mbt999ReqDto));
		cn.com.yusys.yusp.online.client.esb.core.mbt999.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.mbt999.req.Service();
		cn.com.yusys.yusp.online.client.esb.core.mbt999.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.mbt999.resp.Service();
        Mbt999ReqService mbt999ReqService = new Mbt999ReqService();
        Mbt999RespService mbt999RespService = new Mbt999RespService();
        Mbt999RespDto mbt999RespDto = new Mbt999RespDto();
        ResultDto<Mbt999RespDto> mbt999ResultDto = new ResultDto<Mbt999RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将mbt999ReqDto转换成reqService
			BeanUtils.copyProperties(mbt999ReqDto, reqService);

			reqService.setPrcscd(EsbEnum.TRADE_CODE_MBT999.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setDatasq(servsq);//    全局流水
			reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
			LocalDateTime now = LocalDateTime.now();
			reqService.setServdt(tranDateFormtter.format(now));//    交易日期
			reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

			mbt999ReqService.setService(reqService);
			// 将mbt999ReqService转换成mbt999ReqServiceMap
			Map mbt999ReqServiceMap = beanMapUtil.beanToMap(mbt999ReqService);
			context.put("tradeDataMap", mbt999ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT999.key, EsbEnum.TRADE_CODE_MBT999.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_MBT999.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT999.key, EsbEnum.TRADE_CODE_MBT999.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");
			mbt999RespService = beanMapUtil.mapToBean(tradeDataMap, Mbt999RespService.class, Mbt999RespService.class);
			respService = mbt999RespService.getService();
			//  将respService转换成Mbt999RespDto
			BeanUtils.copyProperties(respService, mbt999RespDto);
			mbt999ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			mbt999ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Ln3246RespDto
				BeanUtils.copyProperties(respService, mbt999RespDto);
				List list = Optional.ofNullable(respService.getList()).orElse(new List());
				java.util.List<Record> records = Optional.ofNullable(list.getRecord()).orElse(new ArrayList<>());
				java.util.List<IbmingxOut> ibmingxOuts = records.parallelStream().map(record -> {
					IbmingxOut ibmingxOut = new IbmingxOut();
					BeanUtils.copyProperties(record, ibmingxOut);
					return ibmingxOut;
				}).collect(Collectors.toList());
				mbt999RespDto.setIbmingxOut(ibmingxOuts);
				mbt999ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				mbt999ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				mbt999ResultDto.setCode(EpbEnum.EPB099999.key);
				mbt999ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT999.key, EsbEnum.TRADE_CODE_MBT999.value, e.getMessage());
			mbt999ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			mbt999ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		mbt999ResultDto.setData(mbt999RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_MBT999.key, EsbEnum.TRADE_CODE_MBT999.value, JSON.toJSONString(mbt999ResultDto));
        return mbt999ResultDto;
    }
}
