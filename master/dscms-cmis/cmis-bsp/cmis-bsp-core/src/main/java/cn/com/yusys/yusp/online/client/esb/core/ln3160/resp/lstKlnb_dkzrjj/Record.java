package cn.com.yusys.yusp.online.client.esb.core.ln3160.resp.lstKlnb_dkzrjj;
/**
 * 响应Service：资产证券化信息查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String xieybhao;//协议编号
    private String picihaoo;//批次号
    private String qiandriq;//签订日期
    private String fengbriq;//封包日期
    private String jiebriqi;//解包日期
    private String ruchiriq;//入池日期
    private String huigriqi;//回购日期
    private String zchzhtai;//资产处理状态
    private String fuheztai;//复核状态
    private String byxinxi1;//备用信息1
    private String byxinxi2;//备用信息2
    private String byxinxi3;//备用信息3
    private String ssfenbbz;//实时封包标志

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public String getQiandriq() {
        return qiandriq;
    }

    public void setQiandriq(String qiandriq) {
        this.qiandriq = qiandriq;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getJiebriqi() {
        return jiebriqi;
    }

    public void setJiebriqi(String jiebriqi) {
        this.jiebriqi = jiebriqi;
    }

    public String getRuchiriq() {
        return ruchiriq;
    }

    public void setRuchiriq(String ruchiriq) {
        this.ruchiriq = ruchiriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public String getZchzhtai() {
        return zchzhtai;
    }

    public void setZchzhtai(String zchzhtai) {
        this.zchzhtai = zchzhtai;
    }

    public String getFuheztai() {
        return fuheztai;
    }

    public void setFuheztai(String fuheztai) {
        this.fuheztai = fuheztai;
    }

    public String getByxinxi1() {
        return byxinxi1;
    }

    public void setByxinxi1(String byxinxi1) {
        this.byxinxi1 = byxinxi1;
    }

    public String getByxinxi2() {
        return byxinxi2;
    }

    public void setByxinxi2(String byxinxi2) {
        this.byxinxi2 = byxinxi2;
    }

    public String getByxinxi3() {
        return byxinxi3;
    }

    public void setByxinxi3(String byxinxi3) {
        this.byxinxi3 = byxinxi3;
    }

    public String getSsfenbbz() {
        return ssfenbbz;
    }

    public void setSsfenbbz(String ssfenbbz) {
        this.ssfenbbz = ssfenbbz;
    }

    @Override
    public String toString() {
        return "Record{" +
                "xieybhao='" + xieybhao + '\'' +
                ", picihaoo='" + picihaoo + '\'' +
                ", qiandriq='" + qiandriq + '\'' +
                ", fengbriq='" + fengbriq + '\'' +
                ", jiebriqi='" + jiebriqi + '\'' +
                ", ruchiriq='" + ruchiriq + '\'' +
                ", huigriqi='" + huigriqi + '\'' +
                ", zchzhtai='" + zchzhtai + '\'' +
                ", fuheztai='" + fuheztai + '\'' +
                ", byxinxi1='" + byxinxi1 + '\'' +
                ", byxinxi2='" + byxinxi2 + '\'' +
                ", byxinxi3='" + byxinxi3 + '\'' +
                ", ssfenbbz='" + ssfenbbz + '\'' +
                '}';
    }
}
