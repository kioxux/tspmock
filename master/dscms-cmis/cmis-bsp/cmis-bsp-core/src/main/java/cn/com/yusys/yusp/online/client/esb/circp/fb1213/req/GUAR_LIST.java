package cn.com.yusys.yusp.online.client.esb.circp.fb1213.req;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 16:44
 */
public class GUAR_LIST {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.circp.fb1213.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "GUAR_LIST{" +
                "record=" + record +
                '}';
    }
}
