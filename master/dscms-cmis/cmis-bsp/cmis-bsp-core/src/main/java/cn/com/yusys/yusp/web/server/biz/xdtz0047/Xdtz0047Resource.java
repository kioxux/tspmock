package cn.com.yusys.yusp.web.server.biz.xdtz0047;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0047.req.Xdtz0047ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0047.resp.Xdtz0047RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0047.req.Xdtz0047DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0047.resp.Xdtz0047DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:借据信息查询（按证件号）
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0047:借据信息查询（按证件号）")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0047Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0047Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0047
     * 交易描述：借据信息查询（按证件号）
     *
     * @param xdtz0047ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("借据信息查询（按证件号）")
    @PostMapping("/xdtz0047")
    //@Idempotent({"xdcatz0047", "#xdtz0047ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0047RespDto xdtz0047(@Validated @RequestBody Xdtz0047ReqDto xdtz0047ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047ReqDto));
        Xdtz0047DataReqDto xdtz0047DataReqDto = new Xdtz0047DataReqDto();// 请求Data： 借据信息查询（按证件号）
        Xdtz0047DataRespDto xdtz0047DataRespDto = new Xdtz0047DataRespDto();// 响应Data：借据信息查询（按证件号）
        Xdtz0047RespDto xdtz0047RespDto = new Xdtz0047RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0047.req.Data reqData = null; // 请求Data：借据信息查询（按证件号）
        cn.com.yusys.yusp.dto.server.biz.xdtz0047.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0047.resp.Data();// 响应Data：借据信息查询（按证件号）
        try {
            // 从 xdtz0047ReqDto获取 reqData
            reqData = xdtz0047ReqDto.getData();
            // 将 reqData 拷贝到xdtz0047DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0047DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047DataReqDto));
            ResultDto<Xdtz0047DataRespDto> xdtz0047DataResultDto = dscmsBizTzClientService.xdtz0047(xdtz0047DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdtz0047RespDto.setErorcd(Optional.ofNullable(xdtz0047DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0047RespDto.setErortx(Optional.ofNullable(xdtz0047DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0047DataResultDto.getCode())) {
                xdtz0047DataRespDto = xdtz0047DataResultDto.getData();
                xdtz0047RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0047RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0047DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0047DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, e.getMessage());
            xdtz0047RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0047RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0047RespDto.setDatasq(servsq);

        xdtz0047RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0047.key, DscmsEnum.TRADE_CODE_XDTZ0047.value, JSON.toJSONString(xdtz0047RespDto));
        return xdtz0047RespDto;
    }
}
