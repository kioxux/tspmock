package cn.com.yusys.yusp.online.client.esb.core.ln3053.resp;

/**
 * <br>
 * 0.2ZRC:2021/5/25 20:23:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 20:23
 * @since 2021/5/25 20:23
 */

import java.math.BigDecimal;

/**
 * 响应Service：还款计划调整
 *
 * @author code-generator
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String dkjiejuh;//贷款借据号
    private String kehuhaoo;//客户号
    private String kehmingc;//客户名称
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private String qixiriqi;//起息日期
    private String daoqriqi;//到期日期
    private String jiaoyijg;//交易机构
    private String jiaoyigy;//交易柜员
    private List list;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", list=" + list +
                '}';
    }
}

