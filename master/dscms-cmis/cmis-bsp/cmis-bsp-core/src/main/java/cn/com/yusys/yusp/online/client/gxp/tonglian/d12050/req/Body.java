package cn.com.yusys.yusp.online.client.gxp.tonglian.d12050.req;

/**
 * 请求Body：账单列表查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Body {
    private String cardno;//卡号
    private String crcycd;//币种
    private String stsart;//账单起始年月
    private String enddat;//账单截止日期
    private String smendt;//发送日期
    private String frtrow;//开始位置
    private String lstrow;//结束位置

    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCrcycd() {
        return crcycd;
    }

    public void setCrcycd(String crcycd) {
        this.crcycd = crcycd;
    }

    public String getStsart() {
        return stsart;
    }

    public void setStsart(String stsart) {
        this.stsart = stsart;
    }

    public String getEnddat() {
        return enddat;
    }

    public void setEnddat(String enddat) {
        this.enddat = enddat;
    }

    public String getSmendt() {
        return smendt;
    }

    public void setSmendt(String smendt) {
        this.smendt = smendt;
    }

    public String getFrtrow() {
        return frtrow;
    }

    public void setFrtrow(String frtrow) {
        this.frtrow = frtrow;
    }

    public String getLstrow() {
        return lstrow;
    }

    public void setLstrow(String lstrow) {
        this.lstrow = lstrow;
    }

    @Override
    public String toString() {
        return "Body{" +
                "cardno='" + cardno + '\'' +
                ", crcycd='" + crcycd + '\'' +
                ", stsart='" + stsart + '\'' +
                ", enddat='" + enddat + '\'' +
                ", smendt='" + smendt + '\'' +
                ", frtrow='" + frtrow + '\'' +
                ", lstrow='" + lstrow + '\'' +
                '}';
    }
}
