package cn.com.yusys.yusp.web.server.biz.xdtz0043;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0043.req.Xdtz0043ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0043.resp.Xdtz0043RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0043.req.Xdtz0043DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0043.resp.Xdtz0043DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:统计客户行内信用类贷款余额
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "XDTZ0043:统计客户行内信用类贷款余额")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0043Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0043Resource.class);

    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0043
     * 交易描述：统计客户行内信用类贷款余额
     *
     * @param xdtz0043ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("统计客户行内信用类贷款余额")
    @PostMapping("/xdtz0043")
    //@Idempotent({"xdcatz0043", "#xdtz0043ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0043RespDto xdtz0043(@Validated @RequestBody Xdtz0043ReqDto xdtz0043ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(xdtz0043ReqDto));
        Xdtz0043DataReqDto xdtz0043DataReqDto = new Xdtz0043DataReqDto();// 请求Data： 统计客户行内信用类贷款余额
        Xdtz0043DataRespDto xdtz0043DataRespDto = new Xdtz0043DataRespDto();// 响应Data：统计客户行内信用类贷款余额
        Xdtz0043RespDto xdtz0043RespDto = new Xdtz0043RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdtz0043.req.Data reqData = null; // 请求Data：统计客户行内信用类贷款余额
        cn.com.yusys.yusp.dto.server.biz.xdtz0043.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0043.resp.Data();// 响应Data：统计客户行内信用类贷款余额
        try {
            // 从 xdtz0043ReqDto获取 reqData
            reqData = xdtz0043ReqDto.getData();
            // 将 reqData 拷贝到xdtz0043DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0043DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(xdtz0043DataReqDto));
            ResultDto<Xdtz0043DataRespDto> xdtz0043DataResultDto = dscmsBizTzClientService.xdtz0043(xdtz0043DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(xdtz0043DataResultDto));
            // 从返回值中获取响应码和响应消息

            xdtz0043RespDto.setErorcd(Optional.ofNullable(xdtz0043DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0043RespDto.setErortx(Optional.ofNullable(xdtz0043DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0043DataResultDto.getCode())) {
                xdtz0043DataRespDto = xdtz0043DataResultDto.getData();
                xdtz0043RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0043RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0043DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0043DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, e.getMessage());
            xdtz0043RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0043RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0043RespDto.setDatasq(servsq);
        xdtz0043RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0043.key, DscmsEnum.TRADE_CODE_XDTZ0043.value, JSON.toJSONString(xdtz0043RespDto));
        return xdtz0043RespDto;
    }
}
