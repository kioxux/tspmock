package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 上市股票基本信息
 */
@JsonPropertyOrder(alphabetic = true)
public class LISTEDINFO implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "COMPSNAME")
    private String COMPSNAME;//	上市公司企业简称
    @JsonProperty(value = "EXCHANGE")
    private String EXCHANGE;//	上市场交易所
    @JsonProperty(value = "EXCHANGECODE")
    private String EXCHANGECODE;//	上市场交易所编码
    @JsonProperty(value = "LISTDATE")
    private String LISTDATE;//	股票上市日期
    @JsonProperty(value = "LISTSTATUS")
    private String LISTSTATUS;//	股票上市状态代码
    @JsonProperty(value = "SETYPE")
    private String SETYPE;//	证券类别代码
    @JsonProperty(value = "SYMBOL")
    private String SYMBOL;//	股票代码

    @JsonIgnore
    public String getCOMPSNAME() {
        return COMPSNAME;
    }

    @JsonIgnore
    public void setCOMPSNAME(String COMPSNAME) {
        this.COMPSNAME = COMPSNAME;
    }

    @JsonIgnore
    public String getEXCHANGE() {
        return EXCHANGE;
    }

    @JsonIgnore
    public void setEXCHANGE(String EXCHANGE) {
        this.EXCHANGE = EXCHANGE;
    }

    @JsonIgnore
    public String getEXCHANGECODE() {
        return EXCHANGECODE;
    }

    @JsonIgnore
    public void setEXCHANGECODE(String EXCHANGECODE) {
        this.EXCHANGECODE = EXCHANGECODE;
    }

    @JsonIgnore
    public String getLISTDATE() {
        return LISTDATE;
    }

    @JsonIgnore
    public void setLISTDATE(String LISTDATE) {
        this.LISTDATE = LISTDATE;
    }

    @JsonIgnore
    public String getLISTSTATUS() {
        return LISTSTATUS;
    }

    @JsonIgnore
    public void setLISTSTATUS(String LISTSTATUS) {
        this.LISTSTATUS = LISTSTATUS;
    }

    @JsonIgnore
    public String getSETYPE() {
        return SETYPE;
    }

    @JsonIgnore
    public void setSETYPE(String SETYPE) {
        this.SETYPE = SETYPE;
    }

    @JsonIgnore
    public String getSYMBOL() {
        return SYMBOL;
    }

    @JsonIgnore
    public void setSYMBOL(String SYMBOL) {
        this.SYMBOL = SYMBOL;
    }

    @Override
    public String toString() {
        return "LISTEDINFO{" +
                "COMPSNAME='" + COMPSNAME + '\'' +
                ", EXCHANGE='" + EXCHANGE + '\'' +
                ", EXCHANGECODE='" + EXCHANGECODE + '\'' +
                ", LISTDATE='" + LISTDATE + '\'' +
                ", LISTSTATUS='" + LISTSTATUS + '\'' +
                ", SETYPE='" + SETYPE + '\'' +
                ", SYMBOL='" + SYMBOL + '\'' +
                '}';
    }
}
