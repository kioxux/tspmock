package cn.com.yusys.yusp.online.client.esb.core.dp2352.resp;

import java.math.BigDecimal;

/**
 * 响应Service：组合账户子账户开立
 * @author lihh
 * @version 1.0
 */
public class Service {
	private String erorcd;//错误码
	private String erortx;//错误描述
	private String kehuzhao;//客户账号
	private String zhhaoxuh;//子账户序号
	private String kehuzhmc;//客户账户名称
	private String zhhuzwmc;//账户名称
	private String chanpdma;//产品代码
	private String chanpshm;//产品说明
	private String zhhufenl;//账户分类
	private String zhshuxin;//账户属性
	private String guiylius;//柜员流水号
	private String jiaoyirq;//交易日期
	private String kehuhaoo;//客户号
	private String huobdaih;//货币代号
	private String chaohubz;//账户钞汇标志
	private String keschpmc;//可售产品名称
	private String jifeibzh;//计费标志
	private BigDecimal shijlilv;//实际利率
	private String tduifwei;//通兑范围
	private String zhfutojn;//支付条件
	private String lyzhipbz;//领用支票标志
	private String zijnlaiy;//资金来源
	private String duifkhzh;//对方客户账号
	private String zhhaoxh1;//子账户序号
	private String lyzhumcc;//来源账户名称
	private String cunqiiii;//存期
	private String qixiriqi;//起息日期
	private String doqiriqi;//到期日期
	private BigDecimal jineeeee;//金额
	private String zhcunfsh;//转存方式
	private String zcuncqii;//转存存期
	private String pngzzlei;//凭证种类
	private String pngzxhao;//凭证序号
	private String mobmingc;//组合账户模板名称
	private String aiostype;//组合账户形态
	private String sfkldqsh;//是否开立待清算户
	public String  getErorcd() { return erorcd; }
	public void setErorcd(String erorcd ) { this.erorcd = erorcd;}
	public String  getErortx() { return erortx; }
	public void setErortx(String erortx ) { this.erortx = erortx;}
	public String  getKehuzhao() { return kehuzhao; }
	public void setKehuzhao(String kehuzhao ) { this.kehuzhao = kehuzhao;}
	public String  getZhhaoxuh() { return zhhaoxuh; }
	public void setZhhaoxuh(String zhhaoxuh ) { this.zhhaoxuh = zhhaoxuh;}
	public String  getKehuzhmc() { return kehuzhmc; }
	public void setKehuzhmc(String kehuzhmc ) { this.kehuzhmc = kehuzhmc;}
	public String  getZhhuzwmc() { return zhhuzwmc; }
	public void setZhhuzwmc(String zhhuzwmc ) { this.zhhuzwmc = zhhuzwmc;}
	public String  getChanpdma() { return chanpdma; }
	public void setChanpdma(String chanpdma ) { this.chanpdma = chanpdma;}
	public String  getChanpshm() { return chanpshm; }
	public void setChanpshm(String chanpshm ) { this.chanpshm = chanpshm;}
	public String  getZhhufenl() { return zhhufenl; }
	public void setZhhufenl(String zhhufenl ) { this.zhhufenl = zhhufenl;}
	public String  getZhshuxin() { return zhshuxin; }
	public void setZhshuxin(String zhshuxin ) { this.zhshuxin = zhshuxin;}
	public String  getGuiylius() { return guiylius; }
	public void setGuiylius(String guiylius ) { this.guiylius = guiylius;}
	public String  getJiaoyirq() { return jiaoyirq; }
	public void setJiaoyirq(String jiaoyirq ) { this.jiaoyirq = jiaoyirq;}
	public String  getKehuhaoo() { return kehuhaoo; }
	public void setKehuhaoo(String kehuhaoo ) { this.kehuhaoo = kehuhaoo;}
	public String  getHuobdaih() { return huobdaih; }
	public void setHuobdaih(String huobdaih ) { this.huobdaih = huobdaih;}
	public String  getChaohubz() { return chaohubz; }
	public void setChaohubz(String chaohubz ) { this.chaohubz = chaohubz;}
	public String  getKeschpmc() { return keschpmc; }
	public void setKeschpmc(String keschpmc ) { this.keschpmc = keschpmc;}
	public String  getJifeibzh() { return jifeibzh; }
	public void setJifeibzh(String jifeibzh ) { this.jifeibzh = jifeibzh;}
	public BigDecimal  getShijlilv() { return shijlilv; }
	public void setShijlilv(BigDecimal shijlilv ) { this.shijlilv = shijlilv;}
	public String  getTduifwei() { return tduifwei; }
	public void setTduifwei(String tduifwei ) { this.tduifwei = tduifwei;}
	public String  getZhfutojn() { return zhfutojn; }
	public void setZhfutojn(String zhfutojn ) { this.zhfutojn = zhfutojn;}
	public String  getLyzhipbz() { return lyzhipbz; }
	public void setLyzhipbz(String lyzhipbz ) { this.lyzhipbz = lyzhipbz;}
	public String  getZijnlaiy() { return zijnlaiy; }
	public void setZijnlaiy(String zijnlaiy ) { this.zijnlaiy = zijnlaiy;}
	public String  getDuifkhzh() { return duifkhzh; }
	public void setDuifkhzh(String duifkhzh ) { this.duifkhzh = duifkhzh;}
	public String  getZhhaoxh1() { return zhhaoxh1; }
	public void setZhhaoxh1(String zhhaoxh1 ) { this.zhhaoxh1 = zhhaoxh1;}
	public String  getLyzhumcc() { return lyzhumcc; }
	public void setLyzhumcc(String lyzhumcc ) { this.lyzhumcc = lyzhumcc;}
	public String  getCunqiiii() { return cunqiiii; }
	public void setCunqiiii(String cunqiiii ) { this.cunqiiii = cunqiiii;}
	public String  getQixiriqi() { return qixiriqi; }
	public void setQixiriqi(String qixiriqi ) { this.qixiriqi = qixiriqi;}
	public String  getDoqiriqi() { return doqiriqi; }
	public void setDoqiriqi(String doqiriqi ) { this.doqiriqi = doqiriqi;}
	public BigDecimal  getJineeeee() { return jineeeee; }
	public void setJineeeee(BigDecimal jineeeee ) { this.jineeeee = jineeeee;}
	public String  getZhcunfsh() { return zhcunfsh; }
	public void setZhcunfsh(String zhcunfsh ) { this.zhcunfsh = zhcunfsh;}
	public String  getZcuncqii() { return zcuncqii; }
	public void setZcuncqii(String zcuncqii ) { this.zcuncqii = zcuncqii;}
	public String  getPngzzlei() { return pngzzlei; }
	public void setPngzzlei(String pngzzlei ) { this.pngzzlei = pngzzlei;}
	public String  getPngzxhao() { return pngzxhao; }
	public void setPngzxhao(String pngzxhao ) { this.pngzxhao = pngzxhao;}
	public String  getMobmingc() { return mobmingc; }
	public void setMobmingc(String mobmingc ) { this.mobmingc = mobmingc;}
	public String  getAiostype() { return aiostype; }
	public void setAiostype(String aiostype ) { this.aiostype = aiostype;}
	public String  getSfkldqsh() { return sfkldqsh; }
	public void setSfkldqsh(String sfkldqsh ) { this.sfkldqsh = sfkldqsh;}
    @Override
    public String toString() {
	    return "Service{" +
	"erorcd='" + erorcd+ '\'' +
	"erortx='" + erortx+ '\'' +
	"kehuzhao='" + kehuzhao+ '\'' +
	"zhhaoxuh='" + zhhaoxuh+ '\'' +
	"kehuzhmc='" + kehuzhmc+ '\'' +
	"zhhuzwmc='" + zhhuzwmc+ '\'' +
	"chanpdma='" + chanpdma+ '\'' +
	"chanpshm='" + chanpshm+ '\'' +
	"zhhufenl='" + zhhufenl+ '\'' +
	"zhshuxin='" + zhshuxin+ '\'' +
	"guiylius='" + guiylius+ '\'' +
	"jiaoyirq='" + jiaoyirq+ '\'' +
	"kehuhaoo='" + kehuhaoo+ '\'' +
	"huobdaih='" + huobdaih+ '\'' +
	"chaohubz='" + chaohubz+ '\'' +
	"keschpmc='" + keschpmc+ '\'' +
	"jifeibzh='" + jifeibzh+ '\'' +
	"shijlilv='" + shijlilv+ '\'' +
	"tduifwei='" + tduifwei+ '\'' +
	"zhfutojn='" + zhfutojn+ '\'' +
	"lyzhipbz='" + lyzhipbz+ '\'' +
	"zijnlaiy='" + zijnlaiy+ '\'' +
	"duifkhzh='" + duifkhzh+ '\'' +
	"zhhaoxh1='" + zhhaoxh1+ '\'' +
	"lyzhumcc='" + lyzhumcc+ '\'' +
	"cunqiiii='" + cunqiiii+ '\'' +
	"qixiriqi='" + qixiriqi+ '\'' +
	"doqiriqi='" + doqiriqi+ '\'' +
	"jineeeee='" + jineeeee+ '\'' +
	"zhcunfsh='" + zhcunfsh+ '\'' +
	"zcuncqii='" + zcuncqii+ '\'' +
	"pngzzlei='" + pngzzlei+ '\'' +
	"pngzxhao='" + pngzxhao+ '\'' +
	"mobmingc='" + mobmingc+ '\'' +
	"aiostype='" + aiostype+ '\'' +
	"sfkldqsh='" + sfkldqsh+ '\'' +
 '}';
    }
}
