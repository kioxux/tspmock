package cn.com.yusys.yusp.web.client.esb.core.da3303;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.da3303.Da3303ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3303.Da3303RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.da3303.req.Da3303ReqService;
import cn.com.yusys.yusp.online.client.esb.core.da3303.req.Record;
import cn.com.yusys.yusp.online.client.esb.core.da3303.resp.Da3303RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:抵债资产信息维护
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2coreda")
public class Dscms2Da3303Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Da3303Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：da3303
     * 交易描述：抵债资产信息维护
     *
     * @param da3303ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/da3303")
    protected @ResponseBody
    ResultDto<Da3303RespDto> da3303(@Validated @RequestBody Da3303ReqDto da3303ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3303.key, EsbEnum.TRADE_CODE_DA3303.value, JSON.toJSONString(da3303ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.da3303.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.da3303.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.da3303.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.da3303.resp.Service();
        Da3303ReqService da3303ReqService = new Da3303ReqService();
        Da3303RespService da3303RespService = new Da3303RespService();
        Da3303RespDto da3303RespDto = new Da3303RespDto();
        ResultDto<Da3303RespDto> da3303ResultDto = new ResultDto<Da3303RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将da3303ReqDto转换成reqService
            BeanUtils.copyProperties(da3303ReqDto, reqService);
            // 循环相关的判断开始
            if (CollectionUtils.nonEmpty(da3303ReqDto.getLstweihsr())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.da3303.Lstweihsr> da3303LstweihsrDtos = Optional.ofNullable(da3303ReqDto.getLstweihsr()).orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.core.da3303.req.Lstweihsr_ARRAY lstweihsr_ARRAY = new cn.com.yusys.yusp.online.client.esb.core.da3303.req.Lstweihsr_ARRAY();
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3303.req.Record> records = da3303LstweihsrDtos.parallelStream().map(da3303LstweihsrDto -> {
                    cn.com.yusys.yusp.online.client.esb.core.da3303.req.Record record = new Record();
                    BeanUtils.copyProperties(da3303LstweihsrDto, record);
                    return record;
                }).collect(Collectors.toList());
                lstweihsr_ARRAY.setRecord(records);
                reqService.setLstweihsr_ARRAY(lstweihsr_ARRAY);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_DA3303.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            da3303ReqService.setService(reqService);
            // 将da3303ReqService转换成da3303ReqServiceMap
            Map da3303ReqServiceMap = beanMapUtil.beanToMap(da3303ReqService);
            context.put("tradeDataMap", da3303ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3303.key, EsbEnum.TRADE_CODE_DA3303.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DA3303.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3303.key, EsbEnum.TRADE_CODE_DA3303.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            da3303RespService = beanMapUtil.mapToBean(tradeDataMap, Da3303RespService.class, Da3303RespService.class);
            respService = da3303RespService.getService();

            da3303ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            da3303ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Da3303RespDto
                BeanUtils.copyProperties(respService, da3303RespDto);
                da3303ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                da3303ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                da3303ResultDto.setCode(EpbEnum.EPB099999.key);
                da3303ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3303.key, EsbEnum.TRADE_CODE_DA3303.value, e.getMessage());
            da3303ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            da3303ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        da3303ResultDto.setData(da3303RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DA3303.key, EsbEnum.TRADE_CODE_DA3303.value, JSON.toJSONString(da3303ResultDto));
        return da3303ResultDto;
    }
}
