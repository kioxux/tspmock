package cn.com.yusys.yusp.online.client.esb.lsnp.lsnp02.req;

import java.math.BigDecimal;

/**
 * 请求Service：信用卡业务零售评级
 */
public class Service {
	private String prcscd;//    交易码
	private String servtp;//    渠道
	private String servsq;//    渠道流水
	private String userid;//    柜员号
	private String brchno;//    部门号
	private String txdate;//    交易日期
	private String txtime;//    交易时间
	private String datasq;//    全局流水

    private String apply_seq;//申请流水号
    private String email_flag;//是否有邮箱
    private String sex;//性别
    private String max_edu;//教育程度
    private BigDecimal apply_age;//申请时年龄
    private BigDecimal job_year;//工龄（年）
    private String corp_prop;//公司性质
    private String corp_indus_type;//所属行业
    private String marry_stat;//婚姻状况
    private String max_degree;//最高学位
    private BigDecimal mon_income;//月收入
    private String tel;//手机号码
    private String first_rel_tel;//第一联系人手机号码
    private String second_rel_tel;//第二联系人手机号码
    private String cust_name;//姓名
    private String cert_no;//身份证号
    private BigDecimal attach_card_age;//附属卡申请人年龄
    private BigDecimal applye_amt;//申请金额
    private String business_seq;//业务流水号
    private String cust_type;//客户类型
    private String corp_name;//工作单位名称
    private String product_cd;//产品编号

    public String getFund_deposite_base() {
        return fund_deposite_base;
    }

    public void setFund_deposite_base(String fund_deposite_base) {
        this.fund_deposite_base = fund_deposite_base;
    }

    private String fund_deposite_base;//异地住房公积金缴存基数

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getTxdate() {
		return txdate;
	}

	public void setTxdate(String txdate) {
		this.txdate = txdate;
	}

	public String getTxtime() {
		return txtime;
	}

	public void setTxtime(String txtime) {
		this.txtime = txtime;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getApply_seq() {
        return apply_seq;
    }

    public void setApply_seq(String apply_seq) {
        this.apply_seq = apply_seq;
    }

    public String getEmail_flag() {
        return email_flag;
    }

    public void setEmail_flag(String email_flag) {
        this.email_flag = email_flag;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMax_edu() {
        return max_edu;
    }

    public void setMax_edu(String max_edu) {
        this.max_edu = max_edu;
    }

    public BigDecimal getApply_age() {
        return apply_age;
    }

    public void setApply_age(BigDecimal apply_age) {
        this.apply_age = apply_age;
    }

    public BigDecimal getJob_year() {
        return job_year;
    }

    public void setJob_year(BigDecimal job_year) {
        this.job_year = job_year;
    }

    public String getCorp_prop() {
        return corp_prop;
    }

    public void setCorp_prop(String corp_prop) {
        this.corp_prop = corp_prop;
    }

    public String getCorp_indus_type() {
        return corp_indus_type;
    }

    public void setCorp_indus_type(String corp_indus_type) {
        this.corp_indus_type = corp_indus_type;
    }

    public String getMarry_stat() {
        return marry_stat;
    }

    public void setMarry_stat(String marry_stat) {
        this.marry_stat = marry_stat;
    }

    public String getMax_degree() {
        return max_degree;
    }

    public void setMax_degree(String max_degree) {
        this.max_degree = max_degree;
    }

    public BigDecimal getMon_income() {
        return mon_income;
    }

    public void setMon_income(BigDecimal mon_income) {
        this.mon_income = mon_income;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFirst_rel_tel() {
        return first_rel_tel;
    }

    public void setFirst_rel_tel(String first_rel_tel) {
        this.first_rel_tel = first_rel_tel;
    }

    public String getSecond_rel_tel() {
        return second_rel_tel;
    }

    public void setSecond_rel_tel(String second_rel_tel) {
        this.second_rel_tel = second_rel_tel;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public BigDecimal getAttach_card_age() {
        return attach_card_age;
    }

    public void setAttach_card_age(BigDecimal attach_card_age) {
        this.attach_card_age = attach_card_age;
    }

    public BigDecimal getApplye_amt() {
        return applye_amt;
    }

    public void setApplye_amt(BigDecimal applye_amt) {
        this.applye_amt = applye_amt;
    }

    public String getBusiness_seq() {
        return business_seq;
    }

    public void setBusiness_seq(String business_seq) {
        this.business_seq = business_seq;
    }

    public String getCust_type() {
        return cust_type;
    }

    public void setCust_type(String cust_type) {
        this.cust_type = cust_type;
    }

    public String getCorp_name() {
        return corp_name;
    }

    public void setCorp_name(String corp_name) {
        this.corp_name = corp_name;
    }

    public String getProduct_cd() {
        return product_cd;
    }

    public void setProduct_cd(String product_cd) {
        this.product_cd = product_cd;
    }

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", txdate='" + txdate + '\'' +
				", txtime='" + txtime + '\'' +
				", datasq='" + datasq + '\'' +
				", apply_seq='" + apply_seq + '\'' +
				", email_flag='" + email_flag + '\'' +
				", sex='" + sex + '\'' +
				", max_edu='" + max_edu + '\'' +
				", apply_age=" + apply_age +
				", job_year=" + job_year +
				", corp_prop='" + corp_prop + '\'' +
				", corp_indus_type='" + corp_indus_type + '\'' +
				", marry_stat='" + marry_stat + '\'' +
				", max_degree='" + max_degree + '\'' +
				", mon_income=" + mon_income +
				", tel='" + tel + '\'' +
				", first_rel_tel='" + first_rel_tel + '\'' +
				", second_rel_tel='" + second_rel_tel + '\'' +
				", cust_name='" + cust_name + '\'' +
				", cert_no='" + cert_no + '\'' +
				", attach_card_age=" + attach_card_age +
				", applye_amt=" + applye_amt +
				", business_seq='" + business_seq + '\'' +
				", cust_type='" + cust_type + '\'' +
				", corp_name='" + corp_name + '\'' +
				", product_cd='" + product_cd + '\'' +
				'}';
	}
}
