package cn.com.yusys.yusp.online.client.esb.core.ln3020.req.lstdkzhzy;

public class Record {
    private String dkjiejuh;//贷款借据号
    private Integer xuhaoooo;//序号
    private String shfbhzhh;//是否本行账号
    private String kehuzhao;//客户账号
    private String khzhhzxh;//客户账号子序号
    private String kehmingc;//客户名称
    private String xitongzh;//系统账号
    private String zhjzhyfs;//直接质押方式
    private String huobdhao;//货币代号
    private String zhiyajee;//质押金额
    private String djiebhao;//冻结编号
    private String guanlzht;//关联状态
    private String zyzhleix;//质押账户类型
    private String dzywbhao;//抵质押物编号
    private String zhydaoqr;//质押到期日
    private String zjzrzhao;//资金转入账号
    private String zjzrzzxh;//资金转入账号子序号

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Integer xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getShfbhzhh() {
        return shfbhzhh;
    }

    public void setShfbhzhh(String shfbhzhh) {
        this.shfbhzhh = shfbhzhh;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getXitongzh() {
        return xitongzh;
    }

    public void setXitongzh(String xitongzh) {
        this.xitongzh = xitongzh;
    }

    public String getZhjzhyfs() {
        return zhjzhyfs;
    }

    public void setZhjzhyfs(String zhjzhyfs) {
        this.zhjzhyfs = zhjzhyfs;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getZhiyajee() {
        return zhiyajee;
    }

    public void setZhiyajee(String zhiyajee) {
        this.zhiyajee = zhiyajee;
    }

    public String getDjiebhao() {
        return djiebhao;
    }

    public void setDjiebhao(String djiebhao) {
        this.djiebhao = djiebhao;
    }

    public String getGuanlzht() {
        return guanlzht;
    }

    public void setGuanlzht(String guanlzht) {
        this.guanlzht = guanlzht;
    }

    public String getZyzhleix() {
        return zyzhleix;
    }

    public void setZyzhleix(String zyzhleix) {
        this.zyzhleix = zyzhleix;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getZhydaoqr() {
        return zhydaoqr;
    }

    public void setZhydaoqr(String zhydaoqr) {
        this.zhydaoqr = zhydaoqr;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    @Override
    public String toString() {
        return "Record{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", xuhaoooo='" + xuhaoooo + '\'' +
                ", shfbhzhh='" + shfbhzhh + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", xitongzh='" + xitongzh + '\'' +
                ", zhjzhyfs='" + zhjzhyfs + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", zhiyajee='" + zhiyajee + '\'' +
                ", djiebhao='" + djiebhao + '\'' +
                ", guanlzht='" + guanlzht + '\'' +
                ", zyzhleix='" + zyzhleix + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                ", zhydaoqr='" + zhydaoqr + '\'' +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                '}';
    }
}
