package cn.com.yusys.yusp.online.client.esb.core.ln3062.resp;

/**
 * 响应Service：资产转让借据维护
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3062RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3062RespService{" +
                "service=" + service +
                '}';
    }
}
