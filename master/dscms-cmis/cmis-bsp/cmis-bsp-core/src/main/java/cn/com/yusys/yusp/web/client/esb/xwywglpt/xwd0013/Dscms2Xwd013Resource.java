package cn.com.yusys.yusp.web.client.esb.xwywglpt.xwd0013;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.req.Xwd013ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.Xwd013RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.req.Xwd013ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.AssetsLiabilitieList;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.IncomeStatementList;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.Xwd013RespService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.assetsLiabilitieList.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 接口处理类:新信贷获取调查信息接口
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "BSP封装新微贷平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Xwd013Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xwd013Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xwd013
     * 交易描述：新信贷获取调查信息接口
     *
     * @param xwd013ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("xwd013:新信贷获取调查信息接口")
    @PostMapping("/xwd013")
    protected @ResponseBody
    ResultDto<Xwd013RespDto> xwd013(@Validated @RequestBody Xwd013ReqDto xwd013ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value, JSON.toJSONString(xwd013ReqDto));
        cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.req.Service();
        cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd013.resp.Service();
        Xwd013ReqService xwd013ReqService = new Xwd013ReqService();
        Xwd013RespService xwd013RespService = new Xwd013RespService();
        Xwd013RespDto xwd013RespDto = new Xwd013RespDto();
        ResultDto<Xwd013RespDto> xwd013ResultDto = new ResultDto<Xwd013RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xwd013ReqDto转换成reqService
            BeanUtils.copyProperties(xwd013ReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XWD013.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_XWD.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_XWD.key);//    部门号
            xwd013ReqService.setService(reqService);
            // 将xwd013ReqService转换成xwd013ReqServiceMap
            Map xwd013ReqServiceMap = beanMapUtil.beanToMap(xwd013ReqService);
            context.put("tradeDataMap", xwd013ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XWD013.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xwd013RespService = beanMapUtil.mapToBean(tradeDataMap, Xwd013RespService.class, Xwd013RespService.class);
            respService = xwd013RespService.getService();

            xwd013ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xwd013ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd02RespDto
                BeanUtils.copyProperties(respService, xwd013RespDto);

                xwd013ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xwd013ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xwd013ResultDto.setCode(EpbEnum.EPB099999.key);
                xwd013ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value, e.getMessage());
            xwd013ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xwd013ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xwd013ResultDto.setData(xwd013RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XWD013.key, EsbEnum.TRADE_CODE_XWD013.value, JSON.toJSONString(xwd013ResultDto));
        return xwd013ResultDto;
    }
}
