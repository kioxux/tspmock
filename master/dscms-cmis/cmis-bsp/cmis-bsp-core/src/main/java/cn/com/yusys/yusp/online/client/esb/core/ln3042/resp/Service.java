package cn.com.yusys.yusp.online.client.esb.core.ln3042.resp;

import java.math.BigDecimal;

/**
 * 响应Service：手工指定归还某项利息金额，不按既定顺序归还
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String hetongbh;//合同编号
    private String huobdhao;//货币代号
    private String chanpmch;//产品名称
    private BigDecimal benjheji;//本金合计
    private String jiaoyigy;//交易柜员
    private String jiaoyirq;//交易日期
    private String jiaoyils;//交易流水
    private BigDecimal jiejuuje;//借据金额
    private BigDecimal guihzcbj;//归还正常本金
    private BigDecimal guihyqbj;//归还逾期本金
    private BigDecimal guihdzbj;//归还呆滞本金
    private BigDecimal ghdzhabj;//归还呆账本金
    private BigDecimal ghysyjlx;//归还应收应计利息
    private BigDecimal ghcsyjlx;//归还催收应计利息
    private BigDecimal ghynshqx;//归还应收欠息
    private BigDecimal ghcushqx;//归还催收欠息
    private BigDecimal ghysyjfx;//归还应收应计罚息
    private BigDecimal ghcsyjfx;//归还催收应计罚息
    private BigDecimal ghynshfx;//归还应收罚息
    private BigDecimal ghcushfx;//归还催收罚息
    private BigDecimal ghyjfuxi;//归还应计复息
    private BigDecimal ghfxfuxi;//归还复息
    private BigDecimal ghfeiyin;//归还费用
    private BigDecimal ghfajinn;//归还罚金
    private BigDecimal guihzone;//归还总额
    private String kaihriqi;//开户日期
    private String daoqriqi;//到期日期
    private BigDecimal zhchlilv;//正常利率
    private BigDecimal yuqililv;//逾期利率
    private String dkrzhzhh;//贷款入账账号
    private String zhanghmc;//账户户名

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public BigDecimal getBenjheji() {
        return benjheji;
    }

    public void setBenjheji(BigDecimal benjheji) {
        this.benjheji = benjheji;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public BigDecimal getJiejuuje() {
        return jiejuuje;
    }

    public void setJiejuuje(BigDecimal jiejuuje) {
        this.jiejuuje = jiejuuje;
    }

    public BigDecimal getGuihzcbj() {
        return guihzcbj;
    }

    public void setGuihzcbj(BigDecimal guihzcbj) {
        this.guihzcbj = guihzcbj;
    }

    public BigDecimal getGuihyqbj() {
        return guihyqbj;
    }

    public void setGuihyqbj(BigDecimal guihyqbj) {
        this.guihyqbj = guihyqbj;
    }

    public BigDecimal getGuihdzbj() {
        return guihdzbj;
    }

    public void setGuihdzbj(BigDecimal guihdzbj) {
        this.guihdzbj = guihdzbj;
    }

    public BigDecimal getGhdzhabj() {
        return ghdzhabj;
    }

    public void setGhdzhabj(BigDecimal ghdzhabj) {
        this.ghdzhabj = ghdzhabj;
    }

    public BigDecimal getGhysyjlx() {
        return ghysyjlx;
    }

    public void setGhysyjlx(BigDecimal ghysyjlx) {
        this.ghysyjlx = ghysyjlx;
    }

    public BigDecimal getGhcsyjlx() {
        return ghcsyjlx;
    }

    public void setGhcsyjlx(BigDecimal ghcsyjlx) {
        this.ghcsyjlx = ghcsyjlx;
    }

    public BigDecimal getGhynshqx() {
        return ghynshqx;
    }

    public void setGhynshqx(BigDecimal ghynshqx) {
        this.ghynshqx = ghynshqx;
    }

    public BigDecimal getGhcushqx() {
        return ghcushqx;
    }

    public void setGhcushqx(BigDecimal ghcushqx) {
        this.ghcushqx = ghcushqx;
    }

    public BigDecimal getGhysyjfx() {
        return ghysyjfx;
    }

    public void setGhysyjfx(BigDecimal ghysyjfx) {
        this.ghysyjfx = ghysyjfx;
    }

    public BigDecimal getGhcsyjfx() {
        return ghcsyjfx;
    }

    public void setGhcsyjfx(BigDecimal ghcsyjfx) {
        this.ghcsyjfx = ghcsyjfx;
    }

    public BigDecimal getGhynshfx() {
        return ghynshfx;
    }

    public void setGhynshfx(BigDecimal ghynshfx) {
        this.ghynshfx = ghynshfx;
    }

    public BigDecimal getGhcushfx() {
        return ghcushfx;
    }

    public void setGhcushfx(BigDecimal ghcushfx) {
        this.ghcushfx = ghcushfx;
    }

    public BigDecimal getGhyjfuxi() {
        return ghyjfuxi;
    }

    public void setGhyjfuxi(BigDecimal ghyjfuxi) {
        this.ghyjfuxi = ghyjfuxi;
    }

    public BigDecimal getGhfxfuxi() {
        return ghfxfuxi;
    }

    public void setGhfxfuxi(BigDecimal ghfxfuxi) {
        this.ghfxfuxi = ghfxfuxi;
    }

    public BigDecimal getGhfeiyin() {
        return ghfeiyin;
    }

    public void setGhfeiyin(BigDecimal ghfeiyin) {
        this.ghfeiyin = ghfeiyin;
    }

    public BigDecimal getGhfajinn() {
        return ghfajinn;
    }

    public void setGhfajinn(BigDecimal ghfajinn) {
        this.ghfajinn = ghfajinn;
    }

    public BigDecimal getGuihzone() {
        return guihzone;
    }

    public void setGuihzone(BigDecimal guihzone) {
        this.guihzone = guihzone;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getZhanghmc() {
        return zhanghmc;
    }

    public void setZhanghmc(String zhanghmc) {
        this.zhanghmc = zhanghmc;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", hetongbh='" + hetongbh + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", chanpmch='" + chanpmch + '\'' +
                ", benjheji=" + benjheji +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jiejuuje=" + jiejuuje +
                ", guihzcbj=" + guihzcbj +
                ", guihyqbj=" + guihyqbj +
                ", guihdzbj=" + guihdzbj +
                ", ghdzhabj=" + ghdzhabj +
                ", ghysyjlx=" + ghysyjlx +
                ", ghcsyjlx=" + ghcsyjlx +
                ", ghynshqx=" + ghynshqx +
                ", ghcushqx=" + ghcushqx +
                ", ghysyjfx=" + ghysyjfx +
                ", ghcsyjfx=" + ghcsyjfx +
                ", ghynshfx=" + ghynshfx +
                ", ghcushfx=" + ghcushfx +
                ", ghyjfuxi=" + ghyjfuxi +
                ", ghfxfuxi=" + ghfxfuxi +
                ", ghfeiyin=" + ghfeiyin +
                ", ghfajinn=" + ghfajinn +
                ", guihzone=" + guihzone +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", yuqililv=" + yuqililv +
                ", dkrzhzhh='" + dkrzhzhh + '\'' +
                ", zhanghmc='" + zhanghmc + '\'' +
                '}';
    }
}