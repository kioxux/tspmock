package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 上市公司基本信息
 */
@JsonPropertyOrder(alphabetic = true)
public class LISTEDCOMPINFO implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "BIZSCOPE")
    private String BIZSCOPE;//	经营范围
    @JsonProperty(value = "COMPSNAME")
    private String COMPSNAME;//	公司简称
    @JsonProperty(value = "COMPSTATUS")
    private String COMPSTATUS;//	机构存续状态
    @JsonProperty(value = "COUNTRY")
    private String COUNTRY;//	所在国家
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;//	统一社会信用代码
    @JsonProperty(value = "CUR")
    private String CUR;//	币种代码
    @JsonProperty(value = "CURNAME")
    private String CURNAME;//	币种
    @JsonProperty(value = "ENGNAME")
    private String ENGNAME;//	英文名称
    @JsonProperty(value = "ENGSNAME")
    private String ENGSNAME;//	英文简称
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//	公司全称
    @JsonProperty(value = "EXISTBEGDATE")
    private String EXISTBEGDATE;//	存续起始日
    @JsonProperty(value = "EXISTENDDATE")
    private String EXISTENDDATE;//	存续截止日
    @JsonProperty(value = "FOUNDDATE")
    private String FOUNDDATE;//	成立日期
    @JsonProperty(value = "FRNAME")
    private String FRNAME;//	法定代表人
    @JsonProperty(value = "MAJORBIZ")
    private String MAJORBIZ;//	主营业务
    @JsonProperty(value = "OFFICEADDR")
    private String OFFICEADDR;//	办公地址
    @JsonProperty(value = "OFFICEZIPCODE")
    private String OFFICEZIPCODE;//	办公地址邮编
    @JsonProperty(value = "ORGCODE")
    private String ORGCODE;//	机构组织代码
    @JsonProperty(value = "REGADDR")
    private String REGADDR;//	注册地址
    @JsonProperty(value = "REGCAPITAL")
    private String REGCAPITAL;//	注册资本
    @JsonProperty(value = "REGION")
    private String REGION;//	所在地区
    @JsonProperty(value = "REGPTCODE")
    private String REGPTCODE;//	注册地址邮编
    @JsonProperty(value = "WORKFORCE")
    private String WORKFORCE;//	员工人数

    @JsonIgnore
    public String getBIZSCOPE() {
        return BIZSCOPE;
    }

    @JsonIgnore
    public void setBIZSCOPE(String BIZSCOPE) {
        this.BIZSCOPE = BIZSCOPE;
    }

    @JsonIgnore
    public String getCOMPSNAME() {
        return COMPSNAME;
    }

    @JsonIgnore
    public void setCOMPSNAME(String COMPSNAME) {
        this.COMPSNAME = COMPSNAME;
    }

    @JsonIgnore
    public String getCOMPSTATUS() {
        return COMPSTATUS;
    }

    @JsonIgnore
    public void setCOMPSTATUS(String COMPSTATUS) {
        this.COMPSTATUS = COMPSTATUS;
    }

    @JsonIgnore
    public String getCOUNTRY() {
        return COUNTRY;
    }

    @JsonIgnore
    public void setCOUNTRY(String COUNTRY) {
        this.COUNTRY = COUNTRY;
    }

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getCUR() {
        return CUR;
    }

    @JsonIgnore
    public void setCUR(String CUR) {
        this.CUR = CUR;
    }

    @JsonIgnore
    public String getCURNAME() {
        return CURNAME;
    }

    @JsonIgnore
    public void setCURNAME(String CURNAME) {
        this.CURNAME = CURNAME;
    }

    @JsonIgnore
    public String getENGNAME() {
        return ENGNAME;
    }

    @JsonIgnore
    public void setENGNAME(String ENGNAME) {
        this.ENGNAME = ENGNAME;
    }

    @JsonIgnore
    public String getENGSNAME() {
        return ENGSNAME;
    }

    @JsonIgnore
    public void setENGSNAME(String ENGSNAME) {
        this.ENGSNAME = ENGSNAME;
    }

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getEXISTBEGDATE() {
        return EXISTBEGDATE;
    }

    @JsonIgnore
    public void setEXISTBEGDATE(String EXISTBEGDATE) {
        this.EXISTBEGDATE = EXISTBEGDATE;
    }

    @JsonIgnore
    public String getEXISTENDDATE() {
        return EXISTENDDATE;
    }

    @JsonIgnore
    public void setEXISTENDDATE(String EXISTENDDATE) {
        this.EXISTENDDATE = EXISTENDDATE;
    }

    @JsonIgnore
    public String getFOUNDDATE() {
        return FOUNDDATE;
    }

    @JsonIgnore
    public void setFOUNDDATE(String FOUNDDATE) {
        this.FOUNDDATE = FOUNDDATE;
    }

    @JsonIgnore
    public String getFRNAME() {
        return FRNAME;
    }

    @JsonIgnore
    public void setFRNAME(String FRNAME) {
        this.FRNAME = FRNAME;
    }

    @JsonIgnore
    public String getMAJORBIZ() {
        return MAJORBIZ;
    }

    @JsonIgnore
    public void setMAJORBIZ(String MAJORBIZ) {
        this.MAJORBIZ = MAJORBIZ;
    }

    @JsonIgnore
    public String getOFFICEADDR() {
        return OFFICEADDR;
    }

    @JsonIgnore
    public void setOFFICEADDR(String OFFICEADDR) {
        this.OFFICEADDR = OFFICEADDR;
    }

    @JsonIgnore
    public String getOFFICEZIPCODE() {
        return OFFICEZIPCODE;
    }

    @JsonIgnore
    public void setOFFICEZIPCODE(String OFFICEZIPCODE) {
        this.OFFICEZIPCODE = OFFICEZIPCODE;
    }

    @JsonIgnore
    public String getORGCODE() {
        return ORGCODE;
    }

    @JsonIgnore
    public void setORGCODE(String ORGCODE) {
        this.ORGCODE = ORGCODE;
    }

    @JsonIgnore
    public String getREGADDR() {
        return REGADDR;
    }

    @JsonIgnore
    public void setREGADDR(String REGADDR) {
        this.REGADDR = REGADDR;
    }

    @JsonIgnore
    public String getREGCAPITAL() {
        return REGCAPITAL;
    }

    @JsonIgnore
    public void setREGCAPITAL(String REGCAPITAL) {
        this.REGCAPITAL = REGCAPITAL;
    }

    @JsonIgnore
    public String getREGION() {
        return REGION;
    }

    @JsonIgnore
    public void setREGION(String REGION) {
        this.REGION = REGION;
    }

    @JsonIgnore
    public String getREGPTCODE() {
        return REGPTCODE;
    }

    @JsonIgnore
    public void setREGPTCODE(String REGPTCODE) {
        this.REGPTCODE = REGPTCODE;
    }

    @JsonIgnore
    public String getWORKFORCE() {
        return WORKFORCE;
    }

    @JsonIgnore
    public void setWORKFORCE(String WORKFORCE) {
        this.WORKFORCE = WORKFORCE;
    }

    @Override
    public String toString() {
        return "LISTEDCOMPINFO{" +
                "BIZSCOPE='" + BIZSCOPE + '\'' +
                ", COMPSNAME='" + COMPSNAME + '\'' +
                ", COMPSTATUS='" + COMPSTATUS + '\'' +
                ", COUNTRY='" + COUNTRY + '\'' +
                ", CREDITCODE='" + CREDITCODE + '\'' +
                ", CUR='" + CUR + '\'' +
                ", CURNAME='" + CURNAME + '\'' +
                ", ENGNAME='" + ENGNAME + '\'' +
                ", ENGSNAME='" + ENGSNAME + '\'' +
                ", ENTNAME='" + ENTNAME + '\'' +
                ", EXISTBEGDATE='" + EXISTBEGDATE + '\'' +
                ", EXISTENDDATE='" + EXISTENDDATE + '\'' +
                ", FOUNDDATE='" + FOUNDDATE + '\'' +
                ", FRNAME='" + FRNAME + '\'' +
                ", MAJORBIZ='" + MAJORBIZ + '\'' +
                ", OFFICEADDR='" + OFFICEADDR + '\'' +
                ", OFFICEZIPCODE='" + OFFICEZIPCODE + '\'' +
                ", ORGCODE='" + ORGCODE + '\'' +
                ", REGADDR='" + REGADDR + '\'' +
                ", REGCAPITAL='" + REGCAPITAL + '\'' +
                ", REGION='" + REGION + '\'' +
                ", REGPTCODE='" + REGPTCODE + '\'' +
                ", WORKFORCE='" + WORKFORCE + '\'' +
                '}';
    }
}
