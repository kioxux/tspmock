package cn.com.yusys.yusp.online.client.esb.core.ln3036.resp;

import cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkwtxx.Record;

import java.util.List;

/**
 * 响应Dto：贷款多委托人账户
 *
 * @author code-generator
 * @version 1.0
 */
public class Lstdkwtxx_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3036.resp.lstdkwtxx.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdkwtxx_ARRAY{" +
                "record=" + record +
                '}';
    }
}
