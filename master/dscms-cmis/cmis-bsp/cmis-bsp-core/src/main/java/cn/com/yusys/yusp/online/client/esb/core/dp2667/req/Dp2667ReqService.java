package cn.com.yusys.yusp.online.client.esb.core.dp2667.req;

/**
 * 请求Service：组合账户特殊账户查询
 * @author lihh
 * @version 1.0             
 */      
public class Dp2667ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
