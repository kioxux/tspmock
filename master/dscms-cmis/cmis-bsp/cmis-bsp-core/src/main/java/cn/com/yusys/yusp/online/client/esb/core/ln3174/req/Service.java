package cn.com.yusys.yusp.online.client.esb.core.ln3174.req;

/**
 * 请求Service：资产证券化欠款借据查询
 */
public class Service {
    private String prcscd;//    交易码
    private String servtp;//    渠道
    private String servsq;//    渠道流水
    private String userid;//    柜员号
    private String brchno;//    部门号
    private String servdt;//    交易日期
    private String servti;//    交易时间
    private String datasq;//    全局流水

    private String xieybhao;//协议编号
    private String chaxunzl;//查询种类
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getChaxunzl() {
        return chaxunzl;
    }

    public void setChaxunzl(String chaxunzl) {
        this.chaxunzl = chaxunzl;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", datasq='" + datasq + '\'' +
                ", xieybhao='" + xieybhao + '\'' +
                ", chaxunzl='" + chaxunzl + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                '}';
    }
}
