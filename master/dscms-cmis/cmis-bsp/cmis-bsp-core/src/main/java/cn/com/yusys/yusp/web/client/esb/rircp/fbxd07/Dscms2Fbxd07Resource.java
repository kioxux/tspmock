package cn.com.yusys.yusp.web.client.esb.rircp.fbxd07;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07.Fbxd07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07.Fbxd07RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.req.Fbxd07ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.Fbxd07RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd07）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd07Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd07Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd07
     * 交易描述：获取指定数据日期存在还款记录的借据一览信息，包括借据号、借据金额、借据余额
     *
     * @param fbxd07ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd07")
    protected @ResponseBody
    ResultDto<Fbxd07RespDto> fbxd07(@Validated @RequestBody Fbxd07ReqDto fbxd07ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD07.key, EsbEnum.TRADE_CODE_FBXD07.value, JSON.toJSONString(fbxd07ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.Service();
        Fbxd07ReqService fbxd07ReqService = new Fbxd07ReqService();
        Fbxd07RespService fbxd07RespService = new Fbxd07RespService();
        Fbxd07RespDto fbxd07RespDto = new Fbxd07RespDto();
        ResultDto<Fbxd07RespDto> fbxd07ResultDto = new ResultDto<Fbxd07RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd07ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd07ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD07.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd07ReqService.setService(reqService);
            // 将fbxd07ReqService转换成fbxd07ReqServiceMap
            Map fbxd07ReqServiceMap = beanMapUtil.beanToMap(fbxd07ReqService);
            context.put("tradeDataMap", fbxd07ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD07.key, EsbEnum.TRADE_CODE_FBXD07.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD07.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD07.key, EsbEnum.TRADE_CODE_FBXD07.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd07RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd07RespService.class, Fbxd07RespService.class);
            respService = fbxd07RespService.getService();

            fbxd07ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd07ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd07RespDto
                BeanUtils.copyProperties(respService, fbxd07RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.List fbxd07RespServiceList = Optional.ofNullable(respService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.List());
                respService.setList(fbxd07RespServiceList);
                if (CollectionUtils.nonEmpty(respService.getList().getRecord())) {
                    List<Record> fbxd07RespRecordList = Optional.ofNullable(respService.getList().getRecord())
                            .orElseGet(() -> new ArrayList<cn.com.yusys.yusp.online.client.esb.rircp.fbxd07.resp.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07.List> fbxd07RespDtoLists = fbxd07RespRecordList.parallelStream().map(fbxd07RespRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07.List fbxd07RespDtoList = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07.List();
                        BeanUtils.copyProperties(fbxd07RespRecord, fbxd07RespDtoList);
                        return fbxd07RespDtoList;
                    }).collect(Collectors.toList());
                    fbxd07RespDto.setList(fbxd07RespDtoLists);
                }
                fbxd07ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd07ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd07ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd07ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD07.key, EsbEnum.TRADE_CODE_FBXD07.value, e.getMessage());
            fbxd07ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd07ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd07ResultDto.setData(fbxd07RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD07.key, EsbEnum.TRADE_CODE_FBXD07.value, JSON.toJSONString(fbxd07ResultDto));
        return fbxd07ResultDto;
    }
}
