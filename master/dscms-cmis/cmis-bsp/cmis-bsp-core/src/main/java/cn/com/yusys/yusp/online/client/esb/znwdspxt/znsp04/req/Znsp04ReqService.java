package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp04.req;

/**
 * 请求Service：自动审批调查报告
 */      
public class Znsp04ReqService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}

