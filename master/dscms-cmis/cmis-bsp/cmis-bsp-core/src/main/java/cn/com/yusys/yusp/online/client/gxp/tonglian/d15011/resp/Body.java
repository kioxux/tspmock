package cn.com.yusys.yusp.online.client.gxp.tonglian.d15011.resp;

import java.math.BigDecimal;

/**
 * 响应Body：永久额度调整
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Body {

    private String cardno;//卡号
    private String currcd;//币种
    private BigDecimal cdtlmt;//信用额度
    private String dlcrin;//是否存在外币标识
    private String dlcrcd;//外币币种
    private String platdt;
    private String ebnksq;
    private String ercode;
    private String retrmg;
    private String status;
    private String userid;
    private String brchno;
    private String chanfl;
    private String platsq;


    public String getCardno() {
        return cardno;
    }

    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    public String getCurrcd() {
        return currcd;
    }

    public void setCurrcd(String currcd) {
        this.currcd = currcd;
    }

    public BigDecimal getCdtlmt() {
        return cdtlmt;
    }

    public void setCdtlmt(BigDecimal cdtlmt) {
        this.cdtlmt = cdtlmt;
    }

    public String getDlcrin() {
        return dlcrin;
    }

    public void setDlcrin(String dlcrin) {
        this.dlcrin = dlcrin;
    }

    public String getDlcrcd() {
        return dlcrcd;
    }

    public void setDlcrcd(String dlcrcd) {
        this.dlcrcd = dlcrcd;
    }

    public String getPlatdt() {
        return platdt;
    }

    public void setPlatdt(String platdt) {
        this.platdt = platdt;
    }

    public String getEbnksq() {
        return ebnksq;
    }

    public void setEbnksq(String ebnksq) {
        this.ebnksq = ebnksq;
    }

    public String getErcode() {
        return ercode;
    }

    public void setErcode(String ercode) {
        this.ercode = ercode;
    }

    public String getRetrmg() {
        return retrmg;
    }

    public void setRetrmg(String retrmg) {
        this.retrmg = retrmg;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChanfl() {
        return chanfl;
    }

    public void setChanfl(String chanfl) {
        this.chanfl = chanfl;
    }

    public String getPlatsq() {
        return platsq;
    }

    public void setPlatsq(String platsq) {
        this.platsq = platsq;
    }

    @Override
    public String toString() {
        return "Body{" +
                "cardno='" + cardno + '\'' +
                ", currcd='" + currcd + '\'' +
                ", cdtlmt=" + cdtlmt +
                ", dlcrin='" + dlcrin + '\'' +
                ", dlcrcd='" + dlcrcd + '\'' +
                ", platdt='" + platdt + '\'' +
                ", ebnksq='" + ebnksq + '\'' +
                ", ercode='" + ercode + '\'' +
                ", retrmg='" + retrmg + '\'' +
                ", status='" + status + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", chanfl='" + chanfl + '\'' +
                ", platsq='" + platsq + '\'' +
                '}';
    }
}
