package cn.com.yusys.yusp.online.client.esb.core.ln3176.req;

import java.util.List;

/**
 * 请求Service：本交易用户贷款多个还款账户进行还款
 *
 * @author leehuang
 * @version 1.0
 */
public class LstLnHuankDZhhObj {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3176.req.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstLnHuankDZhhObj{" +
                "record=" + record +
                '}';
    }
}
