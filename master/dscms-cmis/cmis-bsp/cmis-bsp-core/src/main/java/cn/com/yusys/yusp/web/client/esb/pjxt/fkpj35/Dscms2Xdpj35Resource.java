package cn.com.yusys.yusp.web.client.esb.pjxt.fkpj35;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.pjxt.fkpj35.req.Fkpj35ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.fkpj35.resp.Fkpj35RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.Fkpj35ReqService;
import cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.resp.Fkpj35RespService;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 接口处理类: 资产池票据解质押申请
 *
 * @author lihh
 * @version 1.0
 */
@RestController
@RequestMapping("/api/dscms2pjxt")
public class Dscms2Xdpj35Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Xdpj35Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：Fkpj35
     * 交易描述：票据承兑签发审批请求
     *
     * @param fkpj35ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fkpj35")
    protected @ResponseBody
    ResultDto<Fkpj35RespDto> fkpj35(@Validated @RequestBody Fkpj35ReqDto fkpj35ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKPJ35.key, EsbEnum.TRADE_CODE_FKPJ35.value, JSON.toJSONString(fkpj35ReqDto));
        cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.Service();
        cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.resp.Service();
        Fkpj35ReqService fkpj35ReqService = new Fkpj35ReqService();
        Fkpj35RespService fkpj35RespService = new Fkpj35RespService();
        Fkpj35RespDto fkpj35RespDto = new Fkpj35RespDto();
        ResultDto<Fkpj35RespDto> Fkpj35ResultDto = new ResultDto<Fkpj35RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fkpj35ReqDto转换成reqService
            BeanUtils.copyProperties(fkpj35ReqDto, reqService);
//            java.util.List<cn.com.yusys.yusp.dto.client.esb.pjxt.fkpj35.req.List> reqList = Fkpj35ReqDto.getList();
//            if (CollectionUtils.nonEmpty(reqList)) {
//                java.util.List<cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.Record> recordList = reqList.parallelStream().map(req -> {
//                    cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.Record record = new cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.Record();
//                    BeanUtils.copyProperties(req, record);
//                    return record;
//                }).collect(Collectors.toList());
//                cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.List serviceList = new cn.com.yusys.yusp.online.client.esb.pjxt.fkpj35.req.List();
//                serviceList.setRecord(recordList);
//                reqService.setList(serviceList);
//            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_FKPJ35.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_PJ.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_PJ.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            reqService.setTxCode(EsbEnum.TRADE_CODE_FKPJ35.key);
            fkpj35ReqService.setService(reqService);
            // 将Fkpj35ReqService转换成Fkpj35ReqServiceMap
            Map fkpj35ReqServiceMap = beanMapUtil.beanToMap(fkpj35ReqService);
            context.put("tradeDataMap", fkpj35ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKPJ35.key, EsbEnum.TRADE_CODE_FKPJ35.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FKPJ35.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKPJ35.key, EsbEnum.TRADE_CODE_FKPJ35.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fkpj35RespService = beanMapUtil.mapToBean(tradeDataMap, Fkpj35RespService.class, Fkpj35RespService.class);
            respService = fkpj35RespService.getService();
            //  将respService转换成Fkpj35RespDto
            BeanUtils.copyProperties(respService, fkpj35RespDto);
            Fkpj35ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
            Fkpj35ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.SUCCESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypbdccxRespDto
                BeanUtils.copyProperties(respService, fkpj35RespDto);
                Fkpj35ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                Fkpj35ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                Fkpj35ResultDto.setCode(EpbEnum.EPB099999.key);
                Fkpj35ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKPJ35.key, EsbEnum.TRADE_CODE_FKPJ35.value, e.getMessage());
            Fkpj35ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            Fkpj35ResultDto.setMessage(e.getMessage());//系统异常
        }
        Fkpj35ResultDto.setData(fkpj35RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FKPJ35.key, EsbEnum.TRADE_CODE_FKPJ35.value, JSON.toJSONString(Fkpj35ResultDto));
        return Fkpj35ResultDto;
    }
}
