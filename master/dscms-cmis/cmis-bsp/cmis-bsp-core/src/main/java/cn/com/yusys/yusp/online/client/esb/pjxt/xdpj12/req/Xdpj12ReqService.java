package cn.com.yusys.yusp.online.client.esb.pjxt.xdpj12.req;

/**
 * 请求Service：票据池推送保证金账号
 *
 * @author chenyong
 * @version 1.0
 */
public class Xdpj12ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Xdpj12ReqService{" +
                "service=" + service +
                '}';
    }
}
