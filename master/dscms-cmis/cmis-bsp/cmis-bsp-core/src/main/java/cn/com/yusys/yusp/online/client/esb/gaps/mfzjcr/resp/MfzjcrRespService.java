package cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.resp;

/**
 * 响应Service：买方资金存入
 *
 * @author chenyong
 * @version 1.0
 */
public class MfzjcrRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
