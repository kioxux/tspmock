package cn.com.yusys.yusp.web.client.esb.xwywglpt.dhxd01;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.req.Dhxd01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.resp.Dhxd01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.req.Dhxd01ReqService;
import cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.resp.Dhxd01RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 *
 * @author lihh
 * @version 1.0
 */
@Api(tags = "BSP封装小微业务管理平台接口处理类")
@RestController
@RequestMapping("/api/dscms2xwywglpt")
public class Dscms2Dhxd01Resource {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Dhxd01Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：dhxd01
     * 交易描述：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
     *
     * @param dhxd01ReqDto
     * @return
     * @throws Exception
     */
	@ApiOperation("dhxd01:信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口")
    @PostMapping("/dhxd01")
    protected @ResponseBody
    ResultDto<Dhxd01RespDto> dhxd01(@Validated @RequestBody Dhxd01ReqDto dhxd01ReqDto) throws Exception {
		logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD01.key, EsbEnum.TRADE_CODE_DHXD01.value, JSON.toJSONString(dhxd01ReqDto));
		cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.req.Service();
		cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.xwywglpt.dhxd01.resp.Service();
        Dhxd01ReqService dhxd01ReqService = new Dhxd01ReqService();
        Dhxd01RespService dhxd01RespService = new Dhxd01RespService();
        Dhxd01RespDto dhxd01RespDto = new Dhxd01RespDto();
        ResultDto<Dhxd01RespDto> dhxd01ResultDto = new ResultDto<Dhxd01RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
		try {
			//  将dhxd01ReqDto转换成reqService
			BeanUtils.copyProperties(dhxd01ReqDto, reqService);
			reqService.setPrcscd(EsbEnum.TRADE_CODE_DHXD01.key);//    交易码
			reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
			logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
			String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
			logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
			reqService.setServsq(servsq);//    渠道流水
			reqService.setUserid(EsbEnum.USERID_XWYWGLPT.key);//    柜员号
			reqService.setBrchno(EsbEnum.BRCHNO_XWYWGLPT.key);//    部门号
			dhxd01ReqService.setService(reqService);
			// 将dhxd01ReqService转换成dhxd01ReqServiceMap
			Map dhxd01ReqServiceMap = beanMapUtil.beanToMap(dhxd01ReqService);
			context.put("tradeDataMap", dhxd01ReqServiceMap);
			logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD01.key, EsbEnum.TRADE_CODE_DHXD01.value);
			result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_DHXD01.key, context);
			logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD01.key, EsbEnum.TRADE_CODE_DHXD01.value);
			// 从result中获取相关的值
			Map tradeDataMap = (Map) result.get("tradeDataMap");

			dhxd01RespService = beanMapUtil.mapToBean(tradeDataMap, Dhxd01RespService.class, Dhxd01RespService.class);
			respService = dhxd01RespService.getService();
			//  将respService转换成Dhxd01RespDto
			dhxd01ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.key));
			dhxd01ResultDto.setMessage(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.SUCCESS.value));
			if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
				//  将respService转换成Fbxd02RespDto
				BeanUtils.copyProperties(respService, dhxd01RespDto);
				dhxd01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
				dhxd01ResultDto.setMessage(SuccessEnum.SUCCESS.value);
			} else {
				dhxd01ResultDto.setCode(EpbEnum.EPB099999.key);
				dhxd01ResultDto.setMessage(respService.getErortx());
			}
		} catch (Exception e) {
			logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD01.key, EsbEnum.TRADE_CODE_DHXD01.value, e.getMessage());
			dhxd01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
			dhxd01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
		}
		dhxd01ResultDto.setData(dhxd01RespDto);
		logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_DHXD01.key, EsbEnum.TRADE_CODE_DHXD01.value, JSON.toJSONString(dhxd01ResultDto));
        return dhxd01ResultDto;
    }
}
