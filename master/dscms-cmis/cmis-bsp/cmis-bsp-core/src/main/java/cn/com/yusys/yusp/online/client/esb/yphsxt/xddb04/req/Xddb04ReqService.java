package cn.com.yusys.yusp.online.client.esb.yphsxt.xddb04.req;

/**
 * 请求Service：查询质押物信息
 * @author code-generator
 * @version 1.0             
 */      
public class Xddb04ReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
