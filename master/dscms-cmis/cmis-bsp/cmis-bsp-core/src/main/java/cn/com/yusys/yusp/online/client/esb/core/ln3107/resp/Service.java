package cn.com.yusys.yusp.online.client.esb.core.ln3107.resp;

import java.util.List;

/**
 * 响应Dto：贷款形态转移明细查询
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String servsq;//渠道流水
    private String datasq;//全局流水
    private List<Lstdkxtzy> lstdkxtzie;//贷款形态转移明细

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public List<Lstdkxtzy> getLstdkxtzie() {
        return lstdkxtzie;
    }

    public void setLstdkxtzie(List<Lstdkxtzy> lstdkxtzie) {
        this.lstdkxtzie = lstdkxtzie;
    }

    @Override
    public String toString() {
        return "Ln3107RespDtoService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", lstdkxtzie=" + lstdkxtzie +
                '}';
    }
}
