package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Service：工商数据联网核查
 *
 * @author tangxun
 * @version 1.0.0
 * @date 2021/4/11 下午4:28
 */
@JsonPropertyOrder(alphabetic = true)
public class ZsnewReqService implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//    交易码
    @JsonProperty(value = "servtp")
    private String servtp;//    渠道码
    @JsonProperty(value = "servsq")
    private String servsq;//    渠道流水好
    @JsonProperty(value = "userid")
    private String userid;//    柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//    部门号

    @JsonProperty(value = "creditcode")
    private String creditcode;   //统一信用代码
    @JsonProperty(value = "entstatus")
    private String entstatus;    //企业经营状态，1：在营，2：非在营
    @JsonProperty(value = "enttype")
    private String enttype;      //企业类型:1-企业 2-个体
    @JsonProperty(value = "id")
    private String id;           //中数企业ID
    @JsonProperty(value = "name")
    private String name;         //企业名称
    @JsonProperty(value = "orgcode")
    private String orgcode;      //组织机构代码
    @JsonProperty(value = "regno")
    private String regno;        //企业注册号
    @JsonProperty(value = "version")
    private String version;      //高管识别码版本号,返回高管识别码时生效

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCreditcode() {
        return creditcode;
    }

    public void setCreditcode(String creditcode) {
        this.creditcode = creditcode;
    }

    public String getEntstatus() {
        return entstatus;
    }

    public void setEntstatus(String entstatus) {
        this.entstatus = entstatus;
    }

    public String getEnttype() {
        return enttype;
    }

    public void setEnttype(String enttype) {
        this.enttype = enttype;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrgcode() {
        return orgcode;
    }

    public void setOrgcode(String orgcode) {
        this.orgcode = orgcode;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "ZsnewReqService{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", creditcode='" + creditcode + '\'' +
                ", entstatus='" + entstatus + '\'' +
                ", enttype='" + enttype + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", orgcode='" + orgcode + '\'' +
                ", regno='" + regno + '\'' +
                ", version='" + version + '\'' +
                '}';
    }
}
