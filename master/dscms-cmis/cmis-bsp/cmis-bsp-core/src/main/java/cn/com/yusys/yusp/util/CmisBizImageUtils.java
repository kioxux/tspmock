package cn.com.yusys.yusp.util;

import cn.com.yusys.yusp.commons.util.StringUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.dto.client.http.image.callimage.CallImageReqDto;
import cn.com.yusys.yusp.dto.client.http.image.callimage.Para;
import cn.com.yusys.yusp.enums.online.DscmsBizDbEnum;
import cn.com.yusys.yusp.enums.online.DscmsBizZxEnum;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;

/**
 * 档案管理中影像的工具类
 *
 * @author leehuang
 */
public class CmisBizImageUtils {

    private static Logger logger = LoggerFactory.getLogger(CmisBizImageUtils.class);

    public static void main(String[] args) {
        CallImageReqDto callImageReqDto = new CallImageReqDto();
        callImageReqDto.setPrefixUrl("http://10.28.122.198/image.html");
        List<Para> paras = new ArrayList<>();
        Para para = new Para();
        Map<String, Object> index = new HashMap<>();
        index.put("contid", "testcontid");
        para.setIndex(index);
        para.setTop_outsystem_code("CMIS");
        para.setOutsystem_code("xxd");
        paras.add(para);
        callImageReqDto.setPara(paras);
        callImageReqDto.setAuthorization("Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTYxOTQyNDIwMH0.6m2e_IwZBmYcZzV5iIxyF7uf9knd_Jx_c24-YjAP6KxGIyhIZOpSUhuYUqcF0b7z1QUCFvD21DaLUIq1L23o7Q");
        callImageReqDto.setS(DscmsBizZxEnum.IMAGE_S_1.key);
        callImageReqDto.setAuthority("download;scan;import;delImg;insert;replace;copy;cut;setValidDate;order");
        System.out.println(callimage(callImageReqDto));

        String result = "http://10.28.122.198/image.html?para=%5B%7B%22opType%22%3A%220%22%2C%22index%22%3A%7B%22assuretype%22%3A%22%22%2C%22dzyid%22%3A%22%22%2C%22docid%22%3A%22%22%2C%22businessid%22%3A%228400163524%22%2C%22custconduct%22%3A%2294011911%22%2C%22operid%22%3A%2294011911%22%2C%22bzrid%22%3A%22%22%2C%22custconductname%22%3A%22%E6%B2%99%E6%B6%9B%22%2C%22orgid%22%3A%22019000%22%2C%22custtype%22%3A%22211%22%2C%22loadtype%22%3A%22%22%2C%22contid%22%3A%22%22%2C%22orgname%22%3A%22%E8%A5%BF%E5%BC%A0%E6%94%AF%E8%A1%8C%22%2C%22opername%22%3A%22%E6%B2%99%E6%B6%9B%22%2C%22custid%22%3A%228400163524%22%2C%22creditamount%22%3A%22%22%2C%22maintaindate%22%3A%2220210531202026%22%2C%22billno%22%3A%22%22%2C%22custname%22%3A%22%E8%8B%8F%E5%B7%9E%E9%9A%86%E9%98%B3%E5%8C%BB%E7%96%97%E5%B9%BC%E5%9E%9B%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8%22%2C%22credittype%22%3A%22%22%7D%2C%22top_outsystem_code%22%3A%22DGKHZL%22%2C%22outsystem_code%22%3A%22%22%2C%22version%22%3A%7B%7D%7D%5D&Authorization=Bearer%20eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTYyMzY2MzYyNn0.-HUgE8AbXSC5KYIj16nVFR1oY6CHzlnMl203_yTAPpXqoQ2dSqu5QsYA6XArgnBFUPUnte_gAQ2iet3u03fgww&s=2&authority=download&operCode=94011911&orgid=";
        try {
            String test = URLDecoder.decode(result, "UTF-8");
            System.out.println("============================");
            System.out.println(test);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取影像采集和查看页面
     * http://10.28.122.198/image.html?</br>
     * para=[{"index":{"contid":"30ed1a1488ad43e78d7529b038d08f79"},"top_outsystem_code":"XDCZDCCZ","outsystem_code":"XDCZDCCZ"}] </br>
     * &Authorization=Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTYxOTQyNDIwMH0.6m2e_IwZBmYcZzV5iIxyF7uf9knd_Jx_c24-YjAP6KxGIyhIZOpSUhuYUqcF0b7z1QUCFvD21DaLUIq1L23o7Q</br>
     * &s=1</br>
     * &authority=download;scan;import;delImg;insert;replace;copy;cut;setValidDate;order</br>
     *
     * @param callImageReqDto
     * @return
     */
    public static String callimage(CallImageReqDto callImageReqDto) {
        String callImageUrl = StringUtils.EMPTY;
        String prefixUrl = callImageReqDto.getPrefixUrl();//URL前缀
        List<Para> paraList = callImageReqDto.getPara();
        String authorization = callImageReqDto.getAuthorization();// Token值
        String s = Optional.ofNullable(callImageReqDto.getS()).orElse(DscmsBizZxEnum.IMAGE_S_2.key);//1-采集 2-查看
        String authority = callImageReqDto.getAuthority();//权限按钮控制，默认没有任何权限；每个权限用分号;隔开
        if(logger.isDebugEnabled()){
            logger.debug("URL前缀:{}", prefixUrl);
            logger.debug("索引信息json对象:{}", paraList);
            logger.debug("Token值:{}", authorization);
            logger.debug("权限按钮控制:{}", authority);
        }
        //  SYMBOL_QUESTION("?","问号"),
        String SYMBOL_QUESTION = DscmsBizDbEnum.SYMBOL_QUESTION.key;
        //  SYMBOL_AND("&","与"),
        String SYMBOL_AND = DscmsBizDbEnum.SYMBOL_AND.key;
        //  SYMBOL_EQUAL("=","等于号"),
        String SYMBOL_EQUAL = DscmsBizDbEnum.SYMBOL_EQUAL.key;
        callImageUrl = prefixUrl.concat(SYMBOL_QUESTION)// URL前缀
                .concat("para").concat(SYMBOL_EQUAL).concat(JSONArray.toJSONString(paraList))//
                .concat(SYMBOL_AND).concat("Authorization").concat(SYMBOL_EQUAL).concat(authorization)//Token值
                .concat(SYMBOL_AND).concat("s").concat(SYMBOL_EQUAL).concat(s)// 1-采集 2-查看
                .concat(SYMBOL_AND).concat("authority").concat(SYMBOL_EQUAL).concat(authority);//权限按钮控制，默认没有任何权限；每个权限用分号;隔开
        if(logger.isDebugEnabled()){
            logger.debug("callImageUrl:{}", callImageUrl.trim());
        }
        return callImageUrl.trim();
    }
}
