package cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp;

/**
 * 响应Service：查询共有人信息
 *
 * @author chenyong
 * @version 1.0
 */
public class Service {
    private String erorcd; // 响应码,0000表示成功其它表示失败
    private String erortx; // 响应信息,失败详情
    private String returnCode; // 处理结果
    private String returnInfo; // 处理信息
    private cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.List list;//list

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnInfo() {
        return returnInfo;
    }

    public void setReturnInfo(String returnInfo) {
        this.returnInfo = returnInfo;
    }

    public cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.List getList() {
        return list;
    }

    public void setList(cn.com.yusys.yusp.online.client.esb.ypxt.xdypgyrcx.resp.List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", returnCode='" + returnCode + '\'' +
                ", returnInfo='" + returnInfo + '\'' +
                ", list='" + list + '\'' +
                '}';
    }
}
