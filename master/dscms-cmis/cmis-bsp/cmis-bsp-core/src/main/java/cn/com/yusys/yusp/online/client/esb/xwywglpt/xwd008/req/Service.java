package cn.com.yusys.yusp.online.client.esb.xwywglpt.xwd008.req;

/**
 * 请求Service：新信贷同步用户账号
 */
public class Service {
    private String prcscd;//处理码
    private String servtp;//渠道
    private String servsq;//渠道流水
    private String userid;//柜员号
    private String brchno;//部门号

    private String createUser;//创建人
    private String id;//账号
    private String loginName;//工号
    private String name;//姓名
    private String status;//状态 A有效
    private String brhId;//所属部门
    private String orgId;//所在机构
    private String teamType;//团队类型 1车易贷 2快贷 3优企贷
    private String phone;//移动电话
    private String tel;//办公电话
    private String grantOrg;//授权机构id
    private String regionList;//区域编号数组
    private String positionList;//岗位编号数组
    private String roleList;//角色编号数组

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBrhId() {
        return brhId;
    }

    public void setBrhId(String brhId) {
        this.brhId = brhId;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getGrantOrg() {
        return grantOrg;
    }

    public void setGrantOrg(String grantOrg) {
        this.grantOrg = grantOrg;
    }

    public String getRegionList() {
        return regionList;
    }

    public void setRegionList(String regionList) {
        this.regionList = regionList;
    }

    public String getPositionList() {
        return positionList;
    }

    public void setPositionList(String positionList) {
        this.positionList = positionList;
    }

    public String getRoleList() {
        return roleList;
    }

    public void setRoleList(String roleList) {
        this.roleList = roleList;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", createUser='" + createUser + '\'' +
                ", id='" + id + '\'' +
                ", loginName='" + loginName + '\'' +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", brhId='" + brhId + '\'' +
                ", orgId='" + orgId + '\'' +
                ", teamType='" + teamType + '\'' +
                ", phone='" + phone + '\'' +
                ", tel='" + tel + '\'' +
                ", grantOrg='" + grantOrg + '\'' +
                ", regionList='" + regionList + '\'' +
                ", positionList='" + positionList + '\'' +
                ", roleList='" + roleList + '\'' +
                '}';
    }
}
