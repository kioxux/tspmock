package cn.com.yusys.yusp.online.client.gxp.wxpt.qywx.req;

/**
 * 请求Hsiss:企业微信
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Hsiss {
    private String realtime;
    private String text;
    private String mobile;
    private String tranti;
    private String channo;
    private String lastsndtime;
    private String byzd;
    private String byzd2;

    public String getRealtime() {
        return realtime;
    }

    public void setRealtime(String realtime) {
        this.realtime = realtime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getTranti() {
        return tranti;
    }

    public void setTranti(String tranti) {
        this.tranti = tranti;
    }

    public String getChanno() {
        return channo;
    }

    public void setChanno(String channo) {
        this.channo = channo;
    }

    public String getLastsndtime() {
        return lastsndtime;
    }

    public void setLastsndtime(String lastsndtime) {
        this.lastsndtime = lastsndtime;
    }

    public String getByzd() {
        return byzd;
    }

    public void setByzd(String byzd) {
        this.byzd = byzd;
    }

    public String getByzd2() {
        return byzd2;
    }

    public void setByzd2(String byzd2) {
        this.byzd2 = byzd2;
    }

    @Override
    public String toString() {
        return "Hsiss{" +
                "realtime='" + realtime + '\'' +
                ", text='" + text + '\'' +
                ", mobile='" + mobile + '\'' +
                ", tranti='" + tranti + '\'' +
                ", channo='" + channo + '\'' +
                ", lastsndtime='" + lastsndtime + '\'' +
                ", byzd='" + byzd + '\'' +
                ", byzd2='" + byzd2 + '\'' +
                '}';
    }
}
