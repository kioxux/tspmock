package cn.com.yusys.yusp.online.client.esb.ypxt.bdcqcx.req;

/**
 * 请求Service：不动产权证查询接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
public class BdcqcxReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
