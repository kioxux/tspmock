package cn.com.yusys.yusp.online.client.esb.core.co3225.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/31 16:21
 * @since 2021/5/31 16:21
 */
public class lstdzywmx_ARRAY {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.co3225.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Lstdzywmx{" +
                "record=" + record +
                '}';
    }
}
