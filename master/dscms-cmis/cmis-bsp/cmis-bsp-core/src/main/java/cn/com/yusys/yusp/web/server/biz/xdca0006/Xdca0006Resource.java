package cn.com.yusys.yusp.web.server.biz.xdca0006;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdca0006.req.Xdca0006ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdca0006.resp.Xdca0006RespDto;
import cn.com.yusys.yusp.dto.server.xdca0006.req.Xdca0006DataReqDto;
import cn.com.yusys.yusp.dto.server.xdca0006.resp.Xdca0006DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCaClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:信用卡审批结果查询接口
 *
 * @author wzy
 * @version 1.0
 */
@Api(tags = "XDCA0006:信用卡审批结果查询接口")
@RestController
@RequestMapping("/api/dscms")
public class Xdca0006Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdca0006Resource.class);

	@Autowired
	private DscmsBizCaClientService dscmsBizCaClientService;

    /**
     * 交易码：xdca0006
     * 交易描述：大额分期合同签订接口
     *
     * @param xdca0006ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("信用卡审批结果查询接口")
    @PostMapping("/xdca0006")
//   @Idempotent({"xdcaca0006", "#xdca0006ReqDto.datasq"})s
    protected @ResponseBody
	Xdca0006RespDto xdca0006(@Validated @RequestBody Xdca0006ReqDto xdca0006ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006ReqDto));
        Xdca0006DataReqDto xdca0006DataReqDto = new Xdca0006DataReqDto();// 请求Data： 信用卡审批结果查询接口
        Xdca0006DataRespDto xdca0006DataRespDto = new Xdca0006DataRespDto();// 响应Data：信用卡审批结果查询接口
		Xdca0006RespDto xdca0006RespDto = new Xdca0006RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdca0006.req.Data reqData = null; // 请求Data：信用卡审批结果查询接口
		cn.com.yusys.yusp.dto.server.biz.xdca0006.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdca0006.resp.Data();// 响应Data：信用卡审批结果查询接口
        //  此处包导入待确定 结束
        try {
            // 从 xdca0006ReqDto获取 reqData
            reqData = xdca0006ReqDto.getData();
            // 将 reqData 拷贝到xdca0006DataReqDto
            BeanUtils.copyProperties(reqData, xdca0006DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataReqDto));
            ResultDto<Xdca0006DataRespDto> xdca0006DataResultDto = dscmsBizCaClientService.xdca0006(xdca0006DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdca0006RespDto.setErorcd(Optional.ofNullable(xdca0006DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdca0006RespDto.setErortx(Optional.ofNullable(xdca0006DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdca0006DataResultDto.getCode())) {
                xdca0006DataRespDto = xdca0006DataResultDto.getData();
                xdca0006RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdca0006RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdca0006DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdca0006DataRespDto, respData);
            }
        } catch (BizException e) {
            logger.info(TradeLogConstants.SERVICE_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, e.getMessage());
            throw BizException.error(null, e.getErrorCode(), e.getMessage());
        }catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, e.getMessage());
            xdca0006RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdca0006RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdca0006RespDto.setDatasq(servsq);

        xdca0006RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCA0006.key, DscmsEnum.TRADE_CODE_XDCA0006.value, JSON.toJSONString(xdca0006RespDto));
        return xdca0006RespDto;
    }
}
