package cn.com.yusys.yusp.online.client.esb.circp.fb1214.req.endlist;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/10 20:13
 * @since 2021/8/10 20:13
 */
public class Record {

    @JsonProperty(value = "ENT_NAME")
    private String ENT_NAME;//企业名称
    @JsonProperty(value = "ENT_BUZ_LIC_NO")
    private String ENT_BUZ_LIC_NO;//营业执照号
    @JsonProperty(value = "ENT_ANNUAL_SALES")
    private BigDecimal ENT_ANNUAL_SALES;//年销售收入

    @JsonIgnore
    public String getENT_NAME() {
        return ENT_NAME;
    }

    @JsonIgnore
    public void setENT_NAME(String ENT_NAME) {
        this.ENT_NAME = ENT_NAME;
    }

    @JsonIgnore
    public String getENT_BUZ_LIC_NO() {
        return ENT_BUZ_LIC_NO;
    }

    @JsonIgnore
    public void setENT_BUZ_LIC_NO(String ENT_BUZ_LIC_NO) {
        this.ENT_BUZ_LIC_NO = ENT_BUZ_LIC_NO;
    }

    @JsonIgnore
    public BigDecimal getENT_ANNUAL_SALES() {
        return ENT_ANNUAL_SALES;
    }

    @JsonIgnore
    public void setENT_ANNUAL_SALES(BigDecimal ENT_ANNUAL_SALES) {
        this.ENT_ANNUAL_SALES = ENT_ANNUAL_SALES;
    }

    @Override
    public String toString() {
        return "ENT_LIST{" +
                "ENT_NAME='" + ENT_NAME + '\'' +
                ", ENT_BUZ_LIC_NO=" + ENT_BUZ_LIC_NO +
                ", ENT_ANNUAL_SALES=" + ENT_ANNUAL_SALES +
                '}';
    }
}
