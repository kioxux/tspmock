package cn.com.yusys.yusp.web.server.biz.xddh0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xddh0004.req.Xddh0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xddh0004.resp.Xddh0004RespDto;
import cn.com.yusys.yusp.dto.server.xddh0004.req.Xddh0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xddh0004.resp.Xddh0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizDhClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询还款是否在途
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDDH0004:查询还款是否在途")
@RestController
@RequestMapping("/api/dscms")
public class Xddh0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xddh0004Resource.class);
    @Autowired
    private DscmsBizDhClientService dscmsBizDhClientService;

    /**
     * 交易码：xddh0004
     * 交易描述：查询还款是否在途
     *
     * @param xddh0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询还款是否在途")
    @PostMapping("/xddh0004")
//@Idempotent({"xddh0004", "#xddh0004ReqDto.datasq"})
    protected @ResponseBody
    Xddh0004RespDto xddh0004(@Validated @RequestBody Xddh0004ReqDto xddh0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, JSON.toJSONString(xddh0004ReqDto));
        Xddh0004DataReqDto xddh0004DataReqDto = new Xddh0004DataReqDto();// 请求Data： 查询还款是否在途
        Xddh0004DataRespDto xddh0004DataRespDto = new Xddh0004DataRespDto();// 响应Data：查询还款是否在途
        Xddh0004RespDto xddh0004RespDto = new Xddh0004RespDto();
        cn.com.yusys.yusp.dto.server.biz.xddh0004.req.Data reqData = null; // 请求Data：查询还款是否在途
        cn.com.yusys.yusp.dto.server.biz.xddh0004.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xddh0004.resp.Data();// 响应Data：查询还款是否在途
        try {
            // 从 xddh0004ReqDto获取 reqData
            reqData = xddh0004ReqDto.getData();
            // 将 reqData 拷贝到xddh0004DataReqDto
            BeanUtils.copyProperties(reqData, xddh0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, JSON.toJSONString(xddh0004DataReqDto));
            ResultDto<Xddh0004DataRespDto> xddh0004DataResultDto = dscmsBizDhClientService.xddh0004(xddh0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, JSON.toJSONString(xddh0004DataResultDto));
            // 从返回值中获取响应码和响应消息
            xddh0004RespDto.setErorcd(Optional.ofNullable(xddh0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xddh0004RespDto.setErortx(Optional.ofNullable(xddh0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xddh0004DataResultDto.getCode())) {
                xddh0004DataRespDto = xddh0004DataResultDto.getData();
                xddh0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xddh0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xddh0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xddh0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, e.getMessage());
            xddh0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xddh0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xddh0004RespDto.setDatasq(servsq);

        xddh0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDDH0004.key, DscmsEnum.TRADE_CODE_XDDH0004.value, JSON.toJSONString(xddh0004RespDto));
        return xddh0004RespDto;
    }
}
