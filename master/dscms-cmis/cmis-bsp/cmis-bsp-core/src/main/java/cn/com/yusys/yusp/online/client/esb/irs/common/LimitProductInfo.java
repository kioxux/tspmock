package cn.com.yusys.yusp.online.client.esb.irs.common;

import java.util.List;

/**
 * 请求Service：交易请求信息域:产品额度信息
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月15日15:10:55
 */
public class LimitProductInfo {
    private List<LimitProductInfoRecord> record; //	授信分项流水号

    public List<LimitProductInfoRecord> getRecord() {
        return record;
    }

    public void setRecord(List<LimitProductInfoRecord> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LimitProductInfo{" +
                "record=" + record +
                '}';
    }
}