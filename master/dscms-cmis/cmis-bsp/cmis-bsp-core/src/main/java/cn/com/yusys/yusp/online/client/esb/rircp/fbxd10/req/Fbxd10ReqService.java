package cn.com.yusys.yusp.online.client.esb.rircp.fbxd10.req;

/**
 * 请求Service：查询日初（合约）信息历史表（利翃）的合约状态和结清日期
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd10ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd10ReqService{" +
                "service=" + service +
                '}';
    }
}
