package cn.com.yusys.yusp.online.client.esb.core.dp2021.req;

/**
 * 请求Service：客户账号-子账号互查
 */
public class Service {

    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String xzsxnzhi;//限制属性值
    private String chaxleix;//查询类型
    private String chaxfanw;//查询范围
    private String kehuhaoo;//客户号
    private String kehuzhao;//客户账号
    private String zhanghao;//负债账号
    private String dinhuobz;//产品定活标志
    private String zhhaoxuh;//子账户序号
    private String cunkzlei;//存款种类
    private String huobdaih;//币种
    private String chaohubz;//账户钞汇标志
    private String zhcxzhzt;//客户账户查询状态
    private String chaxmima;//查询密码
    private Integer qishibis;//起始笔数
    private Integer chxunbis;//查询笔数
    private String shifoudy;//是否打印
    private String jigouhao;//机构号
    private String mimazlei;//密码种类
    private String sfcxglzh;//是否查询关联账户
    private String zhjnzlei;//证件种类
    private String zhjhaoma;//证件号码
    private String kehuzhmc;//客户账户名称
    private String beiyzd01;//备用字段01
    private String beiyzd02;//备用字段02
    private String jiymkzbz;//限制标志
    private String zhhufenl;//账户分类
    private String zhshuxin;//账户属性


    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getXzsxnzhi() {
        return xzsxnzhi;
    }

    public void setXzsxnzhi(String xzsxnzhi) {
        this.xzsxnzhi = xzsxnzhi;
    }

    public String getChaxleix() {
        return chaxleix;
    }

    public void setChaxleix(String chaxleix) {
        this.chaxleix = chaxleix;
    }

    public String getChaxfanw() {
        return chaxfanw;
    }

    public void setChaxfanw(String chaxfanw) {
        this.chaxfanw = chaxfanw;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getZhanghao() {
        return zhanghao;
    }

    public void setZhanghao(String zhanghao) {
        this.zhanghao = zhanghao;
    }

    public String getDinhuobz() {
        return dinhuobz;
    }

    public void setDinhuobz(String dinhuobz) {
        this.dinhuobz = dinhuobz;
    }

    public String getZhhaoxuh() {
        return zhhaoxuh;
    }

    public void setZhhaoxuh(String zhhaoxuh) {
        this.zhhaoxuh = zhhaoxuh;
    }

    public String getCunkzlei() {
        return cunkzlei;
    }

    public void setCunkzlei(String cunkzlei) {
        this.cunkzlei = cunkzlei;
    }

    public String getHuobdaih() {
        return huobdaih;
    }

    public void setHuobdaih(String huobdaih) {
        this.huobdaih = huobdaih;
    }

    public String getChaohubz() {
        return chaohubz;
    }

    public void setChaohubz(String chaohubz) {
        this.chaohubz = chaohubz;
    }

    public String getZhcxzhzt() {
        return zhcxzhzt;
    }

    public void setZhcxzhzt(String zhcxzhzt) {
        this.zhcxzhzt = zhcxzhzt;
    }

    public String getChaxmima() {
        return chaxmima;
    }

    public void setChaxmima(String chaxmima) {
        this.chaxmima = chaxmima;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    public String getJigouhao() {
        return jigouhao;
    }

    public void setJigouhao(String jigouhao) {
        this.jigouhao = jigouhao;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getSfcxglzh() {
        return sfcxglzh;
    }

    public void setSfcxglzh(String sfcxglzh) {
        this.sfcxglzh = sfcxglzh;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getKehuzhmc() {
        return kehuzhmc;
    }

    public void setKehuzhmc(String kehuzhmc) {
        this.kehuzhmc = kehuzhmc;
    }

    public String getBeiyzd01() {
        return beiyzd01;
    }

    public void setBeiyzd01(String beiyzd01) {
        this.beiyzd01 = beiyzd01;
    }

    public String getBeiyzd02() {
        return beiyzd02;
    }

    public void setBeiyzd02(String beiyzd02) {
        this.beiyzd02 = beiyzd02;
    }

    public String getJiymkzbz() {
        return jiymkzbz;
    }

    public void setJiymkzbz(String jiymkzbz) {
        this.jiymkzbz = jiymkzbz;
    }

    public String getZhhufenl() {
        return zhhufenl;
    }

    public void setZhhufenl(String zhhufenl) {
        this.zhhufenl = zhhufenl;
    }

    public String getZhshuxin() {
        return zhshuxin;
    }

    public void setZhshuxin(String zhshuxin) {
        this.zhshuxin = zhshuxin;
    }


    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", xzsxnzhi='" + xzsxnzhi + '\'' +
                ", chaxleix='" + chaxleix + '\'' +
                ", chaxfanw='" + chaxfanw + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", zhanghao='" + zhanghao + '\'' +
                ", dinhuobz='" + dinhuobz + '\'' +
                ", zhhaoxuh='" + zhhaoxuh + '\'' +
                ", cunkzlei='" + cunkzlei + '\'' +
                ", huobdaih='" + huobdaih + '\'' +
                ", chaohubz='" + chaohubz + '\'' +
                ", zhcxzhzt='" + zhcxzhzt + '\'' +
                ", chaxmima='" + chaxmima + '\'' +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", shifoudy='" + shifoudy + '\'' +
                ", jigouhao='" + jigouhao + '\'' +
                ", mimazlei='" + mimazlei + '\'' +
                ", sfcxglzh='" + sfcxglzh + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", kehuzhmc='" + kehuzhmc + '\'' +
                ", beiyzd01='" + beiyzd01 + '\'' +
                ", beiyzd02='" + beiyzd02 + '\'' +
                ", jiymkzbz='" + jiymkzbz + '\'' +
                ", zhhufenl='" + zhhufenl + '\'' +
                ", zhshuxin='" + zhshuxin + '\'' +
                '}';
    }
}
