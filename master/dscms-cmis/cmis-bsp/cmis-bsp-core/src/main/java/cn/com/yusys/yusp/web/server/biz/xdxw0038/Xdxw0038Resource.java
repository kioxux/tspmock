package cn.com.yusys.yusp.web.server.biz.xdxw0038;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0038.req.Xdxw0038ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0038.resp.Xdxw0038RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0038.req.Xdxw0038DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0038.resp.Xdxw0038DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询调查表和其他关联信息
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0038:查询调查表和其他关联信息")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0038Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0038Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0038
     * 交易描述：查询调查表和其他关联信息
     *
     * @param xdxw0038ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询调查表和其他关联信息")
    @PostMapping("/xdxw0038")
    //@Idempotent({"xdcaxw0038", "#xdxw0038ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0038RespDto xdxw0038(@Validated @RequestBody Xdxw0038ReqDto xdxw0038ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038ReqDto));
        Xdxw0038DataReqDto xdxw0038DataReqDto = new Xdxw0038DataReqDto();// 请求Data： 查询调查表和其他关联信息
        Xdxw0038DataRespDto xdxw0038DataRespDto = new Xdxw0038DataRespDto();// 响应Data：查询调查表和其他关联信息
        Xdxw0038RespDto xdxw0038RespDto = new Xdxw0038RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0038.req.Data reqData = null; // 请求Data：查询调查表和其他关联信息
        cn.com.yusys.yusp.dto.server.biz.xdxw0038.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0038.resp.Data();// 响应Data：查询调查表和其他关联信息
        try {
            // 从 xdxw0038ReqDto获取 reqData
            reqData = xdxw0038ReqDto.getData();
            // 将 reqData 拷贝到xdxw0038DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0038DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038DataReqDto));
            ResultDto<Xdxw0038DataRespDto> xdxw0038DataResultDto = dscmsBizXwClientService.xdxw0038(xdxw0038DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0038RespDto.setErorcd(Optional.ofNullable(xdxw0038DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0038RespDto.setErortx(Optional.ofNullable(xdxw0038DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0038DataResultDto.getCode())) {
                xdxw0038DataRespDto = xdxw0038DataResultDto.getData();
                xdxw0038RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0038RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0038DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0038DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, e.getMessage());
            xdxw0038RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0038RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0038RespDto.setDatasq(servsq);

        xdxw0038RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0038.key, DscmsEnum.TRADE_CODE_XDXW0038.value, JSON.toJSONString(xdxw0038RespDto));
        return xdxw0038RespDto;
    }
}
