package cn.com.yusys.yusp.online.client.esb.core.co3200.req;


import java.math.BigDecimal;

/**
 * 请求Service：抵质押物的开户
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:00
 */
public class Record {

    private String shfbhzhh; // 是否本行账号
    private String kehuzhao; // 客户账号
    private String khzhhzxh; // 客户账号子序号
    private String kehmingc; // 客户名称
    private String zhjzhyfs; // 质押方式
    private BigDecimal zhiyajee; // 质押金额

    public String getShfbhzhh() {
        return shfbhzhh;
    }

    public void setShfbhzhh(String shfbhzhh) {
        this.shfbhzhh = shfbhzhh;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getZhjzhyfs() {
        return zhjzhyfs;
    }

    public void setZhjzhyfs(String zhjzhyfs) {
        this.zhjzhyfs = zhjzhyfs;
    }

    public BigDecimal getZhiyajee() {
        return zhiyajee;
    }

    public void setZhiyajee(BigDecimal zhiyajee) {
        this.zhiyajee = zhiyajee;
    }

    @Override
    public String toString() {
        return "Record{" +
                "shfbhzhh='" + shfbhzhh + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", zhjzhyfs='" + zhjzhyfs + '\'' +
                ", zhiyajee=" + zhiyajee +
                '}';
    }
}
