package cn.com.yusys.yusp.online.client.esb.irs.irs19.resp;

/**
 * 响应Service：专业贷款基本信息同步
 * @author lihh
 * @version 1.0             
 */      
public class Irs19RespService {
    private Service service;  
               
    public Service getService() {     
        return service;        
    }                
                     
    public void setService(Service service) {    
        this.service = service;        
    }                       
}                      
