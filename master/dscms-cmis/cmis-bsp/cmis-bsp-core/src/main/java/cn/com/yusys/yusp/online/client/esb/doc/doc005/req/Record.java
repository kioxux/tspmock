package cn.com.yusys.yusp.online.client.esb.doc.doc005.req;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/23 14:58
 * @since 2021/6/23 14:58
 */
public class Record {
    private String attachName;//
    private String authCode;//
    private String bappApprove;//
    private String bappApproveOrg;//
    private String bappIdCard;//
    private String bappIdType;//
    private String bappOrg;//
    private String bappPhoneNumber;//
    private String bappProve;//
    private String bappReader;//
    private String bappReason;//
    private String bappStatus;//
    private String bappType;//
    private String caseId;//
    private String custName;//
    private String fileNum;//
    private String fileTypeNum;//
    private String inRoomTime;//
    private String outRoomContent;//
    private String outRoomStatus;//
    private String outRoomStatusName;//
    private String outRoomTime;//
    private String outbranch;//
    private String pkBappId;//
    private String print;//
    private String publicCode;//
    private String status;//
    private String storePosition;//
    private String tradeDate;//
    private String tradeOrgCode;//
    private String tradeOrgCodeName;//
    private String tradeUserCode;//
    private String userCode;//
    private String userOrg;//
    private String fkBappId;//
    private String fkFileId;//
    private String pkBFId;//
    private String accFileName;//
    private String accFileStatus;//
    private String createTime;//
    private String czybh;//
    private String docid;//
    private String id;//
    private String indexMap;//
    private String jgm;//
    private String jyrq;//
    private String outtime;//
    private String zjlsh;//
    private String approveStatus;
    // 借阅开始日期
    private String bappStartDate;
    // 借阅借阅日期
    private String bappEndDate;

    private String bappSeqNo;//调阅流水号

    public String getAttachName() {
        return attachName;
    }

    public void setAttachName(String attachName) {
        this.attachName = attachName;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getBappApprove() {
        return bappApprove;
    }

    public void setBappApprove(String bappApprove) {
        this.bappApprove = bappApprove;
    }

    public String getBappApproveOrg() {
        return bappApproveOrg;
    }

    public void setBappApproveOrg(String bappApproveOrg) {
        this.bappApproveOrg = bappApproveOrg;
    }

    public String getBappIdCard() {
        return bappIdCard;
    }

    public void setBappIdCard(String bappIdCard) {
        this.bappIdCard = bappIdCard;
    }

    public String getBappIdType() {
        return bappIdType;
    }

    public void setBappIdType(String bappIdType) {
        this.bappIdType = bappIdType;
    }

    public String getBappOrg() {
        return bappOrg;
    }

    public void setBappOrg(String bappOrg) {
        this.bappOrg = bappOrg;
    }

    public String getBappPhoneNumber() {
        return bappPhoneNumber;
    }

    public void setBappPhoneNumber(String bappPhoneNumber) {
        this.bappPhoneNumber = bappPhoneNumber;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public String getBappProve() {
        return bappProve;
    }

    public void setBappProve(String bappProve) {
        this.bappProve = bappProve;
    }

    public String getBappReader() {
        return bappReader;
    }

    public void setBappReader(String bappReader) {
        this.bappReader = bappReader;
    }

    public String getBappReason() {
        return bappReason;
    }

    public void setBappReason(String bappReason) {
        this.bappReason = bappReason;
    }

    public String getBappStatus() {
        return bappStatus;
    }

    public void setBappStatus(String bappStatus) {
        this.bappStatus = bappStatus;
    }

    public String getBappType() {
        return bappType;
    }

    public void setBappType(String bappType) {
        this.bappType = bappType;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getFileNum() {
        return fileNum;
    }

    public void setFileNum(String fileNum) {
        this.fileNum = fileNum;
    }

    public String getFileTypeNum() {
        return fileTypeNum;
    }

    public void setFileTypeNum(String fileTypeNum) {
        this.fileTypeNum = fileTypeNum;
    }

    public String getInRoomTime() {
        return inRoomTime;
    }

    public void setInRoomTime(String inRoomTime) {
        this.inRoomTime = inRoomTime;
    }

    public String getOutRoomContent() {
        return outRoomContent;
    }

    public void setOutRoomContent(String outRoomContent) {
        this.outRoomContent = outRoomContent;
    }

    public String getOutRoomStatus() {
        return outRoomStatus;
    }

    public void setOutRoomStatus(String outRoomStatus) {
        this.outRoomStatus = outRoomStatus;
    }

    public String getOutRoomStatusName() {
        return outRoomStatusName;
    }

    public void setOutRoomStatusName(String outRoomStatusName) {
        this.outRoomStatusName = outRoomStatusName;
    }

    public String getOutRoomTime() {
        return outRoomTime;
    }

    public void setOutRoomTime(String outRoomTime) {
        this.outRoomTime = outRoomTime;
    }

    public String getOutbranch() {
        return outbranch;
    }

    public void setOutbranch(String outbranch) {
        this.outbranch = outbranch;
    }

    public String getPkBappId() {
        return pkBappId;
    }

    public void setPkBappId(String pkBappId) {
        this.pkBappId = pkBappId;
    }

    public String getPrint() {
        return print;
    }

    public void setPrint(String print) {
        this.print = print;
    }

    public String getPublicCode() {
        return publicCode;
    }

    public void setPublicCode(String publicCode) {
        this.publicCode = publicCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStorePosition() {
        return storePosition;
    }

    public void setStorePosition(String storePosition) {
        this.storePosition = storePosition;
    }

    public String getTradeDate() {
        return tradeDate;
    }

    public void setTradeDate(String tradeDate) {
        this.tradeDate = tradeDate;
    }

    public String getTradeOrgCode() {
        return tradeOrgCode;
    }

    public void setTradeOrgCode(String tradeOrgCode) {
        this.tradeOrgCode = tradeOrgCode;
    }

    public String getTradeOrgCodeName() {
        return tradeOrgCodeName;
    }

    public void setTradeOrgCodeName(String tradeOrgCodeName) {
        this.tradeOrgCodeName = tradeOrgCodeName;
    }

    public String getTradeUserCode() {
        return tradeUserCode;
    }

    public void setTradeUserCode(String tradeUserCode) {
        this.tradeUserCode = tradeUserCode;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserOrg() {
        return userOrg;
    }

    public void setUserOrg(String userOrg) {
        this.userOrg = userOrg;
    }

    public String getFkBappId() {
        return fkBappId;
    }

    public void setFkBappId(String fkBappId) {
        this.fkBappId = fkBappId;
    }

    public String getFkFileId() {
        return fkFileId;
    }

    public void setFkFileId(String fkFileId) {
        this.fkFileId = fkFileId;
    }

    public String getPkBFId() {
        return pkBFId;
    }

    public void setPkBFId(String pkBFId) {
        this.pkBFId = pkBFId;
    }

    public String getAccFileName() {
        return accFileName;
    }

    public void setAccFileName(String accFileName) {
        this.accFileName = accFileName;
    }

    public String getAccFileStatus() {
        return accFileStatus;
    }

    public void setAccFileStatus(String accFileStatus) {
        this.accFileStatus = accFileStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getCzybh() {
        return czybh;
    }

    public void setCzybh(String czybh) {
        this.czybh = czybh;
    }

    public String getDocid() {
        return docid;
    }

    public void setDocid(String docid) {
        this.docid = docid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndexMap() {
        return indexMap;
    }

    public void setIndexMap(String indexMap) {
        this.indexMap = indexMap;
    }

    public String getJgm() {
        return jgm;
    }

    public void setJgm(String jgm) {
        this.jgm = jgm;
    }

    public String getJyrq() {
        return jyrq;
    }

    public void setJyrq(String jyrq) {
        this.jyrq = jyrq;
    }

    public String getOuttime() {
        return outtime;
    }

    public void setOuttime(String outtime) {
        this.outtime = outtime;
    }

    public String getZjlsh() {
        return zjlsh;
    }

    public void setZjlsh(String zjlsh) {
        this.zjlsh = zjlsh;
    }

    public String getBappStartDate() {
        return bappStartDate;
    }

    public void setBappStartDate(String bappStartDate) {
        this.bappStartDate = bappStartDate;
    }

    public String getBappEndDate() {
        return bappEndDate;
    }

    public void setBappEndDate(String bappEndDate) {
        this.bappEndDate = bappEndDate;
    }

    public String getBappSeqNo() {
        return bappSeqNo;
    }

    public void setBappSeqNo(String bappSeqNo) {
        this.bappSeqNo = bappSeqNo;
    }

    @Override
    public String toString() {
        return "Record{" +
                "attachName='" + attachName + '\'' +
                ", authCode='" + authCode + '\'' +
                ", bappApprove='" + bappApprove + '\'' +
                ", bappApproveOrg='" + bappApproveOrg + '\'' +
                ", bappIdCard='" + bappIdCard + '\'' +
                ", bappIdType='" + bappIdType + '\'' +
                ", bappOrg='" + bappOrg + '\'' +
                ", bappPhoneNumber='" + bappPhoneNumber + '\'' +
                ", bappProve='" + bappProve + '\'' +
                ", bappReader='" + bappReader + '\'' +
                ", bappReason='" + bappReason + '\'' +
                ", bappStatus='" + bappStatus + '\'' +
                ", bappType='" + bappType + '\'' +
                ", caseId='" + caseId + '\'' +
                ", custName='" + custName + '\'' +
                ", fileNum='" + fileNum + '\'' +
                ", fileTypeNum='" + fileTypeNum + '\'' +
                ", inRoomTime='" + inRoomTime + '\'' +
                ", outRoomContent='" + outRoomContent + '\'' +
                ", outRoomStatus='" + outRoomStatus + '\'' +
                ", outRoomStatusName='" + outRoomStatusName + '\'' +
                ", outRoomTime='" + outRoomTime + '\'' +
                ", outbranch='" + outbranch + '\'' +
                ", pkBappId='" + pkBappId + '\'' +
                ", print='" + print + '\'' +
                ", publicCode='" + publicCode + '\'' +
                ", status='" + status + '\'' +
                ", storePosition='" + storePosition + '\'' +
                ", tradeDate='" + tradeDate + '\'' +
                ", tradeOrgCode='" + tradeOrgCode + '\'' +
                ", tradeOrgCodeName='" + tradeOrgCodeName + '\'' +
                ", tradeUserCode='" + tradeUserCode + '\'' +
                ", userCode='" + userCode + '\'' +
                ", userOrg='" + userOrg + '\'' +
                ", fkBappId='" + fkBappId + '\'' +
                ", fkFileId='" + fkFileId + '\'' +
                ", pkBFId='" + pkBFId + '\'' +
                ", accFileName='" + accFileName + '\'' +
                ", accFileStatus='" + accFileStatus + '\'' +
                ", createTime='" + createTime + '\'' +
                ", czybh='" + czybh + '\'' +
                ", docid='" + docid + '\'' +
                ", id='" + id + '\'' +
                ", indexMap='" + indexMap + '\'' +
                ", jgm='" + jgm + '\'' +
                ", jyrq='" + jyrq + '\'' +
                ", outtime='" + outtime + '\'' +
                ", zjlsh='" + zjlsh + '\'' +
                ", approveStatus='" + approveStatus + '\'' +
                ", bappStartDate='" + bappStartDate + '\'' +
                ", bappEndDate='" + bappEndDate + '\'' +
                ", bappSeqNo='" + bappSeqNo + '\'' +
                '}';
    }
}
