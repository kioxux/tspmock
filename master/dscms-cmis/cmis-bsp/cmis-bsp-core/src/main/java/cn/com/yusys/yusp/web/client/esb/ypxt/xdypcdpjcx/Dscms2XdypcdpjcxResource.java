package cn.com.yusys.yusp.web.client.esb.ypxt.xdypcdpjcx;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypcdpjcx.req.XdypcdpjcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypcdpjcx.resp.XdypcdpjcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypcdpjcx.req.XdypcdpjcxReqService;
import cn.com.yusys.yusp.online.client.esb.ypxt.xdypcdpjcx.resp.XdypcdpjcxRespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 封装调用押品系统的接口
 */
@Api(tags = "BSP封装调用押品系统的接口(xdypcdpjcx)")
@RestController
@RequestMapping("/api/dscms2ypxt")
public class Dscms2XdypcdpjcxResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2XdypcdpjcxResource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：xdypcdpjcx
     * 交易描述：查询存单票据信息
     *
     * @param xdypcdpjcxReqDto
     * @return
     * @throws Exception
     */

    @PostMapping("/xdypcdpjcx")
    protected @ResponseBody
    ResultDto<XdypcdpjcxRespDto> xdypcdpjcx(@Validated @RequestBody XdypcdpjcxReqDto xdypcdpjcxReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPCDPJCX.key, EsbEnum.TRADE_CODE_XDYPCDPJCX.value, JSON.toJSONString(xdypcdpjcxReqDto));
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypcdpjcx.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypcdpjcx.req.Service();
        cn.com.yusys.yusp.online.client.esb.ypxt.xdypcdpjcx.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ypxt.xdypcdpjcx.resp.Service();

        XdypcdpjcxReqService xdypcdpjcxReqService = new XdypcdpjcxReqService();
        XdypcdpjcxRespService xdypcdpjcxRespService = new XdypcdpjcxRespService();
        XdypcdpjcxRespDto xdypcdpjcxRespDto = new XdypcdpjcxRespDto();
        ResultDto<XdypcdpjcxRespDto> xdypcdpjcxResultDto = new ResultDto<XdypcdpjcxRespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将xdypcdpjcxReqDto转换成reqService
            BeanUtils.copyProperties(xdypcdpjcxReqDto, reqService);
            reqService.setPrcscd(EsbEnum.TRADE_CODE_XDYPCDPJCX.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_YPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_YPXT.key);//    部门号
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间
            xdypcdpjcxReqService.setService(reqService);
            // 将xdypcdpjcxReqService转换成xdypcdpjcxReqServiceMap
            Map xdypcdpjcxReqServiceMap = beanMapUtil.beanToMap(xdypcdpjcxReqService);
            context.put("tradeDataMap", xdypcdpjcxReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPCDPJCX.key, EsbEnum.TRADE_CODE_XDYPCDPJCX.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_XDYPCDPJCX.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPCDPJCX.key, EsbEnum.TRADE_CODE_XDYPCDPJCX.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            xdypcdpjcxRespService = beanMapUtil.mapToBean(tradeDataMap, XdypcdpjcxRespService.class, XdypcdpjcxRespService.class);
            respService = xdypcdpjcxRespService.getService();

            xdypcdpjcxResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            xdypcdpjcxResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成XdypcdpjcxRespDto
                BeanUtils.copyProperties(respService, xdypcdpjcxRespDto);
                xdypcdpjcxResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                xdypcdpjcxResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                xdypcdpjcxResultDto.setCode(EpbEnum.EPB099999.key);
                xdypcdpjcxResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPCDPJCX.key, EsbEnum.TRADE_CODE_XDYPCDPJCX.value, e.getMessage());
            xdypcdpjcxResultDto.setCode(EpbEnum.EPB099999.key);//9999
            xdypcdpjcxResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        xdypcdpjcxResultDto.setData(xdypcdpjcxRespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_XDYPCDPJCX.key, EsbEnum.TRADE_CODE_XDYPCDPJCX.value, JSON.toJSONString(xdypcdpjcxResultDto));
        return xdypcdpjcxResultDto;
    }
}
