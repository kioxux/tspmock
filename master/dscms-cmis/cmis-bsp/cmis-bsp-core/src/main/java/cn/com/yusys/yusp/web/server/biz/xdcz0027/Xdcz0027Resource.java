package cn.com.yusys.yusp.web.server.biz.xdcz0027;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0027.req.Xdcz0027ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0027.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0027.resp.Xdcz0027RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0027.req.Xdcz0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0027.resp.Xdcz0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:审批失败支用申请( 登记支用失败)
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0027:审批失败支用申请( 登记支用失败)")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0027Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0027
     * 交易描述：审批失败支用申请( 登记支用失败)
     *
     * @param xdcz0027ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("审批失败支用申请( 登记支用失败)")
    @PostMapping("/xdcz0027")
   //@Idempotent({"xdcz0027", "#xdcz0027ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0027RespDto xdcz0027(@Validated @RequestBody Xdcz0027ReqDto xdcz0027ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value, JSON.toJSONString(xdcz0027ReqDto));
        Xdcz0027DataReqDto xdcz0027DataReqDto = new Xdcz0027DataReqDto();// 请求Data： 审批失败支用申请( 登记支用失败)
        Xdcz0027DataRespDto xdcz0027DataRespDto = new Xdcz0027DataRespDto();// 响应Data：审批失败支用申请( 登记支用失败)

        cn.com.yusys.yusp.dto.server.biz.xdcz0027.req.Data reqData = null; // 请求Data：审批失败支用申请( 登记支用失败)
        Data respData = new Data();// 响应Data：审批失败支用申请( 登记支用失败)

        Xdcz0027RespDto xdcz0027RespDto = new Xdcz0027RespDto();
        try {
            // 从 xdcz0027ReqDto获取 reqData
            reqData = xdcz0027ReqDto.getData();
            // 将 reqData 拷贝到xdcz0027DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0027DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value, JSON.toJSONString(xdcz0027DataReqDto));
            ResultDto<Xdcz0027DataRespDto> xdcz0027DataResultDto = dscmsBizCzClientService.xdcz0027(xdcz0027DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value, JSON.toJSONString(xdcz0027DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0027RespDto.setErorcd(Optional.ofNullable(xdcz0027DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0027RespDto.setErortx(Optional.ofNullable(xdcz0027DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0027DataResultDto.getCode())) {
                xdcz0027DataRespDto = xdcz0027DataResultDto.getData();
                xdcz0027RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0027RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0027DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0027DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value, e.getMessage());
            xdcz0027RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0027RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0027RespDto.setDatasq(servsq);

        xdcz0027RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0027.key, DscmsEnum.TRADE_CODE_XDCZ0027.value, JSON.toJSONString(xdcz0027RespDto));
        return xdcz0027RespDto;
    }
}
