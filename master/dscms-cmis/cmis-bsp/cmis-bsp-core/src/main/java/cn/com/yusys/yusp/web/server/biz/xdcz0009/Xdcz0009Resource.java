package cn.com.yusys.yusp.web.server.biz.xdcz0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdcz0009.req.Xdcz0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdcz0009.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdcz0009.resp.Xdcz0009RespDto;
import cn.com.yusys.yusp.dto.server.xdcz0009.req.Xdcz0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdcz0009.resp.Xdcz0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizCzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:代开信用证
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDCZ0009:代开信用证")
@RestController
@RequestMapping("/api/dscms")
public class Xdcz0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdcz0009Resource.class);

    @Autowired
    private DscmsBizCzClientService dscmsBizCzClientService;

    /**
     * 交易码：xdcz0009
     * 交易描述：代开信用证
     *
     * @param xdcz0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("代开信用证")
    @PostMapping("/xdcz0009")
//   @Idempotent({"xdcz0009", "#xdcz0009ReqDto.datasq"})
    protected @ResponseBody
    Xdcz0009RespDto xdcz0009(@Validated @RequestBody Xdcz0009ReqDto xdcz0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, JSON.toJSONString(xdcz0009ReqDto));
        Xdcz0009DataReqDto xdcz0009DataReqDto = new Xdcz0009DataReqDto();// 请求Data： 代开信用证
        Xdcz0009DataRespDto xdcz0009DataRespDto = new Xdcz0009DataRespDto();// 响应Data：代开信用证

        cn.com.yusys.yusp.dto.server.biz.xdcz0009.req.Data reqData = null; // 请求Data：代开信用证
        Data respData = new Data();// 响应Data：代开信用证

        Xdcz0009RespDto xdcz0009RespDto = new Xdcz0009RespDto();
        try {
            // 从 xdcz0009ReqDto获取 reqData
            reqData = xdcz0009ReqDto.getData();
            // 将 reqData 拷贝到xdcz0009DataReqDto
            BeanUtils.copyProperties(reqData, xdcz0009DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, JSON.toJSONString(xdcz0009DataReqDto));
            ResultDto<Xdcz0009DataRespDto> xdcz0009DataResultDto = dscmsBizCzClientService.xdcz0009(xdcz0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, JSON.toJSONString(xdcz0009DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdcz0009RespDto.setErorcd(Optional.ofNullable(xdcz0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdcz0009RespDto.setErortx(Optional.ofNullable(xdcz0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdcz0009DataResultDto.getCode())) {
                xdcz0009DataRespDto = xdcz0009DataResultDto.getData();
                xdcz0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdcz0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdcz0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdcz0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, e.getMessage());
            xdcz0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdcz0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdcz0009RespDto.setDatasq(servsq);

        xdcz0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDCZ0009.key, DscmsEnum.TRADE_CODE_XDCZ0009.value, JSON.toJSONString(xdcz0009RespDto));
        return xdcz0009RespDto;
    }
}
