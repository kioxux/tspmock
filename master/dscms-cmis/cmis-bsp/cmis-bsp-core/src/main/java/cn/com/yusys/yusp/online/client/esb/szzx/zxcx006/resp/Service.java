package cn.com.yusys.yusp.online.client.esb.szzx.zxcx006.resp;

/**
 * 响应Service：信贷查询地方征信接口
 * @author muxiang
 * @version 1.0
 * @since 2021/4/15 20:27
 */
public class Service {

    private String erorcd; // 响应码,是,
    private String erortx; // 响应信息,是,state为new时，值为taskId;为old时，为报告URL
    private String servsq; // 渠道流水,是,
    private String taskId; // 任务id,否,state为new时，必输，作为查询时的taskId传入；state为old时，非必输

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public String toString() {
        return "Zxcx006RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", taskId='" + taskId + '\'' +
                '}';
    }
}
