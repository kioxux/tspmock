package cn.com.yusys.yusp.online.client.esb.core.ln3126.resp;

import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/31 15:42
 * @since 2021/5/31 15:42
 */
public class Listdkfycx {

    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3126.resp.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "Listdkfycx{" +
                "record=" + record +
                '}';
    }
}
