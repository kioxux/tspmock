package cn.com.yusys.yusp.online.client.esb.core.ln3176.req;

import java.math.BigDecimal;

/**
 * 请求Service：本交易用户贷款多个还款账户进行还款
 *
 * @author leehuang
 * @version 1.0
 */
public class Record {
    private String huankzhh;//还款账号
    private String hkzhhzxh;//还款账号子序号
    private String huobdhao;//货币代号
    private BigDecimal huankjee;//还款金额

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    @Override
    public String toString() {
        return "Record{" +
                "huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", huankjee=" + huankjee +
                '}';
    }
}
