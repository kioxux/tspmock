package cn.com.yusys.yusp.online.client.esb.yxxt.yxljcx.resp;

/**
 * 响应Service：影像图像路径查询
 *
 * @author code-generator
 * @version 1.0
 */
public class YxljcxRespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
