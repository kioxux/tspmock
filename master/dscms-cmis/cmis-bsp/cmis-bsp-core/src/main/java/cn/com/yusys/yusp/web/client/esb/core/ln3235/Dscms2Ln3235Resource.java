package cn.com.yusys.yusp.web.client.esb.core.ln3235;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Ln3235ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Ln3235RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3235.req.Ln3235ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.Ln3235RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3235)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3235Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3235Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3235
     * 交易描述：还款方式转换
     *
     * @param ln3235ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3235:还款方式转换")
    @PostMapping("/ln3235")
    protected @ResponseBody
    ResultDto<Ln3235RespDto> ln3235(@Validated @RequestBody Ln3235ReqDto ln3235ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3235.key, EsbEnum.TRADE_CODE_LN3235.value, JSON.toJSONString(ln3235ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3235.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3235.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.Service();
        Ln3235ReqService ln3235ReqService = new Ln3235ReqService();
        Ln3235RespService ln3235RespService = new Ln3235RespService();
        Ln3235RespDto ln3235RespDto = new Ln3235RespDto();
        ResultDto<Ln3235RespDto> ln3235ResultDto = new ResultDto<Ln3235RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3235ReqDto转换成reqService
            BeanUtils.copyProperties(ln3235ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3235.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3235ReqService.setService(reqService);
            // 将ln3235ReqService转换成ln3235ReqServiceMap
            Map ln3235ReqServiceMap = beanMapUtil.beanToMap(ln3235ReqService);
            context.put("tradeDataMap", ln3235ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3235.key, EsbEnum.TRADE_CODE_LN3235.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3235.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3235.key, EsbEnum.TRADE_CODE_LN3235.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3235RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3235RespService.class, Ln3235RespService.class);
            respService = ln3235RespService.getService();

            ln3235ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3235ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3235RespDto
                BeanUtils.copyProperties(respService, ln3235RespDto);
                // 循环相关的判断开始
                cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh.Lsdkhbjh lsdkhbjh =
                        Optional.ofNullable(respService.getLsdkhbjh()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh.Lsdkhbjh());//贷款还本计划[LIST]
                cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd.Lsdkbjfd lsdkbjfd =
                        Optional.ofNullable(respService.getLsdkbjfd()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd.Lsdkbjfd());//本金分段登记[LIST]
                respService.setLsdkhbjh(lsdkhbjh);
                respService.setLsdkbjfd(lsdkbjfd);
                if (CollectionUtils.nonEmpty(respService.getLsdkhbjh().getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh.Record> lsdkhbjhRecordList = Optional.ofNullable(respService.getLsdkhbjh().getRecord())
                            .orElseGet(()
                                    -> new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkhbjh.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkhbjh> lsdkhbjhList = lsdkhbjhRecordList.parallelStream().map(lsdkhbjhRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkhbjh lsdkhbjhDto = new cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkhbjh();
                        BeanUtils.copyProperties(lsdkhbjhRecord, lsdkhbjhDto);
                        return lsdkhbjhDto;
                    }).collect(Collectors.toList());
                    ln3235RespDto.setLsdkhbjh(lsdkhbjhList);
                }
                if (CollectionUtils.nonEmpty(respService.getLsdkbjfd().getRecord())) {
                    List<cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd.Record> lsdkbjfdRecordList = Optional.ofNullable(respService.getLsdkbjfd().getRecord())
                            .orElseGet(()
                                    -> new ArrayList<cn.com.yusys.yusp.online.client.esb.core.ln3235.resp.lsdkbjfd.Record>());
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkbjfd> lsdkbjfdList = lsdkbjfdRecordList.parallelStream().map(lsdkbjfdRecord -> {
                        cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkbjfd lsdkbjfdDto = new cn.com.yusys.yusp.dto.client.esb.core.ln3235.Lsdkbjfd();
                        BeanUtils.copyProperties(lsdkbjfdRecord, lsdkbjfdDto);
                        return lsdkbjfdDto;
                    }).collect(Collectors.toList());
                    ln3235RespDto.setLsdkbjfd(lsdkbjfdList);
                }
                ln3235ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3235ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3235ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3235ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3235.key, EsbEnum.TRADE_CODE_LN3235.value, e.getMessage());
            ln3235ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3235ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3235ResultDto.setData(ln3235RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3235.key, EsbEnum.TRADE_CODE_LN3235.value, JSON.toJSONString(ln3235ResultDto));
        return ln3235ResultDto;
    }

}
