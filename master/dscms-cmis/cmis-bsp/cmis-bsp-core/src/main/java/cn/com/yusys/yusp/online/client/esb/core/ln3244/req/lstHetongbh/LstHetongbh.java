package cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh;

import java.util.List;

/**
 * 请求Service：贷款客户经理批量移交
 */
public class LstHetongbh {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.Record> record;

    public List<Record> getRecord() {
        return record;
    }

    public void setRecord(List<Record> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstHetongbh{" +
                "record=" + record +
                '}';
    }
}
