package cn.com.yusys.yusp.web.server.biz.xdht0029;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdht0029.req.Xdht0029ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdht0029.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdht0029.resp.Xdht0029RespDto;
import cn.com.yusys.yusp.dto.server.xdht0029.req.Xdht0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdht0029.resp.Xdht0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizHtClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户非小微业务经营性贷款总金额查询
 * @author code-generator
 * @version 1.0
 */
@Api(tags = "XDHT0029:客户非小微业务经营性贷款总金额查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdht0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdht0029Resource.class);

    @Resource
    private DscmsBizHtClientService dscmsBizHtClientService;
    /**
     * 交易码：xdht0029
     * 交易描述：客户非小微业务经营性贷款总金额查询
     *
     * @param xdht0029ReqDto
     * @throws Exception
     * @return
     */
    @ApiOperation("客户非小微业务经营性贷款总金额查询")
    @PostMapping("/xdht0029")
    //@Idempotent({"xdcaht0029", "#xdht0029ReqDto.datasq"})
    protected @ResponseBody
    Xdht0029RespDto xdht0029(@Validated @RequestBody Xdht0029ReqDto xdht0029ReqDto ) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, JSON.toJSONString(xdht0029ReqDto));
        Xdht0029DataReqDto  xdht0029DataReqDto = new Xdht0029DataReqDto();// 请求Data： 客户非小微业务经营性贷款总金额查询
        Xdht0029DataRespDto  xdht0029DataRespDto =  new Xdht0029DataRespDto();// 响应Data：客户非小微业务经营性贷款总金额查询
        Xdht0029RespDto xdht0029RespDto = new Xdht0029RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdht0029.req.Data reqData = null; // 请求Data：客户非小微业务经营性贷款总金额查询
        cn.com.yusys.yusp.dto.server.biz.xdht0029.resp.Data respData = new Data();// 响应Data：客户非小微业务经营性贷款总金额查询
        try {
            // 从 xdht0029ReqDto获取 reqData
            reqData = xdht0029ReqDto.getData();
            // 将 reqData 拷贝到xdht0029DataReqDto
            BeanUtils.copyProperties(reqData, xdht0029DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, JSON.toJSONString(xdht0029DataReqDto));
            ResultDto<Xdht0029DataRespDto> xdht0029DataResultDto= dscmsBizHtClientService.xdht0029(xdht0029DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, JSON.toJSONString(xdht0029DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdht0029RespDto.setErorcd(Optional.ofNullable(xdht0029DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdht0029RespDto.setErortx(Optional.ofNullable(xdht0029DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key,xdht0029DataResultDto.getCode())) {
                xdht0029DataRespDto = xdht0029DataResultDto.getData();
                xdht0029RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdht0029RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdht0029DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdht0029DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, e.getMessage());
            xdht0029RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdht0029RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdht0029RespDto.setDatasq(servsq);
        xdht0029RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDHT0029.key, DscmsEnum.TRADE_CODE_XDHT0029.value, JSON.toJSONString(xdht0029RespDto));
        return xdht0029RespDto;
    }
}
