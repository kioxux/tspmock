package cn.com.yusys.yusp.online.client.esb.rircp.fbxw08.req;

/**
 * 请求Service：授信审批作废接口
 * @author muxiang
 * @version 1.0
 * @since 2021/4/16 16:36
 */
public class Fbxw08ReqService {

    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}
