package cn.com.yusys.yusp.online.client.esb.gaps.mfzjcr.req;

/**
 * 请求Service：买方资金存入
 * @author code-generator
 * @version 1.0             
 */      
public class MfzjcrReqService {
	   private Service service;  
	               
	    public Service getService() {     
	        return service;        
	    }                
	                     
	    public void setService(Service service) {    
	        this.service = service;        
	    }                       
}                      
