package cn.com.yusys.yusp.online.client.esb.core.ln3026.req.lstdkstzf;

import java.math.BigDecimal;

/**
 * 贷款受托支付
 *
 * @author lihh
 * @version 1.0
 */
public class Record {
    private String ruzjigou;//入账机构
    private String bhchzibz;//本行出资标志
    private String lhdkleix;//联合贷款类型
    private String chzfkehh;//出资方客户号
    private String chzfkhmc;//出资方客户名称
    private String chzfhhao;//出资方行号
    private String czfhming;//出资方行名
    private String chuzfzhh;//出资方账号
    private String czfzhzxh;//出资方账号子序号
    private String chzfzhmc;//出资方账户名称
    private BigDecimal chuzbili;//出资比例
    private BigDecimal chuzjine;//出资金额
    private String shoukzhh;//收款账号
    private String skzhhzxh;//收款账号子序号
    private String skzhhmch;//收款账户名称
    private String bjghrzzh;//本金归还入账账号
    private String bjghrzxh;//本金归还入账账号子序号
    private String lxghrzzh;//利息归还入账账号
    private String lxghrzxh;//利息归还入账账号子序号
    private String lilvleix;//利率类型
    private String zclilvbh;//正常利率编号
    private String nyuelilv;//年/月利率标识
    private BigDecimal zhchlilv;//正常利率
    private BigDecimal hetongll;//合同利率
    private String lilvtzfs;//利率调整方式
    private String lilvtzzq;//利率调整周期
    private String lilvfdfs;//利率浮动方式
    private BigDecimal lilvfdzh;//利率浮动值
    private String yqllcklx;//逾期利率参考类型
    private String yuqillbh;//逾期利率编号
    private String yuqinyll;//逾期年月利率
    private BigDecimal yuqililv;//逾期利率
    private String yuqitzfs;//逾期利率调整方式
    private String qudaohao;//发布系统/渠道
    private BigDecimal stzfjine;//受托金额
    private String zjlyzhao;//资金来源账号
    private String zjlyzzxh;//资金来源账号子序号
    private String zjlyzhmc;//资金来源账号名称
    private String zjzrzhao;//资金转入账号
    private String zjzrzzxh;//资金转入账号子序号
    private String zjzrzhmc;//资金转入账号名称
    private String dfzhhzhl;//对方账号种类
    private String dfzhhkhh;//对方账号开户行
    private String dfzhkhhm;//对方账号开户行名
    private String dfzhangh;//对方账号
    private String dfzhhzxh;//对方账号子序号
    private String dfzhhmch;//对方账号名称
    private String stzffshi;//受托支付方式
    private String stzfriqi;//受托支付日期
    private String stzfclzt;//受托支付处理状态
    private String beizhuxx;//备注
    private BigDecimal shtzfbli;//受托比例

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getBhchzibz() {
        return bhchzibz;
    }

    public void setBhchzibz(String bhchzibz) {
        this.bhchzibz = bhchzibz;
    }

    public String getLhdkleix() {
        return lhdkleix;
    }

    public void setLhdkleix(String lhdkleix) {
        this.lhdkleix = lhdkleix;
    }

    public String getChzfkehh() {
        return chzfkehh;
    }

    public void setChzfkehh(String chzfkehh) {
        this.chzfkehh = chzfkehh;
    }

    public String getChzfkhmc() {
        return chzfkhmc;
    }

    public void setChzfkhmc(String chzfkhmc) {
        this.chzfkhmc = chzfkhmc;
    }

    public String getChzfhhao() {
        return chzfhhao;
    }

    public void setChzfhhao(String chzfhhao) {
        this.chzfhhao = chzfhhao;
    }

    public String getCzfhming() {
        return czfhming;
    }

    public void setCzfhming(String czfhming) {
        this.czfhming = czfhming;
    }

    public String getChuzfzhh() {
        return chuzfzhh;
    }

    public void setChuzfzhh(String chuzfzhh) {
        this.chuzfzhh = chuzfzhh;
    }

    public String getCzfzhzxh() {
        return czfzhzxh;
    }

    public void setCzfzhzxh(String czfzhzxh) {
        this.czfzhzxh = czfzhzxh;
    }

    public String getChzfzhmc() {
        return chzfzhmc;
    }

    public void setChzfzhmc(String chzfzhmc) {
        this.chzfzhmc = chzfzhmc;
    }

    public BigDecimal getChuzbili() {
        return chuzbili;
    }

    public void setChuzbili(BigDecimal chuzbili) {
        this.chuzbili = chuzbili;
    }

    public BigDecimal getChuzjine() {
        return chuzjine;
    }

    public void setChuzjine(BigDecimal chuzjine) {
        this.chuzjine = chuzjine;
    }

    public String getShoukzhh() {
        return shoukzhh;
    }

    public void setShoukzhh(String shoukzhh) {
        this.shoukzhh = shoukzhh;
    }

    public String getSkzhhzxh() {
        return skzhhzxh;
    }

    public void setSkzhhzxh(String skzhhzxh) {
        this.skzhhzxh = skzhhzxh;
    }

    public String getSkzhhmch() {
        return skzhhmch;
    }

    public void setSkzhhmch(String skzhhmch) {
        this.skzhhmch = skzhhmch;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getQudaohao() {
        return qudaohao;
    }

    public void setQudaohao(String qudaohao) {
        this.qudaohao = qudaohao;
    }

    public BigDecimal getStzfjine() {
        return stzfjine;
    }

    public void setStzfjine(BigDecimal stzfjine) {
        this.stzfjine = stzfjine;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZjlyzhmc() {
        return zjlyzhmc;
    }

    public void setZjlyzhmc(String zjlyzhmc) {
        this.zjlyzhmc = zjlyzhmc;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public String getZjzrzhmc() {
        return zjzrzhmc;
    }

    public void setZjzrzhmc(String zjzrzhmc) {
        this.zjzrzhmc = zjzrzhmc;
    }

    public String getDfzhhzhl() {
        return dfzhhzhl;
    }

    public void setDfzhhzhl(String dfzhhzhl) {
        this.dfzhhzhl = dfzhhzhl;
    }

    public String getDfzhhkhh() {
        return dfzhhkhh;
    }

    public void setDfzhhkhh(String dfzhhkhh) {
        this.dfzhhkhh = dfzhhkhh;
    }

    public String getDfzhkhhm() {
        return dfzhkhhm;
    }

    public void setDfzhkhhm(String dfzhkhhm) {
        this.dfzhkhhm = dfzhkhhm;
    }

    public String getDfzhangh() {
        return dfzhangh;
    }

    public void setDfzhangh(String dfzhangh) {
        this.dfzhangh = dfzhangh;
    }

    public String getDfzhhzxh() {
        return dfzhhzxh;
    }

    public void setDfzhhzxh(String dfzhhzxh) {
        this.dfzhhzxh = dfzhhzxh;
    }

    public String getDfzhhmch() {
        return dfzhhmch;
    }

    public void setDfzhhmch(String dfzhhmch) {
        this.dfzhhmch = dfzhhmch;
    }

    public String getStzffshi() {
        return stzffshi;
    }

    public void setStzffshi(String stzffshi) {
        this.stzffshi = stzffshi;
    }

    public String getStzfriqi() {
        return stzfriqi;
    }

    public void setStzfriqi(String stzfriqi) {
        this.stzfriqi = stzfriqi;
    }

    public String getStzfclzt() {
        return stzfclzt;
    }

    public void setStzfclzt(String stzfclzt) {
        this.stzfclzt = stzfclzt;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public BigDecimal getShtzfbli() {
        return shtzfbli;
    }

    public void setShtzfbli(BigDecimal shtzfbli) {
        this.shtzfbli = shtzfbli;
    }

    @Override
    public String toString() {
        return "Service{" +
                "ruzjigou='" + ruzjigou + '\'' +
                "bhchzibz='" + bhchzibz + '\'' +
                "lhdkleix='" + lhdkleix + '\'' +
                "chzfkehh='" + chzfkehh + '\'' +
                "chzfkhmc='" + chzfkhmc + '\'' +
                "chzfhhao='" + chzfhhao + '\'' +
                "czfhming='" + czfhming + '\'' +
                "chuzfzhh='" + chuzfzhh + '\'' +
                "czfzhzxh='" + czfzhzxh + '\'' +
                "chzfzhmc='" + chzfzhmc + '\'' +
                "chuzbili='" + chuzbili + '\'' +
                "chuzjine='" + chuzjine + '\'' +
                "shoukzhh='" + shoukzhh + '\'' +
                "skzhhzxh='" + skzhhzxh + '\'' +
                "skzhhmch='" + skzhhmch + '\'' +
                "bjghrzzh='" + bjghrzzh + '\'' +
                "bjghrzxh='" + bjghrzxh + '\'' +
                "lxghrzzh='" + lxghrzzh + '\'' +
                "lxghrzxh='" + lxghrzxh + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                "nyuelilv='" + nyuelilv + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "hetongll='" + hetongll + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "yqllcklx='" + yqllcklx + '\'' +
                "yuqillbh='" + yuqillbh + '\'' +
                "yuqinyll='" + yuqinyll + '\'' +
                "yuqililv='" + yuqililv + '\'' +
                "yuqitzfs='" + yuqitzfs + '\'' +
                "qudaohao='" + qudaohao + '\'' +
                "stzfjine='" + stzfjine + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "zjlyzhmc='" + zjlyzhmc + '\'' +
                "zjzrzhao='" + zjzrzhao + '\'' +
                "zjzrzzxh='" + zjzrzzxh + '\'' +
                "zjzrzhmc='" + zjzrzhmc + '\'' +
                "dfzhhzhl='" + dfzhhzhl + '\'' +
                "dfzhhkhh='" + dfzhhkhh + '\'' +
                "dfzhkhhm='" + dfzhkhhm + '\'' +
                "dfzhangh='" + dfzhangh + '\'' +
                "dfzhhzxh='" + dfzhhzxh + '\'' +
                "dfzhhmch='" + dfzhhmch + '\'' +
                "stzffshi='" + stzffshi + '\'' +
                "stzfriqi='" + stzfriqi + '\'' +
                "stzfclzt='" + stzfclzt + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "shtzfbli='" + shtzfbli + '\'' +
                '}';
    }
}
