package cn.com.yusys.yusp.web.client.http.image;

import cn.com.yusys.yusp.commons.exception.BizException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.dto.client.http.image.callimage.CallImageReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.util.CmisBizImageUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * BSP封装调用影像系统的接口
 */
@Api(tags = "BSP封装调用影像系统的接口处理类")
@RestController
@RequestMapping("/api/dscms2image")
public class Dscms2ImageResource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2ImageResource.class);
    @Autowired
    private RestTemplate restTemplate;

    @Value("${application.image.username}")
    String imageCommonUserName;

    @Value("${application.image.password}")
    String imageCommonUserPass;

    @Value("${application.image.url}")
    String imageSysUrl;

    /**
     * 图像数量查询接口
     *
     * @param imageDataReqDto
     * @return
     * @throws Exception
     */
    @Deprecated
    @ApiOperation("imageImageDataSize:图像数量查询接口")
    @PostMapping("/imageImageDataSize")
    protected @ResponseBody
    ResultDto<Map<String, Integer>> imageImageDataSize(@Validated @RequestBody ImageDataReqDto imageDataReqDto) {
        ResultDto<Map<String, Integer>> resultDto = new ResultDto<>();
        Map<String, Integer> urlMap = new HashMap<String, Integer>();//返回参数
        imageDataReqDto.setSlice(false);
        imageDataReqDto.setStampCheck(false);
        try {
            String docId = imageDataReqDto.getDocId();
            String outSystemCode = imageDataReqDto.getOutSystemCode();
            Map indexMap = imageDataReqDto.getIndexMap();
            JSONObject paraJson = new JSONObject();
            if(CollectionUtils.nonEmpty(indexMap)){
                JSONObject index = new JSONObject(indexMap);
                paraJson.put("index",index.toJSONString());
            }
            paraJson.put("docId",docId);
            paraJson.put("outCode",outSystemCode);
            paraJson.put("isSlice","false");
            paraJson.put("isStampCheck","false");
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.key, EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.value, JSON.toJSONString(imageDataReqDto));
            String json = paraJson.toJSONString();//"{ \"index\":\"+index.toJSONString()+\",\"docId\":\"" + docId + "\",\"outCode\":\"" + outSystemCode + "\",\"isSlice\":false,\"isStampCheck\":false}";
            logger.info("图像数量查询接口[{}]", imageSysUrl + "/ecm-imagevw/executeQueryByPrimaryKeys" + json);


            String responseEntity = restTemplate.getForObject(imageSysUrl + "/ecm-imagevw/executeQueryByPrimaryKeys?jsonPara=" + json, String.class, json);
            JSONObject resJson = JSONObject.parseObject(responseEntity);
            JSONArray array = (JSONArray) resJson.get("details");
            if (array == null) {
                resultDto.setData(urlMap);
                return resultDto;
            }
            for (int i = 0; i < array.size(); i++) {
                JSONObject json2 = new JSONObject();
                JSONArray array2 = new JSONArray();
                json2 = (JSONObject) array.get(i);
                array2 = (JSONArray) json2.get("typeDetail");
                String typeCode = json2.getString("typeCode");
                urlMap.put(typeCode, array2.size());
            }
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.key, EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.value, responseEntity);
            // String responseStr = responseEntity.getBody();
            resultDto.setData(urlMap);
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.key, EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.value, e.getMessage());
            resultDto.setCode(EpbEnum.EPB099999.key);//9999
            resultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        return resultDto;
    }


    /**
     * 通过用户名和密码获取token
     *
     * @param imageTokenReqDto
     * @return
     * @throws Exception
     */
    @Deprecated
    @ApiOperation("imagetoken:通过用户名和密码获取token")
    @PostMapping("/imagetoken")
    protected @ResponseBody
    ResultDto<ImageTokenRespDto> imagetoken(@Validated @RequestBody ImageTokenReqDto imageTokenReqDto) throws Exception {
        ResultDto<ImageTokenRespDto> imageTokenResultDto = new ResultDto<ImageTokenRespDto>();
        ImageTokenRespDto imageTokenRespDto = new ImageTokenRespDto();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(null, headers);
            String url = imageSysUrl + "/ecm-management/login?username=" + imageTokenReqDto.getUsername() + "&password=" + imageTokenReqDto.getPassword();
            logger.info("调用影像平台的URL为[{}]", url);
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value, JSON.toJSONString(imageTokenReqDto));
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value, responseEntity);
            String responseStr = responseEntity.getBody();
            imageTokenRespDto = (ImageTokenRespDto) JSON.parseObject(responseStr, ImageTokenRespDto.class);
            //  将ImageTokenRespDto装到ResultDto<ImageTokenRespDto>
            imageTokenResultDto.setCode(Optional.ofNullable(imageTokenRespDto.getResultCode()).orElse(SuccessEnum.SUCCESS.key));
            imageTokenResultDto.setMessage(Optional.ofNullable(imageTokenRespDto.getResultMessage()).orElse(SuccessEnum.SUCCESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value, e.getMessage());
            imageTokenResultDto.setCode(EpbEnum.EPB099999.key);//9999
            imageTokenResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        imageTokenResultDto.setData(imageTokenRespDto);
        return imageTokenResultDto;
    }

    /**
     * 默认统一用户及密码获取token
     *
     * @return
     * @throws Exception
     */
    @ApiOperation("imagetoken:默认统一用户及密码获取token")
    @PostMapping("/common/imagetoken")
    protected @ResponseBody
    ResultDto<ImageTokenRespDto> imagetoken() {
        ResultDto<ImageTokenRespDto> imageTokenResultDto = new ResultDto<ImageTokenRespDto>();
        ImageTokenRespDto imageTokenRespDto = new ImageTokenRespDto();
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            HttpEntity<String> entity = new HttpEntity<>(null, headers);
            String url = imageSysUrl + "/ecm-management/login?username=" + imageCommonUserName + "&password=" + imageCommonUserPass;
            logger.info("调用影像平台的URL为[{}]", url);
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value);
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value, responseEntity);
            String responseStr = responseEntity.getBody();
            imageTokenRespDto = (ImageTokenRespDto) JSON.parseObject(responseStr, ImageTokenRespDto.class);
            //  将ImageTokenRespDto装到ResultDto<ImageTokenRespDto>
            imageTokenResultDto.setCode(Optional.ofNullable(imageTokenRespDto.getResultCode()).orElse(SuccessEnum.SUCCESS.key));
            imageTokenResultDto.setMessage(Optional.ofNullable(imageTokenRespDto.getResultMessage()).orElse(SuccessEnum.SUCCESS.value));
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value, e.getMessage());
            imageTokenResultDto.setCode(EpbEnum.EPB099999.key);//9999
            imageTokenResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        imageTokenResultDto.setData(imageTokenRespDto);
        return imageTokenResultDto;
    }

    /**
     * 默认统一用户及密码获取影像地址
     *
     * @return
     * @throws Exception
     */
    @ApiOperation("imageurl:默认统一用户及密码获取影像地址")
    @PostMapping("/imageurl")
    protected @ResponseBody
    ResultDto<String> imageBizUrl(@RequestBody CallImageReqDto callImageReqDto) {
        // 调用接口获取token
        ResultDto<ImageTokenRespDto> tokenResult = this.imagetoken();
        ImageTokenRespDto imageTokenRespDto = tokenResult.getData();
        if ("200".equals(imageTokenRespDto.getResultCode())) {
            callImageReqDto.setAuthorization(imageTokenRespDto.getData().getAuthorization());
        } else {
            throw BizException.error(null, imageTokenRespDto.getResultCode(), imageTokenRespDto.getResultMessage());
        }
        callImageReqDto.setPrefixUrl(imageSysUrl + "/image.html");
        String url = CmisBizImageUtils.callimage(callImageReqDto);
        logger.info("影像系统地址为【{}】", url);
        return new ResultDto<String>(url);
    }

    @ApiOperation("imageappr:影像审批信息推送")
    @PostMapping("/common/imageappr")
    protected ResultDto<String> imageappr(@RequestBody ImageApprDto imageApprDto) {
        ResultDto<String> responseEntityResultDto = new ResultDto<>();
        ResponseEntity<String> responseEntity = null;
        try {
            // 调用接口获取token
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
            ResultDto<ImageTokenRespDto> tokenResult = this.imagetoken();
            ImageTokenRespDto imageTokenRespDto = tokenResult.getData();
            if ("200".equals(imageTokenRespDto.getResultCode())) {
                headers.set("Authorization", imageTokenRespDto.getData().getAuthorization());
            } else {
                throw BizException.error(null, imageTokenRespDto.getResultCode(), imageTokenRespDto.getResultMessage());
            }
            HttpEntity<String> entity = new HttpEntity<>(null, headers);
            String url = imageSysUrl + "/ecm-management/auditxd/image" + "?docId=" + imageApprDto.getDocId() + "&isApproved=" +
                    imageApprDto.getIsApproved() + "&approval=" + imageApprDto.getApproval() + "&outcode=" + imageApprDto.getOutcode()
                    + "&opercode=" + imageApprDto.getOpercode() + "&batchNo=" + imageApprDto.getBatchNo();
            logger.info("调用影像审批平台的URL为[{}]", url);
            logger.info(TradeLogConstants.CALL_HTTP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value);
            responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class, new Object[]{});
            logger.info(TradeLogConstants.CALL_HTTP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value, responseEntity);
            if (responseEntity.getStatusCodeValue() == 200) {
                responseEntityResultDto.setCode("0");
                responseEntityResultDto.setData("true");
            } else {
                responseEntityResultDto.setCode(EpbEnum.EPB099999.key);//9999
                responseEntityResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
                responseEntityResultDto.setData("false");
            }
        } catch (Exception e) {
            responseEntityResultDto.setCode(EpbEnum.EPB099999.key);//9999
            responseEntityResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
            responseEntityResultDto.setData("false");
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_IMAGETOKEN.key, EsbEnum.TRADE_CODE_IMAGETOKEN.value, e.getMessage());
        }
        return responseEntityResultDto;
    }
}
