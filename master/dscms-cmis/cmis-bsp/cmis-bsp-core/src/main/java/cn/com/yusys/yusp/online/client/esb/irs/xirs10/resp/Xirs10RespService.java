package cn.com.yusys.yusp.online.client.esb.irs.xirs10.resp;

/**
 * 响应Service：公司客户信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Xirs10RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
