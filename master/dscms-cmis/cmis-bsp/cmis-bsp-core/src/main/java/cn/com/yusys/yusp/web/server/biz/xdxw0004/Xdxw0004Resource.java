package cn.com.yusys.yusp.web.server.biz.xdxw0004;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0004.req.Xdxw0004ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0004.resp.Data;
import cn.com.yusys.yusp.dto.server.biz.xdxw0004.resp.Xdxw0004RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0004.req.Xdxw0004DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0004.resp.Xdxw0004DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小微营业额校验查询
 *
 * @author xuchao
 * @version 1.0
 */
@Api(tags = "XDXW0004:小微营业额校验查询")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0004Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0004Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0004
     * 交易描述：小微营业额校验查询
     *
     * @param xdxw0004ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微营业额校验查询")
    @PostMapping("/xdxw0004")
    //@Idempotent({"xdcaxw0004", "#xdxw0004ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0004RespDto xdxw0004(@Validated @RequestBody Xdxw0004ReqDto xdxw0004ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, JSON.toJSONString(xdxw0004ReqDto));
        Xdxw0004DataReqDto xdxw0004DataReqDto = new Xdxw0004DataReqDto();// 请求Data： 小微营业额校验查询
        Xdxw0004DataRespDto xdxw0004DataRespDto = new Xdxw0004DataRespDto();// 响应Data：小微营业额校验查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0004.req.Data reqData = null; // 请求Data：小微营业额校验查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0004.resp.Data respData = new Data();// 响应Data：小微营业额校验查询
		Xdxw0004RespDto xdxw0004RespDto = new Xdxw0004RespDto();
		try {
            // 从 xdxw0004ReqDto获取 reqData
            reqData = xdxw0004ReqDto.getData();
            // 将 reqData 拷贝到xdxw0004DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0004DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, JSON.toJSONString(xdxw0004DataReqDto));
            ResultDto<Xdxw0004DataRespDto> xdxw0004DataResultDto = dscmsBizXwClientService.xdxw0004(xdxw0004DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, JSON.toJSONString(xdxw0004DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0004RespDto.setErorcd(Optional.ofNullable(xdxw0004DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0004RespDto.setErortx(Optional.ofNullable(xdxw0004DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0004DataResultDto.getCode())) {
                xdxw0004DataRespDto = xdxw0004DataResultDto.getData();
                xdxw0004RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0004RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0004DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0004DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, e.getMessage());
            xdxw0004RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0004RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0004RespDto.setDatasq(servsq);

        xdxw0004RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0004.key, DscmsEnum.TRADE_CODE_XDXW0004.value, JSON.toJSONString(xdxw0004RespDto));
        return xdxw0004RespDto;
    }
}
