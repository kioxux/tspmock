package cn.com.yusys.yusp.web.client.esb.rircp.fbxd01;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01.Fbxd01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01.Fbxd01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.req.Fbxd01ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.resp.Fbxd01RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd01）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd01Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd01Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd01
     * 交易描述：惠享贷授信申请件数取得
     *
     * @param fbxd01ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd01")
    protected @ResponseBody
    ResultDto<Fbxd01RespDto> fbxd01(@Validated @RequestBody Fbxd01ReqDto fbxd01ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD01.key, EsbEnum.TRADE_CODE_FBXD01.value, JSON.toJSONString(fbxd01ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd01.resp.Service();
        Fbxd01ReqService fbxd01ReqService = new Fbxd01ReqService();
        Fbxd01RespService fbxd01RespService = new Fbxd01RespService();
        Fbxd01RespDto fbxd01RespDto = new Fbxd01RespDto();
        ResultDto<Fbxd01RespDto> fbxd01ResultDto = new ResultDto<Fbxd01RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd01ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd01ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD01.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号

            fbxd01ReqService.setService(reqService);
            // 将fbxd01ReqService转换成fbxd01ReqServiceMap
            Map fbxd01ReqServiceMap = beanMapUtil.beanToMap(fbxd01ReqService);
            context.put("tradeDataMap", fbxd01ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD01.key, EsbEnum.TRADE_CODE_FBXD01.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD01.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD01.key, EsbEnum.TRADE_CODE_FBXD01.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd01RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd01RespService.class, Fbxd01RespService.class);
            respService = fbxd01RespService.getService();

            fbxd01ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd01ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd01RespDto
                BeanUtils.copyProperties(respService, fbxd01RespDto);
                fbxd01ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd01ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd01ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd01ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD01.key, EsbEnum.TRADE_CODE_FBXD01.value, e.getMessage());
            fbxd01ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd01ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd01ResultDto.setData(fbxd01RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD01.key, EsbEnum.TRADE_CODE_FBXD01.value, JSON.toJSONString(fbxd01ResultDto));
        return fbxd01ResultDto;
    }
}
