package cn.com.yusys.yusp.web.server.biz.xdxw0009;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0009.req.Xdxw0009ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0009.resp.Xdxw0009RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0009.req.Xdxw0009DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0009.resp.Xdxw0009DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:查询合同、抵押合同用章编码
 *
 * @author zoubiao
 * @version 1.0
 */
@Api(tags = "XDXW0009:查询合同、抵押合同用章编码")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0009Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0009Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0009
     * 交易描述：查询合同、抵押合同用章编码
     *
     * @param xdxw0009ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("查询合同、抵押合同用章编码")
    @PostMapping("/xdxw0009")
    //@Idempotent({"xdcaxw0009", "#xdxw0009ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0009RespDto xdxw0009(@Validated @RequestBody Xdxw0009ReqDto xdxw0009ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009ReqDto));
        Xdxw0009DataReqDto xdxw0009DataReqDto = new Xdxw0009DataReqDto();// 请求Data： 担保人人数查询
        Xdxw0009DataRespDto xdxw0009DataRespDto = new Xdxw0009DataRespDto();// 响应Data：担保人人数查询
        Xdxw0009RespDto xdxw0009RespDto = new Xdxw0009RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0009.req.Data reqData = new cn.com.yusys.yusp.dto.server.biz.xdxw0009.req.Data(); // 请求Data：担保人人数查询
        cn.com.yusys.yusp.dto.server.biz.xdxw0009.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0009.resp.Data();// 响应Data：担保人人数查询
        try {
            // 从 xdxw0009ReqDto获取 reqData
            reqData = xdxw0009ReqDto.getData();
            // 将 reqData 拷贝到xdxw0009DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0009DataReqDto);
            // 调用服务:
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009DataReqDto));
            ResultDto<Xdxw0009DataRespDto> xdxw0009DataResultDto = dscmsBizXwClientService.xdxw0009(xdxw0009DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0009RespDto.setErorcd(Optional.ofNullable(xdxw0009DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0009RespDto.setErortx(Optional.ofNullable(xdxw0009DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0009DataResultDto.getCode())) {
                xdxw0009DataRespDto = xdxw0009DataResultDto.getData();
                xdxw0009RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0009RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0009DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0009DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, e.getMessage());
            xdxw0009RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0009RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0009RespDto.setDatasq(servsq);

        xdxw0009RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0009.key, DscmsEnum.TRADE_CODE_XDXW0009.value, JSON.toJSONString(xdxw0009RespDto));
        return xdxw0009RespDto;
    }
}
