package cn.com.yusys.yusp.online.client.esb.zjywxt.clfxcx.req;

/**
 * 请求Service：协议信息查询
 */
public class Service {
	private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
	private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
	private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
	private String userid;//	柜员号	否	char(7)	是		userid
	private String brchno;//	部门号	否	char(5)	是		brchno
	private String datasq; //全局流水
	private String servdt;//    交易日期
	private String servti;//    交易时间

	private String xybhsq;//协议号
	private String fkzjh;//买方证件号
	private String fkmc;//买方名称

	public String getPrcscd() {
		return prcscd;
	}

	public void setPrcscd(String prcscd) {
		this.prcscd = prcscd;
	}

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getXybhsq() {
		return xybhsq;
	}

	public void setXybhsq(String xybhsq) {
		this.xybhsq = xybhsq;
	}

	public String getFkzjh() {
		return fkzjh;
	}

	public void setFkzjh(String fkzjh) {
		this.fkzjh = fkzjh;
	}

	public String getFkmc() {
		return fkmc;
	}

	public void setFkmc(String fkmc) {
		this.fkmc = fkmc;
	}

	@Override
	public String toString() {
		return "Service{" +
				"prcscd='" + prcscd + '\'' +
				", servtp='" + servtp + '\'' +
				", servsq='" + servsq + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", datasq='" + datasq + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", xybhsq='" + xybhsq + '\'' +
				", fkzjh='" + fkzjh + '\'' +
				", fkmc='" + fkmc + '\'' +
				'}';
	}
}
