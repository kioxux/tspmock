package cn.com.yusys.yusp.web.client.esb.znwdspxt.znsp07;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.req.Znsp07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.resp.Znsp07RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.Znsp07ReqService;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.guar_list.Record;
import cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.resp.Znsp07RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 调用智能微贷审批系统的接口处理类
 **/
@Api(tags = "BSP封装调用智能微贷审批系统的接口处理类()")
@RestController
@RequestMapping("/api/dscms2znwdspxt")
public class Dscms2Znsp07Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Znsp07Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：znsp07
     * 交易描述：数据修改接口
     *
     * @param reqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("znsp07:数据修改接口")
    @PostMapping("/znsp07")
    protected @ResponseBody
    ResultDto<Znsp07RespDto> znsp07(@Validated @RequestBody Znsp07ReqDto reqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP07.key, EsbEnum.TRADE_CODE_ZNSP07.value, JSON.toJSONString(reqDto));
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.Service();
        cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.resp.Service();
        Znsp07ReqService znsp07ReqService = new Znsp07ReqService();
        Znsp07RespService znsp07RespService = new Znsp07RespService();
        Znsp07RespDto znsp07RespDto = new Znsp07RespDto();
        ResultDto<Znsp07RespDto> znsp07ResultDto = new ResultDto<Znsp07RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将znsp07ReqDto转换成reqService
            BeanUtils.copyProperties(reqDto, reqService);
            if (CollectionUtils.nonEmpty(reqDto.getGuar_list())) {
                cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.Guar_list guar_list = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.Guar_list();
                List<Record> targetList1 = reqDto.getGuar_list().stream().map(g -> {
                    Record target1 = new Record();
                    BeanUtils.copyProperties(g, target1);
                    return target1;
                }).collect(Collectors.toList());
                guar_list.setRecord(targetList1);
                reqService.setGuar_list(guar_list);
            }

            if (CollectionUtils.nonEmpty(reqDto.getPlage_list())) {
                cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.Plage_list plage_list = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.Plage_list();
                List<cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.plage_list.Record> targetList2 = reqDto.getPlage_list().stream().map(p -> {
                    cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.plage_list.Record target2 = new cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp07.req.plage_list.Record();
                    BeanUtils.copyProperties(p, target2);
                    return target2;
                }).collect(Collectors.toList());
                plage_list.setRecord(targetList2);
                reqService.setPlage_list(plage_list);
            }

            reqService.setPrcscd(EsbEnum.TRADE_CODE_ZNSP07.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水
            reqService.setUserid(EsbEnum.USERID_ZNWDSPXT.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ZNWDSPXT.key);//    部门号
            znsp07ReqService.setService(reqService);
            // 将znsp07ReqService转换成znsp07ReqServiceMap
            Map znsp07ReqServiceMap = beanMapUtil.beanToMap(znsp07ReqService);
            context.put("tradeDataMap", znsp07ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP07.key, EsbEnum.TRADE_CODE_ZNSP07.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_ZNSP07.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP07.key, EsbEnum.TRADE_CODE_ZNSP07.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            znsp07RespService = beanMapUtil.mapToBean(tradeDataMap, Znsp07RespService.class, Znsp07RespService.class);
            respService = znsp07RespService.getService();
            //  将respService转换成znsp07RespDto
            BeanUtils.copyProperties(respService, znsp07RespDto);
            znsp07ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            znsp07ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成znsp07RespDto
                BeanUtils.copyProperties(respService, znsp07RespDto);
                znsp07ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                znsp07ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                znsp07ResultDto.setCode(EpbEnum.EPB099999.key);
                znsp07ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP07.key, EsbEnum.TRADE_CODE_ZNSP07.value, e.getMessage());
            znsp07ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            znsp07ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        znsp07ResultDto.setData(znsp07RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_ZNSP07.key, EsbEnum.TRADE_CODE_ZNSP07.value, JSON.toJSONString(znsp07ResultDto));
        return znsp07ResultDto;
    }
}
