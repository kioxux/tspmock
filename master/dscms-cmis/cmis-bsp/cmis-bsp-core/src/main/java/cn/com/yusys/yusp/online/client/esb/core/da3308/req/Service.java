package cn.com.yusys.yusp.online.client.esb.core.da3308.req;

import java.math.BigDecimal;

/**
 * 请求Service：抵债资产费用管理
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //   全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间

    private String dzzcbhao;//抵债资产编号
    private String dzzcminc;//抵债资产名称
    private BigDecimal shoufjee;//收费金额
    private String sfrzhzhh;//收费入账账号
    private String sfrzhzxh;//收费入账账号子序号
    private String fufeizhh;//付费账号
    private String ffzhhzxh;//付费账号子序号
    private String shifoubz;//费用挂账标志
    private String beizhuxx;//备注
    private String dzzcfylx;//抵债资产费用类型
    private String zijnlaiy;//资金来源

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    public String getSfrzhzhh() {
        return sfrzhzhh;
    }

    public void setSfrzhzhh(String sfrzhzhh) {
        this.sfrzhzhh = sfrzhzhh;
    }

    public String getSfrzhzxh() {
        return sfrzhzxh;
    }

    public void setSfrzhzxh(String sfrzhzxh) {
        this.sfrzhzxh = sfrzhzxh;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getShifoubz() {
        return shifoubz;
    }

    public void setShifoubz(String shifoubz) {
        this.shifoubz = shifoubz;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getDzzcfylx() {
        return dzzcfylx;
    }

    public void setDzzcfylx(String dzzcfylx) {
        this.dzzcfylx = dzzcfylx;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", shoufjee=" + shoufjee +
                ", sfrzhzhh='" + sfrzhzhh + '\'' +
                ", sfrzhzxh='" + sfrzhzxh + '\'' +
                ", fufeizhh='" + fufeizhh + '\'' +
                ", ffzhhzxh='" + ffzhhzxh + '\'' +
                ", shifoubz='" + shifoubz + '\'' +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", dzzcfylx='" + dzzcfylx + '\'' +
                ", zijnlaiy='" + zijnlaiy + '\'' +
                '}';
    }
}
