package cn.com.yusys.yusp.online.client.esb.core.ib1243.resp;

/**
 * 响应Service：通用电子记帐交易（多借多贷-多币种）
 *
 * @author lihh
 * @version 1.0
 */
public class Service {
    private String erorcd;//错误码
    private String erortx;//错误描述
    private String jiaoyirq;//交易日期
    private String guiylius;//柜员流水号
    private List list;//交易账户信息输出

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getGuiylius() {
        return guiylius;
    }

    public void setGuiylius(String guiylius) {
        this.guiylius = guiylius;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                "erortx='" + erortx + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "guiylius='" + guiylius + '\'' +
                "list='" + list + '\'' +
                '}';
    }
}
