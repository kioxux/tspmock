package cn.com.yusys.yusp.online.client.esb.fxyjxt.djhhmd.req;

/**
 * 请求Service：查询客户风险预警等级与是否黑灰名单
 *
 * @author code-generator
 * @version 1.0
 */
public class DjhhmdReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}