package cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.resp;

/**
 * 响应Service：查询客户核心编号，客户名称对应的放款和还款信息
 *
 * @author leehuang
 * @version 1.0
 */
public class Fbxd13RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fbxd13RespService{" +
                "service=" + service +
                '}';
    }
}
