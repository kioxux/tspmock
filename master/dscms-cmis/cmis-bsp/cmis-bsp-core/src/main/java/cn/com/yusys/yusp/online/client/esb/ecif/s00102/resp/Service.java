package cn.com.yusys.yusp.online.client.esb.ecif.s00102.resp;

/**
 * 响应Service：对私客户综合信息维护
 *
 * @author hjk
 * @version 1.0
 * @since 2021/4/15 20:36
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String custno;//      客户编号
    private String custna;//      客户名称
    private String custtp;//      客户状态
    private String tsywsq;//      特殊业务流水号
    private String exchmb;//      改客户名标志
    private String prcstx;//      业务对比字段
    private String nnjytp;//      交易类型
    private String nnjyna;//      交易名称
    private String jiaoyils;//      核心流水
    private String jiaoyirq;//      核心交易日期

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCusttp() {
        return custtp;
    }

    public void setCusttp(String custtp) {
        this.custtp = custtp;
    }

    public String getTsywsq() {
        return tsywsq;
    }

    public void setTsywsq(String tsywsq) {
        this.tsywsq = tsywsq;
    }

    public String getExchmb() {
        return exchmb;
    }

    public void setExchmb(String exchmb) {
        this.exchmb = exchmb;
    }

    public String getPrcstx() {
        return prcstx;
    }

    public void setPrcstx(String prcstx) {
        this.prcstx = prcstx;
    }

    public String getNnjytp() {
        return nnjytp;
    }

    public void setNnjytp(String nnjytp) {
        this.nnjytp = nnjytp;
    }

    public String getNnjyna() {
        return nnjyna;
    }

    public void setNnjyna(String nnjyna) {
        this.nnjyna = nnjyna;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    @Override
    public String toString() {
        return "S00102RespService{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", custno='" + custno + '\'' +
                ", custna='" + custna + '\'' +
                ", custtp='" + custtp + '\'' +
                ", tsywsq='" + tsywsq + '\'' +
                ", exchmb='" + exchmb + '\'' +
                ", prcstx='" + prcstx + '\'' +
                ", nnjytp='" + nnjytp + '\'' +
                ", nnjyna='" + nnjyna + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                '}';
    }
}
