package cn.com.yusys.yusp.web.server.biz.xdxw0079;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0079.req.Xdxw0079ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0079.resp.Xdxw0079RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0079.req.Xdxw0079DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0079.resp.Xdxw0079DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:勘验任务状态同步
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0079:勘验任务状态同步")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0079Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0079Resource.class);

	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0079
     * 交易描述：勘验任务状态同步
     *
     * @param xdxw0079ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("勘验任务状态同步")
    @PostMapping("/xdxw0079")
    @Idempotent({"xdxw0079", "#xdxw0079ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0079RespDto xdxw0079(@Validated @RequestBody Xdxw0079ReqDto xdxw0079ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079ReqDto));
        Xdxw0079DataReqDto xdxw0079DataReqDto = new Xdxw0079DataReqDto();// 请求Data： 勘验任务状态同步
        Xdxw0079DataRespDto xdxw0079DataRespDto = new Xdxw0079DataRespDto();// 响应Data：勘验任务状态同步
		Xdxw0079RespDto xdxw0079RespDto = new Xdxw0079RespDto();
        // 此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0079.req.Data reqData = null; // 请求Data：勘验任务状态同步
		cn.com.yusys.yusp.dto.server.biz.xdxw0079.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0079.resp.Data();// 响应Data：勘验任务状态同步
        // 此处包导入待确定 结束
        try {
            // 从 xdxw0079ReqDto获取 reqData
            reqData = xdxw0079ReqDto.getData();
            // 将 reqData 拷贝到xdxw0079DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0079DataReqDto);
            // 调用服务：
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079DataReqDto));
            ResultDto<Xdxw0079DataRespDto> xdxw0079DataResultDto = dscmsBizXwClientService.xdxw0079(xdxw0079DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0079RespDto.setErorcd(Optional.ofNullable(xdxw0079DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0079RespDto.setErortx(Optional.ofNullable(xdxw0079DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0079DataResultDto.getCode())) {
                xdxw0079DataRespDto = xdxw0079DataResultDto.getData();
                xdxw0079RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0079RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0079DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0079DataRespDto, respData);
            }else if(Objects.equals("0001", xdxw0079DataResultDto.getCode())){
                xdxw0079RespDto.setErorcd("0001");
                xdxw0079RespDto.setErortx(SuccessEnum.SUCCESS.value);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, e.getMessage());
            xdxw0079RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0079RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0079RespDto.setDatasq(servsq);

        xdxw0079RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0079.key, DscmsEnum.TRADE_CODE_XDXW0079.value, JSON.toJSONString(xdxw0079RespDto));
        return xdxw0079RespDto;
    }
}
