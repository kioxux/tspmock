package cn.com.yusys.yusp.online.client.esb.core.co3222.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵质押物单笔查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Lstnum1Record {
    private String dkjiejuh;//贷款借据号
    private Long xuhaoooo;//序号
    private String shfbhzhh;//是否本行账号
    private String kehuzhao;//客户账号
    private String khzhhzxh;//客户账号子序号
    private String kehmingc;//客户名称
    private String xitongzh;//系统账号
    private String zhjzhyfs;//质押方式
    private String huobdhao;//货币代号
    private BigDecimal zhiyajee;//质押金额
    private String djiebhao;//冻结编号
    private String guanlzht;//关联状态

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Long getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(Long xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getShfbhzhh() {
        return shfbhzhh;
    }

    public void setShfbhzhh(String shfbhzhh) {
        this.shfbhzhh = shfbhzhh;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getXitongzh() {
        return xitongzh;
    }

    public void setXitongzh(String xitongzh) {
        this.xitongzh = xitongzh;
    }

    public String getZhjzhyfs() {
        return zhjzhyfs;
    }

    public void setZhjzhyfs(String zhjzhyfs) {
        this.zhjzhyfs = zhjzhyfs;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getZhiyajee() {
        return zhiyajee;
    }

    public void setZhiyajee(BigDecimal zhiyajee) {
        this.zhiyajee = zhiyajee;
    }

    public String getDjiebhao() {
        return djiebhao;
    }

    public void setDjiebhao(String djiebhao) {
        this.djiebhao = djiebhao;
    }

    public String getGuanlzht() {
        return guanlzht;
    }

    public void setGuanlzht(String guanlzht) {
        this.guanlzht = guanlzht;
    }

    @Override
    public String toString() {
        return "Lstnum1Record{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", xuhaoooo=" + xuhaoooo +
                ", shfbhzhh='" + shfbhzhh + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", xitongzh='" + xitongzh + '\'' +
                ", zhjzhyfs='" + zhjzhyfs + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", zhiyajee=" + zhiyajee +
                ", djiebhao='" + djiebhao + '\'' +
                ", guanlzht='" + guanlzht + '\'' +
                '}';
    }
}
