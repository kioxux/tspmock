package cn.com.yusys.yusp.online.client.esb.znwdspxt.znsp08.req;

/**
 * 请求Service：客户调查撤销接口
 *
 * @author chenyong
 * @version 1.0
 */
public class Znsp08ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
