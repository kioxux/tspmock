package cn.com.yusys.yusp.online.client.gxp.common.resp;

/**
 * GXP响应Head
 *
 * @author leehuang
 * @version 1.0
 * @since 2021/5/25 21:56
 */
public class GxpRespHead {
    private String prcscd;//业务处理码
    private String seqnum;//交易流水号
    private String retrcd;//交易返回代码
    private String trandt;//
    private String erortx;
    private String pckgsq;

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getSeqnum() {
        return seqnum;
    }

    public void setSeqnum(String seqnum) {
        this.seqnum = seqnum;
    }

    public String getRetrcd() {
        return retrcd;
    }

    public void setRetrcd(String retrcd) {
        this.retrcd = retrcd;
    }

    public String getTrandt() {
        return trandt;
    }

    public void setTrandt(String trandt) {
        this.trandt = trandt;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getPckgsq() {
        return pckgsq;
    }

    public void setPckgsq(String pckgsq) {
        this.pckgsq = pckgsq;
    }

    @Override
    public String toString() {
        return "GxpRespHead{" +
                "prcscd='" + prcscd + '\'' +
                ", seqnum='" + seqnum + '\'' +
                ", retrcd='" + retrcd + '\'' +
                ", trandt='" + trandt + '\'' +
                ", erortx='" + erortx + '\'' +
                ", pckgsq='" + pckgsq + '\'' +
                '}';
    }
}
