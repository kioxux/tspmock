package cn.com.yusys.yusp.online.client.esb.wx.wxp002.req;

/**
 * 请求Service：信贷将授信额度推送给移动端
 */
public class Service {
    private String prcscd;//	处理码	否	varchar(30)	是	接口交易码区分交易	prcscd
    private String servtp;//	渠道	否	varchar(3)	是	交易渠道	servtp
    private String servsq;//	渠道流水	否	varchar(30)	是	由发起渠道生成的唯一标识	servsq
    private String userid;//	柜员号	否	char(7)	是		userid
    private String brchno;//	部门号	否	char(5)	是		brchno
    private String datasq; //全局流水
    private String servdt;//    交易日期
    private String servti;//    交易时间


    private String applid;//对接记录ID
    private String entnam;//企业名称
    private String entpid;//企业代码
    private String connum;//授信合同编号
    private String crerst;//授信结果
    private String crerea;//授信原因
    private String adlmt;//授信金额
    private String crerat;//授信利率
    private String crebeg;//授信开始时间
    private String creend;//授信结束时间
    private String iffist;//是否首次贷款

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getApplid() {
        return applid;
    }

    public void setApplid(String applid) {
        this.applid = applid;
    }

    public String getEntnam() {
        return entnam;
    }

    public void setEntnam(String entnam) {
        this.entnam = entnam;
    }

    public String getEntpid() {
        return entpid;
    }

    public void setEntpid(String entpid) {
        this.entpid = entpid;
    }

    public String getConnum() {
        return connum;
    }

    public void setConnum(String connum) {
        this.connum = connum;
    }

    public String getCrerst() {
        return crerst;
    }

    public void setCrerst(String crerst) {
        this.crerst = crerst;
    }

    public String getCrerea() {
        return crerea;
    }

    public void setCrerea(String crerea) {
        this.crerea = crerea;
    }

    public String getAdlmt() {
        return adlmt;
    }

    public void setAdlmt(String adlmt) {
        this.adlmt = adlmt;
    }

    public String getCrerat() {
        return crerat;
    }

    public void setCrerat(String crerat) {
        this.crerat = crerat;
    }

    public String getCrebeg() {
        return crebeg;
    }

    public void setCrebeg(String crebeg) {
        this.crebeg = crebeg;
    }

    public String getCreend() {
        return creend;
    }

    public void setCreend(String creend) {
        this.creend = creend;
    }

    public String getIffist() {
        return iffist;
    }

    public void setIffist(String iffist) {
        this.iffist = iffist;
    }

    @Override
    public String toString() {
        return "Service{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", servdt='" + servdt + '\'' +
                ", servti='" + servti + '\'' +
                ", applid='" + applid + '\'' +
                ", entnam='" + entnam + '\'' +
                ", entpid='" + entpid + '\'' +
                ", connum='" + connum + '\'' +
                ", crerst='" + crerst + '\'' +
                ", crerea='" + crerea + '\'' +
                ", adlmt='" + adlmt + '\'' +
                ", crerat='" + crerat + '\'' +
                ", crebeg='" + crebeg + '\'' +
                ", creend='" + creend + '\'' +
                ", iffist='" + iffist + '\'' +
                '}';
    }
}
