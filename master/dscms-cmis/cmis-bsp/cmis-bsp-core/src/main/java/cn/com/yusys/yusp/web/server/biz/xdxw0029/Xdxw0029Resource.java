package cn.com.yusys.yusp.web.server.biz.xdxw0029;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0029.req.Xdxw0029ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0029.resp.Xdxw0029RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0029.req.Xdxw0029DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0029.resp.Xdxw0029DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号查询现有融资总额、总余额、担保方式
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0029:根据客户号查询现有融资总额、总余额、担保方式")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0029Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0029Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0029
     * 交易描述：根据客户号查询现有融资总额、总余额、担保方式
     *
     * @param xdxw0029ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询现有融资总额、总余额、担保方式")
    @PostMapping("/xdxw0029")
    //@Idempotent({"xdcaxw0029", "#xdxw0029ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0029RespDto xdxw0029(@Validated @RequestBody Xdxw0029ReqDto xdxw0029ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029ReqDto));
        Xdxw0029DataReqDto xdxw0029DataReqDto = new Xdxw0029DataReqDto();// 请求Data： 根据客户号查询现有融资总额、总余额、担保方式
        Xdxw0029DataRespDto xdxw0029DataRespDto = new Xdxw0029DataRespDto();// 响应Data：根据客户号查询现有融资总额、总余额、担保方式
        Xdxw0029RespDto xdxw0029RespDto = new Xdxw0029RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0029.req.Data reqData = null; // 请求Data：根据客户号查询现有融资总额、总余额、担保方式
        cn.com.yusys.yusp.dto.server.biz.xdxw0029.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0029.resp.Data();// 响应Data：根据客户号查询现有融资总额、总余额、担保方式
        try {
            // 从 xdxw0029ReqDto获取 reqData
            reqData = xdxw0029ReqDto.getData();
            // 将 reqData 拷贝到xdxw0029DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0029DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataReqDto));
            ResultDto<Xdxw0029DataRespDto> xdxw0029DataResultDto = dscmsBizXwClientService.xdxw0029(xdxw0029DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdxw0029RespDto.setErorcd(Optional.ofNullable(xdxw0029DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0029RespDto.setErortx(Optional.ofNullable(xdxw0029DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0029DataResultDto.getCode())) {
                xdxw0029DataRespDto = xdxw0029DataResultDto.getData();
                xdxw0029RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0029RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0029DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0029DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, e.getMessage());
            xdxw0029RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0029RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0029RespDto.setDatasq(servsq);
        xdxw0029RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0029.key, DscmsEnum.TRADE_CODE_XDXW0029.value, JSON.toJSONString(xdxw0029RespDto));
        return xdxw0029RespDto;
    }
}
