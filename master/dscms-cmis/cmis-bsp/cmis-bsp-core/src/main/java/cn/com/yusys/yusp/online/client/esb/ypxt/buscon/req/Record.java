package cn.com.yusys.yusp.online.client.esb.ypxt.buscon.req;

/**
 * @author ZRC
 * @version 0.1
 * @date 2021/5/29 16:17
 * @since 2021/5/29 16:17
 */
public class Record {

    private String ywbhyp;//业务编号
    private String ywlxyp;//业务类型
    private String yptybh;//押品统一编号
    private String isflag;//是否有效

    public String getYwbhyp() {
        return ywbhyp;
    }

    public void setYwbhyp(String ywbhyp) {
        this.ywbhyp = ywbhyp;
    }

    public String getYwlxyp() {
        return ywlxyp;
    }

    public void setYwlxyp(String ywlxyp) {
        this.ywlxyp = ywlxyp;
    }

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getIsflag() {
        return isflag;
    }

    public void setIsflag(String isflag) {
        this.isflag = isflag;
    }

    @Override
    public String toString() {
        return "Record{" +
                "ywbhyp='" + ywbhyp + '\'' +
                ", ywlxyp='" + ywlxyp + '\'' +
                ", yptybh='" + yptybh + '\'' +
                ", isflag='" + isflag + '\'' +
                '}';
    }
}
