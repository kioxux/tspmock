package cn.com.yusys.yusp.web.server.biz.xdxw0027;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0027.req.Xdxw0027ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0027.resp.Xdxw0027RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0027.req.Xdxw0027DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0027.resp.Xdxw0027DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:客户调查信息详情查看
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDXW0027:客户调查信息详情查看")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0027Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0027Resource.class);
    @Autowired
    private DscmsBizXwClientService dscmsBizXwClientService;

    /**
     * 交易码：xdxw0027
     * 交易描述：客户调查信息详情查看
     *
     * @param xdxw0027ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("客户调查信息详情查看")
    @PostMapping("/xdxw0027")
    //@Idempotent({"xdcaxw0027", "#xdxw0027ReqDto.datasq"})
    protected @ResponseBody
    Xdxw0027RespDto xdxw0027(@Validated @RequestBody Xdxw0027ReqDto xdxw0027ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027ReqDto));
        Xdxw0027DataReqDto xdxw0027DataReqDto = new Xdxw0027DataReqDto();// 请求Data： 客户调查信息详情查看
        Xdxw0027DataRespDto xdxw0027DataRespDto = new Xdxw0027DataRespDto();// 响应Data：客户调查信息详情查看
        Xdxw0027RespDto xdxw0027RespDto = new Xdxw0027RespDto();
        cn.com.yusys.yusp.dto.server.biz.xdxw0027.req.Data reqData = null; // 请求Data：客户调查信息详情查看
        cn.com.yusys.yusp.dto.server.biz.xdxw0027.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0027.resp.Data();// 响应Data：客户调查信息详情查看
        try {
            // 从 xdxw0027ReqDto获取 reqData
            reqData = xdxw0027ReqDto.getData();
            // 将 reqData 拷贝到xdxw0027DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0027DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataReqDto));
            ResultDto<Xdxw0027DataRespDto> xdxw0027DataResultDto = dscmsBizXwClientService.xdxw0027(xdxw0027DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0027RespDto.setErorcd(Optional.ofNullable(xdxw0027DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0027RespDto.setErortx(Optional.ofNullable(xdxw0027DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0027DataResultDto.getCode())) {
                xdxw0027DataRespDto = xdxw0027DataResultDto.getData();
                xdxw0027RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0027RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0027DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0027DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, e.getMessage());
            xdxw0027RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0027RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0027RespDto.setDatasq(servsq);

        xdxw0027RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0027.key, DscmsEnum.TRADE_CODE_XDXW0027.value, JSON.toJSONString(xdxw0027RespDto));
        return xdxw0027RespDto;
    }
}
