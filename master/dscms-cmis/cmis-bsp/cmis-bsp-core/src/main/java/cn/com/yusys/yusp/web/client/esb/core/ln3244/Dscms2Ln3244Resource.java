package cn.com.yusys.yusp.web.client.esb.core.ln3244;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.core.ln3244.Ln3244ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3244.Ln3244RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.core.ln3244.req.Ln3244ReqService;
import cn.com.yusys.yusp.online.client.esb.core.ln3244.resp.Ln3244RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * BSP封装调用核心系统的接口
 **/
@Api(tags = "BSP封装调用核心系统的接口处理类(ln3244)")
@RestController
@RequestMapping("/api/dscms2coreln")
public class Dscms2Ln3244Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Ln3244Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：ln3244
     * 交易描述：贷款客户经理批量移交
     *
     * @param ln3244ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("ln3244:贷款客户经理批量移交")
    @PostMapping("/ln3244")
    protected @ResponseBody
    ResultDto<Ln3244RespDto> ln3244(@Validated @RequestBody Ln3244ReqDto ln3244ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3244.key, EsbEnum.TRADE_CODE_LN3244.value, JSON.toJSONString(ln3244ReqDto));
        cn.com.yusys.yusp.online.client.esb.core.ln3244.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.core.ln3244.req.Service();
        cn.com.yusys.yusp.online.client.esb.core.ln3244.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.core.ln3244.resp.Service();
        Ln3244ReqService ln3244ReqService = new Ln3244ReqService();
        Ln3244RespService ln3244RespService = new Ln3244RespService();
        Ln3244RespDto ln3244RespDto = new Ln3244RespDto();
        ResultDto<Ln3244RespDto> ln3244ResultDto = new ResultDto<Ln3244RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将ln3244ReqDto转换成reqService
            BeanUtils.copyProperties(ln3244ReqDto, reqService);
            // 循环相关的判断开始
            //移交客户列表[LIST]
            if (CollectionUtils.nonEmpty(ln3244ReqDto.getLstKehuhaoo())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3244.LstKehuhaoo> ln3244LstKehuhaooDtoList =
                        Optional.ofNullable(ln3244ReqDto.getLstKehuhaoo()).orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.LstKehuhaoo lstKehuhaoo =
                        Optional.ofNullable(reqService.getLstKehuhaoo())
                                .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.LstKehuhaoo());
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.Record> lstKehuhaooRecords =
                        ln3244LstKehuhaooDtoList.parallelStream().map(ln3244LstKehuhaooDto -> {
                            cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.Record lstKehuhaooRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstKehuhaoo.Record();
                            BeanUtils.copyProperties(ln3244LstKehuhaooDto, lstKehuhaooRecord);
                            return lstKehuhaooRecord;
                        }).collect(Collectors.toList());
                lstKehuhaoo.setRecord(lstKehuhaooRecords);
                reqService.setLstKehuhaoo(lstKehuhaoo);
            }

            //移交合同列表[LIST]
            if (CollectionUtils.nonEmpty(ln3244ReqDto.getLstHetongbh())) {
                java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3244.LstHetongbh> ln3244LstHetongbhDtoList =
                        Optional.ofNullable(ln3244ReqDto.getLstHetongbh()).orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.LstHetongbh lstHetongbh =
                        Optional.ofNullable(reqService.getLstHetongbh())
                                .orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.LstHetongbh());
                java.util.List<cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.Record> lstHetongbhRecords =
                        ln3244LstHetongbhDtoList.parallelStream().map(ln3244LstHetongbhDto -> {
                            cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.Record lstHetongbhRecord = new cn.com.yusys.yusp.online.client.esb.core.ln3244.req.lstHetongbh.Record();
                            BeanUtils.copyProperties(ln3244LstHetongbhDto, lstHetongbhRecord);
                            return lstHetongbhRecord;
                        }).collect(Collectors.toList());
                lstHetongbh.setRecord(lstHetongbhRecords);
                reqService.setLstHetongbh(lstHetongbh);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_LN3244.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setUserid(EsbEnum.USERID_CORE.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_CORE.key);//    部门号
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            LocalDateTime now = LocalDateTime.now();
            reqService.setServdt(tranDateFormtter.format(now));//    交易日期
            reqService.setServti(tranTimestampFormatter.format(now));//    交易时间

            ln3244ReqService.setService(reqService);
            // 将ln3244ReqService转换成ln3244ReqServiceMap
            Map ln3244ReqServiceMap = beanMapUtil.beanToMap(ln3244ReqService);
            context.put("tradeDataMap", ln3244ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3244.key, EsbEnum.TRADE_CODE_LN3244.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_LN3244.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3244.key, EsbEnum.TRADE_CODE_LN3244.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            ln3244RespService = beanMapUtil.mapToBean(tradeDataMap, Ln3244RespService.class, Ln3244RespService.class);
            respService = ln3244RespService.getService();

            ln3244ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            ln3244ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Ln3244RespDto
                BeanUtils.copyProperties(respService, ln3244RespDto);
                ln3244ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                ln3244ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                ln3244ResultDto.setCode(EpbEnum.EPB099999.key);
                ln3244ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3244.key, EsbEnum.TRADE_CODE_LN3244.value, e.getMessage());
            ln3244ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            ln3244ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        ln3244ResultDto.setData(ln3244RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_LN3244.key, EsbEnum.TRADE_CODE_LN3244.value, JSON.toJSONString(ln3244ResultDto));
        return ln3244ResultDto;
    }

}
