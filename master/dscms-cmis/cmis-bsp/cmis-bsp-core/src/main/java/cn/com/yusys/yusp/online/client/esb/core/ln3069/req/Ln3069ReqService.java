package cn.com.yusys.yusp.online.client.esb.core.ln3069.req;

/**
 * 请求Service：资产转让/证券化组合查询
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3069ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}

