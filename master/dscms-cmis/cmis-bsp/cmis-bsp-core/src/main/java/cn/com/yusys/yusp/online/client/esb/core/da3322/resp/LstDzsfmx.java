package cn.com.yusys.yusp.online.client.esb.core.da3322.resp;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 17:28
 * @since 2021/6/7 17:28
 */
public class LstDzsfmx {
    private java.util.List<cn.com.yusys.yusp.online.client.esb.core.da3322.resp.Record3> record;

    public List<Record3> getRecord() {
        return record;
    }

    public void setRecord(List<Record3> record) {
        this.record = record;
    }

    @Override
    public String toString() {
        return "LstDzsfmx{" +
                "record=" + record +
                '}';
    }
}
