package cn.com.yusys.yusp.web.client.esb.ecif.g00202;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.RelArrayInfo;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.G00202ReqService;
import cn.com.yusys.yusp.online.client.esb.ecif.g00202.resp.G00202RespService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 封装调用ECIF系统的接口
 */
@Api(tags = "BSP封装调用ECIF系统的接口处理类(g00202)")
@RestController
@RequestMapping("/api/dscms2ecif")
public class Dscms2G00202Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2G00202Resource.class);
    private static DateTimeFormatter tranDateFormtter = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static DateTimeFormatter tranTimestampFormatter = DateTimeFormatter.ofPattern("HHmmss");
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 同业客户开户
     *
     * @param g00202ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("g00202:同业客户开户")
    @PostMapping("/g00202")
    protected @ResponseBody
    ResultDto<G00202RespDto> g00202(@Validated @RequestBody G00202ReqDto g00202ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00202.key, EsbEnum.TRADE_CODE_G00202.value, JSON.toJSONString(g00202ReqDto));
        cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.Service();
        cn.com.yusys.yusp.online.client.esb.ecif.g00202.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.ecif.g00202.resp.Service();
        G00202ReqService g00202ReqService = new G00202ReqService();
        G00202RespService g00202RespService = new G00202RespService();
        G00202RespDto g00202RespDto = new G00202RespDto();
        ResultDto<G00202RespDto> g00202ResultDto = new ResultDto<G00202RespDto>();

        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将G00202ReqDto转换成reqService
            BeanUtils.copyProperties(g00202ReqDto, reqService);
            if (CollectionUtils.nonEmpty(g00202ReqDto.getREL_ARRAY_INFO())) {
                java.util.List<RelArrayInfo> relArrayInfoList = Optional.ofNullable(g00202ReqDto.getREL_ARRAY_INFO()).orElseGet(() -> new ArrayList<>());
                cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.List g00202ReqList = Optional.ofNullable(reqService.getList()).orElseGet(() -> new cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.List());
                java.util.List<cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.Record> g00202ReqRecords = relArrayInfoList.parallelStream().map(
                        relArrayInfo -> {
                            cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.Record g00202ReqRecord = new cn.com.yusys.yusp.online.client.esb.ecif.g00202.req.Record();
                            BeanUtils.copyProperties(relArrayInfo, g00202ReqRecord);
                            return g00202ReqRecord;
                        }
                ).collect(Collectors.toList());
                g00202ReqList.setRecord(g00202ReqRecords);
                reqService.setList(g00202ReqList);
            }
            reqService.setPrcscd(EsbEnum.TRADE_CODE_G00202.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道

            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_ECF.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_ECF.key, EsbEnum.SERVTP_ECF.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]

            //  生成规则：系统编号(3位)+年月日(8位)+时分秒(6位）+seq(10位)
            reqService.setServsq(servsq);//    渠道流水
            reqService.setDatasq(servsq);//    全局流水
            reqService.setUserid(EsbEnum.USERID_ECIF.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_ECIF.key);//    部门号
            LocalDateTime now = LocalDateTime.now();
            reqService.setTxdate(tranDateFormtter.format(now));//    交易日期
            reqService.setTxtime(tranTimestampFormatter.format(now));//    交易时间
            g00202ReqService.setService(reqService);
            // 将g00202ReqService转换成g00202ReqServiceMap
            Map g00202ReqServiceMap = beanMapUtil.beanToMap(g00202ReqService);
            context.put("tradeDataMap", g00202ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00202.key, EsbEnum.TRADE_CODE_G00202.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_G00202.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00202.key, EsbEnum.TRADE_CODE_G00202.value);
            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            g00202RespService = beanMapUtil.mapToBean(tradeDataMap, G00202RespService.class, G00202RespService.class);
            respService = g00202RespService.getService();
            //  将G00202RespDto封装到ResultDto<G00202RespDto>
            g00202ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            g00202ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成G00202RespDto
                BeanUtils.copyProperties(respService, g00202RespDto);
                g00202ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                g00202ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                BeanUtils.copyProperties(respService, g00202RespDto);
                g00202ResultDto.setCode(EpbEnum.EPB099999.key);
                g00202ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00202.key, EsbEnum.TRADE_CODE_G00202.value, e.getMessage());
            g00202ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            g00202ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        g00202ResultDto.setData(g00202RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_G00202.key, EsbEnum.TRADE_CODE_G00202.value, JSON.toJSONString(g00202ResultDto));
        return g00202ResultDto;
    }
}
