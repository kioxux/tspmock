package cn.com.yusys.yusp.online.client.esb.ypxt.spsyyp.req;

/**
 * 请求Service：信贷审批结果同步接口
 *
 * @author lihh
 * @version 1.0
 */
public class SpsyypReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }
}                      
