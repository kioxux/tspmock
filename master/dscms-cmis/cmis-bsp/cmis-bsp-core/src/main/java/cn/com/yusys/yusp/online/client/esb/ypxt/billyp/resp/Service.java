package cn.com.yusys.yusp.online.client.esb.ypxt.billyp.resp;

/**
 * 响应Service：票据信息同步接口
 *
 * @author zhugenrong
 * @version 1.0
 */
public class Service {
    private String yptybh;//押品统一编号
    private String erorcd;//响应码
    private String erortx;//响应信息
    private String assetNo;

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "Service{" +
                "yptybh='" + yptybh + '\'' +
                ", erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", assetNo='" + assetNo + '\'' +
                '}';
    }
}