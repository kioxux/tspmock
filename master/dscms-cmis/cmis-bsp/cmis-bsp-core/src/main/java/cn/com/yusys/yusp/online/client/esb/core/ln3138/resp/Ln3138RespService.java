package cn.com.yusys.yusp.online.client.esb.core.ln3138.resp;

/**
 * 响应Service：等额等本贷款推算
 *
 * @author leehuang
 * @version 1.0
 */
public class Ln3138RespService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Ln3138RespService{" +
                "service=" + service +
                '}';
    }
}
