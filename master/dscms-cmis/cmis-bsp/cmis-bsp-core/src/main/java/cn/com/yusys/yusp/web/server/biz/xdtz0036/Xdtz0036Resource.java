package cn.com.yusys.yusp.web.server.biz.xdtz0036;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdtz0036.req.Xdtz0036ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdtz0036.resp.Xdtz0036RespDto;
import cn.com.yusys.yusp.dto.server.xdtz0036.req.Xdtz0036DataReqDto;
import cn.com.yusys.yusp.dto.server.xdtz0036.resp.Xdtz0036DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizTzClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDTZ0036:根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数")
@RestController
@RequestMapping("/api/dscms")
public class Xdtz0036Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdtz0036Resource.class);
    @Autowired
    private DscmsBizTzClientService dscmsBizTzClientService;

    /**
     * 交易码：xdtz0036
     * 交易描述：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
     *
     * @param xdtz0036ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数")
    @PostMapping("/xdtz0036")
//    @Idempotent({"xdcatz0036", "#xdtz0036ReqDto.datasq"})
    protected @ResponseBody
    Xdtz0036RespDto xdtz0036(@Validated @RequestBody Xdtz0036ReqDto xdtz0036ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036ReqDto));
        Xdtz0036DataReqDto xdtz0036DataReqDto = new Xdtz0036DataReqDto();// 请求Data： 根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
        Xdtz0036DataRespDto xdtz0036DataRespDto = new Xdtz0036DataRespDto();// 响应Data：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
        Xdtz0036RespDto xdtz0036RespDto = new Xdtz0036RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdtz0036.req.Data reqData = null; // 请求Data：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
        cn.com.yusys.yusp.dto.server.biz.xdtz0036.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdtz0036.resp.Data();// 响应Data：根据客户号查询申请人行内还款（利息）单次逾期天数不超N天次数
        try {
            // 从 xdtz0036ReqDto获取 reqData
            reqData = xdtz0036ReqDto.getData();
            // 将 reqData 拷贝到xdtz0036DataReqDto
            BeanUtils.copyProperties(reqData, xdtz0036DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataReqDto));
            ResultDto<Xdtz0036DataRespDto> xdtz0036DataResultDto = dscmsBizTzClientService.xdtz0036(xdtz0036DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdtz0036RespDto.setErorcd(Optional.ofNullable(xdtz0036DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdtz0036RespDto.setErortx(Optional.ofNullable(xdtz0036DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdtz0036DataResultDto.getCode())) {
                xdtz0036DataRespDto = xdtz0036DataResultDto.getData();
                xdtz0036RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdtz0036RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdtz0036DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdtz0036DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, e.getMessage());
            xdtz0036RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdtz0036RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdtz0036RespDto.setDatasq(servsq);

        xdtz0036RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDTZ0036.key, DscmsEnum.TRADE_CODE_XDTZ0036.value, JSON.toJSONString(xdtz0036RespDto));
        return xdtz0036RespDto;
    }
}
