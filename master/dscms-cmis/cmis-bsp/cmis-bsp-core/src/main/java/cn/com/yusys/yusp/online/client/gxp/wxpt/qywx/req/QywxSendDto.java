package cn.com.yusys.yusp.online.client.gxp.wxpt.qywx.req;

public class QywxSendDto {
    private Hsiss hsiss;

    public Hsiss getHsiss() {
        return hsiss;
    }

    public void setHsiss(Hsiss hsiss) {
        this.hsiss = hsiss;
    }

    @Override
    public String toString() {
        return "QywxSendDto{" +
                "hsiss=" + hsiss +
                '}';
    }
}
