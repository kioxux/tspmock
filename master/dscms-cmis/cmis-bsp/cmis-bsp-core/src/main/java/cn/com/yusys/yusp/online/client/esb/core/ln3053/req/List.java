package cn.com.yusys.yusp.online.client.esb.core.ln3053.req;

/**
 * <br>
 * 0.2ZRC:2021/5/25 20:31:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/25 20:31
 * @since 2021/5/25 20:31
 */
public class List {
    java.util.List<Record> recordList;

    public java.util.List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(java.util.List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public String toString() {
        return "List{" +
                "recordList=" + recordList +
                '}';
    }
}
