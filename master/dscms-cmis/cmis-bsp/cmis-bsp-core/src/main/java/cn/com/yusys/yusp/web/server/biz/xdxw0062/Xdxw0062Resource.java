package cn.com.yusys.yusp.web.server.biz.xdxw0062;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdxw0062.req.Xdxw0062ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdxw0062.resp.Xdxw0062RespDto;
import cn.com.yusys.yusp.dto.server.xdxw0062.req.Xdxw0062DataReqDto;
import cn.com.yusys.yusp.dto.server.xdxw0062.resp.Xdxw0062DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizXwClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
 *
 * @author xll
 * @version 1.0
 */
@Api(tags = "XDXW0062:小微平台提交推送待办消息，信贷生成批复，客户经理继续审核")
@RestController
@RequestMapping("/api/dscms")
public class Xdxw0062Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdxw0062Resource.class);
	@Autowired
	private DscmsBizXwClientService dscmsBizXwClientService;
    /**
     * 交易码：xdxw0062
     * 交易描述：小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
     *
     * @param xdxw0062ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("小微平台提交推送待办消息，信贷生成批复，客户经理继续审核")
    @PostMapping("/xdxw0062")
    //@Idempotent({"xdcaxw0062", "#xdxw0062ReqDto.datasq"})
    protected @ResponseBody
	Xdxw0062RespDto xdxw0062(@Validated @RequestBody Xdxw0062ReqDto xdxw0062ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062ReqDto));
        Xdxw0062DataReqDto xdxw0062DataReqDto = new Xdxw0062DataReqDto();// 请求Data： 小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
        Xdxw0062DataRespDto xdxw0062DataRespDto = new Xdxw0062DataRespDto();// 响应Data：小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
		Xdxw0062RespDto xdxw0062RespDto = new Xdxw0062RespDto();
        //  此处包导入待确定 开始
		cn.com.yusys.yusp.dto.server.biz.xdxw0062.req.Data reqData = null; // 请求Data：小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
		cn.com.yusys.yusp.dto.server.biz.xdxw0062.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdxw0062.resp.Data();// 响应Data：小微平台提交推送待办消息，信贷生成批复，客户经理继续审核
        //  此处包导入待确定 结束
        try {
            // 从 xdxw0062ReqDto获取 reqData
            reqData = xdxw0062ReqDto.getData();
            // 将 reqData 拷贝到xdxw0062DataReqDto
            BeanUtils.copyProperties(reqData, xdxw0062DataReqDto);
            // 调用服务
            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062DataReqDto));
            ResultDto<Xdxw0062DataRespDto> xdxw0062DataResultDto = dscmsBizXwClientService.xdxw0062(xdxw0062DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062DataRespDto));
            // 从返回值中获取响应码和响应消息
            xdxw0062RespDto.setErorcd(Optional.ofNullable(xdxw0062DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdxw0062RespDto.setErortx(Optional.ofNullable(xdxw0062DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdxw0062DataResultDto.getCode())) {
                xdxw0062DataRespDto = xdxw0062DataResultDto.getData();
                xdxw0062RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdxw0062RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdxw0062DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdxw0062DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, e.getMessage());
            xdxw0062RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdxw0062RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdxw0062RespDto.setDatasq(servsq);

        xdxw0062RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDXW0062.key, DscmsEnum.TRADE_CODE_XDXW0062.value, JSON.toJSONString(xdxw0062RespDto));
        return xdxw0062RespDto;
    }
}
