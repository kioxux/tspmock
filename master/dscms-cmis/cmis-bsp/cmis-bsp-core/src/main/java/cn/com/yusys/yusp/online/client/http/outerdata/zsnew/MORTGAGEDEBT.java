package cn.com.yusys.yusp.online.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 动产抵押-被担保主债权信息
 */
@JsonPropertyOrder(alphabetic = true)
public class MORTGAGEDEBT implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "DEBT_EDATE")
    private String DEBT_EDATE;//	履行债务结束日期
    @JsonProperty(value = "DEBT_SDATE")
    private String DEBT_SDATE;//	履行债务开始日期
    @JsonProperty(value = "MAB_DEBT_AMT")
    private String MAB_DEBT_AMT;//	数额
    @JsonProperty(value = "MAB_DEBT_RANGE")
    private String MAB_DEBT_RANGE;//	担保范围
    @JsonProperty(value = "MAB_DEBT_RMK")
    private String MAB_DEBT_RMK;//	备注
    @JsonProperty(value = "MAB_DEBT_TYPE")
    private String MAB_DEBT_TYPE;//	种类
    @JsonProperty(value = "MAB_REGNO")
    private String MAB_REGNO;//	登记编号

    @JsonIgnore
    public String getDEBT_EDATE() {
        return DEBT_EDATE;
    }

    @JsonIgnore
    public void setDEBT_EDATE(String DEBT_EDATE) {
        this.DEBT_EDATE = DEBT_EDATE;
    }

    @JsonIgnore
    public String getDEBT_SDATE() {
        return DEBT_SDATE;
    }

    @JsonIgnore
    public void setDEBT_SDATE(String DEBT_SDATE) {
        this.DEBT_SDATE = DEBT_SDATE;
    }

    @JsonIgnore
    public String getMAB_DEBT_AMT() {
        return MAB_DEBT_AMT;
    }

    @JsonIgnore
    public void setMAB_DEBT_AMT(String MAB_DEBT_AMT) {
        this.MAB_DEBT_AMT = MAB_DEBT_AMT;
    }

    @JsonIgnore
    public String getMAB_DEBT_RANGE() {
        return MAB_DEBT_RANGE;
    }

    @JsonIgnore
    public void setMAB_DEBT_RANGE(String MAB_DEBT_RANGE) {
        this.MAB_DEBT_RANGE = MAB_DEBT_RANGE;
    }

    @JsonIgnore
    public String getMAB_DEBT_RMK() {
        return MAB_DEBT_RMK;
    }

    @JsonIgnore
    public void setMAB_DEBT_RMK(String MAB_DEBT_RMK) {
        this.MAB_DEBT_RMK = MAB_DEBT_RMK;
    }

    @JsonIgnore
    public String getMAB_DEBT_TYPE() {
        return MAB_DEBT_TYPE;
    }

    @JsonIgnore
    public void setMAB_DEBT_TYPE(String MAB_DEBT_TYPE) {
        this.MAB_DEBT_TYPE = MAB_DEBT_TYPE;
    }

    @JsonIgnore
    public String getMAB_REGNO() {
        return MAB_REGNO;
    }

    @JsonIgnore
    public void setMAB_REGNO(String MAB_REGNO) {
        this.MAB_REGNO = MAB_REGNO;
    }

    @Override
    public String toString() {
        return "MORTGAGEDEBT{" +
                "DEBT_EDATE='" + DEBT_EDATE + '\'' +
                ", DEBT_SDATE='" + DEBT_SDATE + '\'' +
                ", MAB_DEBT_AMT='" + MAB_DEBT_AMT + '\'' +
                ", MAB_DEBT_RANGE='" + MAB_DEBT_RANGE + '\'' +
                ", MAB_DEBT_RMK='" + MAB_DEBT_RMK + '\'' +
                ", MAB_DEBT_TYPE='" + MAB_DEBT_TYPE + '\'' +
                ", MAB_REGNO='" + MAB_REGNO + '\'' +
                '}';
    }
}
