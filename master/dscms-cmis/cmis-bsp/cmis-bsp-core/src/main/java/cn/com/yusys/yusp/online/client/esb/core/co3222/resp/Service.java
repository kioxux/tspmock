package cn.com.yusys.yusp.online.client.esb.core.co3222.resp;

import java.math.BigDecimal;

/**
 * 响应Service：抵质押物单笔查询
 *
 * @author leehuang
 * @version 1.0
 */
public class Service {
    private String erorcd;//响应码
    private String erortx;// 响应信息
    private String servsq;// 渠道流水
    private String datasq;//全局流水

    private String dzywbhao;//抵质押物编号
    private String dzywminc;//抵质押物名称
    private String yngyjigo;//营业机构
    private String zhngjigo;//账务机构
    private String dzywzlei;//抵质押物种类
    private String dizyfshi;//抵质押方式
    private String huowuhth;//货物合同号
    private String yewmingc;//业务名称
    private String syrkhhao;//受益人客户号
    private String syrkhmin;//受益人客户名
    private String syqrkehh;//所有权人客户号
    private String syqrkehm;//所有权人客户名
    private String huobdhao;//货币代号
    private BigDecimal minyjiaz;//名义价值
    private BigDecimal shijjiaz;//实际价值
    private BigDecimal pingjiaz;//评估价值
    private BigDecimal dizybilv;//抵质押比率
    private BigDecimal keyongje;//可用金额
    private BigDecimal yiyongje;//已用金额
    private String shengxrq;//生效日期
    private String daoqriqi;//到期日期
    private String dzywztai;//抵质押物状态
    private String glywbhao;//关联业务编号
    private Long mingxixh;//明细序号
    private String kaihriqi;//开户日期
    private String kaihguiy;//开户柜员
    private String jiaoyigy;//交易柜员
    private String jiaoyijg;//交易机构
    private String yewusx01;//记账余额属性
    private String zhaiyoms;//摘要
    private cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum1 lstnum1;//贷款账户质押控制表
    private cn.com.yusys.yusp.online.client.esb.core.co3222.resp.Lstnum2 lstnum2;//抵质押物担保关联

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDzywminc() {
        return dzywminc;
    }

    public void setDzywminc(String dzywminc) {
        this.dzywminc = dzywminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getDzywzlei() {
        return dzywzlei;
    }

    public void setDzywzlei(String dzywzlei) {
        this.dzywzlei = dzywzlei;
    }

    public String getDizyfshi() {
        return dizyfshi;
    }

    public void setDizyfshi(String dizyfshi) {
        this.dizyfshi = dizyfshi;
    }

    public String getHuowuhth() {
        return huowuhth;
    }

    public void setHuowuhth(String huowuhth) {
        this.huowuhth = huowuhth;
    }

    public String getYewmingc() {
        return yewmingc;
    }

    public void setYewmingc(String yewmingc) {
        this.yewmingc = yewmingc;
    }

    public String getSyrkhhao() {
        return syrkhhao;
    }

    public void setSyrkhhao(String syrkhhao) {
        this.syrkhhao = syrkhhao;
    }

    public String getSyrkhmin() {
        return syrkhmin;
    }

    public void setSyrkhmin(String syrkhmin) {
        this.syrkhmin = syrkhmin;
    }

    public String getSyqrkehh() {
        return syqrkehh;
    }

    public void setSyqrkehh(String syqrkehh) {
        this.syqrkehh = syqrkehh;
    }

    public String getSyqrkehm() {
        return syqrkehm;
    }

    public void setSyqrkehm(String syqrkehm) {
        this.syqrkehm = syqrkehm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getMinyjiaz() {
        return minyjiaz;
    }

    public void setMinyjiaz(BigDecimal minyjiaz) {
        this.minyjiaz = minyjiaz;
    }

    public BigDecimal getShijjiaz() {
        return shijjiaz;
    }

    public void setShijjiaz(BigDecimal shijjiaz) {
        this.shijjiaz = shijjiaz;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDizybilv() {
        return dizybilv;
    }

    public void setDizybilv(BigDecimal dizybilv) {
        this.dizybilv = dizybilv;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public BigDecimal getYiyongje() {
        return yiyongje;
    }

    public void setYiyongje(BigDecimal yiyongje) {
        this.yiyongje = yiyongje;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getDzywztai() {
        return dzywztai;
    }

    public void setDzywztai(String dzywztai) {
        this.dzywztai = dzywztai;
    }

    public String getGlywbhao() {
        return glywbhao;
    }

    public void setGlywbhao(String glywbhao) {
        this.glywbhao = glywbhao;
    }

    public Long getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Long mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getKaihriqi() {
        return kaihriqi;
    }

    public void setKaihriqi(String kaihriqi) {
        this.kaihriqi = kaihriqi;
    }

    public String getKaihguiy() {
        return kaihguiy;
    }

    public void setKaihguiy(String kaihguiy) {
        this.kaihguiy = kaihguiy;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public Lstnum1 getLstnum1() {
        return lstnum1;
    }

    public void setLstnum1(Lstnum1 lstnum1) {
        this.lstnum1 = lstnum1;
    }

    public Lstnum2 getLstnum2() {
        return lstnum2;
    }

    public void setLstnum2(Lstnum2 lstnum2) {
        this.lstnum2 = lstnum2;
    }

    @Override
    public String toString() {
        return "Service{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                ", dzywminc='" + dzywminc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", dzywzlei='" + dzywzlei + '\'' +
                ", dizyfshi='" + dizyfshi + '\'' +
                ", huowuhth='" + huowuhth + '\'' +
                ", yewmingc='" + yewmingc + '\'' +
                ", syrkhhao='" + syrkhhao + '\'' +
                ", syrkhmin='" + syrkhmin + '\'' +
                ", syqrkehh='" + syqrkehh + '\'' +
                ", syqrkehm='" + syqrkehm + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", minyjiaz=" + minyjiaz +
                ", shijjiaz=" + shijjiaz +
                ", pingjiaz=" + pingjiaz +
                ", dizybilv=" + dizybilv +
                ", keyongje=" + keyongje +
                ", yiyongje=" + yiyongje +
                ", shengxrq='" + shengxrq + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", dzywztai='" + dzywztai + '\'' +
                ", glywbhao='" + glywbhao + '\'' +
                ", mingxixh=" + mingxixh +
                ", kaihriqi='" + kaihriqi + '\'' +
                ", kaihguiy='" + kaihguiy + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", yewusx01='" + yewusx01 + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", lstnum1=" + lstnum1 +
                ", lstnum2=" + lstnum2 +
                '}';
    }
}
