package cn.com.yusys.yusp.online.client.esb.circp.fb1203.req;

/**
 * 请求Service：质押物金额覆盖校验
 *
 * @author chenyong
 * @version 1.0
 */
public class Fb1203ReqService {
    private Service service;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Fb1203ReqService{" +
                "service=" + service +
                '}';
    }
}
