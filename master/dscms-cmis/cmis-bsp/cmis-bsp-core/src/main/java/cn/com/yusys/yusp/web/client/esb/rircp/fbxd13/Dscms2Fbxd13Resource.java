package cn.com.yusys.yusp.web.client.esb.rircp.fbxd13;

import cn.com.yusys.yusp.bsp.communication.BspTemplate;
import cn.com.yusys.yusp.bsp.toolkit.reflect.BeanMapUtil;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.commons.util.collection.CollectionUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13.Fbxd13ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13.Fbxd13RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.req.Fbxd13ReqService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.resp.Fbxd13RespService;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.resp.List;
import cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.resp.Record;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 调用零售智能风控系统的接口处理类
 **/
@Api(tags = "BSP封装调用零售智能风控系统的接口处理类（fbxd13）")
@RestController
@RequestMapping("/api/dscms2rircp")
public class Dscms2Fbxd13Resource {
    private static Logger logger = LoggerFactory.getLogger(Dscms2Fbxd13Resource.class);
    private final BeanMapUtil beanMapUtil = new BeanMapUtil();

    /**
     * 交易码：fbxd13
     * 交易描述：查询客户核心编号，客户名称对应的放款和还款信息
     *
     * @param fbxd13ReqDto
     * @return
     * @throws Exception
     */
    @PostMapping("/fbxd13")
    protected @ResponseBody
    ResultDto<Fbxd13RespDto> fbxd13(@Validated @RequestBody Fbxd13ReqDto fbxd13ReqDto) throws Exception {
        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd13ReqDto));
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.req.Service reqService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.req.Service();
        cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.resp.Service respService = new cn.com.yusys.yusp.online.client.esb.rircp.fbxd13.resp.Service();
        Fbxd13ReqService fbxd13ReqService = new Fbxd13ReqService();
        Fbxd13RespService fbxd13RespService = new Fbxd13RespService();
        Fbxd13RespDto fbxd13RespDto = new Fbxd13RespDto();
        ResultDto<Fbxd13RespDto> fbxd13ResultDto = new ResultDto<Fbxd13RespDto>();
        Map<String, Object> context = new HashMap<>();
        Map<String, Object> result = new HashMap<>();
        try {
            //  将fbxd13ReqDto转换成reqService
            BeanUtils.copyProperties(fbxd13ReqDto, reqService);

            reqService.setPrcscd(EsbEnum.TRADE_CODE_FBXD13.key);//    交易码
            reqService.setServtp(EsbEnum.SERVTP_XDG.key);//    渠道
            reqService.setUserid(EsbEnum.USERID_RIRCP.key);//    柜员号
            reqService.setBrchno(EsbEnum.BRCHNO_RIRCP.key);//    部门号
            logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
            String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
            logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
            reqService.setServsq(servsq);//    渠道流水

            fbxd13ReqService.setService(reqService);
            // 将fbxd13ReqService转换成fbxd13ReqServiceMap
            Map fbxd13ReqServiceMap = beanMapUtil.beanToMap(fbxd13ReqService);
            context.put("tradeDataMap", fbxd13ReqServiceMap);
            logger.info(TradeLogConstants.CALL_ESB_BEGIN_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);
            result = BspTemplate.exchange(EsbEnum.SERVICE_NAME_ESB_TRADE_CLIENT.key, EsbEnum.TRADE_CODE_FBXD10.key, context);
            logger.info(TradeLogConstants.CALL_ESB_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value);

            // 从result中获取相关的值
            Map tradeDataMap = (Map) result.get("tradeDataMap");
            fbxd13RespService = beanMapUtil.mapToBean(tradeDataMap, Fbxd13RespService.class, Fbxd13RespService.class);
            respService = fbxd13RespService.getService();

            fbxd13ResultDto.setCode(Optional.ofNullable(respService.getErorcd()).orElse(SuccessEnum.CMIS_SUCCSESS.key));
            fbxd13ResultDto.setMessage(Optional.ofNullable(respService.getErortx()).orElse(SuccessEnum.CMIS_SUCCSESS.value));
            if (Objects.equals(SuccessEnum.SUCCESS.key, respService.getErorcd())) {
                //  将respService转换成Fbxd13RespDto
                BeanUtils.copyProperties(respService, fbxd13RespDto);
                List list = Optional.ofNullable(respService.getList()).orElse(new List());
                java.util.List<Record> records = list.getRecord();
                if (CollectionUtils.nonEmpty(records)) {
                    java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13.List> collect = records.parallelStream().map(record -> {
                        cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13.List temp = new cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13.List();
                        BeanUtils.copyProperties(record, temp);
                        return temp;
                    }).collect(Collectors.toList());
                    fbxd13RespDto.setList(collect);
                }
                fbxd13ResultDto.setCode(SuccessEnum.CMIS_SUCCSESS.key);
                fbxd13ResultDto.setMessage(SuccessEnum.SUCCESS.value);
            } else {
                fbxd13ResultDto.setCode(EpbEnum.EPB099999.key);
                fbxd13ResultDto.setMessage(respService.getErortx());
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, e.getMessage());
            fbxd13ResultDto.setCode(EpbEnum.EPB099999.key);//9999
            fbxd13ResultDto.setMessage(EpbEnum.EPB099999.value);//系统异常
        }
        fbxd13ResultDto.setData(fbxd13RespDto);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, EsbEnum.TRADE_CODE_FBXD10.key, EsbEnum.TRADE_CODE_FBXD10.value, JSON.toJSONString(fbxd13ResultDto));
        return fbxd13ResultDto;
    }
}
