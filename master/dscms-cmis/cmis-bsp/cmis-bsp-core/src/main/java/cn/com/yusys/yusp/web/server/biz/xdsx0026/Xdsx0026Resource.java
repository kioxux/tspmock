package cn.com.yusys.yusp.web.server.biz.xdsx0026;

import cn.com.yusys.yusp.commons.idempotent.annotation.Idempotent;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.commons.sequence.util.SequenceUtils;
import cn.com.yusys.yusp.constants.TradeLogConstants;
import cn.com.yusys.yusp.dto.server.biz.xdsx0026.req.Xdsx0026ReqDto;
import cn.com.yusys.yusp.dto.server.biz.xdsx0026.resp.Xdsx0026RespDto;
import cn.com.yusys.yusp.dto.server.xdsx0026.req.Xdsx0026DataReqDto;
import cn.com.yusys.yusp.dto.server.xdsx0026.resp.Xdsx0026DataRespDto;
import cn.com.yusys.yusp.enums.online.DscmsEnum;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.enums.returncode.EpbEnum;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import cn.com.yusys.yusp.service.DscmsBizSxClientService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

/**
 * 接口处理类:风控推送面签信息至信贷系统
 *
 * @author chenyong
 * @version 1.0
 */
@Api(tags = "XDSX0026:风控推送面签信息至信贷系统")
@RestController
@RequestMapping("/api/dscms")
public class Xdsx0026Resource {
    private static final Logger logger = LoggerFactory.getLogger(Xdsx0026Resource.class);
    @Autowired
    private DscmsBizSxClientService dscmsBizSxClientService;

    /**
     * 交易码：xdsx0026
     * 交易描述：风控推送面签信息至信贷系统
     *
     * @param xdsx0026ReqDto
     * @return
     * @throws Exception
     */
    @ApiOperation("风控推送面签信息至信贷系统")
    @PostMapping("/xdsx0026")
    //@Idempotent({"xdcasx0026", "#xdsx0026ReqDto.datasq"})
    protected @ResponseBody
    Xdsx0026RespDto xdsx0026(@Validated @RequestBody Xdsx0026ReqDto xdsx0026ReqDto) throws Exception {

        logger.info(TradeLogConstants.BSP_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026ReqDto));
        Xdsx0026DataReqDto xdsx0026DataReqDto = new Xdsx0026DataReqDto();// 请求Data： 风控推送面签信息至信贷系统
        Xdsx0026DataRespDto xdsx0026DataRespDto = new Xdsx0026DataRespDto();// 响应Data：风控推送面签信息至信贷系统
        Xdsx0026RespDto xdsx0026RespDto = new Xdsx0026RespDto();

        cn.com.yusys.yusp.dto.server.biz.xdsx0026.req.Data reqData = null; // 请求Data：风控推送面签信息至信贷系统
        cn.com.yusys.yusp.dto.server.biz.xdsx0026.resp.Data respData = new cn.com.yusys.yusp.dto.server.biz.xdsx0026.resp.Data();// 响应Data：风控推送面签信息至信贷系统
        try {
            // 从 xdsx0026ReqDto获取 reqData
            reqData = xdsx0026ReqDto.getData();
            // 将 reqData 拷贝到xdsx0026DataReqDto
            BeanUtils.copyProperties(reqData, xdsx0026DataReqDto);

            logger.info(TradeLogConstants.CALL_FEIGN_BEGIN_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataReqDto));
            ResultDto<Xdsx0026DataRespDto> xdsx0026DataResultDto = dscmsBizSxClientService.xdsx0026(xdsx0026DataReqDto);
            logger.info(TradeLogConstants.CALL_FEIGN_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026DataResultDto));
            // 从返回值中获取响应码和响应消息
            xdsx0026RespDto.setErorcd(Optional.ofNullable(xdsx0026DataResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key));
            xdsx0026RespDto.setErortx(Optional.ofNullable(xdsx0026DataResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value));

            if (Objects.equals(SuccessEnum.CMIS_SUCCSESS.key, xdsx0026DataResultDto.getCode())) {
                xdsx0026DataRespDto = xdsx0026DataResultDto.getData();
                xdsx0026RespDto.setErorcd(SuccessEnum.SUCCESS.key);
                xdsx0026RespDto.setErortx(SuccessEnum.SUCCESS.value);
                // 将 xdsx0026DataRespDto拷贝到 respData
                BeanUtils.copyProperties(xdsx0026DataRespDto, respData);
            }
        } catch (Exception e) {
            logger.info(TradeLogConstants.BSP_EXCEPTION_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, e.getMessage());
            xdsx0026RespDto.setErorcd(EpbEnum.EPB099999.key); // 9999
            xdsx0026RespDto.setErortx(EpbEnum.EPB099999.value);// 系统异常
        }
        logger.info(TradeLogConstants.CALL_SEQUENCE_BEGIN_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value);// 根据模板名称[{}|{}]获取下一个序列号开始
        String servsq = SequenceUtils.getSequence(EsbEnum.SERVTP_XDG.key);
        logger.info(TradeLogConstants.CALL_SEQUENCE_END_PREFIX_LOGGER, EsbEnum.SERVTP_XDG.key, EsbEnum.SERVTP_XDG.value, servsq);// 根据模板名称[{}|{}]获取下一个序列号值为:[{}]
        xdsx0026RespDto.setDatasq(servsq);

        xdsx0026RespDto.setData(respData);
        logger.info(TradeLogConstants.BSP_END_PREFIX_LOGGER, DscmsEnum.TRADE_CODE_XDSX0026.key, DscmsEnum.TRADE_CODE_XDSX0026.value, JSON.toJSONString(xdsx0026RespDto));
        return xdsx0026RespDto;
    }
}
