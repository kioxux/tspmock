/**
 * 响应Dto的get方法：贷款账户期供
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkzhqgGet {
    public static void main(String[] args) {
        Lstdkzhqg lstdkzhqg = new Lstdkzhqg(); //响应DTO:贷款账户期供
        String dkzhangh = lstdkzhqg.getDkzhangh();//贷款账号
        Integer benqqish = lstdkzhqg.getBenqqish();//本期期数
        Integer benqizqs = lstdkzhqg.getBenqizqs();//本期子期数
        String kxqdqirq = lstdkzhqg.getKxqdqirq();//宽限期到期日
        String schkriqi = lstdkzhqg.getSchkriqi();//上次还款日
        String xchkriqi = lstdkzhqg.getXchkriqi();//下次还款日
        BigDecimal chushibj = lstdkzhqg.getChushibj();//初始本金
        BigDecimal chushilx = lstdkzhqg.getChushilx();//初始利息
        BigDecimal benjinnn = lstdkzhqg.getBenjinnn();//本金
        BigDecimal ysyjlixi = lstdkzhqg.getYsyjlixi();//应收应计利息
        BigDecimal csyjlixi = lstdkzhqg.getCsyjlixi();//催收应计利息
        BigDecimal ysqianxi = lstdkzhqg.getYsqianxi();//应收欠息
        BigDecimal csqianxi = lstdkzhqg.getCsqianxi();//催收欠息
        BigDecimal ysyjfaxi = lstdkzhqg.getYsyjfaxi();//应收应计罚息
        BigDecimal csyjfaxi = lstdkzhqg.getCsyjfaxi();//催收应计罚息
        BigDecimal yshofaxi = lstdkzhqg.getYshofaxi();//应收罚息
        BigDecimal cshofaxi = lstdkzhqg.getCshofaxi();//催收罚息
        BigDecimal yingjifx = lstdkzhqg.getYingjifx();//应计复息
        BigDecimal fuxiiiii = lstdkzhqg.getFuxiiiii();//复息
        BigDecimal hexiaolx = lstdkzhqg.getHexiaolx();//核销利息
        BigDecimal sjlllxsr = lstdkzhqg.getSjlllxsr();//实际利率利息收入
        BigDecimal lxtiaozh = lstdkzhqg.getLxtiaozh();//利息调整
        BigDecimal sjyjlixi = lstdkzhqg.getSjyjlixi();//实际应计利息
        BigDecimal sjyjfaxi = lstdkzhqg.getSjyjfaxi();//实际应计罚息
        BigDecimal sjyjfuxi = lstdkzhqg.getSjyjfuxi();//实际应计复息
        BigDecimal sjllsjsr = lstdkzhqg.getSjllsjsr();//实际利率实际利息收入
        BigDecimal yingjitx = lstdkzhqg.getYingjitx();//应计贴息
        BigDecimal yingshtx = lstdkzhqg.getYingshtx();//应收贴息
        BigDecimal yingshfy = lstdkzhqg.getYingshfy();//应收费用
        BigDecimal yingshfj = lstdkzhqg.getYingshfj();//应收罚金
        String benqizht = lstdkzhqg.getBenqizht();//本期状态
        String yjfyjzht = lstdkzhqg.getYjfyjzht();//应计非应计状态
        String qigengzl = lstdkzhqg.getQigengzl();//期供种类
        String jzhqshrq = lstdkzhqg.getJzhqshrq();//基准起始日
        String jzhdqirq = lstdkzhqg.getJzhdqirq();//基准到期日
        Integer mingxixh = lstdkzhqg.getMingxixh();//明细序号
        Integer qgyqtnsh = lstdkzhqg.getQgyqtnsh();//期供逾期天数
    }
}  
