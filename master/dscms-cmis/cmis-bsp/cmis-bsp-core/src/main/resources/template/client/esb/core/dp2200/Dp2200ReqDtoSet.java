import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：保证金账户部提
 *
 * @author lihh
 * @version 1.0
 */
public class Dp2200ReqDtoSet {
    public static void main(String[] args) {
        Dp2200ReqDto dp2200ReqDto = new Dp2200ReqDto(); //请求DTO:保证金账户部提
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        dp2200ReqDto.setPrcscd(StringUtils.EMPTY);// 处理码
        dp2200ReqDto.setServtp(StringUtils.EMPTY);// 渠道
        dp2200ReqDto.setServsq(StringUtils.EMPTY);// 渠道流水
        dp2200ReqDto.setUserid(StringUtils.EMPTY);// 柜员号
        dp2200ReqDto.setBrchno(StringUtils.EMPTY);// 部门号
        dp2200ReqDto.setDatasq(StringUtils.EMPTY);// 全局流水
        dp2200ReqDto.setServdt(StringUtils.EMPTY);// 交易日期
        dp2200ReqDto.setServti(StringUtils.EMPTY);// 交易时间
        dp2200ReqDto.setKehuzhao(StringUtils.EMPTY);// 客户账号
        dp2200ReqDto.setZhhaoxuh(StringUtils.EMPTY);// 子账户序号
        dp2200ReqDto.setKehuzhlx(StringUtils.EMPTY);// 客户账号类型
        dp2200ReqDto.setZhhuzwmc(StringUtils.EMPTY);// 账户名称
        dp2200ReqDto.setHuobdaih(StringUtils.EMPTY);// 货币代号
        dp2200ReqDto.setChaohubz(StringUtils.EMPTY);// 账户钞汇标志
        dp2200ReqDto.setChapbhao(StringUtils.EMPTY);// 产品编号
        dp2200ReqDto.setZhanghao(StringUtils.EMPTY);// 负债账号
        dp2200ReqDto.setTonzbhao(StringUtils.EMPTY);// 通知编号
        dp2200ReqDto.setZhdaoqir(StringUtils.EMPTY);// 账户到期日
        dp2200ReqDto.setJiaoyije(new BigDecimal(0L));// 交易金额
        dp2200ReqDto.setPngzzlei(StringUtils.EMPTY);// 凭证种类
        dp2200ReqDto.setPngzphao(StringUtils.EMPTY);// 凭证批号
        dp2200ReqDto.setPngzxhao(StringUtils.EMPTY);// 凭证序号
        dp2200ReqDto.setXpizleix(StringUtils.EMPTY);// 新凭证种类
        dp2200ReqDto.setXpnzphao(StringUtils.EMPTY);// 新凭证批号
        dp2200ReqDto.setXpnzxhao(StringUtils.EMPTY);// 新凭证序号
        dp2200ReqDto.setZhfutojn(StringUtils.EMPTY);// 支付条件
        dp2200ReqDto.setJiaoymma(StringUtils.EMPTY);// 交易密码
        dp2200ReqDto.setZijinqux(StringUtils.EMPTY);// 资金去向
        dp2200ReqDto.setSkrkhuzh(StringUtils.EMPTY);// 收款人客户账号
        dp2200ReqDto.setSkrzhalx(StringUtils.EMPTY);// 收款人账号类型
        dp2200ReqDto.setSkrzhamc(StringUtils.EMPTY);// 收款人账户名称
        dp2200ReqDto.setSkzhxuho(StringUtils.EMPTY);// 收款人子账户序号
        dp2200ReqDto.setSkhobidh(StringUtils.EMPTY);// 收款人币种
        dp2200ReqDto.setSkchhubz(StringUtils.EMPTY);// 收款人钞汇
        dp2200ReqDto.setSfsfbzhi(StringUtils.EMPTY);// 是否收费标志
        dp2200ReqDto.setZhaiyodm(StringUtils.EMPTY);// 摘要代码
        dp2200ReqDto.setZhaiyoms(StringUtils.EMPTY);// 摘要描述
        dp2200ReqDto.setBeizhuuu(StringUtils.EMPTY);// 备注
        dp2200ReqDto.setXianzzbz(StringUtils.EMPTY);// 现转标志
        dp2200ReqDto.setMimazlei(StringUtils.EMPTY);// 密码种类
        dp2200ReqDto.setDfzhhxuh(StringUtils.EMPTY);// 对方子账户序号
        dp2200ReqDto.setDuifjgdm(StringUtils.EMPTY);// 对方金融机构代码
        dp2200ReqDto.setDuifjgmc(StringUtils.EMPTY);// 对方金融机构名称
        dp2200ReqDto.setDaokjine(new BigDecimal(0L));// 倒扣金额
        dp2200ReqDto.setLxzrkhzh(StringUtils.EMPTY);// 利息转入客户账号
        dp2200ReqDto.setZrzzhhxh(StringUtils.EMPTY);// 转入子账户序号
        dp2200ReqDto.setLixizjqx(StringUtils.EMPTY);// 利息资金去向
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
