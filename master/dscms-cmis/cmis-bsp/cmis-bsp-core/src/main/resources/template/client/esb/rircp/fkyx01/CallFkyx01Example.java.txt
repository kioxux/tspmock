import org.apache.commons.lang3.StringUtils;

public class CallFkyx01Example {
    // 1）注入：零售智能风控系统的接口
    @Autowired
    private Dscms2RircpClientService dscms2RircpClientService;

    public static void someMethod(String[] args) {
        // 2) 对应的业务逻辑处增加以下代码：
       Fkyx01ReqDto fkyx01ReqDto = new Fkyx01ReqDto(); //请求DTO:优享贷客户经理分配通知接口
       //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
       fkyx01ReqDto.setApp_no(StringUtils.EMPTY);// 申请批复流水号
       fkyx01ReqDto.setManager_id(StringUtils.EMPTY);// 客户经理号
       fkyx01ReqDto.setManager_name(StringUtils.EMPTY);// 客户经理名称
       fkyx01ReqDto.setOrg_id(StringUtils.EMPTY);// 机构ID
       fkyx01ReqDto.setOrg_name(StringUtils.EMPTY);// 机构名称
       fkyx01ReqDto.setManager_phone(StringUtils.EMPTY);// 客户经理电话
       //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
        //  完善后续逻辑
        ResultDto<Fkyx01RespDto> fkyx01ResultDto = dscms2RircpClientService.fkyx01(fkyx01ReqDto);
        String fkyx01Code = Optional.ofNullable(fkyx01ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String fkyx01Meesage = Optional.ofNullable(fkyx01ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Fkyx01RespDto fkyx01RespDto = null;
        if (Objects.equals(fkyx01Code, SuccessEnum.SUCCESS.key)) {
            //  获取相关的值并解析
            fkyx01RespDto = fkyx01ResultDto.getData();//响应DTO:优享贷客户经理分配通知接口
        } else {
            //  抛出错误异常
            throw new YuspException(fkyx01Code, fkyx01Meesage);
        }
    }
}
