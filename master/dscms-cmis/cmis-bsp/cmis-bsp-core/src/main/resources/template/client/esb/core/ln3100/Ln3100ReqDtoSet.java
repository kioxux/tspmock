import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3100ReqDtoSet {
    public static void main(String[] args) {
        Ln3100ReqDto ln3100ReqDto = new Ln3100ReqDto(); //请求DTO:根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        ln3100ReqDto.setDkzhangh(StringUtils.EMPTY);// 贷款账号
        ln3100ReqDto.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        ln3100ReqDto.setQishibis(new Integer(1));// 起始笔数
        ln3100ReqDto.setChxunbis(new Integer(1));// 查询笔数
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
