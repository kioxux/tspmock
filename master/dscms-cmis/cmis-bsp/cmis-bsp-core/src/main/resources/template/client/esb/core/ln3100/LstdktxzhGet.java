/**
 * 响应Dto的get方法：贷款贴息账户属性
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdktxzhGet {
    public static void main(String[] args) {
        Lstdktxzh lstdktxzh = new Lstdktxzh(); //响应DTO:贷款贴息账户属性
        String tiexizhh = lstdktxzh.getTiexizhh();//贴息账号
        String txzhhzxh = lstdktxzh.getTxzhhzxh();//贴息账号子序号
        String txzhhmch = lstdktxzh.getTxzhhmch();//贴息账户名称
        String shengxrq = lstdktxzh.getShengxrq();//生效日期
        BigDecimal tiexibil = lstdktxzh.getTiexibil();//贴息比率
        String fxjxbzhi = lstdktxzh.getFxjxbzhi();//贴息计复利标志
        String zdkoukbz = lstdktxzh.getZdkoukbz();//自动扣款标志
    }
}  
