import org.apache.commons.lang3.StringUtils;

public class CallHhmdkhExample {
    // 1）注入：风险预警系统
    @Autowired
    private Dscms2FxyjxtClientService dscms2FxyjxtClientService;

    public static void someMethod(String[] args) {
        // 2) 对应的业务逻辑处增加以下代码：
        HhmdkhReqDto hhmdkhReqDto = new HhmdkhReqDto(); //请求DTO:查询客户是否为黑灰名单客户
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        hhmdkhReqDto.setCustNm(StringUtils.EMPTY);// 客户名称
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
        //  完善后续逻辑
        ResultDto<HhmdkhRespDto> hhmdkhResultDto = dscms2ZnwdspxtClientService.hhmdkh(hhmdkhReqDto);
        String hhmdkhCode = Optional.ofNullable(hhmdkhResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String hhmdkhMeesage = Optional.ofNullable(hhmdkhResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        HhmdkhRespDto hhmdkhRespDto = null;
        if (Objects.equals(hhmdkhCode, SuccessEnum.SUCCESS.key)) {
            //  获取相关的值并解析
            hhmdkhRespDto = hhmdkhResultDto.getData(); //响应DTO:查询客户是否为黑灰名单客户
            String ifBlack = hhmdkhRespDto.getifBlack();//是否黑名单
            String ifGrey = hhmdkhRespDto.getifGrey();//是否灰名单
        } else {
            //  抛出错误异常
            throw new YuspException(hhmdkhCode, hhmdkhMeesage);
        }
    }
}
