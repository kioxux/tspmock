package template.client.esb.core.ln3054;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.req.Ln3054ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.req.LstStzf;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.resp.Ln3054RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class CallLn3054Example {

    // 1）注入：核心系统管理平台
    @Autowired
    private Dscms2CoreClientService dscms2CoreClientService;

    public static void someMethod(String[] args) {
        // 2) 对应的业务逻辑处增加以下代码：
        Ln3054ReqDto ln3054ReqDto = new Ln3054ReqDto(); //请求DTO:调整贷款还款方式
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        ln3054ReqDto.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        ln3054ReqDto.setDkzhangh(StringUtils.EMPTY);// 贷款账号
        ln3054ReqDto.setKehuhaoo(StringUtils.EMPTY);// 客户号
        ln3054ReqDto.setKehmingc(StringUtils.EMPTY);// 客户名称
        ln3054ReqDto.setQixiriqi(StringUtils.EMPTY);// 起息日期
        ln3054ReqDto.setDaoqriqi(StringUtils.EMPTY);// 到期日期
        ln3054ReqDto.setDkqixian(StringUtils.EMPTY);// 贷款期限(月)
        ln3054ReqDto.setHuobdhao(StringUtils.EMPTY);// 货币代号
        ln3054ReqDto.setHetongje(new BigDecimal(0L));// 合同金额
        ln3054ReqDto.setJiejuuje(new BigDecimal(0L));// 借据金额
        ln3054ReqDto.setHuankzhh(StringUtils.EMPTY);// 还款账号
        ln3054ReqDto.setHkzhhzxh(StringUtils.EMPTY);// 还款账号子序号
        ln3054ReqDto.setHuankfsh(StringUtils.EMPTY);// 还款方式
        ln3054ReqDto.setDechligz(StringUtils.EMPTY);// 等额处理规则
        ln3054ReqDto.setLeijinzh(new BigDecimal(0L));// 累进值
        ln3054ReqDto.setLeijqjsh(new Integer(1));// 累进区间期数
        ln3054ReqDto.setDuophkbz(StringUtils.EMPTY);// 多频率还款标志
        ln3054ReqDto.setHkzhouqi(StringUtils.EMPTY);// 还款周期
        ln3054ReqDto.setHuanbzhq(StringUtils.EMPTY);// 还本周期
        ln3054ReqDto.setYuqhkzhq(StringUtils.EMPTY);// 逾期还款周期
        ln3054ReqDto.setHkqixian(StringUtils.EMPTY);// 还款期限(月)
        ln3054ReqDto.setBaoliuje(new BigDecimal(0L));// 保留金额
        ln3054ReqDto.setDhkzhhbz(StringUtils.EMPTY);// 多还款账户标志
        ln3054ReqDto.setDbdkkksx(new Integer(1));// 多笔贷款扣款顺序

        Lstdkhkzh lstdkhkzh = new Lstdkhkzh(); //请求DTO:多还款账户
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        lstdkhkzh.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        lstdkhkzh.setXuhaoooo(new Integer(1));// 序号
        lstdkhkzh.setYouxianj(new Integer(1));// 优先级
        lstdkhkzh.setHuankzhh(StringUtils.EMPTY);// 还款账号
        lstdkhkzh.setHkzhhzxh(StringUtils.EMPTY);// 还款账号子序号
        lstdkhkzh.setHkzhhmch(StringUtils.EMPTY);// 还款账户名称
        lstdkhkzh.setHkzhhzhl(StringUtils.EMPTY);// 还款账户种类
        lstdkhkzh.setHkzhhgze(StringUtils.EMPTY);// 还款账户规则
        lstdkhkzh.setHkjshzhl(StringUtils.EMPTY);// 还款基数种类
        lstdkhkzh.setHuankbli(new BigDecimal(0L));// 还款比例
        lstdkhkzh.setShengxrq(StringUtils.EMPTY);// 生效日期
        lstdkhkzh.setDaoqriqi(StringUtils.EMPTY);// 到期日期
        ln3054ReqDto.setLstdkhkzh(Arrays.asList(lstdkhkzh));
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
        ResultDto<Ln3054RespDto> ln3054ResultDto = dscms2CoreClientService.ln3054(ln3054ReqDto);
        String ln3054Code = Optional.ofNullable(ln3054ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3054Meesage = Optional.ofNullable(ln3054ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3054RespDto ln3054RespDto = null; //响应DTO:支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还
        if (Objects.equals(ln3054Code, SuccessEnum.SUCCESS.key)) {
            //  获取相关的值并解析
            ln3054RespDto = ln3054ResultDto.getData();//响应DTO:手工指定归还某项利息金额，不按既定顺序归还
            String code = ln3054ResultDto.getCode();//返回码
            String message = ln3054ResultDto.getMessage();//返回提示
            // TODO 从Ln3054RespDtoGet.java中获取
            String dkjiejuh = ln3054RespDto.getDkjiejuh();//贷款借据号
            String huankzhh = ln3054RespDto.getHuankzhh();//还款账号
            String hkzhhzxh = ln3054RespDto.getHkzhhzxh();//还款账号子序号
            String huankfsh = ln3054RespDto.getHuankfsh();//还款方式
            String dechligz = ln3054RespDto.getDechligz();//等额处理规则
            BigDecimal leijinzh = ln3054RespDto.getLeijinzh();//累进值
            Integer leijqjsh = ln3054RespDto.getLeijqjsh();//累进区间期数
            String duophkbz = ln3054RespDto.getDuophkbz();//多频率还款标志
            String hkzhouqi = ln3054RespDto.getHkzhouqi();//还款周期
            String huanbzhq = ln3054RespDto.getHuanbzhq();//还本周期
            String yuqhkzhq = ln3054RespDto.getYuqhkzhq();//逾期还款周期
            String hkqixian = ln3054RespDto.getHkqixian();//还款期限(月)
            BigDecimal baoliuje = ln3054RespDto.getBaoliuje();//保留金额
            String dhkzhhbz = ln3054RespDto.getDhkzhhbz();//多还款账户标志
            String jiaoyirq = ln3054RespDto.getJiaoyirq();//交易日期
            String jiaoyijg = ln3054RespDto.getJiaoyijg();//交易机构
            String jiaoyigy = ln3054RespDto.getJiaoyigy();//交易柜员
            String jiaoyils = ln3054RespDto.getJiaoyils();//交易流水
        } else {
            //  抛出错误异常
            throw new YuspException(ln3054Code, ln3054Meesage);
        }
    }
}
