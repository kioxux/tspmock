/**
 * 响应Dto的get方法：对客借据与内部借据关联关系
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdknbglGet {
    public static void main(String[] args) {
        Lstdknbgl lstdknbgl = new Lstdknbgl(); //响应DTO:对客借据与内部借据关联关系
        String nbjiejuh = lstdknbgl.getNbjiejuh();//内部借据号
        String nbjjxzhi = lstdknbgl.getNbjjxzhi();//内部借据性质
    }
}  
