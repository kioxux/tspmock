/**
 * 响应Dto的get方法：贷款收费事件
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdksfsjGet {
    public static void main(String[] args) {
        Lstdksfsj lstdksfsj = new Lstdksfsj(); //响应DTO:贷款收费事件
        String shoufshj = lstdksfsj.getShoufshj();//收费事件
        String shfshjmc = lstdksfsj.getShfshjmc();//收费事件名称
        String shoufzhl = lstdksfsj.getShoufzhl();//收费种类
        String shoufdma = lstdksfsj.getShoufdma();//收费代码
        String shfdmamc = lstdksfsj.getShfdmamc();//收费代码名称
        BigDecimal shoufjee = lstdksfsj.getShoufjee();//收费金额/比例
        String fufeizhh = lstdksfsj.getFufeizhh();//付费账号
        String ffzhhzxh = lstdksfsj.getFfzhhzxh();//付费账号子序号
        String sfrzhzhh = lstdksfsj.getSfrzhzhh();//收费入账账号
        String sfrzhzxh = lstdksfsj.getSfrzhzxh();//收费入账账号子序号
    }
}  
