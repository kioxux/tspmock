/**
 * 响应Dto的get方法：贷款多委托人账户
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkwtxxGet {
    public static void main(String[] args) {
        Lstdkwtxxo lstdkwtxx = new Lstdkwtxx(); //响应DTO:贷款多委托人账户
        String bhchzibz = lstdkwtxx.getBhchzibz();//本行出资标志
        String bhjiejuh = lstdkwtxx.getBhjiejuh();//本行借据号
        String bhzhaobz = lstdkwtxx.getBhzhaobz();//本行账号标志
        String hkzjhzfs = lstdkwtxx.getHkzjhzfs();//还款资金划转方式
        String zhkaihhh = lstdkwtxx.getZhkaihhh();//账户开户行行号
        String zhkaihhm = lstdkwtxx.getZhkaihhm();//账户开户行行名
        String wtrkehuh = lstdkwtxx.getWtrkehuh();//委托人客户号
        String wtrckuzh = lstdkwtxx.getWtrckuzh();//委托人存款账号
        String wtckzhao = lstdkwtxx.getWtckzhao();//委托存款账号
        String baozjzhh = lstdkwtxx.getBaozjzhh();//保证金账号
        BigDecimal weituoje = lstdkwtxx.getWeituoje();//委托金额
        String wtrckzxh = lstdkwtxx.getWtrckzxh();//委托人存款账号子序号
        String wtckzixh = lstdkwtxx.getWtckzixh();//委托存款账号子序号
        String baozjzxh = lstdkwtxx.getBaozjzxh();//保证金账号子序号
        String wtrmingc = lstdkwtxx.getWtrmingc();//委托人名称
    }
}  
