import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：优享贷客户经理分配通知接口
 *
 * @author code-generator
 * @version 1.0
 */
public class Fkyx01ReqDtoSet {
    public static void main(String[] args) {
        Fkyx01ReqDto fkyx01ReqDto = new Fkyx01ReqDto(); //请求DTO:优享贷客户经理分配通知接口
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        fkyx01ReqDto.setApp_no(StringUtils.EMPTY);// 申请批复流水号
        fkyx01ReqDto.setManager_id(StringUtils.EMPTY);// 客户经理号
        fkyx01ReqDto.setManager_name(StringUtils.EMPTY);// 客户经理名称
        fkyx01ReqDto.setOrg_id(StringUtils.EMPTY);// 机构ID
        fkyx01ReqDto.setOrg_name(StringUtils.EMPTY);// 机构名称
        fkyx01ReqDto.setManager_phone(StringUtils.EMPTY);// 客户经理电话
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
