package template.client.esb.core.ln3032;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.Ln3032ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.LstStzf;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.Ln3032RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class CallCwm001Example {
        // 1）注入：BSP封装调用押品权证系统的接口
        @Autowired
        private Dscms2YpqzxtClientService dscms2YpqzxtClientService;

        public static void main(String[] args) {
            // 2) 对应的业务逻辑处增加以下代码：
            Cwm001ReqDto cwm001ReqDto = new Cwm001ReqDto();
            //  StringUtils.EMPTY的实际值待确认 开始
            cwm001ReqDto.setIsyd(StringUtils.EMPTY); // 是否异地支行
            // cwm001ReqDto其他值赋予对应的业务值
            //  StringUtils.EMPTY的实际值待确认 结束
            //  完善后续逻辑
            ResultDto<Cwm001RespDto> cwm001ResultDto = dscms2YpqzxtClientService.cwm001(cwm001ReqDto);
            String cwm001Code = Optional.ofNullable(cwm001ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String cwm001Meesage = Optional.ofNullable(cwm001ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Cwm001RespDto cwm001RespDto = null;
            if (Objects.equals(cwm001Code, SuccessEnum.SUCCESS.key)) {
                //  获取相关的值并解析
                cwm001RespDto = cwm001ResultDto.getData();
            } else {
                //  抛出错误异常
                throw new YuspException(cwm001Code, cwm001Meesage);
            }

        }
}
