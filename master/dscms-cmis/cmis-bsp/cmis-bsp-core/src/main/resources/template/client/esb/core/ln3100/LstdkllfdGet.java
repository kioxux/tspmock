/**
 * 响应Dto的get方法：贷款利率分段登记
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkllfdGet {
    public static void main(String[] args) {
        Lstdkllfd lstdkllfd = new Lstdkllfd(); //响应DTO:贷款利率分段登记
        String qishriqi = lstdkllf.getQishriqi();//起始日期
        String daoqriqi = lstdkllf.getDaoqriqi();//到期日期
        String lilvleix = lstdkllf.getLilvleix();//利率类型
        String lilvtzfs = lstdkllf.getLilvtzfs();//利率调整方式
        String lilvtzzq = lstdkllf.getLilvtzzq();//利率调整周期
        String lilvfdfs = lstdkllf.getLilvfdfs();//利率浮动方式
        BigDecimal lilvfdzh = lstdkllf.getLilvfdzh();//利率浮动值
        BigDecimal zhchlilv = lstdkllf.getZhchlilv();//正常利率
        String zclilvbh = lstdkllf.getZclilvbh();//正常利率编号
    }
}  
