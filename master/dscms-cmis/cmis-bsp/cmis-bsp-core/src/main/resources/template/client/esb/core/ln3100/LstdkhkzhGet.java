/**
 * 响应Dto的get方法：贷款多还款账户
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkhkzhGet {
    public static void main(String[] args) {
        Lstdkhkzh lstdkhkzh = new Lstdkhkzh(); //响应DTO:贷款多还款账户
        Integer youxianj = lstdkhkzh.getYouxianj();//优先级
        String hkzhhmch = lstdkhkzh.getHkzhhmch();//还款账户名称
        String hkzhhzhl = lstdkhkzh.getHkzhhzhl();//还款账户种类
        String hkzhhgze = lstdkhkzh.getHkzhhgze();//还款账户规则
        String hkjshzhl = lstdkhkzh.getHkjshzhl();//还款基数种类
        BigDecimal huankbli = lstdkhkzh.getHuankbli();//还款比例
    }
}  
