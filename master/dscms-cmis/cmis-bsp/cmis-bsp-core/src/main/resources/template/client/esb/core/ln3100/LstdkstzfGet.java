/**
 * 响应Dto的get方法：贷款受托支付
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkstzfGet {
    public static void main(String[] args) {
        Lstdkstzf lstdkstzf = new Lstdkstzf(); //响应DTO:贷款受托支付
        String qudaohao = lstdkstzf.getQudaohao();//发布系统/渠道
        BigDecimal stzfjine = lstdkstzf.getStzfjine();//受托金额
        String zjlyzhmc = lstdkstzf.getZjlyzhmc();//资金来源账号名称
        String zjzrzhmc = lstdkstzf.getZjzrzhmc();//资金转入账号名称
        String dfzhhzhl = lstdkstzf.getDfzhhzhl();//对方账号种类
        String dfzhhkhh = lstdkstzf.getDfzhhkhh();//对方账号开户行
        String dfzhkhhm = lstdkstzf.getDfzhkhhm();//对方账号开户行名
        String dfzhangh = lstdkstzf.getDfzhangh();//对方账号
        String dfzhhzxh = lstdkstzf.getDfzhhzxh();//对方账号子序号
        String dfzhhmch = lstdkstzf.getDfzhhmch();//对方账号名称
        String stzffshi = lstdkstzf.getStzffshi();//受托支付方式
        String djiebhao = lstdkstzf.getDjiebhao();//冻结编号
        String stzfriqi = lstdkstzf.getStzfriqi();//受托支付日期
        String stzfclzt = lstdkstzf.getStzfclzt();//受托支付处理状态
        String beizhuxx = lstdkstzf.getBeizhuxx();//备注
    }
}  
