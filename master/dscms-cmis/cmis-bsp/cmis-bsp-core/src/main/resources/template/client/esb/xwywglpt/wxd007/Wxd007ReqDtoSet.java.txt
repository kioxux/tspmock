import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：请求小V平台综合决策管理列表查询
 *
 * @author code-generator
 * @version 1.0
 */
public class Wxd007ReqDtoSet {
    public static void main(String[] args) {
        Wxd007ReqDto wxd007ReqDto = new Wxd007ReqDto(); //请求DTO:请求小V平台综合决策管理列表查询
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        wxd007ReqDto.setIndcertid(StringUtils.EMPTY);// 证件号码
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
