/**
 * 响应Dto的get方法：贷款指定还本计划
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkhbjhGet {
    public static void main(String[] args) {
        Lstdkhbjh lstdkhbjh = new Lstdkhbjh(); //响应DTO:贷款指定还本计划
        String dzhhkzhl = lstdkhbjh.getDzhhkzhl();//定制还款种类
        String dzhkriqi = lstdkhbjh.getDzhkriqi();//定制还款日期
        String xzuetqhk = lstdkhbjh.getXzuetqhk();//需足额提前还款
        BigDecimal huanbjee = lstdkhbjh.getHuanbjee();//还本金额
        String huankzhh = lstdkhbjh.getHuankzhh();//还款账号
        String hkzhhzxh = lstdkhbjh.getHkzhhzxh();//还款账号子序号
        String tqhkhxfs = lstdkhbjh.getTqhkhxfs();//还息方式
        String hkyujrgz = lstdkhbjh.getHkyujrgz();//还款遇假日规则
        String sfyxkuxq = lstdkhbjh.getSfyxkuxq();//是否有宽限期
        Integer kuanxqts = lstdkhbjh.getKuanxqts();//宽限期天数
        String kxqjjrgz = lstdkhbjh.getKxqjjrgz();//宽限期节假日规则
    }
}  
