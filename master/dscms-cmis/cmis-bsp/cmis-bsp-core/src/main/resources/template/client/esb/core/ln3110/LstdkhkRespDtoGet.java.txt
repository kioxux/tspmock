/**
 * 响应Dto的get方法：还款计划
 *
 * @author code-generator
 * @version 1.0
 */
public class LstdkhkRespDtoGet {
    public static void main(String[] args) {
        LstdkhkRespDto lstdkhkRespDto = new LstdkhkRespDto(); //响应DTO:还款计划
        String erorcd = lstdkhkRespDto.geterorcd();//响应码
        String erortx = lstdkhkRespDto.geterortx();//响应信息
        Integer benqqish = lstdkhkRespDto.getbenqqish();//本期期数
        String qixiriqi = lstdkhkRespDto.getqixiriqi();//起息日期
        String huankriq = lstdkhkRespDto.gethuankriq();//还款日期
        String kxqdqirq = lstdkhkRespDto.getkxqdqirq();//宽限期到期日
        BigDecimal jixibenj = lstdkhkRespDto.getjixibenj();//计息本金
        BigDecimal leijcslx = lstdkhkRespDto.getleijcslx();//累计产生利息
        BigDecimal meiqhkze = lstdkhkRespDto.getmeiqhkze();//每期还款总额
        BigDecimal zhanghye = lstdkhkRespDto.getzhanghye();//账户余额
    }
}  
