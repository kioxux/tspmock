/**
 * 响应Dto的get方法：贷款账户质押信息
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkzhzyGet {
    public static void main(String[] args) {
        Lstdkzhzy lstdkzhzy = new Lstdkzhzy(); //响应DTO:贷款账户质押信息
        String shfbhzhh = lstdkzhzy.getShfbhzhh();//是否本行账号
        String kehuzhao = lstdkzhzy.getKehuzhao();//客户账号
        String khzhhzxh = lstdkzhzy.getKhzhhzxh();//客户账号子序号
        String zhjzhyfs = lstdkzhzy.getZhjzhyfs();//质押方式
        String huobdhao = lstdkzhzy.getHuobdhao();//货币代号
        BigDecimal zhiyajee = lstdkzhzy.getZhiyajee();//质押金额
        String zyzhleix = lstdkzhzy.getZyzhleix();//质押账户类型
        String zhydaoqr = lstdkzhzy.getZhydaoqr();//质押到期日
        String dzywbhao = lstdkzhzy.getDzywbhao();//抵质押物编号
    }
}  
