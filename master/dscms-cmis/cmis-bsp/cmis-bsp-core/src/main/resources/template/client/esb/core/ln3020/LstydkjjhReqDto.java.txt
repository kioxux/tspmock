/**                
 * 请求Dto：借新还旧列表
 * @author code-generator
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class LstydkjjhReqDto implements Serializable {
	   private static final long serialVersionUID = 1L;
	 @JsonProperty(value = "ydkjiejh")
	private String ydkjiejh;//原贷款借据号
	 @JsonProperty(value = "jxhuanbj")
	private BigDecimal jxhuanbj;//借新本金
	 @JsonProperty(value = "zihuanbj")
	private BigDecimal zihuanbj;//自还本金
	 @JsonProperty(value = "zihuanlx")
	private BigDecimal zihuanlx;//自还利息
	public String  getYdkjiejh() { return ydkjiejh; }
	public void setYdkjiejh(String ydkjiejh ) { this.ydkjiejh = ydkjiejh;}
	public BigDecimal  getJxhuanbj() { return jxhuanbj; }
	public void setJxhuanbj(BigDecimal jxhuanbj ) { this.jxhuanbj = jxhuanbj;}
	public BigDecimal  getZihuanbj() { return zihuanbj; }
	public void setZihuanbj(BigDecimal zihuanbj ) { this.zihuanbj = zihuanbj;}
	public BigDecimal  getZihuanlx() { return zihuanlx; }
	public void setZihuanlx(BigDecimal zihuanlx ) { this.zihuanlx = zihuanlx;}
    @Override   
    public String toString() {
	    return "LstydkjjhReqDto{" +
	"ydkjiejh='" + ydkjiejh+ '\'' +
	"jxhuanbj='" + jxhuanbj+ '\'' +
	"zihuanbj='" + zihuanbj+ '\'' +
	"zihuanlx='" + zihuanlx+ '\'' +
 '}';
    }
}  
