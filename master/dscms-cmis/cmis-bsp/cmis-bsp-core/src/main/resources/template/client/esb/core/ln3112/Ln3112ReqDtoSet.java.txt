import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：贷款展期查询
 *
 * @author lihh
 * @version 1.0
 */
public class Ln3112ReqDtoSet {
    public static void main(String[] args) {
        Ln3112ReqDto ln3112ReqDto = new Ln3112ReqDto(); //请求DTO:贷款展期查询
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        ln3112ReqDto.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        ln3112ReqDto.setDkzhangh(StringUtils.EMPTY);// 贷款账号
        ln3112ReqDto.setDkzhqizt(StringUtils.EMPTY);// 贷款展期状态
        ln3112ReqDto.setQishibis(new Integer(1));// 起始笔数
        ln3112ReqDto.setChxunbis(new Integer(1));// 查询笔数
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
