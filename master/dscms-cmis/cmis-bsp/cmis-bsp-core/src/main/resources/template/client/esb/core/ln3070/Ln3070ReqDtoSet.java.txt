import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：交易说明
 *
 * @author chenyong
 * @version 1.0
 */
public class Ln3070ReqDtoSet {
    public static void main(String[] args) {
        Ln3070ReqDto ln3070ReqDto = new Ln3070ReqDto(); //请求DTO:交易说明
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        ln3070ReqDto.setJiaoyirq(StringUtils.EMPTY);// 交易日期
        ln3070ReqDto.setJiaoyils(StringUtils.EMPTY);// 交易流水
        ln3070ReqDto.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        ln3070ReqDto.setJiaoyijg(StringUtils.EMPTY);// 交易机构
        ln3070ReqDto.setJiaoyigy(StringUtils.EMPTY);// 交易柜员
        ln3070ReqDto.setJiaoyima(StringUtils.EMPTY);// 交易码
        ln3070ReqDto.setWjmingch(StringUtils.EMPTY);// 文件名
        ln3070ReqDto.setJychlizt(StringUtils.EMPTY);// 交易处理状态
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
