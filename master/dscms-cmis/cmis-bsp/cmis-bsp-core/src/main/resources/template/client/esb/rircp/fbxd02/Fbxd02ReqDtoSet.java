import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：为正式客户更新信贷客户信息手机号码
 *
 * @author chenyong
 * @version 1.0
 */
public class Fbxd02ReqDtoSet {
    public static void main(String[] args) {
        Fbxd02ReqDto fbxd02ReqDto = new Fbxd02ReqDto(); //请求DTO:为正式客户更新信贷客户信息手机号码
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        fbxd02ReqDto.setCus_id(StringUtils.EMPTY);// 客户号
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
