/**
 * 响应Dto的get方法：保证金账户部提
 *
 * @author lihh
 * @version 1.0
 */
public class Dp2200RespDtoGet {
    public static void main(String[] args) {
        Dp2200RespDto dp2200RespDto = new Dp2200RespDto(); //响应DTO:保证金账户部提
        String erorcd = dp2200RespDto.getErorcd();//错误码
        String erortx = dp2200RespDto.getErortx();//错误描述
        String kehuzhao = dp2200RespDto.getKehuzhao();//客户账号
        String kehuzhlx = dp2200RespDto.getKehuzhlx();//客户账号类型
        String zhhaoxuh = dp2200RespDto.getZhhaoxuh();//子账户序号
        String huobdaih = dp2200RespDto.getHuobdaih();//货币代号
        String chaohubz = dp2200RespDto.getChaohubz();//账户钞汇标志
        String zhhuzwmc = dp2200RespDto.getZhhuzwmc();//账户名称
        BigDecimal jiaoyije = dp2200RespDto.getJiaoyije();//交易金额
        String zhdaoqir = dp2200RespDto.getZhdaoqir();//账户到期日
        String guiylius = dp2200RespDto.getGuiylius();//柜员流水号
        String jiaoyirq = dp2200RespDto.getJiaoyirq();//交易日期
        String zijinqux = dp2200RespDto.getZijinqux();//资金去向
        String xpizleix = dp2200RespDto.getXpizleix();//新凭证种类
        String xpnzxhao = dp2200RespDto.getXpnzxhao();//新凭证序号
        String skrkhuzh = dp2200RespDto.getSkrkhuzh();//收款人客户账号
        String skrzhamc = dp2200RespDto.getSkrzhamc();//收款人账户名称
        String xiaozxuh = dp2200RespDto.getXiaozxuh();//销账序号
        BigDecimal lixilixi = dp2200RespDto.getLixilixi();//利息
        BigDecimal lixishui = dp2200RespDto.getLixishui();//利息税
        BigDecimal shijzqje = dp2200RespDto.getShijzqje();//实际支取金额
        String chapbhao = dp2200RespDto.getChapbhao();//产品编号
        String chanpshm = dp2200RespDto.getChanpshm();//产品说明
        String zhhufenl = dp2200RespDto.getZhhufenl();//账户分类
        String zhshuxin = dp2200RespDto.getZhshuxin();//账户属性
        String cunqiiii = dp2200RespDto.getCunqiiii();//存期
        String qixiriqi = dp2200RespDto.getQixiriqi();//起息日期
        BigDecimal shijlilv = dp2200RespDto.getShijlilv();//实际利率
        BigDecimal zhanghye = dp2200RespDto.getZhanghye();//账户余额
        BigDecimal daokjine = dp2200RespDto.getDaokjine();//倒扣金额
        String khhhming = dp2200RespDto.getKhhhming();//单位开户行行名
        String cunkzlei = dp2200RespDto.getCunkzlei();//存款种类
        String zhcunfsh = dp2200RespDto.getZhcunfsh();//转存方式
        String chapqcmc = dp2200RespDto.getChapqcmc();//产品期次名称
        String chapqcbh = dp2200RespDto.getChapqcbh();//产品期次编号
        BigDecimal kaihjine = dp2200RespDto.getKaihjine();//开户金额
        String lixizjqx = dp2200RespDto.getLixizjqx();//利息资金去向
    }
}  
