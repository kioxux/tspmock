/**
 * 响应Dto的get方法：从风控获取客户详细信息
 * @author zhugenrong
 * @version 1.0             
 */      
public class Fbxd03RespDtoGet {
	  public static void main(String[] args) {
	     Fbxd03RespDto fbxd03RespDto = new Fbxd03RespDto(); //响应DTO:从风控获取客户详细信息
	 String sex = fbxd03RespDto.getSex();//性別
	 String birthday = fbxd03RespDto.getBirthday();//出生日期
	 String ifwedd = fbxd03RespDto.getIfwedd();//婚姻状况
	 String ifeduc = fbxd03RespDto.getIfeduc();//学历
	 String addr = fbxd03RespDto.getAddr();//联系地址
	 String zpcd = fbxd03RespDto.getZpcd();//联系地址邮政编码
	 String indiv_occ = fbxd03RespDto.getIndiv_occ();//职业
	 String indiv_com_name = fbxd03RespDto.getIndiv_com_name();//工作单位
	 String indiv_com_job_ttl = fbxd03RespDto.getIndiv_com_job_ttl();//职务
	 String siaddr = fbxd03RespDto.getSiaddr();//现居住地
	 String sizpcd = fbxd03RespDto.getSizpcd();//现居住地邮政编码
	 String live_status = fbxd03RespDto.getLive_status();//居住状态
	 String agri_flg = fbxd03RespDto.getAgri_flg();//是否农户
	 String ifmobl = fbxd03RespDto.getIfmobl();//手机号
 }
}  
