/**
 * 响应Dto的get方法：贷款账户保证信息
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkzhbzGet {
    public static void main(String[] args) {
        Lstdkzhbz lstdkzhbz = new Lstdkzhbz(); //响应DTO:贷款账户保证信息
        String bzrkehuh = lstdkzhbz.getBzrkehuh();//保证人客户号
        String baozhfsh = lstdkzhbz.getBaozhfsh();//保证方式
        BigDecimal baozjine = lstdkzhbz.getBaozjine();//保证金额
        String beizhuuu = lstdkzhbz.getBeizhuuu();//备注信息
        String guanlzht = lstdkzhbz.getGuanlzht();//关联状态
        String danbzhao = lstdkzhbz.getDanbzhao();//担保账号
        String dbzhzxuh = lstdkzhbz.getDbzhzxuh();//担保账号子序号
    }
}  
