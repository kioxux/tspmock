package template.client.esb.core.ln3032;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.Ln3032ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.LstStzf;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.Ln3032RespDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class CallG00202Example {
   // 1）注入：BSP封装调用ECIF系统的接口
     @Autowired
     private Dscms2EcifClientService dscms2EcifClientService;

     public static void main(String[] args) {

         // 2) 对应的业务逻辑处增加以下代码：
         G00202ReqDto g00202ReqDto = new G00202ReqDto();
         //  StringUtils.EMPTY的实际值待确认 开始
         //g00202ReqDto.setResotp(StringUtils.EMPTY);//   识别方式
         // g00202ReqDto其他值赋予对应的业务值
         //  StringUtils.EMPTY的实际值待确认 结束
         //  完善后续逻辑
         ResultDto<G00202RespDto> g00202ResultDto = dscms2EcifClientService.g00202(g00202ReqDto);
         String g00202Code = Optional.ofNullable(g00202ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
         String g00202Meesage = Optional.ofNullable(g00202ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
         G00202RespDto g00202RespDto = null;
         if (Objects.equals(g00202Code, SuccessEnum.SUCCESS.key)) {
             //  获取相关的值并解析
             g00202RespDto = g00202ResultDto.getData();
         } else {
             //  抛出错误异常
             throw new YuspException(g00202Code, g00202Meesage);
         }

     }
}
