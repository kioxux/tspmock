/**
 * 响应Dto的get方法：期供提前还款
 *
 * @author lihh
 * @version 1.0
 */
public class LstqgtqhkRespDtoGet {
    public static void main(String[] args) {
        LstqgtqhkRespDto lstqgtqhkRespDto = new LstqgtqhkRespDto(); //响应DTO:期供提前还款
        Integer benqqish = lstqgtqhkRespDto.getbenqqish();//本期期数
        String huankriq = lstqgtqhkRespDto.gethuankriq();//还款日期
        BigDecimal chushibj = lstqgtqhkRespDto.getchushibj();//初始本金
        BigDecimal chushilx = lstqgtqhkRespDto.getchushilx();//初始利息
        BigDecimal meiqhkze = lstqgtqhkRespDto.getmeiqhkze();//每期还款总额
        BigDecimal bjdshyje = lstqgtqhkRespDto.getbjdshyje();//本阶段剩余本金
    }
}  
