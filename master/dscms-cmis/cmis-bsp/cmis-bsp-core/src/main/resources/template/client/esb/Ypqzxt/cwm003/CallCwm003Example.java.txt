package template.client.esb.core.ln3032;

import cn.com.yusys.yusp.commons.module.adapter.exception.YuspException;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.enums.returncode.SuccessEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

public class CallCwm003Example {
        // 1）注入：BSP封装调用押品权证系统的接口
        @Autowired
        private Dscms2YpqzxtClientService dscms2YpqzxtClientService;

        public static void main(String[] args) {
            // 2) 对应的业务逻辑处增加以下代码：
            Cwm003ReqDto cwm003ReqDto = new Cwm003ReqDto();
            //  StringUtils.EMPTY的实际值待确认 开始
            //cwm003ReqDto.setAcctBrch(StringUtils.EMPTY);  // 账务机构
            // cwm003ReqDto其他值赋予对应的业务值
            //  StringUtils.EMPTY的实际值待确认 结束
            //  完善后续逻辑
            ResultDto<Cwm003RespDto> cwm003ResultDto = dscms2YpqzxtClientService.cwm003(cwm003ReqDto);
            String cwm003Code = Optional.ofNullable(cwm003ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
            String cwm003Meesage = Optional.ofNullable(cwm003ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
            Cwm003RespDto cwm003RespDto = null;
            if (Objects.equals(cwm003Code, SuccessEnum.SUCCESS.key)) {
                //  获取相关的值并解析
                cwm003RespDto = cwm003ResultDto.getData();
            } else {
                //  抛出错误异常
                throw new YuspException(cwm003Code, cwm003Meesage);
            }

        }
}
