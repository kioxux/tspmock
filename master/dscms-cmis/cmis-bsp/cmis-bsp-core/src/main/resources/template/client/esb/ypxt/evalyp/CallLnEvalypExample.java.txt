package cn.com.yusys.yusp.service;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public class CallLnEvalypExample {

    // 1）注入：核心系统管理平台
    @Autowired
    private Dscms2CoreClientService dscms2CoreClientService;

    public static void someMethod(String[] args) {
        // 2) 对应的业务逻辑处增加以下代码：

        EvalypReqDto evalypReqDto = new EvalypReqDto(); //请求DTO:押品我行确认价值同步接口
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        evalypReqDto.setPrcscd(StringUtils.EMPTY);// 处理码
        evalypReqDto.setServtp(StringUtils.EMPTY);// 渠道
        evalypReqDto.setServsq(StringUtils.EMPTY);// 渠道流水
        evalypReqDto.setUserid(StringUtils.EMPTY);// 柜员号
        evalypReqDto.setBrchno(StringUtils.EMPTY);// 部门号
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束

        EvalypListInfoReq evalypListInfoReq = new EvalypListInfoReq(); //请求DTO:押品我行确认价值同步接口
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        evalypListInfoReq.setYptybh(StringUtils.EMPTY);// 押品统一编号
        evalypListInfoReq.setWhrdjz(new BigDecimal(0L));// 我行认定价值
        evalypListInfoReq.setRdbzyp(StringUtils.EMPTY);// 认定币种
        evalypListInfoReq.setRdrqyp(StringUtils.EMPTY);// 认定日期
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
        evalypReqDto.setEvalypListInfoReq(Arrays.asList(evalypListInfoReq));
        ResultDto<LnEvalypRespDto> lnEvalypResultDto = dscms2CoreClientService.lnEvalyp(lnEvalypReqDto);
        String lnEvalypCode = Optional.ofNullable(lnEvalypResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String lnEvalypMeesage = Optional.ofNullable(lnEvalypResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        LnEvalypRespDto lnEvalypRespDto = null;
        if (Objects.equals(lnEvalypCode, SuccessEnum.SUCCESS.key)) {
            //  获取相关的值并解析
            lnEvalypRespDto = lnEvalypResultDto.getData();//响应DTO:押品我行确认价值同步接口
            String code = lnEvalypResultDto.getcode();//返回码
            String message = lnEvalypResultDto.getmessage();//返回提示
        } else {
            //  抛出错误异常
            throw new YuspException(lnEvalypCode, lnEvalypMeesage);
        }
    }
}
