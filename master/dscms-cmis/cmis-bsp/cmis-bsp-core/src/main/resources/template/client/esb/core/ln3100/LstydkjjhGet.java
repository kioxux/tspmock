/**
 * 响应Dto的get方法：借新还旧原贷款借据号列表
 *
 * @author chenyong
 * @version 1.0
 */
public class LstydkjjhGet {
    public static void main(String[] args) {
        Lstydkjjh lstydkjjh = new Lstydkjjh(); //响应DTO:借新还旧原贷款借据号列表
        String ydkjiejh = lstydkjjh.getYdkjiejh();//原贷款借据号
        BigDecimal jxhuanbj = lstydkjjh.getJxhuanbj();//借新本金
        BigDecimal zihuanbj = lstydkjjh.getZihuanbj();//自还本金
        BigDecimal zihuanlx = lstydkjjh.getZihuanlx();//自还利息
    }
}  
