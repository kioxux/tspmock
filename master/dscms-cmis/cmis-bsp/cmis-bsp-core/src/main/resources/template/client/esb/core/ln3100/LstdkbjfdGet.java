/**
 * 响应Dto的get方法：本金分段登记
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdkbjfdGet {
    public static void main(String[] args) {
        Lstdkbjfd lstdkbjfd = new Lstdkbjfd(); //响应DTO:本金分段登记
        String bjdhkfsh = lstdkbjfd.getBjdhkfsh();//本阶段还款方式
        Integer bjdhkqsh = lstdkbjfd.getBjdhkqsh();//本阶段还款期数
        BigDecimal bjdhbjee = lstdkbjfd.getBjdhbjee();//本阶段还本金额
        String dechligz = lstdkbjfd.getDechligz();//等额处理规则
        BigDecimal leijinzh = lstdkbjfd.getLeijinzh();//累进值
        Integer leijqjsh = lstdkbjfd.getLeijqjsh();//累进区间期数
        String hkzhouqi = lstdkbjfd.getHkzhouqi();//还款周期
        String jixiguiz = lstdkbjfd.getJixiguiz();//计息规则
    }
}  
