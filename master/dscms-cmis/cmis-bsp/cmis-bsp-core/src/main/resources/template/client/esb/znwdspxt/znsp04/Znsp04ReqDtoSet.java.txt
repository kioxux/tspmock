import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：自动审批调查报告
 *
 * @author code-generator
 * @version 1.0
 */
public class Znsp04ReqDtoSet {
    public static void main(String[] args) {
        Znsp04ReqDto znsp04ReqDto = new Znsp04ReqDto(); //请求DTO:自动审批调查报告
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        znsp04ReqDto.setSurvey_serno(StringUtils.EMPTY);// 业务流水号
        znsp04ReqDto.setApply_type(StringUtils.EMPTY);// 申请类型
        znsp04ReqDto.setPrd_id(StringUtils.EMPTY);// 产品编号
        znsp04ReqDto.setPrd_name(StringUtils.EMPTY);// 产品名称
        znsp04ReqDto.setCus_name(StringUtils.EMPTY);// 客户名称
        znsp04ReqDto.setLoan_type(StringUtils.EMPTY);// 贷款类型
        znsp04ReqDto.setCus_id(StringUtils.EMPTY);// 客户号
        znsp04ReqDto.setCert_no(StringUtils.EMPTY);// 身份证号码
        znsp04ReqDto.setZb_manager_id(StringUtils.EMPTY);// 主办客户经理工号
        znsp04ReqDto.setXb_manager_id(StringUtils.EMPTY);// 协办客户经理
        znsp04ReqDto.setLoan_amt(new BigDecimal(0L));// 贷款金额
        znsp04ReqDto.setReality_ir_y(new BigDecimal(0L));// 执行利率
        znsp04ReqDto.setLoan_term(new BigDecimal(0L));// 贷款期限
        znsp04ReqDto.setGuara_way(StringUtils.EMPTY);// 担保方式
        znsp04ReqDto.setIs_sjsh(StringUtils.EMPTY);// 是否随机随还
        znsp04ReqDto.setRepay_way(StringUtils.EMPTY);// 还款方式
        znsp04ReqDto.setCont_type(StringUtils.EMPTY);// 合同类型
        znsp04ReqDto.setTerm_loan_type(StringUtils.EMPTY);// 期限类型
        znsp04ReqDto.setIs_trustee_pay(StringUtils.EMPTY);// 是否受托支付
        znsp04ReqDto.setEntrust_type(StringUtils.EMPTY);// 受托类型
        znsp04ReqDto.setIs_credit_condition(StringUtils.EMPTY);// 是否有放款条件
        znsp04ReqDto.setCredit_condition(StringUtils.EMPTY);// 放款条件
        znsp04ReqDto.setIs_whbxd(StringUtils.EMPTY);// 是否无还本续贷
        znsp04ReqDto.setTurnover_amt(new BigDecimal(0L));// 周转金额
        znsp04ReqDto.setAdd_amt(new BigDecimal(0L));// 新增金额
        znsp04ReqDto.setCus_phone(StringUtils.EMPTY);// 客户手机号
        znsp04ReqDto.setLoan_use(StringUtils.EMPTY);// 贷款用途
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
