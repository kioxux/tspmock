/**
 * 响应Dto的get方法：贷款账户期供
 *
 * @author lihh
 * @version 1.0
 */
public class LstdkzqgRespDtoGet {
    public static void main(String[] args) {
        LstdkzqgRespDto lstdkzqgRespDto = new LstdkzqgRespDto(); //响应DTO:贷款账户期供
        Integer benqqish = lstdkzqgRespDto.getbenqqish();//本期期数
        Integer benqizqs = lstdkzqgRespDto.getbenqizqs();//本期子期数
        String qishriqi = lstdkzqgRespDto.getqishriqi();//起始日期
        String zhzhriqi = lstdkzqgRespDto.getzhzhriqi();//终止日期
        BigDecimal meiqhkze = lstdkzqgRespDto.getmeiqhkze();//每期还款总额
        BigDecimal chushibj = lstdkzqgRespDto.getchushibj();//初始本金
        BigDecimal chushilx = lstdkzqgRespDto.getchushilx();//初始利息
        String kxqdqirq = lstdkzqgRespDto.getkxqdqirq();//宽限期到期日
        BigDecimal benjinnn = lstdkzqgRespDto.getbenjinnn();//本金
        BigDecimal ysyjlixi = lstdkzqgRespDto.getysyjlixi();//应收应计利息
        BigDecimal csyjlixi = lstdkzqgRespDto.getcsyjlixi();//催收应计利息
        BigDecimal ysqianxi = lstdkzqgRespDto.getysqianxi();//应收欠息
        BigDecimal csqianxi = lstdkzqgRespDto.getcsqianxi();//催收欠息
        BigDecimal ysyjfaxi = lstdkzqgRespDto.getysyjfaxi();//应收应计罚息
        BigDecimal csyjfaxi = lstdkzqgRespDto.getcsyjfaxi();//催收应计罚息
        BigDecimal yshofaxi = lstdkzqgRespDto.getyshofaxi();//应收罚息
        BigDecimal cshofaxi = lstdkzqgRespDto.getcshofaxi();//催收罚息
        BigDecimal yingjifx = lstdkzqgRespDto.getyingjifx();//应计复息
        BigDecimal fuxiiiii = lstdkzqgRespDto.getfuxiiiii();//复息
        String schkriqi = lstdkzqgRespDto.getschkriqi();//上次还款日
        String qigengzl = lstdkzqgRespDto.getqigengzl();//期供种类
        String benqizht = lstdkzqgRespDto.getbenqizht();//本期状态
        String yjfyjzht = lstdkzqgRespDto.getyjfyjzht();//应计非应计状态
        BigDecimal zhanghye = lstdkzqgRespDto.getzhanghye();//账户余额
        BigDecimal yingjitx = lstdkzqgRespDto.getyingjitx();//应计贴息
        BigDecimal yingshtx = lstdkzqgRespDto.getyingshtx();//应收贴息
    }
}  
