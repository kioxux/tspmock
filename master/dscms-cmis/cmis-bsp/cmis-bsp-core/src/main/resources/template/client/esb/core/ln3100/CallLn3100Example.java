package cn.com.yusys.yusp.service;

public class CallLn3100Example {

    // 1）注入：核心系统管理平台
    @Autowired
    private Dscms2CoreClientService dscms2CoreClientService;

    public static void someMethod(String[] args) {
        // 2) 对应的业务逻辑处增加以下代码：

        Ln3100ReqDto ln3100ReqDto = new Ln3100ReqDto(); //请求DTO:根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        ln3100ReqDto.setDkzhangh(StringUtils.EMPTY);// 贷款账号
        ln3100ReqDto.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        ln3100ReqDto.setQishibis(new Integer(1));// 起始笔数
        ln3100ReqDto.setChxunbis(new Integer(1));// 查询笔数
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
        ResultDto<Ln3100RespDto> ln3100ResultDto = dscms2CoreClientService.ln3100(ln3100ReqDto);
        String ln3100Code = Optional.ofNullable(ln3100ResultDto.getCode()).orElse(SuccessEnum.SUCCESS.key);
        String ln3100Meesage = Optional.ofNullable(ln3100ResultDto.getMessage()).orElse(SuccessEnum.SUCCESS.value);
        Ln3100RespDto ln3100RespDto = null;
        if (Objects.equals(ln3100Code, SuccessEnum.SUCCESS.key)) {
            //  获取相关的值并解析
            ln3100RespDto = ln3100ResultDto.getData();//响应DTO:信贷系统请求V平台二级准入接口
            String code = ln3100ResultDto.getcode();//返回码
            String message = ln3100ResultDto.getmessage();//返回提示
        } else {
            //  抛出错误异常
            throw new YuspException(ln3100Code, ln3100Meesage);
        }
    }
}
