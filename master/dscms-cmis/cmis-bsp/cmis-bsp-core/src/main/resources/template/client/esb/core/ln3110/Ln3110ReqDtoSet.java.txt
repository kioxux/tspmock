import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：用于贷款放款前进行还款计划试算
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3110ReqDtoSet {
    public static void main(String[] args) {
        Ln3110ReqDto ln3110ReqDto = new Ln3110ReqDto(); //请求DTO:用于贷款放款前进行还款计划试算
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        ln3110ReqDto.setPrcscd(StringUtils.EMPTY);// 处理码
        ln3110ReqDto.setServtp(StringUtils.EMPTY);// 渠道
        ln3110ReqDto.setServsq(StringUtils.EMPTY);// 渠道流水
        ln3110ReqDto.setUserid(StringUtils.EMPTY);// 柜员号
        ln3110ReqDto.setBrchno(StringUtils.EMPTY);// 部门号
        ln3110ReqDto.setDaikjine(new BigDecimal(0L));// 贷款金额
        ln3110ReqDto.setSftsdkbz(StringUtils.EMPTY);// 是否特殊贷款标志
        ln3110ReqDto.setTsdkjxqs(new Integer(1));// 特殊贷款计息总期数
        ln3110ReqDto.setJixiguiz(StringUtils.EMPTY);// 计息规则
        ln3110ReqDto.setZhchlilv(new BigDecimal(0L));// 正常利率
        ln3110ReqDto.setHkzhouqi(StringUtils.EMPTY);// 还款周期
        ln3110ReqDto.setHkqixian(StringUtils.EMPTY);// 还款期限(月)
        ln3110ReqDto.setQixiriqi(StringUtils.EMPTY);// 起息日期
        ln3110ReqDto.setDaoqriqi(StringUtils.EMPTY);// 到期日期
        ln3110ReqDto.setHuankfsh(StringUtils.EMPTY);// 还款方式
        ln3110ReqDto.setQigscfsh(StringUtils.EMPTY);// 期供生成方式
        ln3110ReqDto.setQglxleix(StringUtils.EMPTY);// 期供利息类型
        ln3110ReqDto.setDechligz(StringUtils.EMPTY);// 等额处理规则
        ln3110ReqDto.setMeiqhkze(new BigDecimal(0L));// 每期还款总额
        ln3110ReqDto.setMeiqhbje(new BigDecimal(0L));// 每期还本金额
        ln3110ReqDto.setBaoliuje(new BigDecimal(0L));// 保留金额
        ln3110ReqDto.setKuanxiqi(new Integer(1));// 宽限期
        ln3110ReqDto.setLeijinzh(new BigDecimal(0L));// 累进值
        ln3110ReqDto.setLeijqjsh(new Integer(1));// 累进区间期数
        ln3110ReqDto.setQishibis(new Integer(1));// 起始笔数
        ln3110ReqDto.setChxunbis(new Integer(1));// 查询笔数
        ln3110ReqDto.setShifoudy(StringUtils.EMPTY);// 是否打印
        ln3110ReqDto.setScihkrbz(StringUtils.EMPTY);// 首次还款日模式
        ln3110ReqDto.setMqihkfsh(StringUtils.EMPTY);// 末期还款方式
        ln3110ReqDto.setDzhhkjih(StringUtils.EMPTY);// 定制还款计划
        ln3110ReqDto.setLjsxqish(new Integer(1));// 累进首段期数
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
