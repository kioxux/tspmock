/**
 * 响应Dto的get方法：贷款联合放款
 *
 * @author chenyong
 * @version 1.0
 */
public class LstdklhmxGet {
    public static void main(String[] args) {
        Lstdklhmx lstdklhmx = new Lstdklhmx(); //响应DTO:贷款联合放款
        String canyfdma = lstdklhmx.getCanyfdma();//参与方代码
        String canyfhum = lstdklhmx.getCanyfhum();//参与方户名
        String lianhfsh = lstdklhmx.getLianhfsh();//联合方式
        String lhdkleix = lstdklhmx.getLhdkleix();//联合贷款类型
        BigDecimal canyjine = lstdklhmx.getCanyjine();//参与金额
        BigDecimal canybili = lstdklhmx.getCanybili();//参与比例
        String zjingjbz = lstdklhmx.getZjingjbz();//资金归集标志
        String zjlyzhao = lstdklhmx.getZjlyzhao();//资金来源账号
        String zjlyzzxh = lstdklhmx.getZjlyzzxh();//资金来源账号子序号
        String zjzrzhao = lstdklhmx.getZjzrzhao();//资金转入账号
        String zjzrzzxh = lstdklhmx.getZjzrzzxh();//资金转入账号子序号
        String canyjjha = lstdklhmx.getCanyjjha();//参与借据号
        String bjghrzzh = lstdklhmx.getBjghrzzh();//本金归还入账账号
        String bjghrzxh = lstdklhmx.getBjghrzxh();//本金归还入账账号子序号
        String lxghrzzh = lstdklhmx.getLxghrzzh();//利息归还入账账号
        String lxghrzxh = lstdklhmx.getLxghrzxh();//利息归还入账账号子序号
        BigDecimal yishbenj = lstdklhmx.getYishbenj();//已收回本金
        BigDecimal yishlixi = lstdklhmx.getYishlixi();//已收回利息
        BigDecimal dhzhbenj = lstdklhmx.getDhzhbenj();//待划转本金
        BigDecimal dhzhlixi = lstdklhmx.getDhzhlixi();//待划转利息
    }
}  
