/**
 * 响应Dto的get方法：查询质押物信息
 * @author code-generator
 * @version 1.0             
 */      
public class XdypzywcxRespDtoGet {
	  public static void main(String[] args) {
	     XdypzywcxRespDto xdypzywcxRespDto = new XdypzywcxRespDto(); //响应DTO:查询质押物信息
	 String name = xdypzywcxRespDto.getName();//名称
	 String amt = xdypzywcxRespDto.getAmt();//价值
	 BigDecimal amount = xdypzywcxRespDto.getAmount();//数量
	 String stock_no = xdypzywcxRespDto.getStock_no();//代码/号码
	 String end_date = xdypzywcxRespDto.getEnd_date();//到期日
	 String cur_type = xdypzywcxRespDto.getCur_type();//币种
 }
}  
