import org.apache.commons.lang3.StringUtils;

/**
 * 请求Dto的set方法：贷款归还试算
 *
 * @author code-generator
 * @version 1.0
 */
public class Ln3111ReqDtoSet {
    public static void main(String[] args) {
        Ln3111ReqDto ln3111ReqDto = new Ln3111ReqDto(); //请求DTO:贷款归还试算
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 开始
        ln3111ReqDto.setDkzhangh(StringUtils.EMPTY);// 贷款账号
        ln3111ReqDto.setDkjiejuh(StringUtils.EMPTY);// 贷款借据号
        ln3111ReqDto.setHetongbh(StringUtils.EMPTY);// 合同编号
        ln3111ReqDto.setKehuhaoo(StringUtils.EMPTY);// 客户号
        ln3111ReqDto.setKehuzwmc(StringUtils.EMPTY);// 客户名
        ln3111ReqDto.setHuobdhao(StringUtils.EMPTY);// 货币代号
        ln3111ReqDto.setHuankriq(StringUtils.EMPTY);// 还款日期
        ln3111ReqDto.setZhchbjin(new BigDecimal(0L));// 正常本金
        ln3111ReqDto.setYingjilx(new BigDecimal(0L));// 应计利息
        ln3111ReqDto.setYuqibjin(new BigDecimal(0L));// 逾期本金
        ln3111ReqDto.setDzhibjin(new BigDecimal(0L));// 呆滞本金
        ln3111ReqDto.setDaizbjin(new BigDecimal(0L));// 呆账本金
        ln3111ReqDto.setYsyjlixi(new BigDecimal(0L));// 应收应计利息
        ln3111ReqDto.setCsyjlixi(new BigDecimal(0L));// 催收应计利息
        ln3111ReqDto.setYsqianxi(new BigDecimal(0L));// 应收欠息
        ln3111ReqDto.setCsqianxi(new BigDecimal(0L));// 催收欠息
        ln3111ReqDto.setYsyjfaxi(new BigDecimal(0L));// 应收应计罚息
        ln3111ReqDto.setCsyjfaxi(new BigDecimal(0L));// 催收应计罚息
        ln3111ReqDto.setYshofaxi(new BigDecimal(0L));// 应收罚息
        ln3111ReqDto.setCshofaxi(new BigDecimal(0L));// 催收罚息
        ln3111ReqDto.setYingjifx(new BigDecimal(0L));// 应计复息
        ln3111ReqDto.setFuxiiiii(new BigDecimal(0L));// 复息
        ln3111ReqDto.setYingshfj(new BigDecimal(0L));// 应收罚金
        ln3111ReqDto.setQiankzee(new BigDecimal(0L));// 欠款总额
        ln3111ReqDto.setHuankzle(StringUtils.EMPTY);// 还款种类
        ln3111ReqDto.setDktqhkzl(StringUtils.EMPTY);// 提前还款种类
        ln3111ReqDto.setHuankjee(new BigDecimal(0L));// 还款金额
        ln3111ReqDto.setTqhkhxfs(StringUtils.EMPTY);// 提前还款还息方式
        //  StringUtils.EMPTY,new BigDecimal(0L),new Integer(1)的实际值待确认 结束
    }
}  
