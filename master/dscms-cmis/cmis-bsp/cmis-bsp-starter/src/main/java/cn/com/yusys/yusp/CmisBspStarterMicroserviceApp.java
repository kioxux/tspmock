package cn.com.yusys.yusp;

import cn.com.yusys.yusp.commons.autoconfigure.mybatis.tkmapper.MapperConfiguration;
import cn.com.yusys.yusp.commons.autoconfigure.mybatis.tkmapper.MapperMybatisAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.amqp.RabbitHealthContributorAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableDiscoveryClient
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, MapperConfiguration.class, MapperMybatisAutoConfiguration.class,
        RabbitHealthContributorAutoConfiguration.class})
@EnableFeignClients("cn.com.yusys.yusp")
@EnableTransactionManagement
public class CmisBspStarterMicroserviceApp {

    public static void main(String[] args) {
        SpringApplication.run(CmisBspStarterMicroserviceApp.class, args).getEnvironment();
    }

}