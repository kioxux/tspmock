package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Com001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.com001.resp.Com001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2ComstarClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类:Comstar系统
 *
 * @author leehuang
 * @version 1.0
 */
@Component
public class Dscms2ComstarClientServiceImpl implements Dscms2ComstarClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2ComstarClientServiceImpl.class);

    /**
     * 交易码：com001
     * 交易描述：额度同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Com001RespDto> com001(Com001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_COM001.key.concat("|").concat(EsbEnum.TRADE_CODE_COM001.value));
        return null;
    }
}
