package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2SzzxClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用苏州征信系统的接口实现类
 */
@Component
public class Dscms2SzzxClientServiceImpl implements Dscms2SzzxClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2SzzxClientService.class);

    /**
     * ESB信贷查询地方征信接口（处理码zxcx006）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Zxcx006RespDto> zxcx006(Zxcx006ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZXCX006.key.concat("|").concat(EsbEnum.TRADE_CODE_ZXCX006.value));
        return null;
    }
}
