package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.req.ClfxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.resp.ClfxcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.req.ClzjcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.resp.ClzjcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.req.JcxxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.resp.JcxxcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.req.SqfyyzReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.resp.SqfyyzRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2ZjywxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用中间业务系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2ZjywxtClientServiceImpl.class)
public interface Dscms2ZjywxtClientService {

    /**
     * 交易码：clzjcx
     * 交易描述：未备注
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2zjywxt/clzjcx")
    public ResultDto<ClzjcxRespDto> clzjcx(ClzjcxReqDto reqDto);

    /**
     * 交易码：sqfyyz
     * 交易描述：行方验证房源信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2zjywxt/sqfyyz")
    public ResultDto<SqfyyzRespDto> sqfyyz(SqfyyzReqDto reqDto);

    /**
     * 交易码：jcxxcx
     * 交易描述：缴存信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2zjywxt/jcxxcx")
    public ResultDto<JcxxcxRespDto> jcxxcx(JcxxcxReqDto reqDto);

    /**
     * 交易码：clfxcx
     * 交易描述：协议信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2zjywxt/clfxcx")
    public ResultDto<ClfxcxRespDto> clfxcx(ClfxcxReqDto reqDto);

}
