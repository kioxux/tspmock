package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.ocr.req.Cbocr3ReqDto;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.Cbocr3RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.req.SxbzxrReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.SxbzxrRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.req.Ztod01ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.resp.Ztod01RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2OuterdataClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用外部数据平台的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2OuterdataClientServiceImpl.class)
public interface Dscms2OuterdataClientService {
    /**
     * 身份核查
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallIdCheckExample.java.txt
     */
    @PostMapping("/dscms2outerdata/idCheck")
    public ResultDto<IdCheckRespDto> idCheck(IdCheckReqDto reqDto);


    /**
     * 企业工商信息查询
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallzsnewExample.java.txt
     */
    @PostMapping("/dscms2outerdata/zsnew")
    public ResultDto<ZsnewRespDto> zsnew(ZsnewReqDto reqDto);

    /**
     * 查询涉诉信息
     *
     * @param reqDto
     * @return
     * @code
     */
    @PostMapping("/dscms2outerdata/qyssxx")
    public ResultDto<QyssxxRespDto> qyssxx(QyssxxReqDto reqDto);

    /**
     * 交易码：ztod01
     * 交易描述：融E开-工商数据查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2outerdata/ztod01")
    public ResultDto<Ztod01RespDto> ztod01(Ztod01ReqDto reqDto);

    /**
     * 交易码：cbocr3
     * 交易描述：报表核对统计总览列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2outerdata/cbocr3")
    public ResultDto<Cbocr3RespDto> cbocr3(Cbocr3ReqDto reqDto);

    /**
     * 交易码：slno01
     * 交易描述：双录流水号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2outerdata/slno01")
    public ResultDto<Slno01RespDto> slno01(Slno01ReqDto reqDto);

    /**
     * 交易码：sxbzxr
     * 交易描述：失信被执行人查询接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2outerdata/sxbzxr")
    public ResultDto<SxbzxrRespDto> sxbzxr(SxbzxrReqDto reqDto);
}

