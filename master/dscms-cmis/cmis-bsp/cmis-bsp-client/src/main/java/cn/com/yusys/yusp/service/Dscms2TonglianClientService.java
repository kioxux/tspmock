package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.req.D11005ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.resp.D11005RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.req.D11010ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.resp.D11010RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.req.D12000ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.resp.D12000RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.req.D13087ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.resp.D13087RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.req.D13160ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.resp.D13160RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.req.D13162ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.resp.D13162RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2TonglianClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:通联系统
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 9:56
 * @since 2021/5/27 9:56
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2TonglianClientServiceImpl.class)
public interface Dscms2TonglianClientService {

    /**
     * 交易码：d15011
     * 交易描述：永久额度调整
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d15011")
    public ResultDto<D15011RespDto> d15011(D15011ReqDto reqDto);

    /**
     * 交易码：d13160
     * 交易描述：大额现金分期申请
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d13160")
    public ResultDto<D13160RespDto> d13160(D13160ReqDto reqDto);

    /**
     * 交易码：d13087
     * 交易描述：现金（大额）放款接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d13087")
    public ResultDto<D13087RespDto> d13087(D13087ReqDto reqDto);

    /**
     * 交易码：d12050
     * 交易描述：账单列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d12050")
    public ResultDto<D12050RespDto> d12050(D12050ReqDto reqDto);

    /**
     * 交易码：d14020
     * 交易描述：卡片信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d14020")
    public ResultDto<D14020RespDto> d14020(D14020ReqDto reqDto);

    /**
     * 交易码：d12011
     * 交易描述：账单交易明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d12011")
    public ResultDto<D12011RespDto> d12011(D12011ReqDto reqDto);

    /**
     * 交易码：d11005
     * 交易描述：信用卡申请
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d11005")
    public ResultDto<D11005RespDto> d11005(D11005ReqDto reqDto);


    /**
     * 交易码：d11010
     * 交易描述：客户基本信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d11010")
    public ResultDto<D11010RespDto> d11010(D11010ReqDto reqDto);

    /**
     * 交易码：d12000
     * 交易描述：账户信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d12000")
    public ResultDto<D12000RespDto> d12000(D12000ReqDto reqDto);

    /**
     * 交易码：d13162
     * 交易描述：分期审核
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2tonglian/d13162")
    public ResultDto<D13162RespDto> d13162(D13162ReqDto reqDto);
}
