package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.req.Dhxd01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.resp.Dhxd01RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.req.Dhxd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.resp.Dhxd02RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007.Wxd007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007.Wxd007RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.req.Xwd008ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.resp.Xwd008RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.req.Xwd013ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.Xwd013RespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req.Xwd009ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp.Xwd009RespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.req.Xwd010ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.resp.Xwd010RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2XwywglptClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:小微业务管理平台
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2XwywglptClientServiceImpl.class)
public interface Dscms2XwywglptClientService {
    /**
     * 交易码：wxd003
     * 交易描述：信贷系统请求V平台二级准入接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/wxd003")
    public ResultDto<Wxd003RespDto> wxd003(Wxd003ReqDto reqDto);

    /**
     * 交易码：wxd004
     * 交易描述：信贷系统获取征信报送监管信息接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/wxd004")
    public ResultDto<Wxd004RespDto> wxd004(Wxd004ReqDto reqDto);

    /**
     * 交易码：wxd007
     * 交易描述：请求小V平台综合决策管理列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/wxd007")
    public ResultDto<Wxd007RespDto> wxd007(Wxd007ReqDto reqDto);

    /**
     * 交易码：wxd009
     * 交易描述：信贷系统请求小V平台推送合同信息接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/wxd009")
    public ResultDto<Wxd009RespDto> wxd009(Wxd009ReqDto reqDto);

    /**
     * 交易码：dhxd01
     * 交易描述：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/dhxd01")
    public ResultDto<Dhxd01RespDto> dhxd01(Dhxd01ReqDto reqDto);

    /**
     * 交易码：dhxd02
     * 交易描述：信贷系统请求小V平台贷后预警回传定期检查任务结果
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/dhxd02")
    public ResultDto<Dhxd02RespDto> dhxd02(Dhxd02ReqDto reqDto);


    /**
     * 交易码：xwd008
     * 交易描述：新信贷同步用户账号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/xwd008")
    public ResultDto<Xwd008RespDto> xwd008(Xwd008ReqDto reqDto);

    /**
     * 交易码：xwd010
     * 交易描述：利率撤销接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/xwd010")
    public ResultDto<Xwd010RespDto> xwd010(Xwd010ReqDto reqDto);

    /**
     * 交易码：xwd009
     * 交易描述：利率申请接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/xwd009")
    public ResultDto<Xwd009RespDto> xwd009(Xwd009ReqDto reqDto);


    /**
     * 交易码：xwd013
     * 交易描述：新信贷获取调查信息接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/xwd013")
    public ResultDto<Xwd013RespDto> xwd013(Xwd013ReqDto reqDto);
}
