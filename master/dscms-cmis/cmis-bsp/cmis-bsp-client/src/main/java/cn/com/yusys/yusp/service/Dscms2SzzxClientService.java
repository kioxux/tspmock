package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006.Zxcx006RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2SzzxClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用苏州征信系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2SzzxClientServiceImpl.class)
public interface Dscms2SzzxClientService {

    /**
     * ESB信贷查询地方征信接口（处理码zxcx006）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallZxcx006Example.java.txt
     */
    @PostMapping("/dscms2szzx/zxcx006")
    public ResultDto<Zxcx006RespDto> zxcx006(Zxcx006ReqDto reqDto);

}
