package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.req.Lc0323ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp.Lc0323RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2LcxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

/**
 * BSP封装调用理财系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2LcxtClientServiceImpl.class)
public interface Dscms2LcxtClientService {

    /**
     * 交易码：lc0323
     * 交易描述：查询理财是否冻结
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/dscms2lcxt/lc0323")
    public ResultDto<Lc0323RespDto> lc0323(Lc0323ReqDto reqDto);

}
