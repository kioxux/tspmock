package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.req.YxljcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.resp.YxljcxRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2YxxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用影像系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2YxxtClientServiceImpl.class)
public interface Dscms2YxxtClientService {
    /**
     * 交易码：yxljcx
     * 交易描述：影像图像路径查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2yxxt/yxljcx")
    public ResultDto<YxljcxRespDto> yxljcx(YxljcxReqDto reqDto);

}
