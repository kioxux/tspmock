package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2YpqzxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用押品权证系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2YpqzxtClientServiceImpl.class)
public interface Dscms2YpqzxtClientService {


    /**
     * 本异地借阅出库接口（处理码cwm001）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCwm001Example.java.txt
     */
    @PostMapping("/dscms2ypqzxt/cwm001")
    public ResultDto<Cwm001RespDto> cwm001(Cwm001ReqDto reqDto);

    /**
     * 本异地借阅归还接口（处理码cwm002）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCwm002Example.java.txt
     */
    @PostMapping("/dscms2ypqzxt/cwm002")
    public ResultDto<Cwm002RespDto> cwm002(Cwm002ReqDto reqDto);

    /**
     * 押品权证入库 （cwm003）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCwm003Example.java.txt
     */
    @PostMapping("/dscms2ypqzxt/cwm003")
    public ResultDto<Cwm003RespDto> cwm003(Cwm003ReqDto reqDto);


    /**
     * 押品权证出库 (cwm004)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCwm004Example.java.txt
     */
    @PostMapping("/dscms2ypqzxt/cwm004")
    public ResultDto<Cwm004RespDto> cwm004(Cwm004ReqDto reqDto);

    /**
     * 押品状态查询接口 （cwm007）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCwm007Example.java.txt
     */
    @PostMapping("/dscms2ypqzxt/cwm007")
    public ResultDto<Cwm007RespDto> cwm007(Cwm007ReqDto reqDto);
}
