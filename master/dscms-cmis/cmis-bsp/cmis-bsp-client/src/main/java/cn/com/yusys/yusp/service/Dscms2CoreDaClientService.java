package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3300.Da3300ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3300.Da3300RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.req.Da3301ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3301.resp.Da3301RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3302.req.Da3302ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3302.resp.Da3302RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3303.Da3303ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3303.Da3303RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3304.req.Da3304ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3304.resp.Da3304RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3305.req.Da3305ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3305.resp.Da3305RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3306.req.Da3306ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3306.resp.Da3306RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3307.req.Da3307ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3307.resp.Da3307RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3308.req.Da3308ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3308.resp.Da3308RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3320.Da3320ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3320.Da3320RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3321.req.Da3321ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3321.resp.Da3321RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3322.req.Da3322ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.Da3322RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2CoreDaClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;


/**
 * 交易码：da3302
 * 交易描述：抵债资产处置
 * BSP封装调用核心系统的接口(dp开头)
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2CoreDaClientServiceImpl.class)
public interface Dscms2CoreDaClientService {

    /**
     * 交易码：da3301
     * 交易描述：抵债资产入账
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3301")
    public ResultDto<Da3301RespDto> da3301(Da3301ReqDto reqDto);

    /**
     * 交易码：da3302
     * 交易描述：抵债资产处置
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3302")
    public ResultDto<Da3302RespDto> da3302(Da3302ReqDto reqDto);

    /**
     * 交易码：da3307
     * 交易描述：抵债资产出租处理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3307")
    public ResultDto<Da3307RespDto> da3307(Da3307ReqDto reqDto);

    /**
     * 交易码：da3308
     * 交易描述：抵债资产费用管理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3308")
    public ResultDto<Da3308RespDto> da3308(Da3308ReqDto reqDto);

    /**
     * 交易码：da3300
     * 交易描述：交易用于抵债资产信息登记（增、删、改）至核心，核心做相应的表外账务处理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3300")
    public ResultDto<Da3300RespDto> da3300(Da3300ReqDto reqDto);

    /**
     * 交易码：da3303
     * 交易描述：抵债资产信息维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3303")
    public ResultDto<Da3303RespDto> da3303(Da3303ReqDto reqDto);

    /**
     * 交易码：da3320
     * 交易描述：查询抵债资产信息以及与贷款、费用、出租的关联信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3320")
    public ResultDto<Da3320RespDto> da3320(Da3320ReqDto reqDto);

    /**
     * 交易码：da3304
     * 交易描述：资产转让借据筛选
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3304")
    public ResultDto<Da3304RespDto> da3304(Da3304ReqDto reqDto);

    /**
     * 交易码：da3305
     * 交易描述：待变现抵债资产销账
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3305")
    public ResultDto<Da3305RespDto> da3305(Da3305ReqDto reqDto);

    /**
     * 交易码：da3306
     * 交易描述：抵债资产拨备计提
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3306")
    public ResultDto<Da3306RespDto> da3306(Da3306ReqDto reqDto);

    /**
     * 交易码：da3321
     * 交易描述：抵债资产模糊查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3321")
    public ResultDto<Da3321RespDto> da3321(Da3321ReqDto reqDto);

    /**
     * 交易码：da3322
     * 交易描述：抵债资产明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreda/da3322")
    public ResultDto<Da3322RespDto> da3322(Da3322ReqDto reqDto);
}






