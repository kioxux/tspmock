package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.req.Xwd001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.resp.Xwd001RespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req.Xwd009ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp.Xwd009RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2XwdClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用新微贷系统的接口实现类
 */
@Component
public class Dscms2XwdClientServiceImpl implements Dscms2XwdClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2XwdClientServiceImpl.class);

    /**
     * 交易描述：利率申请接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwd009RespDto> xwd009(Xwd009ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWD009.key.concat("|").concat(EsbEnum.TRADE_CODE_XWD009.value));
        return null;
    }

    /**
     * 交易描述：贷款申请接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwd001RespDto> xwd001(Xwd001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWD001.key.concat("|").concat(EsbEnum.TRADE_CODE_XWD001.value));
        return null;
    }
}
