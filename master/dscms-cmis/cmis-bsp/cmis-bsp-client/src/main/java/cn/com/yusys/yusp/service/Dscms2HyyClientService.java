package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.hyy.bare01.req.Bare01ReqDto;
import cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp.Bare01RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2HyyClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2HyyClientServiceImpl.class)
public interface Dscms2HyyClientService {

    /**
     * 交易码：bare01
     * 交易描述：押品状态查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2hyy/bare01")
    public ResultDto<Bare01RespDto> bare01(Bare01ReqDto reqDto);


}
