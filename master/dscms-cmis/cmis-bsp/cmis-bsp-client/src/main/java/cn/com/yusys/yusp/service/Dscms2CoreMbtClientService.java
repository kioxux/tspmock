package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.req.Mbt951ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.resp.Mbt951RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.req.Mbt952ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.Mbt952RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt999.Mbt999ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt999.Mbt999RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2CoreMbtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用核心系统的接口(dp开头)
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2CoreMbtClientServiceImpl.class)
public interface Dscms2CoreMbtClientService {

    /**
     * 交易码：mbt999
     * 交易描述：V5通用记账
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2corembt/mbt999")
    public ResultDto<Mbt999RespDto> mbt999(Mbt999ReqDto reqDto);

    /**
     * 交易码：mbt951
     * 交易描述：批量文件处理申请
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2corembt/mbt951")
    public ResultDto<Mbt951RespDto> mbt951(Mbt951ReqDto reqDto);

    /**
     * 交易码：mbt952
     * 交易描述：批量结果确认处理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2corembt/mbt952")
    public ResultDto<Mbt952RespDto> mbt952(Mbt952ReqDto reqDto);

}






