package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs18.Irs18ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs18.Irs18RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.req.Irs19ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.resp.Irs19RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs11.req.Xirs11ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs11.resp.Xirs11RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs15.req.Xirs15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs15.resp.Xirs15RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs27.req.Xirs27ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs27.resp.Xirs27RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Xirs28ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.resp.Xirs28RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2IrsClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用非零内评系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2IrsClientServiceImpl.class)
public interface Dscms2IrsClientService {

    /**
     * 单一客户限额测算信息同步
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallIrs21Example.java.txt
     */
    @PostMapping("/dscms2irs/irs21")
    public ResultDto<Irs21RespDto> irs21(Irs21ReqDto reqDto);


    /**
     * 新增授信时债项评级
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallIrs97Example.java.txt
     */
    @PostMapping("/dscms2irs/irs97")
    public ResultDto<Irs97RespDto> irs97(Irs97ReqDto reqDto);

    /**
     * 单授信申请债项评级
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallIrs98Example.java.txt
     */
    @PostMapping("/dscms2irs/irs98")
    public ResultDto<Irs98RespDto> irs98(Irs98ReqDto reqDto);


    /**
     * 交易码：irs18
     * 交易描述：评级主办权更新
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2irs/irs18")
    public ResultDto<Irs18RespDto> irs18(Irs18ReqDto reqDto);

    /**
     * 交易码：irs19
     * 交易描述：专业贷款基本信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2irs/irs19")
    public ResultDto<Irs19RespDto> irs19(Irs19ReqDto reqDto);


    /**
     * 交易码：xirs10
     * 交易描述：公司客户信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2irs/xirs10")
    public ResultDto<Xirs10RespDto> xirs10(Xirs10ReqDto reqDto);

    /**
     * 业务申请债项评级
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallIrs99Example.java.txt
     */
    @PostMapping("/dscms2irs/irs99")
    public ResultDto<Irs99RespDto> irs99(Irs99ReqDto reqDto);

    /**
     * 交易码：xirs11
     * 交易描述：同业客户信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2irs/xirs11")
    public ResultDto<Xirs11RespDto> xirs11(Xirs11ReqDto reqDto);

    /**
     * 交易码：xirs28
     * 交易描述：财务信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2irs/xirs28")
    public ResultDto<Xirs28RespDto> xirs28(Xirs28ReqDto reqDto);

    /**
     * 交易码：xirs15
     * 交易描述：评级在途申请标识
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2irs/xirs15")
    public ResultDto<Xirs15RespDto> xirs15(Xirs15ReqDto reqDto);

    /**
     * 交易码：xirs27
     * 交易描述：工作台提示条数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2irs/xirs27")
    public ResultDto<Xirs27RespDto> xirs27(Xirs27ReqDto reqDto);
}
