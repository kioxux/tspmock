package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3201.Co3201ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3201.Co3201RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3225.req.Co3225ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3225.resp.Co3225RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3222.Co3222ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3222.Co3222RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2CoreCoClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用核心系统的接口实现类(co开头)
 */
@Component
public class Dscms2CoreCoClientServiceImpl implements Dscms2CoreCoClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2CoreCoClientServiceImpl.class);

    /**
     * 抵质押物的开户（co3200）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Co3200RespDto> co3200(Co3200ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CO3200.key.concat("|").concat(EsbEnum.TRADE_CODE_CO3200.value));
        return null;
    }

    /**
     * 抵质押物出入库处理
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Co3202RespDto> co3202(Co3202ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CO3202.key.concat("|").concat(EsbEnum.TRADE_CODE_CO3202.value));
        return null;
    }


    /**
     * 交易码：co3225
     * 交易描述：抵质押物明细查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Co3225RespDto> co3225(Co3225ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CO3225.key.concat("|").concat(EsbEnum.TRADE_CODE_CO3225.value));
        return null;
    }

    /**
     * 交易码：co3201
     * 交易描述：抵质押物信息修改
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Co3201RespDto> co3201(Co3201ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CO3201.key.concat("|").concat(EsbEnum.TRADE_CODE_CO3201.value));
        return null;
    }

    /**
     * 交易码：co3222
     * 交易描述：抵质押物单笔查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Co3222RespDto> co3222(Co3222ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CO3222.key.concat("|").concat(EsbEnum.TRADE_CODE_CO3222.value));
        return null;
    }


}



