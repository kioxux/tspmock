package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.Cbocr1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.resp.Cbocr1RespDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.req.Cbocr2ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp.Cbocr2RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2OcrClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 11:00
 * @since 2021/6/4 11:00
 */
@Component
public class Dscms2OcrClientServiceImpl implements Dscms2OcrClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2OcrClientService.class);

    /**
     * 交易码：cbocr1
     * 交易描述：新增批次报表数据
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cbocr1RespDto> cbocr1(Cbocr1ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CBOCR1.key.concat("|").concat(EsbEnum.TRADE_CODE_CBOCR1.value));
        return null;
    }

    /**
     * 交易码：cbocr2
     * 交易描述：识别任务的模板匹配结果列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cbocr2RespDto> cbocr2(Cbocr2ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CBOCR2.key.concat("|").concat(EsbEnum.TRADE_CODE_CBOCR2.value));
        return null;
    }
}
