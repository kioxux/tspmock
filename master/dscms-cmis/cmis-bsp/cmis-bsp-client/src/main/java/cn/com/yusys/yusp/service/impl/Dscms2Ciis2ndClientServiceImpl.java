package cn.com.yusys.yusp.service.impl;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi1.Credi1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi1.Credi1RespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi12.Credi12ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi12.Credi12RespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2Ciis2ndClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用二代征信系统的接口实现类
 */
@Component
public class Dscms2Ciis2ndClientServiceImpl implements Dscms2Ciis2ndClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2Ciis2ndClientServiceImpl.class);

    /**
     * ESB信贷查询接口（处理码credi1）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ciis2nd/credi1")
    @Override
    public ResultDto<Credi1RespDto> credi1(Credi1ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CREDI1.key.concat("|").concat(EsbEnum.TRADE_CODE_CREDI1.value));
        return null;
    }

    /**
     * ESB通用查询接口（处理码credi12）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Credi12RespDto> credi12(Credi12ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CRED12.key.concat("|").concat(EsbEnum.TRADE_CODE_CRED12.value));
        return null;
    }

    /**
     * 交易码：credxx
     * 交易描述：线下查询接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<CredxxRespDto> credxx(CredxxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CREDXX.key.concat("|").concat(EsbEnum.TRADE_CODE_CREDXX.value));
        return null;
    }


    /**
     * 交易码：credzb
     * 交易描述：指标通用接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<CredzbRespDto> credzb(CredzbReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CREDZB.key.concat("|").concat(EsbEnum.TRADE_CODE_CREDZB.value));
        return null;
    }
}
