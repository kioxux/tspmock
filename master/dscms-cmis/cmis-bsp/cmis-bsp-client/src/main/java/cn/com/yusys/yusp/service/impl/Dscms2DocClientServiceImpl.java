package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.req.Doc000ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.resp.Doc000RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc002.req.Doc002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc002.resp.Doc002RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc003.req.Doc003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc003.resp.Doc003RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc004.req.Doc004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc004.resp.Doc004RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.req.Doc005ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.resp.Doc005RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.req.Doc006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.resp.Doc006RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2DocClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * @version 0.1
 * @desc 档案系统
 * @date 2021/6/17 14:54
 * @since 2021/6/17 14:54
 */
@Component
public class Dscms2DocClientServiceImpl implements Dscms2DocClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2DocClientServiceImpl.class);

    /**
     * 交易码：doc000
     * 交易描述：入库接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Doc000RespDto> doc000(Doc000ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DOC000.key.concat("|").concat(EsbEnum.TRADE_CODE_DOC000.value));
        return null;
    }

    /**
     * 交易码：doc002
     * 交易描述：入库查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Doc002RespDto> doc002(Doc002ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DOC002.key.concat("|").concat(EsbEnum.TRADE_CODE_DOC002.value));
        return null;
    }

    @Override
    public ResultDto<Doc003RespDto> doc003(Doc003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DOC003.key.concat("|").concat(EsbEnum.TRADE_CODE_DOC003.value));
        return null;
    }

    /**
     * 交易码：doc005
     * 交易描述：申请出库
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Doc005RespDto> doc005(Doc005ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DOC005.key.concat("|").concat(EsbEnum.TRADE_CODE_DOC005.value));
        return null;
    }

    /**
     * 交易码：doc006
     * 交易描述：申请出库
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Doc006RespDto> doc006(Doc006ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DOC006.key.concat("|").concat(EsbEnum.TRADE_CODE_DOC006.value));
        return null;
    }

    /**
     * 交易码：doc004
     * 交易描述：调阅待出库查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Doc004RespDto> doc004(Doc004ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DOC004.key.concat("|").concat(EsbEnum.TRADE_CODE_DOC004.value));
        return null;
    }
}
