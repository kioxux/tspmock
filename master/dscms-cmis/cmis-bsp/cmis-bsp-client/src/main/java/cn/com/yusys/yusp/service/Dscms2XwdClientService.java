package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.req.Xwd001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd001.resp.Xwd001RespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req.Xwd009ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp.Xwd009RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2XwdClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用新微贷系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2XwdClientServiceImpl.class)
public interface Dscms2XwdClientService {

    /**
     * 交易码：xwd009
     * 交易描述：利率申请接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/xwd009")
    public ResultDto<Xwd009RespDto> xwd009(Xwd009ReqDto reqDto);

    /**
     * 交易码：xwd001
     * 交易描述：贷款申请接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwywglpt/xwd001")
    public ResultDto<Xwd001RespDto> xwd001(Xwd001ReqDto reqDto);
}
