package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.req.ClfxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.resp.ClfxcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.req.ClzjcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.resp.ClzjcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.req.JcxxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.resp.JcxxcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.req.SqfyyzReqDto;
import cn.com.yusys.yusp.dto.client.esb.zjywxt.sqfyyz.resp.SqfyyzRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2ZjywxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * <p>
 * BSP封装调用对公智能风控系统的接口
 */
@Component
public class Dscms2ZjywxtClientServiceImpl implements Dscms2ZjywxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2ZjywxtClientServiceImpl.class);

    /**
     * 交易码：clzjcx
     * 交易描述：未备注
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<ClzjcxRespDto> clzjcx(ClzjcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CLZJCX.key.concat("|").concat(EsbEnum.TRADE_CODE_CLZJCX.value));
        return null;
    }


    /**
     * 交易码：sqfyyz
     * 交易描述：行方验证房源信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<SqfyyzRespDto> sqfyyz(SqfyyzReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_SQFYYZ.key.concat("|").concat(EsbEnum.TRADE_CODE_SQFYYZ.value));
        return null;
    }


    /**
     * 交易码：jcxxcx
     * 交易描述：缴存信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<JcxxcxRespDto> jcxxcx(JcxxcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_JCXXCX.key.concat("|").concat(EsbEnum.TRADE_CODE_JCXXCX.value));
        return null;
    }

    /**
     * 交易码：clfxcx
     * 交易描述：协议信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<ClfxcxRespDto> clfxcx(ClfxcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CLFXCX.key.concat("|").concat(EsbEnum.TRADE_CODE_CLFXCX.value));
        return null;
    }
}
