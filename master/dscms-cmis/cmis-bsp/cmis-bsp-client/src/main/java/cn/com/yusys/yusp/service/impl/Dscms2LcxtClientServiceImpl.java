package cn.com.yusys.yusp.service.impl;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.req.Lc0323ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lcxt.lc0323.resp.Lc0323RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2LcxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * BSP封装调用理财系统的接口实现类
 */
@Component
public class Dscms2LcxtClientServiceImpl implements Dscms2LcxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2LcxtClientServiceImpl.class);

    /**
     * 交易码：lc0323
     * 交易描述：查询理财是否冻结
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Lc0323RespDto> lc0323(Lc0323ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_LC0323.key.concat("|").concat(EsbEnum.TRADE_CODE_LC0323.value));
        return null;
    }

}
