package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.income.req.IncomeReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.income.resp.IncomeRespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.req.QuotaReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp.QuotaRespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.req.XdhxQueryTotalListReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.XdhxQueryTotalListRespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.req.ZcOverallBusinessIncomeReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.resp.ZcOverallBusinessIncomeRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2SjztClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:数据中台
 *
 * @author chenyong
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2SjztClientServiceImpl.class)
public interface Dscms2SjztClientService {
    /**
     * 交易码：xdhxQueryTotalList
     * 交易描述：信贷客户核心业绩统计查询列表
     *
     * @param reqDto
     * @return
     * @code
     */
    @PostMapping("/dscms2sjzt/xdhxQueryTotalList")
    public ResultDto<XdhxQueryTotalListRespDto> xdhxQueryTotalList(XdhxQueryTotalListReqDto reqDto);


    /**
     * 交易码：zcOverallBusinessIncome
     * 交易描述：资产池业务总体收益情况详情
     *
     * @param reqDto
     * @return
     * @code
     */
    @PostMapping("/dscms2sjzt/zcOverallBusinessIncome")
    public ResultDto<ZcOverallBusinessIncomeRespDto> zcOverallBusinessIncome(ZcOverallBusinessIncomeReqDto reqDto);


    /**
     * 交易码：income
     * 交易描述：资产池业务总体收益情况详情
     *
     * @param reqDto
     * @return
     * @code
     */
    @PostMapping("/dscms2sjzt/income")
    public ResultDto<IncomeRespDto> income(IncomeReqDto reqDto);


    /**
     * 交易码：quota
     * 交易描述：客户我行授用信分布
     *
     * @param reqDto
     * @return
     * @code
     */
    @PostMapping("/dscms2sjzt/quota")
    public ResultDto<QuotaRespDto> quota(QuotaReqDto reqDto);
}
