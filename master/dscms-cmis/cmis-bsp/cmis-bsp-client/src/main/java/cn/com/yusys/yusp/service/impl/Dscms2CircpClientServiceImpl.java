package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.req.Fb1146ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1146.resp.Fb1146RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.req.Fb1147ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1147.resp.Fb1147RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1149.req.Fb1149ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1149.resp.Fb1149RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.Fb1150ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1150.resp.Fb1150RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1161.req.Fb1161ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1161.resp.Fb1161RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1166.req.Fb1166ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1166.resp.Fb1166RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1167.req.Fb1167ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1167.resp.Fb1167RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.req.Fb1168ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1168.resp.Fb1168RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Fb1170ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1170.resp.Fb1170RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.req.Fb1174ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1174.resp.Fb1174RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1203.req.Fb1203ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1203.resp.Fb1203RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1205.req.Fb1205ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1205.resp.Fb1205RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1213.req.Fb1213ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1213.resp.Fb1213RespDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.req.Fb1214ReqDto;
import cn.com.yusys.yusp.dto.client.esb.circp.fb1214.resp.Fb1214RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2CircpClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * <p>
 * BSP封装调用对公智能风控系统的接口
 */
@Component
public class Dscms2CircpClientServiceImpl implements Dscms2CircpClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2CircpClientServiceImpl.class);

    /**
     * 交易码：fb1161
     * 交易描述：借据信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1161RespDto> fb1161(Fb1161ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1161.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1161.value));
        return null;
    }

    /**
     * 交易码：fb1166
     * 交易描述：面签邀约结果推送
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1166RespDto> fb1166(Fb1166ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1166.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1166.value));
        return null;
    }

    /**
     * 交易码：fb1167
     * 交易描述：企业征信查询通知
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1167RespDto> fb1167(Fb1167ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1167.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1167.value));
        return null;
    }


    /**
     * 交易码：fb1168
     * 交易描述：抵押查封结果推送
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1168RespDto> fb1168(Fb1168ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1168.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1168.value));
        return null;
    }

    /**
     * 交易码：fb1170
     * 交易描述：最高额借款合同信息推送
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1170RespDto> fb1170(Fb1170ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1170.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1170.value));
        return null;
    }


    /**
     * 交易码：fb1203
     * 交易描述：质押物金额覆盖校验
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1203RespDto> fb1203(Fb1203ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1203.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1203.value));
        return null;
    }

    /**
     * 交易码：fb1205
     * 交易描述：放款信息推送
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1205RespDto> fb1205(Fb1205ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1205.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1205.value));
        return null;
    }

    /**
     * 交易码：fb1213
     * 交易描述：押品出库通知
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1213RespDto> fb1213(Fb1213ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1213.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1213.value));
        return null;
    }

    /**
     * 交易码：fb1146
     * 交易描述：受托信息审核
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1146RespDto> fb1146(Fb1146ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1146.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1146.value));
        return null;
    }

    /**
     * 交易码：fb1147
     * 交易描述：无还本续贷申请
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1147RespDto> fb1147(Fb1147ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1147.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1147.value));
        return null;
    }

    /**
     * 交易码：fb1149
     * 交易描述：无还本续贷借据更新
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1149RespDto> fb1149(Fb1149ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1149.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1149.value));
        return null;
    }

    /**
     * 交易码：fb1150
     * 交易描述：省心快贷plus授信申请
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1150RespDto> fb1150(Fb1150ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1150.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1150.value));
        return null;
    }

    /**
     * 交易码：fb1214
     * 交易描述：房产信息修改同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1214RespDto> fb1214(Fb1214ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1214.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1214.value));
        return null;
    }

    /**
     * 交易码：fb1174
     * 交易描述：房抵e点贷尽调结果通知
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fb1174RespDto> fb1174(Fb1174ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FB1174.key.concat("|").concat(EsbEnum.TRADE_CODE_FB1174.value));
        return null;
    }
}
