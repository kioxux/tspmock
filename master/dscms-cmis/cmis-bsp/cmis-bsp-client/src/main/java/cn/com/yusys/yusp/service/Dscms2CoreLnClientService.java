package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3002.req.Ln3002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3002.resp.Ln3002RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3005.Ln3005ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3005.Ln3005RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3006.req.Ln3006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3006.resp.Ln3006RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3007.req.Ln3007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3007.resp.Ln3007RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.req.Ln3020ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp.Ln3020RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3025.Ln3025RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3026.Ln3026ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3026.Ln3026RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3030.Ln3030RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3031.Ln3031ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3031.Ln3031RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.req.Ln3032ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3032.resp.Ln3032RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3036.req.Ln3036ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp.Ln3036RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3038.req.Ln3038ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3038.resp.Ln3038RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.req.Ln3041ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3041.resp.Ln3041RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3042.req.Ln3042ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3042.resp.Ln3042RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3043.req.Ln3043ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3043.resp.Ln3043RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3045.Ln3045RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3046.Ln3046ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3046.Ln3046RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3047.Ln3047ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3047.Ln3047RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.req.Ln3053ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3053.resp.Ln3053RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.req.Ln3054ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3054.resp.Ln3054RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.req.Ln3055ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3055.resp.Ln3055RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3057.req.Ln3057ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3057.resp.Ln3057RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3061.Ln3061ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3061.Ln3061RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3062.req.Ln3062ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3062.resp.Ln3062RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Ln3063ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3063.Ln3063RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3064.req.Ln3064ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3064.resp.Ln3064RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3065.Ln3065ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3065.Ln3065RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3066.req.Ln3066ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3066.resp.Ln3066RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3069.req.Ln3069ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp.Ln3069RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3070.req.Ln3070ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3070.resp.Ln3070RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3073.req.Ln3073ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3073.resp.Ln3073RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3074.req.Ln3074ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3074.resp.Ln3074RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3075.req.Ln3075ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3075.resp.Ln3075RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.req.Ln3077ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3077.resp.Ln3077RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3078.Ln3078ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3078.Ln3078RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3079.Ln3079ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3079.Ln3079RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3083.req.Ln3083ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3083.resp.Ln3083RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3085.req.Ln3085ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3085.resp.Ln3085RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3091.Ln3091ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3091.Ln3091RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.req.Ln3100ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp.Ln3100RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3102.Ln3102RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3103.Ln3103RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3104.Ln3104RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3105.Ln3105ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3105.Ln3105RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3106.Ln3106ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3106.Ln3106RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3107.Ln3107ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3107.Ln3107RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3108.Ln3108RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3110.Ln3110RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3111.Ln3111RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3112.Ln3112ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3112.Ln3112RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3114.Ln3114ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3114.Ln3114RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3120.req.Ln3120ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3120.resp.Ln3120RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3123.Ln3123ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3123.Ln3123RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3126.req.Ln3126ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp.Ln3126RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3128.req.Ln3128ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3128.resp.Ln3128RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3135.req.Ln3135ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3135.resp.Ln3135RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3138.Ln3138ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3138.Ln3138RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3139.req.Ln3139ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3139.resp.Ln3139RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3160.Ln3160ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3160.Ln3160RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3161.Ln3161ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3161.Ln3161RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3163.Ln3163ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3163.Ln3163RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3169.Ln3169ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3169.Ln3169RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3176.Ln3176ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3176.Ln3176RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3177.Ln3177ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3177.Ln3177RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3196.Ln3196ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3196.Ln3196RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Ln3235ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3235.Ln3235RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3236.Ln3236ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3236.Ln3236RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3243.Ln3243ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3243.Ln3243RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3245.req.Ln3245ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3245.resp.Ln3245RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3246.Ln3246RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3247.req.Ln3247ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3247.resp.Ln3247RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3248.req.Ln3248ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3248.resp.Ln3248RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3249.req.Ln3249ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3249.resp.Ln3249RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.req.Ln3251ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ln3251.resp.Ln3251RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2CoreLnClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用核心系统的接口(Ln开头)
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2CoreLnClientServiceImpl.class)
public interface Dscms2CoreLnClientService {

    /**
     * 企业工商信息查询
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallLn3196Example.java.txt
     */
    @PostMapping("/dscms2coreln/ln3196")
    public ResultDto<Ln3196RespDto> ln3196(Ln3196ReqDto reqDto);


    /**
     * 交易码：ln3110
     * 交易描述：用于贷款放款前进行还款计划试算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3110")
    public ResultDto<Ln3110RespDto> ln3110(Ln3110ReqDto reqDto);

    /**
     * 根贷款还款计划明细查询（处理码ln3246）
     *
     * @param reqDto
     * @return
     * @code 示例代码
     */
    @PostMapping("/dscms2coreln/ln3246")
    public ResultDto<Ln3246RespDto> ln3246(Ln3246ReqDto reqDto);

    /**
     * 交易码：ln3123
     * 交易描述：受托支付查询
     *
     * @param reqDto 请求体
     * @return ResultDto<Ln3123RespDto> 封装的返回结果
     */
    @PostMapping("/dscms2coreln/ln3123")
    public ResultDto<Ln3123RespDto> ln3123(Ln3123ReqDto reqDto);

    /**
     * 交易码：ln3114
     * 交易描述：贷款提前还款期供试算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3114")
    public ResultDto<Ln3114RespDto> ln3114(Ln3114ReqDto reqDto);

    /**
     * 交易码：ln3112
     * 交易描述：贷款展期查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3112")
    public ResultDto<Ln3112RespDto> ln3112(Ln3112ReqDto reqDto);

    /**
     * 交易码：ln3111
     * 交易描述：贷款归还试算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3111")
    public ResultDto<Ln3111RespDto> ln3111(Ln3111ReqDto reqDto);

    /**
     * 交易码：ln3108
     * 交易描述：贷款组合查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3108")
    public ResultDto<Ln3108RespDto> ln3108(Ln3108ReqDto reqDto);

    /**
     * 交易码：ln3107
     * 交易描述：贷款形态转移明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3107")
    public ResultDto<Ln3107RespDto> ln3107(Ln3107ReqDto reqDto);

    /**
     * 交易码：ln3106
     * 交易描述：贷款利率变更明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3106")
    public ResultDto<Ln3106RespDto> ln3106(Ln3106ReqDto reqDto);

    /**
     * 交易码：ln3105
     * 交易描述：贷款期供交易明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3105")
    public ResultDto<Ln3105RespDto> ln3105(Ln3105ReqDto reqDto);

    /**
     * 交易码：ln3104
     * 交易描述：客户账交易明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3104")
    public ResultDto<Ln3104RespDto> ln3104(Ln3104ReqDto reqDto);

    /**
     * 交易码：ln3102
     * 交易描述：贷款期供查询试算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3102")
    public ResultDto<Ln3102RespDto> ln3102(Ln3102ReqDto reqDto);

    /**
     * 交易码：ln3032
     * 交易描述：受托支付信息维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3032")
    public ResultDto<Ln3032RespDto> ln3032(Ln3032ReqDto reqDto);

    /**
     * 交易码：ln3036
     * 交易描述：针对录入、复核模式的交易，用于查询信贷录入信息的查询，复核时使用
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3036")
    public ResultDto<Ln3036RespDto> ln3036(Ln3036ReqDto reqDto);

    /**
     * 交易码：ln3041
     * 交易描述：支持提前归还贷款、归还借据欠款、全部结清借据多种类型的贷款归还；
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3041")
    public ResultDto<Ln3041RespDto> ln3041(Ln3041ReqDto reqDto);

    /**
     * 交易码：ln3042
     * 交易描述：手工指定归还某项利息金额，不按既定顺序归还
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3042")
    public ResultDto<Ln3042RespDto> ln3042(Ln3042ReqDto reqDto);

    /**
     * 交易码：ln3057
     * 交易描述：主要实现对贷款期限的缩短和延长修改，实时生效。
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3057")
    public ResultDto<Ln3057RespDto> ln3057(Ln3057ReqDto reqDto);

    /**
     * 交易码：ln3070
     * 交易描述：贷款录入信息的取消，即取消出账指令
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3070")
    public ResultDto<Ln3070RespDto> ln3070(Ln3070ReqDto reqDto);

    /**
     * 交易码：ln3074
     * 交易描述：用于贷款预约展期
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3074")
    public ResultDto<Ln3074RespDto> ln3074(Ln3074ReqDto reqDto);


    /**
     * 交易码：ln3139
     * 交易描述：通过贷款借据号和贷款账号的交易日期试算贷款欠息明细。
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3139")
    public ResultDto<Ln3139RespDto> ln3139(Ln3139ReqDto reqDto);

    /**
     * 交易码：ln3054
     * 交易描述：调整贷款还款方式
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3054")
    public ResultDto<Ln3054RespDto> ln3054(Ln3054ReqDto reqDto);

    /**
     * 交易码：ln3073
     * 交易描述：用于贷款定制期供计划的修改调整
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3073")
    public ResultDto<Ln3073RespDto> ln3073(Ln3073ReqDto reqDto);


    /**
     * 交易码：ln3055
     * 交易描述：贷款利率调整
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3055")
    public ResultDto<Ln3055RespDto> ln3055(Ln3055ReqDto reqDto);


    /**
     * 交易码：ln3135
     * 交易描述：通过贷款借据号和贷款账号查询贷款期供计划
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3135")
    public ResultDto<Ln3135RespDto> ln3135(Ln3135ReqDto reqDto);


    /**
     * 交易码：ln3100
     * 交易描述：根据借据号或者贷款账号查询单笔借据的详细信息，主要包括借据信息、定价信息、还款信息、放款信息等
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3100")
    public ResultDto<Ln3100RespDto> ln3100(Ln3100ReqDto reqDto);

    /**
     * 交易码：ln3020
     * 交易描述：贷款开户
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3020")
    public ResultDto<Ln3020RespDto> ln3020(Ln3020ReqDto reqDto);

    /**
     * 交易码：ln3177
     * 交易描述：资产转让付款状态维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3177")
    public ResultDto<Ln3177RespDto> ln3177(Ln3177ReqDto reqDto);

    /**
     * 交易码：ln3235
     * 交易描述：还款方式转换
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3235")
    public ResultDto<Ln3235RespDto> ln3235(Ln3235ReqDto reqDto);

    /**
     * 交易码：ln3236
     * 交易描述：贷款账隔日冲正
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3236")
    public ResultDto<Ln3236RespDto> ln3236(Ln3236ReqDto reqDto);

    /**
     * 交易码：ln3243
     * 交易描述：停息贷款利息试算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3243")
    public ResultDto<Ln3243RespDto> ln3243(Ln3243ReqDto reqDto);

    /**
     * 交易码：ln3005
     * 交易描述：贷款产品查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3005")
    public ResultDto<Ln3005RespDto> ln3005(Ln3005ReqDto reqDto);

    /**
     * 交易码：ln3025
     * 交易描述：垫款开户
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3025")
    public ResultDto<Ln3025RespDto> ln3025(Ln3025ReqDto reqDto);

    /**
     * 交易码：ln3103
     * 交易描述：贷款账户交易明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3103")
    public ResultDto<Ln3103RespDto> ln3103(Ln3103ReqDto reqDto);

    /**
     * 交易码：ln3138
     * 交易描述：等额等本贷款推算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3138")
    public ResultDto<Ln3138RespDto> ln3138(Ln3138ReqDto reqDto);

    /**
     * 交易码：ln3160
     * 交易描述：资产证券化信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3160")
    public ResultDto<Ln3160RespDto> ln3160(Ln3160ReqDto reqDto);

    /**
     * 交易码：ln3161
     * 交易描述：资产证券化协议登记
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3161")
    public ResultDto<Ln3161RespDto> ln3161(Ln3161ReqDto reqDto);

    /**
     * 交易码：ln3163
     * 交易描述：资产证券化处理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3163")
    public ResultDto<Ln3163RespDto> ln3163(Ln3163ReqDto reqDto);

    /**
     * 交易码：ln3169
     * 交易描述：资产证券化处理文件导入
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3169")
    public ResultDto<Ln3169RespDto> ln3169(Ln3169ReqDto reqDto);

    /**
     * 交易码：ln3026
     * 交易描述：银团贷款开户
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3026")
    public ResultDto<Ln3026RespDto> ln3026(Ln3026ReqDto reqDto);

    /**
     * 交易码：ln3045
     * 交易描述：贷款核销处理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3045")
    public ResultDto<Ln3045RespDto> ln3045(Ln3045ReqDto reqDto);

    /**
     * 交易码：ln3046
     * 交易描述：贷款核销归还
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3046")
    public ResultDto<Ln3046RespDto> ln3046(Ln3046ReqDto reqDto);

    /**
     * 交易码：ln3047
     * 交易描述：质押还贷
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3047")
    public ResultDto<Ln3047RespDto> ln3047(Ln3047ReqDto reqDto);

    /**
     * 交易码：ln3061
     * 交易描述：资产转让协议登记
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3061")
    public ResultDto<Ln3061RespDto> ln3061(Ln3061ReqDto reqDto);

    /**
     * 交易码：ln3063
     * 交易描述：资产转让处理
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3063")
    public ResultDto<Ln3063RespDto> ln3063(Ln3063ReqDto reqDto);

    /**
     * 交易码：ln3065
     * 交易描述：资产转让资金划转
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3065")
    public ResultDto<Ln3065RespDto> ln3065(Ln3065ReqDto reqDto);

    /**
     * 交易码：ln3078
     * 交易描述：贷款机构变更
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3078")
    public ResultDto<Ln3078RespDto> ln3078(Ln3078ReqDto reqDto);

    /**
     * 交易码：ln3079
     * 交易描述：贷款产品变更
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3079")
    public ResultDto<Ln3079RespDto> ln3079(Ln3079ReqDto reqDto);

    /**
     * 交易码：ln3091
     * 交易描述：待付款指令查询
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/dscms2coreln/ln3091")
    public ResultDto<Ln3091RespDto> ln3091(Ln3091ReqDto reqDto);

    /**
     * 交易码：ln3030
     * 交易描述：贷款资料维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3030")
    public ResultDto<Ln3030RespDto> ln3030(Ln3030ReqDto reqDto);

    /**
     * 交易码：ln3031
     * 交易描述：贷款资料维护（关于还款）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3031")
    public ResultDto<Ln3031RespDto> ln3031(Ln3031ReqDto reqDto);

    /**
     * 交易码：ln3053
     * 交易描述：还款计划调整
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3053")
    public ResultDto<Ln3053RespDto> ln3053(Ln3053ReqDto reqDto);

    /**
     * 交易码：ln3002
     * 交易描述：贷款产品删除
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3002")
    public ResultDto<Ln3002RespDto> ln3002(Ln3002ReqDto reqDto);

    /**
     * 交易码：ln3002
     * 交易描述：查询某一产品下配置了几套币种参数
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3007")
    public ResultDto<Ln3007RespDto> ln3007(Ln3007ReqDto reqDto);

    /**
     * 交易码：ln3043
     * 交易描述：贷款指定期供归还
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3043")
    public ResultDto<Ln3043RespDto> ln3043(Ln3043ReqDto reqDto);

    /**
     * 交易码：ln3043
     * 交易描述：根据协议编号查询资产转让协议登记簿明细信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3069")
    public ResultDto<Ln3069RespDto> ln3069(Ln3069ReqDto reqDto);

    /**
     * 交易码：ln3038
     * 交易描述：贷款资料变更明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3038")
    public ResultDto<Ln3038RespDto> ln3038(Ln3038ReqDto reqDto);

    /**
     * 交易码：ln3083
     * 交易描述：银团协议查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3083")
    public ResultDto<Ln3083RespDto> ln3083(Ln3083ReqDto reqDto);

    /**
     * 交易码：ln3176
     * 交易描述：本交易用户贷款多个还款账户进行还款
     * 交易码：ln3006
     * 交易描述：贷款产品组合查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3176")
    public ResultDto<Ln3176RespDto> ln3176(Ln3176ReqDto reqDto);

    /**
     * 交易码：ln3006
     * 交易描述：贷款产品组合查询
     *
     * @param reqDto
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3006")
    public ResultDto<Ln3006RespDto> ln3006(Ln3006ReqDto reqDto);

    /**
     * 交易码：ln3077
     * 交易描述：客户账本息调整
     *
     * @param reqDto
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3077")
    public ResultDto<Ln3077RespDto> ln3077(Ln3077ReqDto reqDto);

    /**
     * 交易码：ln3085
     * 交易描述：贷款本息减免
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3085")
    public ResultDto<Ln3085RespDto> ln3085(Ln3085ReqDto reqDto);

    /**
     * 交易码：ln3128
     * 交易描述：贷款机构变更查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3128")
    public ResultDto<Ln3128RespDto> ln3128(Ln3128ReqDto reqDto);

    /**
     * 交易码：ln3245
     * 交易描述：核销登记簿查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3245")
    public ResultDto<Ln3245RespDto> ln3245(Ln3245ReqDto reqDto);


    /**
     * 交易码：ln3120
     * 交易描述：查询抵质押物录入信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3120")
    public ResultDto<Ln3120RespDto> ln3120(Ln3120ReqDto reqDto);

    /**
     * 交易码：ln3126
     * 交易描述：贷款费用交易明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3126")
    public ResultDto<Ln3126RespDto> ln3126(Ln3126ReqDto reqDto);

    /**
     * 交易码：ln3062
     * 交易描述：资产转让借据维护
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/dscms2coreln/ln3062")
    public ResultDto<Ln3062RespDto> ln3062(Ln3062ReqDto reqDto);

    /**
     * 交易码：ln3064
     * 交易描述：资产转让内部借据信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3064")
    public ResultDto<Ln3064RespDto> ln3064(Ln3064ReqDto reqDto);


    /**
     * 交易码：ln3066
     * 交易描述：资产转让借据筛选
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3066")
    public ResultDto<Ln3066RespDto> ln3066(Ln3066ReqDto reqDto);

    /**
     * 交易码：ln3248
     * 交易描述：委托清收变更
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3248")
    public ResultDto<Ln3248RespDto> ln3248(Ln3248ReqDto reqDto);


    /**
     * 交易码：ln3249
     * 交易描述：贷款指定日期利息试算
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3249")
    public ResultDto<Ln3249RespDto> ln3249(Ln3249ReqDto reqDto);


    /**
     * 交易码：ln3247
     * 交易描述：贷款还款周期预定变更
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3247")
    public ResultDto<Ln3247RespDto> ln3247(Ln3247ReqDto reqDto);

    /**
     * @param reqDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<cn.com.yusys.yusp.dto.client.esb.core.ln3247.resp.Ln3247RespDto>
     * @author 王玉坤
     * @date 2021/8/27 22:42
     * @version 1.0.0
     * @desc 贷款自动追缴明细查询
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/dscms2coreln/ln3251")
    public ResultDto<Ln3251RespDto> ln3251(Ln3251ReqDto reqDto);


    /**
     * 交易码：ln3075
     * 交易描述：逾期贷款展期
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreln/ln3075")
    public ResultDto<Ln3075RespDto> ln3075(Ln3075ReqDto reqDto);

}






