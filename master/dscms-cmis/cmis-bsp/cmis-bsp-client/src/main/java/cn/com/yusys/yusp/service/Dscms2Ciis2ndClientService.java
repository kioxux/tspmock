package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi1.Credi1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi1.Credi1RespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi12.Credi12ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credi12.Credi12RespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credxx.CredxxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb.CredzbRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2Ciis2ndClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用二代征信系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2Ciis2ndClientServiceImpl.class)
public interface Dscms2Ciis2ndClientService {
    /**
     * ESB信贷查询接口（处理码credi1）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCredi1Example.java.txt
     */
    @PostMapping("/dscms2ciis2nd/credi1")
    public ResultDto<Credi1RespDto> credi1(Credi1ReqDto reqDto);



    /**
     * ESB通用查询接口（处理码cred12）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCredi12Example.java.txt
     */
    @PostMapping("/dscms2ciis2nd/credi12")
    public ResultDto<Credi12RespDto> credi12(Credi12ReqDto reqDto);

    /**
     * 交易码：credxx
     * 交易描述：线下查询接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ciis2nd/credxx")
    public ResultDto<CredxxRespDto> credxx(CredxxReqDto reqDto);

    /**
     * 交易码：credzb
     * 交易描述：指标通用接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ciis2nd/credzb")
    public ResultDto<CredzbRespDto> credzb(CredzbReqDto reqDto);
}
