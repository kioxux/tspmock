package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentRespDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.req.XxdrelReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.XxdrelRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2RlzyxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用人力资源系统系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2RlzyxtClientServiceImpl.class)
public interface Dscms2RlzyxtClientService {

    /**
     * 交易码：xxdrel
     * 交易描述：查询人员基本信息岗位信息家庭信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2rlzyxt/xxdrel")
    public ResultDto<XxdrelRespDto> xxdrel(XxdrelReqDto reqDto);

    /**
     * 交易码：xxdent
     * 交易描述：新入职人员信息登记
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2rlzyxt/xxdent")
    public ResultDto<XxdentRespDto> xxdent(XxdentReqDto reqDto);
}
