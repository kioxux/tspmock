package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2DxptClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用短信平台的接口实现类
 */
@Component
public class Dscms2DxptClientServiceImpl implements Dscms2DxptClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2DxptClientServiceImpl.class);

    /**
     * 短信/微信发送批量接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<SenddxRespDto> senddx(SenddxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_SENDDX.key.concat("|").concat(EsbEnum.TRADE_CODE_SENDDX.value));
        return null;
    }
}
