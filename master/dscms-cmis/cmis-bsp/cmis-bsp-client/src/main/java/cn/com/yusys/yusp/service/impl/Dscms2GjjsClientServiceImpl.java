package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj07.Xdgj07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj07.Xdgj07RespDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj08.Xdgj08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj08.Xdgj08RespDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09RespDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj10.Xdgj10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj10.Xdgj10RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2GjjsClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口类:国际结算系统
 */
@Component
public class Dscms2GjjsClientServiceImpl implements Dscms2GjjsClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2GjjsClientServiceImpl.class);

    /**
     * 交易码：xdgj07
     * 交易描述：信贷发送台账信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdgj07RespDto> xdgj07(Xdgj07ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDGJ07.key.concat("|").concat(EsbEnum.TRADE_CODE_XDGJ07.value));
        return null;
    }

    /**
     * 交易码：xdgj08
     * 交易描述：信贷还款信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdgj08RespDto> xdgj08(Xdgj08ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDGJ08.key.concat("|").concat(EsbEnum.TRADE_CODE_XDGJ08.value));
        return null;
    }

    /**
     * 交易码：xdgj09
     * 交易描述：信贷查询牌价信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdgj09RespDto> xdgj09(Xdgj09ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDGJ09.key.concat("|").concat(EsbEnum.TRADE_CODE_XDGJ09.value));
        return null;
    }


    /**
     * 交易码：xdgj10
     * 交易描述：信贷获取国结信用证信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdgj10RespDto> xdgj10(Xdgj10ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDGJ10.key.concat("|").concat(EsbEnum.TRADE_CODE_XDGJ10.value));
        return null;
    }
}
