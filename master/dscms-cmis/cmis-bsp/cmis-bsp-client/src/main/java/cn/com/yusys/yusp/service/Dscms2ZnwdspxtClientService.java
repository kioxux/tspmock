package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.req.Znsp05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.resp.Znsp05RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.req.Znsp06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.resp.Znsp06RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.req.Znsp07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.resp.Znsp07RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.req.Znsp08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.resp.Znsp08RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2ZnwdspxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:智能微贷审批系统
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2ZnwdspxtClientServiceImpl.class)
public interface Dscms2ZnwdspxtClientService {
    /**
     * 交易码：znsp04
     * 交易描述：自动审批调查报告
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallZnsp04Example,Znsp04ReqDtoSet,Znsp04RespDtoGet
     */
    @PostMapping("/dscms2znwdspxt/znsp04")
    public ResultDto<Znsp04RespDto> znsp04(Znsp04ReqDto reqDto);


    /**
     * 交易码：znsp08
     * 交易描述：客户调查撤销接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2znwdspxt/znsp08")
    public ResultDto<Znsp08RespDto> znsp08(Znsp08ReqDto reqDto);


    /**
     * 交易码：znsp05
     * 交易描述：风险拦截截接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2znwdspxt/znsp05")
    public ResultDto<Znsp05RespDto> znsp05(Znsp05ReqDto reqDto);


    /**
     * 交易码：znsp06
     * 交易描述：被拒绝的线上产品推送接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2znwdspxt/znsp06")
    public ResultDto<Znsp06RespDto> znsp06(Znsp06ReqDto reqDto);


    /**
     * 交易码：znsp07
     * 交易描述：数据修改接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2znwdspxt/znsp07")
    public ResultDto<Znsp07RespDto> znsp07(Znsp07ReqDto reqDto);
}
