package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.req.Dp2098ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Dp2098RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2200.Dp2200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2200.Dp2200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2280.Dp2280ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2280.Dp2280RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2352.Dp2352ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2352.Dp2352RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2CoreDpClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用核心系统的接口(dp开头)
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2CoreDpClientServiceImpl.class)
public interface Dscms2CoreDpClientService {


    /**
     * 子账户序号查询（dp2280）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallDp2280Example.java.txt
     */
    @PostMapping("/dscms2coredp/dp2280")
    public ResultDto<Dp2280RespDto> dp2280(Dp2280ReqDto reqDto);

    /**
     * 交易码：dp2667
     * 交易描述：组合账户特殊账户查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coredp/dp2667")
    public ResultDto<Dp2667RespDto> dp2667(Dp2667ReqDto reqDto);

    /**
     * 交易码：dp2200
     * 交易描述：保证金账户部提
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coredp/dp2200")
    public ResultDto<Dp2200RespDto> dp2200(Dp2200ReqDto reqDto);

    /**
     * 交易码：dp2352
     * 交易描述：组合账户子账户开立
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coredp/dp2352")
    public ResultDto<Dp2352RespDto> dp2352(Dp2352ReqDto reqDto);

    /**
     * 交易码：dp2099
     * 交易描述：保证金账户查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coredp/dp2099")
    public ResultDto<Dp2099RespDto> dp2099(Dp2099ReqDto reqDto);

    /**
     * 交易码：dp2021
     * 交易描述：客户账号-子账号互查
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coredp/dp2021")
    public ResultDto<Dp2021RespDto> dp2021(Dp2021ReqDto reqDto);


    /**
     * 交易码：dp2098
     * 交易描述：待清算账户查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coredp/dp2098")
    public ResultDto<Dp2098RespDto> dp2098(Dp2098ReqDto reqDto);
}






