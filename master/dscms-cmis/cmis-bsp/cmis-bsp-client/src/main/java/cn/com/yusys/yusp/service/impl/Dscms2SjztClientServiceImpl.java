package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.income.req.IncomeReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.income.resp.IncomeRespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.req.QuotaReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp.QuotaRespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.req.XdhxQueryTotalListReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp.XdhxQueryTotalListRespDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.req.ZcOverallBusinessIncomeReqDto;
import cn.com.yusys.yusp.dto.client.http.sjzt.zcOverallBusinessIncome.resp.ZcOverallBusinessIncomeRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2SjztClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/22 14:48
 * @since 2021/7/22 14:48
 */
@Component
public class Dscms2SjztClientServiceImpl implements Dscms2SjztClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2SjztClientServiceImpl.class);

    @Override
    public ResultDto<XdhxQueryTotalListRespDto> xdhxQueryTotalList(XdhxQueryTotalListReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.key.concat("|").concat(EsbEnum.TRADE_CODE_XDHXQUERYTOTALLIST.value));
        return null;
    }

    @Override
    public ResultDto<ZcOverallBusinessIncomeRespDto> zcOverallBusinessIncome(ZcOverallBusinessIncomeReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.key.concat("|").concat(EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.value));
        return null;
    }

    @Override
    public ResultDto<IncomeRespDto> income(IncomeReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.key.concat("|").concat(EsbEnum.TRADE_CODE_ZCOVERALLBUSINESSINCOME.value));
        return null;
    }

    @Override
    public ResultDto<QuotaRespDto> quota(QuotaReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_QUOTA.key.concat("|").concat(EsbEnum.TRADE_CODE_QUOTA.value));
        return null;
    }
}
