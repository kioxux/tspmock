package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.req.CljctzReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.resp.CljctzRespDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.req.IdchekReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.req.MfzjcrReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.resp.MfzjcrRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2GapsClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;


/**
 * BSP封装调用GAPS系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2GapsClientServiceImpl.class)
public interface Dscms2GapsClientService {

    /**
     * 交易码：idchek
     * 交易描述：身份证核查
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gaps/idchek")
    public ResultDto<IdchekRespDto> idchek(IdchekReqDto reqDto);

    /**
     * 交易码：mfzjcr
     * 交易描述：买方资金存入
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gaps/mfzjcr")
    public ResultDto<MfzjcrRespDto> mfzjcr(MfzjcrReqDto reqDto);


    /**
     * 交易码：cljctz
     * 交易描述：连云港存量房列表
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gaps/cljctz")
    public ResultDto<CljctzRespDto> cljctz(CljctzReqDto reqDto);

}
