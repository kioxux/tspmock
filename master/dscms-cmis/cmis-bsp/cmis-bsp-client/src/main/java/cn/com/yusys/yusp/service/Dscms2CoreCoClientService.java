package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3200.Co3200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3201.Co3201ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3201.Co3201RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3202.Co3202RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3225.req.Co3225ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3225.resp.Co3225RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3222.Co3222ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.co3222.Co3222RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2CoreCoClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用核心系统的接口(co开头)
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2CoreCoClientServiceImpl.class)
public interface Dscms2CoreCoClientService {

    /**
     * 抵质押物的开户（co3200）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCo3200Example.java.txt
     */
    @PostMapping("/dscms2coreco/co3200")
    public ResultDto<Co3200RespDto> co3200(Co3200ReqDto reqDto);

    /**
     * 抵质押物出入库处理
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCo3202Example.java.txt
     */
    @PostMapping("/dscms2coreco/co3202")
    public ResultDto<Co3202RespDto> co3202(Co3202ReqDto reqDto);

    /**
     * 交易码：co3225
     * 交易描述：抵质押物明细查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreco/co3225")
    public ResultDto<Co3225RespDto> co3225(Co3225ReqDto reqDto);

    /**
     * 交易码：co3201
     * 交易描述：抵质押物信息修改
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreco/co3201")
    public ResultDto<Co3201RespDto> co3201(Co3201ReqDto reqDto);

    /**
     * 交易码：co3222
     * 交易描述：抵质押物单笔查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreco/co3222")
    public ResultDto<Co3222RespDto> co3222(Co3222ReqDto reqDto);
}






