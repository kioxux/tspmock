package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.req.YxljcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yxxt.yxljcx.resp.YxljcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2YxxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 13:34
 * @since 2021/6/8 13:34
 */
@Component
public class Dscms2YxxtClientServiceImpl implements Dscms2YxxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2YxxtClientService.class);

    /**
     * 交易码：yxljcx
     * 交易描述：影像图像路径查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<YxljcxRespDto> yxljcx(YxljcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_YXLJCX.key.concat("|").concat(EsbEnum.TRADE_CODE_YXLJCX.value));
        return null;
    }
}
