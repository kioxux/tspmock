package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd.DjhhmdReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd.DjhhmdRespDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.lsfxbg.LsfxbgReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.lsfxbg.LsfxbgRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2FxyjxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类:查询客户风险预警等级与是否黑灰名单
 *
 * @author code-generator
 * @version 1.0
 */
@Component
public class Dscms2FxyjxtClientServiceImpl implements Dscms2FxyjxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2FxyjxtClientServiceImpl.class);

    /**
     * 交易码：djhhmd
     * 交易描述：查询客户风险预警等级与是否黑灰名单
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<DjhhmdRespDto> djhhmd(DjhhmdReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DJHHMD.key.concat("|").concat(EsbEnum.TRADE_CODE_DJHHMD.value));
        return null;
    }

    /**
     * 交易码：lsfxbg
     * 交易描述：查询客户项下历史风险预警报告
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<LsfxbgRespDto> lsfxbg(LsfxbgReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_LSFXBG.key.concat("|").concat(EsbEnum.TRADE_CODE_LSFXBG.value));
        return null;
    }

    /**
     * 交易码：hhmdkh
     * 交易描述：查询客户是否为黑灰名单客户
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<HhmdkhRespDto> hhmdkh(HhmdkhReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_HHMDKH.key.concat("|").concat(EsbEnum.TRADE_CODE_HHMDKH.value));
        return null;
    }

}
