package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.dto.client.http.image.callimage.CallImageReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2ImageClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * BSP封装调用影像系统的接口实现类
 */
@Component
public class Dscms2ImageClientServiceImpl implements Dscms2ImageClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2ImageClientServiceImpl.class);

    /**
     * 通过用户名和密码获取token
     *
     * @param reqDto
     * @return
     */
    @Deprecated
    @Override
    public ResultDto<ImageTokenRespDto> imagetoken(ImageTokenReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IMAGETOKEN.key.concat("|").concat(EsbEnum.TRADE_CODE_IMAGETOKEN.value));
        return null;
    }

    /**
     * 默认统一用户及密码获取token
     *
     * @return
     */
    @Deprecated
    @Override
    public ResultDto<ImageTokenRespDto> imagetoken() {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_COMMON_IMAGETOKEN.key.concat("|").concat(EsbEnum.TRADE_CODE_COMMON_IMAGETOKEN.value));
        return null;
    }

    @Override
    public ResultDto<String> imageBizUrl(CallImageReqDto callImageReqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_COMMON_IMAGEURL.key.concat("|").concat(EsbEnum.TRADE_CODE_COMMON_IMAGEURL.value));
        return null;
    }

    public ResultDto<Map<String, Integer>> imageImageDataSize(ImageDataReqDto imageDataReqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.key.concat("|").concat(EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.value));
        return null;
    }

    @Override
    public ResultDto<String> imageappr(ImageApprDto imageApprDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.key.concat("|").concat(EsbEnum.TRADE_CODE_IMAGEIMAGEDATASIZE.value));
        return null;
    }
}
