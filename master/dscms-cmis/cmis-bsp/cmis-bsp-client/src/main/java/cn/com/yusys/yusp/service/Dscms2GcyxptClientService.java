package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2GcyxptClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:过程营销平台
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2GcyxptClientServiceImpl.class)
public interface Dscms2GcyxptClientService {
    /**
     * 交易码：yxgc01
     * 交易描述：贷后任务推送
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gcyxpt/yxgc01")
    public ResultDto<Yxgc01RespDto> yxgc01(Yxgc01ReqDto reqDto);

}
