package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;

import cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.Yky001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.resp.Yky001RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2YkClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类: 接口实现类印控系统的接口
 *
 * @author xuwh
 * @version 1.0
 * @since 2021/8/18 19:57
 */
@Component
public class Dscms2YkClientServiceImpl implements Dscms2YkClientService {

    private static final Logger logger = LoggerFactory.getLogger(Dscms2YkClientServiceImpl.class);

    /**
     * 用印申请（yky001）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Yky001RespDto> yky001(Yky001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_YKY001.key.concat("|").concat(EsbEnum.TRADE_CODE_YKY001.value));
        return null;
    }

}
