package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gcyxpt.yxgc01.Yxgc01RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2GcyxptClientService;
import cn.com.yusys.yusp.service.Dscms2YpqzxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用过程营销平台的接口实现类
 *
 */
@Component
public class Dscms2GcyxptClientServiceImpl implements Dscms2GcyxptClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2YpqzxtClientServiceImpl.class);

    /**
     * 贷后任务推送（处理码yxgc01）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Yxgc01RespDto> yxgc01(Yxgc01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_YXGC01.key.concat("|").concat(EsbEnum.TRADE_CODE_YXGC01.value));
        return null;
    }

}
