package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.Lsnp01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.Lsnp01RespDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.req.Lsnp02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.resp.Lsnp02RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2LsnpClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Dscms2LsnpClientServiceImpl implements Dscms2LsnpClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2LsnpClientService.class);

    /**
     * 交易码：lsnp01
     * 交易描述：信贷业务零售评级
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Lsnp01RespDto> lsnp01(Lsnp01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_LSNP01.key.concat("|").concat(EsbEnum.TRADE_CODE_LSNP01.value));
        return null;
    }

    /**
     * 交易码：lsnp02
     * 交易描述：信用卡业务零售评级
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Lsnp02RespDto> lsnp02(Lsnp02ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_LSNP02.key.concat("|").concat(EsbEnum.TRADE_CODE_LSNP02.value));
        return null;
    }
}
