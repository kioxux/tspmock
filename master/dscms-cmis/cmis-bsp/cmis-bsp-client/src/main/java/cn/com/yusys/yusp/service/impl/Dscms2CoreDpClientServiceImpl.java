package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.req.Dp2021ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2021.resp.Dp2021RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.req.Dp2098ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2098.resp.Dp2098RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.req.Dp2099ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2099.resp.Dp2099RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2200.Dp2200ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2200.Dp2200RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2280.Dp2280ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2280.Dp2280RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2352.Dp2352ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2352.Dp2352RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.dp2667.Dp2667RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2CoreDpClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用核心系统的接口实现类(dp开头)
 */
@Component
public class Dscms2CoreDpClientServiceImpl implements Dscms2CoreDpClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2CoreDpClientServiceImpl.class);


    /**
     * 子账户序号查询（dp2280）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dp2280RespDto> dp2280(Dp2280ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DP2280.key.concat("|").concat(EsbEnum.TRADE_CODE_DP2280.value));
        return null;
    }

    /**
     * 交易码：dp2667
     * 交易描述：组合账户特殊账户查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dp2667RespDto> dp2667(Dp2667ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DP2667.key.concat("|").concat(EsbEnum.TRADE_CODE_DP2667.value));
        return null;
    }

    /**
     * 交易码：dp2200
     * 交易描述：保证金账户部提
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dp2200RespDto> dp2200(Dp2200ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DP2200.key.concat("|").concat(EsbEnum.TRADE_CODE_DP2200.value));
        return null;
    }

    /**
     * 交易码：dp2352
     * 交易描述：组合账户子账户开立
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dp2352RespDto> dp2352(Dp2352ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DP2352.key.concat("|").concat(EsbEnum.TRADE_CODE_DP2352.value));
        return null;
    }

    /**
     * 交易码：dp2099
     * 交易描述：保证金账户查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dp2099RespDto> dp2099(Dp2099ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DP2099.key.concat("|").concat(EsbEnum.TRADE_CODE_DP2099.value));
        return null;
    }

    /**
     * 交易码：dp2021
     * 交易描述：客户账号-子账号互查
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dp2021RespDto> dp2021(Dp2021ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DP2021.key.concat("|").concat(EsbEnum.TRADE_CODE_DP2021.value));
        return null;
    }


    /**
     * 交易码：dp2098
     * 交易描述：待清算账户查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dp2098RespDto> dp2098(Dp2098ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DP2098.key.concat("|").concat(EsbEnum.TRADE_CODE_DP2098.value));
        return null;
    }
}



