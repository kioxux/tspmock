package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.req.DjztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.resp.DjztcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.req.HvpsmrReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.resp.HvpsmrRespDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.req.HvpylsReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.HvpylsRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2HvpylsClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口类:二代支付系统
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/8/28 12:52
 * @since 2021/8/28 12:52
 */
@Component
public class Dscms2HvpylsClientServiceImpl implements Dscms2HvpylsClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2HvpylsClientServiceImpl.class);

    @Override
    public ResultDto<HvpylsRespDto> hvpyls(HvpylsReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_HVPYLS.key.concat("|").concat(EsbEnum.TRADE_CODE_HVPYLS.value));
        return null;
    }

    /**
     * 交易码：djztcx
     * 交易描述：贷记入账状态查询申请往帐
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<DjztcxRespDto> djztcx(DjztcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DJZTCX.key.concat("|").concat(EsbEnum.TRADE_CODE_DJZTCX.value));
        return null;
    }

    /**
     * 交易码：hvpsmr
     * 交易描述：大额往帐一体化
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<HvpsmrRespDto> hvpsmr(HvpsmrReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_HVPSMR.key.concat("|").concat(EsbEnum.TRADE_CODE_HVPSMR.value));
        return null;
    }
}
