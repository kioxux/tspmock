package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.ocr.req.Cbocr3ReqDto;
import cn.com.yusys.yusp.dto.client.http.ocr.resp.Cbocr3RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.idcheck.IdCheckRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.qyssxx.QyssxxRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.slno01.Slno01RespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.req.SxbzxrReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.resp.SxbzxrRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.zsnew.ZsnewRespDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.req.Ztod01ReqDto;
import cn.com.yusys.yusp.dto.client.http.outerdata.ztod01.resp.Ztod01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2OuterdataClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Dscms2OuterdataClientServiceImpl implements Dscms2OuterdataClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2OuterdataClientServiceImpl.class);

    /**
     * 身份核查
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<IdCheckRespDto> idCheck(IdCheckReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IDCHECK.key.concat("|").concat(EsbEnum.TRADE_CODE_IDCHECK.value));
        return null;
    }

    /**
     * 企业工商信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<ZsnewRespDto> zsnew(ZsnewReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZSNEW.key.concat("|").concat(EsbEnum.TRADE_CODE_ZSNEW.value));
        return null;
    }

    /**
     * 查询涉诉信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<QyssxxRespDto> qyssxx(QyssxxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_QYSSXX.key.concat("|").concat(EsbEnum.TRADE_CODE_QYSSXX.value));
        return null;
    }

    /**
     * 交易码：ztod01
     * 交易描述：融E开-工商数据查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Ztod01RespDto> ztod01(Ztod01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZTOD01.key.concat("|").concat(EsbEnum.TRADE_CODE_ZTOD01.value));
        return null;
    }

    /**
     * 交易码：cbocr3
     * 交易描述：报表核对统计总览列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cbocr3RespDto> cbocr3(Cbocr3ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CBOCR3.key.concat("|").concat(EsbEnum.TRADE_CODE_CBOCR3.value));
        return null;
    }

    /**
     * 交易码：slno01
     * 交易描述：双录流水号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Slno01RespDto> slno01(Slno01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_SLNO01.key.concat("|").concat(EsbEnum.TRADE_CODE_SLNO01.value));
        return null;
    }

    /**
     * 交易码：sxbzxr
     * 交易描述：失信被执行人查询接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<SxbzxrRespDto> sxbzxr(SxbzxrReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_SXBZXR.key.concat("|").concat(EsbEnum.TRADE_CODE_SXBZXR.value));
        return null;
    }

}
