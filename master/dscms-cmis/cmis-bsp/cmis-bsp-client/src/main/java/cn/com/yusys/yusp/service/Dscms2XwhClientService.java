package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.req.Xwh001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.resp.Xwh001RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.req.Xwh003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.resp.Xwh003RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.req.Yx0003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.resp.Yx0003RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2XwhClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:小微公众号
 *
 * @author lihh
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2XwhClientServiceImpl.class)
public interface Dscms2XwhClientService {
    /**
     * 交易码：xwh001
     * 交易描述：借据台账信息接收
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwh/xwh001")
    public ResultDto<Xwh001RespDto> xwh001(Xwh001ReqDto reqDto);

    /**
     * 交易码：xwh003
     * 交易描述：核销锁定接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwh/xwh003")
    public ResultDto<Xwh003RespDto> xwh003(Xwh003ReqDto reqDto);

    /**
     * 交易码：yx0003
     * 交易描述：市民贷优惠券核销锁定
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2xwh/yx0003")
    public ResultDto<Yx0003RespDto> yx0003(Yx0003ReqDto reqDto);

}
