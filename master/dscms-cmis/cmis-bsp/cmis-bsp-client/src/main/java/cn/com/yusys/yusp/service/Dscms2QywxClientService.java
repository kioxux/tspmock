package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxReqDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2DxptClientServiceImpl;
import cn.com.yusys.yusp.service.impl.Dscms2QywxClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用微信平台的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2QywxClientServiceImpl.class)
public interface Dscms2QywxClientService {

    /**
     * 推送企业微信
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2wxpt/qywx")
    public ResultDto<QywxRespDto> qywx(QywxReqDto reqDto);

}
