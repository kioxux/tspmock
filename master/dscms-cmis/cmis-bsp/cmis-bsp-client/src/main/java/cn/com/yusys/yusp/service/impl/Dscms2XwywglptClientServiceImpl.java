package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.req.Dhxd01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.resp.Dhxd01RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.req.Dhxd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd02.resp.Dhxd02RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd003.Wxd003RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd004.Wxd004RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007.Wxd007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007.Wxd007RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009.Wxd009RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.req.Xwd008ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd008.resp.Xwd008RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.req.Xwd013ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwywglpt.xwd013.resp.Xwd013RespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.req.Xwd009ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd009.resp.Xwd009RespDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.req.Xwd010ReqDto;
import cn.com.yusys.yusp.dto.client.http.xwywglpt.xwd010.resp.Xwd010RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2XwywglptClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类:信贷系统请求V平台二级准入接口
 *
 * @author code-generator
 * @version 1.0
 */
@Component
public class Dscms2XwywglptClientServiceImpl implements Dscms2XwywglptClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2XwywglptClientServiceImpl.class);

    /**
     * 交易码：wxd003
     * 交易描述：信贷系统请求V平台二级准入接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Wxd003RespDto> wxd003(Wxd003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_WXD003.key.concat("|").concat(EsbEnum.TRADE_CODE_WXD003.value));
        return null;
    }

    /**
     * 交易码：wxd004
     * 交易描述：信贷系统获取征信报送监管信息接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Wxd004RespDto> wxd004(Wxd004ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_WXD004.key.concat("|").concat(EsbEnum.TRADE_CODE_WXD004.value));
        return null;
    }

    /**
     * 交易码：wxd007
     * 交易描述：请求小V平台综合决策管理列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Wxd007RespDto> wxd007(Wxd007ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_WXD007.key.concat("|").concat(EsbEnum.TRADE_CODE_WXD007.value));
        return null;
    }

    /**
     * 交易码：wxd009
     * 交易描述：信贷系统请求小V平台推送合同信息接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Wxd009RespDto> wxd009(Wxd009ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_WXD009.key.concat("|").concat(EsbEnum.TRADE_CODE_WXD009.value));
        return null;
    }

    /**
     * 交易码：dhxd01
     * 交易描述：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dhxd01RespDto> dhxd01(Dhxd01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DHXD01.key.concat("|").concat(EsbEnum.TRADE_CODE_DHXD01.value));
        return null;
    }

    /**
     * 交易码：dhxd02
     * 交易描述：信贷系统请求小V平台贷后预警回传定期检查任务结果
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Dhxd02RespDto> dhxd02(Dhxd02ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DHXD02.key.concat("|").concat(EsbEnum.TRADE_CODE_DHXD02.value));
        return null;
    }


    /**
     * 交易码：xwd008
     * 交易描述：新信贷同步用户账号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwd008RespDto> xwd008(Xwd008ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWD008.key.concat("|").concat(EsbEnum.TRADE_CODE_XWD008.value));
        return null;
    }

    /**
     * 交易码：xwd010
     * 交易描述：利率撤销接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwd010RespDto> xwd010(Xwd010ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWD010.key.concat("|").concat(EsbEnum.TRADE_CODE_XWD010.value));
        return null;
    }

    /**
     * 交易码：xwd009
     * 交易描述：利率申请接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwd009RespDto> xwd009(Xwd009ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWD009.key.concat("|").concat(EsbEnum.TRADE_CODE_XWD009.value));
        return null;
    }


    /**
     * 交易码：xwd013
     * 交易描述：新信贷获取调查信息接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwd013RespDto> xwd013(Xwd013ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWD013.key.concat("|").concat(EsbEnum.TRADE_CODE_XWD013.value));
        return null;
    }
}
