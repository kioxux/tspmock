package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj07.Xdgj07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj07.Xdgj07RespDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj08.Xdgj08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj08.Xdgj08RespDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj09.Xdgj09RespDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj10.Xdgj10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj10.Xdgj10RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2GjjsClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:国际结算系统
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2GjjsClientServiceImpl.class)
public interface Dscms2GjjsClientService {
    /**
     * 交易码：xdgj07
     * 交易描述：信贷发送台账信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gjjs/xdgj07")
    public ResultDto<Xdgj07RespDto> xdgj07(Xdgj07ReqDto reqDto);

    /**
     * 交易码：xdgj08
     * 交易描述：信贷还款信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gjjs/xdgj08")
    public ResultDto<Xdgj08RespDto> xdgj08(Xdgj08ReqDto reqDto);

    /**
     * 交易码：xdgj09
     * 交易描述：信贷查询牌价信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gjjs/xdgj09")
    public ResultDto<Xdgj09RespDto> xdgj09(Xdgj09ReqDto reqDto);

    /**
     * 交易码：xdgj10
     * 交易描述：信贷获取国结信用证信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2gjjs/xdgj10")
    public ResultDto<Xdgj10RespDto> xdgj10(Xdgj10ReqDto reqDto);
}
