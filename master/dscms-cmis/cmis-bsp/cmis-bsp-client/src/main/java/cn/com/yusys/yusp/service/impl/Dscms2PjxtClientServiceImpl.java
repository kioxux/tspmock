package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.fkpj35.req.Fkpj35ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.req.Xdpj004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.resp.Xdpj004RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.Xdpj03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.resp.Xdpj03RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.req.Xdpj12ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.resp.Xdpj12RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.req.Xdpj15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.resp.Xdpj15RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.req.Xdpj21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp.Xdpj21RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.req.Xdpj22ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.Xdpj22RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req.Xdpj23ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.Xdpj23RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.req.Xdpj24ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp.Xdpj24RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2PjxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用票据系统的接口实现类
 */
@Component
public class Dscms2PjxtClientServiceImpl implements Dscms2PjxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2PjxtClientServiceImpl.class);

    /**
     * 交易码：xdpj03
     * 交易描述：票据承兑签发审批请求
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj03RespDto> xdpj03(Xdpj03ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ03.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ03.value));
        return null;
    }

    /**
     * 交易码：xdpj14
     * 交易描述：信贷签约通知
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj14RespDto> xdpj14(Xdpj14ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ14.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ14.value));
        return null;
    }

    /**
     * 交易码：xdpj22
     * 交易描述：根据批次号查询票号和票面金额
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj22RespDto> xdpj22(Xdpj22ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ22.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ22.value));
        return null;
    }

    /**
     * 交易码：xdpj15
     * 交易描述：发票补录信息推送（信贷调票据）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj15RespDto> xdpj15(Xdpj15ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ15.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ15.value));
        return null;
    }

    /**
     * 交易码：xdpj12
     * 交易描述：票据池推送保证金账号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj12RespDto> xdpj12(Xdpj12ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ12.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ12.value));
        return null;
    }

    /**
     * 交易码：xdpj21
     * 交易描述：查询银票出账保证金账号信息（新信贷调票据）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj21RespDto> xdpj21(Xdpj21ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ21.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ21.value));
        return null;
    }

    /**
     * 交易码：xdpj23
     * 交易描述：查询批次出账票据信息（新信贷调票据）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj23RespDto> xdpj23(Xdpj23ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ23.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ23.value));
        return null;
    }

    /**
     * 交易码：xdpj004
     * 交易描述：承兑签发审批结果综合服务接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj004RespDto> xdpj004(Xdpj004ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ004.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ004.value));
        return null;
    }

    /**
     * 交易码：xdpj24
     * 交易描述：从票据系统获取当日到期票的日出备款金额
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xdpj24RespDto> xdpj24(Xdpj24ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDPJ24.key.concat("|").concat(EsbEnum.TRADE_CODE_XDPJ24.value));
        return null;
    }

    @Override
    public ResultDto<Fkpj35ReqDto> fkpj35(Fkpj35ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FKPJ35.key.concat("|").concat(EsbEnum.TRADE_CODE_FKPJ35.value));
        return null;
    }
}
