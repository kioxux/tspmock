package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bucont.BucontReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.resp.BusconRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.BusinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.resp.BusinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.req.CreditypisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.resp.CreditypisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.req.GuarstReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.resp.GuarstRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.manage.req.ManageReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.manage.resp.ManageRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.req.SpsyypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.resp.SpsyypRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypbdccx.req.XdypbdccxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypbdccx.resp.XdypbdccxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypcdpjcx.req.XdypcdpjcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypcdpjcx.resp.XdypcdpjcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypdywcx.req.XdypdywcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypdywcx.resp.XdypdywcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypjbxxcx.req.XdypjbxxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypjbxxcx.resp.XdypjbxxcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.req.XdypxxtbyrReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.resp.XdypxxtbyrRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypzywcx.req.XdypzywcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypzywcx.resp.XdypzywcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.yhxxtb.req.YhxxtbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.yhxxtb.resp.YhxxtbRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg.YpztbgReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg.YpztbgRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2YpxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用押品系统的接口实现类
 */
@Component
public class Dscms2YpxtClientServiceImpl implements Dscms2YpxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2YpxtClientServiceImpl.class);

    @Override
    public ResultDto<CertisRespDto> certis(CertisReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CERTIS.key.concat("|").concat(EsbEnum.TRADE_CODE_CERTIS.value));
        return null;
    }

    @Override
    public ResultDto<CetinfRespDto> cetinf(CetinfReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CETINF.key.concat("|").concat(EsbEnum.TRADE_CODE_CETINF.value));
        return null;
    }


    /**
     * 押品状态变更（ypztbg）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<YpztbgRespDto> ypztbg(YpztbgReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_YPZTBG.key.concat("|").concat(EsbEnum.TRADE_CODE_YPZTBG.value));
        return null;
    }

    /**
     * 不动产权证查询（bdcqcx）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<BdcqcxRespDto> bdcqcx(BdcqcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_BDCQCX.key.concat("|").concat(EsbEnum.TRADE_CODE_BDCQCX.value));
        return null;
    }


    /**
     * 业务与担保合同关系接口（处理码bucont）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<BucontReqDto> bucont(BucontReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_BUCONT.key.concat("|").concat(EsbEnum.TRADE_CODE_BUCONT.value));
        return null;
    }

    /**
     * 信贷担保合同信息同步接口（处理码coninf）
     *
     * @param reqDto
p    * @return
     */
    @Override
    public ResultDto<ConinfRespDto> coninf(ConinfReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CONINF.key.concat("|").concat(EsbEnum.TRADE_CODE_CONINF.value));
        return null;
    }

    /**
     * 押品与担保合同关系同步接口（处理码contra）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<ContraRespDto> contra(ContraReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CONTRA.key.concat("|").concat(EsbEnum.TRADE_CODE_CONTRA.value));
        return null;
    }

    /**
     * 交易码：evalyp
     * 交易描述：押品我行确认价值同步接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<EvalypRespDto> evalyp(EvalypReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_EVALYP.key.concat("|").concat(EsbEnum.TRADE_CODE_EVALYP.value));
        return null;
    }

    /**
     * 交易码：xdypgyrcx
     * 交易描述：查询共有人信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdypgyrcxRespDto> xdypgyrcx(XdypgyrcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDYPGYRCX.key.concat("|").concat(EsbEnum.TRADE_CODE_XDYPGYRCX.value));
        return null;
    }

    /**
     * 交易码：xdypjbxxcx
     * 交易描述：查询共有人信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdypjbxxcxRespDto> xdypjbxxcx(XdypjbxxcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDYPJBXXCX.key.concat("|").concat(EsbEnum.TRADE_CODE_XDYPJBXXCX.value));
        return null;
    }

    /**
     * 交易码：xdypbdccx
     * 交易描述：查询不动产信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdypbdccxRespDto> xdypbdccx(XdypbdccxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDYPBDCCX.key.concat("|").concat(EsbEnum.TRADE_CODE_XDYPBDCCX.value));
        return null;
    }

    /**
     * 交易码：xdypzywcx
     * 交易描述：查询质押物信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdypzywcxRespDto> xdypzywcx(XdypzywcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDYPZYWCX.key.concat("|").concat(EsbEnum.TRADE_CODE_XDYPZYWCX.value));
        return null;
    }

    /**
     * 交易码：xdypdywcx
     * 交易描述：查询抵押物信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdypdywcxRespDto> xdypdywcx(XdypdywcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDYPDYWCX.key.concat("|").concat(EsbEnum.TRADE_CODE_XDYPDYWCX.value));
        return null;
    }

    /**
     * 交易码：xdypcdpjcx
     * 交易描述：查询存单票据信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdypcdpjcxRespDto> xdypcdpjcx(XdypcdpjcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDYPCDPJCX.key.concat("|").concat(EsbEnum.TRADE_CODE_XDYPCDPJCX.value));
        return null;
    }

    /**
     * 交易码：billyp
     * 交易描述：票据信息同步接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<BillypRespDto> billyp(BillypReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_BILLYP.key.concat("|").concat(EsbEnum.TRADE_CODE_BILLYP.value));
        return null;
    }

    /**
     * 交易码：creditypis
     * 交易描述：信用证信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<CreditypisRespDto> creditypis(CreditypisReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CREDITYPIS.key.concat("|").concat(EsbEnum.TRADE_CODE_CREDITYPIS.value));
        return null;
    }

    /**
     * 交易码：xdypxxtbyr
     * 交易描述：押品信息同步及引入
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdypxxtbyrRespDto> xdypxxtbyr(XdypxxtbyrReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDYPXXTBYR.key.concat("|").concat(EsbEnum.TRADE_CODE_XDYPXXTBYR.value));
        return null;
    }


    /**
     * 交易码：cxypbh
     * 交易描述：通过票据号码查询押品编号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<CxypbhRespDto> cxypbh(CxypbhReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CXYPBH.key.concat("|").concat(EsbEnum.TRADE_CODE_CXYPBH.value));
        return null;
    }

    /**
     * 交易码：businf
     * 交易描述：信贷业务合同信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<BusinfRespDto> businf(BusinfReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_BUSINF.key.concat("|").concat(EsbEnum.TRADE_CODE_BUSINF.value));
        return null;
    }

    /**
     * 交易码：manage
     * 交易描述：管护权移交信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<ManageRespDto> manage(ManageReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_MANAGE.key.concat("|").concat(EsbEnum.TRADE_CODE_MANAGE.value));
        return null;
    }

    /**
     * 交易码：buscon
     * 交易描述：信贷业务与押品关联关系信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<BusconRespDto> buscon(BusconReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_BUSCON.key.concat("|").concat(EsbEnum.TRADE_CODE_BUSCON.value));
        return null;
    }

    /**
     * 交易码：yhxxtb
     * 交易描述：用户信息同步接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<YhxxtbRespDto> yhxxtb(YhxxtbReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_YHXXTB.key.concat("|").concat(EsbEnum.TRADE_CODE_YHXXTB.value));
        return null;
    }

    /**
     * 交易码：spsyyp
     * 交易描述：信贷审批结果同步接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<SpsyypRespDto> spsyyp(SpsyypReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_SPSYYP.key.concat("|").concat(EsbEnum.TRADE_CODE_SPSYYP.value));
        return null;
    }

    /**
     * 交易码：dealsy
     * 交易描述：押品处置信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<DealsyRespDto> dealsy(DealsyReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DEALSY.key.concat("|").concat(EsbEnum.TRADE_CODE_DEALSY.value));
        return null;
    }

    /**
     * 交易码：lmtinf
     * 交易描述：信贷授信协议信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<LmtinfRespDto> lmtinf(LmtinfReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_LMTINF.key.concat("|").concat(EsbEnum.TRADE_CODE_LMTINF.value));
        return null;
    }

    /**
     * 交易码：guarst
     * 交易描述：押品状态变更推送
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<GuarstRespDto> guarst(GuarstReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_GUARST.key.concat("|").concat(EsbEnum.TRADE_CODE_GUARST.value));
        return null;
    }
}
