package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.req.Wxp001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.resp.Wxp001RespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp002.req.Wxp002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp002.resp.Wxp002RespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp003.req.Wxp003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp003.resp.Wxp003RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2WxClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用Ocr系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2WxClientServiceImpl.class)
public interface Dscms2WxClientService {
    /**
     * 交易码：wxp001
     * 交易描述：信贷将审批结果推送给移动端
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2wx/wxp001")
    public ResultDto<Wxp001RespDto> wxp001(Wxp001ReqDto reqDto);


    /**
     * 交易码：wxp002
     * 交易描述：信贷将授信额度推送给移动端
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2wx/wxp002")
    public ResultDto<Wxp002RespDto> wxp002(Wxp002ReqDto reqDto);

    /**
     * 交易码：wxp003
     * 交易描述：信贷将放款标识推送给移动端
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2wx/wxp003")
    public ResultDto<Wxp003RespDto> wxp003(Wxp003ReqDto reqDto);

}
