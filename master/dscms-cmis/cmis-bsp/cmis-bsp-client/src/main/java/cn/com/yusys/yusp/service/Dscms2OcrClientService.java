package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.req.Cbocr1ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr1.resp.Cbocr1RespDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.req.Cbocr2ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ocr.cbocr2.resp.Cbocr2RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2OcrClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用Ocr系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2OcrClientServiceImpl.class)
public interface Dscms2OcrClientService {
    /**
     * 交易码：cbocr1
     * 交易描述：新增批次报表数据
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ocr/cbocr1")
    ResultDto<Cbocr1RespDto> cbocr1(Cbocr1ReqDto reqDto);

    /**
     * 交易码：cbocr2
     * 交易描述：识别任务的模板匹配结果列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ocr/cbocr2")
    ResultDto<Cbocr2RespDto> cbocr2(Cbocr2ReqDto reqDto);

}
