package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.fkpj35.req.Fkpj35ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.req.Xdpj004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.resp.Xdpj004RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req.Xdpj03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.resp.Xdpj03RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.req.Xdpj12ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj12.resp.Xdpj12RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14.Xdpj14RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.req.Xdpj15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.resp.Xdpj15RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.req.Xdpj21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp.Xdpj21RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.req.Xdpj22ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.Xdpj22RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req.Xdpj23ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp.Xdpj23RespDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.req.Xdpj24ReqDto;
import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj24.resp.Xdpj24RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2PjxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用票据系统系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2PjxtClientServiceImpl.class)
public interface Dscms2PjxtClientService {

    /**
     * 交易码：xdpj03
     * 交易描述：票据承兑签发审批请求
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj03")
    public ResultDto<Xdpj03RespDto> xdpj03(Xdpj03ReqDto reqDto);

    /**
     * 交易码：xdpj14
     * 交易描述：信贷签约通知
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj14")
    public ResultDto<Xdpj14RespDto> xdpj14(Xdpj14ReqDto reqDto);

    /**
     * 交易码：xdpj22
     * 交易描述：根据批次号查询票号和票面金额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj22")
    public ResultDto<Xdpj22RespDto> xdpj22(Xdpj22ReqDto reqDto);

    /**
     * 交易码：xdpj15
     * 交易描述：发票补录信息推送（信贷调票据）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj15")
    public ResultDto<Xdpj15RespDto> xdpj15(Xdpj15ReqDto reqDto);

    /**
     * 交易码：xdpj12
     * 交易描述：票据池推送保证金账号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj12")
    public ResultDto<Xdpj12RespDto> xdpj12(Xdpj12ReqDto reqDto);

    /**
     * 交易码：xdpj21
     * 交易描述：查询银票出账保证金账号信息（新信贷调票据）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj21")
    public ResultDto<Xdpj21RespDto> xdpj21(Xdpj21ReqDto reqDto);


    /**
     * 交易码：xdpj23
     * 交易描述：查询批次出账票据信息（新信贷调票据）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj23")
    public ResultDto<Xdpj23RespDto> xdpj23(Xdpj23ReqDto reqDto);

    /**
     * 交易码：xdpj004
     * 交易描述：承兑签发审批结果综合服务接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj004")
    public ResultDto<Xdpj004RespDto> xdpj004(Xdpj004ReqDto reqDto);


    /**
     * 交易码：xdpj24
     * 交易描述：从票据系统获取当日到期票的日出备款金额
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/xdpj24")
    public ResultDto<Xdpj24RespDto> xdpj24(Xdpj24ReqDto reqDto);

    /**
     * 交易码：fkpj35
     * 交易描述：票据承兑签发审批请求
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2pjxt/fkpj35")
    public ResultDto<Fkpj35ReqDto> fkpj35(Fkpj35ReqDto reqDto);
}
