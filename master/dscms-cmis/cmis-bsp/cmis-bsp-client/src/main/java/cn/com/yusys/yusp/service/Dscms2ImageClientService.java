package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.image.apprimage.ImageApprDto;
import cn.com.yusys.yusp.dto.client.http.image.callimage.CallImageReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imageDataSize.ImageDataReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenReqDto;
import cn.com.yusys.yusp.dto.client.http.image.imagetoken.ImageTokenRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2ImageClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

/**
 * BSP封装调用影像系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2ImageClientServiceImpl.class)
public interface Dscms2ImageClientService {

    /**
     * 通过用户名和密码获取token
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallS00102Example.java.txt
     */
    @Deprecated
    @PostMapping("/dscms2image/imagetoken")
    public ResultDto<ImageTokenRespDto> imagetoken(ImageTokenReqDto reqDto);


    /**
     * 默认统一用户及密码获取token
     *
     * @return
     * @code 示例代码：CallS00102Example.java.txt
     */
    @Deprecated
    @PostMapping("/dscms2image/common/imagetoken")
    public ResultDto<ImageTokenRespDto> imagetoken();


    /**
     * 默认统一用户及密码获取影像地址
     *
     * @return
     * @code 示例代码：CallS00102Example.java.txt
     */
    @PostMapping("/dscms2image/imageurl")
    public ResultDto<String> imageBizUrl(CallImageReqDto callImageReqDto);

    /**
     * 图像数量查询接口
     *
     * @param imageDataReqDto
     * @return
     */

    @PostMapping("/dscms2image/imageImageDataSize")
    public ResultDto<Map<String, Integer>> imageImageDataSize(ImageDataReqDto imageDataReqDto);

    /**
     * @param imageApprDto
     * @return cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto<org.springframework.http.ResponseEntity<java.lang.String>>
     * @author 王玉坤
     * @date 2021/9/29 11:32
     * @version 1.0.0
     * @desc 
     * @修改历史: 修改时间    修改人员    修改原因
     */
    @PostMapping("/dscms2image/common/imageappr")
    public ResultDto<String> imageappr(ImageApprDto imageApprDto);
}
