package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd.DjhhmdReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd.DjhhmdRespDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.hhmdkh.HhmdkhRespDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.lsfxbg.LsfxbgReqDto;
import cn.com.yusys.yusp.dto.client.esb.fxyjxt.lsfxbg.LsfxbgRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2FxyjxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:风险预警系统
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2FxyjxtClientServiceImpl.class)
public interface Dscms2FxyjxtClientService {
    /**
     * 交易码：djhhmd
     * 交易描述：查询客户风险预警等级与是否黑灰名单
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2fxyjxt/djhhmd")
    public ResultDto<DjhhmdRespDto> djhhmd(DjhhmdReqDto reqDto);

    /**
     * 交易码：lsfxbg
     * 交易描述：查询客户项下历史风险预警报告
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2fxyjxt/lsfxbg")
    public ResultDto<LsfxbgRespDto> lsfxbg(LsfxbgReqDto reqDto);

    /**
     * 交易码：hhmdkh
     * 交易描述：查询客户是否为黑灰名单客户
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2fxyjxt/hhmdkh")
    public ResultDto<HhmdkhRespDto> hhmdkh(HhmdkhReqDto reqDto);
}
