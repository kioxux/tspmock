package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm001.Cwm001RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm002.Cwm002RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm003.Cwm003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm004.Cwm004RespDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypqzxt.cwm007.Cwm007RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2YpqzxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用押品权证系统的接口实现类
 *
 */
@Component
public class Dscms2YpqzxtClientServiceImpl implements Dscms2YpqzxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2YpqzxtClientServiceImpl.class);

    /**
     * 本异地借阅出库接口（处理码cwm001）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cwm001RespDto> cwm001(Cwm001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CWM001.key.concat("|").concat(EsbEnum.TRADE_CODE_CWM001.value));
        return null;
    }

    /**
     * 本异地借阅归还接口（处理码cwm002）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cwm002RespDto> cwm002(Cwm002ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CWM002.key.concat("|").concat(EsbEnum.TRADE_CODE_CWM002.value));
        return null;
    }

    /**
     * 押品权证入库接口（处理码Cwm003）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cwm003RespDto> cwm003(Cwm003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CWM003.key.concat("|").concat(EsbEnum.TRADE_CODE_CWM003.value));
        return null;
    }

    /**
     * 押品权证出库接口（处理码Cwm004）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cwm004RespDto> cwm004(Cwm004ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CWM004.key.concat("|").concat(EsbEnum.TRADE_CODE_CWM004.value));
        return null;
    }

    /**
     * 押品状态查询接口（处理码Cwm007）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Cwm007RespDto> cwm007(Cwm007ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CWM007.key.concat("|").concat(EsbEnum.TRADE_CODE_CWM007.value));
        return null;
    }
}
