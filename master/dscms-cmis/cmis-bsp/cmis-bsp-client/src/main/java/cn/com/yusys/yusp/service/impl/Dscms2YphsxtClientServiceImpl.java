package cn.com.yusys.yusp.service.impl;


import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.req.CredisReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.credis.resp.CredisRespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.req.DepoisReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.depois.resp.DepoisRespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.req.GyypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.resp.GyypbhRespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.req.Xddb01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb01.resp.Xddb01RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb02.req.Xddb02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb02.resp.Xddb02RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb03.req.Xddb03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb03.resp.Xddb03RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb04.req.Xddb04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb04.resp.Xddb04RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb05.req.Xddb05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb05.resp.Xddb05RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.req.Xddb06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xddb06.resp.Xddb06RespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.req.XdjzzyReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.resp.XdjzzyRespDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.req.YpztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp.YpztcxRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2YphsxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用押品缓释系统的接口实现类
 */
@Component
public class Dscms2YphsxtClientServiceImpl implements Dscms2YphsxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2YphsxtClientServiceImpl.class);


    /**
     * 交易码：xddb01
     * 交易描述：查询不动产信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb01RespDto> xddb01(Xddb01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDDB01.key.concat("|").concat(EsbEnum.TRADE_CODE_XDDB01.value));
        return null;
    }

    /**
     * 交易码：xddb02
     * 交易描述：查询抵押物信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb02RespDto> xddb02(Xddb02ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDDB02.key.concat("|").concat(EsbEnum.TRADE_CODE_XDDB02.value));
        return null;
    }

    /**
     * 交易码：xddb03
     * 交易描述：查询存单票据信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb03RespDto> xddb03(Xddb03ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDDB03.key.concat("|").concat(EsbEnum.TRADE_CODE_XDDB03.value));
        return null;
    }

    /**
     * 交易码：xddb04
     * 交易描述：查询质押物信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb04RespDto> xddb04(Xddb04ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDDB04.key.concat("|").concat(EsbEnum.TRADE_CODE_XDDB04.value));
        return null;
    }

    /**
     * 交易码：xddb05
     * 交易描述：查询基本信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb05RespDto> xddb05(Xddb05ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDDB05.key.concat("|").concat(EsbEnum.TRADE_CODE_XDDB05.value));
        return null;
    }

    /**
     * 交易码：xddb06
     * 交易描述：查询共有人信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xddb06RespDto> xddb06(Xddb06ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDDB06.key.concat("|").concat(EsbEnum.TRADE_CODE_XDDB06.value));
        return null;
    }


    /**
     * 交易码：credis
     * 交易描述：信用证信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<CredisRespDto> credis(CredisReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CREDIS.key.concat("|").concat(EsbEnum.TRADE_CODE_CREDIS.value));
        return null;
    }

    /**
     * 交易码：depois
     * 交易描述：存单信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<DepoisRespDto> depois(DepoisReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_DEPOIS.key.concat("|").concat(EsbEnum.TRADE_CODE_DEPOIS.value));
        return null;
    }

    /**
     * 交易码：xdjzzy
     * 交易描述：押品信息同步及引入
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XdjzzyRespDto> xdjzzy(XdjzzyReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XDJZZY.key.concat("|").concat(EsbEnum.TRADE_CODE_XDJZZY.value));
        return null;
    }

    /**
     * 交易码：gyypbh
     * 交易描述：通过共有人编号查询押品编号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<GyypbhRespDto> gyypbh(GyypbhReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_GYYPBH.key.concat("|").concat(EsbEnum.TRADE_CODE_GYYPBH.value));
        return null;
    }


    /**
     * 交易码：ypztcx
     * 交易描述：信贷押品状态查询
     *
     * @param reqDto
     * @return
     * @Description: 接口功能的简单描述：
     * 1、国结调用此接口，传合同编号（mocenu），押品系统查询查封信息后，返回对应信息 --0000 查到未查封；0001 未查到信息；9999 存在查封。
     * <p>
     * 2、风控调用此接口，押品系统去不动产平台查了之后返回。
     * 根据区县代码和不动产权证书号、不动产登记证明号，到不动产登记中心去查询具体的不动产信息。
     * 区县代码如果是国际，那根据押品类型去押品权证系统dblink查询具体的不动产信息。
     * 区县代码如果不是国际，那发接口到全国不动产登记中心去查，（高新区、姑苏区、相城区）的区县代码需要做变换。
     * <p>
     * 原信贷代码ID: Bare01Action.java
     */
    @Override
    public ResultDto<YpztcxRespDto> ypztcx(YpztcxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_YPZTCX.key.concat("|").concat(EsbEnum.TRADE_CODE_YPZTCX.value));
        return null;
    }
}
