package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.req.DjztcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.djztcx.resp.DjztcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.req.HvpsmrReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpsmr.resp.HvpsmrRespDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.req.HvpylsReqDto;
import cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.HvpylsRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2HvpylsClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用二代支付系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2HvpylsClientServiceImpl.class)
public interface Dscms2HvpylsClientService {
    /**
     * 交易码：hvpyls
     * 交易描述：大额往账列表查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2edzfxt/hvpyls")
    public ResultDto<HvpylsRespDto> hvpyls(HvpylsReqDto reqDto);


    /**
     * 交易码：djztcx
     * 交易描述：贷记入账状态查询申请往帐
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2edzfxt/djztcx")
    public ResultDto<DjztcxRespDto> djztcx(DjztcxReqDto reqDto);

    /**
     * 交易码：hvpsmr
     * 交易描述：大额往帐一体化
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2edzfxt/hvpsmr")
    public ResultDto<HvpsmrRespDto> hvpsmr(HvpsmrReqDto reqDto);
}
