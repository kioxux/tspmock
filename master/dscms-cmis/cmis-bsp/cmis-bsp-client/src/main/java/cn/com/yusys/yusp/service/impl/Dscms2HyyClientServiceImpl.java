package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.http.hyy.bare01.req.Bare01ReqDto;
import cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp.Bare01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2HyyClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类:BSP封装调用慧押押系统的接口
 *
 * @author chenyong
 * @version 1.0
 */
@Component
public class Dscms2HyyClientServiceImpl implements Dscms2HyyClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2HyyClientServiceImpl.class);

    /**
     * 交易码：bare01
     * 交易描述：押品状态查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Bare01RespDto> bare01(Bare01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_BARE01.key.concat("|").concat(EsbEnum.TRADE_CODE_BARE01.value));
        return null;
    }

}
