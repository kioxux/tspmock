package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04.Znsp04RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.req.Znsp05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp05.resp.Znsp05RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.req.Znsp06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.resp.Znsp06RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.req.Znsp07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.resp.Znsp07RespDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.req.Znsp08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.resp.Znsp08RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2ZnwdspxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类:智能微贷审批系统
 *
 * @author code-generator
 * @version 1.0
 */
@Component
public class Dscms2ZnwdspxtClientServiceImpl implements Dscms2ZnwdspxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2ZnwdspxtClientServiceImpl.class);

    /**
     * 交易码：znsp04
     * 交易描述：自动审批调查报告
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Znsp04RespDto> znsp04(Znsp04ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZNSP04.key.concat("|").concat(EsbEnum.TRADE_CODE_ZNSP04.value));
        return null;
    }

    /**
     * 交易码：znsp08
     * 交易描述：客户调查撤销接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Znsp08RespDto> znsp08(Znsp08ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZNSP08.key.concat("|").concat(EsbEnum.TRADE_CODE_ZNSP08.value));
        return null;
    }

    /**
     * 交易码：znsp05
     * 交易描述：风险拦截截接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Znsp05RespDto> znsp05(Znsp05ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZNSP05.key.concat("|").concat(EsbEnum.TRADE_CODE_ZNSP05.value));
        return null;
    }

    /**
     * 交易码：znsp06
     * 交易描述：被拒绝的线上产品推送接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Znsp06RespDto> znsp06(Znsp06ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZNSP06.key.concat("|").concat(EsbEnum.TRADE_CODE_ZNSP06.value));
        return null;
    }

    /**
     * 交易码：znsp07
     * 交易描述：数据修改接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Znsp07RespDto> znsp07(Znsp07ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_ZNSP07.key.concat("|").concat(EsbEnum.TRADE_CODE_ZNSP07.value));
        return null;
    }
}
