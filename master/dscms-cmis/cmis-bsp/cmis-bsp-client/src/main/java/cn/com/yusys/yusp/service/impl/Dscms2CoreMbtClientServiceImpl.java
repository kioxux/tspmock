package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.req.Mbt951ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt951.resp.Mbt951RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.req.Mbt952ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp.Mbt952RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt999.Mbt999ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.mbt999.Mbt999RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2CoreMbtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用核心系统的接口(mbt开头)
 */
@Component
public class Dscms2CoreMbtClientServiceImpl implements Dscms2CoreMbtClientService {

    private static final Logger logger = LoggerFactory.getLogger(Dscms2CoreMbtClientServiceImpl.class);

    /**
     * 交易码：mbt999
     * 交易描述：V5通用记账
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Mbt999RespDto> mbt999(Mbt999ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_MBT999.key.concat("|").concat(EsbEnum.TRADE_CODE_MBT999.value));
        return null;
    }


    /**
     * 交易码：mbt951
     * 交易描述：批量文件处理申请
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Mbt951RespDto> mbt951(Mbt951ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_MBT951.key.concat("|").concat(EsbEnum.TRADE_CODE_MBT951.value));
        return null;
    }

    /**
     * 交易码：mbt952
     * 交易描述：批量结果确认处理
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Mbt952RespDto> mbt952(Mbt952ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_MBT952.key.concat("|").concat(EsbEnum.TRADE_CODE_MBT952.value));
        return null;
    }
}






