package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.Ib1243ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.Ib1243RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2CoreIbClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用核心系统的接口实现类(ib开头)
 */
@Component
public class Dscms2CoreIbClientServiceImpl implements Dscms2CoreIbClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2CoreIbClientServiceImpl.class);

    @Override
    public ResultDto<Ib1253RespDto> ib1253(Ib1253ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IB1253.key.concat("|").concat(EsbEnum.TRADE_CODE_IB1253.value));
        return null;
    }

    /**
     * 交易码：ib1241
     * 交易描述：外围自动冲正(出库撤销)
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Ib1241RespDto> ib1241(Ib1241ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IB1241.key.concat("|").concat(EsbEnum.TRADE_CODE_IB1241.value));
        return null;
    }

    /**
     * 交易码：ib1243
     * 交易描述：通用电子记帐交易（多借多贷-多币种）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Ib1243RespDto>  ib1243(Ib1243ReqDto reqDto){
        logger.error("访问{}失败，触发熔断。",EsbEnum.TRADE_CODE_IB1243.key.concat("|").concat(EsbEnum.TRADE_CODE_IB1243.value));
        return null;
    }

}



