package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00203.G00203ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00203.G00203RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10001.req.G10001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp.G10001RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10002.req.G10002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10002.resp.G10002RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2EcifClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用ECIF系统的接口实现类
 */
@Component
public class Dscms2EcifClientServiceImpl implements Dscms2EcifClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2EcifClientServiceImpl.class);

    /**
     * 对私客户清单查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<S10501RespDto> s10501(S10501ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_S10501.key.concat("|").concat(EsbEnum.TRADE_CODE_S10501.value));
        return null;
    }

    /**
     * 对私客户创建及维护接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<S00102RespDto> s00102(S00102ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_S00102.key.concat("|").concat(EsbEnum.TRADE_CODE_S00102.value));
        return null;
    }

    /**
     * 对公客户综合信息维护
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G00102RespDto> g00102(G00102ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G00102.key.concat("|").concat(EsbEnum.TRADE_CODE_G00102.value));
        return null;
    }

    /**
     * 对公及同业客户清单查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G10501RespDto> g10501(G10501ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G10501.key.concat("|").concat(EsbEnum.TRADE_CODE_G10501.value));
        return null;
    }

    /**
     * 同业客户开户
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G00202RespDto> g00202(G00202ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G00202.key.concat("|").concat(EsbEnum.TRADE_CODE_G00202.value));
        return null;
    }

    /**
     * 同业客户维护
     *
     * @param reqDto
     * @return
     */
    public ResultDto<G00203RespDto> g00203(G00203ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G00203.key.concat("|").concat(EsbEnum.TRADE_CODE_G00203.value));
        return null;
    }

    /**
     * 客户集团信息维护 (new) （处理码G11003）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G11003RespDto> g11003(G11003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G11003.key.concat("|").concat(EsbEnum.TRADE_CODE_G11003.value));
        return null;
    }

    /**
     * 客户集团信息维护 (new) （处理码G11004）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G11004RespDto> g11004(G11004ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G11004.key.concat("|").concat(EsbEnum.TRADE_CODE_G11004.value));
        return null;
    }

    /**
     * 交易码：g00101
     * 交易描述：对公客户综合信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G00101RespDto> g00101(G00101ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G00101.key.concat("|").concat(EsbEnum.TRADE_CODE_G00101.value));
        return null;
    }

    /**
     * 交易码：s00101
     * 交易描述：对私客户综合信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<S00101RespDto> s00101(S00101ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_S00101.key.concat("|").concat(EsbEnum.TRADE_CODE_S00101.value));
        return null;
    }

    /**
     * 交易码：g10001
     * 交易描述：客户群组信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G10001RespDto> g10001(G10001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G10001.key.concat("|").concat(EsbEnum.TRADE_CODE_G10001.value));
        return null;
    }


    /**
     * 交易码：g10002
     * 交易描述：客户群组信息维护
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<G10002RespDto> g10002(G10002ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_G10002.key.concat("|").concat(EsbEnum.TRADE_CODE_G10002.value));
        return null;
    }
}
