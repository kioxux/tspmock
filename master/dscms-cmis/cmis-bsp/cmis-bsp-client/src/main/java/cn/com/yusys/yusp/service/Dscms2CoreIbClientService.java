package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1241.Ib1241RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.Ib1243ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1243.Ib1243RespDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253ReqDto;
import cn.com.yusys.yusp.dto.client.esb.core.ib1253.Ib1253RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2CoreIbClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用核心系统的接口(ib开头)
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2CoreIbClientServiceImpl.class)
public interface Dscms2CoreIbClientService {

    /**
     * 根据账号查询帐户信息 (Ib1253)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallIb1253Example.java.txt
     */
    @PostMapping("/dscms2coreib/ib1253")
    public ResultDto<Ib1253RespDto> ib1253(Ib1253ReqDto reqDto);


    /**
     * 交易码：ib1241
     * 交易描述：外围自动冲正(出库撤销)
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreib/ib1241")
    public ResultDto<Ib1241RespDto> ib1241(Ib1241ReqDto reqDto);

    /**
     * 交易码：ib1243
     * 交易描述：通用电子记帐交易（多借多贷-多币种）
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2coreib/ib1243")
    public ResultDto<Ib1243RespDto>  ib1243(Ib1243ReqDto reqDto);

}






