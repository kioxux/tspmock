package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.com001.req.Com001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.com001.resp.Com001RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2ComstarClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:Comstar系统
 *
 * @author leehuang
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2ComstarClientServiceImpl.class)
public interface Dscms2ComstarClientService {
    /**
     * 交易码：com001
     * 交易描述：额度同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2comstar/com001")
    public ResultDto<Com001RespDto> com001(Com001ReqDto reqDto);
}
