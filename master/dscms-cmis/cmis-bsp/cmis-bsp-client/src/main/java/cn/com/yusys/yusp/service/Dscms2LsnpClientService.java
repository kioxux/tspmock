package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req.Lsnp01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.resp.Lsnp01RespDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.req.Lsnp02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp02.resp.Lsnp02RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2LsnpClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;


/**
 * BSP封装调用零售内评的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2LsnpClientServiceImpl.class)
public interface Dscms2LsnpClientService {

    /**
     * 交易码：lsnp01
     * 交易描述：信贷业务零售评级
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2lsnp/lsnp01")
    public ResultDto<Lsnp01RespDto> lsnp01(Lsnp01ReqDto reqDto);

    /**
     * 交易码：lsnp02
     * 交易描述：信用卡业务零售评级
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2lsnp/lsnp02")
    public ResultDto<Lsnp02RespDto> lsnp02(Lsnp02ReqDto reqDto);

}
