package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.req.Wxp001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp001.resp.Wxp001RespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp002.req.Wxp002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp002.resp.Wxp002RespDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp003.req.Wxp003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.wx.wxp003.resp.Wxp003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2WxClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 13:34
 * @since 2021/6/8 13:34
 */
@Component
public class Dscms2WxClientServiceImpl implements Dscms2WxClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2WxClientService.class);

    /**
     * 交易码：wxp001
     * 交易描述：信贷将审批结果推送给移动端
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Wxp001RespDto> wxp001(Wxp001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_WXP001.key.concat("|").concat(EsbEnum.TRADE_CODE_WXP001.value));
        return null;
    }

    /**
     * 交易码：wxp002
     * 交易描述：信贷将授信额度推送给移动端
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Wxp002RespDto> wxp002(Wxp002ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_WXP002.key.concat("|").concat(EsbEnum.TRADE_CODE_WXP002.value));
        return null;
    }

    /**
     * 交易码：wxp003
     * 交易描述：信贷将放款标识推送给移动端
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Wxp003RespDto> wxp003(Wxp003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_WXP003.key.concat("|").concat(EsbEnum.TRADE_CODE_WXP003.value));
        return null;
    }
}
