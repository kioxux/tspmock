package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs18.Irs18ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs18.Irs18RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.req.Irs19ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs19.resp.Irs19RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs21.Irs21RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs97.Irs97RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs98.Irs98RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.irs99.Irs99RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs10.Xirs10RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs11.req.Xirs11ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs11.resp.Xirs11RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs15.req.Xirs15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs15.resp.Xirs15RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs27.req.Xirs27ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs27.resp.Xirs27RespDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.req.Xirs28ReqDto;
import cn.com.yusys.yusp.dto.client.esb.irs.xirs28.resp.Xirs28RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2IrsClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用非零内评系统的接口实现类
 */
@Component
public class Dscms2IrsClientServiceImpl implements Dscms2IrsClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2IrsClientServiceImpl.class);

    /**
     * 单一客户限额测算信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Irs21RespDto> irs21(Irs21ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS21.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS21.value));
        return null;
    }

    /**
     * 新增授信时债项评级
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Irs97RespDto> irs97(Irs97ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS97.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS97.value));
        return null;
    }

    /**
     * 单授信申请债项评级
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Irs98RespDto> irs98(Irs98ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS98.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS98.value));
        return null;
    }

    /**
     * 交易码：irs18
     * 交易描述：评级主办权更新
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Irs18RespDto> irs18(Irs18ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS18.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS18.value));
        return null;
    }

    /**
     * 交易码：irs19
     * 交易描述：专业贷款基本信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Irs19RespDto> irs19(Irs19ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS19.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS19.value));
        return null;
    }


    /**
     * 交易码：xirs10
     * 交易描述：公司客户信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xirs10RespDto> xirs10(Xirs10ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS10.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS10.value));

        return null;
    }

    /**
     * 单授信申请债项评级
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Irs99RespDto> irs99(Irs99ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS99.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS99.value));
        return null;
    }

    /**
     * 交易码：xirs11
     * 交易描述：同业客户信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xirs11RespDto> xirs11(Xirs11ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS11.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS11.value));
        return null;
    }

    /**
     * 交易码：xirs28
     * 交易描述：财务信息同步
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xirs28RespDto> xirs28(Xirs28ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS28.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS28.value));
        return null;
    }

    /**
     * 交易码：xirs15
     * 交易描述：评级在途申请标识
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xirs15RespDto> xirs15(Xirs15ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS15.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS15.value));
        return null;
    }

    /**
     * 交易码：xirs27
     * 交易描述：工作台提示条数
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xirs27RespDto> xirs27(Xirs27ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IRS27.key.concat("|").concat(EsbEnum.TRADE_CODE_IRS27.value));
        return null;
    }
}
