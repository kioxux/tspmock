package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.req.D11005ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.resp.D11005RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.req.D11010ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d11010.resp.D11010RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.req.D12000ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.resp.D12000RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12011.D12011RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d12050.D12050RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.req.D13087ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13087.resp.D13087RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.req.D13160ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13160.resp.D13160RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.req.D13162ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d13162.resp.D13162RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d14020.D14020RespDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011ReqDto;
import cn.com.yusys.yusp.dto.client.gxp.tonglian.d15011.D15011RespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.service.Dscms2TonglianClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/27 9:58
 * @since 2021/5/27 9:58
 */
@Component
public class Dscms2TonglianClientServiceImpl implements Dscms2TonglianClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2TonglianClientService.class);


    /**
     * 交易码：d15011
     * 交易描述：永久额度调整
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D15011RespDto> d15011(D15011ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D15011.key.concat("|").concat(GxpEnum.TRADE_CODE_D15011.value));
        return null;
    }

    /**
     * 交易码：d13160
     * 交易描述：大额现金分期申请
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D13160RespDto> d13160(D13160ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D13160.key.concat("|").concat(GxpEnum.TRADE_CODE_D13160.value));
        return null;
    }

    /**
     * 交易码：d13087
     * 交易描述：现金（大额）放款接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D13087RespDto> d13087(D13087ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D13087.key.concat("|").concat(GxpEnum.TRADE_CODE_D13087.value));
        return null;
    }

    /**
     * 交易码：d12050
     * 交易描述：账单列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D12050RespDto> d12050(D12050ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D12050.key.concat("|").concat(GxpEnum.TRADE_CODE_D12050.value));
        return null;
    }

    /**
     * 交易码：d14020
     * 交易描述：卡片信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D14020RespDto> d14020(D14020ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D14020.key.concat("|").concat(GxpEnum.TRADE_CODE_D14020.value));
        return null;
    }

    /**
     * 交易码：d12011
     * 交易描述：账单交易明细查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D12011RespDto> d12011(D12011ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D14020.key.concat("|").concat(GxpEnum.TRADE_CODE_D14020.value));
        return null;
    }

    /**
     * 交易码：信用卡申请
     * 交易描述：d11005
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D11005RespDto> d11005(D11005ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D11005.key.concat("|").concat(GxpEnum.TRADE_CODE_D11005.value));
        return null;
    }

    /**
     * 交易码：d11010
     * 交易描述：客户基本信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D11010RespDto> d11010(D11010ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D11010.key.concat("|").concat(GxpEnum.TRADE_CODE_D11010.value));
        return null;
    }

    /**
     * 交易码：d12000
     * 交易描述：账户信息查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D12000RespDto> d12000(D12000ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D12000.key.concat("|").concat(GxpEnum.TRADE_CODE_D12000.value));
        return null;
    }

    /**
     * 交易码：d13162
     * 交易描述：分期审核
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<D13162RespDto> d13162(D13162ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_D13162.key.concat("|").concat(GxpEnum.TRADE_CODE_D13162.value));
        return null;
    }
}
