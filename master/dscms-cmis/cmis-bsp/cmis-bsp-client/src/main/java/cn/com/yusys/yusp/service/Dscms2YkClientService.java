package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.req.Yky001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.yk.yky001.resp.Yky001RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2YkClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * 封装的接口类:印控系统的接口
 *
 * @author code-generator
 * @version 1.0
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2YkClientServiceImpl.class)
public interface Dscms2YkClientService {

    /**
     * 用印申请 (yky001)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallYky001Example.java.txt
     */
    @PostMapping("/dscms2yk/yky001")
    public ResultDto<Yky001RespDto> yky001(Yky001ReqDto reqDto);

}
