package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.req.CljctzReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.cljctz.resp.CljctzRespDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.req.IdchekReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.idchek.resp.IdchekRespDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.req.MfzjcrReqDto;
import cn.com.yusys.yusp.dto.client.esb.gaps.mfzjcr.resp.MfzjcrRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2GapsClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/29 13:34
 * @since 2021/6/29 13:34
 */
@Component
public class Dscms2GapsClientServiceImpl implements Dscms2GapsClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2GapsClientServiceImpl.class);

    /**
     * 交易码：idchek
     * 交易描述：身份证核查
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<IdchekRespDto> idchek(IdchekReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_IDCHEK.key.concat("|").concat(EsbEnum.TRADE_CODE_IDCHEK.value));
        return null;
    }


    /**
     * 交易码：mfzjcr
     * 交易描述：买方资金存入
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<MfzjcrRespDto> mfzjcr(MfzjcrReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_MFZJCR.key.concat("|").concat(EsbEnum.TRADE_CODE_MFZJCR.value));
        return null;
    }


    /**
     * 交易码：cljctz
     * 交易描述：连云港存量房列表
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<CljctzRespDto> cljctz(CljctzReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_CLJCTZ.key.concat("|").concat(EsbEnum.TRADE_CODE_CLJCTZ.value));
        return null;
    }
}
