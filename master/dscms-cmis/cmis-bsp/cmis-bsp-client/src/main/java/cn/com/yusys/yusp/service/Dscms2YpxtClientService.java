package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhReqDto;
import cn.com.yusys.yusp.dto.client.esb.cxypbh.CxypbhRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx.BdcqcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.billyp.BillypRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.bucont.BucontReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.req.BusconReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.buscon.resp.BusconRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.req.BusinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.businf.resp.BusinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.certis.CertisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.cetinf.CetinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.coninf.ConinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.contra.ContraRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.req.CreditypisReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.resp.CreditypisRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.dealsy.DealsyRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp.EvalypRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.req.GuarstReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.guarst.resp.GuarstRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf.LmtinfRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.manage.req.ManageReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.manage.resp.ManageRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.req.SpsyypReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.resp.SpsyypRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypbdccx.req.XdypbdccxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypbdccx.resp.XdypbdccxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypcdpjcx.req.XdypcdpjcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypcdpjcx.resp.XdypcdpjcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypdywcx.req.XdypdywcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypdywcx.resp.XdypdywcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.req.XdypgyrcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypgyrcx.resp.XdypgyrcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypjbxxcx.req.XdypjbxxcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypjbxxcx.resp.XdypjbxxcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.req.XdypxxtbyrReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypxxtbyr.resp.XdypxxtbyrRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypzywcx.req.XdypzywcxReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.xdypzywcx.resp.XdypzywcxRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.yhxxtb.req.YhxxtbReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.yhxxtb.resp.YhxxtbRespDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg.YpztbgReqDto;
import cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg.YpztbgRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2YpxtClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用押品系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2YpxtClientServiceImpl.class)
public interface Dscms2YpxtClientService {

    /**
     * 权证状态同步接口（处理码certis）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCertisExample.java.txt
     */
    @PostMapping("/dscms2ypxt/certis")
    public ResultDto<CertisRespDto> certis(CertisReqDto reqDto);


    /**
     * 权证信息同步接口（处理码cetinf）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallCetinfExample.java.txt
     */
    @PostMapping("/dscms2ypxt/cetinf")
    public ResultDto<CetinfRespDto> cetinf(CetinfReqDto reqDto);


    /**
     * 押品状态变更（ypztbg）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallYpztbgExample.java.txt
     */
    @PostMapping("/dscms2ypxt/ypztbg")
    public ResultDto<YpztbgRespDto> ypztbg(YpztbgReqDto reqDto);


    /**
     * 不动产权证查询（bdcqcx）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallBdcqcxExample.java.txt
     */
    @PostMapping("/dscms2ypxt/bdcqcx")
    public ResultDto<BdcqcxRespDto> bdcqcx(BdcqcxReqDto reqDto);


    /**
     * 业务与担保合同关系 (bucont)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallBucontExample.java.txt
     */

    @PostMapping("/dscms2ypxt/bucont")
    public ResultDto<BucontReqDto> bucont(BucontReqDto reqDto);

    /**
     * 信贷担保合同信息同步 (coninf)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallConinfExample.java.txt
     */
    @PostMapping("/dscms2ypxt/coninf")
    public ResultDto<ConinfRespDto> coninf(ConinfReqDto reqDto);


    /**
     * 押品与担保合同关系同步 (contra)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallContraExample.java.txt
     */
    @PostMapping("/dscms2ypxt/contra")
    public ResultDto<ContraRespDto> contra(ContraReqDto reqDto);

    /**
     * 交易码：evalyp
     * 交易描述：押品我行确认价值同步接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/evalyp")
    public ResultDto<EvalypRespDto> evalyp(EvalypReqDto reqDto);

    /**
     * 交易码：xdypgyrcx
     * 交易描述：查询共有人信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/xdypgyrcx")
    public ResultDto<XdypgyrcxRespDto> xdypgyrcx(XdypgyrcxReqDto reqDto);

    /**
     * 交易码：xdypjbxxcx
     * 交易描述：查询共有人信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/xdypjbxxcx")
    public ResultDto<XdypjbxxcxRespDto> xdypjbxxcx(XdypjbxxcxReqDto reqDto);

    /**
     * 交易码：xdypbdccx
     * 交易描述：查询不动产信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/xdypbdccx")
    public ResultDto<XdypbdccxRespDto> xdypbdccx(XdypbdccxReqDto reqDto);

    /**
     * 交易码：xdypzywcx
     * 交易描述：查询质押物信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/xdypzywcx")
    public ResultDto<XdypzywcxRespDto> xdypzywcx(XdypzywcxReqDto reqDto);

    /**
     * 交易码：xdypdywcx
     * 交易描述：查询抵押物信息
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/xdypdywcx")
    public ResultDto<XdypdywcxRespDto> xdypdywcx(XdypdywcxReqDto reqDto);

    /**
     * 交易码：xdypcdpjcx
     * 交易描述：查询存单票据信息
     *
     * @param reqDto
     * @return
     */

    @PostMapping("/dscms2ypxt/xdypcdpjcx")
    public ResultDto<XdypcdpjcxRespDto> xdypcdpjcx(XdypcdpjcxReqDto reqDto);

    /**
     * 交易码：billyp
     * 交易描述：票据信息同步接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/billyp")
    public ResultDto<BillypRespDto> billyp(BillypReqDto reqDto);

    /**
     * 交易码：creditypis
     * 交易描述：信用证信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/creditypis")
    public ResultDto<CreditypisRespDto> creditypis(CreditypisReqDto reqDto);

    /**
     * 交易码：xdypxxtbyr
     * 交易描述：押品信息同步及引入
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/xdypxxtbyr")
    public ResultDto<XdypxxtbyrRespDto> xdypxxtbyr(XdypxxtbyrReqDto reqDto);

    /**
     * 交易码：cxypbh
     * 交易描述：通过票据号码查询押品编号
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/cxypbh")
    public ResultDto<CxypbhRespDto> cxypbh(CxypbhReqDto reqDto);


    /**
     * 交易码：businf
     * 交易描述：信贷业务合同信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/businf")
    public ResultDto<BusinfRespDto> businf(BusinfReqDto reqDto);

    /**
     * 交易码：manage
     * 交易描述：管护权移交信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/manage")
    public ResultDto<ManageRespDto> manage(ManageReqDto reqDto);

    /**
     * 交易码：buscon
     * 交易描述：信贷业务与押品关联关系信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/buscon")
    public ResultDto<BusconRespDto> buscon(BusconReqDto reqDto);

    /**
     * 交易码：yhxxtb
     * 交易描述：用户信息同步接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/yhxxtb")
    public ResultDto<YhxxtbRespDto> yhxxtb(YhxxtbReqDto reqDto);

    /**
     * 交易码：spsyyp
     * 交易描述：信贷审批结果同步接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/spsyyp")
    public ResultDto<SpsyypRespDto> spsyyp(SpsyypReqDto reqDto);

    /**
     * 交易码：dealsy
     * 交易描述：押品处置信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/dealsy")
    public ResultDto<DealsyRespDto> dealsy(DealsyReqDto reqDto);

    /**
     * 交易码：lmtinf
     * 交易描述：信贷授信协议信息同步
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/lmtinf")
    public ResultDto<LmtinfRespDto> lmtinf(LmtinfReqDto reqDto);

    /**
     * 交易码：guarst
     * 交易描述：押品状态变更推送
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ypxt/guarst")
    public ResultDto<GuarstRespDto> guarst(GuarstReqDto reqDto);
}
