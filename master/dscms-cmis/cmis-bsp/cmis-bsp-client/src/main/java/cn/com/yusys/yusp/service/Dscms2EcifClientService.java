package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00101.G00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00102.G00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g00202.G00202RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10001.req.G10001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp.G10001RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10002.req.G10002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10002.resp.G10002RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g10501.G10501RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11003.G11003RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.g11004.G11004RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00101.S00101RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s00102.S00102RespDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501ReqDto;
import cn.com.yusys.yusp.dto.client.esb.ecif.s10501.S10501RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2EcifClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用ECIF系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2EcifClientServiceImpl.class)
public interface Dscms2EcifClientService {
    /**
     * 对私客户清单查询 (s10501)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallS10501Example.java.txt
     */
    @PostMapping("/dscms2ecif/s10501")
    public ResultDto<S10501RespDto> s10501(S10501ReqDto reqDto);

    /**
     * 对私客户创建及维护接口 (s00102)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallS00102Example.java.txt
     */
    @PostMapping("/dscms2ecif/s00102")
    public ResultDto<S00102RespDto> s00102(S00102ReqDto reqDto);


    /**
     * 对公客户综合信息维护 (g00102)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallG00102Example.java.txt
     */
    @PostMapping("/dscms2ecif/g00102")
    public ResultDto<G00102RespDto> g00102(G00102ReqDto reqDto);

    /**
     * 对公及同业客户清单查询 (g10501)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallG10501Example.java.txt
     */
    @PostMapping("/dscms2ecif/g10501")
    public ResultDto<G10501RespDto> g10501(G10501ReqDto reqDto);

    /**
     * 同业客户开户 (g00202)
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallG00102Example.java.txt
     */
    @PostMapping("/dscms2ecif/g00202")
    public ResultDto<G00202RespDto> g00202(G00202ReqDto reqDto);

    /**
     * 客户集团信息维护 (new) （处理码G11003）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallG10501Example.java.txt
     */
    @PostMapping("/dscms2ecif/g11003")
    public ResultDto<G11003RespDto> g11003(G11003ReqDto reqDto);


    /**
     * 客户集团信息查询（new） （处理码G11004）
     *
     * @param reqDto
     * @return
     * @code 示例代码：CallG11004Example.java.txt
     */
    @PostMapping("/dscms2ecif/g11004")
    public ResultDto<G11004RespDto> g11004(G11004ReqDto reqDto);

    /**
     * 交易码：g00101
     * 交易描述：对公客户综合信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ecif/g00101")
    public ResultDto<G00101RespDto> g00101(G00101ReqDto reqDto);

    /**
     * 交易码：s00101
     * 交易描述：对私客户综合信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ecif/s00101")
    public ResultDto<S00101RespDto> s00101(S00101ReqDto reqDto);


    /**
     * 交易码：g10001
     * 交易描述：客户群组信息查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ecif/g10001")
    public ResultDto<G10001RespDto> g10001(G10001ReqDto reqDto);

    /**
     * 交易码：g10002
     * 交易描述：客户群组信息维护
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2ecif/g10002")
    public ResultDto<G10002RespDto> g10002(G10002ReqDto reqDto);
}
