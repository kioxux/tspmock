package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdent.XxdentRespDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.req.XxdrelReqDto;
import cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp.XxdrelRespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2RlzyxtClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用人力资源系统的接口实现类
 */
@Component
public class Dscms2RlzyxtClientServiceImpl implements Dscms2RlzyxtClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2RlzyxtClientServiceImpl.class);

    /**
     * 交易码：xxdrel
     * 交易描述：查询人员基本信息岗位信息家庭信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XxdrelRespDto> xxdrel(XxdrelReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XXDREL.key.concat("|").concat(EsbEnum.TRADE_CODE_XXDREL.value));
        return null;
    }

    /**
     * 交易码：xxdent
     * 交易描述：新入职人员信息登记
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<XxdentRespDto> xxdent(XxdentReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XXDENT.key.concat("|").concat(EsbEnum.TRADE_CODE_XXDENT.value));
        return null;
    }
}
