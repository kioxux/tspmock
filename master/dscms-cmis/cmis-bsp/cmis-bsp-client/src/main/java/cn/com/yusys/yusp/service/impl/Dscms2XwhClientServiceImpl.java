package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.req.Xwh001ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.resp.Xwh001RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.req.Xwh003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.resp.Xwh003RespDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.req.Yx0003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.xwh.yx0003.resp.Yx0003RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2XwhClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类:小微公众号
 *
 * @author lihh
 * @version 1.0
 */
@Component
public class Dscms2XwhClientServiceImpl implements Dscms2XwhClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2XwhClientServiceImpl.class);

    /**
     * 交易码：xwh001
     * 交易描述：借据台账信息接收
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwh001RespDto> xwh001(Xwh001ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWH001.key.concat("|").concat(EsbEnum.TRADE_CODE_XWH001.value));
        return null;
    }

    /**
     * 交易码：xwh003
     * 交易描述：核销锁定接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Xwh003RespDto> xwh003(Xwh003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_XWH003.key.concat("|").concat(EsbEnum.TRADE_CODE_XWH003.value));
        return null;
    }

    /**
     * 交易码：yx0003
     * 交易描述：市民贷优惠券核销锁定
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Yx0003RespDto> yx0003(Yx0003ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_YX0003.key.concat("|").concat(EsbEnum.TRADE_CODE_YX0003.value));
        return null;
    }
}
