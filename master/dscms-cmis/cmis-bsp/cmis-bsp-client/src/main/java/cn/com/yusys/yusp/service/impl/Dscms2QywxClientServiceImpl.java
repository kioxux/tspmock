package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxReqDto;
import cn.com.yusys.yusp.dto.client.gxp.wxpt.qywx.QywxRespDto;
import cn.com.yusys.yusp.enums.online.GxpEnum;
import cn.com.yusys.yusp.service.Dscms2QywxClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * BSP封装调用微信平台的接口实现类
 */
@Component
public class Dscms2QywxClientServiceImpl implements Dscms2QywxClientService {
    private static final Logger logger = LoggerFactory.getLogger(Dscms2QywxClientServiceImpl.class);

    /**
     * 推送企业微信
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<QywxRespDto> qywx(QywxReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", GxpEnum.TRADE_CODE_QYWX.key.concat("|").concat(GxpEnum.TRADE_CODE_QYWX.value));
        return null;
    }
}
