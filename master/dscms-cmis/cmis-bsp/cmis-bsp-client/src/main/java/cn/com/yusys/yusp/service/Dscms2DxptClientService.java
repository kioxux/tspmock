package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxReqDto;
import cn.com.yusys.yusp.dto.client.esb.dxpt.senddx.SenddxRespDto;
import cn.com.yusys.yusp.service.impl.Dscms2DxptClientServiceImpl;
import cn.com.yusys.yusp.service.impl.Dscms2IrsClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用短信平台的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2DxptClientServiceImpl.class)
public interface Dscms2DxptClientService {

    /**
     * 短信/微信发送批量接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2dxpt/senddx")
    public ResultDto<SenddxRespDto> senddx(SenddxReqDto reqDto);

}
