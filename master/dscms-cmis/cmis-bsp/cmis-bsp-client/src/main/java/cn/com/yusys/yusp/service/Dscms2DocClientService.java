package cn.com.yusys.yusp.service;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.req.Doc000ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc000.resp.Doc000RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc002.req.Doc002ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc002.resp.Doc002RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc003.req.Doc003ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc003.resp.Doc003RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc004.req.Doc004ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc004.resp.Doc004RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.req.Doc005ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc005.resp.Doc005RespDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.req.Doc006ReqDto;
import cn.com.yusys.yusp.dto.client.esb.doc.doc006.resp.Doc006RespDto;
import cn.com.yusys.yusp.service.impl.Dscms2DocClientServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * BSP封装调用档案系统的接口
 */
@FeignClient(name = "cmis-bsp", path = "/api", fallback = Dscms2DocClientServiceImpl.class)
public interface Dscms2DocClientService {
    /**
     * 交易码：doc000
     * 交易描述：入库接口
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2doc/doc000")
    public ResultDto<Doc000RespDto> doc000(Doc000ReqDto reqDto);

    /**
     * 交易码：doc002
     * 交易描述：入库查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2doc/doc002")
    public ResultDto<Doc002RespDto> doc002(Doc002ReqDto reqDto);

    /**
     * 交易码：doc003
     * 交易描述：出库操作、归还在途操作
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2doc/doc003")
    public ResultDto<Doc003RespDto> doc003(Doc003ReqDto reqDto);

    /**
     * 交易码：doc005
     * 交易描述：申请出库
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2doc/doc005")
    public ResultDto<Doc005RespDto> doc005(Doc005ReqDto reqDto);

    /**
     * 交易码：doc006
     * 交易描述：出库归还
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2doc/doc006")
    public ResultDto<Doc006RespDto> doc006(Doc006ReqDto reqDto);


    /**
     * 交易码：doc004
     * 交易描述：调阅待出库查询
     *
     * @param reqDto
     * @return
     */
    @PostMapping("/dscms2doc/doc004")
    public ResultDto<Doc004RespDto> doc004(Doc004ReqDto reqDto);

}
