package cn.com.yusys.yusp.service.impl;

import cn.com.yusys.yusp.commons.module.adapter.web.rest.ResultDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01.Fbxd01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd01.Fbxd01RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02.Fbxd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02.Fbxd02RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd03.Fbxd03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd03.Fbxd03RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.Fbxd04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.Fbxd04RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd05.Fbxd05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd05.Fbxd05RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd06.Fbxd06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd06.Fbxd06RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07.Fbxd07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd07.Fbxd07RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd08.Fbxd08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd08.Fbxd08RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd09.Fbxd09ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd09.Fbxd09RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10.Fbxd10ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd10.Fbxd10RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.Fbxd11ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd11.Fbxd11RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13.Fbxd13ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13.Fbxd13RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14.Fbxd14ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14.Fbxd14RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15.Fbxd15ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd15.Fbxd15RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16.Fbxd16ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxd16.Fbxd16RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw01.Fbxw01RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw02.Fbxw02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw02.Fbxw02RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03.Fbxw03RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw04.Fbxw04RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05.Fbxw05RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06.Fbxw06RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw07.Fbxw07ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw07.Fbxw07RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw08.Fbxw08ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbxw08.Fbxw08RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd02.Fbyd02ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd02.Fbyd02RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd33.Fbyd33ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd33.Fbyd33RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd34.req.Fbyd34ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd34.resp.Fbyd34RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.req.Fbyd36ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.resp.Fbyd36RespDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01ReqDto;
import cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01.Fkyx01RespDto;
import cn.com.yusys.yusp.enums.online.EsbEnum;
import cn.com.yusys.yusp.service.Dscms2RircpClientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 封装的接口实现类: 接口实现类零售智能风控系统的接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021/4/16 17:07
 */
@Component
public class Dscms2RircpClientServiceImpl implements Dscms2RircpClientService {

    private static final Logger logger = LoggerFactory.getLogger(Dscms2RircpClientServiceImpl.class);

    /**
     * 优惠利率申请（fbxw07）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw07RespDto> fbxw07(Fbxw07ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW07.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW07.value));
        return null;
    }

    /**
     * 授信申请提交（fbxw01）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw01RespDto> fbxw01(Fbxw01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW01.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW01.value));
        return null;
    }

    /**
     * 终审结果同步（fbxw02）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw02RespDto> fbxw02(Fbxw02ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW02.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW02.value));
        return null;
    }

    /**
     * 增享贷风控测算（fbxw03）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw03RespDto> fbxw03(Fbxw03ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW03.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW03.value));
        return null;
    }

    /**
     * 惠享贷规则审批申请接口（fbxw04）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw04RespDto> fbxw04(Fbxw04ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW04.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW04.value));
        return null;
    }

    /**
     * 惠享贷批复同步接口（fbxw05）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw05RespDto> fbxw05(Fbxw05ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW05.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW05.value));
        return null;
    }

    /**
     * 利率定价测算提交接口（fbxw06）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw06RespDto> fbxw06(Fbxw06ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW06.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW06.value));
        return null;
    }

    /**
     * 交易码：fbyd02
     * 交易描述：终审申请提交
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbyd02RespDto> fbyd02(Fbyd02ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBYD02.key.concat("|").concat(EsbEnum.TRADE_CODE_FBYD02.value));
        return null;
    }

    /**
     * 交易码：fkyx01
     * 交易描述：优享贷客户经理分配通知接口
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fkyx01RespDto> fkyx01(Fkyx01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FKYX01.key.concat("|").concat(EsbEnum.TRADE_CODE_FKYX01.value));
        return null;
    }


    /**
     * 授信审批作废（fbxw08）
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxw08RespDto> fbxw08(Fbxw08ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXW08.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXW08.value));
        return null;
    }

    /**
     * 交易码：fbxd01
     * 交易描述：惠享贷授信申请件数取得
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd01RespDto> fbxd01(Fbxd01ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD01.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD01.value));
        return null;
    }

    /**
     * 交易码：fbxd02
     * 交易描述：为正式客户更新信贷客户信息手机号码
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd02RespDto> fbxd02(Fbxd02ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD02.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD02.value));
        return null;
    }

    /**
     * 交易码：fbxd03
     * 交易描述：从风控获取客户详细信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd03RespDto> fbxd03(Fbxd03ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD03.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD03.value));
        return null;
    }


    /**
     * 交易码：fbxd04
     * 交易描述：查找指定数据日期的放款合约明细记录历史表（利翃）一览
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd04RespDto> fbxd04(Fbxd04ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD04.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD04.value));
        return null;
    }

    /**
     * 交易码：fbxd05
     * 交易描述：查找指定数据日期的放款合约明细记录历史表（利翃）一览信息的借据号
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd05RespDto> fbxd05(Fbxd05ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD05.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD05.value));
        return null;
    }

    /**
     * 交易码：fbxd06
     * 交易描述：获取该笔借据的最新五级分类以及数据日期
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd06RespDto> fbxd06(Fbxd06ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD06.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD06.value));
        return null;
    }

    /**
     * 交易码：fbxd07
     * 交易描述：获取指定数据日期存在还款记录的借据一览信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd07RespDto> fbxd07(Fbxd07ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD07.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD07.value));
        return null;
    }

    /**
     * 交易码：fbxd08
     * 交易描述：查找（利翃）实还正常本金和逾期本金之和
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd08RespDto> fbxd08(Fbxd08ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD08.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD08.value));
        return null;
    }

    /**
     * 交易码：fbxd09
     * 交易描述：查询日初（合约）信息历史表（利翃）总数据量
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd09RespDto> fbxd09(Fbxd09ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD09.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD09.value));
        return null;
    }

    /**
     * 交易码：fbxd10
     * 交易描述：查询日初（合约）信息历史表（利翃）的合约状态和结清日期
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd10RespDto> fbxd10(Fbxd10ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD10.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD10.value));
        return null;
    }

    /**
     * 交易码：fbxd11
     * 交易描述：获取还款记录的借据一览信息，包括借据号、借据金额、借据余额
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd11RespDto> fbxd11(Fbxd11ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD11.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD11.value));
        return null;
    }

    /**
     * 交易码：fbxd13
     * 交易描述：查询客户核心编号，客户名称对应的放款和还款信息
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd13RespDto> fbxd13(Fbxd13ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD13.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD13.value));
        return null;
    }

    /**
     * 交易码：fbxd14
     * 交易描述：查询还款（合约）明细历史表（利翃）中的全量借据一览
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd14RespDto> fbxd14(Fbxd14ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD14.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD14.value));
        return null;
    }

    /**
     * 交易码：fbxd15
     * 交易描述：还款日期升序查找（利翃）实还正常本金和逾期本金之和，本次还款前应收未收正常本金和逾期本金之和
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd15RespDto> fbxd15(Fbxd15ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD15.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD15.value));
        return null;
    }

    /**
     * 交易码：fbxd16
     * 交易描述：取得蚂蚁核销记录表的核销借据号一览
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbxd16RespDto> fbxd16(Fbxd16ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBXD16.key.concat("|").concat(EsbEnum.TRADE_CODE_FBXD16.value));
        return null;
    }

    /**
     * 交易码：fbyd34
     * 交易描述：支用模型校验
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbyd34RespDto> fbyd34(Fbyd34ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBYD34.key.concat("|").concat(EsbEnum.TRADE_CODE_FBYD34.value));
        return null;
    }

    /**
     * 交易码：fbyd36
     * 交易描述：授信列表查询
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbyd36RespDto> fbyd36(Fbyd36ReqDto reqDto) {
        logger.error("访问{}失败，触发熔断。", EsbEnum.TRADE_CODE_FBYD36.key.concat("|").concat(EsbEnum.TRADE_CODE_FBYD36.value));
        return null;
    }

    /**
     * 交易码：fbyd33
     * 交易描述：预授信申请提交
     *
     * @param reqDto
     * @return
     */
    @Override
    public ResultDto<Fbyd33RespDto>  fbyd33(Fbyd33ReqDto reqDto){
        logger.error("访问{}失败，触发熔断。",EsbEnum.TRADE_CODE_FBYD33.key.concat("|").concat(EsbEnum.TRADE_CODE_FBYD33.value));
        return null;
    }
}
