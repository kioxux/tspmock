package cn.com.yusys.yusp.dto.client.esb.xwywglpt.dhxd01.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷系统请求小V平台贷后预警推送定期检查清单失败后重发接口
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Dhxd01ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "doc_name")
    private String doc_name;//文件名称

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    @Override
    public String toString() {
        return "Dhxd01ReqDto{" +
                "doc_name='" + doc_name + '\'' +
                '}';
    }
}  
