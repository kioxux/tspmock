package cn.com.yusys.yusp.dto.server.biz.xddb0003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class OwnerList {

    @JsonProperty(value = "guarid")
    private String guarid;//押品编号

    @JsonProperty(value = "commonOwnerId")
    private String commonOwnerId;//共有人客户编号

    @JsonProperty(value = "commonOwnerName")
    private String commonOwnerName;//共有人客户名称

    @JsonProperty(value = "commonOwnerType")
    private String commonOwnerType;//共有人客户类型

    @JsonProperty(value = "commonCertType")
    private String commonCertType;//共有人证件类型

    @JsonProperty(value = "commonCertCode")
    private String commonCertCode;//共有人证件号码

    @JsonProperty(value = "commonHoldPortio")
    private BigDecimal commonHoldPortio;//共有人所占份额

    @JsonProperty(value = "commonHouseLandNo")
    private String commonHouseLandNo;//房产证（共有权证）号码

    @JsonProperty(value = "commonLandNo")
    private String commonLandNo;//土地证（共有权证）号码

    @JsonProperty(value = "shareOwnerType")
    private String shareOwnerType;//共有人类型

    @JsonProperty(value = "commonAddr")
    private String commonAddr;//共有人地址

    @JsonProperty(value = "commonTel")
    private String commonTel;//共有人电话

    @JsonProperty(value = "houseNo")
    private String houseNo;//不动产（房产证号）

    @JsonProperty(value = "commonQq")
    private String commonQq;//共有人QQ号码

    @JsonProperty(value = "commonWxin")
    private String commonWxin;//共有人微信号码

    @JsonProperty(value = "commonMail")
    private String commonMail;//共有人电子邮箱

    @JsonProperty(value = "legalName")
    private String legalName;//联系人

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public String getCommonOwnerId() {
        return commonOwnerId;
    }

    public void setCommonOwnerId(String commonOwnerId) {
        this.commonOwnerId = commonOwnerId;
    }

    public String getCommonOwnerName() {
        return commonOwnerName;
    }

    public void setCommonOwnerName(String commonOwnerName) {
        this.commonOwnerName = commonOwnerName;
    }

    public String getCommonOwnerType() {
        return commonOwnerType;
    }

    public void setCommonOwnerType(String commonOwnerType) {
        this.commonOwnerType = commonOwnerType;
    }

    public String getCommonCertType() {
        return commonCertType;
    }

    public void setCommonCertType(String commonCertType) {
        this.commonCertType = commonCertType;
    }

    public String getCommonCertCode() {
        return commonCertCode;
    }

    public void setCommonCertCode(String commonCertCode) {
        this.commonCertCode = commonCertCode;
    }

    public BigDecimal getCommonHoldPortio() {
        return commonHoldPortio;
    }

    public void setCommonHoldPortio(BigDecimal commonHoldPortio) {
        this.commonHoldPortio = commonHoldPortio;
    }

    public String getCommonHouseLandNo() {
        return commonHouseLandNo;
    }

    public void setCommonHouseLandNo(String commonHouseLandNo) {
        this.commonHouseLandNo = commonHouseLandNo;
    }

    public String getCommonLandNo() {
        return commonLandNo;
    }

    public void setCommonLandNo(String commonLandNo) {
        this.commonLandNo = commonLandNo;
    }

    public String getShareOwnerType() {
        return shareOwnerType;
    }

    public void setShareOwnerType(String shareOwnerType) {
        this.shareOwnerType = shareOwnerType;
    }

    public String getCommonAddr() {
        return commonAddr;
    }

    public void setCommonAddr(String commonAddr) {
        this.commonAddr = commonAddr;
    }

    public String getCommonTel() {
        return commonTel;
    }

    public void setCommonTel(String commonTel) {
        this.commonTel = commonTel;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getCommonQq() {
        return commonQq;
    }

    public void setCommonQq(String commonQq) {
        this.commonQq = commonQq;
    }

    public String getCommonWxin() {
        return commonWxin;
    }

    public void setCommonWxin(String commonWxin) {
        this.commonWxin = commonWxin;
    }

    public String getCommonMail() {
        return commonMail;
    }

    public void setCommonMail(String commonMail) {
        this.commonMail = commonMail;
    }

    public String getLegalName() {
        return legalName;
    }

    public void setLegalName(String legalName) {
        this.legalName = legalName;
    }

    @Override
    public String toString() {
        return "OwnerList{" +
                "guarid='" + guarid + '\'' +
                ", commonOwnerId='" + commonOwnerId + '\'' +
                ", commonOwnerName='" + commonOwnerName + '\'' +
                ", commonOwnerType='" + commonOwnerType + '\'' +
                ", commonCertType='" + commonCertType + '\'' +
                ", commonCertCode='" + commonCertCode + '\'' +
                ", commonHoldPortio=" + commonHoldPortio +
                ", commonHouseLandNo='" + commonHouseLandNo + '\'' +
                ", commonLandNo='" + commonLandNo + '\'' +
                ", shareOwnerType='" + shareOwnerType + '\'' +
                ", commonAddr='" + commonAddr + '\'' +
                ", commonTel='" + commonTel + '\'' +
                ", houseNo='" + houseNo + '\'' +
                ", commonQq='" + commonQq + '\'' +
                ", commonWxin='" + commonWxin + '\'' +
                ", commonMail='" + commonMail + '\'' +
                ", legalName='" + legalName + '\'' +
                '}';
    }
}
