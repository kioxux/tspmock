package cn.com.yusys.yusp.dto.server.biz.xdxw0061.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：通过无还本续贷调查表编号查询配偶核心客户号
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdxw0061RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

	@JsonProperty(value = "data")
	private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdxw0061RespDto{" +
                "data=" + data +
                '}';
    }
}
