package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	年报-网站信息
@JsonPropertyOrder(alphabetic = true)
public class YEARREPORTWEBSITEINFO implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ANCHEID")
    private String ANCHEID;//	年报ID
    @JsonProperty(value = "DOMAIN")
    private String DOMAIN;//	网站（网店）地址
    @JsonProperty(value = "WEBSITNAME")
    private String WEBSITNAME;//	网站（网店）名称
    @JsonProperty(value = "WEBTYPE")
    private String WEBTYPE;//	网站（网店）类型

    @JsonIgnore
    public String getANCHEID() {
        return ANCHEID;
    }

    @JsonIgnore
    public void setANCHEID(String ANCHEID) {
        this.ANCHEID = ANCHEID;
    }

    @JsonIgnore
    public String getDOMAIN() {
        return DOMAIN;
    }

    @JsonIgnore
    public void setDOMAIN(String DOMAIN) {
        this.DOMAIN = DOMAIN;
    }

    @JsonIgnore
    public String getWEBSITNAME() {
        return WEBSITNAME;
    }

    @JsonIgnore
    public void setWEBSITNAME(String WEBSITNAME) {
        this.WEBSITNAME = WEBSITNAME;
    }

    @JsonIgnore
    public String getWEBTYPE() {
        return WEBTYPE;
    }

    @JsonIgnore
    public void setWEBTYPE(String WEBTYPE) {
        this.WEBTYPE = WEBTYPE;
    }

    @Override
    public String toString() {
        return "YEARREPORTWEBSITEINFO{" +
                "ANCHEID='" + ANCHEID + '\'' +
                ", DOMAIN='" + DOMAIN + '\'' +
                ", WEBSITNAME='" + WEBSITNAME + '\'' +
                ", WEBTYPE='" + WEBTYPE + '\'' +
                '}';
    }
}
