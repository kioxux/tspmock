package cn.com.yusys.yusp.dto.server.biz.xdzc0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 17:04
 * @since 2021/6/8 17:04
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "totalQnt")
    private long totalQnt;// 总数
    @JsonProperty(value = "list")
    private   java.util.List<List> list;// list

    public long getTotalQnt() {
        return totalQnt;
    }

    public void setTotalQnt(long totalQnt) {
        this.totalQnt = totalQnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "totalQnt=" + totalQnt +
                ", list=" + list +
                '}';
    }
}
