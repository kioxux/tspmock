package cn.com.yusys.yusp.dto.server.biz.xdxw0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：优企贷共借人、合同信息查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "commonCertNo")
    private String commonCertNo;//共借人证件号

    public String getCommonCertNo() {
        return commonCertNo;
    }

    public void setCommonCertNo(String commonCertNo) {
        this.commonCertNo = commonCertNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "commonCertNo='" + commonCertNo + '\'' +
                '}';
    }
}
