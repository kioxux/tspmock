package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd06;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：获取该笔借据的最新五级分类以及数据日期
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd06RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "classify_level_new")
    private String classify_level_new; //分类级别(新)
    @JsonProperty(value = "data_date")
    private String data_date; //数据日期

    public String getClassify_level_new() {
        return classify_level_new;
    }

    public void setClassify_level_new(String classify_level_new) {
        this.classify_level_new = classify_level_new;
    }

    public String getData_date() {
        return data_date;
    }

    public void setData_date(String data_date) {
        this.data_date = data_date;
    }

    @Override
    public String toString() {
        return "Fbxd06RespDto{" +
                "classify_level_new='" + classify_level_new + '\'' +
                ", data_date='" + data_date + '\'' +
                '}';
    }
}
