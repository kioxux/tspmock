package cn.com.yusys.yusp.dto.server.oca.xdxt0014.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 19:23:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 19:23
 * @since 2021/5/24 19:23
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opttype")
    private String opttype;//字典类型
    @JsonProperty(value = "enname")
    private String enname;//字典项名称

    public String getOpttype() {
        return opttype;
    }

    public void setOpttype(String opttype) {
        this.opttype = opttype;
    }

    public String getEnname() {
        return enname;
    }

    public void setEnname(String enname) {
        this.enname = enname;
    }

    @Override
    public String toString() {
        return "Xdxt0014ReqDto{" +
                "opttype='" + opttype + '\'' +
                "enname='" + enname + '\'' +
                '}';
    }
}
