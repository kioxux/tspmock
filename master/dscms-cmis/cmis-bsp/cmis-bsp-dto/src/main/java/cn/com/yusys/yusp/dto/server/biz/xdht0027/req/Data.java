package cn.com.yusys.yusp.dto.server.biz.xdht0027.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 9:49
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cert_code")
    private String cert_code;//身份证号码

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cert_code='" + cert_code + '\'' +
                '}';
    }
}
