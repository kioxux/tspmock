package cn.com.yusys.yusp.dto.server.biz.xdht0025.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：合同签订/撤销（优享贷和增享贷）
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0025RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.server.biz.xdht0025.resp.Data data;

    public cn.com.yusys.yusp.dto.server.biz.xdht0025.resp.Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "xdht0025RespDto{" +
                "data=" + data +
                '}';
    }
}