package cn.com.yusys.yusp.dto.client.esb.core.ln3020.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款受托支付
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkstzf implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "stzfjine")
    private BigDecimal stzfjine;//受托金额
    @JsonProperty(value = "qudaohao")
    private String qudaohao;//发布系统/渠道
    @JsonProperty(value = "zjzrzhao")
    private String zjzrzhao;//资金转入账号
    @JsonProperty(value = "zjzrzzxh")
    private String zjzrzzxh;//资金转入账号子序号
    @JsonProperty(value = "zjzrzhmc")
    private String zjzrzhmc;//资金转入账号名称
    @JsonProperty(value = "dfzhhzhl")
    private String dfzhhzhl;//对方账号种类
    @JsonProperty(value = "dfzhhkhh")
    private String dfzhhkhh;//对方账号开户行
    @JsonProperty(value = "dfzhkhhm")
    private String dfzhkhhm;//对方账号开户行名
    @JsonProperty(value = "dfzhangh")
    private String dfzhangh;//对方账号
    @JsonProperty(value = "dfzhhzxh")
    private String dfzhhzxh;//对方账号子序号
    @JsonProperty(value = "dfzhhmch")
    private String dfzhhmch;//对方账号名称
    @JsonProperty(value = "djiebhao")
    private String djiebhao;//冻结编号
    @JsonProperty(value = "stzffshi")
    private String stzffshi;//受托支付方式
    @JsonProperty(value = "stzfriqi")
    private String stzfriqi;//受托支付日期
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "stzfclzt")
    private String stzfclzt;//受托支付处理状态

    public BigDecimal getStzfjine() {
        return stzfjine;
    }

    public void setStzfjine(BigDecimal stzfjine) {
        this.stzfjine = stzfjine;
    }

    public String getQudaohao() {
        return qudaohao;
    }

    public void setQudaohao(String qudaohao) {
        this.qudaohao = qudaohao;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public String getZjzrzhmc() {
        return zjzrzhmc;
    }

    public void setZjzrzhmc(String zjzrzhmc) {
        this.zjzrzhmc = zjzrzhmc;
    }

    public String getDfzhhzhl() {
        return dfzhhzhl;
    }

    public void setDfzhhzhl(String dfzhhzhl) {
        this.dfzhhzhl = dfzhhzhl;
    }

    public String getDfzhhkhh() {
        return dfzhhkhh;
    }

    public void setDfzhhkhh(String dfzhhkhh) {
        this.dfzhhkhh = dfzhhkhh;
    }

    public String getDfzhkhhm() {
        return dfzhkhhm;
    }

    public void setDfzhkhhm(String dfzhkhhm) {
        this.dfzhkhhm = dfzhkhhm;
    }

    public String getDfzhangh() {
        return dfzhangh;
    }

    public void setDfzhangh(String dfzhangh) {
        this.dfzhangh = dfzhangh;
    }

    public String getDfzhhzxh() {
        return dfzhhzxh;
    }

    public void setDfzhhzxh(String dfzhhzxh) {
        this.dfzhhzxh = dfzhhzxh;
    }

    public String getDfzhhmch() {
        return dfzhhmch;
    }

    public void setDfzhhmch(String dfzhhmch) {
        this.dfzhhmch = dfzhhmch;
    }

    public String getDjiebhao() {
        return djiebhao;
    }

    public void setDjiebhao(String djiebhao) {
        this.djiebhao = djiebhao;
    }

    public String getStzffshi() {
        return stzffshi;
    }

    public void setStzffshi(String stzffshi) {
        this.stzffshi = stzffshi;
    }

    public String getStzfriqi() {
        return stzfriqi;
    }

    public void setStzfriqi(String stzfriqi) {
        this.stzfriqi = stzfriqi;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getStzfclzt() {
        return stzfclzt;
    }

    public void setStzfclzt(String stzfclzt) {
        this.stzfclzt = stzfclzt;
    }

    @Override
    public String toString() {
        return "Lstdkstzf{" +
                "stzfjine=" + stzfjine +
                ", qudaohao='" + qudaohao + '\'' +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                ", zjzrzhmc='" + zjzrzhmc + '\'' +
                ", dfzhhzhl='" + dfzhhzhl + '\'' +
                ", dfzhhkhh='" + dfzhhkhh + '\'' +
                ", dfzhkhhm='" + dfzhkhhm + '\'' +
                ", dfzhangh='" + dfzhangh + '\'' +
                ", dfzhhzxh='" + dfzhhzxh + '\'' +
                ", dfzhhmch='" + dfzhhmch + '\'' +
                ", djiebhao='" + djiebhao + '\'' +
                ", stzffshi='" + stzffshi + '\'' +
                ", stzfriqi='" + stzfriqi + '\'' +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", stzfclzt='" + stzfclzt + '\'' +
                '}';
    }
}