package cn.com.yusys.yusp.dto.client.esb.core.da3320;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：查询抵债资产信息以及与贷款、费用、出租的关联信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3320RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "dzwzleib")
    private String dzwzleib;//抵债物资类别
    @JsonProperty(value = "cqzmzlei")
    private String cqzmzlei;//产权证明种类
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "dzzcztai")
    private String dzzcztai;//抵债资产状态
    @JsonProperty(value = "daizdzzc")
    private BigDecimal daizdzzc;//待转抵债资产
    @JsonProperty(value = "dcldzzic")
    private BigDecimal dcldzzic;//待处理抵债资产
    @JsonProperty(value = "dbxdzzic")
    private BigDecimal dbxdzzic;//待变现抵债资产
    @JsonProperty(value = "hfeijine")
    private BigDecimal hfeijine;//还费金额
    @JsonProperty(value = "huanbjee")
    private BigDecimal huanbjee;//还本金额
    @JsonProperty(value = "hxijinee")
    private BigDecimal hxijinee;//还息金额
    @JsonProperty(value = "yicldzzc")
    private BigDecimal yicldzzc;//已处置抵债资产
    @JsonProperty(value = "dzzcdanw")
    private String dzzcdanw;//抵债资产单位
    @JsonProperty(value = "dzzcshul")
    private Long dzzcshul;//抵债资产数量
    @JsonProperty(value = "pingjiaz")
    private BigDecimal pingjiaz;//评估价值
    @JsonProperty(value = "dzzcrzjz")
    private BigDecimal dzzcrzjz;//抵债资产入账价值
    @JsonProperty(value = "beizhuuu")
    private String beizhuuu;//备注信息
    @JsonProperty(value = "jitiriqi")
    private String jitiriqi;//计提日期
    @JsonProperty(value = "jitijine")
    private BigDecimal jitijine;//计提金额
    @JsonProperty(value = "jianzzhb")
    private BigDecimal jianzzhb;//减值准备
    @JsonProperty(value = "jianzssh")
    private BigDecimal jianzssh;//减值损失
    @JsonProperty(value = "dzczywsr")
    private BigDecimal dzczywsr;//抵债资产处置营业外收入
    @JsonProperty(value = "dzczywzc")
    private BigDecimal dzczywzc;//抵债资产处置营业外支出
    @JsonProperty(value = "listnm0")
    private java.util.List<Listnm0> listnm0;//费用明细
    @JsonProperty(value = "listnm1")
    private java.util.List<Listnm1> listnm1;//抵债资产与贷款关联信息
    @JsonProperty(value = "lstdzzccz")
    private java.util.List<Lstdzzccz> lstdzzccz;//抵债资产出租信息

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getDzwzleib() {
        return dzwzleib;
    }

    public void setDzwzleib(String dzwzleib) {
        this.dzwzleib = dzwzleib;
    }

    public String getCqzmzlei() {
        return cqzmzlei;
    }

    public void setCqzmzlei(String cqzmzlei) {
        this.cqzmzlei = cqzmzlei;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getDzzcztai() {
        return dzzcztai;
    }

    public void setDzzcztai(String dzzcztai) {
        this.dzzcztai = dzzcztai;
    }

    public BigDecimal getDaizdzzc() {
        return daizdzzc;
    }

    public void setDaizdzzc(BigDecimal daizdzzc) {
        this.daizdzzc = daizdzzc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public BigDecimal getDbxdzzic() {
        return dbxdzzic;
    }

    public void setDbxdzzic(BigDecimal dbxdzzic) {
        this.dbxdzzic = dbxdzzic;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public BigDecimal getHxijinee() {
        return hxijinee;
    }

    public void setHxijinee(BigDecimal hxijinee) {
        this.hxijinee = hxijinee;
    }

    public BigDecimal getYicldzzc() {
        return yicldzzc;
    }

    public void setYicldzzc(BigDecimal yicldzzc) {
        this.yicldzzc = yicldzzc;
    }

    public String getDzzcdanw() {
        return dzzcdanw;
    }

    public void setDzzcdanw(String dzzcdanw) {
        this.dzzcdanw = dzzcdanw;
    }

    public Long getDzzcshul() {
        return dzzcshul;
    }

    public void setDzzcshul(Long dzzcshul) {
        this.dzzcshul = dzzcshul;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDzzcrzjz() {
        return dzzcrzjz;
    }

    public void setDzzcrzjz(BigDecimal dzzcrzjz) {
        this.dzzcrzjz = dzzcrzjz;
    }

    public String getBeizhuuu() {
        return beizhuuu;
    }

    public void setBeizhuuu(String beizhuuu) {
        this.beizhuuu = beizhuuu;
    }

    public String getJitiriqi() {
        return jitiriqi;
    }

    public void setJitiriqi(String jitiriqi) {
        this.jitiriqi = jitiriqi;
    }

    public BigDecimal getJitijine() {
        return jitijine;
    }

    public void setJitijine(BigDecimal jitijine) {
        this.jitijine = jitijine;
    }

    public BigDecimal getJianzzhb() {
        return jianzzhb;
    }

    public void setJianzzhb(BigDecimal jianzzhb) {
        this.jianzzhb = jianzzhb;
    }

    public BigDecimal getJianzssh() {
        return jianzssh;
    }

    public void setJianzssh(BigDecimal jianzssh) {
        this.jianzssh = jianzssh;
    }

    public BigDecimal getDzczywsr() {
        return dzczywsr;
    }

    public void setDzczywsr(BigDecimal dzczywsr) {
        this.dzczywsr = dzczywsr;
    }

    public BigDecimal getDzczywzc() {
        return dzczywzc;
    }

    public void setDzczywzc(BigDecimal dzczywzc) {
        this.dzczywzc = dzczywzc;
    }

    public List<Listnm0> getListnm0() {
        return listnm0;
    }

    public void setListnm0(List<Listnm0> listnm0) {
        this.listnm0 = listnm0;
    }

    public List<Listnm1> getListnm1() {
        return listnm1;
    }

    public void setListnm1(List<Listnm1> listnm1) {
        this.listnm1 = listnm1;
    }

    public List<Lstdzzccz> getLstdzzccz() {
        return lstdzzccz;
    }

    public void setLstdzzccz(List<Lstdzzccz> lstdzzccz) {
        this.lstdzzccz = lstdzzccz;
    }

    @Override
    public String toString() {
        return "Da3320RespDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", dzwzleib='" + dzwzleib + '\'' +
                ", cqzmzlei='" + cqzmzlei + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", yngyjigo='" + yngyjigo + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", dzzcztai='" + dzzcztai + '\'' +
                ", daizdzzc=" + daizdzzc +
                ", dcldzzic=" + dcldzzic +
                ", dbxdzzic=" + dbxdzzic +
                ", hfeijine=" + hfeijine +
                ", huanbjee=" + huanbjee +
                ", hxijinee=" + hxijinee +
                ", yicldzzc=" + yicldzzc +
                ", dzzcdanw='" + dzzcdanw + '\'' +
                ", dzzcshul=" + dzzcshul +
                ", pingjiaz=" + pingjiaz +
                ", dzzcrzjz=" + dzzcrzjz +
                ", beizhuuu='" + beizhuuu + '\'' +
                ", jitiriqi='" + jitiriqi + '\'' +
                ", jitijine=" + jitijine +
                ", jianzzhb=" + jianzzhb +
                ", jianzssh=" + jianzssh +
                ", dzczywsr=" + dzczywsr +
                ", dzczywzc=" + dzczywzc +
                ", listnm0=" + listnm0 +
                ", listnm1=" + listnm1 +
                ", lstdzzccz=" + lstdzzccz +
                '}';
    }
}
