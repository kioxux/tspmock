package cn.com.yusys.yusp.dto.server.biz.xdzc0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/6/3 16:01:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/6/3 16:01
 * @since 2021/6/3 16:01
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "assetNo")
    private String assetNo;//资产编号

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    @Override
    public String toString() {
        return "List{" +
                "assetNo='" + assetNo + '\'' +
                '}';
    }
}
