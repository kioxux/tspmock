package cn.com.yusys.yusp.dto.client.esb.rircp.fbyd36.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：授信列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbyd36ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//处理码
    @JsonProperty(value = "servtp")
    private String servtp;//渠道
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "userid")
    private String userid;//柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//部门号
    @JsonProperty(value = "channel_type")
    private String channel_type;//渠道来源
    @JsonProperty(value = "co_platform")
    private String co_platform;//合作平台
    @JsonProperty(value = "prd_type")
    private String prd_type;//产品类别
    @JsonProperty(value = "prd_code")
    private String prd_code;//产品代码（零售智能风控内部代码）
    @JsonProperty(value = "cust_name")
    private String cust_name;//客户姓名
    @JsonProperty(value = "cert_code")
    private String cert_code;//客户证件号码
    @JsonProperty(value = "manage_status")
    private String manage_status;//处理状态
    @JsonProperty(value = "app_type")
    private String app_type;//申请类型
    @JsonProperty(value = "start_date")
    private String start_date;//申请日期起
    @JsonProperty(value = "end_date")
    private String end_date;//申请日期止
    @JsonProperty(value = "start_no")
    private Integer start_no;//起始记录号
    @JsonProperty(value = "query_num")
    private Integer query_num;//查询记录数

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getManage_status() {
        return manage_status;
    }

    public void setManage_status(String manage_status) {
        this.manage_status = manage_status;
    }

    public String getApp_type() {
        return app_type;
    }

    public void setApp_type(String app_type) {
        this.app_type = app_type;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public Integer getStart_no() {
        return start_no;
    }

    public void setStart_no(Integer start_no) {
        this.start_no = start_no;
    }

    public Integer getQuery_num() {
        return query_num;
    }

    public void setQuery_num(Integer query_num) {
        this.query_num = query_num;
    }

    @Override
    public String toString() {
        return "Fbyd36ReqDto{" +
                "prcscd='" + prcscd + '\'' +
                "servtp='" + servtp + '\'' +
                "servsq='" + servsq + '\'' +
                "userid='" + userid + '\'' +
                "brchno='" + brchno + '\'' +
                "channel_type='" + channel_type + '\'' +
                "co_platform='" + co_platform + '\'' +
                "prd_type='" + prd_type + '\'' +
                "prd_code='" + prd_code + '\'' +
                "cust_name='" + cust_name + '\'' +
                "cert_code='" + cert_code + '\'' +
                "manage_status='" + manage_status + '\'' +
                "app_type='" + app_type + '\'' +
                "start_date='" + start_date + '\'' +
                "end_date='" + end_date + '\'' +
                "start_no='" + start_no + '\'' +
                "query_num='" + query_num + '\'' +
                '}';
    }
}  
