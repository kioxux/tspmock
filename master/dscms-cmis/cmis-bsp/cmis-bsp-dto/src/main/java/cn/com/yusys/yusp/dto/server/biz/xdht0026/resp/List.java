package cn.com.yusys.yusp.dto.server.biz.xdht0026.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：根据zheng获取信贷合同信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cont_no")
    private String cont_no;//借款合同号
    @JsonProperty(value = "cn_cont_no")
    private String cn_cont_no;//中文合同号
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//合同金额（总额度）
    @JsonProperty(value = "avail_amt")
    private BigDecimal avail_amt;//合同可用余额（可用额度）
    @JsonProperty(value = "assure_means_main")
    private String assure_means_main;//担保方式
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同起始日期
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同到期日期
    @JsonProperty(value = "loan_term")
    private String loan_term;//签订期限
    @JsonProperty(value = "cont_type")
    private String cont_type;//合同类型
    @JsonProperty(value = "manager_id")
    private String manager_id;//客户经理
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id;//归属机构
    @JsonProperty(value = "agri_flg")
    private String agri_flg;//是否农户
    @JsonProperty(value = "cus_type")
    private String cus_type;//是否个体工商户
    @JsonProperty(value = "cont_state")
    private String cont_state;//合同状态
    @JsonProperty(value = "biz_type")
    private String biz_type;//业务类型代码
    @JsonProperty(value = "prd_name")
    private String prd_name;//业务类型名称
    @JsonProperty(value = "biz_type_mx")
    private String biz_type_mx;//业务类型代码明细(小贷)
    @JsonProperty(value = "prd_name_mx")
    private String prd_name_mx;//业务类型代码明细(小贷)

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCn_cont_no() {
        return cn_cont_no;
    }

    public void setCn_cont_no(String cn_cont_no) {
        this.cn_cont_no = cn_cont_no;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public BigDecimal getAvail_amt() {
        return avail_amt;
    }

    public void setAvail_amt(BigDecimal avail_amt) {
        this.avail_amt = avail_amt;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(String loan_term) {
        this.loan_term = loan_term;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getAgri_flg() {
        return agri_flg;
    }

    public void setAgri_flg(String agri_flg) {
        this.agri_flg = agri_flg;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    public String getBiz_type() {
        return biz_type;
    }

    public void setBiz_type(String biz_type) {
        this.biz_type = biz_type;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getBiz_type_mx() {
        return biz_type_mx;
    }

    public void setBiz_type_mx(String biz_type_mx) {
        this.biz_type_mx = biz_type_mx;
    }

    public String getPrd_name_mx() {
        return prd_name_mx;
    }

    public void setPrd_name_mx(String prd_name_mx) {
        this.prd_name_mx = prd_name_mx;
    }

    @Override
    public String toString() {
        return "List{" +
                "cus_id='" + cus_id + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", cn_cont_no='" + cn_cont_no + '\'' +
                ", apply_amount=" + apply_amount +
                ", avail_amt=" + avail_amt +
                ", assure_means_main='" + assure_means_main + '\'' +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", loan_term='" + loan_term + '\'' +
                ", cont_type='" + cont_type + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", agri_flg='" + agri_flg + '\'' +
                ", cus_type='" + cus_type + '\'' +
                ", cont_state='" + cont_state + '\'' +
                ", biz_type='" + biz_type + '\'' +
                ", prd_name='" + prd_name + '\'' +
                ", biz_type_mx='" + biz_type_mx + '\'' +
                ", prd_name_mx='" + prd_name_mx + '\'' +
                '}';
    }
}
