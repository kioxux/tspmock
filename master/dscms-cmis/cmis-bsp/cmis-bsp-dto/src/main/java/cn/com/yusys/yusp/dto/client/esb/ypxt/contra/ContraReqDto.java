package cn.com.yusys.yusp.dto.client.esb.ypxt.contra;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求DTO：信贷查询地方征信接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class ContraReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contraListInfo")
    private List<ContraListInfo> contraListInfo;

    public List<ContraListInfo> getContraListInfo() {
        return contraListInfo;
    }

    public void setContraListInfo(List<ContraListInfo> contraListInfo) {
        this.contraListInfo = contraListInfo;
    }

    @Override
    public String toString() {
        return "ContraReqDto{" +
                "contraListInfo=" + contraListInfo +
                '}';
    }
}
