package cn.com.yusys.yusp.dto.client.esb.core.co3200;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author muxiang
 * @version 1.0
 * @since 2021/4/14 16:37
 */
public class LstklnbDkzhzy implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "shfbhzhh")
    private String shfbhzhh; // 是否本行账号
    @JsonProperty(value = "kehuzhao")
    private String kehuzhao; // 客户账号
    @JsonProperty(value = "khzhhzxh")
    private String khzhhzxh; // 客户账号子序号
    @JsonProperty(value = "kehmingc")
    private String kehmingc; // 客户名称
    @JsonProperty(value = "zhjzhyfs")
    private String zhjzhyfs; // 质押方式
    @JsonProperty(value = "zhiyajee")
    private BigDecimal zhiyajee; // 质押金额

    public String getShfbhzhh() {
        return shfbhzhh;
    }

    public void setShfbhzhh(String shfbhzhh) {
        this.shfbhzhh = shfbhzhh;
    }

    public String getKehuzhao() {
        return kehuzhao;
    }

    public void setKehuzhao(String kehuzhao) {
        this.kehuzhao = kehuzhao;
    }

    public String getKhzhhzxh() {
        return khzhhzxh;
    }

    public void setKhzhhzxh(String khzhhzxh) {
        this.khzhhzxh = khzhhzxh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getZhjzhyfs() {
        return zhjzhyfs;
    }

    public void setZhjzhyfs(String zhjzhyfs) {
        this.zhjzhyfs = zhjzhyfs;
    }

    public BigDecimal getZhiyajee() {
        return zhiyajee;
    }

    public void setZhiyajee(BigDecimal zhiyajee) {
        this.zhiyajee = zhiyajee;
    }

    @Override
    public String toString() {
        return "LstklnbDkzhzy{" +
                "shfbhzhh='" + shfbhzhh + '\'' +
                ", kehuzhao='" + kehuzhao + '\'' +
                ", khzhhzxh='" + khzhhzxh + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", zhjzhyfs='" + zhjzhyfs + '\'' +
                ", zhiyajee=" + zhiyajee +
                '}';
    }
}
