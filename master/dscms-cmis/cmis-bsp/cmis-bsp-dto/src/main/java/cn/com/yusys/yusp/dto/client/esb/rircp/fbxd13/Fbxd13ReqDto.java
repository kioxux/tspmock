package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询客户核心编号，客户名称对应的放款和还款信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd13ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "trade_code")
    private String trade_code;//交易码

    public String getTrade_code() {
        return trade_code;
    }

    public void setTrade_code(String trade_code) {
        this.trade_code = trade_code;
    }

    @Override
    public String toString() {
        return "Fbxd13ReqDto{" +
                "trade_code='" + trade_code + '\'' +
                '}';
    }
}  
