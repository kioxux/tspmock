package cn.com.yusys.yusp.dto.server.biz.xdcz0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Data：企业网银省心E付放款
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "entruPayAcctNo")
    private String entruPayAcctNo;//受托支付账户账号
    @JsonProperty(value = "entruPayAcctName")
    private String entruPayAcctName;//受托支付账户户名
    @JsonProperty(value = "entruPayAcctsvcrNo")
    private String entruPayAcctsvcrNo;//受托支付开户行号
    @JsonProperty(value = "entruPayAcctsvcrName")
    private String entruPayAcctsvcrName;//受托支付开户行名
    @JsonProperty(value = "isBankFlag")
    private String isBankFlag;//行内外标识
    @JsonProperty(value = "entruPayAmt")
    private BigDecimal entruPayAmt;//受托支付金额

    public String getEntruPayAcctNo() {
        return entruPayAcctNo;
    }

    public void setEntruPayAcctNo(String entruPayAcctNo) {
        this.entruPayAcctNo = entruPayAcctNo;
    }

    public String getEntruPayAcctName() {
        return entruPayAcctName;
    }

    public void setEntruPayAcctName(String entruPayAcctName) {
        this.entruPayAcctName = entruPayAcctName;
    }

    public String getEntruPayAcctsvcrNo() {
        return entruPayAcctsvcrNo;
    }

    public void setEntruPayAcctsvcrNo(String entruPayAcctsvcrNo) {
        this.entruPayAcctsvcrNo = entruPayAcctsvcrNo;
    }

    public String getEntruPayAcctsvcrName() {
        return entruPayAcctsvcrName;
    }

    public void setEntruPayAcctsvcrName(String entruPayAcctsvcrName) {
        this.entruPayAcctsvcrName = entruPayAcctsvcrName;
    }

    public String getIsBankFlag() {
        return isBankFlag;
    }

    public void setIsBankFlag(String isBankFlag) {
        this.isBankFlag = isBankFlag;
    }

    public BigDecimal getEntruPayAmt() {
        return entruPayAmt;
    }

    public void setEntruPayAmt(BigDecimal entruPayAmt) {
        this.entruPayAmt = entruPayAmt;
    }

    @Override
    public String toString() {
        return "List{" +
                "entruPayAcctNo='" + entruPayAcctNo + '\'' +
                ", entruPayAcctName='" + entruPayAcctName + '\'' +
                ", entruPayAcctsvcrNo='" + entruPayAcctsvcrNo + '\'' +
                ", entruPayAcctsvcrName='" + entruPayAcctsvcrName + '\'' +
                ", isBankFlag='" + isBankFlag + '\'' +
                ", entruPayAmt=" + entruPayAmt +
                '}';
    }
}
