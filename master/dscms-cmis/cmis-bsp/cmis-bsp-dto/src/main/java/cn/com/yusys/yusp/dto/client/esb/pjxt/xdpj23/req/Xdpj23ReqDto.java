package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询批次出账票据信息（新信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj23ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sBatchNo")
    private String sBatchNo;//批次号
    @JsonProperty(value = "turnPageShowNum")
    private String turnPageShowNum;//显示条数
    @JsonProperty(value = "turnPageBeginPos")
    private String turnPageBeginPos;//起始条数
    @JsonProperty(value = "servti")
    private String servti;//交易时间
    @JsonProperty(value = "mac")
    private String mac;//mac地址
    @JsonProperty(value = "ipaddr")
    private String ipaddr;//ip地址
    @JsonProperty(value = "servdt")
    private String servdt;//交易日期

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getsBatchNo() {
        return sBatchNo;
    }

    public void setsBatchNo(String sBatchNo) {
        this.sBatchNo = sBatchNo;
    }

    public String getTurnPageShowNum() {
        return turnPageShowNum;
    }

    public void setTurnPageShowNum(String turnPageShowNum) {
        this.turnPageShowNum = turnPageShowNum;
    }

    public String getTurnPageBeginPos() {
        return turnPageBeginPos;
    }

    public void setTurnPageBeginPos(String turnPageBeginPos) {
        this.turnPageBeginPos = turnPageBeginPos;
    }

    public String getServti() {
        return servti;
    }

    public void setServti(String servti) {
        this.servti = servti;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public String getIpaddr() {
        return ipaddr;
    }

    public void setIpaddr(String ipaddr) {
        this.ipaddr = ipaddr;
    }

    public String getServdt() {
        return servdt;
    }

    public void setServdt(String servdt) {
        this.servdt = servdt;
    }

    @Override
    public String toString() {
        return "Xdpj23ReqDto{" +
                "sBatchNo='" + sBatchNo + '\'' +
                ", turnPageShowNum='" + turnPageShowNum + '\'' +
                ", turnPageBeginPos='" + turnPageBeginPos + '\'' +
                ", servti='" + servti + '\'' +
                ", mac='" + mac + '\'' +
                ", ipaddr='" + ipaddr + '\'' +
                ", servdt='" + servdt + '\'' +
                '}';
    }

}
