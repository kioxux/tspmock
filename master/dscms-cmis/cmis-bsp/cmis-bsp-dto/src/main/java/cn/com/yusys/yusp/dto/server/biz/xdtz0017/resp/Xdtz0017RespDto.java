package cn.com.yusys.yusp.dto.server.biz.xdtz0017.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：票据更换（通知信贷更改票据暂用额度台账）
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdtz0017RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "data")
	private Data data;//成功失败标志

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdtz0017RespDto{" +
				"data=" + data +
				'}';
	}
}
