package cn.com.yusys.yusp.dto.client.http.ocr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/4 20:47
 * @since 2021/6/4 20:47
 */
@JsonPropertyOrder(alphabetic = true)
public class compareResults implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "subjectAName")
    private String subjectAName;//科目A名字
    @JsonProperty(value = "subjectBName")
    private String subjectBName;//科目B名字
    @JsonProperty(value = "compareOperator")
    private Integer compareOperator;//比较符：0：> 1： >=  2：<  3：<=
    @JsonProperty(value = "compareResult")
    private Integer compareResult;//比较结果：0：不满足  1：满足

    public String getSubjectAName() {
        return subjectAName;
    }

    public void setSubjectAName(String subjectAName) {
        this.subjectAName = subjectAName;
    }

    public String getSubjectBName() {
        return subjectBName;
    }

    public void setSubjectBName(String subjectBName) {
        this.subjectBName = subjectBName;
    }

    public Integer getCompareOperator() {
        return compareOperator;
    }

    public void setCompareOperator(Integer compareOperator) {
        this.compareOperator = compareOperator;
    }

    public Integer getCompareResult() {
        return compareResult;
    }

    public void setCompareResult(Integer compareResult) {
        this.compareResult = compareResult;
    }

    @Override
    public String toString() {
        return "compareResults{" +
                "subjectAName='" + subjectAName + '\'' +
                ", subjectBName='" + subjectBName + '\'' +
                ", compareOperator=" + compareOperator +
                ", compareResult=" + compareResult +
                '}';
    }
}
