package cn.com.yusys.yusp.dto.client.esb.irs.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：交易请求信息域:担保合同与合同关联信息
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月14日15:10:55
 */
@JsonPropertyOrder(alphabetic = true)
public class BusinessAssureInfo implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serialno")
    private String serialno; // 流水号
    @JsonProperty(value = "cont_no")
    private String cont_no; // 合同编号
    @JsonProperty(value = "guar_cont_no")
    private String guar_cont_no; // 担保合同流水号

    public String getSerialno() {
        return serialno;
    }

    public void setSerialno(String serialno) {
        this.serialno = serialno;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getGuar_cont_no() {
        return guar_cont_no;
    }

    public void setGuar_cont_no(String guar_cont_no) {
        this.guar_cont_no = guar_cont_no;
    }

    @Override
    public String toString() {
        return "BusinessAssureInfo{" +
                "serialno='" + serialno + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", guar_cont_no='" + guar_cont_no + '\'' +
                '}';
    }
}
