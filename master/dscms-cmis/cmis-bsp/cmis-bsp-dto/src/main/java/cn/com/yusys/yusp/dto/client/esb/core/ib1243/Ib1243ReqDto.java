package cn.com.yusys.yusp.dto.client.esb.core.ib1243;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：通用电子记帐交易（多借多贷-多币种）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ib1243ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "qudaofwm")
    private String qudaofwm;//渠道服务码
    @JsonProperty(value = "xitongbs")
    private String xitongbs;//系统标识号
    @JsonProperty(value = "qianzhrq")
    private String qianzhrq;//前置日期
    @JsonProperty(value = "qianzhls")
    private String qianzhls;//前置流水
    @JsonProperty(value = "laiwzhlx")
    private String laiwzhlx;//来往账类型
    @JsonProperty(value = "fukrzhao")
    private String fukrzhao;//付款人账号
    @JsonProperty(value = "fukrzwmc")
    private String fukrzwmc;//付款人名称
    @JsonProperty(value = "fukhzfho")
    private String fukhzfho;//付款行行号
    @JsonProperty(value = "fukhzwmc")
    private String fukhzwmc;//付款行名称
    @JsonProperty(value = "shkrzhao")
    private String shkrzhao;//收款人账号
    @JsonProperty(value = "shkrzwmc")
    private String shkrzwmc;//收款人名称
    @JsonProperty(value = "shkhzfho")
    private String shkhzfho;//收款行行号
    @JsonProperty(value = "shkhzwmc")
    private String shkhzwmc;//收款行名称
    @JsonProperty(value = "jykonzbz")
    private String jykonzbz;//交易控制标志
    @JsonProperty(value = "piaojulx")
    private String piaojulx;//票据类型
    @JsonProperty(value = "pjuhaoma")
    private String pjuhaoma;//票据号码
    @JsonProperty(value = "pingzhxh")
    private String pingzhxh;//凭证序号
    @JsonProperty(value = "pngzzlei")
    private String pngzzlei;//凭证种类
    @JsonProperty(value = "pingzhma")
    private String pingzhma;//凭证号码
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注信息
    @JsonProperty(value = "yewubhao")
    private String yewubhao;//业务编号
    @JsonProperty(value = "lstAcctIn")
    private List<LstAcctIn> lstAcctIn;//交易账户信息输入
    @JsonProperty(value = "licaibho")
    private String licaibho;//理财编号
    @JsonProperty(value = "jypiciho")
    private String jypiciho;//交易批次号


    public String getQudaofwm() {
        return qudaofwm;
    }

    public void setQudaofwm(String qudaofwm) {
        this.qudaofwm = qudaofwm;
    }

    public String getXitongbs() {
        return xitongbs;
    }

    public void setXitongbs(String xitongbs) {
        this.xitongbs = xitongbs;
    }

    public String getQianzhrq() {
        return qianzhrq;
    }

    public void setQianzhrq(String qianzhrq) {
        this.qianzhrq = qianzhrq;
    }

    public String getQianzhls() {
        return qianzhls;
    }

    public void setQianzhls(String qianzhls) {
        this.qianzhls = qianzhls;
    }

    public String getLaiwzhlx() {
        return laiwzhlx;
    }

    public void setLaiwzhlx(String laiwzhlx) {
        this.laiwzhlx = laiwzhlx;
    }

    public String getFukrzhao() {
        return fukrzhao;
    }

    public void setFukrzhao(String fukrzhao) {
        this.fukrzhao = fukrzhao;
    }

    public String getFukrzwmc() {
        return fukrzwmc;
    }

    public void setFukrzwmc(String fukrzwmc) {
        this.fukrzwmc = fukrzwmc;
    }

    public String getFukhzfho() {
        return fukhzfho;
    }

    public void setFukhzfho(String fukhzfho) {
        this.fukhzfho = fukhzfho;
    }

    public String getFukhzwmc() {
        return fukhzwmc;
    }

    public void setFukhzwmc(String fukhzwmc) {
        this.fukhzwmc = fukhzwmc;
    }

    public String getShkrzhao() {
        return shkrzhao;
    }

    public void setShkrzhao(String shkrzhao) {
        this.shkrzhao = shkrzhao;
    }

    public String getShkrzwmc() {
        return shkrzwmc;
    }

    public void setShkrzwmc(String shkrzwmc) {
        this.shkrzwmc = shkrzwmc;
    }

    public String getShkhzfho() {
        return shkhzfho;
    }

    public void setShkhzfho(String shkhzfho) {
        this.shkhzfho = shkhzfho;
    }

    public String getShkhzwmc() {
        return shkhzwmc;
    }

    public void setShkhzwmc(String shkhzwmc) {
        this.shkhzwmc = shkhzwmc;
    }

    public String getJykonzbz() {
        return jykonzbz;
    }

    public void setJykonzbz(String jykonzbz) {
        this.jykonzbz = jykonzbz;
    }

    public String getPiaojulx() {
        return piaojulx;
    }

    public void setPiaojulx(String piaojulx) {
        this.piaojulx = piaojulx;
    }

    public String getPjuhaoma() {
        return pjuhaoma;
    }

    public void setPjuhaoma(String pjuhaoma) {
        this.pjuhaoma = pjuhaoma;
    }

    public String getPingzhxh() {
        return pingzhxh;
    }

    public void setPingzhxh(String pingzhxh) {
        this.pingzhxh = pingzhxh;
    }

    public String getPngzzlei() {
        return pngzzlei;
    }

    public void setPngzzlei(String pngzzlei) {
        this.pngzzlei = pngzzlei;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getYewubhao() {
        return yewubhao;
    }

    public void setYewubhao(String yewubhao) {
        this.yewubhao = yewubhao;
    }

    public List<LstAcctIn> getLstAcctIn() {
        return lstAcctIn;
    }

    public void setLstAcctIn(List<LstAcctIn> lstAcctIn) {
        this.lstAcctIn = lstAcctIn;
    }

    public String getLicaibho() {
        return licaibho;
    }

    public void setLicaibho(String licaibho) {
        this.licaibho = licaibho;
    }

    public String getJypiciho() {
        return jypiciho;
    }

    public void setJypiciho(String jypiciho) {
        this.jypiciho = jypiciho;
    }

    @Override
    public String toString() {
        return "Ib1243ReqDto{" +
                "qudaofwm='" + qudaofwm + '\'' +
                "xitongbs='" + xitongbs + '\'' +
                "qianzhrq='" + qianzhrq + '\'' +
                "qianzhls='" + qianzhls + '\'' +
                "laiwzhlx='" + laiwzhlx + '\'' +
                "fukrzhao='" + fukrzhao + '\'' +
                "fukrzwmc='" + fukrzwmc + '\'' +
                "fukhzfho='" + fukhzfho + '\'' +
                "fukhzwmc='" + fukhzwmc + '\'' +
                "shkrzhao='" + shkrzhao + '\'' +
                "shkrzwmc='" + shkrzwmc + '\'' +
                "shkhzfho='" + shkhzfho + '\'' +
                "shkhzwmc='" + shkhzwmc + '\'' +
                "jykonzbz='" + jykonzbz + '\'' +
                "piaojulx='" + piaojulx + '\'' +
                "pjuhaoma='" + pjuhaoma + '\'' +
                "pingzhxh='" + pingzhxh + '\'' +
                "pngzzlei='" + pngzzlei + '\'' +
                "pingzhma='" + pingzhma + '\'' +
                "beizhuxx='" + beizhuxx + '\'' +
                "yewubhao='" + yewubhao + '\'' +
                "lstAcctIn='" + lstAcctIn + '\'' +
                "licaibho='" + licaibho + '\'' +
                "jypiciho='" + jypiciho + '\'' +
                '}';
    }
}  
