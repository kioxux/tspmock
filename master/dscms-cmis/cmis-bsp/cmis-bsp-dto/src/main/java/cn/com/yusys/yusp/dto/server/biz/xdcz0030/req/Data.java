package cn.com.yusys.yusp.dto.server.biz.xdcz0030.req;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/16 17:10
 * @since 2021/7/16 17:10
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sysNo")
    private String sysNo;//系统编号
    @JsonProperty(value = "instuCde")
    private String instuCde;//金融机构代码
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "bizNo")
    private String bizNo;//合同编号
    @JsonProperty(value = "dealBizList")
    private java.util.List<cn.com.yusys.yusp.dto.server.biz.xdcz0030.req.DealBizList> dealBizList;
    @JsonProperty(value = "occRelList")
    private java.util.List<cn.com.yusys.yusp.dto.server.biz.xdcz0030.req.OccRelList> occRelList;

    public String getSysNo() {
        return sysNo;
    }

    public void setSysNo(String sysNo) {
        this.sysNo = sysNo;
    }

    public String getInstuCde() {
        return instuCde;
    }

    public void setInstuCde(String instuCde) {
        this.instuCde = instuCde;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getBizNo() {
        return bizNo;
    }

    public void setBizNo(String bizNo) {
        this.bizNo = bizNo;
    }

    public List<DealBizList> getDealBizList() {
        return dealBizList;
    }

    public void setDealBizList(List<DealBizList> dealBizList) {
        this.dealBizList = dealBizList;
    }

    public List<OccRelList> getOccRelList() {
        return occRelList;
    }

    public void setOccRelList(List<OccRelList> occRelList) {
        this.occRelList = occRelList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "sysNo='" + sysNo + '\'' +
                ", instuCde='" + instuCde + '\'' +
                ", serno='" + serno + '\'' +
                ", bizNo='" + bizNo + '\'' +
                ", dealBizList=" + dealBizList +
                ", occRelList=" + occRelList +
                '}';
    }
}
