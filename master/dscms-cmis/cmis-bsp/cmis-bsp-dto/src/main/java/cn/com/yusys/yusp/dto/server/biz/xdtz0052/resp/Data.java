package cn.com.yusys.yusp.dto.server.biz.xdtz0052.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/22 9:51:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/22 9:51
 * @since 2021/5/22 9:51
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "repayThisBillTimes")
    private String repayThisBillTimes;//行内还款（利息、本金）该笔借据次数

    public String getRepayThisBillTimes() {
        return repayThisBillTimes;
    }

    public void setRepayThisBillTimes(String repayThisBillTimes) {
        this.repayThisBillTimes = repayThisBillTimes;
    }

    @Override
    public String toString() {
        return "Xdtz0052RespDto{" +
                "repayThisBillTimes='" + repayThisBillTimes + '\'' +
                '}';
    }

}
