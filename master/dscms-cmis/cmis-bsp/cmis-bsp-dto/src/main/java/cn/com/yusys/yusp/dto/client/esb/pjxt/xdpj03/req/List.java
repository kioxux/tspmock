package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：票据承兑签发审批请求
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "accountType")
    private String accountType;//钞汇标志
    @JsonProperty(value = "currency")
    private String currency;//币种
    @JsonProperty(value = "interestMode")
    private String interestMode;//计结息方式
    @JsonProperty(value = "acctAmt")
    private String acctAmt;//金额
    @JsonProperty(value = "firstAcctSeq")
    private String firstAcctSeq;//母户
    @JsonProperty(value = "twoAcctSeq")
    private String twoAcctSeq;//二级子账户
    @JsonProperty(value = "twoAcctName")
    private String twoAcctName;//二级账户名称
    @JsonProperty(value = "threeAcctNo")
    private String threeAcctNo;//保证金账户
    @JsonProperty(value = "settleAcctNo")
    private String settleAcctNo;//结算账户
    @JsonProperty(value = "settleAcctSeq")
    private String settleAcctSeq;//结算账号子序号
    @JsonProperty(value = "settleAcctName")
    private String settleAcctName;//结算账户名
    @JsonProperty(value = "settleBankCode")
    private String settleBankCode;//结算开户行行号
    @JsonProperty(value = "clearAcctSeq")
    private String clearAcctSeq;//待清算子序号
    @JsonProperty(value = "clearAcctName")
    private String clearAcctName;//待清算账号名
    @JsonProperty(value = "clearBankCode")
    private String clearBankCode;//待清算开户行行号
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInterestMode() {
        return interestMode;
    }

    public void setInterestMode(String interestMode) {
        this.interestMode = interestMode;
    }

    public String getAcctAmt() {
        return acctAmt;
    }

    public void setAcctAmt(String acctAmt) {
        this.acctAmt = acctAmt;
    }

    public String getFirstAcctSeq() {
        return firstAcctSeq;
    }

    public void setFirstAcctSeq(String firstAcctSeq) {
        this.firstAcctSeq = firstAcctSeq;
    }

    public String getTwoAcctSeq() {
        return twoAcctSeq;
    }

    public void setTwoAcctSeq(String twoAcctSeq) {
        this.twoAcctSeq = twoAcctSeq;
    }

    public String getTwoAcctName() {
        return twoAcctName;
    }

    public void setTwoAcctName(String twoAcctName) {
        this.twoAcctName = twoAcctName;
    }

    public String getThreeAcctNo() {
        return threeAcctNo;
    }

    public void setThreeAcctNo(String threeAcctNo) {
        this.threeAcctNo = threeAcctNo;
    }

    public String getSettleAcctNo() {
        return settleAcctNo;
    }

    public void setSettleAcctNo(String settleAcctNo) {
        this.settleAcctNo = settleAcctNo;
    }

    public String getSettleAcctSeq() {
        return settleAcctSeq;
    }

    public void setSettleAcctSeq(String settleAcctSeq) {
        this.settleAcctSeq = settleAcctSeq;
    }

    public String getSettleAcctName() {
        return settleAcctName;
    }

    public void setSettleAcctName(String settleAcctName) {
        this.settleAcctName = settleAcctName;
    }

    public String getSettleBankCode() {
        return settleBankCode;
    }

    public void setSettleBankCode(String settleBankCode) {
        this.settleBankCode = settleBankCode;
    }

    public String getClearAcctSeq() {
        return clearAcctSeq;
    }

    public void setClearAcctSeq(String clearAcctSeq) {
        this.clearAcctSeq = clearAcctSeq;
    }

    public String getClearAcctName() {
        return clearAcctName;
    }

    public void setClearAcctName(String clearAcctName) {
        this.clearAcctName = clearAcctName;
    }

    public String getClearBankCode() {
        return clearBankCode;
    }

    public void setClearBankCode(String clearBankCode) {
        this.clearBankCode = clearBankCode;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    @Override
    public String toString() {
        return "List{" +
                "accountType='" + accountType + '\'' +
                ", currency='" + currency + '\'' +
                ", interestMode='" + interestMode + '\'' +
                ", acctAmt='" + acctAmt + '\'' +
                ", firstAcctSeq='" + firstAcctSeq + '\'' +
                ", twoAcctSeq='" + twoAcctSeq + '\'' +
                ", twoAcctName='" + twoAcctName + '\'' +
                ", threeAcctNo='" + threeAcctNo + '\'' +
                ", settleAcctNo='" + settleAcctNo + '\'' +
                ", settleAcctSeq='" + settleAcctSeq + '\'' +
                ", settleAcctName='" + settleAcctName + '\'' +
                ", settleBankCode='" + settleBankCode + '\'' +
                ", clearAcctSeq='" + clearAcctSeq + '\'' +
                ", clearAcctName='" + clearAcctName + '\'' +
                ", clearBankCode='" + clearBankCode + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                '}';
    }
}
