package cn.com.yusys.yusp.dto.client.http.outerdata.slno01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：双录流水号
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Slno01ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarid")
    private String guarid;//抵押物编号
    @JsonProperty(value = "yxlsno")
    private String yxlsno;//影像主键流水号
    @JsonProperty(value = "jytype")
    private String jytype;//交易类型

    public String getGuarid() {
        return guarid;
    }

    public void setGuarid(String guarid) {
        this.guarid = guarid;
    }

    public String getYxlsno() {
        return yxlsno;
    }

    public void setYxlsno(String yxlsno) {
        this.yxlsno = yxlsno;
    }

    public String getJytype() {
        return jytype;
    }

    public void setJytype(String jytype) {
        this.jytype = jytype;
    }

    @Override
    public String toString() {
        return "Slno01ReqDto{" +
                "guarid='" + guarid + '\'' +
                ", yxlsno='" + yxlsno + '\'' +
                ", jytype='" + jytype + '\'' +
                '}';
    }
}
