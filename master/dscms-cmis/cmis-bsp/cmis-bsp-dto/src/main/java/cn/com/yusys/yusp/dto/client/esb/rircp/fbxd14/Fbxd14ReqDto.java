package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd14;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询还款（合约）明细历史表（利翃）中的全量借据一览
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd14ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "trade_code")
    private String trade_code;//交易码
    @JsonProperty(value = "brchno")
    private String brchno;
    @JsonProperty(value = "contract_no")
    private String contract_no;

    public String getTrade_code() {
        return trade_code;
    }

    public void setTrade_code(String trade_code) {
        this.trade_code = trade_code;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    @Override
    public String toString() {
        return "Fbxd14ReqDto{" +
                "trade_code='" + trade_code + '\'' +
                ", brchno='" + brchno + '\'' +
                ", contract_no='" + contract_no + '\'' +
                '}';
    }
}
