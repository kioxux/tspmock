package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp04;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：自动审批调查报告
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp04RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
}
