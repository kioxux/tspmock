package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查找指定数据日期的放款合约明细记录历史表（利翃）一览
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd04RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.rircp.fbxd04.List> list;//start

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Fbxd04RespDto{" +
                "list=" + list +
                '}';
    }
}
