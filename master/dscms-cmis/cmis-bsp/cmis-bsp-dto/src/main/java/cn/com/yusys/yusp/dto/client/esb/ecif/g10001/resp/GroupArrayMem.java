package cn.com.yusys.yusp.dto.client.esb.ecif.g10001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/6 11:24
 * @since 2021/7/6 11:24
 */
@JsonPropertyOrder(alphabetic = true)
public class GroupArrayMem implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "gpmbna")
    private String gpmbna;//群组成员客户号
    @JsonProperty(value = "gpmbmc")
    private String gpmbmc;//群组成员客户名称
    @JsonProperty(value = "gpmbxt")
    private String gpmbxt;//客户类型
    @JsonProperty(value = "gpmbgs")
    private String gpmbgs;//成员归属
    @JsonProperty(value = "membtp")
    private String membtp;//成员类型

    public String getGpmbna() {
        return gpmbna;
    }

    public void setGpmbna(String gpmbna) {
        this.gpmbna = gpmbna;
    }

    public String getGpmbmc() {
        return gpmbmc;
    }

    public void setGpmbmc(String gpmbmc) {
        this.gpmbmc = gpmbmc;
    }

    public String getGpmbxt() {
        return gpmbxt;
    }

    public void setGpmbxt(String gpmbxt) {
        this.gpmbxt = gpmbxt;
    }

    public String getGpmbgs() {
        return gpmbgs;
    }

    public void setGpmbgs(String gpmbgs) {
        this.gpmbgs = gpmbgs;
    }

    public String getMembtp() {
        return membtp;
    }

    public void setMembtp(String membtp) {
        this.membtp = membtp;
    }

    @Override
    public String toString() {
        return "GroupArrayMem{" +
                "gpmbna='" + gpmbna + '\'' +
                ", gpmbmc='" + gpmbmc + '\'' +
                ", gpmbxt='" + gpmbxt + '\'' +
                ", gpmbgs='" + gpmbgs + '\'' +
                ", membtp='" + membtp + '\'' +
                '}';
    }
}
