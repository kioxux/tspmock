package cn.com.yusys.yusp.dto.server.cus.xdkh0008.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：集团关联信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "grpNo")
    private String grpNo;//集团编号
    @JsonProperty(value = "grpName")
    private String grpName;//集团名称
    @JsonProperty(value = "groupMapList")
    private List<GroupMapList> groupMapList;

    public String getGrpNo() {
        return grpNo;
    }

    public void setGrpNo(String grpNo) {
        this.grpNo = grpNo;
    }

    public String getGrpName() {
        return grpName;
    }

    public void setGrpName(String grpName) {
        this.grpName = grpName;
    }

    public List<GroupMapList> getGroupMapList() {
        return groupMapList;
    }

    public void setGroupMapList(List<GroupMapList> groupMapList) {
        this.groupMapList = groupMapList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "grpNo='" + grpNo + '\'' +
                ", grpName='" + grpName + '\'' +
                ", groupMapList=" + groupMapList +
                '}';
    }
}
