package cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：大额往账列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class HvpylsRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "icount")
    private Integer icount;//总笔数
    @JsonProperty(value = "dcount")
    private BigDecimal dcount;//总金额
    @JsonProperty(value = "listnm")
    private Integer listnm;//当前页面总笔数
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.resp.List> list;

    public Integer getIcount() {
        return icount;
    }

    public void setIcount(Integer icount) {
        this.icount = icount;
    }

    public BigDecimal getDcount() {
        return dcount;
    }

    public void setDcount(BigDecimal dcount) {
        this.dcount = dcount;
    }

    public Integer getListnm() {
        return listnm;
    }

    public void setListnm(Integer listnm) {
        this.listnm = listnm;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "HvpylsRespDto{" +
                "icount=" + icount +
                ", dcount=" + dcount +
                ", listnm=" + listnm +
                ", list=" + list +
                '}';
    }
}
