package cn.com.yusys.yusp.dto.server.biz.xddh0013.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询优抵贷抵质押品信息
 * @Author zhangpeng
 * @Date 2021/4/25 14:17
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryDate")
    private String queryDate;//查询日期

    @JsonProperty(value = "startPageNum")
    private String startPageNum;//起始页数

    @JsonProperty(value = "pageSize")
    private String pageSize;//分页大小

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getQueryDate() {
        return queryDate;
    }

    public void setQueryDate(String queryDate) {
        this.queryDate = queryDate;
    }

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Data{" +
                "queryDate='" + queryDate + '\'' +
                ", startPageNum='" + startPageNum + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
