package cn.com.yusys.yusp.dto.client.esb.dxpt.senddx;


import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：短信/微信发送批量接口
 *
 * @author hjk
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SenddxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}
