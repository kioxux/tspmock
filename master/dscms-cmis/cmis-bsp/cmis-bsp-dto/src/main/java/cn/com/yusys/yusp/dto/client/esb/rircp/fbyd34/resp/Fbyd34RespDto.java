package cn.com.yusys.yusp.dto.client.esb.rircp.fbyd34.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：支用模型校验
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbyd34RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
}
