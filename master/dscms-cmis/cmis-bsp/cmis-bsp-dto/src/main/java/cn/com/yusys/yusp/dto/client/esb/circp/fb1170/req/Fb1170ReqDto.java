package cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：最高额借款合同信息推送
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1170ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "cont_no_cn")
    private String cont_no_cn;//中文合同编号
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "cont_type")
    private String cont_type;//合同类型
    @JsonProperty(value = "assure_means_main")
    private String assure_means_main;//担保方式
    @JsonProperty(value = "cur_type")
    private String cur_type;//币种
    @JsonProperty(value = "apply_amount")
    private String apply_amount;//合同金额
    @JsonProperty(value = "apply_amount_cny")
    private String apply_amount_cny;//折算人名币金额
    @JsonProperty(value = "term_time_type")
    private String term_time_type;//贷款期限类型
    @JsonProperty(value = "loan_term")
    private String loan_term;//贷款期限
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//合同起始日
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//合同到期日
    @JsonProperty(value = "limit_acc_no")
    private String limit_acc_no;//授信台帐编号
    @JsonProperty(value = "limit_ma_no")
    private String limit_ma_no;//授信协议编号
    @JsonProperty(value = "product")
    private String product;//公司部特色产品
    @JsonProperty(value = "cont_state")
    private String cont_state;//合同状态
    @JsonProperty(value = "qd_date")
    private String qd_date;//合同签订时间
    @JsonProperty(value = "submit_time")
    private String submit_time;//合同提交时间戳
    @JsonProperty(value = "manager_id")
    private String manager_id;//管户经理号
    @JsonProperty(value = "manager_name")
    private String manager_name;//管户经理名称
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id;//管户机构
    @JsonProperty(value = "sxpf_amt")
    private BigDecimal sxpf_amt;//授信批复额度
    @JsonProperty(value = "pre_app_no")
    private String pre_app_no;//预授信流水号
    @JsonProperty(value = "dblist")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Dblist> dblist;
    @JsonProperty(value = "dylist")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.circp.fb1170.req.Dylist> dylist;

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getCont_no_cn() {
        return cont_no_cn;
    }

    public void setCont_no_cn(String cont_no_cn) {
        this.cont_no_cn = cont_no_cn;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getCont_type() {
        return cont_type;
    }

    public void setCont_type(String cont_type) {
        this.cont_type = cont_type;
    }

    public String getAssure_means_main() {
        return assure_means_main;
    }

    public void setAssure_means_main(String assure_means_main) {
        this.assure_means_main = assure_means_main;
    }

    public String getCur_type() {
        return cur_type;
    }

    public void setCur_type(String cur_type) {
        this.cur_type = cur_type;
    }

    public String getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(String apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getApply_amount_cny() {
        return apply_amount_cny;
    }

    public void setApply_amount_cny(String apply_amount_cny) {
        this.apply_amount_cny = apply_amount_cny;
    }

    public String getTerm_time_type() {
        return term_time_type;
    }

    public void setTerm_time_type(String term_time_type) {
        this.term_time_type = term_time_type;
    }

    public String getLoan_term() {
        return loan_term;
    }

    public void setLoan_term(String loan_term) {
        this.loan_term = loan_term;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getLimit_acc_no() {
        return limit_acc_no;
    }

    public void setLimit_acc_no(String limit_acc_no) {
        this.limit_acc_no = limit_acc_no;
    }

    public String getLimit_ma_no() {
        return limit_ma_no;
    }

    public void setLimit_ma_no(String limit_ma_no) {
        this.limit_ma_no = limit_ma_no;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getCont_state() {
        return cont_state;
    }

    public void setCont_state(String cont_state) {
        this.cont_state = cont_state;
    }

    public String getQd_date() {
        return qd_date;
    }

    public void setQd_date(String qd_date) {
        this.qd_date = qd_date;
    }

    public String getSubmit_time() {
        return submit_time;
    }

    public void setSubmit_time(String submit_time) {
        this.submit_time = submit_time;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public BigDecimal getSxpf_amt() {
        return sxpf_amt;
    }

    public void setSxpf_amt(BigDecimal sxpf_amt) {
        this.sxpf_amt = sxpf_amt;
    }

    public String getPre_app_no() {
        return pre_app_no;
    }

    public void setPre_app_no(String pre_app_no) {
        this.pre_app_no = pre_app_no;
    }

    public List<Dblist> getDblist() {
        return dblist;
    }

    public void setDblist(List<Dblist> dblist) {
        this.dblist = dblist;
    }

    public List<Dylist> getDylist() {
        return dylist;
    }

    public void setDylist(List<Dylist> dylist) {
        this.dylist = dylist;
    }

    @Override
    public String toString() {
        return "Fb1170ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", cont_no='" + cont_no + '\'' +
                ", cont_no_cn='" + cont_no_cn + '\'' +
                ", cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", cont_type='" + cont_type + '\'' +
                ", assure_means_main='" + assure_means_main + '\'' +
                ", cur_type='" + cur_type + '\'' +
                ", apply_amount='" + apply_amount + '\'' +
                ", apply_amount_cny='" + apply_amount_cny + '\'' +
                ", term_time_type='" + term_time_type + '\'' +
                ", loan_term='" + loan_term + '\'' +
                ", loan_start_date='" + loan_start_date + '\'' +
                ", loan_end_date='" + loan_end_date + '\'' +
                ", limit_acc_no='" + limit_acc_no + '\'' +
                ", limit_ma_no='" + limit_ma_no + '\'' +
                ", product='" + product + '\'' +
                ", cont_state='" + cont_state + '\'' +
                ", qd_date='" + qd_date + '\'' +
                ", submit_time='" + submit_time + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", manager_br_id='" + manager_br_id + '\'' +
                ", sxpf_amt=" + sxpf_amt +
                ", pre_app_no='" + pre_app_no + '\'' +
                ", dblist=" + dblist +
                ", dylist=" + dylist +
                '}';
    }
}
