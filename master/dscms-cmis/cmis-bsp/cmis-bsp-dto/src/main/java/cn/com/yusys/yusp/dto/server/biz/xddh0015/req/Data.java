package cn.com.yusys.yusp.dto.server.biz.xddh0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 16:39:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 16:39
 * @since 2021/5/22 16:39
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号
    @JsonProperty(value = "irAdjustType")
    private String irAdjustType;//利率调整方式
    @JsonProperty(value = "LPRInterzone")
    private String LPRInterzone;//lpr区间
    @JsonProperty(value = "irFloatNum")
    private BigDecimal irFloatNum;//浮动点数
    @JsonProperty(value = "level")
    private String level;//层级
    @JsonProperty(value = "debit")
    private String debit;//借款人
    @JsonProperty(value = "debitCertNo")
    private String debitCertNo;//借款人身份证号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "LPRRate")
    private BigDecimal LPRRate;//lpr利率
    @JsonProperty(value = "addDeclFlag")
    private String addDeclFlag;//加减标志
    @JsonProperty(value = "isHouseLoan")
    private String isHouseLoan;//是否为房贷
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行利率年

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getIrAdjustType() {
        return irAdjustType;
    }

    public void setIrAdjustType(String irAdjustType) {
        this.irAdjustType = irAdjustType;
    }

    public String getLPRInterzone()

    {
        return LPRInterzone;
    }

    public void setLPRInterzone(
            String LPRInterzone )

    {
        this.LPRInterzone = LPRInterzone;
    }

    public BigDecimal getIrFloatNum() {
        return irFloatNum;
    }

    public void setIrFloatNum(BigDecimal irFloatNum) {
        this.irFloatNum = irFloatNum;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getDebitCertNo() {
        return debitCertNo;
    }

    public void setDebitCertNo(String debitCertNo) {
        this.debitCertNo = debitCertNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public BigDecimal getLPRRate()

    {
        return LPRRate;
    }

    public void setLPRRate(
            BigDecimal LPRRate )

    {
        this.LPRRate = LPRRate;
    }

    public String getAddDeclFlag() {
        return addDeclFlag;
    }

    public void setAddDeclFlag(String addDeclFlag) {
        this.addDeclFlag = addDeclFlag;
    }

    public String getIsHouseLoan() {
        return isHouseLoan;
    }

    public void setIsHouseLoan(String isHouseLoan) {
        this.isHouseLoan = isHouseLoan;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    @Override
    public String toString() {
        return "Xddh0015ReqDto{" +
                "certNo='" + certNo + '\'' +
                "billNo='" + billNo + '\'' +
                "irAdjustType='" + irAdjustType + '\'' +
                "LPRInterzone='" + LPRInterzone + '\'' +
                "irFloatNum='" + irFloatNum + '\'' +
                "level='" + level + '\'' +
                "debit='" + debit + '\'' +
                "debitCertNo='" + debitCertNo + '\'' +
                "contNo='" + contNo + '\'' +
                "LPRRate='" + LPRRate + '\'' +
                "addDeclFlag='" + addDeclFlag + '\'' +
                "isHouseLoan='" + isHouseLoan + '\'' +
                "realityIrY='" + realityIrY + '\'' +
                '}';
    }
}
