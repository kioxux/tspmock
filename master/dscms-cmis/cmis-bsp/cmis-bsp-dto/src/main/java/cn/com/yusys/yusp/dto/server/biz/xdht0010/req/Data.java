package cn.com.yusys.yusp.dto.server.biz.xdht0010.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "flag")
    private String flag;//标志
    @JsonProperty(value = "cusId")
    private String cusId;//客户号

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "flag='" + flag + '\'' +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
