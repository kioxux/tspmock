package cn.com.yusys.yusp.dto.client.esb.core.ln3246;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 贷款还款计划明细查询
 *
 * @author lihh
 * @version 1.0
 * @since 2021/4/21 14:37
 */
@JsonPropertyOrder(alphabetic = true)
public class LstLnDkhkmx implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh; // 贷款借据号 BaseType.U_JIEJUHAO (60)
    @JsonProperty(value = "benqqish")
    private Integer benqqish; // 本期期数 BaseType.U_CHANZXLX
    @JsonProperty(value = "benqizqs")
    private Integer benqizqs; // 本期子期数 BaseType.U_CHANZXLX
    @JsonProperty(value = "qishriqi")
    private String qishriqi; // 起始日期 BaseType.U_BZRIQILX
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi; // 终止日期 BaseType.U_BZRIQILX
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze; // 每期还款总额 BaseType.U_JIAOYIJE
    @JsonProperty(value = "hkzongee")
    private BigDecimal hkzongee; // 还款总额 BaseType.U_ZHANGHYE
    @JsonProperty(value = "chushibj")
    private BigDecimal chushibj; // 初始本金 BaseType.U_ZHANGHYE
    @JsonProperty(value = "benjinfs")
    private BigDecimal benjinfs; // 本金发生额 BaseType.U_ZHANGHYE
    @JsonProperty(value = "chushilx")
    private BigDecimal chushilx; // 初始利息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "ghysyjlx")
    private BigDecimal ghysyjlx; // 归还应收应计利息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi; // 应收欠息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "ghynshqx")
    private BigDecimal ghynshqx; // 归还应收欠息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi; // 应收罚息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "ghynshfx")
    private BigDecimal ghynshfx; // 归还应收罚息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi; // 催收欠息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "ghcushqx")
    private BigDecimal ghcushqx; // 归还催收欠息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi; // 催收罚息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "ghcushfx")
    private BigDecimal ghcushfx; // 归还催收罚息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii; // 复息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "ghfxfuxi")
    private BigDecimal ghfxfuxi; // 归还复息 BaseType.U_ZHANGHYE
    @JsonProperty(value = "benqizht")
    private String benqizht; // 本期状态 BaseEnumType.E_BENQIZHT
    @JsonProperty(value = "zhanghye")
    private BigDecimal zhanghye; // 账户余额 BaseType.U_ZHANGHYE
    @JsonProperty(value = "huankriq")
    private String huankriq; // 还款日期 BaseType.U_BZRIQILX

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getBenqqish() {
        return benqqish;
    }

    public void setBenqqish(Integer benqqish) {
        this.benqqish = benqqish;
    }

    public Integer getBenqizqs() {
        return benqizqs;
    }

    public void setBenqizqs(Integer benqizqs) {
        this.benqizqs = benqizqs;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getHkzongee() {
        return hkzongee;
    }

    public void setHkzongee(BigDecimal hkzongee) {
        this.hkzongee = hkzongee;
    }

    public BigDecimal getChushibj() {
        return chushibj;
    }

    public void setChushibj(BigDecimal chushibj) {
        this.chushibj = chushibj;
    }

    public BigDecimal getBenjinfs() {
        return benjinfs;
    }

    public void setBenjinfs(BigDecimal benjinfs) {
        this.benjinfs = benjinfs;
    }

    public BigDecimal getChushilx() {
        return chushilx;
    }

    public void setChushilx(BigDecimal chushilx) {
        this.chushilx = chushilx;
    }

    public BigDecimal getGhysyjlx() {
        return ghysyjlx;
    }

    public void setGhysyjlx(BigDecimal ghysyjlx) {
        this.ghysyjlx = ghysyjlx;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getGhynshqx() {
        return ghynshqx;
    }

    public void setGhynshqx(BigDecimal ghynshqx) {
        this.ghynshqx = ghynshqx;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getGhynshfx() {
        return ghynshfx;
    }

    public void setGhynshfx(BigDecimal ghynshfx) {
        this.ghynshfx = ghynshfx;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getGhcushqx() {
        return ghcushqx;
    }

    public void setGhcushqx(BigDecimal ghcushqx) {
        this.ghcushqx = ghcushqx;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getGhcushfx() {
        return ghcushfx;
    }

    public void setGhcushfx(BigDecimal ghcushfx) {
        this.ghcushfx = ghcushfx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getGhfxfuxi() {
        return ghfxfuxi;
    }

    public void setGhfxfuxi(BigDecimal ghfxfuxi) {
        this.ghfxfuxi = ghfxfuxi;
    }

    public String getBenqizht() {
        return benqizht;
    }

    public void setBenqizht(String benqizht) {
        this.benqizht = benqizht;
    }

    public BigDecimal getZhanghye() {
        return zhanghye;
    }

    public void setZhanghye(BigDecimal zhanghye) {
        this.zhanghye = zhanghye;
    }

    public String getHuankriq() {
        return huankriq;
    }

    public void setHuankriq(String huankriq) {
        this.huankriq = huankriq;
    }

    @Override
    public String toString() {
        return "LstLnDkhkmx{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", benqqish=" + benqqish +
                ", benqizqs=" + benqizqs +
                ", qishriqi='" + qishriqi + '\'' +
                ", zhzhriqi='" + zhzhriqi + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", hkzongee=" + hkzongee +
                ", chushibj=" + chushibj +
                ", benjinfs=" + benjinfs +
                ", chushilx=" + chushilx +
                ", ghysyjlx=" + ghysyjlx +
                ", ysqianxi=" + ysqianxi +
                ", ghynshqx=" + ghynshqx +
                ", yshofaxi=" + yshofaxi +
                ", ghynshfx=" + ghynshfx +
                ", csqianxi=" + csqianxi +
                ", ghcushqx=" + ghcushqx +
                ", cshofaxi=" + cshofaxi +
                ", ghcushfx=" + ghcushfx +
                ", fuxiiiii=" + fuxiiiii +
                ", ghfxfuxi=" + ghfxfuxi +
                ", benqizht='" + benqizht + '\'' +
                ", zhanghye=" + zhanghye +
                ", huankriq='" + huankriq + '\'' +
                '}';
    }
}
