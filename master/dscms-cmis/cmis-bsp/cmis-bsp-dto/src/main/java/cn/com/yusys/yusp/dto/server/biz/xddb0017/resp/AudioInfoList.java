package cn.com.yusys.yusp.dto.server.biz.xddb0017.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 9:22
 * @Version 1.0
 */
public class AudioInfoList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "file_id")
    private String file_id;//文件ID
    @JsonProperty(value = "mortgagor")
    private String mortgagor;//抵押人名称
    @JsonProperty(value = "mortgage_no")
    private String mortgage_no;//抵押物编号
    @JsonProperty(value = "contract_no")
    private String contract_no;//抵押合同编号
    @JsonProperty(value = "mortgage_location")
    private String mortgage_location;//抵押物坐落
    @JsonProperty(value = "create_time")
    private String create_time;//录制时间


    public String getFile_id() {
        return file_id;
    }

    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    public String getMortgagor() {
        return mortgagor;
    }

    public void setMortgagor(String mortgagor) {
        this.mortgagor = mortgagor;
    }

    public String getMortgage_no() {
        return mortgage_no;
    }

    public void setMortgage_no(String mortgage_no) {
        this.mortgage_no = mortgage_no;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getMortgage_location() {
        return mortgage_location;
    }

    public void setMortgage_location(String mortgage_location) {
        this.mortgage_location = mortgage_location;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    @Override
    public String toString() {
        return "AudioInfoList{" +
                "file_id='" + file_id + '\'' +
                ", mortgagor='" + mortgagor + '\'' +
                ", mortgage_no='" + mortgage_no + '\'' +
                ", contract_no='" + contract_no + '\'' +
                ", mortgage_location='" + mortgage_location + '\'' +
                ", create_time='" + create_time + '\'' +
                '}';
    }
}
