package cn.com.yusys.yusp.dto.server.biz.xdcz0029.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：网银推送省心快贷审核任务至信贷
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdcz0029RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdcz0022RespDto{" +
                ", data=" + data +
                '}';
    }
}
