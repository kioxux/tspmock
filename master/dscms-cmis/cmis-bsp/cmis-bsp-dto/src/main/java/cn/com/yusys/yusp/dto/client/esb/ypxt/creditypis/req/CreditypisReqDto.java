package cn.com.yusys.yusp.dto.client.esb.ypxt.creditypis.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信用证信息同步
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CreditypisReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "CreditypisReqDto{" +
                "list=" + list +
                '}';
    }
}
