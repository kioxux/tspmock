package cn.com.yusys.yusp.dto.client.gxp.tonglian.d11005.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：d11005
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D11005RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "appno")
    private String appno;//申请件编号

    public String getAppno() {
        return appno;
    }

    public void setAppno(String appno) {
        this.appno = appno;
    }

    @Override
    public String toString() {
        return "D11005RespDto{" +
                "appno='" + appno + '\'' +
                '}';
    }
}
