package cn.com.yusys.yusp.dto.server.biz.xdcz0022.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：风控发送相关信息至信贷进行支用校验
 * @Author xs
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "apply_amount")
    private BigDecimal apply_amount;//申请金额
    @JsonProperty(value = "loan_start_date")
    private String loan_start_date;//借据起始日
    @JsonProperty(value = "loan_end_date")
    private String loan_end_date;//借据到期日
    @JsonProperty(value = "trade_partner_acc")
    private String trade_partner_acc;//交易对手账号
    @JsonProperty(value = "trade_partner_name")
    private String trade_partner_name;//交易对手名称

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public BigDecimal getApply_amount() {
        return apply_amount;
    }

    public void setApply_amount(BigDecimal apply_amount) {
        this.apply_amount = apply_amount;
    }

    public String getLoan_start_date() {
        return loan_start_date;
    }

    public void setLoan_start_date(String loan_start_date) {
        this.loan_start_date = loan_start_date;
    }

    public String getLoan_end_date() {
        return loan_end_date;
    }

    public void setLoan_end_date(String loan_end_date) {
        this.loan_end_date = loan_end_date;
    }

    public String getTrade_partner_acc() {
        return trade_partner_acc;
    }

    public void setTrade_partner_acc(String trade_partner_acc) {
        this.trade_partner_acc = trade_partner_acc;
    }

    public String getTrade_partner_name() {
        return trade_partner_name;
    }

    public void setTrade_partner_name(String trade_partner_name) {
        this.trade_partner_name = trade_partner_name;
    }

    @Override
    public String toString() {
        return "Xdcz0022ReqDto{" +
                "cont_no='" + cont_no + '\'' +
                "apply_amount='" + apply_amount + '\'' +
                "loan_start_date='" + loan_start_date + '\'' +
                "loan_end_date='" + loan_end_date + '\'' +
                "trade_partner_acc='" + trade_partner_acc + '\'' +
                "trade_partner_name='" + trade_partner_name + '\'' +
                '}';
    }
}
