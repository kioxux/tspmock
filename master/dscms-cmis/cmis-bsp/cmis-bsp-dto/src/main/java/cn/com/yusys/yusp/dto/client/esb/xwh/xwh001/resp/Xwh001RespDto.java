package cn.com.yusys.yusp.dto.client.esb.xwh.xwh001.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：借据台账信息接收
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwh001RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}  
