package cn.com.yusys.yusp.dto.server.biz.hyy.babicr01;

import cn.com.yusys.yusp.dto.server.HyyRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Babicr01RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "code")
    private String code;
    @JsonProperty(value = "message")
    private String message;
    @JsonProperty(value = "success")
    private Boolean success;
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.server.biz.hyy.babicr01.Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @Override
    public String toString() {
        return "Babicr01RespDto{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", success=" + success +
                ", data=" + data +
                '}';
    }
}
