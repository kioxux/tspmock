package cn.com.yusys.yusp.dto.server.biz.xdtz0027.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "isHavingLoan")
    private String isHavingLoan;//是否有贷款指标值

    public String getIsHavingLoan() {
        return isHavingLoan;
    }

    public void setIsHavingLoan(String isHavingLoan) {
        this.isHavingLoan = isHavingLoan;
    }

    @Override
    public String toString() {
        return "Data{" +
                "isHavingLoan='" + isHavingLoan + '\'' +
                '}';
    }
}
