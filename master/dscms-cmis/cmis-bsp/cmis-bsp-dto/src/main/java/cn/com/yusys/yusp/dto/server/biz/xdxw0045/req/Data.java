package cn.com.yusys.yusp.dto.server.biz.xdxw0045.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：优抵贷客户调查撤销
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "op_flag")
    private String op_flag;//操作标识
    @JsonProperty(value = "survey_serno")
    private String survey_serno;//流水号
    @JsonProperty(value = "tax_grade_result")
    private String tax_grade_result;//模型评级结果
    @JsonProperty(value = "tax_score")
    private String tax_score;//模型评分

    public String getOp_flag() {
        return op_flag;
    }

    public void setOp_flag(String op_flag) {
        this.op_flag = op_flag;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }

    public String getTax_grade_result() {
        return tax_grade_result;
    }

    public void setTax_grade_result(String tax_grade_result) {
        this.tax_grade_result = tax_grade_result;
    }

    public String getTax_score() {
        return tax_score;
    }

    public void setTax_score(String tax_score) {
        this.tax_score = tax_score;
    }

    @Override
    public String toString() {
        return "Data{" +
                "op_flag='" + op_flag + '\'' +
                ", survey_serno='" + survey_serno + '\'' +
                ", tax_grade_result='" + tax_grade_result + '\'' +
                ", tax_score='" + tax_score + '\'' +
                '}';
    }
}
