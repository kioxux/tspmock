package cn.com.yusys.yusp.dto.server.biz.xdzc0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 19:16
 * @since 2021/6/8 19:16
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusAccount")
    private String cusAccount;//客户账号
    @JsonProperty(value = "tqAmount")
    private BigDecimal tqAmount;//提取金额
    @JsonProperty(value = "grteac")
    private String grteac;//保证金账号
    @JsonProperty(value = "martype")
    private String martype;//类型

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusAccount() {
        return cusAccount;
    }

    public void setCusAccount(String cusAccount) {
        this.cusAccount = cusAccount;
    }

    public BigDecimal getTqAmount() {
        return tqAmount;
    }

    public void setTqAmount(BigDecimal tqAmount) {
        this.tqAmount = tqAmount;
    }

    public String getGrteac() {
        return grteac;
    }

    public void setGrteac(String grteac) {
        this.grteac = grteac;
    }

    public String getMartype() {
        return martype;
    }

    public void setMartype(String martype) {
        this.martype = martype;
    }

    @Override
    public String toString() {
        return "Xdzc0015DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", cusAccount='" + cusAccount + '\'' +
                ", tqAmount=" + tqAmount +
                ", grteac='" + grteac + '\'' +
                ", martype='" + martype + '\'' +
                '}';
    }
}
