package cn.com.yusys.yusp.dto.server.biz.xdzc0020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 21:28
 * @since 2021/6/8 21:28
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "assetType")
    private String assetType;//资产类型
    @JsonProperty(value = "assetStartDate")
    private String assetStartDate;//资产到期日开始
    @JsonProperty(value = "assetEndDate")
    private String assetEndDate;//资产到期日截止
    @JsonProperty(value = "serno")
    private String serno;//出入池流水编号

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getAssetStartDate() {
        return assetStartDate;
    }

    public void setAssetStartDate(String assetStartDate) {
        this.assetStartDate = assetStartDate;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    @Override
    public String toString() {
        return "Xdzc0020ReqDto{" +
                "assetType='" + assetType + '\'' +
                "assetStartDate='" + assetStartDate + '\'' +
                "assetEndDate='" + assetEndDate + '\'' +
                "serno='" + serno + '\'' +
                '}';
    }
}
