package cn.com.yusys.yusp.dto.server.biz.xddb0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 10:00
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class RegisterHouseList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "rearea")
    private String rearea;//登记簿概要信息-建筑面积
    @JsonProperty(value = "reinar")
    private String reinar;//登记簿概要信息-套内建筑面积
    @JsonProperty(value = "reosha")
    private String reosha;//登记簿概要信息-分摊建筑面积
    @JsonProperty(value = "reouse")
    private String reouse;//登记簿概要信息-房屋用途
    @JsonProperty(value = "reonat")
    private String reonat;//登记簿概要信息-房屋性质
    @JsonProperty(value = "reotla")
    private String reotla;//登记簿概要信息-总层数
    @JsonProperty(value = "reosla")
    private String reosla;//登记簿概要信息-所在层
    @JsonProperty(value = "locate")
    private String locate;//登记簿概要信息-坐落

    public String getRearea() {
        return rearea;
    }

    public void setRearea(String rearea) {
        this.rearea = rearea;
    }

    public String getReinar() {
        return reinar;
    }

    public void setReinar(String reinar) {
        this.reinar = reinar;
    }

    public String getReosha() {
        return reosha;
    }

    public void setReosha(String reosha) {
        this.reosha = reosha;
    }

    public String getReouse() {
        return reouse;
    }

    public void setReouse(String reouse) {
        this.reouse = reouse;
    }

    public String getReonat() {
        return reonat;
    }

    public void setReonat(String reonat) {
        this.reonat = reonat;
    }

    public String getReotla() {
        return reotla;
    }

    public void setReotla(String reotla) {
        this.reotla = reotla;
    }

    public String getReosla() {
        return reosla;
    }

    public void setReosla(String reosla) {
        this.reosla = reosla;
    }

    public String getLocate() {
        return locate;
    }

    public void setLocate(String locate) {
        this.locate = locate;
    }

    @Override
    public String toString() {
        return "RegisterHouseList{" +
                "rearea='" + rearea + '\'' +
                ", reinar='" + reinar + '\'' +
                ", reosha='" + reosha + '\'' +
                ", reouse='" + reouse + '\'' +
                ", reonat='" + reonat + '\'' +
                ", reotla='" + reotla + '\'' +
                ", reosla='" + reosla + '\'' +
                ", locate='" + locate + '\'' +
                '}';
    }
}
