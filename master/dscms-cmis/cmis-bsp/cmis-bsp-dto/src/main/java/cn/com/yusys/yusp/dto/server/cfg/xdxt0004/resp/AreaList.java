package cn.com.yusys.yusp.dto.server.cfg.xdxt0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/25 8:54:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 8:54
 * @since 2021/5/25 8:54
 */
@JsonPropertyOrder(alphabetic = true)
public class AreaList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "area")
    private String area;//所辖区域

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "AreaList{" +
                "area='" + area + '\'' +
                '}';
    }
}
