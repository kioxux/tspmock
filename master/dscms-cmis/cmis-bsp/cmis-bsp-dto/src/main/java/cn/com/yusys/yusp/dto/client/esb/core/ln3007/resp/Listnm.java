package cn.com.yusys.yusp.dto.client.esb.core.ln3007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 10:31
 * @since 2021/5/28 10:31
 */
@JsonPropertyOrder(alphabetic = true)
public class Listnm implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    @Override
    public String toString() {
        return "Ln3007RespDto{" +
                "beizhuxx='" + beizhuxx + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                '}';
    }
}
