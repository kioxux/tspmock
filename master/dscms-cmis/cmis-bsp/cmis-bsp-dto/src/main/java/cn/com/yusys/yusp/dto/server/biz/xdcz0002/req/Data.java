package cn.com.yusys.yusp.dto.server.biz.xdcz0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：电子保函注销
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zxflag")
    private String zxflag;//注销标识
    @JsonProperty(value = "billNO")
    private String billNO;//借据号

    public String getBillNO() {
        return billNO;
    }

    public void setBillNO(String billNO) {
        this.billNO = billNO;
    }

    public String  getZxflag() { return zxflag; }
    public void setZxflag(String zxflag ) { this.zxflag = zxflag;}

    @Override
    public String toString() {
        return "Xdcz0002ReqDto{" +
                "zxflag='" + zxflag+ '\'' +
                ", billNO='" + billNO + '\'' +
                '}';
    }
}
