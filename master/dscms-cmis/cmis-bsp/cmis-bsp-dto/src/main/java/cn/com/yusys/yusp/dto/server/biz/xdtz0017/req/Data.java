package cn.com.yusys.yusp.dto.server.biz.xdtz0017.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "oldDrftNo")
    private String oldDrftNo;//旧票号
    @JsonProperty(value = "newDrftNo")
    private String newDrftNo;//新票号
    @JsonProperty(value = "pyeeName")
    private String pyeeName;//收款人名称
    @JsonProperty(value = "pyeeAcctbNo")
    private String pyeeAcctbNo;//收款人开户行行号
    @JsonProperty(value = "pyeeAcctNo")
    private String pyeeAcctNo;//收款人账号

    public String getOldDrftNo() {
        return oldDrftNo;
    }

    public void setOldDrftNo(String oldDrftNo) {
        this.oldDrftNo = oldDrftNo;
    }

    public String getNewDrftNo() {
        return newDrftNo;
    }

    public void setNewDrftNo(String newDrftNo) {
        this.newDrftNo = newDrftNo;
    }

    public String getPyeeName() {
        return pyeeName;
    }

    public void setPyeeName(String pyeeName) {
        this.pyeeName = pyeeName;
    }

    public String getPyeeAcctbNo() {
        return pyeeAcctbNo;
    }

    public void setPyeeAcctbNo(String pyeeAcctbNo) {
        this.pyeeAcctbNo = pyeeAcctbNo;
    }

    public String getPyeeAcctNo() {
        return pyeeAcctNo;
    }

    public void setPyeeAcctNo(String pyeeAcctNo) {
        this.pyeeAcctNo = pyeeAcctNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "oldDrftNo='" + oldDrftNo + '\'' +
                ", newDrftNo='" + newDrftNo + '\'' +
                ", pyeeName='" + pyeeName + '\'' +
                ", pyeeAcctbNo='" + pyeeAcctbNo + '\'' +
                ", pyeeAcctNo='" + pyeeAcctNo + '\'' +
                '}';
    }
}
