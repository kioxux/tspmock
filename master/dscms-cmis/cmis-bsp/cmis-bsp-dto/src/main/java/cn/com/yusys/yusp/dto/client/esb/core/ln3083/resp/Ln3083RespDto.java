package cn.com.yusys.yusp.dto.client.esb.core.ln3083.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：银团协议查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3083RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    private java.util.List<Lstytczfe> lstytczfe;

    public List<Lstytczfe> getLstytczfe() {
        return lstytczfe;
    }

    public void setLstytczfe(List<Lstytczfe> lstytczfe) {
        this.lstytczfe = lstytczfe;
    }

    @Override
    public String toString() {
        return "Ln3083RespDto{" +
                "lstytczfe=" + lstytczfe +
                '}';
    }
}
