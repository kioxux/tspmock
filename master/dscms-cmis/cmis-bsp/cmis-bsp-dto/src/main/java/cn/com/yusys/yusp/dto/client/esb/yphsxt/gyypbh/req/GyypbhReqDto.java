package cn.com.yusys.yusp.dto.client.esb.yphsxt.gyypbh.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：通过共有人编号查询押品编号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class GyypbhReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "common_owner_id")
    private String common_owner_id;//共有人客户编号

    public String getCommon_owner_id() {
        return common_owner_id;
    }

    public void setCommon_owner_id(String common_owner_id) {
        this.common_owner_id = common_owner_id;
    }

    @Override
    public String toString() {
        return "GyypbhReqDto{" +
                "common_owner_id='" + common_owner_id + '\'' +
                '}';
    }
}  
