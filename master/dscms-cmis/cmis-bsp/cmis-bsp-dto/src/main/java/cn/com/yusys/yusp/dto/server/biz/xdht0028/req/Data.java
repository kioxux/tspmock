package cn.com.yusys.yusp.dto.server.biz.xdht0028.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/30 10:34
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "contNo='" + contNo + '\'' +
                '}';
    }
}
