package cn.com.yusys.yusp.dto.client.esb.cxypbh;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：通过票据号码查询押品编号
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CxypbhRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_no")
    private String guar_no;//押品编号
    @JsonProperty(value = "bill_type")
    private String bill_type;//票据类型
    @JsonProperty(value = "bill_amt")
    private BigDecimal bill_amt;//票面金额

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    public String getBill_type() {
        return bill_type;
    }

    public void setBill_type(String bill_type) {
        this.bill_type = bill_type;
    }

    public BigDecimal getBill_amt() {
        return bill_amt;
    }

    public void setBill_amt(BigDecimal bill_amt) {
        this.bill_amt = bill_amt;
    }

    @Override
    public String toString() {
        return "CxypbhRespDto{" +
                "guar_no='" + guar_no + '\'' +
                "bill_type='" + bill_type + '\'' +
                "bill_amt='" + bill_amt + '\'' +
                '}';
    }
}  
