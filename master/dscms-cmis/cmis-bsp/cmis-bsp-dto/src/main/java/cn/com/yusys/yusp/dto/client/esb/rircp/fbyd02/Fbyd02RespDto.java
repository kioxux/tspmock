package cn.com.yusys.yusp.dto.client.esb.rircp.fbyd02;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：终审申请提交
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbyd02RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

}  
