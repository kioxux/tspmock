package cn.com.yusys.yusp.dto.server.biz.xdsx0017.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
/**
 * 响应Dto：信贷提供风控查询授信信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cus_name")
    private String cus_name;//客户名称
    @JsonProperty(value = "sum_crd")
    private String sum_crd;//总额度
    @JsonProperty(value = "lmt_status")
    private String lmt_status;//授信状态
    @JsonProperty(value = "manager_id")
    private String manager_id;//管护客户经理编号
    @JsonProperty(value = "manager_name")
    private String manager_name;//管护客户经理名称
    @JsonProperty(value = "start_date")
    private String start_date;//额度起始日
    @JsonProperty(value = "over_date")
    private String over_date;//额度到期日

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCus_name() {
        return cus_name;
    }

    public void setCus_name(String cus_name) {
        this.cus_name = cus_name;
    }

    public String getSum_crd() {
        return sum_crd;
    }

    public void setSum_crd(String sum_crd) {
        this.sum_crd = sum_crd;
    }

    public String getLmt_status() {
        return lmt_status;
    }

    public void setLmt_status(String lmt_status) {
        this.lmt_status = lmt_status;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getOver_date() {
        return over_date;
    }

    public void setOver_date(String over_date) {
        this.over_date = over_date;
    }

    @Override
    public String toString() {
        return "List{" +
                "cus_id='" + cus_id + '\'' +
                ", cus_name='" + cus_name + '\'' +
                ", sum_crd='" + sum_crd + '\'' +
                ", lmt_status='" + lmt_status + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", start_date='" + start_date + '\'' +
                ", over_date='" + over_date + '\'' +
                '}';
    }
}
