package cn.com.yusys.yusp.dto.client.esb.core.ln3110;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：用于贷款放款前进行还款计划试算
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3110RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "zongqish")
    private Integer zongqish;//总期数
    @JsonProperty(value = "leijlxsr")
    private BigDecimal leijlxsr;//累计利息收入
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "lstdkhk")
    private List<Lstdkhk> lstdkhk;//还款计划

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public Integer getZongqish() {
        return zongqish;
    }

    public void setZongqish(Integer zongqish) {
        this.zongqish = zongqish;
    }

    public BigDecimal getLeijlxsr() {
        return leijlxsr;
    }

    public void setLeijlxsr(BigDecimal leijlxsr) {
        this.leijlxsr = leijlxsr;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public List<Lstdkhk> getLstdkhk() {
        return lstdkhk;
    }

    public void setLstdkhk(List<Lstdkhk> lstdkhk) {
        this.lstdkhk = lstdkhk;
    }

    @Override
    public String toString() {
        return "Ln3110RespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", zongqish=" + zongqish +
                ", leijlxsr=" + leijlxsr +
                ", huankfsh='" + huankfsh + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", lstdkhk=" + lstdkhk +
                '}';
    }
}