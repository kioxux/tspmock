package cn.com.yusys.yusp.dto.client.esb.core.ln3020.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：联合贷款
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdklhmx implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "canyfdma")
    private String canyfdma;//参与方代码
    @JsonProperty(value = "canyfhum")
    private String canyfhum;//参与方户名
    @JsonProperty(value = "lianhfsh")
    private String lianhfsh;//联合方式
    @JsonProperty(value = "lhdkleix")
    private String lhdkleix;//联合贷款类型
    @JsonProperty(value = "canyjine")
    private BigDecimal canyjine;//参与金额
    @JsonProperty(value = "canybili")
    private BigDecimal canybili;//参与比例
    @JsonProperty(value = "zjingjbz")
    private String zjingjbz;//资金归集标志
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "zjzrzhao")
    private String zjzrzhao;//资金转入账号
    @JsonProperty(value = "zjzrzzxh")
    private String zjzrzzxh;//资金转入账号子序号
    @JsonProperty(value = "dailfeil")
    private BigDecimal dailfeil;//代理费率
    @JsonProperty(value = "canyjjha")
    private String canyjjha;//参与借据号
    @JsonProperty(value = "bjghrzzh")
    private String bjghrzzh;//本金归还入账账号
    @JsonProperty(value = "bjghrzxh")
    private String bjghrzxh;//本金归还入账账号子序号
    @JsonProperty(value = "lxghrzzh")
    private String lxghrzzh;//利息归还入账账号
    @JsonProperty(value = "lxghrzxh")
    private String lxghrzxh;//利息归还入账账号子序号

    public String getCanyfdma() {
        return canyfdma;
    }

    public void setCanyfdma(String canyfdma) {
        this.canyfdma = canyfdma;
    }

    public String getCanyfhum() {
        return canyfhum;
    }

    public void setCanyfhum(String canyfhum) {
        this.canyfhum = canyfhum;
    }

    public String getLianhfsh() {
        return lianhfsh;
    }

    public void setLianhfsh(String lianhfsh) {
        this.lianhfsh = lianhfsh;
    }

    public String getLhdkleix() {
        return lhdkleix;
    }

    public void setLhdkleix(String lhdkleix) {
        this.lhdkleix = lhdkleix;
    }

    public BigDecimal getCanyjine() {
        return canyjine;
    }

    public void setCanyjine(BigDecimal canyjine) {
        this.canyjine = canyjine;
    }

    public BigDecimal getCanybili() {
        return canybili;
    }

    public void setCanybili(BigDecimal canybili) {
        this.canybili = canybili;
    }

    public String getZjingjbz() {
        return zjingjbz;
    }

    public void setZjingjbz(String zjingjbz) {
        this.zjingjbz = zjingjbz;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZjzrzhao() {
        return zjzrzhao;
    }

    public void setZjzrzhao(String zjzrzhao) {
        this.zjzrzhao = zjzrzhao;
    }

    public String getZjzrzzxh() {
        return zjzrzzxh;
    }

    public void setZjzrzzxh(String zjzrzzxh) {
        this.zjzrzzxh = zjzrzzxh;
    }

    public BigDecimal getDailfeil() {
        return dailfeil;
    }

    public void setDailfeil(BigDecimal dailfeil) {
        this.dailfeil = dailfeil;
    }

    public String getCanyjjha() {
        return canyjjha;
    }

    public void setCanyjjha(String canyjjha) {
        this.canyjjha = canyjjha;
    }

    public String getBjghrzzh() {
        return bjghrzzh;
    }

    public void setBjghrzzh(String bjghrzzh) {
        this.bjghrzzh = bjghrzzh;
    }

    public String getBjghrzxh() {
        return bjghrzxh;
    }

    public void setBjghrzxh(String bjghrzxh) {
        this.bjghrzxh = bjghrzxh;
    }

    public String getLxghrzzh() {
        return lxghrzzh;
    }

    public void setLxghrzzh(String lxghrzzh) {
        this.lxghrzzh = lxghrzzh;
    }

    public String getLxghrzxh() {
        return lxghrzxh;
    }

    public void setLxghrzxh(String lxghrzxh) {
        this.lxghrzxh = lxghrzxh;
    }

    @Override
    public String
    toString() {
        return "Lstdklhmx{" +
                "canyfdma='" + canyfdma + '\'' +
                ", canyfhum='" + canyfhum + '\'' +
                ", lianhfsh='" + lianhfsh + '\'' +
                ", lhdkleix='" + lhdkleix + '\'' +
                ", canyjine=" + canyjine +
                ", canybili=" + canybili +
                ", zjingjbz='" + zjingjbz + '\'' +
                ", zjlyzhao='" + zjlyzhao + '\'' +
                ", zjlyzzxh='" + zjlyzzxh + '\'' +
                ", zjzrzhao='" + zjzrzhao + '\'' +
                ", zjzrzzxh='" + zjzrzzxh + '\'' +
                ", dailfeil=" + dailfeil +
                ", canyjjha='" + canyjjha + '\'' +
                ", bjghrzzh='" + bjghrzzh + '\'' +
                ", bjghrzxh='" + bjghrzxh + '\'' +
                ", lxghrzzh='" + lxghrzzh + '\'' +
                ", lxghrzxh='" + lxghrzxh + '\'' +
                '}';
    }
}