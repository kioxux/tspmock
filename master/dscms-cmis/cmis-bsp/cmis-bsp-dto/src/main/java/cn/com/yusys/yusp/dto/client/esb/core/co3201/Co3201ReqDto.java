package cn.com.yusys.yusp.dto.client.esb.core.co3201;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：抵质押物信息修改
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Co3201ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkkhczbz")
    private String dkkhczbz;//开户操作标志
    @JsonProperty(value = "dzywbhao")
    private String dzywbhao;//抵质押物编号
    @JsonProperty(value = "dzywminc")
    private String dzywminc;//抵质押物名称
    @JsonProperty(value = "zhngjigo")
    private String zhngjigo;//账务机构
    @JsonProperty(value = "dizyfshi")
    private String dizyfshi;//抵质押方式
    @JsonProperty(value = "syqrkehh")
    private String syqrkehh;//所有权人客户号
    @JsonProperty(value = "syqrkehm")
    private String syqrkehm;//所有权人客户名
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "minyjiaz")
    private BigDecimal minyjiaz;//名义价值
    @JsonProperty(value = "shijjiaz")
    private BigDecimal shijjiaz;//实际价值
    @JsonProperty(value = "pingjiaz")
    private BigDecimal pingjiaz;//评估价值
    @JsonProperty(value = "dizybilv")
    private BigDecimal dizybilv;//抵质押比率
    @JsonProperty(value = "keyongje")
    private BigDecimal keyongje;//可用金额
    @JsonProperty(value = "yiyongje")
    private BigDecimal yiyongje;//已用金额
    @JsonProperty(value = "shengxrq")
    private String shengxrq;//生效日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "glywbhao")
    private String glywbhao;//关联业务编号
    @JsonProperty(value = "yewusx01")
    private String yewusx01;//记账余额属性
    @JsonProperty(value = "huowuhth")
    private String huowuhth;//货物合同号
    @JsonProperty(value = "yewmingc")
    private String yewmingc;//业务名称
    @JsonProperty(value = "lstklnb_dkzhzy")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.co3201.Lstklnb_dkzhzy> lstklnb_dkzhzy;//抵质押物信息

    public String getDkkhczbz() {
        return dkkhczbz;
    }

    public void setDkkhczbz(String dkkhczbz) {
        this.dkkhczbz = dkkhczbz;
    }

    public String getDzywbhao() {
        return dzywbhao;
    }

    public void setDzywbhao(String dzywbhao) {
        this.dzywbhao = dzywbhao;
    }

    public String getDzywminc() {
        return dzywminc;
    }

    public void setDzywminc(String dzywminc) {
        this.dzywminc = dzywminc;
    }

    public String getZhngjigo() {
        return zhngjigo;
    }

    public void setZhngjigo(String zhngjigo) {
        this.zhngjigo = zhngjigo;
    }

    public String getDizyfshi() {
        return dizyfshi;
    }

    public void setDizyfshi(String dizyfshi) {
        this.dizyfshi = dizyfshi;
    }

    public String getSyqrkehh() {
        return syqrkehh;
    }

    public void setSyqrkehh(String syqrkehh) {
        this.syqrkehh = syqrkehh;
    }

    public String getSyqrkehm() {
        return syqrkehm;
    }

    public void setSyqrkehm(String syqrkehm) {
        this.syqrkehm = syqrkehm;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getMinyjiaz() {
        return minyjiaz;
    }

    public void setMinyjiaz(BigDecimal minyjiaz) {
        this.minyjiaz = minyjiaz;
    }

    public BigDecimal getShijjiaz() {
        return shijjiaz;
    }

    public void setShijjiaz(BigDecimal shijjiaz) {
        this.shijjiaz = shijjiaz;
    }

    public BigDecimal getPingjiaz() {
        return pingjiaz;
    }

    public void setPingjiaz(BigDecimal pingjiaz) {
        this.pingjiaz = pingjiaz;
    }

    public BigDecimal getDizybilv() {
        return dizybilv;
    }

    public void setDizybilv(BigDecimal dizybilv) {
        this.dizybilv = dizybilv;
    }

    public BigDecimal getKeyongje() {
        return keyongje;
    }

    public void setKeyongje(BigDecimal keyongje) {
        this.keyongje = keyongje;
    }

    public BigDecimal getYiyongje() {
        return yiyongje;
    }

    public void setYiyongje(BigDecimal yiyongje) {
        this.yiyongje = yiyongje;
    }

    public String getShengxrq() {
        return shengxrq;
    }

    public void setShengxrq(String shengxrq) {
        this.shengxrq = shengxrq;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getGlywbhao() {
        return glywbhao;
    }

    public void setGlywbhao(String glywbhao) {
        this.glywbhao = glywbhao;
    }

    public String getYewusx01() {
        return yewusx01;
    }

    public void setYewusx01(String yewusx01) {
        this.yewusx01 = yewusx01;
    }

    public String getHuowuhth() {
        return huowuhth;
    }

    public void setHuowuhth(String huowuhth) {
        this.huowuhth = huowuhth;
    }

    public String getYewmingc() {
        return yewmingc;
    }

    public void setYewmingc(String yewmingc) {
        this.yewmingc = yewmingc;
    }

    public List<Lstklnb_dkzhzy> getLstklnb_dkzhzy() {
        return lstklnb_dkzhzy;
    }

    public void setLstklnb_dkzhzy(List<Lstklnb_dkzhzy> lstklnb_dkzhzy) {
        this.lstklnb_dkzhzy = lstklnb_dkzhzy;
    }

    @Override
    public String toString() {
        return "Co3201ReqDto{" +
                "dkkhczbz='" + dkkhczbz + '\'' +
                ", dzywbhao='" + dzywbhao + '\'' +
                ", dzywminc='" + dzywminc + '\'' +
                ", zhngjigo='" + zhngjigo + '\'' +
                ", dizyfshi='" + dizyfshi + '\'' +
                ", syqrkehh='" + syqrkehh + '\'' +
                ", syqrkehm='" + syqrkehm + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", minyjiaz=" + minyjiaz +
                ", shijjiaz=" + shijjiaz +
                ", pingjiaz=" + pingjiaz +
                ", dizybilv=" + dizybilv +
                ", keyongje=" + keyongje +
                ", yiyongje=" + yiyongje +
                ", shengxrq='" + shengxrq + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", zhaiyoms='" + zhaiyoms + '\'' +
                ", glywbhao='" + glywbhao + '\'' +
                ", yewusx01='" + yewusx01 + '\'' +
                ", huowuhth='" + huowuhth + '\'' +
                ", yewmingc='" + yewmingc + '\'' +
                ", lstklnb_dkzhzy=" + lstklnb_dkzhzy +
                '}';
    }
}
