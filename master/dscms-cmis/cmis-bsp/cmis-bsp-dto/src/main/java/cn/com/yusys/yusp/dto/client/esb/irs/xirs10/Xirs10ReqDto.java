package cn.com.yusys.yusp.dto.client.esb.irs.xirs10;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：公司客户信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xirs10ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custid")
    private String custid;//客户编号
    @JsonProperty(value = "custname")
    private String custname;//客户名称
    @JsonProperty(value = "certtype")
    private String certtype;//证件类型
    @JsonProperty(value = "certid")
    private String certid;//证件号码
    @JsonProperty(value = "custtype")
    private String custtype;//客户类型
    @JsonProperty(value = "inputuserid")
    private String inputuserid;//主管客户经理编号
    @JsonProperty(value = "inputorgid")
    private String inputorgid;//主管客户经理所属机构编号
    @JsonProperty(value = "industrycode")
    private String industrycode;//所属国标行业
    @JsonProperty(value = "inputqx")
    private String inputqx;//主管客户经理权限
    @JsonProperty(value = "inputusername")
    private String inputusername;//主管客户经理名称

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getCerttype() {
        return certtype;
    }

    public void setCerttype(String certtype) {
        this.certtype = certtype;
    }

    public String getCertid() {
        return certid;
    }

    public void setCertid(String certid) {
        this.certid = certid;
    }

    public String getCusttype() {
        return custtype;
    }

    public void setCusttype(String custtype) {
        this.custtype = custtype;
    }

    public String getInputuserid() {
        return inputuserid;
    }

    public void setInputuserid(String inputuserid) {
        this.inputuserid = inputuserid;
    }

    public String getInputorgid() {
        return inputorgid;
    }

    public void setInputorgid(String inputorgid) {
        this.inputorgid = inputorgid;
    }

    public String getIndustrycode() {
        return industrycode;
    }

    public void setIndustrycode(String industrycode) {
        this.industrycode = industrycode;
    }

    public String getInputqx() {
        return inputqx;
    }

    public void setInputqx(String inputqx) {
        this.inputqx = inputqx;
    }

    public String getInputusername() {
        return inputusername;
    }

    public void setInputusername(String inputusername) {
        this.inputusername = inputusername;
    }

    @Override
    public String toString() {
        return "Xirs10ReqDto{" +
                "custid='" + custid + '\'' +
                ", custname='" + custname + '\'' +
                ", certtype='" + certtype + '\'' +
                ", certid='" + certid + '\'' +
                ", custtype='" + custtype + '\'' +
                ", inputuserid='" + inputuserid + '\'' +
                ", inputorgid='" + inputorgid + '\'' +
                ", industrycode='" + industrycode + '\'' +
                ", inputqx='" + inputqx + '\'' +
                ", inputusername='" + inputusername + '\'' +
                '}';
    }
}
