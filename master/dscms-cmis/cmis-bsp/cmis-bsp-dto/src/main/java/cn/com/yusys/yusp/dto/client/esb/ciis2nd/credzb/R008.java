package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

//零售风险提示
@JsonPropertyOrder(alphabetic = true)
public class R008 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CONSUMPTIONBALANCE")
    private BigDecimal CONSUMPTIONBALANCE;// 消费类融资余额,
    @JsonProperty(value = "CONSUMPTIONREPAYMENT")
    private BigDecimal CONSUMPTIONREPAYMENT;// 消费类融资月还款额,
    @JsonProperty(value = "CURRENTLOANOVERDUEAMOUNT")
    private BigDecimal CURRENTLOANOVERDUEAMOUNT;// 贷款当前逾期金额,
    @JsonProperty(value = "MAXLOANOVERDUEAMOUNT")
    private BigDecimal MAXLOANOVERDUEAMOUNT;// 贷款单月最高逾期金额,
    @JsonProperty(value = "MAXMONTHSOFLOANOVERDUE")
    private BigDecimal MAXMONTHSOFLOANOVERDUE;// 贷款最长逾期月数,
    @JsonProperty(value = "CURRENTCREDITCARDOVERDUEAMOUNT")
    private BigDecimal CURRENTCREDITCARDOVERDUEAMOUNT;// 贷记卡当前逾期金额,
    @JsonProperty(value = "MAXCREDITCARDOVERDUEAMOUNT")
    private BigDecimal MAXCREDITCARDOVERDUEAMOUNT;// 贷记卡单月最高逾期金额,
    @JsonProperty(value = "MAXMONTHSOFCREDITCARDOVERDUE")
    private BigDecimal MAXMONTHSOFCREDITCARDOVERDUE;// 贷记卡最长逾期月数,
    @JsonProperty(value = "OVERDUENUMOFTWOYEARS")
    private BigDecimal OVERDUENUMOFTWOYEARS;// 两年内逾期次数（贷记卡+贷款）,
    @JsonProperty(value = "OVERDUENUMOVERTWOYEARS")
    private BigDecimal OVERDUENUMOVERTWOYEARS;// 两年外逾期次数（贷记卡+贷款）

    @JsonIgnore
    public BigDecimal getCONSUMPTIONBALANCE() {
        return CONSUMPTIONBALANCE;
    }

    @JsonIgnore
    public void setCONSUMPTIONBALANCE(BigDecimal CONSUMPTIONBALANCE) {
        this.CONSUMPTIONBALANCE = CONSUMPTIONBALANCE;
    }

    @JsonIgnore
    public BigDecimal getCONSUMPTIONREPAYMENT() {
        return CONSUMPTIONREPAYMENT;
    }

    @JsonIgnore
    public void setCONSUMPTIONREPAYMENT(BigDecimal CONSUMPTIONREPAYMENT) {
        this.CONSUMPTIONREPAYMENT = CONSUMPTIONREPAYMENT;
    }

    @JsonIgnore
    public BigDecimal getCURRENTLOANOVERDUEAMOUNT() {
        return CURRENTLOANOVERDUEAMOUNT;
    }

    @JsonIgnore
    public void setCURRENTLOANOVERDUEAMOUNT(BigDecimal CURRENTLOANOVERDUEAMOUNT) {
        this.CURRENTLOANOVERDUEAMOUNT = CURRENTLOANOVERDUEAMOUNT;
    }

    @JsonIgnore
    public BigDecimal getMAXLOANOVERDUEAMOUNT() {
        return MAXLOANOVERDUEAMOUNT;
    }

    @JsonIgnore
    public void setMAXLOANOVERDUEAMOUNT(BigDecimal MAXLOANOVERDUEAMOUNT) {
        this.MAXLOANOVERDUEAMOUNT = MAXLOANOVERDUEAMOUNT;
    }

    @JsonIgnore
    public BigDecimal getMAXMONTHSOFLOANOVERDUE() {
        return MAXMONTHSOFLOANOVERDUE;
    }

    @JsonIgnore
    public void setMAXMONTHSOFLOANOVERDUE(BigDecimal MAXMONTHSOFLOANOVERDUE) {
        this.MAXMONTHSOFLOANOVERDUE = MAXMONTHSOFLOANOVERDUE;
    }

    @JsonIgnore
    public BigDecimal getCURRENTCREDITCARDOVERDUEAMOUNT() {
        return CURRENTCREDITCARDOVERDUEAMOUNT;
    }

    @JsonIgnore
    public void setCURRENTCREDITCARDOVERDUEAMOUNT(BigDecimal CURRENTCREDITCARDOVERDUEAMOUNT) {
        this.CURRENTCREDITCARDOVERDUEAMOUNT = CURRENTCREDITCARDOVERDUEAMOUNT;
    }

    @JsonIgnore
    public BigDecimal getMAXCREDITCARDOVERDUEAMOUNT() {
        return MAXCREDITCARDOVERDUEAMOUNT;
    }

    @JsonIgnore
    public void setMAXCREDITCARDOVERDUEAMOUNT(BigDecimal MAXCREDITCARDOVERDUEAMOUNT) {
        this.MAXCREDITCARDOVERDUEAMOUNT = MAXCREDITCARDOVERDUEAMOUNT;
    }

    @JsonIgnore
    public BigDecimal getMAXMONTHSOFCREDITCARDOVERDUE() {
        return MAXMONTHSOFCREDITCARDOVERDUE;
    }

    @JsonIgnore
    public void setMAXMONTHSOFCREDITCARDOVERDUE(BigDecimal MAXMONTHSOFCREDITCARDOVERDUE) {
        this.MAXMONTHSOFCREDITCARDOVERDUE = MAXMONTHSOFCREDITCARDOVERDUE;
    }

    @JsonIgnore
    public BigDecimal getOVERDUENUMOFTWOYEARS() {
        return OVERDUENUMOFTWOYEARS;
    }

    @JsonIgnore
    public void setOVERDUENUMOFTWOYEARS(BigDecimal OVERDUENUMOFTWOYEARS) {
        this.OVERDUENUMOFTWOYEARS = OVERDUENUMOFTWOYEARS;
    }

    @JsonIgnore
    public BigDecimal getOVERDUENUMOVERTWOYEARS() {
        return OVERDUENUMOVERTWOYEARS;
    }

    @JsonIgnore
    public void setOVERDUENUMOVERTWOYEARS(BigDecimal OVERDUENUMOVERTWOYEARS) {
        this.OVERDUENUMOVERTWOYEARS = OVERDUENUMOVERTWOYEARS;
    }

    @Override
    public String toString() {
        return "R008{" +
                "CONSUMPTIONBALANCE=" + CONSUMPTIONBALANCE +
                ", CONSUMPTIONREPAYMENT=" + CONSUMPTIONREPAYMENT +
                ", CURRENTLOANOVERDUEAMOUNT=" + CURRENTLOANOVERDUEAMOUNT +
                ", MAXLOANOVERDUEAMOUNT=" + MAXLOANOVERDUEAMOUNT +
                ", MAXMONTHSOFLOANOVERDUE=" + MAXMONTHSOFLOANOVERDUE +
                ", CURRENTCREDITCARDOVERDUEAMOUNT=" + CURRENTCREDITCARDOVERDUEAMOUNT +
                ", MAXCREDITCARDOVERDUEAMOUNT=" + MAXCREDITCARDOVERDUEAMOUNT +
                ", MAXMONTHSOFCREDITCARDOVERDUE=" + MAXMONTHSOFCREDITCARDOVERDUE +
                ", OVERDUENUMOFTWOYEARS=" + OVERDUENUMOFTWOYEARS +
                ", OVERDUENUMOVERTWOYEARS=" + OVERDUENUMOVERTWOYEARS +
                '}';
    }
}
