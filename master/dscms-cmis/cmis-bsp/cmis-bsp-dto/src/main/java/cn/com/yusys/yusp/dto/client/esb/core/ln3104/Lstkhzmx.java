package cn.com.yusys.yusp.dto.client.esb.core.ln3104;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款客户帐明细查询输出
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstkhzmx implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "dkrzhzhh")
    private String dkrzhzhh;//贷款入账账号
    @JsonProperty(value = "dkrzhzxh")
    private String dkrzhzxh;//贷款入账账号子序号
    @JsonProperty(value = "fkjineee")
    private BigDecimal fkjineee;//放款金额
    @JsonProperty(value = "fkzjclfs")
    private String fkzjclfs;//放款资金处理方式
    @JsonProperty(value = "daixzhxh")
    private String daixzhxh;//待销账序号
    @JsonProperty(value = "djiebhao")
    private String djiebhao;//冻结编号
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "hkzongee")
    private BigDecimal hkzongee;//还款总额
    @JsonProperty(value = "huankzht")
    private String huankzht;//还款状态
    @JsonProperty(value = "ghbenjin")
    private BigDecimal ghbenjin;//归还本金
    @JsonProperty(value = "ghhxbenj")
    private BigDecimal ghhxbenj;//归还核销本金
    @JsonProperty(value = "ghysyjlx")
    private BigDecimal ghysyjlx;//归还应收应计利息
    @JsonProperty(value = "ghcsyjlx")
    private BigDecimal ghcsyjlx;//归还催收应计利息
    @JsonProperty(value = "ghynshqx")
    private BigDecimal ghynshqx;//归还应收欠息
    @JsonProperty(value = "ghcushqx")
    private BigDecimal ghcushqx;//归还催收欠息
    @JsonProperty(value = "ghysyjfx")
    private BigDecimal ghysyjfx;//归还应收应计罚息
    @JsonProperty(value = "ghcsyjfx")
    private BigDecimal ghcsyjfx;//归还催收应计罚息
    @JsonProperty(value = "ghynshfx")
    private BigDecimal ghynshfx;//归还应收罚息
    @JsonProperty(value = "ghcushfx")
    private BigDecimal ghcushfx;//归还催收罚息
    @JsonProperty(value = "ghyjfuxi")
    private BigDecimal ghyjfuxi;//归还应计复息
    @JsonProperty(value = "ghfxfuxi")
    private BigDecimal ghfxfuxi;//归还复息
    @JsonProperty(value = "ghhxlixi")
    private BigDecimal ghhxlixi;//归还核销利息
    @JsonProperty(value = "ghyhxbjl")
    private BigDecimal ghyhxbjl;//归还已核销本金利息
    @JsonProperty(value = "ghfajinn")
    private BigDecimal ghfajinn;//归还罚金
    @JsonProperty(value = "ghfeiyin")
    private BigDecimal ghfeiyin;//归还费用
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "jiaoyisj")
    private String jiaoyisj;//交易事件
    @JsonProperty(value = "shjshuom")
    private String shjshuom;//事件说明
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "qudaohao")
    private String qudaohao;//渠道号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称


    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getDkrzhzhh() {
        return dkrzhzhh;
    }

    public void setDkrzhzhh(String dkrzhzhh) {
        this.dkrzhzhh = dkrzhzhh;
    }

    public String getDkrzhzxh() {
        return dkrzhzxh;
    }

    public void setDkrzhzxh(String dkrzhzxh) {
        this.dkrzhzxh = dkrzhzxh;
    }

    public BigDecimal getFkjineee() {
        return fkjineee;
    }

    public void setFkjineee(BigDecimal fkjineee) {
        this.fkjineee = fkjineee;
    }

    public String getFkzjclfs() {
        return fkzjclfs;
    }

    public void setFkzjclfs(String fkzjclfs) {
        this.fkzjclfs = fkzjclfs;
    }

    public String getDaixzhxh() {
        return daixzhxh;
    }

    public void setDaixzhxh(String daixzhxh) {
        this.daixzhxh = daixzhxh;
    }

    public String getDjiebhao() {
        return djiebhao;
    }

    public void setDjiebhao(String djiebhao) {
        this.djiebhao = djiebhao;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public BigDecimal getHkzongee() {
        return hkzongee;
    }

    public void setHkzongee(BigDecimal hkzongee) {
        this.hkzongee = hkzongee;
    }

    public String getHuankzht() {
        return huankzht;
    }

    public void setHuankzht(String huankzht) {
        this.huankzht = huankzht;
    }

    public BigDecimal getGhbenjin() {
        return ghbenjin;
    }

    public void setGhbenjin(BigDecimal ghbenjin) {
        this.ghbenjin = ghbenjin;
    }

    public BigDecimal getGhhxbenj() {
        return ghhxbenj;
    }

    public void setGhhxbenj(BigDecimal ghhxbenj) {
        this.ghhxbenj = ghhxbenj;
    }

    public BigDecimal getGhysyjlx() {
        return ghysyjlx;
    }

    public void setGhysyjlx(BigDecimal ghysyjlx) {
        this.ghysyjlx = ghysyjlx;
    }

    public BigDecimal getGhcsyjlx() {
        return ghcsyjlx;
    }

    public void setGhcsyjlx(BigDecimal ghcsyjlx) {
        this.ghcsyjlx = ghcsyjlx;
    }

    public BigDecimal getGhynshqx() {
        return ghynshqx;
    }

    public void setGhynshqx(BigDecimal ghynshqx) {
        this.ghynshqx = ghynshqx;
    }

    public BigDecimal getGhcushqx() {
        return ghcushqx;
    }

    public void setGhcushqx(BigDecimal ghcushqx) {
        this.ghcushqx = ghcushqx;
    }

    public BigDecimal getGhysyjfx() {
        return ghysyjfx;
    }

    public void setGhysyjfx(BigDecimal ghysyjfx) {
        this.ghysyjfx = ghysyjfx;
    }

    public BigDecimal getGhcsyjfx() {
        return ghcsyjfx;
    }

    public void setGhcsyjfx(BigDecimal ghcsyjfx) {
        this.ghcsyjfx = ghcsyjfx;
    }

    public BigDecimal getGhynshfx() {
        return ghynshfx;
    }

    public void setGhynshfx(BigDecimal ghynshfx) {
        this.ghynshfx = ghynshfx;
    }

    public BigDecimal getGhcushfx() {
        return ghcushfx;
    }

    public void setGhcushfx(BigDecimal ghcushfx) {
        this.ghcushfx = ghcushfx;
    }

    public BigDecimal getGhyjfuxi() {
        return ghyjfuxi;
    }

    public void setGhyjfuxi(BigDecimal ghyjfuxi) {
        this.ghyjfuxi = ghyjfuxi;
    }

    public BigDecimal getGhfxfuxi() {
        return ghfxfuxi;
    }

    public void setGhfxfuxi(BigDecimal ghfxfuxi) {
        this.ghfxfuxi = ghfxfuxi;
    }

    public BigDecimal getGhhxlixi() {
        return ghhxlixi;
    }

    public void setGhhxlixi(BigDecimal ghhxlixi) {
        this.ghhxlixi = ghhxlixi;
    }

    public BigDecimal getGhyhxbjl() {
        return ghyhxbjl;
    }

    public void setGhyhxbjl(BigDecimal ghyhxbjl) {
        this.ghyhxbjl = ghyhxbjl;
    }

    public BigDecimal getGhfajinn() {
        return ghfajinn;
    }

    public void setGhfajinn(BigDecimal ghfajinn) {
        this.ghfajinn = ghfajinn;
    }

    public BigDecimal getGhfeiyin() {
        return ghfeiyin;
    }

    public void setGhfeiyin(BigDecimal ghfeiyin) {
        this.ghfeiyin = ghfeiyin;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getShjshuom() {
        return shjshuom;
    }

    public void setShjshuom(String shjshuom) {
        this.shjshuom = shjshuom;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getQudaohao() {
        return qudaohao;
    }

    public void setQudaohao(String qudaohao) {
        this.qudaohao = qudaohao;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    @Override
    public String toString() {
        return "Lstkhzmx{" +
                "dkzhangh='" + dkzhangh + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "yngyjigo='" + yngyjigo + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "dkrzhzhh='" + dkrzhzhh + '\'' +
                "dkrzhzxh='" + dkrzhzxh + '\'' +
                "fkjineee='" + fkjineee + '\'' +
                "fkzjclfs='" + fkzjclfs + '\'' +
                "daixzhxh='" + daixzhxh + '\'' +
                "djiebhao='" + djiebhao + '\'' +
                "zhchbjin='" + zhchbjin + '\'' +
                "yuqibjin='" + yuqibjin + '\'' +
                "dzhibjin='" + dzhibjin + '\'' +
                "daizbjin='" + daizbjin + '\'' +
                "zijnlaiy='" + zijnlaiy + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hkzongee='" + hkzongee + '\'' +
                "huankzht='" + huankzht + '\'' +
                "ghbenjin='" + ghbenjin + '\'' +
                "ghhxbenj='" + ghhxbenj + '\'' +
                "ghysyjlx='" + ghysyjlx + '\'' +
                "ghcsyjlx='" + ghcsyjlx + '\'' +
                "ghynshqx='" + ghynshqx + '\'' +
                "ghcushqx='" + ghcushqx + '\'' +
                "ghysyjfx='" + ghysyjfx + '\'' +
                "ghcsyjfx='" + ghcsyjfx + '\'' +
                "ghynshfx='" + ghynshfx + '\'' +
                "ghcushfx='" + ghcushfx + '\'' +
                "ghyjfuxi='" + ghyjfuxi + '\'' +
                "ghfxfuxi='" + ghfxfuxi + '\'' +
                "ghhxlixi='" + ghhxlixi + '\'' +
                "ghyhxbjl='" + ghyhxbjl + '\'' +
                "ghfajinn='" + ghfajinn + '\'' +
                "ghfeiyin='" + ghfeiyin + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "jiaoyisj='" + jiaoyisj + '\'' +
                "shjshuom='" + shjshuom + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "qudaohao='" + qudaohao + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                '}';
    }
}  
