package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj15.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：发票补录信息推送（信贷调票据）
 * @author lihh
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class Xdpj15RespDto implements Serializable {
	   private static final long serialVersionUID = 1L;
}
