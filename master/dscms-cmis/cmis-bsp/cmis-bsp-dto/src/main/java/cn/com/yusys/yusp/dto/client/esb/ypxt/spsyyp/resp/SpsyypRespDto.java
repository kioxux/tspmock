package cn.com.yusys.yusp.dto.client.esb.ypxt.spsyyp.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷审批结果同步接口
 * @author lihh
 * @version 1.0             
 */             
@JsonPropertyOrder(alphabetic = true)
public class SpsyypRespDto implements Serializable {
	   private static final long serialVersionUID = 1L;
}
