package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 上市公司十大股东
 */
@JsonPropertyOrder(alphabetic = true)
public class LISTEDSHAREHOLDER implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CREDITCODE")
    private String CREDITCODE;//	统一社会信用代码
    @JsonProperty(value = "ENTRYDATE")
    private String ENTRYDATE;//	更新日期
    @JsonProperty(value = "HOLDERAMT")
    private String HOLDERAMT;//	持股数量
    @JsonProperty(value = "HOLDERRTO")
    private String HOLDERRTO;//	持股比例
    @JsonProperty(value = "LIMITHOLDERAMT")
    private String LIMITHOLDERAMT;//	有限售股份数量
    @JsonProperty(value = "REGNO")
    private String REGNO;//	注册号
    @JsonProperty(value = "SHARESTYPE")
    private String SHARESTYPE;//	股份类型
    @JsonProperty(value = "SHHOLDERCREDITCODE")
    private String SHHOLDERCREDITCODE;//	股东统一社会信用代码
    @JsonProperty(value = "SHHOLDERNAME")
    private String SHHOLDERNAME;//	股东名称
    @JsonProperty(value = "SHHOLDERNATURE")
    private String SHHOLDERNATURE;//	股东股权性质
    @JsonProperty(value = "SHHOLDERNATURECODE")
    private String SHHOLDERNATURECODE;//	股东股权性质编码
    @JsonProperty(value = "SHHOLDERREGNO")
    private String SHHOLDERREGNO;//	股东注册号
    @JsonProperty(value = "SHHOLDERTYPE")
    private String SHHOLDERTYPE;//	股东机构类型
    @JsonProperty(value = "SHHOLDERTYPECODE")
    private String SHHOLDERTYPECODE;//	股东机构类型编码
    @JsonProperty(value = "UNLIMHOLDERAMT")
    private String UNLIMHOLDERAMT;//	无限售股份数量

    @JsonIgnore
    public String getCREDITCODE() {
        return CREDITCODE;
    }

    @JsonIgnore
    public void setCREDITCODE(String CREDITCODE) {
        this.CREDITCODE = CREDITCODE;
    }

    @JsonIgnore
    public String getENTRYDATE() {
        return ENTRYDATE;
    }

    @JsonIgnore
    public void setENTRYDATE(String ENTRYDATE) {
        this.ENTRYDATE = ENTRYDATE;
    }

    @JsonIgnore
    public String getHOLDERAMT() {
        return HOLDERAMT;
    }

    @JsonIgnore
    public void setHOLDERAMT(String HOLDERAMT) {
        this.HOLDERAMT = HOLDERAMT;
    }

    @JsonIgnore
    public String getHOLDERRTO() {
        return HOLDERRTO;
    }

    @JsonIgnore
    public void setHOLDERRTO(String HOLDERRTO) {
        this.HOLDERRTO = HOLDERRTO;
    }

    @JsonIgnore
    public String getLIMITHOLDERAMT() {
        return LIMITHOLDERAMT;
    }

    @JsonIgnore
    public void setLIMITHOLDERAMT(String LIMITHOLDERAMT) {
        this.LIMITHOLDERAMT = LIMITHOLDERAMT;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @JsonIgnore
    public String getSHARESTYPE() {
        return SHARESTYPE;
    }

    @JsonIgnore
    public void setSHARESTYPE(String SHARESTYPE) {
        this.SHARESTYPE = SHARESTYPE;
    }

    @JsonIgnore
    public String getSHHOLDERCREDITCODE() {
        return SHHOLDERCREDITCODE;
    }

    @JsonIgnore
    public void setSHHOLDERCREDITCODE(String SHHOLDERCREDITCODE) {
        this.SHHOLDERCREDITCODE = SHHOLDERCREDITCODE;
    }

    @JsonIgnore
    public String getSHHOLDERNAME() {
        return SHHOLDERNAME;
    }

    @JsonIgnore
    public void setSHHOLDERNAME(String SHHOLDERNAME) {
        this.SHHOLDERNAME = SHHOLDERNAME;
    }

    @JsonIgnore
    public String getSHHOLDERNATURE() {
        return SHHOLDERNATURE;
    }

    @JsonIgnore
    public void setSHHOLDERNATURE(String SHHOLDERNATURE) {
        this.SHHOLDERNATURE = SHHOLDERNATURE;
    }

    @JsonIgnore
    public String getSHHOLDERNATURECODE() {
        return SHHOLDERNATURECODE;
    }

    @JsonIgnore
    public void setSHHOLDERNATURECODE(String SHHOLDERNATURECODE) {
        this.SHHOLDERNATURECODE = SHHOLDERNATURECODE;
    }

    @JsonIgnore
    public String getSHHOLDERREGNO() {
        return SHHOLDERREGNO;
    }

    @JsonIgnore
    public void setSHHOLDERREGNO(String SHHOLDERREGNO) {
        this.SHHOLDERREGNO = SHHOLDERREGNO;
    }

    @JsonIgnore
    public String getSHHOLDERTYPE() {
        return SHHOLDERTYPE;
    }

    @JsonIgnore
    public void setSHHOLDERTYPE(String SHHOLDERTYPE) {
        this.SHHOLDERTYPE = SHHOLDERTYPE;
    }

    @JsonIgnore
    public String getSHHOLDERTYPECODE() {
        return SHHOLDERTYPECODE;
    }

    @JsonIgnore
    public void setSHHOLDERTYPECODE(String SHHOLDERTYPECODE) {
        this.SHHOLDERTYPECODE = SHHOLDERTYPECODE;
    }

    @JsonIgnore
    public String getUNLIMHOLDERAMT() {
        return UNLIMHOLDERAMT;
    }

    @JsonIgnore
    public void setUNLIMHOLDERAMT(String UNLIMHOLDERAMT) {
        this.UNLIMHOLDERAMT = UNLIMHOLDERAMT;
    }

    @Override
    public String toString() {
        return "LISTEDSHAREHOLDER{" +
                "CREDITCODE='" + CREDITCODE + '\'' +
                ", ENTRYDATE='" + ENTRYDATE + '\'' +
                ", HOLDERAMT='" + HOLDERAMT + '\'' +
                ", HOLDERRTO='" + HOLDERRTO + '\'' +
                ", LIMITHOLDERAMT='" + LIMITHOLDERAMT + '\'' +
                ", REGNO='" + REGNO + '\'' +
                ", SHARESTYPE='" + SHARESTYPE + '\'' +
                ", SHHOLDERCREDITCODE='" + SHHOLDERCREDITCODE + '\'' +
                ", SHHOLDERNAME='" + SHHOLDERNAME + '\'' +
                ", SHHOLDERNATURE='" + SHHOLDERNATURE + '\'' +
                ", SHHOLDERNATURECODE='" + SHHOLDERNATURECODE + '\'' +
                ", SHHOLDERREGNO='" + SHHOLDERREGNO + '\'' +
                ", SHHOLDERTYPE='" + SHHOLDERTYPE + '\'' +
                ", SHHOLDERTYPECODE='" + SHHOLDERTYPECODE + '\'' +
                ", UNLIMHOLDERAMT='" + UNLIMHOLDERAMT + '\'' +
                '}';
    }
}
