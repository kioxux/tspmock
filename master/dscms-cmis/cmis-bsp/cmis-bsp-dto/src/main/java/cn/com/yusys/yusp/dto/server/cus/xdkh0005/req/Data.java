package cn.com.yusys.yusp.dto.server.cus.xdkh0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Data：同业客户信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusno")
    private String cusno;//客户编号
    @JsonProperty(value = "cusna")
    private String cusna;//客户名称
    @JsonProperty(value = "type")
    private String type;//是否特殊流程标识

    public String getCusno() {
        return cusno;
    }

    public void setCusno(String cusno) {
        this.cusno = cusno;
    }

    public String getCusna() {
        return cusna;
    }

    public void setCusna(String cusna) {
        this.cusna = cusna;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusno='" + cusno + '\'' +
                ", cusna='" + cusna + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
