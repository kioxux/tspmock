package cn.com.yusys.yusp.dto.client.esb.yphsxt.xdjzzy.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：押品信息同步及引入
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XdjzzyRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_no")
    private String guar_no;//押品编号

    public String getGuar_no() {
        return guar_no;
    }

    public void setGuar_no(String guar_no) {
        this.guar_no = guar_no;
    }

    @Override
    public String toString() {
        return "XdjzzyRespDto{" +
                "guar_no='" + guar_no + '\'' +
                '}';
    }
}
