package cn.com.yusys.yusp.dto.server.biz.xdsx0023.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/17 11:28:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/17 11:28
 * @since 2021/5/17 11:28
 */
public class Data {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custid")
    private String custid;//客户号
    @JsonProperty(value = "custna")
    private String custna;//客户名称
    @JsonProperty(value = "lmtamt")
    private BigDecimal lmtamt;//授信额度
    @JsonProperty(value = "kylmam")
    private BigDecimal kylmam;//可用授信额度
    @JsonProperty(value = "contno")
    private String contno;//合同编号
    @JsonProperty(value = "cncono")
    private String cncono;//中文合同编号
    @JsonProperty(value = "prdcod")
    private String prdcod;//产品编号
    @JsonProperty(value = "bhtype")
    private String bhtype;//保函种类
    @JsonProperty(value = "asmean")
    private String asmean;//担保方式
    @JsonProperty(value = "curren")
    private String curren;//币种
    @JsonProperty(value = "hlrate")
    private BigDecimal hlrate;//汇率
    @JsonProperty(value = "conamt")
    private BigDecimal conamt;//合同金额
    @JsonProperty(value = "zsramt")
    private BigDecimal zsramt;//折算人民币金额
    @JsonProperty(value = "stdate")
    private String stdate;//起始日期
    @JsonProperty(value = "eddate")
    private String eddate;//到期日期
    @JsonProperty(value = "litind")
    private String litind;//使用授信额度标识
    @JsonProperty(value = "litzno")
    private String litzno;//授信台账编号
    @JsonProperty(value = "lixyno")
    private String lixyno;//授信协议编号
    @JsonProperty(value = "seccur")
    private String seccur;//保证金币种
    @JsonProperty(value = "sechlr")
    private BigDecimal sechlr;//保证金汇率
    @JsonProperty(value = "secamt")
    private BigDecimal secamt;//保证金金额
    @JsonProperty(value = "sezamt")
    private BigDecimal sezamt;//保证金折算人民币金额
    @JsonProperty(value = "secper")
    private BigDecimal secper;//保证金比例
    @JsonProperty(value = "sejxfs")
    private String sejxfs;//保证金计息方式
    @JsonProperty(value = "riskpe")
    private BigDecimal riskpe;//风险敞口比例
    @JsonProperty(value = "riskam")
    private BigDecimal riskam;//风险敞口金额
    @JsonProperty(value = "sxflll")
    private BigDecimal sxflll;//手续费率
    @JsonProperty(value = "sxfamt")
    private BigDecimal sxfamt;//手续费金额
    @JsonProperty(value = "sfzkdl")
    private String sfzkdl;//是否为转开代理行保函
    @JsonProperty(value = "dlhnam")
    private String dlhnam;//代理行名称
    @JsonProperty(value = "pronam")
    private String pronam;//项目名称
    @JsonProperty(value = "proamt")
    private BigDecimal proamt;//项目金额
    @JsonProperty(value = "cobsbh")
    private String cobsbh;//合同协议或标书编号
    @JsonProperty(value = "xgmyam")
    private BigDecimal xgmyam;//相关贸易合同金额
    @JsonProperty(value = "srrnam")
    private String srrnam;//受益人名称
    @JsonProperty(value = "syradd")
    private String syradd;//受益人地址
    @JsonProperty(value = "syrkhh")
    private String syrkhh;//受益人开户行
    @JsonProperty(value = "srracc")
    private String srracc;//受益人账号
    @JsonProperty(value = "bhfkfs")
    private String bhfkfs;//保函付款方式
    @JsonProperty(value = "cftjsm")
    private String cftjsm;//承付条件说明
    @JsonProperty(value = "contst")
    private String contst;//协议状态
    @JsonProperty(value = "inpuid")
    private String inpuid;//登记人
    @JsonProperty(value = "manaid")
    private String manaid;//责任人
    @JsonProperty(value = "mabrid")
    private String mabrid;//责任机构
    @JsonProperty(value = "billno")
    private String billno;//借据号

    public String getCustid() {
        return custid;
    }

    public void setCustid(String custid) {
        this.custid = custid;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public BigDecimal getLmtamt() {
        return lmtamt;
    }

    public void setLmtamt(BigDecimal lmtamt) {
        this.lmtamt = lmtamt;
    }

    public BigDecimal getKylmam() {
        return kylmam;
    }

    public void setKylmam(BigDecimal kylmam) {
        this.kylmam = kylmam;
    }

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    public String getCncono() {
        return cncono;
    }

    public void setCncono(String cncono) {
        this.cncono = cncono;
    }

    public String getPrdcod() {
        return prdcod;
    }

    public void setPrdcod(String prdcod) {
        this.prdcod = prdcod;
    }

    public String getBhtype() {
        return bhtype;
    }

    public void setBhtype(String bhtype) {
        this.bhtype = bhtype;
    }

    public String getAsmean() {
        return asmean;
    }

    public void setAsmean(String asmean) {
        this.asmean = asmean;
    }

    public String getCurren() {
        return curren;
    }

    public void setCurren(String curren) {
        this.curren = curren;
    }

    public BigDecimal getHlrate() {
        return hlrate;
    }

    public void setHlrate(BigDecimal hlrate) {
        this.hlrate = hlrate;
    }

    public BigDecimal getConamt() {
        return conamt;
    }

    public void setConamt(BigDecimal conamt) {
        this.conamt = conamt;
    }

    public BigDecimal getZsramt() {
        return zsramt;
    }

    public void setZsramt(BigDecimal zsramt) {
        this.zsramt = zsramt;
    }

    public String getStdate() {
        return stdate;
    }

    public void setStdate(String stdate) {
        this.stdate = stdate;
    }

    public String getEddate() {
        return eddate;
    }

    public void setEddate(String eddate) {
        this.eddate = eddate;
    }

    public String getLitind() {
        return litind;
    }

    public void setLitind(String litind) {
        this.litind = litind;
    }

    public String getLitzno() {
        return litzno;
    }

    public void setLitzno(String litzno) {
        this.litzno = litzno;
    }

    public String getLixyno() {
        return lixyno;
    }

    public void setLixyno(String lixyno) {
        this.lixyno = lixyno;
    }

    public String getSeccur() {
        return seccur;
    }

    public void setSeccur(String seccur) {
        this.seccur = seccur;
    }

    public BigDecimal getSechlr() {
        return sechlr;
    }

    public void setSechlr(BigDecimal sechlr) {
        this.sechlr = sechlr;
    }

    public BigDecimal getSecamt() {
        return secamt;
    }

    public void setSecamt(BigDecimal secamt) {
        this.secamt = secamt;
    }

    public BigDecimal getSezamt() {
        return sezamt;
    }

    public void setSezamt(BigDecimal sezamt) {
        this.sezamt = sezamt;
    }

    public BigDecimal getSecper() {
        return secper;
    }

    public void setSecper(BigDecimal secper) {
        this.secper = secper;
    }

    public String getSejxfs() {
        return sejxfs;
    }

    public void setSejxfs(String sejxfs) {
        this.sejxfs = sejxfs;
    }

    public BigDecimal getRiskpe() {
        return riskpe;
    }

    public void setRiskpe(BigDecimal riskpe) {
        this.riskpe = riskpe;
    }

    public BigDecimal getRiskam() {
        return riskam;
    }

    public void setRiskam(BigDecimal riskam) {
        this.riskam = riskam;
    }

    public BigDecimal getSxflll() {
        return sxflll;
    }

    public void setSxflll(BigDecimal sxflll) {
        this.sxflll = sxflll;
    }

    public BigDecimal getSxfamt() {
        return sxfamt;
    }

    public void setSxfamt(BigDecimal sxfamt) {
        this.sxfamt = sxfamt;
    }

    public String getSfzkdl() {
        return sfzkdl;
    }

    public void setSfzkdl(String sfzkdl) {
        this.sfzkdl = sfzkdl;
    }

    public String getDlhnam() {
        return dlhnam;
    }

    public void setDlhnam(String dlhnam) {
        this.dlhnam = dlhnam;
    }

    public String getPronam() {
        return pronam;
    }

    public void setPronam(String pronam) {
        this.pronam = pronam;
    }

    public BigDecimal getProamt() {
        return proamt;
    }

    public void setProamt(BigDecimal proamt) {
        this.proamt = proamt;
    }

    public String getCobsbh() {
        return cobsbh;
    }

    public void setCobsbh(String cobsbh) {
        this.cobsbh = cobsbh;
    }

    public BigDecimal getXgmyam() {
        return xgmyam;
    }

    public void setXgmyam(BigDecimal xgmyam) {
        this.xgmyam = xgmyam;
    }

    public String getSrrnam() {
        return srrnam;
    }

    public void setSrrnam(String srrnam) {
        this.srrnam = srrnam;
    }

    public String getSyradd() {
        return syradd;
    }

    public void setSyradd(String syradd) {
        this.syradd = syradd;
    }

    public String getSyrkhh() {
        return syrkhh;
    }

    public void setSyrkhh(String syrkhh) {
        this.syrkhh = syrkhh;
    }

    public String getSrracc() {
        return srracc;
    }

    public void setSrracc(String srracc) {
        this.srracc = srracc;
    }

    public String getBhfkfs() {
        return bhfkfs;
    }

    public void setBhfkfs(String bhfkfs) {
        this.bhfkfs = bhfkfs;
    }

    public String getCftjsm() {
        return cftjsm;
    }

    public void setCftjsm(String cftjsm) {
        this.cftjsm = cftjsm;
    }

    public String getContst() {
        return contst;
    }

    public void setContst(String contst) {
        this.contst = contst;
    }

    public String getInpuid() {
        return inpuid;
    }

    public void setInpuid(String inpuid) {
        this.inpuid = inpuid;
    }

    public String getManaid() {
        return manaid;
    }

    public void setManaid(String manaid) {
        this.manaid = manaid;
    }

    public String getMabrid() {
        return mabrid;
    }

    public void setMabrid(String mabrid) {
        this.mabrid = mabrid;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "custid='" + custid + '\'' +
                ", custna='" + custna + '\'' +
                ", lmtamt=" + lmtamt +
                ", kylmam=" + kylmam +
                ", contno='" + contno + '\'' +
                ", cncono='" + cncono + '\'' +
                ", prdcod='" + prdcod + '\'' +
                ", bhtype='" + bhtype + '\'' +
                ", asmean='" + asmean + '\'' +
                ", curren='" + curren + '\'' +
                ", hlrate=" + hlrate +
                ", conamt=" + conamt +
                ", zsramt=" + zsramt +
                ", stdate='" + stdate + '\'' +
                ", eddate='" + eddate + '\'' +
                ", litind='" + litind + '\'' +
                ", litzno='" + litzno + '\'' +
                ", lixyno='" + lixyno + '\'' +
                ", seccur='" + seccur + '\'' +
                ", sechlr=" + sechlr +
                ", secamt=" + secamt +
                ", sezamt=" + sezamt +
                ", secper=" + secper +
                ", sejxfs='" + sejxfs + '\'' +
                ", riskpe=" + riskpe +
                ", riskam=" + riskam +
                ", sxflll=" + sxflll +
                ", sxfamt=" + sxfamt +
                ", sfzkdl='" + sfzkdl + '\'' +
                ", dlhnam='" + dlhnam + '\'' +
                ", pronam='" + pronam + '\'' +
                ", proamt=" + proamt +
                ", cobsbh='" + cobsbh + '\'' +
                ", xgmyam=" + xgmyam +
                ", srrnam='" + srrnam + '\'' +
                ", syradd='" + syradd + '\'' +
                ", syrkhh='" + syrkhh + '\'' +
                ", srracc='" + srracc + '\'' +
                ", bhfkfs='" + bhfkfs + '\'' +
                ", cftjsm='" + cftjsm + '\'' +
                ", contst='" + contst + '\'' +
                ", inpuid='" + inpuid + '\'' +
                ", manaid='" + manaid + '\'' +
                ", mabrid='" + mabrid + '\'' +
                ", billno='" + billno + '\'' +
                '}';
    }
}
