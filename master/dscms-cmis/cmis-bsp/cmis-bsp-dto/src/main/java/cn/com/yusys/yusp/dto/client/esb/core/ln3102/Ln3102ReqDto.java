package cn.com.yusys.yusp.dto.client.esb.core.ln3102;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款期供查询试算
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3102ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "qishiqsh")
    private Integer qishiqsh;//起始期数
    @JsonProperty(value = "zhzhiqsh")
    private Integer zhzhiqsh;//终止期数
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "zhzhriqi")
    private String zhzhriqi;//终止日期
    @JsonProperty(value = "jixiqish")
    private Integer jixiqish;//计息期数
    @JsonProperty(value = "benqizht")
    private String benqizht;//本期状态
    @JsonProperty(value = "shisriqi")
    private String shisriqi;//试算日期
    @JsonProperty(value = "chaxunzl")
    private String chaxunzl;//查询种类
    @JsonProperty(value = "shifoudy")
    private String shifoudy;//是否打印


    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public Integer getQishiqsh() {
        return qishiqsh;
    }

    public void setQishiqsh(Integer qishiqsh) {
        this.qishiqsh = qishiqsh;
    }

    public Integer getZhzhiqsh() {
        return zhzhiqsh;
    }

    public void setZhzhiqsh(Integer zhzhiqsh) {
        this.zhzhiqsh = zhzhiqsh;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getZhzhriqi() {
        return zhzhriqi;
    }

    public void setZhzhriqi(String zhzhriqi) {
        this.zhzhriqi = zhzhriqi;
    }

    public Integer getJixiqish() {
        return jixiqish;
    }

    public void setJixiqish(Integer jixiqish) {
        this.jixiqish = jixiqish;
    }

    public String getBenqizht() {
        return benqizht;
    }

    public void setBenqizht(String benqizht) {
        this.benqizht = benqizht;
    }

    public String getShisriqi() {
        return shisriqi;
    }

    public void setShisriqi(String shisriqi) {
        this.shisriqi = shisriqi;
    }

    public String getChaxunzl() {
        return chaxunzl;
    }

    public void setChaxunzl(String chaxunzl) {
        this.chaxunzl = chaxunzl;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    @Override
    public String toString() {
        return "Ln3102ReqDto{" +
                "dkzhangh='" + dkzhangh + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "qishiqsh='" + qishiqsh + '\'' +
                "zhzhiqsh='" + zhzhiqsh + '\'' +
                "qishibis='" + qishibis + '\'' +
                "chxunbis='" + chxunbis + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "zhzhriqi='" + zhzhriqi + '\'' +
                "jixiqish='" + jixiqish + '\'' +
                "benqizht='" + benqizht + '\'' +
                "shisriqi='" + shisriqi + '\'' +
                "chaxunzl='" + chaxunzl + '\'' +
                "shifoudy='" + shifoudy + '\'' +
                '}';
    }
}  
