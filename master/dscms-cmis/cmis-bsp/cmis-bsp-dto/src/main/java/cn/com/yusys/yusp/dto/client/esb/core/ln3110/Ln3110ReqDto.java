package cn.com.yusys.yusp.dto.client.esb.core.ln3110;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：用于贷款放款前进行还款计划试算
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3110ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "daikjine")
    private BigDecimal daikjine;//贷款金额
    @JsonProperty(value = "sftsdkbz")
    private String sftsdkbz;//是否特殊贷款标志
    @JsonProperty(value = "tsdkjxqs")
    private Integer tsdkjxqs;//特殊贷款计息总期数
    @JsonProperty(value = "jixiguiz")
    private String jixiguiz;//计息规则
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "hkzhouqi")
    private String hkzhouqi;//还款周期
    @JsonProperty(value = "hkqixian")
    private String hkqixian;//还款期限(月)
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "qigscfsh")
    private String qigscfsh;//期供生成方式
    @JsonProperty(value = "qglxleix")
    private String qglxleix;//期供利息类型
    @JsonProperty(value = "dechligz")
    private String dechligz;//等额处理规则
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "baoliuje")
    private BigDecimal baoliuje;//保留金额
    @JsonProperty(value = "kuanxiqi")
    private Integer kuanxiqi;//宽限期
    @JsonProperty(value = "leijinzh")
    private BigDecimal leijinzh;//累进值
    @JsonProperty(value = "leijqjsh")
    private Integer leijqjsh;//累进区间期数
    @JsonProperty(value = "qishibis")
    private Integer qishibis;//起始笔数
    @JsonProperty(value = "chxunbis")
    private Integer chxunbis;//查询笔数
    @JsonProperty(value = "shifoudy")
    private String shifoudy;//是否打印
    @JsonProperty(value = "scihkrbz")
    private String scihkrbz;//首次还款日模式
    @JsonProperty(value = "mqihkfsh")
    private String mqihkfsh;//末期还款方式
    @JsonProperty(value = "dzhhkjih")
    private String dzhhkjih;//定制还款计划
    @JsonProperty(value = "ljsxqish")
    private Integer ljsxqish;//累进首段期数
    @JsonProperty(value = "lstdkhbjh")
    private List<Lstdkhbjh> lstdkhbjh;//贷款还本计划
    @JsonProperty(value = "lstdzqgjh")
    private List<Lstdzqgjh> lstdzqgjh;//贷款定制期供计划表


    public BigDecimal getDaikjine() {
        return daikjine;
    }

    public void setDaikjine(BigDecimal daikjine) {
        this.daikjine = daikjine;
    }

    public String getSftsdkbz() {
        return sftsdkbz;
    }

    public void setSftsdkbz(String sftsdkbz) {
        this.sftsdkbz = sftsdkbz;
    }

    public Integer getTsdkjxqs() {
        return tsdkjxqs;
    }

    public void setTsdkjxqs(Integer tsdkjxqs) {
        this.tsdkjxqs = tsdkjxqs;
    }

    public String getJixiguiz() {
        return jixiguiz;
    }

    public void setJixiguiz(String jixiguiz) {
        this.jixiguiz = jixiguiz;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getHkzhouqi() {
        return hkzhouqi;
    }

    public void setHkzhouqi(String hkzhouqi) {
        this.hkzhouqi = hkzhouqi;
    }

    public String getHkqixian() {
        return hkqixian;
    }

    public void setHkqixian(String hkqixian) {
        this.hkqixian = hkqixian;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public String getQigscfsh() {
        return qigscfsh;
    }

    public void setQigscfsh(String qigscfsh) {
        this.qigscfsh = qigscfsh;
    }

    public String getQglxleix() {
        return qglxleix;
    }

    public void setQglxleix(String qglxleix) {
        this.qglxleix = qglxleix;
    }

    public String getDechligz() {
        return dechligz;
    }

    public void setDechligz(String dechligz) {
        this.dechligz = dechligz;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public BigDecimal getBaoliuje() {
        return baoliuje;
    }

    public void setBaoliuje(BigDecimal baoliuje) {
        this.baoliuje = baoliuje;
    }

    public Integer getKuanxiqi() {
        return kuanxiqi;
    }

    public void setKuanxiqi(Integer kuanxiqi) {
        this.kuanxiqi = kuanxiqi;
    }

    public BigDecimal getLeijinzh() {
        return leijinzh;
    }

    public void setLeijinzh(BigDecimal leijinzh) {
        this.leijinzh = leijinzh;
    }

    public Integer getLeijqjsh() {
        return leijqjsh;
    }

    public void setLeijqjsh(Integer leijqjsh) {
        this.leijqjsh = leijqjsh;
    }

    public Integer getQishibis() {
        return qishibis;
    }

    public void setQishibis(Integer qishibis) {
        this.qishibis = qishibis;
    }

    public Integer getChxunbis() {
        return chxunbis;
    }

    public void setChxunbis(Integer chxunbis) {
        this.chxunbis = chxunbis;
    }

    public String getShifoudy() {
        return shifoudy;
    }

    public void setShifoudy(String shifoudy) {
        this.shifoudy = shifoudy;
    }

    public String getScihkrbz() {
        return scihkrbz;
    }

    public void setScihkrbz(String scihkrbz) {
        this.scihkrbz = scihkrbz;
    }

    public String getMqihkfsh() {
        return mqihkfsh;
    }

    public void setMqihkfsh(String mqihkfsh) {
        this.mqihkfsh = mqihkfsh;
    }

    public String getDzhhkjih() {
        return dzhhkjih;
    }

    public void setDzhhkjih(String dzhhkjih) {
        this.dzhhkjih = dzhhkjih;
    }

    public Integer getLjsxqish() {
        return ljsxqish;
    }

    public void setLjsxqish(Integer ljsxqish) {
        this.ljsxqish = ljsxqish;
    }

    public List<Lstdkhbjh> getLstdkhbjh() {
        return lstdkhbjh;
    }

    public void setLstdkhbjh(List<Lstdkhbjh> lstdkhbjh) {
        this.lstdkhbjh = lstdkhbjh;
    }

    public List<Lstdzqgjh> getLstdzqgjh() {
        return lstdzqgjh;
    }

    public void setLstdzqgjh(List<Lstdzqgjh> lstdzqgjh) {
        this.lstdzqgjh = lstdzqgjh;
    }

    @Override
    public String toString() {
        return "Ln3110ReqDto{" +
                ", daikjine=" + daikjine +
                ", sftsdkbz='" + sftsdkbz + '\'' +
                ", tsdkjxqs=" + tsdkjxqs +
                ", jixiguiz='" + jixiguiz + '\'' +
                ", zhchlilv=" + zhchlilv +
                ", hkzhouqi='" + hkzhouqi + '\'' +
                ", hkqixian='" + hkqixian + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", qigscfsh='" + qigscfsh + '\'' +
                ", qglxleix='" + qglxleix + '\'' +
                ", dechligz='" + dechligz + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", baoliuje=" + baoliuje +
                ", kuanxiqi=" + kuanxiqi +
                ", leijinzh=" + leijinzh +
                ", leijqjsh=" + leijqjsh +
                ", qishibis=" + qishibis +
                ", chxunbis=" + chxunbis +
                ", shifoudy='" + shifoudy + '\'' +
                ", scihkrbz='" + scihkrbz + '\'' +
                ", mqihkfsh='" + mqihkfsh + '\'' +
                ", dzhhkjih='" + dzhhkjih + '\'' +
                ", ljsxqish=" + ljsxqish +
                ", lstdkhbjh=" + lstdkhbjh +
                ", lstdzqgjh=" + lstdzqgjh +
                '}';
    }
}
