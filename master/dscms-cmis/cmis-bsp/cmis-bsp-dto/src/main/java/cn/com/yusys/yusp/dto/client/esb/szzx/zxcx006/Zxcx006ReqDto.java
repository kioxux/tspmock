package cn.com.yusys.yusp.dto.client.esb.szzx.zxcx006;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：信贷查询地方征信接口
 *
 * @author hjk
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Zxcx006ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "state")
    private String state;//	请求类型	,	是	,	new：请求、old：查询
    @JsonProperty(value = "reportReason")
    private String reportReason;//	查询原因	,	是	,	1贷前、2贷后、3贷中、4关联、5行政
    @JsonProperty(value = "querystaffNo")
    private String querystaffNo;//	客户经理工号	,	是	,
    @JsonProperty(value = "querystaff")
    private String querystaff;//	客户经理名称	,	是	,
    @JsonProperty(value = "inqueryorgNo")
    private String inqueryorgNo;//	查询机构号	,	是	,	应与上方部门号一致
    @JsonProperty(value = "inqueryorg")
    private String inqueryorg;//	查询机构名称	,	是	,
    @JsonProperty(value = "taskId")
    private String taskId;//	任务ID	,	否	,	state为new时，可为空；state为old时，必输
    @JsonProperty(value = "cardCode")
    private String cardCode;//	证件号码	,	是	,
    @JsonProperty(value = "wsUser")
    private String wsUser;//	征信提供的用户名	,	是	,	测试环境：ZJGNS-ZXD；生产环境：zjgns-ssjk

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReportReason() {
        return reportReason;
    }

    public void setReportReason(String reportReason) {
        this.reportReason = reportReason;
    }

    public String getQuerystaffNo() {
        return querystaffNo;
    }

    public void setQuerystaffNo(String querystaffNo) {
        this.querystaffNo = querystaffNo;
    }

    public String getQuerystaff() {
        return querystaff;
    }

    public void setQuerystaff(String querystaff) {
        this.querystaff = querystaff;
    }

    public String getInqueryorgNo() {
        return inqueryorgNo;
    }

    public void setInqueryorgNo(String inqueryorgNo) {
        this.inqueryorgNo = inqueryorgNo;
    }

    public String getInqueryorg() {
        return inqueryorg;
    }

    public void setInqueryorg(String inqueryorg) {
        this.inqueryorg = inqueryorg;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getCardCode() {
        return cardCode;
    }

    public void setCardCode(String cardCode) {
        this.cardCode = cardCode;
    }

    public String getWsUser() {
        return wsUser;
    }

    public void setWsUser(String wsUser) {
        this.wsUser = wsUser;
    }

    @Override
    public String toString() {
        return "Zxcx006ReqDto{" +
                "state='" + state + '\'' +
                ", reportReason='" + reportReason + '\'' +
                ", querystaffNo='" + querystaffNo + '\'' +
                ", querystaff='" + querystaff + '\'' +
                ", inqueryorgNo='" + inqueryorgNo + '\'' +
                ", inqueryorg='" + inqueryorg + '\'' +
                ", taskId='" + taskId + '\'' +
                ", cardCode='" + cardCode + '\'' +
                ", wsUser='" + wsUser + '\'' +
                '}';
    }
}
