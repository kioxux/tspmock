package cn.com.yusys.yusp.dto.server.biz.xdht0045.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同号

    @JsonProperty(value = "contSt")
    private String contSt;//合同状态

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getContSt() {
        return contSt;
    }

    public void setContSt(String contSt) {
        this.contSt = contSt;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                "contSt='" + contSt + '\'' +
                '}';
    }
}
