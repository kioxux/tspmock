package cn.com.yusys.yusp.dto.client.http.outerdata.sxbzxr.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：失信被执行人查询接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class SxbzxrReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "type")
    private String type;//查询类型
    @JsonProperty(value = "name")
    private String name;//姓名/企业名称
    @JsonProperty(value = "qyid")
    private String qyid;//身份证号/组织机构代码

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQyid() {
        return qyid;
    }

    public void setQyid(String qyid) {
        this.qyid = qyid;
    }

    @Override
    public String toString() {
        return "SxbzxrReqDto{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", qyid='" + qyid + '\'' +
                '}';
    }
}
