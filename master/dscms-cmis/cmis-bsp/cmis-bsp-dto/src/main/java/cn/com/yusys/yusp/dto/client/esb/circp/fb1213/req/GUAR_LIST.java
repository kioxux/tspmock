package cn.com.yusys.yusp.dto.client.esb.circp.fb1213.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 */
@JsonPropertyOrder(alphabetic = true)
public class GUAR_LIST implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "GUAR_NO")
    private String GUAR_NO;//押品编号

    @JsonIgnore
    public String getGUAR_NO() {
        return GUAR_NO;
    }

    @JsonIgnore
    public void setGUAR_NO(String GUAR_NO) {
        this.GUAR_NO = GUAR_NO;
    }

    @Override
    public String toString() {
        return "GUAR_LIST{" +
                "GUAR_NO='" + GUAR_NO + '\'' +
                '}';
    }
}
