package cn.com.yusys.yusp.dto.server.biz.xdxw0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：小微贷前调查信息查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "managerId")
    private String managerId;//客户经理编号
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "surveyType")
    private String surveyType;//调查表类型
    @JsonProperty(value = "cusName")
    private String cusName;//客户名
    @JsonProperty(value = "certNo")
    private String certNo;//客户证件号
    @JsonProperty(value = "pageNo")
    private String pageNo;//页码
    @JsonProperty(value = "pageSize")
    private String pageSize;//每页条数

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getPageNo() {
        return pageNo;
    }

    public void setPageNo(String pageNo) {
        this.pageNo = pageNo;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "Data{" +
                "managerId='" + managerId + '\'' +
                ", queryType='" + queryType + '\'' +
                ", surveyType='" + surveyType + '\'' +
                ", cusName='" + cusName + '\'' +
                ", certNo='" + certNo + '\'' +
                ", pageNo='" + pageNo + '\'' +
                ", pageSize='" + pageSize + '\'' +
                '}';
    }
}
