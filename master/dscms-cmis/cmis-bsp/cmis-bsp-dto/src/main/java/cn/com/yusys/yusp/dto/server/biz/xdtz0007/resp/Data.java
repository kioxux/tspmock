package cn.com.yusys.yusp.dto.server.biz.xdtz0007.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：根据客户号获取非信用方式发放贷款的最长到期日
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "loanEndDate")
    private String loanEndDate;//贷款到期日

    public String getLoanEndDate() {
        return loanEndDate;
    }

    public void setLoanEndDate(String loanEndDate) {
        this.loanEndDate = loanEndDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "loanEndDate='" + loanEndDate + '\'' +
                '}';
    }
}
