package cn.com.yusys.yusp.dto.client.esb.ypxt.bdcqcx;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
/**
 * 响应DTO：不动产权证查询接口
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:12:41
 */
@JsonPropertyOrder(alphabetic = true)
public class BdcqcxRespDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarno")
    private String guarno;// 押品编号 否
    @JsonProperty(value = "bdcnoo")
    private String bdcnoo;// 不动产权证号 否
    @JsonProperty(value = "bdqxdm")
    private String bdqxdm;// 所属区县-代码 否
    @JsonProperty(value = "bdqxmc")
    private String bdqxmc;// 所属区县-名称 否
    @JsonProperty(value = "bdqzll")
    private String bdqzll;// 坐落 否
    @JsonProperty(value = "bdcdyh")
    private String bdcdyh;// 房屋-不动产单元号 否
    @JsonProperty(value = "bdcqzs")
    private String bdcqzs;// 房屋-产权证书号 否
    @JsonProperty(value = "bdcqjc")
    private String bdcqjc;// 房屋-产权证书号简称 否
    @JsonProperty(value = "bdcqmj")
    private BigDecimal bdcqmj;// 房屋-产权面积 否
    @JsonProperty(value = "bdytdm")
    private String bdytdm;// 房屋-用途-代码 否
    @JsonProperty(value = "bdytmc")
    private String bdytmc;// 房屋-用途-名称 否
    @JsonProperty(value = "tdzhno")
    private String tdzhno;// 土地-证号 否
    @JsonProperty(value = "tdaera")
    private BigDecimal tdaera;// 土地-面积 否
    @JsonProperty(value = "tdytdm")
    private String tdytdm;// 土地-用途-代码 否
    @JsonProperty(value = "tdytmc")
    private String tdytmc;// 土地-用途-名称 否

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    public String getBdcnoo() {
        return bdcnoo;
    }

    public void setBdcnoo(String bdcnoo) {
        this.bdcnoo = bdcnoo;
    }

    public String getBdqxdm() {
        return bdqxdm;
    }

    public void setBdqxdm(String bdqxdm) {
        this.bdqxdm = bdqxdm;
    }

    public String getBdqxmc() {
        return bdqxmc;
    }

    public void setBdqxmc(String bdqxmc) {
        this.bdqxmc = bdqxmc;
    }

    public String getBdqzll() {
        return bdqzll;
    }

    public void setBdqzll(String bdqzll) {
        this.bdqzll = bdqzll;
    }

    public String getBdcdyh() {
        return bdcdyh;
    }

    public void setBdcdyh(String bdcdyh) {
        this.bdcdyh = bdcdyh;
    }

    public String getBdcqzs() {
        return bdcqzs;
    }

    public void setBdcqzs(String bdcqzs) {
        this.bdcqzs = bdcqzs;
    }

    public String getBdcqjc() {
        return bdcqjc;
    }

    public void setBdcqjc(String bdcqjc) {
        this.bdcqjc = bdcqjc;
    }

    public BigDecimal getBdcqmj() {
        return bdcqmj;
    }

    public void setBdcqmj(BigDecimal bdcqmj) {
        this.bdcqmj = bdcqmj;
    }

    public String getBdytdm() {
        return bdytdm;
    }

    public void setBdytdm(String bdytdm) {
        this.bdytdm = bdytdm;
    }

    public String getBdytmc() {
        return bdytmc;
    }

    public void setBdytmc(String bdytmc) {
        this.bdytmc = bdytmc;
    }

    public String getTdzhno() {
        return tdzhno;
    }

    public void setTdzhno(String tdzhno) {
        this.tdzhno = tdzhno;
    }

    public BigDecimal getTdaera() {
        return tdaera;
    }

    public void setTdaera(BigDecimal tdaera) {
        this.tdaera = tdaera;
    }

    public String getTdytdm() {
        return tdytdm;
    }

    public void setTdytdm(String tdytdm) {
        this.tdytdm = tdytdm;
    }

    public String getTdytmc() {
        return tdytmc;
    }

    public void setTdytmc(String tdytmc) {
        this.tdytmc = tdytmc;
    }

    @Override
    public String toString() {
        return "BdcqcxRespDto{" +
                ", guarno='" + guarno + '\'' +
                ", bdcnoo='" + bdcnoo + '\'' +
                ", bdqxdm='" + bdqxdm + '\'' +
                ", bdqxmc='" + bdqxmc + '\'' +
                ", bdqzll='" + bdqzll + '\'' +
                ", bdcdyh='" + bdcdyh + '\'' +
                ", bdcqzs='" + bdcqzs + '\'' +
                ", bdcqjc='" + bdcqjc + '\'' +
                ", bdcqmj=" + bdcqmj +
                ", bdytdm='" + bdytdm + '\'' +
                ", bdytmc='" + bdytmc + '\'' +
                ", tdzhno='" + tdzhno + '\'' +
                ", tdaera=" + tdaera +
                ", tdytdm='" + tdytdm + '\'' +
                ", tdytmc='" + tdytmc + '\'' +
                '}';
    }
}
