package cn.com.yusys.yusp.dto.server.biz.xdsx0023.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/17 11:26:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/17 11:26
 * @since 2021/5/17 11:26
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certno")
    private String certno;//组织机构代码
    @JsonProperty(value = "queflg")
    private String queflg;//查询条件

    public String getCertno() {
        return certno;
    }

    public void setCertno(String certno) {
        this.certno = certno;
    }

    public String getQueflg() {
        return queflg;
    }

    public void setQueflg(String queflg) {
        this.queflg = queflg;
    }

    @Override
    public String toString() {
        return "Data{" +
                "certno='" + certno + '\'' +
                ", queflg='" + queflg + '\'' +
                '}';
    }
}
