package cn.com.yusys.yusp.dto.server.cfg.xdxt0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/25 9:48:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 9:48
 * @since 2021/5/25 9:48
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cnname1")
    private String cnname1;//国标地区编码字典中文名
    @JsonProperty(value = "cnname2")
    private String cnname2;//国标地区编码字典中文名

    public String getCnname1() {
        return cnname1;
    }

    public void setCnname1(String cnname1) {
        this.cnname1 = cnname1;
    }

    public String getCnname2() {
        return cnname2;
    }

    public void setCnname2(String cnname2) {
        this.cnname2 = cnname2;
    }

    @Override
    public String toString() {
        return "Xdxt0012ReqDto{" +
                "cnname1='" + cnname1 + '\'' +
                "cnname2='" + cnname2 + '\'' +
                '}';
    }
}
