package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：承兑签发审批结果综合服务接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj004ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pcSerno")
    private String pcSerno;//批次号
    @JsonProperty(value = "handSecurityAmo")
    private String handSecurityAmo;//已交保证金金额
    @JsonProperty(value = "accountStatus")
    private String accountStatus;//出账状态
    @JsonProperty(value = "isdraftpool")
    private String isdraftpool;//票据池出票标记

    public String getPcSerno() {
        return pcSerno;
    }

    public void setPcSerno(String pcSerno) {
        this.pcSerno = pcSerno;
    }

    public String getHandSecurityAmo() {
        return handSecurityAmo;
    }

    public void setHandSecurityAmo(String handSecurityAmo) {
        this.handSecurityAmo = handSecurityAmo;
    }

    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getIsdraftpool() {
        return isdraftpool;
    }

    public void setIsdraftpool(String isdraftpool) {
        this.isdraftpool = isdraftpool;
    }

    @Override
    public String toString() {
        return "Xdpj004ReqDto{" +
                "pcSerno='" + pcSerno + '\'' +
                "handSecurityAmo='" + handSecurityAmo + '\'' +
                "accountStatus='" + accountStatus + '\'' +
                "isdraftpool='" + isdraftpool + '\'' +
                '}';
    }
}  
