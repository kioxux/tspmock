package cn.com.yusys.yusp.dto.server.cus.xdkh0025.resp;

import java.io.Serializable;

/**
 * 响应Dto：优企贷、优农贷客户信息查询（不带下划线）
 *
 * @author 徐超
 * @version 1.0
 */
public class Xdkh0025RespDto4Null implements Serializable {
    private static final long serialVersionUID = 1L;
    private String cusId;//客户编号
    private String cusName;//客户名称
    private String comInitLoanDate;//建立信贷关系时间
    private String comInsCode;//组织机构代码
    private String basAccFlg;//基本存款账户是否在本机构
    private String basAccDate;//基本存款账户开户日期

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getComInitLoanDate() {
        return comInitLoanDate;
    }

    public void setComInitLoanDate(String comInitLoanDate) {
        this.comInitLoanDate = comInitLoanDate;
    }

    public String getComInsCode() {
        return comInsCode;
    }

    public void setComInsCode(String comInsCode) {
        this.comInsCode = comInsCode;
    }

    public String getBasAccFlg() {
        return basAccFlg;
    }

    public void setBasAccFlg(String basAccFlg) {
        this.basAccFlg = basAccFlg;
    }

    public String getBasAccDate() {
        return basAccDate;
    }

    public void setBasAccDate(String basAccDate) {
        this.basAccDate = basAccDate;
    }
}
