package cn.com.yusys.yusp.dto.server.biz.xddh0006.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/22 15:08:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/22 15:08
 * @since 2021/5/22 15:08
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "advRepaymentAmt")
    private BigDecimal advRepaymentAmt;//提前还本金额
    @JsonProperty(value = "recTotlCap")
    private BigDecimal recTotlCap;//应还总本金
    @JsonProperty(value = "recNormalInt")
    private BigDecimal recNormalInt;//应还正常利息
    @JsonProperty(value = "recCi")
    private BigDecimal recCi;//应还复利
    @JsonProperty(value = "recOdint")
    private BigDecimal recOdint;//应还罚息
    @JsonProperty(value = "advRepayInt")
    private BigDecimal advRepayInt;//提前还款利息

    public BigDecimal getAdvRepaymentAmt() {
        return advRepaymentAmt;
    }

    public void setAdvRepaymentAmt(BigDecimal advRepaymentAmt) {
        this.advRepaymentAmt = advRepaymentAmt;
    }

    public BigDecimal getRecTotlCap() {
        return recTotlCap;
    }

    public void setRecTotlCap(BigDecimal recTotlCap) {
        this.recTotlCap = recTotlCap;
    }

    public BigDecimal getRecNormalInt() {
        return recNormalInt;
    }

    public void setRecNormalInt(BigDecimal recNormalInt) {
        this.recNormalInt = recNormalInt;
    }

    public BigDecimal getRecCi() {
        return recCi;
    }

    public void setRecCi(BigDecimal recCi) {
        this.recCi = recCi;
    }

    public BigDecimal getRecOdint() {
        return recOdint;
    }

    public void setRecOdint(BigDecimal recOdint) {
        this.recOdint = recOdint;
    }

    public BigDecimal getAdvRepayInt() {
        return advRepayInt;
    }

    public void setAdvRepayInt(BigDecimal advRepayInt) {
        this.advRepayInt = advRepayInt;
    }

    @Override
    public String toString() {
        return "Xddh0006RespDto{" +
                "advRepaymentAmt='" + advRepaymentAmt + '\'' +
                "recTotlCap='" + recTotlCap + '\'' +
                "recNormalInt='" + recNormalInt + '\'' +
                "recCi='" + recCi + '\'' +
                "recOdint='" + recOdint + '\'' +
                "advRepayInt='" + advRepayInt + '\'' +
                '}';
    }
}
