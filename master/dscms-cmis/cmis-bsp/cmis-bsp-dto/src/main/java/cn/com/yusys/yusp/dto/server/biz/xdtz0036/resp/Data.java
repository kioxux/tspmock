package cn.com.yusys.yusp.dto.server.biz.xdtz0036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/21 16:55:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 16:55
 * @since 2021/5/21 16:55
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "overdueTimes")
    private String overdueTimes;//逾期次数

    public String getOverdueTimes() {
        return overdueTimes;
    }

    public void setOverdueTimes(String overdueTimes) {
        this.overdueTimes = overdueTimes;
    }

    @Override
    public String toString() {
        return "Xdtz0036RespDto{" +
                "overdueTimes='" + overdueTimes + '\'' +
                '}';
    }
}
