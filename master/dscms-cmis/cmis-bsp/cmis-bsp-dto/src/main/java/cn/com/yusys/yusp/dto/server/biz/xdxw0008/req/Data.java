package cn.com.yusys.yusp.dto.server.biz.xdxw0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "rqstrName")
    private String rqstrName;//申请人姓名
    @JsonProperty(value = "rqstrCertNo")
    private String rqstrCertNo;//申请人证件号
    @JsonProperty(value = "rqstrMobile")
    private String rqstrMobile;//申请人手机号
    @JsonProperty(value = "actOperAddr")
    private String actOperAddr;//经营地址
    @JsonProperty(value = "applyAmt")
    private BigDecimal applyAmt;//申请金额
    @JsonProperty(value = "applyTerm")
    private BigDecimal applyTerm;//申请期限
    @JsonProperty(value = "saleManagerId")
    private String saleManagerId;//营销客户经理编号

    public String getRqstrName() {
        return rqstrName;
    }

    public void setRqstrName(String rqstrName) {
        this.rqstrName = rqstrName;
    }

    public String getRqstrCertNo() {
        return rqstrCertNo;
    }

    public void setRqstrCertNo(String rqstrCertNo) {
        this.rqstrCertNo = rqstrCertNo;
    }

    public String getRqstrMobile() {
        return rqstrMobile;
    }

    public void setRqstrMobile(String rqstrMobile) {
        this.rqstrMobile = rqstrMobile;
    }

    public String getActOperAddr() {
        return actOperAddr;
    }

    public void setActOperAddr(String actOperAddr) {
        this.actOperAddr = actOperAddr;
    }

    public BigDecimal getApplyAmt() {
        return applyAmt;
    }

    public void setApplyAmt(BigDecimal applyAmt) {
        this.applyAmt = applyAmt;
    }

    public BigDecimal getApplyTerm() {
        return applyTerm;
    }

    public void setApplyTerm(BigDecimal applyTerm) {
        this.applyTerm = applyTerm;
    }

    public String getSaleManagerId() {
        return saleManagerId;
    }

    public void setSaleManagerId(String saleManagerId) {
        this.saleManagerId = saleManagerId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "rqstrName='" + rqstrName + '\'' +
                ", rqstrCertNo='" + rqstrCertNo + '\'' +
                ", rqstrMobile='" + rqstrMobile + '\'' +
                ", actOperAddr='" + actOperAddr + '\'' +
                ", applyAmt=" + applyAmt +
                ", applyTerm=" + applyTerm +
                ", saleManagerId='" + saleManagerId + '\'' +
                '}';
    }
}
