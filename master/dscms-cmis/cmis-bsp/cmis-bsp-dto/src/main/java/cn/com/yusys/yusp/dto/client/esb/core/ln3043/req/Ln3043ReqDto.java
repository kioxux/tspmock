package cn.com.yusys.yusp.dto.client.esb.core.ln3043.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：贷款指定期供归还
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3043ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "huankzle")
    private String huankzle;//还款种类
    @JsonProperty(value = "huankfsh")
    private String huankfsh;//还款方式
    @JsonProperty(value = "zdhkjine")
    private BigDecimal zdhkjine;//指定还款金额
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "rzzdkouk")
    private String rzzdkouk;//是否日终自动扣款
    @JsonProperty(value = "zjinjizh")
    private String zjinjizh;//资金来源记账标志
    @JsonProperty(value = "hkjinelb")
    private String hkjinelb;//还款金额类别
    @JsonProperty(value = "meiqhkze")
    private BigDecimal meiqhkze;//每期还款总额
    @JsonProperty(value = "meiqhbje")
    private BigDecimal meiqhbje;//每期还本金额
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "sfqzjieq")
    private String sfqzjieq;//强制结清标志
    @JsonProperty(value = "daikghfs")
    private String daikghfs;//贷款归还方式
    @JsonProperty(value = "pingzhzl")
    private String pingzhzl;//凭证种类
    @JsonProperty(value = "pingzhma")
    private String pingzhma;//凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件
    @JsonProperty(value = "jiaoymma")
    private String jiaoymma;//交易密码
    @JsonProperty(value = "mimazlei")
    private String mimazlei;//密码种类
    @JsonProperty(value = "zhjnzlei")
    private String zhjnzlei;//证件种类
    @JsonProperty(value = "zhjhaoma")
    private String zhjhaoma;//证件号码
    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx;//还款账号类型
    @JsonProperty(value = "yanmjine")
    private BigDecimal yanmjine;//验密金额
    @JsonProperty(value = "tblqgmx")
    private java.util.List<Tblqgmx> tblqgmx;

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getHuankzle() {
        return huankzle;
    }

    public void setHuankzle(String huankzle) {
        this.huankzle = huankzle;
    }

    public String getHuankfsh() {
        return huankfsh;
    }

    public void setHuankfsh(String huankfsh) {
        this.huankfsh = huankfsh;
    }

    public BigDecimal getZdhkjine() {
        return zdhkjine;
    }

    public void setZdhkjine(BigDecimal zdhkjine) {
        this.zdhkjine = zdhkjine;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getRzzdkouk() {
        return rzzdkouk;
    }

    public void setRzzdkouk(String rzzdkouk) {
        this.rzzdkouk = rzzdkouk;
    }

    public String getZjinjizh() {
        return zjinjizh;
    }

    public void setZjinjizh(String zjinjizh) {
        this.zjinjizh = zjinjizh;
    }

    public String getHkjinelb() {
        return hkjinelb;
    }

    public void setHkjinelb(String hkjinelb) {
        this.hkjinelb = hkjinelb;
    }

    public BigDecimal getMeiqhkze() {
        return meiqhkze;
    }

    public void setMeiqhkze(BigDecimal meiqhkze) {
        this.meiqhkze = meiqhkze;
    }

    public BigDecimal getMeiqhbje() {
        return meiqhbje;
    }

    public void setMeiqhbje(BigDecimal meiqhbje) {
        this.meiqhbje = meiqhbje;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getSfqzjieq() {
        return sfqzjieq;
    }

    public void setSfqzjieq(String sfqzjieq) {
        this.sfqzjieq = sfqzjieq;
    }

    public String getDaikghfs() {
        return daikghfs;
    }

    public void setDaikghfs(String daikghfs) {
        this.daikghfs = daikghfs;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getJiaoymma() {
        return jiaoymma;
    }

    public void setJiaoymma(String jiaoymma) {
        this.jiaoymma = jiaoymma;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public BigDecimal getYanmjine() {
        return yanmjine;
    }

    public void setYanmjine(BigDecimal yanmjine) {
        this.yanmjine = yanmjine;
    }

    public List<Tblqgmx> getTblqgmx() {
        return tblqgmx;
    }

    public void setTblqgmx(List<Tblqgmx> tblqgmx) {
        this.tblqgmx = tblqgmx;
    }

    @Override
    public String toString() {
        return "Ln3043ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", huankzle='" + huankzle + '\'' +
                ", huankfsh='" + huankfsh + '\'' +
                ", zdhkjine=" + zdhkjine +
                ", zijnlaiy='" + zijnlaiy + '\'' +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", rzzdkouk='" + rzzdkouk + '\'' +
                ", zjinjizh='" + zjinjizh + '\'' +
                ", hkjinelb='" + hkjinelb + '\'' +
                ", meiqhkze=" + meiqhkze +
                ", meiqhbje=" + meiqhbje +
                ", kehmingc='" + kehmingc + '\'' +
                ", sfqzjieq='" + sfqzjieq + '\'' +
                ", daikghfs='" + daikghfs + '\'' +
                ", pingzhzl='" + pingzhzl + '\'' +
                ", pingzhma='" + pingzhma + '\'' +
                ", pngzxhao='" + pngzxhao + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                ", jiaoymma='" + jiaoymma + '\'' +
                ", mimazlei='" + mimazlei + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", kehuzhlx='" + kehuzhlx + '\'' +
                ", yanmjine=" + yanmjine +
                ", tblqgmx=" + tblqgmx +
                '}';
    }
}
