package cn.com.yusys.yusp.dto.client.esb.core.ln3107;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款形态转移明细查询
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3107RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "datasq")
    private String datasq;//全局流水
    @JsonProperty(value = "lstdkxtzie")
    private List<Lstdkxtzy> lstdkxtzie;

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public List<Lstdkxtzy> getLstdkxtzie() {
        return lstdkxtzie;
    }

    public void setLstdkxtzie(List<Lstdkxtzy> lstdkxtzie) {
        this.lstdkxtzie = lstdkxtzie;
    }

    @Override
    public String toString() {
        return "Ln3107RespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                ", servsq='" + servsq + '\'' +
                ", datasq='" + datasq + '\'' +
                ", lstdkxtzie=" + lstdkxtzie +
                '}';
    }
}
