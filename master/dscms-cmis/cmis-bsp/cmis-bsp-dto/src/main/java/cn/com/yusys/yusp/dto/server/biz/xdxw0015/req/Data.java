package cn.com.yusys.yusp.dto.server.biz.xdxw0015.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询客户总行经营性贷款是否存在贷款余额或有效批复或有效合同或授信审批状态为“审批中”
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "certType")
    private String certType;//证件类型

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", certNo='" + certNo + '\'' +
                ", certType='" + certType + '\'' +
                '}';
    }
}
