package cn.com.yusys.yusp.dto.server.biz.xdqt0009.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：微业贷信贷文件处理通知
 *
 * @author zrcbank-fengjj
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdqt0009RespDto extends TradeServerRespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "data")
	private Data data;//data

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xdqt0009RespDto{" +
				"data=" + data +
				'}';
	}
}  
