package cn.com.yusys.yusp.dto.server.biz.xdtz0029.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @Author zhangpeng
 * @Date 2021/4/28 21:25
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "porderNo")
    private String porderNo;//汇票号码

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "porderNo='" + porderNo + '\'' +
                '}';
    }
}
