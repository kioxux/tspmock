package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	股权出质信息（新）
@JsonPropertyOrder(alphabetic = true)
public class STOCKPAWN implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "STK_PAWN_CZAMT")
    private String STK_PAWN_CZAMT;//	出质股权数额
    @JsonProperty(value = "STK_PAWN_CZCERNO")
    private String STK_PAWN_CZCERNO;//	出质人证件/证件号
    @JsonProperty(value = "STK_PAWN_CZPER")
    private String STK_PAWN_CZPER;//	出质人
    @JsonProperty(value = "STK_PAWN_DATE")
    private String STK_PAWN_DATE;//	公示日期
    @JsonProperty(value = "STK_PAWN_REGDATE")
    private String STK_PAWN_REGDATE;//	质权出质设立登记日期
    @JsonProperty(value = "STK_PAWN_REGNO")
    private String STK_PAWN_REGNO;//	登记编号
    @JsonProperty(value = "STK_PAWN_STATUS")
    private String STK_PAWN_STATUS;//	状态
    @JsonProperty(value = "STK_PAWN_ZQCERNO")
    private String STK_PAWN_ZQCERNO;//	质权人证件/证件号
    @JsonProperty(value = "STK_PAWN_ZQPER")
    private String STK_PAWN_ZQPER;//	质权人姓名
    @JsonProperty(value = "URL")
    private String URL;//	关联内容

    @JsonIgnore
    public String getSTK_PAWN_CZAMT() {
        return STK_PAWN_CZAMT;
    }

    @JsonIgnore
    public void setSTK_PAWN_CZAMT(String STK_PAWN_CZAMT) {
        this.STK_PAWN_CZAMT = STK_PAWN_CZAMT;
    }

    @JsonIgnore
    public String getSTK_PAWN_CZCERNO() {
        return STK_PAWN_CZCERNO;
    }

    @JsonIgnore
    public void setSTK_PAWN_CZCERNO(String STK_PAWN_CZCERNO) {
        this.STK_PAWN_CZCERNO = STK_PAWN_CZCERNO;
    }

    @JsonIgnore
    public String getSTK_PAWN_CZPER() {
        return STK_PAWN_CZPER;
    }

    @JsonIgnore
    public void setSTK_PAWN_CZPER(String STK_PAWN_CZPER) {
        this.STK_PAWN_CZPER = STK_PAWN_CZPER;
    }

    @JsonIgnore
    public String getSTK_PAWN_DATE() {
        return STK_PAWN_DATE;
    }

    @JsonIgnore
    public void setSTK_PAWN_DATE(String STK_PAWN_DATE) {
        this.STK_PAWN_DATE = STK_PAWN_DATE;
    }

    @JsonIgnore
    public String getSTK_PAWN_REGDATE() {
        return STK_PAWN_REGDATE;
    }

    @JsonIgnore
    public void setSTK_PAWN_REGDATE(String STK_PAWN_REGDATE) {
        this.STK_PAWN_REGDATE = STK_PAWN_REGDATE;
    }

    @JsonIgnore
    public String getSTK_PAWN_REGNO() {
        return STK_PAWN_REGNO;
    }

    @JsonIgnore
    public void setSTK_PAWN_REGNO(String STK_PAWN_REGNO) {
        this.STK_PAWN_REGNO = STK_PAWN_REGNO;
    }

    @JsonIgnore
    public String getSTK_PAWN_STATUS() {
        return STK_PAWN_STATUS;
    }

    @JsonIgnore
    public void setSTK_PAWN_STATUS(String STK_PAWN_STATUS) {
        this.STK_PAWN_STATUS = STK_PAWN_STATUS;
    }

    @JsonIgnore
    public String getSTK_PAWN_ZQCERNO() {
        return STK_PAWN_ZQCERNO;
    }

    @JsonIgnore
    public void setSTK_PAWN_ZQCERNO(String STK_PAWN_ZQCERNO) {
        this.STK_PAWN_ZQCERNO = STK_PAWN_ZQCERNO;
    }

    @JsonIgnore
    public String getSTK_PAWN_ZQPER() {
        return STK_PAWN_ZQPER;
    }

    @JsonIgnore
    public void setSTK_PAWN_ZQPER(String STK_PAWN_ZQPER) {
        this.STK_PAWN_ZQPER = STK_PAWN_ZQPER;
    }

    @JsonIgnore
    public String getURL() {
        return URL;
    }

    @JsonIgnore
    public void setURL(String URL) {
        this.URL = URL;
    }

    @Override
    public String toString() {
        return "STOCKPAWN{" +
                "STK_PAWN_CZAMT='" + STK_PAWN_CZAMT + '\'' +
                ", STK_PAWN_CZCERNO='" + STK_PAWN_CZCERNO + '\'' +
                ", STK_PAWN_CZPER='" + STK_PAWN_CZPER + '\'' +
                ", STK_PAWN_DATE='" + STK_PAWN_DATE + '\'' +
                ", STK_PAWN_REGDATE='" + STK_PAWN_REGDATE + '\'' +
                ", STK_PAWN_REGNO='" + STK_PAWN_REGNO + '\'' +
                ", STK_PAWN_STATUS='" + STK_PAWN_STATUS + '\'' +
                ", STK_PAWN_ZQCERNO='" + STK_PAWN_ZQCERNO + '\'' +
                ", STK_PAWN_ZQPER='" + STK_PAWN_ZQPER + '\'' +
                ", URL='" + URL + '\'' +
                '}';
    }
}
