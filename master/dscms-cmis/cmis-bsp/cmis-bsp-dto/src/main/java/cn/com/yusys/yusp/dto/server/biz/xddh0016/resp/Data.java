package cn.com.yusys.yusp.dto.server.biz.xddh0016.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 8:55:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 8:55
 * @since 2021/5/24 8:55
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opFLag")
    private String opFLag;//登记标志
    @JsonProperty(value = "opMsg")
    private String opMsg;//描述信息

    public String getOpFLag() {
        return opFLag;
    }

    public void setOpFLag(String opFLag) {
        this.opFLag = opFLag;
    }

    public String getOpMsg() {
        return opMsg;
    }

    public void setOpMsg(String opMsg) {
        this.opMsg = opMsg;
    }

    @Override
    public String toString() {
        return "Xddh0016RespDto{" +
                "opFLag='" + opFLag + '\'' +
                "opMsg='" + opMsg + '\'' +
                '}';
    }
}
