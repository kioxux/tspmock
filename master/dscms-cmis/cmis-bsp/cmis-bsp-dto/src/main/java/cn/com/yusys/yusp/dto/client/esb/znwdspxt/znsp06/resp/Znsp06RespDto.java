package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp06.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：被拒绝的线上产品推送接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp06RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Znsp06RespDto{" +
                '}';
    }
}  
