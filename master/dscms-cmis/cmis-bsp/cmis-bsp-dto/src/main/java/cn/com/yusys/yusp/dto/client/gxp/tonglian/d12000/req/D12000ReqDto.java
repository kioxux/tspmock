package cn.com.yusys.yusp.dto.client.gxp.tonglian.d12000.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：账户信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class D12000ReqDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "cardno")
	private String cardno;//卡号
	@JsonProperty(value = "crcycd")
	private String crcycd;//币种

	public String getCardno() {
		return cardno;
	}

	public void setCardno(String cardno) {
		this.cardno = cardno;
	}

	public String getCrcycd() {
		return crcycd;
	}

	public void setCrcycd(String crcycd) {
		this.crcycd = crcycd;
	}

	@Override
	public String toString() {
		return "D12000ReqDto{" +
				"cardno='" + cardno + '\'' +
				"crcycd='" + crcycd + '\'' +
				'}';
	}
}  
