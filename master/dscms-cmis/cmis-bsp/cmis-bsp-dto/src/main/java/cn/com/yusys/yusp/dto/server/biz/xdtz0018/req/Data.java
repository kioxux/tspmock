package cn.com.yusys.yusp.dto.server.biz.xdtz0018.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/21 10:48:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/21 10:48
 * @since 2021/5/21 10:48
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opType")
    private String opType;//操作类型
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "drftNo")
    private String drftNo;//票号
    @JsonProperty(value = "repasteTotlAmt")
    private BigDecimal repasteTotlAmt;//转帖总金额（元）
    @JsonProperty(value = "aorgName")
    private String aorgName;//承兑行行名
    @JsonProperty(value = "aorgNo")
    private String aorgNo;//承兑行行号
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "startDate")
    private String startDate;//开始时间
    @JsonProperty(value = "endDate")
    private String endDate;//结束时间

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getDrftNo() {
        return drftNo;
    }

    public void setDrftNo(String drftNo) {
        this.drftNo = drftNo;
    }

    public BigDecimal getRepasteTotlAmt() {
        return repasteTotlAmt;
    }

    public void setRepasteTotlAmt(BigDecimal repasteTotlAmt) {
        this.repasteTotlAmt = repasteTotlAmt;
    }

    public String getAorgName() {
        return aorgName;
    }

    public void setAorgName(String aorgName) {
        this.aorgName = aorgName;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "opType='" + opType + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", drftNo='" + drftNo + '\'' +
                ", repasteTotlAmt=" + repasteTotlAmt +
                ", aorgName='" + aorgName + '\'' +
                ", aorgNo='" + aorgNo + '\'' +
                ", prdId='" + prdId + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
