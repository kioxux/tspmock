package cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询人员基本信息岗位信息家庭信息
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List2 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "memnam")
    private String memnam;//成员名字
    @JsonProperty(value = "mencad")
    private String mencad;//成员身份证号
    @JsonProperty(value = "relatn")
    private String relatn;//与本人关系
    @JsonProperty(value = "nation")
    private String nation;//民族
    @JsonProperty(value = "native")
    private String native1;//籍贯
    @JsonProperty(value = "btdate")
    private String btdate;//出生日期
    @JsonProperty(value = "memad")
    private String memad;//工作单位
    @JsonProperty(value = "memmob")
    private String memmob;//联系电话
    @JsonProperty(value = "mempol")
    private String mempol;//政治面貌
    @JsonProperty(value = "memedu")
    private String memedu;//学历


    public String getMemnam() {
        return memnam;
    }

    public void setMemnam(String memnam) {
        this.memnam = memnam;
    }

    public String getMencad() {
        return mencad;
    }

    public void setMencad(String mencad) {
        this.mencad = mencad;
    }

    public String getRelatn() {
        return relatn;
    }

    public void setRelatn(String relatn) {
        this.relatn = relatn;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getNative() {
        return native1;
    }

    public void setNative(String native1) {
        this.native1 = native1;
    }

    public String getBtdate() {
        return btdate;
    }

    public void setBtdate(String btdate) {
        this.btdate = btdate;
    }

    public String getMemad() {
        return memad;
    }

    public void setMemad(String memad) {
        this.memad = memad;
    }

    public String getMemmob() {
        return memmob;
    }

    public void setMemmob(String memmob) {
        this.memmob = memmob;
    }

    public String getMempol() {
        return mempol;
    }

    public void setMempol(String mempol) {
        this.mempol = mempol;
    }

    public String getMemedu() {
        return memedu;
    }

    public void setMemedu(String memedu) {
        this.memedu = memedu;
    }

    @Override
    public String toString() {
        return "List2{" +
                "memnam='" + memnam + '\'' +
                "mencad='" + mencad + '\'' +
                "relatn='" + relatn + '\'' +
                "nation='" + nation + '\'' +
                "native1='" + native1 + '\'' +
                "btdate='" + btdate + '\'' +
                "memad='" + memad + '\'' +
                "memmob='" + memmob + '\'' +
                "mempol='" + mempol + '\'' +
                "memedu='" + memedu + '\'' +
                '}';
    }
}  
