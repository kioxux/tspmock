package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：指标通用接口
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class CredzbReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ruleCode")
    private String ruleCode;//指标编码
    @JsonProperty(value = "reqId")
    private String reqId;//请求业务号
    @JsonProperty(value = "reportId")
    private String reportId;//报告编号
    @JsonProperty(value = "brchno")
    private String brchno;//机构号
    @JsonProperty(value = "customName")
    private String customName;//客户名称
    @JsonProperty(value = "certificateNum")
    private String certificateNum;//客户证件号
    @JsonProperty(value = "businessLine")
    private String businessLine;//产品编号
    @JsonProperty(value = "borrowPersonRelation")
    private String borrowPersonRelation;//与主借款人关系
    @JsonProperty(value = "auditReason")
    private String auditReason;//授权书条款
    @JsonProperty(value = "startDate")
    private String startDate;//开始日期
    @JsonProperty(value = "endDate")
    private String endDate;//结束日期

    public String getRuleCode() {
        return ruleCode;
    }

    public void setRuleCode(String ruleCode) {
        this.ruleCode = ruleCode;
    }

    public String getReqId() {
        return reqId;
    }

    public void setReqId(String reqId) {
        this.reqId = reqId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getCustomName() {
        return customName;
    }

    public void setCustomName(String customName) {
        this.customName = customName;
    }

    public String getCertificateNum() {
        return certificateNum;
    }

    public void setCertificateNum(String certificateNum) {
        this.certificateNum = certificateNum;
    }

    public String getBusinessLine() {
        return businessLine;
    }

    public void setBusinessLine(String businessLine) {
        this.businessLine = businessLine;
    }

    public String getBorrowPersonRelation() {
        return borrowPersonRelation;
    }

    public void setBorrowPersonRelation(String borrowPersonRelation) {
        this.borrowPersonRelation = borrowPersonRelation;
    }

    public String getAuditReason() {
        return auditReason;
    }

    public void setAuditReason(String auditReason) {
        this.auditReason = auditReason;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "CredzbReqDto{" +
                "ruleCode='" + ruleCode + '\'' +
                "reqId='" + reqId + '\'' +
                "reportId='" + reportId + '\'' +
                "brchno='" + brchno + '\'' +
                "customName='" + customName + '\'' +
                "certificateNum='" + certificateNum + '\'' +
                "businessLine='" + businessLine + '\'' +
                "borrowPersonRelation='" + borrowPersonRelation + '\'' +
                "auditReason='" + auditReason + '\'' +
                "startDate='" + startDate + '\'' +
                "endDate='" + endDate + '\'' +
                '}';
    }
}  
