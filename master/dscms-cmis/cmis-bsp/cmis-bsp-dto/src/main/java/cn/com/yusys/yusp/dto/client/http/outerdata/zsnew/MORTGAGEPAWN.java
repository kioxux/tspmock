package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 动产抵押-抵押物信息
 */
@JsonPropertyOrder(alphabetic = true)
public class MORTGAGEPAWN implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "MAB_PAWN_DETAILS")
    private String MAB_PAWN_DETAILS;//	数量、质量、状况、所在地等情况
    @JsonProperty(value = "MAB_PAWN_NAME")
    private String MAB_PAWN_NAME;//	名称
    @JsonProperty(value = "MAB_PAWN_OWNER")
    private String MAB_PAWN_OWNER;//	所有权或使用权归属
    @JsonProperty(value = "MAB_PAWN_RMK")
    private String MAB_PAWN_RMK;//	备注
    @JsonProperty(value = "MAB_REGNO")
    private String MAB_REGNO;//	登记编号

    @JsonIgnore
    public String getMAB_PAWN_DETAILS() {
        return MAB_PAWN_DETAILS;
    }

    @JsonIgnore
    public void setMAB_PAWN_DETAILS(String MAB_PAWN_DETAILS) {
        this.MAB_PAWN_DETAILS = MAB_PAWN_DETAILS;
    }

    @JsonIgnore
    public String getMAB_PAWN_NAME() {
        return MAB_PAWN_NAME;
    }

    @JsonIgnore
    public void setMAB_PAWN_NAME(String MAB_PAWN_NAME) {
        this.MAB_PAWN_NAME = MAB_PAWN_NAME;
    }

    @JsonIgnore
    public String getMAB_PAWN_OWNER() {
        return MAB_PAWN_OWNER;
    }

    @JsonIgnore
    public void setMAB_PAWN_OWNER(String MAB_PAWN_OWNER) {
        this.MAB_PAWN_OWNER = MAB_PAWN_OWNER;
    }

    @JsonIgnore
    public String getMAB_PAWN_RMK() {
        return MAB_PAWN_RMK;
    }

    @JsonIgnore
    public void setMAB_PAWN_RMK(String MAB_PAWN_RMK) {
        this.MAB_PAWN_RMK = MAB_PAWN_RMK;
    }

    @JsonIgnore
    public String getMAB_REGNO() {
        return MAB_REGNO;
    }

    @JsonIgnore
    public void setMAB_REGNO(String MAB_REGNO) {
        this.MAB_REGNO = MAB_REGNO;
    }

    @Override
    public String toString() {
        return "MORTGAGEPAWN{" +
                "MAB_PAWN_DETAILS='" + MAB_PAWN_DETAILS + '\'' +
                ", MAB_PAWN_NAME='" + MAB_PAWN_NAME + '\'' +
                ", MAB_PAWN_OWNER='" + MAB_PAWN_OWNER + '\'' +
                ", MAB_PAWN_RMK='" + MAB_PAWN_RMK + '\'' +
                ", MAB_REGNO='" + MAB_REGNO + '\'' +
                '}';
    }
}
