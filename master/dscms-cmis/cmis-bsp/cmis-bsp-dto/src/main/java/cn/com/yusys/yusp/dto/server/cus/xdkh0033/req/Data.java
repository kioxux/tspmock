package cn.com.yusys.yusp.dto.server.cus.xdkh0033.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：客户评级结果同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "evalApplySerno")
    private String evalApplySerno;//评级申请流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "evalYear")
    private String evalYear;//评级年度
    @JsonProperty(value = "evalHppType")
    private String evalHppType;//评级发生类型
    @JsonProperty(value = "evalModel")
    private String evalModel;//评级模型
    @JsonProperty(value = "sysStartLvl")
    private String sysStartLvl;//系统初始等级
    @JsonProperty(value = "adjdLvl")
    private String adjdLvl;//调整后等级
    @JsonProperty(value = "adviceLvl")
    private String adviceLvl;//建议等级
    @JsonProperty(value = "finalLvl")
    private String finalLvl;//最终认定等级
    @JsonProperty(value = "evalStartDate")
    private String evalStartDate;//评级生效日
    @JsonProperty(value = "evalEndDate")
    private String evalEndDate;//评级到期日
    @JsonProperty(value = "dtghFlag")
    private String dtghFlag;//区分标识
    @JsonProperty(value = "breachProbly")
    private BigDecimal breachProbly;//违约概率
    @JsonProperty(value = "riskTypeMax")
    private String riskTypeMax;//风险大类
    @JsonProperty(value = "riskType")
    private String riskType;//风险种类
    @JsonProperty(value = "riskTypeMin")
    private String riskTypeMin;//风险小类
    @JsonProperty(value = "riskDtghDate")
    private String riskDtghDate;//风险划分日期

    public String getEvalApplySerno() {
        return evalApplySerno;
    }

    public void setEvalApplySerno(String evalApplySerno) {
        this.evalApplySerno = evalApplySerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getEvalYear() {
        return evalYear;
    }

    public void setEvalYear(String evalYear) {
        this.evalYear = evalYear;
    }

    public String getEvalHppType() {
        return evalHppType;
    }

    public void setEvalHppType(String evalHppType) {
        this.evalHppType = evalHppType;
    }

    public String getEvalModel() {
        return evalModel;
    }

    public void setEvalModel(String evalModel) {
        this.evalModel = evalModel;
    }

    public String getSysStartLvl() {
        return sysStartLvl;
    }

    public void setSysStartLvl(String sysStartLvl) {
        this.sysStartLvl = sysStartLvl;
    }

    public String getAdjdLvl() {
        return adjdLvl;
    }

    public void setAdjdLvl(String adjdLvl) {
        this.adjdLvl = adjdLvl;
    }

    public String getAdviceLvl() {
        return adviceLvl;
    }

    public void setAdviceLvl(String adviceLvl) {
        this.adviceLvl = adviceLvl;
    }

    public String getFinalLvl() {
        return finalLvl;
    }

    public void setFinalLvl(String finalLvl) {
        this.finalLvl = finalLvl;
    }

    public String getEvalStartDate() {
        return evalStartDate;
    }

    public void setEvalStartDate(String evalStartDate) {
        this.evalStartDate = evalStartDate;
    }

    public String getEvalEndDate() {
        return evalEndDate;
    }

    public void setEvalEndDate(String evalEndDate) {
        this.evalEndDate = evalEndDate;
    }

    public String getDtghFlag() {
        return dtghFlag;
    }

    public void setDtghFlag(String dtghFlag) {
        this.dtghFlag = dtghFlag;
    }

    public BigDecimal getBreachProbly() {
        return breachProbly;
    }

    public void setBreachProbly(BigDecimal breachProbly) {
        this.breachProbly = breachProbly;
    }

    public String getRiskTypeMax() {
        return riskTypeMax;
    }

    public void setRiskTypeMax(String riskTypeMax) {
        this.riskTypeMax = riskTypeMax;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public String getRiskTypeMin() {
        return riskTypeMin;
    }

    public void setRiskTypeMin(String riskTypeMin) {
        this.riskTypeMin = riskTypeMin;
    }

    public String getRiskDtghDate() {
        return riskDtghDate;
    }

    public void setRiskDtghDate(String riskDtghDate) {
        this.riskDtghDate = riskDtghDate;
    }

    @Override
    public String toString() {
        return "Data{" +
                "evalApplySerno='" + evalApplySerno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", evalYear='" + evalYear + '\'' +
                ", evalHppType='" + evalHppType + '\'' +
                ", evalModel='" + evalModel + '\'' +
                ", sysStartLvl='" + sysStartLvl + '\'' +
                ", adjdLvl='" + adjdLvl + '\'' +
                ", adviceLvl='" + adviceLvl + '\'' +
                ", finalLvl='" + finalLvl + '\'' +
                ", evalStartDate='" + evalStartDate + '\'' +
                ", evalEndDate='" + evalEndDate + '\'' +
                ", dtghFlag='" + dtghFlag + '\'' +
                ", breachProbly=" + breachProbly +
                ", riskTypeMax='" + riskTypeMax + '\'' +
                ", riskType='" + riskType + '\'' +
                ", riskTypeMin='" + riskTypeMin + '\'' +
                ", riskDtghDate='" + riskDtghDate + '\'' +
                '}';
    }
}