package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd02;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：为正式客户更新信贷客户信息手机号码
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd02ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    @Override
    public String toString() {
        return "Fbxd02ReqDto{" +
                ", cus_id='" + cus_id + '\'' +
                '}';
    }
}
