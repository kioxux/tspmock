package cn.com.yusys.yusp.dto.server.biz.xdtz0011.resp;

import cn.com.yusys.yusp.dto.server.biz.xdtz0006.resp.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 * 响应Dto：借据明细查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billStatus")
    private String billStatus;//借据状态
    @JsonProperty(value = "loanAmt")
    private BigDecimal loanAmt;//借据金额
    @JsonProperty(value = "repayAcctNo")
    private String repayAcctNo;//还款账号
    @JsonProperty(value = "contNo")
    private String contNo;//合同号
    @JsonProperty(value = "distrDate")
    private String distrDate;//发放日
    @JsonProperty(value = "acctName")
    private String acctName;//户名
    @JsonProperty(value = "curType")
    private String curType;//币种
    @JsonProperty(value = "bizType")
    private String bizType;//品种
    @JsonProperty(value = "loanBalance")
    private BigDecimal loanBalance;//剩余本金
    @JsonProperty(value = "loanMonths")
    private BigDecimal loanMonths;//贷款月数
    @JsonProperty(value = "eiDate")
    private String eiDate;//结息日
    @JsonProperty(value = "qnt")
    private BigDecimal qnt;//笔数
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getBillStatus() {
        return billStatus;
    }

    public void setBillStatus(String billStatus) {
        this.billStatus = billStatus;
    }

    public BigDecimal getLoanAmt() {
        return loanAmt;
    }

    public void setLoanAmt(BigDecimal loanAmt) {
        this.loanAmt = loanAmt;
    }

    public String getRepayAcctNo() {
        return repayAcctNo;
    }

    public void setRepayAcctNo(String repayAcctNo) {
        this.repayAcctNo = repayAcctNo;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getDistrDate() {
        return distrDate;
    }

    public void setDistrDate(String distrDate) {
        this.distrDate = distrDate;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public BigDecimal getLoanBalance() {
        return loanBalance;
    }

    public void setLoanBalance(BigDecimal loanBalance) {
        this.loanBalance = loanBalance;
    }

    public BigDecimal getLoanMonths() {
        return loanMonths;
    }

    public void setLoanMonths(BigDecimal loanMonths) {
        this.loanMonths = loanMonths;
    }

    public String getEiDate() {
        return eiDate;
    }

    public void setEiDate(String eiDate) {
        this.eiDate = eiDate;
    }

    public BigDecimal getQnt() {
        return qnt;
    }

    public void setQnt(BigDecimal qnt) {
        this.qnt = qnt;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "billStatus='" + billStatus + '\'' +
                ", loanAmt=" + loanAmt +
                ", repayAcctNo='" + repayAcctNo + '\'' +
                ", distrDate='" + distrDate + '\'' +
                ", acctName='" + acctName + '\'' +
                ", curType='" + curType + '\'' +
                ", bizType='" + bizType + '\'' +
                ", loanBalance=" + loanBalance +
                ", loanMonths=" + loanMonths +
                ", eiDate=" + eiDate +
                ", qnt=" + qnt +
                ", list=" + list +
                '}';
    }
}

