package cn.com.yusys.yusp.dto.server.biz.xdzc0020.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/6/8 20:55:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/6/8 20:55
 * @since 2021/6/8 20:55
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//业务流水号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "assetNo")
    private String assetNo;//资产编号
    @JsonProperty(value = "assetType")
    private String assetType;//资产类型
    @JsonProperty(value = "assetValue")
    private BigDecimal assetValue;//资产价值
    @JsonProperty(value = "assetEndDate")
    private String assetEndDate;//资产到期日
    @JsonProperty(value = "assetStatus")
    private String assetStatus;//资产状态
    @JsonProperty(value = "isPool")
    private String isPool;//是否入池
    @JsonProperty(value = "isPledge")
    private String isPledge;//是否入池质押
    @JsonProperty(value = "aorgNo")
    private String aorgNo;//承兑行行号
    @JsonProperty(value = "aorgName")
    private String aorgName;//承兑行名称
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//登记日期
    @JsonProperty(value = "updId")
    private String updId;//最近修改人
    @JsonProperty(value = "updBrId")
    private String updBrId;//最近修改机构
    @JsonProperty(value = "updDate")
    private String updDate;//最近修改日期
    @JsonProperty(value = "createTime")
    private String createTime;//创建时间
    @JsonProperty(value = "updateTime")
    private String updateTime;//修改时间

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getAssetNo() {
        return assetNo;
    }

    public void setAssetNo(String assetNo) {
        this.assetNo = assetNo;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public BigDecimal getAssetValue() {
        return assetValue;
    }

    public void setAssetValue(BigDecimal assetValue) {
        this.assetValue = assetValue;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public String getAssetStatus() {
        return assetStatus;
    }

    public void setAssetStatus(String assetStatus) {
        this.assetStatus = assetStatus;
    }

    public String getIsPool() {
        return isPool;
    }

    public void setIsPool(String isPool) {
        this.isPool = isPool;
    }

    public String getIsPledge() {
        return isPledge;
    }

    public void setIsPledge(String isPledge) {
        this.isPledge = isPledge;
    }

    public String getAorgNo() {
        return aorgNo;
    }

    public void setAorgNo(String aorgNo) {
        this.aorgNo = aorgNo;
    }

    public String getAorgName() {
        return aorgName;
    }

    public void setAorgName(String aorgName) {
        this.aorgName = aorgName;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Xdzc0020DataRespDto{" +
                "serno='" + serno + '\'' +
                ", contNo='" + contNo + '\'' +
                ", assetNo='" + assetNo + '\'' +
                ", assetType='" + assetType + '\'' +
                ", assetValue=" + assetValue +
                ", assetEndDate='" + assetEndDate + '\'' +
                ", assetStatus='" + assetStatus + '\'' +
                ", isPool='" + isPool + '\'' +
                ", isPledge='" + isPledge + '\'' +
                ", aorgNo='" + aorgNo + '\'' +
                ", aorgName='" + aorgName + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
