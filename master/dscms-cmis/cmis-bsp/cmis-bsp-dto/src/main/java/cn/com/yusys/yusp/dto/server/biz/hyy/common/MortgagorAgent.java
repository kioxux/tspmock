package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：抵押权人
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class MortgagorAgent implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "name")
    private String name;//姓名/名称
    @JsonProperty(value = "document_type")
    private DocumentType document_type;//证件类型
    @JsonProperty(value = "document_number")
    private String document_number;//证件号
    @JsonProperty(value = "phone")
    private String phone;//电话

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DocumentType getDocument_type() {
        return document_type;
    }

    public void setDocument_type(DocumentType document_type) {
        this.document_type = document_type;
    }

    public String getDocument_number() {
        return document_number;
    }

    public void setDocument_number(String document_number) {
        this.document_number = document_number;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Mortgagee{" +
                "name='" + name + '\'' +
                ", document_type=" + document_type +
                ", document_number='" + document_number + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
