package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

// 2.12.土地
@JsonPropertyOrder(alphabetic = true)
public class Land implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certificate_number")
    private String certificate_number;//证号
    @JsonProperty(value = "area")
    private String area;//面积
    @JsonProperty(value = "usage")
    private Usage usage;//用途

    public String getCertificate_number() {
        return certificate_number;
    }

    public void setCertificate_number(String certificate_number) {
        this.certificate_number = certificate_number;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return "Land{" +
                "certificate_number='" + certificate_number + '\'' +
                ", area='" + area + '\'' +
                ", usage=" + usage +
                '}';
    }
}
