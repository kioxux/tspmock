package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class List {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billNo")
    private String billNo;//票号
    @JsonProperty(value = "fBillAmount")
    private String fBillAmount;//票面金额
    @JsonProperty(value = "bailAmt")
    private String bailAmt;//保证金金额
    @JsonProperty(value = "chargeFlag")
    private String chargeFlag;//扣款标志
    @JsonProperty(value = "statusFlag")
    private String statusFlag;//承兑状态

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getfBillAmount() {
        return fBillAmount;
    }

    public void setfBillAmount(String fBillAmount) {
        this.fBillAmount = fBillAmount;
    }

    public String getBailAmt() {
        return bailAmt;
    }

    public void setBailAmt(String bailAmt) {
        this.bailAmt = bailAmt;
    }

    public String getChargeFlag() {
        return chargeFlag;
    }

    public void setChargeFlag(String chargeFlag) {
        this.chargeFlag = chargeFlag;
    }

    public String getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(String statusFlag) {
        this.statusFlag = statusFlag;
    }

    @Override
    public String toString() {
        return "List{" +
                "billNo='" + billNo + '\'' +
                ", fBillAmount='" + fBillAmount + '\'' +
                ", bailAmt='" + bailAmt + '\'' +
                ", chargeFlag='" + chargeFlag + '\'' +
                ", statusFlag='" + statusFlag + '\'' +
                '}';
    }
}
