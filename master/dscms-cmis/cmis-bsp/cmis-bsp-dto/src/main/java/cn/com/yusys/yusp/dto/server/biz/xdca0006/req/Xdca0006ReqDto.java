package cn.com.yusys.yusp.dto.server.biz.xdca0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：大额分期合同签订接口
 *
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdca0006ReqDto implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty(value = "servtp")
	private String servtp;//渠道码
	@JsonProperty(value = "waibclma")
	private String waibclma;//外部处理码
	@JsonProperty(value = "userid")
	private String userid;//柜员号
	@JsonProperty(value = "brchno")
	private String brchno;//部门号
	@JsonProperty(value = "datasq")
	private String datasq;//全局流水
	@JsonProperty(value = "servsq")
	private String servsq;//渠道流水
	@JsonProperty(value = "servdt")
	private String servdt;//请求方日期
	@JsonProperty(value = "servti")
	private String servti;//请求方时间
	@JsonProperty(value = "ipaddr")
	private String ipaddr;//请求方IP
	@JsonProperty(value = "mac")
	private String mac;//请求方MAC

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@JsonProperty(value = "data")
	private cn.com.yusys.yusp.dto.server.biz.xdca0006.req.Data data;//data

	public String getServtp() {
		return servtp;
	}

	public void setServtp(String servtp) {
		this.servtp = servtp;
	}

	public String getWaibclma() {
		return waibclma;
	}

	public void setWaibclma(String waibclma) {
		this.waibclma = waibclma;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getBrchno() {
		return brchno;
	}

	public void setBrchno(String brchno) {
		this.brchno = brchno;
	}

	public String getDatasq() {
		return datasq;
	}

	public void setDatasq(String datasq) {
		this.datasq = datasq;
	}

	public String getServsq() {
		return servsq;
	}

	public void setServsq(String servsq) {
		this.servsq = servsq;
	}

	public String getServdt() {
		return servdt;
	}

	public void setServdt(String servdt) {
		this.servdt = servdt;
	}

	public String getServti() {
		return servti;
	}

	public void setServti(String servti) {
		this.servti = servti;
	}

	public String getIpaddr() {
		return ipaddr;
	}

	public void setIpaddr(String ipaddr) {
		this.ipaddr = ipaddr;
	}

	public String getMac() {
		return mac;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}


	@Override
	public String toString() {
		return "Xdca0004ReqDto{" +
				"servtp='" + servtp + '\'' +
				", waibclma='" + waibclma + '\'' +
				", userid='" + userid + '\'' +
				", brchno='" + brchno + '\'' +
				", datasq='" + datasq + '\'' +
				", servsq='" + servsq + '\'' +
				", servdt='" + servdt + '\'' +
				", servti='" + servti + '\'' +
				", ipaddr='" + ipaddr + '\'' +
				", mac='" + mac + '\'' +
				", data=" + data +
				'}';
	}
}  
