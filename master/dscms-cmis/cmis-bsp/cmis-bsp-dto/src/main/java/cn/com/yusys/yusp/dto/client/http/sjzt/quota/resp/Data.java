package cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/30 21:13
 * @since 2021/8/30 21:13
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cus_id")
    private String cus_id;// 客户号
    @JsonProperty(value = "credit_quota")
    private String credit_quota;// 授信额度
    @JsonProperty(value = "quota_used")
    private String quota_used;//用信额度
    @JsonProperty(value = "acctdt")
    private String acctdt;// 数据日期(yyyy-MM)

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCredit_quota() {
        return credit_quota;
    }

    public void setCredit_quota(String credit_quota) {
        this.credit_quota = credit_quota;
    }

    public String getQuota_used() {
        return quota_used;
    }

    public void setQuota_used(String quota_used) {
        this.quota_used = quota_used;
    }

    public String getAcctdt() {
        return acctdt;
    }

    public void setAcctdt(String acctdt) {
        this.acctdt = acctdt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cus_id='" + cus_id + '\'' +
                ", credit_quota='" + credit_quota + '\'' +
                ", quota_used='" + quota_used + '\'' +
                ", acctdt='" + acctdt + '\'' +
                '}';
    }
}
