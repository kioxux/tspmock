package cn.com.yusys.yusp.dto.server.cus.xdkh0014.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：省心E付白名单信息维护
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "serno")
    private String serno;//流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "acctNo")
    private String acctNo;//账号
    @JsonProperty(value = "acctName")
    private String acctName;//户名
    @JsonProperty(value = "acctsvcr")
    private String acctsvcr;//开户行
    @JsonProperty(value = "acctsvcrName")
    private String acctsvcrName;//开户行名
    @JsonProperty(value = "isBankFlag")
    private String isBankFlag;//行内外标识
    @JsonProperty(value = "loanUseType")
    private String loanUseType;//贷款用途
    @JsonProperty(value = "fileUpload")
    private String fileUpload;//附件
    @JsonProperty(value = "status")
    private String status;//

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getAcctNo() {
        return acctNo;
    }

    public void setAcctNo(String acctNo) {
        this.acctNo = acctNo;
    }

    public String getAcctName() {
        return acctName;
    }

    public void setAcctName(String acctName) {
        this.acctName = acctName;
    }

    public String getAcctsvcr() {
        return acctsvcr;
    }

    public void setAcctsvcr(String acctsvcr) {
        this.acctsvcr = acctsvcr;
    }

    public String getAcctsvcrName() {
        return acctsvcrName;
    }

    public void setAcctsvcrName(String acctsvcrName) {
        this.acctsvcrName = acctsvcrName;
    }

    public String getIsBankFlag() {
        return isBankFlag;
    }

    public void setIsBankFlag(String isBankFlag) {
        this.isBankFlag = isBankFlag;
    }

    public String getLoanUseType() {
        return loanUseType;
    }

    public void setLoanUseType(String loanUseType) {
        this.loanUseType = loanUseType;
    }

    public String getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(String fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "List{" +
                "serno='" + serno + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "acctNo='" + acctNo + '\'' +
                "acctName='" + acctName + '\'' +
                "acctsvcr='" + acctsvcr + '\'' +
                "acctsvcrName='" + acctsvcrName + '\'' +
                "isBankFlag='" + isBankFlag + '\'' +
                "loanUseType='" + loanUseType + '\'' +
                "fileUpload='" + fileUpload + '\'' +
                "status='" + status + '\'' +
                '}';
    }
}  
