package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//2.15.交易信息
@JsonPropertyOrder(alphabetic = true)
public class DealInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "stock_business")
    private Boolean stock_business;//存量业务
    @JsonProperty(value = "number")
    private String number;//交易编号
    @JsonProperty(value = "contract_no")
    private String contract_no;//买卖合同编号
    @JsonProperty(value = "contract_value")
    private String contract_value;//合同债权金额

    public Boolean getStock_business() {
        return stock_business;
    }

    public void setStock_business(Boolean stock_business) {
        this.stock_business = stock_business;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getContract_no() {
        return contract_no;
    }

    public void setContract_no(String contract_no) {
        this.contract_no = contract_no;
    }

    public String getContract_value() {
        return contract_value;
    }

    public void setContract_value(String contract_value) {
        this.contract_value = contract_value;
    }

    @Override
    public String toString() {
        return "DealInfo{" +
                "stock_business=" + stock_business +
                ", number='" + number + '\'' +
                ", contract_no='" + contract_no + '\'' +
                ", contract_value='" + contract_value + '\'' +
                '}';
    }
}
