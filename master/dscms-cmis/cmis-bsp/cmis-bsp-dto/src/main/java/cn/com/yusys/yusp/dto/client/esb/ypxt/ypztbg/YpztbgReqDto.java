package cn.com.yusys.yusp.dto.client.esb.ypxt.ypztbg;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：押品状态变更
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月14日15:13:54
 */
@JsonPropertyOrder(alphabetic = true)
public class YpztbgReqDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "areano")
    private String areano;// 区域编码
    @JsonProperty(value = "cfstat")
    private String cfstat;// 查封状态
    @JsonProperty(value = "bdcnoo")
    private String bdcnoo;// 不动产权证书号
    @JsonProperty(value = "cftime")
    private String cftime;// 查封时间
    @JsonProperty(value = "ypstat")
    private String ypstat;// 最高可抵押顺位

    public String getAreano() {
        return areano;
    }

    public void setAreano(String areano) {
        this.areano = areano;
    }

    public String getCfstat() {
        return cfstat;
    }

    public void setCfstat(String cfstat) {
        this.cfstat = cfstat;
    }

    public String getBdcnoo() {
        return bdcnoo;
    }

    public void setBdcnoo(String bdcnoo) {
        this.bdcnoo = bdcnoo;
    }

    public String getCftime() {
        return cftime;
    }

    public void setCftime(String cftime) {
        this.cftime = cftime;
    }

    public String getYpstat() {
        return ypstat;
    }

    public void setYpstat(String ypstat) {
        this.ypstat = ypstat;
    }

    @Override
    public String toString() {
        return "YpztbgReqDto{" +
                "areano='" + areano + '\'' +
                ", cfstat='" + cfstat + '\'' +
                ", bdcnoo='" + bdcnoo + '\'' +
                ", cftime='" + cftime + '\'' +
                ", ypstat='" + ypstat + '\'' +
                '}';
    }
}
