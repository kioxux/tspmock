package cn.com.yusys.yusp.dto.client.esb.edzfxt.hvpyls.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：大额往账列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class HvpylsReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "hvflag")
    private String hvflag;//大小额交易标志
    @JsonProperty(value = "functp")
    private String functp;//业务类型
    @JsonProperty(value = "csbxno")
    private String csbxno;//钱箱号
    @JsonProperty(value = "subprc")
    private String subprc;//前台交易码
    @JsonProperty(value = "operdt")
    private String operdt;//交易日期
    @JsonProperty(value = "srflag")
    private String srflag;//往来账标志
    @JsonProperty(value = "opersq")
    private String opersq;//业务受理编号
    @JsonProperty(value = "termid")
    private String termid;//终端号
    @JsonProperty(value = "systdt")
    private String systdt;//系统日期
    @JsonProperty(value = "bankid")
    private String bankid;//行号

    @JsonProperty(value = "xtrqdt")
    private String xtrqdt;//系统日期
    @JsonProperty(value = "tranam")
    private BigDecimal tranam;//交易金额
    @JsonProperty(value = "sdcode")
    private String sdcode;//发起行行号
    @JsonProperty(value = "rdcode")
    private String rdcode;//接收行行号
    @JsonProperty(value = "pyerac")
    private String pyerac;//付款人账号
    @JsonProperty(value = "pyeeac")
    private String pyeeac;//收款人账号
    @JsonProperty(value = "transt")
    private String transt;//交易状态
    @JsonProperty(value = "hstrsq")
    private String hstrsq;//业务受理编号
    @JsonProperty(value = "cocain")
    private String cocain;//来往帐标志
    @JsonProperty(value = "begnum")
    private String begnum;//起始笔数
    @JsonProperty(value = "countnum")
    private String countnum;//查询笔数
    @JsonProperty(value = "channelseq")
    private String channelseq;//渠道流水

    @JsonProperty(value = "brchno")
    private String brchno;//    部门号,取账务机构号

    public String getHvflag() {
        return hvflag;
    }

    public void setHvflag(String hvflag) {
        this.hvflag = hvflag;
    }

    public String getFunctp() {
        return functp;
    }

    public void setFunctp(String functp) {
        this.functp = functp;
    }

    public String getCsbxno() {
        return csbxno;
    }

    public void setCsbxno(String csbxno) {
        this.csbxno = csbxno;
    }

    public String getSubprc() {
        return subprc;
    }

    public void setSubprc(String subprc) {
        this.subprc = subprc;
    }

    public String getOperdt() {
        return operdt;
    }

    public void setOperdt(String operdt) {
        this.operdt = operdt;
    }

    public String getSrflag() {
        return srflag;
    }

    public void setSrflag(String srflag) {
        this.srflag = srflag;
    }

    public String getOpersq() {
        return opersq;
    }

    public void setOpersq(String opersq) {
        this.opersq = opersq;
    }

    public String getTermid() {
        return termid;
    }

    public void setTermid(String termid) {
        this.termid = termid;
    }

    public String getSystdt() {
        return systdt;
    }

    public void setSystdt(String systdt) {
        this.systdt = systdt;
    }

    public String getBankid() {
        return bankid;
    }

    public void setBankid(String bankid) {
        this.bankid = bankid;
    }

    public String getXtrqdt() {
        return xtrqdt;
    }

    public void setXtrqdt(String xtrqdt) {
        this.xtrqdt = xtrqdt;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public String getSdcode() {
        return sdcode;
    }

    public void setSdcode(String sdcode) {
        this.sdcode = sdcode;
    }

    public String getRdcode() {
        return rdcode;
    }

    public void setRdcode(String rdcode) {
        this.rdcode = rdcode;
    }

    public String getPyerac() {
        return pyerac;
    }

    public void setPyerac(String pyerac) {
        this.pyerac = pyerac;
    }

    public String getPyeeac() {
        return pyeeac;
    }

    public void setPyeeac(String pyeeac) {
        this.pyeeac = pyeeac;
    }

    public String getTranst() {
        return transt;
    }

    public void setTranst(String transt) {
        this.transt = transt;
    }

    public String getHstrsq() {
        return hstrsq;
    }

    public void setHstrsq(String hstrsq) {
        this.hstrsq = hstrsq;
    }

    public String getCocain() {
        return cocain;
    }

    public void setCocain(String cocain) {
        this.cocain = cocain;
    }

    public String getBegnum() {
        return begnum;
    }

    public void setBegnum(String begnum) {
        this.begnum = begnum;
    }

    public String getCountnum() {
        return countnum;
    }

    public void setCountnum(String countnum) {
        this.countnum = countnum;
    }

    public String getChannelseq() {
        return channelseq;
    }

    public void setChannelseq(String channelseq) {
        this.channelseq = channelseq;
    }


    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }


    @Override
    public String toString() {
        return "HvpylsReqDto{" +
                "hvflag='" + hvflag + '\'' +
                ", functp='" + functp + '\'' +
                ", csbxno='" + csbxno + '\'' +
                ", subprc='" + subprc + '\'' +
                ", operdt='" + operdt + '\'' +
                ", srflag='" + srflag + '\'' +
                ", opersq='" + opersq + '\'' +
                ", termid='" + termid + '\'' +
                ", systdt='" + systdt + '\'' +
                ", bankid='" + bankid + '\'' +
                ", xtrqdt='" + xtrqdt + '\'' +
                ", tranam=" + tranam +
                ", sdcode='" + sdcode + '\'' +
                ", rdcode='" + rdcode + '\'' +
                ", pyerac='" + pyerac + '\'' +
                ", pyeeac='" + pyeeac + '\'' +
                ", transt='" + transt + '\'' +
                ", hstrsq='" + hstrsq + '\'' +
                ", cocain='" + cocain + '\'' +
                ", begnum='" + begnum + '\'' +
                ", countnum='" + countnum + '\'' +
                ", channelseq='" + channelseq + '\'' +
                ", brchno='" + brchno + '\'' +
                '}';
    }
}
