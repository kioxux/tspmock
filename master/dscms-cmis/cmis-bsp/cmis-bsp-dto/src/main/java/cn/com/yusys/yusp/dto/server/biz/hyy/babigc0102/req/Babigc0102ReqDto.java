package cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.req;

import cn.com.yusys.yusp.dto.server.HyyRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Babigc0102ReqDto extends HyyRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Babigc0102ReqDto{" +
                "data=" + data +
                '}';
    }
}
