package cn.com.yusys.yusp.dto.server.biz.xdht0031.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据核心客户号查询经营性贷款合同额度
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    public String getCusId() { return cusId; }
    public void setCusId(String cusId ) { this.cusId = cusId;}
    @Override
    public String toString() {
        return "Xdht0031ReqDto{" +
                "cusId='" + cusId+ '\'' +
                '}';
    }
}