package cn.com.yusys.yusp.dto.server.biz.xdht0022.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：合同信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdht0022ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "servtp")
    private String servtp;//渠道码
    @JsonProperty(value = "waibclma")
    private String waibclma;//外部处理码
    @JsonProperty(value = "userid")
    private String userid;//柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//部门号
    @JsonProperty(value = "datasq")
    private String datasq;//全局流水
    @JsonProperty(value = "data")
    private Data data;

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getWaibclma() {
        return waibclma;
    }

    public void setWaibclma(String waibclma) {
        this.waibclma = waibclma;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getDatasq() {
        return datasq;
    }

    public void setDatasq(String datasq) {
        this.datasq = datasq;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdht0022ReqDto{" +
                "servtp='" + servtp + '\'' +
                ", waibclma='" + waibclma + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", datasq='" + datasq + '\'' +
                ", data=" + data +
                '}';
    }
}
