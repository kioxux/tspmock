package cn.com.yusys.yusp.dto.server.oca.xdxt0008.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 16:28:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 16:28
 * @since 2021/5/24 16:28
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    @Override
    public String toString() {
        return "Xdxt0008ReqDto{" +
                "managerId='" + managerId + '\'' +
                '}';
    }
}
