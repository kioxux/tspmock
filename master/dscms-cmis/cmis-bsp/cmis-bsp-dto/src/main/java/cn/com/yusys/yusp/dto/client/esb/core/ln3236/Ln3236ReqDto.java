package cn.com.yusys.yusp.dto.client.esb.core.ln3236;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：贷款账隔日冲正
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3236ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yjiaoyrq")
    private String yjiaoyrq;//原交易日期
    @JsonProperty(value = "yjiaoyls")
    private String yjiaoyls;//原交易流水
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号

    public String getYjiaoyrq() {
        return yjiaoyrq;
    }

    public void setYjiaoyrq(String yjiaoyrq) {
        this.yjiaoyrq = yjiaoyrq;
    }

    public String getYjiaoyls() {
        return yjiaoyls;
    }

    public void setYjiaoyls(String yjiaoyls) {
        this.yjiaoyls = yjiaoyls;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    @Override
    public String toString() {
        return "Ln3236ReqDto{" +
                "yjiaoyrq='" + yjiaoyrq + '\'' +
                "yjiaoyls='" + yjiaoyls + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                '}';
    }
}  
