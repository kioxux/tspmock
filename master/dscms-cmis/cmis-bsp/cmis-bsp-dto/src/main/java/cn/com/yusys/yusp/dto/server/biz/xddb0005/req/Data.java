package cn.com.yusys.yusp.dto.server.biz.xddb0005.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * @Author zhangpeng
 * @Date 2021/4/29 20:37
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "listnm")
    private BigDecimal listnm;//循环列表记录数

    @JsonProperty(value = "reqList")
    private java.util.List<ReqList> reqList;

    public BigDecimal getListnm() {
        return listnm;
    }

    public void setListnm(BigDecimal listnm) {
        this.listnm = listnm;
    }

    public List<ReqList> getReqList() {
        return reqList;
    }

    public void setReqList(List<ReqList> reqList) {
        this.reqList = reqList;
    }


    @Override
    public String toString() {
        return "Data{" +
                "listnm=" + listnm +
                ", reqList=" + reqList +
                '}';
    }
}
