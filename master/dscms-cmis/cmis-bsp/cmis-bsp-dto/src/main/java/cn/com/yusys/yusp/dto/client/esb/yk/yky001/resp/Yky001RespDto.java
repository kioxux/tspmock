package cn.com.yusys.yusp.dto.client.esb.yk.yky001.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：印控系统用印接口
 *
 * @author xuwh
 * @version 1.0
 * @since 2021/8/18下午19:28:27
 */
@JsonPropertyOrder(alphabetic = true)
public class Yky001RespDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "erorcd")
    private String erorcd;//响应码
    @JsonProperty(value = "erortx")
    private String erortx;//响应信息

    public String getErorcd() {
        return erorcd;
    }

    public void setErorcd(String erorcd) {
        this.erorcd = erorcd;
    }

    public String getErortx() {
        return erortx;
    }

    public void setErortx(String erortx) {
        this.erortx = erortx;
    }

    @Override
    public String toString() {
        return "Yky001RespDto{" +
                "erorcd='" + erorcd + '\'' +
                ", erortx='" + erortx + '\'' +
                '}';
    }

}
