package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp08.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：客户调查撤销接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Znsp08ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "suvery_serno")
    private String suvery_serno;//调查流水号

    public String getSuvery_serno() {
        return suvery_serno;
    }

    public void setSuvery_serno(String suvery_serno) {
        this.suvery_serno = suvery_serno;
    }

    @Override
    public String toString() {
        return "Znsp08ReqDto{" +
                "suvery_serno='" + suvery_serno + '\'' +
                '}';
    }
}  
