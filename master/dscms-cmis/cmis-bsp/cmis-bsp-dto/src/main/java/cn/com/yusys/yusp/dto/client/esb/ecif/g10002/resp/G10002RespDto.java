package cn.com.yusys.yusp.dto.client.esb.ecif.g10002.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：客户群组信息维护
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class G10002RespDto implements Serializable {
	private static final long serialVersionUID = 1L;
	@JsonProperty(value = "grouno")
	private String grouno;//群组编号

	public String getGrouno() {
		return grouno;
	}

	public void setGrouno(String grouno) {
		this.grouno = grouno;
	}

	@Override
	public String toString() {
		return "G10002RespDto{" +
				"grouno='" + grouno + '\'' +
				'}';
	}
}  
