package cn.com.yusys.yusp.dto.server.biz.xdca0006.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信用卡调额申请
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    public String getAPPNO_EXTERNAL() {
        return APPNO_EXTERNAL;
    }

    public void setAPPNO_EXTERNAL(String APPNO_EXTERNAL) {
        this.APPNO_EXTERNAL = APPNO_EXTERNAL;
    }

    public String getPRODUCT_CD() {
        return PRODUCT_CD;
    }

    public void setPRODUCT_CD(String PRODUCT_CD) {
        this.PRODUCT_CD = PRODUCT_CD;
    }

    @JsonProperty(value = "APPNO_EXTERNAL")
    private String APPNO_EXTERNAL;//外部流水编号

    @JsonProperty(value = "PRODUCT_CD")
    private String PRODUCT_CD;//卡产品编号

    @Override
    public String toString() {
        return "Data{" +
                "APPNO_EXTERNAL='" + APPNO_EXTERNAL + '\'' +
                "PRODUCT_CD='" + PRODUCT_CD + '\'' +
                '}';
    }
}
