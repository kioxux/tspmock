package cn.com.yusys.yusp.dto.client.esb.core.da3308.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：抵债资产费用管理
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3308RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "shoufjee")
    private BigDecimal shoufjee;//收费金额

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    @Override
    public String toString() {
        return "Da3308RespDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "shoufjee='" + shoufjee + '\'' +
                '}';
    }
}  
