package cn.com.yusys.yusp.dto.client.esb.core.ln3038.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：贷款资料变更明细查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3038RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "zongbish")
    private String zongbish;//总笔数
    @JsonProperty(value = "lstdkbgmx")
    private java.util.List<Lstdkbgmx> lstdkbgmx;

    public String getZongbish() {
        return zongbish;
    }

    public void setZongbish(String zongbish) {
        this.zongbish = zongbish;
    }

    public List<Lstdkbgmx> getLstdkbgmx() {
        return lstdkbgmx;
    }

    public void setLstdkbgmx(List<Lstdkbgmx> lstdkbgmx) {
        this.lstdkbgmx = lstdkbgmx;
    }

    @Override
    public String toString() {
        return "Ln3038RespDto{" +
                "zongbish='" + zongbish + '\'' +
                ", lstdkbgmx=" + lstdkbgmx +
                '}';
    }
}
