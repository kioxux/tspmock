package cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 请求Dto：押品我行确认价值同步接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class EvalypReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd;//处理码
    @JsonProperty(value = "servtp")
    private String servtp;//渠道
    @JsonProperty(value = "servsq")
    private String servsq;//渠道流水
    @JsonProperty(value = "userid")
    private String userid;//柜员号
    @JsonProperty(value = "brchno")
    private String brchno;//部门号
    @JsonProperty(value = "evalypListInfoReq")
    private java.util.List<EvalypListInfoReq> evalypListInfoReq;//押品我行确认价值同步接口

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public List<EvalypListInfoReq> getEvalypListInfoReq() {
        return evalypListInfoReq;
    }

    public void setEvalypListInfoReq(List<EvalypListInfoReq> evalypListInfoReq) {
        this.evalypListInfoReq = evalypListInfoReq;
    }

    @Override
    public String toString() {
        return "EvalypReqDto{" +
                "prcscd='" + prcscd + '\'' +
                ", servtp='" + servtp + '\'' +
                ", servsq='" + servsq + '\'' +
                ", userid='" + userid + '\'' +
                ", brchno='" + brchno + '\'' +
                ", evalypListInfoReq=" + evalypListInfoReq +
                '}';
    }
}
