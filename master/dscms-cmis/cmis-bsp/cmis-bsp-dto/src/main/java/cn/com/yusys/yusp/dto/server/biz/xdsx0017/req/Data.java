package cn.com.yusys.yusp.dto.server.biz.xdsx0017.req;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：信贷提供风控查询授信信息
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cer_type")
    private String cer_type;//开户证件类型
    @JsonProperty(value = "local_no")
    private String local_no;//统一社会信用代码

    public String getCer_type() {
        return cer_type;
    }

    public void setCer_type(String cer_type) {
        this.cer_type = cer_type;
    }

    public String getLocal_no() {
        return local_no;
    }

    public void setLocal_no(String local_no) {
        this.local_no = local_no;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cer_type='" + cer_type + '\'' +
                ", local_no='" + local_no + '\'' +
                '}';
    }
}
