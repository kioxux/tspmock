package cn.com.yusys.yusp.dto.client.esb.ypxt.evalyp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：押品我行确认价值同步接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class EvalypListInfoResp implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "yptybh")
    private String yptybh;//押品统一编号
    @JsonProperty(value = "gzdqry")
    private String gzdqry;//下次估值到期日

    public String getYptybh() {
        return yptybh;
    }

    public void setYptybh(String yptybh) {
        this.yptybh = yptybh;
    }

    public String getGzdqry() {
        return gzdqry;
    }

    public void setGzdqry(String gzdqry) {
        this.gzdqry = gzdqry;
    }

    @Override
    public String toString() {
        return "EvalypListInfoRespDto{" +
                "yptybh='" + yptybh + '\'' +
                "gzdqry='" + gzdqry + '\'' +
                '}';
    }
}  
