package cn.com.yusys.yusp.dto.client.esb.core.ln3177;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：资产转让付款状态维护
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3177RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "jiejuhao")
    private String jiejuhao;//借据号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "xuhaoooo")
    private long xuhaoooo;//序号
    @JsonProperty(value = "dhfkywlx")
    private String dhfkywlx;//业务类型
    @JsonProperty(value = "zrfukzhh")
    private String zrfukzhh;//对外付款账号
    @JsonProperty(value = "zrfukzxh")
    private String zrfukzxh;//对外付款账号子序号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "benjinje")
    private BigDecimal benjinje;//本金金额
    @JsonProperty(value = "lixijine")
    private BigDecimal lixijine;//利息金额
    @JsonProperty(value = "fukuanrq")
    private String fukuanrq;//付款日期
    @JsonProperty(value = "zrfukzht")
    private String zrfukzht;//转让付款状态
    @JsonProperty(value = "jydszhao")
    private String jydszhao;//交易对手账号
    @JsonProperty(value = "jydszhzh")
    private String jydszhzh;//交易对手账号子序号
    @JsonProperty(value = "dfzhhmch")
    private String dfzhhmch;//对方账号名称
    @JsonProperty(value = "zhkaihhh")
    private String zhkaihhh;//账户开户行行号
    @JsonProperty(value = "zhkaihhm")
    private String zhkaihhm;//账户开户行行名
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public long getXuhaoooo() {
        return xuhaoooo;
    }

    public void setXuhaoooo(long xuhaoooo) {
        this.xuhaoooo = xuhaoooo;
    }

    public String getDhfkywlx() {
        return dhfkywlx;
    }

    public void setDhfkywlx(String dhfkywlx) {
        this.dhfkywlx = dhfkywlx;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getBenjinje() {
        return benjinje;
    }

    public void setBenjinje(BigDecimal benjinje) {
        this.benjinje = benjinje;
    }

    public BigDecimal getLixijine() {
        return lixijine;
    }

    public void setLixijine(BigDecimal lixijine) {
        this.lixijine = lixijine;
    }

    public String getFukuanrq() {
        return fukuanrq;
    }

    public void setFukuanrq(String fukuanrq) {
        this.fukuanrq = fukuanrq;
    }

    public String getZrfukzht() {
        return zrfukzht;
    }

    public void setZrfukzht(String zrfukzht) {
        this.zrfukzht = zrfukzht;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getDfzhhmch() {
        return dfzhhmch;
    }

    public void setDfzhhmch(String dfzhhmch) {
        this.dfzhhmch = dfzhhmch;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    @Override
    public String toString() {
        return "Ln3177RespDto{" +
                "xieybhao='" + xieybhao + '\'' +
                "jiejuhao='" + jiejuhao + '\'' +
                "xieyimch='" + xieyimch + '\'' +
                "xuhaoooo='" + xuhaoooo + '\'' +
                "dhfkywlx='" + dhfkywlx + '\'' +
                "zrfukzhh='" + zrfukzhh + '\'' +
                "zrfukzxh='" + zrfukzxh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "benjinje='" + benjinje + '\'' +
                "lixijine='" + lixijine + '\'' +
                "fukuanrq='" + fukuanrq + '\'' +
                "zrfukzht='" + zrfukzht + '\'' +
                "jydszhao='" + jydszhao + '\'' +
                "jydszhzh='" + jydszhzh + '\'' +
                "dfzhhmch='" + dfzhhmch + '\'' +
                "zhkaihhh='" + zhkaihhh + '\'' +
                "zhkaihhm='" + zhkaihhm + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                '}';
    }
}  
