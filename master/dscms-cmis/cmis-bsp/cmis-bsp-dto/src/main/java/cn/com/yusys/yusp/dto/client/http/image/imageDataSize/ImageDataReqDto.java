package cn.com.yusys.yusp.dto.client.http.image.imageDataSize;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Map;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/28 10:00
 * @since 2021/6/28 10:00
 */
public class ImageDataReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "docId")
    private String docId;
    @JsonProperty(value = "outSystemCode")
    private String outSystemCode;
    @JsonProperty(value = "isSlice")
    private Boolean isSlice;
    @JsonProperty(value = "isStampCheck")
    private Boolean isStampCheck;
    @JsonProperty(value = "indexMap")
    private Map indexMap;


    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }

    public String getOutSystemCode() {
        return outSystemCode;
    }

    public void setOutSystemCode(String outSystemCode) {
        this.outSystemCode = outSystemCode;
    }

    public Boolean getSlice() {
        return isSlice;
    }

    public void setSlice(Boolean slice) {
        isSlice = slice;
    }

    public Boolean getStampCheck() {
        return isStampCheck;
    }

    public void setStampCheck(Boolean stampCheck) {
        isStampCheck = stampCheck;
    }

    public Map getIndexMap() {
        return indexMap;
    }

    public void setIndexMap(Map indexMap) {
        this.indexMap = indexMap;
    }

    @Override
    public String toString() {
        return "ImageDataReqDto{" +
                "docId='" + docId + '\'' +
                ", outSystemCode='" + outSystemCode + '\'' +
                ", isSlice=" + isSlice +
                ", isStampCheck=" + isStampCheck +
                '}';
    }
}
