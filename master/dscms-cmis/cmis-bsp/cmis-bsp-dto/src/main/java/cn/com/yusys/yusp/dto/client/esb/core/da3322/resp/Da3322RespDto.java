package cn.com.yusys.yusp.dto.client.esb.core.da3322.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：抵债资产明细查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3322RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "listnm0")
    List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.Listnm0> listnm0;
    @JsonProperty(value = "lstDzjtmx")
    List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzjtmx> lstDzjtmx;
    @JsonProperty(value = "lstDzsfmx")
    List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDzsfmx> lstDzsfmx;
    @JsonProperty(value = "lstDztxmx")
    List<cn.com.yusys.yusp.dto.client.esb.core.da3322.resp.LstDztxmx> lstDztxmx;

    public List<Listnm0> getListnm0() {
        return listnm0;
    }

    public void setListnm0(List<Listnm0> listnm0) {
        this.listnm0 = listnm0;
    }

    public List<LstDzjtmx> getLstDzjtmx() {
        return lstDzjtmx;
    }

    public void setLstDzjtmx(List<LstDzjtmx> lstDzjtmx) {
        this.lstDzjtmx = lstDzjtmx;
    }

    public List<LstDzsfmx> getLstDzsfmx() {
        return lstDzsfmx;
    }

    public void setLstDzsfmx(List<LstDzsfmx> lstDzsfmx) {
        this.lstDzsfmx = lstDzsfmx;
    }

    public List<LstDztxmx> getLstDztxmx() {
        return lstDztxmx;
    }

    public void setLstDztxmx(List<LstDztxmx> lstDztxmx) {
        this.lstDztxmx = lstDztxmx;
    }

    @Override
    public String toString() {
        return "Da3322RespDto{" +
                "listnm0=" + listnm0 +
                ", lstDzjtmx=" + lstDzjtmx +
                ", lstDzsfmx=" + lstDzsfmx +
                ", lstDztxmx=" + lstDztxmx +
                '}';
    }
}
