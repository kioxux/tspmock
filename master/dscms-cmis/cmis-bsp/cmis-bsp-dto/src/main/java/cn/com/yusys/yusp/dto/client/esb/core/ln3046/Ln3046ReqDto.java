package cn.com.yusys.yusp.dto.client.esb.core.ln3046;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：贷款核销归还
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3046ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "dkzhangh")
    private String dkzhangh;//贷款账号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "hexiaobj")
    private BigDecimal hexiaobj;//核销本金
    @JsonProperty(value = "hexiaolx")
    private BigDecimal hexiaolx;//核销利息
    @JsonProperty(value = "yihxbjlx")
    private BigDecimal yihxbjlx;//已核销本金利息
    @JsonProperty(value = "hetongbh")
    private String hetongbh;//合同编号
    @JsonProperty(value = "ghbenjin")
    private BigDecimal ghbenjin;//归还本金
    @JsonProperty(value = "guihlixi")
    private BigDecimal guihlixi;//归还利息
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "hexiaozh")
    private String hexiaozh;//核销来源账号
    @JsonProperty(value = "hexiaozx")
    private String hexiaozx;//核销来源账号子序号

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getDkzhangh() {
        return dkzhangh;
    }

    public void setDkzhangh(String dkzhangh) {
        this.dkzhangh = dkzhangh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getHexiaobj() {
        return hexiaobj;
    }

    public void setHexiaobj(BigDecimal hexiaobj) {
        this.hexiaobj = hexiaobj;
    }

    public BigDecimal getHexiaolx() {
        return hexiaolx;
    }

    public void setHexiaolx(BigDecimal hexiaolx) {
        this.hexiaolx = hexiaolx;
    }

    public BigDecimal getYihxbjlx() {
        return yihxbjlx;
    }

    public void setYihxbjlx(BigDecimal yihxbjlx) {
        this.yihxbjlx = yihxbjlx;
    }

    public String getHetongbh() {
        return hetongbh;
    }

    public void setHetongbh(String hetongbh) {
        this.hetongbh = hetongbh;
    }

    public BigDecimal getGhbenjin() {
        return ghbenjin;
    }

    public void setGhbenjin(BigDecimal ghbenjin) {
        this.ghbenjin = ghbenjin;
    }

    public BigDecimal getGuihlixi() {
        return guihlixi;
    }

    public void setGuihlixi(BigDecimal guihlixi) {
        this.guihlixi = guihlixi;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHexiaozh() {
        return hexiaozh;
    }

    public void setHexiaozh(String hexiaozh) {
        this.hexiaozh = hexiaozh;
    }

    public String getHexiaozx() {
        return hexiaozx;
    }

    public void setHexiaozx(String hexiaozx) {
        this.hexiaozx = hexiaozx;
    }

    @Override
    public String toString() {
        return "Ln3046ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "dkzhangh='" + dkzhangh + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "kehuhaoo='" + kehuhaoo + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "hexiaobj='" + hexiaobj + '\'' +
                "hexiaolx='" + hexiaolx + '\'' +
                "yihxbjlx='" + yihxbjlx + '\'' +
                "hetongbh='" + hetongbh + '\'' +
                "ghbenjin='" + ghbenjin + '\'' +
                "guihlixi='" + guihlixi + '\'' +
                "zijnlaiy='" + zijnlaiy + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hexiaozh='" + hexiaozh + '\'' +
                "hexiaozx='" + hexiaozx + '\'' +
                '}';
    }
}  
