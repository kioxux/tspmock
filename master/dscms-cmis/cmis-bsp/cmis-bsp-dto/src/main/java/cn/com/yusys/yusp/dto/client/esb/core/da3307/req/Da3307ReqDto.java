package cn.com.yusys.yusp.dto.client.esb.core.da3307.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：抵债资产出租处理
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3307ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "dcldzzic")
    private BigDecimal dcldzzic;//待处理抵债资产
    @JsonProperty(value = "jieszhao")
    private String jieszhao;//结算账号
    @JsonProperty(value = "jeszhzxh")
    private String jeszhzxh;//结算账号子序号
    @JsonProperty(value = "shiftanx")
    private String shiftanx;//是否摊销
    @JsonProperty(value = "txrzzhqi")
    private String txrzzhqi;//摊销入账周期
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "jiaoyije")
    private BigDecimal jiaoyije;//交易金额

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getDcldzzic() {
        return dcldzzic;
    }

    public void setDcldzzic(BigDecimal dcldzzic) {
        this.dcldzzic = dcldzzic;
    }

    public String getJieszhao() {
        return jieszhao;
    }

    public void setJieszhao(String jieszhao) {
        this.jieszhao = jieszhao;
    }

    public String getJeszhzxh() {
        return jeszhzxh;
    }

    public void setJeszhzxh(String jeszhzxh) {
        this.jeszhzxh = jeszhzxh;
    }

    public String getShiftanx() {
        return shiftanx;
    }

    public void setShiftanx(String shiftanx) {
        this.shiftanx = shiftanx;
    }

    public String getTxrzzhqi() {
        return txrzzhqi;
    }

    public void setTxrzzhqi(String txrzzhqi) {
        this.txrzzhqi = txrzzhqi;
    }

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public BigDecimal getJiaoyije() {
        return jiaoyije;
    }

    public void setJiaoyije(BigDecimal jiaoyije) {
        this.jiaoyije = jiaoyije;
    }

    @Override
    public String toString() {
        return "Da3307ReqDto{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                "dzzcminc='" + dzzcminc + '\'' +
                "dcldzzic='" + dcldzzic + '\'' +
                "jieszhao='" + jieszhao + '\'' +
                "jeszhzxh='" + jeszhzxh + '\'' +
                "shiftanx='" + shiftanx + '\'' +
                "txrzzhqi='" + txrzzhqi + '\'' +
                "qishriqi='" + qishriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "jiaoyije='" + jiaoyije + '\'' +
                '}';
    }
}  
