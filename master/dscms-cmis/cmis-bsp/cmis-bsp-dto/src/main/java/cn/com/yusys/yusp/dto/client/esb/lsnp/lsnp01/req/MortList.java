package cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class MortList implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "pawn_type")
    private String pawn_type;//抵押物类型
    @JsonProperty(value = "mort_rate")
    private BigDecimal mort_rate;//抵押率
    @JsonProperty(value = "valuation")
    private BigDecimal valuation;//抵质押物评估价

    public String getPawn_type() {
        return pawn_type;
    }

    public void setPawn_type(String pawn_type) {
        this.pawn_type = pawn_type;
    }

    public BigDecimal getMort_rate() {
        return mort_rate;
    }

    public void setMort_rate(BigDecimal mort_rate) {
        this.mort_rate = mort_rate;
    }

    public BigDecimal getValuation() {
        return valuation;
    }

    public void setValuation(BigDecimal valuation) {
        this.valuation = valuation;
    }

    @Override
    public String toString() {
        return "MoreList{" +
                "pawn_type='" + pawn_type + '\'' +
                ", mort_rate=" + mort_rate +
                ", valuation=" + valuation +
                '}';
    }
}
