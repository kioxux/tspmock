package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd06;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：获取该笔借据的最新五级分类以及数据日期
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd06ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bill_no")
    private String bill_no;//交易码

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    @Override
    public String toString() {
        return "Fbxd06ReqDto{" +
                "bill_no='" + bill_no + '\'' +
                '}';
    }
}
