package cn.com.yusys.yusp.dto.client.esb.xwh.xwh003.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：核销锁定接口
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xwh003ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cardSecret")
    private String cardSecret;//唯一码
    @JsonProperty(value = "idCard")
    private String idCard;//证件号
    @JsonProperty(value = "status")
    private String status;//状态
    @JsonProperty(value = "loanContNo")
    private String loanContNo;//借款合同号
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public String getCardSecret() {
        return cardSecret;
    }

    public void setCardSecret(String cardSecret) {
        this.cardSecret = cardSecret;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLoanContNo() {
        return loanContNo;
    }

    public void setLoanContNo(String loanContNo) {
        this.loanContNo = loanContNo;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "Xwh003ReqDto{" +
                "cardSecret='" + cardSecret + '\'' +
                "idCard='" + idCard + '\'' +
                "status='" + status + '\'' +
                "loanContNo='" + loanContNo + '\'' +
                "cusId='" + cusId + '\'' +
                "billNo='" + billNo + '\'' +
                '}';
    }
}  
