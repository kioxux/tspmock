package cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：省心快贷plus授信申请
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1150ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "lmt_serno")
    private String lmt_serno;//信贷授信申请流水号
    @JsonProperty(value = "ent_name")
    private String ent_name;//企业名称
    @JsonProperty(value = "ent_cust_id")
    private String ent_cust_id;//企业核心客户号
    @JsonProperty(value = "org_code")
    private String org_code;//企业统一社会信用代码
    @JsonProperty(value = "cust_name")
    private String cust_name;//企业法人客户名称
    @JsonProperty(value = "cert_code")
    private String cert_code;//企业法人身份证号码
    @JsonProperty(value = "cust_id")
    private String cust_id;//企业法人核心客户号
    @JsonProperty(value = "phone")
    private String phone;//企业法人手机号
    @JsonProperty(value = "spouse_name")
    private String spouse_name;//企业法人配偶客户名称
    @JsonProperty(value = "spouse_code")
    private String spouse_code;//企业法人配偶身份证号码
    @JsonProperty(value = "spouse_cust_id")
    private String spouse_cust_id;//企业法人配偶核心客户号
    @JsonProperty(value = "spouse_phone")
    private String spouse_phone;//企业法人配偶手机号
    @JsonProperty(value = "net_assets")
    private String net_assets;//净资产
    @JsonProperty(value = "sales_indicators")
    private String sales_indicators;//销售指标
    @JsonProperty(value = "appamt")
    private String appamt;//申请金额
    @JsonProperty(value = "sxstartdate")
    private String sxstartdate;//授信起始日期
    @JsonProperty(value = "sxenddate")
    private String sxenddate;//授信到期日期
    @JsonProperty(value = "manager_id")
    private String manager_id;//管户经理ID
    @JsonProperty(value = "manager_name")
    private String manager_name;//管户经理名称
    @JsonProperty(value = "manager_org_id")
    private String manager_org_id;//管户经理所属机构ID
    @JsonProperty(value = "manager_org_name")
    private String manager_org_name;//管户经理所属机构名称
    @JsonProperty(value = "list")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req.List> list;

    public String getLmt_serno() {
        return lmt_serno;
    }

    public void setLmt_serno(String lmt_serno) {
        this.lmt_serno = lmt_serno;
    }

    public String getEnt_name() {
        return ent_name;
    }

    public void setEnt_name(String ent_name) {
        this.ent_name = ent_name;
    }

    public String getEnt_cust_id() {
        return ent_cust_id;
    }

    public void setEnt_cust_id(String ent_cust_id) {
        this.ent_cust_id = ent_cust_id;
    }

    public String getOrg_code() {
        return org_code;
    }

    public void setOrg_code(String org_code) {
        this.org_code = org_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_code() {
        return spouse_code;
    }

    public void setSpouse_code(String spouse_code) {
        this.spouse_code = spouse_code;
    }

    public String getSpouse_cust_id() {
        return spouse_cust_id;
    }

    public void setSpouse_cust_id(String spouse_cust_id) {
        this.spouse_cust_id = spouse_cust_id;
    }

    public String getSpouse_phone() {
        return spouse_phone;
    }

    public void setSpouse_phone(String spouse_phone) {
        this.spouse_phone = spouse_phone;
    }

    public String getNet_assets() {
        return net_assets;
    }

    public void setNet_assets(String net_assets) {
        this.net_assets = net_assets;
    }

    public String getSales_indicators() {
        return sales_indicators;
    }

    public void setSales_indicators(String sales_indicators) {
        this.sales_indicators = sales_indicators;
    }

    public String getAppamt() {
        return appamt;
    }

    public void setAppamt(String appamt) {
        this.appamt = appamt;
    }

    public String getSxstartdate() {
        return sxstartdate;
    }

    public void setSxstartdate(String sxstartdate) {
        this.sxstartdate = sxstartdate;
    }

    public String getSxenddate() {
        return sxenddate;
    }

    public void setSxenddate(String sxenddate) {
        this.sxenddate = sxenddate;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getManager_org_id() {
        return manager_org_id;
    }

    public void setManager_org_id(String manager_org_id) {
        this.manager_org_id = manager_org_id;
    }

    public String getManager_org_name() {
        return manager_org_name;
    }

    public void setManager_org_name(String manager_org_name) {
        this.manager_org_name = manager_org_name;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Fb1150ReqDto{" +
                "lmt_serno='" + lmt_serno + '\'' +
                ", ent_name='" + ent_name + '\'' +
                ", ent_cust_id='" + ent_cust_id + '\'' +
                ", org_code='" + org_code + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", cert_code='" + cert_code + '\'' +
                ", cust_id='" + cust_id + '\'' +
                ", phone='" + phone + '\'' +
                ", spouse_name='" + spouse_name + '\'' +
                ", spouse_code='" + spouse_code + '\'' +
                ", spouse_cust_id='" + spouse_cust_id + '\'' +
                ", spouse_phone='" + spouse_phone + '\'' +
                ", net_assets='" + net_assets + '\'' +
                ", sales_indicators='" + sales_indicators + '\'' +
                ", appamt='" + appamt + '\'' +
                ", sxstartdate='" + sxstartdate + '\'' +
                ", sxenddate='" + sxenddate + '\'' +
                ", manager_id='" + manager_id + '\'' +
                ", manager_name='" + manager_name + '\'' +
                ", manager_org_id='" + manager_org_id + '\'' +
                ", manager_org_name='" + manager_org_name + '\'' +
                ", list=" + list +
                '}';
    }
}
