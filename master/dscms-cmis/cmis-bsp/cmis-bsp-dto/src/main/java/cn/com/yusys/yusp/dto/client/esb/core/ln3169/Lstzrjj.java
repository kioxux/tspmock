package cn.com.yusys.yusp.dto.client.esb.core.ln3169;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：资产证券化处理文件导入
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstzrjj implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "jiejuhao")
    private String jiejuhao;//借据号
    @JsonProperty(value = "zczrjine")
    private BigDecimal zczrjine;//资产转让金额
    @JsonProperty(value = "sddjjine")
    private BigDecimal sddjjine;//收到对价金额
    @JsonProperty(value = "zfdjjine")
    private BigDecimal zfdjjine;//支付对价金额
    @JsonProperty(value = "shfkztai")
    private String shfkztai;//收付款状态
    @JsonProperty(value = "zjjjclzt")
    private String zjjjclzt;//资产借据处理状态

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public BigDecimal getZczrjine() {
        return zczrjine;
    }

    public void setZczrjine(BigDecimal zczrjine) {
        this.zczrjine = zczrjine;
    }

    public BigDecimal getSddjjine() {
        return sddjjine;
    }

    public void setSddjjine(BigDecimal sddjjine) {
        this.sddjjine = sddjjine;
    }

    public BigDecimal getZfdjjine() {
        return zfdjjine;
    }

    public void setZfdjjine(BigDecimal zfdjjine) {
        this.zfdjjine = zfdjjine;
    }

    public String getShfkztai() {
        return shfkztai;
    }

    public void setShfkztai(String shfkztai) {
        this.shfkztai = shfkztai;
    }

    public String getZjjjclzt() {
        return zjjjclzt;
    }

    public void setZjjjclzt(String zjjjclzt) {
        this.zjjjclzt = zjjjclzt;
    }

    @Override
    public String toString() {
        return "Lstzrjj{" +
                "jiejuhao='" + jiejuhao + '\'' +
                ", zczrjine=" + zczrjine +
                ", sddjjine=" + sddjjine +
                ", zfdjjine=" + zfdjjine +
                ", shfkztai='" + shfkztai + '\'' +
                ", zjjjclzt='" + zjjjclzt + '\'' +
                '}';
    }
}
