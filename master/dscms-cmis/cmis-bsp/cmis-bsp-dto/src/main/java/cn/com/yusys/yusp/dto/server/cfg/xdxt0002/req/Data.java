package cn.com.yusys.yusp.dto.server.cfg.xdxt0002.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/24 21:16:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/24 21:16
 * @since 2021/5/24 21:16
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "teamType")
    private String teamType;

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    @Override
    public String toString() {
        return "Data{" +
                "teamType='" + teamType + '\'' +
                '}';
    }
}
