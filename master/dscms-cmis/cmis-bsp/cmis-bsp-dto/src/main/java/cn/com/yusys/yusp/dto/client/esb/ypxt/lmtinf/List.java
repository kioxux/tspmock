package cn.com.yusys.yusp.dto.client.esb.ypxt.lmtinf;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信贷授信协议信息同步
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "sxxybh")
    private String sxxybh;//授信协议号
    @JsonProperty(value = "sxkhbh")
    private String sxkhbh;//授信客户码
    @JsonProperty(value = "sxjeyp")
    private BigDecimal sxjeyp;//授信金额
    @JsonProperty(value = "sxbzyp")
    private String sxbzyp;//授信币种
    @JsonProperty(value = "sxqsrq")
    private String sxqsrq;//授信起始日期
    @JsonProperty(value = "sxdqrq")
    private String sxdqrq;//授信到期日期
    @JsonProperty(value = "sxxyzt")
    private String sxxyzt;//授信协议状态
    @JsonProperty(value = "khghjl")
    private String khghjl;//管户客户经理
    @JsonProperty(value = "khghjg")
    private String khghjg;//管理机构

    public String getSxxybh() {
        return sxxybh;
    }

    public void setSxxybh(String sxxybh) {
        this.sxxybh = sxxybh;
    }

    public String getSxkhbh() {
        return sxkhbh;
    }

    public void setSxkhbh(String sxkhbh) {
        this.sxkhbh = sxkhbh;
    }

    public BigDecimal getSxjeyp() {
        return sxjeyp;
    }

    public void setSxjeyp(BigDecimal sxjeyp) {
        this.sxjeyp = sxjeyp;
    }

    public String getSxbzyp() {
        return sxbzyp;
    }

    public void setSxbzyp(String sxbzyp) {
        this.sxbzyp = sxbzyp;
    }

    public String getSxqsrq() {
        return sxqsrq;
    }

    public void setSxqsrq(String sxqsrq) {
        this.sxqsrq = sxqsrq;
    }

    public String getSxdqrq() {
        return sxdqrq;
    }

    public void setSxdqrq(String sxdqrq) {
        this.sxdqrq = sxdqrq;
    }

    public String getSxxyzt() {
        return sxxyzt;
    }

    public void setSxxyzt(String sxxyzt) {
        this.sxxyzt = sxxyzt;
    }

    public String getKhghjl() {
        return khghjl;
    }

    public void setKhghjl(String khghjl) {
        this.khghjl = khghjl;
    }

    public String getKhghjg() {
        return khghjg;
    }

    public void setKhghjg(String khghjg) {
        this.khghjg = khghjg;
    }

    @Override
    public String toString() {
        return "List{" +
                "sxxybh='" + sxxybh + '\'' +
                ", sxkhbh='" + sxkhbh + '\'' +
                ", sxjeyp=" + sxjeyp +
                ", sxbzyp='" + sxbzyp + '\'' +
                ", sxqsrq='" + sxqsrq + '\'' +
                ", sxdqrq='" + sxdqrq + '\'' +
                ", sxxyzt='" + sxxyzt + '\'' +
                ", khghjl='" + khghjl + '\'' +
                ", khghjg='" + khghjg + '\'' +
                '}';
    }
}
