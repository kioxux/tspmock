package cn.com.yusys.yusp.dto.client.esb.core.mbt952.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/16 15:41
 * @since 2021/6/16 15:41
 */
@JsonPropertyOrder(alphabetic = true)
public class CplWjplxxOut implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "picriqii")
    private String picriqii;//批次日期
    @JsonProperty(value = "picihaoo")
    private String picihaoo;//批次号
    @JsonProperty(value = "zongbshu")
    private Integer zongbshu;//总笔数
    @JsonProperty(value = "filename")
    private String filename;//报表文件名
    @JsonProperty(value = "zongjine")
    private BigDecimal zongjine;//总金额
    @JsonProperty(value = "zhixztai")
    private String zhixztai;//执行状态
    @JsonProperty(value = "chgobshu")
    private Integer chgobshu;//成功笔数
    @JsonProperty(value = "shbabshu")
    private Integer shbabshu;//失败笔数
    @JsonProperty(value = "cuowuxix")
    private String cuowuxix;//错误信息
    @JsonProperty(value = "weituoha")
    private String weituoha;//委托号
    @JsonProperty(value = "koukzhao")
    private String koukzhao;//扣款主账号
    @JsonProperty(value = "koukzhmc")
    private String koukzhmc;//扣款账户名称
    @JsonProperty(value = "dlywhaoo")
    private String dlywhaoo;//代理业务号
    @JsonProperty(value = "zhaiyodm")
    private String zhaiyodm;//摘要代码
    @JsonProperty(value = "lururiqi")
    private String lururiqi;//录入日期
    @JsonProperty(value = "lurujgou")
    private String lurujgou;//录入机构
    @JsonProperty(value = "luruguiy")
    private String luruguiy;//录入柜员号
    @JsonProperty(value = "picileib")
    private String picileib;//批次类别
    @JsonProperty(value = "jizhngje")
    private BigDecimal jizhngje;//记账金额
    @JsonProperty(value = "shbajine")
    private BigDecimal shbajine;//失败金额
    @JsonProperty(value = "guiylius")
    private String guiylius;//柜员流水号

    public String getPicriqii() {
        return picriqii;
    }

    public void setPicriqii(String picriqii) {
        this.picriqii = picriqii;
    }

    public String getPicihaoo() {
        return picihaoo;
    }

    public void setPicihaoo(String picihaoo) {
        this.picihaoo = picihaoo;
    }

    public Integer getZongbshu() {
        return zongbshu;
    }

    public void setZongbshu(Integer zongbshu) {
        this.zongbshu = zongbshu;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public BigDecimal getZongjine() {
        return zongjine;
    }

    public void setZongjine(BigDecimal zongjine) {
        this.zongjine = zongjine;
    }

    public String getZhixztai() {
        return zhixztai;
    }

    public void setZhixztai(String zhixztai) {
        this.zhixztai = zhixztai;
    }

    public Integer getChgobshu() {
        return chgobshu;
    }

    public void setChgobshu(Integer chgobshu) {
        this.chgobshu = chgobshu;
    }

    public Integer getShbabshu() {
        return shbabshu;
    }

    public void setShbabshu(Integer shbabshu) {
        this.shbabshu = shbabshu;
    }

    public String getCuowuxix() {
        return cuowuxix;
    }

    public void setCuowuxix(String cuowuxix) {
        this.cuowuxix = cuowuxix;
    }

    public String getWeituoha() {
        return weituoha;
    }

    public void setWeituoha(String weituoha) {
        this.weituoha = weituoha;
    }

    public String getKoukzhao() {
        return koukzhao;
    }

    public void setKoukzhao(String koukzhao) {
        this.koukzhao = koukzhao;
    }

    public String getKoukzhmc() {
        return koukzhmc;
    }

    public void setKoukzhmc(String koukzhmc) {
        this.koukzhmc = koukzhmc;
    }

    public String getDlywhaoo() {
        return dlywhaoo;
    }

    public void setDlywhaoo(String dlywhaoo) {
        this.dlywhaoo = dlywhaoo;
    }

    public String getZhaiyodm() {
        return zhaiyodm;
    }

    public void setZhaiyodm(String zhaiyodm) {
        this.zhaiyodm = zhaiyodm;
    }

    public String getLururiqi() {
        return lururiqi;
    }

    public void setLururiqi(String lururiqi) {
        this.lururiqi = lururiqi;
    }

    public String getLurujgou() {
        return lurujgou;
    }

    public void setLurujgou(String lurujgou) {
        this.lurujgou = lurujgou;
    }

    public String getLuruguiy() {
        return luruguiy;
    }

    public void setLuruguiy(String luruguiy) {
        this.luruguiy = luruguiy;
    }

    public String getPicileib() {
        return picileib;
    }

    public void setPicileib(String picileib) {
        this.picileib = picileib;
    }

    public BigDecimal getJizhngje() {
        return jizhngje;
    }

    public void setJizhngje(BigDecimal jizhngje) {
        this.jizhngje = jizhngje;
    }

    public BigDecimal getShbajine() {
        return shbajine;
    }

    public void setShbajine(BigDecimal shbajine) {
        this.shbajine = shbajine;
    }

    public String getGuiylius() {
        return guiylius;
    }

    public void setGuiylius(String guiylius) {
        this.guiylius = guiylius;
    }

    @Override
    public String toString() {
        return "Mbt952RespDto{" +
                "picriqii='" + picriqii + '\'' +
                "picihaoo='" + picihaoo + '\'' +
                "zongbshu='" + zongbshu + '\'' +
                "filename='" + filename + '\'' +
                "zongjine='" + zongjine + '\'' +
                "zhixztai='" + zhixztai + '\'' +
                "chgobshu='" + chgobshu + '\'' +
                "shbabshu='" + shbabshu + '\'' +
                "cuowuxix='" + cuowuxix + '\'' +
                "weituoha='" + weituoha + '\'' +
                "koukzhao='" + koukzhao + '\'' +
                "koukzhmc='" + koukzhmc + '\'' +
                "dlywhaoo='" + dlywhaoo + '\'' +
                "zhaiyodm='" + zhaiyodm + '\'' +
                "lururiqi='" + lururiqi + '\'' +
                "lurujgou='" + lurujgou + '\'' +
                "luruguiy='" + luruguiy + '\'' +
                "picileib='" + picileib + '\'' +
                "jizhngje='" + jizhngje + '\'' +
                "shbajine='" + shbajine + '\'' +
                "guiylius='" + guiylius + '\'' +
                '}';
    }
}
