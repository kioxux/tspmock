package cn.com.yusys.yusp.dto.client.esb.irs.irs28.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：财务信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs28RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Irs28RespDto{" +
                '}';
    }
}  
