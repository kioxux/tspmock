package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj23.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查询批次出账票据信息（新信贷调票据）
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj23RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;//list
    @JsonProperty(value = "totalRecordNum")
    private Integer totalRecordNum;//总数据
    @JsonProperty(value = "totleAmount")
    private BigDecimal totleAmount;//总金额

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public Integer getTotalRecordNum() {
        return totalRecordNum;
    }

    public void setTotalRecordNum(Integer totalRecordNum) {
        this.totalRecordNum = totalRecordNum;
    }

    public BigDecimal getTotleAmount() {
        return totleAmount;
    }

    public void setTotleAmount(BigDecimal totleAmount) {
        this.totleAmount = totleAmount;
    }

    @Override
    public String toString() {
        return "Xdpj23RespDto{" +
                "list=" + list +
                ", totalRecordNum=" + totalRecordNum +
                ", totleAmount=" + totleAmount +
                '}';
    }
}
