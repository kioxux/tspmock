package cn.com.yusys.yusp.dto.client.esb.lsnp.lsnp01.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信贷业务零售评级
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lsnp01ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "apply_seq")
    private String apply_seq;//申请流水号
    @JsonProperty(value = "cust_no")
    private String cust_no;//客户编号
    @JsonProperty(value = "cust_type")
    private String cust_type;//客户类型
    @JsonProperty(value = "cert_type")
    private String cert_type;//证件类型
    @JsonProperty(value = "cert_no")
    private String cert_no;//证件号码
    @JsonProperty(value = "age")
    private String age;//年龄（年）
    @JsonProperty(value = "indus_type")
    private String indus_type;//行业类别
    @JsonProperty(value = "sex")
    private String sex;//性别
    @JsonProperty(value = "marry_stat")
    private String marry_stat;//婚姻状况
    @JsonProperty(value = "profession")
    private String profession;//职业
    @JsonProperty(value = "max_degree")
    private String max_degree;//最高学位
    @JsonProperty(value = "corp_estb_dura")
    private Integer corp_estb_dura;//企业成立年限(距所在月份时点)
    @JsonProperty(value = "base_acct_flag")
    private String base_acct_flag;//本行开设基本账户标志
    @JsonProperty(value = "corp_prop")
    private String corp_prop;//公司性质
    @JsonProperty(value = "baby_flag")
    private String baby_flag;//有无子女
    @JsonProperty(value = "cust_dura")
    private Integer cust_dura;//成为我行客户时长（月）
    @JsonProperty(value = "fam_debt_div_asset")
    private BigDecimal fam_debt_div_asset;//家庭资负债比
    @JsonProperty(value = "oper_dura")
    private Integer oper_dura;//本地经营期限
    @JsonProperty(value = "owned_money_rate")
    private BigDecimal owned_money_rate;//自筹资金比例
    @JsonProperty(value = "estate_flag")
    private String estate_flag;//有无房产
    @JsonProperty(value = "cust_name")
    private String cust_name;//客户名称
    @JsonProperty(value = "tel")
    private String tel;//手机号码
    @JsonProperty(value = "corp_name")
    private String corp_name;//经营企业的单位名称
    @JsonProperty(value = "org_cred_cd")
    private String org_cred_cd;//经营企业的企业组织代码证
    @JsonProperty(value = "loan_card_code")
    private String loan_card_code;//经营企业的中征码
    @JsonProperty(value = "sale_price")
    private BigDecimal sale_price;//房屋销售价格
    @JsonProperty(value = "sale_proceeds")
    private BigDecimal sale_proceeds;//年可核实销售收入
    @JsonProperty(value = "net_assets")
    private BigDecimal net_assets;//借款企业和借款人家庭净资产
    @JsonProperty(value = "total_income")
    private BigDecimal total_income;//个人月收入
    @JsonProperty(value = "legal_cert_no")
    private String legal_cert_no;//企业法人身份证号
    @JsonProperty(value = "legal_cust_name")
    private String legal_cust_name;//企业法人姓名
    @JsonProperty(value = "list")
    private java.util.List<List> list;


    public String getApply_seq() {
        return apply_seq;
    }

    public void setApply_seq(String apply_seq) {
        this.apply_seq = apply_seq;
    }

    public String getCust_no() {
        return cust_no;
    }

    public void setCust_no(String cust_no) {
        this.cust_no = cust_no;
    }

    public String getCust_type() {
        return cust_type;
    }

    public void setCust_type(String cust_type) {
        this.cust_type = cust_type;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_no() {
        return cert_no;
    }

    public void setCert_no(String cert_no) {
        this.cert_no = cert_no;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getIndus_type() {
        return indus_type;
    }

    public void setIndus_type(String indus_type) {
        this.indus_type = indus_type;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getMarry_stat() {
        return marry_stat;
    }

    public void setMarry_stat(String marry_stat) {
        this.marry_stat = marry_stat;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getMax_degree() {
        return max_degree;
    }

    public void setMax_degree(String max_degree) {
        this.max_degree = max_degree;
    }

    public Integer getCorp_estb_dura() {
        return corp_estb_dura;
    }

    public void setCorp_estb_dura(Integer corp_estb_dura) {
        this.corp_estb_dura = corp_estb_dura;
    }

    public String getBase_acct_flag() {
        return base_acct_flag;
    }

    public void setBase_acct_flag(String base_acct_flag) {
        this.base_acct_flag = base_acct_flag;
    }

    public String getCorp_prop() {
        return corp_prop;
    }

    public void setCorp_prop(String corp_prop) {
        this.corp_prop = corp_prop;
    }

    public String getBaby_flag() {
        return baby_flag;
    }

    public void setBaby_flag(String baby_flag) {
        this.baby_flag = baby_flag;
    }

    public Integer getCust_dura() {
        return cust_dura;
    }

    public void setCust_dura(Integer cust_dura) {
        this.cust_dura = cust_dura;
    }

    public BigDecimal getFam_debt_div_asset() {
        return fam_debt_div_asset;
    }

    public void setFam_debt_div_asset(BigDecimal fam_debt_div_asset) {
        this.fam_debt_div_asset = fam_debt_div_asset;
    }

    public Integer getOper_dura() {
        return oper_dura;
    }

    public void setOper_dura(Integer oper_dura) {
        this.oper_dura = oper_dura;
    }

    public BigDecimal getOwned_money_rate() {
        return owned_money_rate;
    }

    public void setOwned_money_rate(BigDecimal owned_money_rate) {
        this.owned_money_rate = owned_money_rate;
    }

    public String getEstate_flag() {
        return estate_flag;
    }

    public void setEstate_flag(String estate_flag) {
        this.estate_flag = estate_flag;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getCorp_name() {
        return corp_name;
    }

    public void setCorp_name(String corp_name) {
        this.corp_name = corp_name;
    }

    public String getOrg_cred_cd() {
        return org_cred_cd;
    }

    public void setOrg_cred_cd(String org_cred_cd) {
        this.org_cred_cd = org_cred_cd;
    }

    public String getLoan_card_code() {
        return loan_card_code;
    }

    public void setLoan_card_code(String loan_card_code) {
        this.loan_card_code = loan_card_code;
    }

    public BigDecimal getSale_price() {
        return sale_price;
    }

    public void setSale_price(BigDecimal sale_price) {
        this.sale_price = sale_price;
    }

    public BigDecimal getSale_proceeds() {
        return sale_proceeds;
    }

    public void setSale_proceeds(BigDecimal sale_proceeds) {
        this.sale_proceeds = sale_proceeds;
    }

    public BigDecimal getNet_assets() {
        return net_assets;
    }

    public void setNet_assets(BigDecimal net_assets) {
        this.net_assets = net_assets;
    }

    public BigDecimal getTotal_income() {
        return total_income;
    }

    public void setTotal_income(BigDecimal total_income) {
        this.total_income = total_income;
    }

    public String getLegal_cert_no() {
        return legal_cert_no;
    }

    public void setLegal_cert_no(String legal_cert_no) {
        this.legal_cert_no = legal_cert_no;
    }

    public String getLegal_cust_name() {
        return legal_cust_name;
    }

    public void setLegal_cust_name(String legal_cust_name) {
        this.legal_cust_name = legal_cust_name;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Lsnp01ReqDto{" +
                "apply_seq='" + apply_seq + '\'' +
                ", cust_no='" + cust_no + '\'' +
                ", cust_type='" + cust_type + '\'' +
                ", cert_type='" + cert_type + '\'' +
                ", cert_no='" + cert_no + '\'' +
                ", age='" + age + '\'' +
                ", indus_type='" + indus_type + '\'' +
                ", sex='" + sex + '\'' +
                ", marry_stat='" + marry_stat + '\'' +
                ", profession='" + profession + '\'' +
                ", max_degree='" + max_degree + '\'' +
                ", corp_estb_dura=" + corp_estb_dura +
                ", base_acct_flag='" + base_acct_flag + '\'' +
                ", corp_prop='" + corp_prop + '\'' +
                ", baby_flag='" + baby_flag + '\'' +
                ", cust_dura=" + cust_dura +
                ", fam_debt_div_asset=" + fam_debt_div_asset +
                ", oper_dura=" + oper_dura +
                ", owned_money_rate=" + owned_money_rate +
                ", estate_flag='" + estate_flag + '\'' +
                ", cust_name='" + cust_name + '\'' +
                ", tel='" + tel + '\'' +
                ", corp_name='" + corp_name + '\'' +
                ", org_cred_cd='" + org_cred_cd + '\'' +
                ", loan_card_code='" + loan_card_code + '\'' +
                ", sale_price=" + sale_price +
                ", sale_proceeds=" + sale_proceeds +
                ", net_assets=" + net_assets +
                ", total_income=" + total_income +
                ", legal_cert_no='" + legal_cert_no + '\'' +
                ", legal_cust_name='" + legal_cust_name + '\'' +
                ", list=" + list +
                '}';
    }
}
