package cn.com.yusys.yusp.dto.server.biz.xdht0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 请求Data：查询符合条件的省心快贷合同
 *
 * @author xll
 * @version 1.0
 */
public class Data {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certNo")
    private String certNo;//身份证号
    @JsonProperty(value = "cusType")
    private String cusType;//客户类型
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "managerId")
    private String managerId;//客户经理号
    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "signFlag")
    private String signFlag;//是否签约
    @JsonProperty(value = "chnlSour")
    private String chnlSour;//来源渠道

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getSignFlag() {
        return signFlag;
    }

    public void setSignFlag(String signFlag) {
        this.signFlag = signFlag;
    }

    public String getChnlSour() {
        return chnlSour;
    }

    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour;
    }

    @Override
    public String toString() {
        return "Data{" +
                "certNo='" + certNo + '\'' +
                "cusType='" + cusType + '\'' +
                "cusId='" + cusId + '\'' +
                "cusName='" + cusName + '\'' +
                "managerId='" + managerId + '\'' +
                "contNo='" + contNo + '\'' +
                "signFlag='" + signFlag + '\'' +
                "chnlSour='" + chnlSour + '\'' +
                '}';
    }
}
