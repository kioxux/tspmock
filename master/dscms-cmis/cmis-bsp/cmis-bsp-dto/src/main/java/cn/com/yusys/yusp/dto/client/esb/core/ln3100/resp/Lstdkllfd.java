package cn.com.yusys.yusp.dto.client.esb.core.ln3100.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款利率分段登记
 *
 * @author  chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkllfd implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "qishriqi")
    private String qishriqi;//起始日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "lilvleix")
    private String lilvleix;//利率类型
    @JsonProperty(value = "lilvtzfs")
    private String lilvtzfs;//利率调整方式
    @JsonProperty(value = "lilvtzzq")
    private String lilvtzzq;//利率调整周期
    @JsonProperty(value = "lilvfdfs")
    private String lilvfdfs;//利率浮动方式
    @JsonProperty(value = "lilvfdzh")
    private BigDecimal lilvfdzh;//利率浮动值
    @JsonProperty(value = "zhchlilv")
    private BigDecimal zhchlilv;//正常利率
    @JsonProperty(value = "zclilvbh")
    private String zclilvbh;//正常利率编号

    public String getQishriqi() {
        return qishriqi;
    }

    public void setQishriqi(String qishriqi) {
        this.qishriqi = qishriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getLilvleix() {
        return lilvleix;
    }

    public void setLilvleix(String lilvleix) {
        this.lilvleix = lilvleix;
    }

    public String getLilvtzfs() {
        return lilvtzfs;
    }

    public void setLilvtzfs(String lilvtzfs) {
        this.lilvtzfs = lilvtzfs;
    }

    public String getLilvtzzq() {
        return lilvtzzq;
    }

    public void setLilvtzzq(String lilvtzzq) {
        this.lilvtzzq = lilvtzzq;
    }

    public String getLilvfdfs() {
        return lilvfdfs;
    }

    public void setLilvfdfs(String lilvfdfs) {
        this.lilvfdfs = lilvfdfs;
    }

    public BigDecimal getLilvfdzh() {
        return lilvfdzh;
    }

    public void setLilvfdzh(BigDecimal lilvfdzh) {
        this.lilvfdzh = lilvfdzh;
    }

    public BigDecimal getZhchlilv() {
        return zhchlilv;
    }

    public void setZhchlilv(BigDecimal zhchlilv) {
        this.zhchlilv = zhchlilv;
    }

    public String getZclilvbh() {
        return zclilvbh;
    }

    public void setZclilvbh(String zclilvbh) {
        this.zclilvbh = zclilvbh;
    }

    @Override
    public String toString() {
        return "LstdkllfdRespDto{" +
                "qishriqi='" + qishriqi + '\'' +
                "daoqriqi='" + daoqriqi + '\'' +
                "lilvleix='" + lilvleix + '\'' +
                "lilvtzfs='" + lilvtzfs + '\'' +
                "lilvtzzq='" + lilvtzzq + '\'' +
                "lilvfdfs='" + lilvfdfs + '\'' +
                "lilvfdzh='" + lilvfdzh + '\'' +
                "zhchlilv='" + zhchlilv + '\'' +
                "zclilvbh='" + zclilvbh + '\'' +
                '}';
    }
}  
