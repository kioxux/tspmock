package cn.com.yusys.yusp.dto.client.esb.irs.irs98;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应DTO：授信申请债项评级
 *
 * @author muxiang
 * @version 1.0
 * @since 2021年4月15日 上午11:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class Irs98RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "AppMessageInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo> AppMessageInfo;
    @JsonProperty(value = "AllMessageInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo> AllMessageInfo;
    @JsonProperty(value = "MessageInfo")
    private List<cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo> MessageInfo;

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo> getAppMessageInfo() {
        return AppMessageInfo;
    }

    @JsonIgnore
    public void setAppMessageInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AppMessageInfo> appMessageInfo) {
        AppMessageInfo = appMessageInfo;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo> getAllMessageInfo() {
        return AllMessageInfo;
    }

    @JsonIgnore
    public void setAllMessageInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.AllMessageInfo> allMessageInfo) {
        AllMessageInfo = allMessageInfo;
    }

    @JsonIgnore
    public List<cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo> getMessageInfo() {
        return MessageInfo;
    }

    @JsonIgnore
    public void setMessageInfo(List<cn.com.yusys.yusp.dto.client.esb.irs.common.MessageInfo> messageInfo) {
        MessageInfo = messageInfo;
    }

    @Override
    public String toString() {
        return "Irs98RespDto{" +
                "AppMessageInfo=" + AppMessageInfo +
                ", AllMessageInfo=" + AllMessageInfo +
                ", MessageInfo=" + MessageInfo +
                '}';
    }
}
