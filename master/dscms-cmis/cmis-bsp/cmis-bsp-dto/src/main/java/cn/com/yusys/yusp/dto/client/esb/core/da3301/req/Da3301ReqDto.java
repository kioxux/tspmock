package cn.com.yusys.yusp.dto.client.esb.core.da3301.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：抵债资产入账
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Da3301ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dizruzfs")
    private String dizruzfs;//抵债入账方式
    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "yngyjigo")
    private String yngyjigo;//营业机构
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "dcldzcje")
    private BigDecimal dcldzcje;//待处理抵债资产金额
    @JsonProperty(value = "hfeijine")
    private BigDecimal hfeijine;//还费金额
    @JsonProperty(value = "zijinqux")
    private String zijinqux;//资金去向
    @JsonProperty(value = "dzzszhao")
    private String dzzszhao;//抵债暂收账号
    @JsonProperty(value = "dzzszzxh")
    private String dzzszzxh;//抵债暂收账号子序号
    @JsonProperty(value = "dzzcqdfs")
    private String dzzcqdfs;//抵债资产取得方式
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额
    @JsonProperty(value = "listnm0")
    private List<Listnm0> listnm0;//抵债资产费用明细
    @JsonProperty(value = "listnm1")
    private List<Listnm1> listnm1;//抵债资产与贷款关联表

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDizruzfs() {
        return dizruzfs;
    }

    public void setDizruzfs(String dizruzfs) {
        this.dizruzfs = dizruzfs;
    }

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public String getYngyjigo() {
        return yngyjigo;
    }

    public void setYngyjigo(String yngyjigo) {
        this.yngyjigo = yngyjigo;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public BigDecimal getDcldzcje() {
        return dcldzcje;
    }

    public void setDcldzcje(BigDecimal dcldzcje) {
        this.dcldzcje = dcldzcje;
    }

    public BigDecimal getHfeijine() {
        return hfeijine;
    }

    public void setHfeijine(BigDecimal hfeijine) {
        this.hfeijine = hfeijine;
    }

    public String getZijinqux() {
        return zijinqux;
    }

    public void setZijinqux(String zijinqux) {
        this.zijinqux = zijinqux;
    }

    public String getDzzszhao() {
        return dzzszhao;
    }

    public void setDzzszhao(String dzzszhao) {
        this.dzzszhao = dzzszhao;
    }

    public String getDzzszzxh() {
        return dzzszzxh;
    }

    public void setDzzszzxh(String dzzszzxh) {
        this.dzzszzxh = dzzszzxh;
    }

    public String getDzzcqdfs() {
        return dzzcqdfs;
    }

    public void setDzzcqdfs(String dzzcqdfs) {
        this.dzzcqdfs = dzzcqdfs;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    public List<Listnm0> getListnm0() {
        return listnm0;
    }

    public void setListnm0(List<Listnm0> listnm0) {
        this.listnm0 = listnm0;
    }

    public List<Listnm1> getListnm1() {
        return listnm1;
    }

    public void setListnm1(List<Listnm1> listnm1) {
        this.listnm1 = listnm1;
    }

    @Override
	public String toString() {
		return "Da3301ReqDto{" +
				"daikczbz='" + daikczbz + '\'' +
				", dizruzfs='" + dizruzfs + '\'' +
				", dzzcbhao='" + dzzcbhao + '\'' +
				", dzzcminc='" + dzzcminc + '\'' +
				", yngyjigo='" + yngyjigo + '\'' +
				", kehuhaoo='" + kehuhaoo + '\'' +
				", kehmingc='" + kehmingc + '\'' +
				", dcldzcje=" + dcldzcje +
				", hfeijine=" + hfeijine +
				", zijinqux='" + zijinqux + '\'' +
				", dzzszhao='" + dzzszhao + '\'' +
				", dzzszzxh='" + dzzszzxh + '\'' +
				", dzzcqdfs='" + dzzcqdfs + '\'' +
				", huankjee=" + huankjee +
				", listnm0=" + listnm0 +
				", listnm1=" + listnm1 +
				'}';
	}
}
