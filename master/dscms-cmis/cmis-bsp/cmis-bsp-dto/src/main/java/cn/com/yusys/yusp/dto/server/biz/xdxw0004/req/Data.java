package cn.com.yusys.yusp.dto.server.biz.xdxw0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：小微营业额校验查询
 *
 * @author xuchao
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "surveyNo")
    private String surveyNo;//调查流水号


    @Override
    public String toString() {
        return "Xdxw0004ReqDto{" +
                "surveyNo='" + surveyNo + '\'' +
                '}';
    }
}  
