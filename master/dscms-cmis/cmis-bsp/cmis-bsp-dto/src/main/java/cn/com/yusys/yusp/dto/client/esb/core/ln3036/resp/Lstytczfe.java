package cn.com.yusys.yusp.dto.client.esb.core.ln3036.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class Lstytczfe implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ruzjigou")
    private String ruzjigou;//入账机构
    @JsonProperty(value = "jigoumch")
    private String jigoumch;//机构名称
    @JsonProperty(value = "lhdkleix")
    private String lhdkleix;//联合贷款类型
    @JsonProperty(value = "chzfkehh")
    private String chzfkehh;//出资方客户号
    @JsonProperty(value = "chzfkhmc")
    private String chzfkhmc;//出资方客户名称
    @JsonProperty(value = "chzfhhao")
    private String chzfhhao;//出资方行号
    @JsonProperty(value = "czfhming")
    private String czfhming;//出资方行名
    @JsonProperty(value = "czfzhzxh")
    private String czfzhzxh;//出资方账号子序号
    @JsonProperty(value = "chuzfzhh")
    private String chuzfzhh;//出资方账号
    @JsonProperty(value = "chzfzhmc")
    private String chzfzhmc;//出资方账户名称
    @JsonProperty(value = "chuzbili")
    private BigDecimal chuzbili;//出资比例
    @JsonProperty(value = "chuzjine")
    private BigDecimal chuzjine;//出资金额
    @JsonProperty(value = "shoukzhh")
    private String shoukzhh;//收款账号
    @JsonProperty(value = "skzhhzxh")
    private String skzhhzxh;//收款账号子序号
    @JsonProperty(value = "skzhhmch")
    private String skzhhmch;//收款账户名称
    @JsonProperty(value = "nyuelilv")
    private String nyuelilv;//年/月利率标识
    @JsonProperty(value = "hetongll")
    private BigDecimal hetongll;//合同利率
    @JsonProperty(value = "yqllcklx")
    private String yqllcklx;//逾期利率参考类型
    @JsonProperty(value = "yuqillbh")
    private String yuqillbh;//逾期利率编号
    @JsonProperty(value = "yuqinyll")
    private String yuqinyll;//逾期年月利率
    @JsonProperty(value = "yuqililv")
    private BigDecimal yuqililv;//逾期利率
    @JsonProperty(value = "yuqitzfs")
    private String yuqitzfs;//逾期利率调整方式
    @JsonProperty(value = "yuqitzzq")
    private String yuqitzzq;//逾期利率调整周期
    @JsonProperty(value = "yqfxfdfs")
    private String yqfxfdfs;//逾期罚息浮动方式
    @JsonProperty(value = "yqfxfdzh")
    private BigDecimal yqfxfdzh;//逾期罚息浮动值
    @JsonProperty(value = "flllcklx")
    private String flllcklx;//复利利率参考类型
    @JsonProperty(value = "fulilvbh")
    private String fulilvbh;//复利利率编号
    @JsonProperty(value = "fulilvny")
    private String fulilvny;//复利利率年月标识
    @JsonProperty(value = "fulililv")
    private BigDecimal fulililv;//复利利率
    @JsonProperty(value = "fulitzfs")
    private String fulitzfs;//复利利率调整方式
    @JsonProperty(value = "fulitzzq")
    private String fulitzzq;//复利利率调整周期
    @JsonProperty(value = "fulifdfs")
    private String fulifdfs;//复利利率浮动方式
    @JsonProperty(value = "fulifdzh")
    private BigDecimal fulifdzh;//复利利率浮动值
    @JsonProperty(value = "dfkbjzhh")
    private String dfkbjzhh;//待付款本金账号
    @JsonProperty(value = "dfkbjzxh")
    private String dfkbjzxh;//待付款本金账号子序号
    @JsonProperty(value = "dfklxzhh")
    private String dfklxzhh;//待付款利息账号
    @JsonProperty(value = "dfklxzxh")
    private String dfklxzxh;//待付款利息账号子序号
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "nbjiejuh")
    private String nbjiejuh;//内部借据号
    @JsonProperty(value = "xieyjine")
    private BigDecimal xieyjine;//协议金额
    @JsonProperty(value = "xieybili")
    private BigDecimal xieybili;//协议比例

    public String getRuzjigou() {
        return ruzjigou;
    }

    public void setRuzjigou(String ruzjigou) {
        this.ruzjigou = ruzjigou;
    }

    public String getJigoumch() {
        return jigoumch;
    }

    public void setJigoumch(String jigoumch) {
        this.jigoumch = jigoumch;
    }

    public String getLhdkleix() {
        return lhdkleix;
    }

    public void setLhdkleix(String lhdkleix) {
        this.lhdkleix = lhdkleix;
    }

    public String getChzfkehh() {
        return chzfkehh;
    }

    public void setChzfkehh(String chzfkehh) {
        this.chzfkehh = chzfkehh;
    }

    public String getChzfkhmc() {
        return chzfkhmc;
    }

    public void setChzfkhmc(String chzfkhmc) {
        this.chzfkhmc = chzfkhmc;
    }

    public String getChzfhhao() {
        return chzfhhao;
    }

    public void setChzfhhao(String chzfhhao) {
        this.chzfhhao = chzfhhao;
    }

    public String getCzfhming() {
        return czfhming;
    }

    public void setCzfhming(String czfhming) {
        this.czfhming = czfhming;
    }

    public String getCzfzhzxh() {
        return czfzhzxh;
    }

    public void setCzfzhzxh(String czfzhzxh) {
        this.czfzhzxh = czfzhzxh;
    }

    public String getChuzfzhh() {
        return chuzfzhh;
    }

    public void setChuzfzhh(String chuzfzhh) {
        this.chuzfzhh = chuzfzhh;
    }

    public String getChzfzhmc() {
        return chzfzhmc;
    }

    public void setChzfzhmc(String chzfzhmc) {
        this.chzfzhmc = chzfzhmc;
    }

    public BigDecimal getChuzbili() {
        return chuzbili;
    }

    public void setChuzbili(BigDecimal chuzbili) {
        this.chuzbili = chuzbili;
    }

    public BigDecimal getChuzjine() {
        return chuzjine;
    }

    public void setChuzjine(BigDecimal chuzjine) {
        this.chuzjine = chuzjine;
    }

    public String getShoukzhh() {
        return shoukzhh;
    }

    public void setShoukzhh(String shoukzhh) {
        this.shoukzhh = shoukzhh;
    }

    public String getSkzhhzxh() {
        return skzhhzxh;
    }

    public void setSkzhhzxh(String skzhhzxh) {
        this.skzhhzxh = skzhhzxh;
    }

    public String getSkzhhmch() {
        return skzhhmch;
    }

    public void setSkzhhmch(String skzhhmch) {
        this.skzhhmch = skzhhmch;
    }

    public String getNyuelilv() {
        return nyuelilv;
    }

    public void setNyuelilv(String nyuelilv) {
        this.nyuelilv = nyuelilv;
    }

    public BigDecimal getHetongll() {
        return hetongll;
    }

    public void setHetongll(BigDecimal hetongll) {
        this.hetongll = hetongll;
    }

    public String getYqllcklx() {
        return yqllcklx;
    }

    public void setYqllcklx(String yqllcklx) {
        this.yqllcklx = yqllcklx;
    }

    public String getYuqillbh() {
        return yuqillbh;
    }

    public void setYuqillbh(String yuqillbh) {
        this.yuqillbh = yuqillbh;
    }

    public String getYuqinyll() {
        return yuqinyll;
    }

    public void setYuqinyll(String yuqinyll) {
        this.yuqinyll = yuqinyll;
    }

    public BigDecimal getYuqililv() {
        return yuqililv;
    }

    public void setYuqililv(BigDecimal yuqililv) {
        this.yuqililv = yuqililv;
    }

    public String getYuqitzfs() {
        return yuqitzfs;
    }

    public void setYuqitzfs(String yuqitzfs) {
        this.yuqitzfs = yuqitzfs;
    }

    public String getYuqitzzq() {
        return yuqitzzq;
    }

    public void setYuqitzzq(String yuqitzzq) {
        this.yuqitzzq = yuqitzzq;
    }

    public String getYqfxfdfs() {
        return yqfxfdfs;
    }

    public void setYqfxfdfs(String yqfxfdfs) {
        this.yqfxfdfs = yqfxfdfs;
    }

    public BigDecimal getYqfxfdzh() {
        return yqfxfdzh;
    }

    public void setYqfxfdzh(BigDecimal yqfxfdzh) {
        this.yqfxfdzh = yqfxfdzh;
    }

    public String getFlllcklx() {
        return flllcklx;
    }

    public void setFlllcklx(String flllcklx) {
        this.flllcklx = flllcklx;
    }

    public String getFulilvbh() {
        return fulilvbh;
    }

    public void setFulilvbh(String fulilvbh) {
        this.fulilvbh = fulilvbh;
    }

    public String getFulilvny() {
        return fulilvny;
    }

    public void setFulilvny(String fulilvny) {
        this.fulilvny = fulilvny;
    }

    public BigDecimal getFulililv() {
        return fulililv;
    }

    public void setFulililv(BigDecimal fulililv) {
        this.fulililv = fulililv;
    }

    public String getFulitzfs() {
        return fulitzfs;
    }

    public void setFulitzfs(String fulitzfs) {
        this.fulitzfs = fulitzfs;
    }

    public String getFulitzzq() {
        return fulitzzq;
    }

    public void setFulitzzq(String fulitzzq) {
        this.fulitzzq = fulitzzq;
    }

    public String getFulifdfs() {
        return fulifdfs;
    }

    public void setFulifdfs(String fulifdfs) {
        this.fulifdfs = fulifdfs;
    }

    public BigDecimal getFulifdzh() {
        return fulifdzh;
    }

    public void setFulifdzh(BigDecimal fulifdzh) {
        this.fulifdzh = fulifdzh;
    }

    public String getDfkbjzhh() {
        return dfkbjzhh;
    }

    public void setDfkbjzhh(String dfkbjzhh) {
        this.dfkbjzhh = dfkbjzhh;
    }

    public String getDfkbjzxh() {
        return dfkbjzxh;
    }

    public void setDfkbjzxh(String dfkbjzxh) {
        this.dfkbjzxh = dfkbjzxh;
    }

    public String getDfklxzhh() {
        return dfklxzhh;
    }

    public void setDfklxzhh(String dfklxzhh) {
        this.dfklxzhh = dfklxzhh;
    }

    public String getDfklxzxh() {
        return dfklxzxh;
    }

    public void setDfklxzxh(String dfklxzxh) {
        this.dfklxzxh = dfklxzxh;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getNbjiejuh() {
        return nbjiejuh;
    }

    public void setNbjiejuh(String nbjiejuh) {
        this.nbjiejuh = nbjiejuh;
    }

    public BigDecimal getXieyjine() {
        return xieyjine;
    }

    public void setXieyjine(BigDecimal xieyjine) {
        this.xieyjine = xieyjine;
    }

    public BigDecimal getXieybili() {
        return xieybili;
    }

    public void setXieybili(BigDecimal xieybili) {
        this.xieybili = xieybili;
    }

    @Override
    public String toString() {
        return "Lstytczfe{" +
                "ruzjigou='" + ruzjigou + '\'' +
                ", jigoumch='" + jigoumch + '\'' +
                ", lhdkleix='" + lhdkleix + '\'' +
                ", chzfkehh='" + chzfkehh + '\'' +
                ", chzfkhmc='" + chzfkhmc + '\'' +
                ", chzfhhao='" + chzfhhao + '\'' +
                ", czfhming='" + czfhming + '\'' +
                ", czfzhzxh='" + czfzhzxh + '\'' +
                ", chuzfzhh='" + chuzfzhh + '\'' +
                ", chzfzhmc='" + chzfzhmc + '\'' +
                ", chuzbili=" + chuzbili +
                ", chuzjine=" + chuzjine +
                ", shoukzhh='" + shoukzhh + '\'' +
                ", skzhhzxh='" + skzhhzxh + '\'' +
                ", skzhhmch='" + skzhhmch + '\'' +
                ", nyuelilv='" + nyuelilv + '\'' +
                ", hetongll=" + hetongll +
                ", yqllcklx='" + yqllcklx + '\'' +
                ", yuqillbh='" + yuqillbh + '\'' +
                ", yuqinyll='" + yuqinyll + '\'' +
                ", yuqililv=" + yuqililv +
                ", yuqitzfs='" + yuqitzfs + '\'' +
                ", yuqitzzq='" + yuqitzzq + '\'' +
                ", yqfxfdfs='" + yqfxfdfs + '\'' +
                ", yqfxfdzh=" + yqfxfdzh +
                ", flllcklx='" + flllcklx + '\'' +
                ", fulilvbh='" + fulilvbh + '\'' +
                ", fulilvny='" + fulilvny + '\'' +
                ", fulililv=" + fulililv +
                ", fulitzfs='" + fulitzfs + '\'' +
                ", fulitzzq='" + fulitzzq + '\'' +
                ", fulifdfs='" + fulifdfs + '\'' +
                ", fulifdzh=" + fulifdzh +
                ", dfkbjzhh='" + dfkbjzhh + '\'' +
                ", dfkbjzxh='" + dfkbjzxh + '\'' +
                ", dfklxzhh='" + dfklxzhh + '\'' +
                ", dfklxzxh='" + dfklxzxh + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", nbjiejuh='" + nbjiejuh + '\'' +
                ", xieyjine=" + xieyjine +
                ", xieybili=" + xieybili +
                '}';
    }
}
