package cn.com.yusys.yusp.dto.client.esb.core.ln3069.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 13:57
 * @since 2021/5/28 13:57
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstzczrxy implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xieybhao")
    private String xieybhao;//协议编号
    @JsonProperty(value = "xieyimch")
    private String xieyimch;//协议名称
    @JsonProperty(value = "chanpdma")
    private String chanpdma;//产品代码
    @JsonProperty(value = "chanpmch")
    private String chanpmch;//产品名称
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "xyzuigxe")
    private BigDecimal xyzuigxe;//协议最高限额
    @JsonProperty(value = "xieyshje")
    private BigDecimal xieyshje;//协议实际金额
    @JsonProperty(value = "xieyilil")
    private BigDecimal xieyilil;//协议利率
    @JsonProperty(value = "xieyilix")
    private BigDecimal xieyilix;//协议利息
    @JsonProperty(value = "qiandriq")
    private String qiandriq;//签订日期
    @JsonProperty(value = "fengbriq")
    private String fengbriq;//封包日期
    @JsonProperty(value = "jiebriqi")
    private String jiebriqi;//解包日期
    @JsonProperty(value = "ruchiriq")
    private String ruchiriq;//入池日期
    @JsonProperty(value = "huigriqi")
    private String huigriqi;//回购日期
    @JsonProperty(value = "zchzhtai")
    private String zchzhtai;//资产处理状态
    @JsonProperty(value = "jydsleix")
    private String jydsleix;//交易对手类型
    @JsonProperty(value = "jydshmch")
    private String jydshmch;//交易对手名称
    @JsonProperty(value = "jydszhmc")
    private String jydszhmc;//交易对手账户名称
    @JsonProperty(value = "zhkaihhh")
    private String zhkaihhh;//账户开户行行号
    @JsonProperty(value = "jydszhao")
    private String jydszhao;//交易对手账号
    @JsonProperty(value = "jydszhzh")
    private String jydszhzh;//交易对手账号子序号
    @JsonProperty(value = "zhkaihhm")
    private String zhkaihhm;//账户开户行行名
    @JsonProperty(value = "zcxyleix")
    private String zcxyleix;//资产协议类型
    @JsonProperty(value = "zcrfshii")
    private String zcrfshii;//资产融通方式
    @JsonProperty(value = "zcrtbili")
    private BigDecimal zcrtbili;//资产融通比例
    @JsonProperty(value = "zrjjfshi")
    private String zrjjfshi;//转让计价方式
    @JsonProperty(value = "zjlyzhao")
    private String zjlyzhao;//资金来源账号
    @JsonProperty(value = "zjlyzzxh")
    private String zjlyzzxh;//资金来源账号子序号
    @JsonProperty(value = "zrfkzoqi")
    private String zrfkzoqi;//付款周期
    @JsonProperty(value = "xcfkriqi")
    private String xcfkriqi;//下次付款日期
    @JsonProperty(value = "scifukrq")
    private String scifukrq;//上次付款日期
    @JsonProperty(value = "zrfukzhh")
    private String zrfukzhh;//对外付款账号
    @JsonProperty(value = "zrfukzxh")
    private String zrfukzxh;//对外付款账号子序号
    @JsonProperty(value = "zjguijbz")
    private String zjguijbz;//资金归集标志
    @JsonProperty(value = "fysfzhqh")
    private String fysfzhqh;//费用是否证券化
    @JsonProperty(value = "fjsfzhqh")
    private String fjsfzhqh;//罚金是否证券化
    @JsonProperty(value = "bzesfzrr")
    private String bzesfzrr;//资金来源账号不足额是否转让
    @JsonProperty(value = "chulizht")
    private String chulizht;//处理状态
    @JsonProperty(value = "zhaiyosm")
    private String zhaiyosm;//摘要说明

    public String getXieybhao() {
        return xieybhao;
    }

    public void setXieybhao(String xieybhao) {
        this.xieybhao = xieybhao;
    }

    public String getXieyimch() {
        return xieyimch;
    }

    public void setXieyimch(String xieyimch) {
        this.xieyimch = xieyimch;
    }

    public String getChanpdma() {
        return chanpdma;
    }

    public void setChanpdma(String chanpdma) {
        this.chanpdma = chanpdma;
    }

    public String getChanpmch() {
        return chanpmch;
    }

    public void setChanpmch(String chanpmch) {
        this.chanpmch = chanpmch;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getXyzuigxe() {
        return xyzuigxe;
    }

    public void setXyzuigxe(BigDecimal xyzuigxe) {
        this.xyzuigxe = xyzuigxe;
    }

    public BigDecimal getXieyshje() {
        return xieyshje;
    }

    public void setXieyshje(BigDecimal xieyshje) {
        this.xieyshje = xieyshje;
    }

    public BigDecimal getXieyilil() {
        return xieyilil;
    }

    public void setXieyilil(BigDecimal xieyilil) {
        this.xieyilil = xieyilil;
    }

    public BigDecimal getXieyilix() {
        return xieyilix;
    }

    public void setXieyilix(BigDecimal xieyilix) {
        this.xieyilix = xieyilix;
    }

    public String getQiandriq() {
        return qiandriq;
    }

    public void setQiandriq(String qiandriq) {
        this.qiandriq = qiandriq;
    }

    public String getFengbriq() {
        return fengbriq;
    }

    public void setFengbriq(String fengbriq) {
        this.fengbriq = fengbriq;
    }

    public String getJiebriqi() {
        return jiebriqi;
    }

    public void setJiebriqi(String jiebriqi) {
        this.jiebriqi = jiebriqi;
    }

    public String getRuchiriq() {
        return ruchiriq;
    }

    public void setRuchiriq(String ruchiriq) {
        this.ruchiriq = ruchiriq;
    }

    public String getHuigriqi() {
        return huigriqi;
    }

    public void setHuigriqi(String huigriqi) {
        this.huigriqi = huigriqi;
    }

    public String getZchzhtai() {
        return zchzhtai;
    }

    public void setZchzhtai(String zchzhtai) {
        this.zchzhtai = zchzhtai;
    }

    public String getJydsleix() {
        return jydsleix;
    }

    public void setJydsleix(String jydsleix) {
        this.jydsleix = jydsleix;
    }

    public String getJydshmch() {
        return jydshmch;
    }

    public void setJydshmch(String jydshmch) {
        this.jydshmch = jydshmch;
    }

    public String getJydszhmc() {
        return jydszhmc;
    }

    public void setJydszhmc(String jydszhmc) {
        this.jydszhmc = jydszhmc;
    }

    public String getZhkaihhh() {
        return zhkaihhh;
    }

    public void setZhkaihhh(String zhkaihhh) {
        this.zhkaihhh = zhkaihhh;
    }

    public String getJydszhao() {
        return jydszhao;
    }

    public void setJydszhao(String jydszhao) {
        this.jydszhao = jydszhao;
    }

    public String getJydszhzh() {
        return jydszhzh;
    }

    public void setJydszhzh(String jydszhzh) {
        this.jydszhzh = jydszhzh;
    }

    public String getZhkaihhm() {
        return zhkaihhm;
    }

    public void setZhkaihhm(String zhkaihhm) {
        this.zhkaihhm = zhkaihhm;
    }

    public String getZcxyleix() {
        return zcxyleix;
    }

    public void setZcxyleix(String zcxyleix) {
        this.zcxyleix = zcxyleix;
    }

    public String getZcrfshii() {
        return zcrfshii;
    }

    public void setZcrfshii(String zcrfshii) {
        this.zcrfshii = zcrfshii;
    }

    public BigDecimal getZcrtbili() {
        return zcrtbili;
    }

    public void setZcrtbili(BigDecimal zcrtbili) {
        this.zcrtbili = zcrtbili;
    }

    public String getZrjjfshi() {
        return zrjjfshi;
    }

    public void setZrjjfshi(String zrjjfshi) {
        this.zrjjfshi = zrjjfshi;
    }

    public String getZjlyzhao() {
        return zjlyzhao;
    }

    public void setZjlyzhao(String zjlyzhao) {
        this.zjlyzhao = zjlyzhao;
    }

    public String getZjlyzzxh() {
        return zjlyzzxh;
    }

    public void setZjlyzzxh(String zjlyzzxh) {
        this.zjlyzzxh = zjlyzzxh;
    }

    public String getZrfkzoqi() {
        return zrfkzoqi;
    }

    public void setZrfkzoqi(String zrfkzoqi) {
        this.zrfkzoqi = zrfkzoqi;
    }

    public String getXcfkriqi() {
        return xcfkriqi;
    }

    public void setXcfkriqi(String xcfkriqi) {
        this.xcfkriqi = xcfkriqi;
    }

    public String getScifukrq() {
        return scifukrq;
    }

    public void setScifukrq(String scifukrq) {
        this.scifukrq = scifukrq;
    }

    public String getZrfukzhh() {
        return zrfukzhh;
    }

    public void setZrfukzhh(String zrfukzhh) {
        this.zrfukzhh = zrfukzhh;
    }

    public String getZrfukzxh() {
        return zrfukzxh;
    }

    public void setZrfukzxh(String zrfukzxh) {
        this.zrfukzxh = zrfukzxh;
    }

    public String getZjguijbz() {
        return zjguijbz;
    }

    public void setZjguijbz(String zjguijbz) {
        this.zjguijbz = zjguijbz;
    }

    public String getFysfzhqh() {
        return fysfzhqh;
    }

    public void setFysfzhqh(String fysfzhqh) {
        this.fysfzhqh = fysfzhqh;
    }

    public String getFjsfzhqh() {
        return fjsfzhqh;
    }

    public void setFjsfzhqh(String fjsfzhqh) {
        this.fjsfzhqh = fjsfzhqh;
    }

    public String getBzesfzrr() {
        return bzesfzrr;
    }

    public void setBzesfzrr(String bzesfzrr) {
        this.bzesfzrr = bzesfzrr;
    }

    public String getChulizht() {
        return chulizht;
    }

    public void setChulizht(String chulizht) {
        this.chulizht = chulizht;
    }

    public String getZhaiyosm() {
        return zhaiyosm;
    }

    public void setZhaiyosm(String zhaiyosm) {
        this.zhaiyosm = zhaiyosm;
    }

    @Override
    public String toString() {
        return "Ln3069RespDto{" +
                "xieybhao='" + xieybhao + '\'' +
                "xieyimch='" + xieyimch + '\'' +
                "chanpdma='" + chanpdma + '\'' +
                "chanpmch='" + chanpmch + '\'' +
                "huobdhao='" + huobdhao + '\'' +
                "xyzuigxe='" + xyzuigxe + '\'' +
                "xieyshje='" + xieyshje + '\'' +
                "xieyilil='" + xieyilil + '\'' +
                "xieyilix='" + xieyilix + '\'' +
                "qiandriq='" + qiandriq + '\'' +
                "fengbriq='" + fengbriq + '\'' +
                "jiebriqi='" + jiebriqi + '\'' +
                "ruchiriq='" + ruchiriq + '\'' +
                "huigriqi='" + huigriqi + '\'' +
                "zchzhtai='" + zchzhtai + '\'' +
                "jydsleix='" + jydsleix + '\'' +
                "jydshmch='" + jydshmch + '\'' +
                "jydszhmc='" + jydszhmc + '\'' +
                "zhkaihhh='" + zhkaihhh + '\'' +
                "jydszhao='" + jydszhao + '\'' +
                "jydszhzh='" + jydszhzh + '\'' +
                "zhkaihhm='" + zhkaihhm + '\'' +
                "zcxyleix='" + zcxyleix + '\'' +
                "zcrfshii='" + zcrfshii + '\'' +
                "zcrtbili='" + zcrtbili + '\'' +
                "zrjjfshi='" + zrjjfshi + '\'' +
                "zjlyzhao='" + zjlyzhao + '\'' +
                "zjlyzzxh='" + zjlyzzxh + '\'' +
                "zrfkzoqi='" + zrfkzoqi + '\'' +
                "xcfkriqi='" + xcfkriqi + '\'' +
                "scifukrq='" + scifukrq + '\'' +
                "zrfukzhh='" + zrfukzhh + '\'' +
                "zrfukzxh='" + zrfukzxh + '\'' +
                "zjguijbz='" + zjguijbz + '\'' +
                "fysfzhqh='" + fysfzhqh + '\'' +
                "fjsfzhqh='" + fjsfzhqh + '\'' +
                "bzesfzrr='" + bzesfzrr + '\'' +
                "chulizht='" + chulizht + '\'' +
                "zhaiyosm='" + zhaiyosm + '\'' +
                '}';
    }
}
