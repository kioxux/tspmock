package cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：查询人员基本信息岗位信息家庭信息
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XxdrelRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list1")
    private List<List1> list1;
    @JsonProperty(value = "list2")
    private List<List2> list2;
    @JsonProperty(value = "listnm")
    private String listnm;//循环数量

    public List<List1> getList1() {
        return list1;
    }

    public void setList1(List<List1> list1) {
        this.list1 = list1;
    }

    public List<List2> getList2() {
        return list2;
    }

    public void setList2(List<List2> list2) {
        this.list2 = list2;
    }

    public String getListnm() {
        return listnm;
    }

    public void setListnm(String listnm) {
        this.listnm = listnm;
    }

    @Override
    public String toString() {
        return "XxdrelRespDto{" +
                "list1=" + list1 +
                ", list2=" + list2 +
                ", listnm='" + listnm + '\'' +
                '}';
    }
}
