package cn.com.yusys.yusp.dto.client.esb.core.ln3126.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/31 15:58
 * @since 2021/5/31 15:58
 */
public class Listdkfycx {
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "mingxixh")
    private Integer mingxixh;//明细序号
    @JsonProperty(value = "shoufshj")
    private String shoufshj;//收费事件
    @JsonProperty(value = "shfshjmc")
    private String shfshjmc;//收费事件名称
    @JsonProperty(value = "shoufzhl")
    private String shoufzhl;//收费种类
    @JsonProperty(value = "shoufdma")
    private String shoufdma;//收费代码
    @JsonProperty(value = "shfdmamc")
    private String shfdmamc;//收费代码名称
    @JsonProperty(value = "yingshfy")
    private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "shoushfy")
    private BigDecimal shoushfy;//实收费用
    @JsonProperty(value = "fufeizhh")
    private String fufeizhh;//付费账号
    @JsonProperty(value = "ffzhhzxh")
    private String ffzhhzxh;//付费账号子序号
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "jiaoyisj")
    private String jiaoyisj;//交易事件
    @JsonProperty(value = "shjshuom")
    private String shjshuom;//事件说明
    @JsonProperty(value = "jiaoyima")
    private String jiaoyima;//交易码
    @JsonProperty(value = "zhaiyoms")
    private String zhaiyoms;//摘要
    @JsonProperty(value = "farendma")
    private String farendma;//法人代码
    @JsonProperty(value = "weihguiy")
    private String weihguiy;//维护柜员
    @JsonProperty(value = "weihjigo")
    private String weihjigo;//维护机构
    @JsonProperty(value = "weihriqi")
    private String weihriqi;//维护日期
    @JsonProperty(value = "shijchuo")
    private Integer shijchuo;//时间戳
    @JsonProperty(value = "jiluztai")
    private String jiluztai;//记录状态

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public Integer getMingxixh() {
        return mingxixh;
    }

    public void setMingxixh(Integer mingxixh) {
        this.mingxixh = mingxixh;
    }

    public String getShoufshj() {
        return shoufshj;
    }

    public void setShoufshj(String shoufshj) {
        this.shoufshj = shoufshj;
    }

    public String getShfshjmc() {
        return shfshjmc;
    }

    public void setShfshjmc(String shfshjmc) {
        this.shfshjmc = shfshjmc;
    }

    public String getShoufzhl() {
        return shoufzhl;
    }

    public void setShoufzhl(String shoufzhl) {
        this.shoufzhl = shoufzhl;
    }

    public String getShoufdma() {
        return shoufdma;
    }

    public void setShoufdma(String shoufdma) {
        this.shoufdma = shoufdma;
    }

    public String getShfdmamc() {
        return shfdmamc;
    }

    public void setShfdmamc(String shfdmamc) {
        this.shfdmamc = shfdmamc;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public BigDecimal getShoushfy() {
        return shoushfy;
    }

    public void setShoushfy(BigDecimal shoushfy) {
        this.shoushfy = shoushfy;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getJiaoyisj() {
        return jiaoyisj;
    }

    public void setJiaoyisj(String jiaoyisj) {
        this.jiaoyisj = jiaoyisj;
    }

    public String getShjshuom() {
        return shjshuom;
    }

    public void setShjshuom(String shjshuom) {
        this.shjshuom = shjshuom;
    }

    public String getJiaoyima() {
        return jiaoyima;
    }

    public void setJiaoyima(String jiaoyima) {
        this.jiaoyima = jiaoyima;
    }

    public String getZhaiyoms() {
        return zhaiyoms;
    }

    public void setZhaiyoms(String zhaiyoms) {
        this.zhaiyoms = zhaiyoms;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public Integer getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Integer shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "Ln3126RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                "kehmingc='" + kehmingc + '\'' +
                "mingxixh='" + mingxixh + '\'' +
                "shoufshj='" + shoufshj + '\'' +
                "shfshjmc='" + shfshjmc + '\'' +
                "shoufzhl='" + shoufzhl + '\'' +
                "shoufdma='" + shoufdma + '\'' +
                "shfdmamc='" + shfdmamc + '\'' +
                "yingshfy='" + yingshfy + '\'' +
                "shoushfy='" + shoushfy + '\'' +
                "fufeizhh='" + fufeizhh + '\'' +
                "ffzhhzxh='" + ffzhhzxh + '\'' +
                "jiaoyirq='" + jiaoyirq + '\'' +
                "jiaoyijg='" + jiaoyijg + '\'' +
                "jiaoyigy='" + jiaoyigy + '\'' +
                "jiaoyils='" + jiaoyils + '\'' +
                "jiaoyisj='" + jiaoyisj + '\'' +
                "shjshuom='" + shjshuom + '\'' +
                "jiaoyima='" + jiaoyima + '\'' +
                "zhaiyoms='" + zhaiyoms + '\'' +
                "farendma='" + farendma + '\'' +
                "weihguiy='" + weihguiy + '\'' +
                "weihjigo='" + weihjigo + '\'' +
                "weihriqi='" + weihriqi + '\'' +
                "shijchuo='" + shijchuo + '\'' +
                "jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
