package cn.com.yusys.yusp.dto.client.esb.core.ln3042.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：手工指定归还某项利息金额，不按既定顺序归还
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3042ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "daikczbz")
    private String daikczbz;//业务操作标志
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehuzwmc")
    private String kehuzwmc;//客户名
    @JsonProperty(value = "zdhkjine")
    private BigDecimal zdhkjine;//指定还款金额
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "zhchbjin")
    private BigDecimal zhchbjin;//正常本金
    @JsonProperty(value = "yuqibjin")
    private BigDecimal yuqibjin;//逾期本金
    @JsonProperty(value = "dzhibjin")
    private BigDecimal dzhibjin;//呆滞本金
    @JsonProperty(value = "daizbjin")
    private BigDecimal daizbjin;//呆账本金
    @JsonProperty(value = "ysyjlixi")
    private BigDecimal ysyjlixi;//应收应计利息
    @JsonProperty(value = "csyjlixi")
    private BigDecimal csyjlixi;//催收应计利息
    @JsonProperty(value = "ysqianxi")
    private BigDecimal ysqianxi;//应收欠息
    @JsonProperty(value = "csqianxi")
    private BigDecimal csqianxi;//催收欠息
    @JsonProperty(value = "ysyjfaxi")
    private BigDecimal ysyjfaxi;//应收应计罚息
    @JsonProperty(value = "csyjfaxi")
    private BigDecimal csyjfaxi;//催收应计罚息
    @JsonProperty(value = "yshofaxi")
    private BigDecimal yshofaxi;//应收罚息
    @JsonProperty(value = "cshofaxi")
    private BigDecimal cshofaxi;//催收罚息
    @JsonProperty(value = "yingjifx")
    private BigDecimal yingjifx;//应计复息
    @JsonProperty(value = "fuxiiiii")
    private BigDecimal fuxiiiii;//复息
    @JsonProperty(value = "yingshfj")
    private BigDecimal yingshfj;//应收罚金
    @JsonProperty(value = "rzzdkouk")
    private String rzzdkouk;//是否日终自动扣款
    @JsonProperty(value = "zjinjizh")
    private String zjinjizh;//资金来源记账标志
    @JsonProperty(value = "sfqzjieq")
    private String sfqzjieq;//强制结清标志
    @JsonProperty(value = "hkjinelb")
    private String hkjinelb;//还款金额类别
    @JsonProperty(value = "yingshfy")
    private BigDecimal yingshfy;//应收费用
    @JsonProperty(value = "jzhdkbzh")
    private String jzhdkbzh;//减值贷款标志
    @JsonProperty(value = "daikghfs")
    private String daikghfs;//贷款归还方式
    @JsonProperty(value = "pingzhzl")
    private String pingzhzl;//凭证种类
    @JsonProperty(value = "pingzhma")
    private String pingzhma;//凭证批号
    @JsonProperty(value = "pngzxhao")
    private String pngzxhao;//凭证序号
    @JsonProperty(value = "zhfutojn")
    private String zhfutojn;//支付条件
    @JsonProperty(value = "jiaoymma")
    private String jiaoymma;//交易密码
    @JsonProperty(value = "mimazlei")
    private String mimazlei;//密码种类
    @JsonProperty(value = "zhjnzlei")
    private String zhjnzlei;//证件种类
    @JsonProperty(value = "zhjhaoma")
    private String zhjhaoma;//证件号码
    @JsonProperty(value = "kehuzhlx")
    private String kehuzhlx;//还款账号类型
    @JsonProperty(value = "yanmjine")
    private BigDecimal yanmjine;//验密金额
    @JsonProperty(value = "huandzms")
    private String huandzms;//还贷证明书
    @JsonProperty(value = "brchno")
    private String brchno;//    部门号,取账务机构号

    public String getDaikczbz() {
        return daikczbz;
    }

    public void setDaikczbz(String daikczbz) {
        this.daikczbz = daikczbz;
    }

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehuzwmc() {
        return kehuzwmc;
    }

    public void setKehuzwmc(String kehuzwmc) {
        this.kehuzwmc = kehuzwmc;
    }

    public BigDecimal getZdhkjine() {
        return zdhkjine;
    }

    public void setZdhkjine(BigDecimal zdhkjine) {
        this.zdhkjine = zdhkjine;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public BigDecimal getZhchbjin() {
        return zhchbjin;
    }

    public void setZhchbjin(BigDecimal zhchbjin) {
        this.zhchbjin = zhchbjin;
    }

    public BigDecimal getYuqibjin() {
        return yuqibjin;
    }

    public void setYuqibjin(BigDecimal yuqibjin) {
        this.yuqibjin = yuqibjin;
    }

    public BigDecimal getDzhibjin() {
        return dzhibjin;
    }

    public void setDzhibjin(BigDecimal dzhibjin) {
        this.dzhibjin = dzhibjin;
    }

    public BigDecimal getDaizbjin() {
        return daizbjin;
    }

    public void setDaizbjin(BigDecimal daizbjin) {
        this.daizbjin = daizbjin;
    }

    public BigDecimal getYsyjlixi() {
        return ysyjlixi;
    }

    public void setYsyjlixi(BigDecimal ysyjlixi) {
        this.ysyjlixi = ysyjlixi;
    }

    public BigDecimal getCsyjlixi() {
        return csyjlixi;
    }

    public void setCsyjlixi(BigDecimal csyjlixi) {
        this.csyjlixi = csyjlixi;
    }

    public BigDecimal getYsqianxi() {
        return ysqianxi;
    }

    public void setYsqianxi(BigDecimal ysqianxi) {
        this.ysqianxi = ysqianxi;
    }

    public BigDecimal getCsqianxi() {
        return csqianxi;
    }

    public void setCsqianxi(BigDecimal csqianxi) {
        this.csqianxi = csqianxi;
    }

    public BigDecimal getYsyjfaxi() {
        return ysyjfaxi;
    }

    public void setYsyjfaxi(BigDecimal ysyjfaxi) {
        this.ysyjfaxi = ysyjfaxi;
    }

    public BigDecimal getCsyjfaxi() {
        return csyjfaxi;
    }

    public void setCsyjfaxi(BigDecimal csyjfaxi) {
        this.csyjfaxi = csyjfaxi;
    }

    public BigDecimal getYshofaxi() {
        return yshofaxi;
    }

    public void setYshofaxi(BigDecimal yshofaxi) {
        this.yshofaxi = yshofaxi;
    }

    public BigDecimal getCshofaxi() {
        return cshofaxi;
    }

    public void setCshofaxi(BigDecimal cshofaxi) {
        this.cshofaxi = cshofaxi;
    }

    public BigDecimal getYingjifx() {
        return yingjifx;
    }

    public void setYingjifx(BigDecimal yingjifx) {
        this.yingjifx = yingjifx;
    }

    public BigDecimal getFuxiiiii() {
        return fuxiiiii;
    }

    public void setFuxiiiii(BigDecimal fuxiiiii) {
        this.fuxiiiii = fuxiiiii;
    }

    public BigDecimal getYingshfj() {
        return yingshfj;
    }

    public void setYingshfj(BigDecimal yingshfj) {
        this.yingshfj = yingshfj;
    }

    public String getRzzdkouk() {
        return rzzdkouk;
    }

    public void setRzzdkouk(String rzzdkouk) {
        this.rzzdkouk = rzzdkouk;
    }

    public String getZjinjizh() {
        return zjinjizh;
    }

    public void setZjinjizh(String zjinjizh) {
        this.zjinjizh = zjinjizh;
    }

    public String getSfqzjieq() {
        return sfqzjieq;
    }

    public void setSfqzjieq(String sfqzjieq) {
        this.sfqzjieq = sfqzjieq;
    }

    public String getHkjinelb() {
        return hkjinelb;
    }

    public void setHkjinelb(String hkjinelb) {
        this.hkjinelb = hkjinelb;
    }

    public BigDecimal getYingshfy() {
        return yingshfy;
    }

    public void setYingshfy(BigDecimal yingshfy) {
        this.yingshfy = yingshfy;
    }

    public String getJzhdkbzh() {
        return jzhdkbzh;
    }

    public void setJzhdkbzh(String jzhdkbzh) {
        this.jzhdkbzh = jzhdkbzh;
    }

    public String getDaikghfs() {
        return daikghfs;
    }

    public void setDaikghfs(String daikghfs) {
        this.daikghfs = daikghfs;
    }

    public String getPingzhzl() {
        return pingzhzl;
    }

    public void setPingzhzl(String pingzhzl) {
        this.pingzhzl = pingzhzl;
    }

    public String getPingzhma() {
        return pingzhma;
    }

    public void setPingzhma(String pingzhma) {
        this.pingzhma = pingzhma;
    }

    public String getPngzxhao() {
        return pngzxhao;
    }

    public void setPngzxhao(String pngzxhao) {
        this.pngzxhao = pngzxhao;
    }

    public String getZhfutojn() {
        return zhfutojn;
    }

    public void setZhfutojn(String zhfutojn) {
        this.zhfutojn = zhfutojn;
    }

    public String getJiaoymma() {
        return jiaoymma;
    }

    public void setJiaoymma(String jiaoymma) {
        this.jiaoymma = jiaoymma;
    }

    public String getMimazlei() {
        return mimazlei;
    }

    public void setMimazlei(String mimazlei) {
        this.mimazlei = mimazlei;
    }

    public String getZhjnzlei() {
        return zhjnzlei;
    }

    public void setZhjnzlei(String zhjnzlei) {
        this.zhjnzlei = zhjnzlei;
    }

    public String getZhjhaoma() {
        return zhjhaoma;
    }

    public void setZhjhaoma(String zhjhaoma) {
        this.zhjhaoma = zhjhaoma;
    }

    public String getKehuzhlx() {
        return kehuzhlx;
    }

    public void setKehuzhlx(String kehuzhlx) {
        this.kehuzhlx = kehuzhlx;
    }

    public BigDecimal getYanmjine() {
        return yanmjine;
    }

    public void setYanmjine(BigDecimal yanmjine) {
        this.yanmjine = yanmjine;
    }

    public String getHuandzms() {
        return huandzms;
    }

    public void setHuandzms(String huandzms) {
        this.huandzms = huandzms;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    @Override
    public String toString() {
        return "Ln3042ReqDto{" +
                "daikczbz='" + daikczbz + '\'' +
                ", dkjiejuh='" + dkjiejuh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehuzwmc='" + kehuzwmc + '\'' +
                ", zdhkjine=" + zdhkjine +
                ", zijnlaiy='" + zijnlaiy + '\'' +
                ", huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", zhchbjin=" + zhchbjin +
                ", yuqibjin=" + yuqibjin +
                ", dzhibjin=" + dzhibjin +
                ", daizbjin=" + daizbjin +
                ", ysyjlixi=" + ysyjlixi +
                ", csyjlixi=" + csyjlixi +
                ", ysqianxi=" + ysqianxi +
                ", csqianxi=" + csqianxi +
                ", ysyjfaxi=" + ysyjfaxi +
                ", csyjfaxi=" + csyjfaxi +
                ", yshofaxi=" + yshofaxi +
                ", cshofaxi=" + cshofaxi +
                ", yingjifx=" + yingjifx +
                ", fuxiiiii=" + fuxiiiii +
                ", yingshfj=" + yingshfj +
                ", rzzdkouk='" + rzzdkouk + '\'' +
                ", zjinjizh='" + zjinjizh + '\'' +
                ", sfqzjieq='" + sfqzjieq + '\'' +
                ", hkjinelb='" + hkjinelb + '\'' +
                ", yingshfy=" + yingshfy +
                ", jzhdkbzh='" + jzhdkbzh + '\'' +
                ", daikghfs='" + daikghfs + '\'' +
                ", pingzhzl='" + pingzhzl + '\'' +
                ", pingzhma='" + pingzhma + '\'' +
                ", pngzxhao='" + pngzxhao + '\'' +
                ", zhfutojn='" + zhfutojn + '\'' +
                ", jiaoymma='" + jiaoymma + '\'' +
                ", mimazlei='" + mimazlei + '\'' +
                ", zhjnzlei='" + zhjnzlei + '\'' +
                ", zhjhaoma='" + zhjhaoma + '\'' +
                ", kehuzhlx='" + kehuzhlx + '\'' +
                ", yanmjine=" + yanmjine +
                ", huandzms='" + huandzms + '\'' +
                ", brchno='" + brchno + '\'' +
                '}';
    }
}  
