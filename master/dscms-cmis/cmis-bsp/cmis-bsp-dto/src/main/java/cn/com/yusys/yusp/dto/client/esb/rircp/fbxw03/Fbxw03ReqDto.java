package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw03;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：增享贷风控测算接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午7:42:26
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw03ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd; // 处理码
    @JsonProperty(value = "servtp")
    private String servtp; // 渠道
    @JsonProperty(value = "servsq")
    private String servsq; // 渠道流水
    @JsonProperty(value = "userid")
    private String userid; // 柜员号
    @JsonProperty(value = "brchno")
    private String brchno; // 部门号
    @JsonProperty(value = "channel_type")
    private String channel_type; // 渠道来源
    @JsonProperty(value = "co_platform")
    private String co_platform; // 合作平台
    @JsonProperty(value = "prd_type")
    private String prd_type; // 产品类别
    @JsonProperty(value = "prd_code")
    private String prd_code; // 产品代码（零售智能风控内部代码）
    @JsonProperty(value = "apply_no")
    private String apply_no; // 业务唯一编号
    @JsonProperty(value = "op_flag")
    private String op_flag; // 操作类型
    @JsonProperty(value = "cust_id")
    private String cust_id; // 客户号
    @JsonProperty(value = "telphone")
    private String telphone; // 手机号码
    @JsonProperty(value = "bill_no")
    private String bill_no; // 借据号
    @JsonProperty(value = "manager_id")
    private String manager_id; // 客户经理号
    @JsonProperty(value = "manager_br_id")
    private String manager_br_id; // 管户机构
    @JsonProperty(value = "cust_name")
    private String cust_name; // 借款人姓名
    @JsonProperty(value = "cert_code")
    private String cert_code; // 借款人身份证号码
    @JsonProperty(value = "spouse_name")
    private String spouse_name; // 配偶姓名（暂时改为了居住地址）
    @JsonProperty(value = "spouse_cert_code")
    private String spouse_cert_code; // 配偶身份证号码
    @JsonProperty(value = "amount")
    private String amount; // 申请金额
    @JsonProperty(value = "manager_name")
    private String manager_name; // 客户经理姓名
    @JsonProperty(value = "amt")
    private String amt; // 客户经理建议金额
    @JsonProperty(value = "rate")
    private String rate; // 客户经理建议利率
    @JsonProperty(value = "repay_type")
    private String repay_type; // 还款方式
    @JsonProperty(value = "loan_purpose")
    private String loan_purpose; // 贷款用途
    @JsonProperty(value = "crd_term")
    private String crd_term; // 贷款期限
    @JsonProperty(value = "agri_flg")
    private String agri_flg; // 是否农户
    @JsonProperty(value = "cert_type")
    private String cert_type; // 证件类型
    @JsonProperty(value = "loan_direction")
    private String loan_direction; // 贷款投向
    @JsonProperty(value = "qq")
    private String qq; // qq
    @JsonProperty(value = "email")
    private String email; // 电子邮箱
    @JsonProperty(value = "wechat")
    private String wechat; // 微信

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getChannel_type() {
        return channel_type;
    }

    public void setChannel_type(String channel_type) {
        this.channel_type = channel_type;
    }

    public String getCo_platform() {
        return co_platform;
    }

    public void setCo_platform(String co_platform) {
        this.co_platform = co_platform;
    }

    public String getPrd_type() {
        return prd_type;
    }

    public void setPrd_type(String prd_type) {
        this.prd_type = prd_type;
    }

    public String getPrd_code() {
        return prd_code;
    }

    public void setPrd_code(String prd_code) {
        this.prd_code = prd_code;
    }

    public String getApply_no() {
        return apply_no;
    }

    public void setApply_no(String apply_no) {
        this.apply_no = apply_no;
    }

    public String getOp_flag() {
        return op_flag;
    }

    public void setOp_flag(String op_flag) {
        this.op_flag = op_flag;
    }

    public String getCust_id() {
        return cust_id;
    }

    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_br_id() {
        return manager_br_id;
    }

    public void setManager_br_id(String manager_br_id) {
        this.manager_br_id = manager_br_id;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public String getSpouse_cert_code() {
        return spouse_cert_code;
    }

    public void setSpouse_cert_code(String spouse_cert_code) {
        this.spouse_cert_code = spouse_cert_code;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRepay_type() {
        return repay_type;
    }

    public void setRepay_type(String repay_type) {
        this.repay_type = repay_type;
    }

    public String getLoan_purpose() {
        return loan_purpose;
    }

    public void setLoan_purpose(String loan_purpose) {
        this.loan_purpose = loan_purpose;
    }

    public String getCrd_term() {
        return crd_term;
    }

    public void setCrd_term(String crd_term) {
        this.crd_term = crd_term;
    }

    public String getAgri_flg() {
        return agri_flg;
    }

    public void setAgri_flg(String agri_flg) {
        this.agri_flg = agri_flg;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getLoan_direction() {
        return loan_direction;
    }

    public void setLoan_direction(String loan_direction) {
        this.loan_direction = loan_direction;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }
}