package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw06;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：利率定价测算提交接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午8:14:55
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw06ReqDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "prcscd")
    private String prcscd; // 处理码
    @JsonProperty(value = "servtp")
    private String servtp; // 渠道
    @JsonProperty(value = "servsq")
    private String servsq; // 渠道流水
    @JsonProperty(value = "userid")
    private String userid; // 柜员号
    @JsonProperty(value = "brchno")
    private String brchno; // 部门号
    @JsonProperty(value = "prd_name")
    private String prd_name; // 产品名称
    @JsonProperty(value = "cert_type")
    private String cert_type; // 证件类型
    @JsonProperty(value = "cert_code")
    private String cert_code; // 证件号码
    @JsonProperty(value = "cust_name")
    private String cust_name; // 客户姓名
    @JsonProperty(value = "phone")
    private String phone; // 移动电话
    @JsonProperty(value = "cust_id_core")
    private String cust_id_core; // 核心客户号
    @JsonProperty(value = "loan_investment")
    private String loan_investment; // 贷款投向
    @JsonProperty(value = "guarantee_method")
    private String guarantee_method; // 主担保方式
    @JsonProperty(value = "cus_type")
    private String cus_type; // 客户类型
    @JsonProperty(value = "collateral_pledge_type")
    private String collateral_pledge_type; // 抵押/质押类型
    @JsonProperty(value = "survey_serno")
    private String survey_serno; // 调查表编号

    public String getPrcscd() {
        return prcscd;
    }

    public void setPrcscd(String prcscd) {
        this.prcscd = prcscd;
    }

    public String getServtp() {
        return servtp;
    }

    public void setServtp(String servtp) {
        this.servtp = servtp;
    }

    public String getServsq() {
        return servsq;
    }

    public void setServsq(String servsq) {
        this.servsq = servsq;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getBrchno() {
        return brchno;
    }

    public void setBrchno(String brchno) {
        this.brchno = brchno;
    }

    public String getPrd_name() {
        return prd_name;
    }

    public void setPrd_name(String prd_name) {
        this.prd_name = prd_name;
    }

    public String getCert_type() {
        return cert_type;
    }

    public void setCert_type(String cert_type) {
        this.cert_type = cert_type;
    }

    public String getCert_code() {
        return cert_code;
    }

    public void setCert_code(String cert_code) {
        this.cert_code = cert_code;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCust_id_core() {
        return cust_id_core;
    }

    public void setCust_id_core(String cust_id_core) {
        this.cust_id_core = cust_id_core;
    }

    public String getLoan_investment() {
        return loan_investment;
    }

    public void setLoan_investment(String loan_investment) {
        this.loan_investment = loan_investment;
    }

    public String getGuarantee_method() {
        return guarantee_method;
    }

    public void setGuarantee_method(String guarantee_method) {
        this.guarantee_method = guarantee_method;
    }

    public String getCus_type() {
        return cus_type;
    }

    public void setCus_type(String cus_type) {
        this.cus_type = cus_type;
    }

    public String getCollateral_pledge_type() {
        return collateral_pledge_type;
    }

    public void setCollateral_pledge_type(String collateral_pledge_type) {
        this.collateral_pledge_type = collateral_pledge_type;
    }

    public String getSurvey_serno() {
        return survey_serno;
    }

    public void setSurvey_serno(String survey_serno) {
        this.survey_serno = survey_serno;
    }
}