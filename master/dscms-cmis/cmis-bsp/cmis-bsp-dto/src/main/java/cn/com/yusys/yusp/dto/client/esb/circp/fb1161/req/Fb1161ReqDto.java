package cn.com.yusys.yusp.dto.client.esb.circp.fb1161.req;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：借据信息同步
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1161ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CHANNEL_TYPE")
    private String CHANNEL_TYPE;//渠道来源
    @JsonProperty(value = "CO_PLATFORM")
    private String CO_PLATFORM;//合作平台
    @JsonProperty(value = "LOAN_PROP")
    private String LOAN_PROP;//贷款属性
    @JsonProperty(value = "PRD_TYPE")
    private String PRD_TYPE;//产品类别
    @JsonProperty(value = "PRD_CODE")
    private String PRD_CODE;//产品代码
    @JsonProperty(value = "CUST_ID_CORE")
    private String CUST_ID_CORE;//核心客户号
    @JsonProperty(value = "CERT_TYPE")
    private String CERT_TYPE;//证件类型
    @JsonProperty(value = "CERT_CODE")
    private String CERT_CODE;//证件号码
    @JsonProperty(value = "CUST_NAME")
    private String CUST_NAME;//客户姓名
    @JsonProperty(value = "CRD_CONT_NO")
    private String CRD_CONT_NO;//授信合同编号
    @JsonProperty(value = "CN_CONT_NO")
    private String CN_CONT_NO;//合同编号（中文）
    @JsonProperty(value = "BILL_NO")
    private String BILL_NO;//借据编号
    @JsonProperty(value = "LOAN_PURPOSE")
    private String LOAN_PURPOSE;//贷款用途
    @JsonProperty(value = "CURRENCY")
    private String CURRENCY;//币种
    @JsonProperty(value = "REPAY_ACC_BANK")
    private String REPAY_ACC_BANK;//还款账户开户行行号
    @JsonProperty(value = "REPAY_ACC")
    private String REPAY_ACC;//还款账号
    @JsonProperty(value = "REPAY_CARD_NO")
    private String REPAY_CARD_NO;//还款卡号
    @JsonProperty(value = "LOAN_TERM")
    private String LOAN_TERM;//贷款期限
    @JsonProperty(value = "LOAN_START_DATE")
    private String LOAN_START_DATE;//贷款起始日期
    @JsonProperty(value = "LOAN_END_DATE")
    private String LOAN_END_DATE;//贷款到期日期
    @JsonProperty(value = "LOAN_AMT")
    private BigDecimal LOAN_AMT;//贷款金额
    @JsonProperty(value = "SIGN_TIME")
    private String SIGN_TIME;//签订时间
    @JsonProperty(value = "REPAY_TYPE")
    private String REPAY_TYPE;//还款方式
    @JsonProperty(value = "REPAY_DATE_TYPE")
    private String REPAY_DATE_TYPE;//还款日类型
    @JsonProperty(value = "RATE_ADJ_TYPE")
    private String RATE_ADJ_TYPE;//利率调整方式
    @JsonProperty(value = "INTEREST_CYCLE")
    private String INTEREST_CYCLE;//计息周期
    @JsonProperty(value = "REPAY_DAY")
    private String REPAY_DAY;//扣款日期
    @JsonProperty(value = "P_RATE_TYPE")
    private String P_RATE_TYPE;//罚息类型
    @JsonProperty(value = "P_RATE_FLOAT")
    private BigDecimal P_RATE_FLOAT;//罚息浮动利率
    @JsonProperty(value = "EXEC_RATE")
    private BigDecimal EXEC_RATE;//执行年利率（%）
    @JsonProperty(value = "FIXED_RATE_FLAG")
    private String FIXED_RATE_FLAG;//是否固定利率
    @JsonProperty(value = "CONT_STATUS")
    private String CONT_STATUS;//合同状态
    @JsonProperty(value = "PRE_APP_NO")
    private String PRE_APP_NO;//预授信申请流水号
    @JsonProperty(value = "MANAGER_ID")
    private String MANAGER_ID;//管户客户经理编号
    @JsonProperty(value = "MANAGER_NAME")
    private String MANAGER_NAME;//管户客户经理姓名
    @JsonProperty(value = "MANAGER_ORG_ID")
    private String MANAGER_ORG_ID;//管户客户经理所在机构ID
    @JsonProperty(value = "MANAGER_ORG_NAME")
    private String MANAGER_ORG_NAME;//管户客户经理所在机构名称

    @JsonIgnore
    public String getCHANNEL_TYPE() {
        return CHANNEL_TYPE;
    }

    @JsonIgnore
    public void setCHANNEL_TYPE(String CHANNEL_TYPE) {
        this.CHANNEL_TYPE = CHANNEL_TYPE;
    }

    @JsonIgnore
    public String getCO_PLATFORM() {
        return CO_PLATFORM;
    }

    @JsonIgnore
    public void setCO_PLATFORM(String CO_PLATFORM) {
        this.CO_PLATFORM = CO_PLATFORM;
    }

    @JsonIgnore
    public String getLOAN_PROP() {
        return LOAN_PROP;
    }

    @JsonIgnore
    public void setLOAN_PROP(String LOAN_PROP) {
        this.LOAN_PROP = LOAN_PROP;
    }

    @JsonIgnore
    public String getPRD_TYPE() {
        return PRD_TYPE;
    }

    @JsonIgnore
    public void setPRD_TYPE(String PRD_TYPE) {
        this.PRD_TYPE = PRD_TYPE;
    }

    @JsonIgnore
    public String getPRD_CODE() {
        return PRD_CODE;
    }

    @JsonIgnore
    public void setPRD_CODE(String PRD_CODE) {
        this.PRD_CODE = PRD_CODE;
    }

    @JsonIgnore
    public String getCUST_ID_CORE() {
        return CUST_ID_CORE;
    }

    @JsonIgnore
    public void setCUST_ID_CORE(String CUST_ID_CORE) {
        this.CUST_ID_CORE = CUST_ID_CORE;
    }

    @JsonIgnore
    public String getCERT_TYPE() {
        return CERT_TYPE;
    }

    @JsonIgnore
    public void setCERT_TYPE(String CERT_TYPE) {
        this.CERT_TYPE = CERT_TYPE;
    }

    @JsonIgnore
    public String getCERT_CODE() {
        return CERT_CODE;
    }

    @JsonIgnore
    public void setCERT_CODE(String CERT_CODE) {
        this.CERT_CODE = CERT_CODE;
    }

    @JsonIgnore
    public String getCUST_NAME() {
        return CUST_NAME;
    }

    @JsonIgnore
    public void setCUST_NAME(String CUST_NAME) {
        this.CUST_NAME = CUST_NAME;
    }

    @JsonIgnore
    public String getCRD_CONT_NO() {
        return CRD_CONT_NO;
    }

    @JsonIgnore
    public void setCRD_CONT_NO(String CRD_CONT_NO) {
        this.CRD_CONT_NO = CRD_CONT_NO;
    }

    @JsonIgnore
    public String getCN_CONT_NO() {
        return CN_CONT_NO;
    }

    @JsonIgnore
    public void setCN_CONT_NO(String CN_CONT_NO) {
        this.CN_CONT_NO = CN_CONT_NO;
    }

    @JsonIgnore
    public String getBILL_NO() {
        return BILL_NO;
    }

    @JsonIgnore
    public void setBILL_NO(String BILL_NO) {
        this.BILL_NO = BILL_NO;
    }

    @JsonIgnore
    public String getLOAN_PURPOSE() {
        return LOAN_PURPOSE;
    }

    @JsonIgnore
    public void setLOAN_PURPOSE(String LOAN_PURPOSE) {
        this.LOAN_PURPOSE = LOAN_PURPOSE;
    }

    @JsonIgnore
    public String getCURRENCY() {
        return CURRENCY;
    }

    @JsonIgnore
    public void setCURRENCY(String CURRENCY) {
        this.CURRENCY = CURRENCY;
    }

    @JsonIgnore
    public String getREPAY_ACC_BANK() {
        return REPAY_ACC_BANK;
    }

    @JsonIgnore
    public void setREPAY_ACC_BANK(String REPAY_ACC_BANK) {
        this.REPAY_ACC_BANK = REPAY_ACC_BANK;
    }

    @JsonIgnore
    public String getREPAY_ACC() {
        return REPAY_ACC;
    }

    @JsonIgnore
    public void setREPAY_ACC(String REPAY_ACC) {
        this.REPAY_ACC = REPAY_ACC;
    }

    @JsonIgnore
    public String getREPAY_CARD_NO() {
        return REPAY_CARD_NO;
    }

    @JsonIgnore
    public void setREPAY_CARD_NO(String REPAY_CARD_NO) {
        this.REPAY_CARD_NO = REPAY_CARD_NO;
    }

    @JsonIgnore
    public String getLOAN_TERM() {
        return LOAN_TERM;
    }

    @JsonIgnore
    public void setLOAN_TERM(String LOAN_TERM) {
        this.LOAN_TERM = LOAN_TERM;
    }

    @JsonIgnore
    public String getLOAN_START_DATE() {
        return LOAN_START_DATE;
    }

    @JsonIgnore
    public void setLOAN_START_DATE(String LOAN_START_DATE) {
        this.LOAN_START_DATE = LOAN_START_DATE;
    }

    @JsonIgnore
    public String getLOAN_END_DATE() {
        return LOAN_END_DATE;
    }

    @JsonIgnore
    public void setLOAN_END_DATE(String LOAN_END_DATE) {
        this.LOAN_END_DATE = LOAN_END_DATE;
    }

    @JsonIgnore
    public BigDecimal getLOAN_AMT() {
        return LOAN_AMT;
    }

    @JsonIgnore
    public void setLOAN_AMT(BigDecimal LOAN_AMT) {
        this.LOAN_AMT = LOAN_AMT;
    }

    @JsonIgnore
    public String getSIGN_TIME() {
        return SIGN_TIME;
    }

    @JsonIgnore
    public void setSIGN_TIME(String SIGN_TIME) {
        this.SIGN_TIME = SIGN_TIME;
    }

    @JsonIgnore
    public String getREPAY_TYPE() {
        return REPAY_TYPE;
    }

    @JsonIgnore
    public void setREPAY_TYPE(String REPAY_TYPE) {
        this.REPAY_TYPE = REPAY_TYPE;
    }

    @JsonIgnore
    public String getREPAY_DATE_TYPE() {
        return REPAY_DATE_TYPE;
    }

    @JsonIgnore
    public void setREPAY_DATE_TYPE(String REPAY_DATE_TYPE) {
        this.REPAY_DATE_TYPE = REPAY_DATE_TYPE;
    }

    @JsonIgnore
    public String getRATE_ADJ_TYPE() {
        return RATE_ADJ_TYPE;
    }

    @JsonIgnore
    public void setRATE_ADJ_TYPE(String RATE_ADJ_TYPE) {
        this.RATE_ADJ_TYPE = RATE_ADJ_TYPE;
    }

    @JsonIgnore
    public String getINTEREST_CYCLE() {
        return INTEREST_CYCLE;
    }

    @JsonIgnore
    public void setINTEREST_CYCLE(String INTEREST_CYCLE) {
        this.INTEREST_CYCLE = INTEREST_CYCLE;
    }

    @JsonIgnore
    public String getREPAY_DAY() {
        return REPAY_DAY;
    }

    @JsonIgnore
    public void setREPAY_DAY(String REPAY_DAY) {
        this.REPAY_DAY = REPAY_DAY;
    }

    @JsonIgnore
    public String getP_RATE_TYPE() {
        return P_RATE_TYPE;
    }

    @JsonIgnore
    public void setP_RATE_TYPE(String p_RATE_TYPE) {
        P_RATE_TYPE = p_RATE_TYPE;
    }

    @JsonIgnore
    public BigDecimal getP_RATE_FLOAT() {
        return P_RATE_FLOAT;
    }

    @JsonIgnore
    public void setP_RATE_FLOAT(BigDecimal p_RATE_FLOAT) {
        P_RATE_FLOAT = p_RATE_FLOAT;
    }

    @JsonIgnore
    public BigDecimal getEXEC_RATE() {
        return EXEC_RATE;
    }

    @JsonIgnore
    public void setEXEC_RATE(BigDecimal EXEC_RATE) {
        this.EXEC_RATE = EXEC_RATE;
    }

    @JsonIgnore
    public String getFIXED_RATE_FLAG() {
        return FIXED_RATE_FLAG;
    }

    @JsonIgnore
    public void setFIXED_RATE_FLAG(String FIXED_RATE_FLAG) {
        this.FIXED_RATE_FLAG = FIXED_RATE_FLAG;
    }

    @JsonIgnore
    public String getCONT_STATUS() {
        return CONT_STATUS;
    }

    @JsonIgnore
    public void setCONT_STATUS(String CONT_STATUS) {
        this.CONT_STATUS = CONT_STATUS;
    }

    @JsonIgnore
    public String getPRE_APP_NO() {
        return PRE_APP_NO;
    }

    @JsonIgnore
    public void setPRE_APP_NO(String PRE_APP_NO) {
        this.PRE_APP_NO = PRE_APP_NO;
    }

    @JsonIgnore
    public String getMANAGER_ID() {
        return MANAGER_ID;
    }

    @JsonIgnore
    public void setMANAGER_ID(String MANAGER_ID) {
        this.MANAGER_ID = MANAGER_ID;
    }

    @JsonIgnore
    public String getMANAGER_NAME() {
        return MANAGER_NAME;
    }

    @JsonIgnore
    public void setMANAGER_NAME(String MANAGER_NAME) {
        this.MANAGER_NAME = MANAGER_NAME;
    }

    @JsonIgnore
    public String getMANAGER_ORG_ID() {
        return MANAGER_ORG_ID;
    }

    @JsonIgnore
    public void setMANAGER_ORG_ID(String MANAGER_ORG_ID) {
        this.MANAGER_ORG_ID = MANAGER_ORG_ID;
    }

    @JsonIgnore
    public String getMANAGER_ORG_NAME() {
        return MANAGER_ORG_NAME;
    }

    @JsonIgnore
    public void setMANAGER_ORG_NAME(String MANAGER_ORG_NAME) {
        this.MANAGER_ORG_NAME = MANAGER_ORG_NAME;
    }

    @Override
    public String toString() {
        return "Fb1161ReqDto{" +
                "CHANNEL_TYPE='" + CHANNEL_TYPE + '\'' +
                ", CO_PLATFORM='" + CO_PLATFORM + '\'' +
                ", LOAN_PROP='" + LOAN_PROP + '\'' +
                ", PRD_TYPE='" + PRD_TYPE + '\'' +
                ", PRD_CODE='" + PRD_CODE + '\'' +
                ", CUST_ID_CORE='" + CUST_ID_CORE + '\'' +
                ", CERT_TYPE='" + CERT_TYPE + '\'' +
                ", CERT_CODE='" + CERT_CODE + '\'' +
                ", CUST_NAME='" + CUST_NAME + '\'' +
                ", CRD_CONT_NO='" + CRD_CONT_NO + '\'' +
                ", CN_CONT_NO='" + CN_CONT_NO + '\'' +
                ", BILL_NO='" + BILL_NO + '\'' +
                ", LOAN_PURPOSE='" + LOAN_PURPOSE + '\'' +
                ", CURRENCY='" + CURRENCY + '\'' +
                ", REPAY_ACC_BANK='" + REPAY_ACC_BANK + '\'' +
                ", REPAY_ACC='" + REPAY_ACC + '\'' +
                ", REPAY_CARD_NO='" + REPAY_CARD_NO + '\'' +
                ", LOAN_TERM='" + LOAN_TERM + '\'' +
                ", LOAN_START_DATE='" + LOAN_START_DATE + '\'' +
                ", LOAN_END_DATE='" + LOAN_END_DATE + '\'' +
                ", LOAN_AMT=" + LOAN_AMT +
                ", SIGN_TIME='" + SIGN_TIME + '\'' +
                ", REPAY_TYPE='" + REPAY_TYPE + '\'' +
                ", REPAY_DATE_TYPE='" + REPAY_DATE_TYPE + '\'' +
                ", RATE_ADJ_TYPE='" + RATE_ADJ_TYPE + '\'' +
                ", INTEREST_CYCLE='" + INTEREST_CYCLE + '\'' +
                ", REPAY_DAY='" + REPAY_DAY + '\'' +
                ", P_RATE_TYPE='" + P_RATE_TYPE + '\'' +
                ", P_RATE_FLOAT=" + P_RATE_FLOAT +
                ", EXEC_RATE=" + EXEC_RATE +
                ", FIXED_RATE_FLAG='" + FIXED_RATE_FLAG + '\'' +
                ", CONT_STATUS='" + CONT_STATUS + '\'' +
                ", PRE_APP_NO='" + PRE_APP_NO + '\'' +
                ", MANAGER_ID='" + MANAGER_ID + '\'' +
                ", MANAGER_NAME='" + MANAGER_NAME + '\'' +
                ", MANAGER_ORG_ID='" + MANAGER_ORG_ID + '\'' +
                ", MANAGER_ORG_NAME='" + MANAGER_ORG_NAME + '\'' +
                '}';
    }
}
