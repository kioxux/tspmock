package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 上市公司高管信息
 */
@JsonPropertyOrder(alphabetic = true)
public class LISTEDMANAGER implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ACTDUTYNAME")
    private String ACTDUTYNAME;//	职位
    @JsonProperty(value = "AGE")
    private String AGE;//	年龄
    @JsonProperty(value = "BEGINDATE")
    private String BEGINDATE;//	在职起始日期
    @JsonProperty(value = "CNAME")
    private String CNAME;//	姓名
    @JsonProperty(value = "DIMREASON")
    private String DIMREASON;//	离职原因
    @JsonProperty(value = "EDUCATIONLEVEL")
    private String EDUCATIONLEVEL;//	最高学历
    @JsonProperty(value = "ENDDATE")
    private String ENDDATE;//	离职日期/当前届次截止日
    @JsonProperty(value = "GENDER")
    private String GENDER;//	性别
    @JsonProperty(value = "HOLDAFAMT")
    private String HOLDAFAMT;//	最新持股数量
    @JsonProperty(value = "HOLDBEGINDATE")
    private String HOLDBEGINDATE;//	变动起始日期
    @JsonProperty(value = "HOLDENDDATE")
    private String HOLDENDDATE;//	变动截止日期
    @JsonProperty(value = "HOLDNAME")
    private String HOLDNAME;//	持股人
    @JsonProperty(value = "NOWSTATUS")
    private String NOWSTATUS;//	当前状态
    @JsonProperty(value = "REALTETYPE")
    private String REALTETYPE;//	与高管关系
    @JsonProperty(value = "REMARK")
    private String REMARK;//	简历

    @JsonIgnore
    public String getACTDUTYNAME() {
        return ACTDUTYNAME;
    }

    @JsonIgnore
    public void setACTDUTYNAME(String ACTDUTYNAME) {
        this.ACTDUTYNAME = ACTDUTYNAME;
    }

    @JsonIgnore
    public String getAGE() {
        return AGE;
    }

    @JsonIgnore
    public void setAGE(String AGE) {
        this.AGE = AGE;
    }

    @JsonIgnore
    public String getBEGINDATE() {
        return BEGINDATE;
    }

    @JsonIgnore
    public void setBEGINDATE(String BEGINDATE) {
        this.BEGINDATE = BEGINDATE;
    }

    @JsonIgnore
    public String getCNAME() {
        return CNAME;
    }

    @JsonIgnore
    public void setCNAME(String CNAME) {
        this.CNAME = CNAME;
    }

    @JsonIgnore
    public String getDIMREASON() {
        return DIMREASON;
    }

    @JsonIgnore
    public void setDIMREASON(String DIMREASON) {
        this.DIMREASON = DIMREASON;
    }

    @JsonIgnore
    public String getEDUCATIONLEVEL() {
        return EDUCATIONLEVEL;
    }

    @JsonIgnore
    public void setEDUCATIONLEVEL(String EDUCATIONLEVEL) {
        this.EDUCATIONLEVEL = EDUCATIONLEVEL;
    }

    @JsonIgnore
    public String getENDDATE() {
        return ENDDATE;
    }

    @JsonIgnore
    public void setENDDATE(String ENDDATE) {
        this.ENDDATE = ENDDATE;
    }

    @JsonIgnore
    public String getGENDER() {
        return GENDER;
    }

    @JsonIgnore
    public void setGENDER(String GENDER) {
        this.GENDER = GENDER;
    }

    @JsonIgnore
    public String getHOLDAFAMT() {
        return HOLDAFAMT;
    }

    @JsonIgnore
    public void setHOLDAFAMT(String HOLDAFAMT) {
        this.HOLDAFAMT = HOLDAFAMT;
    }

    @JsonIgnore
    public String getHOLDBEGINDATE() {
        return HOLDBEGINDATE;
    }

    @JsonIgnore
    public void setHOLDBEGINDATE(String HOLDBEGINDATE) {
        this.HOLDBEGINDATE = HOLDBEGINDATE;
    }

    @JsonIgnore
    public String getHOLDENDDATE() {
        return HOLDENDDATE;
    }

    @JsonIgnore
    public void setHOLDENDDATE(String HOLDENDDATE) {
        this.HOLDENDDATE = HOLDENDDATE;
    }

    @JsonIgnore
    public String getHOLDNAME() {
        return HOLDNAME;
    }

    @JsonIgnore
    public void setHOLDNAME(String HOLDNAME) {
        this.HOLDNAME = HOLDNAME;
    }

    @JsonIgnore
    public String getNOWSTATUS() {
        return NOWSTATUS;
    }

    @JsonIgnore
    public void setNOWSTATUS(String NOWSTATUS) {
        this.NOWSTATUS = NOWSTATUS;
    }

    @JsonIgnore
    public String getREALTETYPE() {
        return REALTETYPE;
    }

    @JsonIgnore
    public void setREALTETYPE(String REALTETYPE) {
        this.REALTETYPE = REALTETYPE;
    }

    @JsonIgnore
    public String getREMARK() {
        return REMARK;
    }

    @JsonIgnore
    public void setREMARK(String REMARK) {
        this.REMARK = REMARK;
    }

    @Override
    public String toString() {
        return "LISTEDMANAGER{" +
                "ACTDUTYNAME='" + ACTDUTYNAME + '\'' +
                ", AGE='" + AGE + '\'' +
                ", BEGINDATE='" + BEGINDATE + '\'' +
                ", CNAME='" + CNAME + '\'' +
                ", DIMREASON='" + DIMREASON + '\'' +
                ", EDUCATIONLEVEL='" + EDUCATIONLEVEL + '\'' +
                ", ENDDATE='" + ENDDATE + '\'' +
                ", GENDER='" + GENDER + '\'' +
                ", HOLDAFAMT='" + HOLDAFAMT + '\'' +
                ", HOLDBEGINDATE='" + HOLDBEGINDATE + '\'' +
                ", HOLDENDDATE='" + HOLDENDDATE + '\'' +
                ", HOLDNAME='" + HOLDNAME + '\'' +
                ", NOWSTATUS='" + NOWSTATUS + '\'' +
                ", REALTETYPE='" + REALTETYPE + '\'' +
                ", REMARK='" + REMARK + '\'' +
                '}';
    }
}
