package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd08;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：查找（利翃）实还正常本金和逾期本金之和
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd08RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "total_shbj")
    private BigDecimal total_shbj;//本次实还正常本金金额（单位元）+本次实还逾期本金金额（单位元）

    public BigDecimal getTotal_shbj() {
        return total_shbj;
    }

    public void setTotal_shbj(BigDecimal total_shbj) {
        this.total_shbj = total_shbj;
    }

    @Override
    public String toString() {
        return "Fbxd08RespDto{" +
                "total_shbj='" + total_shbj + '\'' +
                '}';
    }
}  
