package cn.com.yusys.yusp.dto.client.esb.wx.wxp002.resp;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷将授信额度推送给移动端
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxp002RespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Wxp002RespDto{" +
                '}';
    }
}  
