package cn.com.yusys.yusp.dto.client.esb.zjywxt.jcxxcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：缴存信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class JcxxcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "xybhsq")
    private String xybhsq;//协议编号
    @JsonProperty(value = "qydm")
    private String qydm;//区域代码
    @JsonProperty(value = "tranam")
    private BigDecimal tranam;//存款金额
    @JsonProperty(value = "yjcje")
    private BigDecimal yjcje;//已缴存金额
    @JsonProperty(value = "syjcje")
    private BigDecimal syjcje;//剩余缴存金额
    @JsonProperty(value = "acctno")
    private String acctno;//买方账号
    @JsonProperty(value = "acctsa")
    private String acctsa;//买方户名
    @JsonProperty(value = "acctba")
    private String acctba;//资金托管账号
    @JsonProperty(value = "dataid")
    private String dataid;//资金托管账户名

    public String getXybhsq() {
        return xybhsq;
    }

    public void setXybhsq(String xybhsq) {
        this.xybhsq = xybhsq;
    }

    public String getQydm() {
        return qydm;
    }

    public void setQydm(String qydm) {
        this.qydm = qydm;
    }

    public BigDecimal getTranam() {
        return tranam;
    }

    public void setTranam(BigDecimal tranam) {
        this.tranam = tranam;
    }

    public BigDecimal getYjcje() {
        return yjcje;
    }

    public void setYjcje(BigDecimal yjcje) {
        this.yjcje = yjcje;
    }

    public BigDecimal getSyjcje() {
        return syjcje;
    }

    public void setSyjcje(BigDecimal syjcje) {
        this.syjcje = syjcje;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctsa() {
        return acctsa;
    }

    public void setAcctsa(String acctsa) {
        this.acctsa = acctsa;
    }

    public String getAcctba() {
        return acctba;
    }

    public void setAcctba(String acctba) {
        this.acctba = acctba;
    }

    public String getDataid() {
        return dataid;
    }

    public void setDataid(String dataid) {
        this.dataid = dataid;
    }

    @Override
    public String toString() {
        return "JcxxcxRespDto{" +
                "xybhsq='" + xybhsq + '\'' +
                ", qydm='" + qydm + '\'' +
                ", tranam=" + tranam +
                ", yjcje=" + yjcje +
                ", syjcje=" + syjcje +
                ", acctno='" + acctno + '\'' +
                ", acctsa='" + acctsa + '\'' +
                ", acctba='" + acctba + '\'' +
                ", dataid='" + dataid + '\'' +
                '}';
    }
}
