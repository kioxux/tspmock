package cn.com.yusys.yusp.dto.client.http.sjzt.xdhxQueryTotalList.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/7/22 14:14
 * @since 2021/7/22 14:14
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "dkyerj")
    private BigDecimal dkyerj;//贷款余额日均
    @JsonProperty(value = "jsnmxze")
    private BigDecimal jsnmxze;//较上年末新增额
    @JsonProperty(value = "ckyerj")
    private BigDecimal ckyerj;//存款余额年日均
    @JsonProperty(value = "jsnxze")
    private BigDecimal jsnxze;//较上年新增额
    @JsonProperty(value = "srzhckjehj")
    private BigDecimal srzhckjehj;//上日账户存款余额合计
    @JsonProperty(value = "dqzhckye")
    private BigDecimal dqzhckye;//当前账户存款余额

    @JsonProperty(value = "cusManagerName")
    private String cusManagerName;//客户经理名称
    @JsonProperty(value = "orgIdName")
    private String orgIdName; //返回机构中文

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public BigDecimal getDkyerj() {
        return dkyerj;
    }

    public void setDkyerj(BigDecimal dkyerj) {
        this.dkyerj = dkyerj;
    }

    public BigDecimal getJsnmxze() {
        return jsnmxze;
    }

    public void setJsnmxze(BigDecimal jsnmxze) {
        this.jsnmxze = jsnmxze;
    }

    public BigDecimal getCkyerj() {
        return ckyerj;
    }

    public void setCkyerj(BigDecimal ckyerj) {
        this.ckyerj = ckyerj;
    }

    public BigDecimal getJsnxze() {
        return jsnxze;
    }

    public void setJsnxze(BigDecimal jsnxze) {
        this.jsnxze = jsnxze;
    }

    public BigDecimal getSrzhckjehj() {
        return srzhckjehj;
    }

    public void setSrzhckjehj(BigDecimal srzhckjehj) {
        this.srzhckjehj = srzhckjehj;
    }

    public BigDecimal getDqzhckye() {
        return dqzhckye;
    }

    public void setDqzhckye(BigDecimal dqzhckye) {
        this.dqzhckye = dqzhckye;
    }

    public String getCusManagerName() {
        return cusManagerName;
    }

    public void setCusManagerName(String cusManagerName) {
        this.cusManagerName = cusManagerName;
    }

    public String getOrgIdName() {
        return orgIdName;
    }

    public void setOrgIdName(String orgIdName) {
        this.orgIdName = orgIdName;
    }

    @Override
    public String toString() {
        return "List{" +
                "cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", dkyerj=" + dkyerj +
                ", jsnmxze=" + jsnmxze +
                ", ckyerj=" + ckyerj +
                ", jsnxze=" + jsnxze +
                ", srzhckjehj=" + srzhckjehj +
                ", dqzhckye=" + dqzhckye +
                ", cusManagerName='" + cusManagerName + '\'' +
                ", orgIdName='" + orgIdName + '\'' +
                '}';
    }
}
