package cn.com.yusys.yusp.dto.server.biz.xdxw0029.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：根据客户号查询现有融资总额、总余额、担保方式
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusNo")
    private String cusNo;//客户号

    public String getCusNo() {
        return cusNo;
    }

    public void setCusNo(String cusNo) {
        this.cusNo = cusNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusNo='" + cusNo + '\'' +
                '}';
    }
}
