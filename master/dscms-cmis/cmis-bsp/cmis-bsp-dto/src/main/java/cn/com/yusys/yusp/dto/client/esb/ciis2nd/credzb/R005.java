package cn.com.yusys.yusp.dto.client.esb.ciis2nd.credzb;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//个人获取客户最新授权
@JsonPropertyOrder(alphabetic = true)
public class R005 implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "CREDITDOCID")
    private String CREDITDOCID;// 影像编号
    @JsonProperty(value = "ARCHIVECREATEDATE")
    private String ARCHIVECREATEDATE;//授权签订日期

    @JsonIgnore
    public String getCREDITDOCID() {
        return CREDITDOCID;
    }

    @JsonIgnore
    public void setCREDITDOCID(String CREDITDOCID) {
        this.CREDITDOCID = CREDITDOCID;
    }

    @JsonIgnore
    public String getARCHIVECREATEDATE() {
        return ARCHIVECREATEDATE;
    }

    @JsonIgnore
    public void setARCHIVECREATEDATE(String ARCHIVECREATEDATE) {
        this.ARCHIVECREATEDATE = ARCHIVECREATEDATE;
    }

    @Override
    public String toString() {
        return "R005{" +
                "CREDITDOCID='" + CREDITDOCID + '\'' +
                ", ARCHIVECREATEDATE='" + ARCHIVECREATEDATE + '\'' +
                '}';
    }
}
