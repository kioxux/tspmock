package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj14;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信贷签约通知
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj14ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "manageBrId")
    private String manageBrId;//管理机构号
    @JsonProperty(value = "custName")
    private String custName;//客户名称
    @JsonProperty(value = "discountNo")
    private String discountNo;//协议编号
    @JsonProperty(value = "phoneNo")
    private String phoneNo;//电话
    @JsonProperty(value = "secondDisBalance")
    private BigDecimal secondDisBalance;//
    @JsonProperty(value = "totalDisBalance")
    private BigDecimal totalDisBalance;//总额度
    @JsonProperty(value = "custNo")
    private String custNo;//客户号
    @JsonProperty(value = "totalOnlineBalance")
    private BigDecimal totalOnlineBalance;//总线上额度
    @JsonProperty(value = "startDate")
    private String startDate;//起始日期
    @JsonProperty(value = "endDate")
    private String endDate;//终止日期
    @JsonProperty(value = "type")
    private String type;//


    public String getManageBrId() {
        return manageBrId;
    }

    public void setManageBrId(String manageBrId) {
        this.manageBrId = manageBrId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getDiscountNo() {
        return discountNo;
    }

    public void setDiscountNo(String discountNo) {
        this.discountNo = discountNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public BigDecimal getSecondDisBalance() {
        return secondDisBalance;
    }

    public void setSecondDisBalance(BigDecimal secondDisBalance) {
        this.secondDisBalance = secondDisBalance;
    }

    public BigDecimal getTotalDisBalance() {
        return totalDisBalance;
    }

    public void setTotalDisBalance(BigDecimal totalDisBalance) {
        this.totalDisBalance = totalDisBalance;
    }

    public String getCustNo() {
        return custNo;
    }

    public void setCustNo(String custNo) {
        this.custNo = custNo;
    }

    public BigDecimal getTotalOnlineBalance() {
        return totalOnlineBalance;
    }

    public void setTotalOnlineBalance(BigDecimal totalOnlineBalance) {
        this.totalOnlineBalance = totalOnlineBalance;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Xdpj14ReqDto{" +
                ", manageBrId='" + manageBrId + '\'' +
                ", custName='" + custName + '\'' +
                ", discountNo='" + discountNo + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                ", secondDisBalance=" + secondDisBalance +
                ", totalDisBalance=" + totalDisBalance +
                ", custNo='" + custNo + '\'' +
                ", totalOnlineBalance=" + totalOnlineBalance +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}  
