package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	动产抵押-登记信息
@JsonPropertyOrder(alphabetic = true)
public class MORTGAGEREG implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "MAB_GUAR_AMT")
    private String MAB_GUAR_AMT;//	被担保债权数额
    @JsonProperty(value = "MAB_REGNO")
    private String MAB_REGNO;//	登记编号
    @JsonProperty(value = "MAB_REG_DATE")
    private String MAB_REG_DATE;//	登记日期
    @JsonProperty(value = "MAB_REG_ORG")
    private String MAB_REG_ORG;//	登记机关

    @JsonIgnore
    public String getMAB_GUAR_AMT() {
        return MAB_GUAR_AMT;
    }

    @JsonIgnore
    public void setMAB_GUAR_AMT(String MAB_GUAR_AMT) {
        this.MAB_GUAR_AMT = MAB_GUAR_AMT;
    }

    @JsonIgnore
    public String getMAB_REGNO() {
        return MAB_REGNO;
    }

    @JsonIgnore
    public void setMAB_REGNO(String MAB_REGNO) {
        this.MAB_REGNO = MAB_REGNO;
    }

    @JsonIgnore
    public String getMAB_REG_DATE() {
        return MAB_REG_DATE;
    }

    @JsonIgnore
    public void setMAB_REG_DATE(String MAB_REG_DATE) {
        this.MAB_REG_DATE = MAB_REG_DATE;
    }

    @JsonIgnore
    public String getMAB_REG_ORG() {
        return MAB_REG_ORG;
    }

    @JsonIgnore
    public void setMAB_REG_ORG(String MAB_REG_ORG) {
        this.MAB_REG_ORG = MAB_REG_ORG;
    }

    @Override
    public String toString() {
        return "MORTGAGEREG{" +
                "MAB_GUAR_AMT='" + MAB_GUAR_AMT + '\'' +
                ", MAB_REGNO='" + MAB_REGNO + '\'' +
                ", MAB_REG_DATE='" + MAB_REG_DATE + '\'' +
                ", MAB_REG_ORG='" + MAB_REG_ORG + '\'' +
                '}';
    }
}
