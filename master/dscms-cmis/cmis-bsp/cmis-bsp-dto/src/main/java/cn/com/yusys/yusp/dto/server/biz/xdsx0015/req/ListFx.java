package cn.com.yusys.yusp.dto.server.biz.xdsx0015.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 14:49:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 14:49
 * @since 2021/5/19 14:49
 */
@JsonPropertyOrder(alphabetic = true)
public class ListFx implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "fx_assure_means_main")
    private String fx_assure_means_main;//分项担保方式
    @JsonProperty(value = "crd_lmt_type")
    private String crd_lmt_type;//授信额度类型
    @JsonProperty(value = "crd_lmt")
    private String crd_lmt;//授信额度（元）
    @JsonProperty(value = "product")
    private String product;//公司特色产品
    @JsonProperty(value = "fx_score")
    private String fx_score;//风险评估得分
    @JsonProperty(value = "flag_serno")
    private String flag_serno;//标志字段

    public String getFx_assure_means_main() {
        return fx_assure_means_main;
    }

    public void setFx_assure_means_main(String fx_assure_means_main) {
        this.fx_assure_means_main = fx_assure_means_main;
    }

    public String getCrd_lmt_type() {
        return crd_lmt_type;
    }

    public void setCrd_lmt_type(String crd_lmt_type) {
        this.crd_lmt_type = crd_lmt_type;
    }

    public String getCrd_lmt() {
        return crd_lmt;
    }

    public void setCrd_lmt(String crd_lmt) {
        this.crd_lmt = crd_lmt;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getFx_score() {
        return fx_score;
    }

    public void setFx_score(String fx_score) {
        this.fx_score = fx_score;
    }

    public String getFlag_serno() {
        return flag_serno;
    }

    public void setFlag_serno(String flag_serno) {
        this.flag_serno = flag_serno;
    }

    @Override
    public String toString() {
        return "ListFx{" +
                "fx_assure_means_main='" + fx_assure_means_main + '\'' +
                ", crd_lmt_type='" + crd_lmt_type + '\'' +
                ", crd_lmt='" + crd_lmt + '\'' +
                ", product='" + product + '\'' +
                ", fx_score='" + fx_score + '\'' +
                ", flag_serno='" + flag_serno + '\'' +
                '}';
    }
}
