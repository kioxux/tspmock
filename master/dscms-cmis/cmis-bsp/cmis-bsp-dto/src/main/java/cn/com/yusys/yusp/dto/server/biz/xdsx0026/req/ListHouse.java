package cn.com.yusys.yusp.dto.server.biz.xdsx0026.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/19 13:49:<br>
 * 房产信息
 * @author ZRC
 * @version 0.1
 * @date 2021/5/19 13:49
 * @since 2021/5/19 13:49
 */
@JsonPropertyOrder(alphabetic = true)
public class ListHouse implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "province_id")
    private String province_id;//省id
    @JsonProperty(value = "province_name")
    private String province_name;//省名称
    @JsonProperty(value = "province_alias")
    private String province_alias;//省别名
    @JsonProperty(value = "province_zip_code")
    private String province_zip_code;//省国际编码
    @JsonProperty(value = "city_id")
    private String city_id;//城市id
    @JsonProperty(value = "city_name")
    private String city_name;//城市名称
    @JsonProperty(value = "city_alias")
    private String city_alias;//城市别名
    @JsonProperty(value = "city_zip_code")
    private String city_zip_code;//城市国际编码
    @JsonProperty(value = "area_id")
    private String area_id;//区县id
    @JsonProperty(value = "area_name")
    private String area_name;//区县名称
    @JsonProperty(value = "area_zip_code")
    private String area_zip_code;//区县国际编码
    @JsonProperty(value = "project_id")
    private String project_id;//楼盘id
    @JsonProperty(value = "project_name")
    private String project_name;//楼盘名称关键字
    @JsonProperty(value = "building_id")
    private String building_id;//楼栋id
    @JsonProperty(value = "building_name")
    private String building_name;//楼栋名称
    @JsonProperty(value = "floor")
    private String floor;//楼层(实际层)
    @JsonProperty(value = "total_floor")
    private String total_floor;//总层
    @JsonProperty(value = "house_id")
    private String house_id;//房号id
    @JsonProperty(value = "house_name")
    private String house_name;//房号名称
    @JsonProperty(value = "house_type")
    private String house_type;//房产类型
    @JsonProperty(value = "house_area")
    private BigDecimal house_area;//房产面积
    @JsonProperty(value = "house_right_no")
    private String house_right_no;//产权证号
    @JsonProperty(value = "house_region_info")
    private String house_region_info;//区位信息
    @JsonProperty(value = "acreage")
    private BigDecimal acreage;//土地面积
    @JsonProperty(value = "is_rent")
    private String is_rent;//是否出租
    @JsonProperty(value = "eval_method")
    private String eval_method;//评估方式
    @JsonProperty(value = "eval_value")
    private BigDecimal eval_value;//评估价值
    @JsonProperty(value = "mortgagee")
    private String mortgagee;//抵押物所有权人
    @JsonProperty(value = "bicycle_garage")
    private String bicycle_garage;//自行车库面积
    @JsonProperty(value = "parking_area")
    private String parking_area;//车位面积
    @JsonProperty(value = "loft_area")
    private String loft_area;//阁楼面积
    @JsonProperty(value = "guaranty_id")
    private String guaranty_id;//阁楼面积
    @JsonProperty(value = "bicycle_garage_eval")
    private String bicycle_garage_eval;//阁楼面积
    @JsonProperty(value = "park_space_eval")
    private String park_space_eval;//阁楼面积
    @JsonProperty(value = "loft_eval")
    private String loft_eval;//阁楼面积
    @JsonProperty(value = "total_value")
    private String total_value;//阁楼面积

    public String getGuaranty_id() {
        return guaranty_id;
    }

    public void setGuaranty_id(String guaranty_id) {
        this.guaranty_id = guaranty_id;
    }

    public String getBicycle_garage_eval() {
        return bicycle_garage_eval;
    }

    public void setBicycle_garage_eval(String bicycle_garage_eval) {
        this.bicycle_garage_eval = bicycle_garage_eval;
    }

    public String getPark_space_eval() {
        return park_space_eval;
    }

    public void setPark_space_eval(String park_space_eval) {
        this.park_space_eval = park_space_eval;
    }

    public String getLoft_eval() {
        return loft_eval;
    }

    public void setLoft_eval(String loft_eval) {
        this.loft_eval = loft_eval;
    }

    public String getTotal_value() {
        return total_value;
    }

    public void setTotal_value(String total_value) {
        this.total_value = total_value;
    }

    public String getProvince_id() {
        return province_id;
    }

    public void setProvince_id(String province_id) {
        this.province_id = province_id;
    }

    public String getProvince_name() {
        return province_name;
    }

    public void setProvince_name(String province_name) {
        this.province_name = province_name;
    }

    public String getProvince_alias() {
        return province_alias;
    }

    public void setProvince_alias(String province_alias) {
        this.province_alias = province_alias;
    }

    public String getProvince_zip_code() {
        return province_zip_code;
    }

    public void setProvince_zip_code(String province_zip_code) {
        this.province_zip_code = province_zip_code;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCity_alias() {
        return city_alias;
    }

    public void setCity_alias(String city_alias) {
        this.city_alias = city_alias;
    }

    public String getCity_zip_code() {
        return city_zip_code;
    }

    public void setCity_zip_code(String city_zip_code) {
        this.city_zip_code = city_zip_code;
    }

    public String getArea_id() {
        return area_id;
    }

    public void setArea_id(String area_id) {
        this.area_id = area_id;
    }

    public String getArea_name() {
        return area_name;
    }

    public void setArea_name(String area_name) {
        this.area_name = area_name;
    }

    public String getArea_zip_code() {
        return area_zip_code;
    }

    public void setArea_zip_code(String area_zip_code) {
        this.area_zip_code = area_zip_code;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getBuilding_id() {
        return building_id;
    }

    public void setBuilding_id(String building_id) {
        this.building_id = building_id;
    }

    public String getBuilding_name() {
        return building_name;
    }

    public void setBuilding_name(String building_name) {
        this.building_name = building_name;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getTotal_floor() {
        return total_floor;
    }

    public void setTotal_floor(String total_floor) {
        this.total_floor = total_floor;
    }

    public String getHouse_id() {
        return house_id;
    }

    public void setHouse_id(String house_id) {
        this.house_id = house_id;
    }

    public String getHouse_name() {
        return house_name;
    }

    public void setHouse_name(String house_name) {
        this.house_name = house_name;
    }

    public String getHouse_type() {
        return house_type;
    }

    public void setHouse_type(String house_type) {
        this.house_type = house_type;
    }

    public BigDecimal getHouse_area() {
        return house_area;
    }

    public void setHouse_area(BigDecimal house_area) {
        this.house_area = house_area;
    }

    public String getHouse_right_no() {
        return house_right_no;
    }

    public void setHouse_right_no(String house_right_no) {
        this.house_right_no = house_right_no;
    }

    public String getHouse_region_info() {
        return house_region_info;
    }

    public void setHouse_region_info(String house_region_info) {
        this.house_region_info = house_region_info;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public String getIs_rent() {
        return is_rent;
    }

    public void setIs_rent(String is_rent) {
        this.is_rent = is_rent;
    }

    public String getEval_method() {
        return eval_method;
    }

    public void setEval_method(String eval_method) {
        this.eval_method = eval_method;
    }

    public BigDecimal getEval_value() {
        return eval_value;
    }

    public void setEval_value(BigDecimal eval_value) {
        this.eval_value = eval_value;
    }

    public String getMortgagee() {
        return mortgagee;
    }

    public void setMortgagee(String mortgagee) {
        this.mortgagee = mortgagee;
    }

    public String getBicycle_garage() {
        return bicycle_garage;
    }

    public void setBicycle_garage(String bicycle_garage) {
        this.bicycle_garage = bicycle_garage;
    }

    public String getParking_area() {
        return parking_area;
    }

    public void setParking_area(String parking_area) {
        this.parking_area = parking_area;
    }

    public String getLoft_area() {
        return loft_area;
    }

    public void setLoft_area(String loft_area) {
        this.loft_area = loft_area;
    }

    @Override
    public String toString() {
        return "ListHouse{" +
                "province_id='" + province_id + '\'' +
                ", province_name='" + province_name + '\'' +
                ", province_alias='" + province_alias + '\'' +
                ", province_zip_code='" + province_zip_code + '\'' +
                ", city_id='" + city_id + '\'' +
                ", city_name='" + city_name + '\'' +
                ", city_alias='" + city_alias + '\'' +
                ", city_zip_code='" + city_zip_code + '\'' +
                ", area_id='" + area_id + '\'' +
                ", area_name='" + area_name + '\'' +
                ", area_zip_code='" + area_zip_code + '\'' +
                ", project_id='" + project_id + '\'' +
                ", project_name='" + project_name + '\'' +
                ", building_id='" + building_id + '\'' +
                ", building_name='" + building_name + '\'' +
                ", floor='" + floor + '\'' +
                ", total_floor='" + total_floor + '\'' +
                ", house_id='" + house_id + '\'' +
                ", house_name='" + house_name + '\'' +
                ", house_type='" + house_type + '\'' +
                ", house_area=" + house_area +
                ", house_right_no='" + house_right_no + '\'' +
                ", house_region_info='" + house_region_info + '\'' +
                ", acreage=" + acreage +
                ", is_rent='" + is_rent + '\'' +
                ", eval_method='" + eval_method + '\'' +
                ", eval_value=" + eval_value +
                ", mortgagee='" + mortgagee + '\'' +
                ", bicycle_garage='" + bicycle_garage + '\'' +
                ", parking_area='" + parking_area + '\'' +
                ", loft_area='" + loft_area + '\'' +
                ", guaranty_id='" + guaranty_id + '\'' +
                ", bicycle_garage_eval='" + bicycle_garage_eval + '\'' +
                ", park_space_eval='" + park_space_eval + '\'' +
                ", loft_eval='" + loft_eval + '\'' +
                ", total_value='" + total_value + '\'' +
                '}';
    }
}

