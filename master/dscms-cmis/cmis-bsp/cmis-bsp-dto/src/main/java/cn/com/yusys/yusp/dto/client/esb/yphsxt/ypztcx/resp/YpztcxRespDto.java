package cn.com.yusys.yusp.dto.client.esb.yphsxt.ypztcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应Dto：信贷押品状态查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class YpztcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "reocnu")
    private String reocnu;//登记簿概要信息-不动产权证号
    @JsonProperty(value = "reoreu")
    private String reoreu;//登记簿概要信息-不动产单元号
    @JsonProperty(value = "reoona")
    private String reoona;//登记簿概要信息-权利人
    @JsonProperty(value = "reoodn")
    private String reoodn;//登记簿概要信息-证件号
    @JsonProperty(value = "reobda")
    private String reobda;//登记簿概要信息-登簿日期
    @JsonProperty(value = "reooss")
    private String reooss;//登记簿概要信息-权属状态
    @JsonProperty(value = "reorem")
    private String reorem;//登记簿概要信息-附记

    @JsonProperty(value = "registerfacilitylist")
    private java.util.List<Registerfacilitylist> registerfacilitylist;
    @JsonProperty(value = "registerhouselist")
    private java.util.List<Registerhouselist> registerhouselist;
    @JsonProperty(value = "registerlandlist")
    private java.util.List<Registerlandlist> registerlandlist;
    @JsonProperty(value = "registermortgagelist")
    private java.util.List<Registermortgagelist> registermortgagelist;
    @JsonProperty(value = "registerownerlist")
    private java.util.List<Registerownerlist> registerownerlist;
    @JsonProperty(value = "registerseizurelist")
    private java.util.List<Registerseizurelist> registerseizurelist;


    public String getReocnu() {
        return reocnu;
    }

    public void setReocnu(String reocnu) {
        this.reocnu = reocnu;
    }

    public String getReoreu() {
        return reoreu;
    }

    public void setReoreu(String reoreu) {
        this.reoreu = reoreu;
    }

    public String getReoona() {
        return reoona;
    }

    public void setReoona(String reoona) {
        this.reoona = reoona;
    }

    public String getReoodn() {
        return reoodn;
    }

    public void setReoodn(String reoodn) {
        this.reoodn = reoodn;
    }

    public String getReobda() {
        return reobda;
    }

    public void setReobda(String reobda) {
        this.reobda = reobda;
    }

    public String getReooss() {
        return reooss;
    }

    public void setReooss(String reooss) {
        this.reooss = reooss;
    }

    public String getReorem() {
        return reorem;
    }

    public void setReorem(String reorem) {
        this.reorem = reorem;
    }

    public List<Registerfacilitylist> getRegisterfacilitylist() {
        return registerfacilitylist;
    }

    public void setRegisterfacilitylist(List<Registerfacilitylist> registerfacilitylist) {
        this.registerfacilitylist = registerfacilitylist;
    }

    public List<Registerhouselist> getRegisterhouselist() {
        return registerhouselist;
    }

    public void setRegisterhouselist(List<Registerhouselist> registerhouselist) {
        this.registerhouselist = registerhouselist;
    }

    public List<Registerlandlist> getRegisterlandlist() {
        return registerlandlist;
    }

    public void setRegisterlandlist(List<Registerlandlist> registerlandlist) {
        this.registerlandlist = registerlandlist;
    }

    public List<Registermortgagelist> getRegistermortgagelist() {
        return registermortgagelist;
    }

    public void setRegistermortgagelist(List<Registermortgagelist> registermortgagelist) {
        this.registermortgagelist = registermortgagelist;
    }

    public List<Registerownerlist> getRegisterownerlist() {
        return registerownerlist;
    }

    public void setRegisterownerlist(List<Registerownerlist> registerownerlist) {
        this.registerownerlist = registerownerlist;
    }

    public List<Registerseizurelist> getRegisterseizurelist() {
        return registerseizurelist;
    }

    public void setRegisterseizurelist(List<Registerseizurelist> registerseizurelist) {
        this.registerseizurelist = registerseizurelist;
    }

    @Override
    public String toString() {
        return "YpztcxRespDto{" +
                "reocnu='" + reocnu + '\'' +
                ", reoreu='" + reoreu + '\'' +
                ", reoona='" + reoona + '\'' +
                ", reoodn='" + reoodn + '\'' +
                ", reobda='" + reobda + '\'' +
                ", reooss='" + reooss + '\'' +
                ", reorem='" + reorem + '\'' +
                ", registerfacilitylist=" + registerfacilitylist +
                ", registerhouselist=" + registerhouselist +
                ", registerlandlist=" + registerlandlist +
                ", registermortgagelist=" + registermortgagelist +
                ", registerownerlist=" + registerownerlist +
                ", registerseizurelist=" + registerseizurelist +
                '}';
    }
}
