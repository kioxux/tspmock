package cn.com.yusys.yusp.dto.server.biz.xdxw0019.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/19 11:22:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/19 11:22
 * @since 2021/5/19 11:22
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "sendStatus")
    private String sendStatus;//推送状态

    public String getSendStatus() {
        return sendStatus;
    }

    public void setSendStatus(String sendStatus) {
        this.sendStatus = sendStatus;
    }

    @Override
    public String toString() {
        return "Data{" +
                "sendStatus='" + sendStatus + '\'' +
                '}';
    }
}
