package cn.com.yusys.yusp.dto.client.http.outerdata.slno01;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：双录流水号
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Slno01RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "guarno")
    private String guarno;//抵押物编号
    @JsonProperty(value = "yxzjls")
    private String yxzjls;//影像主键流水号
    @JsonProperty(value = "insert_time")
    private String insert_time;
    @JsonProperty(value = "slyxserno")
    private String slyxserno;

    public String getGuarno() {
        return guarno;
    }

    public void setGuarno(String guarno) {
        this.guarno = guarno;
    }

    public String getYxzjls() {
        return yxzjls;
    }

    public void setYxzjls(String yxzjls) {
        this.yxzjls = yxzjls;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getInsert_time() {
        return insert_time;
    }

    public void setInsert_time(String insert_time) {
        this.insert_time = insert_time;
    }

    public String getSlyxserno() {
        return slyxserno;
    }

    public void setSlyxserno(String slyxserno) {
        this.slyxserno = slyxserno;
    }

    @Override
    public String toString() {
        return "Slno01RespDto{" +
                "guarno='" + guarno + '\'' +
                ", yxzjls='" + yxzjls + '\'' +
                ", insert_time='" + insert_time + '\'' +
                ", slyxserno='" + slyxserno + '\'' +
                '}';
    }
}
