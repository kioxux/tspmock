package cn.com.yusys.yusp.dto.server.biz.xddb0018.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：押品状态同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guarNo")
    private String guarNo;//押品编号
    @JsonProperty(value = "guarStatus")
    private String guarStatus;//押品状态状态

    public String getGuarNo() {
        return guarNo;
    }

    public void setGuarNo(String guarNo) {
        this.guarNo = guarNo;
    }

    public String getGuarStatus() {
        return guarStatus;
    }

    public void setGuarStatus(String guarStatus) {
        this.guarStatus = guarStatus;
    }

    @Override
    public String toString() {
        return "Data{" +
                "guarNo='" + guarNo + '\'' +
                ", guarStatus='" + guarStatus + '\'' +
                '}';
    }
}