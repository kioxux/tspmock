package cn.com.yusys.yusp.dto.server.cus.xdkh0011.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：对私客户信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class RelInfoList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indivCusRel")
    private String indivCusRel;//关系类型
    @JsonProperty(value = "valitg")
    private String valitg;//有效标志
    @JsonProperty(value = "jhrflg")
    private String jhrflg;//监护人标志
    @JsonProperty(value = "name")
    private String name;//关系人姓名
    @JsonProperty(value = "correCusId")
    private String correCusId;//关系人客户编号
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certNo")
    private String certNo;//证件号码
    @JsonProperty(value = "sex")
    private String sex;//性别
    @JsonProperty(value = "indivDtOfBirth")
    private String indivDtOfBirth;//出生日期
    @JsonProperty(value = "nation")
    private String nation;//国籍
    @JsonProperty(value = "indivFolk")
    private String indivFolk;//民族
    @JsonProperty(value = "propts")
    private String propts;//居民性质
    @JsonProperty(value = "indivEdt")
    private String indivEdt;//教育水平（学历）
    @JsonProperty(value = "unitName")
    private String unitName;//工作单位
    @JsonProperty(value = "occu")
    private String occu;//职业
    @JsonProperty(value = "duty")
    private String duty;//职务
    @JsonProperty(value = "monthn")
    private BigDecimal monthn;//月收入
    @JsonProperty(value = "yearn")
    private BigDecimal yearn;//年收入
    @JsonProperty(value = "linkArea")
    private String linkArea;//联系地址
    @JsonProperty(value = "linkPhone")
    private String linkPhone;//联系电话
    @JsonProperty(value = "email")
    private String email;//邮箱

    public String getIndivCusRel() {
        return indivCusRel;
    }

    public void setIndivCusRel(String indivCusRel) {
        this.indivCusRel = indivCusRel;
    }

    public String getValitg() {
        return valitg;
    }

    public void setValitg(String valitg) {
        this.valitg = valitg;
    }

    public String getJhrflg() {
        return jhrflg;
    }

    public void setJhrflg(String jhrflg) {
        this.jhrflg = jhrflg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCorreCusId() {
        return correCusId;
    }

    public void setCorreCusId(String correCusId) {
        this.correCusId = correCusId;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getIndivDtOfBirth() {
        return indivDtOfBirth;
    }

    public void setIndivDtOfBirth(String indivDtOfBirth) {
        this.indivDtOfBirth = indivDtOfBirth;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getIndivFolk() {
        return indivFolk;
    }

    public void setIndivFolk(String indivFolk) {
        this.indivFolk = indivFolk;
    }

    public String getPropts() {
        return propts;
    }

    public void setPropts(String propts) {
        this.propts = propts;
    }

    public String getIndivEdt() {
        return indivEdt;
    }

    public void setIndivEdt(String indivEdt) {
        this.indivEdt = indivEdt;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getOccu() {
        return occu;
    }

    public void setOccu(String occu) {
        this.occu = occu;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    public BigDecimal getMonthn() {
        return monthn;
    }

    public void setMonthn(BigDecimal monthn) {
        this.monthn = monthn;
    }

    public BigDecimal getYearn() {
        return yearn;
    }

    public void setYearn(BigDecimal yearn) {
        this.yearn = yearn;
    }

    public String getLinkArea() {
        return linkArea;
    }

    public void setLinkArea(String linkArea) {
        this.linkArea = linkArea;
    }

    public String getLinkPhone() {
        return linkPhone;
    }

    public void setLinkPhone(String linkPhone) {
        this.linkPhone = linkPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "List{" +
                "indivCusRel='" + indivCusRel + '\'' +
                ", valitg='" + valitg + '\'' +
                ", jhrflg='" + jhrflg + '\'' +
                ", name='" + name + '\'' +
                ", correCusId='" + correCusId + '\'' +
                ", certType='" + certType + '\'' +
                ", certNo='" + certNo + '\'' +
                ", sex='" + sex + '\'' +
                ", indivDtOfBirth='" + indivDtOfBirth + '\'' +
                ", nation='" + nation + '\'' +
                ", indivFolk='" + indivFolk + '\'' +
                ", propts='" + propts + '\'' +
                ", indivEdt='" + indivEdt + '\'' +
                ", unitName='" + unitName + '\'' +
                ", occu='" + occu + '\'' +
                ", duty='" + duty + '\'' +
                ", monthn=" + monthn +
                ", yearn=" + yearn +
                ", linkArea='" + linkArea + '\'' +
                ", linkPhone='" + linkPhone + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}