package cn.com.yusys.yusp.dto.client.esb.ecif.s10501;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * 响应DTO：对私客户清单查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class S10501RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "bginnm")
    private String bginnm;//起始笔数
    @JsonProperty(value = "qurynm")
    private String qurynm;// 查询笔数
    @JsonProperty(value = "totlnm")
    private String totlnm;//  总笔数
    @JsonProperty(value = "listnm")
    private Integer listnm;//    循环列表记录数
    @JsonProperty(value = "LIST_ARRAY_INFO")
    private List<ListArrayInfo> listArrayInfo;//清单客户信息_ARRAY

    public String getBginnm() {
        return bginnm;
    }

    public void setBginnm(String bginnm) {
        this.bginnm = bginnm;
    }

    public String getQurynm() {
        return qurynm;
    }

    public void setQurynm(String qurynm) {
        this.qurynm = qurynm;
    }

    public String getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(String totlnm) {
        this.totlnm = totlnm;
    }

    public Integer getListnm() {
        return listnm;
    }

    public void setListnm(Integer listnm) {
        this.listnm = listnm;
    }

    public List<ListArrayInfo> getListArrayInfo() {
        return listArrayInfo;
    }

    public void setListArrayInfo(List<ListArrayInfo> listArrayInfo) {
        this.listArrayInfo = listArrayInfo;
    }

    @Override
    public String toString() {
        return "S10501RespDto{" +
                "bginnm='" + bginnm + '\'' +
                ", qurynm='" + qurynm + '\'' +
                ", totlnm='" + totlnm + '\'' +
                ", listnm=" + listnm +
                ", listArrayInfo=" + listArrayInfo +
                '}';
    }
}
