package cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/30 21:11
 * @since 2021/8/30 21:11
 */
@JsonPropertyOrder(alphabetic = true)
public class QuotaRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private java.util.List<cn.com.yusys.yusp.dto.client.http.sjzt.quota.resp.Data> data;
    @JsonProperty(value = "status")
    private String status;
    @JsonProperty(value = "message")
    private String message;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "QuotaRespDto{" +
                "data=" + data +
                ", status='" + status + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
