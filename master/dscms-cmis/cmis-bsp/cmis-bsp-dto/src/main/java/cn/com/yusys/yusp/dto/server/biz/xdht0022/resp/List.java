package cn.com.yusys.yusp.dto.server.biz.xdht0022.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：合同信息列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "contNo")
    private String contNo;//合同编号
    @JsonProperty(value = "cnContNo")
    private String cnContNo;//中文合同编号
    @JsonProperty(value = "serno")
    private String serno;//全局流水号
    @JsonProperty(value = "iqpSerno")
    private String iqpSerno;//业务申请流水号
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "prdId")
    private String prdId;//产品编号
    @JsonProperty(value = "prdName")
    private String prdName;//产品名称
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码
    @JsonProperty(value = "phone")
    private String phone;//手机号码
    @JsonProperty(value = "loanModal")
    private String loanModal;//贷款形式 STD_ZB_LOAN_MODAL
    @JsonProperty(value = "loanCha")
    private String loanCha;//贷款性质 STD_ZB_LOAN_CHA
    @JsonProperty(value = "isHasRefused")
    private String isHasRefused;//是否曾被拒绝
    @JsonProperty(value = "guarWay")
    private String guarWay;//主担保方式 STD_ZB_GUAR_WAY
    @JsonProperty(value = "isCommonRqstr")
    private String isCommonRqstr;//是否共同申请人 STD_ZB_YES_NO
    @JsonProperty(value = "isCfirmPayWay")
    private String isCfirmPayWay;//是否确认支付方式 STD_ZB_YES_NO
    @JsonProperty(value = "defrayMode")
    private String defrayMode;//支付方式 STD_ZB_RAY_MODE
    @JsonProperty(value = "curType")
    private String curType;//币种 STD_ZB_CUR_TYP
    @JsonProperty(value = "contAmt")
    private BigDecimal contAmt;//合同金额
    @JsonProperty(value = "contBalance")
    private BigDecimal contBalance;//合同余额
    @JsonProperty(value = "contRate")
    private BigDecimal contRate;//合同汇率
    @JsonProperty(value = "contType")
    private String contType;//合同类型
    @JsonProperty(value = "contSignDate")
    private String contSignDate;//纸质合同签订日期
    @JsonProperty(value = "curtLprRate")
    private BigDecimal curtLprRate;//当前LPR利率(%)
    @JsonProperty(value = "lprPriceInterval")
    private String lprPriceInterval;//LPR定价区间
    @JsonProperty(value = "lprBp")
    private BigDecimal lprBp;//LPR浮动点(BP)
    @JsonProperty(value = "eiMode")
    private String eiMode;//结息方式
    @JsonProperty(value = "eiModeExpl")
    private String eiModeExpl;//结息具体说明
    @JsonProperty(value = "drawMode")
    private String drawMode;//提款方式
    @JsonProperty(value = "dayLimit")
    private String dayLimit;//提款天数限制
    @JsonProperty(value = "address")
    private String address;//地址
    @JsonProperty(value = "fax")
    private String fax;//传真
    @JsonProperty(value = "telephone")
    private String telephone;//电话
    @JsonProperty(value = "mail")
    private String mail;//邮箱
    @JsonProperty(value = "qq")
    private String qq;//QQ
    @JsonProperty(value = "wechat")
    private String wechat;//微信
    @JsonProperty(value = "otherPhone")
    private String otherPhone;//其他通讯方式及账号
    @JsonProperty(value = "doubleVideoNo")
    private String doubleVideoNo;//双录编号
    @JsonProperty(value = "signMode")
    private String signMode;//签订方式
    @JsonProperty(value = "belgLine")
    private String belgLine;//所属条线
    @JsonProperty(value = "irFloatType")
    private String irFloatType;//正常利率浮动方式 STD_ZB_RFLOAT_TYP
    @JsonProperty(value = "irFloatRate")
    private BigDecimal irFloatRate;//利率浮动百分比
    @JsonProperty(value = "irFloatPoint")
    private BigDecimal irFloatPoint;//固定加点值
    @JsonProperty(value = "realityIrY")
    private BigDecimal realityIrY;//执行利率（年）
    @JsonProperty(value = "realityIrM")
    private BigDecimal realityIrM;//执行利率(月)
    @JsonProperty(value = "overdueFloatType")
    private String overdueFloatType;//逾期利率浮动方式 STD_ZB_RFLOAT_TYP
    @JsonProperty(value = "overduePoint")
    private BigDecimal overduePoint;//逾期利率浮动加点值
    @JsonProperty(value = "overdueRate")
    private BigDecimal overdueRate;//逾期利率浮动百分比
    @JsonProperty(value = "overdueRateY")
    private BigDecimal overdueRateY;//逾期利率（年）
    @JsonProperty(value = "defaultFloatType")
    private String defaultFloatType;//违约利率浮动方式 STD_ZB_RFLOAT_TYP
    @JsonProperty(value = "defaultPoint")
    private BigDecimal defaultPoint;//违约利率浮动加点值
    @JsonProperty(value = "defaultRate")
    private BigDecimal defaultRate;//违约利率浮动百分比
    @JsonProperty(value = "defaultRateY")
    private BigDecimal defaultRateY;//违约利率（年）
    @JsonProperty(value = "riskOpenAmt")
    private BigDecimal riskOpenAmt;//风险敞口金额
    @JsonProperty(value = "repayType")
    private String repayType;//还款方式 STD_ZB_REPAY_TYP
    @JsonProperty(value = "stopPintTerm")
    private String stopPintTerm;//停本付息期间 STD_ZB_PINT_TERM
    @JsonProperty(value = "repayTerm")
    private String repayTerm;//还款间隔周期 STD_ZB_REPAY_TERM
    @JsonProperty(value = "repaySpace")
    private String repaySpace;//还款间隔 STD_ZB_REPAY_SPACE
    @JsonProperty(value = "repayRule")
    private String repayRule;//还款日确定规则 STD_ZB_REPAY_RULE
    @JsonProperty(value = "repayDtType")
    private String repayDtType;//还款日类型 STD_ZB_REPAY_DT_TYPE
    @JsonProperty(value = "repayDate")
    private BigDecimal repayDate;//还款日
    @JsonProperty(value = "capGraperType")
    private String capGraperType;//本金宽限方式 STD_ZB_GRAPER_TYPE
    @JsonProperty(value = "capGraperDay")
    private BigDecimal capGraperDay;//本金宽限天数
    @JsonProperty(value = "intGraperType")
    private String intGraperType;//利息宽限方式 STD_ZB_GRAPER_TYPE
    @JsonProperty(value = "intGraperDay")
    private Integer intGraperDay;//利息宽限天数
    @JsonProperty(value = "deductDeduType")
    private String deductDeduType;//扣款扣息方式 STD_ZB_DEDUCT_TYPE
    @JsonProperty(value = "repayFreType")
    private String repayFreType;//还款频率类型 STD_ZB_REPAY_FRE_TYPE
    @JsonProperty(value = "repayFre")
    private BigDecimal repayFre;//本息还款频率
    @JsonProperty(value = "liquFreeTime")
    private Integer liquFreeTime;//提前还款违约金免除时间(月)
    @JsonProperty(value = "subType")
    private String subType;//分段方式 STD_ZB_SUB_TYPE
    @JsonProperty(value = "reserveTerm")
    private Integer reserveTerm;//保留期限
    @JsonProperty(value = "calTerm")
    private Integer calTerm;//计算期限
    @JsonProperty(value = "reserveAmt")
    private BigDecimal reserveAmt;//保留金额
    @JsonProperty(value = "repayTermOne")
    private Integer repayTermOne;//第一阶段还款期数
    @JsonProperty(value = "repayAmtOne")
    private BigDecimal repayAmtOne;//第一阶段还款本金
    @JsonProperty(value = "repayTermTwo")
    private Integer repayTermTwo;//第二阶段还款期数
    @JsonProperty(value = "repayAmtTwo")
    private BigDecimal repayAmtTwo;//第二阶段还款本金
    @JsonProperty(value = "rateSelType")
    private String rateSelType;//利率选取日期种类 STD_ZB_RATE_SEL_TYPE
    @JsonProperty(value = "sbsyMode")
    private String sbsyMode;//贴息方式 STD_ZB_SBSY_WAY
    @JsonProperty(value = "sbsyPerc")
    private BigDecimal sbsyPerc;//贴息比例
    @JsonProperty(value = "sbsyAmt")
    private BigDecimal sbsyAmt;//贴息金额
    @JsonProperty(value = "sbsyUnitName")
    private String sbsyUnitName;//贴息单位名称
    @JsonProperty(value = "sbsyAcct")
    private String sbsyAcct;//贴息方账户
    @JsonProperty(value = "sbsyAcctName")
    private String sbsyAcctName;//贴息方账户户名
    @JsonProperty(value = "fiveClass")
    private String fiveClass;//五级分类 STD_ZB_FIVE_SORT
    @JsonProperty(value = "loanDirection")
    private String loanDirection;//贷款投向 STD_GB_4754-2011
    @JsonProperty(value = "comUpIndtify")
    private String comUpIndtify;//工业转型升级标识 STD_ZB_YES_NO
    @JsonProperty(value = "strategyNewLoan")
    private String strategyNewLoan;//战略新兴产业类型 STD_ZB_SEIS_TYP
    @JsonProperty(value = "isCulEstate")
    private String isCulEstate;//是否文化产业 STD_ZB_YES_NO
    @JsonProperty(value = "loanType")
    private String loanType;//贷款种类 STD_PER_POSITIONTYPE
    @JsonProperty(value = "estateAdjustType")
    private String estateAdjustType;//产业结构调整类型 STD_ZB_EST_ADJ_TYP
    @JsonProperty(value = "newPrdLoan")
    private String newPrdLoan;//新兴产业贷款 STD_ZB_NEWPRD_LOAN
    @JsonProperty(value = "repaySrcDes")
    private String repaySrcDes;//还款来源
    @JsonProperty(value = "isAuthorize")
    private String isAuthorize;//是否委托人办理 STD_ZB_YES_NO
    @JsonProperty(value = "authedName")
    private String authedName;//委托人姓名
    @JsonProperty(value = "authedCertType")
    private String authedCertType;//委托人证件类型 STD_ZB_CERT_TYP
    @JsonProperty(value = "authedCertCode")
    private String authedCertCode;//委托人证件号
    @JsonProperty(value = "authedTelNo")
    private String authedTelNo;//委托人联系方式
    @JsonProperty(value = "signDate")
    private String signDate;//签订日期
    @JsonProperty(value = "logoutDate")
    private String logoutDate;//注销日期
    @JsonProperty(value = "chnlSour")
    private String chnlSour;//渠道来源 STD_ZB_CHNL_SOUR
    @JsonProperty(value = "billDispupeOpt")
    private String billDispupeOpt;//争议解决方式 STD_ZB_DISPUPE_OPT
    @JsonProperty(value = "courtAddr")
    private String courtAddr;//法院所在地
    @JsonProperty(value = "arbitrateBch")
    private String arbitrateBch;//仲裁委员会
    @JsonProperty(value = "arbitrateAddr")
    private String arbitrateAddr;//仲裁委员会地点
    @JsonProperty(value = "otherOpt")
    private String otherOpt;//其他方式
    @JsonProperty(value = "contQnt")
    private Integer contQnt;//合同份数
    @JsonProperty(value = "pundContNo")
    private String pundContNo;//公积金贷款合同编号
    @JsonProperty(value = "spplClause")
    private String spplClause;//补充条款
    @JsonProperty(value = "signAddr")
    private String signAddr;//签约地点
    @JsonProperty(value = "busiNetwork")
    private String busiNetwork;//营业网点
    @JsonProperty(value = "mainBusiPalce")
    private String mainBusiPalce;//主要营业场所地址
    @JsonProperty(value = "contTemplate")
    private String contTemplate;//合同模板
    @JsonProperty(value = "contPrintNum")
    private Integer contPrintNum;//合同打印次数
    @JsonProperty(value = "contStatus")
    private String contStatus;//合同状态 STD_ZB_CONT_TYP
    @JsonProperty(value = "managerBrId")
    private String managerBrId;//主办机构
    @JsonProperty(value = "inputId")
    private String inputId;//登记人
    @JsonProperty(value = "inputBrId")
    private String inputBrId;//登记机构
    @JsonProperty(value = "inputDate")
    private String inputDate;//最后修改日期
    @JsonProperty(value = "updId")
    private String updId;//最后修改人
    @JsonProperty(value = "updBrId")
    private String updBrId;//最后修改机构
    @JsonProperty(value = "updDate")
    private String updDate;//最后修改日期
    @JsonProperty(value = "createTime")
    private String createTime;//创建时间
    @JsonProperty(value = "updateTime")
    private String updateTime;//修改时间
    @JsonProperty(value = "surveySerno")
    private String surveySerno;//调查流水号
    @JsonProperty(value = "contStartDate")
    private String contStartDate;//合同起始日期
    @JsonProperty(value = "contEndDate")
    private String contEndDate;//合同到期日期
    @JsonProperty(value = "termType")
    private String termType;//期限类型
    @JsonProperty(value = "appTerm")
    private String appTerm;//申请期限

    public String getContNo() {
        return contNo;
    }

    public void setContNo(String contNo) {
        this.contNo = contNo;
    }

    public String getCnContNo() {
        return cnContNo;
    }

    public void setCnContNo(String cnContNo) {
        this.cnContNo = cnContNo;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getIqpSerno() {
        return iqpSerno;
    }

    public void setIqpSerno(String iqpSerno) {
        this.iqpSerno = iqpSerno;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getPrdId() {
        return prdId;
    }

    public void setPrdId(String prdId) {
        this.prdId = prdId;
    }

    public String getPrdName() {
        return prdName;
    }

    public void setPrdName(String prdName) {
        this.prdName = prdName;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLoanModal() {
        return loanModal;
    }

    public void setLoanModal(String loanModal) {
        this.loanModal = loanModal;
    }

    public String getLoanCha() {
        return loanCha;
    }

    public void setLoanCha(String loanCha) {
        this.loanCha = loanCha;
    }

    public String getIsHasRefused() {
        return isHasRefused;
    }

    public void setIsHasRefused(String isHasRefused) {
        this.isHasRefused = isHasRefused;
    }

    public String getGuarWay() {
        return guarWay;
    }

    public void setGuarWay(String guarWay) {
        this.guarWay = guarWay;
    }

    public String getIsCommonRqstr() {
        return isCommonRqstr;
    }

    public void setIsCommonRqstr(String isCommonRqstr) {
        this.isCommonRqstr = isCommonRqstr;
    }

    public String getIsCfirmPayWay() {
        return isCfirmPayWay;
    }

    public void setIsCfirmPayWay(String isCfirmPayWay) {
        this.isCfirmPayWay = isCfirmPayWay;
    }

    public String getDefrayMode() {
        return defrayMode;
    }

    public void setDefrayMode(String defrayMode) {
        this.defrayMode = defrayMode;
    }

    public String getCurType() {
        return curType;
    }

    public void setCurType(String curType) {
        this.curType = curType;
    }

    public BigDecimal getContAmt() {
        return contAmt;
    }

    public void setContAmt(BigDecimal contAmt) {
        this.contAmt = contAmt;
    }

    public BigDecimal getContBalance() {
        return contBalance;
    }

    public void setContBalance(BigDecimal contBalance) {
        this.contBalance = contBalance;
    }

    public BigDecimal getContRate() {
        return contRate;
    }

    public void setContRate(BigDecimal contRate) {
        this.contRate = contRate;
    }

    public String getContType() {
        return contType;
    }

    public void setContType(String contType) {
        this.contType = contType;
    }

    public String getContSignDate() {
        return contSignDate;
    }

    public void setContSignDate(String contSignDate) {
        this.contSignDate = contSignDate;
    }

    public BigDecimal getCurtLprRate() {
        return curtLprRate;
    }

    public void setCurtLprRate(BigDecimal curtLprRate) {
        this.curtLprRate = curtLprRate;
    }

    public String getLprPriceInterval() {
        return lprPriceInterval;
    }

    public void setLprPriceInterval(String lprPriceInterval) {
        this.lprPriceInterval = lprPriceInterval;
    }

    public BigDecimal getLprBp() {
        return lprBp;
    }

    public void setLprBp(BigDecimal lprBp) {
        this.lprBp = lprBp;
    }

    public String getEiMode() {
        return eiMode;
    }

    public void setEiMode(String eiMode) {
        this.eiMode = eiMode;
    }

    public String getEiModeExpl() {
        return eiModeExpl;
    }

    public void setEiModeExpl(String eiModeExpl) {
        this.eiModeExpl = eiModeExpl;
    }

    public String getDrawMode() {
        return drawMode;
    }

    public void setDrawMode(String drawMode) {
        this.drawMode = drawMode;
    }

    public String getDayLimit() {
        return dayLimit;
    }

    public void setDayLimit(String dayLimit) {
        this.dayLimit = dayLimit;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWechat() {
        return wechat;
    }

    public void setWechat(String wechat) {
        this.wechat = wechat;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public String getDoubleVideoNo() {
        return doubleVideoNo;
    }

    public void setDoubleVideoNo(String doubleVideoNo) {
        this.doubleVideoNo = doubleVideoNo;
    }

    public String getSignMode() {
        return signMode;
    }

    public void setSignMode(String signMode) {
        this.signMode = signMode;
    }

    public String getBelgLine() {
        return belgLine;
    }

    public void setBelgLine(String belgLine) {
        this.belgLine = belgLine;
    }

    public String getIrFloatType() {
        return irFloatType;
    }

    public void setIrFloatType(String irFloatType) {
        this.irFloatType = irFloatType;
    }

    public BigDecimal getIrFloatRate() {
        return irFloatRate;
    }

    public void setIrFloatRate(BigDecimal irFloatRate) {
        this.irFloatRate = irFloatRate;
    }

    public BigDecimal getIrFloatPoint() {
        return irFloatPoint;
    }

    public void setIrFloatPoint(BigDecimal irFloatPoint) {
        this.irFloatPoint = irFloatPoint;
    }

    public BigDecimal getRealityIrY() {
        return realityIrY;
    }

    public void setRealityIrY(BigDecimal realityIrY) {
        this.realityIrY = realityIrY;
    }

    public BigDecimal getRealityIrM() {
        return realityIrM;
    }

    public void setRealityIrM(BigDecimal realityIrM) {
        this.realityIrM = realityIrM;
    }

    public String getOverdueFloatType() {
        return overdueFloatType;
    }

    public void setOverdueFloatType(String overdueFloatType) {
        this.overdueFloatType = overdueFloatType;
    }

    public BigDecimal getOverduePoint() {
        return overduePoint;
    }

    public void setOverduePoint(BigDecimal overduePoint) {
        this.overduePoint = overduePoint;
    }

    public BigDecimal getOverdueRate() {
        return overdueRate;
    }

    public void setOverdueRate(BigDecimal overdueRate) {
        this.overdueRate = overdueRate;
    }

    public BigDecimal getOverdueRateY() {
        return overdueRateY;
    }

    public void setOverdueRateY(BigDecimal overdueRateY) {
        this.overdueRateY = overdueRateY;
    }

    public String getDefaultFloatType() {
        return defaultFloatType;
    }

    public void setDefaultFloatType(String defaultFloatType) {
        this.defaultFloatType = defaultFloatType;
    }

    public BigDecimal getDefaultPoint() {
        return defaultPoint;
    }

    public void setDefaultPoint(BigDecimal defaultPoint) {
        this.defaultPoint = defaultPoint;
    }

    public BigDecimal getDefaultRate() {
        return defaultRate;
    }

    public void setDefaultRate(BigDecimal defaultRate) {
        this.defaultRate = defaultRate;
    }

    public BigDecimal getDefaultRateY() {
        return defaultRateY;
    }

    public void setDefaultRateY(BigDecimal defaultRateY) {
        this.defaultRateY = defaultRateY;
    }

    public BigDecimal getRiskOpenAmt() {
        return riskOpenAmt;
    }

    public void setRiskOpenAmt(BigDecimal riskOpenAmt) {
        this.riskOpenAmt = riskOpenAmt;
    }

    public String getRepayType() {
        return repayType;
    }

    public void setRepayType(String repayType) {
        this.repayType = repayType;
    }

    public String getStopPintTerm() {
        return stopPintTerm;
    }

    public void setStopPintTerm(String stopPintTerm) {
        this.stopPintTerm = stopPintTerm;
    }

    public String getRepayTerm() {
        return repayTerm;
    }

    public void setRepayTerm(String repayTerm) {
        this.repayTerm = repayTerm;
    }

    public String getRepaySpace() {
        return repaySpace;
    }

    public void setRepaySpace(String repaySpace) {
        this.repaySpace = repaySpace;
    }

    public String getRepayRule() {
        return repayRule;
    }

    public void setRepayRule(String repayRule) {
        this.repayRule = repayRule;
    }

    public String getRepayDtType() {
        return repayDtType;
    }

    public void setRepayDtType(String repayDtType) {
        this.repayDtType = repayDtType;
    }

    public BigDecimal getRepayDate() {
        return repayDate;
    }

    public void setRepayDate(BigDecimal repayDate) {
        this.repayDate = repayDate;
    }

    public String getCapGraperType() {
        return capGraperType;
    }

    public void setCapGraperType(String capGraperType) {
        this.capGraperType = capGraperType;
    }

    public BigDecimal getCapGraperDay() {
        return capGraperDay;
    }

    public void setCapGraperDay(BigDecimal capGraperDay) {
        this.capGraperDay = capGraperDay;
    }

    public String getIntGraperType() {
        return intGraperType;
    }

    public void setIntGraperType(String intGraperType) {
        this.intGraperType = intGraperType;
    }

    public Integer getIntGraperDay() {
        return intGraperDay;
    }

    public void setIntGraperDay(Integer intGraperDay) {
        this.intGraperDay = intGraperDay;
    }

    public String getDeductDeduType() {
        return deductDeduType;
    }

    public void setDeductDeduType(String deductDeduType) {
        this.deductDeduType = deductDeduType;
    }

    public String getRepayFreType() {
        return repayFreType;
    }

    public void setRepayFreType(String repayFreType) {
        this.repayFreType = repayFreType;
    }

    public BigDecimal getRepayFre() {
        return repayFre;
    }

    public void setRepayFre(BigDecimal repayFre) {
        this.repayFre = repayFre;
    }

    public Integer getLiquFreeTime() {
        return liquFreeTime;
    }

    public void setLiquFreeTime(Integer liquFreeTime) {
        this.liquFreeTime = liquFreeTime;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Integer getReserveTerm() {
        return reserveTerm;
    }

    public void setReserveTerm(Integer reserveTerm) {
        this.reserveTerm = reserveTerm;
    }

    public Integer getCalTerm() {
        return calTerm;
    }

    public void setCalTerm(Integer calTerm) {
        this.calTerm = calTerm;
    }

    public BigDecimal getReserveAmt() {
        return reserveAmt;
    }

    public void setReserveAmt(BigDecimal reserveAmt) {
        this.reserveAmt = reserveAmt;
    }

    public Integer getRepayTermOne() {
        return repayTermOne;
    }

    public void setRepayTermOne(Integer repayTermOne) {
        this.repayTermOne = repayTermOne;
    }

    public BigDecimal getRepayAmtOne() {
        return repayAmtOne;
    }

    public void setRepayAmtOne(BigDecimal repayAmtOne) {
        this.repayAmtOne = repayAmtOne;
    }

    public Integer getRepayTermTwo() {
        return repayTermTwo;
    }

    public void setRepayTermTwo(Integer repayTermTwo) {
        this.repayTermTwo = repayTermTwo;
    }

    public BigDecimal getRepayAmtTwo() {
        return repayAmtTwo;
    }

    public void setRepayAmtTwo(BigDecimal repayAmtTwo) {
        this.repayAmtTwo = repayAmtTwo;
    }

    public String getRateSelType() {
        return rateSelType;
    }

    public void setRateSelType(String rateSelType) {
        this.rateSelType = rateSelType;
    }

    public String getSbsyMode() {
        return sbsyMode;
    }

    public void setSbsyMode(String sbsyMode) {
        this.sbsyMode = sbsyMode;
    }

    public BigDecimal getSbsyPerc() {
        return sbsyPerc;
    }

    public void setSbsyPerc(BigDecimal sbsyPerc) {
        this.sbsyPerc = sbsyPerc;
    }

    public BigDecimal getSbsyAmt() {
        return sbsyAmt;
    }

    public void setSbsyAmt(BigDecimal sbsyAmt) {
        this.sbsyAmt = sbsyAmt;
    }

    public String getSbsyUnitName() {
        return sbsyUnitName;
    }

    public void setSbsyUnitName(String sbsyUnitName) {
        this.sbsyUnitName = sbsyUnitName;
    }

    public String getSbsyAcct() {
        return sbsyAcct;
    }

    public void setSbsyAcct(String sbsyAcct) {
        this.sbsyAcct = sbsyAcct;
    }

    public String getSbsyAcctName() {
        return sbsyAcctName;
    }

    public void setSbsyAcctName(String sbsyAcctName) {
        this.sbsyAcctName = sbsyAcctName;
    }

    public String getFiveClass() {
        return fiveClass;
    }

    public void setFiveClass(String fiveClass) {
        this.fiveClass = fiveClass;
    }

    public String getLoanDirection() {
        return loanDirection;
    }

    public void setLoanDirection(String loanDirection) {
        this.loanDirection = loanDirection;
    }

    public String getComUpIndtify() {
        return comUpIndtify;
    }

    public void setComUpIndtify(String comUpIndtify) {
        this.comUpIndtify = comUpIndtify;
    }

    public String getStrategyNewLoan() {
        return strategyNewLoan;
    }

    public void setStrategyNewLoan(String strategyNewLoan) {
        this.strategyNewLoan = strategyNewLoan;
    }

    public String getIsCulEstate() {
        return isCulEstate;
    }

    public void setIsCulEstate(String isCulEstate) {
        this.isCulEstate = isCulEstate;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getEstateAdjustType() {
        return estateAdjustType;
    }

    public void setEstateAdjustType(String estateAdjustType) {
        this.estateAdjustType = estateAdjustType;
    }

    public String getNewPrdLoan() {
        return newPrdLoan;
    }

    public void setNewPrdLoan(String newPrdLoan) {
        this.newPrdLoan = newPrdLoan;
    }

    public String getRepaySrcDes() {
        return repaySrcDes;
    }

    public void setRepaySrcDes(String repaySrcDes) {
        this.repaySrcDes = repaySrcDes;
    }

    public String getIsAuthorize() {
        return isAuthorize;
    }

    public void setIsAuthorize(String isAuthorize) {
        this.isAuthorize = isAuthorize;
    }

    public String getAuthedName() {
        return authedName;
    }

    public void setAuthedName(String authedName) {
        this.authedName = authedName;
    }

    public String getAuthedCertType() {
        return authedCertType;
    }

    public void setAuthedCertType(String authedCertType) {
        this.authedCertType = authedCertType;
    }

    public String getAuthedCertCode() {
        return authedCertCode;
    }

    public void setAuthedCertCode(String authedCertCode) {
        this.authedCertCode = authedCertCode;
    }

    public String getAuthedTelNo() {
        return authedTelNo;
    }

    public void setAuthedTelNo(String authedTelNo) {
        this.authedTelNo = authedTelNo;
    }

    public String getSignDate() {
        return signDate;
    }

    public void setSignDate(String signDate) {
        this.signDate = signDate;
    }

    public String getLogoutDate() {
        return logoutDate;
    }

    public void setLogoutDate(String logoutDate) {
        this.logoutDate = logoutDate;
    }

    public String getChnlSour() {
        return chnlSour;
    }

    public void setChnlSour(String chnlSour) {
        this.chnlSour = chnlSour;
    }

    public String getBillDispupeOpt() {
        return billDispupeOpt;
    }

    public void setBillDispupeOpt(String billDispupeOpt) {
        this.billDispupeOpt = billDispupeOpt;
    }

    public String getCourtAddr() {
        return courtAddr;
    }

    public void setCourtAddr(String courtAddr) {
        this.courtAddr = courtAddr;
    }

    public String getArbitrateBch() {
        return arbitrateBch;
    }

    public void setArbitrateBch(String arbitrateBch) {
        this.arbitrateBch = arbitrateBch;
    }

    public String getArbitrateAddr() {
        return arbitrateAddr;
    }

    public void setArbitrateAddr(String arbitrateAddr) {
        this.arbitrateAddr = arbitrateAddr;
    }

    public String getOtherOpt() {
        return otherOpt;
    }

    public void setOtherOpt(String otherOpt) {
        this.otherOpt = otherOpt;
    }

    public Integer getContQnt() {
        return contQnt;
    }

    public void setContQnt(Integer contQnt) {
        this.contQnt = contQnt;
    }

    public String getPundContNo() {
        return pundContNo;
    }

    public void setPundContNo(String pundContNo) {
        this.pundContNo = pundContNo;
    }

    public String getSpplClause() {
        return spplClause;
    }

    public void setSpplClause(String spplClause) {
        this.spplClause = spplClause;
    }

    public String getSignAddr() {
        return signAddr;
    }

    public void setSignAddr(String signAddr) {
        this.signAddr = signAddr;
    }

    public String getBusiNetwork() {
        return busiNetwork;
    }

    public void setBusiNetwork(String busiNetwork) {
        this.busiNetwork = busiNetwork;
    }

    public String getMainBusiPalce() {
        return mainBusiPalce;
    }

    public void setMainBusiPalce(String mainBusiPalce) {
        this.mainBusiPalce = mainBusiPalce;
    }

    public String getContTemplate() {
        return contTemplate;
    }

    public void setContTemplate(String contTemplate) {
        this.contTemplate = contTemplate;
    }

    public Integer getContPrintNum() {
        return contPrintNum;
    }

    public void setContPrintNum(Integer contPrintNum) {
        this.contPrintNum = contPrintNum;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    public String getInputId() {
        return inputId;
    }

    public void setInputId(String inputId) {
        this.inputId = inputId;
    }

    public String getInputBrId() {
        return inputBrId;
    }

    public void setInputBrId(String inputBrId) {
        this.inputBrId = inputBrId;
    }

    public String getInputDate() {
        return inputDate;
    }

    public void setInputDate(String inputDate) {
        this.inputDate = inputDate;
    }

    public String getUpdId() {
        return updId;
    }

    public void setUpdId(String updId) {
        this.updId = updId;
    }

    public String getUpdBrId() {
        return updBrId;
    }

    public void setUpdBrId(String updBrId) {
        this.updBrId = updBrId;
    }

    public String getUpdDate() {
        return updDate;
    }

    public void setUpdDate(String updDate) {
        this.updDate = updDate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getSurveySerno() {
        return surveySerno;
    }

    public void setSurveySerno(String surveySerno) {
        this.surveySerno = surveySerno;
    }

    public void setContStartDate(String contStartDate) {
        this.contStartDate = contStartDate;
    }

    public String getContStartDate() {
        return this.contStartDate;
    }

    public void setContEndDate(String contEndDate) {
        this.contEndDate = contEndDate;
    }

    public String getContEndDate() {
        return this.contEndDate;
    }

    public String getTermType() {
        return termType;
    }

    public void setTermType(String termType) {
        this.termType = termType;
    }

    public String getAppTerm() {
        return appTerm;
    }

    public void setAppTerm(String appTerm) {
        this.appTerm = appTerm;
    }

    @Override
    public String toString() {
        return "List{" +
                "contNo='" + contNo + '\'' +
                ", cnContNo='" + cnContNo + '\'' +
                ", serno='" + serno + '\'' +
                ", iqpSerno='" + iqpSerno + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", prdId='" + prdId + '\'' +
                ", prdName='" + prdName + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", phone='" + phone + '\'' +
                ", loanModal='" + loanModal + '\'' +
                ", loanCha='" + loanCha + '\'' +
                ", isHasRefused='" + isHasRefused + '\'' +
                ", guarWay='" + guarWay + '\'' +
                ", isCommonRqstr='" + isCommonRqstr + '\'' +
                ", isCfirmPayWay='" + isCfirmPayWay + '\'' +
                ", defrayMode='" + defrayMode + '\'' +
                ", curType='" + curType + '\'' +
                ", contAmt=" + contAmt +
                ", contBalance=" + contBalance +
                ", contRate=" + contRate +
                ", contType='" + contType + '\'' +
                ", contSignDate='" + contSignDate + '\'' +
                ", curtLprRate=" + curtLprRate +
                ", lprPriceInterval='" + lprPriceInterval + '\'' +
                ", lprBp=" + lprBp +
                ", eiMode='" + eiMode + '\'' +
                ", eiModeExpl='" + eiModeExpl + '\'' +
                ", drawMode='" + drawMode + '\'' +
                ", dayLimit='" + dayLimit + '\'' +
                ", address='" + address + '\'' +
                ", fax='" + fax + '\'' +
                ", telephone='" + telephone + '\'' +
                ", mail='" + mail + '\'' +
                ", qq='" + qq + '\'' +
                ", wechat='" + wechat + '\'' +
                ", otherPhone='" + otherPhone + '\'' +
                ", doubleVideoNo='" + doubleVideoNo + '\'' +
                ", signMode='" + signMode + '\'' +
                ", belgLine='" + belgLine + '\'' +
                ", irFloatType='" + irFloatType + '\'' +
                ", irFloatRate=" + irFloatRate +
                ", irFloatPoint=" + irFloatPoint +
                ", realityIrY=" + realityIrY +
                ", realityIrM=" + realityIrM +
                ", overdueFloatType='" + overdueFloatType + '\'' +
                ", overduePoint=" + overduePoint +
                ", overdueRate=" + overdueRate +
                ", overdueRateY=" + overdueRateY +
                ", defaultFloatType='" + defaultFloatType + '\'' +
                ", defaultPoint=" + defaultPoint +
                ", defaultRate=" + defaultRate +
                ", defaultRateY=" + defaultRateY +
                ", riskOpenAmt=" + riskOpenAmt +
                ", repayType='" + repayType + '\'' +
                ", stopPintTerm='" + stopPintTerm + '\'' +
                ", repayTerm='" + repayTerm + '\'' +
                ", repaySpace='" + repaySpace + '\'' +
                ", repayRule='" + repayRule + '\'' +
                ", repayDtType='" + repayDtType + '\'' +
                ", repayDate=" + repayDate +
                ", capGraperType='" + capGraperType + '\'' +
                ", capGraperDay=" + capGraperDay +
                ", intGraperType='" + intGraperType + '\'' +
                ", intGraperDay=" + intGraperDay +
                ", deductDeduType='" + deductDeduType + '\'' +
                ", repayFreType='" + repayFreType + '\'' +
                ", repayFre=" + repayFre +
                ", liquFreeTime=" + liquFreeTime +
                ", subType='" + subType + '\'' +
                ", reserveTerm=" + reserveTerm +
                ", calTerm=" + calTerm +
                ", reserveAmt=" + reserveAmt +
                ", repayTermOne=" + repayTermOne +
                ", repayAmtOne=" + repayAmtOne +
                ", repayTermTwo=" + repayTermTwo +
                ", repayAmtTwo=" + repayAmtTwo +
                ", rateSelType='" + rateSelType + '\'' +
                ", sbsyMode='" + sbsyMode + '\'' +
                ", sbsyPerc=" + sbsyPerc +
                ", sbsyAmt=" + sbsyAmt +
                ", sbsyUnitName='" + sbsyUnitName + '\'' +
                ", sbsyAcct='" + sbsyAcct + '\'' +
                ", sbsyAcctName='" + sbsyAcctName + '\'' +
                ", fiveClass='" + fiveClass + '\'' +
                ", loanDirection='" + loanDirection + '\'' +
                ", comUpIndtify='" + comUpIndtify + '\'' +
                ", strategyNewLoan='" + strategyNewLoan + '\'' +
                ", isCulEstate='" + isCulEstate + '\'' +
                ", loanType='" + loanType + '\'' +
                ", estateAdjustType='" + estateAdjustType + '\'' +
                ", newPrdLoan='" + newPrdLoan + '\'' +
                ", repaySrcDes='" + repaySrcDes + '\'' +
                ", isAuthorize='" + isAuthorize + '\'' +
                ", authedName='" + authedName + '\'' +
                ", authedCertType='" + authedCertType + '\'' +
                ", authedCertCode='" + authedCertCode + '\'' +
                ", authedTelNo='" + authedTelNo + '\'' +
                ", signDate='" + signDate + '\'' +
                ", logoutDate='" + logoutDate + '\'' +
                ", chnlSour='" + chnlSour + '\'' +
                ", billDispupeOpt='" + billDispupeOpt + '\'' +
                ", courtAddr='" + courtAddr + '\'' +
                ", arbitrateBch='" + arbitrateBch + '\'' +
                ", arbitrateAddr='" + arbitrateAddr + '\'' +
                ", otherOpt='" + otherOpt + '\'' +
                ", contQnt=" + contQnt +
                ", pundContNo='" + pundContNo + '\'' +
                ", spplClause='" + spplClause + '\'' +
                ", signAddr='" + signAddr + '\'' +
                ", busiNetwork='" + busiNetwork + '\'' +
                ", mainBusiPalce='" + mainBusiPalce + '\'' +
                ", contTemplate='" + contTemplate + '\'' +
                ", contPrintNum=" + contPrintNum +
                ", contStatus='" + contStatus + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", inputId='" + inputId + '\'' +
                ", inputBrId='" + inputBrId + '\'' +
                ", inputDate='" + inputDate + '\'' +
                ", updId='" + updId + '\'' +
                ", updBrId='" + updBrId + '\'' +
                ", updDate='" + updDate + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", surveySerno='" + surveySerno + '\'' +
                ", contStartDate='" + contStartDate + '\'' +
                ", contEndDate='" + contEndDate + '\'' +
                ", termType='" + termType + '\'' +
                ", appTerm='" + appTerm + '\'' +
                '}';
    }
}
