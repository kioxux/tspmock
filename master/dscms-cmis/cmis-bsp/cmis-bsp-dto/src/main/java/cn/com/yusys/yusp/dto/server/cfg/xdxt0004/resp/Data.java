package cn.com.yusys.yusp.dto.server.cfg.xdxt0004.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

/**
 * <br>
 * 0.2ZRC:2021/5/25 8:54:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/25 8:54
 * @since 2021/5/25 8:54
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "areaList")
    private java.util.List<AreaList> areaList;

    public List<AreaList> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<AreaList> areaList) {
        this.areaList = areaList;
    }

    @Override
    public String toString() {
        return "Data{" +
                "areaList=" + areaList +
                '}';
    }
}
