package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd007;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：请求小V平台综合决策管理列表查询
 *
 * @author code-generator
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd007ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "indcertid")
    private String indcertid;//证件号码

    public String getIndcertid() {
        return indcertid;
    }

    public void setIndcertid(String indcertid) {
        this.indcertid = indcertid;
    }

    @Override
    public String toString() {
        return "Wxd007ReqDto{" +
                "indcertid='" + indcertid + '\'' +
                '}';
    }
}
