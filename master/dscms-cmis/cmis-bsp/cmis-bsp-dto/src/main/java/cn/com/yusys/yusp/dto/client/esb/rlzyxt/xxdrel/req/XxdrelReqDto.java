package cn.com.yusys.yusp.dto.client.esb.rlzyxt.xxdrel.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询人员基本信息岗位信息家庭信息
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class XxdrelReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ctmgno")
    private String ctmgno;//工号
    @JsonProperty(value = "cardid")
    private String cardid;//身份证号

    public String getCtmgno() {
        return ctmgno;
    }

    public void setCtmgno(String ctmgno) {
        this.ctmgno = ctmgno;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "XxdrelReqDto{" +
                "ctmgno='" + ctmgno + '\'' +
                "cardid='" + cardid + '\'' +
                '}';
    }
}  
