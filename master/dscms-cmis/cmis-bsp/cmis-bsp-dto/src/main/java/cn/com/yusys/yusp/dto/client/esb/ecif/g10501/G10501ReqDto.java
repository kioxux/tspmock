package cn.com.yusys.yusp.dto.client.esb.ecif.g10501;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求DTO：对公及同业客户清单查询
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
@JsonPropertyOrder(alphabetic = true)
public class G10501ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "resotp")
    private String resotp;// 识别方式
    @JsonProperty(value = "custno")
    private String custno;// 客户编号
    @JsonProperty(value = "idtftp")
    private String idtftp;// 证件类型
    @JsonProperty(value = "idtfno")
    private String idtfno;// 证件号码
    @JsonProperty(value = "custna")
    private String custna;// 客户名称
    @JsonProperty(value = "custst")
    private String custst;// 客户状态
    @JsonProperty(value = "bginnm")
    private String bginnm;// 起始笔数
    @JsonProperty(value = "qurynm")
    private String qurynm;// 查询笔数

    public String getResotp() {
        return resotp;
    }

    public void setResotp(String resotp) {
        this.resotp = resotp;
    }

    public String getCustno() {
        return custno;
    }

    public void setCustno(String custno) {
        this.custno = custno;
    }

    public String getIdtftp() {
        return idtftp;
    }

    public void setIdtftp(String idtftp) {
        this.idtftp = idtftp;
    }

    public String getIdtfno() {
        return idtfno;
    }

    public void setIdtfno(String idtfno) {
        this.idtfno = idtfno;
    }

    public String getCustna() {
        return custna;
    }

    public void setCustna(String custna) {
        this.custna = custna;
    }

    public String getCustst() {
        return custst;
    }

    public void setCustst(String custst) {
        this.custst = custst;
    }

    public String getBginnm() {
        return bginnm;
    }

    public void setBginnm(String bginnm) {
        this.bginnm = bginnm;
    }

    public String getQurynm() {
        return qurynm;
    }

    public void setQurynm(String qurynm) {
        this.qurynm = qurynm;
    }

    @Override
    public String toString() {
        return "G10501ReqDto{" +
                "resotp='" + resotp + '\'' +
                ", custno='" + custno + '\'' +
                ", idtftp='" + idtftp + '\'' +
                ", idtfno='" + idtfno + '\'' +
                ", custna='" + custna + '\'' +
                ", custst='" + custst + '\'' +
                ", bginnm='" + bginnm + '\'' +
                ", qurynm='" + qurynm + '\'' +
                '}';
    }
}
