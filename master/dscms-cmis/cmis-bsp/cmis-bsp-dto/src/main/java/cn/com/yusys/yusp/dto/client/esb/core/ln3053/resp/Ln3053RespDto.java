package cn.com.yusys.yusp.dto.client.esb.core.ln3053.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 响应Dto：还款计划调整
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Ln3053RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dkjiejuh")
    private String dkjiejuh;//贷款借据号
    @JsonProperty(value = "kehuhaoo")
    private String kehuhaoo;//客户号
    @JsonProperty(value = "kehmingc")
    private String kehmingc;//客户名称
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "qixiriqi")
    private String qixiriqi;//起息日期
    @JsonProperty(value = "daoqriqi")
    private String daoqriqi;//到期日期
    @JsonProperty(value = "jiaoyijg")
    private String jiaoyijg;//交易机构
    @JsonProperty(value = "jiaoyigy")
    private String jiaoyigy;//交易柜员
    @JsonProperty(value = "outdkzqg")
    private java.util.List<cn.com.yusys.yusp.dto.client.esb.core.ln3053.resp.Outdkzqg> outdkzqg;

    public String getDkjiejuh() {
        return dkjiejuh;
    }

    public void setDkjiejuh(String dkjiejuh) {
        this.dkjiejuh = dkjiejuh;
    }

    public String getKehuhaoo() {
        return kehuhaoo;
    }

    public void setKehuhaoo(String kehuhaoo) {
        this.kehuhaoo = kehuhaoo;
    }

    public String getKehmingc() {
        return kehmingc;
    }

    public void setKehmingc(String kehmingc) {
        this.kehmingc = kehmingc;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getQixiriqi() {
        return qixiriqi;
    }

    public void setQixiriqi(String qixiriqi) {
        this.qixiriqi = qixiriqi;
    }

    public String getDaoqriqi() {
        return daoqriqi;
    }

    public void setDaoqriqi(String daoqriqi) {
        this.daoqriqi = daoqriqi;
    }

    public String getJiaoyijg() {
        return jiaoyijg;
    }

    public void setJiaoyijg(String jiaoyijg) {
        this.jiaoyijg = jiaoyijg;
    }

    public String getJiaoyigy() {
        return jiaoyigy;
    }

    public void setJiaoyigy(String jiaoyigy) {
        this.jiaoyigy = jiaoyigy;
    }

    public List<Outdkzqg> getOutdkzqg() {
        return outdkzqg;
    }

    public void setOutdkzqg(List<Outdkzqg> outdkzqg) {
        this.outdkzqg = outdkzqg;
    }

    @Override
    public String toString() {
        return "Ln3053RespDto{" +
                "dkjiejuh='" + dkjiejuh + '\'' +
                ", kehuhaoo='" + kehuhaoo + '\'' +
                ", kehmingc='" + kehmingc + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", qixiriqi='" + qixiriqi + '\'' +
                ", daoqriqi='" + daoqriqi + '\'' +
                ", jiaoyijg='" + jiaoyijg + '\'' +
                ", jiaoyigy='" + jiaoyigy + '\'' +
                ", outdkzqg=" + outdkzqg +
                '}';
    }
}
