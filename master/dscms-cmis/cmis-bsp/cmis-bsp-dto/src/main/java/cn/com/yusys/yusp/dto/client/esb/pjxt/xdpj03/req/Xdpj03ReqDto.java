package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj03.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：票据承兑签发审批请求
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdpj03ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "txCode")
    private String txCode;//交易码
    @JsonProperty(value = "batchNo")
    private String batchNo;//批次号
    @JsonProperty(value = "protocolNo")
    private String protocolNo;//承兑协议编号
    @JsonProperty(value = "transType")
    private String transType;//处理码
    @JsonProperty(value = "isdraftpool")
    private String isdraftpool;//是否票据池编号
    @JsonProperty(value = "yp_sj")
    private String yp_sj;//发票信息补录状态
    @JsonProperty(value = "list")
    private java.util.List<List> list;//list

    public String getTxCode() {
        return txCode;
    }

    public void setTxCode(String txCode) {
        this.txCode = txCode;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public String getProtocolNo() {
        return protocolNo;
    }

    public void setProtocolNo(String protocolNo) {
        this.protocolNo = protocolNo;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getIsdraftpool() {
        return isdraftpool;
    }

    public void setIsdraftpool(String isdraftpool) {
        this.isdraftpool = isdraftpool;
    }

    public String getYp_sj() {
        return yp_sj;
    }

    public void setYp_sj(String yp_sj) {
        this.yp_sj = yp_sj;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdpj03ReqDto{" +
                "txCode='" + txCode + '\'' +
                "batchNo='" + batchNo + '\'' +
                "protocolNo='" + protocolNo + '\'' +
                "transType='" + transType + '\'' +
                "isdraftpool='" + isdraftpool + '\'' +
                "yp_sj='" + yp_sj + '\'' +
                "list='" + list + '\'' +
                '}';
    }
}  
