package cn.com.yusys.yusp.dto.client.http.image.imagetoken;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * 响应DTO：通过用户名和密码获取token
 *
 * @author leehuang
 * @version 1.0
 * @since 2021年4月10日 下午1:22:06
 */
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "Authorization")
    private String Authorization;//token值
    @JsonProperty(value = "name")
    private String name;//用户名

    @JsonIgnore
    public String getAuthorization() {
        return Authorization;
    }

    @JsonIgnore
    public void setAuthorization(String authorization) {
        Authorization = authorization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Data{" +
                "Authorization='" + Authorization + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
