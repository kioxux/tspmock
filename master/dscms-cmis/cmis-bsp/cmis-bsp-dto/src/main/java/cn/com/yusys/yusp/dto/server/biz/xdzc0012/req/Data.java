package cn.com.yusys.yusp.dto.server.biz.xdzc0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 15:42
 * @since 2021/6/8 15:42
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "opType")
    private String opType;//操作类型
    @JsonProperty("managerId")
    private String managerId;//主管客户经理
    @JsonProperty("managerBrId")
    private String managerBrId;//账务机构（管理机构）
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerBrId() {
        return managerBrId;
    }

    public void setManagerBrId(String managerBrId) {
        this.managerBrId = managerBrId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "opType='" + opType + '\'' +
                ", managerId='" + managerId + '\'' +
                ", managerBrId='" + managerBrId + '\'' +
                ", list=" + list +
                '}';
    }
}
