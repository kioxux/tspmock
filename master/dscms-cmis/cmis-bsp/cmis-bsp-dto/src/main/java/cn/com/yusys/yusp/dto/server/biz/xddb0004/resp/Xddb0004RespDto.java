package cn.com.yusys.yusp.dto.server.biz.xddb0004.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：提醒任务处理结果实时同步
 *
 * @author zhangpeng
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xddb0004RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

	@JsonProperty(value = "data")
	private Data data;

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "Xddb0004RespDto{" +
				"data=" + data +
				'}';
	}
}
