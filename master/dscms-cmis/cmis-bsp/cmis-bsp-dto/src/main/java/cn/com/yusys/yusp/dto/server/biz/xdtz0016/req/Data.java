package cn.com.yusys.yusp.dto.server.biz.xdtz0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "drftNo")
    private String drftNo;//票号
    @JsonProperty(value = "drftStatus")
    private String drftStatus;//票据状态

    public String getDrftNo() {
        return drftNo;
    }

    public void setDrftNo(String drftNo) {
        this.drftNo = drftNo;
    }

    public String getDrftStatus() {
        return drftStatus;
    }

    public void setDrftStatus(String drftStatus) {
        this.drftStatus = drftStatus;
    }

    @Override
    public String toString() {
        return "Data{" +
                "drftNo='" + drftNo + '\'' +
                ", drftStatus='" + drftStatus + '\'' +
                '}';
    }
}
