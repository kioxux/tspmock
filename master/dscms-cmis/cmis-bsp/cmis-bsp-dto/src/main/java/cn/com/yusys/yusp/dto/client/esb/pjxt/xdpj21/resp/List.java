package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj21.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/5/28 21:28
 * @since 2021/5/28 21:28
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "acctno")
    private String acctno;//账号
    @JsonProperty(value = "acctseq")
    private String acctseq;//保证金账号子序号
    @JsonProperty(value = "currency")
    private String currency;//币种
    @JsonProperty(value = "acctAmt")
    private BigDecimal acctAmt;//账户金额
    @JsonProperty(value = "interestMode")
    private String interestMode;//计息方式

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctseq() {
        return acctseq;
    }

    public void setAcctseq(String acctseq) {
        this.acctseq = acctseq;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public BigDecimal getAcctAmt() {
        return acctAmt;
    }

    public void setAcctAmt(BigDecimal acctAmt) {
        this.acctAmt = acctAmt;
    }

    public String getInterestMode() {
        return interestMode;
    }

    public void setInterestMode(String interestMode) {
        this.interestMode = interestMode;
    }

    @Override
    public String toString() {
        return "List{" +
                "acctno='" + acctno + '\'' +
                ", acctseq='" + acctseq + '\'' +
                ", currency='" + currency + '\'' +
                ", acctAmt=" + acctAmt +
                ", interestMode='" + interestMode + '\'' +
                '}';
    }
}
