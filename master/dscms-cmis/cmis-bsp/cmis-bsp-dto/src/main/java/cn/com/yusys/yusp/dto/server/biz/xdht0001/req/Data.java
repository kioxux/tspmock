package cn.com.yusys.yusp.dto.server.biz.xdht0001.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查询省心E付合同信息
 * @author xll
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "startPageNum")
    private String startPageNum;//起始页数
    @JsonProperty(value = "pageSize")
    private String pageSize;//分页大小
    @JsonProperty(value = "cusId")
    private String cusId;//客户号

    public String getStartPageNum() {
        return startPageNum;
    }

    public void setStartPageNum(String startPageNum) {
        this.startPageNum = startPageNum;
    }

    public String getPageSize() {
        return pageSize;
    }

    public void setPageSize(String pageSize) {
        this.pageSize = pageSize;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Data{" +
                "startPageNum='" + startPageNum + '\'' +
                "pageSize='" + pageSize + '\'' +
                "cusId='" + cusId + '\'' +
                '}';
    }
}
