package cn.com.yusys.yusp.dto.client.esb.rircp.fkyx01;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：优享贷客户经理分配通知接口
 */
@JsonPropertyOrder(alphabetic = true)
public class Fkyx01ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "app_no")
    private String app_no;//申请批复流水号
    @JsonProperty(value = "manager_id")
    private String manager_id;//客户经理号
    @JsonProperty(value = "manager_name")
    private String manager_name;//客户经理名称
    @JsonProperty(value = "org_id")
    private String org_id;//机构ID
    @JsonProperty(value = "org_name")
    private String org_name;//机构名称
    @JsonProperty(value = "manager_phone")
    private String manager_phone;//客户经理电话

    public String getApp_no() {
        return app_no;
    }

    public void setApp_no(String app_no) {
        this.app_no = app_no;
    }

    public String getManager_id() {
        return manager_id;
    }

    public void setManager_id(String manager_id) {
        this.manager_id = manager_id;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getOrg_name() {
        return org_name;
    }

    public void setOrg_name(String org_name) {
        this.org_name = org_name;
    }

    public String getManager_phone() {
        return manager_phone;
    }

    public void setManager_phone(String manager_phone) {
        this.manager_phone = manager_phone;
    }

    @Override
    public String toString() {
        return "Fkyx01ReqDto{" +
                "app_no='" + app_no + '\'' +
                "manager_id='" + manager_id + '\'' +
                "manager_name='" + manager_name + '\'' +
                "org_id='" + org_id + '\'' +
                "org_name='" + org_name + '\'' +
                "manager_phone='" + manager_phone + '\'' +
                '}';
    }
}  
