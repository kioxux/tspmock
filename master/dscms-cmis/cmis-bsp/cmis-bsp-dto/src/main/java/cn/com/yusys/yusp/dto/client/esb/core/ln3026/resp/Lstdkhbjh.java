package cn.com.yusys.yusp.dto.client.esb.core.ln3026.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：贷款还本计划
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstdkhbjh implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "dzhhkzhl")
    private String dzhhkzhl;//定制还款种类
    @JsonProperty(value = "xzuetqhk")
    private String xzuetqhk;//需足额提前还款
    @JsonProperty(value = "dzhkriqi")
    private String dzhkriqi;//定制还款日期
    @JsonProperty(value = "huanbjee")
    private BigDecimal huanbjee;//还本金额
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "hkyujrgz")
    private String hkyujrgz;//还款遇假日规则
    @JsonProperty(value = "sfyxkuxq")
    private String sfyxkuxq;//是否有宽限期
    @JsonProperty(value = "kuanxqts")
    private Integer kuanxqts;//宽限期天数
    @JsonProperty(value = "kxqjjrgz")
    private String kxqjjrgz;//宽限期节假日规则
    @JsonProperty(value = "tqhkhxfs")
    private String tqhkhxfs;//还息方式

    public String getDzhhkzhl() {
        return dzhhkzhl;
    }

    public void setDzhhkzhl(String dzhhkzhl) {
        this.dzhhkzhl = dzhhkzhl;
    }

    public String getXzuetqhk() {
        return xzuetqhk;
    }

    public void setXzuetqhk(String xzuetqhk) {
        this.xzuetqhk = xzuetqhk;
    }

    public String getDzhkriqi() {
        return dzhkriqi;
    }

    public void setDzhkriqi(String dzhkriqi) {
        this.dzhkriqi = dzhkriqi;
    }

    public BigDecimal getHuanbjee() {
        return huanbjee;
    }

    public void setHuanbjee(BigDecimal huanbjee) {
        this.huanbjee = huanbjee;
    }

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHkyujrgz() {
        return hkyujrgz;
    }

    public void setHkyujrgz(String hkyujrgz) {
        this.hkyujrgz = hkyujrgz;
    }

    public String getSfyxkuxq() {
        return sfyxkuxq;
    }

    public void setSfyxkuxq(String sfyxkuxq) {
        this.sfyxkuxq = sfyxkuxq;
    }

    public Integer getKuanxqts() {
        return kuanxqts;
    }

    public void setKuanxqts(Integer kuanxqts) {
        this.kuanxqts = kuanxqts;
    }

    public String getKxqjjrgz() {
        return kxqjjrgz;
    }

    public void setKxqjjrgz(String kxqjjrgz) {
        this.kxqjjrgz = kxqjjrgz;
    }

    public String getTqhkhxfs() {
        return tqhkhxfs;
    }

    public void setTqhkhxfs(String tqhkhxfs) {
        this.tqhkhxfs = tqhkhxfs;
    }

    @Override
    public String toString() {
        return "Lstdkhbjh{" +
                "dzhhkzhl='" + dzhhkzhl + '\'' +
                "xzuetqhk='" + xzuetqhk + '\'' +
                "dzhkriqi='" + dzhkriqi + '\'' +
                "huanbjee='" + huanbjee + '\'' +
                "huankzhh='" + huankzhh + '\'' +
                "hkzhhzxh='" + hkzhhzxh + '\'' +
                "hkyujrgz='" + hkyujrgz + '\'' +
                "sfyxkuxq='" + sfyxkuxq + '\'' +
                "kuanxqts='" + kuanxqts + '\'' +
                "kxqjjrgz='" + kxqjjrgz + '\'' +
                "tqhkhxfs='" + tqhkhxfs + '\'' +
                '}';
    }
}  
