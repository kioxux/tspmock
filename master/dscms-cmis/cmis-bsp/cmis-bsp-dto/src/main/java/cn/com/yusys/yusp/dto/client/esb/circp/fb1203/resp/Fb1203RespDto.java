package cn.com.yusys.yusp.dto.client.esb.circp.fb1203.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：质押物金额覆盖校验
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fb1203RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "OLS_TRAN_NO")
    private String OLS_TRAN_NO;//系统交易流水
    @JsonProperty(value = "OLS_DATE")
    private String OLS_DATE;//系统交易日期
    @JsonProperty(value = "PASS_FLAG")
    private String PASS_FLAG;//校验是否通过

    public String getOLS_TRAN_NO() {
        return OLS_TRAN_NO;
    }

    public void setOLS_TRAN_NO(String OLS_TRAN_NO) {
        this.OLS_TRAN_NO = OLS_TRAN_NO;
    }

    public String getOLS_DATE() {
        return OLS_DATE;
    }

    public void setOLS_DATE(String OLS_DATE) {
        this.OLS_DATE = OLS_DATE;
    }

    public String getPASS_FLAG() {
        return PASS_FLAG;
    }

    public void setPASS_FLAG(String PASS_FLAG) {
        this.PASS_FLAG = PASS_FLAG;
    }

    @Override
    public String toString() {
        return "Fb1203RespDto{" +
                "OLS_TRAN_NO='" + OLS_TRAN_NO + '\'' +
                ", OLS_DATE='" + OLS_DATE + '\'' +
                ", PASS_FLAG='" + PASS_FLAG + '\'' +
                '}';
    }
}
