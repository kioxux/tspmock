package cn.com.yusys.yusp.dto.client.esb.core.ln3063;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：资产转让处理
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstzrjj implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "jiejuhao")
    private String jiejuhao;//借据号
    @JsonProperty(value = "sddjjine")
    private BigDecimal sddjjine;//收到对价金额

    public String getJiejuhao() {
        return jiejuhao;
    }

    public void setJiejuhao(String jiejuhao) {
        this.jiejuhao = jiejuhao;
    }

    public BigDecimal getSddjjine() {
        return sddjjine;
    }

    public void setSddjjine(BigDecimal sddjjine) {
        this.sddjjine = sddjjine;
    }

    @Override
    public String toString() {
        return "Lstzrjj{" +
                "jiejuhao='" + jiejuhao + '\'' +
                ", sddjjine=" + sddjjine +
                '}';
    }
}
