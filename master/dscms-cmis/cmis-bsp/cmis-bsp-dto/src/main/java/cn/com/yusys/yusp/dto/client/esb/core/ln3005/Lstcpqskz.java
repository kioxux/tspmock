package cn.com.yusys.yusp.dto.client.esb.core.ln3005;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：产品缺省控制对象
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Lstcpqskz implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cpjbiaom")
    private String cpjbiaom;//产品级表名
    @JsonProperty(value = "chanpjzd")
    private String chanpjzd;//产品级字段
    @JsonProperty(value = "chpjzdms")
    private String chpjzdms;//产品级字段描述
    @JsonProperty(value = "zhjbiaom")
    private String zhjbiaom;//账户级表名
    @JsonProperty(value = "zhhujizd")
    private String zhhujizd;//账户级字段
    @JsonProperty(value = "zhhjzdms")
    private String zhhjzdms;//账户级字段描述
    @JsonProperty(value = "zhhuxggz")
    private String zhhuxggz;//账户修改规则
    @JsonProperty(value = "zhhuxdfw")
    private String zhhuxdfw;//账户级字段范围值

    public String getCpjbiaom() {
        return cpjbiaom;
    }

    public void setCpjbiaom(String cpjbiaom) {
        this.cpjbiaom = cpjbiaom;
    }

    public String getChanpjzd() {
        return chanpjzd;
    }

    public void setChanpjzd(String chanpjzd) {
        this.chanpjzd = chanpjzd;
    }

    public String getChpjzdms() {
        return chpjzdms;
    }

    public void setChpjzdms(String chpjzdms) {
        this.chpjzdms = chpjzdms;
    }

    public String getZhjbiaom() {
        return zhjbiaom;
    }

    public void setZhjbiaom(String zhjbiaom) {
        this.zhjbiaom = zhjbiaom;
    }

    public String getZhhujizd() {
        return zhhujizd;
    }

    public void setZhhujizd(String zhhujizd) {
        this.zhhujizd = zhhujizd;
    }

    public String getZhhjzdms() {
        return zhhjzdms;
    }

    public void setZhhjzdms(String zhhjzdms) {
        this.zhhjzdms = zhhjzdms;
    }

    public String getZhhuxggz() {
        return zhhuxggz;
    }

    public void setZhhuxggz(String zhhuxggz) {
        this.zhhuxggz = zhhuxggz;
    }

    public String getZhhuxdfw() {
        return zhhuxdfw;
    }

    public void setZhhuxdfw(String zhhuxdfw) {
        this.zhhuxdfw = zhhuxdfw;
    }

    @Override
    public String toString() {
        return "Lstcpqskz{" +
                "cpjbiaom='" + cpjbiaom + '\'' +
                "chanpjzd='" + chanpjzd + '\'' +
                "chpjzdms='" + chpjzdms + '\'' +
                "zhjbiaom='" + zhjbiaom + '\'' +
                "zhhujizd='" + zhhujizd + '\'' +
                "zhhjzdms='" + zhhjzdms + '\'' +
                "zhhuxggz='" + zhhuxggz + '\'' +
                "zhhuxdfw='" + zhhuxdfw + '\'' +
                '}';
    }
}  
