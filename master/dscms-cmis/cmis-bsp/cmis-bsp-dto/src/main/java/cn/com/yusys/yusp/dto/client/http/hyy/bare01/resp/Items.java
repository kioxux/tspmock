package cn.com.yusys.yusp.dto.client.http.hyy.bare01.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 */
@JsonPropertyOrder(alphabetic = true)
public class Items implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "register_outline")
    private String register_outline;//登记簿概要信息
    @JsonProperty(value = "houses")
    private String houses;//房产信息
    @JsonProperty(value = "affiliated_facilities")
    private String affiliated_facilities;//附属设施信息
    @JsonProperty(value = "lands")
    private String lands;//土地信息
    @JsonProperty(value = "mortgages")
    private String mortgages;//抵押信息
    @JsonProperty(value = "seizures")
    private String seizures;//查封信息
    @JsonProperty(value = "owners")
    private String owners;//权利人信息

    public String getRegister_outline() {
        return register_outline;
    }

    public void setRegister_outline(String register_outline) {
        this.register_outline = register_outline;
    }

    public String getHouses() {
        return houses;
    }

    public void setHouses(String houses) {
        this.houses = houses;
    }

    public String getAffiliated_facilities() {
        return affiliated_facilities;
    }

    public void setAffiliated_facilities(String affiliated_facilities) {
        this.affiliated_facilities = affiliated_facilities;
    }

    public String getLands() {
        return lands;
    }

    public void setLands(String lands) {
        this.lands = lands;
    }

    public String getMortgages() {
        return mortgages;
    }

    public void setMortgages(String mortgages) {
        this.mortgages = mortgages;
    }

    public String getSeizures() {
        return seizures;
    }

    public void setSeizures(String seizures) {
        this.seizures = seizures;
    }

    public String getOwners() {
        return owners;
    }

    public void setOwners(String owners) {
        this.owners = owners;
    }

    @Override
    public String toString() {
        return "Items{" +
                "register_outline='" + register_outline + '\'' +
                ", houses='" + houses + '\'' +
                ", affiliated_facilities='" + affiliated_facilities + '\'' +
                ", lands='" + lands + '\'' +
                ", mortgages='" + mortgages + '\'' +
                ", seizures='" + seizures + '\'' +
                ", owners='" + owners + '\'' +
                '}';
    }
}
