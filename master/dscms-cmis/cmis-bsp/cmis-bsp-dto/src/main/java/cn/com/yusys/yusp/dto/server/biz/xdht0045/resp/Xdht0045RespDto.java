package cn.com.yusys.yusp.dto.server.biz.xdht0045.resp;

import cn.com.yusys.yusp.dto.server.TradeServerRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Xdht0045RespDto extends TradeServerRespDto implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.server.biz.xdht0045.resp.Data data;

    public cn.com.yusys.yusp.dto.server.biz.xdht0045.resp.Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Xdht0045RespDto{" +
                "data='" + data + '\'' +
                '}';
    }
}
