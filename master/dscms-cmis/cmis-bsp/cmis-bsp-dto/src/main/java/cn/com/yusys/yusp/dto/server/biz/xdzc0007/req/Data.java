package cn.com.yusys.yusp.dto.server.biz.xdzc0007.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：资产池出池校验
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;// 客户编号
    @JsonProperty(value = "opType")
    private String opType;//操作类型
    @JsonProperty(value = "resn")
    private String resn;//出池原因
    @JsonProperty(value = "serno")
    private String serno;//出池批次号
    @JsonProperty(value = "list")
    private java.util.List<List> list;

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getOpType() {
        return opType;
    }

    public void setOpType(String opType) {
        this.opType = opType;
    }

    public String getResn() {
        return resn;
    }

    public void setResn(String resn) {
        this.resn = resn;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", opType='" + opType + '\'' +
                ", resn='" + resn + '\'' +
                ", serno='" + serno + '\'' +
                ", list=" + list +
                '}';
    }
}
