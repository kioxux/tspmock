package cn.com.yusys.yusp.dto.server.biz.xdzc0004.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：客户资产清单查询接口
 *
 * @author lihh
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "assetType")
    private String assetType;//资产类型
    @JsonProperty(value = "assetStartDate")
    private String assetStartDate;//资产到期日起
    @JsonProperty(value = "assetEndDate")
    private String assetEndDate;//资产到期日止
    @JsonProperty(value = "assetCeiling")
    private BigDecimal assetCeiling;//资产价值上限
    @JsonProperty(value = "assetFloor")
    private BigDecimal assetFloor;//资产价值下限
    @JsonProperty(value = "isAllQuery")
    private String isAllQuery;//是否查询全量数据
    @JsonProperty(value = "page")
    private int page;//页数
    @JsonProperty(value = "size")
    private int size;//单页大小
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getAssetStartDate() {
        return assetStartDate;
    }

    public void setAssetStartDate(String assetStartDate) {
        this.assetStartDate = assetStartDate;
    }

    public String getAssetEndDate() {
        return assetEndDate;
    }

    public void setAssetEndDate(String assetEndDate) {
        this.assetEndDate = assetEndDate;
    }

    public BigDecimal getAssetCeiling() {
        return assetCeiling;
    }

    public void setAssetCeiling(BigDecimal assetCeiling) {
        this.assetCeiling = assetCeiling;
    }

    public BigDecimal getAssetFloor() {
        return assetFloor;
    }

    public void setAssetFloor(BigDecimal assetFloor) {
        this.assetFloor = assetFloor;
    }

    public String getIsAllQuery() {
        return isAllQuery;
    }

    public void setIsAllQuery(String isAllQuery) {
        this.isAllQuery = isAllQuery;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    @Override
    public String toString() {
        return "Xdzc0004DataReqDto{" +
                "cusId='" + cusId + '\'' +
                ", assetType='" + assetType + '\'' +
                ", assetStartDate='" + assetStartDate + '\'' +
                ", assetEndDate='" + assetEndDate + '\'' +
                ", assetCeiling=" + assetCeiling +
                ", assetFloor=" + assetFloor +
                ", isAllQuery='" + isAllQuery + '\'' +
                ", page=" + page +
                ", size=" + size +
                ", queryType='" + queryType + '\'' +
                '}';
    }
}
