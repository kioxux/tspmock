package cn.com.yusys.yusp.dto.client.esb.ypxt.coninf;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.math.BigDecimal;

@JsonPropertyOrder(alphabetic = true)
public class ConinfListInfo {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ywxtyp")
    private String ywxtyp;//	业务系统	是	varchar(20)	是	01-信贷；02-小微	ywxtyp
    @JsonProperty(value = "htbhyp")
    private String htbhyp;//	合同编号	是	varchar(50)	是		htbhyp
    @JsonProperty(value = "htlxyp")
    private String htlxyp;//	合同类型	是	varchar(5)	是	010-一般担保；020-最高额担保	htlxyp
    @JsonProperty(value = "sernoy")
    private String sernoy;//	核心担保编号	是	varchar(40)	否	信贷生成YP编号	sernoy
    @JsonProperty(value = "dbfsyp")
    private String dbfsyp;//	担保方式	是	varchar(5)	是	BZ-保证；XY-信用；DY-抵押；ZY-质押	dbfsyp
    @JsonProperty(value = "htqsrq")
    private String htqsrq;//	合同签订起始日期	是	varchar(10)	是		htqsrq
    @JsonProperty(value = "htjsrq")
    private String htjsrq;//	合同签订到期日期	是	varchar(10)	是		htjsrq
    @JsonProperty(value = "dbhtzt")
    private String dbhtzt;//	担保合同状态	是	varchar(5)	是	010-未签合同；020-已签合同；030-已失效	dbhtzt
    @JsonProperty(value = "dbhtje")
    private BigDecimal dbhtje;//	担保合同金额	是	number(16,2)	是		dbhtje
    @JsonProperty(value = "bzypxt")
    private String bzypxt;//	币种	是	varchar(10)	是		bzypxt
    @JsonProperty(value = "dbrzye")
    private String dbrzye;//	担保合同对应融资余额	是	number(16,2)	是		dbrzye
    @JsonProperty(value = "khghjl")
    private String khghjl;//	管户客户经理	是	varchar(20)	是		khghjl
    @JsonProperty(value = "khghjg")
    private String khghjg;//	管理机构	是	varchar(20)	是		khghjg

    public String getYwxtyp() {
        return ywxtyp;
    }

    public void setYwxtyp(String ywxtyp) {
        this.ywxtyp = ywxtyp;
    }

    public String getHtbhyp() {
        return htbhyp;
    }

    public void setHtbhyp(String htbhyp) {
        this.htbhyp = htbhyp;
    }

    public String getHtlxyp() {
        return htlxyp;
    }

    public void setHtlxyp(String htlxyp) {
        this.htlxyp = htlxyp;
    }

    public String getSernoy() {
        return sernoy;
    }

    public void setSernoy(String sernoy) {
        this.sernoy = sernoy;
    }

    public String getDbfsyp() {
        return dbfsyp;
    }

    public void setDbfsyp(String dbfsyp) {
        this.dbfsyp = dbfsyp;
    }

    public String getHtqsrq() {
        return htqsrq;
    }

    public void setHtqsrq(String htqsrq) {
        this.htqsrq = htqsrq;
    }

    public String getHtjsrq() {
        return htjsrq;
    }

    public void setHtjsrq(String htjsrq) {
        this.htjsrq = htjsrq;
    }

    public String getDbhtzt() {
        return dbhtzt;
    }

    public void setDbhtzt(String dbhtzt) {
        this.dbhtzt = dbhtzt;
    }

    public BigDecimal getDbhtje() {
        return dbhtje;
    }
    public void setDbhtje(BigDecimal dbhtje) {
        this.dbhtje = dbhtje;
    }

    public String getBzypxt() {
        return bzypxt;
    }

    public void setBzypxt(String bzypxt) {
        this.bzypxt = bzypxt;
    }

    public String getDbrzye() {
        return dbrzye;
    }

    public void setDbrzye(String dbrzye) {
        this.dbrzye = dbrzye;
    }

    public String getKhghjl() {
        return khghjl;
    }

    public void setKhghjl(String khghjl) {
        this.khghjl = khghjl;
    }

    public String getKhghjg() {
        return khghjg;
    }

    public void setKhghjg(String khghjg) {
        this.khghjg = khghjg;
    }


    @Override
    public String toString() {
        return "ConinfReqDto{" +
                "ywxtyp='" + ywxtyp + '\'' +
                ", htbhyp='" + htbhyp + '\'' +
                ", htlxyp='" + htlxyp + '\'' +
                ", sernoy='" + sernoy + '\'' +
                ", dbfsyp='" + dbfsyp + '\'' +
                ", htqsrq='" + htqsrq + '\'' +
                ", htjsrq='" + htjsrq + '\'' +
                ", dbhtzt='" + dbhtzt + '\'' +
                ", dbhtje='" + dbhtje + '\'' +
                ", bzypxt='" + bzypxt + '\'' +
                ", dbrzye='" + dbrzye + '\'' +
                ", khghjl='" + khghjl + '\'' +
                ", khghjg='" + khghjg + '\'' +
                '}';
    }

}
