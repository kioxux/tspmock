package cn.com.yusys.yusp.dto.server.biz.xdcz0025.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：未中标业务注销
 * @Author xs
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billno")
    private String billno;//借据编号
    @JsonProperty(value = "contno")
    private String contno;//保函协议编号

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    @Override
    public String toString() {
        return "Xdcz0025DataReqDto{" +
                "billno='" + billno + '\'' +
                ", contno='" + contno + '\'' +
                '}';
    }
}
