package cn.com.yusys.yusp.dto.server.biz.xdtz0013.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * <br>
 * 0.2ZRC:2021/5/24 9:48:<br>
 *
 * @author ZRC
 * @version 0.1
 * @date 2021/5/24 9:48
 * @since 2021/5/24 9:48
 */
@JsonPropertyOrder(alphabetic = true)
public class BillList implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "billBal")
    private BigDecimal billBal;//借据余额
    @JsonProperty(value = "lastSettlDate")
    private String lastSettlDate;//最近一笔借据结清日期
    @JsonProperty(value = "loanType")
    private String loanType;//贷款类型
    @JsonProperty(value = "managerId")
    private String managerId;//归属客户经理工号
    @JsonProperty(value = "managerName")
    private String managerName;//归属客户经理姓名
    @JsonProperty(value = "orgNo")
    private String orgNo;//所在机构编号
    @JsonProperty(value = "orgName")
    private String orgName;//所在机构名称
    @JsonProperty(value = "teamType")
    private String teamType;//直营团队类型
    @JsonProperty(value = "surveyType")
    private String surveyType;//调查类型

    public BigDecimal getBillBal() {
        return billBal;
    }

    public void setBillBal(BigDecimal billBal) {
        this.billBal = billBal;
    }

    public String getLastSettlDate() {
        return lastSettlDate;
    }

    public void setLastSettlDate(String lastSettlDate) {
        this.lastSettlDate = lastSettlDate;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getTeamType() {
        return teamType;
    }

    public void setTeamType(String teamType) {
        this.teamType = teamType;
    }

    public String getSurveyType() {
        return surveyType;
    }

    public void setSurveyType(String surveyType) {
        this.surveyType = surveyType;
    }

    @Override
    public String toString() {
        return "Xddh0013RespDto{" +
                "billBal='" + billBal + '\'' +
                "lastSettlDate='" + lastSettlDate + '\'' +
                "loanType='" + loanType + '\'' +
                "managerId='" + managerId + '\'' +
                "managerName='" + managerName + '\'' +
                "orgNo='" + orgNo + '\'' +
                "orgName='" + orgName + '\'' +
                "teamType='" + teamType + '\'' +
                "surveyType='" + surveyType + '\'' +
                '}';
    }
}
