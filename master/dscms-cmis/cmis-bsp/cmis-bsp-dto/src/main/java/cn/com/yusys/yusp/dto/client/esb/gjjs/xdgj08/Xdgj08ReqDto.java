package cn.com.yusys.yusp.dto.client.esb.gjjs.xdgj08;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：信贷还款信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Xdgj08ReqDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "type")
    private String type;//业务类型
    @JsonProperty(value = "op_flag")
    private String op_flag;//操作类型
    @JsonProperty(value = "serno")
    private String serno;//交易流水号
    @JsonProperty(value = "loan_no")
    private String loan_no;//借据号
    @JsonProperty(value = "cust_name")
    private String cust_name;//客户名称
    @JsonProperty(value = "cus_id")
    private String cus_id;//客户号
    @JsonProperty(value = "cont_no")
    private String cont_no;//合同号
    @JsonProperty(value = "loan_ccy")
    private String loan_ccy;//币种
    @JsonProperty(value = "loan_os_prcp")
    private BigDecimal loan_os_prcp;//借据余额
    @JsonProperty(value = "loan_int_rate")
    private String loan_int_rate;//贷款执行利率年
    @JsonProperty(value = "loan_base_rate")
    private BigDecimal loan_base_rate;//借据基准利率年
    @JsonProperty(value = "int_adj_pct")
    private String int_adj_pct;//浮动比率
    @JsonProperty(value = "int_start_dt")
    private String int_start_dt;//发生日期
    @JsonProperty(value = "last_due_dt")
    private BigDecimal last_due_dt;//到期日期
    @JsonProperty(value = "setl_mode")
    private BigDecimal setl_mode;//还款模式
    @JsonProperty(value = "loan_pay_type")
    private String loan_pay_type;//贷款收回方式
    @JsonProperty(value = "repay_amount")
    private String repay_amount;//还款金额
    @JsonProperty(value = "paym_acct_no")
    private String paym_acct_no;//还款账号
    @JsonProperty(value = "acct_id_type")
    private String acct_id_type;//是否第三方还款
    @JsonProperty(value = "acct_id_no")
    private String acct_id_no;//第三方还款账号
    @JsonProperty(value = "paym_acct_name")
    private String paym_acct_name;//账户名称

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOp_flag() {
        return op_flag;
    }

    public void setOp_flag(String op_flag) {
        this.op_flag = op_flag;
    }

    public String getSerno() {
        return serno;
    }

    public void setSerno(String serno) {
        this.serno = serno;
    }

    public String getLoan_no() {
        return loan_no;
    }

    public void setLoan_no(String loan_no) {
        this.loan_no = loan_no;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCus_id() {
        return cus_id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public String getCont_no() {
        return cont_no;
    }

    public void setCont_no(String cont_no) {
        this.cont_no = cont_no;
    }

    public String getLoan_ccy() {
        return loan_ccy;
    }

    public void setLoan_ccy(String loan_ccy) {
        this.loan_ccy = loan_ccy;
    }

    public BigDecimal getLoan_os_prcp() {
        return loan_os_prcp;
    }

    public void setLoan_os_prcp(BigDecimal loan_os_prcp) {
        this.loan_os_prcp = loan_os_prcp;
    }

    public String getLoan_int_rate() {
        return loan_int_rate;
    }

    public void setLoan_int_rate(String loan_int_rate) {
        this.loan_int_rate = loan_int_rate;
    }

    public BigDecimal getLoan_base_rate() {
        return loan_base_rate;
    }

    public void setLoan_base_rate(BigDecimal loan_base_rate) {
        this.loan_base_rate = loan_base_rate;
    }

    public String getInt_adj_pct() {
        return int_adj_pct;
    }

    public void setInt_adj_pct(String int_adj_pct) {
        this.int_adj_pct = int_adj_pct;
    }

    public String getInt_start_dt() {
        return int_start_dt;
    }

    public void setInt_start_dt(String int_start_dt) {
        this.int_start_dt = int_start_dt;
    }

    public BigDecimal getLast_due_dt() {
        return last_due_dt;
    }

    public void setLast_due_dt(BigDecimal last_due_dt) {
        this.last_due_dt = last_due_dt;
    }

    public BigDecimal getSetl_mode() {
        return setl_mode;
    }

    public void setSetl_mode(BigDecimal setl_mode) {
        this.setl_mode = setl_mode;
    }

    public String getLoan_pay_type() {
        return loan_pay_type;
    }

    public void setLoan_pay_type(String loan_pay_type) {
        this.loan_pay_type = loan_pay_type;
    }

    public String getRepay_amount() {
        return repay_amount;
    }

    public void setRepay_amount(String repay_amount) {
        this.repay_amount = repay_amount;
    }

    public String getPaym_acct_no() {
        return paym_acct_no;
    }

    public void setPaym_acct_no(String paym_acct_no) {
        this.paym_acct_no = paym_acct_no;
    }

    public String getAcct_id_type() {
        return acct_id_type;
    }

    public void setAcct_id_type(String acct_id_type) {
        this.acct_id_type = acct_id_type;
    }

    public String getAcct_id_no() {
        return acct_id_no;
    }

    public void setAcct_id_no(String acct_id_no) {
        this.acct_id_no = acct_id_no;
    }

    public String getPaym_acct_name() {
        return paym_acct_name;
    }

    public void setPaym_acct_name(String paym_acct_name) {
        this.paym_acct_name = paym_acct_name;
    }

    @Override
    public String toString() {
        return "Xdgj08ReqDto{" +
                "type='" + type + '\'' +
                "op_flag='" + op_flag + '\'' +
                "serno='" + serno + '\'' +
                "loan_no='" + loan_no + '\'' +
                "cust_name='" + cust_name + '\'' +
                "cus_id='" + cus_id + '\'' +
                "cont_no='" + cont_no + '\'' +
                "loan_ccy='" + loan_ccy + '\'' +
                "loan_os_prcp='" + loan_os_prcp + '\'' +
                "loan_int_rate='" + loan_int_rate + '\'' +
                "loan_base_rate='" + loan_base_rate + '\'' +
                "int_adj_pct='" + int_adj_pct + '\'' +
                "int_start_dt='" + int_start_dt + '\'' +
                "last_due_dt='" + last_due_dt + '\'' +
                "setl_mode='" + setl_mode + '\'' +
                "loan_pay_type='" + loan_pay_type + '\'' +
                "repay_amount='" + repay_amount + '\'' +
                "paym_acct_no='" + paym_acct_no + '\'' +
                "acct_id_type='" + acct_id_type + '\'' +
                "acct_id_no='" + acct_id_no + '\'' +
                "paym_acct_name='" + paym_acct_name + '\'' +
                '}';
    }
}  
