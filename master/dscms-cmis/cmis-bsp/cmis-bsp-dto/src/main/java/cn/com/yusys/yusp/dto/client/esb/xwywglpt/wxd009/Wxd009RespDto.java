package cn.com.yusys.yusp.dto.client.esb.xwywglpt.wxd009;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：信贷系统请求小V平台推送合同信息接口
 */
@JsonPropertyOrder(alphabetic = true)
public class Wxd009RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "code")
    private String code;//返回码
    @JsonProperty(value = "message")
    private String message;//返回信息

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Wxd009RespDto{" +
                "code='" + code + '\'' +
                "message='" + message + '\'' +
                '}';
    }
}
