package cn.com.yusys.yusp.dto.client.esb.core.da3322.resp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/7 19:25
 * @since 2021/6/7 19:25
 */
public class LstDzsfmx {

    @JsonProperty(value = "dzzcbhao")
    private String dzzcbhao;//抵债资产编号
    @JsonProperty(value = "dzzcminc")
    private String dzzcminc;//抵债资产名称
    @JsonProperty(value = "shoufjee")
    private BigDecimal shoufjee;//收费金额
    @JsonProperty(value = "sfrzhzhh")
    private String sfrzhzhh;//收费入账账号
    @JsonProperty(value = "sfrzhzxh")
    private String sfrzhzxh;//收费入账账号子序号
    @JsonProperty(value = "fufeizhh")
    private String fufeizhh;//付费账号
    @JsonProperty(value = "ffzhhzxh")
    private String ffzhhzxh;//付费账号子序号
    @JsonProperty(value = "beizhuxx")
    private String beizhuxx;//备注
    @JsonProperty(value = "dzzcfylx")
    private String dzzcfylx;//抵债资产费用类型
    @JsonProperty(value = "zijnlaiy")
    private String zijnlaiy;//资金来源
    @JsonProperty(value = "jiaoyirq")
    private String jiaoyirq;//交易日期
    @JsonProperty(value = "jiaoyils")
    private String jiaoyils;//交易流水
    @JsonProperty(value = "farendma")
    private String farendma;//法人代码
    @JsonProperty(value = "weihguiy")
    private String weihguiy;//维护柜员
    @JsonProperty(value = "weihjigo")
    private String weihjigo;//维护机构
    @JsonProperty(value = "weihriqi")
    private String weihriqi;//维护日期
    @JsonProperty(value = "weihshij")
    private String weihshij;//维护时间
    @JsonProperty(value = "shijchuo")
    private Integer shijchuo;//时间戳
    @JsonProperty(value = "jiluztai")
    private String jiluztai;//记录状态

    public String getDzzcbhao() {
        return dzzcbhao;
    }

    public void setDzzcbhao(String dzzcbhao) {
        this.dzzcbhao = dzzcbhao;
    }

    public String getDzzcminc() {
        return dzzcminc;
    }

    public void setDzzcminc(String dzzcminc) {
        this.dzzcminc = dzzcminc;
    }

    public BigDecimal getShoufjee() {
        return shoufjee;
    }

    public void setShoufjee(BigDecimal shoufjee) {
        this.shoufjee = shoufjee;
    }

    public String getSfrzhzhh() {
        return sfrzhzhh;
    }

    public void setSfrzhzhh(String sfrzhzhh) {
        this.sfrzhzhh = sfrzhzhh;
    }

    public String getSfrzhzxh() {
        return sfrzhzxh;
    }

    public void setSfrzhzxh(String sfrzhzxh) {
        this.sfrzhzxh = sfrzhzxh;
    }

    public String getFufeizhh() {
        return fufeizhh;
    }

    public void setFufeizhh(String fufeizhh) {
        this.fufeizhh = fufeizhh;
    }

    public String getFfzhhzxh() {
        return ffzhhzxh;
    }

    public void setFfzhhzxh(String ffzhhzxh) {
        this.ffzhhzxh = ffzhhzxh;
    }

    public String getBeizhuxx() {
        return beizhuxx;
    }

    public void setBeizhuxx(String beizhuxx) {
        this.beizhuxx = beizhuxx;
    }

    public String getDzzcfylx() {
        return dzzcfylx;
    }

    public void setDzzcfylx(String dzzcfylx) {
        this.dzzcfylx = dzzcfylx;
    }

    public String getZijnlaiy() {
        return zijnlaiy;
    }

    public void setZijnlaiy(String zijnlaiy) {
        this.zijnlaiy = zijnlaiy;
    }

    public String getJiaoyirq() {
        return jiaoyirq;
    }

    public void setJiaoyirq(String jiaoyirq) {
        this.jiaoyirq = jiaoyirq;
    }

    public String getJiaoyils() {
        return jiaoyils;
    }

    public void setJiaoyils(String jiaoyils) {
        this.jiaoyils = jiaoyils;
    }

    public String getFarendma() {
        return farendma;
    }

    public void setFarendma(String farendma) {
        this.farendma = farendma;
    }

    public String getWeihguiy() {
        return weihguiy;
    }

    public void setWeihguiy(String weihguiy) {
        this.weihguiy = weihguiy;
    }

    public String getWeihjigo() {
        return weihjigo;
    }

    public void setWeihjigo(String weihjigo) {
        this.weihjigo = weihjigo;
    }

    public String getWeihriqi() {
        return weihriqi;
    }

    public void setWeihriqi(String weihriqi) {
        this.weihriqi = weihriqi;
    }

    public String getWeihshij() {
        return weihshij;
    }

    public void setWeihshij(String weihshij) {
        this.weihshij = weihshij;
    }

    public Integer getShijchuo() {
        return shijchuo;
    }

    public void setShijchuo(Integer shijchuo) {
        this.shijchuo = shijchuo;
    }

    public String getJiluztai() {
        return jiluztai;
    }

    public void setJiluztai(String jiluztai) {
        this.jiluztai = jiluztai;
    }

    @Override
    public String toString() {
        return "LstDzsfmx{" +
                "dzzcbhao='" + dzzcbhao + '\'' +
                ", dzzcminc='" + dzzcminc + '\'' +
                ", shoufjee=" + shoufjee +
                ", sfrzhzhh='" + sfrzhzhh + '\'' +
                ", sfrzhzxh='" + sfrzhzxh + '\'' +
                ", fufeizhh='" + fufeizhh + '\'' +
                ", ffzhhzxh='" + ffzhhzxh + '\'' +
                ", beizhuxx='" + beizhuxx + '\'' +
                ", dzzcfylx='" + dzzcfylx + '\'' +
                ", zijnlaiy='" + zijnlaiy + '\'' +
                ", jiaoyirq='" + jiaoyirq + '\'' +
                ", jiaoyils='" + jiaoyils + '\'' +
                ", farendma='" + farendma + '\'' +
                ", weihguiy='" + weihguiy + '\'' +
                ", weihjigo='" + weihjigo + '\'' +
                ", weihriqi='" + weihriqi + '\'' +
                ", weihshij='" + weihshij + '\'' +
                ", shijchuo=" + shijchuo +
                ", jiluztai='" + jiluztai + '\'' +
                '}';
    }
}
