package cn.com.yusys.yusp.dto.client.esb.znwdspxt.znsp07.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/9/8 21:57
 * @since 2021/9/8 21:57
 */
@JsonPropertyOrder(alphabetic = true)
public class Guar_list implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "guar_serno")
    private String guar_serno;//担保人流水号
    @JsonProperty(value = "guar_name")
    private String guar_name;//担保人名称
    @JsonProperty(value = "guar_type")
    private String guar_type;//担保人类型

    public String getGuar_serno() {
        return guar_serno;
    }

    public void setGuar_serno(String guar_serno) {
        this.guar_serno = guar_serno;
    }

    public String getGuar_name() {
        return guar_name;
    }

    public void setGuar_name(String guar_name) {
        this.guar_name = guar_name;
    }

    public String getGuar_type() {
        return guar_type;
    }

    public void setGuar_type(String guar_type) {
        this.guar_type = guar_type;
    }

    @Override
    public String toString() {
        return "Guar_list{" +
                "guar_serno='" + guar_serno + '\'' +
                ", guar_name='" + guar_name + '\'' +
                ", guar_type='" + guar_type + '\'' +
                '}';
    }
}
