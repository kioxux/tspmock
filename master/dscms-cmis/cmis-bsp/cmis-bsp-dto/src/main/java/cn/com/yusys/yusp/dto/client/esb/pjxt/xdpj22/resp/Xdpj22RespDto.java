package cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp;

import cn.com.yusys.yusp.dto.client.esb.pjxt.xdpj22.resp.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic = true)
public class Xdpj22RespDto {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;//list

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Xdpj22RespDto{" +
                "list=" + list +
                '}';
    }
}
