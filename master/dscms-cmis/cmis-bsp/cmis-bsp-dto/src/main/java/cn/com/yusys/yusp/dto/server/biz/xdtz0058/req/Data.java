package cn.com.yusys.yusp.dto.server.biz.xdtz0058.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：通过借据号查询借据信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "queryType")
    private String queryType;//查询条件
    @JsonProperty(value = "billNo")
    private String billNo;//借据编号
    @JsonProperty(value = "certNo")
    private String certNo;//客户证件号

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCertNo() {
        return certNo;
    }

    public void setCertNo(String certNo) {
        this.certNo = certNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "queryType='" + queryType + '\'' +
                ", billNo='" + billNo + '\'' +
                ", certNo='" + certNo + '\'' +
                '}';
    }
}
