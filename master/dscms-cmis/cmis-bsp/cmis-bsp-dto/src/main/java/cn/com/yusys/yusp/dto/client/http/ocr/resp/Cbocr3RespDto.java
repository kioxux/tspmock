package cn.com.yusys.yusp.dto.client.http.ocr.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：报表核对统计总览列表查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Cbocr3RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "success")
    private String success;//是否请求成功
    @JsonProperty(value = "code")
    private String code;//业务编码
    @JsonProperty(value = "message")
    private String message;//消息
    @JsonProperty(value = "data")
    private cn.com.yusys.yusp.dto.client.http.ocr.resp.Data data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Cbocr3RespDto{" +
                "success='" + success + '\'' +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
