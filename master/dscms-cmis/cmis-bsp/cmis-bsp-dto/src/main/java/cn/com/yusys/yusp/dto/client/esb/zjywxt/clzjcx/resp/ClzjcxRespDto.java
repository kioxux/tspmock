package cn.com.yusys.yusp.dto.client.esb.zjywxt.clzjcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 响应Dto：未备注
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ClzjcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "savvou")
    private String savvou;//监管协议号（缴款凭证编号）
    @JsonProperty(value = "contno")
    private String contno;//合同编号
    @JsonProperty(value = "acctno")
    private String acctno;//监管账号
    @JsonProperty(value = "acctna")
    private String acctna;//监管账户名称
    @JsonProperty(value = "totamt")
    private BigDecimal totamt;//监管总额
    @JsonProperty(value = "payamt")
    private BigDecimal payamt;//已交金额
    @JsonProperty(value = "byidna")
    private String byidna;//买方姓名（买方人名称）
    @JsonProperty(value = "byidno")
    private String byidno;//买方人证件号码

    public String getSavvou() {
        return savvou;
    }

    public void setSavvou(String savvou) {
        this.savvou = savvou;
    }

    public String getContno() {
        return contno;
    }

    public void setContno(String contno) {
        this.contno = contno;
    }

    public String getAcctno() {
        return acctno;
    }

    public void setAcctno(String acctno) {
        this.acctno = acctno;
    }

    public String getAcctna() {
        return acctna;
    }

    public void setAcctna(String acctna) {
        this.acctna = acctna;
    }

    public BigDecimal getTotamt() {
        return totamt;
    }

    public void setTotamt(BigDecimal totamt) {
        this.totamt = totamt;
    }

    public BigDecimal getPayamt() {
        return payamt;
    }

    public void setPayamt(BigDecimal payamt) {
        this.payamt = payamt;
    }

    public String getByidna() {
        return byidna;
    }

    public void setByidna(String byidna) {
        this.byidna = byidna;
    }

    public String getByidno() {
        return byidno;
    }

    public void setByidno(String byidno) {
        this.byidno = byidno;
    }

    @Override
    public String toString() {
        return "ClzjcxRespDto{" +
                "savvou='" + savvou + '\'' +
                "contno='" + contno + '\'' +
                "acctno='" + acctno + '\'' +
                "acctna='" + acctna + '\'' +
                "totamt='" + totamt + '\'' +
                "payamt='" + payamt + '\'' +
                "byidna='" + byidna + '\'' +
                "byidno='" + byidno + '\'' +
                '}';
    }
}  
