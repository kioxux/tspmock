package cn.com.yusys.yusp.dto.client.esb.fxyjxt.djhhmd;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户风险预警等级与是否黑灰名单
 */
@JsonPropertyOrder(alphabetic = true)
public class DjhhmdRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "custRiskStatus")
    private String custRiskStatus;//风险预警等级
    @JsonProperty(value = "ifBlack")
    private String ifBlack;//是否黑名单
    @JsonProperty(value = "ifGrey")
    private String ifGrey;//是否灰名单

    public String getCustRiskStatus() {
        return custRiskStatus;
    }

    public void setCustRiskStatus(String custRiskStatus) {
        this.custRiskStatus = custRiskStatus;
    }

    public String getIfBlack() {
        return ifBlack;
    }

    public void setIfBlack(String ifBlack) {
        this.ifBlack = ifBlack;
    }

    public String getIfGrey() {
        return ifGrey;
    }

    public void setIfGrey(String ifGrey) {
        this.ifGrey = ifGrey;
    }

    @Override
    public String toString() {
        return "DjhhmdRespDto{" +
                "custRiskStatus='" + custRiskStatus + '\'' +
                ", ifBlack='" + ifBlack + '\'' +
                ", ifGrey='" + ifGrey + '\'' +
                '}';
    }
}
