package cn.com.yusys.yusp.dto.server.biz.xdcz0017.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：支用(微信小程序)
 *
 * @author xull
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "loanSour")
    private String loanSour;//贷款来源
    @JsonProperty(value = "imageSer")
    private String imageSer;//影像流水
    @JsonProperty(value = "billNo")
    private String billNo;//借据号

    public String getLoanSour() {
        return loanSour;
    }

    public void setLoanSour(String loanSour) {
        this.loanSour = loanSour;
    }

    public String getImageSer() {
        return imageSer;
    }

    public void setImageSer(String imageSer) {
        this.imageSer = imageSer;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "loanSour='" + loanSour + '\'' +
                ", imageSer='" + imageSer + '\'' +
                ", billNo='" + billNo + '\'' +
                '}';
    }
}
