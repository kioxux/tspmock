package cn.com.yusys.yusp.dto.server.biz.xdzc0016.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/6/8 19:35
 * @since 2021/6/8 19:35
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "cusId")
    private String cusId;//客户号
    @JsonProperty(value = "queryType")
    private String queryType;//查询类型
    @JsonProperty(value = "tContNo")
    private String tContNo;//合同编号
    @JsonProperty(value = "tContCnName")
    private String tContCnName;//合同名称
    @JsonProperty(value = "contStartDateAfter")
    private String contStartDateAfter;//合同起始日开始
    @JsonProperty(value = "contStartDateBerfore")
    private String contStartDateBerfore;//合同起始日截止
    @JsonProperty(value = "contEndDateAfter")
    private String contEndDateAfter;//合同到期日开始
    @JsonProperty(value = "contEndDateBefore")
    private String contEndDateBefore;//合同到期日截止
    @JsonProperty(value = "page")
    private int page;//页数
    @JsonProperty(value = "size")
    private int size;//单页大小
    @JsonProperty(value = "tContYXSerno")
    private String tContYXSerno;//购销合同影像流水

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getQueryType() {
        return queryType;
    }

    public void setQueryType(String queryType) {
        this.queryType = queryType;
    }

    public String gettContNo() {
        return tContNo;
    }

    public void settContNo(String tContNo) {
        this.tContNo = tContNo;
    }

    public String gettContCnName() {
        return tContCnName;
    }

    public void settContCnName(String tContCnName) {
        this.tContCnName = tContCnName;
    }

    public String getContStartDateAfter() {
        return contStartDateAfter;
    }

    public void setContStartDateAfter(String contStartDateAfter) {
        this.contStartDateAfter = contStartDateAfter;
    }

    public String getContStartDateBerfore() {
        return contStartDateBerfore;
    }

    public void setContStartDateBerfore(String contStartDateBerfore) {
        this.contStartDateBerfore = contStartDateBerfore;
    }

    public String getContEndDateAfter() {
        return contEndDateAfter;
    }

    public void setContEndDateAfter(String contEndDateAfter) {
        this.contEndDateAfter = contEndDateAfter;
    }

    public String getContEndDateBefore() {
        return contEndDateBefore;
    }

    public void setContEndDateBefore(String contEndDateBefore) {
        this.contEndDateBefore = contEndDateBefore;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String gettContYXSerno() {
        return tContYXSerno;
    }

    public void settContYXSerno(String tContYXSerno) {
        this.tContYXSerno = tContYXSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "cusId='" + cusId + '\'' +
                ", queryType='" + queryType + '\'' +
                ", tContNo='" + tContNo + '\'' +
                ", tContCnName='" + tContCnName + '\'' +
                ", contStartDateAfter='" + contStartDateAfter + '\'' +
                ", contStartDateBerfore='" + contStartDateBerfore + '\'' +
                ", contEndDateAfter='" + contEndDateAfter + '\'' +
                ", contEndDateBefore='" + contEndDateBefore + '\'' +
                ", page=" + page +
                ", size=" + size +
                ", tContYXSerno='" + tContYXSerno + '\'' +
                '}';
    }
}
