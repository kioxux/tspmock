package cn.com.yusys.yusp.dto.server.biz.xdxw0012.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * <br>
 * 0.2ZRC:2021/5/18 13:39:<br>
 *
 * @author chenyong
 * @version 0.1
 * @date 2021/5/18 13:39
 * @since 2021/5/18 13:39
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "pdfDepoAddr")
    private String pdfDepoAddr;//pdf存放地址
    @JsonProperty(value = "pdfFileName")
    private String pdfFileName;//pdf文件名称
    @JsonProperty(value = "ftpAddr")
    private String ftpAddr;//ftp地址
    @JsonProperty(value = "port")
    private String port;//端口
    @JsonProperty(value = "userName")
    private String userName;//用户名
    @JsonProperty(value = "password")
    private String password;//密码

    public String getPdfDepoAddr() {
        return pdfDepoAddr;
    }

    public void setPdfDepoAddr(String pdfDepoAddr) {
        this.pdfDepoAddr = pdfDepoAddr;
    }

    public String getPdfFileName() {
        return pdfFileName;
    }

    public void setPdfFileName(String pdfFileName) {
        this.pdfFileName = pdfFileName;
    }

    public String getFtpAddr() {
        return ftpAddr;
    }

    public void setFtpAddr(String ftpAddr) {
        this.ftpAddr = ftpAddr;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Data{" +
                "pdfDepoAddr='" + pdfDepoAddr + '\'' +
                ", pdfFileName='" + pdfFileName + '\'' +
                ", ftpAddr='" + ftpAddr + '\'' +
                ", port='" + port + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
