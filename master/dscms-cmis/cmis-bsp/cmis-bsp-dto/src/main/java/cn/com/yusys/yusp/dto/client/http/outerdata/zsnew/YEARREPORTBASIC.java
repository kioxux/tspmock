package cn.com.yusys.yusp.dto.client.http.outerdata.zsnew;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

//	年报-企业年报基础信息
@JsonPropertyOrder(alphabetic = true)
public class YEARREPORTBASIC implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ADDR")
    private String ADDR;//	地址
    @JsonProperty(value = "ANCHEDATE")
    private String ANCHEDATE;//	年报日期
    @JsonProperty(value = "ANCHEID")
    private String ANCHEID;//	年报ID
    @JsonProperty(value = "ANCHEYEAR")
    private String ANCHEYEAR;//	年报年份
    @JsonProperty(value = "BUSST")
    private String BUSST;//	经营状态
    @JsonProperty(value = "CREDITNO")
    private String CREDITNO;//	统一信用代码
    @JsonProperty(value = "EMAIL")
    private String EMAIL;//	电子邮箱
    @JsonProperty(value = "ENTNAME")
    private String ENTNAME;//	企业名称
    @JsonProperty(value = "POSTALCODE")
    private String POSTALCODE;//	邮政编码
    @JsonProperty(value = "REGNO")
    private String REGNO;//	注册号

    @JsonIgnore
    public String getADDR() {
        return ADDR;
    }

    @JsonIgnore
    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    @JsonIgnore
    public String getANCHEDATE() {
        return ANCHEDATE;
    }

    @JsonIgnore
    public void setANCHEDATE(String ANCHEDATE) {
        this.ANCHEDATE = ANCHEDATE;
    }

    @JsonIgnore
    public String getANCHEID() {
        return ANCHEID;
    }

    @JsonIgnore
    public void setANCHEID(String ANCHEID) {
        this.ANCHEID = ANCHEID;
    }

    @JsonIgnore
    public String getANCHEYEAR() {
        return ANCHEYEAR;
    }

    @JsonIgnore
    public void setANCHEYEAR(String ANCHEYEAR) {
        this.ANCHEYEAR = ANCHEYEAR;
    }

    @JsonIgnore
    public String getBUSST() {
        return BUSST;
    }

    @JsonIgnore
    public void setBUSST(String BUSST) {
        this.BUSST = BUSST;
    }

    @JsonIgnore
    public String getCREDITNO() {
        return CREDITNO;
    }

    @JsonIgnore
    public void setCREDITNO(String CREDITNO) {
        this.CREDITNO = CREDITNO;
    }

    @JsonIgnore
    public String getEMAIL() {
        return EMAIL;
    }

    @JsonIgnore
    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    @JsonIgnore
    public String getENTNAME() {
        return ENTNAME;
    }

    @JsonIgnore
    public void setENTNAME(String ENTNAME) {
        this.ENTNAME = ENTNAME;
    }

    @JsonIgnore
    public String getPOSTALCODE() {
        return POSTALCODE;
    }

    @JsonIgnore
    public void setPOSTALCODE(String POSTALCODE) {
        this.POSTALCODE = POSTALCODE;
    }

    @JsonIgnore
    public String getREGNO() {
        return REGNO;
    }

    @JsonIgnore
    public void setREGNO(String REGNO) {
        this.REGNO = REGNO;
    }

    @Override
    public String toString() {
        return "YEARREPORTBASIC{" +
                "ADDR='" + ADDR + '\'' +
                ", ANCHEDATE='" + ANCHEDATE + '\'' +
                ", ANCHEID='" + ANCHEID + '\'' +
                ", ANCHEYEAR='" + ANCHEYEAR + '\'' +
                ", BUSST='" + BUSST + '\'' +
                ", CREDITNO='" + CREDITNO + '\'' +
                ", EMAIL='" + EMAIL + '\'' +
                ", ENTNAME='" + ENTNAME + '\'' +
                ", POSTALCODE='" + POSTALCODE + '\'' +
                ", REGNO='" + REGNO + '\'' +
                '}';
    }
}
