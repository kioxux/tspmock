package cn.com.yusys.yusp.dto.server.cus.xdkh0012.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * 请求Dto：对公客户信息同步
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "syccde")
    private String syccde;//同步系统编码
    @JsonProperty(value = "cusId")
    private String cusId;//客户编号
    @JsonProperty(value = "cusName")
    private String cusName;//客户名称
    @JsonProperty(value = "csstna")
    private String csstna;//客户简称
    @JsonProperty(value = "cusEnName")
    private String cusEnName;//客户英文名称
    @JsonProperty(value = "cusStatus")
    private String cusStatus;//客户状态
    @JsonProperty(value = "cusType")
    private String cusType;//客户类型
    @JsonProperty(value = "corpType")
    private String corpType;//对公客户类型细分
    @JsonProperty(value = "certType")
    private String certType;//证件类型
    @JsonProperty(value = "certCode")
    private String certCode;//证件号码
    @JsonProperty(value = "certStartDate")
    private String certStartDate;//证件生效日期
    @JsonProperty(value = "certEndDate")
    private String certEndDate;//证件失效日期
    @JsonProperty(value = "areaad")
    private String areaad;//发证机关国家或地区
    @JsonProperty(value = "depart")
    private String depart;//发证机关
    @JsonProperty(value = "gvrgno")
    private String gvrgno;//营业执照号码
    @JsonProperty(value = "gvbgdt")
    private String gvbgdt;//营业执照生效日期
    @JsonProperty(value = "gveddt")
    private String gveddt;//营业执照失效日期
    @JsonProperty(value = "gvtion")
    private String gvtion;//营业执照发证机关
    @JsonProperty(value = "cropcd")
    private String cropcd;//组织机构代码
    @JsonProperty(value = "oreddt")
    private String oreddt;//组织机构代码失效日期
    @JsonProperty(value = "nttxno")
    private String nttxno;//税务证件号
    @JsonProperty(value = "swbgdt")
    private String swbgdt;//税务证件生效日期
    @JsonProperty(value = "sweddt")
    private String sweddt;//税务证件失效日期
    @JsonProperty(value = "swtion")
    private String swtion;//税务证件发证机关
    @JsonProperty(value = "jgxyno")
    private String jgxyno;//机构信用代码证
    @JsonProperty(value = "cropdt")
    private String cropdt;//机构信用代码证失效日期
    @JsonProperty(value = "opcfno")
    private String opcfno;//开户许可证
    @JsonProperty(value = "basicDepAccNoOpenLic")
    private String basicDepAccNoOpenLic;//基本账户核准号
    @JsonProperty(value = "bulidDate")
    private String bulidDate;//成立日期
    @JsonProperty(value = "corppr")
    private String corppr;//行业类别
    @JsonProperty(value = "coppbc")
    private String coppbc;//人行行业类别
    @JsonProperty(value = "entetp")
    private String entetp;//产业分类
    @JsonProperty(value = "corpnu")
    private String corpnu;//企业性质
    @JsonProperty(value = "adminSubRel")
    private String adminSubRel;//隶属关系
    @JsonProperty(value = "propts")
    private String propts;//居民性质
    @JsonProperty(value = "holdType")
    private String holdType;//控股类型（出资人经济类型）
    @JsonProperty(value = "investMbody")
    private String investMbody;//投资主体
    @JsonProperty(value = "regiType")
    private String regiType;//登记注册类型
    @JsonProperty(value = "regiStartDate")
    private String regiStartDate;//注册日期
    @JsonProperty(value = "regicd")
    private String regicd;//注册地国家代码
    @JsonProperty(value = "regiAddr")
    private String regiAddr;//人民银行注册地区代码
    @JsonProperty(value = "rgctry")
    private String rgctry;//注册地行政区划
    @JsonProperty(value = "regiCurType")
    private String regiCurType;//注册资本币别
    @JsonProperty(value = "regiCapAmt")
    private BigDecimal regiCapAmt;//注册资本金额
    @JsonProperty(value = "paidCapCurType")
    private String paidCapCurType;//实收资本币别
    @JsonProperty(value = "paidCapAmt")
    private BigDecimal paidCapAmt;//实收资本金额
    @JsonProperty(value = "orgatp")
    private String orgatp;//组织机构类型
    @JsonProperty(value = "spectp")
    private String spectp;//特殊经济区类型
    @JsonProperty(value = "busstp")
    private String busstp;//企业经济类型
    @JsonProperty(value = "naticd")
    private String naticd;//国民经济部门
    @JsonProperty(value = "debttp")
    private String debttp;//境内主体类型
    @JsonProperty(value = "envirt")
    private String envirt;//企业环保级别
    @JsonProperty(value = "finacd")
    private String finacd;//金融机构类型代码
    @JsonProperty(value = "swifcd")
    private String swifcd;//SWIFT编号
    @JsonProperty(value = "spntcd")
    private String spntcd;//该SPV或壳机构所属国家/地区
    @JsonProperty(value = "operTerm")
    private String operTerm;//经营期限
    @JsonProperty(value = "operStatus")
    private String operStatus;//经营状态
    @JsonProperty(value = "corpScale")
    private String corpScale;//企业规模
    @JsonProperty(value = "commonOperPro")
    private String commonOperPro;//经营范围
    @JsonProperty(value = "acesbs")
    private String acesbs;//客户兼营业务
    @JsonProperty(value = "emplnm")
    private BigDecimal emplnm;//员工总数
    @JsonProperty(value = "comput")
    private String comput;//主管单位
    @JsonProperty(value = "upcrps")
    private String upcrps;//主管单位法人名称
    @JsonProperty(value = "upidtp")
    private String upidtp;//主管单位法人证件类型
    @JsonProperty(value = "upidno")
    private String upidno;//主管单位法人证件号码
    @JsonProperty(value = "loanno")
    private String loanno;//贷款卡编码
    @JsonProperty(value = "wkflar")
    private BigDecimal wkflar;//经营场地面积
    @JsonProperty(value = "operPlaceOwnshp")
    private String operPlaceOwnshp;//经营场地所有权
    @JsonProperty(value = "basicDepAccob")
    private String basicDepAccob;//基本账户开户行名称
    @JsonProperty(value = "basicDepAccobId")
    private String basicDepAccobId;//基本账户开户行号
    @JsonProperty(value = "basicDepAccNo")
    private String basicDepAccNo;//基本账户账号
    @JsonProperty(value = "basicAccNoOpenDate")
    private String basicAccNoOpenDate;//基本账户开户日期
    @JsonProperty(value = "depotp")
    private String depotp;//存款人类别
    @JsonProperty(value = "rhtype")
    private String rhtype;//人行统计分类
    @JsonProperty(value = "sumast")
    private BigDecimal sumast;//公司总资产
    @JsonProperty(value = "netast")
    private BigDecimal netast;//公司净资产
    @JsonProperty(value = "isBankShd")
    private String isBankShd;//是否我行股东
    @JsonProperty(value = "sharhd")
    private BigDecimal sharhd;//拥有我行股份金额
    @JsonProperty(value = "isStockCorp")
    private String isStockCorp;//是否上市企业
    @JsonProperty(value = "listld")
    private String listld;//上市地
    @JsonProperty(value = "stoctp")
    private String stoctp;//股票类别
    @JsonProperty(value = "stoccd")
    private String stoccd;//股票代码
    @JsonProperty(value = "onacnm")
    private BigDecimal onacnm;//一类户数量
    @JsonProperty(value = "totlnm")
    private BigDecimal totlnm;//总户数
    @JsonProperty(value = "grpctg")
    private String grpctg;//是否集团客户
    @JsonProperty(value = "reletg")
    private String reletg;//是否是我行关联客户
    @JsonProperty(value = "famltg")
    private String famltg;//是否家族企业
    @JsonProperty(value = "hightg")
    private String hightg;//是否高新技术企业
    @JsonProperty(value = "isbdcp")
    private String isbdcp;//是否是担保公司
    @JsonProperty(value = "spcltg")
    private String spcltg;//是否为特殊经济区内企业
    @JsonProperty(value = "areaPriorCopr")
    private String areaPriorCopr;//是否地区重点企业
    @JsonProperty(value = "finatg")
    private String finatg;//是否融资类企业
    @JsonProperty(value = "resutg")
    private String resutg;//是否国资委所属企业
    @JsonProperty(value = "isNatctl")
    private String isNatctl;//是否国家宏观调控限控行业
    @JsonProperty(value = "farmtg")
    private String farmtg;//是否农户
    @JsonProperty(value = "hgegtg")
    private String hgegtg;//是否高能耗企业
    @JsonProperty(value = "exegtg")
    private String exegtg;//是否产能过剩企业
    @JsonProperty(value = "elmntg")
    private String elmntg;//是否属于淘汰产能目录
    @JsonProperty(value = "envifg")
    private String envifg;//是否环保企业
    @JsonProperty(value = "hgplcp")
    private String hgplcp;//是否高污染企业
    @JsonProperty(value = "istdcs")
    private String istdcs;//是否是我行贸易融资客户
    @JsonProperty(value = "spOperFlag")
    private String spOperFlag;//特种经营标识
    @JsonProperty(value = "isSteelCus")
    private String isSteelCus;//是否钢贸企业
    @JsonProperty(value = "domitg")
    private String domitg;//是否优势企业
    @JsonProperty(value = "strutp")
    private String strutp;//产业结构调整类型
    @JsonProperty(value = "stratp")
    private String stratp;//战略新兴产业类型
    @JsonProperty(value = "indutg")
    private String indutg;//工业转型升级标识
    @JsonProperty(value = "leadtg")
    private String leadtg;//是否龙头企业
    @JsonProperty(value = "cncatg")
    private String cncatg;//中资标志
    @JsonProperty(value = "impexpFlag")
    private String impexpFlag;//进出口权标志
    @JsonProperty(value = "clostg")
    private String clostg;//企业关停标志
    @JsonProperty(value = "plattg")
    private String plattg;//是否苏州综合平台企业
    @JsonProperty(value = "fibrtg")
    private String fibrtg;//是否为工业园区、经济开发区等行政管理区
    @JsonProperty(value = "goverInvestPlat")
    private String goverInvestPlat;//是否政府投融资平台
    @JsonProperty(value = "gvlnlv")
    private String gvlnlv;//地方政府融资平台隶属关系
    @JsonProperty(value = "gvlwtp")
    private String gvlwtp;//地方政府融资平台法律性质
    @JsonProperty(value = "loanType")
    private String loanType;//政府融资贷款类型
    @JsonProperty(value = "petytg")
    private String petytg;//是否小额贷款公司
    @JsonProperty(value = "initLoanDate")
    private String initLoanDate;//首次与我行建立信贷关系时间
    @JsonProperty(value = "supptg")
    private String supptg;//是否列入重点扶持产业
    @JsonProperty(value = "licetg")
    private String licetg;//是否一照一码
    @JsonProperty(value = "landtg")
    private String landtg;//是否土地整治机构
    @JsonProperty(value = "pasvfg")
    private String pasvfg;//是否消极非金融机构
    @JsonProperty(value = "imagno")
    private String imagno;//影像编号
    @JsonProperty(value = "taxptp")
    private String taxptp;//纳税人身份
    @JsonProperty(value = "needfp")
    private String needfp;//是否需要开具增值税专用发票
    @JsonProperty(value = "taxpna")
    private String taxpna;//纳税人全称
    @JsonProperty(value = "taxpad")
    private String taxpad;//纳税人地址
    @JsonProperty(value = "taxptl")
    private String taxptl;//纳税人电话
    @JsonProperty(value = "txbkna")
    private String txbkna;//纳税人行名
    @JsonProperty(value = "taxpac")
    private String taxpac;//纳税人账号
    @JsonProperty(value = "txpstp")
    private String txpstp;//税收居民类型
    @JsonProperty(value = "taxadr")
    private String taxadr;//税收单位英文地址
    @JsonProperty(value = "txnion")
    private String txnion;//税收居民国（地区）
    @JsonProperty(value = "txennm")
    private String txennm;//税收英文姓名
    @JsonProperty(value = "txbcty")
    private String txbcty;//税收出生国家或地区
    @JsonProperty(value = "taxnum")
    private String taxnum;//纳税人识别号
    @JsonProperty(value = "notxrs")
    private String notxrs;//不能提供纳税人识别号原因
    @JsonProperty(value = "ctigno")
    private String ctigno;//客户经理号
    @JsonProperty(value = "ctigna")
    private String ctigna;//客户经理姓名
    @JsonProperty(value = "ctigph")
    private String ctigph;//客户经理手机号
    @JsonProperty(value = "corptl")
    private String corptl;//公司电话
    @JsonProperty(value = "hometl")
    private String hometl;//联系电话
    @JsonProperty(value = "rgpost")
    private String rgpost;//注册地址邮编
    @JsonProperty(value = "rgcuty")
    private String rgcuty;//注册地址所在国家
    @JsonProperty(value = "rgprov")
    private String rgprov;//注册地址所在省
    @JsonProperty(value = "rgcity")
    private String rgcity;//注册地址所在市
    @JsonProperty(value = "rgerea")
    private String rgerea;//注册地址所在区
    @JsonProperty(value = "regadr")
    private String regadr;//注册地址
    @JsonProperty(value = "bspost")
    private String bspost;//经营地址邮编
    @JsonProperty(value = "bscuty")
    private String bscuty;//经营地址所在国家
    @JsonProperty(value = "bsprov")
    private String bsprov;//经营地址所在省
    @JsonProperty(value = "bscity")
    private String bscity;//经营地址所在市
    @JsonProperty(value = "bserea")
    private String bserea;//经营地址所在区
    @JsonProperty(value = "busiad")
    private String busiad;//经营地址
    @JsonProperty(value = "enaddr")
    private String enaddr;//英文地址
    @JsonProperty(value = "websit")
    private String websit;//公司网址
    @JsonProperty(value = "fax")
    private String fax;//传真
    @JsonProperty(value = "email")
    private String email;//邮箱
    @JsonProperty(value = "sorgno")
    private String sorgno;//同业机构(行)号
    @JsonProperty(value = "sorgtp")
    private String sorgtp;//同业机构(行)类型
    @JsonProperty(value = "orgsit")
    private String orgsit;//同业机构(行)网址
    @JsonProperty(value = "bkplic")
    private String bkplic;//同业客户金融业务许可证
    @JsonProperty(value = "reguno")
    private String reguno;//同业非现场监管统计机构编码
    @JsonProperty(value = "pdcpat")
    private BigDecimal pdcpat;//同业客户实际到位资金(万元)
    @JsonProperty(value = "sorglv")
    private String sorglv;//同业授信评估等级
    @JsonProperty(value = "evalda")
    private String evalda;//同业客户评级到期日期
    @JsonProperty(value = "reldgr")
    private String reldgr;//同业客户与我行合作关系
    @JsonProperty(value = "limind")
    private String limind;//同业客户是否授信
    @JsonProperty(value = "mabrid")
    private String mabrid;//同业客户主管机构(同业)
    @JsonProperty(value = "manmgr")
    private String manmgr;//同业客户主管客户经理(同业)
    @JsonProperty(value = "briskc")
    private String briskc;//同业客户风险大类
    @JsonProperty(value = "mriskc")
    private String mriskc;//同业客户风险种类
    @JsonProperty(value = "friskc")
    private String friskc;//同业客户风险小类
    @JsonProperty(value = "riskda")
    private String riskda;//同业客户风险划分日期
    @JsonProperty(value = "listnm")
    private Integer listnm;//循环列表记录数
    @JsonProperty(value = "chflag")
    private String chflag;//境内标识
    @JsonProperty(value = "nnjytp")
    private String nnjytp;//交易类型
    @JsonProperty(value = "nnjyna")
    private String nnjyna;//交易名称
    @JsonProperty(value = "fullfg")
    private String fullfg;//信息完整性标识
    @JsonProperty(value = "chsdsc")
    private String chsdsc;//校验结果登记说明
    @JsonProperty(value = "reqList")
    private java.util.List<ReqList> reqList;

    public List<ReqList> getReqList() {
        return reqList;
    }

    public void setReqList(List<ReqList> reqList) {
        this.reqList = reqList;
    }

    public String getSyccde() {
        return syccde;
    }

    public void setSyccde(String syccde) {
        this.syccde = syccde;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCsstna() {
        return csstna;
    }

    public void setCsstna(String csstna) {
        this.csstna = csstna;
    }

    public String getCusEnName() {
        return cusEnName;
    }

    public void setCusEnName(String cusEnName) {
        this.cusEnName = cusEnName;
    }

    public String getCusStatus() {
        return cusStatus;
    }

    public void setCusStatus(String cusStatus) {
        this.cusStatus = cusStatus;
    }

    public String getCusType() {
        return cusType;
    }

    public void setCusType(String cusType) {
        this.cusType = cusType;
    }

    public String getCorpType() {
        return corpType;
    }

    public void setCorpType(String corpType) {
        this.corpType = corpType;
    }

    public String getCertType() {
        return certType;
    }

    public void setCertType(String certType) {
        this.certType = certType;
    }

    public String getCertCode() {
        return certCode;
    }

    public void setCertCode(String certCode) {
        this.certCode = certCode;
    }

    public String getCertStartDate() {
        return certStartDate;
    }

    public void setCertStartDate(String certStartDate) {
        this.certStartDate = certStartDate;
    }

    public String getCertEndDate() {
        return certEndDate;
    }

    public void setCertEndDate(String certEndDate) {
        this.certEndDate = certEndDate;
    }

    public String getAreaad() {
        return areaad;
    }

    public void setAreaad(String areaad) {
        this.areaad = areaad;
    }

    public String getDepart() {
        return depart;
    }

    public void setDepart(String depart) {
        this.depart = depart;
    }

    public String getGvrgno() {
        return gvrgno;
    }

    public void setGvrgno(String gvrgno) {
        this.gvrgno = gvrgno;
    }

    public String getGvbgdt() {
        return gvbgdt;
    }

    public void setGvbgdt(String gvbgdt) {
        this.gvbgdt = gvbgdt;
    }

    public String getGveddt() {
        return gveddt;
    }

    public void setGveddt(String gveddt) {
        this.gveddt = gveddt;
    }

    public String getGvtion() {
        return gvtion;
    }

    public void setGvtion(String gvtion) {
        this.gvtion = gvtion;
    }

    public String getCropcd() {
        return cropcd;
    }

    public void setCropcd(String cropcd) {
        this.cropcd = cropcd;
    }

    public String getOreddt() {
        return oreddt;
    }

    public void setOreddt(String oreddt) {
        this.oreddt = oreddt;
    }

    public String getNttxno() {
        return nttxno;
    }

    public void setNttxno(String nttxno) {
        this.nttxno = nttxno;
    }

    public String getSwbgdt() {
        return swbgdt;
    }

    public void setSwbgdt(String swbgdt) {
        this.swbgdt = swbgdt;
    }

    public String getSweddt() {
        return sweddt;
    }

    public void setSweddt(String sweddt) {
        this.sweddt = sweddt;
    }

    public String getSwtion() {
        return swtion;
    }

    public void setSwtion(String swtion) {
        this.swtion = swtion;
    }

    public String getJgxyno
            () {
        return jgxyno
                ;
    }

    public void setJgxyno
            (String jgxyno
            ) {
        this.jgxyno
                = jgxyno
        ;
    }

    public String getCropdt() {
        return cropdt;
    }

    public void setCropdt(String cropdt) {
        this.cropdt = cropdt;
    }

    public String getBasicDepAccNoOpenLic() {
        return basicDepAccNoOpenLic;
    }

    public void setBasicDepAccNoOpenLic(String basicDepAccNoOpenLic) {
        this.basicDepAccNoOpenLic = basicDepAccNoOpenLic;
    }

    public String getBulidDate() {
        return bulidDate;
    }

    public void setBulidDate(String bulidDate) {
        this.bulidDate = bulidDate;
    }

    public String getCorppr() {
        return corppr;
    }

    public void setCorppr(String corppr) {
        this.corppr = corppr;
    }

    public String getCoppbc() {
        return coppbc;
    }

    public void setCoppbc(String coppbc) {
        this.coppbc = coppbc;
    }

    public String getEntetp() {
        return entetp;
    }

    public void setEntetp(String entetp) {
        this.entetp = entetp;
    }

    public String getCorpnu() {
        return corpnu;
    }

    public void setCorpnu(String corpnu) {
        this.corpnu = corpnu;
    }

    public String getAdminSubRel() {
        return adminSubRel;
    }

    public void setAdminSubRel(String adminSubRel) {
        this.adminSubRel = adminSubRel;
    }

    public String getPropts() {
        return propts;
    }

    public void setPropts(String propts) {
        this.propts = propts;
    }

    public String getHoldType() {
        return holdType;
    }

    public void setHoldType(String holdType) {
        this.holdType = holdType;
    }

    public String getInvestMbody() {
        return investMbody;
    }

    public void setInvestMbody(String investMbody) {
        this.investMbody = investMbody;
    }

    public String getRegiType() {
        return regiType;
    }

    public void setRegiType(String regiType) {
        this.regiType = regiType;
    }

    public String getRegiStartDate() {
        return regiStartDate;
    }

    public void setRegiStartDate(String regiStartDate) {
        this.regiStartDate = regiStartDate;
    }

    public String getRegicd() {
        return regicd;
    }

    public void setRegicd(String regicd) {
        this.regicd = regicd;
    }

    public String getRegiAddr() {
        return regiAddr;
    }

    public void setRegiAddr(String regiAddr) {
        this.regiAddr = regiAddr;
    }

    public String getRgctry() {
        return rgctry;
    }

    public void setRgctry(String rgctry) {
        this.rgctry = rgctry;
    }

    public String getRegiCurType() {
        return regiCurType;
    }

    public void setRegiCurType(String regiCurType) {
        this.regiCurType = regiCurType;
    }

    public BigDecimal getRegiCapAmt() {
        return regiCapAmt;
    }

    public void setRegiCapAmt(BigDecimal regiCapAmt) {
        this.regiCapAmt = regiCapAmt;
    }

    public String getPaidCapCurType() {
        return paidCapCurType;
    }

    public void setPaidCapCurType(String paidCapCurType) {
        this.paidCapCurType = paidCapCurType;
    }

    public BigDecimal getPaidCapAmt() {
        return paidCapAmt;
    }

    public void setPaidCapAmt(BigDecimal paidCapAmt) {
        this.paidCapAmt = paidCapAmt;
    }

    public String getOrgatp() {
        return orgatp;
    }

    public void setOrgatp(String orgatp) {
        this.orgatp = orgatp;
    }

    public String getSpectp() {
        return spectp;
    }

    public void setSpectp(String spectp) {
        this.spectp = spectp;
    }

    public String getBusstp() {
        return busstp;
    }

    public void setBusstp(String busstp) {
        this.busstp = busstp;
    }

    public String getNaticd() {
        return naticd;
    }

    public void setNaticd(String naticd) {
        this.naticd = naticd;
    }

    public String getDebttp() {
        return debttp;
    }

    public void setDebttp(String debttp) {
        this.debttp = debttp;
    }

    public String getEnvirt() {
        return envirt;
    }

    public void setEnvirt(String envirt) {
        this.envirt = envirt;
    }

    public String getFinacd() {
        return finacd;
    }

    public void setFinacd(String finacd) {
        this.finacd = finacd;
    }

    public String getSwifcd() {
        return swifcd;
    }

    public void setSwifcd(String swifcd) {
        this.swifcd = swifcd;
    }

    public String getSpntcd() {
        return spntcd;
    }

    public void setSpntcd(String spntcd) {
        this.spntcd = spntcd;
    }

    public String getOperTerm() {
        return operTerm;
    }

    public void setOperTerm(String operTerm) {
        this.operTerm = operTerm;
    }

    public String getOperStatus() {
        return operStatus;
    }

    public void setOperStatus(String operStatus) {
        this.operStatus = operStatus;
    }

    public String getCorpScale() {
        return corpScale;
    }

    public void setCorpScale(String corpScale) {
        this.corpScale = corpScale;
    }

    public String getCommonOperPro() {
        return commonOperPro;
    }

    public void setCommonOperPro(String commonOperPro) {
        this.commonOperPro = commonOperPro;
    }

    public String getAcesbs() {
        return acesbs;
    }

    public void setAcesbs(String acesbs) {
        this.acesbs = acesbs;
    }

    public BigDecimal getEmplnm() {
        return emplnm;
    }

    public void setEmplnm(BigDecimal emplnm) {
        this.emplnm = emplnm;
    }

    public String getComput() {
        return comput;
    }

    public void setComput(String comput) {
        this.comput = comput;
    }

    public String getUpcrps() {
        return upcrps;
    }

    public void setUpcrps(String upcrps) {
        this.upcrps = upcrps;
    }

    public String getUpidtp() {
        return upidtp;
    }

    public void setUpidtp(String upidtp) {
        this.upidtp = upidtp;
    }

    public String getUpidno() {
        return upidno;
    }

    public void setUpidno(String upidno) {
        this.upidno = upidno;
    }

    public String getLoanno() {
        return loanno;
    }

    public void setLoanno(String loanno) {
        this.loanno = loanno;
    }

    public BigDecimal getWkflar() {
        return wkflar;
    }

    public void setWkflar(BigDecimal wkflar) {
        this.wkflar = wkflar;
    }

    public String getOperPlaceOwnshp() {
        return operPlaceOwnshp;
    }

    public void setOperPlaceOwnshp(String operPlaceOwnshp) {
        this.operPlaceOwnshp = operPlaceOwnshp;
    }

    public String getBasicDepAccob() {
        return basicDepAccob;
    }

    public void setBasicDepAccob(String basicDepAccob) {
        this.basicDepAccob = basicDepAccob;
    }

    public String getBasicDepAccobId() {
        return basicDepAccobId;
    }

    public void setBasicDepAccobId(String basicDepAccobId) {
        this.basicDepAccobId = basicDepAccobId;
    }

    public String getBasicDepAccNo() {
        return basicDepAccNo;
    }

    public void setBasicDepAccNo(String basicDepAccNo) {
        this.basicDepAccNo = basicDepAccNo;
    }

    public String getBasicAccNoOpenDate() {
        return basicAccNoOpenDate;
    }

    public void setBasicAccNoOpenDate(String basicAccNoOpenDate) {
        this.basicAccNoOpenDate = basicAccNoOpenDate;
    }

    public String getDepotp() {
        return depotp;
    }

    public void setDepotp(String depotp) {
        this.depotp = depotp;
    }

    public String getRhtype() {
        return rhtype;
    }

    public void setRhtype(String rhtype) {
        this.rhtype = rhtype;
    }

    public BigDecimal getSumast() {
        return sumast;
    }

    public void setSumast(BigDecimal sumast) {
        this.sumast = sumast;
    }

    public BigDecimal getNetast() {
        return netast;
    }

    public void setNetast(BigDecimal netast) {
        this.netast = netast;
    }

    public String getIsBankShd() {
        return isBankShd;
    }

    public void setIsBankShd(String isBankShd) {
        this.isBankShd = isBankShd;
    }

    public BigDecimal getSharhd() {
        return sharhd;
    }

    public void setSharhd(BigDecimal sharhd) {
        this.sharhd = sharhd;
    }

    public String getIsStockCorp() {
        return isStockCorp;
    }

    public void setIsStockCorp(String isStockCorp) {
        this.isStockCorp = isStockCorp;
    }

    public String getListld() {
        return listld;
    }

    public void setListld(String listld) {
        this.listld = listld;
    }

    public String getStoctp() {
        return stoctp;
    }

    public void setStoctp(String stoctp) {
        this.stoctp = stoctp;
    }

    public String getStoccd() {
        return stoccd;
    }

    public void setStoccd(String stoccd) {
        this.stoccd = stoccd;
    }

    public BigDecimal getOnacnm() {
        return onacnm;
    }

    public void setOnacnm(BigDecimal onacnm) {
        this.onacnm = onacnm;
    }

    public BigDecimal getTotlnm() {
        return totlnm;
    }

    public void setTotlnm(BigDecimal totlnm) {
        this.totlnm = totlnm;
    }

    public String getGrpctg() {
        return grpctg;
    }

    public void setGrpctg(String grpctg) {
        this.grpctg = grpctg;
    }

    public String getReletg() {
        return reletg;
    }

    public void setReletg(String reletg) {
        this.reletg = reletg;
    }

    public String getFamltg() {
        return famltg;
    }

    public void setFamltg(String famltg) {
        this.famltg = famltg;
    }

    public String getHightg() {
        return hightg;
    }

    public void setHightg(String hightg) {
        this.hightg = hightg;
    }

    public String getIsbdcp() {
        return isbdcp;
    }

    public void setIsbdcp(String isbdcp) {
        this.isbdcp = isbdcp;
    }

    public String getSpcltg() {
        return spcltg;
    }

    public void setSpcltg(String spcltg) {
        this.spcltg = spcltg;
    }

    public String getAreaPriorCopr() {
        return areaPriorCopr;
    }

    public void setAreaPriorCopr(String areaPriorCopr) {
        this.areaPriorCopr = areaPriorCopr;
    }

    public String getFinatg() {
        return finatg;
    }

    public void setFinatg(String finatg) {
        this.finatg = finatg;
    }

    public String getResutg() {
        return resutg;
    }

    public void setResutg(String resutg) {
        this.resutg = resutg;
    }

    public String getIsNatctl() {
        return isNatctl;
    }

    public void setIsNatctl(String isNatctl) {
        this.isNatctl = isNatctl;
    }

    public String getFarmtg() {
        return farmtg;
    }

    public void setFarmtg(String farmtg) {
        this.farmtg = farmtg;
    }

    public String getHgegtg() {
        return hgegtg;
    }

    public void setHgegtg(String hgegtg) {
        this.hgegtg = hgegtg;
    }

    public String getExegtg() {
        return exegtg;
    }

    public void setExegtg(String exegtg) {
        this.exegtg = exegtg;
    }

    public String getElmntg() {
        return elmntg;
    }

    public void setElmntg(String elmntg) {
        this.elmntg = elmntg;
    }

    public String getEnvifg() {
        return envifg;
    }

    public void setEnvifg(String envifg) {
        this.envifg = envifg;
    }

    public String getHgplcp() {
        return hgplcp;
    }

    public void setHgplcp(String hgplcp) {
        this.hgplcp = hgplcp;
    }

    public String getIstdcs() {
        return istdcs;
    }

    public void setIstdcs(String istdcs) {
        this.istdcs = istdcs;
    }

    public String getSpOperFlag() {
        return spOperFlag;
    }

    public void setSpOperFlag(String spOperFlag) {
        this.spOperFlag = spOperFlag;
    }

    public String getIsSteelCus() {
        return isSteelCus;
    }

    public void setIsSteelCus(String isSteelCus) {
        this.isSteelCus = isSteelCus;
    }

    public String getDomitg() {
        return domitg;
    }

    public void setDomitg(String domitg) {
        this.domitg = domitg;
    }

    public String getStrutp() {
        return strutp;
    }

    public void setStrutp(String strutp) {
        this.strutp = strutp;
    }

    public String getStratp() {
        return stratp;
    }

    public void setStratp(String stratp) {
        this.stratp = stratp;
    }

    public String getIndutg() {
        return indutg;
    }

    public void setIndutg(String indutg) {
        this.indutg = indutg;
    }

    public String getLeadtg() {
        return leadtg;
    }

    public void setLeadtg(String leadtg) {
        this.leadtg = leadtg;
    }

    public String getCncatg() {
        return cncatg;
    }

    public void setCncatg(String cncatg) {
        this.cncatg = cncatg;
    }

    public String getImpexpFlag() {
        return impexpFlag;
    }

    public void setImpexpFlag(String impexpFlag) {
        this.impexpFlag = impexpFlag;
    }

    public String getClostg() {
        return clostg;
    }

    public void setClostg(String clostg) {
        this.clostg = clostg;
    }

    public String getPlattg() {
        return plattg;
    }

    public void setPlattg(String plattg) {
        this.plattg = plattg;
    }

    public String getFibrtg() {
        return fibrtg;
    }

    public void setFibrtg(String fibrtg) {
        this.fibrtg = fibrtg;
    }

    public String getGoverInvestPlat() {
        return goverInvestPlat;
    }

    public void setGoverInvestPlat(String goverInvestPlat) {
        this.goverInvestPlat = goverInvestPlat;
    }

    public String getGvlnlv() {
        return gvlnlv;
    }

    public void setGvlnlv(String gvlnlv) {
        this.gvlnlv = gvlnlv;
    }

    public String getGvlwtp() {
        return gvlwtp;
    }

    public void setGvlwtp(String gvlwtp) {
        this.gvlwtp = gvlwtp;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getPetytg() {
        return petytg;
    }

    public void setPetytg(String petytg) {
        this.petytg = petytg;
    }

    public String getInitLoanDate() {
        return initLoanDate;
    }

    public void setInitLoanDate(String initLoanDate) {
        this.initLoanDate = initLoanDate;
    }

    public String getSupptg() {
        return supptg;
    }

    public void setSupptg(String supptg) {
        this.supptg = supptg;
    }

    public String getLicetg() {
        return licetg;
    }

    public void setLicetg(String licetg) {
        this.licetg = licetg;
    }

    public String getLandtg() {
        return landtg;
    }

    public void setLandtg(String landtg) {
        this.landtg = landtg;
    }

    public String getPasvfg() {
        return pasvfg;
    }

    public void setPasvfg(String pasvfg) {
        this.pasvfg = pasvfg;
    }

    public String getImagno() {
        return imagno;
    }

    public void setImagno(String imagno) {
        this.imagno = imagno;
    }

    public String getTaxptp() {
        return taxptp;
    }

    public void setTaxptp(String taxptp) {
        this.taxptp = taxptp;
    }

    public String getNeedfp() {
        return needfp;
    }

    public void setNeedfp(String needfp) {
        this.needfp = needfp;
    }

    public String getTaxpna() {
        return taxpna;
    }

    public void setTaxpna(String taxpna) {
        this.taxpna = taxpna;
    }

    public String getTaxpad() {
        return taxpad;
    }

    public void setTaxpad(String taxpad) {
        this.taxpad = taxpad;
    }

    public String getTaxptl() {
        return taxptl;
    }

    public void setTaxptl(String taxptl) {
        this.taxptl = taxptl;
    }

    public String getTxbkna() {
        return txbkna;
    }

    public void setTxbkna(String txbkna) {
        this.txbkna = txbkna;
    }

    public String getTaxpac() {
        return taxpac;
    }

    public void setTaxpac(String taxpac) {
        this.taxpac = taxpac;
    }

    public String getTxpstp() {
        return txpstp;
    }

    public void setTxpstp(String txpstp) {
        this.txpstp = txpstp;
    }

    public String getTaxadr() {
        return taxadr;
    }

    public void setTaxadr(String taxadr) {
        this.taxadr = taxadr;
    }

    public String getTxnion() {
        return txnion;
    }

    public void setTxnion(String txnion) {
        this.txnion = txnion;
    }

    public String getTxennm() {
        return txennm;
    }

    public void setTxennm(String txennm) {
        this.txennm = txennm;
    }

    public String getTxbcty() {
        return txbcty;
    }

    public void setTxbcty(String txbcty) {
        this.txbcty = txbcty;
    }

    public String getTaxnum() {
        return taxnum;
    }

    public void setTaxnum(String taxnum) {
        this.taxnum = taxnum;
    }

    public String getNotxrs() {
        return notxrs;
    }

    public void setNotxrs(String notxrs) {
        this.notxrs = notxrs;
    }

    public String getCtigno() {
        return ctigno;
    }

    public void setCtigno(String ctigno) {
        this.ctigno = ctigno;
    }

    public String getCtigna() {
        return ctigna;
    }

    public void setCtigna(String ctigna) {
        this.ctigna = ctigna;
    }

    public String getCtigph() {
        return ctigph;
    }

    public void setCtigph(String ctigph) {
        this.ctigph = ctigph;
    }

    public String getCorptl() {
        return corptl;
    }

    public void setCorptl(String corptl) {
        this.corptl = corptl;
    }

    public String getHometl() {
        return hometl;
    }

    public void setHometl(String hometl) {
        this.hometl = hometl;
    }

    public String getRgpost() {
        return rgpost;
    }

    public void setRgpost(String rgpost) {
        this.rgpost = rgpost;
    }

    public String getRgcuty() {
        return rgcuty;
    }

    public void setRgcuty(String rgcuty) {
        this.rgcuty = rgcuty;
    }

    public String getRgprov() {
        return rgprov;
    }

    public void setRgprov(String rgprov) {
        this.rgprov = rgprov;
    }

    public String getRgcity() {
        return rgcity;
    }

    public void setRgcity(String rgcity) {
        this.rgcity = rgcity;
    }

    public String getRgerea() {
        return rgerea;
    }

    public void setRgerea(String rgerea) {
        this.rgerea = rgerea;
    }

    public String getRegadr() {
        return regadr;
    }

    public void setRegadr(String regadr) {
        this.regadr = regadr;
    }

    public String getBspost() {
        return bspost;
    }

    public void setBspost(String bspost) {
        this.bspost = bspost;
    }

    public String getBscuty() {
        return bscuty;
    }

    public void setBscuty(String bscuty) {
        this.bscuty = bscuty;
    }

    public String getBsprov() {
        return bsprov;
    }

    public void setBsprov(String bsprov) {
        this.bsprov = bsprov;
    }

    public String getBscity() {
        return bscity;
    }

    public void setBscity(String bscity) {
        this.bscity = bscity;
    }

    public String getBserea() {
        return bserea;
    }

    public void setBserea(String bserea) {
        this.bserea = bserea;
    }

    public String getBusiad() {
        return busiad;
    }

    public void setBusiad(String busiad) {
        this.busiad = busiad;
    }

    public String getEnaddr() {
        return enaddr;
    }

    public void setEnaddr(String enaddr) {
        this.enaddr = enaddr;
    }

    public String getWebsit() {
        return websit;
    }

    public void setWebsit(String websit) {
        this.websit = websit;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSorgno() {
        return sorgno;
    }

    public void setSorgno(String sorgno) {
        this.sorgno = sorgno;
    }

    public String getSorgtp() {
        return sorgtp;
    }

    public void setSorgtp(String sorgtp) {
        this.sorgtp = sorgtp;
    }

    public String getOrgsit() {
        return orgsit;
    }

    public void setOrgsit(String orgsit) {
        this.orgsit = orgsit;
    }

    public String getBkplic() {
        return bkplic;
    }

    public void setBkplic(String bkplic) {
        this.bkplic = bkplic;
    }

    public String getReguno() {
        return reguno;
    }

    public void setReguno(String reguno) {
        this.reguno = reguno;
    }

    public BigDecimal getPdcpat() {
        return pdcpat;
    }

    public void setPdcpat(BigDecimal pdcpat) {
        this.pdcpat = pdcpat;
    }

    public String getSorglv() {
        return sorglv;
    }

    public void setSorglv(String sorglv) {
        this.sorglv = sorglv;
    }

    public String getEvalda() {
        return evalda;
    }

    public void setEvalda(String evalda) {
        this.evalda = evalda;
    }

    public String getReldgr() {
        return reldgr;
    }

    public void setReldgr(String reldgr) {
        this.reldgr = reldgr;
    }

    public String getLimind() {
        return limind;
    }

    public void setLimind(String limind) {
        this.limind = limind;
    }

    public String getMabrid() {
        return mabrid;
    }

    public void setMabrid(String mabrid) {
        this.mabrid = mabrid;
    }

    public String getManmgr() {
        return manmgr;
    }

    public void setManmgr(String manmgr) {
        this.manmgr = manmgr;
    }

    public String getBriskc() {
        return briskc;
    }

    public void setBriskc(String briskc) {
        this.briskc = briskc;
    }

    public String getMriskc() {
        return mriskc;
    }

    public void setMriskc(String mriskc) {
        this.mriskc = mriskc;
    }

    public String getFriskc() {
        return friskc;
    }

    public void setFriskc(String friskc) {
        this.friskc = friskc;
    }

    public String getRiskda() {
        return riskda;
    }

    public void setRiskda(String riskda) {
        this.riskda = riskda;
    }

    public Integer getListnm() {
        return listnm;
    }

    public void setListnm(Integer listnm) {
        this.listnm = listnm;
    }

    public String getChflag() {
        return chflag;
    }

    public void setChflag(String chflag) {
        this.chflag = chflag;
    }

    public String getNnjytp() {
        return nnjytp;
    }

    public void setNnjytp(String nnjytp) {
        this.nnjytp = nnjytp;
    }

    public String getNnjyna() {
        return nnjyna;
    }

    public void setNnjyna(String nnjyna) {
        this.nnjyna = nnjyna;
    }

    public String getFullfg() {
        return fullfg;
    }

    public void setFullfg(String fullfg) {
        this.fullfg = fullfg;
    }

    public String getChsdsc() {
        return chsdsc;
    }

    public void setChsdsc(String chsdsc) {
        this.chsdsc = chsdsc;
    }

    public String getOpcfno() {
        return opcfno;
    }

    public void setOpcfno(String opcfno) {
        this.opcfno = opcfno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "syccde='" + syccde + '\'' +
                ", cusId='" + cusId + '\'' +
                ", cusName='" + cusName + '\'' +
                ", csstna='" + csstna + '\'' +
                ", cusEnName='" + cusEnName + '\'' +
                ", cusStatus='" + cusStatus + '\'' +
                ", cusType='" + cusType + '\'' +
                ", corpType='" + corpType + '\'' +
                ", certType='" + certType + '\'' +
                ", certCode='" + certCode + '\'' +
                ", certStartDate='" + certStartDate + '\'' +
                ", certEndDate='" + certEndDate + '\'' +
                ", areaad='" + areaad + '\'' +
                ", depart='" + depart + '\'' +
                ", gvrgno='" + gvrgno + '\'' +
                ", gvbgdt='" + gvbgdt + '\'' +
                ", gveddt='" + gveddt + '\'' +
                ", gvtion='" + gvtion + '\'' +
                ", cropcd='" + cropcd + '\'' +
                ", oreddt='" + oreddt + '\'' +
                ", nttxno='" + nttxno + '\'' +
                ", swbgdt='" + swbgdt + '\'' +
                ", sweddt='" + sweddt + '\'' +
                ", swtion='" + swtion + '\'' +
                ", jgxyno='" + jgxyno + '\'' +
                ", cropdt='" + cropdt + '\'' +
                ", opcfno='" + opcfno + '\'' +
                ", basicDepAccNoOpenLic='" + basicDepAccNoOpenLic + '\'' +
                ", bulidDate='" + bulidDate + '\'' +
                ", corppr='" + corppr + '\'' +
                ", coppbc='" + coppbc + '\'' +
                ", entetp='" + entetp + '\'' +
                ", corpnu='" + corpnu + '\'' +
                ", adminSubRel='" + adminSubRel + '\'' +
                ", propts='" + propts + '\'' +
                ", holdType='" + holdType + '\'' +
                ", investMbody='" + investMbody + '\'' +
                ", regiType='" + regiType + '\'' +
                ", regiStartDate='" + regiStartDate + '\'' +
                ", regicd='" + regicd + '\'' +
                ", regiAddr='" + regiAddr + '\'' +
                ", rgctry='" + rgctry + '\'' +
                ", regiCurType='" + regiCurType + '\'' +
                ", regiCapAmt=" + regiCapAmt +
                ", paidCapCurType='" + paidCapCurType + '\'' +
                ", paidCapAmt=" + paidCapAmt +
                ", orgatp='" + orgatp + '\'' +
                ", spectp='" + spectp + '\'' +
                ", busstp='" + busstp + '\'' +
                ", naticd='" + naticd + '\'' +
                ", debttp='" + debttp + '\'' +
                ", envirt='" + envirt + '\'' +
                ", finacd='" + finacd + '\'' +
                ", swifcd='" + swifcd + '\'' +
                ", spntcd='" + spntcd + '\'' +
                ", operTerm='" + operTerm + '\'' +
                ", operStatus='" + operStatus + '\'' +
                ", corpScale='" + corpScale + '\'' +
                ", commonOperPro='" + commonOperPro + '\'' +
                ", acesbs='" + acesbs + '\'' +
                ", emplnm=" + emplnm +
                ", comput='" + comput + '\'' +
                ", upcrps='" + upcrps + '\'' +
                ", upidtp='" + upidtp + '\'' +
                ", upidno='" + upidno + '\'' +
                ", loanno='" + loanno + '\'' +
                ", wkflar=" + wkflar +
                ", operPlaceOwnshp='" + operPlaceOwnshp + '\'' +
                ", basicDepAccob='" + basicDepAccob + '\'' +
                ", basicDepAccobId='" + basicDepAccobId + '\'' +
                ", basicDepAccNo='" + basicDepAccNo + '\'' +
                ", basicAccNoOpenDate='" + basicAccNoOpenDate + '\'' +
                ", depotp='" + depotp + '\'' +
                ", rhtype='" + rhtype + '\'' +
                ", sumast=" + sumast +
                ", netast=" + netast +
                ", isBankShd='" + isBankShd + '\'' +
                ", sharhd=" + sharhd +
                ", isStockCorp='" + isStockCorp + '\'' +
                ", listld='" + listld + '\'' +
                ", stoctp='" + stoctp + '\'' +
                ", stoccd='" + stoccd + '\'' +
                ", onacnm=" + onacnm +
                ", totlnm=" + totlnm +
                ", grpctg='" + grpctg + '\'' +
                ", reletg='" + reletg + '\'' +
                ", famltg='" + famltg + '\'' +
                ", hightg='" + hightg + '\'' +
                ", isbdcp='" + isbdcp + '\'' +
                ", spcltg='" + spcltg + '\'' +
                ", areaPriorCopr='" + areaPriorCopr + '\'' +
                ", finatg='" + finatg + '\'' +
                ", resutg='" + resutg + '\'' +
                ", isNatctl='" + isNatctl + '\'' +
                ", farmtg='" + farmtg + '\'' +
                ", hgegtg='" + hgegtg + '\'' +
                ", exegtg='" + exegtg + '\'' +
                ", elmntg='" + elmntg + '\'' +
                ", envifg='" + envifg + '\'' +
                ", hgplcp='" + hgplcp + '\'' +
                ", istdcs='" + istdcs + '\'' +
                ", spOperFlag='" + spOperFlag + '\'' +
                ", isSteelCus='" + isSteelCus + '\'' +
                ", domitg='" + domitg + '\'' +
                ", strutp='" + strutp + '\'' +
                ", stratp='" + stratp + '\'' +
                ", indutg='" + indutg + '\'' +
                ", leadtg='" + leadtg + '\'' +
                ", cncatg='" + cncatg + '\'' +
                ", impexpFlag='" + impexpFlag + '\'' +
                ", clostg='" + clostg + '\'' +
                ", plattg='" + plattg + '\'' +
                ", fibrtg='" + fibrtg + '\'' +
                ", goverInvestPlat='" + goverInvestPlat + '\'' +
                ", gvlnlv='" + gvlnlv + '\'' +
                ", gvlwtp='" + gvlwtp + '\'' +
                ", loanType='" + loanType + '\'' +
                ", petytg='" + petytg + '\'' +
                ", initLoanDate='" + initLoanDate + '\'' +
                ", supptg='" + supptg + '\'' +
                ", licetg='" + licetg + '\'' +
                ", landtg='" + landtg + '\'' +
                ", pasvfg='" + pasvfg + '\'' +
                ", imagno='" + imagno + '\'' +
                ", taxptp='" + taxptp + '\'' +
                ", needfp='" + needfp + '\'' +
                ", taxpna='" + taxpna + '\'' +
                ", taxpad='" + taxpad + '\'' +
                ", taxptl='" + taxptl + '\'' +
                ", txbkna='" + txbkna + '\'' +
                ", taxpac='" + taxpac + '\'' +
                ", txpstp='" + txpstp + '\'' +
                ", taxadr='" + taxadr + '\'' +
                ", txnion='" + txnion + '\'' +
                ", txennm='" + txennm + '\'' +
                ", txbcty='" + txbcty + '\'' +
                ", taxnum='" + taxnum + '\'' +
                ", notxrs='" + notxrs + '\'' +
                ", ctigno='" + ctigno + '\'' +
                ", ctigna='" + ctigna + '\'' +
                ", ctigph='" + ctigph + '\'' +
                ", corptl='" + corptl + '\'' +
                ", hometl='" + hometl + '\'' +
                ", rgpost='" + rgpost + '\'' +
                ", rgcuty='" + rgcuty + '\'' +
                ", rgprov='" + rgprov + '\'' +
                ", rgcity='" + rgcity + '\'' +
                ", rgerea='" + rgerea + '\'' +
                ", regadr='" + regadr + '\'' +
                ", bspost='" + bspost + '\'' +
                ", bscuty='" + bscuty + '\'' +
                ", bsprov='" + bsprov + '\'' +
                ", bscity='" + bscity + '\'' +
                ", bserea='" + bserea + '\'' +
                ", busiad='" + busiad + '\'' +
                ", enaddr='" + enaddr + '\'' +
                ", websit='" + websit + '\'' +
                ", fax='" + fax + '\'' +
                ", email='" + email + '\'' +
                ", sorgno='" + sorgno + '\'' +
                ", sorgtp='" + sorgtp + '\'' +
                ", orgsit='" + orgsit + '\'' +
                ", bkplic='" + bkplic + '\'' +
                ", reguno='" + reguno + '\'' +
                ", pdcpat=" + pdcpat +
                ", sorglv='" + sorglv + '\'' +
                ", evalda='" + evalda + '\'' +
                ", reldgr='" + reldgr + '\'' +
                ", limind='" + limind + '\'' +
                ", mabrid='" + mabrid + '\'' +
                ", manmgr='" + manmgr + '\'' +
                ", briskc='" + briskc + '\'' +
                ", mriskc='" + mriskc + '\'' +
                ", friskc='" + friskc + '\'' +
                ", riskda='" + riskda + '\'' +
                ", listnm=" + listnm +
                ", chflag='" + chflag + '\'' +
                ", nnjytp='" + nnjytp + '\'' +
                ", nnjyna='" + nnjyna + '\'' +
                ", fullfg='" + fullfg + '\'' +
                ", chsdsc='" + chsdsc + '\'' +
                ", reqList=" + reqList +
                '}';
    }
}