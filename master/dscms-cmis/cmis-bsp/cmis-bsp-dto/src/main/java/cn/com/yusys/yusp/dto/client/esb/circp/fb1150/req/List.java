package cn.com.yusys.yusp.dto.client.esb.circp.fb1150.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * @author chenyong
 * @version 0.1
 * @date 2021/8/11 10:52
 * @since 2021/8/11 10:52
 */
@JsonPropertyOrder(alphabetic = true)
public class List implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "shareholder")
    private String shareholder;//股东名称

    public String getShareholder() {
        return shareholder;
    }

    public void setShareholder(String shareholder) {
        this.shareholder = shareholder;
    }

    @Override
    public String toString() {
        return "List{" +
                "shareholder='" + shareholder + '\'' +
                '}';
    }
}
