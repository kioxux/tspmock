package cn.com.yusys.yusp.dto.client.esb.rircp.fbxw05;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应DTO：惠享贷批复同步接口
 *
 * @author jijian
 * @version 1.0
 * @since 2021/4/16下午8:04:38
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxw05RespDto implements Serializable {

    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "ols_tran_no")
    private String ols_tran_no; // 交易流水号
    @JsonProperty(value = "ols_date")
    private String ols_date; // 交易日期

    public String getOls_tran_no() {
        return ols_tran_no;
    }

    public void setOls_tran_no(String ols_tran_no) {
        this.ols_tran_no = ols_tran_no;
    }

    public String getOls_date() {
        return ols_date;
    }

    public void setOls_date(String ols_date) {
        this.ols_date = ols_date;
    }

    @Override
    public String toString() {
        return "Fbxw05RespDto{" +
                "ols_tran_no='" + ols_tran_no + '\'' +
                ", ols_date='" + ols_date + '\'' +
                '}';
    }
}