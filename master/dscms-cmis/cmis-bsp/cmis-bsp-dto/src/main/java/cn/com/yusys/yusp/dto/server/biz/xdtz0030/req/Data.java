package cn.com.yusys.yusp.dto.server.biz.xdtz0030.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 请求Dto：查看信贷贴现台账中票据是否已经存在
 *
 * @author zhugenrong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "porderNo")
    private String porderNo;//汇票号码

    public String getPorderNo() {
        return porderNo;
    }

    public void setPorderNo(String porderNo) {
        this.porderNo = porderNo;
    }

    @Override
    public String toString() {
        return "Data{" +
                "porderNo='" + porderNo + '\'' +
                '}';
    }
}
