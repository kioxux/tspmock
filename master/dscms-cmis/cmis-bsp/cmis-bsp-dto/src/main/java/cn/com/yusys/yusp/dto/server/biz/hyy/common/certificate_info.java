package cn.com.yusys.yusp.dto.server.biz.hyy.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 表单
 */
@JsonPropertyOrder(alphabetic = true)
public class certificate_info implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "certificate")
    private int certificate;// 押品登记证明
}
