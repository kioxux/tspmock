package cn.com.yusys.yusp.dto.client.esb.zjywxt.clfxcx.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：协议信息查询
 *
 * @author chenyong
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class ClfxcxRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "jgzh")
    private String jgzh;//资金托管账号
    @JsonProperty(value = "zzhxh")
    private String zzhxh;//子账户序号
    @JsonProperty(value = "qyrq")
    private String qyrq;//签约日期
    @JsonProperty(value = "zxdm")
    private String zxdm;//房产中心区域代码

    public String getJgzh() {
        return jgzh;
    }

    public void setJgzh(String jgzh) {
        this.jgzh = jgzh;
    }

    public String getZzhxh() {
        return zzhxh;
    }

    public void setZzhxh(String zzhxh) {
        this.zzhxh = zzhxh;
    }

    public String getQyrq() {
        return qyrq;
    }

    public void setQyrq(String qyrq) {
        this.qyrq = qyrq;
    }

    public String getZxdm() {
        return zxdm;
    }

    public void setZxdm(String zxdm) {
        this.zxdm = zxdm;
    }

    @Override
    public String toString() {
        return "ClfxcxRespDto{" +
                "jgzh='" + jgzh + '\'' +
                "zzhxh='" + zzhxh + '\'' +
                "qyrq='" + qyrq + '\'' +
                "zxdm='" + zxdm + '\'' +
                '}';
    }
}  
