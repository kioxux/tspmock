package cn.com.yusys.yusp.dto.client.esb.rircp.fbxd13;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：查询客户核心编号，客户名称对应的放款和还款信息
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Fbxd13RespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "list")
    private java.util.List<List> list;//list

    public java.util.List<List> getList() {
        return list;
    }

    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "Fbxd13RespDto{" +
                "list=" + list +
                '}';
    }
}
