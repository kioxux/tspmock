package cn.com.yusys.yusp.dto.server.biz.xdtz0037.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：无还本续贷额度状态更新
 * @Author zhangpeng
 * @Date 2021/4/27 13:56
 * @Version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "zxSerno")
    private String zxSerno;//放款笔数

    public String getZxSerno() {
        return zxSerno;
    }

    public void setZxSerno(String zxSerno) {
        this.zxSerno = zxSerno;
    }

    @Override
    public String toString() {
        return "Data{" +
                "zxSerno='" + zxSerno + '\'' +
                '}';
    }
}
