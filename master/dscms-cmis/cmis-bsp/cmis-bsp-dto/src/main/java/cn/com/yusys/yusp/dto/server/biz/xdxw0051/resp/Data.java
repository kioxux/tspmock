package cn.com.yusys.yusp.dto.server.biz.xdxw0051.resp;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder(alphabetic = true)
public class Data implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "saleAmt")
    private String saleAmt;//销售收入

    public String getSaleAmt() {
        return saleAmt;
    }

    public void setSaleAmt(String saleAmt) {
        this.saleAmt = saleAmt;
    }

    @Override
    public String toString() {
        return "Data{" +
                "saleAmt='" + saleAmt + '\'' +
                '}';
    }
}
