package cn.com.yusys.yusp.dto.client.esb.core.ln3176;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 请求Dto：本交易用户贷款多个还款账户进行还款
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class LstLnHuankDZhhObj implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "huankzhh")
    private String huankzhh;//还款账号
    @JsonProperty(value = "hkzhhzxh")
    private String hkzhhzxh;//还款账号子序号
    @JsonProperty(value = "huobdhao")
    private String huobdhao;//货币代号
    @JsonProperty(value = "huankjee")
    private BigDecimal huankjee;//还款金额

    public String getHuankzhh() {
        return huankzhh;
    }

    public void setHuankzhh(String huankzhh) {
        this.huankzhh = huankzhh;
    }

    public String getHkzhhzxh() {
        return hkzhhzxh;
    }

    public void setHkzhhzxh(String hkzhhzxh) {
        this.hkzhhzxh = hkzhhzxh;
    }

    public String getHuobdhao() {
        return huobdhao;
    }

    public void setHuobdhao(String huobdhao) {
        this.huobdhao = huobdhao;
    }

    public BigDecimal getHuankjee() {
        return huankjee;
    }

    public void setHuankjee(BigDecimal huankjee) {
        this.huankjee = huankjee;
    }

    @Override
    public String toString() {
        return "LstLnHuankDZhhObj{" +
                "huankzhh='" + huankzhh + '\'' +
                ", hkzhhzxh='" + hkzhhzxh + '\'' +
                ", huobdhao='" + huobdhao + '\'' +
                ", huankjee=" + huankjee +
                '}';
    }
}
