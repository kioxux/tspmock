package cn.com.yusys.yusp.dto.server.biz.hyy.babigc0102.resp;

import cn.com.yusys.yusp.dto.server.HyyRespDto;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

/**
 * 响应Dto：获取抵押信息详情
 *
 * @author leehuang
 * @version 1.0
 */
@JsonPropertyOrder(alphabetic = true)
public class Babigc0102RespDto extends HyyRespDto implements Serializable {
    private static final long serialVersionUID = 1L;
    @JsonProperty(value = "data")
    private Data data;
    @JsonProperty(value = "count")
    private Integer count;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Babigc01RespDto{" +
                "data=" + data +
                "count=" + count +
                '}';
    }
}
